#pragma once


#include "Score.h"


class CDatabaseConnection;
class CInsertionBatch;


struct SRating
{
	f64 Value = 0;
	f64 Accuracy = 0;
};

struct SIntelligence
{
	bool IsSuspicious = false;
	f64 BestPerformance = 0;
	f64 MedianPerformance = 0;
};

class CUser
{
public:


	void AddScore(SScoreLight score)
	{
		_scores.push_back(score);
	}

	void AppendToBatch(u32 sizeThreshold, CInsertionBatch& batch);


	size_t NumScores() const { return _scores.size(); }

	SRating ComputeRating();
	SIntelligence ComputeIntelligence();

	SScoreLight XthBestScore(unsigned int i);

private:

	SRating _rating;

	std::vector<SScoreLight> _scores;
};