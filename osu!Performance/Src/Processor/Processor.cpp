#include <PrecompiledHeader.h>


#include "Processor.h"
#include "Score.h"
#include "SharedEnums.h"
#include "User.h"

#include "Standard/StandardScore.h"
#include "CatchTheBeat/CatchTheBeatScore.h"
#include "Taiko/TaikoScore.h"
#include "Mania/ManiaScore.h"

#include "../Shared/Config.h"

#include "../Shared/Network/DatabaseConnection.h"
#include "../Shared/Network/InsertionBatch.h"
#include "../Shared/Network/PreparedStatement.h"
#include "../Shared/Network/QueryResult.h"
#include "../Shared/Network/UpdateBatch.h"


using namespace SharedEnums;
using namespace std::chrono;

const std::string CProcessor::s_configFile = "./Data/Config.cfg";


const std::array<const std::string, AmountGamemodes> CProcessor::s_gamemodeSuffixes =
{
	"",
	"_taiko",
	"_fruits",
	"_mania",
};

const std::array<const std::string, AmountGamemodes> CProcessor::s_gamemodeNames =
{
	"osu!",
	"Taiko",
	"Catch the Beat",
	"osu!mania",
};

const std::array<const std::string, AmountGamemodes> CProcessor::s_gamemodeTags =
{
	"osu",
	"taiko",
	"catch_the_beat",
	"osu_mania",
};



CProcessor::CProcessor(EGamemode gamemode, bool reProcess)
:
_gamemode{gamemode},
_config{s_configFile},
_dataDog{"127.0.0.1", 8125}
{
	Log(CLog::None,           "---------------------------------------------------");
	Log(CLog::None, StrFormat("---- pp processor for gamemode {0}", GamemodeName(gamemode)));
	Log(CLog::None,           "---------------------------------------------------");

	try
	{
		Run(reProcess);
	}
	catch (CException& e)
	{
#ifndef PLAYER_TESTING
		_curl.SendToSentry(
			_config.SentryDomain,
			_config.SentryProjectID,
			_config.SentryPublicKey,
			_config.SentryPrivateKey,
			e,
			GamemodeName(gamemode)
		);
#endif

		// We just logged this exception. Let's re-throw since this wasn't actually handled.
		throw;
	}
}

CProcessor::~CProcessor()
{
	_shallShutdown = true;

	if(_backgroundProcessThread.joinable())
	{
		_backgroundProcessThread.join();
	}
}

std::shared_ptr<CDatabaseConnection> CProcessor::NewDBConnectionMaster()
{
	return std::make_shared<CDatabaseConnection>(
		_config.MySQL_db_host,
		_config.MySQL_db_port,
		_config.MySQL_db_username,
		_config.MySQL_db_password,
		_config.MySQL_db_database);
}

std::shared_ptr<CDatabaseConnection> CProcessor::NewDBConnectionSlave()
{
	return std::make_shared<CDatabaseConnection>(
		_config.MySQL_db_slave_host,
		_config.MySQL_db_slave_port,
		_config.MySQL_db_slave_username,
		_config.MySQL_db_slave_password,
		_config.MySQL_db_slave_database);
}

void CProcessor::Run(bool reProcess)
{
	_dataDog.Increment("osu.pp.startups", 1, {StrFormat("mode:{0}", GamemodeTag(_gamemode))});

	// Force reprocess if there are more than 10,000 scores to catch up to
	static const s64 s_maximumScoreIdDifference = 10000;

	_pDB = NewDBConnectionMaster();
	_pDBSlave = NewDBConnectionSlave();


	// Obtain current maximum score id so we have a starting point
	auto res = _pDBSlave->Query(StrFormat(
		"SELECT MAX(`score_id`) FROM `osu_scores{0}_high` WHERE 1",
		GamemodeSuffix(_gamemode)));

	if(!res.NextRow())
	{
		throw CProcessorException(
			SRC_POS,
			StrFormat("Couldn't find maximum score id for mode {0}.", GamemodeName(_gamemode)));
	}

	_currentScoreId = res.S64(0);
	s64 lastScoreId = RetrieveCount(*_pDB, LastScoreIdKey());
	
	// If we are too far behind, then force a reprocess
	if(_currentScoreId - lastScoreId > s_maximumScoreIdDifference)
	{
		reProcess = true;

		// We should immediately store where we left off to make sure on restart we won't skip things by choosing the maximum again
		StoreCount(*_pDB, LastScoreIdKey(), _currentScoreId);
	}
	// Otherwise take the value where we stopped last time and resume polling
	else
	{
		_currentScoreId = lastScoreId;
	}

	res = _pDBSlave->Query("SELECT MAX(`approved_date`) FROM `osu_beatmapsets` WHERE 1");

	if(!res.NextRow())
	{
		throw CProcessorException(
			SRC_POS,
			"Couldn't find maximum approved date.");
	}

	_lastApprovedDate = res.String(0);

	QueryBeatmapBlacklist();
	QueryBeatmapDifficultyAttributes();
	QueryBeatmapDifficulty();

#ifdef PLAYER_TESTING
	CUpdateBatch newUsers{_pDB, 0};  // We want them updates to occur immediately
	CUpdateBatch newScores{_pDB, 0}; // batches are used to conform the interface of ProcessSingleUser

	ProcessSingleUser(
		true,
		0,
		*_pDB,
		*_pDBSlave,
		newUsers,
		newScores,
		PLAYER_TESTING);

#else
	ProcessAllScores(reProcess);

	_lastScorePollTime = system_clock::now();
	_lastBeatmapSetPollTime = system_clock::now();

	// Main loop
	while(true)
	{
		if(Update() == false)
		{
			break;
		}

		// Avoid to have the processor "spinning" at full power if there is no work to do in Update()
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
#endif

	Log(CLog::Info, "Shutting down.");
}



bool CProcessor::Update()
{
	if(system_clock::now() - _lastScorePollTime >
	   milliseconds{_config.ScoreUpdateInterval})
	{
		PollAndProcessNewScores();
	}

	if(system_clock::now() - _lastBeatmapSetPollTime >
		milliseconds{_config.DifficultyUpdateInterval})
	{
		PollAndProcessNewBeatmapSets();
	}

	return true;
}

void CProcessor::QueryBeatmapDifficulty()
{
	static const s32 step = 10000;

	s32 begin = 0;
	while(QueryBeatmapDifficulty(begin, begin + step))
	{
		begin += step;

		// This keeps our connections alive during the long running difficulty retrieval.
		_pDB->ping();
		_pDBSlave->ping();
	}

	Log(CLog::Success, StrFormat("Loaded difficulties for a total of {0} beatmaps.", _beatmaps.size()));
}

bool CProcessor::QueryBeatmapDifficulty(s32 startId, s32 endId)
{
	auto pDBSlave = NewDBConnectionSlave();

	std::string query = StrFormat(
		"SELECT `osu_beatmaps`.`beatmap_id`,`countNormal`,`mods`,`attrib_id`,`value` "
		"FROM `osu_beatmaps` "
		"JOIN `osu_beatmap_difficulty_attribs` ON `osu_beatmaps`.`beatmap_id` = `osu_beatmap_difficulty_attribs`.`beatmap_id` "
		"WHERE `osu_beatmap_difficulty_attribs`.`mode`={0} AND `approved`>=1",
		_gamemode);

	if(endId == 0)
	{
		query += StrFormat(" AND `osu_beatmaps`.`beatmap_id`={0}", startId);
	}
	else
	{
		query += StrFormat(" AND `osu_beatmaps`.`beatmap_id`>={0} AND `osu_beatmaps`.`beatmap_id`<{1}", startId, endId);
	}

	auto res = pDBSlave->Query(query);

	bool success = true;

	if(res.AmountRows() == 0)
	{
		success = false;
	}

	while(res.NextRow())
	{
		s32 id = res.S32(0);

		if(_beatmaps.count(id) == 0)
		{
			_beatmaps.emplace(std::make_pair(id, id));
		}

		auto& beatmap = _beatmaps.at(id);

		beatmap.SetAmountNormal(res.IsNull(1) ? 0 : res.S32(1));
		beatmap.SetDiffAttribute(
			static_cast<EMods>(res.U32(2)),
			_difficultyAttributes[res.S32(3)],
			res.F32(4));
	}

	if(endId == 0)
	{
		if(_beatmaps.count(startId) == 0)
		{
			std::string message = StrFormat("Couldn't find beatmap /b/{0}.", startId);

			Log(CLog::Warning, message.c_str());
			_dataDog.Increment("osu.pp.difficulty.retrieval_not_found", 1, {StrFormat("mode:{0}", GamemodeTag(_gamemode))});

			/*CProcessorException e{SRC_POS, message};

			m_CURL.SendToSentry(
				m_Config.SentryDomain,
				m_Config.SentryProjectID,
				m_Config.SentryPublicKey,
				m_Config.SentryPrivateKey,
				e,
				GamemodeName(m_Gamemode)
			);*/

			success = false;
		}
		else
		{
			Log(CLog::Success, StrFormat("Obtained beatmap difficulty of /b/{0}.", startId));
			_dataDog.Increment("osu.pp.difficulty.retrieval_success", 1, { StrFormat("mode:{0}", GamemodeTag(_gamemode)) });
		}
	}
	else
	{
		Log(CLog::Success, StrFormat("Obtained beatmap difficulties from ID {0} to {1}.", startId, endId - 1));
	}

	return success;
}



void CProcessor::PollAndProcessNewScores()
{
	static const s64 s_lastScoreIdUpdateStep = 100;

	CUpdateBatch newUsers{_pDB, 0};  // We want them updates to occur immediately
	CUpdateBatch newScores{_pDB, 0}; // batches are used to conform the interface of ProcessSingleUser

	// Obtain all new scores since the last poll and process them
	auto res = _pDB->Query(StrFormat(
		"SELECT `score_id`,`user_id`,`pp` FROM `osu_scores{0}_high` WHERE `score_id` > {1} ORDER BY `score_id` ASC",
		GamemodeSuffix(_gamemode), _currentScoreId));

	// Only reset the poll timer when we find nothing. Otherwise we want to directly keep going
	if(res.AmountRows() == 0)
	{
		_lastScorePollTime = system_clock::now();
	}

	_dataDog.Gauge("osu.pp.score.amount_behind_newest", res.AmountRows(), {StrFormat("mode:{0}", GamemodeTag(_gamemode))});

	while(res.NextRow())
	{
		// Only process scores where pp is still null.
		if(!res.IsNull(2))
		{
			continue;
		}

		s64 ScoreId = res.S64(0);
		s64 UserId = res.S64(1);

		_currentScoreId = max_(_currentScoreId, ScoreId);

		Log(CLog::Info, StrFormat("New score {0} in mode {1} by {2}.", ScoreId, GamemodeName(_gamemode), UserId));

		ProcessSingleUser(
			true,    // High priority on lock. Polling should be real-time
			ScoreId, // Only update the new score, old ones are caught by the background processor anyways
			*_pDB,
			*_pDB,
			newUsers,
			newScores,
			UserId);

		++_amountScoresProcessedSinceLastStore;
		if(_amountScoresProcessedSinceLastStore > s_lastScoreIdUpdateStep)
		{
			StoreCount(*_pDB, LastScoreIdKey(), _currentScoreId);
			_amountScoresProcessedSinceLastStore = 0;
		}

		_dataDog.Increment("osu.pp.score.processed_new", 1, {StrFormat("mode:{0}", GamemodeTag(_gamemode))});
	}
}

void CProcessor::PollAndProcessNewBeatmapSets()
{
	_lastBeatmapSetPollTime = system_clock::now();

	Log(CLog::Info, "Retrieving new beatmap sets.");

	auto res = _pDBSlave->Query(StrFormat(
		"SELECT `beatmap_id`, `approved_date` "
		"FROM `osu_beatmapsets` JOIN `osu_beatmaps` ON `osu_beatmapsets`.`beatmapset_id` = `osu_beatmaps`.`beatmapset_id` "
		"WHERE `approved_date` > '{0}' "
		"ORDER BY `approved_date` ASC",
		_lastApprovedDate));

	Log(CLog::Success, StrFormat("Retrieved {0} new beatmaps.", res.AmountRows()));

	CPriorityLock lock{ &_beatmapMutex, true };

	while(res.NextRow())
	{
		_lastApprovedDate = res.String(1);
		QueryBeatmapDifficulty(res.S32(0));

		_dataDog.Increment("osu.pp.difficulty.required_retrieval", 1, { StrFormat("mode:{0}", GamemodeTag(_gamemode)) });
	}
}


void CProcessor::ProcessAllScores(bool reProcess, bool force)
{
	if(force == false)
	{
		if(_backgroundProcessThread.joinable())
		{
			_backgroundProcessThread.join();
		}

		_backgroundProcessThread =
			std::thread{[reProcess, this]() { this->ProcessAllScores(reProcess, true); }};

		return;
	}

	auto pDB = NewDBConnectionMaster();
	auto pDBSlave = NewDBConnectionSlave();

	static const s32 userIdStep = 10000;

	s64 begin; // Will be initialized in the next few lines
	if(reProcess)
	{
		begin = 0;

		// Make sure in case of a restart we still do the full process, even if we didn't trigger a store before
		StoreCount(*pDB, LastUserIdKey(), begin);
	}
	else
	{
		begin = RetrieveCount(*pDB, LastUserIdKey());
	}

	// We're done, nothing to reprocess
	if(begin == -1)
	{
		return;
	}


	Log(CLog::Info, StrFormat("Querying all scores, starting from user id {0}.", begin));

	// We will break out as soon as there are no more results
	while(true)
	{
		s64 end = begin + userIdStep;
		Log(CLog::Info, StrFormat("Updating users {0} - {1}.", begin, end));

		CUpdateBatch newUsers{pDB, 10000};
		CUpdateBatch newScores{pDB, 10000};

		auto res = pDBSlave->Query(StrFormat(
			"SELECT "
			"`user_id`"
			"FROM `phpbb_users` "
			"WHERE `user_id` BETWEEN {0} AND {1}", begin, end));

		// We are done if there are no users left
		if(res.AmountRows() == 0)
		{
			break;
		}

		while(res.NextRow())
		{
			s64 userId = res.S64(0);

			ProcessSingleUser(
				false, // Low priority on lock - this is running in the background
				0,     // We want to update all scores
				*pDB,
				*pDBSlave,
				newUsers,
				newScores,
				userId);

			// Shut down when requested!
			if(_shallShutdown)
			{
				return;
			}
		}

		begin += userIdStep;

		// Update our user_id counter
		StoreCount(*pDB, LastUserIdKey(), begin);
	}
}


std::unique_ptr<CScore> CProcessor::NewScore(s64 scoreId,
											 SharedEnums::EGamemode mode,
											 s32 userId,
											 s32 beatmapId,
											 s32 score,
											 s32 maxCombo,
											 s32 amount300,
											 s32 amount100,
											 s32 amount50,
											 s32 amountMiss,
											 s32 amountGeki,
											 s32 amountKatu,
											 SharedEnums::EMods mods)
{
#define SCORE_INITIALIZER_LIST \
	/* Score id */ scoreId, \
	/* Mode */ mode, \
	/* Player id */ userId, \
	/* Beatmap id */ beatmapId, \
	/* Score */ score, \
	/* Maxcombo */ maxCombo, \
	/* Amount300 */ amount300, \
	/* Amount100 */ amount100, \
	/* Amount50 */ amount50, \
	/* AmountMiss */ amountMiss, \
	/* AmountGeki */ amountGeki, \
	/* AmountKatu */ amountKatu, \
	/* Mods */ mods, \
	/* Beatmap */ _beatmaps.at(beatmapId)

	std::unique_ptr<CScore> pScore = nullptr;

	// Create the correct score type, so the right formulas are used
	switch(_gamemode)
	{
	case EGamemode::Standard:
		pScore = std::make_unique<CStandardScore>(SCORE_INITIALIZER_LIST);
		break;

	case EGamemode::Taiko:
		pScore = std::make_unique<CTaikoScore>(SCORE_INITIALIZER_LIST);
		break;

	case EGamemode::CatchTheBeat:
		pScore = std::make_unique<CCatchTheBeatScore>(SCORE_INITIALIZER_LIST);
		break;

	case EGamemode::Mania:
		pScore = std::make_unique<CManiaScore>(SCORE_INITIALIZER_LIST);
		break;

	default:
		throw CProcessorException(SRC_POS, StrFormat("Unknown gamemode requested. ({0})", _gamemode));
	}

#undef SCORE_INITIALIZER_LIST

	return pScore;
}


void CProcessor::QueryBeatmapBlacklist()
{
	Log(CLog::Info, "Retrieving blacklisted beatmaps.");

	auto res = _pDBSlave->Query(StrFormat(
		"SELECT `beatmap_id` "
		"FROM `osu_beatmap_performance_blacklist` "
		"WHERE `mode`={0}", _gamemode));

	while(res.NextRow())
	{
		_blacklistedBeatmapIds.insert(res.S32(0));
	}

	Log(CLog::Success, StrFormat("Retrieved {0} blacklisted beatmaps.", _blacklistedBeatmapIds.size()));
}

void CProcessor::QueryBeatmapDifficultyAttributes()
{
	Log(CLog::Info, "Retrieving difficulty attribute names.");

	u32 amountNames = 0;

	auto res = _pDBSlave->Query("SELECT `attrib_id`,`name` FROM `osu_difficulty_attribs` WHERE 1 ORDER BY `attrib_id` DESC");
	while(res.NextRow())
	{
		u32 id = res.S32(0);
		if(_difficultyAttributes.size() < id + 1)
		{
			_difficultyAttributes.resize(id + 1);
		}

		_difficultyAttributes[id] = CBeatmap::DifficultyAttributeFromName(res.String(1));
		++amountNames;
	}

	Log(CLog::Success, StrFormat("Retrieved {0} difficulty attributes, stored in {1} entries.", amountNames, _difficultyAttributes.size()));
}


void CProcessor::ProcessSingleUser(
	bool highPriority,
	s64 selectedScoreId,
	CDatabaseConnection& db,
	CDatabaseConnection& dbSlave,
	CUpdateBatch& newUsers,
	CUpdateBatch& newScores,
	s64 userId)
{
	static const f32 s_notableEventRatingThreshold = 1.0f / 21.5f;
	static const f32 s_notableEventRatingDifferenceMinimum = 5.0f;

	auto res = dbSlave.Query(StrFormat(
		"SELECT "
		"`score_id`,"
		"`user_id`,"
		"`beatmap_id`,"
		"`score`,"
		"`maxcombo`,"
		"`count300`,"
		"`count100`,"
		"`count50`,"
		"`countmiss`,"
		"`countgeki`,"
		"`countkatu`,"
		"`enabled_mods`,"
		"`pp` "
		"FROM `osu_scores{0}_high` "
		"WHERE `user_id`={1}", GamemodeSuffix(_gamemode), userId));

	CUser user{};
	std::vector<std::unique_ptr<CScore>> scoresThatNeedDBUpdate;

	{
		CPriorityLock lock{&_beatmapMutex, highPriority};

		// Process the data we got
		while(res.NextRow())
		{
			s64 scoreId = res.S64(0);
			s32 beatmapId = res.S32(2);

			EMods mods = static_cast<EMods>(res.S32(11));

			// Blacklisted maps don't count
			if(_blacklistedBeatmapIds.count(beatmapId) > 0)
			{
				continue;
			}

			// We don't want to look at scores on beatmaps we have no information about
			if(_beatmaps.count(beatmapId) == 0)
			{
				//Log(CLog::Warning, StrFormat("No difficulty information of beatmap {0} available. Ignoring for calculation.", BeatmapId));
				QueryBeatmapDifficulty(beatmapId);

				// If after querying we still didn't find anything, then we can just leave it.
				if(_beatmaps.count(beatmapId) == 0)
				{
					continue;
				}
			}


#define SCORE_INITIALIZER_LIST \
	/* Score id */ scoreId, \
	/* Mode */ _gamemode, \
	/* Player id */ res.S32(1), \
	/* Beatmap id */ beatmapId, \
	/* Score */ res.S32(3), \
	/* Maxcombo */ res.S32(4), \
	/* Amount300 */ res.S32(5), \
	/* Amount100 */ res.S32(6), \
	/* Amount50 */ res.S32(7), \
	/* AmountMiss */ res.S32(8), \
	/* AmountGeki */ res.S32(9), \
	/* AmountKatu */ res.S32(10), \
	/* Mods */ mods

			std::unique_ptr<CScore> pScore = NewScore(SCORE_INITIALIZER_LIST);

#undef SCORE_INITIALIZER_LIST

			user.AddScore(pScore->LightVersion());

			if(res.IsNull(12) || selectedScoreId == 0 || selectedScoreId == scoreId)
			{
				// Column 12 is the pp value of the score from the database.
				// Only update score if it differs a lot!
				if(res.IsNull(12) || fabs(res.F32(12) - pScore->TotalValue()) > 0.001f)
				{
					scoresThatNeedDBUpdate.emplace_back(std::move(pScore));
				}
			}
		}
	}

#ifndef PLAYER_TESTING
	// Update scores *outside* of the lock
	for(const auto& pScore : scoresThatNeedDBUpdate)
	{
		pScore->AppendToUpdateBatch(newScores);
	}

	_dataDog.Increment("osu.pp.score.updated", scoresThatNeedDBUpdate.size(), {StrFormat("mode:{0}", GamemodeTag(_gamemode))});
#endif
	

	SRating userRating = user.ComputeRating();

	if(selectedScoreId == user.XthBestScore(0).ScoreId)
	{
		SIntelligence intelligence = user.ComputeIntelligence();
		if(intelligence.IsSuspicious)
		{
			std::string message = StrFormat("Suspicious best performance distribution for http://osu.ppy.sh/u/{0}#_hax on {1}. (Best: {2}, Median: {3})",
				userId,
				GamemodeName(_gamemode),
				intelligence.BestPerformance,
				intelligence.MedianPerformance);

			_curl.SendToSlack(
				_config.SlackHookDomain,
				_config.SlackHookKey,
				_config.SlackHookUsername,
				_config.SlackHookIconURL,
				_config.SlackHookChannel,
				message);

			Log(CLog::Info, message + ""); // "+ \"\"" little hack to make it an RValue. (Log doesn't support LValues, I need to fix that.)
		}
	}

	

#ifdef PLAYER_TESTING
	Log(CLog::Info, StrFormat("pp of {0}: {1}, {2}%", PLAYER_TESTING, userRating.Value, userRating.Accuracy));

	for (int i = 0; i < 10; ++i)
	{
		const auto& score = user.XthBestScore(i);
		Log(CLog::Info, StrFormat("{0}: {1} - {2}", i + 1, score.Value, score.ScoreId));
	}

	return;
#endif

	// Check for notable event
	if(selectedScoreId > 0 && // We only check for notable events if a score has been selected
	   !scoresThatNeedDBUpdate.empty() && // Did the score actually get found (this _should_ never be false, but better make sure)
	   scoresThatNeedDBUpdate.front()->TotalValue() > userRating.Value * s_notableEventRatingThreshold)
	{
		_dataDog.Increment("osu.pp.score.notable_events", 1, {StrFormat("mode:{0}", GamemodeTag(_gamemode))});

		const auto& pScore = scoresThatNeedDBUpdate.front();

		// Obtain user's previous pp rating for determining the difference
		auto res = dbSlave.Query(StrFormat(
			"SELECT `{0}` FROM `osu_user_stats{1}` WHERE `user_id`={2}",
			std::string{_config.UserPPColumnName},
			GamemodeSuffix(_gamemode),
			userId));

		while(res.NextRow())
		{
			if(res.IsNull(0))
			{
				continue;
			}

			f64 ratingChange = userRating.Value - res.F32(0);

			// We don't want to log scores, that give less than a mere 5 pp
			if(ratingChange < s_notableEventRatingDifferenceMinimum)
			{
				continue;
			}

			Log(CLog::Info, StrFormat("Notable event: /b/{0} /u/{1}", pScore->BeatmapId(), userId));

			db.NonQuery(StrFormat(
				"INSERT INTO "
				"osu_user_performance_change(user_id, mode, beatmap_id, performance_change, rank) "
				"VALUES({0},{1},{2},{3},null)",
				userId,
				_gamemode,
				pScore->BeatmapId(),
				ratingChange));
		}
	}

	newUsers.AppendAndCommit(StrFormat(
		"UPDATE `osu_user_stats{0}` "
		"SET `{1}`= CASE "
			// Set pp to 0 if the user is inactive or restricted.
			"WHEN (CURDATE() > DATE_ADD(`last_played`, INTERVAL 3 MONTH) OR (SELECT `user_warnings` FROM `phpbb_users` WHERE `user_id`={4}) > 0) THEN 0 "
			"ELSE {2} "
		"END,"
		"`accuracy_new`={3} "
		"WHERE `user_id`={4} AND ABS(`{1}` - {2}) > 0.01;",
		GamemodeSuffix(_gamemode),
		std::string{_config.UserPPColumnName},
		userRating.Value,
		userRating.Accuracy,
		userId));

	_dataDog.Increment("osu.pp.user.amount_processed", 1, {StrFormat("mode:{0}", GamemodeTag(_gamemode))});
}

void CProcessor::StoreCount(CDatabaseConnection& db, std::string key, s64 value)
{
	db.NonQuery(StrFormat(
		"INSERT INTO `osu_counts`(`name`,`count`) VALUES('{0}',{1}) "
		"ON DUPLICATE KEY UPDATE `name`=VALUES(`name`),`count`=VALUES(`count`)",
		key, value));
}

s64 CProcessor::RetrieveCount(CDatabaseConnection& db, std::string key)
{
	auto res = db.Query(StrFormat(
		"SELECT `count` FROM `osu_counts` WHERE `name`='{0}'", key));

	while(res.NextRow())
	{
		if(!res.IsNull(0))
		{
			return res.S64(0);
		}
	}

	return -1;
}


