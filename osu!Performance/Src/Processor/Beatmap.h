#pragma once



#include "SharedEnums.h"


DEFINE_LOGGED_EXCEPTION(CBeatmapException);



class CInsertionBatch;


class CBeatmap
{
public:

	CBeatmap(s32 id);

	enum EDifficultyAttribute : byte
	{
		Aim = 0,
		Speed,
		OD,
		AR,
		MaxCombo,
		Strain,
		HitWindow300,
		ScoreMultiplier,
	};

	s32 Id() const { return _id; }

	s32 AmountNormal() const { return _amountNormal; }
	f32 Difficulty(SharedEnums::EMods mods, EDifficultyAttribute type) const;

	void SetAmountNormal(s32 amountNormal) { _amountNormal = amountNormal; }
	void SetDiffAttribute(SharedEnums::EMods mods, EDifficultyAttribute name, f32 value);

	static EDifficultyAttribute DifficultyAttributeFromName(const std::string& difficultyAttributeName)
	{
		return s_difficultyAttributes.at(difficultyAttributeName);
	}

	using difficulty_t =
		std::unordered_map<std::underlying_type_t<SharedEnums::EMods>, std::unordered_map<std::underlying_type_t<EDifficultyAttribute>, f32>>;

private:

	static const SharedEnums::EMods s_relevantDifficultyMods = static_cast<SharedEnums::EMods>(
		SharedEnums::EMods::DoubleTime |
		SharedEnums::EMods::HalfTime |
		SharedEnums::EMods::HardRock |
		SharedEnums::EMods::Easy |
		SharedEnums::EMods::keyMod);

	static SharedEnums::EMods MaskRelevantDifficultyMods(SharedEnums::EMods mods)
	{
		return static_cast<SharedEnums::EMods>(mods & s_relevantDifficultyMods);
	}

	static const std::unordered_map<std::string, EDifficultyAttribute> s_difficultyAttributes;

	// General information
	s32 _id;
	SharedEnums::EGamemode _mode = SharedEnums::EGamemode::Standard;

	// Calculated difficulty
	difficulty_t _difficulty;

	// Additional info required for processor
	s32 _amountNormal = 0;
};