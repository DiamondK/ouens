#include <PrecompiledHeader.h>


#include "Processor.h"

#include "Score.h"
#include "SharedEnums.h"

#include "../Shared/Network/InsertionBatch.h"
#include "../Shared/Network/UpdateBatch.h"


using namespace SharedEnums;


CScore::CScore(
	s64 scoreId,
	EGamemode mode,
	s32 userId,
	s32 beatmapId,
	s32 score,
	s32 maxCombo,
	s32 amount300,
	s32 amount100,
	s32 amount50,
	s32 amountMiss,
	s32 amountGeki,
	s32 amountKatu,
	EMods mods)
:
_scoreId{scoreId},
_mode{mode},
_userId{userId},
_beatmapId{beatmapId},
_score{max_(0, score)},
_maxCombo{max_(0, maxCombo)},
_amount300{max_(0, amount300)},
_amount100{max_(0, amount100)},
_amount50{max_(0, amount50)},
_amountMiss{max_(0, amountMiss)},
_amountGeki{max_(0, amountGeki)},
_amountKatu{max_(0, amountKatu)},
_mods{mods}
{
}




void CScore::AppendToUpdateBatch(CUpdateBatch& batch)
{
	batch.AppendAndCommit(StrFormat(
		"UPDATE `osu_scores{0}_high` "
		"SET `pp`={1} "
		"WHERE `score_id`={2};",
		CProcessor::GamemodeSuffix(_mode),
		TotalValue(),
		_scoreId));
}

