#pragma once



class CSendBuffer;
class CRingBuffer;

class ISerializable
{
public:



	virtual void Serialize(CSendBuffer& SendBuffer) const = 0;
	virtual void Deserialize(CRingBuffer& RecvBuffer) = 0;


};