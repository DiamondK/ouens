#pragma once



class CDatabaseConnection;

class CInsertionBatch
{
public:

	CInsertionBatch(std::shared_ptr<CDatabaseConnection> pDB, std::string table, std::vector<std::string> columns, u32 sizeThreshold);
	~CInsertionBatch();

	void Append(const std::string& values)
	{
		_empty = false;
		_query += std::string("(") + values + "),";
	}

	void AppendAndCommit(const std::string& values);

	void Reset();
	const std::string& Query();
	

	u32 Size() const { return _query.size(); }

private:

	u32 _sizeThreshold;

	bool _empty = true;

	std::shared_ptr<CDatabaseConnection> _pDB;

	std::string _query;

	std::string _table;
	std::vector<std::string> _columns;
};
