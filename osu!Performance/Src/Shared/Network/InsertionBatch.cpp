#include <Shared.h>

#include "DatabaseConnection.h"
#include "InsertionBatch.h"


CInsertionBatch::CInsertionBatch(
	std::shared_ptr<CDatabaseConnection> pDB,
	std::string Table,
	std::vector<std::string> Columns,
	u32 SizeThreshold)
:
_pDB{std::move(pDB)},
_table{std::move(Table)},
_columns{std::move(Columns)},
_sizeThreshold{SizeThreshold}
{
	Reset();
}

CInsertionBatch::~CInsertionBatch()
{
	// If we are not empty we want to commit what's left in here
	if(!_empty)
	{
		_pDB->NonQuery(Query());
	}
}

void CInsertionBatch::AppendAndCommit(const std::string& Values)
{
	Append(Values);

	if(Size() > _sizeThreshold)
	{
		_pDB->NonQuery(Query());
		Reset();
	}
}


void CInsertionBatch::Reset()
{
	_query = StrFormat("INSERT INTO `{0}`(", _table);

	for(const auto& Column : _columns)
	{
		_query += std::string("`") + Column + "`,";
	}

	// Remove the last comma written!
	_query.pop_back();
	_query += ") VALUES";

	_empty = true;
}


const std::string& CInsertionBatch::Query()
{
	// Remove the last comma written!
	_query.pop_back();

	_query += "ON DUPLICATE KEY UPDATE ";

	for(const auto& Column : _columns)
	{
		_query += std::string("`") + Column + "`=VALUES(`" + Column + "`),";
	}

	// Remove the last comma written!
	_query.pop_back();

	return _query;
}
