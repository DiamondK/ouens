#include <Shared.h>

#include "DatabaseConnection.h"
#include "PreparedStatement.h"


CDatabaseConnection::CDatabaseConnection(const char* szHost,
										 s16        wPort,
										 const char* szUsername,
										 const char* szPassword,
										 const char* szDatabase)
	: m_dwQueriesAmount(0)
{
	

	m_pMySQL = new MYSQL;



	if(!mysql_init(m_pMySQL))
	{
		// Clean up in case of error
		delete m_pMySQL;

		throw CDatabaseException(SRC_POS, StrFormat("MySQL struct could not be initialized. ({0})", get_Error()));
	}


	connect(szHost, wPort, szUsername, szPassword, szDatabase);
}

CDatabaseConnection::~CDatabaseConnection()
{
	mysql_close(m_pMySQL);
	delete m_pMySQL;
}


void CDatabaseConnection::connect(const char* szHost,
								  s16        wPort,
								  const char* szUsername,
								  const char* szPassword,
								  const char* szDatabase)
{

	if(!mysql_real_connect(m_pMySQL, szHost, szUsername, szPassword, szDatabase, wPort, NULL, CLIENT_MULTI_STATEMENTS))
	{
		throw CDatabaseException(SRC_POS, StrFormat("Could not connect. ({0})", get_Error()));
	}
}



std::unique_ptr<CPreparedStatement> CDatabaseConnection::prepare(const std::string& Str)
{
	MYSQL_STMT* pStmt = mysql_stmt_init(m_pMySQL);
	if(pStmt == NULL)
	{
		throw CDatabaseException(SRC_POS, StrFormat("Error initializing prepared statement {0}.", get_Error()));
	}

	if(mysql_stmt_prepare(pStmt, Str.c_str(), Str.length()) != 0)
	{
		throw CDatabaseException(SRC_POS, StrFormat("Error preparing statement query {0}. ({1})", get_Error(), Str));
	}

	return std::unique_ptr<CPreparedStatement>(new CPreparedStatement{pStmt});
}



void CDatabaseConnection::NonQuery(const std::string& _str)
{
	m_dwQueriesAmount++;

	s32 Status = mysql_query(m_pMySQL, _str.c_str());
	if(Status != 0)
	{
		throw CDatabaseException(SRC_POS, StrFormat("Error executing query {0}. ({1})", _str, get_Error()));
	}


	do
	{
		/* did current statement return data? */
		MYSQL_RES* pRes = mysql_store_result(m_pMySQL);
		if(pRes != NULL)
		{
			mysql_free_result(pRes);
		}
		else          /* no result set or error */
		{
			if(mysql_field_count(m_pMySQL) == 0)
			{
			}
			else  /* some error occurred */
			{
				throw CDatabaseException(SRC_POS, StrFormat("Error getting result. ({0})", get_Error()));
			}
		}
		/* more results? -1 = no, >0 = error, 0 = yes (keep looping) */

		Status = mysql_next_result(m_pMySQL);
		if(Status > 0)
		{
			throw CDatabaseException(SRC_POS, StrFormat("Error executing query {0}. ({1})", _str, get_Error()));
		}
	}
	while(Status == 0);
}

CQueryResult CDatabaseConnection::Query(const std::string& _str)
{
	m_dwQueriesAmount++;

	if(mysql_query(m_pMySQL, _str.c_str()) != 0)
	{
		throw CDatabaseException(SRC_POS, StrFormat("Error executing query {0}. ({1})", _str, get_Error()));
	}

	MYSQL_RES* pRes = mysql_store_result(m_pMySQL);

	if(pRes == NULL)
	{
		throw CDatabaseException(SRC_POS, StrFormat("Error getting result. ({0})", get_Error()));
	}

	return CQueryResult{pRes};
}

u32 CDatabaseConnection::get_QueryCount()
{
	return m_dwQueriesAmount;
}

bool CDatabaseConnection::ping()
{
	return (mysql_ping(m_pMySQL) == 0);
}


const char *CDatabaseConnection::get_Error()
{
	return mysql_error(m_pMySQL);
}

u32 CDatabaseConnection::get_AffectedRows()
{
	return u32(mysql_affected_rows(m_pMySQL));
}
