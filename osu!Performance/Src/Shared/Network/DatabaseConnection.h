#pragma once


#include "QueryResult.h"
#include "PreparedStatement.h"



DEFINE_LOGGED_EXCEPTION(CDatabaseException);


class CDatabaseConnection
{
public:

	CDatabaseConnection(const char* szHost,
						s16        wPort,
						const char* szUsername,
						const char* szPassword,
						const char* szDatabase);
	~CDatabaseConnection();




	std::unique_ptr<CPreparedStatement> prepare(const std::string& Str);


	void NonQuery(const std::string& _str);
	CQueryResult Query(const std::string& _str);

	//statistic: count of queries sent
	u32 get_QueryCount();


	//kind of connection test, not really important
	bool ping();


	//returns error messages
	const char *get_Error();

	//returns the number of rows e.g. returned by a SELECT
	u32 get_AffectedRows();


	inline void escape(char* dest, char* src)
	{
		mysql_real_escape_string(m_pMySQL, dest, src, strlen(src));
	}

	inline s32 get_FieldCount()
	{
		return mysql_field_count(m_pMySQL);
	}


private:

	//connects to a MySQL server
	void connect(const char* szHost,
				 s16        wPort,
				 const char* szUsername,
				 const char* szPassword,
				 const char* szDatabase);

	std::mutex m_mDB;


	MYSQL* m_pMySQL;
	u32 m_dwQueriesAmount = 0;

};
