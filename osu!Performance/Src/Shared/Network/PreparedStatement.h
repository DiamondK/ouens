#pragma once

#include <Shared.h>


class CPreparedStatement
{
public:



	~CPreparedStatement();


	void BindType(u32 index, enum_field_types type);
	void BindValue(u32 index, void* data);

	void Execute();

private:

	CPreparedStatement(MYSQL_STMT* pStmt);

	MYSQL_STMT* _pStmt;

	MYSQL_BIND* _pBindings;
	s32* _pData;

	friend class CDatabaseConnection;
	
};