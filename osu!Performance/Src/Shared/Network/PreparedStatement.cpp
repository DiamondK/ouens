#include <Shared.h>


#include "DatabaseConnection.h"
#include "PreparedStatement.h"


CPreparedStatement::CPreparedStatement(MYSQL_STMT* pStmt)
: _pStmt{pStmt}
{
	u32 size = mysql_stmt_param_count(pStmt);

	_pBindings = new MYSQL_BIND[size];
	_pData = new s32[size];
	for(u32 i = 0; i < size; ++i)
	{
		_pBindings[i].buffer = &_pData[i];
		_pBindings[i].is_null = 0;
		_pBindings[i].length = 0;
	}

	if(mysql_stmt_bind_param(pStmt, _pBindings) != 0)
	{
		throw CDatabaseException(SRC_POS, "Couldn't bind prepared statement.");
	}
}

CPreparedStatement::~CPreparedStatement()
{
	delete [] _pBindings;
	mysql_stmt_close(_pStmt);
}

void CPreparedStatement::BindType(u32 index, enum_field_types type)
{
	_pBindings[index].buffer_type = type;
}

void CPreparedStatement::BindValue(u32 index, void* data)
{
	_pData[index] = *reinterpret_cast<s32*>(data);
}

void CPreparedStatement::Execute()
{
	if(mysql_stmt_execute(_pStmt) != 0)
	{
		throw CDatabaseException(SRC_POS, "Couldn't execute prepared statement.");
	}
}
