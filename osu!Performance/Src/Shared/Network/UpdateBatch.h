#pragma once



class CDatabaseConnection;

class CUpdateBatch
{
public:

	CUpdateBatch(std::shared_ptr<CDatabaseConnection> pDB, u32 sizeThreshold);
	~CUpdateBatch();

	void Append(const std::string& values)
	{
		_empty = false;
		_query += values;
	}

	void AppendAndCommit(const std::string& values);

	void Reset();
	const std::string& Query();
	

	u32 Size() const { return _query.size(); }

private:

	void Execute();

	u32 _sizeThreshold;

	bool _empty = true;

	std::shared_ptr<CDatabaseConnection> _pDB;

	std::string _query;
};