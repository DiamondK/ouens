#include <Shared.h>
#include "Threading.h"


void CPriorityMutex::LockLowPrio()
{
	L.lock();
	N.lock();
	M.lock();
	N.unlock();
}

void CPriorityMutex::UnlockLowPrio()
{
	M.unlock();
	L.unlock();
}


void CPriorityMutex::LockHighPrio()
{
	N.lock();
	M.lock();
	N.unlock();
}

void CPriorityMutex::UnlockHighPrio()
{
	M.unlock();
}


CPriorityLock::CPriorityLock(CPriorityMutex* pPriorityMutex, bool isHighPriority)
: _pPriorityMutex(pPriorityMutex), _isHighPriority(isHighPriority)
{
	if(_isHighPriority)
	{
		_pPriorityMutex->LockHighPrio();
	}
	else
	{
		_pPriorityMutex->LockLowPrio();
	}
}

CPriorityLock::~CPriorityLock()
{
	if(_isHighPriority)
	{
		_pPriorityMutex->UnlockHighPrio();
	}
	else
	{
		_pPriorityMutex->UnlockLowPrio();
	}
}
