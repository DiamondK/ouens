#include <Shared.h>


#include <iostream>
#include <iomanip>
#include <sstream>


///////////////////////////////////////////////////////////////////////////////

// XXX adapted from eApp (comments are left untouched) [flaviojs]

///////////////////////////////////////////////////////////////////////////////
//  ansi compatible printf with control sequence parser for windows
//  fast hack, m_hStream with care, not everything implemented
//
// \033[#;...;#m - Set Graphics Rendition (SGR) 
//
//  printf("\x1b[1;31;40m");    // Bright red on black
//  printf("\x1b[3;33;45m");    // Blinking yellow on magenta (blink not implemented)
//  printf("\x1b[1;30;47m");    // Bright black (grey) on dim white
//
//  Style           Foreground      Background
//  1st Digit       2nd Digit       3rd Digit           RGB
//  0 - Reset       30 - Black      40 - Black          000
//  1 - FG Bright   31 - Red        41 - Red            100
//  2 - Unknown     32 - Green      42 - Green          010
//  3 - Blink       33 - Yellow     43 - Yellow         110
//  4 - Underline   34 - Blue       44 - Blue           001
//  5 - BG Bright   35 - Magenta    45 - Magenta        101
//  6 - Unknown     36 - Cyan       46 - Cyan           011
//  7 - Reverse     37 - White      47 - White          111
//  8 - Concealed (invisible)
//
// \033[#A - Cursor Up (CUU)
//    Moves the cursor up by the specified number of lines without changing columns. 
//    If the cursor is already on the top line, this sequence is ignored. \e[A is equivalent to \e[1A.
//
// \033[#B - Cursor Down (CUD)
//    Moves the cursor down by the specified number of lines without changing columns. 
//    If the cursor is already on the bottom line, this sequence is ignored. \e[B is equivalent to \e[1B.
//
// \033[#C - Cursor Forward (CUF)
//    Moves the cursor forward by the specified number of columns without changing lines. 
//    If the cursor is already in the rightmost column, this sequence is ignored. \e[C is equivalent to \e[1C.
//
// \033[#D - Cursor Backward (CUB)
//    Moves the cursor back by the specified number of columns without changing lines. 
//    If the cursor is already in the leftmost column, this sequence is ignored. \e[D is equivalent to \e[1D.
//
// \033[#E - Cursor Next Line (CNL)
//    Moves the cursor down the indicated # of rows, to column 1. \e[E is equivalent to \e[1E.
//
// \033[#F - Cursor Preceding Line (CPL)
//    Moves the cursor up the indicated # of rows, to column 1. \e[F is equivalent to \e[1F.
//
// \033[#G - Cursor Horizontal Absolute (CHA)
//    Moves the cursor to indicated column in current row. \e[G is equivalent to \e[1G.
//
// \033[#;#H - Cursor Position (CUP)
//    Moves the cursor to the specified position. The first # specifies the line number, 
//    the second # specifies the column. If you do not specify a position, the cursor moves to the home position: 
//    the upper-left corner of the screen (line 1, column 1).
//
// \033[#;#f - Horizontal & Vertical Position
//    (same as \033[#;#H)
//
// \033[s - Save Cursor Position (SCP)
//    The current cursor position is saved. 
//
// \033[u - Restore cursor position (RCP)
//    Restores the cursor position saved with the (SCP) sequence \033[s.
//    (addition, restore to 0,0 if nothinh was saved before)
//

// \033[#J - Erase Display (ED)
//    Clears the screen and moves to the home position
//    \033[0J - Clears the screen from cursor to end of display. The cursor position is unchanged. (default)
//    \033[1J - Clears the screen from start to cursor. The cursor position is unchanged.
//    \033[2J - Clears the screen and moves the cursor to the home position (line 1, column 1).
//
// \033[#K - Erase Line (EL)
//    Clears the current line from the cursor position
//    \033[0K - Clears all characters from the cursor position to the end of the line (including the character at the cursor position). The cursor position is unchanged. (default)
//    \033[1K - Clears all characters from start of line to the cursor position. (including the character at the cursor position). The cursor position is unchanged.
//    \033[2K - Clears all characters of the whole line. The cursor position is unchanged.


/*
not implemented

\033[#L
IL: Insert Lines: The cursor line and all lines below it move down # lines, leaving blank space. The cursor position is unchanged. The bottommost # lines are lost. \e[L is equivalent to \e[1L.
\033[#M
DL: Delete Line: The block of # lines at and below the cursor are deleted; all lines below them move up # lines to fill in the gap, leaving # blank lines at the bottom of the screen. The cursor position is unchanged. \e[M is equivalent to \e[1M.
\033[#\@
ICH: Insert CHaracter: The cursor character and all characters to the right of it move right # columns, leaving behind blank space. The cursor position is unchanged. The rightmost # characters on the line are lost. \e[\@ is equivalent to \e[1\@.
\033[#P
DCH: Delete CHaracter: The block of # characters at and to the right of the cursor are deleted; all characters to the right of it move left # columns, leaving behind blank space. The cursor position is unchanged. \e[P is equivalent to \e[1P.

Escape sequences for Select Character Set
*/

///////////////////////////////////////////////////////////////////////////////




void Log(CLog::EType Flags, std::string&& Text)
{
	static auto pLog = CLog::CreateLogger();
	pLog->Log(Flags, std::forward<std::string>(Text));
}



std::unique_ptr<CLog> CLog::CreateLogger()
{
	auto pLogger = std::make_unique<CLog>();


	// Reset initially (also blank line)
	pLogger->Log(EType::None, CL_RESET);


	// Obligatory 'Blizzard is a homo!' message!
#ifdef __DEBUG
	pLogger->Log(EType::Info, "Blizzard is a homo! (Program runs in debug mode.)");
#endif

	return pLogger;
}


CLog::CLog()
#ifdef __WIN32
: m_Saveposition{0, 0}, m_Endposition{0, 0}
#endif
{
	m_pActive = CActive::Create();
}

CLog::~CLog()
{
#ifndef __WIN32

	// Reset
	Log(EType::None, CL_RESET);

#endif
}





void CLog::Log(EType Flags, std::string&& Text)
{
	m_pActive->Send([this, Flags, Text]() { LogText(Flags, Text); });
}



void CLog::LogText(EType Flags, std::string Text)
{

	// Set the stream
	EStream Stream;
	if(Flags & EType::Error ||
	   Flags & EType::CriticalError ||
	   Flags & EType::SQL ||
	   Flags & EType::Exception)
	{
		Stream = EStream::STDERR;
	}
	else
	{
		Stream = EStream::STDOUT;
	}




	auto TextOut = std::string{};



	if(!(Flags & EType::None))
	{
		// Display time format
		const auto Time = std::chrono::system_clock::now();
		const auto Now = std::chrono::system_clock::to_time_t(Time);

		

#ifdef __WIN32
		TextOut += STREAMTOSTRING(std::put_time(std::localtime(&Now), __CONSOLE_TIMESTAMP));
#else
		// gcc didn't implement put_time yet
		char TimeBuf[128];
		const auto tm_now = localtime(&Now);
		strftime(TimeBuf, 127, __CONSOLE_TIMESTAMP, tm_now);

		TextOut += TimeBuf;
#endif

	}


	if(Flags & EType::Success)
	{
		TextOut += CL_LT_GREEN   "SUCCESS"  CL_RESET;
	}
	else if(Flags & EType::SQL)
	{
		TextOut += CL_BT_BLUE    "SQL"      CL_RESET;
	}
	else if(Flags & EType::Threads)
	{
		TextOut += CL_BT_MAGENTA "THREADS"  CL_RESET;
	}
	else if(Flags & EType::Info)
	{
		TextOut += CL_LT_CYAN    "INFO"     CL_RESET;
	}
	else if(Flags & EType::Notice)
	{
		TextOut += CL_BT_WHITE   "NOTICE"   CL_RESET;
	}
	else if(Flags & EType::Warning)
	{
		TextOut += CL_BT_YELLOW  "WARNING"  CL_RESET;
	}
	else if(Flags & EType::Debug)
	{
		TextOut += CL_BT_CYAN    "DEBUG"    CL_RESET;
	}
	else if(Flags & EType::Error)
	{
		TextOut += CL_LT_RED     "ERROR"    CL_RESET;
	}
	else if(Flags & EType::CriticalError)
	{
		TextOut += CL_LT_RED     "CRITICAL" CL_RESET;
	}
	else if(Flags & EType::Exception)
	{
		TextOut += CL_BT_RED     "EXCEPT"   CL_RESET;
	}
	else if(Flags & EType::Graphics)
	{
		TextOut += CL_BT_BLUE    "GRAPHICS" CL_RESET;
	}




	if(!(Flags & EType::None))
	{
		TextOut += CL_FMT_BEGIN;

		// start with processing
		m_Offset = 0;
		WriteFormat(TextOut.c_str(), Stream);
	}


	

	// Make sure there is a linebreak in the end. We don't want duplicates!
	if(Text.back() != '\n')
	{
		Text += '\n';
	}

	// Reset after each message
	Text += CL_RESET;


	// start with processing
	m_Offset = Flags & EType::None ? 0 : __CONSOLE_PREFIX_LEN;


	WriteFormat(Text.c_str(), Stream);
}


#ifdef __WIN32

void CLog::WriteFormat(const std::string& Text, EStream Stream)
{
	// Set the stream
	if(Stream == EStream::STDERR)
	{
		m_hStream = GetStdHandle(STD_ERROR_HANDLE);
	}
	else if(Stream == EStream::STDOUT)
	{
		m_hStream = GetStdHandle(STD_OUTPUT_HANDLE);
	}
	else
	{
		std::cerr << "Unknown stream specified.\n";
	}


	auto p = Text.c_str();
	const char* q; // q will be initialized in the while condition

	while((q = strchr(p, *__CONSOLE_FMT_ESCAPE)) != nullptr)
	{   
		// find the escape character
		WriteToConsole(p, static_cast<u32>(q - p));



		if(q[1] != '[')
		{       
			// write the escape char (whatever purpose it has) 
			WriteToConsole(q, 1);



			p = q + 1;  //and start searching again
		}
		else
		{       
			// from here, we will skip the '\033['
			// we break at the first unprocessible position
			// assuming regular text is starting there
			byte numbers[16], numpoint=0;

			// initialize
			GetPosition();
			memset(numbers, 0, sizeof(numbers));

			// skip escape and bracket
			q = q + 2;

			for(;;)
			{
				if(::isdigit(static_cast<byte>(*q))) 
				{       
					// add number to number array, only accept 2digits, shift out the rest
					// so // \033[123456789m will become \033[89m
					numbers[numpoint] = (numbers[numpoint]<<4) | (*q-'0');
					++q;
					// and next character
					continue;
				}
				else if(*q == ';')
				{       
					// delimiter
					if(numpoint<sizeof(numbers)/sizeof(*numbers))
					{       
						// go to next array position
						++numpoint;
					}
					else
					{       
						// array is full, so we 'forget' the first value
						memmove(numbers,numbers+1,sizeof(numbers)/sizeof(*numbers)-1);
						numbers[sizeof(numbers)/sizeof(*numbers)-1]=0;
					}
					++q;

					// and next number
					continue;
				}
				else if(*q == 'm')
				{       
					// \033[#;...;#m - Set Graphics Rendition (SGR)
					byte i;

					for(i=0; i<= numpoint; ++i)
					{
						if(0x00 == (0xF0 & numbers[i]))
						{       
							// upper nibble 0
							if(numbers[i] == 0)
							{      
								// reset
								m_Info.wAttributes = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
							}
							else if(numbers[i] == 1)
							{       
								// set foreground intensity
								m_Info.wAttributes |= FOREGROUND_INTENSITY;
							}
							else if(numbers[i] == 2)
							{       
								// set background intensity
								m_Info.wAttributes |= BACKGROUND_INTENSITY;
							}
							else if(numbers[i] == 7)
							{       
								// reverse colors (just xor them)
								m_Info.wAttributes ^= FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE |
									BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE;
							}

							//case '2': // not existing
							//case '3':     // blinking (not implemented)
							//case '4':     // underline (not implemented)
							//case '6': // not existing
							//case '8': // concealed (not implemented)
							//case '9': // not existing
						}
						else if(0x20 == (0xF0 & numbers[i]))
						{       
							// off
							if(numbers[i] == 1)
							{       
								// set foreground intensity off
								m_Info.wAttributes &= ~FOREGROUND_INTENSITY;
							}
							else if(numbers[i] == 5)
							{       
								// set background intensity off
								m_Info.wAttributes &= ~BACKGROUND_INTENSITY;
							}
							else if(numbers[i] == 7)
							{      
								// reverse colors (just xor them)
								m_Info.wAttributes ^= FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE |
									BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE;
							}
						}
						else if(0x30 == (0xF0 & numbers[i]))
						{       
							// foreground
							byte num = numbers[i] & 0x0F;

							if(num == 9)
								m_Info.wAttributes |= FOREGROUND_INTENSITY;

							if(num > 7)
								num=7; // set white for 37, 38 and 39

							m_Info.wAttributes &= ~(FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);

							if((num & 0x01) > 0) // lowest bit set = red
								m_Info.wAttributes |= FOREGROUND_RED;
							if((num & 0x02) > 0) // second bit set = green
								m_Info.wAttributes |= FOREGROUND_GREEN;
							if((num & 0x04) > 0) // third bit set = blue
								m_Info.wAttributes |= FOREGROUND_BLUE;
						}
						else if(0x40 == (0xF0 & numbers[i]))
						{       
							// background
							byte num = numbers[i]&0x0F;

							if(num == 9)
								m_Info.wAttributes |= BACKGROUND_INTENSITY;

							if(num > 7)
								num=7; // set white for 47, 48 and 49

							m_Info.wAttributes &= ~(BACKGROUND_RED|BACKGROUND_GREEN|BACKGROUND_BLUE);

							if((num & 0x01) > 0) // lowest bit set = red
								m_Info.wAttributes |= BACKGROUND_RED;
							if((num & 0x02) > 0) // second bit set = green
								m_Info.wAttributes |= BACKGROUND_GREEN;
							if((num & 0x04) > 0) // third bit set = blue
								m_Info.wAttributes |= BACKGROUND_BLUE;
						}
					}

					// set the attributes
					SetConsoleTextAttribute(m_hStream, m_Info.wAttributes);
				}
				else if(*q=='J')
				{       
					// \033[#J - Erase Display (ED)
					// \033[0J - Clears the screen from cursor to end of display. The cursor position is unchanged.
					// \033[1J - Clears the screen from start to cursor. The cursor position is unchanged.
					// \033[2J - Clears the screen and moves the cursor to the home position (line 1, column 1).
					
					CVector2d<s16> Pos = GetPosition();
					byte num = (numbers[numpoint] >> 4) * 10 + (numbers[numpoint] & 0x0F);
					int cnt;
					DWORD tmp;
					COORD origin = {0,0};

					if(num == 1)
					{       
						// chars from start up to and including cursor
						cnt = m_ConsoleSize.x * Pos.y + Pos.x + 1;
					}
					else if(num == 2)
					{       
						// Number of chars on screen.
						cnt = m_ConsoleSize.x * m_ConsoleSize.y;    
						SetConsoleCursorPosition(m_hStream, origin); 
					}
					else// 0 and default
					{       
						// number of chars from cursor to end
						origin = m_Info.dwCursorPosition;
						cnt = m_ConsoleSize.x * (m_ConsoleSize.y - Pos.y) - Pos.x;
					}   

					FillConsoleOutputAttribute(m_hStream, m_Info.wAttributes, cnt, origin, &tmp);
					FillConsoleOutputCharacter(m_hStream, ' ',                cnt, origin, &tmp);
				}
				else if(*q=='K')
				{       
					// \033[K  : clear line from actual position to end of the line
					//    \033[0K - Clears all characters from the cursor position to the end of the line.
					//    \033[1K - Clears all characters from start of line to the cursor position.
					//    \033[2K - Clears all characters of the whole line.

					byte num = (numbers[numpoint]>>4)*10+(numbers[numpoint]&0x0F);
					COORD origin = {0,m_Info.dwCursorPosition.Y}; //warning C4204
					SHORT cnt;
					DWORD tmp;

					if(num == 1)
					{       
						cnt = m_Info.dwCursorPosition.X + 1;
					}
					else if(num == 2)
					{       
						cnt = m_Info.dwSize.X;
					}
					else// 0 and default
					{
						origin = m_Info.dwCursorPosition;
						cnt = m_Info.dwSize.X - m_Info.dwCursorPosition.X; // how many spaces until line is full
					}

					FillConsoleOutputAttribute(m_hStream, m_Info.wAttributes, cnt, origin, &tmp);
					FillConsoleOutputCharacter(m_hStream, ' ',                cnt, origin, &tmp);
				}
				else if(*q == 'H')
				{       
					// \033[#;#H - Cursor Position (CUP)
					// \033[#;#f - Horizontal & Vertical Position
					// The first # specifies the line number, the second # specifies the column. 
					// The default for both is 1
					CVector2d<s16> Pos;
					Pos.x = (numbers[numpoint])                  ? (numbers[numpoint]   >> 4) * 10 + ((numbers[numpoint]   & 0x0F) - 1) : 0;
					Pos.y = (numpoint && numbers[numpoint - 1])  ? (numbers[numpoint-1] >> 4)  *10 + ((numbers[numpoint-1] & 0x0F) - 1) : 0;

					if(Pos.x >= m_ConsoleSize.x)
					{
						Pos.x = m_ConsoleSize.x - 1;
					}

					if(Pos.y >= m_ConsoleSize.y)
					{
						Pos.y = m_ConsoleSize.y - 1;
					}

					SetPosition(Pos);
				}
				else if(*q=='s')
				{       
					// \033[s - Save Cursor Position (SCP)
					m_Saveposition = GetPosition();
				}
				else if(*q=='u')
				{      
					// \033[u - Restore cursor position (RCP)
					SetPosition(m_Saveposition);
				}
				else if(*q == 'F')
				{
					CVector2d<s16> Pos = GetPosition();

					s32 ToWrite =
						m_Info.dwSize.X - 2 -
						max_(static_cast<s32>(Pos.x), __CONSOLE_PREFIX_LEN-1);
					
					for(s32 i = 0; i < ToWrite; ++i)
					{
						WriteToConsole(q+1, 1);
					}
				}
				else if(*q=='e')
				{      
					// \033[e - cursor position to end
					SetPosition(m_Endposition);
				}
				else if(*q == 'A')
				{       
					// \033[#A - Cursor Up (CUU)
					// Moves the cursor UP # number of lines
					CVector2d<s16> Pos = GetPosition();
					Pos.y -= (numbers[numpoint]) ? (numbers[numpoint] >> 4) * 10 + (numbers[numpoint] & 0x0F) : 1;

					if(Pos.y < 0)
					{
						Pos.y = 0;
					}

					SetPosition(Pos);
				}
				else if(*q == 'B')
				{       
					// \033[#B - Cursor Down (CUD)
					// Moves the cursor DOWN # number of lines
					CVector2d<s16> Pos = GetPosition();
					Pos.y += (numbers[numpoint]) ? (numbers[numpoint] >> 4) * 10 + (numbers[numpoint] & 0x0F) : 1;

					if(Pos.y >= m_ConsoleSize.y)
					{
						Pos.y = m_ConsoleSize.y - 1;
					}

					SetPosition(Pos);
				}
				else if(*q == 'C')
				{       
					// \033[#C - Cursor Forward (CUF)
					// Moves the cursor RIGHT # number of columns
					CVector2d<s16> Pos = GetPosition();
					Pos.x += (numbers[numpoint]) ? (numbers[numpoint] >> 4) * 10 + (numbers[numpoint] & 0x0F) : 1;

					if(Pos.x >= m_ConsoleSize.x)
					{
						Pos.x = m_ConsoleSize.x - 1;
					}

					SetPosition(Pos);
				}
				else if(*q == 'D')
				{       
					// \033[#D - Cursor Backward (CUB)
					// Moves the cursor LEFT # number of columns
					CVector2d<s16> Pos = GetPosition();
					Pos.x -= (numbers[numpoint]) ? (numbers[numpoint] >> 4) * 10 + (numbers[numpoint] & 0x0F) : 1;

					if(Pos.x < 0)
					{
						Pos.x = 0;
					}

					SetPosition(Pos);
				}
				else if(*q == 'E')
				{       
					// \033[#E - Cursor Next Line (CNL)
					// Moves the cursor down the indicated # of rows, to column 1
					CVector2d<s16> Pos;
					Pos.y += (numbers[numpoint]) ? (numbers[numpoint] >> 4) * 10 + (numbers[numpoint] & 0x0F) : 1;
					Pos.x = 0;

					if(Pos.y >= m_ConsoleSize.y)
					{
						Pos.y = m_ConsoleSize.y - 1;
					}

					SetPosition(Pos);
				}
				else if(*q == 'f')
				{       
					// \033[#f - Cursor Preceding Line (CPL)
					// Moves the cursor up the indicated # of rows, to column 1.
					CVector2d<s16> Pos;
					Pos.y -= (numbers[numpoint]) ? (numbers[numpoint] >> 4) * 10 + (numbers[numpoint] & 0x0F) : 1;
					Pos.x = 0;

					if(Pos.y < 0)
					{
						Pos.y = 0;
					}

					SetPosition(Pos);
				}
				else if( *q == 'G' )
				{      
					// \033[#G - Cursor Horizontal Absolute (CHA)
					// Moves the cursor to indicated column in current row.
					CVector2d<s16> Pos = GetPosition();
					Pos.x = (numbers[numpoint]) ? (numbers[numpoint] >> 4) * 10 + ((numbers[numpoint] & 0x0F) - 1) : 0;

					if(Pos.x >= m_ConsoleSize.x)
					{
						Pos.x = m_ConsoleSize.x - 1;
					}

					SetPosition(Pos);
				}
				else if( *q == 'L' || *q == 'M' || *q == '@' || *q == 'P')
				{       
					// not implemented, just skip
				}
				else
				{       
					// no number nor valid sequencer
					// something is fishy, we break and give the current char free
					--q;
				}

				// skip the sequencer and search again
				p = q + 1; 
				break;

			} // end while
		}

	}

	if(*p) // write the rest of the buffer
	{
		WriteToConsole(p, strlen(p));
	}

	// End position
	CVector2d<s16> Pos = GetPosition();
	if(Pos.y > m_Endposition.y)
	{
		m_Endposition.y = Pos.y;
	}
}


void CLog::OnLineBreak()
{
	if(m_Info.dwCursorPosition.Y == m_Info.dwSize.Y-1 &&
		m_Saveposition.y != 0)
	{
		m_Saveposition.y--;
	}
}


void CLog::WriteToConsole(const char* p, s32 len)
{
	CVector2d<s16> Pos;


	for(s32 i = 0; i < len; ++i)
	{
		Pos = GetPosition();

		// Check if word is out of boundaries
		if(i == 0 || (i > 0 && isspace(*(p-1+i))))
		{
			s32 j = i;
			for(; j < len && !isspace(*(p+j)); ++j)
			{

			}

			if(Pos.x + (j-i) >= m_ConsoleSize.x &&
				j-i + GetOffset() < m_ConsoleSize.x)
			{
				OnLineBreak();

				PrintRaw("\n", 1);

				Pos = GetPosition();
			}
		}
		


		// Make early linebreak
		if(Pos.x >= m_ConsoleSize.x-1 && !isspace(*(p+i)))
		{
			OnLineBreak();

			PrintRaw("\n", 1);

			Pos = GetPosition();
		}

		// Fill offset
		if(Pos.x < GetOffset())
		{
			for(s16 j = 0; j < (GetOffset() - Pos.x - 1); ++j)
			{
				PrintRaw(" ", 1);
			}


			while(isspace(*(p+i)))
			{
				++i;
			}
		}


		if(*(p+i) == '\n')
		{
			OnLineBreak();
		}

		PrintRaw(p+i, 1);
	}
}


void CLog::PrintRaw(const char* p, s32 len)
{
	DWORD dwWritten;
	if(WriteConsoleA(m_hStream, p, len, &dwWritten, 0) == 0)
	{
		WriteFile(m_hStream, p, len, &dwWritten, 0);
	}
}

// x = ABSOLUTE, y = RELATIVE
void CLog::Move(const CVector2d<s16>& Distance)
{
    // initialize
	CVector2d<s16> Pos = GetPosition();


	Pos.x  = Distance.x;
	Pos.y += Distance.y;

	if(Pos.x >= m_ConsoleSize.x)
	{
        Pos.x = m_ConsoleSize.x - 1;
	}

	if(Pos.y < 0)
	{
        Pos.y = 0;
	}

    SetPosition(Pos);
}







// x = ABSOLUTE, y = RELATIVE
void CLog::EmptyLine(s32 nHow)
{
    // initialize
    GetConsoleScreenBufferInfo(m_hStream, &m_Info);

    COORD origin = {0, m_Info.dwCursorPosition.Y}; //warning C4204
    SHORT cnt;
	DWORD tmp;

    if(nHow == 1)
    {       
        cnt = m_Info.dwCursorPosition.X + 1;
    }
    else if(nHow == 2)
    {       
        cnt = m_Info.dwSize.X;
    }
    else// 0 and default
    {
        origin = m_Info.dwCursorPosition;
        cnt = m_Info.dwSize.X - m_Info.dwCursorPosition.X; // how many spaces until line is full
    }

    FillConsoleOutputAttribute(m_hStream, m_Info.wAttributes, cnt, origin, &tmp);
    FillConsoleOutputCharacter(m_hStream, ' ',                cnt, origin, &tmp);
}



void CLog::SetPosition(const CVector2d<s16>& Pos)
{
	SetConsoleCursorPosition(m_hStream, *(COORD*)&Pos);
}

CVector2d<s16> CLog::GetPosition()
{
	CVector2d<s16> Pos;

	GetConsoleScreenBufferInfo(m_hStream, &m_Info);
	Pos = *(CVector2d<s16>*)&m_Info.dwCursorPosition;
	m_ConsoleSize = *(CVector2d<s16>*)&m_Info.dwSize;

	return Pos;
}

#else





void CLog::WriteFormat(const std::string& Text, EStream Stream)
{

	if(Stream == EStream::STDERR)
	{
		fwrite(Text.c_str(), sizeof(char), Text.length(), stderr);
	}
	else if(Stream == EStream::STDOUT)
	{
		fwrite(Text.c_str(), sizeof(char), Text.length(), stdout);
	}
	else
	{
		std::cerr << "Unknown stream specified.\n";
	}
}

#endif