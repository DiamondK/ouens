#pragma once



class CPriorityMutex
{
public:

	void LockLowPrio();
	void UnlockLowPrio();

	void LockHighPrio();
	void UnlockHighPrio();

private:

	std::mutex M;
	std::mutex N;
	std::mutex L;
};


class CPriorityLock
{
public:

	CPriorityLock(CPriorityMutex* pPriorityMutex, bool isHighPriority);
	~CPriorityLock();

private:

	CPriorityMutex* _pPriorityMutex;
	bool _isHighPriority;
};
