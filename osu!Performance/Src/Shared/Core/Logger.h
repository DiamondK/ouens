#pragma once


#include "Active.h"

//#define __CONSOLE_LOG
//#define __CONSOLE_LOG_PATH "./log/console.log"
#define __CONSOLE_TIMESTAMP "  %H:%M:%S "
#define __CONSOLE_PREFIX_LEN 21
#define __CONSOLE_PREFIX_LEN_REAL __CONSOLE_PREFIX_LEN + 13
#define __CONSOLE_FMT_ESCAPE "\033"





#define CL_RESET        __CONSOLE_FMT_ESCAPE"[0;37m"
#define CL_CLS          __CONSOLE_FMT_ESCAPE"[2J"
#define CL_CLL          __CONSOLE_FMT_ESCAPE"[0K"
#define CL_UP			__CONSOLE_FMT_ESCAPE"[1f"
#define CL_BEGIN		__CONSOLE_FMT_ESCAPE"[1G"
#define CL_FMT_BEGIN	__CONSOLE_FMT_ESCAPE"[" TOSTRING(__CONSOLE_PREFIX_LEN) "G"
#define CL_STORE		__CONSOLE_FMT_ESCAPE"[s"
#define CL_LOAD			__CONSOLE_FMT_ESCAPE"[u"
#define CL_END			__CONSOLE_FMT_ESCAPE"[e"
#define CL_FILL			__CONSOLE_FMT_ESCAPE"[F"


#define CL_PREVMSG      CL_UP CL_FMT_BEGIN CL_CLL



// font settings
#define CL_BOLD         __CONSOLE_FMT_ESCAPE"[1m"
// foreground color and bold font (bright color on windows)
#define CL_WHITE        __CONSOLE_FMT_ESCAPE"[1;37m"
#define CL_GRAY         __CONSOLE_FMT_ESCAPE"[0;30m"
#define CL_RED          __CONSOLE_FMT_ESCAPE"[1;31m"
#define CL_GREEN        __CONSOLE_FMT_ESCAPE"[0;32m"
#define CL_YELLOW       __CONSOLE_FMT_ESCAPE"[1;33m"
#define CL_BLUE         __CONSOLE_FMT_ESCAPE"[1;34m"
#define CL_MAGENTA      __CONSOLE_FMT_ESCAPE"[1;35m"
#define CL_CYAN         __CONSOLE_FMT_ESCAPE"[0;36m"

// background color
#define CL_BG_BLACK     __CONSOLE_FMT_ESCAPE"[40m"
#define CL_BG_RED       __CONSOLE_FMT_ESCAPE"[41m"
#define CL_BG_GREEN     __CONSOLE_FMT_ESCAPE"[42m"
#define CL_BG_YELLOW    __CONSOLE_FMT_ESCAPE"[43m"
#define CL_BG_BLUE      __CONSOLE_FMT_ESCAPE"[44m"
#define CL_BG_MAGENTA   __CONSOLE_FMT_ESCAPE"[45m"
#define CL_BG_CYAN      __CONSOLE_FMT_ESCAPE"[46m"
#define CL_BG_WHITE     __CONSOLE_FMT_ESCAPE"[47m"
// foreground color and normal font (normal color on windows)
#define CL_LT_BLACK     __CONSOLE_FMT_ESCAPE"[0;30m"
#define CL_LT_RED       __CONSOLE_FMT_ESCAPE"[0;31m"
#define CL_LT_GREEN     __CONSOLE_FMT_ESCAPE"[0;32m"
#define CL_LT_YELLOW    __CONSOLE_FMT_ESCAPE"[0;33m"
#define CL_LT_BLUE      __CONSOLE_FMT_ESCAPE"[0;34m"
#define CL_LT_MAGENTA   __CONSOLE_FMT_ESCAPE"[0;35m"
#define CL_LT_CYAN      __CONSOLE_FMT_ESCAPE"[0;36m"
#define CL_LT_WHITE     __CONSOLE_FMT_ESCAPE"[0;37m"
// foreground color and bold font (bright color on windows)
#define CL_BT_BLACK     __CONSOLE_FMT_ESCAPE"[1;30m"
#define CL_BT_RED       __CONSOLE_FMT_ESCAPE"[1;31m"
#define CL_BT_GREEN     __CONSOLE_FMT_ESCAPE"[1;32m"
#define CL_BT_YELLOW    __CONSOLE_FMT_ESCAPE"[1;33m"
#define CL_BT_BLUE      __CONSOLE_FMT_ESCAPE"[1;34m"
#define CL_BT_MAGENTA   __CONSOLE_FMT_ESCAPE"[1;35m"
#define CL_BT_CYAN      __CONSOLE_FMT_ESCAPE"[1;36m"
#define CL_BT_WHITE     __CONSOLE_FMT_ESCAPE"[1;37m"

#define CL_WTBL         __CONSOLE_FMT_ESCAPE"[37;44m"   // white on blue
#define CL_XXBL         __CONSOLE_FMT_ESCAPE"[0;44m"    // default on blue
#define CL_PASS         __CONSOLE_FMT_ESCAPE"[0;32;42m" // green on green

#define CL_SPACE                "           "   // space equivalent of the print messages







DEFINE_EXCEPTION(CLoggerException);




class CLog
{
public:

	CLog();
	~CLog();
	
	enum EType : u32
	{
		None          = 0x00000001,
		Success       = 0x00000002,
		SQL           = 0x00000004,
		Threads       = 0x00000008,
		Info          = 0x00000010,
		Notice        = 0x00000020,
		Warning       = 0x00000040,
		Debug         = 0x00000080,
		Error         = 0x00000100,
		CriticalError = 0x00000200,
		Exception     = 0x00000400,
		Graphics      = 0x00000800,
	};



	static std::unique_ptr<CLog> CreateLogger();



	void ShutdownThread();

	//void log(byte bColor, const char* pcSys, const char* pcFmt, ...);
	void Log(EType Flags, std::string&& Text);
	

	INLINE s16 GetOffset() const
	{
		return m_Offset;
	}




	

	enum class EStream : byte
	{
		STDOUT=0,
		STDERR,
	};

	std::unique_ptr<CActive> m_pActive;

private:

#ifdef __WIN32

	CVector2d<s16> m_Saveposition;
	CVector2d<s16> m_Endposition;
	CVector2d<s16> m_ConsoleSize;

	void WriteToConsole(const char* p, s32 len);

	void PrintRaw(const char* p, s32 len);

	void OnLineBreak();


	void SetPosition(const CVector2d<s16>& Pos);
	CVector2d<s16> GetPosition();



	// x = ABSOLUTE, y = RELATIVE
	void Move(const CVector2d<s16>& Distance);

	// Empty the line
	void EmptyLine(s32 nHow);


	// Output stream
	HANDLE m_hStream;
	// Consoleinfo
	CONSOLE_SCREEN_BUFFER_INFO m_Info;

#endif

	static const size_t OutputBufferSize = 10000;

	char m_OutputBufferStdout[CLog::OutputBufferSize];
	char m_OutputBufferStderr[CLog::OutputBufferSize];


	s16 m_Offset;
	void LogText(EType Flags, std::string Text);

	void WriteFormat(const std::string& Text, EStream Stream);



};



void Log(CLog::EType Flags, std::string&& Text);

