#pragma once

#include "SharedQueue.h"

class CActive
{
public:

	CActive(const CActive&) = delete;
	CActive& operator=(const CActive&) = delete;

	virtual ~CActive();

	void Send(std::function<void()> callback);
	void Run();
	static std::unique_ptr<CActive> Create();

private:

	// See Create
	CActive() = default;

	CSharedQueue<std::function<void()>> _tasks;

	std::thread _thread;
	bool _isDone = false;

	void DoDone();
};