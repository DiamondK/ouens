#include <Shared.h>

#include "Active.h"

CActive::~CActive()
{
	Send(std::bind(&CActive::DoDone, this));
	_thread.join();
}

void CActive::Send(std::function<void()> callback)
{
	_tasks.Push(callback);
}

void CActive::Run()
{
	while (_isDone == false)
	{
		// Wait for a task, pop it and execute it. Hence the double ()()
		_tasks.WaitAndPop()();
	}

#ifdef __WIN32
	// ExitThread is required to prevent a deadlock with joining in the case, that main is returning at the same time
	// Remove when fixed in VC - not an issue in gcc / clang
	ExitThread(NULL);
#endif
}

std::unique_ptr<CActive> CActive::Create()
{
	auto pActive = std::unique_ptr<CActive>{ new CActive{} };
	pActive->_thread = std::thread(&CActive::Run, pActive.get());
	return pActive;
}

void CActive::DoDone()
{
	_isDone = true;
}
