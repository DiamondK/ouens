#pragma once





#ifndef FLT_MAX
#define FLT_MAX 3.402823466E+38F
#endif




const s32 ROUNDING_ERROR_S32 = 0;
const f32 ROUNDING_ERROR_f32 = 0.000001f;
const f64 ROUNDING_ERROR_f64 = 0.00000001;

#ifdef PI
#undef PI
#endif


const f32 PI = 3.14159265359f;
const f32 RECIPROCAL_PI	= 1.0f/PI;
const f32 HALF_PI = PI/2.0f;

#ifdef PI64
#undef PI64
#endif


const f64 PI64		= 3.1415926535897932384626433832795028841971693993751;
const f64 RECIPROCAL_PI64 = 1.0/PI64;
const f32 DEGTORAD = PI / 180.0f;
const f32 RADTODEG   = 180.0f / PI;
const f64 DEGTORAD64 = PI64 / 180.0;
const f64 RADTODEG64 = 180.0 / PI64;





INLINE f32 RadToDeg(f32 radians)
{
	return RADTODEG * radians;
}


INLINE f64 RadToDeg(f64 radians)
{
	return RADTODEG64 * radians;
}


INLINE f32 DegToRad(f32 degrees)
{
	return DEGTORAD * degrees;
}


INLINE f64 DegToRad(f64 degrees)
{
	return DEGTORAD64 * degrees;
}



template<class T>
INLINE const T& min_(const T& a, const T& b)
{
	return a < b ? a : b;
}



template<class T>
INLINE const T& min_(const T& a, const T& b, const T& c)
{
	return a < b ? min_(a, c) : min_(b, c);
}



template<class T>
INLINE const T& max_(const T& a, const T& b)
{
	return a < b ? b : a;
}



template<class T>
INLINE const T& max_(const T& a, const T& b, const T& c)
{
	return a < b ? max_(b, c) : max_(a, c);
}



template<class T>
INLINE T abs_(const T& a)
{
	return a < (T)0 ? -a : a;
}


template<class T>
INLINE T lerp(const T& a, const T& b, const f32 t)
{
	return (T)(a*(1.f-t)) + (b*t);
}


template <class T>
INLINE const T clamp (const T& value, const T& low, const T& high)
{
	return min_ (max_(value,low), high);
}



template <class T1, class T2>
INLINE void swap(T1& a, T2& b)
{
	T1 c(a);
	a = b;
	b = c;
}



INLINE bool equals(const f64 a, const f64 b, const f64 tolerance = ROUNDING_ERROR_f64)
{
	return (a + tolerance >= b) && (a - tolerance <= b);
}

INLINE bool equals(const f32 a, const f32 b, const f32 tolerance = ROUNDING_ERROR_f32)
{
	return (a + tolerance >= b) && (a - tolerance <= b);
}



INLINE bool equals(const s32 a, const s32 b, const s32 tolerance = ROUNDING_ERROR_S32)
{
	return (a + tolerance >= b) && (a - tolerance <= b);
}



INLINE bool equals(const u32 a, const u32 b, const s32 tolerance = ROUNDING_ERROR_S32)
{
	return (a + tolerance >= b) && (a - tolerance <= b);
}



INLINE bool iszero(const f64 a, const f64 tolerance = ROUNDING_ERROR_f64)
{
	return fabs(a) <= tolerance;
}


INLINE bool iszero(const f32 a, const f32 tolerance = ROUNDING_ERROR_f32)
{
	return fabsf(a) <= tolerance;
}


INLINE bool isnotzero(const f32 a, const f32 tolerance = ROUNDING_ERROR_f32)
{
	return fabsf(a) > tolerance;
}


INLINE bool iszero(const s32 a, const s32 tolerance = 0)
{
	return ( a & 0x7ffffff ) <= tolerance;
}


INLINE bool iszero(const u32 a, const u32 tolerance = 0)
{
	return a <= tolerance;
}

INLINE s32 s32_min(s32 a, s32 b)
{
	const s32 mask = (a - b) >> 31;
	return (a & mask) | (b & ~mask);
}

INLINE s32 s32_max(s32 a, s32 b)
{
	const s32 mask = (a - b) >> 31;
	return (b & mask) | (a & ~mask);
}

INLINE s32 s32_clamp (s32 value, s32 low, s32 high)
{
	return s32_min(s32_max(value,low), high);
}


INLINE f32 round_( f32 x )
{
	return floorf( x + 0.5f );
}




INLINE f32 squareroot(const f32 f)
{
	return sqrtf(f);
}


INLINE f64 squareroot(const f64 f)
{
	return sqrt(f);
}


INLINE s32 squareroot(const s32 f)
{
	return static_cast<s32>(squareroot(static_cast<f32>(f)));
}


INLINE f64 reciprocal_squareroot(const f64 x)
{
	return 1.0 / sqrt(x);
}


INLINE f32 reciprocal_squareroot(const f32 f)
{
	return 1.f / sqrtf(f);
}


INLINE s32 reciprocal_squareroot(const s32 x)
{
	return static_cast<s32>(reciprocal_squareroot(static_cast<f32>(x)));
}


INLINE f32 reciprocal(const f32 f)
{
	return 1.f / f;
}


INLINE f64 reciprocal(const f64 f)
{
	return 1.0 / f;
}



INLINE f32 reciprocal_approxim(const f32 f)
{
	return 1.f / f;
}


INLINE s32 floor32(f32 x)
{
	return static_cast<s32>(floorf(x));
}


INLINE s32 ceil32(f32 x)
{
	return static_cast<s32>(ceilf(x));
}



INLINE s32 round32(f32 x)
{
	return static_cast<s32>(round_(x));
}

INLINE f32 f32_max3(const f32 a, const f32 b, const f32 c)
{
	return a > b ? (a > c ? a : c) : (b > c ? b : c);
}

INLINE f32 f32_min3(const f32 a, const f32 b, const f32 c)
{
	return a < b ? (a < c ? a : c) : (b < c ? b : c);
}

INLINE f32 fract(f32 x)
{
	return x - floorf(x);
}


