﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace OsuXnbCreator
{
    /// <summary>
    /// Creates the most simple xnb files.
    /// </summary>
    class XnbCreator
    {
        const string RESOURCES_DIRECTORY = "Resources/";
        const string OUTPUT_DIRECTORY = "ouenresources/Resources/";

        static void Main(string[] args)
        {
            string baseDirectory = "./";
            while (Directory.GetFiles(baseDirectory, "osu!.sln").Length == 0)
                baseDirectory += "../";

            Console.WriteLine("Found base directory (" + baseDirectory + ")");

            while (true)
            {
                Console.Write("Please enter the PNG resource you wish to convert.\nHit enter to exit.\nBase Filename:");
                string pngName = Console.ReadLine().Replace(".png", "");

                if (pngName.Length == 0) break;

                if (pngName == "all")
                {
                    string[] files = Directory.GetFiles(baseDirectory + RESOURCES_DIRECTORY, "*.png");

                    foreach (string file in files)
                        CreateXnb(file, baseDirectory + OUTPUT_DIRECTORY + Path.GetFileName(file.Replace(".png", ".xnb")));

                    continue;
                }

                //confirm this file exists
                if (!File.Exists(baseDirectory + RESOURCES_DIRECTORY + pngName + ".png"))
                {
                    Console.WriteLine("file doesn't exist!");
                    continue;
                }

                CreateXnb(baseDirectory + RESOURCES_DIRECTORY + pngName + ".png", baseDirectory + OUTPUT_DIRECTORY + pngName + ".xnb");
            }
        }

        /// <summary>
        /// Converts an image to a one-level xnb file.
        /// </summary>
        /// <param name="source">image file source (any format)</param>
        /// <param name="destination">destination path (xnb output)</param>
        private static void CreateXnb(string source, string destination)
        {
            byte[] bitmap;
            int width;
            int height;

            using (Bitmap b = (Bitmap)Image.FromFile(source, false))
            {
                BitmapData data = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadOnly,
                                             PixelFormat.Format32bppArgb);
                bitmap = new byte[b.Width * b.Height * 4];
                width = b.Width;
                height = b.Height;
                Marshal.Copy(data.Scan0, bitmap, 0, bitmap.Length);
                b.UnlockBits(data);
            }

            File.Delete(destination);

            using (FileStream stream = File.Create(destination))
            using (BinaryWriter bw = new BinaryWriter(stream))
            {
                //xna header crap.  not used by us.
                for (int i = 0; i < 13; i++)
                    stream.WriteByte(0);

                bw.Write(1); //surfaceformat.Color
                bw.Write(width);
                bw.Write(height);
                bw.Write(1);//number of levels

                //write image data
                bw.Write(bitmap.Length);
                bw.Write(bitmap);

            }
        }
    }
}
