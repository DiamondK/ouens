﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using osu_common.Bancho.Requests;

namespace PacketAnalyser
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] text = File.ReadAllLines(args[0]);

            List<byte[]> packets = new List<byte[]>();

            string currentPacket = null;
            foreach (string s in text)
            {
                if (s.Length == 0) continue;
                if (s.StartsWith("char"))
                {
                    if (currentPacket != null)
                        packets.Add(StringToByteArray(currentPacket));
                    currentPacket = string.Empty;
                    continue;
                }

                currentPacket += s.Replace("0x", "").Replace(" ", "").Replace("}", "").Replace(";", "").Replace(",", "").Trim();

            }

            if (currentPacket != null)
                packets.Add(StringToByteArray(currentPacket));

            foreach (byte[] p in packets)
            {
                using (MemoryStream m = new MemoryStream(p))
                using (BinaryReader r = new BinaryReader(m))
                    Console.WriteLine((RequestType)r.ReadInt16());
            }

            Console.ReadLine();

        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}
