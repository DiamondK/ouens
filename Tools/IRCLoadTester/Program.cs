﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using osu_common.Helpers;
using System.Net.NetworkInformation;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;

namespace IRCLoadTester
{
    internal class Program
    {
        private static string[] names = new[] { "Gabe", "Gabe1", "Gabe31", "Gabe13", "IamGabe", "WifiConnection", "PumpkinGabe", "BeaverGabe", "SpamIAm" };
        private static string[] chat = new[] { "Pokemon XD", "Sorry, but I have to close my account.", "I got COCK-BLOCKED", "I am still watching The Cartoon Cartoon Show.", "make me AN ANGRY BEAVER!!!", "I once got an SSSSSSSSSS grade on this song", 
        "On mIRC , **** came here so everyone can talk about **** and I LOVE THAT WORD!","I got DOCKED FANTASY","Toasters toast toast!","I need some more gay pr0n to watch.","Spam. Spam. Spam. Spam. Spam. Spam. Spam.","I wonder if Toaster! will work on top of a TV...","I'm a Legendary Agent and I am a Hero's Hero!","Why isn't my beatmap ranked? My song is 全力少年 and it is not ranked.","I didn't cosplay anyone for Halloween",
        " Then you shall have it! The Hamtaro Skin!","It's from Bowser!","You are just making fun of E","You're making fun of me","Let me clear this up! Toaster!","THEN THAT'S ENOUGH!"};
        private static int name;
        static Random r = new Random();
        private static int instances;
        private static string clientHash;

        private static void Main(string[] args)
        {
            string adapters = "";
            foreach (NetworkInterface n in NetworkInterface.GetAllNetworkInterfaces())
                adapters += n.GetPhysicalAddress() + ".";

            clientHash = CryptoHelper.GetMd5String("test") + ":" + adapters + ":" + CryptoHelper.GetMd5String(adapters);

            while (instances < 200)
            {
                Thread t = new Thread(runOsu);
                instances++;
                t.Start();
                Thread.Sleep(10);
            }

            while (instances > 0)
                Thread.Sleep(50);

            Console.WriteLine("Done!");
            Console.ReadLine();
        }

        private static void runOsu()
        {
            try
            {
                name++;
                int localname = name;
                Console.WriteLine("started thread " + name);
                //TcpClient t = new TcpClient("192.168.0.1",6667);
                TcpClient t = new TcpClient("127.0.0.1", 13382);
                StreamWriter writer = new StreamWriter(t.GetStream());

                writer.AutoFlush = true;
                writer.WriteLine(name + r.Next(0, 10000));
                writer.Flush();
                writer.WriteLine("nopassword");
                writer.Flush();
                writer.WriteLine("{0}|{1}|{2}|{3}", "b1802", TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Hours, "1", clientHash);
                writer.Flush();

                MemoryStream ms = new MemoryStream();
                SerializationWriter sw = new SerializationWriter(ms);


                int timeToRun = 6000;

                string msg = string.Empty.PadRight(6000, 'a');

                //while (r.Next(0,100) != -1)
                while (timeToRun-- > 0)
                {
                    ms.Position = 0;

                    msg += "a";


                    int len = 0;
                        Request req = new Request(RequestType.Osu_SendIrcMessage, new bMessage("me", "#osu", string.Empty.PadRight(10, 'a')));
                        len += req.Send(RequestTarget.Osu, sw);

                        Console.WriteLine(len);

                    t.GetStream().Write(ms.GetBuffer(), 0, len);

                    byte[] read = new byte[8192];
                    t.GetStream().Read(read, 0, t.Available);

                    /*bBeatmapInfoRequest bir = new bBeatmapInfoRequest(100, 100);
                    for (int i = 0; i < 100; i++)
                        bir.filenames.Add("test timename is lonetest timename is lonetest timename is lonetest timename is lonetest timename is lonetest timename is lonetest timename is lone");
                    req = new Request(RequestType.Osu_BeatmapInfoRequest, bir);
                    len = req.Send(RequestTarget.Osu, sw);*/



                    //Thread.Sleep(r.Next(1000, 5000));
                    Thread.Sleep(1000);
                }

                t.Client.Close(500);
                t.Close();
            }
            catch { }
            instances--;
        }

        private static void run()
        {
            try
            {
                name++;
                int localname = name;
                Console.WriteLine("started thread " + name);
                //TcpClient t = new TcpClient("192.168.0.1",6667);
                TcpClient t = new TcpClient("127.0.0.1", 6667);
                StreamWriter w = new StreamWriter(t.GetStream());
                w.WriteLine("USER s 8 * :d");
                w.WriteLine("NICK " + names[localname % names.Length] + r.Next(0, 10024));

                int timeToRun = 60;

                //while (r.Next(0,100) != -1)
                while (timeToRun-- > 0)
                {
                    w.WriteLine("PING osu.ppy.sh");
                    w.WriteLine("PONG");
                    w.WriteLine("NOTICE #osu :" + chat[r.Next(0, chat.Length)] + r.NextDouble());
                    w.Flush();
                    //Thread.Sleep(r.Next(1000, 5000));
                    Thread.Sleep(500);
                }

                t.Client.Close(500);
                t.Close();
            }
            catch { }
            instances--;
        }
    }
}