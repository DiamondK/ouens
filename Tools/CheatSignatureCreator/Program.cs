﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using osu_common.Helpers;

namespace CheatSignatureCreator
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
                return;

            FileInfo fi = new FileInfo(args[0]);
            Console.WriteLine(args[0] + " : " + CryptoHelper.GetMd5String(fi.Length.ToString()));
            Console.ReadLine();
        }
    }
}
