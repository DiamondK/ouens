uniform extern float4x4 worldViewProj : WORLDVIEWPROJECTION;
float time;
float length;
float rotation;
float radius;
float4 lineColor;


struct VS_OUTPUT
{
    float4 position : POSITION;
    float3 polar : TEXCOORD0;
    float2 col : TEXCOORD1;
};


VS_OUTPUT MyVS(
    float4 pos  : POSITION, 
    float3 norm : NORMAL, 
    float2 tex : TEXCOORD0 )
{
    VS_OUTPUT Out = (VS_OUTPUT)0;
    
    // Scale X by radius, and translate X by length, in worldspace
    // based on what part of the line we're on
	pos.x = pos.x * (tex.x * radius) + (tex.y * length);
		
	Out.col.x = pos.x;
	Out.col.y = abs(pos.y);
		
	// Always scale Y by radius regardless of what part of the line we're on
	pos.y *= radius;

	Out.position = mul(pos, worldViewProj);
	

	
	Out.polar = norm;
	//Out.polar.z = Out.polar.y + rotation; // world-space theta

    return Out;
}


// Helper function used by several pixel shaders to blur the line edges
float BlurEdge( float rho )
{
	float blurThreshold = 0.95; // This really should vary based on line's screenspace size
	if( rho < blurThreshold )
	{
		return 1.0f;
	}
	else
	{
		//float normrho = (rho - blurThreshold) * 1 / (1 - blurThreshold);
		float normrho = 1- (rho - blurThreshold) / (1 - blurThreshold);
		return normrho;
	}
}

// Helper function used by several pixel shaders to blur the line edges
float BlurEdgeHax( float rho, float pos )
{
	float blurThreshold = 0.95; // This really should vary based on line's screenspace size
	if( rho < blurThreshold )
	{
		return 0.9 + pos/4;
	}
	else
	{
		//float normrho = (rho - blurThreshold) * 1 / (1 - blurThreshold);
		float normrho = 1 - (rho - blurThreshold) / (1 - blurThreshold);
		return normrho;
	}
}

float4 MyPSStandard( float3 polar : TEXCOORD0, float2 pos : TEXCOORD1) : COLOR0
{
	float4 finalColor;
	//finalColor.rgb = lineColor.rgb * (1.45-0.4*pos.y);
	finalColor.r = lineColor.r + (0.8-pos.y)*0.25;
	finalColor.g = lineColor.g + (0.8-pos.y)*0.25;
	finalColor.b = lineColor.b + (0.8-pos.y)*0.25;
	finalColor.a = lineColor.a *  BlurEdgeHax( polar.x, pos.y );
	return finalColor;
}

float4 MyPSStandard11( float3 polar : TEXCOORD0) : COLOR0
{
	float4 finalColor;
	finalColor.rgb = lineColor.rgb;
	finalColor.a = lineColor.a *  BlurEdge( polar.x );
	return finalColor;
}

float4 MyPSStandardNoGradient( float3 polar : TEXCOORD0) : COLOR0
{
	float4 finalColor;
	finalColor.rgb = lineColor.rgb;
	finalColor.a = lineColor.a *  BlurEdge( polar.x );
	return finalColor;
}

float4 MyPSStandardBorder( float3 polar : TEXCOORD0, float2 pos : TEXCOORD1) : COLOR0
{
	float4 finalColor;
	finalColor.rgb = lineColor.rgb;
	finalColor.a = lineColor.a *  BlurEdge( polar.x );
	return finalColor;
}

float4 MyPSStandardBorder11( float3 polar : TEXCOORD0) : COLOR0
{
	float4 finalColor;
	finalColor.rgb = lineColor.rgb;
	finalColor.a = lineColor.a *  BlurEdge( polar.x );
	return finalColor;
}


float4 MyPSNoBlur() : COLOR0
{
	float4 finalColor = lineColor;
	return finalColor;
}

technique Standard
{
    pass P0
    {
		CullMode = None;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		BlendOp = Add;
        vertexShader = compile vs_1_1 MyVS();
        pixelShader = compile ps_2_0 MyPSStandard();
    }
}

technique StandardNoGradient
{
    pass P0
    {
		CullMode = None;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		BlendOp = Add;
        vertexShader = compile vs_1_1 MyVS();
        pixelShader = compile ps_2_0 MyPSStandardNoGradient();
    }
}

technique StandardNoGradient11
{
    pass P0
    {
		CullMode = None;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		BlendOp = Add;
        vertexShader = compile vs_1_1 MyVS();
        pixelShader = compile ps_1_1 MyPSStandardNoGradient();
    }
}

technique Standard11
{
    pass P0
    {
		CullMode = None;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		BlendOp = Add;
        vertexShader = compile vs_1_1 MyVS();
        pixelShader = compile ps_1_1 MyPSStandard11();
    }
}

technique StandardBorder
{
    pass P0
    {
		CullMode = None;
		BlendOp = Add;
        vertexShader = compile vs_1_1 MyVS();
        pixelShader = compile ps_2_0 MyPSStandardBorder();
        SrcBlend = SrcAlpha;
        DestBlend = InvSrcAlpha;
    }
}

technique StandardBorder11
{
    pass P0
    {
		CullMode = None;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		BlendOp = Add;
        vertexShader = compile vs_1_1 MyVS();
        pixelShader = compile ps_1_1 MyPSStandardBorder();
    }
}

technique StandardBorder
{
    pass P0
    {
		CullMode = None;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		BlendOp = Add;
        vertexShader = compile vs_1_1 MyVS();
        pixelShader = compile ps_2_0 MyPSStandardBorder();
    }
}

technique StandardBorder11
{
    pass P0
    {
		CullMode = None;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		BlendOp = Add;
        vertexShader = compile vs_1_1 MyVS();
        pixelShader = compile ps_1_1 MyPSStandardBorder11();
    }
}

technique NoBlur
{
    pass P0
    {
		CullMode = None;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		BlendOp = Add;
        vertexShader = compile vs_1_1 MyVS();
        pixelShader = compile ps_1_1 MyPSNoBlur();
    }
}