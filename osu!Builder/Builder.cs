﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using osu_common.Libraries.NetLib;
using osu_common.Helpers;
using osu_common.Updater;
using ICSharpCode.SharpZipLib.Zip;
using osu_common.Libraries;
using System.Threading;
using Newtonsoft.Json;

namespace osu_Builder
{
    enum ReleaseType
    {
        Release, //test build
        Public,
        PublicNoUpdate,
        MainDll,
        GameplayDll,
        UiDll,
        Updater,
        Framework,
        Continuous,
        IntegrationTest,
        External,
        Beta
    }

    class Builder
    {
        static string SolutionPath;
        static ReleaseType releaseType;
        static int Version;
        static int InternalVersion;
        static int OldVersion;
        static int OldInternalVersion;

        private static ReleaseStream Stream;

        static bool Interactive;

        static string BuiltVersion
        {
            get
            {
                string releaseSuffix = string.Empty;
                switch (releaseType)
                {
                    case ReleaseType.Release:
                        releaseSuffix = "test";
                        break;
                    case ReleaseType.PublicNoUpdate:
                        releaseSuffix = "local";
                        break;
                    case ReleaseType.IntegrationTest:
                        releaseSuffix = "intg";
                        break;
                }

                switch (Stream)
                {
                    case ReleaseStream.Beta:
                        releaseSuffix = "beta";
                        break;
                    case ReleaseStream.CuttingEdge:
                        releaseSuffix = "cuttingedge";
                        break;
                }

                return (InternalVersion > 0 ? (Version + "." + InternalVersion) : Version.ToString()) + releaseSuffix;
            }
        }

        static internal string HomeDir { get { return Environment.GetFolderPath(Environment.SpecialFolder.UserProfile); } }

        static internal string MSBUILD_PATH = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe";
        static internal string NUGET_PATH = HomeDir + @"\Dropbox\Apps\NuGet.exe";
        static internal string SA_PATH = HomeDir + @"\Dropbox\Apps\sa\{smartassembly}.com";
        static internal string CONFUSER_PATH = HomeDir + @"\Dropbox\Apps\confuser\Confuser.Console.exe";
        static internal string SIGNTOOL_PATH = HomeDir + @"\Dropbox\Apps\signtool.exe";
        static internal string CODE_SIGNING_CERT = HomeDir + @"\Dropbox\Important Stuffs\deanherbert.pfx";

        const string STAGING_FOLDER = @"Staging\";

        static List<string> updateableFiles = new List<string>();
        static List<string> patchFiles = new List<string>();

        static void Main(string[] args)
        {
            FindSolutionPath();

            Interactive = args.Length == 0;

            if (!Interactive)
                Console.WriteLine("Running in non-interactive mode.");

            ChooseReleaseType(Interactive ? null : args[0], args.Length > 1 ? args[1] : null);

            UpdateVersionNumber();

            if (releaseType == ReleaseType.Continuous)
                RunContinuousBuild();

            Console.WriteLine("_________________________________________________________________________");
            Console.WriteLine();

            Console.WriteLine("Solution Path:".PadRight(20) + SolutionPath);
            Console.WriteLine("Build Type:".PadRight(20) + releaseType);
            Console.WriteLine("Stream:".PadRight(20) + Stream);
            Console.WriteLine("Version: ".PadRight(20) + BuiltVersion);
            Console.WriteLine();
            Console.WriteLine("Press any key to begin.");
            Console.ReadLine();

            //check some files exist
            if (!File.Exists(MSBUILD_PATH))
                Error("MSBuild not found at specified path (" + MSBUILD_PATH + ")");
            if (!File.Exists(SA_PATH))
                Error("smartassembly not found at specified path (" + SA_PATH + ")");
            if (!File.Exists(SIGNTOOL_PATH))
                Error("signtool not found at specified path (" + SIGNTOOL_PATH + ")");
            if (!File.Exists(CODE_SIGNING_CERT))
                Error("code signing certificate not found at specified path (" + CODE_SIGNING_CERT + ")");

            if (!Directory.Exists(STAGING_FOLDER))
                Directory.CreateDirectory(STAGING_FOLDER);

            sw.Start();

            RunVisualStudioBuild();

            ObfuscateExecutables();

            if (releaseType != ReleaseType.External)
            {
                Out("Signing output...");
                SignFiles();
            }

            if (releaseType != ReleaseType.PublicNoUpdate && releaseType != ReleaseType.IntegrationTest)
            {
                foreach (string file in updateableFiles)
                {
                    string filename = STAGING_FOLDER + file;

                    UpdaterFileInfo newFile = new UpdaterFileInfo()
                    {
                        filename = Path.GetFileName(filename),
                        filesize = (int)new FileInfo(filename).Length,
                        file_hash = CryptoHelper.GetMd5(filename),
                    };

                    Out("Requesting current file information from server...");

                    FormNetRequest req = new FormNetRequest("http://osu.ppy.sh/web/check-updates.php");
                    req.AddField("action", "request-put");
                    req.AddField("stream", Stream.ToString().ToLower());
                    req.AddField("fileinfo", JsonConvert.SerializeObject(newFile));
                    UpdaterFileInfo res = JsonConvert.DeserializeObject<UpdaterFileInfo>(req.BlockingPerform());

                    //move the new file to a "permanent" locatiuon with hash suffix.
                    File.Delete(filename + "_" + newFile.file_hash);
                    File.Move(filename, filename + "_" + newFile.file_hash);

                    filename = filename + "_" + newFile.file_hash;

                    switch (res.response)
                    {
                        case @"ok":
                            Out("Uploading complete file (no patchable version available)...");
                            break;
                        case @"patch":
                            string previousVersion = STAGING_FOLDER + res.filename + "_" + res.file_hash;
                            string patchFilename = previousVersion + "_" + newFile.file_hash;

                            if (!File.Exists(previousVersion))
                            {
                                Out("Downloading previous version of file to create a patch...");
                                FileNetRequest fnr = new FileNetRequest(previousVersion, res.url_full);
                                fnr.Perform();
                            }

                            Out("Creating binary patch...");

                            BSDiffer diff = new BSDiffer();
                            diff.Diff(previousVersion, filename, patchFilename);

                            //change the target filename to be the patch rather than the full file.
                            filename = patchFilename;

                            //let the server know we are sending patch data.
                            newFile.patch_from = res.file_version;
                            break;
                        default:
                            Error("invalid response for put request");
                            break;
                    }

                    Out("Performing upload...");

                    FileUploadNetRequest reqUpload = new FileUploadNetRequest("http://osu.ppy.sh/web/check-updates.php",
                                File.ReadAllBytes(filename),
                                Path.GetFileName(filename),
                                "file");

                    reqUpload.request.Items.AddFormField("action", "put");
                    reqUpload.request.Items.AddFormField("stream", Stream.ToString().ToLower());
                    reqUpload.request.Items.AddFormField("fileinfo", JsonConvert.SerializeObject(newFile));
                    reqUpload.request.Items.AddFormField("buildname", BuiltVersion);

                    res = JsonConvert.DeserializeObject<UpdaterFileInfo>(reqUpload.BlockingPerform());

                    if (res.response != "ok")
                        Error("Bad response from server after file upload: " + res.response);
                }
            }

            if (!Interactive)
                Console.WriteLine("##teamcity[buildNumber 'b{0}']", BuiltVersion);

            UpdateVersionNumber(OldVersion.ToString(), OldInternalVersion.ToString());

            Out("Build complete!");

            Console.ReadLine();
        }

        private static void SignFiles()
        {
            foreach (string file in updateableFiles)
            {
                int attempts = 5;
                while (attempts-- > 0)
                {
                    DateTime before = File.GetLastWriteTimeUtc(STAGING_FOLDER + file);
                    ProcessStartInfo psi = new ProcessStartInfo(SIGNTOOL_PATH, "sign /f \"" + CODE_SIGNING_CERT + @""" /p osuismyfriend /t http://timestamp.comodoca.com/authenticode " + STAGING_FOLDER + file);
                    psi.WorkingDirectory = SolutionPath;
                    psi.UseShellExecute = false;
                    psi.CreateNoWindow = true;
                    psi.WindowStyle = ProcessWindowStyle.Hidden;
                    Process p = Process.Start(psi);
                    p.WaitForExit();

                    if (File.GetLastWriteTimeUtc(STAGING_FOLDER + file) == before)
                    {
                        Thread.Sleep(1000);
                        attempts--;
                        continue;
                    }

                    break; //success
                }

                if (attempts <= 0)
                    Error("Digital signature was not applied (file has not been modified by signtool.exe)");
            }
        }

        private static void ObfuscateExecutables()
        {
            ProcessStartInfo psi = null;

            Stopwatch sw = new Stopwatch();

            sw.Start();

            switch (releaseType)
            {
                default:
                    return;
                case ReleaseType.Public:
                case ReleaseType.PublicNoUpdate:
                case ReleaseType.Beta:
                case ReleaseType.Release:
                    //psi = new ProcessStartInfo(CONFUSER_PATH, SolutionPath + @"osu!\osu!.crproj");
                    psi = new ProcessStartInfo(SA_PATH, @"/build " + SolutionPath + @"osu!\osu!.{sa}proj /output=$self");
                    break;
                case ReleaseType.Updater:
                    psi = new ProcessStartInfo(SA_PATH, @"/build " + SolutionPath + @"Updater\osume.{sa}proj /output=$self");
                    break;
            }

            Out("Adding code protection...");
            //this always runs on the executable set in the saproj file, ie. we want to run this before the possible rename below.

            if (psi != null)
            {
                psi.WorkingDirectory = SolutionPath;
                psi.UseShellExecute = true;
                psi.CreateNoWindow = true;
                psi.WindowStyle = ProcessWindowStyle.Hidden;
                Process p = Process.Start(psi);
                p.WaitForExit();
            }

            if (releaseType < ReleaseType.MainDll && sw.ElapsedMilliseconds < 5000)
                Error("Obfuscation took shorter than it should! Bailing!");
        }

        private static void Error(string p)
        {
            if (Interactive)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ERROR: " + p);
            }
            else
                Console.WriteLine("##teamcity[message text='{0}' errorDetails='{1}' status='ERROR']", "Build error", p);

            try
            {
                UpdateVersionNumber(OldVersion.ToString(), OldInternalVersion.ToString());
            }
            catch { }

            Console.ReadLine();
            Environment.Exit(-1);
        }

        static Stopwatch sw = new Stopwatch();

        private static void Out(string p)
        {
            if (Interactive)
                Console.WriteLine(sw.ElapsedMilliseconds.ToString().PadRight(8) + ": " + p);
            else
                Console.WriteLine("##teamcity[progressMessage '{0}']", p);
        }

        private static bool RunVisualStudioBuild()
        {
            ProcessStartInfo psi = null;

            string stagingPath = SolutionPath + STAGING_FOLDER;

            switch (releaseType)
            {
                default:
                    return true;
                case ReleaseType.Continuous:
                    psi = new ProcessStartInfo(MSBUILD_PATH, "/v:quiet /m /t:Client\\osu! /p:Configuration=\"Debug (Client)\" osu!.sln");
                    break;
                case ReleaseType.Public:
                case ReleaseType.PublicNoUpdate:
                case ReleaseType.IntegrationTest:
                case ReleaseType.Release:
                case ReleaseType.Beta:
                    File.Delete(STAGING_FOLDER + "osu!.exe"); //delete this else build will fail due to conflicting types

                    //Restore nuget packages.
                    psi = new ProcessStartInfo(NUGET_PATH, "restore " + SolutionPath);
                    if (psi != null)
                    {
                        psi.WorkingDirectory = SolutionPath;
                        psi.CreateNoWindow = true;
                        psi.RedirectStandardOutput = true;
                        psi.UseShellExecute = false;
                        psi.WindowStyle = ProcessWindowStyle.Hidden;
                        Process p = Process.Start(psi);
                        string output = p.StandardOutput.ReadToEnd();
                        if (p.ExitCode != 0)
                        {
                            Out(output);
                            if (releaseType != ReleaseType.Continuous)
                                Out("Nuget update failed!");
                        }
                    }

                    psi = new ProcessStartInfo(MSBUILD_PATH, "/v:quiet /m /t:Client\\osu! /p:OutputPath=" + stagingPath + ";Configuration=" + releaseType.ToString() + " osu!.sln");
                    break;
                case ReleaseType.Updater:
                    psi = new ProcessStartInfo(MSBUILD_PATH, "/v:quiet /m /t:Client\\pUpdater /p:OutputPath=" + stagingPath + ";Configuration=Public osu!.sln");
                    break;
                case ReleaseType.Framework:
                    psi = new ProcessStartInfo(MSBUILD_PATH, "/v:quiet /m /t:Shared\\osu!framework /p:OutputPath=" + stagingPath + ";Configuration=Public osu!.sln");
                    break;
                case ReleaseType.MainDll:
                    psi = new ProcessStartInfo(MSBUILD_PATH, "/v:quiet /m /t:Client\\osudata /p:OutputPath=" + stagingPath + ";Configuration=Public osu!.sln");
                    break;
                case ReleaseType.GameplayDll:
                    psi = new ProcessStartInfo(MSBUILD_PATH, "/v:quiet /m /t:Client\\osu!gameplay /p:OutputPath=" + stagingPath + ";Configuration=Public osu!.sln");
                    break;
                case ReleaseType.UiDll:
                    psi = new ProcessStartInfo(MSBUILD_PATH, "/v:quiet /m /t:Client\\osu!ui /p:OutputPath=" + stagingPath + ";Configuration=Public osu!.sln");
                    break;
            }

            Out("Running build process...");

            if (psi != null)
            {
                psi.WorkingDirectory = SolutionPath;
                psi.CreateNoWindow = true;
                psi.RedirectStandardOutput = true;
                psi.UseShellExecute = false;
                psi.WindowStyle = ProcessWindowStyle.Hidden;
                Process p = Process.Start(psi);
                string output = p.StandardOutput.ReadToEnd();
                if (p.ExitCode != 0)
                {
                    Out(output);
                    if (releaseType != ReleaseType.Continuous) Error("Build process failed!");
                    return false;
                }
            }

            return true;
        }

        private static void ChooseReleaseType(string response = null, string stream = null)
        {
            if (response == null)
            {
                Console.Write("[T]ester [P]ublic [D]ll [G]ameplayDll [U]pdater [F]ramework [L]ocalPublic ");
                response = Console.ReadLine();
            }

            if (response.Length > 1)
            {
                releaseType = ReleaseType.External;

                if (!File.Exists(response))
                    Error("Requested file could not be accessed.");

                string finalPath = STAGING_FOLDER + Path.GetFileName(response);

                if (finalPath != response)
                {
                    File.Delete(finalPath);
                    File.Copy(response, finalPath);
                }

                updateableFiles.Add(Path.GetFileName(response));
            }
            else
            {
                switch (response.ToUpper()[0])
                {
                    case 'C':
                        releaseType = ReleaseType.Continuous;
                        break;
                    case 'B':
                        releaseType = ReleaseType.Beta;
                        updateableFiles.Add(@"osu!.exe");
                        break;
                    case 'T':
                        releaseType = ReleaseType.Release;
                        updateableFiles.Add(@"osu!.exe");
                        break;
                    case 'P':
                        releaseType = ReleaseType.Public;
                        updateableFiles.Add(@"osu!.exe");
                        break;
                    case 'N':
                        releaseType = ReleaseType.IntegrationTest;
                        updateableFiles.Add(@"osu!.exe");
                        break;
                    case 'L':
                        releaseType = ReleaseType.PublicNoUpdate;
                        updateableFiles.Add(@"osu!.exe");
                        break;
                    case 'D':
                        releaseType = ReleaseType.MainDll;
                        updateableFiles.Add(@"osu.dll");
                        break;
                    case 'G':
                        releaseType = ReleaseType.GameplayDll;
                        updateableFiles.Add(@"osu!gameplay.dll");
                        break;
                    case 'I':
                        releaseType = ReleaseType.UiDll;
                        updateableFiles.Add(@"osu!ui.dll");
                        break;
                    case 'U':
                        releaseType = ReleaseType.Updater;
                        updateableFiles.Add(@"osume.exe");
                        break;
                    case 'F':
                        releaseType = ReleaseType.Framework;
                        updateableFiles.Add(@"osu!framework.dll");
                        break;
                }
            }

            if (stream == null)
            {
                int i = 0;
                foreach (ReleaseStream s in Enum.GetValues(typeof(ReleaseStream)))
                    Console.Write(@"[{0}]{1} ", i++, s.ToString());
                try
                {
                    Stream = (ReleaseStream)Int32.Parse(Console.ReadLine());
                }
                catch
                {
                    Stream = ReleaseStream.CuttingEdge;
                }
            }
            else
            {
                if (!Enum.TryParse<ReleaseStream>(stream, out Stream))
                    Stream = ReleaseStream.CuttingEdge;
            }
        }

        /// <summary>
        /// Find the base path of the osu! solution (git checkout location)
        /// </summary>
        private static void FindSolutionPath()
        {
            string path = Path.GetDirectoryName(Environment.CommandLine.Replace("\"", "").Trim());

            if (string.IsNullOrEmpty(path))
                path = Environment.CurrentDirectory;

            while (!File.Exists(path + "\\osu!.sln"))
                path = path.Remove(path.LastIndexOf('\\'));
            path += "\\";

            Environment.CurrentDirectory = SolutionPath = path;
        }

        /// <summary>
        /// Read the current version/build number and prompt for a new one.
        /// </summary>
        private static void UpdateVersionNumber(string version = null, string internalVersion = null)
        {
            string[] lines = File.ReadAllLines("osu!/Constants/General_Version.cs");
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains("internal const int VERSION ="))
                {
                    int index = lines[i].IndexOf("=");

                    OldVersion = Version = Int32.Parse(lines[i].Split('=')[1].Trim(' ', ';'));

                    if (version == null)
                        version = DateTime.Now.ToString("yyyyMMdd");
                    else if (version.Length == 0)
                        return;

                    try
                    {
                        Version = Int32.Parse(version);
                    }
                    catch { }

                    lines[i] = lines[i].Remove(index + 2) + Version + ";";
                }
                else if (lines[i].Contains("internal const int internal_version ="))
                {
                    int index = lines[i].IndexOf("=");

                    OldInternalVersion = Int32.Parse(lines[i].Split('=')[1].Trim(' ', ';'));

                    StringNetRequest snr = new StringNetRequest("http://osu.ppy.sh/web/get-internal-version.php?v=" + Version);

                    InternalVersion = Int32.Parse(internalVersion ?? snr.BlockingPerform());
                    lines[i] = lines[i].Remove(index + 2) + InternalVersion + ";";
                }
            }

            File.WriteAllLines("osu!/Constants/General_Version.cs", lines);
        }

        static bool hasChanges;
        private static void RunContinuousBuild()
        {
            FileSystemWatcher fsw = new FileSystemWatcher(SolutionPath, "*.cs");
            fsw.IncludeSubdirectories = true;
            fsw.Changed += delegate { hasChanges = true; };
            fsw.Created += delegate { hasChanges = true; };
            fsw.Deleted += delegate { hasChanges = true; };
            fsw.EnableRaisingEvents = true;

            Console.WriteLine("Continuous Build Starting..");
            hasChanges = true;

            while (true)
            {
                if (hasChanges)
                {
                    foreach (Process p in Process.GetProcessesByName("osu!"))
                        p.Kill();

                    Console.Write("Building...");
                    hasChanges = false;
                    if (RunVisualStudioBuild())
                    {
                        Console.WriteLine("Done!");

                        Console.WriteLine("Starting osu!");
                        Process.Start(SolutionPath + @"osu!\bin\x86\Debug\osu!.exe");
                    }
                    else
                    {
                        Console.WriteLine("ERROR!");
                    }
                }
                Thread.Sleep(50);
            }
        }
    }
}
