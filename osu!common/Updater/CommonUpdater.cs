﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using osu_common.Helpers;
using osu_common.Libraries;
using osu_common.Libraries.NetLib;

namespace osu_common.Updater
{
    public static class CommonUpdater
    {
        static Thread updateThread;

        public static string CENTRAL_UPDATE_URL = @"http://osu.ppy.sh/web/check-updates.php";
        public static string MIRROR_UPDATE_URL = @"http://m1.ppy.sh/release/";

        private static List<DownloadableFileInfo> ActiveFiles = new List<DownloadableFileInfo>();

        private static UpdateCompleteDelegate completeCallback;

        private static int totalUpdatableFiles = 0;

        public static float Percentage
        {
            get
            {
                lock (ActiveFiles)
                {
                    totalUpdatableFiles = Math.Max(ActiveFiles.Count, totalUpdatableFiles);

                    switch (totalUpdatableFiles)
                    {
                        case 0:
                            return 0;
                        case 1:
                            return ActiveFiles.Count > 0 ? ActiveFiles[0].progress : 100;
                        default:
                            float progress = (totalUpdatableFiles - ActiveFiles.Count) * 100;
                            foreach (DownloadableFileInfo dfi in ActiveFiles)
                                progress += dfi.progress;
                            return (progress / (totalUpdatableFiles * 100)) * 100;
                    }
                }
            }
        }

        public static Exception LastError;
        public static string LastErrorExtraInformation = string.Empty;

        public static ReleaseStream ReleaseStream;
        public static UpdateStates State;

        public const string STAGING_FOLDER = @"_staging";
        public const string STAGING_COMPLETE_FOLDER = @"_pending";

        public static string GetStatusString(bool includeProgress = false)
        {
            switch (State)
            {
                case UpdateStates.Checking:
                    return LocalisationManager.GetString(OsuString.CommonUpdater_CheckingForUpdates);
                case UpdateStates.Error:
                    return LocalisationManager.GetString(OsuString.CommonUpdater_ErrorOccurred);
                case UpdateStates.NeedsRestart:
                    return LocalisationManager.GetString(OsuString.CommonUpdater_RestartRequired);
                case UpdateStates.Updating:
                    lock (ActiveFiles)
                    {
                        totalUpdatableFiles = Math.Max(ActiveFiles.Count, totalUpdatableFiles);

                        string progress = includeProgress && Percentage > 0 ? " (" + (int)Percentage + "%)" : string.Empty;

                        if (ActiveFiles.Count == 0 || totalUpdatableFiles == 0)
                            return LocalisationManager.GetString(OsuString.CommonUpdater_PerformingUpdates);

                        int runningCount = ActiveFiles.FindAll(f => f.isRunning).Count;

                        if (runningCount > 1)
                        {
                            if (totalUpdatableFiles == ActiveFiles.Count)
                                return string.Format(LocalisationManager.GetString(OsuString.CommonUpdater_DownloadingRequiredFiles), totalUpdatableFiles) + progress;
                            else
                                //has at least one file finished.
                                return string.Format(LocalisationManager.GetString(OsuString.CommonUpdater_DownloadingRequiredFiles2), totalUpdatableFiles - ActiveFiles.Count, totalUpdatableFiles) + progress;
                        }

                        DownloadableFileInfo lastRunning = ActiveFiles.FindLast(f => f.isRunning);
                        if (lastRunning != null)
                        {
                            if (lastRunning.isPatching)
                                return String.Format(LocalisationManager.GetString(OsuString.CommonUpdater_PatchingFilePercentage), lastRunning.filename) + progress;
                            else
                                return String.Format(LocalisationManager.GetString(OsuString.CommonUpdater_DownloadingFile), lastRunning.filename + (lastRunning.url_patch != null ? @"_patch" : string.Empty)) + progress;
                        }

                        return LocalisationManager.GetString(OsuString.CommonUpdater_PerformingUpdates);
                    }
                default:
                    return LocalisationManager.GetString(OsuString.CommonUpdater_Updated);
            }
        }

        static object UpdateLock = new object();
        public static void Check(UpdateCompleteDelegate callback = null, ReleaseStream stream = Updater.ReleaseStream.Stable, ThreadPriority priority = ThreadPriority.Normal)
        {
            lock (UpdateLock)
            {
                if (updateThread != null) return;

                ReleaseStream = stream;
                completeCallback = callback;

                setCallbackStatus(UpdateStates.Checking);

                updateThread = new Thread(() =>
                {
                    try
                    {
                        UpdateStates state = doUpdate();

                        setCallbackStatus(state);
                    }
                    catch
                    {
                        setCallbackStatus(UpdateStates.NoUpdate);
                    }

                    completeCallback = null;
                    updateThread = null;
                });

                updateThread.IsBackground = true;
                updateThread.Priority = priority;

                updateThread.Start();
            }
        }

        private static void setCallbackStatus(UpdateStates state)
        {
            if (State == state) return;

            State = state;
            if (completeCallback != null) completeCallback(state);
        }

        private static UpdateStates doUpdate()
        {
            try
            {
                ConfigManagerCompact.LoadConfig();

                //ensure we are starting clean.
                //note that we don't clear a pending update just yet because there are scenarios we can use this (see variable stagedAndWaitingToUpdate below).
                Cleanup(10000, false);

                List<DownloadableFileInfo> streamFiles = null;

                string rawString = string.Empty;

                try
                {
                    StringNetRequest sn = new StringNetRequest(CENTRAL_UPDATE_URL + "?action=check&stream=" + ReleaseStream.ToString().ToLower() + @"&time=" + DateTime.Now.Ticks);

                    rawString = sn.BlockingPerform(true);

                    if (rawString == @"fallback")
                        return UpdateStates.EmergencyFallback;

                    streamFiles = JsonConvert.DeserializeObject<List<DownloadableFileInfo>>(rawString);
                }
                catch (Exception e)
                {
                    LastError = e;
                    LastErrorExtraInformation = rawString;
                    return UpdateStates.Error;
                }

                if (streamFiles == null || streamFiles.Count == 0)
                {
                    LastError = new Exception(@"update file returned no results");
                    return UpdateStates.Error;
                }

            tryAgain:
                bool stagedAndWaitingToUpdate = Directory.Exists(STAGING_COMPLETE_FOLDER);
                List<DownloadableFileInfo> updateFiles = new List<DownloadableFileInfo>();

                foreach (DownloadableFileInfo file in streamFiles)
                {
                    if (stagedAndWaitingToUpdate)
                    {
                        //special case where we already have a pending update.
                        //if we find *any* file which doesn't match, we should remove the folder and restart the process.

                        string pendingFilename = STAGING_COMPLETE_FOLDER + @"/" + file.filename;

                        if (File.Exists(pendingFilename))
                        {
                            //pending file exists and matches this update precisely.
                            if (getMd5(pendingFilename) == file.file_hash)
                                continue;
                        }
                        else if (File.Exists(file.filename) && getMd5(file.filename, true) == file.file_hash)
                            //current version is already newest.
                            continue;

                        //something isn't up-to-date. let's run the update process again without our pending version.
                        Reset(false);

                        goto tryAgain;
                    }

                    if (!File.Exists(file.filename))
                        updateFiles.Add(file);
                    else if (getMd5(file.filename, true) != file.file_hash)
                        updateFiles.Add(file); //cached md5 check failed.
                    else if (file.filename == @"osu!.exe" && getMd5(file.filename, true) != getMd5(file.filename)) //special intensive check for main osu!.exe
                    {
                        ConfigManagerCompact.ResetHashes();
                        goto tryAgain;
                    }
                }

                if (stagedAndWaitingToUpdate)
                    //return early, as we've already done the rest of the process below in a previous run.
                    return MoveInPlace() ? UpdateStates.Completed : UpdateStates.NeedsRestart;

                if (updateFiles.Count == 0)
                    return UpdateStates.NoUpdate;

                totalUpdatableFiles = updateFiles.Count;

                if (!Directory.Exists(STAGING_FOLDER))
                {
                    DirectoryInfo di = Directory.CreateDirectory(STAGING_FOLDER);
                    di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                }

                setCallbackStatus(UpdateStates.Updating);

                lock (ActiveFiles) ActiveFiles.AddRange(updateFiles);

                foreach (DownloadableFileInfo file in updateFiles)
                {
                    if (File.Exists(file.filename))
                    {
                        string localHash = getMd5(file.filename);

                        //check for patchability...
                        try
                        {
                            StringNetRequest sn = new StringNetRequest(CENTRAL_UPDATE_URL + "?action=path&stream=" + ReleaseStream.ToString().ToLower() + "&target=" + file.file_version + "&existing=" + localHash + @"&time=" + DateTime.Now.Ticks);
                            List<DownloadableFileInfo> patchFiles = JsonConvert.DeserializeObject<List<DownloadableFileInfo>>(sn.BlockingPerform());

                            if (patchFiles.Count > 1)
                            {
                                //copy the local version to the staging path.

                                File.Copy(file.filename, STAGING_FOLDER + @"/" + file.filename);

                                bool success = false;

                                lock (ActiveFiles)
                                    ActiveFiles.AddRange(patchFiles.GetRange(0, patchFiles.Count - 1));
                                totalUpdatableFiles += patchFiles.Count - 1;

                                try
                                {
                                    //we now know we have a patchable path, so let's try patching!
                                    foreach (DownloadableFileInfo patch in patchFiles)
                                    {
                                        try
                                        {
                                            if (patch.file_version == file.file_version)
                                            {
                                                //reached the end of patching.
                                                if (getMd5(STAGING_FOLDER + @"/" + file.filename) != file.file_hash)
                                                    throw new Exception(@"patching failed to end with correct checksum.");

                                                success = true;
                                                break; //patching successful!
                                            }

                                            string localPatchFilename = STAGING_FOLDER + @"/" + patch.filename + @"_patch";

                                            patch.Perform(true);
                                        }
                                        finally
                                        {
                                            lock (ActiveFiles) ActiveFiles.Remove(patch);
                                        }
                                    }

                                    if (success)
                                    {
                                        lock (ActiveFiles) ActiveFiles.Remove(file);
                                        continue;
                                    }
                                }
                                finally
                                {
                                    lock (ActiveFiles)
                                        patchFiles.ForEach(f => ActiveFiles.Remove(f));
                                }
                            }
                        }
                        catch
                        {
                            //an error occurred when trying to patch; fallback to a full update.
                        }
                    }

                    file.PerformThreaded(delegate
                    {
                        lock (ActiveFiles) ActiveFiles.Remove(file);
                    },
                    delegate
                    {
                        //error occurred
                    });
                }

                while (ActiveFiles.Count > 0)
                {
                    foreach (DownloadableFileInfo dfi in ActiveFiles)
                        if (dfi.Error != null)
                        {
                            LastError = dfi.Error;
                            return UpdateStates.Error;
                        }
                    Thread.Sleep(100);
                }

                if (State == UpdateStates.Updating)
                {
                    if (Directory.Exists(STAGING_COMPLETE_FOLDER))
                        Directory.Delete(STAGING_COMPLETE_FOLDER, true);

                    GeneralHelper.RecursiveMove(STAGING_FOLDER, STAGING_COMPLETE_FOLDER);

                    ConfigManagerCompact.Configuration[@"_ReleaseStream"] = ReleaseStream.ToString();

                    return MoveInPlace() ? UpdateStates.Completed : UpdateStates.NeedsRestart;
                }

                return UpdateStates.NoUpdate;
            }
            catch (ThreadAbortException)
            {
                foreach (DownloadableFileInfo dfi in ActiveFiles)
                    dfi.Abort();
                return UpdateStates.NoUpdate;
            }
            catch (Exception e)
            {
                LastError = e;
                return UpdateStates.Error;
            }
            finally
            {
                ConfigManagerCompact.SaveConfig();
                updateThread = null;
            }
        }

        private static string getMd5(string filename, bool useCache = false)
        {
            if (!File.Exists(filename)) return null;

            if (useCache)
            {
                try
                {
                    if (!ConfigManagerCompact.Configuration.ContainsKey(@"h_" + filename))
                        ConfigManagerCompact.Configuration[@"h_" + filename] = getMd5(filename);
                    return ConfigManagerCompact.Configuration[@"h_" + filename];
                }
                catch (Exception)
                {

                }
            }

            try
            {
                using (Stream s = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    MD5 md5 = MD5.Create();
                    byte[] data = md5.ComputeHash(s);
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < data.Length; i++)
                        sb.Append(data[i].ToString(@"x2"));
                    return sb.ToString();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static bool SafelyMove(string src, string dest, int timeoutMilliseconds = 2000, int retryCount = 5, bool allowDefiniteMove = true)
        {
            int waitLength = timeoutMilliseconds / retryCount;

            try
            {
                GeneralHelper.FileDelete(dest + @"_old");
            }
            catch { }

            while (retryCount-- > 0)
            {
                try
                {
                    if (allowDefiniteMove)
                        GeneralHelper.FileDelete(dest);
                    else
                        File.Delete(dest);
                }
                catch
                { }

                try
                {
                    File.Move(src, dest);
                    return true; //success pattern 1
                }
                catch
                {
                    try
                    {
                        File.Copy(src, dest, true);
                        GeneralHelper.FileDelete(src);
                        return true; //success pattern 2
                    }
                    catch
                    {
                    }
                }

                Thread.Sleep(waitLength);
            }

            return false;
        }

        public static bool MoveInPlace(bool allowDefiniteMove = false)
        {
            if (!Directory.Exists(STAGING_COMPLETE_FOLDER)) return true;

            ConfigManagerCompact.LoadConfig();

            try
            {
                foreach (string f in Directory.GetFiles(STAGING_COMPLETE_FOLDER))
                {
                    if (new FileInfo(f).Length == 0)
                    {
                        GeneralHelper.FileDelete(f);
                        continue;
                    }

                    string destination = Path.GetFileName(f);

                    if (SafelyMove(f, destination, 200, 5, allowDefiniteMove))
                        ConfigManagerCompact.Configuration[@"h_" + destination] = getMd5(destination);
                    else
                        return false;
                }
            }
            catch
            {
                return false;
            }

            Cleanup();

            ConfigManagerCompact.SaveConfig();

            return true;
        }

        public static bool Cleanup(int waitTime = 5000, bool cleanPending = true)
        {
            const int sleep_period = 100;

            //in case the user has had an issue with the inline updater, this will ensure they can still run osu! after running osume
            //using the old update method.
            int attempts = Math.Max(2, waitTime / sleep_period);

            while (attempts-- > 0)
            {
                bool couldClean = true;

                if (cleanPending)
                {
                    try
                    {
                        if (File.Exists(STAGING_COMPLETE_FOLDER))
                            File.Delete(STAGING_COMPLETE_FOLDER);
                    }
                    catch
                    {
                        couldClean = false;
                    }

                    try
                    {
                        if (Directory.Exists(STAGING_COMPLETE_FOLDER))
                            Directory.Delete(STAGING_COMPLETE_FOLDER, true);
                    }
                    catch
                    {
                        couldClean = false;
                    }
                }

                try
                {
                    if (File.Exists(STAGING_FOLDER))
                        File.Delete(STAGING_FOLDER);
                }
                catch
                {
                    couldClean = false;
                }

                try
                {
                    if (Directory.Exists(STAGING_FOLDER))
                        Directory.Delete(STAGING_FOLDER, true);
                }
                catch
                {
                    couldClean = false;
                }

                if (couldClean) return true;

                Thread.Sleep(sleep_period);
            }

            return false;
        }

        public static void Reset(bool abortThread = true)
        {
            if (abortThread)
            {
                Thread t = updateThread;
                if (t != null && t.IsAlive)
                {
                    t.Abort();
                    while (t.IsAlive) Thread.Sleep(20);
                }

                updateThread = null;
            }

            lock (ActiveFiles) ActiveFiles.Clear();
            totalUpdatableFiles = 0;

            Cleanup();
            ResetError();
        }

        public static void ResetError()
        {
            LastError = null;
            LastErrorExtraInformation = string.Empty;
        }
    }

    public delegate void UpdateCompleteDelegate(UpdateStates state);

    public enum UpdateStates
    {
        NoUpdate,
        Checking,
        Updating,
        Error,
        NeedsRestart,
        Completed,
        EmergencyFallback
    }

    public enum ReleaseStream
    {
        Stable,
        Beta,
        CuttingEdge,
        CuttingEdge45
    }
}
