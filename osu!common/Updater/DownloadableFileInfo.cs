﻿using System;
using System.IO;
using System.Threading;
using ICSharpCode.SharpZipLib.Zip;
using osu_common.Helpers;
using osu_common.Libraries;
using osu_common.Libraries.NetLib;

namespace osu_common.Updater
{
    public class DownloadableFileInfo : UpdaterFileInfo
    {
        internal FileNetRequest netRequest;
        internal float progress;
        internal bool isPatching;
        internal bool isRunning;
        private int downloaded_bytes;
        public Exception Error;

        internal void Perform(bool doPatching = false)
        {
            isRunning = true;
            progress = 0;
            downloaded_bytes = 0;
            isPatching = false;

            if (doPatching && url_patch == null)
                throw new Exception(@"patch not available for this file!");

            string localPath = CommonUpdater.STAGING_FOLDER + "/" + filename + (doPatching ? "_patch" : @".zip");

            netRequest = new FileNetRequest(localPath, doPatching ? url_patch : (url_full + @".zip"));
            netRequest.onUpdate += delegate(object sender, long current, long total)
            {
                progress = (float)current / total * (doPatching ? 50 : 100);
                downloaded_bytes = (int)current;
                filesize = (int)total;
            };

            try
            {
                netRequest.Perform(true);
                if (!File.Exists(localPath))
                    throw new Exception(@"couldn't find downloaded file (" + localPath + ")");
            }
            catch (ThreadAbortException)
            {
                isRunning = false;
                return;
            }

            if (netRequest.m_filename.EndsWith(@".zip"))
            {
                FastZip fz = new FastZip();
                fz.ExtractZip(netRequest.m_filename, CommonUpdater.STAGING_FOLDER, @".*");
                GeneralHelper.FileDelete(netRequest.m_filename);
            }

            if (doPatching)
            {
                string beforePatch = CommonUpdater.STAGING_FOLDER + @"/" + filename;
                string afterPatch = CommonUpdater.STAGING_FOLDER + @"/" + filename + @"_patched";

                try
                {
                    isPatching = true;
                    BSPatcher patcher = new BSPatcher();
                    patcher.OnProgress += delegate(object sender, long current, long total) { progress = 50 + (float)current / total * 50; };
                    patcher.Patch(beforePatch, afterPatch, localPath);
                }
                finally
                {
                    GeneralHelper.FileDelete(localPath);
                    GeneralHelper.FileDelete(beforePatch);
                    if (File.Exists(afterPatch))
                        GeneralHelper.FileMove(afterPatch, beforePatch);
                    isPatching = false;
                }

                isRunning = false;
            }
        }

        private Thread thread;
        internal void PerformThreaded(VoidDelegate onComplete = null, VoidDelegate onError = null)
        {
            thread = new Thread(() =>
            {
                int attempts = 0;
                const int max_attempts = 4;

            retry:
                try
                {
                    Perform();
                    if (onComplete != null) onComplete();
                }
                catch (ThreadAbortException)
                {
                    Abort();
                }
                catch (SocketError ex)
                {
                    while (++attempts < max_attempts)
                    {
                        if (attempts > 1)
                        {
                            string currentMirror = @"m" + (attempts - 1) + @".";
                            string nextMirror = @"m" + attempts + @".";

                            if (!string.IsNullOrEmpty(url_full)) url_full = url_full.Replace(currentMirror, nextMirror);
                            if (!string.IsNullOrEmpty(url_patch)) url_patch = url_patch.Replace(currentMirror, nextMirror);
                        }

                        Thread.Sleep(1500);
                        goto retry;
                    }

                    CommonUpdater.LastErrorExtraInformation = url_patch ?? url_full;

                    Error = ex;
                    if (onError != null) onError();

                }
                catch (Exception ex)
                {
                    Error = ex;
                    if (onError != null) onError();
                }
            });

            thread.IsBackground = true;
            thread.Start();
        }

        internal void Abort()
        {
            if (thread != null)
            {
                thread.Abort();
                thread = null;
            }
        }
    }
}
