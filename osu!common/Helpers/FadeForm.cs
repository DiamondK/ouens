﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace osu_common.Helpers
{
    public class FadeForm : Form
    {
        private Timer timer;
        private bool fadeInComplete;

        public FadeForm()
        {
            Opacity = 0;
            timer = new Timer();
            timer.Interval = 10;
            timer.Tick += t_Tick;
            timer.Start();
        }

        void t_Tick(object sender, EventArgs e)
        {
            if (IsDisposed || Opacity == 1)
            {
                timer.Stop();
                fadeInComplete = true;
                return;
            }

            try
            {
                Opacity = Math.Min(1, Opacity + Math.Max(0.01f, (1 - Opacity) * 0.1));
            }
            catch { }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FadeForm
            // 
            this.ClientSize = new System.Drawing.Size(0, 0);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.ResumeLayout(false);

        }
    }
}
