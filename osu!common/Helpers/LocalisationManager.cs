﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using osu_common.Libraries.NetLib;
using osu_common.Updater;
using System.Globalization;
using System.Threading;

namespace osu_common.Helpers
{
    public static partial class LocalisationManager
    {
        private static Dictionary<OsuString, string> strings = new Dictionary<OsuString, string>();
        public static bool IsInitialised;
        public static string CurrentLanguage = string.Empty;

        public static string GetString(OsuString stringType)
        {
            if (!IsInitialised)
                init();

            try
            {
                return strings[stringType];
            }
            catch
            {
                return string.Empty;
            }
        }

        public static bool SetLanguage(string langCode = null, bool forceUpdate = false, VoidDelegate onCompletionAction = null, string defaults = null)
        {
            Thread MainThread = Thread.CurrentThread;

            if (string.IsNullOrEmpty(langCode) || langCode == @"unknown")
            {
                //set a default language
                string culture = MainThread.CurrentUICulture.Name;
                string major = culture.Substring(0, 2);
                switch (major)
                {
                    default:
                        langCode = major;
                        break;
                    case @"ms":
                    case @"vi":
                    case @"zh":
                        langCode = culture;
                        break;
                }
            }

            IsInitialised = false;
            CurrentLanguage = langCode;

            if (langCode == @"en" || !forceUpdate)
            {
                if (init(defaults))
                {
                    try
                    {
                        MainThread.CurrentUICulture = new CultureInfo(langCode);
                    }
                    catch
                    {
                    }

                    if (onCompletionAction != null) onCompletionAction();

                    //successful completion
                    return true;
                }
            }

            //we want to download an updated version of this localisation...
            string filename = @"Localisation/" + langCode + @".txt";
            string newFilename = filename + @".new";

            FileNetRequest req = new FileNetRequest(newFilename, CommonUpdater.MIRROR_UPDATE_URL + @"Localisation/" + langCode + @".txt?" + DateTime.Today.ToFileTimeUtc());
            req.onFinish += delegate(string location, Exception e)
            {
                if (e != null)
                {
                    //error occurred. try to use existing copy if it exists, 
                    //else fail with thread localisation
                    if (!File.Exists(filename))
                    {
                        (new Thread(new ThreadStart(delegate
                        {
                            //Make sure we don't overload the client in-case FNR continuously fails
                            Thread.Sleep(1000);
                            SetLanguage(null, false, onCompletionAction, defaults);
                        }))
                        {
                            IsBackground = true
                        }).Start();

                        return;
                    }
                }
                else
                    GeneralHelper.FileMove(newFilename, filename);

                SetLanguage(langCode, false, onCompletionAction, defaults);
            };

            NetManager.AddRequest(req);
            return false;
        }

        private static bool init(string defaults = @"")
        {
            lock (strings)
            {
                if (IsInitialised) return true; //we may have already initialised in another threaded instance.

                try
                {
                    strings.Clear();

                    if (!string.IsNullOrEmpty(defaults))
                        readResourcesFromString(defaults);
                    else
                        readResources(@"en");

                    if (CurrentLanguage == @"en")
                        return true;


                    return readResources(CurrentLanguage);
                }
                finally
                {
                    IsInitialised = true;
                }
            }
        }

        private static bool readResourcesFromString(string input)
        {
            using (StringReader sr = new StringReader(input))
            {
                while (sr.Peek() != -1)
                {
                    string line = sr.ReadLine();
                    int index = line.IndexOf('=');
                    if (index <= 0)
                        continue;
                    try
                    {
                        strings[(OsuString)Enum.Parse(typeof(OsuString), line.Remove(index), false)] = line.Substring(index + 1).Replace("\\n", "\n");
                    }
                    catch { }
                }
            }

            return true;
        }

        private static bool readResources(string lang)
        {
            string resFile = @"Localisation/" + lang + @".txt";

            if (!File.Exists(resFile))
                return false;

            try
            {
                readResourcesFromString(File.ReadAllText(resFile));
            }
            catch
            {
                return false;
            }

            return true;
        }

    }
}
