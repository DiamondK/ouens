﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using osu_common.Helpers;

namespace osu.Helpers
{
    //marshals delegates to run from the scheduler's basethread in a threadsafe manner
    public class Scheduler
    {
        private readonly Queue<VoidDelegate> schedulerQueue = new Queue<VoidDelegate>();
        private readonly pList<ScheduledDelegate> timedQueue = new pList<ScheduledDelegate>();
        protected int mainThreadID;
        private Stopwatch timer = new Stopwatch();

        //we assume that basethread calls the constructor
        public Scheduler()
        {
            mainThreadID = Thread.CurrentThread.ManagedThreadId;
            timer.Start();
        }

        //run scheduled events, returns true if any tasks were run.
        public bool Update()
        {
            VoidDelegate[] runnable;

            lock (timedQueue)
            {
                long currentTime = timer.ElapsedMilliseconds;

                for (int i = 0; i < timedQueue.Count; i++)
                {
                    ScheduledDelegate sd = timedQueue[i];
                    if (sd.ExecuteTime < currentTime)
                    {
                        lock (schedulerQueue)
                            schedulerQueue.Enqueue(sd.Task);
                        timedQueue.RemoveAt(i--);

                        if (sd.RepeatInterval > 0)
                            timedQueue.AddInPlace(new ScheduledDelegate(sd.Task, timer.ElapsedMilliseconds + sd.RepeatInterval, sd.RepeatInterval));
                    }
                }

                //while (timedQueue.Count > 0 && timedQueue[0].ExecuteTime <= currentTime)
                //{
                //    schedulerQueue.Enqueue(timedQueue[0].Task);
                //    timedQueue.RemoveAt(0);
                //}
            }

            lock (schedulerQueue)
            {
                int c = schedulerQueue.Count;
                if (c == 0) return false;
                runnable = new VoidDelegate[c];
                schedulerQueue.CopyTo(runnable, 0);
                schedulerQueue.Clear();
            }

            foreach (VoidDelegate v in runnable)
            {
                try
                {
                    VoidDelegate mi = new VoidDelegate(v);
                    mi.Invoke();
                }
                catch (Exception e)
                {
                    Console.WriteLine("super duper error on delegate " + e);
                }
            }
            return true;
        }

        public void AddDelayed(VoidDelegate d, int timeUntilRun, bool repeat = false)
        {
            ScheduledDelegate del = new ScheduledDelegate(d, timer.ElapsedMilliseconds + timeUntilRun, repeat ? timeUntilRun : 0);
            lock (timedQueue) timedQueue.AddInPlace(del);
        }

        public void AddBackground(VoidDelegate d)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object sender)
            {
                d();
            }));
        }

        public virtual void Add(VoidDelegate d, bool forceDelayed = false)
        {
            if (!forceDelayed && Thread.CurrentThread.ManagedThreadId == mainThreadID)
            {
                //We are on the main thread already - don't need to schedule.
                d.Invoke();
                return;
            }

            lock (schedulerQueue)
                schedulerQueue.Enqueue(d);
        }

        /// <summary>
        /// Adds a task which will only be run once per frame, no matter how many times it was scheduled in the previous frame.
        /// </summary>
        /// <returns>Whether this is the first queue attempt of this work.</returns>
        public bool AddOnce(VoidDelegate d)
        {
            if (schedulerQueue.Contains(d))
                return false;

            lock (schedulerQueue)
                schedulerQueue.Enqueue(d);

            return true;
        }
    }

    struct ScheduledDelegate : IComparable<ScheduledDelegate>
    {
        public ScheduledDelegate(VoidDelegate task, long time, int repeatInterval = 0)
        {
            Task = task;
            ExecuteTime = time;
            RepeatInterval = repeatInterval;
        }

        public VoidDelegate Task;
        public long ExecuteTime;
        public int RepeatInterval;

        #region IComparable<ScheduledDelegate> Members

        public int CompareTo(ScheduledDelegate other)
        {
            return ExecuteTime == other.ExecuteTime ? -1 : ExecuteTime.CompareTo(other.ExecuteTime);
        }

        #endregion
    }
}
