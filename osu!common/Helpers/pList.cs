﻿using System;
using System.Collections.Generic;
using System.Text;

namespace osu_common.Helpers
{
    public class pList<T> : List<T>
    {
        private IComparer<T> comparer;

        public pList()
        { }

        public pList(List<T> otherlist)
            : base(otherlist)
        {
        }

        public pList(int capacity)
            : base(capacity)
        {
        }

        public pList(IComparer<T> comparer)
        {
            this.comparer = comparer;
        }

        public int AddInPlace(T item)
        {
            int index = comparer != null ? BinarySearch(item, comparer) : BinarySearch(item);
            if (index < 0) index = ~index;
            Insert(index, item);
            return index;
        }
    }
}
