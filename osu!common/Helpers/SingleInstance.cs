﻿using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;

namespace osu_common.Helpers
{
    /// <summary>
    /// Enforces single instance for an application.
    /// </summary>
    public class SingleInstance : IDisposable
    {
        private Mutex mutex = null;
        private Boolean ownsMutex = false;
        private Guid identifier;

        /// <summary>
        /// Enforce a single instance of a code section (use using).
        /// </summary>
        /// <param name="identifier">instance identifier</param>
        /// <param name="timeout">time to wait for existing instances to disappear. set to -1 to allow concurrent instances.</param>
        public SingleInstance(Guid identifier, int timeout = 5000)
        {
            this.identifier = identifier;
            
            init: mutex = new Mutex(true, identifier.ToString(), out ownsMutex);

            if (timeout < 0) return;

            try
            {
                if (!mutex.WaitOne(timeout, false))
                    throw new TooManyInstancesException();
            }
            catch (AbandonedMutexException)
            {
                goto init;
            }
        }

        /// <summary>
        /// Indicates whether this is the first instance of this application.
        /// </summary>
        public Boolean IsFirstInstance
        { get { return ownsMutex; } }

        #region IDisposable
        private Boolean disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (mutex != null)
                {
                    try
                    {
                        mutex.Close();
                    }
                    catch { }

                    mutex = null;
                }
                disposed = true;
            }
        }

        ~SingleInstance()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }

    public class TooManyInstancesException : Exception
    {
    }
}