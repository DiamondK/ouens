﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.AccessControl;
using System.Text;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Threading;
using System.Security.Principal;

namespace osu_common.Helpers
{
    public static class GeneralHelper
    {
        public const int MAX_PATH_LENGTH = 248;

        public const string CLEANUP_DIRECTORY = @"_cleanup";

        public static string FormatEnum(string s)
        {
            string o = string.Empty;
            for (int i = 0; i < s.Length; i++)
            {
                if (i > 0 && Char.IsUpper(s[i]))
                    o += @" ";
                o += s[i];
            }

            return o;
        }

        public static string WindowsFilenameStrip(string entry)
        {
            foreach (char c in Path.GetInvalidFileNameChars())
                entry = entry.Replace(c.ToString(), string.Empty);
            return entry;
        }

        public static string WindowsPathStrip(string entry)
        {
            foreach (char c in Path.GetInvalidFileNameChars())
                entry = entry.Replace(c.ToString(), string.Empty);
            entry = entry.Replace(".", string.Empty);
            return entry;
        }

        public static bool RegistryCheck(string progId, string executable)
        {
            try
            {
                using (RegistryKey key = Registry.ClassesRoot.OpenSubKey(progId))
                {
                    if (key == null || key.OpenSubKey(@"shell\open\command").GetValue(String.Empty).ToString() != String.Format("\"{0}\" \"%1\"", executable))
                        return false;
                    return true;
                }
            }
            catch
            {
            }

            return false;
        }

        public static bool RegistryAdd(string extension, string progId, string description, string executable, bool urlHandler = false)
        {
            try
            {
                if (!string.IsNullOrEmpty(extension))
                {
                    if (extension[0] != '.')
                        extension = "." + extension;

                    // register the extension, if necessary
                    using (RegistryKey key = Registry.ClassesRoot.OpenSubKey(extension, true))
                    {
                        if (key == null)
                        {
                            using (RegistryKey extKey = Registry.ClassesRoot.CreateSubKey(extension))
                            {
                                extKey.SetValue(String.Empty, progId);
                            }
                        }
                        else
                        {
                            key.SetValue(String.Empty, progId);
                        }
                    }
                }

                // register the progId, if necessary
                using (RegistryKey key = Registry.ClassesRoot.OpenSubKey(progId))
                {
                    if (key == null || key.OpenSubKey("shell\\open\\command").GetValue(String.Empty).ToString() != String.Format("\"{0}\" \"%1\"", executable))
                    {
                        using (RegistryKey progIdKey = Registry.ClassesRoot.CreateSubKey(progId))
                        {
                            progIdKey.SetValue(String.Empty, description);
                            if (urlHandler) progIdKey.SetValue("URL Protocol", string.Empty);

                            using (RegistryKey command = progIdKey.CreateSubKey("shell\\open\\command"))
                            {
                                command.SetValue(String.Empty, String.Format("\"{0}\" \"%1\"", executable));
                            }

                            using (RegistryKey command = progIdKey.CreateSubKey("DefaultIcon"))
                            {
                                command.SetValue(String.Empty, String.Format("\"{0}\",1", executable));
                            }
                        }
                    }
                    // Add new icon for existing osu! users anyway
                    else if (key.OpenSubKey("DefaultIcon").GetValue(String.Empty) == null)
                    {
                        using (RegistryKey progIdKey = Registry.ClassesRoot.CreateSubKey(progId))
                        {
                            using (RegistryKey command = progIdKey.CreateSubKey("DefaultIcon"))
                            {
                                command.SetValue(String.Empty, String.Format("\"{0}\",1", executable));
                            }
                        }
                    }
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static void RegistryDelete(string progId)
        {
            try
            {
                if (null != Registry.ClassesRoot.OpenSubKey(progId))
                    Registry.ClassesRoot.DeleteSubKeyTree(progId);
            }
            catch
            {
            }

        }

        // Adds an ACL entry on the specified directory for the specified account. 
        public static void AddDirectorySecurity(string FileName, string Account, FileSystemRights Rights,
                                                InheritanceFlags Inheritance, PropagationFlags Propogation,
                                                AccessControlType ControlType)
        {
            // Create a new DirectoryInfo object. 
            DirectoryInfo dInfo = new DirectoryInfo(FileName);
            // Get a DirectorySecurity object that represents the  
            // current security settings. 
            DirectorySecurity dSecurity = dInfo.GetAccessControl();
            // Add the FileSystemAccessRule to the security settings.  
            dSecurity.AddAccessRule(new FileSystemAccessRule(Account,
                                                             Rights,
                                                             Inheritance,
                                                             Propogation,
                                                             ControlType));
            // Set the new access settings. 
            dInfo.SetAccessControl(dSecurity);
        }

        public static string UrlEncode(string str)
        {
            if (str == null)
            {
                return null;
            }
            return UrlEncode(str, Encoding.UTF8, false);
        }

        public static string UrlEncodeParam(string str)
        {
            if (str == null)
            {
                return null;
            }
            return UrlEncode(str, Encoding.UTF8, true);
        }

        public static string UrlEncode(string str, Encoding e, bool paramEncode)
        {
            if (str == null)
            {
                return null;
            }
            return Encoding.ASCII.GetString(UrlEncodeToBytes(str, e, paramEncode));
        }

        public static byte[] UrlEncodeToBytes(string str, Encoding e, bool paramEncode)
        {
            if (str == null)
            {
                return null;
            }
            byte[] bytes = e.GetBytes(str);
            return UrlEncodeBytesToBytespublic(bytes, 0, bytes.Length, false, paramEncode);
        }

        private static byte[] UrlEncodeBytesToBytespublic(byte[] bytes, int offset, int count, bool alwaysCreateReturnValue, bool paramEncode)
        {
            int num = 0;
            int num2 = 0;
            for (int i = 0; i < count; i++)
            {
                char ch = (char)bytes[offset + i];
                if (paramEncode && ch == ' ')
                {
                    num++;
                }
                else if (!IsSafe(ch))
                {
                    num2++;
                }
            }
            if ((!alwaysCreateReturnValue && (num == 0)) && (num2 == 0))
            {
                return bytes;
            }
            byte[] buffer = new byte[count + (num2 * 2)];
            int num4 = 0;
            for (int j = 0; j < count; j++)
            {
                byte num6 = bytes[offset + j];
                char ch2 = (char)num6;
                if (IsSafe(ch2))
                {
                    buffer[num4++] = num6;
                }
                else if (paramEncode && ch2 == ' ')
                {
                    buffer[num4++] = 0x2b;
                }
                else
                {
                    buffer[num4++] = 0x25;
                    buffer[num4++] = (byte)IntToHex((num6 >> 4) & 15);
                    buffer[num4++] = (byte)IntToHex(num6 & 15);
                }
            }
            return buffer;
        }

        public static bool IsSafe(char ch)
        {
            if ((((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z'))) || ((ch >= '0') && (ch <= '9')))
            {
                return true;
            }
            switch (ch)
            {
                case '\'':
                case '(':
                case ')':
                case '*':
                case '-':
                case '.':
                case '_':
                case '!':
                    return true;
            }
            return false;
        }

        public static char IntToHex(int n)
        {
            if (n <= 9)
            {
                return (char)(n + 0x30);
            }
            return (char)((n - 10) + 0x61);
        }

        public static void RemoveReadOnlyRecursive(string s)
        {
            foreach (string f in Directory.GetFiles(s))
            {
                FileInfo myFile = new FileInfo(f);
                if ((myFile.Attributes & FileAttributes.ReadOnly) > 0)
                    myFile.Attributes &= ~FileAttributes.ReadOnly;
            }

            foreach (string d in Directory.GetDirectories(s))
                RemoveReadOnlyRecursive(d);
        }

        public static bool FileMove(string src, string dest, bool overwrite = true, bool useTimeout = true)
        {
            src = PathSanitise(src);
            dest = PathSanitise(dest);
            if (src == dest)
                return true; //no move necessary

            try
            {
                if (overwrite)
                    FileDelete(dest);
                File.Move(src, dest);
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Converts all slashes and backslashes to OS-specific directory separator characters. Useful for sanitising user input.
        /// </summary>
        public static string PathSanitise(string path)
        {
            return path.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar).TrimEnd(Path.DirectorySeparatorChar);
        }

        /// <summary>
        /// Converts all OS-specific directory separator characters to '/'. Useful for outputting to a config file or similar.
        /// </summary>
        public static string PathStandardise(string path)
        {
            return path.Replace(Path.DirectorySeparatorChar, '/');
        }

        [Flags]
        internal enum MoveFileFlags
        {
            None = 0,
            ReplaceExisting = 1,
            CopyAllowed = 2,
            DelayUntilReboot = 4,
            WriteThrough = 8,
            CreateHardlink = 16,
            FailIfNotTrackable = 32,
        }

        internal static class NativeMethods
        {
            [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
            public static extern bool MoveFileEx(
                string lpExistingFileName,
                string lpNewFileName,
                MoveFileFlags dwFlags);
        }

        public static bool FileDeleteOnReboot(string filename)
        {
            filename = PathSanitise(filename);

            try
            {
                File.Delete(filename);
                return true;
            }
            catch { }

            string deathLocation = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
            try
            {
                File.Move(filename, deathLocation);
            }
            catch
            {
                deathLocation = filename;
            }

            return NativeMethods.MoveFileEx(deathLocation, null, MoveFileFlags.DelayUntilReboot);
        }

        public static bool FileDelete(string filename)
        {
            filename = PathSanitise(filename);

            if (!File.Exists(filename)) return false;

            try
            {
                File.Delete(filename);
                return true;
            }
            catch { }

            try
            {
                //try alternative method: move to a cleanup folder and delete later.
                if (!Directory.Exists(@"_cleanup"))
                {
                    DirectoryInfo di = Directory.CreateDirectory(@"_cleanup");
                    di.Attributes |= FileAttributes.Hidden;
                }

                File.Move(filename, CLEANUP_DIRECTORY + @"/" + Guid.NewGuid());
                return true;
            }
            catch { }

            return false;
        }

        public static string AsciiOnly(string input)
        {
            if (input == null) return null;

            input = PathStandardise(input);
            int last = input.LastIndexOf('/');

            string asc = last >= 0 ? input.Substring(0, last + 1) : string.Empty;

            foreach (char c in last >= 0 ? input.Remove(0, last + 1) : input)
                if (c <= 126)
                    asc += c;
            return asc;
        }

        public static void RecursiveMove(string oldDirectory, string newDirectory)
        {
            oldDirectory = PathSanitise(oldDirectory);
            newDirectory = PathSanitise(newDirectory);

            if (oldDirectory == @"Songs" + Path.DirectorySeparatorChar)
                return;

            foreach (string dir in Directory.GetDirectories(oldDirectory))
            {
                string newSubDirectory = dir;
                newSubDirectory = Path.Combine(newDirectory, newSubDirectory.Remove(0, 1 + newSubDirectory.LastIndexOf(Path.DirectorySeparatorChar)));

                try
                {
                    DirectoryInfo newDirectoryInfo = Directory.CreateDirectory(newSubDirectory);

                    if ((new DirectoryInfo(dir).Attributes & FileAttributes.Hidden) > 0)
                        newDirectoryInfo.Attributes |= FileAttributes.Hidden;
                }
                catch { }

                RecursiveMove(dir, newSubDirectory);
            }

            bool didExist = Directory.Exists(newDirectory);
            if (!didExist)
            {
                DirectoryInfo newDirectoryInfo = Directory.CreateDirectory(newDirectory);
                try
                {
                    if ((new DirectoryInfo(oldDirectory).Attributes & FileAttributes.Hidden) > 0)
                        newDirectoryInfo.Attributes |= FileAttributes.Hidden;
                }
                catch { }
            }

            foreach (string file in Directory.GetFiles(oldDirectory))
            {
                string newFile = Path.Combine(newDirectory, Path.GetFileName(file));

                bool didMove = FileMove(file, newFile, didExist, false); //allow timeout period?
                if (!didMove)
                {
                    try
                    {
                        File.Copy(file, newFile);
                    }
                    catch { }
                    File.Delete(file);
                }

            }

            Directory.Delete(oldDirectory, true);
        }

        public static int GetMaxPathLength(string directory)
        {
            int highestPathLength = directory.Length;
            int tempPathLength;

            foreach (string file in Directory.GetFiles(directory))
            {
                if (file.Length > highestPathLength)
                    highestPathLength = file.Length;
            }

            foreach (string dir in Directory.GetDirectories(directory))
            {
                tempPathLength = GetMaxPathLength(dir);
                if (tempPathLength > highestPathLength)
                    highestPathLength = tempPathLength;
            }

            return highestPathLength;
        }

        public static string CleanStoryboardFilename(string filename)
        {
            return PathStandardise(filename.Trim(new[] { '"' }));
        }

        //This is better than encoding as it doesn't check for origin specific data or remove invalid chars.
        public unsafe static string rawBytesToString(byte[] encoded)
        {
            if (encoded.Length == 0)
                return String.Empty;

            char[] converted = new char[(encoded.Length + 1) / 2];
            fixed (byte* bytePtr = encoded)
            fixed (char* stringPtr = converted)
            {
                byte* stringBytes = (byte*)stringPtr;
                byte* stringEnd = (byte*)(stringPtr) + converted.Length * 2;
                byte* bytePtr2 = bytePtr;
                do
                {
                    *stringBytes = *(bytePtr2++);
                    stringBytes++;
                } while (stringBytes != stringEnd);
            }
            return new string(converted);
        }


        //This is better than encoding as it doesn't check for origin specific data or remove invalid chars.
        public unsafe static byte[] rawStringToBytes(string decoded)
        {
            if (decoded == string.Empty)
                return new byte[] { };

            char[] decodedC = decoded.ToCharArray();
            byte[] converted = new byte[decodedC.Length * 2];
            fixed (char* stringPtr = decodedC)
            fixed (byte* convertedB = converted)
            {
                char* stringPtr2 = stringPtr;
                char* convertedC = (char*)convertedB;
                char* convertedEnd = convertedC + converted.Length;
                do
                {
                    *convertedC = *(stringPtr2++);
                    convertedC++;
                } while (convertedC != convertedEnd);
            }
            return converted;
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }


        public static unsafe bool CompareByteSequence(byte[] a1, byte[] a2)
        {
            if (a1 == null || a2 == null || a1.Length != a2.Length)
                return false;
            fixed (byte* p1 = a1, p2 = a2)
            {
                byte* x1 = p1, x2 = p2;
                int l = a1.Length;
                for (int i = 0; i < l / 8; i++, x1 += 8, x2 += 8)
                    if (*((long*)x1) != *((long*)x2)) return false;
                if ((l & 4) != 0) { if (*((int*)x1) != *((int*)x2)) return false; x1 += 4; x2 += 4; }
                if ((l & 2) != 0) { if (*((short*)x1) != *((short*)x2)) return false; x1 += 2; x2 += 2; }
                if ((l & 1) != 0) if (*((byte*)x1) != *((byte*)x2)) return false;
                return true;
            }
        }

        public static TDest[] ConvertArray<TSource, TDest>(TSource[] source)
            where TSource : struct
            where TDest : struct
        {

            if (source == null)
                throw new ArgumentNullException("source");

            var sourceType = typeof(TSource);
            var destType = typeof(TDest);

            if (sourceType == typeof(char) || destType == typeof(char))
                throw new NotSupportedException(
                    "Can not convert from/to a char array. Char is special " +
                    "in a somewhat unknown way (like enums can't be based on " +
                    "char either), and Marshal.SizeOf returns 1 even when the " +
                    "values held by a char can be above 255."
                );

            var sourceByteSize = Buffer.ByteLength(source);
            var destTypeSize = Marshal.SizeOf(destType);
            if (sourceByteSize % destTypeSize != 0)
                throw new Exception(
                    "The source array is " + sourceByteSize + " bytes, which can " +
                    "not be transfered to chunks of " + destTypeSize + ", the size " +
                    "of type " + typeof(TDest).Name + ". Change destination type or " +
                    "pad the source array with additional values."
                );

            var destCount = sourceByteSize / destTypeSize;
            var destArray = new TDest[destCount];

            Buffer.BlockCopy(source, 0, destArray, 0, sourceByteSize);

            return destArray;
        }

        public static bool SetDirectoryPermissions(string directory)
        {
            SecurityIdentifier sid = new SecurityIdentifier(WellKnownSidType.BuiltinUsersSid, null);
            NTAccount acct = sid.Translate(typeof(NTAccount)) as NTAccount;
            string strEveryoneAccount = acct.ToString();

            try
            {

                GeneralHelper.AddDirectorySecurity(directory, strEveryoneAccount, FileSystemRights.FullControl,
                                                   InheritanceFlags.None, PropagationFlags.NoPropagateInherit,
                                                   AccessControlType.Allow);
                GeneralHelper.AddDirectorySecurity(directory, strEveryoneAccount, FileSystemRights.FullControl,
                                                   InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                                                   PropagationFlags.InheritOnly, AccessControlType.Allow);

                GeneralHelper.RemoveReadOnlyRecursive(directory);
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
