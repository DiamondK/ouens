﻿using System;
using System.Collections.Generic;
using System.Text;

namespace osu_common.Helpers
{
    public delegate void VoidDelegate();
    public delegate void StringDelegate(string s);
    public delegate void BoolDelegate(bool b);
    public delegate bool BoolReturnDelegate(bool b);
}
