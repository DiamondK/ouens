﻿using System;
using System.Collections.Generic;
using System.Text;

namespace osu_common.Helpers
{
    public class pLockedList<T> : pList<T>
    {
        public object Lock = new object();
        public pLockedList()
        { }

        public pLockedList(List<T> otherlist)
            : base(otherlist)
        {
        }

        public bool TryAdd(T item)
        {
            lock (Lock)
            {
                if (Contains(item)) return false;
                Add(item);
                return true;
            }
        }

        public new bool Contains(T item)
        {
            lock (Lock) return base.Contains(item);
        }

        public new void InsertRange(int index, IEnumerable<T> collection)
        {
            lock (Lock) base.InsertRange(index, collection);
        }

        public new bool Remove(T item)
        {
            lock (Lock) return base.Remove(item);
        }

        public new void Add(T item)
        {
            lock (Lock) base.Add(item);
        }

        public new void ForEach(Action<T> action)
        {
            GetCopy().ForEach(action);
        }

        public new void Clear()
        {
            lock (Lock) base.Clear();
        }

        /// <summary>
        /// Get a copy and clear list.
        /// </summary>
        /// <returns></returns>
        public List<T> Empty()
        {
            lock (Lock)
            {
                List<T> copy = GetCopy();
                base.Clear();
                base.TrimExcess();
                return copy;
            }
        }

        public List<T> GetCopy()
        {
            lock (Lock) return new List<T>(this);
        }

        public new T Find(Predicate<T> match)
        {
            lock (Lock) return base.Find(match);
        }

    }
}
