﻿using System;
using System.Globalization;

namespace osu_common
{
    [Flags]
    public enum Mods
    {
        None                   = 0,
        NoFail                 = 1 << 0,
        Easy                   = 1 << 1,
        //NoVideo              = 1 << 2,
        Hidden                 = 1 << 3,
        HardRock               = 1 << 4,
        SuddenDeath            = 1 << 5,
        DoubleTime             = 1 << 6,
        Relax                  = 1 << 7,
        HalfTime               = 1 << 8,
        Nightcore              = 1 << 9,
        Flashlight             = 1 << 10,
        Autoplay               = 1 << 11,
        SpunOut                = 1 << 12,
        Relax2                 = 1 << 13,
        Perfect                = 1 << 14,
        Key4                   = 1 << 15,
        Key5                   = 1 << 16,
        Key6                   = 1 << 17,
        Key7                   = 1 << 18,
        Key8                   = 1 << 19,
        FadeIn                 = 1 << 20,
        Random                 = 1 << 21,
        Cinema                 = 1 << 22,
        Target                 = 1 << 23,
        Key9                   = 1 << 24,
        KeyCoop                = 1 << 25,
        Key1                   = 1 << 26,
        Key3                   = 1 << 27,
        Key2                   = 1 << 28,
        LastMod                = 1 << 29,
        KeyMod = Key1 | Key2 | Key3 | Key4 | Key5 | Key6 | Key7 | Key8 | Key9 | KeyCoop,
        FreeModAllowed = NoFail | Easy | Hidden | HardRock | SuddenDeath | Flashlight | FadeIn | Relax | Relax2 | SpunOut | KeyMod,
        ScoreIncreaseMods = Hidden | HardRock | DoubleTime | Flashlight | FadeIn
    }

    public enum SlotTeams
    {
        Neutral,
        Blue,
        Red
    }

    public enum PlayModes
    {
        Osu                    = 0,
        Taiko                  = 1,
        CatchTheBeat           = 2,
        OsuMania               = 3
    }

    public enum LinkId
    {
        Set,
        Beatmap,
        Topic,
        Post,
        Checksum
    }

    [Flags]
    public enum Permissions
    {
        None                   = 0,
        Normal                 = 1,
        BAT                    = 2,
        Supporter             = 4,
        Friend                 = 8,
        Peppy                  = 16
    }

    public enum MatchTypes
    {
        Standard               = 0,
        Powerplay              = 1
    }

    public enum MatchScoringTypes
    {
        Score                  = 0,
        Accuracy               = 1,
        Combo                  = 2
    }

    public enum MatchTeamTypes
    {
        HeadToHead             = 0,
        //TagCoop              = 1
        TagCoop                = 1,
        TeamVs                 = 2,
        TagTeamVs              = 3
    }

    public enum SubmissionStatus
    {
        Unknown,
        NotSubmitted,
        Pending,
        EditableCutoff,
        Ranked,
        Approved
    }

    public enum Rankings
    {
        XH,
        SH,
        X,
        S,
        A,
        B,
        C,
        D,
        F,
        N
    } ;

    public enum bStatus
    {
        Idle,
        Afk,
        Playing,
        Editing,
        Modding,
        Multiplayer,
        Watching,
        Unknown,
        Testing,
        Submitting,
        Paused,
        Lobby,
        Multiplaying,
        OsuDirect
    }

    public enum bReceiveUpdates
    {
        None                   = 0,
        All                    = 1,
        Friends                = 2
    }

    public enum ScaleMode
    {
        Letterbox              = 0,
        WidescreenConservative = 1,
        WidescreenAlways       = 2
    }

    public enum ScreenMode
    {
        Windowed               = 0,
        Fullscreen             = 1,
        BorderlessWindow       = 2
    }
}