﻿using System;
using System.Collections.Generic;
using System.Text;
using osu_common.Bancho.Requests;

namespace osu_common.Bancho
{
    public class SlimClient
    {
        public SlimClient()
        {
        }

        public SlimClient(string username)
        {
            Username = username;
        }

        public RequestTarget RequestTargetType;

        public string IrcFullName { get { return UsernameUnderscored + "!cho@ppy.sh"; } }

        public string Address;

        public bool IsAdmin;

        public int UserId;

        protected string username;

        public string UsernameUnderscored;

        public virtual string Username
        {
            get { return username; }
            set
            {
                username = value;
                UsernameUnderscored = Username.Replace(' ', '_');
            }
        }

        public string IrcPrefix
        {
            get
            {
                if (IsAdmin)
                    return "@";
                if (RequestTargetType == RequestTarget.Irc)
                    return "+";
                return string.Empty;
            }
        }

        public override string ToString()
        {
            return Username;
        }
    }
}
