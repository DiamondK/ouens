﻿using System;
using System.IO;
using osu_common.Helpers;

namespace osu_common.Bancho.Objects
{
    public enum AvatarExtension
    {
        None,
        Png,
        Jpeg
    }

    public class bUserPresence : bSerializable
    {
        public byte countryCode;
        public readonly Permissions permission;
        public AvatarExtension avatarExtension;
        public int timezone;
        public int userId;
        public string username;
        public float longitude;
        public float latitude;
        public int rank;
        public bool isOsu;
        public PlayModes playMode;

        public bUserPresence(int userId, string username, AvatarExtension avatar, int timezone, int countryCode, Permissions permission, float longitude, float latitude, int rank, bool isOsu, PlayModes playMode)
        {
            this.userId = userId;
            this.username = username;
            this.timezone = timezone;
            this.countryCode = (byte)countryCode;
            this.permission = permission;
            this.avatarExtension = avatar;
            this.longitude = longitude;
            this.latitude = latitude;
            this.rank = rank;
            this.isOsu = isOsu;
            this.playMode = playMode;
        }

        public bUserPresence(SerializationReader sr)
        {
            userId = sr.ReadInt32();
            if (userId < 0)
                userId = -userId;
            else
                isOsu = userId != 0;

            username = sr.ReadString();

            timezone = sr.ReadByte() - 24;
            countryCode = sr.ReadByte();

            byte b = sr.ReadByte();
            permission = (Permissions)(b & ~0xe0);
            playMode = (PlayModes)Math.Max((byte)0, Math.Min((byte)3, ((b & 0xe0) >> 5)));

            longitude = sr.ReadSingle();
            latitude = sr.ReadSingle();
            rank = sr.ReadInt32();
        }

        public void ReadFromStream(SerializationReader sr)
        {
            throw new System.NotImplementedException();
        }

        public void WriteToStream(SerializationWriter sw)
        {
            sw.Write(isOsu ? userId : -userId);
            sw.Write(username);
            sw.Write((byte)(timezone + 24));
            sw.Write((byte)countryCode);
            sw.Write((byte)((byte)permission | ((byte)playMode << 5)));
            sw.Write(longitude);
            sw.Write(latitude);
            sw.Write(rank);
        }
    }
}
