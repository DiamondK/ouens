using System;
using System.IO;
using osu_common.Helpers;

namespace osu_common.Bancho.Objects
{
    public class bStatusUpdate : bSerializable
    {
        public string beatmapChecksum;
        public int beatmapId;
        public Mods currentMods;
        public PlayModes playMode;
        public bStatus status;
        public string statusText;


        public bStatusUpdate(bStatus status, bool beatmapUpdate, string statusText, string songChecksum, int beatmapId, Mods mods,
                             PlayModes playMode)
        {
            this.status = status;
            beatmapChecksum = songChecksum;
            this.statusText = statusText;
            currentMods = mods;
            this.playMode = playMode;
            this.beatmapId = beatmapId;
        }

        public bStatusUpdate(Stream s) : this(new SerializationReader(s))
        {
        }

        public bStatusUpdate(SerializationReader sr)
        {
            status = (bStatus)sr.ReadByte();
            statusText = sr.ReadString();
            beatmapChecksum = sr.ReadString();
            if (OsuCommon.ProtocolVersion > 10)
                currentMods = (Mods)sr.ReadUInt32();
            else
                currentMods = (Mods)sr.ReadInt16();
            playMode = (PlayModes)Math.Max((byte)0, Math.Min((byte)3, sr.ReadByte()));
            beatmapId = sr.ReadInt32();
        }

        #region bSerializable Members

        public void ReadFromStream(SerializationReader sr)
        {
            throw new System.NotImplementedException();
        }

        public void WriteToStream(SerializationWriter sw)
        {
            sw.Write((byte)status);

            sw.Write(statusText);
            sw.Write(beatmapChecksum);
            sw.Write((uint)currentMods);

            sw.Write((byte)playMode);
            sw.Write(beatmapId);
        }

        #endregion

        internal bStatusUpdate Clone()
        {
            return (bStatusUpdate)MemberwiseClone();
        }
    }
}