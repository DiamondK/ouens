using System;
using System.IO;
using osu_common.Bancho;
using osu_common.Helpers;
using System.Collections.Generic;

namespace osu_common.Bancho.Objects
{
    public enum ReplayAction
    {
        Standard,
        NewSong,
        Skip,
        Completion,
        Fail,
        Pause,
        Unpause,
        SongSelect,
        WatchingOther
    }
    public class bReplayFrameBundle : bSerializable
    {
        public List<bReplayFrame> ReplayFrames;
        public bScoreFrame ScoreFrame;
        public ReplayAction Action;
        public int Extra;

        public bReplayFrameBundle(List<bReplayFrame> frames, ReplayAction action, bScoreFrame scoreFrame, int extra)
        {
            ReplayFrames = frames;
            Action = action;
            ScoreFrame = scoreFrame;
            Extra = extra;
        }

        public bReplayFrameBundle(Stream s) : this(new SerializationReader(s))
        {
        }

        public bReplayFrameBundle(SerializationReader sr)
        {
            if (OsuCommon.ProtocolVersion >= 18)
                Extra = sr.ReadInt32();
            ReplayFrames = new List<bReplayFrame>();

            int frameCount = sr.ReadUInt16();
            for (int i = 0; i < frameCount; i++)
                ReplayFrames.Add(new bReplayFrame(sr));

            Action = (ReplayAction)sr.ReadByte();

            try
            {
                ScoreFrame = new bScoreFrame(sr);
            }
            catch (Exception)
            { }

            
        }

        #region bSerializable Members

        public void ReadFromStream(SerializationReader sr)
        {
            throw new System.NotImplementedException();
        }

        public void WriteToStream(SerializationWriter sw)
        {
            if (OsuCommon.ProtocolVersion >= 18)
                sw.Write(Extra);
            sw.Write((ushort)ReplayFrames.Count);
            foreach(bReplayFrame f in ReplayFrames)
                f.WriteToStream(sw);

            sw.Write((byte)Action);
            
            ScoreFrame.WriteToStream(sw);
            
        }

        #endregion
    }
}