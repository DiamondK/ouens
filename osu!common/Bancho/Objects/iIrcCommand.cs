﻿using System;
using System.Collections.Generic;
using System.Text;

namespace osu_common.Bancho.Objects
{
    public class iIrcCommand : iSerializable
    {
        public string Command;

        public iIrcCommand(string command)
        {
            Command = command;
        }
        #region iSerializable Members

        public void WriteToStreamIrc(Helpers.SerializationWriter sw)
        {
            sw.WriteUTF8(Command);
        }

        #endregion
    }
}
