﻿using System;
using System.Collections.Generic;
using System.Text;

namespace osu_common.Bancho.Objects
{
    public class bChannelJoin : iSerializable
    {
        public SlimClient Client;
        public string ChannelName;

        public bChannelJoin(SlimClient client, string channelName)
        {
            Client = client;
            ChannelName = channelName;
        }
        #region iSerializable Members

        public void WriteToStreamIrc(Helpers.SerializationWriter sw)
        {
            string command = ":" + Client.IrcFullName + " JOIN :" + ChannelName + "\n";

            string prefix = Client.IrcPrefix;
            if (prefix.Length > 0)
            {
                switch (prefix)
                {
                    case "+":
                        command += ":BanchoBot!cho@cho.ppy.sh MODE " + ChannelName + " +v " + Client.UsernameUnderscored + "\n";
                        break;
                    case "@":
                        command += ":BanchoBot!cho@cho.ppy.sh MODE " + ChannelName + " +o " + Client.UsernameUnderscored + "\n";
                        break;
                }
            }

            sw.WriteUTF8(command);
        }

        #endregion
    }
}

