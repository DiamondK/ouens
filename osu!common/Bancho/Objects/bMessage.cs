using System;
using System.IO;
using osu_common.Bancho;
using osu_common.Helpers;

namespace osu_common.Bancho.Objects
{
    public class bMessage : bSerializable, iSerializable
    {
        public object sendingClient;
        public string message;
        public string target;
        public int senderId;
        public bool isprivate { get { return target.Length == 0 || target[0] != '#'; } }

        public bMessage(object sender, string target, string message)
        {
            sendingClient = sender ?? string.Empty;
            this.message = message;
            this.target = target;

            SlimClient sc = sendingClient as SlimClient;
            if (sc != null) senderId = sc.UserId;
        }

        public bMessage(Stream s)
            : this(new SerializationReader(s))
        {
        }

        public bMessage(SerializationReader sr)
        {
            sendingClient = sr.ReadString();
            message = sr.ReadString();
            target = sr.ReadString();
            if (OsuCommon.ProtocolVersion > 14) senderId = sr.ReadInt32();
        }

        #region bSerializable Members

        public void ReadFromStream(SerializationReader sr)
        {
            throw new NotImplementedException();
        }

        public void WriteToStream(SerializationWriter sw)
        {
            sw.Write(sendingClient.ToString());
            sw.Write(message);

            //cloak multiplayer channels before serialising any messages.
            string targetCloaked = target;
            if (target != null)
            {
                if (target.StartsWith("#mp_"))
                    targetCloaked = "#multiplayer";
                else if (target.StartsWith("#spect_"))
                    targetCloaked = "#spectator";
            }

            sw.Write(targetCloaked);
            sw.Write(senderId);
        }

        #endregion

        #region iSerializable Members

        public void WriteToStreamIrc(SerializationWriter sw)
        {
            string sender;
            SlimClient sc = sendingClient as SlimClient;
            if (sc != null)
                sender = sc.IrcFullName;
            else
                sender = sendingClient.ToString();

            sw.WriteUTF8(":" + sender + " PRIVMSG " + target.Replace(' ', '_') + " :" + message + "\n");
        }

        #endregion
    }
}