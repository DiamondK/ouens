﻿using System;
using System.Collections.Generic;
using System.Text;
using osu_common.Helpers;

namespace osu_common.Bancho.Objects
{
    public class bChannel : bSerializable
    {
        public string Name;
        public string Topic;

        internal int userCount;
        public virtual int UserCount { get { return userCount; } set { userCount = value; } }


        public bChannel()
        {
        }

        public bChannel(SerializationReader sr)
        {
            Name = sr.ReadString();
            Topic = sr.ReadString();
            userCount = sr.ReadInt16();
        }

        #region bSerializable Members

        public void ReadFromStream(Helpers.SerializationReader sr)
        {
            throw new System.NotImplementedException();
        }

        public void WriteToStream(Helpers.SerializationWriter sw)
        {
            sw.Write(Name);
            sw.Write(Topic);
            sw.Write(UserCount);
        }

        #endregion
    }
}

