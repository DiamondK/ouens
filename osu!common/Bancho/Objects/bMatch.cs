﻿using System;
using System.IO;
using osu_common.Helpers;

namespace osu_common.Bancho.Objects
{
    [Flags]
    public enum SlotStatus
    {
        Open = 1,
        Locked = 2,
        NotReady = 4,
        Ready = 8,
        NoMap = 16,
        Playing = 32,
        Complete = 64,
        HasPlayer = NotReady | Ready | NoMap | Playing | Complete,
        Quit = 128
    }

    [Flags]
    public enum MultiSpecialModes
    {
        None = 0,
        FreeMod = 1
    }

    [Serializable]
    public class bMatch : bSerializable
    {
        public string gameName;
        public int matchId;
        public MatchTypes matchType;

        public SlotStatus[] slotStatus = new SlotStatus[MAX_PLAYERS];
        public int[] slotId = new int[MAX_PLAYERS];
        public SlotTeams[] slotTeam = new SlotTeams[MAX_PLAYERS];
        public Mods[] slotMods = new Mods[MAX_PLAYERS];

        public string beatmapName;
        public string beatmapChecksum;
        public int beatmapId = -1;
        public bool inProgress;
        public Mods activeMods;
        public int hostId;
        public PlayModes playMode;
        public MatchScoringTypes matchScoringType;
        public MatchTeamTypes matchTeamType;
        public MultiSpecialModes specialModes;
        protected bool SendPassword;
        public int Seed;

        /// <summary>
        /// gamePassword being null is a special case where an there is no change to the password (server-side keeps its current value).
        /// </summary>
        public string gamePassword;

        public static int MAX_PLAYERS { get { return OsuCommon.ProtocolVersion > 18 ? 16 : 8; } }


        public bool passwordRequired { get { return gamePassword != null; } }

        public bMatch()
        {
        }

        public bMatch(MatchTypes matchType, MatchScoringTypes matchScoringType, MatchTeamTypes matchTeamType, PlayModes playMode, string gameName,
            string gamePassword, int initialSlotCount, string beatmapName, string beatmapChecksum, int beatmapId, Mods activeMods, int hostId,
            MultiSpecialModes specialModes = MultiSpecialModes.None, int seed = 0)
        {
            this.matchType = matchType;
            this.playMode = playMode;
            this.matchScoringType = matchScoringType;
            this.matchTeamType = matchTeamType;
            this.gameName = gameName;
            this.gamePassword = gamePassword;
            this.beatmapName = beatmapName;
            this.beatmapChecksum = beatmapChecksum;
            this.beatmapId = beatmapId;
            this.activeMods = activeMods;
            this.hostId = hostId;
            this.specialModes = specialModes;
            this.Seed = seed;

            SendPassword = true;

            for (int i = 0; i < MAX_PLAYERS; i++)
            {
                slotStatus[i] = i < initialSlotCount ? SlotStatus.Open : SlotStatus.Locked;
                slotId[i] = -1;
            }

            if (gameName.Length > 50)
                gameName = gameName.Remove(50);
        }

        public bMatch(Stream s)
            : this(new SerializationReader(s))
        {
        }

        public bMatch(SerializationReader sr)
        {
            SendPassword = false;

            matchId = sr.ReadUInt16();
            inProgress = sr.ReadBoolean();
            matchType = (MatchTypes)sr.ReadByte();
            activeMods = (Mods)sr.ReadUInt32();
            gameName = sr.ReadString();
            gamePassword = sr.ReadString();
            beatmapName = sr.ReadString();
            beatmapId = sr.ReadInt32();
            beatmapChecksum = sr.ReadString();

            for (int i = 0; i < MAX_PLAYERS; i++)
                slotStatus[i] = (SlotStatus)sr.ReadByte();

            for (int i = 0; i < MAX_PLAYERS; i++)
                slotTeam[i] = (SlotTeams)sr.ReadByte();

            for (int i = 0; i < MAX_PLAYERS; i++)
                slotId[i] = (slotStatus[i] & SlotStatus.HasPlayer) > 0 ? sr.ReadInt32() : -1;

            hostId = sr.ReadInt32();

            playMode = (PlayModes)sr.ReadByte();

            matchScoringType = (MatchScoringTypes)sr.ReadByte();
            matchTeamType = (MatchTeamTypes)sr.ReadByte();

            specialModes = (MultiSpecialModes)sr.ReadByte();

            if (gameName.Length > 50)
                gameName = gameName.Remove(50);

            if ((specialModes & MultiSpecialModes.FreeMod) > 0)
                for (int i = 0; i < MAX_PLAYERS; i++)
                    slotMods[i] = (Mods)sr.ReadInt32();
            Seed = sr.ReadInt32();
        }

        public int slotUsedCount
        {
            get
            {
                int count = 0;
                for (int i = 0; i < MAX_PLAYERS; i++)
                    if ((slotStatus[i] & SlotStatus.HasPlayer) > 0)
                        count++;
                return count;
            }
        }

        public int slotPlayingCount
        {
            get
            {
                int count = 0;
                for (int i = 0; i < MAX_PLAYERS; i++)
                    if ((slotStatus[i] & SlotStatus.Playing) > 0)
                        count++;
                return count;
            }
        }

        public int slotOpenCount
        {
            get
            {
                int count = 0;
                for (int i = 0; i < MAX_PLAYERS; i++)
                    if (slotStatus[i] != SlotStatus.Locked)
                        count++;
                return count;
            }
        }

        public int slotFreeCount
        {
            get
            {
                int count = 0;
                for (int i = 0; i < MAX_PLAYERS; i++)
                    if (slotStatus[i] == SlotStatus.Open)
                        count++;
                return count;
            }
        }

        public int slotReadyCount
        {
            get
            {
                int count = 0;
                for (int i = 0; i < MAX_PLAYERS; i++)
                    if (slotStatus[i] == SlotStatus.Ready)
                        count++;
                return count;
            }
        }

        public bool TeamMode
        {
            get
            {
                return matchTeamType == MatchTeamTypes.TagTeamVs || matchTeamType == MatchTeamTypes.TeamVs;
            }
        }

        public bool HasValidTeams
        {
            get
            {
                if (!TeamMode) return true;

                SlotTeams? firstColour = null;
                for (int i = 0; i < MAX_PLAYERS; i++)
                {
                    if ((slotStatus[i] & SlotStatus.HasPlayer) > 0 && (slotStatus[i] & SlotStatus.NoMap) == 0)
                    {
                        if (firstColour == null)
                            firstColour = slotTeam[i];
                        else if (firstColour != slotTeam[i])
                            return true;
                    }
                }
                return false;
            }
        }

        #region bSerializable Members

        public void ReadFromStream(SerializationReader sr)
        {
            throw new System.NotImplementedException();
        }

        public void WriteToStream(SerializationWriter sw)
        {
            sw.Write((short)matchId);
            sw.Write(inProgress);
            sw.Write((byte)matchType);
            sw.Write((uint)activeMods);
            sw.Write(gameName);
            sw.Write(!SendPassword && gamePassword != null ? string.Empty : gamePassword);
            sw.Write(beatmapName);
            sw.Write(beatmapId);
            sw.Write(beatmapChecksum);
            for (int i = 0; i < MAX_PLAYERS; i++)
                sw.Write((byte)slotStatus[i]);

            for (int i = 0; i < MAX_PLAYERS; i++)
                sw.Write((byte)slotTeam[i]);

            for (int i = 0; i < MAX_PLAYERS; i++)
                if ((slotStatus[i] & SlotStatus.HasPlayer) > 0)
                    sw.Write(slotId[i]);
            sw.Write(hostId);

            sw.Write((byte)playMode);

            sw.Write((byte)matchScoringType);
            sw.Write((byte)matchTeamType);

            sw.Write((byte)specialModes);

            if ((specialModes & MultiSpecialModes.FreeMod) > 0)
                for (int i = 0; i < MAX_PLAYERS; i++)
                    sw.Write((int)slotMods[i]);
            sw.Write(Seed);
        }

        public int findPlayerFromId(int userId)
        {
            int pos = 0;
            while (pos < MAX_PLAYERS && slotId[pos] != userId)
                pos++;
            if (pos > MAX_PLAYERS - 1)
                return -1;
            return pos;
        }



        #endregion

        public bool slotUsedAboveEight
        {
            get
            {
                for (int i = 8; i < MAX_PLAYERS; i++)
                    if (slotStatus[i] != SlotStatus.Locked)
                        return true;
                return false;
            }
        }
    }
}