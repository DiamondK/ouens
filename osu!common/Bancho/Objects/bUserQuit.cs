﻿using System;
using System.Collections.Generic;
using System.Text;
using osu_common.Bancho.Requests;
using osu_common.Helpers;

namespace osu_common.Bancho.Objects
{
    public class bUserQuit : bSerializable, iSerializable
    {
        public int UserId;

        public string Reason;
        public QuitNewState State;

        SlimClient client;

        public bUserQuit(SlimClient client, string reason, QuitNewState state = QuitNewState.Gone)
        {
            this.client = client;
            UserId = client.UserId;
            Reason = reason;
            State = state;
        }

        public bUserQuit(SerializationReader sr)
        {
            UserId = sr.ReadInt32();
            State = (QuitNewState)sr.ReadByte();
        }

        #region iSerializable Members

        public void WriteToStreamIrc(Helpers.SerializationWriter sw)
        {
            if (State != QuitNewState.Gone)
                return;

            string command = command = ":" + client.IrcFullName + " QUIT :" + Reason + "\n";
            sw.WriteUTF8(command);
        }

        #endregion

        #region bSerializable Members

        public void ReadFromStream(SerializationReader sr)
        {
            throw new NotImplementedException();
        }

        public void WriteToStream(SerializationWriter sw)
        {
            sw.Write(UserId);
            sw.Write((byte)State);
        }

        #endregion
    }

    public enum QuitNewState
    {
        Gone,
        OsuRemaining,
        IrcRemaining
    }
}


