using System.IO;
using osu_common.Helpers;

namespace osu_common.Bancho.Objects
{
    public enum Completeness
    {
        StatusOnly,
        Statistics,
    }

    public class bUserStats : bSerializable
    {
        public float accuracy;
        public int level;
        public int playcount;
        public int rank;
        public long rankedScore;
        public bStatusUpdate status;
        public long totalScore;
        public int userId;
        public short performance;

        public bUserStats()
        {
        }

        public bUserStats(SerializationReader sr)
        {
            userId = sr.ReadInt32();

            status = new bStatusUpdate(sr);

            rankedScore = sr.ReadInt64();
            accuracy = sr.ReadSingle();
            playcount = sr.ReadInt32();
            totalScore = sr.ReadInt64();
            rank = sr.ReadInt32();
            performance = sr.ReadInt16();
        }

        public void ReadFromStream(SerializationReader sr)
        {
            throw new System.NotImplementedException();
        }

        public void WriteToStream(SerializationWriter sw)
        {
            sw.Write(userId);
            status.WriteToStream(sw);
            sw.Write(rankedScore);
            sw.Write(accuracy);
            sw.Write(playcount);
            sw.Write(totalScore);
            sw.Write(rank);
            sw.Write(performance);
        }

        public bUserStats Clone(bool hardcore)
        {
            bUserStats cl = (bUserStats)MemberwiseClone();
            if (hardcore)
                cl.status = status.Clone();
            return cl;
        }
    }
}