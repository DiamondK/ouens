﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using osu_common.Helpers;
using System.Runtime.InteropServices;

namespace osu_common.Bancho
{

    public class BufferStack : Stack<bBuffer>
    {
        byte[] block;

        int SingleBufferSize;
        int TotalBuffers;
        private GCHandle handle;

        public BufferStack(int singleBufferSize, int totalBuffers)
            : base(totalBuffers)
        {
            SingleBufferSize = singleBufferSize;
            TotalBuffers = totalBuffers;

            block = new byte[singleBufferSize * totalBuffers];
            handle = GCHandle.Alloc(block, GCHandleType.Pinned);

            for (int i = 0; i < singleBufferSize * totalBuffers - 1; i++)
                block[i] = 0;

            for (int i = 0; i < totalBuffers; i++)
                Push(new bBuffer(block, i * singleBufferSize, singleBufferSize));
        }

        public new bBuffer Pop()
        {
            lock (this)
            {
                bBuffer b = base.Pop();
                b.Reset();
                return b;
            }
        }

        public new void Push(bBuffer buffer)
        {
            lock (this)
                base.Push(buffer);
        }

    }

    public class bBuffer
    {
        public byte[] bBlock;
        public int bOffset;
        public int bSize;

        SerializationReader reader;
        public SerializationReader Reader
        {
            get
            {
                if (reader == null)
                    reader = new SerializationReader(Stream);
                return reader;
            }
        }

        public MemoryStream Stream;

        public bBuffer(byte[] block, int offset, int size)
        {
            this.bBlock = block;
            this.bOffset = offset;
            this.bSize = size;

            Stream = new MemoryStream(block, offset, size, true);
        }

        /// <summary>
        /// The offset of the current buffer in the byte[] block *plus* how much has been read from the attached stream.
        /// </summary>
        public int StreamOffset { get { return bOffset + (int)Stream.Position; } }

        internal void Reset()
        {
            Stream.Position = 0;
        }
    }
}
