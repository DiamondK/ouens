using System.Collections.Generic;
using System.IO;

namespace osu_common.Libraries
{
    public class ConfigManagerCompact
    {
        public static Dictionary<string, string> Configuration;

        public static void LoadConfig()
        {
            if (Configuration != null) return;

            Configuration = new Dictionary<string, string>();

            if (!File.Exists(@"osu!.cfg"))
                return;

            StreamReader r = File.OpenText(@"osu!.cfg");
            while (!r.EndOfStream)
            {
                string[] line = r.ReadLine().Split('=');

                if (line.Length < 2)
                    continue;
                Configuration[line[0].Trim()] = line[1].Trim();
            }

            r.Close();
        }

        public static void SaveConfig()
        {
            LoadConfig();

            StreamWriter w = new StreamWriter(@"osu!.cfg", false);
            foreach (KeyValuePair<string, string> p in Configuration)
            {
                w.WriteLine(@"{0} = {1}", p.Key, p.Value);
            }
            w.Close();
        }

        internal static void ResetHashes()
        {
            //cached checksum is farked; let's clear and start again.
            List<string> resetKeys = new List<string>();
            foreach (string s in ConfigManagerCompact.Configuration.Keys)
                if (s.StartsWith(@"h_")) resetKeys.Add(s);
            foreach (string s in resetKeys)
                ConfigManagerCompact.Configuration.Remove(s);
            ConfigManagerCompact.SaveConfig();
        }
    }
}