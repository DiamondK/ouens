﻿using System;
using System.Collections.Generic;
using System.Text;
using osu_common.Bancho;
using osu_common.Helpers;

namespace osu_common.Libraries.Osz2
{
    public struct FileInfo : bSerializable
    {
        public string Filename { get; private set; }
        public int Length { get; private set; }
        public int Offset { get; private set; }
        public byte[/*16*/] Hash { get; private set; }
        public DateTime CreationTime { get; private set; }
        public DateTime ModifiedTime { get; private set; }


        public FileInfo(string filename, int offset, int length, byte[] hash, DateTime creationTime, DateTime modifiedTime)
            : this()
        {
            Filename = filename;
            Offset = offset;
            Length = length;
            Hash = hash;
            CreationTime = creationTime;
            ModifiedTime = modifiedTime;


        }

        public void ReadFromStream(SerializationReader sr)
        {
            Filename = sr.ReadString();
            Length = sr.ReadInt32();
            Offset = sr.ReadInt32();
            Hash = sr.ReadByteArray();
            CreationTime = (DateTime)sr.ReadObject();
            ModifiedTime = (DateTime)sr.ReadObject();


        }

        public void WriteToStream(SerializationWriter sw)
        {
            sw.Write(Filename);
            sw.Write(Length);
            sw.Write(Offset);
            sw.WriteByteArray(Hash);
            sw.WriteObject(CreationTime);
            sw.WriteObject(ModifiedTime);
        }
    }

    public enum MapMetaType
    {
        Title,
        Artist,
        Creator,
        Version,
        Source,
        Tags,
        VideoDataOffset,
        VideoDataLength,
        VideoHash,
        BeatmapSetID,
        Genre,
        Language,
        TitleUnicode,
        ArtistUnicode,
        Unknown = 9999,
        Difficulty,
        PreviewTime,
        ArtistFullName,
        ArtistTwitter,
        SourceUnicode,
        ArtistUrl,
        Revision,
        PackId
    }
}
