﻿namespace Microsoft.Xna.Framework
{
    using Microsoft.Xna.Framework.Graphics;
    using System;

    public class GraphicsDeviceInformation
    {
        private GraphicsAdapter adapter = GraphicsAdapter.DefaultAdapter;
        private CreateOptions createOptions = CreateOptions.HardwareVertexProcessing;
        private Microsoft.Xna.Framework.Graphics.DeviceType deviceType = Microsoft.Xna.Framework.Graphics.DeviceType.Hardware;
        private Microsoft.Xna.Framework.Graphics.PresentationParameters presentationParameters = new Microsoft.Xna.Framework.Graphics.PresentationParameters();

        public GraphicsDeviceInformation Clone()
        {
            GraphicsDeviceInformation information = new GraphicsDeviceInformation();
            information.presentationParameters = this.presentationParameters.Clone();
            information.createOptions = this.createOptions;
            information.adapter = this.adapter;
            information.deviceType = this.deviceType;
            return information;
        }

        public override bool Equals(object obj)
        {
            GraphicsDeviceInformation information = obj as GraphicsDeviceInformation;
            if (information == null)
            {
                return false;
            }
            if (!information.adapter.Equals(this.adapter))
            {
                return false;
            }
            if (!information.createOptions.Equals(this.createOptions))
            {
                return false;
            }
            if (!information.deviceType.Equals(this.deviceType))
            {
                return false;
            }
            if (information.PresentationParameters.AutoDepthStencilFormat != this.PresentationParameters.AutoDepthStencilFormat)
            {
                return false;
            }
            if (information.PresentationParameters.BackBufferCount != this.PresentationParameters.BackBufferCount)
            {
                return false;
            }
            if (information.PresentationParameters.BackBufferFormat != this.PresentationParameters.BackBufferFormat)
            {
                return false;
            }
            if (information.PresentationParameters.BackBufferHeight != this.PresentationParameters.BackBufferHeight)
            {
                return false;
            }
            if (information.PresentationParameters.BackBufferWidth != this.PresentationParameters.BackBufferWidth)
            {
                return false;
            }
            if (information.PresentationParameters.DeviceWindowHandle != this.PresentationParameters.DeviceWindowHandle)
            {
                return false;
            }
            if (information.PresentationParameters.EnableAutoDepthStencil != this.PresentationParameters.EnableAutoDepthStencil)
            {
                return false;
            }
            if (information.PresentationParameters.FullScreenRefreshRateInHz != this.PresentationParameters.FullScreenRefreshRateInHz)
            {
                return false;
            }
            if (information.PresentationParameters.IsFullScreen != this.PresentationParameters.IsFullScreen)
            {
                return false;
            }
            if (information.PresentationParameters.MultiSampleQuality != this.PresentationParameters.MultiSampleQuality)
            {
                return false;
            }
            if (information.PresentationParameters.MultiSampleType != this.PresentationParameters.MultiSampleType)
            {
                return false;
            }
            if (information.PresentationParameters.PresentationInterval != this.PresentationParameters.PresentationInterval)
            {
                return false;
            }
            if (information.PresentationParameters.PresentOptions != this.PresentationParameters.PresentOptions)
            {
                return false;
            }
            if (information.PresentationParameters.SwapEffect != this.PresentationParameters.SwapEffect)
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            return (((this.deviceType.GetHashCode() ^ this.createOptions.GetHashCode()) ^ this.adapter.GetHashCode()) ^ this.presentationParameters.GetHashCode());
        }

        public GraphicsAdapter Adapter
        {
            get
            {
                return this.adapter;
            }
            set
            {
                if (this.adapter == null)
                {
                    throw new ArgumentNullException(Microsoft.Xna.Framework.Resources.NoNullUseDefaultAdapter);
                }
                this.adapter = value;
            }
        }

        public CreateOptions CreationOptions
        {
            get
            {
                return this.createOptions;
            }
            set
            {
                this.createOptions = value;
            }
        }

        public Microsoft.Xna.Framework.Graphics.DeviceType DeviceType
        {
            get
            {
                return this.deviceType;
            }
            set
            {
                this.deviceType = value;
            }
        }

        public Microsoft.Xna.Framework.Graphics.PresentationParameters PresentationParameters
        {
            get
            {
                return this.presentationParameters;
            }
            set
            {
                this.presentationParameters = value;
            }
        }
    }
}

