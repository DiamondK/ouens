﻿using System;

namespace Microsoft.Xna.Framework
{
    public abstract class GameWindow
    {
        internal static readonly int DefaultClientHeight = 600;
        internal static readonly int DefaultClientWidth = 800;
        private string title = string.Empty;

        internal GameWindow()
        {
        }

        public abstract bool AllowUserResizing { get; set; }

        public abstract Rectangle ClientBounds { get; }

        public abstract IntPtr Handle { get; }

        public abstract bool IsMinimized { get; }

        internal abstract bool IsMouseVisible { get; set; }

        public abstract string ScreenDeviceName { get; }

        public string Title
        {
            get { return title; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(Resources.TitleCannotBeNull);
                }
                if (title != value)
                {
                    title = value;
                    SetTitle(title);
                }
            }
        }

        internal event EventHandler Activated;

        public event EventHandler ClientSizeChanged;

        internal event EventHandler Deactivated;

        internal event EventHandler Paint;

        public event EventHandler ScreenDeviceNameChanged;

        public abstract void BeginScreenDeviceChange(bool willBeFullScreen);

        public void EndScreenDeviceChange(string screenDeviceName)
        {
            EndScreenDeviceChange(screenDeviceName, ClientBounds.Width, ClientBounds.Height);
        }

        public abstract void EndScreenDeviceChange(string screenDeviceName, int clientWidth, int clientHeight);

        protected void OnActivated()
        {
            if (Activated != null)
            {
                Activated(this, EventArgs.Empty);
            }
        }

        protected void OnClientSizeChanged()
        {
            if (ClientSizeChanged != null)
            {
                ClientSizeChanged(this, EventArgs.Empty);
            }
        }

        protected void OnDeactivated()
        {
            if (Deactivated != null)
            {
                Deactivated(this, EventArgs.Empty);
            }
        }

        protected void OnPaint()
        {
            if (Paint != null)
            {
                Paint(this, EventArgs.Empty);
            }
        }

        protected void OnScreenDeviceNameChanged()
        {
            if (ScreenDeviceNameChanged != null)
            {
                ScreenDeviceNameChanged(this, EventArgs.Empty);
            }
        }

        protected abstract void SetTitle(string title);
    }
}