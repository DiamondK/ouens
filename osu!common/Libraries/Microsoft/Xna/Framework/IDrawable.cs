﻿namespace Microsoft.Xna.Framework
{
    using System;
    using System.Runtime.CompilerServices;

    public interface IDrawable
    {
        event EventHandler DrawOrderChanged;

        event EventHandler VisibleChanged;

        void Draw();

        int DrawOrder { get; }

        bool Visible { get; }
    }
}

