﻿namespace Microsoft.Xna.Framework
{
    using System;

    public class NoSuitableGraphicsDeviceException : ApplicationException
    {
        public NoSuitableGraphicsDeviceException(string message) : base(message)
        {
        }

        public NoSuitableGraphicsDeviceException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}

