﻿namespace Microsoft.Xna.Framework
{
    using System;

    public class GameTime
    {
        private TimeSpan elapsedGameTime;
        private TimeSpan elapsedRealTime;
        private bool isRunningSlowly;
        private TimeSpan totalGameTime;
        private TimeSpan totalRealTime;

        public GameTime()
        {
        }

        public GameTime(TimeSpan totalRealTime, TimeSpan elapsedRealTime, TimeSpan totalGameTime, TimeSpan elapsedGameTime) : this(totalRealTime, elapsedRealTime, totalGameTime, elapsedGameTime, false)
        {
        }

        public GameTime(TimeSpan totalRealTime, TimeSpan elapsedRealTime, TimeSpan totalGameTime, TimeSpan elapsedGameTime, bool isRunningSlowly)
        {
            this.totalRealTime = totalRealTime;
            this.elapsedRealTime = elapsedRealTime;
            this.totalGameTime = totalGameTime;
            this.elapsedGameTime = elapsedGameTime;
            this.isRunningSlowly = isRunningSlowly;
        }

        public TimeSpan ElapsedGameTime
        {
            get
            {
                return this.elapsedGameTime;
            }
            internal set
            {
                this.elapsedGameTime = value;
            }
        }

        public TimeSpan ElapsedRealTime
        {
            get
            {
                return this.elapsedRealTime;
            }
            internal set
            {
                this.elapsedRealTime = value;
            }
        }

        public bool IsRunningSlowly
        {
            get
            {
                return this.isRunningSlowly;
            }
            internal set
            {
                this.isRunningSlowly = value;
            }
        }

        public TimeSpan TotalGameTime
        {
            get
            {
                return this.totalGameTime;
            }
            internal set
            {
                this.totalGameTime = value;
            }
        }

        public TimeSpan TotalRealTime
        {
            get
            {
                return this.totalRealTime;
            }
            internal set
            {
                this.totalRealTime = value;
            }
        }
    }
}

