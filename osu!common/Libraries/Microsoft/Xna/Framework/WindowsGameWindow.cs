﻿namespace Microsoft.Xna.Framework
{
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Reflection;
    using System.Resources;
    using System.Runtime.CompilerServices;
    using System.Windows.Forms;

    public class WindowsGameWindow : GameWindow
    {
        private bool inDeviceTransition;
        private bool isMouseVisible;
        public WindowsGameForm WindowsForm = new WindowsGameForm();

        internal event EventHandler Resume;

        internal event EventHandler Suspend;

        public WindowsGameWindow()
        {
            base.Title = GetDefaultTitleName();
            this.WindowsForm.Suspend += new EventHandler(this.mainForm_Suspend);
            this.WindowsForm.Resume += new EventHandler(this.mainForm_Resume);
            this.WindowsForm.ScreenChanged += new EventHandler(this.mainForm_ScreenChanged);
            this.WindowsForm.ApplicationActivated += new EventHandler(this.mainForm_ApplicationActivated);
            this.WindowsForm.ApplicationDeactivated += new EventHandler(this.mainForm_ApplicationDeactivated);
            this.WindowsForm.UserResized += new EventHandler(this.mainForm_UserResized);
            this.WindowsForm.Closing += new CancelEventHandler(this.mainForm_Closing);
            this.WindowsForm.Paint += new PaintEventHandler(this.mainForm_Paint);
            this.WindowsForm.BackColor = System.Drawing.Color.Black;
        }

        public override void BeginScreenDeviceChange(bool willBeFullScreen)
        {
            this.WindowsForm.BeginScreenDeviceChange(willBeFullScreen);
            this.inDeviceTransition = true;
        }

        internal void Close()
        {
            if (this.WindowsForm != null)
            {
                this.WindowsForm.Close();
                this.WindowsForm = null;
            }
        }

        internal static string DeviceNameFromScreen(Screen screen)
        {
            string deviceName = screen.DeviceName;
            int index = screen.DeviceName.IndexOf('\0');
            if (index != -1)
            {
                deviceName = screen.DeviceName.Substring(0, index);
            }
            return deviceName;
        }

        public override void EndScreenDeviceChange(string screenDeviceName, int clientWidth, int clientHeight)
        {
            try
            {
                this.WindowsForm.EndScreenDeviceChange(screenDeviceName, clientWidth, clientHeight);
            }
            finally
            {
                this.inDeviceTransition = false;
            }
        }

        private static Icon FindFirstIcon(Assembly assembly)
        {
            if (assembly != null)
            {
                foreach (string str in assembly.GetManifestResourceNames())
                {
                    try
                    {
                        return new Icon(assembly.GetManifestResourceStream(str));
                    }
                    catch
                    {
                        try
                        {
                            IDictionaryEnumerator enumerator = new ResourceReader(assembly.GetManifestResourceStream(str)).GetEnumerator();
                            while (enumerator.MoveNext())
                            {
                                Icon icon = enumerator.Value as Icon;
                                if (icon != null)
                                {
                                    return icon;
                                }
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }
            return null;
        }

        private static string GetAssemblyTitle(Assembly assembly)
        {
            if (assembly != null)
            {
                AssemblyTitleAttribute[] customAttributes = (AssemblyTitleAttribute[]) assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), true);
                if ((customAttributes != null) && (customAttributes.Length > 0))
                {
                    return customAttributes[0].Title;
                }
            }
            return null;
        }

        private static Icon GetDefaultIcon()
        {
            Icon icon;
            Assembly entryAssembly = Assembly.GetEntryAssembly();
            if (entryAssembly != null)
            {
                try
                {
                    icon = Icon.ExtractAssociatedIcon(new Uri(entryAssembly.EscapedCodeBase).LocalPath);
                    if (icon != null)
                    {
                        return icon;
                    }
                }
                catch (ArgumentException)
                {
                }
                catch (UriFormatException)
                {
                }
            }
            icon = FindFirstIcon(entryAssembly);
            if (icon != null)
            {
                return icon;
            }
            return new Icon(typeof(Game), "Game.ico");
        }

        private static string GetDefaultTitleName()
        {
            string assemblyTitle = GetAssemblyTitle(Assembly.GetEntryAssembly());
            if (!string.IsNullOrEmpty(assemblyTitle))
            {
                return assemblyTitle;
            }
            try
            {
                Uri uri = new Uri(Application.ExecutablePath);
                return Path.GetFileNameWithoutExtension(uri.LocalPath);
            }
            catch (ArgumentNullException)
            {
            }
            catch (UriFormatException)
            {
            }
            return Microsoft.Xna.Framework.Resources.DefaultTitleName;
        }

        private void mainForm_ApplicationActivated(object sender, EventArgs e)
        {
            base.OnActivated();
        }

        private void mainForm_ApplicationDeactivated(object sender, EventArgs e)
        {
            base.OnDeactivated();
        }

        private void mainForm_Closing(object sender, CancelEventArgs e)
        {
            base.OnDeactivated();
        }

        private void mainForm_Paint(object sender, PaintEventArgs e)
        {
            if (!this.inDeviceTransition)
            {
                base.OnPaint();
            }
        }

        private void mainForm_Resume(object sender, EventArgs e)
        {
            this.OnResume();
        }

        private void mainForm_ScreenChanged(object sender, EventArgs e)
        {
            base.OnScreenDeviceNameChanged();
        }

        private void mainForm_Suspend(object sender, EventArgs e)
        {
            this.OnSuspend();
        }

        private void mainForm_UserResized(object sender, EventArgs e)
        {
            base.OnClientSizeChanged();
        }

        protected void OnResume()
        {
            if (this.Resume != null)
            {
                this.Resume(this, EventArgs.Empty);
            }
        }

        protected void OnSuspend()
        {
            if (this.Suspend != null)
            {
                this.Suspend(this, EventArgs.Empty);
            }
        }

        internal static Screen ScreenFromAdapter(GraphicsAdapter adapter)
        {
            foreach (Screen screen in Screen.AllScreens)
            {
                if (DeviceNameFromScreen(screen) == adapter.DeviceName)
                {
                    return screen;
                }
            }
            throw new ArgumentException(Microsoft.Xna.Framework.Resources.InvalidScreenAdapter, "adapter");
        }

        internal static Screen ScreenFromDeviceName(string screenDeviceName)
        {
            if (string.IsNullOrEmpty(screenDeviceName))
            {
                throw new ArgumentException(Microsoft.Xna.Framework.Resources.NullOrEmptyScreenDeviceName);
            }
            foreach (Screen screen in Screen.AllScreens)
            {
                if (DeviceNameFromScreen(screen) == screenDeviceName)
                {
                    return screen;
                }
            }
            throw new ArgumentException(Microsoft.Xna.Framework.Resources.InvalidScreenDeviceName, "screenDeviceName");
        }

        internal static Screen ScreenFromHandle(IntPtr windowHandle)
        {
            Microsoft.Xna.Framework.NativeMethods.RECT rect;
            int num = 0;
            Screen screen = null;
            Microsoft.Xna.Framework.NativeMethods.GetWindowRect(windowHandle, out rect);
            System.Drawing.Rectangle rectangle = new System.Drawing.Rectangle(rect.Left, rect.Top, rect.Right - rect.Left, rect.Bottom - rect.Top);
            foreach (Screen screen2 in Screen.AllScreens)
            {
                System.Drawing.Rectangle rectangle2 = rectangle;
                rectangle2.Intersect(screen2.Bounds);
                int num2 = rectangle2.Width * rectangle2.Height;
                if (num2 > num)
                {
                    num = num2;
                    screen = screen2;
                }
            }
            if (screen == null)
            {
                screen = Screen.AllScreens[0];
            }
            return screen;
        }

        protected override void SetTitle(string title)
        {
            if (this.WindowsForm != null)
            {
                this.WindowsForm.Text = title;
            }
        }

        public override bool AllowUserResizing
        {
            get
            {
                return ((this.WindowsForm != null) && this.WindowsForm.AllowUserResizing);
            }
            set
            {
                if (this.WindowsForm != null)
                {
                    this.WindowsForm.AllowUserResizing = value;
                }
            }
        }

        public override Microsoft.Xna.Framework.Rectangle ClientBounds
        {
            get
            {
                return this.WindowsForm.ClientBounds;
            }
        }

        internal System.Windows.Forms.Form Form
        {
            get
            {
                return this.WindowsForm;
            }
        }

        public override IntPtr Handle
        {
            get
            {
                if (this.WindowsForm != null)
                {
                    return this.WindowsForm.Handle;
                }
                return IntPtr.Zero;
            }
        }

        public override bool IsMinimized
        {
            get
            {
                if (this.WindowsForm == null)
                {
                    return false;
                }
                return this.WindowsForm.IsMinimized;
            }
        }

        internal override bool IsMouseVisible
        {
            get
            {
                return this.isMouseVisible;
            }
            set
            {
                this.isMouseVisible = value;
                if (this.WindowsForm != null)
                {
                    this.WindowsForm.IsMouseVisible = value;
                }
            }
        }

        public override string ScreenDeviceName
        {
            get
            {
                if (this.WindowsForm == null)
                {
                    return string.Empty;
                }
                if (this.WindowsForm.DeviceScreen == null)
                {
                    return string.Empty;
                }
                return DeviceNameFromScreen(this.WindowsForm.DeviceScreen);
            }
        }
    }
}

