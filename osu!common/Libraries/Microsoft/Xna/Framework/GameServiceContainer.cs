﻿namespace Microsoft.Xna.Framework
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    public class GameServiceContainer : IServiceProvider
    {
        private Dictionary<Type, object> services = new Dictionary<Type, object>();

        public void AddService(Type type, object provider)
        {
            if (type == null)
            {
                throw new ArgumentNullException(Microsoft.Xna.Framework.Resources.ServiceTypeCannotBeNull);
            }
            if (provider == null)
            {
                throw new ArgumentNullException(Microsoft.Xna.Framework.Resources.ServiceProviderCannotBeNull);
            }
            if (this.services.ContainsKey(type))
            {
                throw new ArgumentException(Microsoft.Xna.Framework.Resources.ServiceAlreadyPresent, "type");
            }
            if (!type.IsAssignableFrom(provider.GetType()))
            {
                throw new ArgumentException(string.Format(CultureInfo.CurrentUICulture, Microsoft.Xna.Framework.Resources.ServiceMustBeAssignable, new object[] { provider.GetType().FullName, type.GetType().FullName }));
            }
            this.services.Add(type, provider);
        }

        public object GetService(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(Microsoft.Xna.Framework.Resources.ServiceTypeCannotBeNull);
            }
            if (this.services.ContainsKey(type))
            {
                return this.services[type];
            }
            return null;
        }

        public void RemoveService(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(Microsoft.Xna.Framework.Resources.ServiceTypeCannotBeNull);
            }
            this.services.Remove(type);
        }
    }
}

