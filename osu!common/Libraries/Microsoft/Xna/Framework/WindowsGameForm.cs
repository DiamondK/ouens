﻿namespace Microsoft.Xna.Framework
{
    using System;
    using System.Drawing;
    using System.Runtime.CompilerServices;
    using System.Windows.Forms;
    using osu_common.Helpers;

    public class WindowsGameForm : FadeForm
    {
        private bool allowUserResizing;
        private bool centerScreen = true;
        private bool deviceChangeChangedVisible;
        private bool? deviceChangeWillBeFullScreen;
        private bool firstPaint = true;
        private bool freezeOurEvents;
        private bool hidMouse;
        private bool isFullScreenMaximized;
        private bool isMouseVisible = true;
        private Size oldClientSize;
        private bool oldVisible;
        private FormWindowState resizeWindowState;
        private System.Drawing.Rectangle savedBounds;
        private FormBorderStyle savedFormBorderStyle;
        private System.Drawing.Rectangle savedRestoreBounds;
        private FormWindowState savedWindowState;
        private Screen screen;
        private Size startResizeSize = Size.Empty;
        private bool userResizing;

        internal event EventHandler ApplicationActivated;

        internal event EventHandler ApplicationDeactivated;

        internal event EventHandler Resume;

        internal event EventHandler ScreenChanged;

        internal event EventHandler Suspend;

        internal event EventHandler UserResized;

        public WindowsGameForm()
        {
            base.SuspendLayout();
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CausesValidation = false;
            base.ClientSize = new Size(0x124, 0x10a);
            base.ResizeBegin += new EventHandler(this.Form_ResizeBegin);
            base.ClientSizeChanged += new EventHandler(this.Form_ClientSizeChanged);
            base.Resize += new EventHandler(this.Form_Resize);
            base.LocationChanged += new EventHandler(this.Form_LocationChanged);
            base.ResizeEnd += new EventHandler(this.Form_ResizeEnd);
            base.MouseEnter += new EventHandler(this.Form_MouseEnter);
            base.MouseLeave += new EventHandler(this.Form_MouseLeave);
            base.ResumeLayout(false);
            try
            {
                this.freezeOurEvents = true;
                this.resizeWindowState = base.WindowState;
                this.screen = WindowsGameWindow.ScreenFromHandle(base.Handle);
                base.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.Opaque, false);
                base.ClientSize = new Size(GameWindow.DefaultClientWidth, GameWindow.DefaultClientHeight);
                this.UpdateBorderStyle();
            }
            finally
            {
                this.freezeOurEvents = false;
            }
        }

        internal void BeginScreenDeviceChange(bool willBeFullScreen)
        {
            this.oldClientSize = base.ClientSize;
            if (willBeFullScreen && !this.isFullScreenMaximized)
            {
                this.savedFormBorderStyle = base.FormBorderStyle;
                this.savedWindowState = base.WindowState;
                this.savedBounds = base.Bounds;
                if (base.WindowState == FormWindowState.Maximized)
                {
                    this.savedRestoreBounds = base.RestoreBounds;
                }
            }
            if (willBeFullScreen != this.isFullScreenMaximized)
            {
                this.deviceChangeChangedVisible = true;
                this.oldVisible = base.Visible;
                base.Visible = false;
            }
            else
            {
                this.deviceChangeChangedVisible = false;
            }
            if (!willBeFullScreen && this.isFullScreenMaximized)
            {
                base.TopMost = false;
                base.FormBorderStyle = this.savedFormBorderStyle;
                if (this.savedWindowState == FormWindowState.Maximized)
                {
                    this.SetBoundsCore(this.screen.Bounds.X, this.screen.Bounds.Y, this.savedRestoreBounds.Width, this.savedRestoreBounds.Height, BoundsSpecified.Size);
                }
                else
                {
                    this.SetBoundsCore(this.screen.Bounds.X, this.screen.Bounds.Y, this.savedBounds.Width, this.savedBounds.Height, BoundsSpecified.Size);
                }
            }
            if (willBeFullScreen != this.isFullScreenMaximized)
            {
                base.SendToBack();
            }
            this.deviceChangeWillBeFullScreen = new bool?(willBeFullScreen);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        internal void EndScreenDeviceChange(string screenDeviceName, int clientWidth, int clientHeight)
        {
            if (!this.deviceChangeWillBeFullScreen.HasValue)
            {
                throw new InvalidOperationException(Microsoft.Xna.Framework.Resources.MustCallBeginDeviceChange);
            }
            bool flag = false;
            if (this.deviceChangeWillBeFullScreen.Value)
            {
                Screen screen = WindowsGameWindow.ScreenFromDeviceName(screenDeviceName);
                System.Drawing.Rectangle bounds = Screen.GetBounds(new System.Drawing.Point(screen.Bounds.X, screen.Bounds.Y));
                if (!this.isFullScreenMaximized)
                {
                    flag = true;
                    base.TopMost = true;
                    base.FormBorderStyle = FormBorderStyle.None;
                    base.WindowState = FormWindowState.Normal;
                    base.BringToFront();
                }
                base.Location = new System.Drawing.Point(bounds.X, bounds.Y);
                base.ClientSize = new Size(bounds.Width, bounds.Height);
                this.isFullScreenMaximized = true;
            }
            else
            {
                if (this.isFullScreenMaximized)
                {
                    flag = true;
                    base.BringToFront();
                }
                this.ResizeWindow(screenDeviceName, clientWidth, clientHeight, this.centerScreen);
            }
            if (this.deviceChangeChangedVisible)
            {
                base.Visible = this.oldVisible;
            }
            if (flag && (this.oldClientSize != base.ClientSize))
            {
                this.OnUserResized(true);
            }
            this.deviceChangeWillBeFullScreen = null;
        }

        private void Form_ClientSizeChanged(object sender, EventArgs e)
        {
            this.UpdateScreen();
        }

        private void Form_LocationChanged(object sender, EventArgs e)
        {
            if (this.userResizing)
            {
                this.centerScreen = false;
            }
            this.UpdateScreen();
        }

        private void Form_MouseEnter(object sender, EventArgs e)
        {
            if (!this.isMouseVisible && !this.hidMouse)
            {
                //Cursor.Hide();
                this.hidMouse = true;
            }
        }

        private void Form_MouseLeave(object sender, EventArgs e)
        {
            if (this.hidMouse)
            {
                //Cursor.Show();
                this.hidMouse = false;
            }
        }

        private void Form_Resize(object sender, EventArgs e)
        {
            if (this.resizeWindowState != base.WindowState)
            {
                this.resizeWindowState = base.WindowState;
                this.firstPaint = true;
                this.OnUserResized(false);
                base.Invalidate();
            }
            if (this.userResizing && (base.ClientSize != this.startResizeSize))
            {
                base.Invalidate();
            }
        }

        private void Form_ResizeBegin(object sender, EventArgs e)
        {
            this.startResizeSize = base.ClientSize;
            this.userResizing = true;
            this.OnSuspend();
        }

        private void Form_ResizeEnd(object sender, EventArgs e)
        {
            this.userResizing = false;
            if (base.ClientSize != this.startResizeSize)
            {
                this.centerScreen = false;
                this.OnUserResized(false);
            }
            this.firstPaint = true;
            this.OnResume();
        }

        private void OnActivateApp(bool active)
        {
            if (active)
            {
                this.firstPaint = true;
                this.freezeOurEvents = false;
                if (this.isFullScreenMaximized)
                {
                    base.TopMost = true;
                }
                if (this.ApplicationActivated != null)
                {
                    this.ApplicationActivated(this, EventArgs.Empty);
                }
            }
            else
            {
                if (this.ApplicationDeactivated != null)
                {
                    this.ApplicationDeactivated(this, EventArgs.Empty);
                }
                this.freezeOurEvents = true;
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (this.firstPaint)
            {
                base.OnPaintBackground(e);
                this.firstPaint = false;
            }
        }

        private void OnResume()
        {
            if (this.Resume != null)
            {
                this.Resume(this, EventArgs.Empty);
            }
        }

        private void OnScreenChanged()
        {
            if (this.ScreenChanged != null)
            {
                this.ScreenChanged(this, EventArgs.Empty);
            }
        }

        private void OnSuspend()
        {
            if (this.Suspend != null)
            {
                this.Suspend(this, EventArgs.Empty);
            }
        }

        private void OnUserResized(bool forceEvent)
        {
            if ((!this.freezeOurEvents || forceEvent) && (this.UserResized != null))
            {
                this.UserResized(this, EventArgs.Empty);
            }
        }

        private void ResizeWindow(string screenDeviceName, int clientWidth, int clientHeight, bool center)
        {
            int x;
            int y;
            Screen screen = WindowsGameWindow.ScreenFromDeviceName(screenDeviceName);
            System.Drawing.Rectangle bounds = Screen.GetBounds(new System.Drawing.Point(screen.Bounds.X, screen.Bounds.Y));
            if (screenDeviceName != WindowsGameWindow.DeviceNameFromScreen(this.DeviceScreen))
            {
                x = bounds.X;
                y = bounds.Y;
            }
            else
            {
                x = this.screen.Bounds.X;
                y = this.screen.Bounds.Y;
            }
            if (this.isFullScreenMaximized)
            {
                Size size = this.SizeFromClientSize(new Size(clientWidth, clientHeight));
                if (this.savedWindowState == FormWindowState.Maximized)
                {
                    int num3 = (this.savedRestoreBounds.X - this.screen.Bounds.X) + x;
                    int num4 = (this.savedRestoreBounds.Y - this.screen.Bounds.Y) + y;
                    this.SetBoundsCore(num3, num4, this.savedRestoreBounds.Width, this.savedRestoreBounds.Height, BoundsSpecified.All);
                }
                else if (center)
                {
                    int num5 = (x + (bounds.Width / 2)) - (size.Width / 2);
                    int num6 = (y + (bounds.Height / 2)) - (size.Height / 2);
                    this.SetBoundsCore(num5, num6, size.Width, size.Height, BoundsSpecified.All);
                }
                else
                {
                    int num7 = (this.savedBounds.X - this.screen.Bounds.X) + x;
                    int num8 = (this.savedBounds.Y - this.screen.Bounds.Y) + y;
                    this.SetBoundsCore(num7, num8, size.Width, size.Height, BoundsSpecified.All);
                }
                base.WindowState = this.savedWindowState;
                this.isFullScreenMaximized = false;
            }
            else if (base.WindowState == FormWindowState.Normal)
            {
                int num9;
                int num10;
                if (center)
                {
                    Size size2 = this.SizeFromClientSize(new Size(clientWidth, clientHeight));
                    num9 = (x + (bounds.Width / 2)) - (size2.Width / 2);
                    num10 = (y + (bounds.Height / 2)) - (size2.Height / 2);
                }
                else
                {
                    num9 = (x + base.Bounds.X) - this.screen.Bounds.X;
                    num10 = (y + base.Bounds.Y) - this.screen.Bounds.Y;
                }
                if ((num9 != base.Location.X) || (num10 != base.Location.Y))
                {
                    base.Location = new System.Drawing.Point(num9, num10);
                }
                if ((base.ClientSize.Width != clientWidth) || (base.ClientSize.Height != clientHeight))
                {
                    base.ClientSize = new Size(clientWidth, clientHeight);
                }
            }
        }

        private void UpdateBorderStyle()
        {
            if (!this.allowUserResizing)
            {
                if (!this.isFullScreenMaximized)
                {
                    base.FormBorderStyle = FormBorderStyle.FixedSingle;
                }
            }
            else
            {
                if (!this.isFullScreenMaximized)
                {
                    base.FormBorderStyle = FormBorderStyle.Sizable;
                }
            }
        }

        private void UpdateScreen()
        {
            if (!this.freezeOurEvents)
            {
                Screen screen = Screen.FromHandle(base.Handle);
                if ((this.screen == null) || !this.screen.Equals(screen))
                {
                    this.screen = screen;
                    if (this.screen != null)
                    {
                        this.OnScreenChanged();
                    }
                }
            }
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x1c)
            {
                bool active = m.WParam != IntPtr.Zero;
                this.OnActivateApp(active);
            }

            if (OnWndProc != null) OnWndProc(ref m);

            if (m.Result.ToInt32() < 0)
            {
                m.Result = new IntPtr(-m.Result.ToInt32() - 1);
                return;
            }

            base.WndProc(ref m);
        }

        public event WndProcDelegate OnWndProc;

        internal bool AllowUserResizing
        {
            get
            {
                return this.allowUserResizing;
            }
            set
            {
                if (this.allowUserResizing != value)
                {
                    this.allowUserResizing = value;
                    this.UpdateBorderStyle();
                }
            }
        }

        internal Microsoft.Xna.Framework.Rectangle ClientBounds
        {
            get
            {
                System.Drawing.Point point = base.PointToScreen(System.Drawing.Point.Empty);
                return new Microsoft.Xna.Framework.Rectangle(point.X, point.Y, base.ClientSize.Width, base.ClientSize.Height);
            }
        }

        internal Screen DeviceScreen
        {
            get
            {
                return this.screen;
            }
        }

        public bool IsMinimized
        {
            get
            {
                if (base.ClientSize.Width != 0)
                {
                    return (base.ClientSize.Height == 0);
                }
                return true;
            }
        }

        internal bool IsMouseVisible
        {
            get
            {
                return this.isMouseVisible;
            }
            set
            {
                if (this.isMouseVisible != value)
                {
                    this.isMouseVisible = value;
                    if (this.isMouseVisible)
                    {
                        if (this.hidMouse)
                        {
                            //Cursor.Show();
                            this.hidMouse = false;
                        }
                    }
                    else if (!this.hidMouse)
                    {
                        //Cursor.Hide();
                        this.hidMouse = true;
                    }
                }
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.MaximizeBox = false;
            this.ResumeLayout(false);

        }
    }

    public delegate void WndProcDelegate(ref Message m);
}

