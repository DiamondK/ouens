﻿namespace Microsoft.Xna.Framework
{
    using System;

    public class PreparingDeviceSettingsEventArgs : EventArgs
    {
        private Microsoft.Xna.Framework.GraphicsDeviceInformation graphicsDeviceInformation;

        public PreparingDeviceSettingsEventArgs(Microsoft.Xna.Framework.GraphicsDeviceInformation graphicsDeviceInformation)
        {
            this.graphicsDeviceInformation = graphicsDeviceInformation;
        }

        public Microsoft.Xna.Framework.GraphicsDeviceInformation GraphicsDeviceInformation
        {
            get
            {
                return this.graphicsDeviceInformation;
            }
        }
    }
}

