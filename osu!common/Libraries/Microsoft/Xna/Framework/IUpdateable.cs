﻿namespace Microsoft.Xna.Framework
{
    using System;
    using System.Runtime.CompilerServices;

    public interface IUpdateable
    {
        event EventHandler EnabledChanged;

        event EventHandler UpdateOrderChanged;

        void Update();

        bool Enabled { get; }

        int UpdateOrder { get; }
    }
}

