﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Input;

namespace Microsoft.Xna.Framework
{
    internal class WindowsGameHost : GameHost
    {
        private readonly WindowsGameWindow gameWindow;
        private bool doneRun;
        private bool exitRequested;
        private Game game;

        public WindowsGameHost(Game game)
        {
            this.game = game;
            gameWindow = new WindowsGameWindow();
            Mouse.WindowHandle = gameWindow.Handle;
            gameWindow.IsMouseVisible = game.IsMouseVisible;
            gameWindow.Activated += GameWindowActivated;
            gameWindow.Deactivated += GameWindowDeactivated;
            gameWindow.Suspend += GameWindowSuspend;
            gameWindow.Resume += GameWindowResume;
        }

        internal override GameWindow Window
        {
            get { return gameWindow; }
        }

        private void ApplicationIdle(object sender, EventArgs e)
        {
            NativeMethods.Message message;
            while (!NativeMethods.PeekMessage(out message, IntPtr.Zero, 0, 0, 0))
            {
                if (exitRequested)
                {
                    gameWindow.Close();
                }
                else
                {
                    base.OnIdle();
                }
            }
        }

        internal override void Exit()
        {
            exitRequested = true;
        }

        private void GameWindowActivated(object sender, EventArgs e)
        {
            base.OnActivated();
        }

        private void GameWindowDeactivated(object sender, EventArgs e)
        {
            base.OnDeactivated();
        }

        private void GameWindowResume(object sender, EventArgs e)
        {
            base.OnResume();
        }

        private void GameWindowSuspend(object sender, EventArgs e)
        {
            base.OnSuspend();
        }

        internal override void Run()
        {
            if (doneRun)
                throw new InvalidOperationException(Resources.NoMulitpleRuns);

            Exception error = null;

            try
            {
                Application.Idle += ApplicationIdle;
                Application.Run(gameWindow.Form);
            }
            catch (OutOfMemoryException e)
            {
                error = e;
            }
            finally
            {
                Application.Idle -= ApplicationIdle;
                doneRun = true;
                
                if (error == null || !(error is OutOfMemoryException))
                    //we don't want to attempt a safe shutdown is memory is low; it may corrupt database files.
                    OnExiting();
            }
        }
    }
}