﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.Xna.Framework.Graphics;

namespace Microsoft.Xna.Framework
{
    public class Game : IDisposable
    {
        public static bool IsActive = true;
        private readonly GameClock clock;
        private readonly List<IDrawable> drawableComponents = new List<IDrawable>();
        private readonly GameComponentCollection gameComponents;
        private readonly GameServiceContainer gameServices = new GameServiceContainer();
        private readonly List<IUpdateable> updateableComponents = new List<IUpdateable>();
        private TimeSpan accumulatedElapsedGameTime;
        private bool doneFirstUpdate;
        private bool drawRunningSlowly;
        private TimeSpan elapsedRealTime;
        private bool exitRequested;
        private IGraphicsDeviceManager graphicsDeviceManager;
        private IGraphicsDeviceService graphicsDeviceService;
        private GameHost host;
        private TimeSpan inactiveSleepTime;
        private bool inRun;
        private bool isFixedTimeStep = true;
        private bool isMouseVisible;
        private TimeSpan lastFrameElapsedGameTime;
        private TimeSpan targetElapsedTime;
        private TimeSpan totalGameTime;
        public bool XnaRendererEnabled;
        public bool SkipDrawcall;

        public Game()
        {
            EnsureHost();
            gameComponents = new GameComponentCollection();
            gameComponents.ComponentAdded += GameComponentAdded;
            gameComponents.ComponentRemoved += GameComponentRemoved;
            clock = new GameClock();
            totalGameTime = TimeSpan.Zero;
            accumulatedElapsedGameTime = TimeSpan.Zero;
            lastFrameElapsedGameTime = TimeSpan.Zero;
            targetElapsedTime = TimeSpan.FromTicks(0x28b0bL);
            inactiveSleepTime = TimeSpan.FromMilliseconds(20.0);
        }

        public GameComponentCollection Components
        {
            get { return gameComponents; }
        }

        public TimeSpan InactiveSleepTime
        {
            get { return inactiveSleepTime; }
            set
            {
                if (value < TimeSpan.Zero)
                {
                    throw new ArgumentOutOfRangeException(Resources.InactiveSleepTimeCannotBeZero, "value");
                }
                inactiveSleepTime = value;
            }
        }

        public bool IsMouseVisible
        {
            get { return isMouseVisible; }
            set
            {
                isMouseVisible = value;
                if (Window != null)
                {
                    Window.IsMouseVisible = value;
                }
            }
        }

        public GameServiceContainer Services
        {
            get { return gameServices; }
        }

        public TimeSpan TargetElapsedTime
        {
            get { return targetElapsedTime; }
            set
            {
                if (value <= TimeSpan.Zero)
                {
                    throw new ArgumentOutOfRangeException(Resources.TargetElaspedCannotBeZero, "value");
                }
                targetElapsedTime = value;
            }
        }

        public GameWindow Window
        {
            get
            {
                if (host != null)
                {
                    return host.Window;
                }
                return null;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        public event EventHandler Activated;

        public event EventHandler Deactivated;

        public event EventHandler Disposed;

        public event EventHandler Exiting;

        protected virtual bool BeginDraw()
        {
            if ((graphicsDeviceManager != null) && !graphicsDeviceManager.BeginDraw())
            {
                return false;
            }
            return true;
        }

        protected virtual void BeginRun()
        {
        }

        private void DeviceCreated(object sender, EventArgs e)
        {
            LoadGraphicsContent(true);
        }

        private void DeviceDisposing(object sender, EventArgs e)
        {
            UnloadGraphicsContent(true);
        }

        private void DeviceReset(object sender, EventArgs e)
        {
            LoadGraphicsContent(false);
        }

        private void DeviceResetting(object sender, EventArgs e)
        {
            UnloadGraphicsContent(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                lock (this)
                {
                    IGameComponent[] array = new IGameComponent[gameComponents.Count];
                    gameComponents.CopyTo(array, 0);
                    for (int i = 0; i < array.Length; i++)
                    {
                        IDisposable disposable = array[i] as IDisposable;
                        if (disposable != null)
                        {
                            disposable.Dispose();
                        }
                    }
                    IDisposable graphicsDeviceManager = this.graphicsDeviceManager as IDisposable;
                    if (graphicsDeviceManager != null)
                    {
                        graphicsDeviceManager.Dispose();
                    }
                    UnhookDeviceEvents();
                    if (Disposed != null)
                    {
                        Disposed(this, EventArgs.Empty);
                    }
                }
            }
        }

        protected virtual void Draw()
        {
            for (int i = 0; i < drawableComponents.Count; i++)
            {
                IDrawable drawable = drawableComponents[i];
                if (drawable.Visible)
                {
                    drawable.Draw();
                }
            }
        }

        private void DrawableDrawOrderChanged(object sender, EventArgs e)
        {
            IDrawable item = sender as IDrawable;
            drawableComponents.Remove(item);
            int num = drawableComponents.BinarySearch(item, DrawOrderComparer.Default);
            if (num < 0)
            {
                drawableComponents.Insert(~num, item);
            }
        }

        private void DrawFrame()
        {
            if (exitRequested || !doneFirstUpdate) return;

            Update();

            if (SkipDrawcall) return;

            BeginDraw();
            Draw();
            EndDraw();
        }

        protected virtual void EndDraw()
        {
            if (graphicsDeviceManager != null)
            {
                graphicsDeviceManager.EndDraw();
            }
        }

        protected virtual void EndRun()
        {
        }

        private void EnsureHost()
        {
            if (host == null)
            {
                host = new WindowsGameHost(this);
                host.Activated += HostActivated;
                host.Deactivated += HostDeactivated;
                host.Suspend += HostSuspend;
                host.Resume += HostResume;
                host.Idle += HostIdle;
                host.Exiting += HostExiting;
            }
        }

        public void Exit()
        {
            exitRequested = true;
            host.Exit();
        }

        ~Game()
        {
            Dispose(false);
        }

        private void GameComponentAdded(object sender, GameComponentCollectionEventArgs e)
        {
            if (inRun)
            {
                e.GameComponent.Initialize();
            }
            IUpdateable gameComponent = e.GameComponent as IUpdateable;
            if (gameComponent != null)
            {
                int num = updateableComponents.BinarySearch(gameComponent, UpdateOrderComparer.Default);
                if (num < 0)
                {
                    updateableComponents.Insert(~num, gameComponent);
                    gameComponent.UpdateOrderChanged += UpdateableUpdateOrderChanged;
                }
            }
            IDrawable item = e.GameComponent as IDrawable;
            if (item != null)
            {
                int num2 = drawableComponents.BinarySearch(item, DrawOrderComparer.Default);
                if (num2 < 0)
                {
                    drawableComponents.Insert(~num2, item);
                    item.DrawOrderChanged += DrawableDrawOrderChanged;
                }
            }
        }

        private void GameComponentRemoved(object sender, GameComponentCollectionEventArgs e)
        {
            IUpdateable gameComponent = e.GameComponent as IUpdateable;
            if (gameComponent != null)
            {
                updateableComponents.Remove(gameComponent);
                gameComponent.UpdateOrderChanged -= UpdateableUpdateOrderChanged;
            }
            IDrawable item = e.GameComponent as IDrawable;
            if (item != null)
            {
                drawableComponents.Remove(item);
                item.DrawOrderChanged -= DrawableDrawOrderChanged;
            }
        }

        private void HookDeviceEvents()
        {
            graphicsDeviceService = Services.GetService(typeof(IGraphicsDeviceService)) as IGraphicsDeviceService;
            if (graphicsDeviceService != null)
            {
                graphicsDeviceService.DeviceCreated += DeviceCreated;
                graphicsDeviceService.DeviceResetting += DeviceResetting;
                graphicsDeviceService.DeviceReset += DeviceReset;
                graphicsDeviceService.DeviceDisposing += DeviceDisposing;
            }
        }

        private void HostActivated(object sender, EventArgs e)
        {
        }

        private void HostDeactivated(object sender, EventArgs e)
        {
        }

        private void HostExiting(object sender, EventArgs e)
        {
            OnExiting(this, EventArgs.Empty);
        }

        private void HostIdle(object sender, EventArgs e)
        {
            Tick();
        }

        private void HostResume(object sender, EventArgs e)
        {
        }

        private void HostSuspend(object sender, EventArgs e)
        {
        }

        protected virtual void Initialize()
        {
            for (int i = 0; i < gameComponents.Count; i++)
            {
                gameComponents[i].Initialize();
            }
            if (XnaRendererEnabled)
            {
                HookDeviceEvents();
                if ((graphicsDeviceService != null) && (graphicsDeviceService.GraphicsDevice != null))
                {
                    LoadGraphicsContent(true);
                }
            }
        }

        protected virtual void LoadGraphicsContent(bool loadAllContent)
        {
        }

        protected virtual void OnActivated(object sender, EventArgs args)
        {
            if (Activated != null)
            {
                Activated(this, args);
            }
        }

        protected virtual void OnDeactivated(object sender, EventArgs args)
        {
            if (Deactivated != null)
            {
                Deactivated(this, args);
            }
        }

        protected virtual void OnExiting(object sender, EventArgs args)
        {
            if (Exiting != null)
            {
                Exiting(null, args);
            }
        }

        public void Run()
        {
            if (XnaRendererEnabled)
            {
                graphicsDeviceManager = Services.GetService(typeof(IGraphicsDeviceManager)) as IGraphicsDeviceManager;
                if (graphicsDeviceManager != null)
                {
                    graphicsDeviceManager.CreateDevice();
                }
            }

            Initialize();
            inRun = true;
            try
            {
                BeginRun();

                Update();

                doneFirstUpdate = true;
                if (host != null)
                {
                    host.Run();
                }
                EndRun();
            }
            finally
            {
                inRun = false;
            }
        }

        public void Tick()
        {
            if (!exitRequested)
            {
                DrawFrame();
            }
        }

        private void UnhookDeviceEvents()
        {
            if (graphicsDeviceService != null)
            {
                graphicsDeviceService.DeviceCreated -= DeviceCreated;
                graphicsDeviceService.DeviceResetting -= DeviceResetting;
                graphicsDeviceService.DeviceReset -= DeviceReset;
                graphicsDeviceService.DeviceDisposing -= DeviceDisposing;
            }
        }

        protected virtual void UnloadGraphicsContent(bool unloadAllContent)
        {
        }

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();



        bool activePreviousFrame = true;
        protected virtual void Update()
        {
            IntPtr fg = GetForegroundWindow();
            IsActive = fg.Equals(Window.Handle);

            if (!activePreviousFrame && IsActive)
                OnActivated(this, EventArgs.Empty);
            else if (activePreviousFrame && !IsActive)
                OnDeactivated(this, EventArgs.Empty);


            for (int i = 0; i < updateableComponents.Count; i++)
            {
                IUpdateable updateable = updateableComponents[i];
                if (updateable.Enabled)
                {
                    updateable.Update();
                }
            }
            activePreviousFrame = IsActive;
        }

        private void UpdateableUpdateOrderChanged(object sender, EventArgs e)
        {
            IUpdateable item = sender as IUpdateable;
            updateableComponents.Remove(item);
            int num = updateableComponents.BinarySearch(item, UpdateOrderComparer.Default);
            if (num < 0)
            {
                updateableComponents.Insert(~num, item);
            }
        }
    }
}