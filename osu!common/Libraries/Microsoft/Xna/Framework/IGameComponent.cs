﻿namespace Microsoft.Xna.Framework
{
    using System;

    public interface IGameComponent
    {
        void Initialize();
    }
}

