﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.Xna.Framework.Graphics;

namespace Microsoft.Xna.Framework
{
    public class GraphicsDeviceManager : IGraphicsDeviceService, IDisposable, IGraphicsDeviceManager
    {
        public static readonly int DefaultBackBufferHeight = 600;
        public static readonly int DefaultBackBufferWidth = 800;
        private static readonly TimeSpan deviceLostSleepTime = TimeSpan.FromMilliseconds(50.0);

        public static readonly SurfaceFormat[] ValidAdapterFormats =
            new SurfaceFormat[]
                {SurfaceFormat.Bgr32, SurfaceFormat.Bgr555, SurfaceFormat.Bgr565, SurfaceFormat.Bgra1010102};

        public static readonly SurfaceFormat[] ValidBackBufferFormats =
            new SurfaceFormat[]
                {
                    SurfaceFormat.Bgr565, SurfaceFormat.Bgr555, SurfaceFormat.Bgra5551, SurfaceFormat.Bgr32,
                    SurfaceFormat.Color, SurfaceFormat.Bgra1010102
                };

        public static readonly DeviceType[] ValidDeviceTypes = new DeviceType[] {DeviceType.Hardware};
        private static MultiSampleType[] multiSampleTypes;
        private bool allowMultiSampling;
        private SurfaceFormat backBufferFormat = SurfaceFormat.Color;
        private int backBufferHeight = DefaultBackBufferHeight;
        private int backBufferWidth = DefaultBackBufferWidth;
        private bool beginDrawOk;
        private DepthFormat depthStencilFormat = DepthFormat.Depth24;
        private GraphicsDevice device;
        private Game game;
        private bool inDeviceTransition;
        private bool isDeviceDirty;
        private bool isFullScreen;
        private bool isReallyFullScreen;
        private ShaderProfile minimumPixelShaderProfile;
        private ShaderProfile minimumVertexShaderProfile = ShaderProfile.VS_1_1;
        private int resizedBackBufferHeight;
        private int resizedBackBufferWidth;
        private bool synchronizeWithVerticalRetrace = true;
        private bool useResizedBackBuffer;

        static GraphicsDeviceManager()
        {
            MultiSampleType[] typeArray2 = new MultiSampleType[0x11];
            typeArray2[0] = MultiSampleType.NonMaskable;
            typeArray2[1] = MultiSampleType.SixteenSamples;
            typeArray2[2] = MultiSampleType.FifteenSamples;
            typeArray2[3] = MultiSampleType.FourteenSamples;
            typeArray2[4] = MultiSampleType.ThirteenSamples;
            typeArray2[5] = MultiSampleType.TwelveSamples;
            typeArray2[6] = MultiSampleType.ElevenSamples;
            typeArray2[7] = MultiSampleType.TenSamples;
            typeArray2[8] = MultiSampleType.NineSamples;
            typeArray2[9] = MultiSampleType.EightSamples;
            typeArray2[10] = MultiSampleType.SevenSamples;
            typeArray2[11] = MultiSampleType.SixSamples;
            typeArray2[12] = MultiSampleType.FiveSamples;
            typeArray2[13] = MultiSampleType.FourSamples;
            typeArray2[14] = MultiSampleType.ThreeSamples;
            typeArray2[15] = MultiSampleType.TwoSamples;
            multiSampleTypes = typeArray2;
        }

        public GraphicsDeviceManager(Game game)
        {
            if (game == null)
            {
                throw new ArgumentNullException(Resources.GameCannotBeNull);
            }
            this.game = game;
            if (game.Services.GetService(typeof (IGraphicsDeviceManager)) != null)
            {
                throw new ArgumentException(Resources.GraphicsDeviceManagerAlreadyPresent);
            }
            game.Services.AddService(typeof (IGraphicsDeviceManager), this);
            game.Services.AddService(typeof (IGraphicsDeviceService), this);
            game.Window.ClientSizeChanged += new EventHandler(GameWindowClientSizeChanged);
            game.Window.ScreenDeviceNameChanged += new EventHandler(GameWindowScreenDeviceNameChanged);
        }

        public bool IsFullScreen
        {
            get { return isFullScreen; }
            set
            {
                isFullScreen = value;
                isDeviceDirty = true;
            }
        }

        public ShaderProfile MinimumPixelShaderProfile
        {
            get { return minimumPixelShaderProfile; }
            set
            {
                if ((value < ShaderProfile.PS_1_1) || (value > ShaderProfile.XPS_3_0))
                {
                    throw new ArgumentOutOfRangeException("value", Resources.InvalidPixelShaderProfile);
                }
                minimumPixelShaderProfile = value;
                isDeviceDirty = true;
            }
        }

        public ShaderProfile MinimumVertexShaderProfile
        {
            get { return minimumVertexShaderProfile; }
            set
            {
                if ((value < ShaderProfile.VS_1_1) || (value > ShaderProfile.XVS_3_0))
                {
                    throw new ArgumentOutOfRangeException("value", Resources.InvalidVertexShaderProfile);
                }
                minimumVertexShaderProfile = value;
                isDeviceDirty = true;
            }
        }

        public bool PreferMultiSampling
        {
            get { return allowMultiSampling; }
            set
            {
                allowMultiSampling = value;
                isDeviceDirty = true;
            }
        }

        public SurfaceFormat PreferredBackBufferFormat
        {
            get { return backBufferFormat; }
            set
            {
                if (Array.IndexOf<SurfaceFormat>(ValidBackBufferFormats, value) == -1)
                {
                    throw new ArgumentOutOfRangeException("value", Resources.ValidateBackBufferFormatIsInvalid);
                }
                backBufferFormat = value;
                isDeviceDirty = true;
            }
        }

        public int PreferredBackBufferHeight
        {
            get { return backBufferHeight; }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("value", Resources.BackBufferDimMustBePositive);
                }
                backBufferHeight = value;
                useResizedBackBuffer = false;
                isDeviceDirty = true;
            }
        }

        public int PreferredBackBufferWidth
        {
            get { return backBufferWidth; }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("value", Resources.BackBufferDimMustBePositive);
                }
                backBufferWidth = value;
                useResizedBackBuffer = false;
                isDeviceDirty = true;
            }
        }

        public DepthFormat PreferredDepthStencilFormat
        {
            get { return depthStencilFormat; }
            set
            {
                switch (value)
                {
                    case DepthFormat.Depth24Stencil8:
                    case DepthFormat.Depth24Stencil8Single:
                    case DepthFormat.Depth24Stencil4:
                    case DepthFormat.Depth24:
                    case DepthFormat.Depth32:
                    case DepthFormat.Depth16:
                    case DepthFormat.Depth15Stencil1:
                        depthStencilFormat = value;
                        isDeviceDirty = true;
                        return;
                }
                throw new ArgumentOutOfRangeException("value", Resources.ValidateDepthStencilFormatIsInvalid);
            }
        }

        public bool SynchronizeWithVerticalRetrace
        {
            get { return synchronizeWithVerticalRetrace; }
            set
            {
                synchronizeWithVerticalRetrace = value;
                isDeviceDirty = true;
            }
        }

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region IGraphicsDeviceManager Members

        bool IGraphicsDeviceManager.BeginDraw()
        {
            if (!EnsureDevice())
            {
                return false;
            }
            beginDrawOk = true;
            return true;
        }

        void IGraphicsDeviceManager.CreateDevice()
        {
            ChangeDevice(true);
        }

        void IGraphicsDeviceManager.EndDraw()
        {
            if (beginDrawOk && (device != null))
            {
                try
                {
                    device.Present();
                }
                catch (InvalidOperationException)
                {
                }
                catch (DeviceLostException)
                {
                }
                catch (DeviceNotResetException)
                {
                }
                catch (DriverInternalErrorException)
                {
                }
            }
        }

        #endregion

        #region IGraphicsDeviceService Members

        public event EventHandler DeviceCreated;

        public event EventHandler DeviceDisposing;

        public event EventHandler DeviceReset;

        public event EventHandler DeviceResetting;

        public GraphicsDevice GraphicsDevice
        {
            get { return device; }
        }

        #endregion

        public event EventHandler Disposed;

        public event EventHandler<PreparingDeviceSettingsEventArgs> PreparingDeviceSettings;

        private void AddDevices(bool anySuitableDevice, List<GraphicsDeviceInformation> foundDevices)
        {
            IntPtr handle = game.Window.Handle;
            foreach (GraphicsAdapter adapter in GraphicsAdapter.Adapters)
            {
                if (!anySuitableDevice && !IsWindowOnAdapter(handle, adapter))
                {
                    continue;
                }
                foreach (DeviceType type in ValidDeviceTypes)
                {
                    try
                    {
                        GraphicsDeviceCapabilities capabilities = adapter.GetCapabilities(type);
                        if ((capabilities.DeviceCapabilities.IsDirect3D9Driver &&
                             (capabilities.MaxPixelShaderProfile >= MinimumPixelShaderProfile)) &&
                            (capabilities.MaxVertexShaderProfile >= MinimumVertexShaderProfile))
                        {
                            CreateOptions none = CreateOptions.None;
                            if (capabilities.DeviceCapabilities.SupportsHardwareTransformAndLight)
                            {
                                none |= CreateOptions.HardwareVertexProcessing;
                            }
                            else
                            {
                                none |= CreateOptions.SoftwareVertexProcessing;
                            }
                            GraphicsDeviceInformation baseDeviceInfo = new GraphicsDeviceInformation();
                            baseDeviceInfo.Adapter = adapter;
                            baseDeviceInfo.DeviceType = type;
                            baseDeviceInfo.CreationOptions = none;
                            baseDeviceInfo.PresentationParameters.DeviceWindowHandle = IntPtr.Zero;
                            baseDeviceInfo.PresentationParameters.EnableAutoDepthStencil = true;
                            baseDeviceInfo.PresentationParameters.BackBufferCount = 1;
                            baseDeviceInfo.PresentationParameters.PresentOptions = PresentOptions.None;
                            baseDeviceInfo.PresentationParameters.SwapEffect = SwapEffect.Default;
                            baseDeviceInfo.PresentationParameters.FullScreenRefreshRateInHz = 0;
                            baseDeviceInfo.PresentationParameters.MultiSampleQuality = 0;
                            baseDeviceInfo.PresentationParameters.MultiSampleType = MultiSampleType.None;
                            baseDeviceInfo.PresentationParameters.IsFullScreen = IsFullScreen;
                            baseDeviceInfo.PresentationParameters.PresentationInterval = SynchronizeWithVerticalRetrace
                                                                                             ? PresentInterval.One
                                                                                             : PresentInterval.Immediate;
                            for (int i = 0; i < ValidAdapterFormats.Length; i++)
                            {
                                AddDevices(adapter, type, adapter.CurrentDisplayMode, baseDeviceInfo, foundDevices);
                                if (isFullScreen)
                                {
                                    foreach (DisplayMode mode in adapter.SupportedDisplayModes[ValidAdapterFormats[i]])
                                    {
                                        if ((mode.Width >= 640) && (mode.Height >= 480))
                                        {
                                            AddDevices(adapter, type, mode, baseDeviceInfo, foundDevices);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (DeviceNotSupportedException)
                    {
                    }
                }
            }
        }

        private void AddDevices(GraphicsAdapter adapter, DeviceType deviceType, DisplayMode mode,
                                GraphicsDeviceInformation baseDeviceInfo, List<GraphicsDeviceInformation> foundDevices)
        {
            for (int i = 0; i < ValidBackBufferFormats.Length; i++)
            {
                SurfaceFormat backBufferFormat = ValidBackBufferFormats[i];
                if (adapter.CheckDeviceType(deviceType, mode.Format, backBufferFormat, IsFullScreen))
                {
                    GraphicsDeviceInformation item = baseDeviceInfo.Clone();
                    if (IsFullScreen)
                    {
                        item.PresentationParameters.BackBufferWidth = mode.Width;
                        item.PresentationParameters.BackBufferHeight = mode.Height;
                        item.PresentationParameters.FullScreenRefreshRateInHz = mode.RefreshRate;
                    }
                    else if (useResizedBackBuffer)
                    {
                        item.PresentationParameters.BackBufferWidth = resizedBackBufferWidth;
                        item.PresentationParameters.BackBufferHeight = resizedBackBufferHeight;
                    }
                    else
                    {
                        item.PresentationParameters.BackBufferWidth = PreferredBackBufferWidth;
                        item.PresentationParameters.BackBufferHeight = PreferredBackBufferHeight;
                    }
                    item.PresentationParameters.BackBufferFormat = backBufferFormat;
                    item.PresentationParameters.AutoDepthStencilFormat = PreferredDepthStencilFormat;
                    if (PreferMultiSampling)
                    {
                        for (int j = 0; j < multiSampleTypes.Length; j++)
                        {
                            int qualityLevels = 0;
                            MultiSampleType sampleType = multiSampleTypes[j];
                            if (
                                adapter.CheckDeviceMultiSampleType(deviceType, backBufferFormat, IsFullScreen,
                                                                   sampleType, out qualityLevels))
                            {
                                GraphicsDeviceInformation information2 = item.Clone();
                                information2.PresentationParameters.MultiSampleType = sampleType;
                                if (!foundDevices.Contains(information2))
                                {
                                    foundDevices.Add(information2);
                                }
                                break;
                            }
                        }
                    }
                    else if (!foundDevices.Contains(item))
                    {
                        foundDevices.Add(item);
                    }
                }
            }
        }

        public void ApplyChanges()
        {
            if ((device == null) || isDeviceDirty)
            {
                ChangeDevice(false);
            }
        }

        protected virtual bool CanResetDevice(GraphicsDeviceInformation newDeviceInfo)
        {
            if (device.CreationParameters.Adapter != newDeviceInfo.Adapter)
            {
                return false;
            }
            if (device.CreationParameters.DeviceType != newDeviceInfo.DeviceType)
            {
                return false;
            }
            if (device.CreationParameters.CreationOptions != newDeviceInfo.CreationOptions)
            {
                return false;
            }
            return true;
        }

        private void ChangeDevice(bool forceCreate)
        {
            if (game == null)
            {
                throw new InvalidOperationException(Resources.GraphicsComponentNotAttachedToGame);
            }

            string screenDeviceName = game.Window.ScreenDeviceName;
            int width = game.Window.ClientBounds.Width;
            int height = game.Window.ClientBounds.Height;
            if (game.XnaRendererEnabled)
            {
                CheckForAvailableSupportedHardware();
                inDeviceTransition = true;
                bool didStartDeviceChange = false;

                try
                {
                    GraphicsDeviceInformation graphicsDeviceInformation = FindBestDevice(forceCreate);
                    game.Window.BeginScreenDeviceChange(graphicsDeviceInformation.PresentationParameters.IsFullScreen);
                    didStartDeviceChange = true;
                    
                    bool stillNeedsDevice = true;
                    if (!forceCreate && device != null)
                    {
                        //if (device.PresentationParameters.IsFullScreen ||
                        //    graphicsDeviceInformation.PresentationParameters.BackBufferWidth != device.PresentationParameters.BackBufferWidth ||
                        //    graphicsDeviceInformation.PresentationParameters.BackBufferHeight != device.PresentationParameters.BackBufferHeight ||
                        //    graphicsDeviceInformation.PresentationParameters.IsFullScreen != device.PresentationParameters.IsFullScreen ||
                        //    device.CreationParameters.Adapter.DeviceName != graphicsDeviceInformation.Adapter.DeviceName)
                        //{
                            OnPreparingDeviceSettings(this, new PreparingDeviceSettingsEventArgs(graphicsDeviceInformation));
                            if (CanResetDevice(graphicsDeviceInformation))
                            {
                                try
                                {
                                    GraphicsDeviceInformation information2 = graphicsDeviceInformation.Clone();
                                    MassagePresentParameters(graphicsDeviceInformation.PresentationParameters);
                                    ValidateGraphicsDeviceInformation(graphicsDeviceInformation);
                                    device.Reset(information2.PresentationParameters);
                                    stillNeedsDevice = false;
                                }
                                catch
                                {
                                }
                            }
                        //}
                        //else
                        //    stillNeedsDevice = false;
                    }
                    if (stillNeedsDevice)
                    {
                        CreateDevice(graphicsDeviceInformation);
                    }
                    PresentationParameters presentationParameters = device.PresentationParameters;
                    screenDeviceName = device.CreationParameters.Adapter.DeviceName;
                    isReallyFullScreen = presentationParameters.IsFullScreen;
                    if (presentationParameters.BackBufferWidth != 0)
                    {
                        width = PreferredBackBufferWidth;
                    }
                    if (presentationParameters.BackBufferHeight != 0)
                    {
                        height = PreferredBackBufferHeight;
                    }
                    isDeviceDirty = false;
                }
                finally
                {
                    if (didStartDeviceChange)
                    {
                        game.Window.EndScreenDeviceChange(screenDeviceName, width, height);
                    }
                    inDeviceTransition = false;
                }
            }
            else
            {
                isReallyFullScreen = IsFullScreen;
                width = PreferredBackBufferWidth;
                height = PreferredBackBufferHeight;

                game.Window.BeginScreenDeviceChange(IsFullScreen);
                game.Window.EndScreenDeviceChange(screenDeviceName, width, height);
            }
        }

        private void CheckForAvailableSupportedHardware()
        {
            bool flag = false;
            bool flag2 = false;
            foreach (GraphicsAdapter adapter in GraphicsAdapter.Adapters)
            {
                if (adapter.IsDeviceTypeAvailable(DeviceType.Hardware))
                {
                    flag = true;
                    GraphicsDeviceCapabilities capabilities = adapter.GetCapabilities(DeviceType.Hardware);
                    if (((capabilities.MaxPixelShaderProfile != ShaderProfile.Unknown) &&
                         (capabilities.MaxPixelShaderProfile >= ShaderProfile.PS_1_1)) &&
                        capabilities.DeviceCapabilities.IsDirect3D9Driver)
                    {
                        flag2 = true;
                        break;
                    }
                }
            }
            if (!flag)
            {
                if (GetSystemMetrics(0x1000) != 0)
                {
                    throw new NoSuitableGraphicsDeviceException("");
                }
                throw new NoSuitableGraphicsDeviceException("");
            }
            if (!flag2)
            {
                throw new NoSuitableGraphicsDeviceException("");
            }
        }

        private void CreateDevice(GraphicsDeviceInformation newInfo)
        {
            if (device != null)
            {
                device.Dispose();
                device = null;
            }
            OnPreparingDeviceSettings(this, new PreparingDeviceSettingsEventArgs(newInfo));
            MassagePresentParameters(newInfo.PresentationParameters);
            try
            {
                ValidateGraphicsDeviceInformation(newInfo);
                device =
                    new GraphicsDevice(newInfo.Adapter, newInfo.DeviceType, game.Window.Handle, newInfo.CreationOptions,
                                       newInfo.PresentationParameters);
                this.device.DeviceResetting += new EventHandler(HandleDeviceResetting);
                this.device.DeviceReset += new EventHandler(HandleDeviceReset);
                this.device.DeviceLost += new EventHandler(HandleDeviceLost);
                this.device.Disposing += new EventHandler(HandleDisposing);
            }
            catch (DeviceNotSupportedException exception)
            {
                throw new NoSuitableGraphicsDeviceException("", exception);
            }
            catch (DriverInternalErrorException exception2)
            {
                throw new NoSuitableGraphicsDeviceException("", exception2);
            }
            catch (ArgumentException exception3)
            {
                throw new NoSuitableGraphicsDeviceException("", exception3);
            }
            catch (Exception exception4)
            {
                throw new NoSuitableGraphicsDeviceException("", exception4);
            }
            OnDeviceCreated(this, EventArgs.Empty);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (game != null)
                {
                    if (game.Services.GetService(typeof (IGraphicsDeviceService)) == this)
                    {
                        game.Services.RemoveService(typeof (IGraphicsDeviceService));
                    }
                    game.Window.ClientSizeChanged -= new EventHandler(GameWindowClientSizeChanged);
                    game.Window.ScreenDeviceNameChanged -= new EventHandler(GameWindowScreenDeviceNameChanged);
                }
                if (device != null)
                {
                    device.Dispose();
                    device = null;
                }
                if (Disposed != null)
                {
                    Disposed(this, EventArgs.Empty);
                }
            }
        }

        private bool EnsureDevice()
        {
            if (device == null)
            {
                return false;
            }
            return EnsureDevicePlatform();
        }

        private bool EnsureDevicePlatform()
        {
            if (isReallyFullScreen && !Game.IsActive)
            {
                return false;
            }
            switch (device.GraphicsDeviceStatus)
            {
                case GraphicsDeviceStatus.Lost:
                    Thread.Sleep((int) deviceLostSleepTime.TotalMilliseconds);
                    return false;

                case GraphicsDeviceStatus.NotReset:
                    Thread.Sleep((int) deviceLostSleepTime.TotalMilliseconds);
                    try
                    {
                        ChangeDevice(false);
                    }
                    catch (DeviceLostException)
                    {
                        return false;
                    }
                    catch
                    {
                        ChangeDevice(true);
                    }
                    break;
            }
            return true;
        }

        protected virtual GraphicsDeviceInformation FindBestDevice(bool anySuitableDevice)
        {
            return FindBestPlatformDevice(anySuitableDevice);
        }

        private GraphicsDeviceInformation FindBestPlatformDevice(bool anySuitableDevice)
        {
            List<GraphicsDeviceInformation> foundDevices = new List<GraphicsDeviceInformation>();
            AddDevices(anySuitableDevice, foundDevices);
            if ((foundDevices.Count == 0) && PreferMultiSampling)
            {
                PreferMultiSampling = false;
                AddDevices(anySuitableDevice, foundDevices);
            }
            if (foundDevices.Count == 0)
            {
                throw new NoSuitableGraphicsDeviceException(Resources.NoCompatibleDevices);
            }
            RankDevices(foundDevices);
            if (foundDevices.Count == 0)
            {
                throw new NoSuitableGraphicsDeviceException(Resources.NoCompatibleDevicesAfterRanking);
            }
            return foundDevices[0];
        }

        private void GameWindowClientSizeChanged(object sender, EventArgs e)
        {
            if (!inDeviceTransition && ((game.Window.ClientBounds.Height != 0) || (game.Window.ClientBounds.Width != 0)))
            {
                resizedBackBufferWidth = game.Window.ClientBounds.Width;
                resizedBackBufferHeight = game.Window.ClientBounds.Height;
                useResizedBackBuffer = true;
                ChangeDevice(false);
            }
        }

        private void GameWindowScreenDeviceNameChanged(object sender, EventArgs e)
        {
            if (!inDeviceTransition)
            {
                ChangeDevice(false);
            }
        }

        [DllImport("user32.dll")]
        private static extern int GetSystemMetrics(uint smIndex);

        private void HandleDeviceLost(object sender, EventArgs e)
        {

        }

        private void HandleDeviceReset(object sender, EventArgs e)
        {
            OnDeviceReset(this, EventArgs.Empty);
        }

        private void HandleDeviceResetting(object sender, EventArgs e)
        {
            OnDeviceResetting(this, EventArgs.Empty);
        }

        private void HandleDisposing(object sender, EventArgs e)
        {
            OnDeviceDisposing(this, EventArgs.Empty);
        }

        private bool IsWindowOnAdapter(IntPtr windowHandle, GraphicsAdapter adapter)
        {
            return (WindowsGameWindow.ScreenFromAdapter(adapter) == WindowsGameWindow.ScreenFromHandle(windowHandle));
        }

        private void MassagePresentParameters(PresentationParameters pp)
        {
            bool flag = pp.BackBufferWidth == 0;
            bool flag2 = pp.BackBufferHeight == 0;
            if (!pp.IsFullScreen)
            {
                NativeMethods.RECT rect;
                IntPtr deviceWindowHandle = pp.DeviceWindowHandle;
                if (deviceWindowHandle == IntPtr.Zero)
                {
                    if (game == null)
                    {
                        throw new InvalidOperationException(Resources.GraphicsComponentNotAttachedToGame);
                    }
                    deviceWindowHandle = game.Window.Handle;
                }
                NativeMethods.GetClientRect(deviceWindowHandle, out rect);
                if (flag && (rect.Right == 0))
                {
                    pp.BackBufferWidth = 1;
                }
                if (flag2 && (rect.Bottom == 0))
                {
                    pp.BackBufferHeight = 1;
                }
            }
        }

        protected virtual void OnDeviceCreated(object sender, EventArgs args)
        {
            if (DeviceCreated != null)
            {
                DeviceCreated(sender, args);
            }
        }

        protected virtual void OnDeviceDisposing(object sender, EventArgs args)
        {
            if (DeviceDisposing != null)
            {
                DeviceDisposing(sender, args);
            }
        }

        protected virtual void OnDeviceReset(object sender, EventArgs args)
        {
            if (DeviceReset != null)
            {
                DeviceReset(sender, args);
            }
        }

        protected virtual void OnDeviceResetting(object sender, EventArgs args)
        {
            if (DeviceResetting != null)
            {
                DeviceResetting(sender, args);
            }
        }

        protected virtual void OnPreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs args)
        {
            if (PreparingDeviceSettings != null)
            {
                PreparingDeviceSettings(sender, args);
            }
        }

        protected virtual void RankDevices(List<GraphicsDeviceInformation> foundDevices)
        {
            RankDevicesPlatform(foundDevices);
        }

        private void RankDevicesPlatform(List<GraphicsDeviceInformation> foundDevices)
        {
            int index = 0;
            while (index < foundDevices.Count)
            {
                DeviceType deviceType = foundDevices[index].DeviceType;
                GraphicsAdapter adapter = foundDevices[index].Adapter;
                PresentationParameters presentationParameters = foundDevices[index].PresentationParameters;
                if (
                    !adapter.CheckDeviceFormat(deviceType, adapter.CurrentDisplayMode.Format, ResourceUsage.None,
                                               QueryUsages.PostPixelShaderBlending, ResourceType.Texture2D,
                                               presentationParameters.BackBufferFormat))
                {
                    foundDevices.RemoveAt(index);
                }
                else
                {
                    index++;
                }
            }
            foundDevices.Sort(new GraphicsDeviceInformationComparer(this));
        }

        public void ToggleFullScreen()
        {
            IsFullScreen = !IsFullScreen;
            ChangeDevice(false);
        }

        private void ValidateGraphicsDeviceInformation(GraphicsDeviceInformation devInfo)
        {
            SurfaceFormat format;
            GraphicsAdapter adapter = devInfo.Adapter;
            DeviceType deviceType = devInfo.DeviceType;
            bool enableAutoDepthStencil = devInfo.PresentationParameters.EnableAutoDepthStencil;
            DepthFormat autoDepthStencilFormat = devInfo.PresentationParameters.AutoDepthStencilFormat;
            SurfaceFormat backBufferFormat = devInfo.PresentationParameters.BackBufferFormat;
            int backBufferWidth = devInfo.PresentationParameters.BackBufferWidth;
            int backBufferHeight = devInfo.PresentationParameters.BackBufferHeight;
            PresentationParameters presentationParameters = devInfo.PresentationParameters;
            SurfaceFormat format4 = presentationParameters.BackBufferFormat;
            if (!presentationParameters.IsFullScreen)
            {
                format = adapter.CurrentDisplayMode.Format;
                if (SurfaceFormat.Unknown == presentationParameters.BackBufferFormat)
                {
                    format4 = format;
                }
            }
            else
            {
                SurfaceFormat format5 = presentationParameters.BackBufferFormat;
                if (format5 != SurfaceFormat.Color)
                {
                    if (format5 != SurfaceFormat.Bgra5551)
                    {
                        format = presentationParameters.BackBufferFormat;
                    }
                    else
                    {
                        format = SurfaceFormat.Bgr555;
                    }
                }
                else
                {
                    format = SurfaceFormat.Bgr32;
                }
            }
            if (-1 == Array.IndexOf<SurfaceFormat>(ValidBackBufferFormats, format4))
            {
                throw new ArgumentException(Resources.ValidateBackBufferFormatIsInvalid);
            }
            if (
                !adapter.CheckDeviceType(deviceType, format, presentationParameters.BackBufferFormat,
                                         presentationParameters.IsFullScreen))
            {
                throw new ArgumentException(Resources.ValidateDeviceType);
            }
            if ((presentationParameters.BackBufferCount < 0) || (presentationParameters.BackBufferCount > 3))
            {
                throw new ArgumentException(Resources.ValidateBackBufferCount);
            }
            if ((presentationParameters.BackBufferCount > 1) && (presentationParameters.SwapEffect == SwapEffect.Copy))
            {
                throw new ArgumentException(Resources.ValidateBackBufferCountSwapCopy);
            }
            switch (presentationParameters.SwapEffect)
            {
                case SwapEffect.Default:
                case SwapEffect.Flip:
                case SwapEffect.Copy:
                    {
                        int num3;
                        if (
                            !adapter.CheckDeviceMultiSampleType(deviceType, format4, presentationParameters.IsFullScreen,
                                                                presentationParameters.MultiSampleType, out num3))
                        {
                            throw new ArgumentException(Resources.ValidateMultiSampleTypeInvalid);
                        }
                        if (presentationParameters.MultiSampleQuality >= num3)
                        {
                            throw new ArgumentException(Resources.ValidateMultiSampleQualityInvalid);
                        }
                        if ((presentationParameters.MultiSampleType != MultiSampleType.None) &&
                            (presentationParameters.SwapEffect != SwapEffect.Default))
                        {
                            throw new ArgumentException(Resources.ValidateMultiSampleSwapEffect);
                        }
                        if (((presentationParameters.PresentOptions & PresentOptions.DiscardDepthStencil) !=
                             PresentOptions.None) && !presentationParameters.EnableAutoDepthStencil)
                        {
                            throw new ArgumentException(Resources.ValidateAutoDepthStencilMismatch);
                        }
                        if (presentationParameters.EnableAutoDepthStencil)
                        {
                            if (
                                !adapter.CheckDeviceFormat(deviceType, format, ResourceUsage.None, QueryUsages.None,
                                                           ResourceType.DepthStencilBuffer,
                                                           presentationParameters.AutoDepthStencilFormat))
                            {
                                throw new ArgumentException(Resources.ValidateAutoDepthStencilFormatInvalid);
                            }
                            if (
                                !adapter.CheckDepthStencilMatch(deviceType, format, format4,
                                                                presentationParameters.AutoDepthStencilFormat))
                            {
                                throw new ArgumentException(Resources.ValidateAutoDepthStencilFormatIncompatible);
                            }
                        }
                        if (!presentationParameters.IsFullScreen)
                        {
                            if (presentationParameters.FullScreenRefreshRateInHz != 0)
                            {
                                throw new ArgumentException(Resources.ValidateRefreshRateInWindow);
                            }
                            switch (presentationParameters.PresentationInterval)
                            {
                                case PresentInterval.Default:
                                case PresentInterval.One:
                                case PresentInterval.Immediate:
                                    return;
                            }
                            throw new ArgumentException(Resources.ValidatePresentationIntervalInWindow);
                        }
                        if (presentationParameters.FullScreenRefreshRateInHz == 0)
                        {
                            throw new ArgumentException(Resources.ValidateRefreshRateInFullScreen);
                        }
                        GraphicsDeviceCapabilities capabilities = adapter.GetCapabilities(deviceType);
                        switch (presentationParameters.PresentationInterval)
                        {
                            case PresentInterval.Default:
                            case PresentInterval.One:
                            case PresentInterval.Immediate:
                                goto Label_02E5;

                            case PresentInterval.Two:
                            case PresentInterval.Three:
                            case PresentInterval.Four:
                                if ((capabilities.PresentInterval & presentationParameters.PresentationInterval) ==
                                    PresentInterval.Default)
                                {
                                    throw new ArgumentException(
                                        Resources.ValidatePresentationIntervalIncompatibleInFullScreen);
                                }
                                goto Label_02E5;
                        }
                        break;
                    }
                default:
                    throw new ArgumentException(Resources.ValidateSwapEffectInvalid);
            }
            throw new ArgumentException(Resources.ValidatePresentationIntervalInFullScreen);
            Label_02E5:
            if (presentationParameters.IsFullScreen)
            {
                if ((presentationParameters.BackBufferWidth == 0) || (presentationParameters.BackBufferHeight == 0))
                {
                    throw new ArgumentException(Resources.ValidateBackBufferDimsFullScreen);
                }
                bool flag2 = true;
                bool flag3 = false;
                DisplayMode currentDisplayMode = adapter.CurrentDisplayMode;
                if (((currentDisplayMode.Format != format) &&
                     (currentDisplayMode.Width != presentationParameters.BackBufferHeight)) &&
                    ((currentDisplayMode.Height != presentationParameters.BackBufferHeight) &&
                     (currentDisplayMode.RefreshRate != presentationParameters.FullScreenRefreshRateInHz)))
                {
                    flag2 = false;
                    foreach (DisplayMode mode2 in adapter.SupportedDisplayModes[format])
                    {
                        if ((mode2.Width == presentationParameters.BackBufferWidth) &&
                            (mode2.Height == presentationParameters.BackBufferHeight))
                        {
                            flag3 = true;
                            if (mode2.RefreshRate == presentationParameters.FullScreenRefreshRateInHz)
                            {
                                flag2 = true;
                                break;
                            }
                        }
                    }
                }
                if (!flag2 && flag3)
                {
                    throw new ArgumentException(Resources.ValidateBackBufferDimsModeFullScreen);
                }
                if (!flag2)
                {
                    throw new ArgumentException(Resources.ValidateBackBufferHzModeFullScreen);
                }
            }
            if (presentationParameters.EnableAutoDepthStencil != enableAutoDepthStencil)
            {
                throw new ArgumentException(Resources.ValidateAutoDepthStencilAdapterGroup);
            }
            if (presentationParameters.EnableAutoDepthStencil)
            {
                if (presentationParameters.AutoDepthStencilFormat != autoDepthStencilFormat)
                {
                    throw new ArgumentException(Resources.ValidateAutoDepthStencilAdapterGroup);
                }
                if (presentationParameters.BackBufferFormat != backBufferFormat)
                {
                    throw new ArgumentException(Resources.ValidateAutoDepthStencilAdapterGroup);
                }
                if (presentationParameters.BackBufferWidth != backBufferWidth)
                {
                    throw new ArgumentException(Resources.ValidateAutoDepthStencilAdapterGroup);
                }
                if (presentationParameters.BackBufferHeight != backBufferHeight)
                {
                    throw new ArgumentException(Resources.ValidateAutoDepthStencilAdapterGroup);
                }
            }
        }
    }
}