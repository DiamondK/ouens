﻿namespace Microsoft.Xna.Framework
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Resources;

    [DebuggerNonUserCode, GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
    internal class Resources
    {
        private static CultureInfo resourceCulture;
        private static System.Resources.ResourceManager resourceMan;

        internal Resources()
        {
        }

        internal static string BackBufferDimMustBePositive
        {
            get
            {
                return ResourceManager.GetString("BackBufferDimMustBePositive", resourceCulture);
            }
        }

        internal static string CannotAddSameComponentMultipleTimes
        {
            get
            {
                return ResourceManager.GetString("CannotAddSameComponentMultipleTimes", resourceCulture);
            }
        }

        internal static string CannotSetItemsIntoGameComponentCollection
        {
            get
            {
                return ResourceManager.GetString("CannotSetItemsIntoGameComponentCollection", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }

        internal static string DefaultTitleName
        {
            get
            {
                return ResourceManager.GetString("DefaultTitleName", resourceCulture);
            }
        }

        internal static string Direct3DCreateError
        {
            get
            {
                return ResourceManager.GetString("Direct3DCreateError", resourceCulture);
            }
        }

        internal static string Direct3DInternalDriverError
        {
            get
            {
                return ResourceManager.GetString("Direct3DInternalDriverError", resourceCulture);
            }
        }

        internal static string Direct3DInvalidCreateParameters
        {
            get
            {
                return ResourceManager.GetString("Direct3DInvalidCreateParameters", resourceCulture);
            }
        }

        internal static string Direct3DNotAvailable
        {
            get
            {
                return ResourceManager.GetString("Direct3DNotAvailable", resourceCulture);
            }
        }

        internal static string GameCannotBeNull
        {
            get
            {
                return ResourceManager.GetString("GameCannotBeNull", resourceCulture);
            }
        }

        internal static string GraphicsComponentNotAttachedToGame
        {
            get
            {
                return ResourceManager.GetString("GraphicsComponentNotAttachedToGame", resourceCulture);
            }
        }

        internal static string GraphicsDeviceManagerAlreadyPresent
        {
            get
            {
                return ResourceManager.GetString("GraphicsDeviceManagerAlreadyPresent", resourceCulture);
            }
        }

        internal static string InactiveSleepTimeCannotBeZero
        {
            get
            {
                return ResourceManager.GetString("InactiveSleepTimeCannotBeZero", resourceCulture);
            }
        }

        internal static string InvalidPixelShaderProfile
        {
            get
            {
                return ResourceManager.GetString("InvalidPixelShaderProfile", resourceCulture);
            }
        }

        internal static string InvalidScreenAdapter
        {
            get
            {
                return ResourceManager.GetString("InvalidScreenAdapter", resourceCulture);
            }
        }

        internal static string InvalidScreenDeviceName
        {
            get
            {
                return ResourceManager.GetString("InvalidScreenDeviceName", resourceCulture);
            }
        }

        internal static string InvalidVertexShaderProfile
        {
            get
            {
                return ResourceManager.GetString("InvalidVertexShaderProfile", resourceCulture);
            }
        }

        internal static string MissingGraphicsDeviceService
        {
            get
            {
                return ResourceManager.GetString("MissingGraphicsDeviceService", resourceCulture);
            }
        }

        internal static string MustCallBeginDeviceChange
        {
            get
            {
                return ResourceManager.GetString("MustCallBeginDeviceChange", resourceCulture);
            }
        }

        internal static string NoCompatibleDevices
        {
            get
            {
                return ResourceManager.GetString("NoCompatibleDevices", resourceCulture);
            }
        }

        internal static string NoCompatibleDevicesAfterRanking
        {
            get
            {
                return ResourceManager.GetString("NoCompatibleDevicesAfterRanking", resourceCulture);
            }
        }

        internal static string NoDirect3DAcceleration
        {
            get
            {
                return ResourceManager.GetString("NoDirect3DAcceleration", resourceCulture);
            }
        }

        internal static string NoDirect3DAccelerationRemoteDesktop
        {
            get
            {
                return ResourceManager.GetString("NoDirect3DAccelerationRemoteDesktop", resourceCulture);
            }
        }

        internal static string NoHighResolutionTimer
        {
            get
            {
                return ResourceManager.GetString("NoHighResolutionTimer", resourceCulture);
            }
        }

        internal static string NoMulitpleRuns
        {
            get
            {
                return ResourceManager.GetString("NoMulitpleRuns", resourceCulture);
            }
        }

        internal static string NoNullUseDefaultAdapter
        {
            get
            {
                return ResourceManager.GetString("NoNullUseDefaultAdapter", resourceCulture);
            }
        }

        internal static string NoPixelShader11OrDDI9Support
        {
            get
            {
                return ResourceManager.GetString("NoPixelShader11OrDDI9Support", resourceCulture);
            }
        }

        internal static string NullOrEmptyScreenDeviceName
        {
            get
            {
                return ResourceManager.GetString("NullOrEmptyScreenDeviceName", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null))
                {
                    System.Resources.ResourceManager manager = new System.Resources.ResourceManager("Microsoft.Xna.Framework.Resources", typeof(Microsoft.Xna.Framework.Resources).Assembly);
                    resourceMan = manager;
                }
                return resourceMan;
            }
        }

        internal static string ServiceAlreadyPresent
        {
            get
            {
                return ResourceManager.GetString("ServiceAlreadyPresent", resourceCulture);
            }
        }

        internal static string ServiceMustBeAssignable
        {
            get
            {
                return ResourceManager.GetString("ServiceMustBeAssignable", resourceCulture);
            }
        }

        internal static string ServiceProviderCannotBeNull
        {
            get
            {
                return ResourceManager.GetString("ServiceProviderCannotBeNull", resourceCulture);
            }
        }

        internal static string ServiceTypeCannotBeNull
        {
            get
            {
                return ResourceManager.GetString("ServiceTypeCannotBeNull", resourceCulture);
            }
        }

        internal static string TargetElaspedCannotBeZero
        {
            get
            {
                return ResourceManager.GetString("TargetElaspedCannotBeZero", resourceCulture);
            }
        }

        internal static string TitleCannotBeNull
        {
            get
            {
                return ResourceManager.GetString("TitleCannotBeNull", resourceCulture);
            }
        }

        internal static string ValidateAutoDepthStencilAdapterGroup
        {
            get
            {
                return ResourceManager.GetString("ValidateAutoDepthStencilAdapterGroup", resourceCulture);
            }
        }

        internal static string ValidateAutoDepthStencilFormatIncompatible
        {
            get
            {
                return ResourceManager.GetString("ValidateAutoDepthStencilFormatIncompatible", resourceCulture);
            }
        }

        internal static string ValidateAutoDepthStencilFormatInvalid
        {
            get
            {
                return ResourceManager.GetString("ValidateAutoDepthStencilFormatInvalid", resourceCulture);
            }
        }

        internal static string ValidateAutoDepthStencilMismatch
        {
            get
            {
                return ResourceManager.GetString("ValidateAutoDepthStencilMismatch", resourceCulture);
            }
        }

        internal static string ValidateBackBufferCount
        {
            get
            {
                return ResourceManager.GetString("ValidateBackBufferCount", resourceCulture);
            }
        }

        internal static string ValidateBackBufferCountSwapCopy
        {
            get
            {
                return ResourceManager.GetString("ValidateBackBufferCountSwapCopy", resourceCulture);
            }
        }

        internal static string ValidateBackBufferDimsFullScreen
        {
            get
            {
                return ResourceManager.GetString("ValidateBackBufferDimsFullScreen", resourceCulture);
            }
        }

        internal static string ValidateBackBufferDimsModeFullScreen
        {
            get
            {
                return ResourceManager.GetString("ValidateBackBufferDimsModeFullScreen", resourceCulture);
            }
        }

        internal static string ValidateBackBufferFormatIsInvalid
        {
            get
            {
                return ResourceManager.GetString("ValidateBackBufferFormatIsInvalid", resourceCulture);
            }
        }

        internal static string ValidateBackBufferHzModeFullScreen
        {
            get
            {
                return ResourceManager.GetString("ValidateBackBufferHzModeFullScreen", resourceCulture);
            }
        }

        internal static string ValidateDepthStencilFormatIsInvalid
        {
            get
            {
                return ResourceManager.GetString("ValidateDepthStencilFormatIsInvalid", resourceCulture);
            }
        }

        internal static string ValidateDeviceType
        {
            get
            {
                return ResourceManager.GetString("ValidateDeviceType", resourceCulture);
            }
        }

        internal static string ValidateMultiSampleQualityInvalid
        {
            get
            {
                return ResourceManager.GetString("ValidateMultiSampleQualityInvalid", resourceCulture);
            }
        }

        internal static string ValidateMultiSampleSwapEffect
        {
            get
            {
                return ResourceManager.GetString("ValidateMultiSampleSwapEffect", resourceCulture);
            }
        }

        internal static string ValidateMultiSampleTypeInvalid
        {
            get
            {
                return ResourceManager.GetString("ValidateMultiSampleTypeInvalid", resourceCulture);
            }
        }

        internal static string ValidatePresentationIntervalIncompatibleInFullScreen
        {
            get
            {
                return ResourceManager.GetString("ValidatePresentationIntervalIncompatibleInFullScreen", resourceCulture);
            }
        }

        internal static string ValidatePresentationIntervalInFullScreen
        {
            get
            {
                return ResourceManager.GetString("ValidatePresentationIntervalInFullScreen", resourceCulture);
            }
        }

        internal static string ValidatePresentationIntervalInWindow
        {
            get
            {
                return ResourceManager.GetString("ValidatePresentationIntervalInWindow", resourceCulture);
            }
        }

        internal static string ValidatePresentationIntervalOnXbox
        {
            get
            {
                return ResourceManager.GetString("ValidatePresentationIntervalOnXbox", resourceCulture);
            }
        }

        internal static string ValidateRefreshRateInFullScreen
        {
            get
            {
                return ResourceManager.GetString("ValidateRefreshRateInFullScreen", resourceCulture);
            }
        }

        internal static string ValidateRefreshRateInWindow
        {
            get
            {
                return ResourceManager.GetString("ValidateRefreshRateInWindow", resourceCulture);
            }
        }

        internal static string ValidateSwapEffectInvalid
        {
            get
            {
                return ResourceManager.GetString("ValidateSwapEffectInvalid", resourceCulture);
            }
        }
    }
}

