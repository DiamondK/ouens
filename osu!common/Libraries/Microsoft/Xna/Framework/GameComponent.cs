﻿namespace Microsoft.Xna.Framework
{
    using System;
    using System.Runtime.CompilerServices;

    public class GameComponent : IGameComponent, IUpdateable, IDisposable
    {
        private bool enabled = true;
        private Microsoft.Xna.Framework.Game game;
        private int updateOrder;

        public event EventHandler Disposed;

        public event EventHandler EnabledChanged;

        public event EventHandler UpdateOrderChanged;

        public GameComponent(Microsoft.Xna.Framework.Game game)
        {
            this.game = game;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                lock (this)
                {
                    if (this.Game != null)
                    {
                        this.Game.Components.Remove(this);
                    }
                    if (this.Disposed != null)
                    {
                        this.Disposed(this, EventArgs.Empty);
                    }
                }
            }
        }

        ~GameComponent()
        {
            this.Dispose(false);
        }

        public virtual void Initialize()
        {
        }

        protected virtual void OnEnabledChanged(object sender, EventArgs args)
        {
            if (this.EnabledChanged != null)
            {
                this.EnabledChanged(this, EventArgs.Empty);
            }
        }

        protected virtual void OnUpdateOrderChanged(object sender, EventArgs args)
        {
            if (this.UpdateOrderChanged != null)
            {
                this.UpdateOrderChanged(this, EventArgs.Empty);
            }
        }

        public virtual void Update()
        {
        }

        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                if (this.enabled != value)
                {
                    this.enabled = value;
                    this.OnEnabledChanged(this, EventArgs.Empty);
                }
            }
        }

        protected Microsoft.Xna.Framework.Game Game
        {
            get
            {
                return this.game;
            }
        }

        public int UpdateOrder
        {
            get
            {
                return this.updateOrder;
            }
            set
            {
                if (this.updateOrder != value)
                {
                    this.updateOrder = value;
                    this.OnUpdateOrderChanged(this, EventArgs.Empty);
                }
            }
        }
    }
}

