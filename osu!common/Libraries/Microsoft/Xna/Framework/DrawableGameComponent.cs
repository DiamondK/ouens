﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace Microsoft.Xna.Framework
{
    public class DrawableGameComponent : GameComponent, IDrawable
    {
        private IGraphicsDeviceService deviceService;
        private int drawOrder;
        private bool visible;

        public DrawableGameComponent(Game game) : base(game)
        {
            visible = true;
        }

        protected GraphicsDevice GraphicsDevice
        {
            get { return deviceService.GraphicsDevice; }
        }

        #region IDrawable Members

        public event EventHandler DrawOrderChanged;

        public event EventHandler VisibleChanged;

        public virtual void Draw()
        {
        }

        public int DrawOrder
        {
            get { return drawOrder; }
            set
            {
                if (drawOrder != value)
                {
                    drawOrder = value;
                    OnDrawOrderChanged(this, EventArgs.Empty);
                }
            }
        }

        public virtual bool Visible
        {
            get { return visible; }
            set
            {
                if (visible != value)
                {
                    visible = value;
                    OnVisibleChanged(this, EventArgs.Empty);
                }
            }
        }

        #endregion

        private void DeviceCreated(object sender, EventArgs e)
        {
            LoadGraphicsContent(true);
        }

        private void DeviceDisposing(object sender, EventArgs e)
        {
            UnloadGraphicsContent(true);
        }

        private void DeviceReset(object sender, EventArgs e)
        {
            LoadGraphicsContent(false);
        }

        private void DeviceResetting(object sender, EventArgs e)
        {
            UnloadGraphicsContent(false);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (deviceService != null))
            {
                deviceService.DeviceCreated -= new EventHandler(DeviceCreated);
                deviceService.DeviceResetting -= new EventHandler(DeviceResetting);
                deviceService.DeviceReset -= new EventHandler(DeviceReset);
                deviceService.DeviceDisposing -= new EventHandler(DeviceDisposing);
            }
            base.Dispose(disposing);
        }

        public override void Initialize()
        {
            base.Initialize();
            deviceService = base.Game.Services.GetService(typeof (IGraphicsDeviceService)) as IGraphicsDeviceService;
            if (deviceService == null)
            {
                throw new InvalidOperationException(Resources.MissingGraphicsDeviceService);
            }
            deviceService.DeviceCreated += new EventHandler(DeviceCreated);
            deviceService.DeviceResetting += new EventHandler(DeviceResetting);
            deviceService.DeviceReset += new EventHandler(DeviceReset);
            deviceService.DeviceDisposing += new EventHandler(DeviceDisposing);
            if (deviceService.GraphicsDevice != null)
            {
                LoadGraphicsContent(true);
            }
        }

        protected virtual void LoadGraphicsContent(bool loadAllContent)
        {
        }

        protected virtual void OnDrawOrderChanged(object sender, EventArgs args)
        {
            if (DrawOrderChanged != null)
            {
                DrawOrderChanged(this, EventArgs.Empty);
            }
        }

        protected virtual void OnVisibleChanged(object sender, EventArgs args)
        {
            if (VisibleChanged != null)
            {
                VisibleChanged(this, EventArgs.Empty);
            }
        }

        protected virtual void UnloadGraphicsContent(bool unloadAllContent)
        {
        }
    }
}