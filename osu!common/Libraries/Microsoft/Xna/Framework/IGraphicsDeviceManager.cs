﻿namespace Microsoft.Xna.Framework
{
    using System;

    public interface IGraphicsDeviceManager
    {
        bool BeginDraw();
        void CreateDevice();
        void EndDraw();
    }
}

