namespace Ionic.Zlib
{
    using System;

    public enum CompressionLevel
    {
        BestCompression = 9,
        BestSpeed = 1,
        Default = 6,
        Level0 = 0,
        Level1 = 1,
        Level2 = 2,
        Level3 = 3,
        Level4 = 4,
        Level5 = 5,
        Level6 = 6,
        Level7 = 7,
        Level8 = 8,
        Level9 = 9,
        None = 0
    }
}

