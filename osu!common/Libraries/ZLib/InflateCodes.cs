namespace Ionic.Zlib
{
    using System;

    internal sealed class InflateCodes
    {
        private const int BADCODE = 9;
        private const int COPY = 5;
        internal byte dbits;
        internal int dist;
        private const int DIST = 3;
        private const int DISTEXT = 4;
        internal int[] dtree;
        internal int dtree_index;
        private const int END = 8;
        internal int get_Renamed;
        private static readonly int[] inflate_mask = new int[] { 
            0, 1, 3, 7, 15, 0x1f, 0x3f, 0x7f, 0xff, 0x1ff, 0x3ff, 0x7ff, 0xfff, 0x1fff, 0x3fff, 0x7fff, 
            0xffff
         };
        internal byte lbits;
        internal int len;
        private const int LEN = 1;
        private const int LENEXT = 2;
        internal int lit;
        private const int LIT = 6;
        internal int[] ltree;
        internal int ltree_index;
        internal int mode;
        internal int need;
        private const int START = 0;
        internal int[] tree;
        internal int tree_index = 0;
        private const int WASH = 7;

        internal InflateCodes()
        {
        }

        internal int InflateFast(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index, InflateBlocks s, ZlibCodec z)
        {
            int num12;
            int nextIn = z.NextIn;
            int availableBytesIn = z.AvailableBytesIn;
            int bitb = s.bitb;
            int bitk = s.bitk;
            int write = s.write;
            int num9 = (write < s.read) ? ((s.read - write) - 1) : (s.end - write);
            int num10 = inflate_mask[bl];
            int num11 = inflate_mask[bd];
        Label_0096:
            while (bitk < 20)
            {
                availableBytesIn--;
                bitb |= (z.InputBuffer[nextIn++] & 0xff) << bitk;
                bitk += 8;
            }
            int num = bitb & num10;
            int[] numArray = tl;
            int num2 = tl_index;
            int index = (num2 + num) * 3;
            int num3 = numArray[index];
            if (num3 == 0)
            {
                bitb = bitb >> numArray[index + 1];
                bitk -= numArray[index + 1];
                s.window[write++] = (byte) numArray[index + 2];
                num9--;
            }
            else
            {
                while (true)
                {
                    bool flag;
                    bitb = bitb >> numArray[index + 1];
                    bitk -= numArray[index + 1];
                    if ((num3 & 0x10) != 0)
                    {
                        num3 &= 15;
                        num12 = numArray[index + 2] + (bitb & inflate_mask[num3]);
                        bitb = bitb >> num3;
                        bitk -= num3;
                        while (bitk < 15)
                        {
                            availableBytesIn--;
                            bitb |= (z.InputBuffer[nextIn++] & 0xff) << bitk;
                            bitk += 8;
                        }
                        num = bitb & num11;
                        numArray = td;
                        num2 = td_index;
                        index = (num2 + num) * 3;
                        num3 = numArray[index];
                        while (true)
                        {
                            bitb = bitb >> numArray[index + 1];
                            bitk -= numArray[index + 1];
                            if ((num3 & 0x10) != 0)
                            {
                                int num14;
                                num3 &= 15;
                                while (bitk < num3)
                                {
                                    availableBytesIn--;
                                    bitb |= (z.InputBuffer[nextIn++] & 0xff) << bitk;
                                    bitk += 8;
                                }
                                int num13 = numArray[index + 2] + (bitb & inflate_mask[num3]);
                                bitb = bitb >> num3;
                                bitk -= num3;
                                num9 -= num12;
                                if (write >= num13)
                                {
                                    num14 = write - num13;
                                    if (((write - num14) > 0) && (2 > (write - num14)))
                                    {
                                        s.window[write++] = s.window[num14++];
                                        s.window[write++] = s.window[num14++];
                                        num12 -= 2;
                                    }
                                    else
                                    {
                                        Array.Copy(s.window, num14, s.window, write, 2);
                                        write += 2;
                                        num14 += 2;
                                        num12 -= 2;
                                    }
                                }
                                else
                                {
                                    num14 = write - num13;
                                    do
                                    {
                                        num14 += s.end;
                                    }
                                    while (num14 < 0);
                                    num3 = s.end - num14;
                                    if (num12 > num3)
                                    {
                                        num12 -= num3;
                                        if (((write - num14) > 0) && (num3 > (write - num14)))
                                        {
                                            do
                                            {
                                                s.window[write++] = s.window[num14++];
                                            }
                                            while (--num3 != 0);
                                        }
                                        else
                                        {
                                            Array.Copy(s.window, num14, s.window, write, num3);
                                            write += num3;
                                            num14 += num3;
                                            num3 = 0;
                                        }
                                        num14 = 0;
                                    }
                                }
                                if (((write - num14) > 0) && (num12 > (write - num14)))
                                {
                                    do
                                    {
                                        s.window[write++] = s.window[num14++];
                                    }
                                    while (--num12 != 0);
                                }
                                else
                                {
                                    Array.Copy(s.window, num14, s.window, write, num12);
                                    write += num12;
                                    num14 += num12;
                                    num12 = 0;
                                }
                                goto Label_06B8;
                            }
                            if ((num3 & 0x40) == 0)
                            {
                                num += numArray[index + 2];
                                num += bitb & inflate_mask[num3];
                                index = (num2 + num) * 3;
                                num3 = numArray[index];
                            }
                            else
                            {
                                z.Message = "invalid distance code";
                                num12 = z.AvailableBytesIn - availableBytesIn;
                                num12 = ((bitk >> 3) < num12) ? (bitk >> 3) : num12;
                                availableBytesIn += num12;
                                nextIn -= num12;
                                bitk -= num12 << 3;
                                s.bitb = bitb;
                                s.bitk = bitk;
                                z.AvailableBytesIn = availableBytesIn;
                                z.TotalBytesIn += nextIn - z.NextIn;
                                z.NextIn = nextIn;
                                s.write = write;
                                return -3;
                            }
                            flag = true;
                        }
                    }
                    if ((num3 & 0x40) == 0)
                    {
                        num += numArray[index + 2];
                        num += bitb & inflate_mask[num3];
                        index = (num2 + num) * 3;
                        num3 = numArray[index];
                        if (num3 == 0)
                        {
                            bitb = bitb >> numArray[index + 1];
                            bitk -= numArray[index + 1];
                            s.window[write++] = (byte) numArray[index + 2];
                            num9--;
                            break;
                        }
                    }
                    else
                    {
                        if ((num3 & 0x20) != 0)
                        {
                            num12 = z.AvailableBytesIn - availableBytesIn;
                            num12 = ((bitk >> 3) < num12) ? (bitk >> 3) : num12;
                            availableBytesIn += num12;
                            nextIn -= num12;
                            bitk -= num12 << 3;
                            s.bitb = bitb;
                            s.bitk = bitk;
                            z.AvailableBytesIn = availableBytesIn;
                            z.TotalBytesIn += nextIn - z.NextIn;
                            z.NextIn = nextIn;
                            s.write = write;
                            return 1;
                        }
                        z.Message = "invalid literal/length code";
                        num12 = z.AvailableBytesIn - availableBytesIn;
                        num12 = ((bitk >> 3) < num12) ? (bitk >> 3) : num12;
                        availableBytesIn += num12;
                        nextIn -= num12;
                        bitk -= num12 << 3;
                        s.bitb = bitb;
                        s.bitk = bitk;
                        z.AvailableBytesIn = availableBytesIn;
                        z.TotalBytesIn += nextIn - z.NextIn;
                        z.NextIn = nextIn;
                        s.write = write;
                        return -3;
                    }
                    flag = true;
                }
            }
        Label_06B8:
            if ((num9 >= 0x102) && (availableBytesIn >= 10))
            {
                goto Label_0096;
            }
            num12 = z.AvailableBytesIn - availableBytesIn;
            num12 = ((bitk >> 3) < num12) ? (bitk >> 3) : num12;
            availableBytesIn += num12;
            nextIn -= num12;
            bitk -= num12 << 3;
            s.bitb = bitb;
            s.bitk = bitk;
            z.AvailableBytesIn = availableBytesIn;
            z.TotalBytesIn += nextIn - z.NextIn;
            z.NextIn = nextIn;
            s.write = write;
            return 0;
        }

        internal void Init(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index)
        {
            this.mode = 0;
            this.lbits = (byte) bl;
            this.dbits = (byte) bd;
            this.ltree = tl;
            this.ltree_index = tl_index;
            this.dtree = td;
            this.dtree_index = td_index;
            this.tree = null;
        }

        internal int Process(InflateBlocks blocks, int r)
        {
            int num;
            int num10;
            bool flag;
            int number = 0;
            int bitk = 0;
            int nextIn = 0;
            ZlibCodec z = blocks._codec;
            nextIn = z.NextIn;
            int availableBytesIn = z.AvailableBytesIn;
            number = blocks.bitb;
            bitk = blocks.bitk;
            int write = blocks.write;
            int num9 = (write < blocks.read) ? ((blocks.read - write) - 1) : (blocks.end - write);
            goto Label_0C3A;
        Label_0196:
            this.need = this.lbits;
            this.tree = this.ltree;
            this.tree_index = this.ltree_index;
            this.mode = 1;
        Label_01C3:
            num = this.need;
            while (bitk < num)
            {
                if (availableBytesIn != 0)
                {
                    r = 0;
                }
                else
                {
                    blocks.bitb = number;
                    blocks.bitk = bitk;
                    z.AvailableBytesIn = availableBytesIn;
                    z.TotalBytesIn += nextIn - z.NextIn;
                    z.NextIn = nextIn;
                    blocks.write = write;
                    return blocks.Flush(r);
                }
                availableBytesIn--;
                number |= (z.InputBuffer[nextIn++] & 0xff) << bitk;
                bitk += 8;
            }
            int index = (this.tree_index + (number & inflate_mask[num])) * 3;
            number = SharedUtils.URShift(number, this.tree[index + 1]);
            bitk -= this.tree[index + 1];
            int num3 = this.tree[index];
            if (num3 == 0)
            {
                this.lit = this.tree[index + 2];
                this.mode = 6;
                goto Label_0C3A;
            }
            if ((num3 & 0x10) != 0)
            {
                this.get_Renamed = num3 & 15;
                this.len = this.tree[index + 2];
                this.mode = 2;
                goto Label_0C3A;
            }
            if ((num3 & 0x40) == 0)
            {
                this.need = num3;
                this.tree_index = (index / 3) + this.tree[index + 2];
                goto Label_0C3A;
            }
            if ((num3 & 0x20) != 0)
            {
                this.mode = 7;
                goto Label_0C3A;
            }
            this.mode = 9;
            z.Message = "invalid literal/length code";
            r = -3;
            blocks.bitb = number;
            blocks.bitk = bitk;
            z.AvailableBytesIn = availableBytesIn;
            z.TotalBytesIn += nextIn - z.NextIn;
            z.NextIn = nextIn;
            blocks.write = write;
            return blocks.Flush(r);
        Label_04AE:
            num = this.need;
            while (bitk < num)
            {
                if (availableBytesIn != 0)
                {
                    r = 0;
                }
                else
                {
                    blocks.bitb = number;
                    blocks.bitk = bitk;
                    z.AvailableBytesIn = availableBytesIn;
                    z.TotalBytesIn += nextIn - z.NextIn;
                    z.NextIn = nextIn;
                    blocks.write = write;
                    return blocks.Flush(r);
                }
                availableBytesIn--;
                number |= (z.InputBuffer[nextIn++] & 0xff) << bitk;
                bitk += 8;
            }
            index = (this.tree_index + (number & inflate_mask[num])) * 3;
            number = number >> this.tree[index + 1];
            bitk -= this.tree[index + 1];
            num3 = this.tree[index];
            if ((num3 & 0x10) != 0)
            {
                this.get_Renamed = num3 & 15;
                this.dist = this.tree[index + 2];
                this.mode = 4;
                goto Label_0C3A;
            }
            if ((num3 & 0x40) == 0)
            {
                this.need = num3;
                this.tree_index = (index / 3) + this.tree[index + 2];
                goto Label_0C3A;
            }
            this.mode = 9;
            z.Message = "invalid distance code";
            r = -3;
            blocks.bitb = number;
            blocks.bitk = bitk;
            z.AvailableBytesIn = availableBytesIn;
            z.TotalBytesIn += nextIn - z.NextIn;
            z.NextIn = nextIn;
            blocks.write = write;
            return blocks.Flush(r);
        Label_0730:
            num10 = write - this.dist;
            while (num10 < 0)
            {
                num10 += blocks.end;
            }
            while (this.len != 0)
            {
                if (num9 == 0)
                {
                    if ((write == blocks.end) && (blocks.read != 0))
                    {
                        write = 0;
                        num9 = (write < blocks.read) ? ((blocks.read - write) - 1) : (blocks.end - write);
                    }
                    if (num9 == 0)
                    {
                        blocks.write = write;
                        r = blocks.Flush(r);
                        write = blocks.write;
                        num9 = (write < blocks.read) ? ((blocks.read - write) - 1) : (blocks.end - write);
                        if ((write == blocks.end) && (blocks.read != 0))
                        {
                            write = 0;
                            num9 = (write < blocks.read) ? ((blocks.read - write) - 1) : (blocks.end - write);
                        }
                        if (num9 == 0)
                        {
                            blocks.bitb = number;
                            blocks.bitk = bitk;
                            z.AvailableBytesIn = availableBytesIn;
                            z.TotalBytesIn += nextIn - z.NextIn;
                            z.NextIn = nextIn;
                            blocks.write = write;
                            return blocks.Flush(r);
                        }
                    }
                }
                blocks.window[write++] = blocks.window[num10++];
                num9--;
                if (num10 == blocks.end)
                {
                    num10 = 0;
                }
                this.len--;
            }
            this.mode = 0;
            goto Label_0C3A;
        Label_0B44:
            r = 1;
            blocks.bitb = number;
            blocks.bitk = bitk;
            z.AvailableBytesIn = availableBytesIn;
            z.TotalBytesIn += nextIn - z.NextIn;
            z.NextIn = nextIn;
            blocks.write = write;
            return blocks.Flush(r);
        Label_0C3A:
            flag = true;
            switch (this.mode)
            {
                case 0:
                    if ((num9 < 0x102) || (availableBytesIn < 10))
                    {
                        goto Label_0196;
                    }
                    blocks.bitb = number;
                    blocks.bitk = bitk;
                    z.AvailableBytesIn = availableBytesIn;
                    z.TotalBytesIn += nextIn - z.NextIn;
                    z.NextIn = nextIn;
                    blocks.write = write;
                    r = this.InflateFast(this.lbits, this.dbits, this.ltree, this.ltree_index, this.dtree, this.dtree_index, blocks, z);
                    nextIn = z.NextIn;
                    availableBytesIn = z.AvailableBytesIn;
                    number = blocks.bitb;
                    bitk = blocks.bitk;
                    write = blocks.write;
                    num9 = (write < blocks.read) ? ((blocks.read - write) - 1) : (blocks.end - write);
                    if (r == 0)
                    {
                        goto Label_0196;
                    }
                    this.mode = (r == 1) ? 7 : 9;
                    goto Label_0C3A;

                case 1:
                    goto Label_01C3;

                case 2:
                    num = this.get_Renamed;
                    while (bitk < num)
                    {
                        if (availableBytesIn != 0)
                        {
                            r = 0;
                        }
                        else
                        {
                            blocks.bitb = number;
                            blocks.bitk = bitk;
                            z.AvailableBytesIn = availableBytesIn;
                            z.TotalBytesIn += nextIn - z.NextIn;
                            z.NextIn = nextIn;
                            blocks.write = write;
                            return blocks.Flush(r);
                        }
                        availableBytesIn--;
                        number |= (z.InputBuffer[nextIn++] & 0xff) << bitk;
                        bitk += 8;
                    }
                    this.len += number & inflate_mask[num];
                    number = number >> num;
                    bitk -= num;
                    this.need = this.dbits;
                    this.tree = this.dtree;
                    this.tree_index = this.dtree_index;
                    this.mode = 3;
                    goto Label_04AE;

                case 3:
                    goto Label_04AE;

                case 4:
                    num = this.get_Renamed;
                    while (bitk < num)
                    {
                        if (availableBytesIn != 0)
                        {
                            r = 0;
                        }
                        else
                        {
                            blocks.bitb = number;
                            blocks.bitk = bitk;
                            z.AvailableBytesIn = availableBytesIn;
                            z.TotalBytesIn += nextIn - z.NextIn;
                            z.NextIn = nextIn;
                            blocks.write = write;
                            return blocks.Flush(r);
                        }
                        availableBytesIn--;
                        number |= (z.InputBuffer[nextIn++] & 0xff) << bitk;
                        bitk += 8;
                    }
                    this.dist += number & inflate_mask[num];
                    number = number >> num;
                    bitk -= num;
                    this.mode = 5;
                    goto Label_0730;

                case 5:
                    goto Label_0730;

                case 6:
                    if (num9 == 0)
                    {
                        if ((write == blocks.end) && (blocks.read != 0))
                        {
                            write = 0;
                            num9 = (write < blocks.read) ? ((blocks.read - write) - 1) : (blocks.end - write);
                        }
                        if (num9 == 0)
                        {
                            blocks.write = write;
                            r = blocks.Flush(r);
                            write = blocks.write;
                            num9 = (write < blocks.read) ? ((blocks.read - write) - 1) : (blocks.end - write);
                            if ((write == blocks.end) && (blocks.read != 0))
                            {
                                write = 0;
                                num9 = (write < blocks.read) ? ((blocks.read - write) - 1) : (blocks.end - write);
                            }
                            if (num9 == 0)
                            {
                                blocks.bitb = number;
                                blocks.bitk = bitk;
                                z.AvailableBytesIn = availableBytesIn;
                                z.TotalBytesIn += nextIn - z.NextIn;
                                z.NextIn = nextIn;
                                blocks.write = write;
                                return blocks.Flush(r);
                            }
                        }
                    }
                    r = 0;
                    blocks.window[write++] = (byte) this.lit;
                    num9--;
                    this.mode = 0;
                    goto Label_0C3A;

                case 7:
                    if (bitk > 7)
                    {
                        bitk -= 8;
                        availableBytesIn++;
                        nextIn--;
                    }
                    blocks.write = write;
                    r = blocks.Flush(r);
                    write = blocks.write;
                    num9 = (write < blocks.read) ? ((blocks.read - write) - 1) : (blocks.end - write);
                    if (blocks.read != blocks.write)
                    {
                        blocks.bitb = number;
                        blocks.bitk = bitk;
                        z.AvailableBytesIn = availableBytesIn;
                        z.TotalBytesIn += nextIn - z.NextIn;
                        z.NextIn = nextIn;
                        blocks.write = write;
                        return blocks.Flush(r);
                    }
                    this.mode = 8;
                    goto Label_0B44;

                case 8:
                    goto Label_0B44;

                case 9:
                    r = -3;
                    blocks.bitb = number;
                    blocks.bitk = bitk;
                    z.AvailableBytesIn = availableBytesIn;
                    z.TotalBytesIn += nextIn - z.NextIn;
                    z.NextIn = nextIn;
                    blocks.write = write;
                    return blocks.Flush(r);
            }
            r = -2;
            blocks.bitb = number;
            blocks.bitk = bitk;
            z.AvailableBytesIn = availableBytesIn;
            z.TotalBytesIn += nextIn - z.NextIn;
            z.NextIn = nextIn;
            blocks.write = write;
            return blocks.Flush(r);
        }
    }
}

