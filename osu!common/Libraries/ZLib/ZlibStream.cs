namespace Ionic.Zlib
{
    using System;
    using System.IO;
    using System.Text;

    public class ZlibStream : Stream
    {
        internal ZlibBaseStream _baseStream;
        private bool _disposed;

        public ZlibStream(Stream stream, CompressionMode mode) : this(stream, mode, CompressionLevel.Default, false)
        {
        }

        public ZlibStream(Stream stream, CompressionMode mode, CompressionLevel level) : this(stream, mode, level, false)
        {
        }

        public ZlibStream(Stream stream, CompressionMode mode, bool leaveOpen) : this(stream, mode, CompressionLevel.Default, leaveOpen)
        {
        }

        public ZlibStream(Stream stream, CompressionMode mode, CompressionLevel level, bool leaveOpen)
        {
            this._baseStream = new ZlibBaseStream(stream, mode, level, ZlibStreamFlavor.ZLIB, leaveOpen);
        }

        public static byte[] CompressBuffer(byte[] b)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (Stream stream2 = new ZlibStream(stream, CompressionMode.Compress, CompressionLevel.BestCompression))
                {
                    stream2.Write(b, 0, b.Length);
                }
                return stream.ToArray();
            }
        }

        public static byte[] CompressString(string s)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(s);
            using (MemoryStream stream = new MemoryStream())
            {
                using (Stream stream2 = new ZlibStream(stream, CompressionMode.Compress, CompressionLevel.BestCompression))
                {
                    stream2.Write(bytes, 0, bytes.Length);
                }
                return stream.ToArray();
            }
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!this._disposed)
                {
                    if (disposing && (this._baseStream != null))
                    {
                        this._baseStream.Close();
                    }
                    this._disposed = true;
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        public override void Flush()
        {
            if (this._disposed)
            {
                throw new ObjectDisposedException("ZlibStream");
            }
            this._baseStream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (this._disposed)
            {
                throw new ObjectDisposedException("ZlibStream");
            }
            return this._baseStream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public static byte[] UncompressBuffer(byte[] compressed)
        {
            byte[] buffer2;
            byte[] buffer = new byte[0x400];
            using (MemoryStream stream = new MemoryStream())
            {
                using (MemoryStream stream2 = new MemoryStream(compressed))
                {
                    using (Stream stream3 = new ZlibStream(stream2, CompressionMode.Decompress))
                    {
                        int num;
                        while ((num = stream3.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            stream.Write(buffer, 0, num);
                        }
                    }
                    buffer2 = stream.ToArray();
                }
            }
            return buffer2;
        }

        public static string UncompressString(byte[] compressed)
        {
            string str;
            byte[] buffer = new byte[0x400];
            Encoding encoding = Encoding.UTF8;
            using (MemoryStream stream = new MemoryStream())
            {
                using (MemoryStream stream2 = new MemoryStream(compressed))
                {
                    using (Stream stream3 = new ZlibStream(stream2, CompressionMode.Decompress))
                    {
                        int num;
                        while ((num = stream3.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            stream.Write(buffer, 0, num);
                        }
                    }
                    stream.Seek(0L, SeekOrigin.Begin);
                    str = new StreamReader(stream, encoding).ReadToEnd();
                }
            }
            return str;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (this._disposed)
            {
                throw new ObjectDisposedException("ZlibStream");
            }
            this._baseStream.Write(buffer, offset, count);
        }

        public int BufferSize
        {
            get
            {
                return this._baseStream._bufferSize;
            }
            set
            {
                if (this._disposed)
                {
                    throw new ObjectDisposedException("ZlibStream");
                }
                if (this._baseStream._workingBuffer != null)
                {
                    throw new ZlibException("The working buffer is already set.");
                }
                if (value < 0x80)
                {
                    throw new ZlibException(string.Format("Don't be silly. {0} bytes?? Use a bigger buffer.", value));
                }
                this._baseStream._bufferSize = value;
            }
        }

        public override bool CanRead
        {
            get
            {
                if (this._disposed)
                {
                    throw new ObjectDisposedException("ZlibStream");
                }
                return this._baseStream._stream.CanRead;
            }
        }

        public override bool CanSeek
        {
            get
            {
                return false;
            }
        }

        public override bool CanWrite
        {
            get
            {
                if (this._disposed)
                {
                    throw new ObjectDisposedException("ZlibStream");
                }
                return this._baseStream._stream.CanWrite;
            }
        }

        public virtual FlushType FlushMode
        {
            get
            {
                return this._baseStream._flushMode;
            }
            set
            {
                if (this._disposed)
                {
                    throw new ObjectDisposedException("ZlibStream");
                }
                this._baseStream._flushMode = value;
            }
        }

        public override long Length
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override long Position
        {
            get
            {
                if (this._baseStream._streamMode == ZlibBaseStream.StreamMode.Writer)
                {
                    return this._baseStream._z.TotalBytesOut;
                }
                if (this._baseStream._streamMode == ZlibBaseStream.StreamMode.Reader)
                {
                    return this._baseStream._z.TotalBytesIn;
                }
                return 0L;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public virtual long TotalIn
        {
            get
            {
                return this._baseStream._z.TotalBytesIn;
            }
        }

        public virtual long TotalOut
        {
            get
            {
                return this._baseStream._z.TotalBytesOut;
            }
        }
    }
}

