namespace Ionic.Zlib
{
    using System;
    using System.IO;
    using System.Text;

    internal class SharedUtils
    {
        public static int ReadInput(TextReader sourceTextReader, byte[] target, int start, int count)
        {
            if (target.Length == 0)
            {
                return 0;
            }
            char[] buffer = new char[target.Length];
            int num = sourceTextReader.Read(buffer, start, count);
            if (num == 0)
            {
                return -1;
            }
            for (int i = start; i < (start + num); i++)
            {
                target[i] = (byte) buffer[i];
            }
            return num;
        }

        internal static byte[] ToByteArray(string sourceString)
        {
            return Encoding.UTF8.GetBytes(sourceString);
        }

        internal static char[] ToCharArray(byte[] byteArray)
        {
            return Encoding.UTF8.GetChars(byteArray);
        }

        public static int URShift(int number, int bits)
        {
            return (number >> bits);
        }

        public static long URShift(long number, int bits)
        {
            return (number >> bits);
        }
    }
}

