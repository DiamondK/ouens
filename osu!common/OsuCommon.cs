﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace osu_common
{
    public static class OsuCommon
    {
        public static NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

        public static string PlayModeString(PlayModes mode)
        {
            switch (mode)
            {
                default:
                case PlayModes.Osu:
                    return "osu!";
                case PlayModes.Taiko:
                    return "Taiko";
                case PlayModes.CatchTheBeat:
                    return "Catch the Beat";
                case PlayModes.OsuMania:
                    return "osu!mania";
            }
        }

        public static int ProtocolVersion = 0;
        public static bool UseCompression = false;
    }
}
