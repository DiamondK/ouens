﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using osu;
using osu_common.Helpers;
using System.Text.RegularExpressions;

namespace osu_common
{
    [TestClass]
    public class LinkFormatterTests
    {
        [TestMethod]
        public void TestBareLink()
        {
            LinkFormatterResult result = LinkFormatter.Format("This is a http://www.basic-link.com/?test=test.");

            Assert.AreEqual("This is a http://www.basic-link.com/?test=test.", result.Text);
            Assert.AreEqual(1, result.Links.Count);
            Assert.AreEqual("http://www.basic-link.com/?test=test", result.Links[0].Url);
            Assert.AreEqual(10, result.Links[0].Index);
            Assert.AreEqual(36, result.Links[0].Length);
        }

        [TestMethod]
        public void TestMultipleComplexLinks()
        {
            LinkFormatterResult result = LinkFormatter.Format("This is a http://test.io/link#fragment. (see https://twitter.com). Also, This string should not be altered. http://example.com/");

            Assert.AreEqual("This is a http://test.io/link#fragment. (see https://twitter.com). Also, This string should not be altered. http://example.com/", result.Text);
            Assert.AreEqual(3, result.Links.Count);

            Assert.AreEqual("http://test.io/link#fragment", result.Links[0].Url);
            Assert.AreEqual(10, result.Links[0].Index);
            Assert.AreEqual(28, result.Links[0].Length);

            Assert.AreEqual("https://twitter.com", result.Links[1].Url);
            Assert.AreEqual(45, result.Links[1].Index);
            Assert.AreEqual(19, result.Links[1].Length);

            Assert.AreEqual("http://example.com/", result.Links[2].Url);
            Assert.AreEqual(108, result.Links[2].Index);
            Assert.AreEqual(19, result.Links[2].Length);

        }

        [TestMethod]
        public void TestAjaxLinks()
        {
            LinkFormatterResult result = LinkFormatter.Format("https://twitter.com/#!/hashbanglinks");

            Assert.AreEqual("https://twitter.com/#!/hashbanglinks", result.Text);
            Assert.AreEqual(0, result.Links[0].Index);
            Assert.AreEqual(36, result.Links[0].Length);
        }

        [TestMethod]
        public void TestUnixHomeLinks()
        {
            LinkFormatterResult result = LinkFormatter.Format("http://www.chiark.greenend.org.uk/~sgtatham/putty/");

            Assert.AreEqual("http://www.chiark.greenend.org.uk/~sgtatham/putty/", result.Text);
            Assert.AreEqual(0, result.Links[0].Index);
            Assert.AreEqual(50, result.Links[0].Length);
        }

        [TestMethod]
        public void TestCaseInsensitiveLinks()
        {
            LinkFormatterResult result = LinkFormatter.Format("look: http://puu.sh/7Ggh8xcC6/asf0asd9876.NEF");

            Assert.AreEqual("look: http://puu.sh/7Ggh8xcC6/asf0asd9876.NEF", result.Text);
            Assert.AreEqual(6, result.Links[0].Index);
            Assert.AreEqual(39, result.Links[0].Length);

        }

        [TestMethod]
        public void TestWikiLink()
        {
            LinkFormatterResult result = LinkFormatter.Format("This is a [[Wiki Link]].");

            Assert.AreEqual("This is a wiki:Wiki Link.", result.Text);
            Assert.AreEqual(1, result.Links.Count);
            Assert.AreEqual("http://osu.ppy.sh/wiki/Wiki Link", result.Links[0].Url);
            Assert.AreEqual(10, result.Links[0].Index);
            Assert.AreEqual(14, result.Links[0].Length);
        }

        [TestMethod]
        public void TestMultiWikiLink()
        {
            LinkFormatterResult result = LinkFormatter.Format("This is a [[Wiki Link]] [[Wiki:Link]][[Wiki.Link]].");

            Assert.AreEqual("This is a wiki:Wiki Link wiki:Wiki:Linkwiki:Wiki.Link.", result.Text);
            Assert.AreEqual(3, result.Links.Count);
            Assert.AreEqual("http://osu.ppy.sh/wiki/Wiki Link", result.Links[0].Url);
            Assert.AreEqual(10, result.Links[0].Index);
            Assert.AreEqual(14, result.Links[0].Length);

            Assert.AreEqual("http://osu.ppy.sh/wiki/Wiki:Link", result.Links[1].Url);
            Assert.AreEqual(25, result.Links[1].Index);
            Assert.AreEqual(14, result.Links[1].Length);

            Assert.AreEqual("http://osu.ppy.sh/wiki/Wiki.Link", result.Links[2].Url);
            Assert.AreEqual(39, result.Links[2].Index);
            Assert.AreEqual(14, result.Links[2].Length);
        }

        [TestMethod]
        public void TestOldFormatLink()
        {
            LinkFormatterResult result = LinkFormatter.Format("This is a (simple test)[http://osu.ppy.sh].");

            Assert.AreEqual("This is a simple test.", result.Text);
            Assert.AreEqual(1, result.Links.Count);
            Assert.AreEqual("http://osu.ppy.sh", result.Links[0].Url);
            Assert.AreEqual(10, result.Links[0].Index);
            Assert.AreEqual(11, result.Links[0].Length);
        }

        [TestMethod]
        public void TestNewFormatLink()
        {
            LinkFormatterResult result = LinkFormatter.Format("This is a [http://osu.ppy.sh simple test].");

            Assert.AreEqual("This is a simple test.", result.Text);
            Assert.AreEqual(1, result.Links.Count);
            Assert.AreEqual("http://osu.ppy.sh", result.Links[0].Url);
            Assert.AreEqual(10, result.Links[0].Index);
            Assert.AreEqual(11, result.Links[0].Length);
        }

        [TestMethod]
        public void TestRecursiveBreaking()
        {
            LinkFormatterResult result = LinkFormatter.Format("This is a [http://osu.ppy.sh [[simple test]]].");

            Assert.AreEqual("This is a [[simple test]].", result.Text);
            Assert.AreEqual(1, result.Links.Count);
            Assert.AreEqual("http://osu.ppy.sh", result.Links[0].Url);
            Assert.AreEqual(10, result.Links[0].Index);
            Assert.AreEqual(15, result.Links[0].Length);
        }

        [TestMethod]
        public void TestStartOffset()
        {
            LinkFormatterResult result = LinkFormatter.Format("username[[test]] nothing to see here.", 15);
            Assert.AreEqual(0, result.Links.Count);

            result = LinkFormatter.Format("username[http://test nothing] to [[see]] here.", 21);
            Assert.AreEqual(1, result.Links.Count);
        }

        [TestMethod]
        public void TestLinkComplex()
        {


            LinkFormatterResult result = LinkFormatter.Format("This is a [http://www.simple-test.com simple test] with some [traps] and [[wiki links]]. Don't forget to visit http://osu.ppy.sh (now!)[http://google.com]\uD83D\uDE12");

            Assert.AreEqual("This is a simple test with some [traps] and wiki:wiki links. Don't forget to visit http://osu.ppy.sh now!\0\0\0", result.Text);
            Assert.AreEqual(5, result.Links.Count);

            Link f = result.Links.Find(l => l.Url == "http://osu.ppy.sh/wiki/wiki links");
            Assert.AreEqual(44, f.Index);
            Assert.AreEqual(15, f.Length);

            f = result.Links.Find(l => l.Url == "http://www.simple-test.com");
            Assert.AreEqual(10, f.Index);
            Assert.AreEqual(11, f.Length);

            f = result.Links.Find(l => l.Url == "http://google.com");
            Assert.AreEqual(101, f.Index);
            Assert.AreEqual(4, f.Length);

            f = result.Links.Find(l => l.Url == "http://osu.ppy.sh");
            Assert.AreEqual(83, f.Index);
            Assert.AreEqual(17, f.Length);

            f = result.Links.Find(l => l.Url == "\uD83D\uDE12");
            Assert.AreEqual(105, f.Index);
            Assert.AreEqual(3, f.Length);
        }

        [TestMethod]
        public void TestEmoji()
        {
            LinkFormatterResult result = LinkFormatter.Format("Hello world\uD83D\uDE12<--This is an emoji,There are more:\uD83D\uDE10\uD83D\uDE00,\uD83D\uDE20");
            Assert.AreEqual("Hello world\0\0\0<--This is an emoji,There are more:\0\0\0\0\0\0,\0\0\0", result.Text);
            Assert.AreEqual(result.Links.Count, 4);
            Assert.AreEqual(result.Links[0].Index, 11);
            Assert.AreEqual(result.Links[1].Index, 49);
            Assert.AreEqual(result.Links[2].Index, 52);
            Assert.AreEqual(result.Links[3].Index, 56);
            Assert.AreEqual(result.Links[0].Url, "\uD83D\uDE12");
            Assert.AreEqual(result.Links[1].Url, "\uD83D\uDE10");
            Assert.AreEqual(result.Links[2].Url, "\uD83D\uDE00");
            Assert.AreEqual(result.Links[3].Url, "\uD83D\uDE20");
        }

        [TestMethod]
        public void TestSearchReg()
        {
            Regex reg = new Regex(@"(\w*)([\>|\<|\=]\=?)(\d+)");
            string test1 = "od>8";
            string test3 = "AR>=9";
            Match m1 = reg.Match(test1);
            Assert.AreEqual(m1.Groups.Count, 4);
            Assert.AreEqual(m1.Groups[1].Value, "od");
            Assert.AreEqual(m1.Groups[2].Value, ">");
            Assert.AreEqual(m1.Groups[3].Value, "8");

            m1 = reg.Match(test3);
            Assert.AreEqual(m1.Groups.Count, 4);
            Assert.AreEqual(m1.Groups[1].Value, "AR");
            Assert.AreEqual(m1.Groups[2].Value, ">=");
            Assert.AreEqual(m1.Groups[3].Value, "9");
        }
    }
}
