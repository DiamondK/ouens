﻿using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.Threading;
using osu.GameModes.Play;
using System.Windows.Forms;
using osu.Graphics.Notifications;
using osu.GameModes.Edit;
using osu.Configuration;
using osu_common.Helpers;
using osu.Online;
using osu.Online.Drawable;
using osu.Online.Social;
using osu.Audio;
using osu.GameplayElements.Beatmaps;
using System.Collections.Generic;
using osu.GameModes.Online;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu.Input;
using osu.GameplayElements.Scoring;
using osu.GameModes.Menus;
using osu_common;
using osu.GameModes.Select;
using osu.GameplayElements.Events;

namespace osu.Helpers
{
    public class InterProcessOsu : MarshalByRefObject
    {
        public void HandleArguments(string args)
        {
            if (args.StartsWith("osu://"))
            {
                GameBase.Scheduler.Add(delegate
                {
                    GameBase.BringToFront();
                    ChatEngine.HandleLink(args);
                });
            }
            else
            {
                //file handling.
                OsuModes gs = GameBase.ReceiveFile(args);
                if (gs != OsuModes.Unknown)
                {
                    if (GameBase.IsMinimized && GameBase.Form.Visible)
                    {
                        GameBase.Form.WindowState = FormWindowState.Normal;
                        GameBase.BringToFront();
                    }
                    GameBase.ChangeMode(gs, true);
                }
            }
        }

        public bool WakeUp()
        {
            GameBase.MinimizedToTray = false;
            return true;
        }

        public bool IsPlaying()
        {
            return true; //for now we don't give a fuck if they are playing or not.
        }

        public bool IsInitialised()
        {
            return GameBase.Time > 0;
        }

        public int GetElapsedTime()
        {
            return GameBase.Tournament ? GameBase.Time : 0;
        }

        public void Quit()
        {
            //unregister immediately and quit.
            IPC.Unregister();
            GameBase.ChangeMode(OsuModes.Exit);
        }

        public void SetResolution(int width, int height)
        {
            if (!GameBase.Tournament) return;

            GameBase.Scheduler.Add(delegate { GameBase.SetScreenSize(width, height, ConfigManager.sScreenMode, false); });
        }

        public void SetLocation(int x, int y)
        {
            if (!GameBase.Tournament) return;

            GameBase.Scheduler.Add(delegate
            {
                //if (GameBase.Form.Location.X == x && GameBase.Form.Location.Y == y)
                //    return;
                GameBase.Form.Location = new System.Drawing.Point(x, y);
            });
        }

        public bool IsConnected()
        {
            return BanchoClient.Connected && BanchoClient.InitializationComplete;
        }

        public ClientData GetBulkClientData()
        {
            return new ClientData()
            {
                Buffering = IsBuffering,
                AudioTime = GetCurrentTime(),
                ReplayTime = GetAvailableTime(),
                SpectatingID = GetSpectatingId(),
                Score = GetCurrentScore(),
                Mode = GetCurrentMode(),
                SkipCalculations = GameBase.TournamentSkipCalculations
            };
        }

        public int GetCurrentTime()
        {
            Score replayScore = InputManager.ReplayScore;
            if (!InputManager.ReplayMode || !InputManager.ReplayStreaming || StreamingManager.CurrentlySpectating == null ||
                !GameBase.Tournament || replayScore == null || replayScore.replay == null || replayScore.replay.Count == 0 || Player.Failed)
            {
                return int.MaxValue;
            }

            return AudioEngine.Time;
        }

        public int GetAvailableTime()
        {
            Score replayScore = InputManager.ReplayScore;
            if (!InputManager.ReplayMode || !InputManager.ReplayStreaming || StreamingManager.CurrentlySpectating == null ||
                !GameBase.Tournament || replayScore == null || replayScore.replay == null || replayScore.replay.Count == 0 || Player.Failed)
            {
                return int.MaxValue;
            }

            return replayScore.replay[replayScore.replay.Count - 1].time;
        }

        public bool IsBuffering { get { return GameBase.TournamentBuffering; } }
        
        public void ToggleClientBuffer()
        {
            GameBase.TournamentBuffering = !GameBase.TournamentBuffering;
        }

        public void ToggleSkipCalculations()
        {
            GameBase.TournamentSkipCalculations = !GameBase.TournamentSkipCalculations;
        }

        public void SetMenuTime(int time)
        {
            if (GameBase.FadeState != FadeStates.Idle) return;

            GameBase.Scheduler.Add(delegate
            {
                if (GameBase.Mode == OsuModes.Menu && Math.Abs(AudioEngine.Time - time) > 50)
                    AudioEngine.SeekTo(time);
            });
        }

        public string GetUsername(int userId)
        {
            try
            {
                return BanchoClient.GetUserById(userId).Name;
            }
            catch { }

            return string.Empty;
        }

        public void SetSpectate(int userId)
        {
            if (!GameBase.Tournament || GameBase.FadeState != FadeStates.Idle) return;

            GameBase.Scheduler.Add(delegate
            {
                User found = BanchoClient.GetUserById(userId);
                if (found != null && found == StreamingManager.CurrentlySpectating)
                {
                    if (GameBase.TourneySpectatorName != null) GameBase.TourneySpectatorName.Text = found.Name;
                    return;
                }

                StreamingManager.StopSpectating();
                if (found != null)
                    StreamingManager.StartSpectating(found);
            });
        }

        public void ReloadDatabase()
        {
            GameBase.Scheduler.Add(delegate
            {
                if (!GameBase.Tournament) return;
                if (GameBase.FadeState != FadeStates.Idle) return;

                BeatmapManager.Initialize(true);

                if (GameBase.Mode != OsuModes.Play)
                {
                    StreamingManager.StopSpectating();
                    StreamingManager.StartSpectating(StreamingManager.CurrentlySpectating);
                }
            });
        }

        public bMatch GetMatchData(int matchId = 0)
        {
            if (!GameBase.Tournament) return null;
            if (matchId > 0) BanchoClient.SendRequest(RequestType.Osu_SpecialMatchInfoRequest, new bInt(matchId));
            return IPC.MatchInfo;
        }

        public void SetBeatmap(string checksum)
        {
            if (!GameBase.Tournament) return;
            if (GameBase.FadeState != FadeStates.Idle) return;
            if (GameBase.Mode == OsuModes.Play) return;

            Beatmap b = BeatmapManager.GetBeatmapByChecksum(checksum);

            if (b == null && !GameBase.TournamentManager)
            {
                BeatmapManager.Initialize(true);
                b = BeatmapManager.GetBeatmapByChecksum(checksum);
            }

            if (BeatmapManager.Current == b)
                return;

            if (b != null)
            {
                GameBase.Scheduler.Add(delegate
                {
                    BeatmapManager.Current = b;
                    AudioEngine.LoadAudioForPreview(BeatmapManager.Current, true, true, false);
                });
            }
            else
            {
                OsuDirect.HandlePickup(checksum, delegate
                {
                    BeatmapImport.ForceRun(false);
                    GameBase.Scheduler.Add(delegate { SetBeatmap(checksum); }, true);
                }, null);
            }

        }

        public int GetCurrentScore()
        {
            if (!GameBase.Tournament) return 0;

            Player p = Player.Instance;

            if (p != null && p.Status == PlayerStatus.Busy)
                return 0;

            Score s = Player.currentScore;

            if (s == null) return 0;

            return Player.Failed ? 0 : s.totalScore;
        }

        public void SetUserDim(int dim)
        {
            if (!GameBase.Tournament) return;

            EventManager.UserDimLevel = dim;
        }

        public OsuModes GetCurrentMode()
        {
            IPC.LastContact = GameBase.Time;
            return GameBase.Mode;
        }

        public int GetSpectatingId()
        {
            User u = StreamingManager.CurrentlySpectating;
            if (u == null) return 0;
            return u.Id;
        }

        public void ChangeMode(OsuModes mode)
        {
            GameBase.Scheduler.Add(delegate
            {
                GameBase.ChangeMode(mode);
            });
        }

        [Serializable]
        public class ClientData
        {
            public bool Buffering;
            public OsuModes Mode;
            public int SpectatingID;
            public int Score;
            public int AudioTime;
            public int ReplayTime;
            public bool OverridePause;
            public bool SkipCalculations;
        }
    }

    [Flags]
    public enum IPCMessage
    {
        ReprocessDatabase = 1 << 0,
        NewMatchData = 1 << 1
    }

    internal static class IPC
    {
        internal static List<IPCMessage> AwaitingMessages = new List<IPCMessage>();

        internal static IpcChannel Channel;

        /// <summary>
        /// Temporary storage area for spectator clients.
        /// </summary>
        internal static bMatch MatchInfo;
        public static int LastContact;

        internal static void AcceptConnections(string channelName = "osu!")
        {
            bool accepted = acceptConnections(channelName);

            if (accepted)
                return;

#if DEBUG
            if (!GameBase.Tournament)
                return;
#endif

            //there's likely already an osu! listening... let's kill it off.
            if (!osuMain.AllowMultipleInstances && killOtherOsu())
            {
                int tryCount = 20;
                while (tryCount-- > 0 && !accepted)
                {
                    Thread.Sleep(200);
                    accepted = acceptConnections(channelName);
                }

                if (!accepted)
                    GameBase.ChangeMode(OsuModes.Exit);
            }


            return;
        }

        private static bool acceptConnections(string channelName)
        {
            try
            {
                Channel = new IpcChannel(channelName);

                ChannelServices.RegisterChannel(Channel, false);
                RemotingConfiguration.RegisterWellKnownServiceType(typeof(InterProcessOsu), "loader", WellKnownObjectMode.Singleton);

                return true;
            }
            catch
            {
                Channel = null;
            }

            return false;
        }

        internal static void Unregister()
        {
            if (Channel != null)
            {
                try
                {
                    ChannelServices.UnregisterChannel(Channel);
                }
                catch { }
                Channel = null;
            }
        }

        private static bool killOtherOsu()
        {
#if !Public
            return false;
#endif

            try
            {
                using (SafeIpcChannel ipcCh = SafeIpcChannel.GetChannel())
                {
                    if (ipcCh == null) return false;

                    InterProcessOsu otherOsu = (InterProcessOsu)Activator.GetObject(typeof(InterProcessOsu), "ipc://osu!/loader");
                    if (otherOsu != null)
                    {
                        otherOsu.Quit();
                        return true;
                    }
                }
            }
            catch { }

            return false;
        }

        /// <summary>
        /// GEt anoter osu! client should it exist.
        /// </summary>
        /// <returns></returns>
        internal static InterProcessOsu GetRunningOsu()
        {
            try
            {
                using (SafeIpcChannel ipcCh = SafeIpcChannel.GetChannel())
                {
                    if (ipcCh == null) return null;

                    InterProcessOsu otherOsu = (InterProcessOsu)Activator.GetObject(typeof(InterProcessOsu), @"ipc://osu!/loader");
                    return otherOsu;
                }
            }
            catch
            {
                return null;
            }
        }

        internal static void HandleArguments(string file)
        {
            try
            {
                using (SafeIpcChannel ipcCh = SafeIpcChannel.GetChannel())
                {
                    if (ipcCh == null) return;

                    InterProcessOsu otherOsu = (InterProcessOsu)Activator.GetObject(typeof(InterProcessOsu), "ipc://osu!/loader");
                    if (otherOsu != null) otherOsu.HandleArguments(file);
                }
            }
            catch { }
        }
    }

    /// <summary>
    /// Implement a self-registering IPC Channel which is usable with a using() statement.
    /// Null channel name for a client-only channel.
    /// </summary>
    class SafeIpcChannel : IpcClientChannel, IDisposable
    {
        public static SafeIpcChannel GetChannel()
        {
            SafeIpcChannel channel = null;
            try
            {
                channel = new SafeIpcChannel();
            }
            catch
            {
                if (channel != null)
                {
                    channel.Dispose();
                    channel = null;
                }
            }

            return channel;
        }

        public SafeIpcChannel()
            : base()
        {
            try
            {
                ChannelServices.RegisterChannel(this, false);
            }
            catch { }
        }

        bool isDisposed;
        public void Dispose()
        {
            if (isDisposed) return;
            isDisposed = true;

            try
            {
                ChannelServices.UnregisterChannel(this);
            }
            catch { }
        }
    }
}