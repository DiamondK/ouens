using System;
using System.Collections.Generic;
using Microsoft.Win32;

namespace osu
{
    public enum FrameworkVersion
    {
        dotnet30,
        dotnet35,
        dotnet4,
        dotnet45,
    }

    public static class FrameworkDetection
    {
        static List<FrameworkVersion> available;
        public static List<FrameworkVersion> GetVersions()
        {
            if (available == null)
            {
                available = new List<FrameworkVersion>();

                using (RegistryKey ndpKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP\"))
                {
                    foreach (string versionKeyName in ndpKey.GetSubKeyNames())
                    {
                        if (versionKeyName.StartsWith("v"))
                        {
                            RegistryKey versionKey = ndpKey.OpenSubKey(versionKeyName);
                            string name = (string)versionKey.GetValue("Version", "");
                            string sp = versionKey.GetValue("SP", "").ToString();
                            string install = versionKey.GetValue("Install", "").ToString();
                            if (install != "") //no install info, must be later.
                            {
                                if (sp != "" && install == "1")
                                {
                                    if (versionKeyName.StartsWith("v3.0"))
                                        available.Add(FrameworkVersion.dotnet30);
                                    else if (versionKeyName.StartsWith("v3.5"))
                                        available.Add(FrameworkVersion.dotnet35);
                                }

                                continue;
                            }

                            foreach (string subKeyName in versionKey.GetSubKeyNames())
                            {
                                RegistryKey subKey = versionKey.OpenSubKey(subKeyName);
                                name = (string)subKey.GetValue("Version", "");
                                install = subKey.GetValue("Install", "").ToString();

                                if (install == "1") //no install info, must be later.
                                {
                                    if (name.StartsWith("4.5") || name.StartsWith("4.6"))
                                        available.Add(FrameworkVersion.dotnet45);
                                    else if (name.StartsWith("4"))
                                        available.Add(FrameworkVersion.dotnet4);
                                }
                            }
                        }
                    }
                }
            }

            return available;
        }
    }
}