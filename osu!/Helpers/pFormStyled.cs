﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using osu.Graphics.Sprites;
using osu.Helpers.Forms;

namespace osu_common.Helpers
{
    public class pFormStyled : pForm
    {
        protected override void OnLoad(EventArgs e)
        {
            applyStyling(this);

            base.OnLoad(e);
        }

        private void applyStyling(Control c)
        {
            BackColor = Color.White;

            foreach (Control d in c.Controls)
                applyStyling(d);
        }
    }
}