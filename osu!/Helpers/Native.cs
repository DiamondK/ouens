﻿using osu.Input.Handlers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace osu.Helpers
{
    unsafe internal static partial class Native
    {
        [DllImport(@"kernel32.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        internal static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [DllImport(@"kernel32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr GetModuleHandle(string lpModuleName);

        [DllImport(@"kernel32.dll", SetLastError = true)]
        public static extern bool AllocConsole();

        [DllImport("user32.dll")]
        public static extern bool RegisterTouchWindow(IntPtr hWnd, int flags);

        [DllImport(@"Kernel32.dll")]
        internal static extern ushort GlobalAddAtom(string lpString);

        [DllImport(@"Kernel32.dll")]
        internal static extern ushort GlobalDeleteAtom(ushort nAtom);

        [DllImport(@"user32.dll")]
        internal static extern int SetProp(IntPtr hWnd, string lpString, int hData);

        [DllImport(@"user32.dll")]
        internal static extern int RemoveProp(IntPtr hWnd, string lpString);

        [DllImport("user32.dll")]
        public static extern int GetSystemMetrics(int nIndex);

        [DllImport("user32.dll")]
        public static extern bool RegisterRawInputDevices(
            [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] RawInputDevice[] pRawInputDevices,
            int uiNumDevices,
            int cbSize);

        [DllImport("user32.dll")]
        public static extern bool GetTouchInputInfo(
            IntPtr hTouchInput,
            int uiNumDevices,
            [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1), Out] RawTouchInput[] pRawTouchInputs,
            int cbSize);

        [DllImport("user32.dll")]
        public static extern bool CloseTouchInputHandle(IntPtr hTouchInput);

        [DllImport("user32.dll")]
        public static extern int GetRawInputData(IntPtr hRawInput, RawInputCommand uiCommand, out RawInput pData, ref int pcbSize, int cbSizeHeader);

        [DllImport("user32.dll")]
        public static extern bool GetPointerInfo(int pointerID, out RawPointerInput type);

        unsafe internal static class Helper
        {
            unsafe internal class WndProcHook : NativeWindow
            {
                internal event WndProcDelegate OnWndProc;

                internal WndProcHook(IntPtr handle)
                {
                    AssignHandle(handle);
                }

                internal void Dispose()
                {
                    ReleaseHandle();
                }

                protected override void WndProc(ref Message m)
                {
                    if (OnWndProc != null) OnWndProc(ref m);

                    if (m.Result.ToInt32() < 0)
                    {
                        m.Result = new IntPtr(-m.Result.ToInt32() - 1);
                        return;
                    }

                    base.WndProc(ref m);
                }
            }

            internal static Microsoft.Xna.Framework.Rectangle VirtualScreenRect
            {
                get
                {
                    return new Microsoft.Xna.Framework.Rectangle(
                        GetSystemMetrics(SM_XVIRTUALSCREEN),
                        GetSystemMetrics(SM_YVIRTUALSCREEN),
                        GetSystemMetrics(SM_CXVIRTUALSCREEN),
                        GetSystemMetrics(SM_CYVIRTUALSCREEN));
                }
            }
        }

        internal const int SM_XVIRTUALSCREEN = 76;
        internal const int SM_YVIRTUALSCREEN = 77;

        internal const int SM_CXVIRTUALSCREEN = 78;
        internal const int SM_CYVIRTUALSCREEN = 79;

        internal const int WM_NCPOINTERUPDATE = 0x0241;
        internal const int WM_NCPOINTERDOWN = 0x0242;
        internal const int WM_NCPOINTERUP = 0x0243;
        internal const int WM_POINTERUPDATE = 0x0245;
        internal const int WM_POINTERDOWN = 0x0246;
        internal const int WM_POINTERUP = 0x0247;
        internal const int WM_POINTERENTER = 0x0249;
        internal const int WM_POINTERLEAVE = 0x024A;
        internal const int WM_POINTERACTIVATE = 0x024B;
        internal const int WM_POINTERCAPTURECHANGED = 0x024C;
        internal const int WM_POINTERWHEEL = 0x024E;
        internal const int WM_POINTERHWHEEL = 0x024F;

        internal const int WM_INPUT = 0x00FF;
        internal const int WM_TOUCH = 0x0240;

        internal const int TWF_FINETOUCH = 0x00000001;
        internal const int TWF_WANTPALM = 0x00000002;

        internal const int TABLET_DISABLE_PRESSANDHOLD = 0x00000001;
        internal const int TABLET_DISABLE_PENTAPFEEDBACK = 0x00000008;
        internal const int TABLET_DISABLE_PENBARRELFEEDBACK = 0x00000010;
        internal const int TABLET_DISABLE_TOUCHUIFORCEON = 0x00000100;
        internal const int TABLET_DISABLE_TOUCHUIFORCEOFF = 0x00000200;
        internal const int TABLET_DISABLE_TOUCHSWITCH = 0x00008000;
        internal const int TABLET_DISABLE_FLICKS = 0x00010000;
        internal const int TABLET_ENABLE_FLICKSONCONTEXT = 0x00020000;
        internal const int TABLET_ENABLE_FLICKLEARNINGMODE = 0x00040000;
        internal const int TABLET_DISABLE_SMOOTHSCROLLING = 0x00080000;
        internal const int TABLET_DISABLE_FLICKFALLBACKKEYS = 0x00100000;
        internal const int TABLET_ENABLE_MULTITOUCHDATA = 0x01000000;
    }
}
