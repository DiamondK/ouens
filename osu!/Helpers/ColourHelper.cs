﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace osu.Helpers
{
    internal static class ColourHelper
    {
        /// <summary>
        /// Returns a lightened version of the colour.
        /// </summary>
        /// <param name="color">Original colour</param>
        /// <param name="amount">Decimal light addition</param>
        /// <returns></returns>
        internal static Color Lighten(Color color, float amount)
        {
            return new Color(
                (byte)Math.Min(255, color.R * (1+amount)),
                (byte)Math.Min(255, color.G * (1+amount)),
                (byte)Math.Min(255, color.B * (1+amount)),
                color.A);
        }

        /// <summary>
        /// Lightens a colour in a way more friendly to dark or strong colours.
        /// </summary>
        internal static Color Lighten2(Color color, float amount)
        {
            amount *= 0.5f;
            return new Color(
                (byte)Math.Min(255, color.R * (1 + 0.5f * amount) + 255.0f * amount),
                (byte)Math.Min(255, color.G * (1 + 0.5f * amount) + 255.0f * amount),
                (byte)Math.Min(255, color.B * (1 + 0.5f * amount) + 255.0f * amount),
                color.A);
        }

        /// <summary>
        /// Returns a darkened version of the colour.
        /// </summary>
        /// <param name="color">Original colour</param>
        /// <param name="amount">Percentage light reduction</param>
        /// <returns></returns>
        internal static Color Darken(Color color, float amount)
        {
            return new Color(
                (byte)Math.Min(255, color.R / (1+amount)),
                (byte)Math.Min(255, color.G / (1+amount)),
                (byte)Math.Min(255, color.B / (1+amount)),
                color.A);
        }

        /// <summary>
        /// Hurr derp
        /// </summary>
        internal static Color ColourLerp(Color first, Color second, float weight)
        {
            return new Color((byte)OsuMathHelper.Lerp((float)first.R, (float)second.R, weight),
                             (byte)OsuMathHelper.Lerp((float)first.G, (float)second.G, weight),
                             (byte)OsuMathHelper.Lerp((float)first.B, (float)second.B, weight),
                             (byte)OsuMathHelper.Lerp((float)first.A, (float)second.A, weight));
        }

        /// <summary>
        /// Hurr derp
        /// </summary>
        internal static System.Drawing.Color ColourLerp(System.Drawing.Color first, System.Drawing.Color second, float weight)
        {
            return System.Drawing.Color.FromArgb((byte)OsuMathHelper.Lerp((float)first.A, (float)second.A, weight),
                                                 (byte)OsuMathHelper.Lerp((float)first.R, (float)second.R, weight),
                                                 (byte)OsuMathHelper.Lerp((float)first.G, (float)second.G, weight),
                                                 (byte)OsuMathHelper.Lerp((float)first.B, (float)second.B, weight));
        }

        internal static string Color2Hex(Color color)
        {
            return string.Format("{0:X2}{1:X2}{2:X2}", color.R, color.G, color.B);
        }

        internal static Color Hex2Color(string color)
        {
            if (color.Length != 6)
                return Color.White;
            string r = color.Substring(0, 2);
            string g = color.Substring(2, 2);
            string b = color.Substring(4, 2);
            return new Color(Convert.ToByte(r, 16), Convert.ToByte(g, 16), Convert.ToByte(b, 16));
        }

        internal static Color ChangeAlpha(Color c, byte alphaByte)
        {
            return new Color(c.R, c.G, c.B, alphaByte);
        }
    }
}
