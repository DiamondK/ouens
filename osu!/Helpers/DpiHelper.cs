﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace osu.Helpers
{
    internal static class DpiHelper
    {
        private static float dpi = 0;

        internal static float DPI(Control c, bool force = false)
        {
            if (!force && dpi > 0) return dpi;

            using (System.Drawing.Graphics g = c.CreateGraphics())
            {
                return dpi = g.DpiX;
            }
        }

    }
}
