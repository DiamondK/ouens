﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Graphics.Sprites;
using Color = Microsoft.Xna.Framework.Graphics.Color;
using System.Diagnostics;

namespace osu.Helpers
{
    class OsuMathHelper
    {
        public const float E = 2.71828f;
        public const float Log10E = 0.434294f;
        public const float Log2E = 1.4427f;
        public const float Pi = 3.14159f;
        public const float PiOver2 = 1.5708f;
        public const float PiOver4 = 0.785398f;
        public const float TwoPi = 6.28319f;

        internal static float Lerp(float value1, float value2, float amount)
        {
            return value1 + (value2 - value1) * amount;
        }

        internal static int Clamp(int value, int min, int max)
        {
            Debug.Assert(max >= min);

            if (value > max)
            {
                return max;
            }

            if (value < min)
            {
                return min;
            }

            return value;
        }

        internal static float Clamp(float value, float min, float max)
        {
            if (Single.IsNaN(min) || Single.IsNaN(max))
            {
                return Single.NaN;
            }

            Debug.Assert(max >= min);

            if (value > max)
            {
                return max;
            }

            if (value < min)
            {
                return min;
            }

            return value;
        }

        internal static List<Vector2> CreateBezier(List<Vector2> input)
        {
            int count = input.Count;

            Vector2[] working = new Vector2[count];
            List<Vector2> output = new List<Vector2>();

            int points = General.SLIDER_DETAIL_LEVEL * count;

            for (int iteration = 0; iteration <= points; iteration++)
            {
                for (int i = 0; i < count; i++)
                    working[i] = input[i];

                for (int level = 0; level < count; level++)
                    for (int i = 0; i < count - level - 1; i++)
                        Vector2.Lerp(ref working[i], ref working[i + 1], (float)iteration / points, out working[i]);
                output.Add(working[0]);
            }

            return output;
        }

        internal static List<Vector2> CreateBezierWrong(List<Vector2> input)
        {
            int count = input.Count;

            Vector2[] working = new Vector2[count];
            List<Vector2> output = new List<Vector2>();

            int points = General.SLIDER_DETAIL_LEVEL * count;

            for (int iteration = 0; iteration < points; iteration++)
            {
                for (int i = 0; i < count; i++)
                    working[i] = input[i];

                for (int level = 0; level < count; level++)
                    for (int i = 0; i < count - level - 1; i++)
                        Vector2.Lerp(ref working[i], ref working[i + 1], (float)iteration / points, out working[i]);
                output.Add(working[0]);
            }

            return output;
        }

        internal static Color CConvert(System.Drawing.Color c)
        {
            return new Color(c.R, c.G, c.B, c.A);
        }

        internal static System.Drawing.Color CConvert(Color c)
        {
            return System.Drawing.Color.FromArgb(c.A, c.R, c.G, c.B);
        }

        internal static Color CMix(Color c1, Color c2)
        {
            return new Color((byte)(c1.R * c2.R / 255), (byte)(c1.G * c2.G / 255),
                                               (byte)(c1.B * c2.B / 255), (byte)(c1.A * c2.A / 255));
        }

        internal static float[] MatrixToFloats(Matrix m)
        {
            return new float[16]{m.M11, m.M12, m.M13, m.M14,
                                 m.M21, m.M22, m.M23, m.M24,
                                 m.M31, m.M32, m.M33, m.M34,
                                 m.M41, m.M42, m.M43, m.M44};
        }

        internal static void CircleThroughPoints(Vector2 A, Vector2 B, Vector2 C,
            out Vector2 centre, out float radius, out double t_initial, out double t_final)
        {
            // Circle through 3 points
            // http://en.wikipedia.org/wiki/Circumscribed_circle#Cartesian_coordinates
            float D = 2 * (A.X * (B.Y - C.Y) + B.X * (C.Y - A.Y) + C.X * (A.Y - B.Y));
            float AMagSq = A.LengthSquared();
            float BMagSq = B.LengthSquared();
            float CMagSq = C.LengthSquared();
            centre = new Vector2(
                (AMagSq * (B.Y - C.Y) + BMagSq * (C.Y - A.Y) + CMagSq * (A.Y - B.Y)) / D,
                (AMagSq * (C.X - B.X) + BMagSq * (A.X - C.X) + CMagSq * (B.X - A.X)) / D);
            radius = Vector2.Distance(centre, A);

            t_initial = CircleTAt(A, centre);
            double t_mid = CircleTAt(B, centre);
            t_final = CircleTAt(C, centre);

            while (t_mid < t_initial) t_mid += 2 * Pi;
            while (t_final < t_initial) t_final += 2 * Pi;
            if (t_mid > t_final)
            {
                t_final -= 2 * Pi;
            }
        }

        internal static double CircleTAt(Vector2 pt, Vector2 centre)
        {
            return Math.Atan2(pt.Y - centre.Y, pt.X - centre.X);
        }

        internal static Vector2 CirclePoint(Vector2 centre, float radius, double t)
        {
            return new Vector2((float)(Math.Cos(t) * radius), (float)(Math.Sin(t) * radius)) + centre;
        }

        /// <summary>
        /// deprecated. use ApplyEasing
        /// </summary>
        internal static float easeOutVal(float currTime, float start, float end, float duration)
        {
            return (float)ApplyEasing(EasingTypes.OutQuad, currTime, start, end - start, duration);
        }

        /// <summary>
        /// deprecated. use ApplyEasing
        /// </summary>
        internal static float easeInVal(float currTime, float start, float end, float duration)
        {
            return (float)ApplyEasing(EasingTypes.InQuad, currTime, start, end - start, duration);
        }

        /// <summary>
        /// deprecated. use ApplyEasing
        /// </summary>
        internal static float easeNoneVal(float currTime, float start, float end, int duration)
        {
            return Lerp(start, end, currTime / duration);
        }

        public static Color TweenValues(Color startColour, Color endColour, int runningTime, int startTime, int endTime,
                                         EasingTypes easing)
        {
            if (startColour == endColour)
                return startColour;

            int current = runningTime - startTime;
            int duration = endTime - startTime;

            if (duration == 0 || current == 0)
                return startColour;

            return new Color(
                            (byte)Math.Max(0,Math.Min(255,ApplyEasing(easing, current, startColour.R, endColour.R - startColour.R, duration))),
                            (byte)Math.Max(0,Math.Min(255,ApplyEasing(easing, current, startColour.G, endColour.G - startColour.G, duration))),
                            (byte)Math.Max(0,Math.Min(255,ApplyEasing(easing, current, startColour.B, endColour.B - startColour.B, duration))),
                            (byte)Math.Max(0,Math.Min(255,ApplyEasing(easing, current, startColour.A, endColour.A - startColour.A, duration))));
        }

        public static float TweenValues(float val1, float val2, int runningTime, int startTime, int endTime,
                              EasingTypes easing)
        {
            if (val1 == val2)
                return val1;

            int current = runningTime - startTime;
            int duration = endTime - startTime;

            if (current == 0)
                return val1;
            if (duration == 0)
                return val2;

            return (float)ApplyEasing(easing, current, val1, val2 - val1, duration);
        }


        internal static Vector2 TweenValues(Vector2 val1, Vector2 val2, double runningTime, double startTime,
                                            float endTime,
                                            EasingTypes easing)
        {
            float current = (float)(runningTime - startTime);
            float duration = (float)(endTime - startTime);

            if (duration == 0 || current == 0)
                return val1;

            return new Vector2(
                (float)ApplyEasing(easing, current, val1.X, val2.X - val1.X, duration),
                (float)ApplyEasing(easing, current, val1.Y, val2.Y - val1.Y, duration));
        }

        internal static Graphics.Primitives.RectangleF TweenValues(Graphics.Primitives.RectangleF val1, Graphics.Primitives.RectangleF val2,
                                                                   double runningTime, double startTime, float endTime, EasingTypes easing)
        {
            float current = (float)(runningTime - startTime);
            float duration = (float)(endTime - startTime);

            if (duration == 0 || current == 0)
                return val1;

            return new Graphics.Primitives.RectangleF(
                (float)ApplyEasing(easing, current, val1.X, val2.X - val1.X, duration),
                (float)ApplyEasing(easing, current, val1.Y, val2.Y - val1.Y, duration),
                (float)ApplyEasing(easing, current, val1.Width, val2.Width - val1.Width, duration),
                (float)ApplyEasing(easing, current, val1.Height, val2.Height - val1.Height, duration));
        }

        internal static double ApplyEasing(EasingTypes easing, double time, double initial, double change, double duration)
        {
            if (change == 0 || time == 0 || duration == 0) return initial;
            if (time == duration) return initial + change;

            switch (easing)
            {
                default:
                    return change * (time / duration) + initial;
                case EasingTypes.In:
                case EasingTypes.InQuad:
                    return change * (time /= duration) * time + initial;
                case EasingTypes.Out:
                case EasingTypes.OutQuad:
                    return -change * (time /= duration) * (time - 2) + initial;
                case EasingTypes.InOutQuad:
                    if ((time /= duration / 2) < 1) return change / 2 * time * time + initial;
                    return -change / 2 * ((--time) * (time - 2) - 1) + initial;
                case EasingTypes.InCubic:
                    return change * (time /= duration) * time * time + initial;
                case EasingTypes.OutCubic:
                    return change * ((time = time / duration - 1) * time * time + 1) + initial;
                case EasingTypes.InOutCubic:
                    if ((time /= duration / 2) < 1) return change / 2 * time * time * time + initial;
                    return change / 2 * ((time -= 2) * time * time + 2) + initial;
                case EasingTypes.InQuart:
                    return change * (time /= duration) * time * time * time + initial;
                case EasingTypes.OutQuart:
                    return -change * ((time = time / duration - 1) * time * time * time - 1) + initial;
                case EasingTypes.InOutQuart:
                    if ((time /= duration / 2) < 1) return change / 2 * time * time * time * time + initial;
                    return -change / 2 * ((time -= 2) * time * time * time - 2) + initial;
                case EasingTypes.InQuint:
                    return change * (time /= duration) * time * time * time * time + initial;
                case EasingTypes.OutQuint:
                    return change * ((time = time / duration - 1) * time * time * time * time + 1) + initial;
                case EasingTypes.InOutQuint:
                    if ((time /= duration / 2) < 1) return change / 2 * time * time * time * time * time + initial;
                    return change / 2 * ((time -= 2) * time * time * time * time + 2) + initial;
                case EasingTypes.InSine:
                    return -change * Math.Cos(time / duration * (Pi / 2)) + change + initial;
                case EasingTypes.OutSine:
                    return change * Math.Sin(time / duration * (Pi / 2)) + initial;
                case EasingTypes.InOutSine:
                    return -change / 2 * (Math.Cos(Pi * time / duration) - 1) + initial;
                case EasingTypes.InExpo:
                    return change * Math.Pow(2, 10 * (time / duration - 1)) + initial;
                case EasingTypes.OutExpo:
                    return (time == duration) ? initial + change : change * (-Math.Pow(2, -10 * time / duration) + 1) + initial;
                case EasingTypes.InOutExpo:
                    if ((time /= duration / 2) < 1) return change / 2 * Math.Pow(2, 10 * (time - 1)) + initial;
                    return change / 2 * (-Math.Pow(2, -10 * --time) + 2) + initial;
                case EasingTypes.InCirc:
                    return -change * (Math.Sqrt(1 - (time /= duration) * time) - 1) + initial;
                case EasingTypes.OutCirc:
                    return change * Math.Sqrt(1 - (time = time / duration - 1) * time) + initial;
                case EasingTypes.InOutCirc:
                    if ((time /= duration / 2) < 1) return -change / 2 * (Math.Sqrt(1 - time * time) - 1) + initial;
                    return change / 2 * (Math.Sqrt(1 - (time -= 2) * time) + 1) + initial;
                case EasingTypes.InElastic:
                    {
                        if ((time /= duration) == 1) return initial + change;

                        var p = duration * .3;
                        var a = change;
                        var s = 1.70158;
                        if (a < Math.Abs(change)) { a = change; s = p / 4; }
                        else s = p / (2 * Pi) * Math.Asin(change / a);
                        return -(a * Math.Pow(2, 10 * (time -= 1)) * Math.Sin((time * duration - s) * (2 * Pi) / p)) + initial;
                    }
                case EasingTypes.OutElastic:
                    {
                        if ((time /= duration) == 1) return initial + change;

                        var p = duration * .3;
                        var a = change;
                        var s = 1.70158;
                        if (a < Math.Abs(change)) { a = change; s = p / 4; }
                        else s = p / (2 * Pi) * Math.Asin(change / a);
                        return a * Math.Pow(2, -10 * time) * Math.Sin((time * duration - s) * (2 * Pi) / p) + change + initial;
                    }
                case EasingTypes.OutElasticHalf:
                    {
                        if ((time /= duration) == 1) return initial + change;

                        var p = duration * .3;
                        var a = change;
                        var s = 1.70158;
                        if (a < Math.Abs(change)) { a = change; s = p / 4; }
                        else s = p / (2 * Pi) * Math.Asin(change / a);
                        return a * Math.Pow(2, -10 * time) * Math.Sin((0.5f * time * duration - s) * (2 * Pi) / p) + change + initial;
                    }
                case EasingTypes.OutElasticQuarter:
                    {
                        if ((time /= duration) == 1) return initial + change;

                        var p = duration * .3;
                        var a = change;
                        var s = 1.70158;
                        if (a < Math.Abs(change)) { a = change; s = p / 4; }
                        else s = p / (2 * Pi) * Math.Asin(change / a);
                        return a * Math.Pow(2, -10 * time) * Math.Sin((0.25f * time * duration - s) * (2 * Pi) / p) + change + initial;
                    }
                case EasingTypes.InOutElastic:
                    {
                        if ((time /= duration / 2) == 2) return initial + change;

                        var p = duration * (.3 * 1.5);
                        var a = change;
                        var s = 1.70158;
                        if (a < Math.Abs(change)) { a = change; s = p / 4; }
                        else s = p / (2 * Pi) * Math.Asin(change / a);
                        if (time < 1) return -.5 * (a * Math.Pow(2, 10 * (time -= 1)) * Math.Sin((time * duration - s) * (2 * Pi) / p)) + initial;
                        return a * Math.Pow(2, -10 * (time -= 1)) * Math.Sin((time * duration - s) * (2 * Pi) / p) * .5 + change + initial;
                    }
                case EasingTypes.InBack:
                    {
                        var s = 1.70158;
                        return change * (time /= duration) * time * ((s + 1) * time - s) + initial;
                    }
                case EasingTypes.OutBack:
                    {
                        var s = 1.70158;
                        return change * ((time = time / duration - 1) * time * ((s + 1) * time + s) + 1) + initial;
                    }
                case EasingTypes.InOutBack:
                    {
                        var s = 1.70158;
                        if ((time /= duration / 2) < 1) return change / 2 * (time * time * (((s *= (1.525)) + 1) * time - s)) + initial;
                        return change / 2 * ((time -= 2) * time * (((s *= (1.525)) + 1) * time + s) + 2) + initial;
                    }
                case EasingTypes.InBounce:
                    return change - ApplyEasing(EasingTypes.OutBounce, duration - time, 0, change, duration) + initial;
                case EasingTypes.OutBounce:
                    if ((time /= duration) < (1 / 2.75))
                    {
                        return change * (7.5625 * time * time) + initial;
                    }
                    else if (time < (2 / 2.75))
                    {
                        return change * (7.5625 * (time -= (1.5 / 2.75)) * time + .75) + initial;
                    }
                    else if (time < (2.5 / 2.75))
                    {
                        return change * (7.5625 * (time -= (2.25 / 2.75)) * time + .9375) + initial;
                    }
                    else
                    {
                        return change * (7.5625 * (time -= (2.625 / 2.75)) * time + .984375) + initial;
                    }
                case EasingTypes.InOutBounce:
                    if (time < duration / 2) return ApplyEasing(EasingTypes.InBounce, time * 2, 0, change, duration) * .5 + initial;
                    return ApplyEasing(EasingTypes.OutBounce, time * 2 - duration, 0, change, duration) * .5 + change * .5 + initial;
            }
        }
    }
}
