﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace osu.Helpers.Forms
{
    class GlassGroupPanel : Panel
    {
        public GlassGroupPanel() : base()
        {
            this.BackColor = Color.FromArgb(128, 255, 255, 255);
            this.m_corner = osu.Properties.Resources.glasscorner;
        }

        private Bitmap m_corner;

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            System.Drawing.Graphics g = e.Graphics;

            g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
            g.Clear(Color.FromArgb(191, 255, 255, 255));

            int w = this.Width;
            int h = this.Height;

            if ((w < 8) || (h < 8)) return;

            Pen inner = new Pen(Color.FromArgb(223, 255, 255, 255));
            Pen border = new Pen(Color.FromArgb(191, 0, 0, 0));
            Pen outer = new Pen(Color.FromArgb(128, 255, 255, 255));

            g.DrawRectangle(outer, 0, 0, w - 1, h - 1);
            g.DrawRectangle(border, 1, 1, w - 3, h - 3);
            g.DrawRectangle(inner, 2, 2, w - 5, h - 5);

            // four corners
            g.DrawImage(m_corner, new Rectangle(0, 0, 4, 4),
                                  new Rectangle(0, 0, 4, 4), GraphicsUnit.Pixel);

            g.DrawImage(m_corner, new Rectangle(this.Width - 4, 0, 4, 4),
                                  new Rectangle(4, 0, 4, 4), GraphicsUnit.Pixel);

            g.DrawImage(m_corner, new Rectangle(0, this.Height - 4, 4, 4),
                                  new Rectangle(0, 4, 4, 4), GraphicsUnit.Pixel);

            g.DrawImage(m_corner, new Rectangle(this.Width - 4, this.Height - 4, 4, 4),
                                  new Rectangle(4, 4, 4, 4), GraphicsUnit.Pixel);
        }
    }
}
