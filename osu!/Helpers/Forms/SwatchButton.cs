﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace osu.Helpers.Forms
{
    class SwatchButton : Button
    {
        public SwatchButton() : base()
        {
            if (corner == null) corner = osu.Properties.Resources.glassbutton;
            this.MouseEnter += SwatchButton_MouseEnter;
            this.MouseLeave += SwatchButton_MouseLeave;
        }

        private static Bitmap corner;
        private Color m_swatch_colour;
        private bool m_hover = false;

        /// <summary>
        /// Hack around somewhat confounding behaviour with Visible
        /// </summary>
        public bool Visible2 = true;

        public Color SwatchColor
        {
            get
            {
                return m_swatch_colour;
            }
            set
            {
                m_swatch_colour = value;
                Invalidate();
            }
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            base.OnPaintBackground(pevent);
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            const int CORNER_SIZE = 11;
            const int IMAGE_WIDTH = 32;

            this.OnPaintBackground(pevent);
            System.Drawing.Graphics g = pevent.Graphics;

            int w = this.Width;
            int h = this.Height;

            Brush bkg = new SolidBrush(m_swatch_colour);

            if ((w < 2 * CORNER_SIZE) || (h < 2 * CORNER_SIZE))
            {
                g.FillRectangle(bkg, 0, 0, w, h);
                return;
            }

            // rounded rectangle in the swatch's colour
            g.FillRectangle(bkg, 3, 1, w - 6, 1);
            g.FillRectangle(bkg, 2, 2, w - 4, 1);
            g.FillRectangle(bkg, 1, 3, w - 2, h - 6);
            g.FillRectangle(bkg, 2, h - 3, w - 4, 1);
            g.FillRectangle(bkg, 3, h - 2, w - 6, 1);

            // borders. Think "css border image"
            int ct = m_hover ? 2 : 1;

            for (int x = 0; x < ct; x++)
            {
                // corners
                g.DrawImage(corner, new Rectangle(0, 0, CORNER_SIZE, CORNER_SIZE),
                            new Rectangle(0, 0, CORNER_SIZE, CORNER_SIZE), GraphicsUnit.Pixel);

                g.DrawImage(corner, new Rectangle(w - CORNER_SIZE, 0, CORNER_SIZE, CORNER_SIZE),
                            new Rectangle(IMAGE_WIDTH - CORNER_SIZE, 0, CORNER_SIZE, CORNER_SIZE), GraphicsUnit.Pixel);

                g.DrawImage(corner, new Rectangle(0, h - CORNER_SIZE, CORNER_SIZE, CORNER_SIZE),
                            new Rectangle(0, IMAGE_WIDTH - CORNER_SIZE, CORNER_SIZE, CORNER_SIZE), GraphicsUnit.Pixel);

                g.DrawImage(corner, new Rectangle(w - CORNER_SIZE, h - CORNER_SIZE, CORNER_SIZE, CORNER_SIZE),
                            new Rectangle(IMAGE_WIDTH - CORNER_SIZE, IMAGE_WIDTH - CORNER_SIZE, CORNER_SIZE, CORNER_SIZE), GraphicsUnit.Pixel);

                // sides
                g.DrawImage(corner, new Rectangle(CORNER_SIZE, 0, w - 2 * CORNER_SIZE, CORNER_SIZE),
                            new Rectangle(CORNER_SIZE, 0, IMAGE_WIDTH - 2 * CORNER_SIZE, CORNER_SIZE), GraphicsUnit.Pixel);

                g.DrawImage(corner, new Rectangle(0, CORNER_SIZE, CORNER_SIZE, h - 2 * CORNER_SIZE),
                            new Rectangle(0, CORNER_SIZE, CORNER_SIZE, IMAGE_WIDTH - 2 * CORNER_SIZE), GraphicsUnit.Pixel);

                g.DrawImage(corner, new Rectangle(CORNER_SIZE, h - CORNER_SIZE, w - 2 * CORNER_SIZE, CORNER_SIZE),
                            new Rectangle(CORNER_SIZE, IMAGE_WIDTH - CORNER_SIZE, IMAGE_WIDTH - 2 * CORNER_SIZE, CORNER_SIZE), GraphicsUnit.Pixel);

                g.DrawImage(corner, new Rectangle(w - CORNER_SIZE, CORNER_SIZE, CORNER_SIZE, h - 2 * CORNER_SIZE),
                            new Rectangle(IMAGE_WIDTH - CORNER_SIZE, CORNER_SIZE, CORNER_SIZE, IMAGE_WIDTH - 2 * CORNER_SIZE), GraphicsUnit.Pixel);
            }

            Brush fore = new SolidBrush(this.ForeColor);
            Brush shadow = new SolidBrush(Color.FromArgb(191, 0, 0, 0));
            StringFormat fmt = new StringFormat();
            fmt.Alignment = StringAlignment.Center;
            fmt.LineAlignment = StringAlignment.Center;

            Point text_pos = new Point(w / 2, h / 2);
            g.DrawString(this.Text, this.Font, shadow, new Point(text_pos.X, text_pos.Y + 1), fmt);
            g.DrawString(this.Text, this.Font, fore, text_pos, fmt);
        }

        private void SwatchButton_MouseEnter(object sender, EventArgs e)
        {
            m_hover = true;
            Invalidate();
        }

        private void SwatchButton_MouseLeave(object sender, EventArgs e)
        {
            m_hover = false;
            Invalidate();
        }
    }
}
