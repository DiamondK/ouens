﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;

namespace osu.Helpers.Forms
{
    class MonochromeLabel : Label
    {
        public MonochromeLabel()
            : base()
        {
        }

        private StringFormat m_string_format = new StringFormat();
        private SolidBrush m_brush = new SolidBrush(Color.White);

        protected override void OnPaint(PaintEventArgs e)
        {
            if (BackColor.A < 255)
            {
                System.Drawing.Graphics g = e.Graphics;

                Bitmap img = new Bitmap(this.Size.Width, this.Size.Height, PixelFormat.Format32bppArgb);
                System.Drawing.Graphics buf = System.Drawing.Graphics.FromImage(img);

                buf.Clear(Color.Black);
                buf.DrawString(this.Text, this.Font, m_brush, new PointF(0, 0), m_string_format);

                RecolourText(img, this.ForeColor);

                g.DrawImageUnscaled(img, new Point(0, 0));
            }
            else base.OnPaint(e);
        }

        private unsafe void RecolourText(Bitmap img, Color colour)
        {
            Rectangle rect = new Rectangle(0, 0, img.Width, img.Height);
            int width = img.Width * 4;
            int height = img.Height;

            BitmapData bmp_data = img.LockBits(rect, ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            int stride = bmp_data.Stride;
            byte r = colour.R, g = colour.G, b = colour.B;

            byte* data = (byte *)bmp_data.Scan0;
            for (int y = 0; y < height; y++)
            {
                int offset = y * stride;

                for (int x = 0; x < width; x+= 4)
                {
                    byte alpha = data[offset + x + 1]; // alpha from the text's green component: in cleartype this represents proper greyscale AA.
                    data[offset + x] = b;
                    data[offset + x + 1] = g;
                    data[offset + x + 2] = r;
                    data[offset + x + 3] = alpha;
                }
            }

            img.UnlockBits(bmp_data);
        }
    }
}
