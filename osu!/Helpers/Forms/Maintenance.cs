﻿using System;
using System.IO;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Input;
using osu_common.Helpers;
using System.ComponentModel;
using System.Threading;
using osu_common.Updater;
using osu.Configuration;
using System.Windows.Forms;
using osu_common.Libraries.NetLib;
using ICSharpCode.SharpZipLib.Zip;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;

namespace osu.Helpers.Forms
{
    public enum MaintenanceType
    {
        AvatarFix,
        RestoreLocalScoresFromReplays,
        Update,
        Uninstall
    }

    public partial class Maintenance : pForm
    {
        BackgroundWorker worker = new BackgroundWorker();

        private SingleInstance instance;

        static string icon_path_desktop
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), @"osu!.lnk");
            }
        }

        static string icon_path_startmenu
        {
            get
            {
                return Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.StartMenu), @"Programs"), @"osu!.lnk");
            }
        }

        public Maintenance(MaintenanceType maintenanceType)
        {
            InitializeComponent();

            switch (maintenanceType)
            {
                case MaintenanceType.AvatarFix:
                    worker.DoWork += avatarFix;
                    break;
                case MaintenanceType.RestoreLocalScoresFromReplays:
                    worker.DoWork += restoreScores;
                    break;
                case MaintenanceType.Update:
                    if (!osuMain.Started) TopMost = false;
                    ShowInTaskbar = true;

                    Text = @"osu! updater";
                    worker.DoWork += update;
                    break;
                case MaintenanceType.Uninstall:
                    if (!osuMain.Started) TopMost = false;
                    ShowInTaskbar = true;

                    Text = @"osu! uninstaller";
                    worker.DoWork += uninstall;
                    break;
            }

            worker.WorkerReportsProgress = true;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();

            updateTimer.Tick += updateTimer_Tick;
            updateTimer.Interval = 10;
            updateTimer.Start();

        }

        float updateProgress;
        void updateTimer_Tick(object sender, EventArgs e)
        {
            updateProgress += 0.01f;
            Invalidate();
        }

        private System.Windows.Forms.Timer updateTimer = new System.Windows.Forms.Timer();

        Image layer1 = osu.Properties.Resources.updater_grid1;
        Image layer2 = osu.Properties.Resources.updater_grid2;
        Image layer3 = osu.Properties.Resources.updater_grid3;
        Image layer4 = osu.Properties.Resources.updater_grid4;

        protected override void OnPaint(PaintEventArgs e)
        {
            Image fade1;
            Image fade2;

            switch ((int)(updateProgress % 4))
            {
                default:
                    fade1 = layer1;
                    fade2 = layer2;
                    break;
                case 1:
                    fade1 = layer2;
                    fade2 = layer3;
                    break;
                case 2:
                    fade1 = layer3;
                    fade2 = layer4;
                    break;
                case 3:
                    fade1 = layer4;
                    fade2 = layer1;
                    break;
            }

            System.Drawing.Graphics g = e.Graphics;

            float dpiRatio = e.Graphics.DpiX / 96f;

            int width = (int)Math.Ceiling(380 * dpiRatio);
            int height = (int)Math.Ceiling(350 * dpiRatio);

            g.DrawImage(fade1, new Rectangle(0, 0, width, height), 0, 0, 380, 350, GraphicsUnit.Pixel);

            ColorMatrix CMFade = new ColorMatrix();
            ImageAttributes AFade = new ImageAttributes();
            CMFade.Matrix33 = updateProgress % 1;
            AFade.SetColorMatrix(CMFade, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            g.DrawImage(fade2, new Rectangle(0, 0, width, height), 0, 0, 380, 350, GraphicsUnit.Pixel, AFade);

            base.OnPaint(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (updateTimer != null) updateTimer.Dispose();
            base.OnClosing(e);

        }

        void setProgress(float percentage = 0, OsuString idleText = OsuString.Maintenance_Hi)
        {
            Invoke(delegate
            {
                if (percentage <= 0)
                {
                    progressBar1.Value = 0;
                    label1.Text = LocalisationManager.GetString(idleText);
                }
                else
                {
                    progressBar1.Value = (int)percentage;
                    label1.Text = (int)percentage + @"%";
                }
            });
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Close();
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        string installPath = Environment.CurrentDirectory;

        void init()
        {
            try
            {
                SplashScreen.CloseAll();

                BringToFrontEx();

                bool hasLanguage = LocalisationManager.IsInitialised;

                if (!hasLanguage)
                {
                    Invoke(delegate
                    {
                        LocalisationManager.SetLanguage(null, false, delegate { hasLanguage = true; }, osu.Properties.Resources.en);
                    });

                    int timeout = 20000;
                    while (!hasLanguage && (timeout -= 100) > 0)
                    {
                        setText(@"Downloading localisation...", true);
                        Thread.Sleep(100);
                    }
                }

                while (true)
                {
                    try
                    {
                        instance = new SingleInstance(osuMain.ClientGuid, 500);
                    }
                    catch (TooManyInstancesException)
                    {
                        if (osuMain.IsWine)
                            break;

                        setText(LocalisationManager.GetString(OsuString.Maintenance_OsuIsAlreadyRunning), true);
                        Thread.Sleep(100);
                        continue;
                    }

                    break;
                }

                setText();
                setProgress();
            }
            catch
            {

            }

        }

        void uninstall(object sender, DoWorkEventArgs e)
        {
            if (!osuMain.IsElevated)
            {
                osuMain.Elevate(@"-uninstall", false);
                return;
            }

            try
            {
                init();
                setProgress(0, OsuString.Maintenance_Bye);

                int seconds = 10;
                while (seconds > 0)
                {
                    setText(string.Format(LocalisationManager.GetString(OsuString.Maintenance_Uninstall) + '\n' +
                    LocalisationManager.GetString(OsuString.Maintenance_Uninstall2), seconds), true);
                    Thread.Sleep(1000);
                    seconds--;

                    while (installPath == null)
                        Thread.Sleep(100);
                }

                setText(LocalisationManager.GetString(OsuString.Maintenance_Uninstalling), true);

                string[] directories = Directory.GetDirectories(Environment.CurrentDirectory);

                int totalProgressItems = directories.Length + 8;
                int currentProgress = 0;

                //delete subdirectories
                foreach (string dir in directories)
                {
                    setProgress((int)(((float)++currentProgress / totalProgressItems) * 100));
                    try
                    {
                        Directory.Delete(dir, true);
                    }
                    catch
                    {
                    }

                    Thread.Sleep(100);
                }

                //delete main directory
                try
                {
                    setProgress((int)(((float)++currentProgress / totalProgressItems) * 100));
                    Directory.Delete(Environment.CurrentDirectory, true);
                }
                catch
                {
                }

                try
                {
                    setProgress((int)(((float)++currentProgress / totalProgressItems) * 100));
                    foreach (string file in Directory.GetFiles(Environment.CurrentDirectory))
                    {
                        GeneralHelper.FileDeleteOnReboot(file);
                    }
                }
                catch
                {
                }

                try
                {
                    setProgress((int)(((float)++currentProgress / totalProgressItems) * 100));
                    Directory.Delete(Environment.CurrentDirectory);
                }
                catch
                {
                }

                try
                {
                    setProgress((int)(((float)++currentProgress / totalProgressItems) * 100));
                    GeneralHelper.FileDeleteOnReboot(Environment.CurrentDirectory);
                }
                catch
                {

                }

                try
                {
                    setProgress((int)(((float)++currentProgress / totalProgressItems) * 100));
                    Uninstaller.RemoveUninstaller();
                }
                catch
                {

                }

                try
                {
                    setProgress((int)(((float)++currentProgress / totalProgressItems) * 100));
                    File.Delete(icon_path_desktop);
                    File.Delete(icon_path_startmenu);
                }
                catch
                {

                }

                try
                {
                    setProgress((int)(((float)++currentProgress / totalProgressItems) * 100));
                    GeneralHelper.RegistryDelete(@"osu!");
                    GeneralHelper.RegistryDelete(@"osu");
                }
                catch
                {
                }

                setProgress(100);
                setText(LocalisationManager.GetString(OsuString.Maintenance_Farewell), true);
                Thread.Sleep(5000);
            }
            catch { }
        }

        void update(object sender, DoWorkEventArgs e)
        {
            init();

            try
            {
#if Public
                ReleaseStream stream = ReleaseStream.Stable;
#else
                ReleaseStream stream = ReleaseStream.CuttingEdge;
#endif

                bool hasFramework = File.Exists(@"Microsoft.Xna.Framework.dll") && File.Exists(@"x3daudio1_1.dll");
                bool useCurrentDirectory = File.Exists(@"osu!.cfg") || (!hasFramework && Directory.GetFiles(Environment.CurrentDirectory).Length == 1);

                if (!hasFramework)
                {

                retry_with_default:
                    if (!useCurrentDirectory)
                    {
                        installPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"osu!");
                        label.Click += chooseFolder;

                        int seconds = 10;
                        while (seconds > 0)
                        {
                            setText(string.Format(LocalisationManager.GetString(OsuString.Maintenance_InstallationPath) + '\n' +
                            LocalisationManager.GetString(OsuString.Maintenance_InstallationPath2),
                                installPath, seconds), true);
                            Thread.Sleep(1000);
                            seconds--;

                            while (installPath == null)
                                Thread.Sleep(100);
                        }

                        label.Click -= chooseFolder;
                    }

                retry:
                    if (installPath != Environment.CurrentDirectory)
                    {
                        string installExecutable = Path.Combine(installPath, @"osu!.exe");

                        try
                        {
                            if (!Directory.Exists(installPath))
                                Directory.CreateDirectory(installPath);

                            try
                            {
                                File.Delete(installExecutable);
                            }
                            catch (IOException)
                            {
                                //ignore IOExceptions for now
                            }

                            byte[] executable = File.ReadAllBytes(osuMain.FullPath);
                            using (FileStream fs = File.Create(installExecutable))
                                fs.Write(executable, 0, executable.Length);
                        }
                        catch (UnauthorizedAccessException)
                        {
                            if (!osuMain.IsElevated)
                            {
                                if (osuMain.Elevate(@"-allowuserwrites " + installPath, true))
                                    goto retry;
                                else
                                    //error occurred while elevating. try again using the default path.
                                    goto retry_with_default;
                            }
                        }

                        Process.Start(installExecutable);
                        osuMain.ExitImmediately();
                    }
                }

                try
                {
                    if (hasFramework)
                        stream = ConfigManager.sReleaseStream.Value;
                }
                catch
                {
                    osuMain.Repair(true);
                }

                setText(string.Empty);

            tryAgain:
                setProgress();

                if (hasFramework) //force a complete check.
                    ConfigManager.ResetHashes();

                CommonUpdater.Check(delegate(UpdateStates s) { }, stream);

                while (true)
                {
                    switch (CommonUpdater.State)
                    {
                        case UpdateStates.Checking:
                            setText(CommonUpdater.GetStatusString(), detailedTextImportant);
                            setProgress();
                            Thread.Sleep(20);
                            continue;
                        case UpdateStates.Updating:
                            setText(CommonUpdater.GetStatusString());
                            setProgress(CommonUpdater.Percentage);
                            Thread.Sleep(20);
                            continue;
                        case UpdateStates.Completed:
                        case UpdateStates.NeedsRestart:
                            setText(LocalisationManager.GetString(OsuString.CommonUpdater_Updated));
                            break;
                        case UpdateStates.NoUpdate:
                            setText(LocalisationManager.GetString(OsuString.CommonUpdater_Updated), true);
                            Thread.Sleep(1000);
                            break;
                        case UpdateStates.Error:
                            CommonUpdater.Reset();

                            setProgress();
                            setText(LocalisationManager.GetString(OsuString.Maintenance_ErrorOccurred), true);
                            Thread.Sleep(5000);

                            goto tryAgain;
                        default:
                            break;
                    }

                    break;
                }

                if (!osuMain.IsWine && !hasFramework)
                {
                    ShellLinkHelper.Create(icon_path_desktop, Path.Combine(Environment.CurrentDirectory, @"osu!.exe"), @"osu!");
                    ShellLinkHelper.Create(icon_path_startmenu, Path.Combine(Environment.CurrentDirectory, @"osu!.exe"), @"osu!");
                }

                instance.Dispose();

                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Error occured (please report this!):\n\n" + ex.ToString());
            }
        }

        private void chooseFolder(object sender, EventArgs e)
        {
            string oldInstallPath = installPath;

            installPath = null;

            //let the user choose a path for installation
            Invoke(delegate
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                fbd.Description = LocalisationManager.GetString(OsuString.Maintenance_ChooseInstallFolder);
                fbd.ShowNewFolderButton = true;
                fbd.SelectedPath = Environment.CurrentDirectory;

                if (fbd.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
                    installPath = oldInstallPath;
                else
                {
                    installPath = fbd.SelectedPath;
                    
                    if (!Directory.Exists(installPath))
                    {
                        installPath = oldInstallPath;
                        return;
                    }

                    if (File.Exists(Path.Combine(installPath, @"osu!.cfg")))
                        return; //we have an existing osu! install we are repairing.

                    if (Directory.GetDirectories(installPath).Length + Directory.GetFiles(installPath).Length > 0)
                        installPath = Path.Combine(installPath, @"osu!");
                }
            });
        }

        void avatarFix(object sender, DoWorkEventArgs e)
        {
            if (Directory.Exists(@"Avatars"))
            {
                string[] files = Directory.GetFiles(@"Avatars");
                for (int i = 0; i < files.Length; i++)
                {
                    File.Delete(files[i]);
                    worker.ReportProgress((int)((float)i / files.Length * 100));
                }

                Directory.Delete(@"Avatars", true);
            }

            DialogResult = DialogResult.OK;
        }

        void restoreScores(object sender, DoWorkEventArgs e)
        {
            ScoreManager.LocalScores.Clear();

            if (!Directory.Exists(@"Data\r"))
                return;

            string[] files = Directory.GetFiles(@"Data\r");

            for (int i = 0; i < files.Length; i++)
            {
                string file = files[i];
                ScoreManager.ReadReplayFromFile(file, false);
                worker.ReportProgress((int)((float)i / files.Length * 100));
            }

            DialogResult = DialogResult.OK;
        }

        string detailedText = string.Empty;
        bool detailedTextImportant;
        void setText(string text = null, bool important = false)
        {
            if (text != null)
            {
                detailedTextImportant = important;
                detailedText = text;
            }

            if (displayDetailedText || detailedTextImportant)
                Invoke(delegate { label.Text = detailedText; });
            else
                Invoke(delegate { label.Text = LocalisationManager.GetString(OsuString.CommonUpdater_UpdatingGeneral); });
        }

        bool displayDetailedText;
        private void label_MouseEnter(object sender, EventArgs e)
        {
            displayDetailedText = true;
            setText();
        }

        private void label_MouseLeave(object sender, EventArgs e)
        {
            displayDetailedText = false;
            setText();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(@"https://osu.zendesk.com/hc/en-us/articles/201584269");
        }
    }
}
