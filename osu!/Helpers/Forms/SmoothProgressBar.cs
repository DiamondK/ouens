﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace osu.Helpers.Forms
{
    public partial class SmoothProgressBar : UserControl
    {
        Timer updateTimer = new Timer();

        public SmoothProgressBar()
        {
            BackColor = Color.FromArgb(85, 85, 85);
            InitializeComponent();

            updateTimer.Interval = 16;
            updateTimer.Tick += updateTimer_Tick;
            updateTimer.Start();

            DoubleBuffered = true;
        }

        float current;
        float cycle;
        float percent { get { return (float)(val - min) / (float)(max - min); } }

        void updateTimer_Tick(object sender, EventArgs e)
        {
            if (percent <= 0)
            {
                cycle += 0.05f;
                current = 1;

                Invalidate();
            }
            else if (Math.Abs(current - percent) > 0.005f)
            {
                if (cycle != 0)
                {
                    current = pulseAlpha > 0.5f ? 1 : 0;
                    cycle = 0;
                }
                else
                {
                    current = current * 0.90f + percent * 0.10f;
                }

                Invalidate();
            }
        }

        int min = 0;
        int max = 100;
        int val = 0;
        
        Color barColour = Color.FromArgb(255, 102, 170);

        float pulseAlpha { get { return (float)Math.Max(0, Math.Sin(cycle)); } }

        protected override void OnPaint(PaintEventArgs e)
        {
            int alpha = percent <= 0 ? (int)(255 * pulseAlpha) : 255;
            using (SolidBrush brush = new SolidBrush(Color.FromArgb(alpha, barColour)))
            {
                RectangleF rect = ClientRectangle;

                // Calculate area for drawing the progress.
                rect.Width = 1 + (float)rect.Width * current;

                // Draw the progress meter.
                e.Graphics.FillRectangle(brush, rect);
            }
        }

        public int Minimum
        {
            get
            {
                return min;
            }

            set
            {
                // Prevent a negative value.
                if (value < 0)
                {
                    min = 0;
                }

                // Make sure that the minimum value is never set higher than the maximum value.
                if (value > max)
                {
                    min = value;
                    min = value;
                }

                // Ensure value is still in range
                if (val < min)
                {
                    val = min;
                }

                // Invalidate the control to get a repaint.
                this.Invalidate();
            }
        }

        public int Maximum
        {
            get
            {
                return max;
            }

            set
            {
                // Make sure that the maximum value is never set lower than the minimum value.
                if (value < min)
                {
                    min = value;
                }

                max = value;

                // Make sure that value is still in range.
                if (val > max)
                {
                    val = max;
                }

                // Invalidate the control to get a repaint.
                this.Invalidate();
            }
        }

        public int Value
        {
            get
            {
                return val;
            }

            set
            {
                int oldValue = val;

                // Make sure that the value does not stray outside the valid range.
                if (value < min)
                {
                    val = min;
                }
                else if (value > max)
                {
                    val = max;
                }
                else
                {
                    val = value;
                }

                // Invalidate only the changed area.
                float percent;

                Rectangle newValueRect = this.ClientRectangle;
                Rectangle oldValueRect = this.ClientRectangle;

                // Use a new value to calculate the rectangle for progress.
                percent = (float)(val - min) / (float)(max - min);
                newValueRect.Width = (int)((float)newValueRect.Width * percent);

                // Use an old value to calculate the rectangle for progress.
                percent = (float)(oldValue - min) / (float)(max - min);
                oldValueRect.Width = (int)((float)oldValueRect.Width * percent);

                Rectangle updateRect = new Rectangle();

                // Find only the part of the screen that must be updated.
                if (newValueRect.Width > oldValueRect.Width)
                {
                    updateRect.X = oldValueRect.Size.Width;
                    updateRect.Width = newValueRect.Width - oldValueRect.Width;
                }
                else
                {
                    updateRect.X = newValueRect.Size.Width;
                    updateRect.Width = oldValueRect.Width - newValueRect.Width;
                }

                updateRect.Height = this.Height;

                // Invalidate the intersection region only.
                this.Invalidate(updateRect);
            }
        }

        public Color ProgressBarColor
        {
            get
            {
                return barColour;
            }

            set
            {
                barColour = value;

                // Invalidate the control to get a repaint.
                this.Invalidate();
            }
        }
    }
}
