﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.Online;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu_common.Helpers;
using System.ComponentModel;
using osu.Graphics.Notifications;
using Microsoft.Xna.Framework.Graphics;
using System.Globalization;
using System.Threading;
using System.Drawing;

namespace osu.Helpers.Forms
{
    public partial class ErrorDialog : pFormStyled
    {
        private BackgroundWorker worker;
        public static bool IsHandlingError;

        public ErrorDialog(string error, bool submitError) : this(new Exception(error), submitError) { }

        public ErrorDialog(Exception ex, bool submitError)
        {
            SplashScreen.CloseAll();

            IsHandlingError = true;

            BringToFrontEx();

            ControlAdded += control_ControlAdded;
            ControlRemoved += control_ControlRemoved;

            InitializeComponent();

            StringBuilder errBuf = new StringBuilder();
            foreach (string line in ex.ToString().Split('\n'))
                if (line.Contains("Microsoft.Xna.Framework.Game.Tick"))
                    break;
                else
                    errBuf.AppendLine(line.Trim('\r'));
            string errorString = errBuf.ToString();

            textBoxDetails.Text = errorString;

            StackTrace trace = new StackTrace(ex);

            if (submitError)
            {
                worker = new BackgroundWorker();
                worker.DoWork += delegate
                {
                    OsuError osuError = new OsuError();

                    StringBuilder report = new StringBuilder();
                    foreach (StackFrame frame in DebugHelper.FilterFrames(trace.GetFrames()))
                    {
                        report.AppendLine("METHOD " + frame.GetMethod());
                        report.AppendLine("Module " + frame.GetMethod().Module);
                        report.AppendLine("Il-Offset: " + frame.GetILOffset() + " Jit-offset: " + frame.GetNativeOffset());
                        report.AppendLine("IL code ");
                        report.AppendLine(DebugHelper.OpcodeToIL(frame.GetMethod()));
                        report.AppendLine();
                        MethodBody MB = frame.GetMethod().GetMethodBody();
                        List<ExceptionHandlingClause> Exceptions = MB != null ? new List<ExceptionHandlingClause>(MB.ExceptionHandlingClauses) : null;

                        if (Exceptions == null || Exceptions.Count == 0)
                            continue;
                        report.AppendLine("possible caught exceptions:");

                        //we don't use toString as this method assumes FilterOffset!=null
                        //(bad ms code)
                        foreach (ExceptionHandlingClause E in Exceptions)
                        {
                            try
                            {
                                report.AppendLine(String.Format("catchType:{0}, Flags:{1}, HandlerLength:{2}, " + "HandlerOffset:{3}, TryLength:{4}, TryOffset:{5}", E.CatchType, E.Flags, E.HandlerLength, E.HandlerOffset, E.TryLength, E.TryOffset));
                            }
                            catch
                            {
                                report.AppendLine("Unknown ExceptionType");
                            }
                        }
                        report.AppendLine("ENDMETHOD");
                        report.AppendLine();
                    }

                    osuError.ILTrace = report.ToString();
                    osuError.StackTrace = errorString;
                    osuError.Exception = ex != null ? ex.GetType().ToString() : string.Empty;
                    ErrorSubmission.Submit(osuError);
                };
                worker.RunWorkerAsync();
            }
        }

        private void buttonRestart_Click(object sender, EventArgs e)
        {
            osuMain.Repair();
            Close();
        }

        private void buttonReport_Click(object sender, EventArgs e)
        {
            Process.Start(General.WEB_ROOT + "/forum/viewtopic.php?f=5&t=576");
        }

        private void ErrorDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.WindowsShutDown)
                osuMain.ExitImmediately();
        }

        private void pippiBox_Click(object sender, EventArgs e)
        {
            textBoxDetails.Visible = !textBoxDetails.Visible;
        }

        private void textBoxDetails_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxDetails.SelectedText))
                textBoxDetails.SelectAll();
        }

        private void subscribeEvents(Control control)
        {
            control.MouseLeave += control_MouseLeave;
            control.ControlAdded += control_ControlAdded;
            control.ControlRemoved += control_ControlRemoved;

            foreach (Control innerControl in control.Controls)
                subscribeEvents(innerControl);
        }

        private void unsubscribeEvents(Control control)
        {
            control.MouseLeave -= control_MouseLeave;
            control.ControlAdded -= control_ControlAdded;
            control.ControlRemoved -= control_ControlRemoved;

            foreach (Control innerControl in control.Controls)
                unsubscribeEvents(innerControl);
        }

        private void control_ControlAdded(object sender, ControlEventArgs e)
        {
            subscribeEvents(e.Control);
        }

        private void control_ControlRemoved(object sender, ControlEventArgs e)
        {
            unsubscribeEvents(e.Control);
        }

        private void control_MouseLeave(object sender, EventArgs e)
        {
            if (ClientRectangle.Contains(PointToClient(Cursor.Position)) == false)
                textBoxDetails.Visible = false;
        }
    }
}