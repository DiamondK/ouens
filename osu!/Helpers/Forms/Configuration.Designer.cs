﻿namespace osu.Helpers.Forms
{
    partial class Configuration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.releaseStreams = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonRepair = new System.Windows.Forms.Button();
            this.buttonStartOsu = new System.Windows.Forms.Button();
            this.buttonResetSettings = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelRenderer = new System.Windows.Forms.Label();
            this.checkBoxFullScreen = new System.Windows.Forms.CheckBox();
            this.comboBoxRenderer = new System.Windows.Forms.ComboBox();
            this.labelResolution = new System.Windows.Forms.Label();
            this.labelFrameLimiter = new System.Windows.Forms.Label();
            this.comboBoxFrameLimiter = new System.Windows.Forms.ComboBox();
            this.comboBoxResolutions = new System.Windows.Forms.ComboBox();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.releaseStreams);
            this.groupBox2.Location = new System.Drawing.Point(13, 159);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(336, 69);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Recovery";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(5, 33);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "Release Stream:";
            // 
            // releaseStreams
            // 
            this.releaseStreams.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.releaseStreams.FormattingEnabled = true;
            this.releaseStreams.Location = new System.Drawing.Point(99, 30);
            this.releaseStreams.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.releaseStreams.Name = "releaseStreams";
            this.releaseStreams.Size = new System.Drawing.Size(227, 21);
            this.releaseStreams.TabIndex = 0;
            this.releaseStreams.SelectedIndexChanged += new System.EventHandler(this.releaseStreams_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.panel1.Controls.Add(this.buttonRepair);
            this.panel1.Controls.Add(this.buttonStartOsu);
            this.panel1.Controls.Add(this.buttonResetSettings);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 244);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(361, 141);
            this.panel1.TabIndex = 8;
            // 
            // buttonRepair
            // 
            this.buttonRepair.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.buttonRepair.FlatAppearance.BorderSize = 0;
            this.buttonRepair.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.buttonRepair.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(44)))), ((int)(((byte)(44)))));
            this.buttonRepair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRepair.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRepair.ForeColor = System.Drawing.Color.White;
            this.buttonRepair.Location = new System.Drawing.Point(7, 98);
            this.buttonRepair.Name = "buttonRepair";
            this.buttonRepair.Size = new System.Drawing.Size(350, 35);
            this.buttonRepair.TabIndex = 7;
            this.buttonRepair.Text = "repair osu!";
            this.buttonRepair.UseVisualStyleBackColor = true;
            this.buttonRepair.Click += new System.EventHandler(this.buttonRepair_Click);
            // 
            // buttonStartOsu
            // 
            this.buttonStartOsu.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.buttonStartOsu.FlatAppearance.BorderSize = 0;
            this.buttonStartOsu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(172)))), ((int)(((byte)(220)))));
            this.buttonStartOsu.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(127)))), ((int)(((byte)(161)))));
            this.buttonStartOsu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStartOsu.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartOsu.ForeColor = System.Drawing.Color.White;
            this.buttonStartOsu.Location = new System.Drawing.Point(7, 14);
            this.buttonStartOsu.Name = "buttonStartOsu";
            this.buttonStartOsu.Size = new System.Drawing.Size(350, 35);
            this.buttonStartOsu.TabIndex = 2;
            this.buttonStartOsu.Text = "start osu!";
            this.buttonStartOsu.UseVisualStyleBackColor = true;
            this.buttonStartOsu.Click += new System.EventHandler(this.buttonStartOsu_Click);
            // 
            // buttonResetSettings
            // 
            this.buttonResetSettings.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.buttonResetSettings.FlatAppearance.BorderSize = 0;
            this.buttonResetSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonResetSettings.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonResetSettings.ForeColor = System.Drawing.Color.White;
            this.buttonResetSettings.Location = new System.Drawing.Point(7, 57);
            this.buttonResetSettings.Name = "buttonResetSettings";
            this.buttonResetSettings.Size = new System.Drawing.Size(350, 35);
            this.buttonResetSettings.TabIndex = 6;
            this.buttonResetSettings.Text = "reset settings";
            this.buttonResetSettings.UseVisualStyleBackColor = true;
            this.buttonResetSettings.Click += new System.EventHandler(this.buttonResetSettings_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxResolutions);
            this.groupBox1.Controls.Add(this.labelRenderer);
            this.groupBox1.Controls.Add(this.checkBoxFullScreen);
            this.groupBox1.Controls.Add(this.comboBoxRenderer);
            this.groupBox1.Controls.Add(this.labelResolution);
            this.groupBox1.Controls.Add(this.labelFrameLimiter);
            this.groupBox1.Controls.Add(this.comboBoxFrameLimiter);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(336, 134);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Graphics";
            // 
            // labelRenderer
            // 
            this.labelRenderer.AutoSize = true;
            this.labelRenderer.ForeColor = System.Drawing.Color.Black;
            this.labelRenderer.Location = new System.Drawing.Point(5, 64);
            this.labelRenderer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelRenderer.Name = "labelRenderer";
            this.labelRenderer.Size = new System.Drawing.Size(54, 15);
            this.labelRenderer.TabIndex = 1;
            this.labelRenderer.Text = "Renderer";
            // 
            // checkBoxFullScreen
            // 
            this.checkBoxFullScreen.AutoSize = true;
            this.checkBoxFullScreen.ForeColor = System.Drawing.Color.Black;
            this.checkBoxFullScreen.Location = new System.Drawing.Point(239, 63);
            this.checkBoxFullScreen.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.checkBoxFullScreen.Name = "checkBoxFullScreen";
            this.checkBoxFullScreen.Size = new System.Drawing.Size(83, 19);
            this.checkBoxFullScreen.TabIndex = 2;
            this.checkBoxFullScreen.Text = "Full Screen";
            this.checkBoxFullScreen.UseVisualStyleBackColor = true;
            this.checkBoxFullScreen.CheckedChanged += new System.EventHandler(this.checkBoxFullScreen_CheckedChanged);
            // 
            // comboBoxRenderer
            // 
            this.comboBoxRenderer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRenderer.FormattingEnabled = true;
            this.comboBoxRenderer.Location = new System.Drawing.Point(100, 61);
            this.comboBoxRenderer.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.comboBoxRenderer.Name = "comboBoxRenderer";
            this.comboBoxRenderer.Size = new System.Drawing.Size(127, 21);
            this.comboBoxRenderer.TabIndex = 0;
            this.comboBoxRenderer.SelectedIndexChanged += new System.EventHandler(this.comboBoxRenderer_SelectedIndexChanged);
            // 
            // labelResolution
            // 
            this.labelResolution.AutoSize = true;
            this.labelResolution.ForeColor = System.Drawing.Color.Black;
            this.labelResolution.Location = new System.Drawing.Point(5, 29);
            this.labelResolution.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelResolution.Name = "labelResolution";
            this.labelResolution.Size = new System.Drawing.Size(63, 15);
            this.labelResolution.TabIndex = 1;
            this.labelResolution.Text = "Resolution";
            // 
            // labelFrameLimiter
            // 
            this.labelFrameLimiter.AutoSize = true;
            this.labelFrameLimiter.ForeColor = System.Drawing.Color.Black;
            this.labelFrameLimiter.Location = new System.Drawing.Point(5, 100);
            this.labelFrameLimiter.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelFrameLimiter.Name = "labelFrameLimiter";
            this.labelFrameLimiter.Size = new System.Drawing.Size(80, 15);
            this.labelFrameLimiter.TabIndex = 1;
            this.labelFrameLimiter.Text = "Frame Limiter";
            // 
            // comboBoxFrameLimiter
            // 
            this.comboBoxFrameLimiter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFrameLimiter.FormattingEnabled = true;
            this.comboBoxFrameLimiter.Location = new System.Drawing.Point(100, 97);
            this.comboBoxFrameLimiter.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.comboBoxFrameLimiter.Name = "comboBoxFrameLimiter";
            this.comboBoxFrameLimiter.Size = new System.Drawing.Size(227, 21);
            this.comboBoxFrameLimiter.TabIndex = 0;
            this.comboBoxFrameLimiter.SelectedIndexChanged += new System.EventHandler(this.comboBoxFrameLimiter_SelectedIndexChanged);
            // 
            // comboBoxResolutions
            // 
            this.comboBoxResolutions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxResolutions.FormattingEnabled = true;
            this.comboBoxResolutions.Location = new System.Drawing.Point(100, 25);
            this.comboBoxResolutions.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.comboBoxResolutions.Name = "comboBoxResolutions";
            this.comboBoxResolutions.Size = new System.Drawing.Size(227, 23);
            this.comboBoxResolutions.TabIndex = 3;
            this.comboBoxResolutions.SelectedIndexChanged += new System.EventHandler(this.comboBoxResolutions_SelectedIndexChanged);
            // 
            // Configuration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(361, 385);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Configuration";
            this.Opacity = 1D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "osu! configuration";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Configuration_FormClosing);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelFrameLimiter;
        private System.Windows.Forms.ComboBox comboBoxFrameLimiter;
        private System.Windows.Forms.Label labelRenderer;
        private System.Windows.Forms.ComboBox comboBoxRenderer;
        private System.Windows.Forms.CheckBox checkBoxFullScreen;
        private System.Windows.Forms.Label labelResolution;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonStartOsu;
        private System.Windows.Forms.Button buttonResetSettings;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox releaseStreams;
        private System.Windows.Forms.Button buttonRepair;
        private System.Windows.Forms.ComboBox comboBoxResolutions;

    }
}