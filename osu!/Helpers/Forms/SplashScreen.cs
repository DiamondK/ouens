﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using osu_common.Helpers;

namespace osu.Helpers.Forms
{
    public partial class SplashScreen : Form
    {
        Timer timer;

        private static SplashScreen instance;
        private Bitmap splashImage;

        public SplashScreen(Bitmap img)
        {
            InitializeComponent();

            splashImage = img;

            instance = this;

            if (AeroGlassHelper.GlassEnabled)
                Opacity = 0;

            timer = new Timer();
            timer.Interval = 30;
            timer.Tick += new EventHandler(t_Tick);
            timer.Start();
        }

        void t_Tick(object sender, EventArgs e)
        {
            if (Opacity < 0.8)
                Opacity += (1 - Opacity) * 0.1;

            if (GameBase.Form != null && GameBase.Form.Created)
                EndSplash();
        }

        public static void CloseAll()
        {
            try
            {
                SplashScreen s = instance;
                if (s != null) s.EndSplash();
            }
            catch { }
        }

        private void EndSplash()
        {
            try
            {
                timer.Stop();
                Close();
                Dispose();
                instance = null;
            }
            catch { }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (timer.Enabled)
                e.Cancel = true;
            if (splashImage != null)
                splashImage.Dispose();

            base.OnClosing(e);
        }

        bool firstPaint = true;
        protected override void OnPaint(PaintEventArgs e)
        {
            if (firstPaint)
            {
                firstPaint = false;

                WindowState = FormWindowState.Minimized;
                Show();
                WindowState = FormWindowState.Normal;
            }

            e.Graphics.DrawImage(splashImage, new Rectangle(0, 0, splashImage.Width, splashImage.Height));
        }
    }
}