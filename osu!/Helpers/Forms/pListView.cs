﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using osu_common.Helpers;

namespace osu.Helpers.Forms
{
    class pListView : ListView
    {
        public pListView() : base()
        {
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            FixColumns();
        }

        private bool beenScaled = false;
        private bool columnsSized = false;

        private void FixColumns()
        {
            if (beenScaled || Columns.Count == 0 || !columnsSized) return;

            float ratio = DpiHelper.DPI(this) / 96.0f;

            foreach (ColumnHeader c in Columns)
            {
                c.Width = (int)(c.Width * ratio);
            }
            beenScaled = true;
        }

        protected override void OnLayout(LayoutEventArgs levent)
        {
            base.OnLayout(levent);
            FixColumns();
        }

        protected override void OnColumnWidthChanged(ColumnWidthChangedEventArgs e)
        {
            base.OnColumnWidthChanged(e);
            columnsSized = true;
        }

    }
}
