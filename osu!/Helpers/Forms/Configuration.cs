﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using osu.GameModes.Options;
using osu.Configuration;
using osu_common.Helpers;
using osu_common;
using osu_common.Updater;
using osu.Graphics.UserInterface;
using Microsoft.Xna.Framework;
using osu.Graphics.Sprites;

namespace osu.Helpers.Forms
{
    public partial class Configuration : pFormStyled
    {
        ReleaseStream initialReleaseStream = ConfigManager.sReleaseStream.Value;
        pDropdown resolutionDropdown = new pDropdown(null, string.Empty, Vector2.Zero);

        public Configuration()
        {
            InitializeComponent();
            initializeSettings();
        }

        protected void initializeSettings()
        {
            checkBoxFullScreen.Checked = ConfigManager.sScreenMode.Value == ScreenMode.Fullscreen;

            releaseStreams.Items.Clear();
            foreach (pDropdownItem i in Options.DropdownItemsReleaseStream)
            {
                releaseStreams.Items.Add(i);
                if ((ReleaseStream)i.Value == ConfigManager.sReleaseStream.Value)
                    releaseStreams.SelectedItem = i;
            }

            comboBoxRenderer.Items.Clear();
            comboBoxRenderer.Items.Add(@"OpenGL");
            comboBoxRenderer.Items.Add(@"DirectX");
            comboBoxRenderer.SelectedIndex = ConfigManager.sRenderer == @"opengl" ? 0 : 1;

            comboBoxFrameLimiter.Items.Clear();
            foreach (pDropdownItem i in Options.DropdownItemsFrameLimiter)
            {
                comboBoxFrameLimiter.Items.Add(i);
                if ((FrameSync)i.Value == ConfigManager.sFrameSync.Value)
                    comboBoxFrameLimiter.SelectedItem = i;
            }

            comboBoxResolutions.Items.Clear();

            Options.UpdateResolutions(resolutionDropdown);
            foreach (pText t in resolutionDropdown.OptionsSprites)
            {
                if (t is pTextAwesome) continue;
                
                pDropdownItem item = new pDropdownItem(t.Text, t.Tag);
                
                comboBoxResolutions.Items.Add(item);

                if ((string)t.Tag == (string)resolutionDropdown.SelectedObject)
                    comboBoxResolutions.SelectedItem = item;
            }
        }

        //When button Clicked, update all settings and start osu!
        protected void buttonStartOsu_Click(object sender, EventArgs e)
        {
            //update sRenderer
            ConfigManager.sRenderer.Value = comboBoxRenderer.SelectedItem.ToString().Trim().ToLower();

            //Set screen mod
            ConfigManager.sScreenMode.Value = checkBoxFullScreen.Checked ? ScreenMode.Fullscreen : ScreenMode.Windowed;

            DialogResult = DialogResult.OK;

            try
            {
                ConfigManager.SaveConfig();
            }
            catch
            {
                DialogResult = DialogResult.Abort;
            }

            Close();
        }

        protected void buttonResetSettings_Click(object sender, EventArgs e)
        {
            ConfigManager.sScreenMode.Reset();
            ConfigManager.sFrameSync.Reset();
            ConfigManager.sRenderer.Reset();
            ConfigManager.sWidth.Reset();
            ConfigManager.sHeight.Reset();
            ConfigManager.sWidthFullscreen.Reset();
            ConfigManager.sHeightFullscreen.Reset();

            initializeSettings();
        }

        private void buttonRepair_Click(object sender, EventArgs e)
        {
            try
            {
                ConfigManager.SaveConfig();
            }
            catch { }

            osuMain.Repair();
        }

        private void Configuration_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown || DialogResult == DialogResult.Cancel)
                osuMain.ExitImmediately();
        }

        private void releaseStreams_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConfigManager.sReleaseStream.Value = (ReleaseStream)((pDropdownItem)releaseStreams.SelectedItem).Value;

            buttonStartOsu.Enabled = initialReleaseStream == ConfigManager.sReleaseStream.Value;
            buttonResetSettings.Enabled = initialReleaseStream == ConfigManager.sReleaseStream.Value;
        }

        private void comboBoxFrameLimiter_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConfigManager.sFrameSync.Value = (FrameSync)((pDropdownItem)comboBoxFrameLimiter.SelectedItem).Value;
        }

        private void checkBoxFullScreen_CheckedChanged(object sender, System.EventArgs e)
        {
            ConfigManager.sScreenMode.Value = checkBoxFullScreen.Checked ? ScreenMode.Fullscreen : ScreenMode.Windowed;
            initializeSettings();
        }

        private void comboBoxRenderer_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConfigManager.sRenderer.Value = comboBoxRenderer.SelectedIndex == 0 ? @"opengl" : @"d3d";
        }

        private void comboBoxResolutions_SelectedIndexChanged(object sender, EventArgs e)
        {
            pDropdownItem selected = (pDropdownItem)comboBoxResolutions.SelectedItem;

            if (selected.Value == null)
            {
                if (comboBoxResolutions.Items.Count <= comboBoxResolutions.SelectedIndex + 1)
                    return;

                selected = (pDropdownItem)comboBoxResolutions.SelectedItem;
            }

            string[] split = ((string)selected.Value).Split('x');

            int width = Int32.Parse(split[0]);
            int height = Int32.Parse(split[1]);

            if (ConfigManager.sFullscreen.Value)
            {
                ConfigManager.sWidthFullscreen.Value = width;
                ConfigManager.sHeightFullscreen.Value = height;
            }
            else
            {
                ConfigManager.sWidth.Value = width;
                ConfigManager.sHeight.Value = height;
            }
        }
    }
}