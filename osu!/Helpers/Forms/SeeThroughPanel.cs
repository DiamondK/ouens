﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using osu_common.Helpers;

namespace osu.Helpers.Forms
{
    /// <summary>
    /// A panel which allows you to see through the window when glass is enabled.
    /// </summary>
    class SeeThroughPanel : Panel
    {
        public SeeThroughPanel()
            : base()
        {
        }

        private bool m_click_through;

        public bool ClickThrough
        {
            get
            {
                return m_click_through;
            }
            set
            {
                m_click_through = value;
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (BackColor.A < 255)
            {
                System.Drawing.Graphics g = e.Graphics;

                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                g.Clear(Color.FromArgb(0, 0, 0, 0));
            }
            else base.OnPaintBackground(e);
        }

        private const int WM_NCHITTEST = 0x0084;

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            switch (m.Msg)
            {
                case WM_NCHITTEST:

                    if (m_click_through) m.Result = (IntPtr)NonClientRegions.Transparent;
                    break;
            }
        }

    }
}
