﻿

using System.Reflection;
using Helpers.Debugger;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.IO;
using osu;
static class DebugHelper
{
    static internal StackTrace LastSavedStack { get; private set; }
    static DebugHelper()
    {
        DGlobals.LoadOpCodes();
    }
    internal static string OpcodeToIL(MethodBase MB)
    {
        return new MethodBodyReader(MB).GetBodyCode();
    }

    /// <summary>
    /// dumps IL code of the caller to file.
    /// </summary>
    internal static void GetThisMethodIL()
    {
        StackTrace trace = new StackTrace();
        StackFrame framePrevious = trace.GetFrame(1);
        StreamWriter SW;
        SW = File.CreateText("IlDUMP_" + framePrevious.GetMethod().Name + ".txt");
        SW.WriteLine("IL-OFFSET:" + framePrevious.GetILOffset());
        SW.Write(OpcodeToIL(framePrevious.GetMethod()));
        SW.Close();
    }

    internal static void ThrowTrace(Exception E)
    {
        StackTrace LastSavedStack = new StackTrace();
        throw E;
    }
    internal static List<StackFrame> FilterFrames(StackFrame[] FrameArray)
    {
        List<StackFrame> FrameList = new List<StackFrame>();

        if (FrameArray == null) return FrameList;

        FrameList.AddRange(FrameArray);

        string NotWanted = "ErrorDialog~" +
                           "Void Update(Microsoft.Xna.Framework.GameTime)~" +
                           "Void Main()~" +
                           "Void UpdateFadeState()~" +
                           "Void UpdateChildren()";
        List<string> Disallow = new List<string>(NotWanted.Split('~'));

        FrameList.RemoveAll(F => !F.GetMethod().Module.ToString().Contains(osuMain.FilenameWithoutExtension)
                             || Disallow.Find(S => F.GetMethod().ToString().Contains(S)) != null);
        return FrameList;
    }
}