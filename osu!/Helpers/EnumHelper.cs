﻿using System;

namespace osu.Helpers
{
    internal static class EnumHelper
    {
        internal static bool TryParseStartsWith<TEnum>(string value, out TEnum result)
        {
            if (value == null)
                throw new ArgumentNullException();

            if (value.Length > 0)
            {
                foreach (object v in Enum.GetValues(typeof(TEnum)))
                {
                    if (value.StartsWith(v.ToString()))
                    {
                        result = (TEnum)v;
                        return true;
                    }
                }
            }

            result = default(TEnum);
            return false;
        }
    }
}
