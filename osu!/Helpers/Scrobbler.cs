﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Audio;
using osu.Configuration;
using osu.GameplayElements.Beatmaps;
using osu.Online;
using osu_common.Libraries.NetLib;

namespace osu.Helpers
{
    static class Scrobbler
    {
        static bool shouldContinueSending = true;
        static Beatmap last;
        static ScrobbleStatus status;
        static double timeElapsed;

        internal static void Update()
        {
            if (!shouldContinueSending || GameBase.IdleTime > 10000 * 60 || !BanchoClient.Connected) return;

            if (!AudioEngine.Paused && BeatmapManager.Current != last)
            {
                last = BeatmapManager.Current;
                status = ScrobbleStatus.Pending;
                timeElapsed = 0;
                if (last == null) return;
            }

            if (!AudioEngine.Paused)
            {
                timeElapsed += GameBase.ElapsedMilliseconds;

                switch (status)
                {
                    case ScrobbleStatus.Pending:
                        if (timeElapsed > 3000)
                        {
                            status = ScrobbleStatus.NowPlaying;
                            sendCurrentTrack(false);
                        }
                        break;
                    case ScrobbleStatus.NowPlaying:
                        if (timeElapsed > 40000 || timeElapsed > AudioEngine.AudioLength / 2)
                        {
                            status = ScrobbleStatus.Scrobbled;
                            sendCurrentTrack(true);
                        }
                        break;
                    case ScrobbleStatus.Scrobbled:
                        if (timeElapsed > AudioEngine.AudioLength)
                        {
                            status = ScrobbleStatus.Pending;
                            timeElapsed = 0;
                        }
                        break;
                }
            }
        }

        private static void sendCurrentTrack(bool scrobble)
        {
#if DEBUG
            return;
#endif

            if (last == null) return;

            string url = General.WEB_ROOT + "/web/lastfm.php?b=" + last.BeatmapId +
                "&action=" + (scrobble ? "scrobble" : "np") +
                "&us=" + ConfigManager.sUsername + "&ha=" + ConfigManager.sPassword;

            StringNetRequest snr = new StringNetRequest(url);
            snr.onFinish += snr_onFinish;
            NetManager.AddRequest(snr);
        }

        static void snr_onFinish(string _result, Exception e)
        {
            if (_result == null)
                return;
            try
            {
                switch (Int32.Parse(_result))
                {
                    case -3:
                        shouldContinueSending = false;
                        break;
                }
            }
            catch { }
        }

        enum ScrobbleStatus
        {
            Pending,
            NowPlaying,
            Scrobbled
        }
    }
}
