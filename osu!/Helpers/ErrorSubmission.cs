﻿using System;
using System.Threading;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu_common.Libraries.NetLib;
using System.IO;
using System.Text;

namespace osu.Helpers
{
    internal class OsuError
    {
        internal string Feedback = string.Empty;
        internal string ILTrace = string.Empty;
        internal string StackTrace = string.Empty;
        public string Exception = string.Empty;
        public byte[] Screenshot;

        internal OsuError()
        {
        }

        internal OsuError(Exception e)
        {
            Exception = e.GetType().ToString();

            StringBuilder errBuf = new StringBuilder();
            foreach (string line in e.ToString().Split('\n'))
                if (line.Contains("Microsoft.Xna.Framework.Game.Tick"))
                    break;
                else
                    errBuf.AppendLine(line.Trim('\r'));
            StackTrace = errBuf.ToString();

        }
    }

    internal static class ErrorSubmission
    {
        static int lastSubmission;
        internal static void Submit(OsuError err)
        {
            if (lastSubmission > 0 && GameBase.Time - lastSubmission < 10000)
                return;
            lastSubmission = GameBase.Time;

            try
            {
                Http h = new Http();
                HttpRequest r = new HttpRequest();
                h.Request = r;
                r.Items.AddFormField("u", ConfigManager.sUsername);
                if (GameBase.User != null) r.Items.AddFormField("i", GameBase.User.Id.ToString());
                r.Items.AddFormField("osumode", ((int)GameBase.Mode).ToString());
                r.Items.AddFormField("gamemode", ((int)Player.Mode).ToString());
                r.Items.AddFormField("gametime", GameBase.Time.ToString());
                r.Items.AddFormField("audiotime", AudioEngine.Time.ToString());
                r.Items.AddFormField("culture", Thread.CurrentThread.CurrentCulture.Name);
                r.Items.AddFormField("b", BeatmapManager.Current != null ? BeatmapManager.Current.BeatmapId.ToString() : "");
                r.Items.AddFormField("bc", BeatmapManager.Current != null ? BeatmapManager.Current.BeatmapChecksum : "");
                r.Items.AddFormField("exception", err.Exception);
                r.Items.AddFormField("feedback", err.Feedback);
                r.Items.AddFormField("stacktrace", err.StackTrace);
                r.Items.AddFormField("iltrace", err.ILTrace);
                r.Items.AddFormField("version", General.BUILD_NAME);
                r.Items.AddFormField("exehash", GameBase.ExeHash(Path.GetFileName(osuMain.FullPath)));
                r.Items.AddFormField("config", File.ReadAllText(ConfigManager.ConfigFilename));
                if (err.Screenshot != null) r.Items.AddSubmitFile(@"ss", @"ss").AddDataArray(err.Screenshot);
                h.TimeOut = 6000;

                string[] response = h.Post(General.WEB_ROOT + "/web/osu-error.php");
                h.Close();
            }
            catch
            { }
        }
    }
}