﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.GameplayElements.HitObjects;

namespace osu.Helpers
{
    internal delegate void VoidDelegate();
    internal delegate void BoolDelegate(bool b);
    internal delegate void HitCircleDelegate(HitObject h);
}
