﻿using Microsoft.Win32;

namespace osu.Helpers
{
    /// <summary>
    /// Handle disabling the screensaver when osu! is the active window.
    /// </summary>
    internal static class Screensaver
    {
        private static bool CouldntOpenKey;
        private static bool ScreensaverIsActive;
        private static RegistryKey ScreensaverRegKey;
        private static bool ScreensaverWasActive;

        static bool? wasEnabled = null;

        public static void Disable()
        {
            if (CouldntOpenKey || (wasEnabled.HasValue && !wasEnabled.Value)) return;
            wasEnabled = false;

            if (ScreensaverRegKey == null)
            {
                try
                {
                    ScreensaverRegKey = Registry.CurrentUser.OpenSubKey("ControlPanel\\Desktop", true);
                    if (ScreensaverRegKey != null)
                        ScreensaverWasActive = ScreensaverRegKey.GetValue("ScreenSaveActive").ToString() == "1";
                    else
                        CouldntOpenKey = true;
                }
                catch
                {
                    CouldntOpenKey = true;
                    //No registry access
                }
            }

            if (ScreensaverWasActive && ScreensaverIsActive)
            {
                if (ScreensaverRegKey != null)
                    ScreensaverRegKey.SetValue("ScreenSaveActive", "0");
                ScreensaverIsActive = false;
            }
        }

        public static void Restore()
        {
            if (CouldntOpenKey || (wasEnabled.HasValue && wasEnabled.Value)) return;
            wasEnabled = true;

            if (ScreensaverWasActive && !ScreensaverIsActive)
            {
                if (ScreensaverRegKey != null)
                    ScreensaverRegKey.SetValue("ScreenSaveActive", "1");
                ScreensaverIsActive = true;
            }
        }
    }
}