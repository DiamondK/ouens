﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Windows.Forms;

namespace osu.Helpers
{
    internal class AeroGlassHelper
    {
        /// <summary>
        /// True if Aero Glass compositig is enabled
        /// </summary>
        internal static bool GlassEnabled
        {
            get
            {
                if (System.Environment.OSVersion.Version.Major < 6) return false;
                else return DwmApi.DwmIsCompositionEnabled();
            }
        }

        /// <summary>
        /// True if the Aero theme is enabled (regardless of compositing)
        /// </summary>
        internal static bool AeroEnabled
        {
            get
            {
                return (System.Environment.OSVersion.Version.Major >= 6) && Application.RenderWithVisualStyles;
            }
        }

        /// <summary>
        /// Extends the glassed region into the client area.
        /// </summary>
        internal static void ExtendFrame(Control window, Margins margins)
        {
            if (!GlassEnabled) return;

            DwmApi.DwmExtendFrameIntoClientArea(window.Handle, new DwmApi.MARGINS(margins.Left, margins.Right, margins.Top, margins.Bottom));
        }

        /// <summary>
        /// Turns the window completely into aero glass.
        /// </summary>
        /// <param name="window"></param>
        internal static void GlassSlate(Control window)
        {
            if (!GlassEnabled) return;

            DwmApi.DwmExtendFrameIntoClientArea(window.Handle, new DwmApi.MARGINS(-1, -1, -1, -1));
        }

        /// <summary>
        /// Resets the glassed area to default.
        /// </summary>
        internal static void ResetFrame(Control window)
        {
            if (!GlassEnabled) return;

            DwmApi.DwmExtendFrameIntoClientArea(window.Handle, new DwmApi.MARGINS(0, 0, 0, 0));
        }

        internal static void EnableGlass(Control window)
        {
            if (!GlassEnabled) return;

            int enable = (int)DwmApi.DWMNCRENDERINGPOLICY.DWMNCRP_ENABLED;
            DwmApi.DwmSetWindowAttribute(window.Handle, (int)DwmApi.DWMWINDOWATTRIBUTE.DWMWA_NCRENDERING_POLICY, ref enable, sizeof(int));
        }

        internal static void DisableGlass(Control window)
        {
            if (!GlassEnabled) return;

            int enable = (int)DwmApi.DWMNCRENDERINGPOLICY.DWMNCRP_USEWINDOWSTYLE;
            DwmApi.DwmSetWindowAttribute(window.Handle, (int)DwmApi.DWMWINDOWATTRIBUTE.DWMWA_NCRENDERING_POLICY, ref enable, sizeof(int));
        }

        internal static void SetWindowTheme(Control window, String SubAppName, String SubIdList)
        {
            if (!Application.RenderWithVisualStyles) return;
            int result;

            if ((result = UxTheme.SetWindowTheme(window.Handle, SubAppName, SubIdList)) < 0)
            {
                throw new InvalidOperationException("SetWindowTheme failed with error " + result.ToString());
            }
        }

        internal static void SetWindowTheme(Control window, String SubAppName)
        {
            SetWindowTheme(window, SubAppName, null);
        }

        internal static void SetWindowTheme(Control window)
        {
            SetWindowTheme(window, null, null);
        }

        // not actually aero but I cba making a separate class for this. Only used on vista+ anyway.
        private class UxTheme
        {
            [DllImport("uxtheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
            public static extern int SetWindowTheme(IntPtr hWnd, String pszSubAppName, String pszSubIdList);
        }

        // Compositor interop as found on MSDN Magazine: http://msdn.microsoft.com/en-us/magazine/cc163435.aspx
        //
        private class DwmApi
        {
            [DllImport("dwmapi.dll", PreserveSig = false)]
            public static extern void DwmEnableBlurBehindWindow(
                IntPtr hWnd, DWM_BLURBEHIND pBlurBehind);

            [DllImport("dwmapi.dll", PreserveSig = false)]
            public static extern void DwmExtendFrameIntoClientArea(
                IntPtr hWnd, MARGINS pMargins);

            [DllImport("dwmapi.dll", PreserveSig = false)]
            public static extern bool DwmIsCompositionEnabled();

            [DllImport("dwmapi.dll", PreserveSig = false)]
            public static extern void DwmEnableComposition(bool bEnable);

            [DllImport("dwmapi.dll", PreserveSig = false)]
            public static extern void DwmGetColorizationColor(
                out int pcrColorization,
                [MarshalAs(UnmanagedType.Bool)]out bool pfOpaqueBlend);

            [DllImport("dwmapi.dll", PreserveSig = false)]
            public static extern IntPtr DwmRegisterThumbnail(
                IntPtr dest, IntPtr source);

            [DllImport("dwmapi.dll", PreserveSig = false)]
            public static extern void DwmUnregisterThumbnail(IntPtr hThumbnail);

            [DllImport("dwmapi.dll", PreserveSig = false)]
            public static extern void DwmUpdateThumbnailProperties(
                IntPtr hThumbnail, DWM_THUMBNAIL_PROPERTIES props);

            [DllImport("dwmapi.dll", PreserveSig = false)]
            public static extern void DwmQueryThumbnailSourceSize(
                IntPtr hThumbnail, out Size size);

            [DllImport("dwmapi.dll", PreserveSig = false)]
            public static extern void DwmGetWindowAttribute(
                IntPtr hwnd, int dwAttribute, out int pvAttribute);

            [DllImport("dwmapi.dll", PreserveSig = false)]
            public static extern void DwmSetWindowAttribute(
                IntPtr hwnd, int dwAttribute, ref int pvAttribute, int cbAttribute);

            [StructLayout(LayoutKind.Sequential)]
            public class DWM_THUMBNAIL_PROPERTIES
            {
                public uint dwFlags;
                public RECT rcDestination;
                public RECT rcSource;
                public byte opacity;
                [MarshalAs(UnmanagedType.Bool)]
                public bool fVisible;
                [MarshalAs(UnmanagedType.Bool)]
                public bool fSourceClientAreaOnly;
                public const uint DWM_TNP_RECTDESTINATION = 0x00000001;
                public const uint DWM_TNP_RECTSOURCE = 0x00000002;
                public const uint DWM_TNP_OPACITY = 0x00000004;
                public const uint DWM_TNP_VISIBLE = 0x00000008;
                public const uint DWM_TNP_SOURCECLIENTAREAONLY = 0x00000010;
            }

            [StructLayout(LayoutKind.Sequential)]
            public class MARGINS
            {
                public int cxLeftWidth, cxRightWidth,
                           cyTopHeight, cyBottomHeight;

                public MARGINS(int left, int right, int top, int bottom)
                {
                    cxLeftWidth = left;
                    cxRightWidth = right;
                    cyTopHeight = top;
                    cyBottomHeight = bottom;
                }
            }

            [StructLayout(LayoutKind.Sequential)]
            public class DWM_BLURBEHIND
            {
                public uint dwFlags;
                [MarshalAs(UnmanagedType.Bool)]
                public bool fEnable;
                public IntPtr hRegionBlur;
                [MarshalAs(UnmanagedType.Bool)]
                public bool fTransitionOnMaximized;

                public const uint DWM_BB_ENABLE = 0x00000001;
                public const uint DWM_BB_BLURREGION = 0x00000002;
                public const uint DWM_BB_TRANSITIONONMAXIMIZED = 0x00000004;
            }

            [StructLayout(LayoutKind.Sequential)]
            public struct RECT
            {
                public int left, top, right, bottom;

                public RECT(int left, int top, int right, int bottom)
                {
                    this.left = left; this.top = top;
                    this.right = right; this.bottom = bottom;
                }
            }

            public enum DWMWINDOWATTRIBUTE
            {
                DWMWA_NCRENDERING_ENABLED = 1,
                DWMWA_NCRENDERING_POLICY,
                DWMWA_TRANSITIONS_FORCEDISABLED,
                DWMWA_ALLOW_NCPAINT,
                DWMWA_CAPTION_BUTTON_BOUNDS,
                DWMWA_NONCLIENT_RTL_LAYOUT,
                DWMWA_FORCE_ICONIC_REPRESENTATION,
                DWMWA_FLIP3D_POLICY,
                DWMWA_EXTENDED_FRAME_BOUNDS,
                DWMWA_LAST
            }

            public enum DWMNCRENDERINGPOLICY
            {
                DWMNCRP_USEWINDOWSTYLE,
                DWMNCRP_DISABLED,
                DWMNCRP_ENABLED,
                DWMNCRP_LAST 
            }

        }
    }

    internal struct Margins
    {
        public int Left, Right, Top, Bottom;

        public Margins(int left, int right, int top, int bottom)
        {
            Left = left;
            Right = right;
            Top = top;
            Bottom = bottom;
        }
    }
}
