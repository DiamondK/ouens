﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using osu.Graphics.Sprites;
using osu.Helpers.Forms;

namespace osu_common.Helpers
{
    public class pForm : FadeForm
    {
        protected override void OnLoad(EventArgs e)
        {
            FixFonts();

            base.OnLoad(e);
        }

        private void FixFonts()
        {
            Font newFont = this.Font;
            Font baz = new Font(newFont.Name, 5, newFont.Style, GraphicsUnit.Point);

            //AutoScaleMode = AutoScaleMode.None;

            FixFonts(newFont, this);
        }

        protected void BringToFrontEx()
        {
            if (osu.osuMain.IsWine) return;

            Invoke(delegate
            {
                if (Focused || WindowState != FormWindowState.Normal) return;

                WindowState = FormWindowState.Minimized;
                Show();
                WindowState = FormWindowState.Normal;
                Activate();
            });
        }

        private void FixFonts(Font newFont, Control control)
        {
            Font baz = new Font(newFont.Name, 5, newFont.Style, GraphicsUnit.Point);

            foreach (Control c in control.Controls)
            {
                Font old = c.Font;
                string fontname = Blacklist(old.Name) ? newFont.Name : old.Name;

                c.Font = baz; // hack to force winforms to refresh fonts
                c.Font = new Font(fontname, old.Size, old.Style, old.Unit);

                FixFonts(newFont, c);
            }
        }

        private Font m_font;

        public void Invoke(MethodInvoker moo)
        {
            if (this.IsDisposed || !this.IsHandleCreated)
                return;

            try
            {
                base.Invoke(moo);
            }
            catch
            {
                //don't really need to handle this.
            }
        }

        public override Font Font
        {
            get
            {
                try
                {
                    if (m_font != null) return m_font;

                    if (Environment.OSVersion.Version.Major < 6)
                    {
                        //Windows 2000 (5.0), Windows XP (5.1), Windows Server 2003 and XP Pro x64 Edtion v2003 (5.2)
                        Font old = SystemFonts.DialogFont; //Tahoma hopefully
                        m_font = new Font(old.Name, 8, old.Style, GraphicsUnit.Point);
                    }
                    else
                    {
                        //Vista and above
                        Font old = SystemFonts.MessageBoxFont; //should be SegoiUI
                        m_font = new Font(old.Name, 9, old.Style, GraphicsUnit.Point);
                    }
                }
                catch // segoe ui error of doom
                {
                    Font old = base.Font;
                    m_font = new Font(old.Name, 8, old.Style, GraphicsUnit.Point);
                }
                return m_font;
            }
            set
            {
                base.Font = value;
            }
        }

        private bool Blacklist(string font_name)
        {
            font_name = font_name.ToLowerInvariant();
            if (font_name == "system") return true;
            if (font_name == "ms sans serif") return true;
            if (font_name == "microsoft sans serif") return true;
            if (font_name == "tahoma") return true;
            if (font_name == "segoe ui") return true;
            return false;
        }

        private const int WM_NCHITTEST = 0x0084;
        private const int WM_DWMCOMPOSITIONCHANGED = 0x031E;

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            switch (m.Msg)
            {
                case WM_NCHITTEST: // allows for custom draggable regions to move the window
                    if (NonClientHitTest == null) break;

                    int lparam = (int)m.LParam;
                    NonClientTestEventArgs result = new NonClientTestEventArgs((NonClientRegions)m.Result, new Point(lparam & 0xffff, lparam >> 16));
                    NonClientHitTest(this, result);
                    m.Result = (IntPtr)result.Region;
                    break;
                case WM_DWMCOMPOSITIONCHANGED:
                    if (DwmCompositionChanged == null) break;

                    DwmCompositionChanged(this, EventArgs.Empty);
                    break;
            }
        }

        internal event EventHandler<NonClientTestEventArgs> NonClientHitTest;
        internal event EventHandler DwmCompositionChanged;

        internal void ShowAtCursor(Origins origin)
        {
            moveToCursor(origin);
            this.Show();
        }

        internal void ShowAtCursor(Origins origin, IWin32Window wnd)
        {
            moveToCursor(origin);
            this.Show(wnd);
        }

        private void moveToCursor(Origins origin)
        {
            Point cursor_pos = GetCursorPos();
            switch (origin)
            {
                case Origins.TopLeft:
                case Origins.CentreLeft:
                case Origins.BottomLeft:
                    Left = cursor_pos.X;
                    break;
                case Origins.TopCentre:
                case Origins.Centre:
                case Origins.BottomCentre:
                    Left = cursor_pos.X - Width / 2;
                    break;
                case Origins.TopRight:
                case Origins.CentreRight:
                case Origins.BottomRight:
                    Left = cursor_pos.X - Width;
                    break;
            }

            switch (origin)
            {
                case Origins.TopLeft:
                case Origins.TopCentre:
                case Origins.TopRight:
                    Top = cursor_pos.Y;
                    break;
                case Origins.CentreLeft:
                case Origins.Centre:
                case Origins.CentreRight:
                    Top = cursor_pos.Y - Height / 2;
                    break;
                case Origins.BottomLeft:
                case Origins.BottomCentre:
                case Origins.BottomRight:
                    Top = cursor_pos.Y - Height;
                    break;
            }
        }

        [DllImport("user32.dll")]
        static extern bool GetCursorPos(ref Point lpPoint);

        static Point GetCursorPos()
        {
            Point result = new Point();
            if (!GetCursorPos(ref result)) throw new InvalidOperationException();
            return result;
        }

        // methods to suspend repainting on controls
        // http://stackoverflow.com/questions/487661/how-do-i-suspend-painting-for-a-control-and-its-children
        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);

        private const int WM_SETREDRAW = 11;

        internal static void SuspendDrawing(Control control)
        {
            SendMessage(control.Handle, WM_SETREDRAW, false, 0);
        }

        internal static void ResumeDrawing(Control control)
        {
            SendMessage(control.Handle, WM_SETREDRAW, true, 0);
            control.Refresh();
        }

        /*
                private bool _FontSet;
                 * 
                /// <summary>
                /// This member overrides <see cref="Control.Font"/>.
                /// </summary>
                public override Font Font
                {
                    get
                    {
                        if (_FontSet)
                            return base.Font;
                        else
                            return DefaultFont;
                    }
                    set
                    {
                        // Determine if we will need to raise the FontChanged event.
                        // We will need to raise the event manually if the "base" Font is 
                        // equal to the specified value and the font has not been set.
                        bool raiseChangedEvent = false;
                        if ((!_FontSet) && (base.Font == value))
                            raiseChangedEvent = true;

                        // Change the Font property
                        _FontSet = true;
                        base.Font = value;

                        if (raiseChangedEvent)
                            OnFontChanged(EventArgs.Empty);
                    }
                }

                /// <summary>
                /// Gets the default font of the control.
                /// </summary>
                /// <value>The default <see cref="Font"/> of the control. The default is the dialog font currently in use by the user's operating system.</value>
                public new static Font DefaultFont
                {
                    get
                    {
                        return SystemFonts.DialogFont;
                    }
                }

                /// <summary>
                /// Indicates whether the <see cref="Control.Font">Font</see> property should be persisted.
                /// </summary>
                /// <returns><see langword="true"/> if the property value has changed from its default; otherwise, <see langword="false"/>.</returns>
                private bool ShouldSerializeFont()
                {
                    if (_FontSet)
                        return true;
                    else
                        return false;
                }

                /// <summary>
                /// This member overrides <see cref="Control.ResetFont"/>.
                /// </summary>
                public override void ResetFont()
                {
                    if (_FontSet)
                    {
                        _FontSet = false;
                        OnFontChanged(EventArgs.Empty);
                    }
                }
                */

    }

    internal class NonClientTestEventArgs : EventArgs
    {
        internal NonClientTestEventArgs(NonClientRegions region, Point location)
            : base()
        {
            Region = region;
            Location = location;
        }

        internal NonClientRegions Region;
        internal readonly Point Location;
    }

    internal enum NonClientRegions
    {
        Error = -2,
        Transparent = -1,

        Nowhere = 0,

        Client = 1,
        Caption = 2,
        SysMenu = 3,
        Size = 4,
        GrowBox = 4,
        Menu = 5,

        HScroll = 6,
        VScroll = 7,

        MinButton = 8,
        Reduce = 8,
        MaxButton = 9,
        Zoom = 9,

        Left = 10,
        Right = 11,
        Top = 12,
        TopLeft = 13,
        TopRight = 14,
        Bottom = 15,
        BottomLeft = 16,
        BottomRight = 17,

        Border = 18,

        Close = 20,
        Help = 21,
    }
}