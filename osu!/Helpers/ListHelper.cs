﻿using System;
using System.Collections.Generic;
using System.Text;

namespace osu.Helpers
{
    static class ListHelper
    {
        public static T[] StableSort<T>(IList<T> values) where T : IComparable<T>
        {
            //TODO: can this be more efficient?
            KeyValuePair<int, T>[] keys = new KeyValuePair<int, T>[values.Count];
            T[] items = new T[values.Count];
            values.CopyTo(items, 0);

            for (int i = 0; i < values.Count; i++)
            {
                keys[i] = new KeyValuePair<int, T>(i, values[i]);
            }

            Array.Sort(keys, items, new StableComparer<T>());
            return items;
        }

        private sealed class StableComparer<T> : IComparer<KeyValuePair<int, T>> where T : IComparable<T>
        {
            public int Compare(KeyValuePair<int, T> a, KeyValuePair<int, T> b)
            {
                int result = a.Value.CompareTo(b.Value);
                return (result != 0) ? result : a.Key.CompareTo(b.Key);
            }
        }

        public static bool IsSorted<T>(IList<T> values, bool ascending = true) where T : IComparable<T>
        {
            for (int i = 1, count = values.Count; i < count; i++)
            {
                int comparison = values[i - 1].CompareTo(values[i]);
                if (ascending && comparison > 0 || 
                    !ascending && comparison < 0)
                    return false;
            }
            return true;
        }
    }
}
