﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace osu.Helpers
{
    [StructLayout(LayoutKind.Sequential)]
    public struct DEVMODE
    {
        private const int CCHDEVICENAME = 0x20;
        private const int CCHFORMNAME = 0x20;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 0x20)]
        public string dmDeviceName;
        public short dmSpecVersion;
        public short dmDriverVersion;
        public short dmSize;
        public short dmDriverExtra;
        public int dmFields;
        public int dmPositionX;
        public int dmPositionY;
        public ScreenOrientation dmDisplayOrientation;
        public int dmDisplayFixedOutput;
        public short dmColor;
        public short dmDuplex;
        public short dmYResolution;
        public short dmTTOption;
        public short dmCollate;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 0x20)]
        public string dmFormName;
        public short dmLogPixels;
        public int dmBitsPerPel;
        public int dmPelsWidth;
        public int dmPelsHeight;
        public int dmDisplayFlags;
        public int dmDisplayFrequency;
        public int dmICMMethod;
        public int dmICMIntent;
        public int dmMediaType;
        public int dmDitherType;
        public int dmReserved1;
        public int dmReserved2;
        public int dmPanningWidth;
        public int dmPanningHeight;
    }

    class ResolutionHelper
    {
        // Retireve screen resolution info, courtesy of
        // http://stackoverflow.com/questions/744541/how-to-list-available-video-modes-using-c
        [DllImport("user32.dll")]
        private static extern bool EnumDisplaySettings(string deviceName, int modeNum, ref DEVMODE devMode);
        const int ENUM_CURRENT_SETTINGS = -1;

        const int ENUM_REGISTRY_SETTINGS = -2;

        internal static List<Size> GetResolutions()
        {
            List<Size> results = new List<Size>();

            DEVMODE vDevMode = new DEVMODE();
            int i = 0;

            while (EnumDisplaySettings(null, i, ref vDevMode))
            {
                results.Add(new Size(vDevMode.dmPelsWidth, vDevMode.dmPelsHeight));
                i++;
            }

            return results;
        }

        internal static string ToString(Size res)
        {
            return res.Width.ToString() + 'x' + res.Height.ToString();
        }

        internal static Size FindNativeResolution()
        {
            List<Size> resolutions = GetResolutions();

            Size native_res = new Size(640, 480);
            foreach (Size res in resolutions)
                if ((res.Width > native_res.Width) || ((res.Width == native_res.Width) && (res.Height > native_res.Height))) native_res = res;

            return native_res;
        }

        // http://www.dotnetspark.com/kb/1948-change-display-settings-programmatically.aspx
        private static DEVMODE? GetCurrentSettings()
        {
            DEVMODE mode = new DEVMODE();
            mode.dmSize = (short)Marshal.SizeOf(mode);

            if (EnumDisplaySettings(null, ENUM_CURRENT_SETTINGS, ref mode) == true) // Succeeded
            {
                return mode;
            }
            else return null;
        }

        internal static int GetCurrentRefreshRate()
        {
            DEVMODE? mode = GetCurrentSettings();
            if (mode == null) return 0;
            else return ((DEVMODE)mode).dmDisplayFrequency;
        }

        internal static Size GetDesktopResolution()
        {
            MonitorInfo lpmi = new MonitorInfo() { Size = 40 };

            IntPtr monitor = MonitorFromWindow(GameBase.Game.Window.Handle, MONITOR_DEFAULTTONEAREST);

            GetMonitorInfo(monitor, ref lpmi);
            return new Size(lpmi.Monitor.Right - lpmi.Monitor.Left, lpmi.Monitor.Bottom - lpmi.Monitor.Top);
        }

        const uint MONITORINFOF_PRIMARY = 1;

        [DllImport("user32.dll")]
        static extern bool GetMonitorInfo(IntPtr hMonitor, ref MonitorInfo lpmi);

        [DllImport("user32.dll")]
        static extern IntPtr MonitorFromWindow(IntPtr hwnd, uint dwFlags);

        const int MONITOR_DEFAULTTONULL = 0;
        const int MONITOR_DEFAULTTOPRIMARY = 1;
        const int MONITOR_DEFAULTTONEAREST = 2;

        internal static Point GetDesktopPosition()
        {
            MonitorInfo lpmi = new MonitorInfo() { Size = 40 };

            IntPtr monitor = MonitorFromWindow(GameBase.Game.Window.Handle, MONITOR_DEFAULTTONEAREST);

            GetMonitorInfo(monitor, ref lpmi);

            return new Point(lpmi.Monitor.Left, lpmi.Monitor.Top);
        }
    }



    [StructLayout(LayoutKind.Sequential)]
    internal struct MonitorInfo
    {
        /// <summary>
        /// The size, in bytes, of the structure. Set this member to sizeof(MONITORINFO) (40) before calling the GetMonitorInfo function. 
        /// Doing so lets the function determine the type of structure you are passing to it.
        /// </summary>
        public int Size;

        /// <summary>
        /// A RECT structure that specifies the display monitor rectangle, expressed in virtual-screen coordinates. 
        /// Note that if the monitor is not the primary display monitor, some of the rectangle's coordinates may be negative values.
        /// </summary>
        public RectStruct Monitor;

        /// <summary>
        /// A RECT structure that specifies the work area rectangle of the display monitor that can be used by applications, 
        /// expressed in virtual-screen coordinates. Windows uses this rectangle to maximize an application on the monitor. 
        /// The rest of the area in rcMonitor contains system windows such as the task bar and side bars. 
        /// Note that if the monitor is not the primary display monitor, some of the rectangle's coordinates may be negative values.
        /// </summary>
        public RectStruct WorkArea;

        /// <summary>
        /// The attributes of the display monitor.
        /// 
        /// This member can be the following value:
        ///   1 : MONITORINFOF_PRIMARY
        /// </summary>
        public uint Flags;

        public void Init()
        {
            this.Size = 40;
        }
    }

    /// <summary>
    /// The RECT structure defines the coordinates of the upper-left and lower-right corners of a rectangle.
    /// </summary>
    /// <see cref="http://msdn.microsoft.com/en-us/library/dd162897%28VS.85%29.aspx"/>
    /// <remarks>
    /// By convention, the right and bottom edges of the rectangle are normally considered exclusive. 
    /// In other words, the pixel whose coordinates are ( right, bottom ) lies immediately outside of the the rectangle. 
    /// For example, when RECT is passed to the FillRect function, the rectangle is filled up to, but not including, 
    /// the right column and bottom row of pixels. This structure is identical to the RECTL structure.
    /// </remarks>
    [StructLayout(LayoutKind.Sequential)]
    public struct RectStruct
    {
        /// <summary>
        /// The x-coordinate of the upper-left corner of the rectangle.
        /// </summary>
        public int Left;

        /// <summary>
        /// The y-coordinate of the upper-left corner of the rectangle.
        /// </summary>
        public int Top;

        /// <summary>
        /// The x-coordinate of the lower-right corner of the rectangle.
        /// </summary>
        public int Right;

        /// <summary>
        /// The y-coordinate of the lower-right corner of the rectangle.
        /// </summary>
        public int Bottom;
    }
}
