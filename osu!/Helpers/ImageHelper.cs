﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace osu.Helpers
{
    internal static class ImageHelper
    {
        /// <summary> 
        /// Returns the image codec with the given mime type 
        /// </summary> 
        private static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats 
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec 
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];
            return null;
        }

        internal static string ResizeIfLarger(string filename, int width, int height)
        {
            string extension = Path.GetExtension(filename).Trim('.').ToLower();

            if (!(extension == "jpg" || extension == "png"))
                return filename; //Not an image

            //Initialise high quality jpeg encoder.
            ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");
            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 90L);

            using (Image i = Image.FromFile(filename))
            {
                if (i.Width <= width && i.Height <= height)
                    return filename; //Within limits - do nothing.

                int newWidth, newHeight;

                float ratio = i.Height > i.Width ? (float)width / i.Width : (float)height / i.Height;
                newWidth = (int)(i.Width * ratio);
                newHeight = (int)(i.Height * ratio);

                using (Bitmap b = new Bitmap(newWidth, newHeight))
                using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(b))
                {
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.CompositingQuality = CompositingQuality.HighQuality;
                    g.DrawImage(i, 0, 0, newWidth, newHeight);

                    string newFilename = string.Format("{0}/{1}.jpg", Path.GetDirectoryName(filename), Path.GetFileNameWithoutExtension(filename));

                    i.Dispose();

                    File.Delete(filename);
                    b.Save(newFilename, jpegCodec, encoderParams);

                    return newFilename;
                }
            }
        }
    }
}
