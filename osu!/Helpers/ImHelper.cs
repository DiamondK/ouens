﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Win32;
using osu.Configuration;
using System.Security;

namespace osu.Helpers
{
    internal static class ImHelper
    {
        private const int WM_COPYDATA = 0x4a;

        [SuppressUnmanagedCodeSecurityAttribute()]
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        internal static extern IntPtr SendMessage(IntPtr hwnd, uint wMsg, IntPtr wParam, IntPtr lParam);

        [SuppressUnmanagedCodeSecurityAttribute()]
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        internal static extern bool PostMessage(IntPtr hWnd, uint iMsg, long wParam, long lParam);

        [SuppressUnmanagedCodeSecurityAttribute()]
        [DllImport("user32.dll", EntryPoint = "FindWindowExA")]
        private static extern IntPtr FindWindowEx(IntPtr hWnd1, IntPtr hWnd2, string lpsz1, string lpsz2);

        private static IntPtr VarPtr(object e)
        {
            GCHandle handle = GCHandle.Alloc(e, GCHandleType.Pinned);
            IntPtr ptr = handle.AddrOfPinnedObject();
            handle.Free();
            return ptr;
        }

        internal static void SetAllMusic(bool enable, string verb, string title, string artist, string diff)
        {
            if (ConfigManager.sMsnIntegration)
                SetMsnMusic(enable, verb, title, artist, diff);
            if (ConfigManager.sYahooIntegration)
                SetYahooMusic(enable, verb, title, artist, diff);
        }

        internal static void SetYahooMusic(bool enable, string verb, string title, string artist, string diff)
        {
            string sUserName;

            using (RegistryKey keyYahooPager = Registry.CurrentUser.OpenSubKey("Software\\Yahoo\\Pager"))
            {
                if (keyYahooPager == null) return;
                sUserName = (string)keyYahooPager.GetValue("Yahoo! User ID");
            }

            using (
                RegistryKey key =
                    Registry.CurrentUser.OpenSubKey("Software\\Yahoo\\Pager\\profiles\\" + sUserName + "\\Custom Msgs",
                                                    true))
            {
                if (key == null) return;

                string status = string.Empty;

                if (enable)
                    status = string.Format("{0} {1} - {2} {3} (osu!)",
                                           (verb.Length > 0 ? char.ToUpper(verb[0]) + verb.Substring(1) : ""),
                                           artist,
                                           title,
                                           (diff.Length > 0 ? @"[{3}] " : ""));


                key.SetValue("5_W", status);
                byte[] statusBin = (new ASCIIEncoding()).GetBytes(status);
                key.SetValue("5_DND", statusBin);
            }

            IntPtr ptr = FindWindowEx(IntPtr.Zero, IntPtr.Zero, "YahooBuddyMain", null);

            if (ptr.ToInt32() > 0)
                PostMessage(ptr, 0x111, 0x188, 0);
        }

        internal static void SetMsnMusic(bool enable, string verb, string title, string artist, string diff)
        {
            string format = string.Format(
                "\\0Music\\0{0}\\0{1} {{0}} - {{1}} {2}({{2}})\\0{3}\\0{4}\\0osu!\\0{5}\\0\0", (enable ? "1" : "0"),
                (verb.Length > 0 ? char.ToUpper(verb[0]) + verb.Substring(1) : ""), (diff.Length > 0 ? @"[{3}] " : ""),
                title, artist, diff);

            COPYDATASTRUCT data;
            data.dwData = (IntPtr)0x547;
            data.lpData = VarPtr(format);
            data.cbData = format.Length * 2;

            IntPtr ptr = FindWindowEx(IntPtr.Zero, IntPtr.Zero, "MsnMsgrUIManager", null);

            if (ptr.ToInt32() > 0)
                SendMessage(ptr, WM_COPYDATA, IntPtr.Zero, VarPtr(data));
        }

        #region Nested type: COPYDATASTRUCT

        [StructLayout(LayoutKind.Sequential)]
        private struct COPYDATASTRUCT
        {
            public IntPtr dwData;
            public int cbData;
            public IntPtr lpData;
        }

        #endregion
    }
}