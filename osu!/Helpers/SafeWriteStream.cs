﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace osu.Helpers
{
    class SafeWriteStream : FileStream
    {
        static object SafeLock = new object(); //ensure we are only ever writing one stream to disk at a time, application wide.

        string finalFilename;
        string temporaryFilename;
        public SafeWriteStream(string filename)
            : base(filename + "." + DateTime.Now.Ticks, FileMode.Create)
        {
            temporaryFilename = this.Name;
            finalFilename = filename;
        }

        ~SafeWriteStream()
        {
            if (!isDisposed) Dispose();
        }

        bool isDisposed;
        protected override void Dispose(bool disposing)
        {
            if (isDisposed) return;
            isDisposed = true;

            base.Dispose(disposing);

            lock (SafeLock)
            {
                if (!File.Exists(temporaryFilename)) return;

                try
                {
                    File.Delete(finalFilename);
                    File.Move(temporaryFilename, finalFilename);
                }
                catch
                {
                }
            }
        }
    }
}
