﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Microsoft.Win32;

namespace osu.Helpers
{
    class Uninstaller
    {
        internal static void RemoveUninstaller()
        {
            Guid guid = GetUninstallId();
            if (guid == Guid.Empty) return;

            using (RegistryKey parent = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", true))
            {
                try
                {
                    parent.DeleteSubKeyTree(guid.ToString(@"B"));
                }
                catch
                {
                }
            }
        }

        internal static bool CreateUninstaller()
        {
            Guid guid = GetUninstallId();
            if (guid == Guid.Empty) return false;

            try
            {
                string guidText = guid.ToString(@"B");

                using (RegistryKey parent = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", false))
                {
                    using (RegistryKey check = parent.OpenSubKey(guidText))
                    {
                        if (check != null && (string)check.GetValue(@"DisplayIcon") == osuMain.FullPath)
                            return true;
                    }
                }

                try
                {
                    using (RegistryKey parent = Registry.ClassesRoot.OpenSubKey(@"Installer\Products", true))
                    {
                        using (RegistryKey check = parent.OpenSubKey(@"6242953CE135011419D1FBCE2EEC82C4"))
                            if (check != null)
                            {
                                //old installer
                                parent.DeleteSubKeyTree(@"6242953CE135011419D1FBCE2EEC82C4");
                            }
                    }
                }
                catch { }

                using (RegistryKey parent = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", true))
                {
                    if (parent == null)
                        return false;

                    try
                    {
                        using (RegistryKey check = parent.OpenSubKey(@"{C3592426-531E-4110-911D-BFECE2CE284C}"))
                            if (check != null)
                            {
                                //old uninstaller
                                parent.DeleteSubKeyTree(@"{C3592426-531E-4110-911D-BFECE2CE284C}");
                            }
                    }
                    catch { }

                    using (RegistryKey key = parent.OpenSubKey(guidText, true) ?? parent.CreateSubKey(guidText))
                    {
                        if (key == null) return false;

                        key.SetValue(@"DisplayName", @"osu!");
                        key.SetValue(@"ApplicationVersion", @"latest");
                        key.SetValue(@"Publisher", @"ppy Pty Ltd");
                        key.SetValue(@"DisplayIcon", osuMain.FullPath);
                        key.SetValue(@"DisplayVersion", @"latest");
                        key.SetValue(@"EstimatedSize", 125952, RegistryValueKind.DWord);
                        key.SetValue(@"NoModify", 1, RegistryValueKind.DWord);
                        key.SetValue(@"NoRepair", 1, RegistryValueKind.DWord);
                        key.SetValue(@"URLInfoAbout", "http://osu.ppy.sh");
                        key.SetValue(@"Contact", @"osu@ppy.sh");
                        key.SetValue(@"InstallDate", DateTime.Now.ToString(@"yyyyMMdd"));
                        key.SetValue(@"UninstallString", osuMain.FullPath + @" -uninstall");
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        internal static Guid GetUninstallId()
        {
            try
            {
                using (RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\\osu!", false))
                    if (key != null)
                    {
                        string val = key.GetValue(@"UninstallID") as string;
                        if (!string.IsNullOrEmpty(val))
                            return new Guid((string)key.GetValue(@"UninstallID"));
                    }


                using (RegistryKey key = Registry.CurrentUser.CreateSubKey(@"Software\\osu!"))
                {
                    Guid guid = Guid.NewGuid();
                    key.SetValue(@"UninstallID", guid.ToString());
                    return guid;
                }
            }
            catch
            {
            }

            return Guid.Empty;
        }
    }
}
