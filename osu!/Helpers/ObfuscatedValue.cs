﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using osu_common.Helpers;

namespace osu.Helpers
{
    public interface Obfuscator<T>
    {
        T Obfuscate(T CurrentValue, int randomvalue);
        T DeObfuscate(T Obfuscated, int randomvalue);

    }
    public class DefaultObfuscator<T> : Obfuscator<T>
    {
        public T Obfuscate(T currentvalue, int salt, bool reverse = false)
        {
            Type valType = currentvalue.GetType();

            if (valType == typeof(string))
            {
                string casted = Convert.ToString(currentvalue);

                char[] mangled = new char[casted.Length];

                for (int i = 0; i < casted.Length; i++)
                    mangled[i] = (char)(casted[i] ^ salt);

                return (T)Convert.ChangeType(new string(mangled), valType);
            }
            else if (valType == typeof(int))
            {
                return (T)(object)(((int)Convert.ChangeType((object)currentvalue, typeof(int))) ^ salt);
            }
            else if (valType == typeof(double))
            {
                return (T)(object)(((double)Convert.ChangeType((object)currentvalue, typeof(double))) * (reverse ? 1.0 / salt : salt));
            }
            else if (valType == typeof(float))
            {
                return (T)(object)(((float)Convert.ChangeType((object)currentvalue, typeof(float))) * (reverse ? 1.0f / salt : salt));
            }
            else if (valType.BaseType == typeof(Enum))
            {
                return (T)(object)(((int)Convert.ChangeType((object)currentvalue, typeof(int))) ^ salt);
            }

            //use op_ExclusiveOr method via reflection.
            Type acquireType = ((Object)currentvalue).GetType();
            var gotmethod = acquireType.GetMethod("op_ExclusiveOr");
            return (T)gotmethod.Invoke((Object)currentvalue, new object[] { (Object)salt });
        }

        public T Obfuscate(T currentvalue, int salt)
        {
            return Obfuscate(currentvalue, salt, false);
        }

        public T DeObfuscate(T Obfuscated, int randomvalue)
        {
            return Obfuscate(Obfuscated, randomvalue, true);
        }
    }

    public class Obfuscated<T>
    {
        public static FastRandom random = new FastRandom();
        private T _Value;
        private int Randomvalue = random.Next(int.MinValue, int.MaxValue);
        private Obfuscator<T> obfus = new DefaultObfuscator<T>();

        internal int c = 0;
        internal bool allowDestruction = true;

        ~Obfuscated()
        {
            switch (GameBase.Mode)
            {
                case OsuModes.Update:
                case OsuModes.Exit:
                case OsuModes.Unknown:
                    break;
            }

            if (!allowDestruction)
                GameBase.Scheduler.AddDelayed(delegate { Environment.Exit(-1); }, 1000);
        }

        public T Value
        {
            get { return obfus.DeObfuscate(_Value, Randomvalue); }
            set
            {
                Randomvalue = (int)random.NextUInt();
                _Value = obfus.Obfuscate(value, Randomvalue);
                c++;
            }
        }
        public static implicit operator T(Obfuscated<T> value)
        {
            return value.Value;
        }
        public static implicit operator Obfuscated<T>(T value)
        {
            return new Obfuscated<T>(value);
        }

        public Obfuscated(T value)
        {
            Value = value;
        }
        public Obfuscated()
            : this(new Object[] { })
        {
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public Obfuscated(params Object[] cParams)
        {
            //we want to implicitly call a constructor for the given type.
            //first, get the type of our T.
            Type usetype = typeof(T);
            //now, find a matching constructor...
            //create a Type[] array corresponding to the parameter types.
            List<Type> types = new List<Type>();
            if (cParams.Length > 0)
                foreach (Object o in cParams)
                    types.Add(o.GetType());
            else
                types = null;

            ConstructorInfo ci = usetype.GetConstructor(types.ToArray());
            T result = (T)ci.Invoke(cParams);
            _Value = result;
        }
    }
}
