﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Online.Social;

namespace osu.Helpers
{
    internal static class Logger
    {
        internal static bool Enabled;

        internal static void Log(string message)
        {
            if (!Enabled || !ChatEngine.Initialized) return;
            ChatEngine.HandleMessage(message);
        }

        internal static void Log(string message, params string[] arguments)
        {
            Log(string.Format(message, arguments));
        }
    }
}
