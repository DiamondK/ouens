using System;
using System.IO;

static class InterpreterConsole
{
    const string prompt = "> ";
    public static void Run()
    {
        Interpreter.Console = new TextConsole();
        Interpreter interp = new Interpreter();
        interp.ReadIncludeFile(@"Helpers\Interpreter\csi.csi");
        InterpreterUtils.Write(prompt);
        while (interp.ProcessLine(InterpreterUtils.ReadLine()))
            InterpreterUtils.Write(prompt);
    }
}

class TextConsole : IConsole
{
    public string ReadLine()
    {
        return Console.In.ReadLine();
    }

    public void Write(string s)
    {
        Console.Write(s);
    }
}
