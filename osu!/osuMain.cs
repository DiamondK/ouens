﻿using System;
using System.Diagnostics;
using System.IO;
using System.Resources;
using System.Threading;
using System.Windows.Forms;
using osu.Configuration;
using osu.GameModes.Other;
using osu.Helpers;
using osu.Helpers.Forms;
using osu_common.Helpers;
using osu_common.Updater;

namespace osu
{
    internal static class osuMain
    {
        internal static bool AllowMultipleInstances;

#if DEBUG
        internal static Guid ClientGuid = new Guid(@"{1248FAA3-79D8-4446-9D0A-58CAFFA49279}");
#elif !Public
        internal static Guid ClientGuid = new Guid(@"{2F4B6B91-D8D6-4615-8718-202E808B355A}");
#else
        internal static Guid ClientGuid = new Guid(@"{20F7B388-7444-42CC-9388-C23275781FF8}");
#endif

        private static string fullPath;
        internal static string FullPath { get { return fullPath ?? (fullPath = Application.ExecutablePath); } }

        internal static string FilenameWithoutExtension { get { return Path.GetFileNameWithoutExtension(FullPath); } }

        internal static string Filename { get { return Path.GetFileName(FullPath); } }

        /// <summary>
        /// True after we've started to display a form or game window.
        /// </summary>
        public static bool Started;

        /// <summary>
        /// Should we restart osu! after quitting?
        /// </summary>
        internal static bool Restart;

        internal static string[] Args;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Args = Environment.GetCommandLineArgs();
            Environment.CurrentDirectory = Path.GetDirectoryName(FullPath);

            Application.SetCompatibleTextRenderingDefault(false);
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;

            bool forceUpdate = File.Exists(@".require_update") || File.Exists(@"help.txt");

            //check we have the dlls required
            string[] requiredDlls = {
                    //native
                    @"avcodec-51.dll",
                    @"avformat-52.dll",
                    @"avutil-49.dll",
                    @"bass.dll",
                    @"bass_fx.dll",
                    @"d3dx9_31.dll",
                    @"pthreadGC2.dll",
                    @"x3daudio1_1.dll",

                    //managed
                    @"Microsoft.Xna.Framework.dll",
                    @"Microsoft.Ink.dll",

                    @"osu.dll",                    
                    @"osu!gameplay.dll",
                    @"osu!ui.dll",
            };

            if (Args.Length > 1)
            {
                //handle some very early argument options
                switch (Args[1])
                {
                    case @"-fix-framework":
                        //a very special case to fix framework DLLs that could cause an endless loop if corrupt.
                        foreach (string f in requiredDlls)
                            GeneralHelper.FileDelete(f);
                        GeneralHelper.FileDelete(@"Microsoft.Xna.Framework.dll");
                        forceUpdate = true;
                        break;
                    case @"-repair":
                        forceUpdate = true;
                        break;
                    case @"-allowuserwrites":
                        string path = string.Empty;
                        for (int i = 2; i < Args.Length; i++)
                            path += osuMain.Args[i] + @" ";
                        SetDirectoryPermissions(path.Trim());
                        return;
                    case @"-uninstall":
                        new Maintenance(MaintenanceType.Uninstall).ShowDialog();
                        return;
                }

            }

            if (Filename == @"osu!test.exe")
            {
                GeneralHelper.FileDelete(@"osu!.exe");
                File.Move(@"osu!test.exe", @"osu!.exe");

                //we are running a test build but haven't yet been migrated back to the single-executable architecture.
                MessageBox.Show("osu! test build no longer runs as a separate executable.\nplease update your shortcuts to point to osu!.exe!", "osu! is changing!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                Process.Start(@"osu!.exe");
                return;
            }

            //Check if we are forcing an update
            if (forceUpdate)
            {
                GeneralHelper.FileDelete(@".require_update");
                GeneralHelper.FileDelete(@"help.txt");

                if (new Maintenance(MaintenanceType.Update).ShowDialog() == DialogResult.Cancel)
                    return;

                Process.Start(@"osu!.exe");
                return;
            }

            //Check we have required DLLs
            foreach (string dll in requiredDlls)
            {
                if (!File.Exists(dll))
                {
                    if (new Maintenance(MaintenanceType.Update).ShowDialog() == DialogResult.Cancel)
                        return;
                    break;
                }
            }

#if !DEBUG
            foreach (string dll in Directory.GetFiles(@".", "*.dll"))
            {
                if ((File.GetAttributes(dll) & FileAttributes.Hidden) == 0)
                    File.SetAttributes(dll, FileAttributes.Hidden);
            }

            foreach (string file in Directory.GetFiles(@".", @"osu!test*"))
            {
                if (Path.GetExtension(file) != @".exe")
                    GeneralHelper.FileDelete(file);
            }

            if (Directory.Exists(@"Data"))
            {
                if ((File.GetAttributes(@"Data") & FileAttributes.Hidden) == 0)
                    File.SetAttributes(@"Data", FileAttributes.Hidden);
            }
#endif

            Cleanup();

            //osume.exe (the old updater) is no longer required by osu!, and is therefore deleted by default now.
            GeneralHelper.FileDelete(@"osume.exe");

            if ((forceUpdate || Args.Length < 2 || Args[1] == @"-go") && Directory.Exists(CommonUpdater.STAGING_COMPLETE_FOLDER))
            {
                if (!CommonUpdater.MoveInPlace(true))
                {
                    //we failed once, so let's clear all update files for safety measures.
                    CommonUpdater.Reset();
                }

                Process.Start(@"osu!.exe", @"-go");
                ExitImmediately();
                return;
            }

#if Release
            if (Args.Length < 2)
            {
                new Maintenance(MaintenanceType.Update).ShowDialog();
                Process.Start(@"osu!.exe", @"-go");
                return;
            }
#endif

            startup(Args);
        }

        private static void Cleanup()
        {
            if (!Directory.Exists(GeneralHelper.CLEANUP_DIRECTORY)) return;

            foreach (string file in Directory.GetFiles(GeneralHelper.CLEANUP_DIRECTORY))
            {
                try
                {
                    File.Delete(file);
                }
                catch { }
            }

            try
            {
                Directory.Delete(GeneralHelper.CLEANUP_DIRECTORY);
            }
            catch { }
        }

        private static void startup(string[] args)
        {
            //we can use the argument as the full path if it contains folders.
            //this is faster but not always available.
            string path = args[0].Trim('"');

            if (path.IndexOf(Path.DirectorySeparatorChar) >= 0)
                fullPath = path;

            if (args.Length > 1)
                if (HandleArgs(ref args)) return;

            bool hasTournamentConfig = File.Exists(Tournament.CONFIG_FILENAME);

            string fileArgs = args.Length > 1 ? string.Join(@" ", args, 1, args.Length - 1) : null;

            bool allowMultipleInstances = fileArgs != null || AllowMultipleInstances || hasTournamentConfig;

            try
            {
                try
                {
                    if (!allowMultipleInstances)
                    {
                        //attempt a wake-up if an existing osu! client is running.
                        InterProcessOsu osu = IPC.GetRunningOsu();
                        if (osu != null && osu.WakeUp())
                            return;
                    }
                }
                catch { }

                using (SingleInstance instance = new SingleInstance(ClientGuid, allowMultipleInstances ? -1 : 5000))
                {
#if !DEBUG
                try
                {
#endif

                    if (!instance.IsFirstInstance && fileArgs != null)
                    {
                        IPC.HandleArguments(fileArgs);
                        return;
                    }

                    RunGame(fileArgs, instance.IsFirstInstance, hasTournamentConfig && !GameBase.Tournament);
#if !DEBUG
                }
                catch (BadImageFormatException)
                {
                    Repair(true);
                }
                catch { }
#endif
                }
            }
            catch (TooManyInstancesException)
            {
            }

            exitOrRestart();
        }

        /// <summary>
        /// Handles command line parameters.
        /// </summary>
        /// <returns>true if we handled this execution and want to exit.</returns>
        private static bool HandleArgs(ref string[] args)
        {
            switch (args[1])
            {
                case @"-setpermissions":
                    Environment.CurrentDirectory = Path.GetDirectoryName(FullPath);
                    GameBase.SetPermissions(false);
                    return true;
                case @"-setassociations":
                    Environment.CurrentDirectory = Path.GetDirectoryName(FullPath);
                    GameBase.SetPermissions(true);
                    return true;
                case @"-benchmark":
                    GameBase.BenchmarkMode = true;
                    return false;
                case @"-spectateclient":
                    GameBase.Tournament = true;
                    GameBase.TournamentClientId = Int32.Parse(args[2]);
                    GameBase.IpcChannelName = @"osu!-spectator-" + args[2];
                    args = new string[] { };
                    return false;
                case @"-multi":
                    AllowMultipleInstances = true;
                    args = new string[] { };
                    return false;
                default:
                    return false;
            }
        }

        /// <summary>
        /// This code is run in a separate method as it *requires* XNA framework.
        /// Anything before this point can be executed without XNA present (updater etc.)
        /// </summary>
        private static void RunGame(string fileArgs, bool isFirstInstance, bool isTournamentManager)
        {
            if (fileArgs != null && fileArgs.StartsWith(@"-"))
                fileArgs = null;

#if !DEBUG
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
#endif

            GameBase.CheckForUpdates();

            GameBase game = null;

#if !DEBUG
            try
            {
#endif

            if (isTournamentManager)
            {
                GameBase.Tournament = true;
                GameBase.TournamentManager = true;
                GameBase.GameQueuedState = OsuModes.Tourney;
            }

            if (!osuMain.IsWine && !GameBase.Tournament)
                ShowSplash();

            Started = true;

            Process.GetCurrentProcess().PriorityBoostEnabled = true;

            game = new GameBase(fileArgs);
            GameBase.IsFirstInstance = isFirstInstance;
            game.Run();
#if !DEBUG
            }
            catch (Microsoft.Xna.Framework.NoSuitableGraphicsDeviceException)
            {
                if (GameBase.TotalFramesRendered < 10)
                {
                    if (game != null) game.IsMouseVisible = true;
                    if (DialogResult.Yes ==
                        TopMostMessageBox.Show(
                            "Your graphics card does not seem to support Shader Model 1.1+.\nIf you have successfully run osu! in the past, this may indicate that your graphics card is low on memory, in which case you should close some other programs or restart your computer.\nWould you like to swap to OpenGl mode and continue anyway?",
                            "video card error", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1))
                    {
                        ConfigManager.sRenderer.Value = @"opengl";
                        ConfigManager.SaveConfig();
                        Restart = true;
                    }
                }
                else
                    Restart = true;
            }
            catch (Microsoft.Xna.Framework.Graphics.OutOfVideoMemoryException)
            {
                if (game != null) game.IsMouseVisible = true;
                new ErrorDialog("Your graphics card has run out of memory.  If you are using a custom skin, please try reverting to the default to save on texture memory. Disabling in-game video may also help!", true).ShowDialog();
                Restart = true;
            }
            catch (BadImageFormatException)
            {
                Repair(true);
            }
            catch (MissingManifestResourceException)
            {
                Repair(true);
            }
            catch (UnauthorizedAccessException)
            {
                Restart = true;
            }
            catch (TypeInitializationException)
            {
                Repair(true);
            }
            catch (Exception e)
            {
                if (game != null) game.IsMouseVisible = true;
                new ErrorDialog(e, true).ShowDialog();
            }
#endif
            osu.Online.BanchoClient.Exit();
        }

        private static void ShowSplash()
        {
            try
            {
                System.Drawing.Bitmap img = osu.Properties.Resources.splash;
                Thread t = new Thread(() =>
                {
                    try
                    {
                        Application.Run(new SplashScreen(img));
                    }
                    catch { }
                });
                t.IsBackground = true;
                t.Start();
            }
            catch { }
        }

        static System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            //TopMostMessageBox.ShowMessage(@"Couldn't resolve " + args.Name.ToString() + ".\May need repair...");
            return null;
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.ExceptionObject is BadImageFormatException || e.ExceptionObject is TypeInitializationException)
                osuMain.Repair(true);

            GameBase.Scheduler.Add(delegate
            {
                GameBase.MenuActive = true;
                GameBase.Game.SkipDrawcall = true;

                ErrorDialog dialog = new ErrorDialog(e.ExceptionObject as Exception, true);
                dialog.ShowDialog();

                osu.Online.BanchoClient.Exit();

                Environment.Exit(-1);
            }, false);

            while (true)
                Thread.Sleep(60000);
        }

        static bool exitOrRestartTriggered;

        private static void exitOrRestart(bool force = false)
        {
            if (exitOrRestartTriggered) return;
            exitOrRestartTriggered = true;

            if (GameBase.UpdateState == UpdateStates.NeedsRestart && GameBase.Mode != OsuModes.Exit)
            {
                try
                {
                    Process.Start(@"osu!.exe");
                    return;
                }
                catch { }
            }
            else if (Restart || force)
            {
                Process.Start(@"osu!.exe");
            }
        }

        internal static void KillStuckProcesses(string name, bool forceful)
        {
            Process myproc = new Process();
            try
            {
                int handle = Process.GetCurrentProcess().Id;
                foreach (Process proc in Process.GetProcessesByName(name))
                    if (proc.Id != handle && !proc.CloseMainWindow())
                        if (forceful) proc.Kill();
            }
            catch
            {
            }
        }

        internal static void ForceUpdate()
        {
            if (GameBase.UpdateState != UpdateStates.NeedsRestart)
                File.WriteAllBytes(@".require_update", new byte[] { });
            exitOrRestart(true);
        }

        internal static void Repair(bool deleteFramework = false)
        {
            //there's an issue with one of the framework DLLs.
            //this needs special handling at a very early stage of execution.
            Process.Start(@"osu!.exe", deleteFramework ? @"-fix-framework" : @"-repair");
            osuMain.ExitImmediately();
        }

        internal static void ExitImmediately()
        {
            Process.GetCurrentProcess().Kill();
        }

        internal static bool Elevate(string arguments = null, bool waitForExit = false)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.UseShellExecute = true;
            startInfo.WorkingDirectory = Environment.CurrentDirectory;
            startInfo.FileName = osuMain.FullPath;
            if (!string.IsNullOrEmpty(arguments)) startInfo.Arguments = arguments;
            startInfo.Verb = "runas";

            try
            {
                Process pr = Process.Start(startInfo);
                if (waitForExit)
                {
                    if (pr != null && !pr.WaitForExit(60000))
                    {
                        TopMostMessageBox.Show(@"osu! took a bit too long while trying to perform an elevated operation (" + (arguments ?? @"unknown") + @").\nPlease report this.");
                        return false;
                    }

                    return true;
                }
            }
            catch
            {
                return false;
            }

            ExitImmediately();
            return true;
        }

        internal static bool IsElevated
        {
            get
            {
                return new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent())
                    .IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator);
            }
        }

        internal static void SetDirectoryPermissions(string directory = null)
        {
            if (string.IsNullOrEmpty(directory)) directory = Environment.CurrentDirectory;

            if (!IsElevated)
            {
                Elevate(@"-allowuserwrites " + directory, true);
                return;
            }

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            GeneralHelper.SetDirectoryPermissions(directory);
        }

        #region Wine Support

        [System.Runtime.InteropServices.DllImport(@"kernel32.dll", CharSet = System.Runtime.InteropServices.CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        internal static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [System.Runtime.InteropServices.DllImport(@"kernel32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        public static extern IntPtr GetModuleHandle(string lpModuleName);

        static bool? isWine;
        internal static bool IsWine
        {
            get
            {
                if (isWine.HasValue) return isWine.Value;

                IntPtr hModule = GetModuleHandle(@"ntdll.dll");
                if (hModule == IntPtr.Zero)
                    isWine = false;
                else
                {
                    IntPtr fptr = GetProcAddress(hModule, @"wine_get_version");
                    if (fptr == IntPtr.Zero)
                        isWine = false;
                    else
                        isWine = true;
                }

                return isWine.Value;
            }
        }

        #endregion
    }
}
