#region

using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Select;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input.Handlers;
using osu.Online;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu_common.Helpers;
using osu_common.Libraries.NetLib;
using osudata;
using osu.Helpers;
using osu.GameModes.Play.Components;
using osu.Input;
using osu.GameModes.Play.Rulesets.Mania;
using osu.Graphics;
using osu.GameplayElements;
using osu.Online.Social;

#endregion

namespace osu.GameModes.Online
{
    internal class MatchSetup : DrawableGameComponent
    {
        private static bool hasSong;
        private static bool HostChangePending;
        public static bool IsHost;
        public static ClientSideMatch Match;
        private static bool SongChangePending;
        private static bool UpdatePending;
        private readonly List<pSprite> modSprites = new List<pSprite>();
        private readonly pSprite[] slotBackground = new pSprite[bMatch.MAX_PLAYERS];
        private readonly pSprite[] slotLock = new pSprite[bMatch.MAX_PLAYERS];
        private readonly pSprite[] slotStatus = new pSprite[bMatch.MAX_PLAYERS];
        private readonly pText[] slotText = new pText[bMatch.MAX_PLAYERS];
        private readonly pText[] slotTextInfo = new pText[bMatch.MAX_PLAYERS];
        private bool allReady;
        private pButton buttonPassword;
        private pButton buttonBeatmap;
        private pButton buttonLeave;
        private pButton buttonMods;
        private pButton buttonStart;
        private pTextBox detailsGameName;
        private pDropdown gameScoringTypeDropdown;
        private pDropdown gameTeamTypeDropdown;
        private pDropdown gameComboColourPicker;
        private pText gameComboColourText;
        private pText headerText2;
        private Mods oldMods;
        private string oldPassword;
        private bool paused;
        private bool PendingMapUpdate;
        private SpriteManager spriteManager = new SpriteManager(true);
        pScrollableArea scrollableSlots = new pScrollableArea(new Rectangle(2, 85, (int)(285 * GameBase.WidthWidescreenRatio), 25 * 8), Vector2.Zero, true);

        private BeatmapTreeItem treeItem;

        private bool TeamMode;
        internal static bool PendingScoreUpdate;
        internal static Color TagComboColour = Color.TransparentWhite;

        public MatchSetup(Game game)
            : base(game)
        {
            Instance = this;
        }

        public override void Initialize()
        {
            scrollableSlots.ScrollMultiplier = 0.5f;

            BeatmapTreeItem.LoadSkinElements();

            if (Match == null)
            {
                GameBase.ChangeModeInstant(OsuModes.Lobby);
                return;
            }

            confirmExitByEscape = false;

            //Fix a case where accepting a game invite could leave ReplayMode enabled.
            //Not the tidiest fix, but should work correctly for now.
            lock (StreamingManager.LockReplayScore)
            {
                InputManager.ReplayMode = false;
                InputManager.ReplayStreaming = false;
            }

            KeyboardHandler.OnKeyPressed += KeyboardHandler_OnKeyPressed;

            if (Lobby.Status == LobbyStatus.Play)
                Lobby.Status = LobbyStatus.Setup;

            pText desc = new pText(LocalisationManager.GetString(OsuString.MatchSetup_TeamMode), 12, new Vector2(GameBase.WindowWidthScaled - 115, 220), 0.955F, true, Color.White);
            desc.TextBold = true;
            desc.Origin = Origins.TopRight;
            spriteManager.Add(desc);

            gameTeamTypeDropdown = new pDropdown(spriteManager, string.Empty, new Vector2(GameBase.WindowWidthScaled - 110, 220), 90, 0.98f);
            int m = 0;
            foreach (string s in Enum.GetNames(typeof(MatchTeamTypes)))
                gameTeamTypeDropdown.AddOption(GeneralHelper.FormatEnum(s), (MatchTeamTypes)m++);
            gameTeamTypeDropdown.OnSelect += gameTeamTypeDropdown_OnSelect;

            desc = new pText(LocalisationManager.GetString(OsuString.MatchSetup_WinCondition), 12, new Vector2(GameBase.WindowWidthScaled - 115, 240), 0.955F, true, Color.White);
            desc.TextBold = true;
            desc.Origin = Origins.TopRight;
            spriteManager.Add(desc);

            gameScoringTypeDropdown = new pDropdown(spriteManager, string.Empty, new Vector2(GameBase.WindowWidthScaled - 110, 240), 90, 0.98f);
            m = 0;
            foreach (string s in Enum.GetNames(typeof(MatchScoringTypes)))
                gameScoringTypeDropdown.AddOption(GeneralHelper.FormatEnum(s), (MatchScoringTypes)m++);
            gameScoringTypeDropdown.OnSelect += gameScoringTypeDropdown_OnSelect;

            gameComboColourText = new pText(LocalisationManager.GetString(OsuString.MatchSetup_ComboColour), 12, new Vector2(GameBase.WindowWidthScaled - 115, 260), 0.955F, true, Color.TransparentWhite);
            gameComboColourText.Origin = Origins.TopRight;
            gameComboColourText.TextBold = true;
            spriteManager.Add(gameComboColourText);

            gameComboColourPicker = new pDropdown(spriteManager, string.Empty, new Vector2(GameBase.WindowWidthScaled - 110, 260), 60, 0.98f, true);

            gameComboColourPicker.SpriteMainBox.ToolTip = LocalisationManager.GetString(OsuString.MatchSetup_ComboColour_Tooltip);

            gameComboColourPicker.AddOption(LocalisationManager.GetString(OsuString.General_Default), Color.TransparentWhite);
            gameComboColourPicker.AddOption(@" ", Color.OrangeRed, ColourHelper.Darken(Color.OrangeRed, 0.1f));
            gameComboColourPicker.AddOption(@" ", Color.BlueViolet, ColourHelper.Darken(Color.BlueViolet, 0.1f));
            gameComboColourPicker.AddOption(@" ", Color.YellowGreen, ColourHelper.Darken(Color.YellowGreen, 0.1f));
            gameComboColourPicker.AddOption(@" ", Color.Yellow, ColourHelper.Darken(Color.Yellow, 0.1f));
            gameComboColourPicker.AddOption(@" ", Color.LightPink, ColourHelper.Darken(Color.PeachPuff, 0.1f));


            gameComboColourPicker.SetSelected(TagComboColour, true);
            gameComboColourPicker.OnSelect += gameComboColorPicker_OnSelect;
            gameComboColourPicker.SpriteCollection.ForEach(p => p.Hide());

            pText headerText = new pText(LocalisationManager.GetString(OsuString.MatchSetup_Header), 30, new Vector2(0, 0), 0.955F, true, Color.White);
            headerText.TextShadow = true;
            spriteManager.Add(headerText);

            headerText2 = new pText(IsHost ? LocalisationManager.GetString(OsuString.MatchSetup_YouAreTheHost) : LocalisationManager.GetString(OsuString.MatchSetup_YouAreAPlayer), 16, new Vector2(4, 26), 0.955F, true, Color.White);
            spriteManager.Add(headerText2);

            pSprite topBlackPiece =
                new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft,
                        Clocks.Game, new Vector2(0, 0), 0.68F, true, Color.TransparentBlack);
            topBlackPiece.Alpha = 1;
            topBlackPiece.VectorScale.X = GameBase.WindowWidthScaled;
            topBlackPiece.VectorScale.Y = 52;
            topBlackPiece.Scale = 1.6f;
            spriteManager.Add(topBlackPiece);

            buttonStart = new pButton(LocalisationManager.GetString(OsuString.MatchSetup_Ready), new Vector2(GameBase.WindowWidthScaled / 2 + 10 * GameBase.WidthWidescreenRatio, 285), new Vector2(300 * GameBase.WidthWidescreenRatio, 30),
                0.92f, new Color(99, 139, 228), OnReady);
            spriteManager.Add(buttonStart.SpriteCollection);

            buttonLeave = new pButton(LocalisationManager.GetString(OsuString.MatchSetup_LeaveMatch), new Vector2(GameBase.WindowWidthScaled / 2 - 310 * GameBase.WidthWidescreenRatio, 285), new Vector2(300 * GameBase.WidthWidescreenRatio, 30),
                0.92f, new Color(235, 160, 62), OnLeaveGame);
            spriteManager.Add(buttonLeave.SpriteCollection);

            pText gameNameSprite = new pText(LocalisationManager.GetString(OsuString.MatchSetup_GameName), 18, new Vector2(GameBase.WindowWidthScaled - 335, 62), 0.90f, true, Color.White);
            gameNameSprite.TextBold = true;
            spriteManager.Add(gameNameSprite);

            buttonPassword = new pButton(LocalisationManager.GetString(OsuString.MatchSetup_ChangePassword), new Vector2(GameBase.WindowWidthScaled - 226, 62), new Vector2(80, 18), 0.90f,
                                        Color.YellowGreen, OnChangePassword);
            spriteManager.Add(buttonPassword.SpriteCollection);

            pText beatmapTitleSprite = new pText(LocalisationManager.GetString(OsuString.MatchSetup_Beatmap), 18, new Vector2(GameBase.WindowWidthScaled - 335, 112), 0.90f, true, Color.White);
            beatmapTitleSprite.TextBold = true;
            spriteManager.Add(beatmapTitleSprite);

            buttonBeatmap = new pButton(LocalisationManager.GetString(OsuString.MatchSetup_ChangeBeatmap), new Vector2(GameBase.WindowWidthScaled - 256, 112), new Vector2(80, 18), 0.90f,
                                        Color.YellowGreen, OnSelectBeatmap);
            spriteManager.Add(buttonBeatmap.SpriteCollection);

            pText modsTitleSprite = new pText(LocalisationManager.GetString(OsuString.MatchSetup_Mods), 18, new Vector2(GameBase.WindowWidthScaled - 335, 196), 0.90f, true, Color.White);
            modsTitleSprite.TextBold = true;
            spriteManager.Add(modsTitleSprite);

            buttonMods = new pButton(LocalisationManager.GetString(OsuString.MatchSetup_ChangeBeatmap), new Vector2(GameBase.WindowWidthScaled - 288, 196), new Vector2(80, 18), 0.90f,
                                     Color.YellowGreen, OnSelectMods);
            spriteManager.Add(buttonMods.SpriteCollection);

            freeMods = new pCheckbox(LocalisationManager.GetString(OsuString.MatchSetup_FreeMods), new Vector2(GameBase.WindowWidthScaled - 200, 196), 1, false);

            freeMods.OnCheckChanged += delegate(object sender, bool status)
            {
                if (!allowChangeMod())
                    return;
                if (status)
                    Match.specialModes |= MultiSpecialModes.FreeMod;
                else
                {
                    Match.slotMods = new Mods[bMatch.MAX_PLAYERS];
                    Match.specialModes &= ~MultiSpecialModes.FreeMod;
                }

                BanchoClient.SendRequest(RequestType.Osu_MatchChangeSettings, Match);
            };

            if (OsuCommon.ProtocolVersion >= 17)
                spriteManager.Add(freeMods.SpriteCollection);

            currentPlayersInfo = new pText(LocalisationManager.GetString(OsuString.MatchSetup_CurrentPlayers), 18, new Vector2(21 * GameBase.WidthWidescreenRatio, 60), 0.90f, true, Color.White);
            currentPlayersInfo.TextBold = true;
            spriteManager.Add(currentPlayersInfo);

            for (int i = 0; i < bMatch.MAX_PLAYERS; i++)
            {
                slotText[i] = new pText(string.Empty, 18, new Vector2(42 * GameBase.WidthWidescreenRatio, i * 25), 1, true, Color.White);
                scrollableSlots.SpriteManager.Add(slotText[i]);

                slotTextInfo[i] = new pText(string.Empty, 12, new Vector2(194 * GameBase.WidthWidescreenRatio, 3 + i * 25), new Vector2(77, 0), 1, true,
                                            Color.White, false);
                slotTextInfo[i].TextAlignment = TextAlignment.Right;
                scrollableSlots.SpriteManager.Add(slotTextInfo[i]);

                pSprite status = new pSprite(GameBase.WhitePixel, Fields.TopLeft,
                                             Origins.TopLeft, Clocks.Game, new Vector2(33 * GameBase.WidthWidescreenRatio, i * 25), 0.974F,
                                             true, Color.White);
                status.HandleInput = true;
                status.OnClick += clickStatus;
                status.ClickRequiresConfirmation = true;
                status.VectorScale = new Vector2(5, 20);
                status.TagNumeric = i;
                status.Scale = 1.6f;
                status.ToolTip = LocalisationManager.GetString(OsuString.MatchSetup_ChangeTeam);
                scrollableSlots.SpriteManager.Add(status);

                slotStatus[i] = status;

                pSprite slot = new pSprite(GameBase.WhitePixel, Fields.TopLeft,
                                           Origins.TopLeft, Clocks.Game, new Vector2(41 * GameBase.WidthWidescreenRatio, i * 25), 0.974F,
                                           true,
                                           new Color(255, 255, 255, 50));
                slot.HandleInput = true;
                slot.VectorScale = new Vector2((float)230 * GameBase.WidthWidescreenRatio, 20);
                slot.Scale = 1.6f;
                slot.ToolTip = LocalisationManager.GetString(OsuString.MatchSetup_MoveToThisSlot);
                slot.HoverEffect = new Transformation(TransformationType.Fade, 0.2f, 0.4f, 0, 60);
                slot.OnClick += clickSlot;
                slot.ClickRequiresConfirmation = true;
                slot.TagNumeric = i;
                slotBackground[i] = slot;
                scrollableSlots.SpriteManager.Add(slot);

                pSprite lockToggle = new pSprite(SkinManager.Load(@"lobby-unlock", SkinSource.Osu), Origins.Centre,
                                                 new Vector2(21 * GameBase.WidthWidescreenRatio, 10 + i * 25), 1, true, Color.LightGray);
                slotLock[i] = lockToggle;
                lockToggle.OnClick += lockToggle_OnClick;
                lockToggle.HoverEffect = new Transformation(TransformationType.Rotation, 0, 0.1f, 0, 100);
                lockToggle.ClickEffect = new Transformation(TransformationType.Scale, 2, 1, 0, 200);
                lockToggle.ToolTip = LocalisationManager.GetString(OsuString.MatchSetup_LockThisSlot);
                lockToggle.ClickRequiresConfirmation = true;
                lockToggle.TagNumeric = i;
                scrollableSlots.SpriteManager.Add(lockToggle);
            }

            detailsGameName = new pTextBox(Match.gameName, 16, new Vector2(GameBase.WindowWidthScaled - 330, 85), 320, 1); //310
            detailsGameName.ResetTextOnEdit = true;
            detailsGameName.OnCommit += detailsGameName_OnCommit;
            detailsGameName.ReadOnly = true;
            detailsGameName.LengthLimit = 50;
            spriteManager.Add(detailsGameName.SpriteCollection);

            Transformation t = new Transformation(TransformationType.Scale, 1, 1.2F, 0, 100);
            t.Easing = EasingTypes.Out;

            Transformation t2 = new Transformation(TransformationType.Scale, 1.1F, 1.2F, 0, 100);
            t2.Easing = EasingTypes.Out;

            pSprite pauseButton =
                    new pSprite(SkinManager.Load(@"editor-button-pause", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                        Clocks.Game, new Vector2(80, 12), 0.96F, true, Color.White, @"b");
            pauseButton.HandleInput = true;
            pauseButton.ToolTip = LocalisationManager.GetString(OsuString.MatchSetup_Pause);
            pauseButton.ClickEffect = t2;
            pauseButton.HoverEffect = t;
            pauseButton.OnClick += pause;
            spriteManager.Add(pauseButton);

            UpdatePending = true;
            SongChangePending = true;
            HostChangePending = true;

            SetHost(true);
            syncMods();

            InitializeUser();

            if (!ChatEngine.IsVisible)
                ChatEngine.Toggle();

            base.Initialize();

            GameBase.LoadComplete();
        }

        private void syncMods()
        {
            Mods freeMod = Mods.None;
            for (var i = 0; i < bMatch.MAX_PLAYERS; i++)
            {
                if (Match.UserSlots[i] != null && Match.UserSlots[i].Id == GameBase.User.Id)
                {
                    freeMod = Match.slotMods[i] & Mods.FreeModAllowed;
                    break;
                }
            }

            ModManager.ModStatus = Match.activeMods | freeMod;

            UpdateBeatmapProperties();
        }

        void gameComboColorPicker_OnSelect(object sender, EventArgs e)
        {
            TagComboColour = (Color)sender;
        }

        private void InitializeUser()
        {
            GameBase.User.DrawAt(new Vector2(GameBase.WindowWidthScaled / 2 - 100, 1), false, 0); //328
            spriteManager.Add(GameBase.User.Sprites);
            if (PendingScoreUpdate)
            {
                GameBase.User.Refresh();
                PendingScoreUpdate = false;
            }
        }

        void comboColourHoverGained(object sender, EventArgs e)
        {
            pText senderText = sender as pText;
            if (senderText != null)
            {
                senderText.BorderWidth = 2;
            }
        }

        void comboColourHoverLost(object sender, EventArgs e)
        {
            pText senderText = sender as pText;
            if (senderText != null)
            {
                senderText.BorderWidth = 0;
            }
        }

        private void gameScoringTypeDropdown_OnSelect(object sender, EventArgs e)
        {
            Match.matchScoringType = (MatchScoringTypes)sender;
            BanchoClient.SendRequest(RequestType.Osu_MatchChangeSettings, Match);
        }

        private void gameTeamTypeDropdown_OnSelect(object sender, EventArgs e)
        {
            Match.matchTeamType = (MatchTeamTypes)sender;

            if (Match.matchTeamType == MatchTeamTypes.TagCoop || Match.matchTeamType == MatchTeamTypes.TagTeamVs)
            {
                Match.playMode = PlayModes.Osu;

                gameComboColourText.FadeIn(50);
                gameComboColourPicker.SpriteMainBox.FadeIn(50);

                if (Match.matchTeamType == MatchTeamTypes.TagCoop)
                    Match.matchScoringType = MatchScoringTypes.Score;
            }
            else
            {
                gameComboColourText.Hide();
                gameComboColourPicker.SpriteCollection.ForEach(p => p.FadeOut(50));
            }


            if (e == null)
                BanchoClient.SendRequest(RequestType.Osu_MatchChangeSettings, Match);
        }

        private void pause(object sender, EventArgs e)
        {
            NotificationManager.ShowMessageMassive(AudioEngine.Paused ? "Unpause" : LocalisationManager.GetString(OsuString.MatchSetup_Pause), 1000);
            paused = !AudioEngine.Paused;
            AudioEngine.TogglePause();
        }

        private static bool confirmExitByEscape;
        internal static void DoExit()
        {
            if (confirmExitByEscape)
            {
                GameBase.ChangeMode(OsuModes.Lobby);
                return;
            }

            NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.MatchSetup_HitEscapeToLeave), 1500);
            confirmExitByEscape = true;
        }

        private SlotStatus localPlayerStatus
        {
            get
            {
                int localUser = Match.findPlayerFromId(GameBase.User.Id);
                if (localUser < 0) return SlotStatus.Open;

                return Match.slotStatus[localUser];
            }
        }

        private bool KeyboardHandler_OnKeyPressed(object sender, Keys k)
        {
            if (BeatmapManager.Current != null && BeatmapManager.Current.AppropriateMode == PlayModes.OsuMania)
            {
                if (KeyboardHandler.ControlPressed && (k == Keys.OemPlus || k == BindingManager.For(Bindings.IncreaseSpeed)))
                {
                    SpeedMania.AdjustSpeed(1);
                    return true;
                }
                else if (KeyboardHandler.ControlPressed && (k == Keys.OemMinus || k == BindingManager.For(Bindings.DecreaseSpeed)))
                {
                    SpeedMania.AdjustSpeed(-1);
                    return true;
                }
            }

            switch (k)
            {
                case Keys.F5:
                    if (IsHost)
                    {
                        OnSelectBeatmap(null, null);
                        return true;
                    }
                    else if (GameBase.FadeState == FadeStates.Idle && localPlayerStatus == SlotStatus.NoMap)
                    {
                        BeatmapImport.ForceRun(true);
                        return true;
                    }
                    break;
            }

            return false;
        }

        private void detailsGameName_OnCommit(pTextBox sender, bool newText)
        {
            if (Match == null)
                return;

            if (detailsGameName.Box.Text.Length > 0)
                Match.gameName = detailsGameName.Box.Text;

            detailsGameName.ResetTextOnEdit = (Match.gameName.Contains(@"(room #"));
            BanchoClient.SendRequest(RequestType.Osu_MatchChangeSettings, Match);
        }

        private void SetHost(bool force)
        {
            if (IsHost)
            {
                if (!slotLock[0].HandleInput || force)
                {
                    detailsGameName.ReadOnly = false;
                    if (treeItem != null)
                        treeItem.backgroundSprite.HandleInput = true;

                    buttonBeatmap.Show();
                    buttonPassword.Show();
                    foreach (pSprite p in slotLock)
                    {
                        p.HandleInput = true;
                        p.FadeColour(Color.White, 100);
                    }
                    headerText2.Text = LocalisationManager.GetString(OsuString.MatchSetup_YouAreTheHost);

                    UpdatePending = true;
                }

                gameScoringTypeDropdown.Enabled = Match.matchTeamType != MatchTeamTypes.TagCoop;
                gameTeamTypeDropdown.Enabled = true;
                freeMods.Enabled = true;
                ChangeBeatmap();
            }
            else
            {
                freeMods.Enabled = false;

                if (slotLock[0].HandleInput || force)
                {
                    detailsGameName.ReadOnly = true;
                    if (treeItem != null)
                        treeItem.backgroundSprite.HandleInput = false;

                    buttonBeatmap.Hide();
                    buttonPassword.Hide();
                    foreach (pSprite p in slotLock)
                    {
                        p.HandleInput = false;
                        p.FadeColour(Color.LightGray, 100);
                    }
                    headerText2.Text = LocalisationManager.GetString(OsuString.MatchSetup_YouAreAPlayer);

                    UpdatePending = true;
                }

                gameScoringTypeDropdown.Enabled = false;
                gameTeamTypeDropdown.Enabled = false;
            }

            HostChangePending = false;
        }

        private void OnSelectBeatmap(object sender, EventArgs e)
        {
            if (InputManager.rightButton == ButtonState.Pressed && BeatmapManager.Current != null)
            {
                CollectionDialog d = new CollectionDialog(null);
                GameBase.ShowDialog(d);
                return;

            }

            if (!IsHost)
                return;

            if (treeItem != null)
            {
                treeItem.backgroundSprite.MoveToRelative(new Vector2(400, 0), 600, EasingTypes.Out);
            }

            ChangeBeatmap(true);
            Player.Mode = Match.playMode; //Ensure we have the correct mode set before displaying song select.

            SongSelection.AllowModeChange = Match.matchTeamType != MatchTeamTypes.TagCoop && Match.matchTeamType != MatchTeamTypes.TagTeamVs;
            GameBase.ChangeMode(OsuModes.SelectMulti);
        }

        private bool allowChangeMod()
        {
            int localUser = Match.findPlayerFromId(GameBase.User.Id);
            if (localUser < 0) return false;
            if (Match.slotStatus[localUser] == SlotStatus.Ready)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.MatchSetup_CantChangeMod), Color.Red, 1500);
                return false;
            }
            return true;
        }

        private void OnSelectMods(object sender, EventArgs e)
        {
            if (!allowChangeMod())
                return;
            oldMods = ModManager.ModStatus;

            ModSelectionType type = ModSelectionType.ClientSide;

            if (IsHost) type |= ModSelectionType.ServerSide;
            if ((Match.specialModes & MultiSpecialModes.FreeMod) > 0) type |= ModSelectionType.ClientSideFreeMod;

            ModSelection m = new ModSelection(type);
            m.Closed += mods_Closed;
            GameBase.ShowDialog(m);
        }

        private void mods_Closed(object sender, EventArgs e)
        {
            if (ModManager.ModStatus != oldMods)
                BanchoClient.SendRequest(RequestType.Osu_MatchChangeMods, new bInt((int)(Mods)ModManager.ModStatus));
        }

        private void OnChangePassword(object sender, EventArgs e)
        {
            oldPassword = Match.gamePassword;
            ChangePasswordDialog cp = new ChangePasswordDialog(oldPassword);
            cp.Closed += (obj, ev) =>
            {
                if (cp.newPassword != oldPassword)
                {
                    Match.gamePassword = cp.newPassword;
                    BanchoClient.SendRequest(RequestType.Osu_MatchChangePassword, Match);
                }
            };
            GameBase.ShowDialog(cp);
        }

        private void lockToggle_OnClick(object sender, EventArgs e)
        {
            int id = ((pSprite)sender).TagNumeric;
            togglelock(id);
        }

        private void OnReady(object sender, EventArgs e)
        {
            if (GameBase.FadeState != FadeStates.Idle)
                return;

            if ((BeatmapManager.Current.PlayMode == PlayModes.Taiko || BeatmapManager.Current.PlayMode == PlayModes.OsuMania) && (Match.matchTeamType == MatchTeamTypes.TagCoop || Match.matchTeamType == MatchTeamTypes.TagTeamVs))
            {
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.MatchSetup_NoTaikoCoop), 3000);
                return;
            }

            int localUser = Match.findPlayerFromId(GameBase.User.Id);
            if (localUser < 0) return;

            if (Match.slotStatus[localUser] == SlotStatus.NotReady)
                BanchoClient.SendRequest(RequestType.Osu_MatchReady, null);
            else if (IsHost && Match.slotReadyCount > 1 && Match.HasValidTeams)
            {
                BanchoClient.SendRequest(RequestType.Osu_MatchStart, null);
                buttonLeave.Hide();
            }
            else if (Match.slotStatus[localUser] == SlotStatus.Ready)
                BanchoClient.SendRequest(RequestType.Osu_MatchNotReady, null);
        }

        private void clickStatus(object sender, EventArgs e)
        {
            changeTeam();
        }

        private void clickSlot(object sender, EventArgs e)
        {
            int id = ((pSprite)sender).TagNumeric;

            if (Match.slotStatus[id] == SlotStatus.Locked)
                return;

            if (Match.slotStatus[id] == SlotStatus.Open)
                changeSlot(id);
            else if (IsHost && Match.slotId[id] != GameBase.User.Id)
            {
                pDialog dialog =
                    new pDialog(string.Format(LocalisationManager.GetString(OsuString.MatchSetup_UserActionsHeader), slotText[id].Text), true);
                dialog.AddOption(LocalisationManager.GetString(OsuString.MatchSetup_TransferHostPrivileges), Color.YellowGreen, delegate { BanchoClient.SendRequest(RequestType.Osu_MatchTransferHost, new bInt(id)); });
                dialog.AddOption(LocalisationManager.GetString(OsuString.MatchSetup_Kick), Color.Red, delegate { togglelock(id); });
                dialog.AddOption(LocalisationManager.GetString(OsuString.MatchSetup_UserOptions), new Color(58, 110, 165), delegate
                {
                    User u = BanchoClient.GetUserById(Match.slotId[id]);
                    if (u != null) UserProfile.DisplayProfileFor(u, true);
                });
                dialog.AddOption(LocalisationManager.GetString(OsuString.General_Cancel), Color.Gray, null);
                GameBase.ShowDialog(dialog);
            }
            else
            {
                User u = BanchoClient.GetUserById(Match.slotId[id]);
                if (u != null) UserProfile.DisplayProfileFor(u, true);
            }
        }

        private void togglelock(int id)
        {
            BanchoClient.SendRequest(RequestType.Osu_MatchLock, new bInt(id));
        }

        private void changeSlot(int id)
        {
            if (id == Match.findPlayerFromId(GameBase.User.Id))
                return;
            BanchoClient.SendRequest(RequestType.Osu_MatchChangeSlot, new bInt(id));
        }

        private void changeTeam()
        {
            BanchoClient.SendRequest(RequestType.Osu_MatchChangeTeam, null);
        }

        private void changeSeed()
        {
            if (IsHost)
            {
                Player.Seed = GameBase.Time;
                Match.Seed = Player.Seed;
            }
        }

        private void ChangeBeatmap(bool changingMap = false)
        {
            if (changingMap)
            {
                Match.beatmapName = string.Empty;
                Match.beatmapChecksum = string.Empty;
                Match.beatmapId = -1;
            }
            else if (BeatmapManager.Current != null && Match.beatmapChecksum != BeatmapManager.Current.BeatmapChecksum)
            {
                SongChangePending = true;
                Match.beatmapName = BeatmapManager.Current.DisplayTitle;
                Match.beatmapChecksum = BeatmapManager.Current.BeatmapChecksum;
                Match.beatmapId = BeatmapManager.Current.BeatmapId;
                Match.playMode = Player.Mode;
                changeSeed();
            }
            //ensure random seed is different from the last one if they play the same map again.
            if ((Match.activeMods & Mods.Random) > 0 && Match.beatmapChecksum == BeatmapManager.Current.BeatmapChecksum)
            {
                changeSeed();
            }

            BanchoClient.SendRequest(RequestType.Osu_MatchChangeSettings, Match);
        }

        private void OnLeaveGame(object sender, EventArgs e)
        {
            LeaveGame();
        }

        protected override void Dispose(bool disposing)
        {
            Instance = null;

            KeyboardHandler.OnKeyPressed -= KeyboardHandler_OnKeyPressed;

            if (GameBase.Mode != OsuModes.MatchSetup && GameBase.Mode != OsuModes.SelectMulti &&
                GameBase.Mode != OsuModes.BeatmapImport &&
                Lobby.Status != LobbyStatus.Idle &&
                Lobby.Status != LobbyStatus.Play)
                OnLeaveGame(null, null);

            if (Lobby.Status == LobbyStatus.Play || Lobby.Status == LobbyStatus.Setup)
                if (!Lobby.chatOpenOnStart && ChatEngine.IsVisible)
                    ChatEngine.Toggle(true);

            if (spriteManager != null) spriteManager.Dispose();
            if (detailsGameName != null) detailsGameName.Dispose();

            scrollableSlots.Dispose();

            base.Dispose(disposing);
        }

        public override void Draw()
        {
            if (treeItem != null)
            {
                treeItem.Position = treeItem.backgroundSprite.Position;
                treeItem.Move(Vector2.Zero);
            }

            spriteManager.Draw();

            scrollableSlots.Draw();

            base.Draw();
        }

        public override void Update()
        {
            if (!BanchoClient.InitializationComplete && GameBase.FadeState == FadeStates.Idle)
            {
                GameBase.ChangeMode(OsuModes.Menu);
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.MatchSetup_NeedsBancho));
            }

            if (Match == null)
            {
                if (GameBase.FadeState == FadeStates.Idle)
                    GameBase.ChangeMode(OsuModes.Menu);
                return;
            }

            if (GameBase.FadeState == FadeStates.FadeOut) return;

            if (PendingMapUpdate)
            {
                BeatmapImport.ForceRun(false);
                PendingMapUpdate = false;
                return;
            }

            scrollableSlots.Update();

            if (!paused && AudioEngine.AudioState == AudioStates.Stopped && GameBase.FadeState == FadeStates.Idle && BeatmapManager.Current != null)
            {
                try
                {
                    AudioEngine.LoadAudioForPreview(BeatmapManager.Current, true, false);
                }
                catch
                {
                    paused = true;
                }
            }

            if (UpdatePending)
            {
                scrollableSlots.SetContentDimensions(new Vector2(280, bMatch.MAX_PLAYERS * 25));

                currentPlayersInfo.Text = LocalisationManager.GetString(OsuString.MatchSetup_CurrentPlayers) + string.Format(" ({0}/{1})", Match.slotUsedCount, Match.slotOpenCount);

                int localUid = Match.findPlayerFromId(GameBase.User.Id);

                if (localUid < 0)
                {
                    LeaveGame();
                    return;
                }

                detailsGameName.Box.Text = Match.gameName;

                TeamMode = Match.TeamMode;

                if (Player.Mode != Match.playMode)
                {
                    Player.Mode = Match.playMode;
                    SongChangePending = true;
                }

                gameScoringTypeDropdown.SetSelected(Match.matchScoringType, true);
                gameTeamTypeDropdown.SetSelected(Match.matchTeamType, true);
                gameTeamTypeDropdown_OnSelect(Match.matchTeamType, EventArgs.Empty);

                if (IsHost || (Match.specialModes & MultiSpecialModes.FreeMod) > 0)
                    buttonMods.Show();
                else
                    buttonMods.Hide();

                if (IsHost)
                {
                    freeMods.Show();
                    freeMods.SetStatusQuietly((Match.specialModes & MultiSpecialModes.FreeMod) > 0);
                }
                else
                    freeMods.Hide();

                if (Match.slotReadyCount == Match.slotUsedCount != allReady)
                {
                    allReady = Match.slotReadyCount == Match.slotUsedCount;
                    if (allReady)
                        AudioEngine.PlaySample(@"match-confirm");
                }

                if (Match.slotStatus[localUid] == SlotStatus.Ready && IsHost && Match.slotReadyCount > 1 && Match.HasValidTeams)
                {
                    if (allReady)
                        buttonStart.Text.Text = LocalisationManager.GetString(OsuString.MatchSetup_StartGame);
                    else
                        buttonStart.Text.Text = LocalisationManager.GetString(OsuString.MatchSetup_ForceStartGame) + string.Format(" ({0}/{1})", Match.slotReadyCount, Match.slotUsedCount);
                }
                else if (Match.slotStatus[localUid] == SlotStatus.NotReady)
                {
                    if (buttonStart.Text.Text != LocalisationManager.GetString(OsuString.MatchSetup_Ready))
                    {
                        buttonStart.Text.Text = LocalisationManager.GetString(OsuString.MatchSetup_Ready);
                        AudioEngine.PlaySample(@"match-notready");
                    }
                }
                else
                {
                    if (buttonStart.Text.Text != LocalisationManager.GetString(OsuString.MatchSetup_NotReady))
                    {
                        buttonStart.Text.Text = LocalisationManager.GetString(OsuString.MatchSetup_NotReady);
                        AudioEngine.PlaySample(@"match-ready");
                    }
                }

                for (int i = 0; i < bMatch.MAX_PLAYERS; i++)
                {
                    User u = Match.UserSlots[i];

                    bool isLocalUser = false;

                    if ((Match.slotStatus[i] & SlotStatus.HasPlayer) > 0)
                    {
                        if (u == null) continue;

                        isLocalUser = u.Id == GameBase.User.Id;

                        slotText[i].Text = u.Name;
                        if (Match.slotStatus[i] != SlotStatus.Playing && u.Rank > 0)
                            slotTextInfo[i].Text = string.Format(@"Rank:#{0}", u.Rank);
                        slotLock[i].ToolTip = string.Empty;
                        slotLock[i].Texture = SkinManager.Load(IsHost ? "lobby-boot" : "lobby-unlock", SkinSource.Osu);
                        if (isLocalUser)
                            slotLock[i].ToolTip = LocalisationManager.GetString(OsuString.MatchSetup_YouAreTheHost);
                        else
                            slotLock[i].ToolTip = LocalisationManager.GetString(OsuString.MatchSetup_KickAndLock);

                        slotBackground[i].ToolTip = string.Format(LocalisationManager.GetString(OsuString.MatchSetup_Player_Tooltip), (int)u.Level, u.Accuracy, u.Location);
                    }
                    else
                    {
                        slotTextInfo[i].Text = string.Empty;
                        slotBackground[i].ToolTip = LocalisationManager.GetString(OsuString.MatchSetup_MoveToThisSlot);
                    }

                    slotStatus[i].HandleInput = false;

                    switch (Match.slotStatus[i])
                    {
                        case SlotStatus.Open:
                            slotText[i].Text = LocalisationManager.GetString(OsuString.MatchSetup_SlotOpen);

                            slotLock[i].Texture = SkinManager.Load(@"lobby-unlock", SkinSource.Osu);
                            slotLock[i].ToolTip = LocalisationManager.GetString(OsuString.MatchSetup_LockThisSlot);

                            slotStatus[i].InitialColour = Color.White;
                            slotBackground[i].InitialColour = Color.White;
                            break;
                        case SlotStatus.Locked:
                            slotText[i].Text = LocalisationManager.GetString(OsuString.MatchSetup_Locked);

                            slotLock[i].Texture = SkinManager.Load(@"lobby-lock", SkinSource.Osu);
                            slotLock[i].ToolTip = LocalisationManager.GetString(OsuString.MatchSetup_UnlockThisSlot);

                            slotStatus[i].InitialColour = Color.Black;
                            slotBackground[i].InitialColour = Color.Black;
                            break;
                        case SlotStatus.NotReady:
                            slotStatus[i].InitialColour = Color.White;
                            slotBackground[i].InitialColour = new Color(214, 255, 151);
                            break;
                        case SlotStatus.Ready:
                            slotStatus[i].InitialColour = Color.YellowGreen;
                            slotBackground[i].InitialColour = Color.YellowGreen;
                            break;
                        case SlotStatus.NoMap:
                            slotStatus[i].InitialColour = Color.OrangeRed;
                            slotBackground[i].InitialColour = Color.OrangeRed;
                            slotText[i].Text += @" " + LocalisationManager.GetString(OsuString.MatchSetup_NoMap);
                            break;
                        case SlotStatus.Playing:
                            slotStatus[i].InitialColour = Color.LightBlue;
                            slotBackground[i].InitialColour = Color.LightBlue;
                            slotText[i].Text += @" " + LocalisationManager.GetString(OsuString.MatchSetup_Playing);
                            break;
                    }

                    if (TeamMode)
                    {
                        switch (Match.slotStatus[i])
                        {
                            case SlotStatus.Open:
                                break;
                            case SlotStatus.Locked:
                                break;
                            default:
                                slotStatus[i].InitialColour = Match.slotTeam[i] == SlotTeams.Blue ? Color.Blue : Color.Red;
                                slotStatus[i].HandleInput = isLocalUser;
                                break;
                        }
                    }


                    if (Match.hostId == Match.slotId[i])
                    {
                        if (u != null)
                        {
                            HostChangePending |= IsHost != (u.Id == GameBase.User.Id);
                            IsHost = u.Id == GameBase.User.Id;
                        }
                        slotLock[i].Texture = SkinManager.Load(@"lobby-crown", SkinSource.Osu);
                    }
                }

                UpdatePending = false;

                if (SongChangePending)
                    UpdateSong();

                if (HostChangePending)
                    SetHost(false);

                UpdateMods();
            }

            base.Update();
        }

        private void UpdateSong()
        {
            if (treeItem != null)
            {
                treeItem.RemoveFromSpriteManager(spriteManager);
                treeItem.Dispose();
                treeItem = null;
            }

            if (string.IsNullOrEmpty(Match.beatmapChecksum))
            {
                buttonStart.Hide();

                Beatmap bm = new Beatmap();
                bm.BeatmapPresent = true;
                bm.Title = LocalisationManager.GetString(OsuString.MatchSetup_HostIsChangingTheMap);

                bm.Creator = LocalisationManager.GetString(OsuString.MatchSetup_PleaseWait);

                treeItem = new BeatmapTreeItem(bm, null, Match.playMode);
                treeItem.SingleItem = true;

                treeItem.Initialize();
                treeItem.State = TreeItemState.Grouped;

                treeItem.backgroundSprite.FadeColour(Color.DarkGray, 0);
                treeItem.backgroundSprite.HoverEffect = null;
                treeItem.backgroundSprite.ToolTip = null;
            }
            else
            {
                Beatmap map = BeatmapManager.GetBeatmapByChecksum(Match.beatmapChecksum);

                if (map != null)
                {
                    map.ProcessHeaders();
                    if (!map.BeatmapPresent)
                    {
                        BeatmapManager.Remove(map);
                        map = null;
                    }
                }

                hasSong = map != null && map.BeatmapChecksum == Match.beatmapChecksum;

                if (!hasSong)
                {
                    buttonStart.Hide();
                    BeatmapManager.Current = BeatmapManager.GetBeatmapById(Match.beatmapId);
                    AudioEngine.Stop();

                    Beatmap bm = new Beatmap();
                    bm.Title = Match.beatmapName;
                    bm.BeatmapPresent = true;

                    bm.Creator = BeatmapManager.Current != null
                                     ? LocalisationManager.GetString(OsuString.MatchSetup_UpdateToLatest)
                                     : Match.beatmapId > 0
                                           ? LocalisationManager.GetString(OsuString.MatchSetup_DownloadThisMap)
                                           : LocalisationManager.GetString(OsuString.MatchSetup_YouDontHaveThisMap);

                    treeItem = new BeatmapTreeItem(bm, null, Match.playMode);
                    treeItem.SingleItem = true;

                    treeItem.Initialize();
                    treeItem.State = TreeItemState.Grouped;

                    treeItem.backgroundSprite.FadeColour(Color.OrangeRed, 0);

                    if (Match.beatmapId > 0)
                    {
                        treeItem.backgroundSprite.HandleInput = true;
                        treeItem.backgroundSprite.OnClick += DownloadMap;
                        treeItem.backgroundSprite.HoverEffect =
                            new Transformation(treeItem.BackgroundColour, Color.YellowGreen, 0, 100);
                    }
                    else
                    {
                        treeItem.backgroundSprite.HandleInput = false;
                    }

                    BanchoClient.SendRequest(RequestType.Osu_MatchNoBeatmap, null);

                    treeItem.textArtist.TextBold = true;
                }
                else
                {
                    if (Match.slotStatus[Match.findPlayerFromId(GameBase.User.Id)] == SlotStatus.NoMap)
                        BanchoClient.SendRequest(RequestType.Osu_MatchHasBeatmap, null);
                    buttonStart.Show();
                    BeatmapManager.Current = map;

                    treeItem = new BeatmapTreeItem(map, null, Match.playMode);
                    treeItem.SingleItem = true;

                    treeItem.Initialize();
                    treeItem.State = TreeItemState.ExtrasVisible;

                    treeItem.backgroundSprite.OnClick += OnSelectBeatmap;
                    treeItem.backgroundSprite.HoverEffect =
                        new Transformation(treeItem.BackgroundColour, Color.Orange, 0, 100);

                    treeItem.backgroundSprite.ToolTip = string.Empty;

                    paused = false;

                    UpdateBeatmapProperties();

                    try
                    {
                        AudioEngine.LoadAudioForPreview(BeatmapManager.Current, true, false);
                    }
                    catch
                    {
                        NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.SongSelection_AudioError));
                        paused = true;
                    }
                }
            }

            if (treeItem != null)
            {
                treeItem.UpdateDrawDepth(BeatmapTreeManager.DRAW_DEPTH_START);

                treeItem.backgroundSprite.Position = new Vector2(GameBase.WindowWidthScaled - 40, 165); //600
                treeItem.backgroundSprite.MoveTo(new Vector2(GameBase.WindowWidthScaled - 340, 165), 500, EasingTypes.Out); //300

                treeItem.AddToSpriteManager(spriteManager);
            }

            SongChangePending = false;
        }

        private void UpdateBeatmapProperties()
        {
            // We check for BeatmapManager.Current to make sure a valid map is loaded.
            if (BeatmapManager.Current == null || treeItem == null)
            {
                return;
            }

            UpdateBeatmapStarDifficulty();

            if (treeItem.backgroundSprite.ToolTip != null)
            {
                UpdateBeatmapTooltip();
            }
        }

        private void UpdateBeatmapStarDifficulty()
        {
            Beatmap b = BeatmapManager.Current;

            if (b.StarDisplay < 0)
            {
                b.ComputeAndSetDifficultiesTomStars();
            }

            treeItem.UpdateStarDifficultySprites(false);
        }

        private void UpdateBeatmapTooltip()
        {
            Beatmap b = BeatmapManager.Current;

            Vector3 bpmRange = b.BeatsPerMinuteRange;
            double totalLength = HitObjectManager.ApplyModsToTime(b.TotalLength);

            if (ModManager.CheckActive(Mods.DoubleTime))
            {
                bpmRange *= 1.5f;
            }
            else if (ModManager.CheckActive(Mods.HalfTime))
            {
                bpmRange *= 0.75f;
            }

            int mins = (int)totalLength / 60000;
            int secs = (int)(totalLength % 60000) / 1000;

            treeItem.backgroundSprite.ToolTip = string.Format(LocalisationManager.GetString(OsuString.MatchSetup_BeatmapInfo) + "\n" + b.InfoText,
                        Beatmap.RangeToString(bpmRange),
                        mins,
                        secs,
                        b.DateLastPlayed != DateTime.MinValue ? b.DateLastPlayed.ToShortDateString() : LocalisationManager.GetString(OsuString.MatchSetup_NeverPlayed));
        }

        private void UpdateMods()
        {
            if ((Match.specialModes & MultiSpecialModes.FreeMod) > 0)
                ModManager.ModStatus = (ModManager.ModStatus & Mods.FreeModAllowed) | (Match.activeMods);
            else
                ModManager.ModStatus = Match.activeMods;

            showMods(Match.activeMods, new Vector2(GameBase.WindowWidthScaled - 310, 235), 1, @"main");

            for (int i = 0; i < bMatch.MAX_PLAYERS; i++)
                showMods(Match.slotMods[i] & Mods.FreeModAllowed, new Vector2(170 * GameBase.WidthWidescreenRatio, 10 + i * 25), 0.5f, @"slot" + i);

            UpdateBeatmapProperties();
        }

        Dictionary<string, Mods> previousModDisplays = new Dictionary<string, Mods>();
        private void showMods(Mods mods, Vector2 position, float scale, string tag)
        {
            Mods before;
            if (previousModDisplays.TryGetValue(tag, out before))
                if (before == mods) return;
            previousModDisplays[tag] = mods;

            modSprites.FindAll(p => (string)p.Tag == tag).ForEach(
                s =>
                {
                    s.FadeOut(400);
                    s.MoveToRelative(new Vector2(0, 20), 400, EasingTypes.In);
                    s.AlwaysDraw = false;
                    modSprites.Remove(s);
                });

            List<pSprite> sprites = ModManager.ShowModSprites(mods, position, scale, tag);

            modSprites.AddRange(sprites);
            if (tag == @"main")
                spriteManager.Add(sprites);
            else
                scrollableSlots.SpriteManager.Add(sprites);
        }

        private void DownloadMap(object sender, EventArgs e)
        {
            if (BeatmapManager.Current != null)
            {
                if (BeatmapManager.Current.InOszContainer)
                {
                    BeatmapManager.Current.UpdateOsz2Container();
                    return;
                }

                BeatmapManager.SongWatcher.EnableRaisingEvents = false;
                //check it matches the downloadable copy
                FileNetRequest request = new FileNetRequest(BeatmapManager.Current.FilenameFull + @"_",
                                                            Urls.PATH_MAPS +
                                                            Path.GetFileName(BeatmapManager.Current.Filename));

                request.onFinish += c_DownloadFileCompleted;
                NetManager.AddRequest(request);
                ((pSprite)sender).HandleInput = false;
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.MatchSetup_DownloadingUpdate), Color.YellowGreen, 2000);
            }
            else
            {
                OsuDirect.HandlePickup(LinkId.Beatmap, Match.beatmapId, delegate { PendingMapUpdate = true; });
            }
        }

        private void c_DownloadFileCompleted(string _fileLocation, Exception e)
        {
            GameBase.Scheduler.Add(delegate
            {
                BeatmapManager.Current.ReplaceWithUpdate();
                BeatmapManager.SongWatcher.EnableRaisingEvents = true;
                UpdatePending = true;
                SongChangePending = true;
            });
        }

        public static void IncomingMatch(ClientSideMatch newMatch)
        {
            if (newMatch.inProgress && PlayerVs.Match != null)
            {
                for (int i = 0; i < bMatch.MAX_PLAYERS; i++)
                    if (Match.slotId[i] >= 0 && newMatch.slotId[i] < 0)
                        PlayerVs.MatchPlayerLeft(i);
            }
            else
            {
                for (int i = 0; i < bMatch.MAX_PLAYERS; i++)
                {
                    if (newMatch.slotId[i] != Match.slotId[i])
                    {
                        if (newMatch.slotId[i] == -1)
                        {
                            int existing = Match.slotId[i];

                            //User has moved to another slot or left the match.
                            //We only need to take special action if they've left so...
                            bool userActuallyLeft = true;
                            for (int j = 0; j < bMatch.MAX_PLAYERS; j++)
                                if (newMatch.slotId[j] == existing)
                                {
                                    userActuallyLeft = false;
                                    break;
                                }

                            if (userActuallyLeft)
                            {
                                NotificationManager.ShowMessage(Match.UserSlots[i].Name + " left the game.", Color.OrangeRed, 3000);
                                AudioEngine.PlaySample(@"match-leave");
                            }
                        }
                        else
                        {
                            int newplace = newMatch.slotId[i];

                            //User either moved to another slot of joined the match.
                            //Check for join...
                            bool userActuallyJoined = true;
                            for (int j = 0; j < bMatch.MAX_PLAYERS; j++)
                                if (Match.slotId[j] == newplace)
                                {
                                    userActuallyJoined = false;
                                    break;
                                }

                            if (userActuallyJoined && newMatch.UserSlots[i].Id != GameBase.User.Id)
                            {
                                NotificationManager.ShowMessage(newMatch.UserSlots[i].Name + " joined the game.", Color.YellowGreen, 3000);
                                AudioEngine.PlaySample(@"match-join");
                            }
                        }

                    }
                }
            }

            if (newMatch.beatmapChecksum != Match.beatmapChecksum)
                SongChangePending = true;
            if (!IsHost)
                Player.Seed = newMatch.Seed;

            newMatch.gamePassword = Match.gamePassword; //carry password to new match data (isn't sent from server)

            Match = newMatch;

            UpdatePending = true;
        }

        public static void MatchDisbanded()
        {
            LeaveGame();
        }

        internal static void LeaveGame()
        {
            if (Lobby.Status == LobbyStatus.Idle)
                return;

            Lobby.Status = LobbyStatus.Idle;

            BanchoClient.SendRequest(RequestType.Osu_MatchPart, null);

            //Reset all mod settings on exiting a multiplayer match.
            ModManager.ModStatus = Mods.None;

            AudioEngine.PlaySamplePositional(@"menuback");

            GameBase.ChangeMode(OsuModes.Lobby);
        }

        public static void MatchStart(ClientSideMatch match)
        {
            Match = match;

            if (!hasSong)
            {
                LeaveGame();
                return;
            }

            if (MatchSetup.Instance != null) MatchSetup.Instance.syncMods();
            AudioEngine.PlaySamplePositional(@"menuhit");
            AudioEngine.PlaySample(@"match-start");

            PlayerVs.Match = match;


            Lobby.Status = LobbyStatus.Play;
            GameBase.ChangeMode(OsuModes.Play);

        }

        public static void MatchTransferHost()
        {
            IsHost = true;
            UpdatePending = true;
            HostChangePending = true;
            if (Match != null) Match.gamePassword = null;
            NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.MatchSetup_BecameHost), Color.YellowGreen, 6000);
            AudioEngine.PlaySample(@"match-confirm");
            //GameBase.ChangeMode(Modes.MatchSetup,true);
        }

        private static MatchSetup Instance;
        private pCheckbox freeMods;
        private pText currentPlayersInfo;

        public static void IncomingScoreUpdate(bScoreFrame frame)
        {
            GameBase.Scheduler.Add(delegate
            {

                if (Instance == null) return;

                Score s = ScoreFactory.Create(Match.playMode, frame, string.Empty);
            });
        }
    }
}

