﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Configuration;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Notifications;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Online;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu.GameModes.Play;

namespace osu.GameModes.Online
{
    class CreateGameDialog : pDialog
    {
        private pTextBox gameName;
        private pTextBox gamePassword;
        private pCheckbox usePassword;

        List<pSprite> passwordSprites = new List<pSprite>();
        private pDropdown playerCount;

        internal CreateGameDialog()
            : base("Create New Game...", true)
        {
        }

        internal override void Display()
        {
            pText t = new pText("Game Name:",16,new Vector2(30,100),0.92f, true, Color.White);
            spriteManager.Add(t);
            
            gameName = new pTextBox(ConfigManager.sUsername + "'s game",16,new Vector2(150,100),450,0.92f);
            gameName.LengthLimit = 50;
            gameName.OnCommit += gameName_OnCommit;
            gameName.ResetTextOnEdit = true;
            spriteManager.Add(gameName.SpriteCollection);

            usePassword = new pCheckbox("Require password to join",new Vector2(30,130),0.92f,false);
            usePassword.OnCheckChanged += usePassword_OnCheckChanged;
            spriteManager.Add(usePassword.SpriteCollection);

            t = new pText("Password:", 16, new Vector2(30, 160), 0.92f, true, Color.White);
            spriteManager.Add(t);
            passwordSprites.Add(t);

            gamePassword = new pTextBox("", 16, new Vector2(150, 160), 450, 0.92f);
            gamePassword.LengthLimit = 50;
            spriteManager.Add(gamePassword.SpriteCollection);
            passwordSprites.AddRange(gamePassword.SpriteCollection);

            t = new pText("Max Players:", 16, new Vector2(30, 190), 0.92f, true, Color.White);
            spriteManager.Add(t);

            currentVerticalSpace = 250;

            AddOption("Start Game",Color.LimeGreen,onClick,false);
            AddOption("Cancel", Color.LightGray, cancel, false);

            playerCount = new pDropdown(null, "", new Vector2(150, 190), 200, 0.96f);
            playerCount.AddOption("2 players",2);
            playerCount.AddOption("3 players", 3);
            playerCount.AddOption("4 players", 4);
            playerCount.AddOption("5 players", 5);
            playerCount.AddOption("6 players", 6);
            playerCount.AddOption("7 players", 7);
            playerCount.AddOption("8 players", 8);
            if (bMatch.MAX_PLAYERS > 8)
                playerCount.AddOption(bMatch.MAX_PLAYERS + " players", bMatch.MAX_PLAYERS);
            playerCount.SetSelected(8,true);
            playerCount.SpriteManager = spriteManager;

            spriteManager.Add(playerCount.SpriteCollection);
            
            base.Display();

            Background.InitialColour = new Color(0,0,0,200);
            passwordSprites.ForEach(s => s.FadeOut(0));
        }

        private void cancel(object sender, EventArgs e)
        {
            if (Lobby.Status == LobbyStatus.PendingJoin)
                return;

            Close();
        }

        void usePassword_OnCheckChanged(object sender, bool status)
        {
            if (status)
                passwordSprites.ForEach(s => s.FadeIn(50));
            else
                passwordSprites.ForEach(s => s.FadeOut(50));
        }

        void gameName_OnCommit(pTextBox sender, bool newText)
        {
            if (gameName.Box.Text.Length == 0)
                gameName.Box.Text = gameName.textBeforeCommit;

            if (newText)
                gameName.ResetTextOnEdit = false;
        }

        private void onClick(object sender, EventArgs e)
        {
            if (Lobby.Status == LobbyStatus.PendingCreate)
                return;

            if (usePassword.Checked && gamePassword.Box.Text.Length == 0)
            {
                NotificationManager.ShowMessage("Enter a password before attempting to start the game...");
                return;
            }

            NotificationManager.ShowMessageMassive("Creating game...",1600);
            Lobby.Status = LobbyStatus.PendingCreate;

            ClientSideMatch match = new ClientSideMatch(MatchTypes.Standard,
                                         MatchScoringTypes.Score,
                                         MatchTeamTypes.HeadToHead,
                                         PlayModes.Osu,
                                         gameName.Box.Text,
                                         usePassword.Checked ? gamePassword.Box.Text : null,
                                         (int)playerCount.SelectedObject,
                                         BeatmapManager.Current.DisplayTitle,
                                         BeatmapManager.Current.BeatmapChecksum,
                                         BeatmapManager.Current.BeatmapId,
                                         Mods.None,
                                         GameBase.User.Id,
                                         MultiSpecialModes.None,
                                         Player.Seed);
            MatchSetup.Match = match;
            BanchoClient.SendRequest(RequestType.Osu_MatchCreate, match);
        }


        protected override void Dispose(bool disposing)
        {
            gameName.Dispose();

            base.Dispose(disposing);
        }

        internal override bool HandleKey(Keys k)
        {
            if (!base.HandleKey(k))
                return false;

            if (k == Keys.Enter)
                onClick(null, null);

            return true;
            
        }

    }
}