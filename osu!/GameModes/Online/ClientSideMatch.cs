﻿using System;
using System.IO;
using osu.Helpers;
using osu.Online;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Helpers;

namespace osu.GameModes.Online
{
    internal class ClientSideMatch : bMatch
    {
        public User[] UserSlots = new User[MAX_PLAYERS];

        public ClientSideMatch(bMatch info)
            : this(info.matchType, info.matchScoringType, info.matchTeamType,
                info.playMode, info.gameName, info.gamePassword, info.slotOpenCount,
                info.beatmapName, info.beatmapChecksum, info.beatmapId, info.activeMods, info.hostId, info.specialModes, info.Seed)
        {
            this.slotId = info.slotId;
        }

        internal ClientSideMatch(MatchTypes matchType, MatchScoringTypes matchScoringType, MatchTeamTypes matchTeamType,
                                 PlayModes playMode, string gameName, string gamePassword, int initialSlotCount,
                                 string beatmapName, string beatmapChecksum, int beatmapId, Mods activeMods, int hostId, MultiSpecialModes specialModes, int seed)
            : base(
                matchType, matchScoringType, matchTeamType, playMode, gameName, gamePassword, initialSlotCount,
                beatmapName, beatmapChecksum, beatmapId, activeMods, hostId, specialModes, seed)
        {
            SendPassword = true;
        }

        static bool alertedOne = false;

        internal ClientSideMatch(SerializationReader sr)
            : base(sr)
        {
            SendPassword = true;

            for (int i = 0; i < MAX_PLAYERS; i++)
            {
                if (slotId[i] != -1)
                {
                    User u = BanchoClient.GetUserById(slotId[i]);
                    if (u != null) UserSlots[i] = u;
                }
                else
                {
                    UserSlots[i] = null;
                }
            }



        }
    }
}