using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Notifications;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input.Handlers;
using osu.Online;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osudata;
using osu_common.Helpers;
using osu.Graphics.Primitives;
using osu.GameModes.Menus;
using osu.Online.Social;

namespace osu.GameModes.Online
{
    internal class Lobby : DrawableGameComponent
    {
        internal static bool LobbyUpdatePending;
        internal static List<LobbyMatch> Matches = new List<LobbyMatch>();
        internal static LobbyStatus Status;

        const int content_y = 75;
        const int content_height = 216;

        const int filters_height = 35;

        private readonly pScrollableArea areaGameList = new pScrollableArea(new Rectangle(0, content_y, GameBase.WindowWidthScaled, content_height), Vector2.Zero, true);

        private SpriteManager spriteManager;

        internal static bool chatOpenOnStart;
        private pText noMatches;

        private string text_noMatchFound;
        private string text_loading;

        public Lobby(Game game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            MatchSetup.Match = null;

            StreamingManager.StopSpectating(false);

            text_noMatchFound = LocalisationManager.GetString(OsuString.Lobby_NoMatch);
            text_loading = LocalisationManager.GetString(OsuString.General_Loading);

            spriteManager = new SpriteManager(true);

            spriteManagerFilters = new SpriteManager(true);
            //spriteManagerFilters.Alpha = 0;
            spriteManagerFilters.SetVisibleArea(new RectangleF(0, content_y - filters_height, 640, filters_height));

            BanchoClient.OnConnect += BanchoClient_OnConnect;
            KeyboardHandler.OnKeyRepeat += KeyboardHandler_OnKeyRepeat;

            pText headerText =
                new pText(LocalisationManager.GetString(OsuString.Lobby_Header), 24, new Vector2(0, 0), 0.955F, true, new Color(255, 255, 255, 255));
            spriteManager.Add(headerText);

            headerText2 = new pText(text_loading, 12, new Vector2(170, 8), 0.955F, true,
                              new Color(255, 255, 255, 255));
            spriteManager.Add(headerText2);


            //pSprite bgf =
            //    new pSprite(SkinManager.Load(@"lobby-background"), Fields.TopLeft, Origins.TopLeft,
            //                Clocks.Game, Vector2.Zero, 0, true, Color.White);
            //spriteManager.Add(bgf);

            pSprite p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(0, content_y - filters_height), 0.1f, true, new Color(0, 0, 0, 180));
            p.Scale = 1.6f;
            p.VectorScale = new Vector2(GameBase.WindowWidthScaled, content_height + filters_height + 30);
            spriteManager.Add(p);

            int button_height = content_y + content_height + 2;

            pButton pbut = new pButton(LocalisationManager.GetString(OsuString.Lobby_NewGame), new Vector2(GameBase.WindowWidthScaled / 2 - 100, button_height), new Vector2(200, 25), 0.92f, new Color(99, 139, 228), OnCreateGame);
            spriteManager.Add(pbut.SpriteCollection);

            pbut = new pButton(LocalisationManager.GetString(OsuString.Lobby_QuickJoin), new Vector2(GameBase.WindowWidthScaled / 2 + 110, button_height), new Vector2(200, 25), 0.92f, Color.YellowGreen, OnQuickJoin);
            spriteManager.Add(pbut.SpriteCollection);

            pbut = new pButton(LocalisationManager.GetString(OsuString.Lobby_BackToMenu), new Vector2(GameBase.WindowWidthScaled / 2 - 310, button_height), new Vector2(200, 25), 0.92f, new Color(235, 160, 62), back_OnClick);
            spriteManager.Add(pbut.SpriteCollection);

            noMatches = new pText(text_loading, 18, new Vector2(GameBase.WindowWidthScaled / 2, 180), new Vector2(400, 0), 1, true, Color.White, false);
            noMatches.TextBold = true;
            noMatches.Alpha = 0;
            noMatches.TextAlignment = TextAlignment.Centre;
            noMatches.Origin = Origins.Centre;
            spriteManager.Add(noMatches);

            sortingTabs = new pTabCollection(spriteManager, 6, new Vector2(50, 40), 0.98f, false,
                                             Color.Crimson);
            sortingTabs.Add(LocalisationManager.GetString(OsuString.Lobby_All), -1);
            sortingTabs.Add(OsuCommon.PlayModeString(PlayModes.Osu), (int)PlayModes.Osu);
            sortingTabs.Add(OsuCommon.PlayModeString(PlayModes.Taiko), (int)PlayModes.Taiko);
            sortingTabs.Add(OsuCommon.PlayModeString(PlayModes.CatchTheBeat), (int)PlayModes.CatchTheBeat);
            sortingTabs.Add(OsuCommon.PlayModeString(PlayModes.OsuMania), (int)PlayModes.OsuMania);
            sortingTabs.SetSelected(ConfigManager.sLobbyPlayMode.Value, true);
            sortingTabs.OnTabChanged += delegate { ConfigManager.sLobbyPlayMode.Value = (int)sortingTabs.SelectedTab.Tag; LobbyUpdatePending = true; };

            checkExistingMaps = new pCheckbox(LocalisationManager.GetString(OsuString.Lobby_ExistMapOnly), 0.8f, new Vector2(5, 2), 1, ConfigManager.sLobbyShowExistingOnly);
            spriteManagerFilters.Add(checkExistingMaps.SpriteCollection);

            checkExistingMaps.OnCheckChanged +=
                delegate(object sender, bool status)
                {
                    ConfigManager.sLobbyShowExistingOnly.Value = status;
                    LobbyUpdatePending = true;
                };

            checkFriendsOnly = new pCheckbox(LocalisationManager.GetString(OsuString.Lobby_FriendOnly), 0.8f, new Vector2(5, 19), 1, ConfigManager.sLobbyShowFriendsOnly);
            spriteManagerFilters.Add(checkFriendsOnly.SpriteCollection);

            checkFriendsOnly.OnCheckChanged +=
                delegate(object sender, bool status)
                {
                    ConfigManager.sLobbyShowFriendsOnly.Value = status;
                    LobbyUpdatePending = true;
                };

            checkInProgress = new pCheckbox(LocalisationManager.GetString(OsuString.Lobby_InProgress), 0.8f, new Vector2(360, 19), 1, ConfigManager.sLobbyShowInProgress);
            spriteManagerFilters.Add(checkInProgress.SpriteCollection);

            checkInProgress.OnCheckChanged +=
                delegate(object sender, bool status)
                {
                    ConfigManager.sLobbyShowInProgress.Value = status;
                    LobbyUpdatePending = true;
                };

            checkShowFullGames = new pCheckbox(LocalisationManager.GetString(OsuString.Lobby_ShowFull), 0.8f, new Vector2(180, 2), 1, ConfigManager.sLobbyShowFull);
            spriteManagerFilters.Add(checkShowFullGames.SpriteCollection);

            checkShowFullGames.OnCheckChanged += delegate(object sender, bool status)
            {
                ConfigManager.sLobbyShowFull.Value = status;
                LobbyUpdatePending = true;
            };

            checkShowPasswordedGames = new pCheckbox(LocalisationManager.GetString(OsuString.Lobby_ShowLocked), 0.8f, new Vector2(180, 19), 1, ConfigManager.sLobbyShowPassworded);
            spriteManagerFilters.Add(checkShowPasswordedGames.SpriteCollection);

            checkShowPasswordedGames.OnCheckChanged +=
                delegate(object sender, bool status)
                {
                    ConfigManager.sLobbyShowPassworded.Value = status;
                    LobbyUpdatePending = true;
                };

            pText pt = new pText(LocalisationManager.GetString(OsuString.Lobby_Search), 14, new Vector2(360, 2), 1, true, Color.White);
            spriteManagerFilters.Add(pt);

            filterTextBox = new pTextBox(string.Empty, 14, new Vector2(360 + pt.MeasureText().X, 2), 134, 1);
            filterTextBox.LengthLimit = 20;
            filterTextBox.OnChange += delegate
            {
                LobbyUpdatePending = true;

                if (filterTextBox.Text.Length > 0)
                {
                    checkFriendsOnly.Hide();
                    checkShowFullGames.Hide();
                    checkShowPasswordedGames.Hide();
                    checkExistingMaps.Hide();
                    sortingTabs.SetSelected(-1, true);
                }
                else
                {
                    checkFriendsOnly.Show();
                    checkShowFullGames.Show();
                    checkShowPasswordedGames.Show();
                    checkExistingMaps.Show();
                    sortingTabs.SetSelected(ConfigManager.sLobbyPlayMode.Value, true);
                }
            };
            spriteManagerFilters.Add(filterTextBox.SpriteCollection);

            //buttonFilters = new pButton(LocalisationManager.GetString(OsuString.Lobby_Filters), new Vector2(390, 32), new Vector2(100, 20), 1, Color.YellowGreen, toggleFilters);
            //spriteManager.Add(buttonFilters.SpriteCollection);

            base.Initialize();

            if (BeatmapManager.Current == null || string.IsNullOrEmpty(BeatmapManager.Current.RelativeContainingFolder) || !BeatmapManager.Current.BeatmapPresent)
                MusicControl.ChooseRandomSong(true);

            JoinLobby();

            LobbyUpdatePending = true;

            GameBase.LoadComplete();

            if (!ChatEngine.IsVisible)
            {
                if (ChatEngine.IsFullVisible)
                    ChatEngine.ToggleFull();
                ChatEngine.Toggle();
            }
        }

        bool filtersVisible;
        const int FILTER_ADJUSTMENT = 40;
        private void toggleFilters(object sender, EventArgs e)
        {
            pButton but = sender as pButton;

            filtersVisible = !filtersVisible;


            const int ADJUSTMENT_HEADER = FILTER_ADJUSTMENT + 12;

            if (filtersVisible)
            {
                but.SetColour(Color.Red);
                but.Text.Text = LocalisationManager.GetString(OsuString.Lobby_Hide);

                but.SpriteCollection.ForEach(s => s.MoveToRelative(new Vector2(0, FILTER_ADJUSTMENT), 200, EasingTypes.Out));

                //headerText2.MoveToRelative(new Vector2(0, ADJUSTMENT_HEADER), 200, EasingTypes.In);
            }
            else
            {
                but.SetColour(Color.YellowGreen);
                but.Text.Text = LocalisationManager.GetString(OsuString.Lobby_Filters);

                but.SpriteCollection.ForEach(s => s.MoveToRelative(new Vector2(0, -FILTER_ADJUSTMENT), 200, EasingTypes.Out));


                //headerText2.MoveToRelative(new Vector2(0, -ADJUSTMENT_HEADER), 200, EasingTypes.In);
            }
        }

        void BanchoClient_OnConnect()
        {
            JoinLobby();
        }

        bool KeyboardHandler_OnKeyRepeat(object sender, Keys k, bool first)
        {
            if (!first) return false;

            switch (k)
            {
                case Keys.Escape:
                    back_OnClick(null, null);
                    return true;
            }

            return false;
        }

        private void OnQuickJoin(object sender, EventArgs e)
        {
            PlayModes pm = (PlayModes)ConfigManager.sLobbyPlayMode.Value;
            List<LobbyMatch> localMatch = null;
            lock (Matches)
            {
                localMatch = Matches.FindAll(m =>
                {
                    bMatch bm = m.matchInfo;
                    if (bm.passwordRequired || bm.inProgress || bm.slotFreeCount == 0)
                        return false;
                    if (ConfigManager.sLobbyPlayMode.Value != -1 && bm.playMode != pm)
                        return false;
                    return true;
                });
            }
            if (localMatch.Count > 0)
            {
                List<bMatch> fitMatch = new List<bMatch>();
                for (int i = 0; i < localMatch.Count; i++)
                {
                    bMatch match = localMatch[i].matchInfo;
                    int totalPP = 0;
                    int total = 0;
                    for (int j = 0; j < match.slotId.Length; j++)
                    {
                        if (match.slotId[j] <= 0)
                            continue;
                        User u = BanchoClient.GetUserById(match.slotId[j]);
                        if (u != null)
                        {
                            total++;
                            totalPP += u.Rank;
                        }
                    }
                    int averageRank = totalPP / Math.Max(1, total);
                    if (Math.Abs(averageRank - GameBase.User.Rank) < (int)GameBase.User.Rank * 0.3)
                        fitMatch.Add(match);
                }

                if (fitMatch.Count > 5)
                {
                    int next = GameBase.random.Next(fitMatch.Count);
                    JoinMatch(fitMatch[next]);
                }
                else
                {
                    int next = GameBase.random.Next(localMatch.Count);
                    JoinMatch(localMatch[next].matchInfo);
                }
            }
            else
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Lobby_QuickJoin_Fail), Color.Red, 1500);
            }

        }

        private void OnCreateGame(object sender, EventArgs e)
        {
            if (!BanchoClient.Connected)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Lobby_LoginFirst));
                return;
            }

            if (Status == LobbyStatus.PendingCreate) return;


            if (BeatmapManager.Beatmaps.Count == 0)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Lobby_NoBeatmapsAvailable));
                return;
            }

            if (BeatmapManager.Current == null)
                BeatmapManager.Current = BeatmapManager.Beatmaps[0];

            CreateGameDialog cgd = new CreateGameDialog();
            GameBase.ShowDialog(cgd);
        }

        internal static void JoinLobby()
        {
            if (Joined) return;
            Joined = true;
            Matches.Clear();
            Status = LobbyStatus.Idle;
            BanchoClient.SendRequest(RequestType.Osu_LobbyJoin, null);

            ChatEngine.SwapChannel(ChatEngine.AddChannel(@"#lobby"));
        }

        internal static void LeaveLobby(bool changeMode)
        {
            if (!Joined) return;
            Joined = false;

            Matches.Clear();
            BanchoClient.SendRequest(RequestType.Osu_LobbyPart, null);

            if (Status == LobbyStatus.Idle)
            {
                ChatEngine.RemoveChannel(@"#lobby");

                if (!chatOpenOnStart && ChatEngine.IsVisible)
                    ChatEngine.Toggle(true);
                Status = LobbyStatus.NotJoined;
            }

            if (changeMode)
            {
                AudioEngine.PlaySamplePositional(@"menuback");
                Status = LobbyStatus.NotJoined;
                GameBase.ChangeMode(OsuModes.Menu);
            }
        }

        static bool Joined;
        private pTabCollection sortingTabs;
        private pCheckbox checkShowFullGames;
        private pCheckbox checkShowPasswordedGames;
        private pCheckbox checkExistingMaps;
        private pText headerText2;
        private SpriteManager spriteManagerFilters;
        private pCheckbox checkFriendsOnly;
        private pCheckbox checkInProgress;
        private pTextBox filterTextBox;
        private pButton buttonFilters;

        protected override void Dispose(bool disposing)
        {
            KeyboardHandler.OnKeyRepeat -= KeyboardHandler_OnKeyRepeat;
            BanchoClient.OnConnect -= BanchoClient_OnConnect;

            LeaveLobby(false);

            spriteManager.Dispose();
            spriteManagerFilters.Dispose();
            areaGameList.Dispose();
            Matches.Clear();

            filterTextBox.Dispose();

            base.Dispose(disposing);
        }

        private void back_OnClick(object sender, EventArgs e)
        {
            AudioEngine.PlaySamplePositional(@"menuback");
            GameBase.ChangeMode(OsuModes.Menu);
        }

        public override void Draw()
        {
            spriteManager.Draw();

            spriteManagerFilters.Draw();

            areaGameList.Draw();

            base.Draw();
        }

        public override void Update()
        {
            if (!BanchoClient.Connected && GameBase.FadeState == FadeStates.Idle)
            {
                GameBase.ChangeMode(OsuModes.Menu);
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Lobby_Bancho_Fail));
            }

            spriteManager.Blackness = LobbyStatus.Idle != Status ? 0.6f : 0;

            if (LobbyUpdatePending)
            {
                int x = 0;

                lock (Matches)
                {
                    Matches.Sort();

                    int visibleMatches = 0;

                    foreach (LobbyMatch m in Matches)
                    {
                        if (CheckFilter(m))
                        {
                            visibleMatches++;

                            bool firstDraw = m.DrawAt(new Vector2(3, x));

                            if (firstDraw || m.Filtered)
                                m.SpriteCollection.ForEach(s => s.FadeInFromZero(500));

                            if (firstDraw)
                                areaGameList.SpriteManager.Add(m.SpriteCollection);

                            m.Filtered = false;

                            x += 49;
                        }
                        else
                        {
                            if (!m.Filtered)
                            {
                                if (m.SpriteCollection != null) m.SpriteCollection.ForEach(s => s.FadeOut(300));
                                m.Filtered = true;
                            }
                        }
                    }

                    headerText2.Text = string.Format(LocalisationManager.GetString(OsuString.Lobby_FilteredMatches), visibleMatches, Matches.Count);

                    if (visibleMatches > 0)
                        noMatches.FadeOut(50);
                    else
                    {
                        noMatches.Text = BanchoClient.InitializationComplete ? text_noMatchFound : text_loading;
                        noMatches.FadeIn(50);
                    }
                }

                areaGameList.SetContentDimensions(new Vector2(640, x + 50));

                LobbyUpdatePending = false;
            }

            areaGameList.Update();
            base.Update();
        }

        private bool CheckFilter(LobbyMatch match)
        {
            bool hasTextSearch = filterTextBox.Box.Text.Length > 0;

            if (ConfigManager.sLobbyPlayMode.Value >= 0 && match.matchInfo.playMode != (PlayModes)ConfigManager.sLobbyPlayMode.Value)
                if (!hasTextSearch) return false;

            if (!checkShowFullGames.Checked && match.matchInfo.slotFreeCount == 0)
                if (!hasTextSearch) return false;

            if (!checkInProgress.Checked && match.matchInfo.inProgress)
                return false;

            if (checkFriendsOnly.Checked)
            {
                bool found = false;

                for (int i = 0; i < 16; i++)
                {
                    if (match.matchInfo.slotId[i] < 0) continue;

                    User u = BanchoClient.GetUserById(match.matchInfo.slotId[i]);

                    if (u != null && u.IsFriend)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found) return false;
            }

            if (!checkShowPasswordedGames.Checked && match.matchInfo.passwordRequired)
                if (!hasTextSearch) return false;

            if (ConfigManager.sLobbyShowExistingOnly && BeatmapManager.GetBeatmapByChecksum(match.matchInfo.beatmapChecksum) == null)
                if (!hasTextSearch) return false;

            if (hasTextSearch)
            {
                string filter = filterTextBox.Box.Text.ToLower();

                bool success = false;

                if (match.matchInfo.gameName.ToLower().Contains(filter))
                    success = true;
                else if (match.matchInfo.beatmapName.ToLower().Contains(filter))
                    success = true;
                else
                {
                    for (int i = 0; i < 8; i++)
                    {
                        if (match.matchInfo.slotId[i] < 0) continue;

                        User u = BanchoClient.GetUserById(match.matchInfo.slotId[i]);

                        if (u != null && u.Name.ToLower().Contains(filter))
                        {
                            success = true;
                            break;
                        }
                    }
                }

                return success;
            }

            return true;
        }

        public static void IncomingMatch(ClientSideMatch match)
        {
            if (MatchSetup.Match != null && MatchSetup.Match.matchId == match.matchId)
                MatchSetup.IncomingMatch(match);

            lock (Matches)
            {
                int found = Matches.FindIndex(m => m.matchInfo.matchId == match.matchId);
                if (found < 0)
                    Matches.Add(new LobbyMatch(match));
                else
                    Matches[found].matchInfo = match;
            }

            LobbyUpdatePending = true;
        }

        public static void DisbandedMatch(int id)
        {
            if (MatchSetup.Match != null && MatchSetup.Match.matchId == id)
                MatchSetup.MatchDisbanded();

            lock (Matches)
            {
                List<LobbyMatch> found = Matches.FindAll(m => m.matchInfo.matchId == id);

                foreach (LobbyMatch m in found)
                {
                    if (m.SpriteCollection != null)
                        foreach (pSprite p in m.SpriteCollection)
                        {
                            p.FadeOut(50);
                            p.AlwaysDraw = false;
                        }
                    lock (Matches)
                        Matches.Remove(m);
                }
            }
            LobbyUpdatePending = true;
        }

        public static void JoinMatch(int matchId, string password = null)
        {
            if (Status == LobbyStatus.PendingJoin) return;

            NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.Lobby_Join), 1500);
            BanchoClient.SendRequest(RequestType.Osu_MatchJoin, new bMatchJoin(matchId, password ?? MatchSetup.Match.gamePassword));
            Status = LobbyStatus.PendingJoin;
        }

        public static void JoinMatch(bMatch info, string password = null)
        {
            if (Status == LobbyStatus.PendingJoin) return;

            if (info.slotOpenCount <= info.slotUsedCount || Status != LobbyStatus.Idle)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Lobby_MatchFull));
                AudioEngine.PlaySample(@"match-leave");
            }
            else if (info.passwordRequired && string.IsNullOrEmpty(password))
            {
                JoinGameDialog jgd = new JoinGameDialog(info);
                GameBase.ShowDialog(jgd);
                return;
            }

            MatchSetup.Match = new ClientSideMatch(info);
            if (password != null) MatchSetup.Match.gamePassword = password;

            JoinMatch(info.matchId, password);
        }

        public static void OnJoinFail()
        {
            MatchSetup.Match = null;

            NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.Lobby_Join_Fail), 2000);
            AudioEngine.PlaySample(@"match-leave");
            Status = GameBase.Mode == OsuModes.Lobby ? LobbyStatus.Idle : LobbyStatus.NotJoined;
        }

        public static void OnJoinSuccess()
        {
            AudioEngine.PlaySample(@"match-join");
            switch (Status)
            {
                case LobbyStatus.PendingCreate:
                    MatchSetup.IsHost = true;
                    Status = LobbyStatus.Setup;
                    break;
                case LobbyStatus.PendingJoin:
                    MatchSetup.IsHost = false;
                    Status = LobbyStatus.Setup;
                    break;
            }

            GameBase.ChangeMode(OsuModes.MatchSetup, true);
        }
    }

    internal enum LobbyStatus
    {
        NotJoined,
        Idle,
        PendingJoin,
        PendingCreate,
        Setup,
        Play,
        Results
    }
}