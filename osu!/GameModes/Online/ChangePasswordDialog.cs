﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Notifications;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Online;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;

namespace osu.GameModes.Online
{
    internal class ChangePasswordDialog : pDialog
    {
        private readonly List<pSprite> passwordSprites = new List<pSprite>();
        private pTextBox gamePassword;
        internal string oldPassword;
        internal string newPassword;

        internal ChangePasswordDialog(string old)
            : base("Enter a new password...", true)
        {
            oldPassword = old == null ? "" : old;
        }

        internal override void Display()
        {
            pText t = new pText("Password:", 16, new Vector2(30, 120), 0.92f, true, Color.White);
            spriteManager.Add(t);
            passwordSprites.Add(t);

            gamePassword = new pTextBox(oldPassword, 16, new Vector2(150, 120), 450, 0.92f);
            gamePassword.LengthLimit = 50;

            currentVerticalSpace = 180;

            spriteManager.Add(gamePassword.SpriteCollection);

            AddOption("OK", Color.LimeGreen, onClose, false);
            AddOption("Cancel", Color.LightGray, onClose, false);

            base.Display();

            Background.InitialColour = new Color(15, 59, 87, 200);

            gamePassword.Select();
        }

        private void onClose(object sender, EventArgs e)
        {
            Close();
        }

        internal override void Close(bool isCancel = false)
        {
            newPassword = gamePassword.Text;
            base.Close(isCancel);
        }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        internal override bool HandleKey(Keys k)
        {
            if (!base.HandleKey(k))
                return false;

            return true;
        }
    }
}