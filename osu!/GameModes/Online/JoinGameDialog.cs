﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Notifications;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Online;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;

namespace osu.GameModes.Online
{
    internal class JoinGameDialog : pDialog
    {
        private readonly bMatch info;
        private readonly List<pSprite> passwordSprites = new List<pSprite>();
        private pTextBox gamePassword;

        internal JoinGameDialog(bMatch info)
            : base("Joining this game requires a password...", true)
        {
            this.info = info;
        }

        internal override void Display()
        {
            pText t = new pText("Password:", 16, new Vector2(30, 120), 0.92f, true, Color.White);
            spriteManager.Add(t);
            passwordSprites.Add(t);

            gamePassword = new pTextBox("", 16, new Vector2(150, 120), 450, 0.92f);
            gamePassword.LengthLimit = 50;

            currentVerticalSpace = 180;

            spriteManager.Add(gamePassword.SpriteCollection);

            AddOption("Join Game", Color.LimeGreen, onClick, false);
            AddOption("Cancel", Color.LightGray, cancel, false);

            base.Display();

            Background.InitialColour = new Color(15, 59, 87, 200);

            gamePassword.Select();
        }

        private void cancel(object sender, EventArgs e)
        {
            if (Lobby.Status == LobbyStatus.PendingJoin)
                return;

            Close();
        }

        private void onClick(object sender, EventArgs e)
        {
            if (Lobby.Status == LobbyStatus.PendingJoin)
                return;

            if (gamePassword.Box.Text.Length == 0)
            {
                NotificationManager.ShowMessage("Enter a password before attempting to start the game...");
                return;
            }

            Lobby.JoinMatch(info, gamePassword.Box.Text);
            Close();
        }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        internal override bool HandleKey(Keys k)
        {
            if (!base.HandleKey(k))
                return false;

            if (k == Keys.Enter)
                onClick(null,null);

            return true;
        }
    }
}