﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Graphics.Sprites;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Renderers;
using osu.Input;
using osu.Input.Handlers;
using Microsoft.Xna.Framework.Input;
using osu.GameModes.Play.Components;
using osu.Graphics.Skinning;
using System.Threading;
using osu_common;
using osu.Configuration;
using osu.Helpers;
using System.Diagnostics;
using osu.GameplayElements.Beatmaps;
using osu.GameModes.Select;
using osu.Graphics.Notifications;
using osu.GameModes.Menus;
using osu.Audio;
using osu.Online;
using osu.GameModes.Online;
using osu.Online.Drawable;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu.Graphics.UserInterface;
using osu_common.Helpers;
using osu.GameplayElements.Scoring;
using System.IO;

namespace osu.GameModes.Other
{
    class Tournament : DrawableGameComponent
    {
        public const int padding = 4;

        internal static Vector2 ClientWorkingSpace = new Vector2(1280, 720);
        internal static Vector2 CompleteWorkingSpace = ClientWorkingSpace + new Vector2(0, 200);

        public const string CONFIG_FILENAME = @"tournament.cfg";

        static Tournament Instance;

        static Color purple = new Color(255, 255, 255);

        const int hudHeight = 280;

        pText nowPlaying = new pText(string.Empty, 12, new Vector2(0, 350), 1, true, Color.White)
        {
            Origin = Origins.BottomLeft,
            TextShadow = true,
            TextColour = new Color(85, 85, 85)
        };

        pText team1name = new pText(string.Empty, 24, new Vector2(0, hudHeight - 20), 1, true, purple)
        {
            Origin = Origins.TopLeft,
            TextBold = true,
            TextBorder = true
        };
        pText team2name = new pText(string.Empty, 24, new Vector2(0, hudHeight - 20), 1, true, purple)
        {
            Field = Fields.TopRight,
            Origin = Origins.TopRight,
            TextBold = true,
            TextBorder = true
        };

        pText team1count = new pText(@"0", 48, new Vector2(GameBase.WindowWidthScaled / 2 - 15, hudHeight), 1, true, Color.White)
        {
            Origin = Origins.CentreRight,
            TextBold = true,
            TextBorder = true
        };

        pText team2count = new pText(@"0", 48, new Vector2(GameBase.WindowWidthScaled / 2 + 15, hudHeight), 1, true, Color.White)
        {
            Origin = Origins.CentreLeft,
            TextBold = true,
            TextBorder = true
        };

        pText warmup = new pText(@"warm-up", 26, new Vector2(GameBase.WindowWidthScaled / 2, 300), 1, true, Color.TransparentWhite) { BackgroundColour = Color.Black, Origin = Origins.TopCentre, TextBold = true };

        pText team1score = new pText("0000000", 24, new Vector2(0, hudHeight), 1, true, purple)
        {
            Origin = Origins.TopLeft,
            TextBold = true,
            TextBorder = true
        };

        pText team2score = new pText("0000000", 24, new Vector2(0, hudHeight), 1, true, purple)
        {
            Field = Fields.TopRight,
            Origin = Origins.TopRight,
            TextBold = true,
            TextBorder = true
        };

        pSprite team1image = new pSprite(null, new Vector2(30, hudHeight), 1, true, Color.White) { Origin = Origins.Centre, Scale = 0.3f };
        pSprite team2image = new pSprite(null, new Vector2(30, hudHeight), 1, true, Color.White) { Origin = Origins.Centre, Field = Fields.TopRight, Scale = 0.3f };

        List<pSprite> clientBackgrounds = new List<pSprite>();

        bool pickingUp;
        private static int score1;
        private static int score2;
        static string acronym;
        internal static pConfigManager Config = new pConfigManager(CONFIG_FILENAME);

        static int team_size = Config.GetValue<int>(@"TeamSize", 4);
        static int client_count = team_size * 2;

        internal static Vector2 ClientSize
        {
            get
            {
                switch (client_count)
                {
                    case 1:
                    case 2:
                        return new Vector2(ClientWorkingSpace.X / 2, ClientWorkingSpace.X / 2 / 4 * 3);
                    default:
                        return new Vector2(ClientWorkingSpace.X / 4, ClientWorkingSpace.X / 4 / 4 * 3);
                }
            }
        }

        internal static Vector2 ClientSizePadded
        {
            get
            {
                return ClientSize - new Vector2(padding * 2, padding * 2);
            }
        }

        static List<InterProcessOsu> clients = new List<InterProcessOsu>();

        SpriteManager spriteManager = new SpriteManager(true);
        public Tournament(GameBase game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            Instance = this;

            acronym = Config.GetValue<string>("acronym", "OWC");

            team1score.OnRefreshTexture += onScoreTextChanged;
            team2score.OnRefreshTexture += onScoreTextChanged;

            GameBase.LoadComplete();

            spriteManager.Add(nowPlaying);
            spriteManager.Add(team1name);
            spriteManager.Add(team2name);

            spriteManager.Add(team1image);
            spriteManager.Add(team2image);

            spriteManager.Add(team1count);
            spriteManager.Add(team2count);

            spriteManager.Add(team1score);
            spriteManager.Add(team2score);

            spriteManager.Add(warmup);

            int x = GameBase.WindowWidthScaled - 200;
            int y = 380;

            spriteManager.Add(new pButton(@"sync music", new Vector2(x, y), new Vector2(80, 15), 1, Color.YellowGreen, delegate
            {
                AudioEngine.Play();
            }).SpriteCollection);

            y += 16;

            spriteManager.Add(new pButton(@"toggle warmup", new Vector2(x, y), new Vector2(80, 15), 1, Color.YellowGreen, delegate
            {
                toggleWarmup();
            }).SpriteCollection);

            y += 16;

            spriteManager.Add(new pButton(@"panic", new Vector2(x, y), new Vector2(80, 15), 1, Color.Red, delegate
            {
                ResetClients();
            }).SpriteCollection);

            y += 16;

            spriteManager.Add(new pButton(@"exit", new Vector2(x, y), new Vector2(80, 15), 1, Color.Red, delegate
            {
                GameBase.BeginExit();
            }).SpriteCollection);

            team1count.HandleInput = true;
            team2count.HandleInput = true;

            team1count.OnClick += changeScore;
            team2count.OnClick += changeScore;

            BeatmapManager.Initialize();

            BeatmapManager.DatabaseSerialize();

            int connectPeriod = 10000;
            while (!BanchoClient.Connected && (connectPeriod -= 50) > 0)
                Thread.Sleep(50);

            if (!BanchoClient.Connected)
            {
                TopMostMessageBox.Show("tournament server is not available. please contact peppy");
                osuMain.ExitImmediately();
                return;
            }

            Lobby.JoinLobby();

            updateLobby();

            if (clients.Count == 0)
            {
                GameBase.RunBackgroundThread(() =>
                {
                    for (int i = 0; i < client_count; i++)
                    {
                        pSprite bg = new pSprite(GameBase.WhitePixel, PositionForClient(i), 1, true, Color.TransparentWhite) { VectorScale = ClientSize, Field = Fields.Native };
                        spriteManager.Add(bg);
                        clientBackgrounds.Add(bg);

                        clients.Add(startClient(i));
                    }

                    Logger.Log("Initialisation Complete!");
                    State = TourneyState.Idle;
                });
            }
            else
            {
                if (GameBase.LastMode == OsuModes.BeatmapImport)
                    foreach (InterProcessOsu o in clients)
                        o.ReloadDatabase();
            }

            pSpriteDynamic background = new pSpriteDynamic(Config.GetValue<string>("background", "http://dl.dropboxusercontent.com/u/16332218/tourney/default.png"), null, 0, Vector2.Zero, 0);
            background.AlwaysDraw = true;
            background.OnTextureLoaded += delegate
            {
                background.ScaleToScreen(false, true);
            };
            spriteManager.Add(background);

            requestMatchInfo();

            base.Initialize();
        }

        void onScoreTextChanged(object sender, EventArgs args)
        {
            pText text = sender as pText;
            text.Scale = 1.05f;
            text.ScaleTo(1, 100);
        }

        private void toggleWarmup()
        {
            if (warmup.IsVisible)
                warmup.FadeOut(500);
            else
                warmup.FadeIn(500);
        }

        void changeScore(object sender, EventArgs e)
        {
            pText p = sender as pText;

            if (MouseManager.MouseRight == ButtonState.Pressed)
                p.TagNumeric--;
            else
                p.TagNumeric++;

            p.Text = p.TagNumeric.ToString();
            p.Scale = 1.2f;
            p.ScaleTo(1, 500, EasingTypes.Out);
        }

        private void updateLobby()
        {
            matches.ForEach(p => p.FadeOut(0));
            matches.Clear();

            lock (Lobby.Matches)
            {
                int y = 380;

                bool limitDisplay = false;

                foreach (LobbyMatch m in Lobby.Matches)
                {
                    if (limitDisplay && m.matchInfo.slotPlayingCount < 5)
                        continue;

                    if (!m.matchInfo.gameName.StartsWith(acronym))
                        continue;

                    pText t = new pText(m.matchInfo.matchId + @" : @" + m.matchInfo.gameName + @" (" + m.matchInfo.beatmapName + @") " + m.matchInfo.slotPlayingCount + @"/" + m.matchInfo.slotOpenCount, 7, new Vector2(0, y), 1, true, Color.White);
                    t.HandleInput = true;
                    t.TextBold = m.matchInfo == SpectatingMatch;
                    t.OnClick += delegate
                    {
                        SetMatch(m);
                    };
                    matches.Add(t);
                    spriteManager.Add(t);

                    y += (int)t.MeasureText().Y;
                }

                if (y == 380)
                {
                    pText t = new pText("Make a multiplayer room with format:", 10, new Vector2(0, y), 1, true, Color.White);
                    matches.Add(t);
                    spriteManager.Add(t);
                    y += (int)t.MeasureText().Y;
                    t = new pText(acronym + ": (team 1 name) vs (team 2 name)", 10, new Vector2(0, y), 1, true, Color.White);
                    t.TextBold = true;
                    matches.Add(t);
                    spriteManager.Add(t);
                }
            }

            GameBase.Scheduler.AddDelayed(updateLobby, 5000);
        }

        private void requestMatchInfo()
        {
            if (SpectatingMatch != null)
            {
                BanchoClient.SendRequest(RequestType.Osu_SpecialMatchInfoRequest, new bInt(SpectatingMatch.matchId));
            }

            GameBase.Scheduler.AddDelayed(requestMatchInfo, 1000);
        }

        internal static Vector2 PositionForClient(int i, bool padded = false)
        {
            switch (client_count)
            {
                case 1:
                case 2:
                    return new Vector2((i >= 1 ? ClientSize.X : 0) + (padded ? padding : 0), (padded ? padding : 0));
                case 3:
                case 4:
                    return new Vector2((i >= 2 ? ClientSize.X : 0) + (padded ? padding : 0) + ClientSize.X, (i % 2 == 1 ? 1 : 0) * ClientSize.Y + (padded ? padding : 0));
                case 6:
                    switch (i)
                    {
                        case 0:
                            return new Vector2((padded ? padding : 0) + ClientSize.X / 2, (padded ? padding : 0));
                        case 1:
                        case 2:
                            return new Vector2((padded ? padding : 0) + ClientSize.X * (i - 1), (padded ? padding : 0) + ClientSize.Y);
                        case 3:
                            return new Vector2((padded ? padding : 0) + ClientSize.X * 2.5f, (padded ? padding : 0));
                        case 4:
                        case 5:
                            return new Vector2((padded ? padding : 0) + ClientSize.X * (2 + (i - 4)), (padded ? padding : 0) + ClientSize.Y);
                    }

                    //fallback
                    return new Vector2((i >= 4 ? ClientSize.X * 2 : 0) + i % 2 * ClientSize.X + (padded ? padding : 0), ((i % 4 > 1 ? 1 : 0) * ClientSize.Y) + (padded ? padding : 0));
                case 5:
                case 7:
                case 8:
                default:
                    return new Vector2((i >= 4 ? ClientSize.X * 2 : 0) + i % 2 * ClientSize.X + (padded ? padding : 0), ((i % 4 > 1 ? 1 : 0) * ClientSize.Y) + (padded ? padding : 0));
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (GameBase.Mode == OsuModes.Exit)
                killAllClients();

            spriteManager.Dispose();
            base.Dispose(disposing);
        }

        public override void Draw()
        {
            base.Draw();

            spriteManager.Draw();
        }

        private LobbyMatch SpectatingMatchContainer;
        private bMatch SpectatingMatch
        {
            get
            {
                return SpectatingMatchContainer == null ? null : SpectatingMatchContainer.matchInfo;
            }
        }

        enum TourneyState
        {
            Initialising,
            Idle,
            WaitingForClients,
            Playing,
            Ranking
        }

        static TourneyState state;
        static TourneyState State
        {
            get { return state; }
            set
            {
                if (state == value) return;

                state = value;

                switch (state)
                {
                    case TourneyState.Ranking:
                        if (!Instance.warmup.IsVisible)
                            if (score1 > score2)
                                Instance.team1count.Click();
                            else
                                Instance.team2count.Click();
                        break;
                }

                AccumulatedStateTime = 0;
                waitForBuffer = true;
                Logger.Log("Tournament Manager state changed to {0}", state.ToString());
            }
        }

        static double AccumulatedStateTime;

        List<pSprite> matches = new List<pSprite>();

        private static readonly int BUFFER_TOTAL = Config.GetValue<int>("BufferTotalTime", 3000);
        private static readonly int BUFFER_DANGER = Config.GetValue<int>("BufferDangerTime", 1000);
        private static readonly int BUFFER_TIMEOUT = Config.GetValue<int>("BufferTimeoutTime", 20000);
        private static bool waitForBuffer;
        private static double waitStartTime;

        public override void Update()
        {
            AccumulatedStateTime += GameBase.ElapsedMilliseconds;
            base.Update();

            if (State != TourneyState.Initialising)
                updateStatus();

            if ((BeatmapManager.Current == null && BeatmapManager.Beatmaps.Count > 0)
                || SpectatingMatch == null
                || State == TourneyState.Initialising || (State == TourneyState.Idle && AudioEngine.AudioState == AudioStates.Stopped))
            {
                MusicControl.StartRandomSongIfSilent();

                if (BeatmapManager.Current != null && State != TourneyState.Initialising)
                {
                    for (int i = 0; i < clients.Count; i++)
                    {
                        InterProcessOsu o = clients[i];
                        o.SetBeatmap(BeatmapManager.Current.BeatmapChecksum);
                        o.SetMenuTime(AudioEngine.Time);
                    }
                }
                return;
            }

            updateBeatmap();

            int actualTime = AudioEngine.Time;

            List<InterProcessOsu.ClientData> clientData = new List<InterProcessOsu.ClientData>();
            for (int i = 0; i < clients.Count; i++)
                clientData.Add(clients[i].GetBulkClientData());

            int clientTime = int.MaxValue;
            int availableTime = int.MaxValue;

            switch (State)
            {
                case TourneyState.Ranking:
                    actualTime = int.MaxValue / 2;
                    clientData.ForEach(c => actualTime = Math.Min(c.AudioTime, actualTime));
                    actualTime += 1000;

                    if (AccumulatedStateTime > 40000)
                        clients.ForEach(c => c.ChangeMode(OsuModes.Menu));
                    break;
                case TourneyState.Playing:
                    for (int i = 0; i < clientData.Count; i++)
                    {
                        if (clientData[i].SkipCalculations)
                            continue;
                        clientTime = Math.Min(clientData[i].AudioTime, clientTime);
                        availableTime = Math.Min(clientData[i].ReplayTime, availableTime);
                    }

                    if (clientTime <= 0)
                    {
                        AudioEngine.SeekTo(0);
                        if (!AudioEngine.Paused)
                            AudioEngine.TogglePause();
                    }

                    //Check if we need to pause and refill the buffer
                    //There's a 3 second buffer window until the last replay frame which the client will sync the audio to.
                    //We are safe if we're outside this window or inside unless we are 1 second away from the last replay frame.
                    //If we are 1 second away from the last replay frame, we stop the audio until we're 3 seconds away from the last
                    //replay frame (at the buffer time)
                    ////////////////////////////////////////////////////////////////////////////////////
                    //   Buffer Time               Safe Until Here    Stop Region    Available Time   //
                    //      //\\                         //\\            //\\              //\\       //
                    //        |----------------------------|--------------------------------|         //
                    //                     2s                             1s                          //
                    ////////////////////////////////////////////////////////////////////////////////////
                    waitForBuffer = waitForBuffer ? AudioEngine.Time >= availableTime - BUFFER_TOTAL : AudioEngine.Time >= availableTime - BUFFER_DANGER;

                    //Sync follows the same concept as above except that
                    //desync is a lot more sensitive, so we must use a moving average
                    //rather than a static value to reduce the total stuttering
                    int clientDiff = AudioEngine.Time - clientTime;

                    actualTime = waitForBuffer ? availableTime : clientTime + (AudioEngine.CurrentPlaybackRate > 100 ? 140 : 100);

                    bool pauseRequired = waitForBuffer || (actualTime <= AudioEngine.Time && Math.Abs(actualTime - AudioEngine.Time) < 1000);

                    if ((waitForBuffer && !AudioEngine.Paused) || !waitForBuffer)
                        waitStartTime = AccumulatedStateTime;

                    if ((pauseRequired && !AudioEngine.Paused) || (!pauseRequired && AudioEngine.Paused))
                        AudioEngine.TogglePause();

                    if (AudioEngine.Paused)
                    {
                        for (int i = 0; i < clientData.Count; i++)
                        {
                            if (clientData[i].AudioTime < 0)
                                continue;

                            if (!clientData[i].Buffering)
                                clients[i].ToggleClientBuffer();
                            clientData[i].OverridePause = true;
                        }
                    }

                    string logString = string.Format("SYNC: {0} | DIFF: {1} | CLNT: {2} | AUDIO: {3} | BUF: {4} | AVAIL: {5}",
                                                        actualTime.ToString().PadRight(6), Math.Abs(AudioEngine.Time - clientTime).ToString().PadRight(6),
                                                        clientTime.ToString().PadRight(6), AudioEngine.Time.ToString().PadRight(6), (availableTime - BUFFER_TOTAL).ToString().PadRight(6),
                                                        availableTime.ToString().PadRight(6));
                    logString += AudioEngine.Paused ? " | PAUSED" : "";
                    Debug.WriteLine(logString);

                    if (clientTime > 0 && Math.Abs(AudioEngine.Time - clientTime) >= 20)
                        AudioEngine.SeekTo(clientTime);
                    break;
                default:
                    break;
            }

            score1 = 0;
            score2 = 0;

            for (int i = 0; i < clientData.Count; i++)
            {
                int s = clientData[i].Score;
                if (i < clients.Count / 2)
                    score1 += s;
                else
                    score2 += s;

                OsuModes mode = clientData[i].Mode;

                if (mode != OsuModes.Rank && clientData[i].SpectatingID != SpectatingMatch.slotId[i])
                    clients[i].SetSpectate(SpectatingMatch.slotId[i]);

                if (mode != OsuModes.Play && BeatmapManager.Current != null)
                    clients[i].SetBeatmap(BeatmapManager.Current.BeatmapChecksum);

                //If we're still waiting on a client for more than the timeout time
                //skip them from any future sync calculations
                if ((waitForBuffer && Math.Abs(AccumulatedStateTime - waitStartTime) > BUFFER_TIMEOUT
                    && clientData[i].ReplayTime == availableTime && !clientData[i].SkipCalculations)
                    //Or if they recover and are within acceptable range of the current time
                    //include them in sync calculations again
                    || (clientData[i].AudioTime >= actualTime && clientData[i].SkipCalculations))
                {
                    clients[i].ToggleSkipCalculations();
                    waitStartTime = AccumulatedStateTime;
                }

                if (clientData[i].AudioTime > actualTime && !clientData[i].Buffering)
                    clients[i].ToggleClientBuffer();
                else if (clientData[i].AudioTime <= actualTime && !clientData[i].OverridePause && clientData[i].Buffering)
                    clients[i].ToggleClientBuffer();

                clients[i].SetUserDim(50);
            }

            if (State == TourneyState.Idle)
            {
                team1score.Text = string.Empty;
                team2score.Text = string.Empty;
            }
            else
            {
                team1score.Text = score1 > 0 ? score1.ToString("#,0") : string.Empty;
                team2score.Text = score2 > 0 ? score2.ToString("#,0") : string.Empty;

                team1score.InitialColour = score1 > score2 ? Color.YellowGreen : Color.White;
                team2score.InitialColour = score2 > score1 ? Color.YellowGreen : Color.White;
            }
        }

        private void ResetClients()
        {
            Logger.Log("Resetting all clients");

            clients.ForEach(c =>
            {
                c.SetSpectate(-1);
                c.ChangeMode(OsuModes.Menu);
            });
        }

        private void SetMatch(LobbyMatch match)
        {
            ResetClients();
            SpectatingMatchContainer = match;

            string team1 = string.Empty;
            string team2 = string.Empty;

            string gameName = match.matchInfo.gameName;

            int i = 0;

            if ((i = gameName.IndexOf(':')) > 0)
                gameName = gameName.Substring(i + 1);

            string[] teams = gameName.Split('(', ')', '（', '）');

            if (teams.Length >= 4)
            {
                team1 = teams[1].Trim();
                team2 = teams[3].Trim();
            }
            else
            {
                team1 = "-";
                team2 = "-";
            }


            team1name.Text = team1;
            team2name.Text = team2;

            team1image.Texture = SkinManager.Load("flags/" + team1);
            team2image.Texture = SkinManager.Load("flags/" + team2);

            team1name.Position.X = team1score.Position.X = team1image.Width / 1.6f * team1image.Scale + 10;
            team2name.Position.X = team2score.Position.X = team2image.Width / 1.6f * team2image.Scale + 10;
        }

        private void updateBeatmap()
        {
            if (BeatmapImport.IsProcessing) return;

            if ((SpectatingMatch.activeMods & Mods.DoubleTime) > 0)
                AudioEngine.CurrentPlaybackRate = 150;
            else if ((SpectatingMatch.activeMods & Mods.HalfTime) > 0)
                AudioEngine.CurrentPlaybackRate = 75;
            else
                AudioEngine.CurrentPlaybackRate = 100;

            if (BeatmapManager.Current != null && SpectatingMatch.beatmapChecksum == BeatmapManager.Current.BeatmapChecksum) return;

            if (string.IsNullOrEmpty(SpectatingMatch.beatmapChecksum))
            {
                if (pickingUp) pickingUp = false;
            }
            else
            {
                Beatmap b = BeatmapManager.GetBeatmapByChecksum(SpectatingMatch.beatmapChecksum);

                if (b == null)
                {
                    if (pickingUp) return;
                    pickingUp = true;

                    OsuDirect.HandlePickup(SpectatingMatch.beatmapChecksum, delegate
                    {
                        BeatmapImport.Start();
                        pickingUp = false;
                    }, null);
                }
                else
                {
                    nowPlaying.Text = "Now Playing: " + b.DisplayTitleFullAuto;

                    BeatmapManager.Load(b);
                    ResetClients();
                }
            }
        }

        private void updateStatus()
        {
            int playing = clients.FindAll(o => o.GetCurrentMode() == OsuModes.Play).Count;
            int ranking = clients.FindAll(o => o.GetCurrentMode() == OsuModes.Rank).Count;

            if (ranking > 0)
                State = TourneyState.Ranking;
            else if (playing > 0)
                State = TourneyState.Playing;
            else
                State = TourneyState.Idle;
        }

        private InterProcessOsu startClient(int id)
        {
            Process.Start(osuMain.FilenameWithoutExtension, "-spectateclient " + id);
            InterProcessOsu osu = null;

            while (osu == null)
            {
                try
                {
                    Thread.Sleep(50);
                    osu = (InterProcessOsu)Activator.GetObject(typeof(InterProcessOsu), "ipc://osu!-spectator-" + id + "/loader");
                    osu.GetCurrentMode();
                }
                catch
                {
                    osu = null;
                }
            }

            flashClient(id, Color.YellowGreen);

            return osu;
        }

        private void flashClient(int id, Color colour)
        {
            clientBackgrounds[id].InitialColour = colour;
            clientBackgrounds[id].FadeOutFromOne(1500);
        }

        private void killAllClients()
        {
            foreach (InterProcessOsu o in clients)
            {
                try
                {
                    o.Quit();
                }
                catch { }
            }

            clients.Clear();
        }
    }
}
