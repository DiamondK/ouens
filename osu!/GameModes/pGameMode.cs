﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Graphics.Sprites;

namespace osu.GameModes
{
    class pGameMode : DrawableGameComponent
    {
        internal SpriteManager baseSpriteManager = new SpriteManager(true);

        public pGameMode() : base(GameBase.Instance)
        {

        }

        public override void Initialize()
        {
            base.Initialize();
            GameBase.LoadComplete();
        }

        public override void Draw()
        {
            baseSpriteManager.Draw();
            base.Draw();
        }

        public override void Update()
        {
            base.Update();
        }
    }
}
