using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Renderers;
using osu.Graphics.Sprites;

namespace osu.GameModes.Options
{
    internal class OptionVersion : OptionElement
    {
        private pText header;
        public OptionVersion(string title)
        {
            int extraHeight = 20;
            header = new pText(title, 12, new Vector2(0, extraHeight / 2), new Vector2(ELEMENT_SIZE, 0), 1, true, Color.White);
            header.TextAlignment = TextAlignment.Centre;
            header.HandleInput = true;
            header.OnClick += delegate
            {
                GameBase.ProcessStart(@"http://osu.ppy.sh/p/changelog?v=" + General.BUILD_NAME);
            };

            header.ClickRequiresConfirmation = true;

            spriteManager.Add(header);

            Size = Size + new Vector2(0, extraHeight);
            addKeyword("version");
        }

        internal override bool IsHeaderElement
        {
            get
            {
                return true;
            }
        }
    }
}