using System;
using System.Collections.Generic;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameModes.Select;
using osu.GameModes.Select.Drawable;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu_common.Helpers;

namespace osu.GameModes.Options
{
    internal class OptionsOffsetWizard : DrawableGameComponent
    {
        private SpriteManager spriteManager;

        private pText text;
        private pCheckbox tickonotherbeat;
        private pSprite middle;

        private List<pSprite> lines;
        private const float velocity = 120;
        private int totaltime;

        private pSprite bg;

        internal OptionsOffsetWizard(Game game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            KeyboardHandler.OnKeyRepeat += GameBase_OnKeyRepeat;

            InputManager.Bind(InputEventType.OnMouseWheelUp, onMouseWheelUp);
            InputManager.Bind(InputEventType.OnMouseWheelDown, onMouseWheelDown); 

            spriteManager = new SpriteManager(true);

            BackButton back = new BackButton(back_OnClick);
            spriteManager.Add(back.SpriteCollection);

            // instructions
            pText instructions =
                new pText(LocalisationManager.GetString(OsuString.OptionsOffsetWizard_Instructions), 16f,
                          new Vector2(GameBase.WindowWidthScaled / 2, 400), new Vector2(500, 0), 0.8f, true, Color.White, false);
            instructions.TextAlignment = TextAlignment.Centre;
            instructions.Origin = Origins.TopCentre;
            spriteManager.Add(instructions);

            // icon in middle
            middle = new pSprite(SkinManager.Load(@"options-offset-tick"), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(GameBase.WindowWidthScaled / 2, 240), 0.8f, true, Color.White);
            middle.VectorScale = new Vector2(0.1f, 2);
            spriteManager.Add(middle);

            //offset
            text = new pText(LocalisationManager.GetString(OsuString.OptionsOffsetWizard_UniversalOffset), 30f, new Vector2(50, 20), 0.8f, true, Color.White);
            spriteManager.Add(text);



            text = new pText(ConfigManager.sOffset.ToString(), 30f, new Vector2(50 + text.MeasureText().X, 20), 0.8f, true, Color.YellowGreen);
            text.TextBold = true;
            spriteManager.Add(text);

            //ticking beat checkbox
            tickonotherbeat = new pCheckbox(LocalisationManager.GetString(OsuString.OptionsOffsetWizard_TickOther), new Vector2(50, 80), 0.8f, false);
            spriteManager.Add(tickonotherbeat.SpriteCollection);

            tickonotherbeat.OnCheckChanged += tickonotherbeat_OnCheckChanged;
            halfticker = new pCheckbox(LocalisationManager.GetString(OsuString.OptionsOffsetWizard_HalfTick), new Vector2(50, 60), 0.8f, false);
            spriteManager.Add(halfticker.SpriteCollection);

            halfticker.OnCheckChanged += halfticker_OnCheckChanged;
            doubleticker = new pCheckbox(LocalisationManager.GetString(OsuString.OptionsOffsetWizard_DoubleTick), new Vector2(150, 60), 0.8f, false);
            spriteManager.Add(doubleticker.SpriteCollection);

            doubleticker.OnCheckChanged += doubleticker_OnCheckChanged;
            tickonotherbeat.Hide();

            //list for moving lines
            lines = new List<pSprite>();

            if (AudioEngine.AudioTrack.Preview)
            {
                AudioEngine.LoadAudio(BeatmapManager.Current, false, false);
                AudioEngine.ResetAudioTrack();
                AudioEngine.Play();
            }


            base.Initialize();

            //Calling this allows the fade-in transition to proceed, and Update()/Draw() to be called.
            GameBase.LoadComplete();

            setupBeatmap();


        }

        Beatmap loadedMap;
        private void setupBeatmap(bool force = false)
        {
            if (BeatmapManager.Current == loadedMap && !force)
                return;

            loadedMap = BeatmapManager.Current;
            beatcount = 0;
            halfbeatlength = 0;
            timebefore = 0;
            totaltime = 0;
            dummycount = 0;
            clearall = true;
        }

        bool onMouseWheelUp(object sender, EventArgs e)
        {
            ChangeOffset(1);
            return true;
        }

        private void ChangeOffset(int amount)
        {
            ConfigManager.sOffset.Value += amount;
            text.Text = ConfigManager.sOffset.ToString();
            text.FlashColour(Color.White, 200);
        }

        bool onMouseWheelDown(object sender, EventArgs e)
        {
            ChangeOffset(-1);
            return true;
        }

        private static void back_OnClick(object sender, EventArgs e)
        {
            //AudioEngine.continuePlayback = true;
            AudioEngine.PlaySamplePositional(@"menuback");
            GameBase.ChangeMode(OsuModes.Menu);
        }

        private bool clearall = false;
        private bool stopticking = false;
        private bool linecreated = false;

        private void halfticker_OnCheckChanged(object sender, bool state)
        {
            if (state)
            {
                tickonotherbeat.Show();
                doubleticker.SetStatusQuietly(false);
            }
            else
                tickonotherbeat.Hide();

            clearall = true;
            linecreated = false;
            stopticking = true;
        }

        private void tickonotherbeat_OnCheckChanged(object sender, bool state)
        {
            clearall = true;
            linecreated = false;
            stopticking = true;
        }

        private void doubleticker_OnCheckChanged(object sender, bool state)
        {
            halfticker.SetStatusQuietly(false);
            tickonotherbeat.Hide();

            clearall = true;
            linecreated = false;
            stopticking = true;
        }

        private bool GameBase_OnKeyRepeat(object sender, Keys k, bool first)
        {
            //Keyboard handling
            switch (k)
            {
                case Keys.Up:
                    if (KeyboardHandler.ShiftPressed)
                        ChangeOffset(5);
                    else
                        ChangeOffset(1);
                    return true;
                case Keys.Down:
                    if (KeyboardHandler.ShiftPressed)
                        ChangeOffset(-5);
                    else
                        ChangeOffset(-1);
                    return true;
                case Keys.Escape:
                    back_OnClick(sender, null);
                    return true;
            }

            return false;
        }

        // this is the entry point for new lines
        private const int linex = 560;

        private void CreateLine()
        {
            pSprite line = new pSprite(SkinManager.Load(@"options-offset-tick"), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(linex, 240), 0.8f, true, Color.White);
            line.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, GameBase.Time, GameBase.Time + 300));
            line.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + totaltime - 300, GameBase.Time + totaltime));
            line.Transformations.Add(new Transformation(TransformationType.Movement, new Vector2(linex, 240), new Vector2(GameBase.WindowWidthScaled - linex, 240),
                                                        GameBase.Time, GameBase.Time + totaltime));
            line.VectorScale = new Vector2(0.3f, 1);
            line.Transformations.Add(new Transformation(TransformationType.VectorScale, new Vector2(1, 1), new Vector2(0.3f, 1), GameBase.Time + totaltime / 2, GameBase.Time + totaltime / 2 + 100));
            spriteManager.Add(line);
            lines.Add(line);

            if (!linecreated)
            {
                linecreated = true;
                linecreationtime = (int)(AudioEngine.Time - AudioEngine.CurrentOffset);
            }
        }

        private int linecreationtime;

        protected override void Dispose(bool disposing)
        {
            KeyboardHandler.OnKeyRepeat -= GameBase_OnKeyRepeat;

            spriteManager.Dispose();
            base.Dispose(disposing);
        }

        private int beatcount;
        private int dummycount;
        private pCheckbox doubleticker;
        private pCheckbox halfticker;
        private int timebefore;
        private double halfbeatlength;

        public override void Update()
        {
            setupBeatmap();

            if (totaltime == 0)
                totaltime = (int)(((2 * linex - GameBase.WindowWidthScaled) / velocity) * AudioEngine.beatLength);
            if (timebefore == 0)
                timebefore = (int)((linex - GameBase.WindowWidthScaled / 2) / velocity * AudioEngine.beatLength);
            if (halfbeatlength == 0)
                halfbeatlength = AudioEngine.beatLength / 2;

            double time = AudioEngine.Time - AudioEngine.CurrentOffset;

            // this calculates when to show the next line
            int dummy = (int)((time + timebefore) / halfbeatlength);
            if (dummy > dummycount)
            {
                dummycount = dummy;

                if (Activate(dummycount))
                    CreateLine();
            }

            // this calculates when to sound a tick
            int bc = (int)(time / halfbeatlength);
            if (bc < beatcount)
                setupBeatmap(true);
            else if (bc > beatcount)
            {
                beatcount = bc;

                if (linecreated && !stopticking)
                {
                    if (Activate(beatcount))
                    {
                        AudioEngine.PlaySample(@"metronomelow");
                        middle.Transformations.Add(new Transformation(Color.White, Color.Black, GameBase.Time, GameBase.Time + 80));
                    }
                }
                else if (linecreated)
                {
                    if (time > linecreationtime + (timebefore / 2))
                        stopticking = false;
                }
            }

            // this calculates when to delete sprites
            for (int i = 0; i < lines.Count; i++)
            {
                if (lines[i].Position.X <= GameBase.WindowWidthScaled - linex)
                {
                    spriteManager.Remove(lines[i]);
                    lines.RemoveAt(i);
                }
            }

            if (clearall)
            {
                lines.ForEach(l => { l.FadeOut(300); l.AlwaysDraw = false; });
                clearall = false;
            }

            base.Update();
        }

        private bool Activate(int count)
        {
            int divisor = 2;
            if (doubleticker.Checked) divisor = 1;
            else if (halfticker.Checked) divisor = 4;

            int remainder = 0;
            if (halfticker.Checked && tickonotherbeat.Checked) remainder = 2;

            return count % divisor == remainder;
        }

        public override void Draw()
        {
            //Call anything you need to actually draw, exclusive of any calculations.  Keep it to only GPU-intensive calls if possible.
            //You can control draw order of different components depending on the arrangement in this method.

            spriteManager.Draw();

            base.Draw();
        }
    }
}