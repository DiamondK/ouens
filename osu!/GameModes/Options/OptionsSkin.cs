using System;
using System.Collections.Generic;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Scoring;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input.Handlers;
using osu_common;
using osu.Graphics.Notifications;
using osu_common.Helpers;

namespace osu.GameModes.Options
{
    internal class OptionsSkin : DrawableGameComponent
    {
        private const int SKINS_PER_PAGE = 12;

        private HitObjectManager hitObjectManager;
        private readonly SpriteManager spriteManager;
        private pText currentSkin;
        private int page;
        private List<SkinOsu> Skins = new List<SkinOsu>();

        internal OptionsSkin(Game game)
            : base(game)
        {
            KeyboardHandler.OnKeyPressed += GameBase_OnKeyPressed;

            spriteManager = new SpriteManager(true);

            Player.Mode = PlayModes.Osu;

            Player.currentScore = null;

            foreach (string f in Directory.GetFiles(@"Skins"))
            {
                if (Path.GetExtension(f) == @".osk" || Path.GetExtension(f) == @".zip")
                {
                    try
                    {
                        FastZip fz = new FastZip();
                        string d = @"Skins/" + Path.GetFileNameWithoutExtension(f);
                        Directory.CreateDirectory(d);
                        fz.ExtractZip(f, d, @".*");
                    }
                    catch
                    {
                        NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.OptionsSkin_ImportFailed), f));
                    }
                }
            }

            SkinManager.RefreshAvailableSkins();

            foreach (string s in SkinManager.Skins)
            {
                SkinOsu sk = SkinManager.LoadSkinRaw(s);
                if (sk.RawName == @"Default")
                    Skins.Insert(0, sk);
                else
                    Skins.Add(sk);
            }

            page = Skins.IndexOf(SkinManager.Current) / SKINS_PER_PAGE;

            Reload(false);
        }

        private void Reload(bool loadNewSkin)
        {
            spriteManager.Clear();

            if (loadNewSkin)
            {
                SkinManager.LoadSkin(null, true);
                AudioEngine.CurrentSampleSet = SampleSet.None;
            }

            if (loadNewSkin || hitObjectManager == null)
            {
                if (hitObjectManager != null) hitObjectManager.Dispose();
                hitObjectManager = new HitObjectManagerOsu();

                if (BeatmapManager.Current != null)
                {
                    hitObjectManager.SetBeatmap(BeatmapManager.Current, Mods.None);
                    hitObjectManager.LoadWithEvents();
                }
            }

            int overlayX = GameBase.WindowWidthScaled - 200;

            BackButton back = new BackButton(back_OnClick);
            spriteManager.Add(back.SpriteCollection);

            pButton pbutton = new pButton(LocalisationManager.GetString(OsuString.OptionsSkin_GetMore), new Vector2(overlayX, 400), new Vector2(200, 30), 1, Color.White,
                                          delegate { GameBase.ProcessStart(@"http://osu.ppy.sh/forum/109"); });
            pbutton.Text.InitialColour = Color.Black;
            spriteManager.Add(pbutton.SpriteCollection);

            pbutton = new pButton(LocalisationManager.GetString(OsuString.OptionsSkin_Random), new Vector2(overlayX, 440), new Vector2(200, 30), 1, Color.White, newSong);
            pbutton.Text.InitialColour = Color.Black;
            spriteManager.Add(pbutton.SpriteCollection);

            pbutton = new pButton(LocalisationManager.GetString(OsuString.OptionsSkin_ExportAsOsk), new Vector2(overlayX, 360), new Vector2(200, 30), 1, Color.White, packageSkin);
            pbutton.Text.InitialColour = Color.Black;
            spriteManager.Add(pbutton.SpriteCollection);

            int verticalSpacing = 10;

            pText pt;
            pSprite pb;

            spriteManager.Add(new pText(LocalisationManager.GetString(OsuString.OptionsSkin_Available), 20, new Vector2(overlayX + 20, verticalSpacing), Vector2.Zero, 1, true, Color.White, true));

            verticalSpacing += 20;

            for (int i = page * SKINS_PER_PAGE; i < (page + 1) * SKINS_PER_PAGE; i++)
            {
                int linetextoffset = 0;
                if (i < Skins.Count)
                {
                    SkinOsu skin = Skins[i];
                    if (skin.RawName == SkinManager.Current.RawName)
                    {
                        pt =
                            new pText(SkinManager.Current.SkinName, 15, new Vector2(overlayX + 4, verticalSpacing),
                                      new Vector2(200, 0), 0.9f,
                                      true, Color.White, false);
                        pt.Tag = skin.RawName;
                        pt.TextAlignment = TextAlignment.LeftFixed;
                        pt.BackgroundColour = Color.Black;
                        pt.BorderWidth = 2;
                        pt.BorderColour = Color.LimeGreen;
                        pt.OnClick += pt_OnClick;
                        spriteManager.Add(pt);
                        linetextoffset = (int)pt.MeasureText().Y - 15;
                    }
                    else
                    {
                        pt =
                            new pText(skin.SkinName, 15, new Vector2(overlayX + 40, verticalSpacing), new Vector2(200, 0),
                                      0.9f,
                                      true, Color.White, false);
                        pt.Tag = skin.RawName;
                        pt.HandleInput = true;
                        pt.TextAlignment = TextAlignment.LeftFixed;
                        pt.BackgroundColour = Color.Black;
                        pt.BorderWidth = 2;
                        pt.BorderColour = Color.OrangeRed;
                        pt.HoverEffect =
                            new Transformation(pt.InitialPosition, pt.InitialPosition - new Vector2(36, 0), 0, 200);
                        pt.HoverEffect.Easing = EasingTypes.Out;
                        pt.OnClick += pt_OnClick;

                        spriteManager.Add(pt);
                        linetextoffset = (int)pt.MeasureText().Y - 15;
                    }
                }
                verticalSpacing += 20 + linetextoffset;
            }

            if (page > 0)
            {
                pt =
                    new pText(LocalisationManager.GetString(OsuString.OptionsSkin_Previous), 15, new Vector2(overlayX + 40, verticalSpacing), new Vector2(200, 0), 0.9f,
                              true, Color.White, false);
                pt.HandleInput = true;
                pt.TextAlignment = TextAlignment.LeftFixed;
                pt.BackgroundColour = Color.Black;
                pt.BorderWidth = 2;
                pt.BorderColour = Color.Orange;
                pt.HoverEffect = new Transformation(pt.InitialPosition, pt.InitialPosition - new Vector2(36, 0), 0, 200);
                pt.HoverEffect.Easing = EasingTypes.Out;
                pt.OnClick += prevPage;
                spriteManager.Add(pt);
            }


            verticalSpacing += 20;

            if (Skins.Count > (page + 1) * SKINS_PER_PAGE)
            {
                pt =
                    new pText(LocalisationManager.GetString(OsuString.OptionsSkin_Next), 15, new Vector2(overlayX + 40, verticalSpacing), new Vector2(200, 0), 0.9f,
                              true, Color.White, false);
                pt.HandleInput = true;
                pt.TextAlignment = TextAlignment.LeftFixed;
                pt.BackgroundColour = Color.Black;
                pt.BorderWidth = 2;
                pt.BorderColour = Color.Orange;
                pt.HoverEffect = new Transformation(pt.InitialPosition, pt.InitialPosition - new Vector2(36, 0), 0, 200);
                pt.HoverEffect.Easing = EasingTypes.Out;
                pt.OnClick += nextPage;
                spriteManager.Add(pt);
            }

            pAnimation pa =
                new pAnimation(SkinManager.LoadAll(@"scorebar-colour"), Fields.TopLeft, Origins.TopLeft,
                               Clocks.Game, new Vector2(3, 10), 0.95F, true, Color.White, null);
            pa.DrawWidth = 460;
            spriteManager.Add(pa);
            pb =
                new pSprite(SkinManager.Load(@"scorebar-ki"), Origins.Centre, new Vector2(287, 16), 0.97F, true,
                            Color.White);
            spriteManager.Add(pb);
            pb =
                new pSprite(SkinManager.Load(@"scorebar-kidanger"), Origins.Centre,
                            new Vector2(160, 16), 0.98F, true, Color.White);
            spriteManager.Add(pb);
            pb =
                new pSprite(SkinManager.Load(@"scorebar-kidanger2"), Origins.Centre, new Vector2(100, 16), 0.99F,
                            true, Color.White);
            spriteManager.Add(pb);

            pb = new pSprite(SkinManager.Load(@"scorebar-bg"), Vector2.Zero, 0.9F, true, Color.White);
            spriteManager.Add(pb);


            pSprite detailsBack = new pSprite(GameBase.WhitePixel, new Vector2(overlayX, 0), 0.2F, true,
                                              new Color(0, 0, 0, 160));
            detailsBack.VectorScale = new Vector2(320, 768);
            spriteManager.Add(detailsBack);

            currentSkin =
                new pText(SkinManager.Current.SkinName + (!string.IsNullOrEmpty(SkinManager.Current.SkinAuthor) ? @" " + string.Format(LocalisationManager.GetString(OsuString.SongSelection_BeatmapBy), SkinManager.Current.SkinAuthor) : string.Empty), 24, new Vector2(0, 30),
                          Vector2.Zero,
                          1, true, Color.White, true);
            currentSkin.TextBold = true;
            currentSkin.FadeInFromZero(1500);
            spriteManager.Add(currentSkin);
        }

        private void skinSamples_OnCheckChanged(object sender, bool status)
        {
            ConfigManager.sSkinSamples.Value = status;
            AudioEngine.LoadAllSamples();
            AudioEngine.LoadSampleSet(AudioEngine.CurrentSampleSet, false);
        }

        private void useTaikoSkin_OnCheckChanged(object sender, bool status)
        {
            ConfigManager.sUseTaikoSkin.Value = status;
        }

        private void nextPage(object sender, EventArgs e)
        {
            page++;
            Reload(false);
        }

        private void prevPage(object sender, EventArgs e)
        {
            page--;
            Reload(false);
        }

        private void pt_OnClick(object sender, EventArgs e)
        {
            ConfigManager.sSkin.Value = (string)((pSprite)sender).Tag;
            Reload(true);
        }

        private void newSong(object sender, EventArgs e)
        {
            if (BeatmapManager.Beatmaps.Count == 0)
                return;

            Beatmap next =
                BeatmapManager.Beatmaps[GameBase.random.Next(0, BeatmapManager.Beatmaps.Count - 1)];

            int chance = 0;
            if (BeatmapManager.Current != null)
            {
                while (next.SortTitle == BeatmapManager.Current.SortTitle || !next.AudioPresent)
                {
                    next =
                        BeatmapManager.Beatmaps[
                            GameBase.random.Next(0, BeatmapManager.Beatmaps.Count - 1)];
                    if (chance++ > 5)
                        break; //Give up
                }
            }

            BeatmapManager.Current = next;

            AudioEngine.LoadAudio(BeatmapManager.Current, false, false);

            Reload(true);

            AudioEngine.SeekTo(BeatmapManager.Current.PreviewTime);
            AudioEngine.Play();

            AudioEngine.Time = 0;
        }

        private void packageSkin(object sender, EventArgs e)
        {
            if (SkinManager.IsDefault)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.OptionsSkin_CannotExportDefault));
                return;
            }
            else if (SkinManager.Current == null) return;

            GameBase.PackageFile(SkinManager.Current.SkinName + @".osk", @"Skins/" + SkinManager.Current.RawName);
        }

        private static void back_OnClick(object sender, EventArgs e)
        {
            AudioEngine.PlaySamplePositional(@"menuback");
            GameBase.ChangeMode(OsuModes.Menu);
        }

        private bool GameBase_OnKeyPressed(object sender, Keys k)
        {
            switch (k)
            {
                case Keys.R:
                    Reload(true);
                    return true;
                case Keys.Q:
                case Keys.Escape:
                    back_OnClick(null, null);
                    return true;
            }

            return false;

        }

        protected override void Dispose(bool disposing)
        {
            KeyboardHandler.OnKeyPressed -= GameBase_OnKeyPressed;
            hitObjectManager.Dispose();
            spriteManager.Dispose();
            base.Dispose(disposing);
        }

        public override void Update()
        {
            //Handle the case where the audio has reached the end of the track.
            if (AudioEngine.AudioState == AudioStates.Stopped)
                newSong(null, null);

            //We can't rely on the audioengine to load samples because we aren't in play/edit mode..
            AudioEngine.LoadSampleSet(AudioEngine.CurrentSampleSet, false);

            //Handle the case where no beatmaps are present.
            if (BeatmapManager.Current == null)
                return;

            hitObjectManager.Update();

            foreach (HitObject o in hitObjectManager.hitObjectsMinimal)
            {
                if (!o.IsHit && o.EndTime - AudioEngine.Time < 16 &&
                    GameBase.random.NextDouble() > 0.57)
                    hitObjectManager.Hit(o);

                o.Update();

                SliderOsu s = o as SliderOsu;

                if (s != null)
                {
                    if (!s.StartIsHit && s.StartTime - AudioEngine.Time < 16 && GameBase.random.NextDouble() > 0.56)
                        s.HitStart();

                    if (s.IsScorable)
                    {
                        Player.IsSliding = true;
                        MouseManager.GameDownState = true;
                        o.GetScorePoints(GameBase.GamefieldToDisplay(s.PositionAtTime(AudioEngine.Time)));
                    }
                }
                else if (o is SpinnerOsu && o.IsScorable)
                {
                    ((SpinnerOsu)o).velocityCurrent = 0.1f;
                    o.GetScorePoints(Vector2.Zero);
                }
            }

            base.Update();
        }

        public override void Draw()
        {
            if (hitObjectManager.eventManager != null)
            {
                hitObjectManager.eventManager.DrawBG();
                hitObjectManager.eventManager.DrawFG();
                hitObjectManager.Draw();
            }

            spriteManager.Draw();
            base.Draw();
        }

        public override void Initialize()
        {
            base.Initialize();
            GameBase.LoadComplete();
        }
    }
}