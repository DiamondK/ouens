using Microsoft.Xna.Framework;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu_common.Helpers;

namespace osu.GameModes.Options
{
    internal class OptionCategory : OptionElement
    {
        internal FontAwesome Icon;
        internal string Title;

        public OptionCategory(OsuString titleString, FontAwesome icon = FontAwesome.circle)
        {
            Icon = icon;

            addKeyword(icon.ToString());
            addKeyword(titleString.ToString());

            Title = LocalisationManager.GetString(titleString);
            addKeyword(Title);

            headerText = new pText(Title.ToUpper(), 24, Vector2.Zero, 0, true, SkinManager.NEW_SKIN_COLOUR_SECONDARY)
            {
                Alpha = 0.4f,
                Origin = Origins.TopRight,
                Field = Fields.TopRight,
                TextShadow = false,
            };

            spriteManager.Add(headerText);

            Size = new Vector2(0, 10);

            addKeyword(Title);
        }

        internal override bool IsHeaderElement
        {
            get
            {
                return true;
            }
        }

        bool highlighted;
        private pText headerText;
        public bool Highlighted
        {
            get
            { return Highlighted; }
            set
            {
                if (value == highlighted) return;
                highlighted = value;
                if (highlighted)
                {
                    headerText.FadeIn(100);
                    headerText.FadeColour(ColourHelper.Lighten2(SkinManager.NEW_SKIN_COLOUR_SECONDARY, 0.4f), 100);
                }
                else
                {
                    headerText.FadeTo(0.4f, 100);
                    headerText.FadeColour(SkinManager.NEW_SKIN_COLOUR_SECONDARY, 100);
                }
            }
        }
    }
}