using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.UserInterface;
using osu_common.Helpers;

namespace osu.GameModes.Options
{
    internal class OptionButton : OptionElement
    {
        private pButton Button;

        public OptionButton(OsuString title, Color colour, EventHandler onClick = null)
            : this(LocalisationManager.GetString(title), colour, onClick)
        {
            addKeyword(title.ToString());
        }

        public OptionButton(string title, Color colour, EventHandler onClick = null)
        {
            Size = new Vector2(ELEMENT_SIZE, 20);
            Button = new pButton(title, new Vector2(0, 0), Size, 1, colour, onClick, false, false, 12);
            spriteManager.Add(Button.SpriteCollection);

            addKeyword(title);
        }

        public string Text
        {
            get
            {
                return Button.Text.Text;
            }
            set
            {
                Button.Text.Text = value;
            }
        }
    }
}