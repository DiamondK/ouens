using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play.Components;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Notifications;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Helpers;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu_common;
using osu_common.Bancho.Requests;
using osu.Properties;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using osu.GameModes.Select;
using System.Threading;
using System.Globalization;
using osu_common.Helpers;
using osu.Graphics;
using Microsoft.Win32;
using osu.GameModes.Menus;
using osu.GameModes.Play.Rulesets.Mania;
using osu.Graphics.Primitives;
using osu_common.Updater;
using osu.Online.Social;
using Un4seen.Bass;
using Size = System.Drawing.Size;

namespace osu.GameModes.Options
{
    internal partial class Options : DrawableGameComponent
    {
        pScrollableArea scrollableArea;
        pScrollableArea scrollableAreaTabs;

        List<OptionElement> options = new List<OptionElement>();
        List<OptionElement> visibleOptions = new List<OptionElement>();

        const int left_offsest = 15;

        SpriteManager spriteManager;
        SpriteManager spriteManagerAbove;
        private SpriteManager spriteManagerBackground;
        private SpriteManager spriteManagerTabs;
        private pSprite background;
        private pSprite tabsBackground;
        private OptionsToggleButton toggle;

        float expandProgress = 0;
        float alphaProgress = 0;
        private Rectangle visibleBounds;

        public const int padding = 10;

        const int column_width = OptionElement.ELEMENT_SIZE + padding;
        const int max_columns = 1;
        const int column_padding = 20;

        internal const int TAB_WIDTH = 40;
        internal const int TAB_WIDTH_EXPANDED = 160;

        const int header_offset = padding * 14;

        const float alpha_main_background = 0.7f;
        const float alpha_overlays = 0.7f;

        private bool initialised;

        internal event BoolDelegate OnLoginResult;

        public const int EXPANDED_WIDTH = (column_width + column_padding) * max_columns;

        bool expanded;
        internal bool Expanded
        {
            get { return expanded; }
            set
            {
                if (expanded == value) return;

                if (!initialised) Initialize();

                PoppedOut = expanded = value;

                if (!expanded)
                {
                    if (!BanchoClient.Authenticated && !GameBase.HasLogin && OnLoginResult != null)
                        OnLoginResult(false);
                    OnLoginResult = null;
                }

                if (expanded)
                {
                    scrollableArea.Rebind();
                    scrollableAreaTabs.Rebind();

                    GameBase.CheckForUpdates();
                    Layout();
                }
            }
        }

        private bool loginOnly;
        internal bool LoginOnly
        {
            get { return loginOnly; }
            set
            {
                if (loginOnly != value)
                {
                    loginOnly = value;
                    ReloadElements();
                }
                else
                    loginOnly = value;
            }
        }

        internal Options(Game game)
            : base(game)
        {
            spriteManagerBackground = new SpriteManager(true);
            spriteManagerTabs = new SpriteManager(true);
            spriteManagerAbove = new SpriteManager(true) { HandleOverlayInput = true };

            scrollableArea = new pScrollableArea(new Rectangle(TAB_WIDTH, 0, GameBase.WindowWidthScaled, GameBase.WindowHeightScaled), Vector2.Zero, true, 0, InputTargetType.Options)
            {
                ScrollDragger = false,
                ScrollDraggerPadding = false,
                HandleOverlayInput = true,
            };

            scrollableAreaTabs = new pScrollableArea(new Rectangle(0, 0, TAB_WIDTH, GameBase.WindowHeightScaled), Vector2.Zero, true, 0, InputTargetType.Options)
            {
                ScrollDraggerOnLeft = true,
                ScrollDraggerPadding = false,
                HandleOverlayInput = true,
            };

            spriteManager = scrollableArea.SpriteManager;
            spriteManager.FirstDraw = false;

            spriteManagerTabs = scrollableAreaTabs.SpriteManager;
            spriteManagerTabs.FirstDraw = false;

            spriteManagerBackground.HandleOverlayInput = true;

            //toggle = new OptionsToggleButton(this);
        }

        public override void Initialize()
        {
            if (initialised) return;

            searchBox = new pSearchBox(18, new Vector2(0, padding * 10), 20, TextAlignment.Centre);
            searchBox.OnChange += delegate(pTextBox sender, bool newtext)
            {
                if (newtext)
                {
                    Layout();
                    scrollableArea.ScrollToPosition(0, -0.99f);
                }
            };
            searchBox.MaintainFocus = false;

            headerBackground = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0.95F, true, new Color(8, 8, 8));
            headerBackground.Alpha = alpha_overlays;
            headerBackground.Scale = 1.6f;
            headerBackground.VectorScale = new Vector2(EXPANDED_WIDTH, padding * 13);
            headerBackground.ViewOffsetImmune = true;
            searchBox.SpriteCollection.Add(headerBackground);

            searchBox.SpriteCollection.ForEach(s =>
            {
                s.ViewOffsetImmune = true;
                s.ViewOffsetImmunityPoint = padding * 9;
            });

            spriteManager.Add(searchBox.SpriteCollection);

            BanchoClient.LoginResult += banchoLoginResult;
            SystemEvents.DisplaySettingsChanged += new EventHandler(DisplaySettingsChanged);
            ConfigManager.sScreenMode.ValueChanged += delegate { UpdateResolutions(dropdownResolution.Dropdown); };

            headerText = new pText(string.Empty, 20, new Vector2(0, padding * 4), 1, true, new Color(255, 255, 255, 255));
            headerText.TextShadow = true;
            headerText.ViewOffsetImmune = true;
            headerText.ViewOffsetImmunityPoint = padding * 9;
            headerText.Origin = Origins.TopCentre;
            headerText.Field = Fields.TopCentre;
            spriteManager.Add(headerText);

            headerText2 = new pText(string.Empty, 14, new Vector2(0, padding * 6), 1, true, SkinManager.NEW_SKIN_COLOUR_MAIN);
            headerText2.ViewOffsetImmune = true;
            headerText2.TextShadow = true;
            headerText2.ViewOffsetImmunityPoint = padding * 9;
            headerText2.Origin = Origins.TopCentre;
            headerText2.Field = Fields.TopCentre;
            spriteManager.Add(headerText2);

            InitializeOptions();

            //initialise state to not visible (popped out)
            background = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0.95F, true, new Color(8, 8, 8));
            background.Alpha = alpha_main_background;
            background.Scale = 1.6f;
            background.ClickRequiresConfirmation = true;
            background.VectorScale = new Vector2(EXPANDED_WIDTH + TAB_WIDTH, GameBase.WindowHeightScaled);
            background.ViewOffsetImmune = true;
            background.HandleInput = true;
            background.OnClick += delegate
            {
                OptionElement elm = shiftingBackground.Tag as OptionElement;
                if (elm == null) return;

                pDrawable d = elm.spriteManager.SpriteList.Find(s => s.HandleInput);

                if (d != null) d.Click();
            };
            spriteManagerBackground.Add(background);

            tabsBackground = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0.951F, true, new Color(8, 8, 8));
            tabsBackground.Alpha = 1;
            tabsBackground.Scale = 1.6f;
            tabsBackground.ViewOffsetImmune = true;
            tabsBackground.VectorScale = new Vector2(TAB_WIDTH_EXPANDED, GameBase.WindowHeightScaled);
            scrollableAreaTabs.SpriteManagerBelow.Add(tabsBackground);

            shiftingBackground = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0F, true, new Color(8, 8, 8));
            shiftingBackground.Alpha = alpha_overlays;
            shiftingBackground.Scale = 1.6f;
            shiftingBackground.VectorScale = new Vector2(EXPANDED_WIDTH, 0);

            scrollableArea.SpriteManagerBelow.Add(shiftingBackground);

            scrollableArea.Hide();

            scrollableArea.SetDisplayRectangle(RectangleF.Empty);
            spriteManagerBackground.SetVisibleArea(RectangleF.Empty);

            Layout();

            spriteManagerAbove.Add(new BackButton(delegate
            {
                AudioEngine.PlaySamplePositional(@"menuback");
                Expanded = false;
            }, false).SpriteCollection);

            scrollableArea.SetScrollPosition(new Vector2(0, 200));

            scrollableAreaTabs.SpriteManagerBelow.Alpha =
                spriteManagerAbove.Alpha =
                spriteManagerBackground.Alpha =
                spriteManagerTabs.Alpha =
                spriteManager.Alpha = 0;

            base.Initialize();

            initialised = true;
        }

        List<OptionTab> sidebarTabs = new List<OptionTab>();

        private void Layout()
        {
            GameBase.Scheduler.AddOnce(delegate
            {
                blurShiftingBackground();

                visibleBounds = new Rectangle(0, 0, EXPANDED_WIDTH, GameBase.WindowHeightScaled);

                visibleOptions.Clear();

                if (searchBox.Text.Length > 0)
                {
                    foreach (OptionElement o in options)
                    {
                        if (o.Match(searchBox.Text) && !visibleOptions.Contains(o))
                        {
                            //add all parents
                            OptionElement parent = o;
                            int loopCount = 0;
                            while ((parent = parent.Parent) != null && !visibleOptions.Contains(parent))
                                visibleOptions.Insert(visibleOptions.Count - (loopCount++), parent);

                            visibleOptions.Add(o);

                            if (o.IsHeaderElement)
                                visibleOptions.AddRange(o.AllChildren);
                        }
                        else
                            o.Hide();
                    }
                }
                else
                    visibleOptions.AddRange(options);

                if (visibleOptions.Count == 0 && searchBox.Text.Length > 0)
                {
                    searchBox.Shake();
                    searchBox.Text = searchBox.Text.Remove(searchBox.Text.Length - 1);
                    Layout();
                    return;
                }

                Vector2 currentPosition = new Vector2(0, header_offset);
                int column = 0;

                float[] columnY = new float[max_columns];
                for (int i = 0; i < max_columns; i++)
                    columnY[i] = header_offset;

                bool first = true;
                float overflowHeightRequirement = 0;

                for (int i = 0; i < visibleOptions.Count; i++)
                {
                    OptionElement e = visibleOptions[i];

                    e.Show();

                    bool isHeader = e.IsHeaderElement;

                    if (e.ExpandedOnly && !expanded)
                    {
                        e.spriteManager.HandleInput = false;
                        e.spriteManager.Blackness = 0.5f;
                    }
                    else
                    {
                        e.spriteManager.HandleInput = true;
                        e.spriteManager.Blackness = 0;
                    }

                    if (!first)
                    {
                        if (isHeader)
                        {
                            first = true;
                            column = 0;
                        }
                        else
                            column = (column + 1) % max_columns;
                    }

                    if (!isHeader)
                        first = false;

                    if (e.PaddingBefore > 0)
                        columnY[column] += e.PaddingBefore;

                    float paddingLeft = padding;
                    if (!isHeader) paddingLeft *= 2;

                    e.spriteManager.BaseOffset = new Vector2((column % max_columns) * (column_width + column_padding) + paddingLeft, columnY[column]);
                    e.VerticalPosition = columnY[column] - header_offset / 2;

                    if (e.Size.Y > 0)
                        columnY[column] += e.Size.Y + padding;

                    if (e.SizeOverflow > 0)
                        overflowHeightRequirement = Math.Max(overflowHeightRequirement, columnY[column] + e.SizeOverflow);

                    if (isHeader)
                        for (int j = 0; j < max_columns; j++)
                            columnY[j] = columnY[column];
                }

                visibleOptions.ForEach(o => o.Layout());

                float iconYPosition = GameBase.WindowHeightScaled / 2 - (sidebarTabs.Count * OptionTab.tab_height / 2);

                foreach (OptionTab s in sidebarTabs)
                {
                    if (!visibleOptions.Contains(s.Category))
                        s.Hide();
                    else
                        s.Show();

                    s.MoveTo(new Vector2(0, iconYPosition));
                    iconYPosition += OptionTab.tab_height;
                }

                scrollableArea.SetContentDimensions(new Vector2(visibleBounds.Width, Math.Max(overflowHeightRequirement, columnY[column] + (Expanded ? 100 : 0))));
            });
        }

        private void InitializeOptions()
        {
            headerText.Text = LocalisationManager.GetString(OsuString.Options_Options);
            headerText2.Text = LocalisationManager.GetString(OsuString.Options_HeaderBlurb);
            searchBox.DefaultText = LocalisationManager.GetString(OsuString.SongSelection_TypeToBegin);

            if (loginOnly)
            {
                Add(new OptionCategory(OsuString.Options_General, FontAwesome.gear)
                {
                    Children = new[] { getLoginSection() }
                });

                searchBox.Enabled = false;

                if (loginUserTB != null)
                {
                    if (loginUserTB.Textbox.Text == string.Empty)
                        loginUserTB.Focus();
                    else
                        loginPassTB.Focus();
                }

                return;
            }

            searchBox.Enabled = true;

            Add(new OptionCategory(OsuString.Options_General, FontAwesome.gear)
            {
                Children = new OptionElement[]
                {
                    getLoginSection(),
                    new OptionSection(OsuString.Options_Graphics_Language)
                    {
                        ExpandedOnly = true,
                        Children = new OptionElement[]
                        {
                            new OptionDropdown(OsuString.Options_Graphics_SelectLanguage, new []
                            {
                                new pDropdownItem(@"English", @"en"),
                                new pDropdownItem(@"中文(简体)", @"zh-CHS"),
                                new pDropdownItem(@"中文(繁體)", @"zh-CHT"),
                                new pDropdownItem(@"日本語", @"ja"),
                                new pDropdownItem(@"한국어", @"ko"),

                                new pDropdownItem(@"Dansk", @"da"),
                                new pDropdownItem(@"Deutsch", @"de"),
                                new pDropdownItem(@"Español", @"es"),
                                new pDropdownItem(@"Français", @"fr"),
                                new pDropdownItem(@"Български", @"bg"),
                                new pDropdownItem(@"Ελληνικά", @"el"),
                                new pDropdownItem(@"Suomi", @"fi"),
                                new pDropdownItem(@"עברית", @"he"),
                                new pDropdownItem(@"Hrvatski", @"hr"),
                                new pDropdownItem(@"Bahasa Indonesia", @"id"),
                                new pDropdownItem(@"Italiano", @"it"),
                                new pDropdownItem(@"Magyar", @"hu"),
                                new pDropdownItem(@"Mongɣol kele", @"mn"),
                                new pDropdownItem(@"Bahasa Malaysia", @"ms-MY"),
                                new pDropdownItem(@"Nederlands", @"nl"),
                                new pDropdownItem(@"Norsk", @"no"),
                                new pDropdownItem(@"latviešu valoda", @"lv"),
                                new pDropdownItem(@"Polski", @"pl"),
                                new pDropdownItem(@"Português (Brasil)", @"br"),
                                new pDropdownItem(@"Português (Portugal)", @"pt"),
                                new pDropdownItem(@"Русский", @"ru"),
                                new pDropdownItem(@"Slovenčina", @"sk"),
                                new pDropdownItem(@"Slovenščina", @"sl"),
                                new pDropdownItem(@"Svenska", @"sv"),
                                new pDropdownItem(@"ภาษาไทย", @"th"),
                                new pDropdownItem(@"Türkçe", @"tr"),
                                new pDropdownItem(@"Tiếng Việt", @"vi-VN"),
                                new pDropdownItem(@"Українська", @"uk-UA")
                            }, ConfigManager.sLanguage),
                            new OptionCheckbox(OsuString.Option_Graphics_ShowUnicode, ConfigManager.sShowUnicode),
                            new OptionCheckbox(OsuString.Options_Language_Font_Override, ConfigManager.sAlternativeChatFont)
                        }
                    },
                    new OptionSection(OsuString.Options_Updates)
                    {
                        Children = new OptionElement[]
                        {
                            new OptionDropdown(OsuString.Options_ReleaseStream, DropdownItemsReleaseStream, ConfigManager.sReleaseStream),
                            (updateText = new OptionText(CommonUpdater.GetStatusString(), delegate { GameBase.CheckForUpdates(false, true); })),
                            new OptionButton(OsuString.Options_OpenOsuFolder, SkinManager.NEW_SKIN_COLOUR_SECONDARY, openOsuFolder),
                        },
                    }
                }
            });

            Add(new OptionCategory(OsuString.Options_TabGraphics, FontAwesome.desktop)
            {
                Children = new[] {
                    new OptionSection(OsuString.Options_Graphics_Renderer) {
                        Children = new OptionElement[] {
                            new OptionCheckbox(@"OpenGL", LocalisationManager.GetString(OsuString.Options_Graphics_OpenGl_Tooltip), ConfigManager.OpenGL) { ExpandedOnly = true},
                            new OptionCheckbox(@"DirectX", LocalisationManager.GetString(OsuString.Options_Graphics_DirectX_Tooltip), ConfigManager.DirectX) { ExpandedOnly = true},
                            new OptionDropdown(OsuString.Options_Graphics_FrameLimiter, DropdownItemsFrameLimiter, ConfigManager.sFrameSync)  { ExpandedOnly = true},
                            new OptionCheckbox(OsuString.Options_Graphics_FpsCounter, ConfigManager.sFpsCounter),
                        }
                    },
                    new OptionSection(OsuString.Options_Graphics_ScreenResolution) {
                        ExpandedOnly = true,
                        Children = new OptionElement[] {
                            dropdownResolution = new OptionDropdown(OsuString.Options_Graphics_SelectResolution, null, null, resolutionChange),
                            fullscreenCheckbox = new OptionCheckbox(OsuString.Options_Graphics_Fullscreen, ConfigManager.sFullscreen) { Enabled = !GameBase.OGL }
                        }
                    },
                    new OptionSection(OsuString.Options_Graphics_Detail) {
                        Children = new OptionElement[] {
                            new OptionCheckbox(OsuString.Options_Graphics_Snaking, ConfigManager.sSnakingSliders),
                            new OptionCheckbox(OsuString.Options_Graphics_Video, ConfigManager.sVideo),
                            new OptionCheckbox(OsuString.Options_Graphics_Storyboards, ConfigManager.sShowStoryboard),
                            new OptionCheckbox(OsuString.Options_Graphics_Combo, ConfigManager.sComboBurst),
                            new OptionCheckbox(OsuString.Options_Graphics_HitLighting, ConfigManager.sHitLighting),
                            new OptionCheckbox(OsuString.Options_Graphics_Shader, ConfigManager.sBloom),
                            new OptionCheckbox(OsuString.Options_Graphics_Softening, ConfigManager.sBloomSoftening),
                            new OptionDropdown(OsuString.Options_Graphics_Screenshot, new pDropdownItem[] {
                                new pDropdownItem(LocalisationManager.GetString(OsuString.Options_Graphics_Screenshot1), ImageFileFormat.Png),
                                new pDropdownItem(LocalisationManager.GetString(OsuString.Options_Graphics_Screenshot2), ImageFileFormat.Jpg),
                            }, ConfigManager.sScreenshotFormat),
                        }
                    },
                    new OptionSection(OsuString.Options_Graphics_Menu) {
                        Children = new OptionElement[] {
                            
#if CHRISTMAS
                            new OptionCheckbox(OsuString.Options_Menu_Snow, ConfigManager.sMenuSnow) { Enabled = false },
#else
                            new OptionCheckbox(OsuString.Options_Menu_Snow, ConfigManager.sMenuSnow),
#endif

#if TRIANGLES
                            new OptionCheckbox(OsuString.Options_Menu_Triangles, ConfigManager.sMenuTriangles),
#endif

                            new OptionCheckbox(OsuString.Options_Menu_Parallax, ConfigManager.sMenuParallax),
                            new OptionCheckbox(OsuString.Options_Menu_ShowTips, ConfigManager.sShowMenuTips),
                            new OptionCheckbox(OsuString.Options_Menu_Voice, ConfigManager.sMenuVoice),
#if CHRISTMAS
                            new OptionCheckbox(OsuString.Options_Menu_Music, ConfigManager.sMenuMusic) { Enabled = false },
#else
                            new OptionCheckbox(OsuString.Options_Menu_Music, ConfigManager.sMenuMusic),
#endif
                        }
                    },
                    new OptionSection(OsuString.Options_SongSelect) {
                        Children = new OptionElement[] {
                            new OptionCheckbox(OsuString.Options_SongSelect_Thumbnails, ConfigManager.sSongSelectThumbnails),
                        }
                    }
                }
            });

            UpdateResolutions(dropdownResolution.Dropdown);

            Add(new OptionCategory(OsuString.Options_TabGameplay, FontAwesome.circle_o)
            {
                Children = new[] {
                    new OptionSection(OsuString.Options_General) {
                        Children = new OptionElement[] {
                            new OptionSlider(OsuString.FunSpoiler_BackgroundDim, ConfigManager.sDimLevel, @"%"),
                            new OptionDropdown(OsuString.Options_Graphics_Progress, new pDropdownItem[] {
                                new pDropdownItem(LocalisationManager.GetString(OsuString.Options_Graphics_Progress1), ProgressBarTypes.Pie),
                                new pDropdownItem(LocalisationManager.GetString(OsuString.Options_Graphics_Progress2), ProgressBarTypes.TopRight),
                                new pDropdownItem(LocalisationManager.GetString(OsuString.Options_Graphics_Progress3), ProgressBarTypes.BottomRight),
                                new pDropdownItem(LocalisationManager.GetString(OsuString.Options_Graphics_Progress4), ProgressBarTypes.Bottom)
                            }, ConfigManager.sProgressBarType),
                            new OptionDropdown(OsuString.Options_TabSkin_ScoreMeter, new pDropdownItem[] {
                                new pDropdownItem(LocalisationManager.GetString(OsuString.Options_Graphics_Meter0), ScoreMeterType.None),
                                new pDropdownItem(LocalisationManager.GetString(OsuString.General_Colour), ScoreMeterType.Colour),
                                new pDropdownItem(LocalisationManager.GetString(OsuString.Options_Graphics_Meter2), ScoreMeterType.Error),
                            }, ConfigManager.sScoreMeter),
                            new OptionSlider(OsuString.Options_ScoreMeterScale, ConfigManager.sScoreMeterScale, @"x"),
                            new OptionCheckbox(OsuString.Options_KeyOverlay, ConfigManager.sKeyOverlay),
                            new OptionCheckbox(OsuString.Options_Gameplay_Hidden_Approach, ConfigManager.sHiddenShowFirstApproach),
                            new OptionCheckbox(OsuString.Options_ScaleManiaSpeedWithBPM, ConfigManager.sManiaSpeedBPMScale),
                            new OptionCheckbox(OsuString.Options_UsePerBeatmapManiaSpeed, ConfigManager.sUsePerBeatmapManiaSpeed),
                        }
                    }
                }
            });

            Add(new OptionCategory(OsuString.Options_TabAudio, FontAwesome.volume_off)
            {
                Children = new[] {
                    new OptionSection(OsuString.Options_Audio_Device) {
                        Children = new OptionElement[] {
                            dropdownAudioDevices = new OptionDropdown(OsuString.Options_Audio_AudioDevice, null, ConfigManager.sAudioDevice, null),
                        }
                    },
                    new OptionSection(OsuString.Options_Audio_Volume) {
                        Children = new OptionElement[] {
                            new OptionSlider(OsuString.Options_Audio_Master, AudioEngine.VolumeMaster, @"%"),
                            new OptionSlider(OsuString.Options_Audio_Music, AudioEngine.VolumeMusic, @"%"),
                            new OptionSlider(OsuString.Options_Audio_Effect, AudioEngine.VolumeEffect, @"%"),
                            new OptionCheckbox(OsuString.FunSpoiler_Hitsounds, ConfigManager.sIgnoreBeatmapSamples)
                        }
                    },
                    new OptionSection(OsuString.Options_Audio_Offset) {
                        ExpandedOnly = true,
                        Children = new OptionElement[] {
                            new OptionSlider(OsuString.OptionsOffsetWizard_UniversalOffset, ConfigManager.sOffset, @"ms"),
                            new OptionButton(OsuString.Options_Audio_OffsetWizard, SkinManager.NEW_SKIN_COLOUR_SECONDARY, delegate {
                                if (BeatmapManager.Current == null || BeatmapManager.Current.ControlPoints == null || BeatmapManager.Current.ControlPoints.Count == 0)
                                {
                                    NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.Options_OffsetWizard_NotTimed), 3000);
                                    return;
                                }

                                GameBase.ChangeMode(OsuModes.OptionsOffsetWizard);
                            })
                        }
                    }
                }
            });

            UpdateAudioDevices();
            AudioEngine.AvailableDevicesChanged += delegate { UpdateAudioDevices(); };

            List<pDropdownItem> skins = new List<pDropdownItem>();
            foreach (string s in SkinManager.Skins)
                skins.Add(new pDropdownItem(s, s));

            Add(new OptionCategory(OsuString.Options_TabSkin, FontAwesome.fa_paint_brush)
            {
                Children = new[] {
                  new OptionSection(OsuString.Options_Skin) {
                        Children = new OptionElement[] {
                            new OptionSkinPreview(),
                            new OptionDropdown(OsuString.Options_SkinSelect, skins, ConfigManager.sSkin, delegate {
                                GameBase.OnNextModeChange += delegate { SkinManager.LoadSkin(null, true); };
                                if (GameBase.ModeCanReload)
                                    GameBase.ChangeMode(GameBase.Mode, true);
                                else
                                {
                                    //notify the user
                                }
                            }),
                            new OptionButton(OsuString.Options_OpenSkinFolder, SkinManager.NEW_SKIN_COLOUR_SECONDARY, delegate {
                                SkinManager.CreateUserSkin();
                                GameBase.OpenFolder(@"Skins\" + SkinManager.Current.RawName);
                            }) { ExpandedOnly = true },
                            new OptionButton(OsuString.OptionsSkin_ExportAsOsk, SkinManager.NEW_SKIN_COLOUR_SECONDARY, delegate {
                                if (SkinManager.Current == null) return;

                                if (SkinManager.IsDefault)
                                {
                                    NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.OptionsSkin_CannotExportDefault));
                                    return;
                                }

                                GameBase.PackageFile(SkinManager.Current.SkinName + @".osk", @"Skins/" + SkinManager.Current.RawName);
                            }) { ExpandedOnly = true },
                            new OptionCheckbox(OsuString.Options_TabSkin_IgnoreBeatmapSkins, ConfigManager.sIgnoreBeatmapSkins),
                            new OptionCheckbox(OsuString.Options_UseSkinSamples, ConfigManager.sSkinSamples),
                            new OptionCheckbox(OsuString.Options_UseTaikoSkin, ConfigManager.sUseTaikoSkin),
                            new OptionCheckbox(OsuString.Options_UseSkinCursor, ConfigManager.sUseSkinCursor),
                            new OptionSlider(OsuString.Options_CursorSize, ConfigManager.sCursorSize, @"x"),
                            new OptionCheckbox(OsuString.Options_AutomaticCursorSizing, ConfigManager.sAutomaticCursorSizing),
                            new OptionCheckbox(OsuString.Options_ComboColourSliderBall, ConfigManager.sComboColourSliderBall),
                        }
                    }
                }
            });

            Add(new OptionCategory(OsuString.Options_TabInput, FontAwesome.gamepad)
            {
                Children = new[] {
                    new OptionSection(OsuString.Options_Input_Mouse) {
                        Children = new OptionElement[] {
                            new OptionSlider(OsuString.Options_Input_MouseSensitivity, ConfigManager.sMouseSpeed, @"x", false),
                            (rawInput = new OptionCheckbox(OsuString.Options_Input_RawInput, ConfigManager.sRawInput)),
                            new OptionCheckbox(OsuString.Options_Input_AbsoluteToOsuWindow, ConfigManager.sAbsoluteToOsuWindow),
                            new OptionCheckbox(OsuString.Options_Input_DisableWheel, ConfigManager.sMouseDisableWheel),
                            new OptionCheckbox(OsuString.Options_Input_DisableButtons, ConfigManager.sMouseDisableButtons),
                            new OptionCheckbox(OsuString.Options_Input_CursorRipples, ConfigManager.sCursorRipple),
                        }
                    },
                    new OptionSection(OsuString.Options_Input_Keyboard) {
                        Children = new OptionElement[] {
                            new OptionButton(OsuString.Options_Input_KeyBindings, SkinManager.NEW_SKIN_COLOUR_SECONDARY, delegate {
                                OptionsBindingDialog d = new OptionsBindingDialog();
                                d.Closed += delegate { searchBox.Enabled = true; };
                                searchBox.Enabled = false;
                                GameBase.ShowDialog(d);
                            }),
                            new OptionButton(OsuString.Options_OsuManiaLayout, SkinManager.NEW_SKIN_COLOUR_SECONDARY, delegate {
                                OptionsManiaBindingWizard d = new OptionsManiaBindingWizard();
                                d.Closed += delegate { searchBox.Enabled = true; };
                                searchBox.Enabled = false;
                                GameBase.ShowDialog(d);
                            })
                        }
                    },
                    new OptionSection(OsuString.Options_Input_Other) {
                        Children = new OptionElement[] {
                            new OptionCheckbox(OsuString.Options_Input_Tablet, ConfigManager.sTablet),
                            new OptionCheckbox(OsuString.Options_Input_Joystick, ConfigManager.sJoystick),
                            new OptionCheckbox(OsuString.Options_Input_Wii, ConfigManager.sWiimote)
                        }
                    }
                }
            });

            Add(new OptionCategory(OsuString.Options_Editor, FontAwesome.pencil)
            {
                Children = new[]
                {
                    new OptionSection(OsuString.Options_General)
                    {
                        Children = new OptionElement[]
                        {
                            new OptionCheckbox(OsuString.Options_Editor_Video, ConfigManager.sEditorVideo),
                            new OptionCheckbox(OsuString.Options_Editor_DefaultSkin, ConfigManager.sEditorDefaultSkin),
                            new OptionCheckbox(OsuString.Options_Editor_NoUndo, ConfigManager.sFastEditor),
                            new OptionCheckbox(OsuString.Options_Editor_Snaking_Sliders, ConfigManager.sEditorSnakingSliders),
                            new OptionCheckbox(OsuString.Options_Editor_Hit_Animations, ConfigManager.sEditorHitAnimations),
                            new OptionCheckbox(OsuString.Options_Editor_Follow_Points, ConfigManager.sEditorFollowPoints),
                        }
                    }
                }
            });

            Add(new OptionCategory(OsuString.Options_TabOnline, FontAwesome.globe)
            {
                Children = new[] {
                    new OptionSection(OsuString.Options_Online_AlertsPrivacy) {
                        Children = new OptionElement[] {
                            new OptionCheckbox(OsuString.Options_Online_ShowChatTicker, ConfigManager.sTicker),
                            new OptionCheckbox(OsuString.Options_Online_HideChat, ConfigManager.sAutoChatHide),
                            new OptionCheckbox(OsuString.Options_Online_ChatHighlight, ConfigManager.sChatHighlightName),
                            new OptionCheckbox(OsuString.Options_Online_ChatAudibleHighlight, ConfigManager.sChatAudibleHighlight),
                            new OptionCheckbox(OsuString.Options_Online_DisplayCityLocation, ConfigManager.sDisplayCityLocation),
                            new OptionCheckbox(OsuString.Options_Online_ShowSpectators, ConfigManager.sShowSpectators),
                            new OptionCheckbox(OsuString.Options_Online_AutoNP, ConfigManager.sAutoSendNowPlaying),
                            new OptionCheckbox(OsuString.Options_Online_PopupDuringGameplay, ConfigManager.sPopupDuringGameplay),
                            new OptionCheckbox(OsuString.Options_Online_NotifyFriends, ConfigManager.sNotifyFriends),
                            new OptionCheckbox(OsuString.Options_Online_AllowPublicInvites, ConfigManager.sAllowPublicInvites)
                        }
                    },
                    new OptionSection(OsuString.Options_Online_Integration) {
                        Children = new [] {
                            new OptionCheckbox(OsuString.Options_Online_YahooIntegration, ConfigManager.sYahooIntegration),
                            new OptionCheckbox(OsuString.Options_Online_MsnIntegration, ConfigManager.sMsnIntegration),
                            new OptionCheckbox(OsuString.Options_Online_AutomaticDownload, ConfigManager.sAutomaticDownload)
                        }
                    },
                    new OptionSection(OsuString.Options_Online_InGameChat) {
                        Children = new OptionElement[] {
                            new OptionCheckbox(OsuString.Options_Online_ChatFilter, ConfigManager.sChatFilter),
                            new OptionCheckbox(OsuString.Options_Online_ChatRemoveForeign, ConfigManager.sChatRemoveForeign),
                            new OptionCheckbox(OsuString.Options_Online_LogPrivateMessages, ConfigManager.sLogPrivateMessages),
                            new OptionCheckbox(OsuString.Options_Online_BlockNonFriendPM, ConfigManager.sBlockNonFriendPM),
                            new OptionTextbox(OsuString.Options_Online_ChatIgnoreList, ConfigManager.sIgnoreList),
                            new OptionTextbox(OsuString.Options_Online_ChatHighlights, ConfigManager.sHighlightWords),
                        }
                    }
                }
            });

            Add(new OptionCategory(OsuString.Options_TabMaintenance, FontAwesome.wrench)
            {
                ExpandedOnly = true,
                Children = new[] {
                    new OptionSection(OsuString.Options_General) {
                        Children = new OptionElement[] {
                            new OptionButton(OsuString.Options_DeleteAllUnrankedMaps, SkinManager.NEW_SKIN_COLOUR_SECONDARY, maintenanceDeleteAllUnranked),
                            new OptionButton(OsuString.Options_ForceFolderPermissions, SkinManager.NEW_SKIN_COLOUR_SECONDARY, maintenanceForceFolderPermissions),
                            new OptionButton(OsuString.Options_MarkMapsPlayed, SkinManager.NEW_SKIN_COLOUR_SECONDARY, maintenanceMarkMapsPlayed),
                            new OptionButton(OsuString.Options_RunUpdater, SkinManager.NEW_SKIN_COLOUR_SECONDARY, runUpdater),
                        }
                    }
                },
            });

            Add(new OptionVersion(General.BUILD_NAME));

            //we want events to be handled in reverse creation order for this special case.
            for (int i = scrollableArea.Children.Count - 1; i >= 0; i--)
                scrollableArea.Children[i].RefreshCreationOrder();

            scrollableArea.SpriteManager.RefreshCreationOrder();
            scrollableAreaTabs.SpriteManager.RefreshCreationOrder();
        }

        private void UpdateAudioDevices()
        {
            dropdownAudioDevices.Dropdown.RemoveAllOptions();
            dropdownAudioDevices.Dropdown.AddOption(LocalisationManager.GetString(OsuString.General_Default), string.Empty);

            foreach (BASS_DEVICEINFO device in AudioEngine.AudioDevices)
            {
                if (!device.IsEnabled || device.driver == null)
                    continue;

                dropdownAudioDevices.Dropdown.AddOption(device.name, device.name);
            }

            if (!dropdownAudioDevices.Dropdown.SetSelected(ConfigManager.sAudioDevice.Value, true))
                dropdownAudioDevices.Dropdown.SetSelected(string.Empty, true);
        }

        private void openOsuFolder(object sender, EventArgs e)
        {
            GameBase.OpenFolder(Environment.CurrentDirectory);
        }

        private void runUpdater(object sender, EventArgs e)
        {
            GameBase.ChangeMode(OsuModes.Update);
        }

        private void maintenanceMarkMapsPlayed(object sender, EventArgs e)
        {
            BeatmapManager.Beatmaps.ForEach(bm => bm.NewFile = false);
            NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Options_MarkMapsPlayed_Success), Color.YellowGreen, 4000);
        }

        private void maintenanceForceFolderPermissions(object sender, EventArgs e)
        {
            GameBase.SetPermissions(false);
            NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Options_ForceFolderPermissions_Successful), Color.YellowGreen, 4000);
        }

        private void maintenanceDeleteAllUnranked(object sender, EventArgs e)
        {
            List<Beatmap> deleteList = BeatmapManager.Beatmaps.FindAll(
    bm => (bm.SubmissionStatus == SubmissionStatus.Pending || bm.SubmissionStatus == SubmissionStatus.NotSubmitted) &&
        bm.Creator != ConfigManager.sUsername &&
        BeatmapManager.Beatmaps.FindAll(bm2 => bm2.ContainingFolder == bm.ContainingFolder && (bm2.SubmissionStatus == SubmissionStatus.Ranked || bm2.Creator == ConfigManager.sUsername)).Count == 0);
            GameBase.MenuActive = true;

            if (MessageBox.Show(string.Format(LocalisationManager.GetString(OsuString.Options_DeleteWarning), deleteList.Count), @"osu!", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                deleteList.ForEach(dl => BeatmapManager.DeleteBeatmap(dl, true));
                BeatmapImport.SignalBeatmapCheck();
            }

            GameBase.MenuActive = false;

            BeatmapManager.Current = null;
        }

        private void resolutionChange(object sender, EventArgs e)
        {
            string s = sender as string;

            if (s == null || s == LocalisationManager.GetString(OsuString.Options_Graphics_CustomResolution))
            {
                UpdateResolutions(dropdownResolution.Dropdown);
                return;
            }

            string[] split = s.Split('x');

            int width = Int32.Parse(split[0]);
            int height = Int32.Parse(split[1]);

            GameBase.SetScreenSize(width, height, null, true);
        }

        void DisplaySettingsChanged(object sender, EventArgs e)
        {
            GameBase.Scheduler.Add(delegate
            {
                //If the desktop's resolution is changed outside of the game through windows (i.e: not by fullscreen changing it by force)
                if (resolutionDesktop != ResolutionHelper.GetDesktopResolution() && GameBase.CurrentScreenMode != ScreenMode.Fullscreen)
                {
                    //Set variable desktop to get up to date resolution for updating resolution list in options
                    resolutionDesktop = ResolutionHelper.GetDesktopResolution();
                    //Because the monitor resolution has changed through windows, we need to update the initial monitor resolution to reflect that so borderless windowed works properly.
                    GameBase.InitialDesktopResolution = ResolutionHelper.GetDesktopResolution();
                    //Finally, update resolution list in options
                    UpdateResolutions(dropdownResolution.Dropdown);
                }
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (toggle != null) toggle.Dispose();
            scrollableArea.Dispose();
            scrollableAreaTabs.Dispose();
            spriteManagerBackground.Dispose();
            spriteManagerTabs.Dispose();
            spriteManagerAbove.Dispose();

            if (searchBox != null) searchBox.Dispose();

            options.ForEach(o => o.Dispose());

            BanchoClient.LoginResult -= banchoLoginResult;

            base.Dispose(disposing);
        }

        internal bool KeyboardHandler_OnKeyRepeat(object sender, Keys k, bool first)
        {
            if (first)
            {
                switch (k)
                {
                    case Keys.Escape:
                        if (!searchBox.IsDefault)
                            searchBox.SetToDefault();
                        else if (Expanded)
                            Expanded = false;
                        else if (PoppedOut)
                            PoppedOut = false;
                        return true;
                    case Keys.Tab:

                        if (loginUserTB == null || loginPassTB == null)
                            return true;

                        if (loginUserTB.Textbox.InputControl.Focused)
                        {
                            loginUserTB.UnFocus();
                            loginPassTB.Focus();
                        }
                        else
                        {
                            loginPassTB.UnFocus();
                            loginUserTB.Focus();
                        }
                        return true;
                }
            }

            return false;
        }

        private void Add(OptionElement option)
        {
            options.Add(option);

            if (option.Children != null)
            {
                foreach (OptionElement c in option.Children)
                {
                    c.HeightChanged += c_HeightChanged;
                    Add(c);
                }
            }

            OptionCategory oc = option as OptionCategory;
            if (oc != null)
            {
                OptionTab icon = new OptionTab(oc, icon_OnClick);
                sidebarTabs.Add(icon);
                spriteManagerTabs.Add(icon.Sprites);
            }

            scrollableArea.AddChild(option.spriteManager);
        }

        void icon_OnClick(object sender, EventArgs e)
        {
            pSprite s = sender as pSprite;
            if (s == null) return;

            OptionElement ele = s.Tag as OptionElement;
            if (ele == null) return;

            AudioEngine.Click(null, @"select-expand");

            scrollableArea.ScrollToPosition(OsuMathHelper.Clamp(ele.VerticalPosition - GameBase.WindowHeightScaled / 10, 0, Math.Max(0, scrollableArea.ContentDimensions.Y - scrollableArea.ActualDisplayHeight)), -0.99f);
        }

        void c_HeightChanged(object sender, EventArgs e)
        {
            Layout();
        }

        bool firstDisplay = true;

        bool poppedOut;
        public bool PoppedOut
        {
            get { return poppedOut; }

            set
            {
                if (value == poppedOut) return;

                if (!initialised) Initialize();

                if (poppedOut = value)
                {
                    //ensure expanded chat isn't visible (doesn't work well for now)
                    if (ChatEngine.IsVisible) ChatEngine.Toggle();
                    scrollableArea.Show();
                    Layout();

                    if (firstDisplay)
                        scrollableArea.ScrollToPosition(-100, 0.5f);

                    firstDisplay = false;
                    //toggle.Selected = true;
                }
                else
                {
                    Expanded = false;
                    //toggle.Selected = false;
                }

                searchBox.MaintainFocus = value;
            }
        }

        public override void Draw()
        {
            if (!initialised) return;

            if (toggle != null) toggle.Draw();

            spriteManagerBackground.Draw();

            scrollableArea.Draw();

            scrollableAreaTabs.Draw();

            spriteManagerAbove.Draw();

            base.Draw();
        }
        public override void Update()
        {
            if (!initialised) return;

            if (toggle != null) toggle.Update();

            headerBackground.Alpha = Math.Min(0.5f, scrollableArea.ScrollPosition.Y / headerBackground.ViewOffsetImmunityPoint);

            float target = PoppedOut ? (visibleBounds.Width + 1) : 0;

            if (GameBase.FrameRatio < 4 && (expandProgress > 0 || expandProgress != target))
            {
                float alphaTarget = PoppedOut ? 1 : 0;

                if (expandProgress != target)
                {
                    expandProgress -= (expandProgress - target) * 0.1f * (float)GameBase.FrameRatio;
                    alphaProgress -= (alphaProgress - alphaTarget) * 0.1f * (float)GameBase.FrameRatio;

                    if (expandProgress < 1)
                        expandProgress = alphaProgress = 0;

                    scrollableArea.Alpha = alphaProgress;
                    spriteManagerAbove.Alpha = alphaProgress;
                    scrollableAreaTabs.Alpha = alphaProgress;

                    spriteManagerBackground.Alpha = Math.Min(1, alphaProgress * 4);
                    spriteManagerTabs.Alpha = Math.Min(1, loginOnly ? 0 : alphaProgress * 4);
                }

                RectangleF rect = new RectangleF(TAB_WIDTH, 0, (int)expandProgress, GameBase.WindowHeightScaled);
                scrollableArea.SetDisplayRectangle(rect);

                RectangleF rect2 = new RectangleF(0, 0, (int)expandProgress + TAB_WIDTH, GameBase.WindowHeightScaled);
                spriteManagerBackground.SetVisibleArea(rect2);

                if (GameBase.IdleTime > 30000)
                    PoppedOut = false;

                if (GameBase.SixtyFramesPerSecondFrame)
                {
                    if (ConfigManager.sRawInput)
                        rawInput.UpdateLabel(string.Format(LocalisationManager.GetString(OsuString.Options_Input_RawInputPoll),
                            MouseManager.MousePollRateInterpolated > 0.5f ? Math.Ceiling(MouseManager.MousePollRateInterpolated) : 0,
                            MouseManager.MousePollLatency > 0 ? MouseManager.MousePollLatency + @"ms" : @"-"));
                    else
                        rawInput.UpdateLabel(LocalisationManager.GetString(OsuString.Options_Input_RawInput));

                    updateText.UpdateText(CommonUpdater.GetStatusString(true));
                }

                bool found = false;
                foreach (OptionElement o in options)
                {
                    if (o.spriteManager.Bypass)
                        continue;

                    RectangleF r = new RectangleF(expandProgressTabs * GameBase.WindowRatio, (o.spriteManager.BaseOffset.Y - o.spriteManager.ScrollOffset.Y - padding / 2) * GameBase.WindowRatio, (EXPANDED_WIDTH - expandProgressTabs) * GameBase.WindowRatio, (o.Size.Y + padding) * GameBase.WindowRatio + GameBase.WindowOffsetY);
                    if (r.Contains(MouseManager.MousePoint))
                    {
                        if (o is OptionCategory || o is OptionSection)
                            break;

                        found = true;
                        background.ToolTip = o.Tooltip;

                        if (shiftingBackground.Tag == o)
                            break;

                        const int duration = 140;

                        AudioEngine.Click(null, @"click-short");

                        shiftingBackground.Tag = o;
                        shiftingBackground.Transformations.Clear();
                        shiftingBackground.FadeIn(100);
                        shiftingBackground.MoveTo(new Vector2(0, o.spriteManager.BaseOffset.Y - padding / 2), duration, EasingTypes.OutBack);
                        shiftingBackground.Transformations.Add(new Transformation(TransformationType.VectorScale, shiftingBackground.VectorScale, new Vector2(EXPANDED_WIDTH, o.Size.Y + padding), GameBase.Time, GameBase.Time + duration, EasingTypes.OutBack));
                        break;
                    }
                }

                if (!found) blurShiftingBackground();
            }

            scrollableArea.Update();

            UpdateTabs();

            base.Update();
        }

        private const float EXPAND_TABS_DELAY = 400;

        float expandProgressTabs = TAB_WIDTH;
        float expandDelayTabs = 0;
        bool tabsReachedTarget;
        OptionCategory activeCategory;
        private void UpdateTabs()
        {
            float target = expandProgressTabs;
            float newTarget = MouseManager.MousePositionWindow.X < TAB_WIDTH && scrollableAreaTabs.SpriteManager.CurrentHoverSprite != null ? TAB_WIDTH_EXPANDED : TAB_WIDTH;
            if (expandDelayTabs > 0)
            {
                if (newTarget != target && (newTarget == TAB_WIDTH || (!scrollableArea.IsScrolling && GameBase.IdleTime > 200)))
                    expandDelayTabs -= (float)GameBase.ElapsedMilliseconds;
            }
            else
                target = newTarget;

            if (expandProgressTabs != target)
            {
                float distance = expandProgressTabs - target;
                expandProgressTabs = target + distance * (float)Math.Pow(0.9, GameBase.FrameRatio);

                if (expandProgressTabs < TAB_WIDTH)
                    expandProgressTabs = TAB_WIDTH;

                scrollableAreaTabs.SetDisplayRectangle(new RectangleF(0, 0, expandProgressTabs, GameBase.WindowHeightScaled));
            }

            if (Math.Abs(expandProgressTabs - newTarget) < 1f)
                expandDelayTabs = EXPAND_TABS_DELAY;

            OptionCategory newCategory = null;
            foreach (OptionElement e in visibleOptions)
            {
                OptionCategory cat = e as OptionCategory;
                if (cat != null)
                    newCategory = cat;

                if (e.VerticalPosition - GameBase.WindowHeightScaled / 4 > scrollableArea.ScrollPosition.Y)
                    break;
            }

            if (newCategory != activeCategory)
            {
                if (activeCategory != null) activeCategory.Highlighted = false;
                activeCategory = newCategory;
                if (activeCategory != null) activeCategory.Highlighted = true;
            }

            foreach (OptionTab t in sidebarTabs)
                t.Active = t.Category == newCategory;

            scrollableAreaTabs.Update();
        }

        private void blurShiftingBackground()
        {
            if (shiftingBackground != null && shiftingBackground.Tag != null)
            {
                shiftingBackground.FadeOut(500);
                shiftingBackground.Tag = null;

                background.ToolTip = null;
            }
        }

        internal void Toggle()
        {
            if (PoppedOut)
            {
                PoppedOut = false;
                return;
            }

            if (GameBase.Mode == OsuModes.Menu)
                Expanded = true;
            else
                PoppedOut = true;
        }

        private OptionDropdown dropdownResolution;
        private OptionCheckbox fullscreenCheckbox;
        private pSearchBox searchBox;
        private pSprite headerBackground;
        private pText headerText;
        private pText headerText2;
        private OptionCheckbox rawInput;
        private OptionTextbox loginUserTB;
        private OptionTextbox loginPassTB;
        private pSprite shiftingBackground;
        private static Size resolutionDesktop = ResolutionHelper.GetDesktopResolution();
        private OptionText updateText;
        private OptionDropdown dropdownAudioDevices;

        private static void AddResolution(pDropdown dropdown, Size res, Size native)
        {
            if (res.Height > native.Height)
                return;

            string s = (res == native) ? ' ' + LocalisationManager.GetString(OsuString.Options_Graphics_ResolutionNative) : String.Empty;
            string resString = ResolutionHelper.ToString(res);

            dropdown.AddOption(resString + s, resString);
        }

        private static void AddWindowedResolution(pDropdown dropdown, Size res, Size desktop)
        {
            string s = res == desktop ? @" " + LocalisationManager.GetString(OsuString.Options_Graphics_ResolutionBorderless) : string.Empty;

            if ((res.Width <= desktop.Width) && (res.Height <= desktop.Height))
            {
                string resString = ResolutionHelper.ToString(res);
                dropdown.AddOption(resString + s, resString);
            }
        }

        private static void AddResolutions(pDropdown dropdown, List<Size> resolutions, Size nativeRes)
        {
            foreach (Size res in resolutions)
            {
                if (ConfigManager.sScreenMode == ScreenMode.Fullscreen)
                    AddResolution(dropdown, res, nativeRes);
                else
                    AddWindowedResolution(dropdown, res, resolutionDesktop);
            }
        }

        private static void SortAndRemoveDuplicates(List<Size> resolutions)
        {
            resolutions.Sort((res1, res2) => res1.Width == res2.Width ? res1.Height.CompareTo(res2.Height) : res1.Width.CompareTo(res2.Width));

            // Remove duplicates and everything below 800x600 in one sweep
            int i = 0;
            while (i < resolutions.Count - 1)
            {
                Size current = resolutions[i];
                if (current.Width < 800 || current.Height < 600 || current == resolutions[i + 1])
                {
                    resolutions.RemoveAt(i);
                }
                else
                {
                    ++i;
                }
            }
        }

        internal static void UpdateResolutions(pDropdown dropdown)
        {
            dropdown.RemoveAllOptions();

            List<Size> resolutions = ResolutionHelper.GetResolutions();

            if (resolutions.Count == 0)
                return;

            SortAndRemoveDuplicates(resolutions);

            Size nativeRes = resolutions[resolutions.Count - 1];

            // In desktop usage we want some more resolutions to be available. It is important, that we add them AFTER determining the native res, so AddResolution will be able to drop too large ones
            if (ConfigManager.sScreenMode != ScreenMode.Fullscreen)
            {
                // 4:3
                resolutions.Add(new Size(800, 600));
                resolutions.Add(new Size(1024, 768));
                resolutions.Add(new Size(1280, 960));
                resolutions.Add(new Size(1600, 1200));
                resolutions.Add(new Size(1920, 1440));
                resolutions.Add(new Size(2560, 1920));

                // 16:9 and 16:10
                resolutions.Add(new Size(1024, 600));
                resolutions.Add(new Size(1280, 800));
                resolutions.Add(new Size(1366, 768));
                resolutions.Add(new Size(1440, 900));
                resolutions.Add(new Size(1680, 1050));
                resolutions.Add(new Size(1920, 1080));
                resolutions.Add(new Size(1920, 1200));
                resolutions.Add(new Size(2560, 1440));
                resolutions.Add(new Size(2560, 1600));
            }

            SortAndRemoveDuplicates(resolutions);


            List<Size> normalResolutions = resolutions.FindAll(res => (res.Width * 3.0f) / (res.Height * 4.0f) <= 1.0f);
            if (normalResolutions.Count > 0)
            {
                dropdown.AddOption(LocalisationManager.GetString(OsuString.Options_Graphics_ResolutionStandard), null, SkinManager.NEW_SKIN_COLOUR_SECONDARY);
                AddResolutions(dropdown, normalResolutions, nativeRes);
            }

            List<Size> wideResolutions = resolutions.FindAll(res => (res.Width * 3.0f) / (res.Height * 4.0f) > 1.0f);
            if (wideResolutions.Count > 0)
            {
                dropdown.AddOption(LocalisationManager.GetString(OsuString.Options_Graphics_ResolutionWidescreen), null, SkinManager.NEW_SKIN_COLOUR_SECONDARY);
                AddResolutions(dropdown, wideResolutions, nativeRes);
            }

            Size currentWindowSize =  Size.Empty;

            switch (ConfigManager.sScreenMode.Value)
            {
                case ScreenMode.Windowed:
                    currentWindowSize = new Size(ConfigManager.sWidth, ConfigManager.sHeight);
                    break;
                case ScreenMode.Fullscreen:
                    currentWindowSize = new Size(ConfigManager.sWidthFullscreen, ConfigManager.sHeightFullscreen);
                    break;
                case ScreenMode.BorderlessWindow:
                    currentWindowSize = ResolutionHelper.GetDesktopResolution();
                    break;
            }

            string currentWindowSizeString = ResolutionHelper.ToString(currentWindowSize);

            if (!dropdown.SetSelected(currentWindowSizeString, true))
            {
                if (currentWindowSize.Width == 9999)
                {
                    dropdown.SetSelected(ResolutionHelper.ToString(resolutions[resolutions.Count - 1]), true);
                }
                else
                {
                    dropdown.AddOption(currentWindowSizeString + @" (" + LocalisationManager.GetString(OsuString.Options_Graphics_CustomResolution) + @")", currentWindowSizeString);
                    dropdown.SetSelected(currentWindowSizeString, true);
                }
            }
        }

        internal void ReloadElements(bool jumpToTop = true)
        {
            if (!initialised) return;

            options.ForEach(o => o.Dispose());
            options.Clear();

            sidebarTabs.Clear();

            spriteManagerTabs.Clear();

            InitializeOptions();

            Layout();
        }

        internal static IEnumerable<pDropdownItem> DropdownItemsReleaseStream
        {
            get
            {
                return new[] {
                    new pDropdownItem(LocalisationManager.GetString(OsuString.Options_ReleaseStrategy_Stable), ReleaseStream.Stable),
                    new pDropdownItem(LocalisationManager.GetString(OsuString.Options_ReleaseStrategy_Beta), ReleaseStream.Beta),
                    new pDropdownItem(LocalisationManager.GetString(OsuString.Options_ReleaseStrategy_CuttingEdge), ReleaseStream.CuttingEdge),
#if Release || DEBUG
                    new pDropdownItem(@"Cutting Edge (opengl / .net45)", ReleaseStream.CuttingEdge45)
#endif
                };
            }
        }

        public static IEnumerable<pDropdownItem> DropdownItemsFrameLimiter
        {
            get
            {
                return new[] {
                    new pDropdownItem(LocalisationManager.GetString(OsuString.Options_Graphics_FrameLimiter_60), FrameSync.VSync),
                    new pDropdownItem(LocalisationManager.GetString(OsuString.Options_Graphics_FrameLimiter_120), FrameSync.Limit120),
                    new pDropdownItem(ConfigManager.sCustomFrameLimit + @"fps", FrameSync.Limit240),
                    new pDropdownItem(LocalisationManager.GetString(OsuString.Options_Graphics_FrameLimiter_UnlimGreen), FrameSync.Unlimited),
                    new pDropdownItem(LocalisationManager.GetString(OsuString.Options_Graphics_FrameLimiter_Unlim), FrameSync.CompletelyUnlimited)
                };
            }
        }

        public IEnumerable<pDropdownItem> DropdownItemsResolutions { get; set; }
    }

    internal class OptionsToggleButton : pDrawableComponent
    {
        pSprite button;
        private pTextAwesome icon;

        Color colour_idle = new Color(238, 51, 153);

        public OptionsToggleButton(Options instance)
            : base(true)
        {
            spriteManager.Add(button = new pSprite(SkinManager.Load(@"back-button-layer", SkinSource.Osu), Fields.CentreLeft, Origins.CentreLeft, Clocks.Game, Vector2.Zero, 0.91F, true, colour_idle)
            {
                DrawLeft = 120
            });

            spriteManager.Add(icon = new pTextAwesome(FontAwesome.gear, 18, new Vector2(10, 0))
            {
                Depth = 0.92f,
                Field = Fields.CentreLeft,
                Origin = Origins.Centre
            });

            button.HandleInput = true;
            button.OnClick += delegate { instance.Toggle(); };
            spriteManager.Masking = true;
        }

        internal bool selected;

        internal bool Selected
        {
            get { return selected; }
            set
            {
                if (selected == value) return;
                selected = value;
            }
        }
    }
}
