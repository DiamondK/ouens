﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;

namespace osu.GameModes.Options
{
    class OptionTab
    {
        internal List<pSprite> Sprites = new List<pSprite>();
        internal OptionCategory Category;
        private pSprite background;
        private pSprite light;
        private pTextAwesome icon;
        private pText text;

        internal const int tab_height = 40;

        public OptionTab(OptionCategory oc, EventHandler icon_OnClick)
        {
            Category = oc;

            background = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0.1F, true, new Color(8, 8, 8, 191));
            background.Alpha = 0.01f;
            background.Scale = 1.6f;
            background.VectorScale = new Vector2(Options.TAB_WIDTH_EXPANDED, tab_height);
            background.HandleInput = true;
            background.OnClick += icon_OnClick;
            background.ClickRequiresConfirmation = true;
            background.OnHover += background_OnHover;
            background.OnHoverLost += background_OnHoverLost;
            background.Tag = Category;

            Sprites.Add(background);

            light = new pSprite(GameBase.WhitePixel, Fields.TopRight, Origins.TopRight, Clocks.Game, Vector2.Zero, 0.11F, true, SkinManager.NEW_SKIN_COLOUR_MAIN);
            light.Alpha = 0;
            light.Scale = 1.6f;
            light.VectorScale = new Vector2(4, tab_height);

            Sprites.Add(light);

            icon = new pTextAwesome(oc.Icon, 18, new Vector2(Options.TAB_WIDTH / 2, tab_height / 2))
            {
                InitialColour = new Color(153, 153, 153),
            };

            Sprites.Add(icon);

            text = new pText(oc.Title, 14, new Vector2(Options.TAB_WIDTH + 5, tab_height / 2), 0.5f, true, Color.White)
            {
                Origin = Origins.CentreLeft
            };

            Sprites.Add(text);
        }

        void background_OnHoverLost(object sender, EventArgs e)
        {
            if (!active) icon.FadeColour(new Color(153, 153, 153), 100);
        }

        void background_OnHover(object sender, EventArgs e)
        {
            icon.FadeColour(Color.White, 100);
        }

        internal void MoveTo(Vector2 position)
        {
            foreach (pSprite s in Sprites)
                s.MoveTo(new Vector2(s.InitialPosition.X + position.X, s.InitialPosition.Y + position.Y), 0);
        }

        internal void Hide()
        {
            icon.FadeTo(0.2f, 100);
            text.FadeTo(0.2f, 100);
        }

        internal void Show()
        {
            icon.FadeIn(100);
            text.FadeIn(100);
        }

        bool active;
        internal bool Active
        {
            get { return active; }
            set
            {
                if (active == value) return;

                active = value;

                if (active)
                {
                    background.FadeIn(100);
                    light.FadeIn(300, EasingTypes.OutCubic);

                    background_OnHover(null, null);
                }
                else
                {
                    background.FadeTo(0.01f, 300);
                    light.FadeOut(300, EasingTypes.OutCubic);

                    background_OnHoverLost(null, null);
                }
            }
        }


    }
}
