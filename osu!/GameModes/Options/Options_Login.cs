﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using osu.Configuration;
using osu.Graphics.Skinning;
using osu.Online;
using osu_common.Helpers;

namespace osu.GameModes.Options
{
    internal partial class Options
    {
        private OptionSection getLoginSection()
        {
            loginUserTB = null;
            loginPassTB = null;

            if (!BanchoClient.AllowUserSwitching)
            {
                return new OptionSection(OsuString.General_Login)
                {
                    Children = new[] { new OptionText(LocalisationManager.GetString(OsuString.Options_ClientLocked)) }
                };
            }

            if (BanchoClient.Authenticated)
            {
                return new OptionSection(OsuString.General_Login)
                {
                    Children = new[] 
                    { 
                        new OptionText(string.Format(LocalisationManager.GetString(OsuString.Options_LoggedIn), ConfigManager.sUsername), delegate 
                        {
                            UserProfile.DisplayProfileFor(GameBase.User);
                        })
                    }
                };
            }

            if (GameBase.HasLogin)
            {
                return new OptionSection(OsuString.General_Login)
                {
                    Children = new OptionElement[] { new OptionSpinner() }
                };
            }

            ;
            BanchoClient.DisableBancho = true;
            OptionSection loginSection = new OptionSection(OsuString.General_Login)
            {
                Children = new OptionElement[] 
                {
                    (loginUserTB = new OptionTextbox(OsuString.Options_Username, ConfigManager.sUsername)),
                    (loginPassTB = new OptionTextbox(OsuString.Options_Password, true)),
                    new OptionCheckbox(LocalisationManager.GetString(OsuString.Options_RememberUsername), string.Empty, ConfigManager.sSaveUsername),
                    new OptionCheckbox(LocalisationManager.GetString(OsuString.Options_RememberPassword), string.Empty, ConfigManager.sSavePassword),
                    new OptionButton(OsuString.General_Login, SkinManager.NEW_SKIN_COLOUR_SECONDARY, delegate { performLogin(); }),
                    new OptionButton(LocalisationManager.GetString(OsuString.Options_CreateAnAccount), SkinManager.NEW_SKIN_COLOUR_SECONDARY, delegate 
                    {
                        GameBase.ProcessStart(General.WEB_ROOT + @"/p/register");
                    })
                }
            };

            loginUserTB.Textbox.InputControl.OnKey += handleLoginTextBoxInput;
            loginPassTB.Textbox.InputControl.OnKey += handleLoginTextBoxInput;

            return loginSection;
        }

        private void handleLoginTextBoxInput(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;

            performLogin();
        }

        private void performLogin()
        {
            if (string.IsNullOrEmpty(loginUserTB.Textbox.Text) || string.IsNullOrEmpty(loginPassTB.Textbox.Text))
                return;
            ConfigManager.sUsername.Value = loginUserTB.Textbox.Text;
            ConfigManager.sPassword.Value = CryptoHelper.GetMd5String(loginPassTB.Textbox.Text);
            BanchoClient.pingTimeout = 0;
            BanchoClient.DisableBancho = false;
            ReloadElements();
        }

        internal void PerformLogout()
        {
            BanchoClient.DisconnectImmediately();
            ConfigManager.sUsername.Value = string.Empty;
            ConfigManager.sPassword.Value = string.Empty;
            GameBase.User.Name = string.Empty;
            GameBase.User.Refresh();
            ReloadElements();
        }

        private void banchoLoginResult(object sender, EventArgs e)
        {
            if ((bool)sender)
            {
                GameBase.User.Name = ConfigManager.sUsername;
                GameBase.User.Refresh();

                if (OnLoginResult != null)
                    OnLoginResult(true);

                if (loginOnly)
                {
                    GameBase.Scheduler.AddDelayed(delegate
                    {
                        if (Expanded)
                            Expanded = false;
                        if (PoppedOut)
                            PoppedOut = false;
                    }, 500);

                }
            }

            GameBase.Scheduler.Add(delegate
            {
                ReloadElements();
                if (loginPassTB != null)
                    loginPassTB.Focus();
            });
        }
    }
}
