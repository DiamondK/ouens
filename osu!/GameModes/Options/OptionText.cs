using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Renderers;
using osu.Graphics.Sprites;
using osu_common.Helpers;

namespace osu.GameModes.Options
{
    internal class OptionText : OptionElement
    {
        private pText textSprite;

        internal VoidDelegate OnClick;

        public OptionText(string text, VoidDelegate onClick = null)
        {
            int extraHeight = 0;
            textSprite = new pText(text, 12, new Vector2(0, extraHeight / 2), new Vector2(ELEMENT_SIZE, 0), 1, true, Color.White);
            textSprite.TextAlignment = TextAlignment.Left;
            textSprite.HandleInput = true;
            textSprite.OnClick += textSprite_OnClick;
            textSprite.ClickRequiresConfirmation = true;
            spriteManager.Add(textSprite);

            Size = textSprite.MeasureText() + new Vector2(0, extraHeight);
            addKeyword(text);

            if (onClick != null) OnClick = onClick;
        }

        void textSprite_OnClick(object sender, EventArgs e)
        {
            if (OnClick != null) OnClick();
        }

        internal override bool IsHeaderElement
        {
            get
            {
                return false;
            }
        }

        internal void UpdateText(string text)
        {
            textSprite.Text = text;
        }
    }
}