﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Graphics.Sprites;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Renderers;
using osu.Input;
using osu.Input.Handlers;
using Microsoft.Xna.Framework.Input;
using osu.GameModes.Play.Components;
using osu.Graphics.Skinning;
using System.Threading;
using osu_common;

namespace osu.GameModes.Options
{
    class Benchmark : DrawableGameComponent
    {
        SpriteManager spriteManager = new SpriteManager(true);
        SpriteManager benchmarkSpriteManager = new SpriteManager(true);

        bool running;
        Stages activeStage;

        Dictionary<Stages, int> stageScores = new Dictionary<Stages, int>();

        enum Stages
        {
            Idle,
            Start,
            IdleFramerate,
            TextRendering,
            FillRate,
            TexturedFillRate,
            TextureLoading,
            Threading,
            CPUPerformance,
            GarbageCollection,
            Finished
        }

        public Benchmark(GameBase game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            descriptiveText = new pText("Please click to start the benchmark.\nWhile running do not move your mouse or switch windows.", 20, new Vector2(0, 0), new Vector2(600, 0), 1, true, Color.White, true)
            {
                TextAlignment = TextAlignment.Right,
                Origin = Origins.Centre,
                Field = Fields.Centre
            };
            spriteManager.Add(descriptiveText);

            GameBase.LoadComplete();

            base.Initialize();

            KeyboardHandler.OnKeyPressed += KeyboardHandler_OnKeyPressed;
            InputManager.Bind(InputEventType.OnClick, onClick);

            progressBar = new ProgressBar(spriteManager, ProgressBarTypes.Bottom);
        }

        double elapsedTime;
        private pText descriptiveText;
        private void start()
        {
            if (running) return;

            running = true;
            elapsedTime = GameBase.Time;
            startNextStage();
        }

        protected override void Dispose(bool disposing)
        {
            spriteManager.Dispose();
            benchmarkSpriteManager.Dispose();
            progressBar.Dispose();

            KeyboardHandler.OnKeyPressed -= KeyboardHandler_OnKeyPressed;

            base.Dispose(disposing);
        }

        public override void Draw()
        {
            base.Draw();

            benchmarkSpriteManager.Draw();
            spriteManager.Draw();
        }

        const int test_time = 5000;
        const int test_time_cooldown = test_time + 1000;

        bool cooldown;
        private ProgressBar progressBar;

        public override void Update()
        {
            base.Update();

            progressBar.SetProgress((float)(elapsedTime / test_time_cooldown));

            if (!running) return;

            elapsedTime += GameBase.ElapsedMilliseconds;

            if (elapsedTime > test_time_cooldown)
            {
                startNextStage();
                return;
            }
            else if (elapsedTime > test_time)
            {
                if (!cooldown)
                {
                    benchmarkSpriteManager.SpriteList.ForEach(s => s.FadeOut(300));
                    cooldown = true;
                }

                GC.Collect();
            }

            if (stageScores.ContainsKey(activeStage))
                stageScores[activeStage]++;

            if (cooldown)
                return;

            switch (activeStage)
            {
                case Stages.TextRendering:
                    const int text_lines = 4;

                    if (benchmarkSpriteManager.SpriteList.Count == 0)
                    {
                        for (int i = 0; i < text_lines; i++)
                            benchmarkSpriteManager.Add(new pText("Testing text rendering", 20, new Vector2(GameBase.random.Next(0, GameBase.WindowWidth), GameBase.random.Next(0, GameBase.WindowHeight)), new Vector2(400, 0), 1, true, Color.White, true)
                            {
                                TextAlignment = TextAlignment.Centre,
                                Origin = Origins.Centre,
                            });
                    }

                    for (int i = 0; i < text_lines; i++)
                    {
                        ((pText)benchmarkSpriteManager.SpriteList[i]).Text = GameBase.random.NextDouble().ToString();
                    }
                    break;
                case Stages.CPUPerformance:
                case Stages.GarbageCollection:
                    Color col = Color.MediumPurple;
                    if (activeStage == Stages.CPUPerformance)
                    {
                        benchmarkSpriteManager.Clear();
                        col = Color.GreenYellow;
                    }
                    else
                        benchmarkSpriteManager.SpriteList.Clear();

                    for (int i = 0; i < 1000; i++)
                        benchmarkSpriteManager.Add(new pSprite(GameBase.WhitePixel, Fields.Native, Origins.TopLeft, Clocks.Game, new Vector2(GameBase.random.Next(0, GameBase.WindowWidth), GameBase.random.Next(0, GameBase.WindowHeight)), 0, true, col) { Alpha = 1, Additive = true });
                    break;
                case Stages.TextureLoading:
                    {
                        SkinManager.ClearDefaultCache();
                        pSprite p = new pSprite(SkinManager.Load(@"sliderb" + GameBase.random.Next(0, 10), SkinSource.Osu), Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(GameBase.random.Next(0, GameBase.WindowWidth), GameBase.random.Next(0, GameBase.WindowHeight)), 0, false, Color.White) { Additive = true, };
                        p.FadeOutFromOne(500);

                        benchmarkSpriteManager.Add(p);

                        benchmarkSpriteManager.SpriteList.ForEach(s => ((pSprite)s).Texture = SkinManager.Load(@"sliderb" + GameBase.random.Next(0, 10), SkinSource.Osu));
                    }
                    break;
                case Stages.Threading:
                    if (benchmarkSpriteManager.SpriteList.Count == 0)
                    {
                        pSprite p = new pSprite(GameBase.WhitePixel, Fields.Native, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0, true, Color.Orange)
                        {
                            Additive = true,
                            VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight + GameBase.WindowOffsetY)
                        };
                        p.FadeInFromZero(test_time);
                        benchmarkSpriteManager.Add(p);

                        for (int i = 0; i < 2; i++)
                            GameBase.RunBackgroundThread(delegate { while (elapsedTime < test_time) Thread.SpinWait(1); });
                    }
                    break;
                case Stages.FillRate:
                    if (benchmarkSpriteManager.SpriteList.Count == 0)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            pSprite p = new pSprite(GameBase.WhitePixel, Fields.Native, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0, true,
                                new Color((byte)GameBase.random.Next(0, 255), (byte)GameBase.random.Next(0, 255), (byte)GameBase.random.Next(0, 255), 255))
                            {
                                Additive = true,
                                VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight + GameBase.WindowOffsetY)
                            };

                            p.FadeInFromZero(GameBase.random.Next(10000, 30000));
                            benchmarkSpriteManager.Add(p);
                        }
                    }
                    break;
                case Stages.TexturedFillRate:
                    if (benchmarkSpriteManager.SpriteList.Count == 0)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            pSprite p = new pSprite(SkinManager.Load(@"menu-background", SkinSource.Osu), Fields.Native, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0, true, Color.White);

                            if (GameBase.WindowWidth / GameBase.WindowHeight > 1.6f)
                            {
                                p.VectorScale = new Vector2(GameBase.WindowWidth / GameBase.WindowRatio / GameBase.WindowDefaultWidth, GameBase.WindowHeight / GameBase.WindowRatio / GameBase.WindowDefaultHeight);
                            }

                            p.Additive = true;

                            p.FadeInFromZero(20000);
                            benchmarkSpriteManager.Add(p);
                        }
                    }
                    break;
            }
        }

        private void startNextStage()
        {
            benchmarkSpriteManager.Clear();
            activeStage = (Stages)((int)activeStage + 1);
            elapsedTime = 0;
            cooldown = false;

            //display progress text
            switch (activeStage)
            {
                case Stages.Start:
                    descriptiveText.Text = "Preparing...";
                    break;
                case Stages.Finished:
                    descriptiveText.TextAlignment = TextAlignment.Right;
                    descriptiveText.Text = "Complete!\n";

                    int cumulativeScore = 0;
                    int cumulativeRawScore = 0;
                    int testsRun = 0;

                    int baseScore = stageScores[Stages.IdleFramerate];
                    foreach (KeyValuePair<Stages, int> ss in stageScores)
                    {
                        int score = 0;
                        switch (ss.Key)
                        {
                            case Stages.IdleFramerate:
                                descriptiveText.Text += string.Format("\nIdle framerate: {0}fps\n\nIndividual tests:", ss.Value / (test_time_cooldown / 1000));
                                cumulativeRawScore += ss.Value;
                                continue;
                            default:
                                score = (int)Math.Min(100, (float)ss.Value / baseScore * 100);

                                testsRun++;
                                break;
                        }

                        int fps = ss.Value / (test_time_cooldown / 1000);

                        if (fps > 120)
                            score = (int)Math.Min(100, score + (float)(fps - 120) / 2);

                        cumulativeScore += score;
                        cumulativeRawScore += ss.Value;

                        descriptiveText.Text += string.Format("\n{0}: {1} ({2}fps)", ss.Key, score, fps);
                    }

                    descriptiveText.Text += "\n\nOverall Smoothness: " + (cumulativeScore / testsRun);
                    descriptiveText.Text += "\nRaw Score: " + cumulativeRawScore;


                    running = false;
                    activeStage = Stages.Idle;
                    break;
                default:
                    descriptiveText.Text = string.Format("Running stage {0} of {1}\n{2}", (int)(activeStage - Stages.Start), (int)(Stages.Finished - Stages.Start) - 1, activeStage);
                    stageScores[activeStage] = 0;
                    break;
            }
        }

        bool onClick(object sender, EventArgs e)
        {
            if (!running)
            {
                start();
                return true;
            }

            return false;
        }

        bool KeyboardHandler_OnKeyPressed(object sender, Keys k)
        {
            if (k == Keys.Escape)
            {
                GameBase.ChangeMode(OsuModes.Menu);
                return true;
            }

            return false;
        }

    }
}
