using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Helpers;
using osu_common.Helpers;

namespace osu.GameModes.Options
{
    internal class OptionSlider : OptionElement
    {
        internal pSliderBar Slider;
        private BindableDouble value;

        private double localValue;
        private string unit;

        private bool immediatelyAdjust;

        public OptionSlider(OsuString title, BindableDouble value, string unit = null, bool immediatelyAdjust = true)
        {
            this.value = value;
            localValue = value.Value;

            this.unit = unit;

            this.immediatelyAdjust = immediatelyAdjust;

            value.ValueChanged += value_ValueChanged;

            string text = LocalisationManager.GetString(title);

            pText header = new pText(text, 12, Vector2.Zero, 1, true, Color.White);
            spriteManager.Add(header);

            Size = header.MeasureText();

            Slider = new pSliderBar(spriteManager, value.MinValue, value.MaxValue, value.Value, new Vector2(Size.X + 5, Size.Y / 2), (int)(ELEMENT_SIZE - Size.X - 5));
            Slider.ValueChanged += Slider_ValueChanged;

            Slider.KeyboardControl = true;
            Slider.KeyboardControlIncrement = value is BindableInt ? 1 : 0.01;

            addKeyword(text);
            addKeyword(title.ToString());
            updateUnit();
        }

        void value_ValueChanged(object sender, EventArgs e)
        {
            localValue = value.Value;
            Slider.SetValue(value.Value, false);
        }

        private void updateUnit()
        {
            if (!string.IsNullOrEmpty(unit))
            {
                if (immediatelyAdjust)
                {
                    Slider.Tooltip = value + unit;
                }
                else
                {
                    Slider.Tooltip = localValue.ToString("0.##", GameBase.nfi) + unit;
                }
            }
        }

        void Slider_ValueChanged(object sender, EventArgs e)
        {
            localValue = Slider.current;

            if (immediatelyAdjust || !Slider.IsDragging)
            {
                value.Value = localValue;
            }

            updateUnit();
        }

        public override void Dispose()
        {
            base.Dispose();
            Slider.Dispose();
            value.ValueChanged -= value_ValueChanged;
        }
    }
}