﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Helpers;
using osu.Input;
using osu_common.Helpers;

namespace osu.GameModes.Options
{
    internal class OptionElement : IComparable<OptionElement>, IDisposable
    {
        internal SpriteManager spriteManager = new SpriteManager(true) { Masking = true };

        internal float VerticalPosition;

        private Vector2 size = new Vector2(ELEMENT_SIZE, 13.125f);
        internal Vector2 Size
        {
            get { return size; }
            set
            {
                if (value == size) return;

                float oldHeight = size.Y;
                size = value;

                if (oldHeight != size.Y) triggerHeightChanged();
            }
        }

        private float sizeOverflow = 0;
        internal float SizeOverflow
        {
            get { return sizeOverflow; }
            set
            {
                if (value == sizeOverflow) return;

                sizeOverflow = value;
                triggerHeightChanged();
            }
        }

        internal List<string> Keywords = new List<string>() { };

        internal event EventHandler HeightChanged;
        protected void triggerHeightChanged()
        {
            if (HeightChanged != null) HeightChanged(this, null);
        }

        internal const int ELEMENT_SIZE = 244;

        private bool expandedOnly;
        internal bool ExpandedOnly
        {
            get { return expandedOnly; }
            set
            {
                expandedOnly = value;
                if (children != null)
                    foreach (OptionElement c in children)
                        c.ExpandedOnly = expandedOnly;
            }
        }

        public OptionElement Parent;

        private IEnumerable<OptionElement> children;
        internal virtual IEnumerable<OptionElement> Children
        {
            get { return children; }
            set
            {
                children = value;
                if (children != null)
                    foreach (OptionElement c in children)
                    {
                        c.Parent = this;
                        if (ExpandedOnly) c.ExpandedOnly = true;
                    }
            }
        }

        protected void addKeyword(string keyword)
        {
            Keywords.Add(keyword.ToLower());
        }

        public bool Match(string search)
        {
            search = search.ToLower();
            foreach (string k in Keywords)
                if (k.Contains(search))
                    return true;
            return false;
        }

        int IComparable<OptionElement>.CompareTo(OptionElement other)
        {
            return 0;
        }

        internal void Show(bool showChildren = false)
        {
            if (showChildren && Children != null)
                foreach (OptionElement c in Children)
                    c.Show();

            spriteManager.Bypass = false;
        }

        internal void Hide()
        {
            spriteManager.Bypass = true;
        }

        internal virtual void Layout() { }

        internal virtual bool IsHeaderElement { get { return false; } }

        public IEnumerable<OptionElement> AllChildren
        {
            get
            {
                List<OptionElement> children = new List<OptionElement>();
                if (Children != null)
                    foreach (OptionElement c in Children)
                    {
                        children.Add(c);
                        children.AddRange(c.AllChildren);
                    }
                return children;
            }
        }

        public virtual void Dispose()
        {
            spriteManager.Dispose();
            foreach (OptionElement e in AllChildren)
                e.Dispose();
        }

        internal virtual int PaddingBefore { get { return 0; } }

        public string Tooltip
        {
            get
            {
                pSprite found = spriteManager.SpriteList.FindLast(s =>
                {
                    pSprite ps = s as pSprite;
                    return ps != null && !string.IsNullOrEmpty(ps.ToolTip);
                }) as pSprite;

                if (found != null) return found.ToolTip;

                return null;
            }
        }
    }
}
