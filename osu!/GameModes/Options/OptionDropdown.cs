using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Helpers;
using osu_common.Helpers;

namespace osu.GameModes.Options
{
    internal class OptionDropdown : OptionElement
    {
        private OsuString osuString;
        internal pDropdown Dropdown;

        HasObjectValue binding;

        public OptionDropdown(OsuString title, IEnumerable<pDropdownItem> items = null, HasObjectValue binding = null, EventHandler onChange = null)
        {
            this.binding = binding;

            ValueChangedObservable changeObservation = binding as ValueChangedObservable;
            if (changeObservation != null) changeObservation.ValueChanged += changeObservation_ValueChanged;

            string text = LocalisationManager.GetString(title);
            pText header = new pText(text, 12, Vector2.Zero, 1, true, Color.White);
            spriteManager.Add(header);

            Size = header.MeasureText() + new Vector2(5, 0);

            Dropdown = new pDropdown(spriteManager, String.Empty, new Vector2(Size.X, 0), ELEMENT_SIZE - Size.X, 1) { ColumnLimit = 10 };
            Dropdown.OnSelect += Dropdown_OnSelect;
            Dropdown.OnDisplayChanged += Dropdown_OnDisplayChanged;

            Dropdown.SpriteMainBox.ClickRequiresConfirmation = true;

            if (onChange != null) Dropdown.OnSelect += onChange;

            if (items != null)
                foreach (pDropdownItem i in items)
                {
                    Dropdown.AddOption(i);
                    addKeyword(i.Text);
                }

            if (binding != null)
                Dropdown.SetSelected(binding.ObjectValue, true);


            addKeyword(text);
            addKeyword(title.ToString());
        }

        void changeObservation_ValueChanged(object sender, EventArgs e)
        {
            Dropdown.SetSelected(binding.ObjectValue, true);
        }

        void Dropdown_OnDisplayChanged(object sender, EventArgs e)
        {
            SizeOverflow = Dropdown.Visible ? Dropdown.OptionsSprites.Count * Size.Y : 0;
        }

        void Dropdown_OnSelect(object sender, EventArgs e)
        {
            if (binding != null)
                binding.ObjectValue = Dropdown.SelectedObject;

            SizeOverflow = 0;
        }

        public override void Dispose()
        {
            base.Dispose();

            ValueChangedObservable changeObservation = binding as ValueChangedObservable;
            if (changeObservation != null) changeObservation.ValueChanged -= changeObservation_ValueChanged;
        }
    }
}