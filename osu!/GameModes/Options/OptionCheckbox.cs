using System;
using Microsoft.Xna.Framework;
using osu.Graphics.UserInterface;
using osu.Helpers;
using osu_common.Helpers;

namespace osu.GameModes.Options
{
    internal class OptionCheckbox : OptionElement
    {
        private BindableBool binding;
        event EventHandler ValueChanged;
        internal pCheckbox Checkbox;

        internal OptionCheckbox(OsuString title, BindableBool binding, EventHandler onChange = null)
            : this(LocalisationManager.GetString(title), LocalisationManager.GetString(title + 1), binding, onChange)
        {
            addKeyword(title.ToString());
        }

        internal OptionCheckbox(string title, string tooltip, BindableBool binding = null, EventHandler onChange = null)
            : this(title, tooltip, binding != null ? binding.Value : false, onChange)
        {
            this.binding = binding;

            if (binding != null) binding.ValueChanged += binding_ValueChanged;
        }

        void binding_ValueChanged(object sender, EventArgs e)
        {
            Checkbox.SetStatusQuietly(binding.Value);
        }

        internal OptionCheckbox(string title, string tooltip, bool initialState = false, EventHandler onChange = null)
        {
            Checkbox = new pCheckbox(title, 0.8f, Vector2.Zero, 1, initialState, OptionElement.ELEMENT_SIZE) { Tooltip = tooltip };

            Checkbox.OnCheckChanged += cb_OnCheckChanged;

            if (onChange != null)
                ValueChanged += onChange;

            spriteManager.Add(Checkbox.SpriteCollection);

            addKeyword(title);
            addKeyword(tooltip);

            UpdateLabel(title);
        }

        internal void UpdateLabel(string newLabel)
        {
            Checkbox.Text = newLabel;
            spriteManager.SpriteList.ForEach(s => s.Position = s.InitialPosition + new Vector2(0, -8 + Checkbox.sText.MeasureText().Y / 2));
            Size = new Vector2(20, 8 - 8) + Checkbox.sText.MeasureText();
        }

        void cb_OnCheckChanged(object sender, bool status)
        {
            binding.Value = status;
            if (ValueChanged != null) ValueChanged(this, null);
        }

        public override void Dispose()
        {
            base.Dispose();
            if (binding != null) binding.ValueChanged -= binding_ValueChanged;
        }

        internal bool Enabled
        {
            get { return Checkbox.Enabled; }
            set { Checkbox.Enabled = value; }
        }
    }
}