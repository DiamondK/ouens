﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.GameModes.Play.Rulesets.Mania;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input.Handlers;
using osu_common;
using osu_common.Helpers;
using osu.Input;
using osu.Helpers;

namespace osu.GameModes.Options
{
    internal class OptionsManiaBindingWizard : pDialog
    {
        private int currentKeyConfig = 4;
        private int currentBindingColumn = 0;

        private pText[] keys;
        private pSprite arrow;

        private pDropdown dKeyConfig;
        private pCheckbox cSplitLayout;
        private pCheckbox cUpsideDown;
        private pCheckbox cJudgementLine;
        private pDropdown dSpecialStyle;
        private pCheckbox cBindAlternateKeys;

        private StageMania stage;

        internal OptionsManiaBindingWizard()
            : base(LocalisationManager.GetString(OsuString.Options_TabSkin_CustonKey), false)
        {
            SkinManager.CreateUserSkin();

            spriteManager.HandleOverlayInput = true;
            BackButton back = new BackButton(delegate
            {
                AudioEngine.PlaySamplePositional(@"menuback");
                Close();
            }, false, true);
            spriteManager.Add(back.SpriteCollection);

            int horizontal = 225 + (int)GameBase.WindowOffsetXScaled;
            int vertCurrent = 80;
            int vertSpacing = 35;

            dKeyConfig = new pDropdown(spriteManager, String.Empty, new Vector2(horizontal + 10, vertCurrent), 75, 1);

            for (int i = 1; i <= StageMania.MAX_COLUMNS; incrementKeyConfig(ref i))
            {
                dKeyConfig.AddOption(getKeyOptionText(i, i >= 10), i);
            }

            dKeyConfig.OnSelect += (obj, ev) =>
            {
                currentKeyConfig = (int)obj;

                cBindAlternateKeys.SetStatusQuietly(false);

                LoadKeys(true);
            };

            cSplitLayout = new pCheckbox(LocalisationManager.GetString(OsuString.Options_TabSkin_SplitLayout), new Vector2(horizontal + 105, vertCurrent), 0.8F, false);
            cSplitLayout.OnCheckChanged += (obj, bl) =>
            {
                LoadKeys();
            };
            spriteManager.Add(cSplitLayout.SpriteCollection);

            vertCurrent = 300;

            dSpecialStyle = new pDropdown(spriteManager, string.Empty, new Vector2(horizontal - 65, vertCurrent), 150, 0.8f, false);
            dSpecialStyle.AddOption(LocalisationManager.GetString(OsuString.Options_ManiaSpecial_Normal), 0);
            dSpecialStyle.AddOption(LocalisationManager.GetString(OsuString.Options_ManiaSpecial_Left), 1);
            dSpecialStyle.AddOption(LocalisationManager.GetString(OsuString.Options_ManiaSpecial_Right), 2);
            dSpecialStyle.OnSelect += (obj, ev) =>
            {
                switch ((int)obj)
                {
                    case 1:
                        stage.Skin.SpecialStyle = ManiaSpecialStyle.Left;
                        break;
                    case 2:
                        stage.Skin.SpecialStyle = ManiaSpecialStyle.Right;
                        break;
                    default:
                        stage.Skin.SpecialStyle = ManiaSpecialStyle.None;
                        break;
                }
                SkinManager.SaveSkin();

                LoadKeys();
            };

            cBindAlternateKeys = new pCheckbox(LocalisationManager.GetString(OsuString.Options_TabSkin_BindAlternateSpecialKeys), new Vector2(horizontal + 105, vertCurrent), 0.8F, false);
            cBindAlternateKeys.OnCheckChanged += (obj, bl) =>
            {
                LoadKeys();
            };
            spriteManager.Add(cBindAlternateKeys.SpriteCollection);

            vertCurrent += vertSpacing;

            cUpsideDown = new pCheckbox(LocalisationManager.GetString(OsuString.Options_TabSkin_UpsiedDown), new Vector2(horizontal, vertCurrent), 0.8F, false);
            cUpsideDown.OnCheckChanged += (obj, bl) =>
            {
                stage.Skin.UpsideDown = bl;
                SkinManager.SaveSkin();
            };
            spriteManager.Add(cUpsideDown.SpriteCollection);

            vertCurrent += vertSpacing;

            cJudgementLine = new pCheckbox(LocalisationManager.GetString(OsuString.Options_TabSkin_ShowJudgement), new Vector2(horizontal, vertCurrent), 0.8F, false);
            cJudgementLine.OnCheckChanged += (obj, bl) =>
            {
                stage.Skin.JudgementLine = bl;
                SkinManager.SaveSkin();
            };

            spriteManager.Add(cJudgementLine.SpriteCollection);

            dKeyConfig.SetSelected(currentKeyConfig, false);
        }

        protected override void Dispose(bool disposing)
        {
            disposeManiaStage();
            base.Dispose(disposing);
        }

        internal void LoadKeys(bool setDefaultSplitStages = false)
        {
            currentBindingColumn = 0;

            spriteManager.SpriteList.FindAll(s => s.Tag == this).ForEach(s =>
            {
                s.FadeOut(50);
                s.AlwaysDraw = false;
                spriteManager.Remove(s);
            });

            disposeManiaStage();

            SkinMania skin = SkinManager.LoadManiaSkin(currentKeyConfig);

            cSplitLayout.Enabled = currentKeyConfig > 1 && skin.SplitStagesFromSkin == null;
            if (!setDefaultSplitStages)
                skin.SplitStages = cSplitLayout.Checked;
            cSplitLayout.SetStatusQuietly(skin.SplitStages);

            dKeyConfig.SpriteMainBox.Text = getKeyOptionText(currentKeyConfig, skin.SplitStages);

            stage = new StageMania(skin, true);

            //Validate the layout by recopying it to itself
            stage.CopyBindingToLayout(stage.LayoutList.Layout);

            dSpecialStyle.Enabled = stage.AllowSpecialStyle;

            switch (stage.SpecialStyle)
            {
                case ManiaSpecialStyle.Left:
                    dSpecialStyle.SetSelected(1, true);
                    break;
                case ManiaSpecialStyle.Right:
                    dSpecialStyle.SetSelected(2, true);
                    break;
                default:
                    dSpecialStyle.SetSelected(0, true);
                    break;
            }

            cBindAlternateKeys.Enabled = stage.SpecialStyle != ManiaSpecialStyle.None;
            if (stage.SpecialStyle == ManiaSpecialStyle.None)
                cBindAlternateKeys.SetStatusQuietly(false);

            cJudgementLine.SetStatusQuietly(skin.JudgementLine);
            cUpsideDown.SetStatusQuietly(skin.UpsideDown);

            float delta = 40f;
            float separation = skin.SplitStages ? 1f : 0f;
            float totalWidth = (currentKeyConfig + separation) * delta;

            //Scale down if the keys are too wide to fit on the screen
            float scale = Math.Min(1f, GameBase.WindowWidthScaled / totalWidth);

            totalWidth *= scale;
            delta *= scale;
            float start = (GameBase.WindowWidthScaled - totalWidth) / 2;

            arrow = new pSprite(SkinManager.Load(@"play-warningarrow", SkinSource.Osu), Fields.TopLeft, Origins.CentreLeft, Clocks.Game, new Vector2(0, 100), 0.92f, true, GameBase.NewGraphicsAvailable ? Color.SkyBlue : Color.White);
            arrow.Tag = this;
            arrow.Rotation = OsuMathHelper.PiOver2;
            spriteManager.Add(arrow);

            keys = new pText[currentKeyConfig];
            int layoutIndex = 0;
            for (int i = 0; i < currentKeyConfig; i++)
            {
                bool skip = false;
                if (cBindAlternateKeys.Checked)
                {
                    skip = true;
                    foreach (StageMania s in stage)
                    {
                        if (i == s.Columns[s.SpecialColumn])
                            skip = false;
                    }
                }

                if (!skip)
                {
                    pSprite p = new pSprite(SkinManager.Load(@"mania-key1D", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(start, 192), 0.92f, true, stage.Columns[i].Colour);
                    p.VectorScale = 1.3f * new Vector2(scale, 1f);
                    p.Tag = this;
                    spriteManager.Add(p);

                    keys[i] = new pText(getKeyText(i), Math.Max(1, (int)Math.Round(14 * scale)), new Vector2(start + delta / 2f, 192 + 40), 0.93f, true, Color.White);
                    keys[i].TextBorder = true;
                    keys[i].Origin = Origins.Centre;
                    keys[i].Tag = this;
                    keys[i].TagNumeric = layoutIndex;
                    spriteManager.Add(keys[i]);

                    //Found the first non-skipped key
                    if (layoutIndex++ == 0)
                    {
                        currentBindingColumn = i;
                        arrow.Position.X = start + delta / 2;
                    }
                }

                start += delta + delta * (i == stage.PrimaryStageColumns - 1 ? separation : 0);
            }
        }

        private void ProcessKey(Keys key)
        {
            LayoutListMania ll = stage.LayoutList;
            if (ll.Selected < 0)
            {
                ll.Insert(0);
                ll.Selected = 0;
                stage.CopyBindingToLayout(ll.Layout);
            }

            ManiaLayoutAccess access = cBindAlternateKeys.Checked ? ManiaLayoutAccess.AlternateKeys : ManiaLayoutAccess.Keys;
            ll.Layout[keys[currentBindingColumn].TagNumeric, access] = key;
            stage.RebindColumnKeys();
            for (int i = 0; i < currentKeyConfig; i++)
            {
                if (keys[i] != null)
                    keys[i].Text = getKeyText(i);
            }

            while (++currentBindingColumn < currentKeyConfig && keys[currentBindingColumn] == null) { }
            if (currentBindingColumn >= currentKeyConfig)
            {
                SkinManager.SaveSkin();

                if (incrementKeyConfig(ref currentKeyConfig) > StageMania.MAX_COLUMNS)
                    currentKeyConfig = 1;

                dKeyConfig.SetSelected(currentKeyConfig, false);
            }
            else
                arrow.MoveToX(keys[currentBindingColumn].Position.X, 300, EasingTypes.Out);
        }

        internal override bool HandleKey(Keys k)
        {
            if (k == Keys.Escape)
                Close();
            else
                ProcessKey(k);
            return true;
        }

        private void disposeManiaStage()
        {
            if (stage != null)
            {
                stage.Dispose();
                stage = null;
            }
        }

        private string getKeyText(int i)
        {
            return BindingManager.NiceName(cBindAlternateKeys.Checked ? stage.Columns[i].AlternateKey : stage.Columns[i].Key);
        }

        private int incrementKeyConfig(ref int i)
        {
            i++;
            // skip odd key configs above 10
            if (i > 10 && i % 2 != 0)
                i++;
            return i;
        }

        private string getKeyOptionText(int i, bool coop)
        {
            return coop && i % 2 == 0 ?
                i / 2 + @"K Co-op" :
                i + @"K";
        }
    }
}