using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;

namespace osu.GameModes.Options
{
    internal class OptionSkinPreview : OptionElement
    {
        float height = 80;

        private List<pSprite> previewSprites = new List<pSprite>();

        int currentGroup = 0;
        private string[][] previewGroups = {
            new string[] {@"hitcircle(0,78,155)|hitcircleoverlay|default-1", @"hit50", @"hit100", @"hit300"},
            new string[] {@"hit0", @"hit100k", @"hit300g"},
            new string[] {@"default-0", @"default-1", @"default-2", @"default-3", @"default-4", @"default-5"},
            new string[] {@"score-0", @"score-1", @"score-2", @"score-3", @"score-4", @"score-5"}
        };

        public override void Dispose()
        {
            SkinManager.OnSkinChanged -= SkinManager_OnSkinChanged;
            base.Dispose();
        }

        public OptionSkinPreview()
        {
            pSprite ps = new pSprite(null, Vector2.Zero);
            ps.HandleInput = true;
            ps.OnClick += delegate { Cycle(); };
            spriteManager.Add(ps);

            Reload();

            Size = new Vector2(OptionElement.ELEMENT_SIZE, height);

            SkinManager.OnSkinChanged += SkinManager_OnSkinChanged;
        }

        void SkinManager_OnSkinChanged()
        {
            Reload();
        }

        private void Cycle()
        {
            currentGroup = (currentGroup + 1) % previewGroups.Length;

            Reload();
        }

        private void Reload()
        {
            foreach (pSprite p in previewSprites)
            {
                p.FadeOut(100);
                p.AlwaysDraw = false;
            }

            previewSprites.Clear();

            int elementCount = previewGroups[currentGroup].Length;
            for (int i = 0; i < elementCount; i++)
            {
                float groupScale = 1;

                List<pSprite> group = new List<pSprite>();
                float currentDepth = 0;
                foreach (string element in previewGroups[currentGroup][i].Split('|'))
                {
                    Color colour = Color.White;
                    string baseName = element;
                    if (baseName.IndexOf('(') > 0)
                    {
                        //read colour
                        int index = baseName.IndexOf('(');
                        baseName = baseName.Remove(index);

                        string[] parts = element.Substring(index + 1).TrimEnd(')').Split(',');
                        colour = new Color(byte.Parse(parts[0]),byte.Parse(parts[1]), byte.Parse(parts[2]));
                    }

                    pSprite p = new pSprite(baseName, new Vector2((i + 1) * OptionElement.ELEMENT_SIZE / (elementCount + 1), height / 2), SkinSource.Skin | SkinSource.Osu)
                    {
                        Depth = currentDepth++ / 100f,
                        InitialColour = colour
                    };

                    groupScale = Math.Min(groupScale, Math.Min((OptionElement.ELEMENT_SIZE / (elementCount + 1)) / (p.DrawWidth / 1.6f), (height) / (p.DrawHeight / 1.6f)));
                    group.Add(p);
                    p.FadeInFromZero(50);
                }

                group.ForEach(p => p.Scale = groupScale);
                previewSprites.AddRange(group);
                spriteManager.Add(group);
            }
        }
    }
}