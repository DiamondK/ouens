using Microsoft.Xna.Framework;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;

namespace osu.GameModes.Options
{
    internal class OptionSpinner : OptionElement
    {
        pSprite spinner;
        public OptionSpinner()
        {
            Size = new Vector2(ELEMENT_SIZE, 30);

            spinner = pStatusDialog.CreateSpinner(SkinManager.RandomDefaultColour());
            spinner.Field = Fields.TopLeft;
            spinner.VectorScale = new Vector2(0.5f, 0.5f);
            spinner.Position = new Vector2(Size.X / 2, Size.Y / 2);

            spriteManager.Add(spinner);
        }

        public override void Dispose()
        {
            base.Dispose();
            spinner.Dispose();
        }
    }
}