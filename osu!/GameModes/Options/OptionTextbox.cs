using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Helpers;
using osu_common.Helpers;

namespace osu.GameModes.Options
{
    internal class OptionTextbox : OptionElement
    {
        internal pTextBox Textbox;
        Bindable<string> Content;
        private pText header;

        public OptionTextbox(string title, bool passwordBox = false)
        {
            header = new pText(title, 12, Vector2.Zero, 1, true, Color.White);
            spriteManager.Add(header);

            Textbox = new pTextBox(string.Empty, 12, new Vector2(0, header.MeasureText().Y + 5), ELEMENT_SIZE - 5, 1, passwordBox);
            Textbox.OnChange += delegate { calculateHeight(); };
            Textbox.OnCommit += delegate { calculateHeight(); };
            spriteManager.Add(Textbox.SpriteCollection);

            addKeyword(title);

            calculateHeight();
        }

        public OptionTextbox(string title, Bindable<string> content, bool passwordBox = false)
            : this(title, passwordBox)
        {
            Content = content;
            Content.ValueChanged += contentChange;

            Textbox.Text = Content.Value;
            Textbox.OnCommit += delegate { content.Value = Textbox.Text; };
        }

        public OptionTextbox(OsuString title, Bindable<string> content, bool passwordBox = false)
            : this(LocalisationManager.GetString(title), content, passwordBox)
        {
            addKeyword(title.ToString());
        }

        public OptionTextbox(OsuString title, bool passwordBox = false)
            : this(LocalisationManager.GetString(title), passwordBox)
        {
            addKeyword(title.ToString());
        }

        private void contentChange(object sender, EventArgs e)
        {
            Textbox.Text = Content.Value;
        }

        private void calculateHeight()
        {
            float newSize = Textbox.Box.Position.Y + Textbox.Box.MeasureText().Y;
            Size = new Vector2(Size.X, newSize);
        }

        internal void Focus()
        {
            Textbox.Focus(true);
        }

        internal void UnFocus()
        {
            Textbox.UnFocus(true);
        }

        public override void Dispose()
        {
            base.Dispose();
            Textbox.Dispose();
        }
    }
}