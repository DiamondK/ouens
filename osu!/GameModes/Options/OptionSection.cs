using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Sprites;
using osu_common.Helpers;

namespace osu.GameModes.Options
{
    internal class OptionSection : OptionElement
    {
        private pSprite line;
        public pText Header;

        public OptionSection(OsuString title)
            : this(LocalisationManager.GetString(title))
        {
            addKeyword(title.ToString());
        }

        public OptionSection(string title)
        {

            Header = new pText(title.ToUpper(), 12, new Vector2(Options.padding, 0), 1, true, Color.White);
            Header.TextBold = true;
            spriteManager.Add(Header);

            Size = new Vector2(0, 14);

            line = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(Options.padding / 2, 0))
            {
                Scale = 1.6f,
                VectorScale = new Vector2(2, 0),
                AlwaysDraw = true,
                Additive = true,
                InitialColour = new Color(255, 255, 255, 40)
            };

            updateLineHeight();

            spriteManager.Add(line);

            addKeyword(title);
        }

        internal override int PaddingBefore
        {
            get
            {
                return Options.padding * 2;
            }
        }

        internal override void Layout()
        {
            base.Layout();
            updateLineHeight();
        }

        private void updateLineHeight()
        {
            float lineHeight = Size.Y + Options.padding / 2;
            if (Children != null)
                foreach (OptionElement e in Children)
                    if (!e.spriteManager.Bypass) lineHeight += e.Size.Y + Options.padding;
            line.VectorScale.Y = lineHeight;
        }

        internal override IEnumerable<OptionElement> Children
        {
            get
            {
                return base.Children;
            }
            set
            {
                base.Children = value;
                updateLineHeight();
            }
        }

        internal override bool IsHeaderElement
        {
            get
            {
                return true;
            }
        }
    }
}