using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Graphics.Notifications;
using osu.Graphics.Renderers;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input;
using osu_common.Helpers;

namespace osu.GameModes.Options
{
    internal class OptionsBindingDialog : pDialog
    {
        pScrollableArea pa;
        SpriteManager bindingManager = new SpriteManager(true);

        Bindings currentlyBinding;
        private pText bindText1;
        private pText bindText2;
        private pText bindTextKey;

        public OptionsBindingDialog()
            : base(LocalisationManager.GetString(OsuString.Options_Input_KeyBindings), false)
        {
            pa = new pScrollableArea(new Rectangle(110, 30, 420, 420), Vector2.Zero, false);
        }

        internal override void Display()
        {
            spriteManager.HandleOverlayInput = true;
            pa.HandleOverlayInput = true;
            BackButton back = new BackButton(delegate
            {
                AudioEngine.PlaySamplePositional(@"menuback");
                Close();
            }, false, true);
            spriteManager.Add(back.SpriteCollection);

            bindTextKey = new pText(string.Empty, 40, new Vector2(GameBase.WindowWidthScaled / 2, 120), 1, true, Color.Orange);
            bindTextKey.TextShadow = true;
            bindTextKey.TextBold = true;
            bindTextKey.TextBounds = new Vector2(1024, 0);
            bindTextKey.TextAlignment = TextAlignment.Centre;
            bindTextKey.Origin = Origins.Centre;
            bindingManager.Add(bindTextKey);

            bindText1 = new pText(LocalisationManager.GetString(OsuString.OptionsBindKey_ChangeBinding), 40, new Vector2(GameBase.WindowWidthScaled / 2, 240), 1, true, Color.White);
            bindText1.TextShadow = true;
            bindText1.TextBounds = new Vector2(1024, 0);
            bindText1.TextAlignment = TextAlignment.Centre;
            bindText1.Origin = Origins.Centre;
            bindingManager.Add(bindText1);

            bindText2 = new pText(LocalisationManager.GetString(OsuString.OptionsBindKey_ConflictsWarning), 30, new Vector2(GameBase.WindowWidthScaled / 2, 360), 1, true, Color.LightYellow);
            bindText2.TextShadow = true;
            bindText2.TextBounds = new Vector2(1024, 0);
            bindText2.TextAlignment = TextAlignment.Centre;
            bindText2.Origin = Origins.Centre;
            bindingManager.Add(bindText2);

            pSprite background = new pSprite(GameBase.WhitePixel, Fields.Native, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0.91F, true, new Color(0, 0, 0, 220));
            background.VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight + GameBase.WindowOffsetY);
            bindingManager.Add(background);

            RefreshDisplay();
            base.Display();
        }

        internal void RefreshDisplay()
        {
            pa.ClearSprites(false);

            const int height = 17;

            int currentHeight = 0;

            bool editor = false;

            for (int i = 1; i < BindingManager.BindingNames.Length; i++)
            {
                string na = BindingManager.BindingNames[i];

                //Check for skip flag
                if (na.StartsWith("__"))
                    continue;

                bool header = na[0] == '_';

                if (header)
                    na = na.Substring(1);


                pText naText = new pText(na, header ? 16 : 14, new Vector2(0, currentHeight),
                                         new Vector2(402, height), header ? 0.97f : 0.98f, true, Color.White, false);
                pa.SpriteManager.Add(naText);

                naText.BackgroundColour = Color.Black;

                if (header)
                {
                    naText.TextColour = Color.YellowGreen;
                    naText.TextAlignment = TextAlignment.Centre;
                }
                else
                {
                    naText.BorderWidth = 2;
                    naText.BorderColour = Color.LightGray;

                    naText.HandleInput = true;
                    naText.OnHover += delegate
                                          {
                                              naText.BackgroundColour = Color.Gray;
                                              naText.TextColour = Color.Black;
                                              naText.TextChanged = true;
                                          };
                    naText.OnHoverLost += delegate
                                              {
                                                  naText.BackgroundColour = Color.Black;
                                                  naText.TextColour = Color.White;
                                                  naText.TextChanged = true;
                                              };

                    string name = na;
                    Bindings b = (Bindings)i;

                    naText.OnClick += delegate { startBinding(b, name); };
                    naText.ClickRequiresConfirmation = true;
                    naText.TextAlignment = TextAlignment.LeftFixed;


                    pText naText2 = new pText(BindingManager.NiceName((Bindings)i), 12, new Vector2(300, currentHeight + 1),
                                              new Vector2(100, height - 2), 1, true, Color.Gold, false);
                    naText2.BackgroundColour = new Color(0, 0, 0, 160);
                    naText2.BorderWidth = 2;
                    naText2.BorderColour = Color.LightGray;
                    naText2.TextAlignment = TextAlignment.Centre;

                    pa.SpriteManager.Add(naText2);
                }

                currentHeight += height;
            }

            pText reset = new pText(LocalisationManager.GetString(OsuString.Options_ResetAllBindings), 14, new Vector2(0, currentHeight),
                                         new Vector2(402, height), 0.98f, true, Color.White, false);

            reset.BorderColour = Color.LightGray;
            reset.BackgroundColour = Color.Black;
            reset.TextColour = Color.White;

            reset.HandleInput = true;
            reset.OnHover += delegate
            {
                reset.BackgroundColour = Color.Red;
                reset.TextChanged = true;
            };
            reset.OnHoverLost += delegate
            {
                reset.BackgroundColour = Color.Black;
                reset.TextChanged = true;
            };

            reset.OnClick += delegate
            {
                //pDialogConfirmation d = new pDialogConfirmation(LocalisationManager.GetString(OsuString.Options_ResetAllBindings_Confirm), delegate
                //{
                    BindingManager.Initialize(true);
                    RefreshDisplay();
                    pa.ScrollToPosition(0, -0.99f);
                //}, null);
                //GameBase.ShowDialog(d);
            };
            reset.TextAlignment = TextAlignment.Centre;

            currentHeight += height;

            pa.SpriteManager.Add(reset);

            pa.SetContentDimensions(new Vector2(510, currentHeight));
        }

        private void startBinding(Bindings b, string name)
        {
            bindTextKey.Text = name;

            bindText1.FadeIn(200);
            bindText2.FadeIn(200);

            currentlyBinding = b;
        }

        private bool completeBinding(Keys k)
        {
            if (currentlyBinding == Bindings.None)
                return false;

            BindingManager.Set(currentlyBinding, k);
            currentlyBinding = Bindings.None;

            RefreshDisplay();

            return true;
        }

        internal override bool HandleKey(Keys k)
        {
            if (currentlyBinding != Bindings.None && k == Keys.Escape)
            {
                currentlyBinding = Bindings.None;
                return true;
            }

            if (base.HandleKey(k))
                return true;

            return completeBinding(k);
        }

        internal override void Draw()
        {
            base.Draw();
            pa.Draw();

            if (currentlyBinding != Bindings.None)
                bindingManager.Draw();
        }

        public override void Update()
        {
            base.Update();
            pa.Update();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            bindingManager.Dispose();
            pa.Dispose();
        }
    }
}
