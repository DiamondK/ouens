﻿using osu.Audio;
using osu.Graphics.Sprites;

namespace osu.GameModes.Edit.Modes
{
    internal partial class EditorModeDesign
    {
        private bool clickUpPass;

        private int AddKeyframe(int time, out Transformation trans, bool force)
        {
            editor.Dirty = true;

            int index = -1;
            Transformation found = null;

            trans = null;

            if (selectedEvent == null) return index;

            pSprite p = selectedEvent.Sprite;

            for (int i = selectedEventTransformations.Count - 1; i >= 0; i--)
            {
                Transformation t = selectedEventTransformations[i];

                if (t.Time2 == time)
                {
                    found = t;
                    index = p.Transformations.IndexOf(t);
                    break;
                }

                if (t.Time1 == time)
                {
                    found = t;
                    index = p.Transformations.IndexOf(t);
                    break;
                }

                if (t.Time1 == t.Time2)
                {
                    if (t.Time2 > time)
                        t.Time1 = time;
                    else
                        t.Time2 = time;

                    found = t;
                    index = p.Transformations.IndexOf(t);
                    break;
                }

                //First transformation
                if (t.Time1 > time && i == 0)
                {
                    //Add a new transformation before the existing one.
                    found = new Transformation
                                {
                                    Time1 = time,
                                    Time2 = t.Time1,
                                    Type = currentTransformType,
                                    EndVector = t.StartVector,
                                    EndFloat = t.StartFloat,
                                    EndColour = t.StartColour
                                };


                    index = p.Transformations.IndexOf(t);
                    p.Transformations.Insert(index, found);
                    break;
                }

                //Last transformation
                if (i == selectedEventTransformations.Count - 1 && t.Time2 < AudioEngine.Time)
                {
                    //Add a new transformation after the existing one.
                    found = new Transformation
                                {
                                    Time1 = t.Time2,
                                    Time2 = time,
                                    Type = currentTransformType,
                                    StartVector = t.EndVector,
                                    StartFloat = t.EndFloat,
                                    StartColour = t.EndColour
                                };

                    index = p.Transformations.IndexOf(t) + 1;
                    p.Transformations.Insert(index, found);
                    break;
                }

                if (t.Time2 > time)
                {
                    if (t.Time1 < time)
                    {
                        //Need to split the current transformation in to two.
                        found = new Transformation
                                    {
                                        Time1 = time,
                                        Time2 = t.Time2,
                                        Type = currentTransformType,
                                        EndVector = t.EndVector,
                                        EndFloat = t.EndFloat,
                                        EndColour = t.EndColour
                                    };

                        t.Time2 = time;

                        index = p.Transformations.IndexOf(t) + 1;

                        p.Transformations.Insert(index, found);
                        break;
                    }

                    //Check the transformation before the current one
                    if (selectedEventTransformations[i - 1].Time2 < time)
                    {
                        index = p.Transformations.IndexOf(t);

                        //need to place two new transformations to make the new keyframe.

                        found = new Transformation
                                    {
                                        Time1 = time,
                                        Time2 = t.Time1,
                                        Type = currentTransformType,
                                        EndVector = t.StartVector,
                                        EndFloat = t.StartFloat,
                                        EndColour = t.StartColour
                                    };
                        p.Transformations.Insert(index, found);

                        found = new Transformation
                                    {
                                        Time1 = selectedEventTransformations[i - 1].Time2,
                                        Time2 = time,
                                        Type = currentTransformType,
                                        StartVector = selectedEventTransformations[i - 1].EndVector,
                                        StartFloat = selectedEventTransformations[i - 1].EndFloat,
                                        StartColour = selectedEventTransformations[i - 1].EndColour
                                    };
                        p.Transformations.Insert(index, found);

                        break;
                    }
                }
            }

            if (found == null)
            {
                if (!force) return -1;

                found = new Transformation
                            {
                                Time1 = time,
                                Time2 = time,
                                Type = currentTransformType,
                            };

                p.Transformations.Insert(0, found);
            }

            //Did we match on start or endtime for the transformation?
            bool matchedOnTime1 = found.Time1 == AudioEngine.Time;
            bool matchedOnTime2 = found.Time2 == AudioEngine.Time;

            //We found a matching transformation (time1 or time2 == currentTime)

            if (matchedOnTime1 && matchedOnTime2)
            {
                SetStartTransformation(found);
                SetEndTransformation(found);
            }
            else if (matchedOnTime2)
            {
                SetEndTransformation(found);

                //Check next transformation..
                Transformation next = selectedEventTransformations.Find(t => t.Time1 == found.Time2);
                if (next != null)
                    SetStartTransformation(next);
            }
            else if (matchedOnTime1)
            {
                SetStartTransformation(found);

                //Check previous transformation..
                Transformation next = selectedEventTransformations.Find(t => t.Time2 == found.Time1);
                if (next != null)
                    SetEndTransformation(next);
            }

            trans = found;
            return index;
        }

        private void RemoveKeyframe(int time)
        {
            if (selectedEvent == null) return;

            editor.UndoPush();

            int part1index = -1;
            int part2index = -1;

            for (int i = 0; i < selectedEventTransformations.Count; i++)
            {
                Transformation t = selectedEventTransformations[i];

                if (t.Time1 == time && t.Time2 == time)
                {
                    //This is the first transformation found and has no tween coming into it.
                    //Can safely remove.
                    selectedEvent.Sprite.Transformations.Remove(t);
                    return;
                    //break;
                }

                if (t.Time1 == time)
                {
                    part2index = i;
                    break;
                }

                if (t.Time2 == time)
                    part1index = i;
            }

            if (part1index >= 0 && part2index >= 0)
            {
                //join tween together
                Transformation p2 = selectedEventTransformations[part2index];
                Transformation p1 = selectedEventTransformations[part1index];

                selectedEvent.Sprite.Transformations.Remove(p2);
                selectedEvent.Sprite.Transformations.Remove(p1);

                Transformation joined = new Transformation();
                joined.Type = p1.Type;
                joined.EndVector = p2.EndVector;
                joined.EndFloat = p2.EndFloat;
                joined.EndColour = p2.EndColour;
                joined.StartVector = p1.StartVector;
                joined.StartFloat = p1.StartFloat;
                joined.StartColour = p1.StartColour;
                joined.Time1 = p1.Time1;
                joined.Time2 = p2.Time2;

                selectedEvent.Sprite.Transformations.Insert(part1index, joined);
                return;
            }

            if (part1index >= 0)
            {
                Transformation p1 = selectedEventTransformations[part1index];
                p1.Time2 = p1.Time1;
                p1.EndVector = p1.StartVector;
                p1.EndFloat = p1.StartFloat;
                p1.EndColour = p1.StartColour;
            }

            if (part2index >= 0)
            {
                Transformation p2 = selectedEventTransformations[part2index];
                p2.Time1 = p2.Time2;
                p2.StartVector = p2.EndVector;
                p2.StartFloat = p2.EndFloat;
                p2.StartColour = p2.EndColour;
            }
        }

        private void PrevKeyframe()
        {
            if (selectedEvent == null) return;

            Transformation f = selectedEventTransformations.FindLast(t => t.Time1 < AudioEngine.Time || t.Time2 < AudioEngine.Time);
            if (f == null) return;
            if (f.Time2 < AudioEngine.Time) AudioEngine.SeekTo(f.Time2);
            else if (f.Time1 < AudioEngine.Time) AudioEngine.SeekTo(f.Time1);
        }

        private void NextKeyframe()
        {
            if (selectedEvent == null) return;

            Transformation f = selectedEventTransformations.Find(t => t.Time1 > AudioEngine.Time || t.Time2 > AudioEngine.Time);
            if (f == null) return;
            if (f.Time1 > AudioEngine.Time) AudioEngine.SeekTo(f.Time1);
            else if (f.Time2 > AudioEngine.Time) AudioEngine.SeekTo(f.Time2);
        }

        private void SetTweening(bool enabled)
        {
            if (selectedEvent == null) return;

            editor.UndoPush();

            if (!enabled)
            {
                checkTweenEaseIn.SetStatusQuietly(false);
                checkTweenEaseOut.SetStatusQuietly(false);
            }

            if (enabled)
            {
                int index =
                    selectedEventTransformations.FindIndex(
                        t => t.Time1 <= AudioEngine.Time && t.Time2 >= AudioEngine.Time);

                if (index >= 0) return;

                index = selectedEventTransformations.FindLastIndex(t => t.Time2 < AudioEngine.Time);

                if (index < 0) return;

                Transformation tr = selectedEventTransformations[index];

                Transformation tween = null;

                int idx = -1;

                if (tr.Time1 == tr.Time2)
                {
                    //remove the existing non-tween.
                    idx = selectedEvent.Sprite.Transformations.IndexOf(tr);
                    tween = tr;
                }
                else
                {
                    tween = tr.Clone();
                    tween.Time1 = tween.Time2;
                    tween.StartFloat = tween.EndFloat;
                    tween.StartVector = tween.EndVector;
                    tween.StartColour = tween.EndColour;
                }

                //We have our starting point for the tween - now we should get the other end.

                index = selectedEventTransformations.FindIndex(t => t.Time1 > AudioEngine.Time);

                if (index < 0) return;

                if (idx >= 0) //delete the first static transform if necessary (after checking the last exit condition)
                    selectedEvent.Sprite.Transformations.RemoveAt(idx);

                tr = selectedEventTransformations[index];

                if (tr.Time1 == tr.Time2)
                {
                    //remove the existing non-tween.
                    idx = selectedEvent.Sprite.Transformations.IndexOf(tr);
                    selectedEvent.Sprite.Transformations.RemoveAt(idx);
                }

                tween.EndFloat = tr.StartFloat;
                tween.EndVector = tr.StartVector;
                tween.EndColour = tr.StartColour;
                tween.Time2 = tr.Time1;

                if (idx >= 0)
                    selectedEvent.Sprite.Transformations.Insert(idx, tween);
                else
                    selectedEvent.Sprite.Transformations.Insert(index + 1, tween);
            }
            else
            {
                int index = selectedEventTransformations.FindIndex(t => t.Time1 <= AudioEngine.Time && t.Time2 >= AudioEngine.Time);

                if (index >= 0)
                {
                    Transformation t = selectedEventTransformations[index];

                    int idx = selectedEvent.Sprite.Transformations.IndexOf(t);
                    selectedEvent.Sprite.Transformations.RemoveAt(idx);


                    if (index == selectedEventTransformations.Count - 1 || //doesn't collide with last transformation
                        selectedEventTransformations[index + 1].Time1 != t.Time2) //doesn't collide with next transformation
                    {
                        Transformation nt = t.Clone();
                        nt.Time1 = nt.Time2;
                        nt.StartFloat = nt.EndFloat;
                        nt.StartVector = nt.EndVector;
                        nt.StartColour = nt.EndColour;

                        selectedEvent.Sprite.Transformations.Insert(idx, nt);
                    }

                    //Decide whether we need to split the transformation into two parts or not...
                    if (index == 0 || //is first transformation
                        selectedEventTransformations[index - 1].Time2 != t.Time1)
                    {
                        Transformation nt = t.Clone();
                        nt.Time2 = nt.Time1;
                        nt.EndFloat = nt.StartFloat;
                        nt.EndVector = nt.StartVector;
                        nt.EndColour = nt.StartColour;

                        selectedEvent.Sprite.Transformations.Insert(idx, nt);
                    }



                }
            }
        }

        private void SetEasing(EasingTypes easing)
        {
            if (selectedEvent == null) return;

            editor.UndoPush();

            currentEasing = easing;
            checkTweenEaseIn.SetStatusQuietly(currentEasing == EasingTypes.In);
            checkTweenEaseOut.SetStatusQuietly(currentEasing == EasingTypes.Out);

            if (!checkTweenEnable.Checked && currentEasing != EasingTypes.None)
                checkTweenEnable.Checked = true; //Force enable tweening.

            //enable easing.

            Transformation tween = selectedEventTransformations.Find(t => t.Time1 <= AudioEngine.Time && t.Time2 >= AudioEngine.Time);
            if (tween != null)
                tween.Easing = currentEasing;

        }
    }
}