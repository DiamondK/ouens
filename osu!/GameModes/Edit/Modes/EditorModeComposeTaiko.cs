﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
using osu.Audio;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Mania;
using osu.GameplayElements.HitObjects.Osu;
using osu.Graphics.Notifications;
using osu.Graphics.Primitives;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;
using osu_common;
using osu.GameplayElements.Events;
using osu.GameModes.Edit.Forms;
using osu.Graphics;

namespace osu.GameModes.Edit.Modes
{
    class EditorModeComposeTaiko : EditorModeCompose
    {
        public EditorModeComposeTaiko(Editor e) : base(e)
        {

        }
        internal override void Update()
        {
            base.Update();

            List<HitCircle> timelineCircles = new List<HitCircle>();
            List<Slider> timelineSliders = new List<Slider>();

            editor.timelineManager.SpriteList.ForEach(delegate (pDrawable p)
            {
                HitCircle c = p.Tag as HitCircle;
                Slider sl;
                if (c != null) timelineCircles.Add(c);
                else if ((sl = p.Tag as Slider) != null) timelineSliders.Add(sl);
            });

            foreach (HitObject h in timelineCircles)
            {
                if (h.Whistle || h.Clap)
                    h.SetColour(new Color(67, 142, 172));
                else
                    h.SetColour(new Color(235, 69, 44));
            }
            foreach (Slider sl in timelineSliders)
            {
                sl.SetColour(new Color(252, 184, 6));
            }
        }
    }
}
