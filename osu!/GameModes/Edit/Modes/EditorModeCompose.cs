using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameplayElements;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu.Graphics.Notifications;
using osu.Graphics.Primitives;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input;
using osu.Input.Handlers;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using osu.Helpers;
using osu.Online;
using osu.GameplayElements.Beatmaps;
using osu_common;
using osu_common.Helpers;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameModes.Edit.Forms;
using osu.Graphics;
using osu.Online.Social;
using System.Diagnostics;

namespace osu.GameModes.Edit.Modes
{
    internal class EditorModeCompose : EditorMode
    {
        private static readonly Color fillColour = new Color(255, 255, 255, 150);
        private static readonly Color fillColour2 = new Color(0, 0, 0, 100);
        private static readonly Color fillColour3 = new Color(255, 255, 255, 255);
        private static readonly Color fillColour4 = new Color(255, 40, 40, 255);
        private CurveTypes SliderCurveType = CurveTypes.PerfectCurve;
        protected readonly Color colourDragBoxFill = new Color(255, 255, 255, 20);
        private readonly Color colourDragBoxTimeline = new Color(255, 255, 255, 50);

        protected bool undoKeysPressed = false;
        protected bool applyGridSnap;
        internal pSliderBar beatDivisorSlider;
        private pText beatDivisorText;
        private pSprite[] composeToolSprites;
        protected List<HitObject> copiedObjects;
        protected ComposeTools CurrentComposeTool;
        internal HitObject currentPlacementObject;
        private double distanceSpacingFactor;
        private bool distSnapOverride;
        protected Vector2 DragPoint1;
        protected Vector2 DragPoint2;
        internal bool dragSelection;
        private Vector2 dragStartPosition;
        private int dragStartTime;
        private bool gridSnapOverride;
        private Event hoveredEvent;
        protected HitObject hoveredObject;
        private bool hoveredResizeMode;
        private SliderOsu hoveredSlider;
        protected bool IsDragModified;
        private Vector2 mouseGamefieldVector;
        private bool dragBreakEnd;
        private EventBreak dragBreak;
        internal bool ignoreDrag = false;
        internal bool resetDrag = false;
        internal bool ignoreDragReset = false;
        internal bool ignoreDragSelect = false;

        internal List<HitObject> selectedObjects;
        internal SliderOsu selectedSlider;
        internal int selectedSliderIndex;
        protected bool selectionChanged;
        private pText sliderBarTitle;
        private bool allowPlacement = true;
        internal SliderOsu sliderPlacementCircle;
        private Vector2 sliderPlacementPoint;
        private bool SnapDivisorCheckWarning;

        private pSprite[] soundAdditionSprites;
        internal static Dictionary<SoundAdditions, bool> SoundAdditionStatus;
        private SpinnerOsu spinnerPlacementCircle;

        private int timelineDrag1;
        private int timelineDrag2;
        private pText warningMessage;
        private Vector2 snapCoordinates;
        private Line snapLine;
        private bool stackSnapTriggered = false;
        private bool ignoreAutoStack = false;
        private Vector2 selectionDragOrigin;

        private pSprite comboColourExpand;
        private pSpriteMenu comboColour;

        private pText txtSampleSet;
        private pText txtSampleSetAddition;

        protected pDropdown ddSampleSet;
        protected pDropdown ddSampleSetAddition;

        protected pText coordinateText;
        protected CoordinateForm coordinateForm;

        protected HitObject sampleNote;
        private bool isTaiko = false;

        internal HitObjectManager ReferenceHitObjectManager;

        internal override bool Enabled
        {
            get { return base.Enabled; }
            set
            {
                base.Enabled = value;
                hitObjectManager.spriteManager.HandleInput = value;
                if (ReferenceHitObjectManager != null)
                    ReferenceHitObjectManager.spriteManager.HandleInput = value;
                ChangeComposeTool(ComposeTools.Select);
            }
        }

        public EditorModeCompose(Editor editor)
            : base(editor)
        {
            selectedObjects = new List<HitObject>();
            copiedObjects = new List<HitObject>();
            isTaiko = BeatmapManager.Current.PlayMode == PlayModes.Taiko;
            KeyboardHandler.OnKeyReleased += KeyboardHandler_OnKeyReleased;
        }

        internal override bool IgnoreInterfaceClick
        {
            get { return sliderPlacementCircle != null || selectedSliderIndex >= 0 || KeyboardHandler.ControlPressed && selectedSlider != null; }
        }

        private HitObjectSoundType soundAdditionSoundType
        {
            get
            {
                HitObjectSoundType t = HitObjectSoundType.None;
                if (SoundAdditionStatus[SoundAdditions.Whistle])
                    t |= HitObjectSoundType.Whistle;
                if (SoundAdditionStatus[SoundAdditions.Finish])
                    t |= HitObjectSoundType.Finish;
                if (SoundAdditionStatus[SoundAdditions.Clap])
                    t |= HitObjectSoundType.Clap;
                return t;
            }
        }

        protected override void InitializeSettings()
        {
            if (hitObjectManager.hitObjects.Count > 0)
                ChangeComposeTool(ComposeTools.Select);
            else
                ChangeComposeTool(ComposeTools.Normal);

            ShowSoundAddition(SoundAdditions.NewCombo | SoundAdditions.Whistle | SoundAdditions.Finish |
                              SoundAdditions.Clap |
                              SoundAdditions.GridSnap | SoundAdditions.DistSnap | SoundAdditions.NoteLock);

            ChangeSnapDivisor(ConfigManager.sEditorBeatDivisor);
            ChangeAltDistance(ConfigManager.sDistanceSpacing);
        }

        internal override bool OnMouseWheelDown()
        {
            if (KeyboardHandler.ControlPressed && KeyboardHandler.AltPressed)
            {
                ChangeComposeTool((ComposeTools)Math.Min(3, (int)CurrentComposeTool + 1));
                return true;
            }

            if (KeyboardHandler.AltPressed)
            {
                ChangeAltDistance(OsuMathHelper.Clamp((float)distanceSpacingFactor - (KeyboardHandler.ShiftPressed ? 0.01f : 0.1F), 0, 2));
                return true;
            }

            if (KeyboardHandler.ControlPressed)
            {
                ChangeSnapDivisor(Math.Max(1, editor.beatSnapDivisor / 2));
                return true;
            }

            return false;
        }

        internal override bool OnMouseWheelUp()
        {
            if (KeyboardHandler.ControlPressed && KeyboardHandler.AltPressed)
            {
                ChangeComposeTool((ComposeTools)Math.Max(0.1F, (int)CurrentComposeTool - 1));
                return true;
            }

            if (KeyboardHandler.AltPressed)
            {
                ChangeAltDistance(OsuMathHelper.Clamp((float)distanceSpacingFactor + (KeyboardHandler.ShiftPressed ? 0.01f : 0.1F), 0, 2));
                return true;
            }

            if (KeyboardHandler.ControlPressed)
            {
                ChangeSnapDivisor(Math.Min(16, editor.beatSnapDivisor * 2));
                return true;
            }

            return false;
        }

        bool IsDragUndoPushed;

        private EventBreak BreakToDrag(out bool drag_end)
        {
            drag_end = false;

            EventBreak b = hoveredEvent as EventBreak;
            if (b == null) return null; // Hovered event is not a break (currently impossible)

            int x = editor.TimelinePosToTime((int)MouseManager.MousePosition.X - 8);
            if (x <= b.StartTime) return b; // Hovered within 8px of the break's start

            x = editor.TimelinePosToTime((int)MouseManager.MousePosition.X + 8);
            if (x >= b.EndTime)
            {
                drag_end = true;
                return b; // Hovered within 8px of the break's end
            }
            else
                return null; // Hovered somewhere within the break
        }

        internal override bool OnDragStart()
        {
            if (ignoreDrag) return true;

            ignoreDragReset = true;

            IsDragModified = false;
            IsDragUndoPushed = false;
            dragBreak = null;

            DragPoint1 = DragPoint2 = InputManager.CursorPosition;

            if (editor.InTimeline && selectedSliderIndex < 0)
            {
                editor.DragCaptured = DragCapture.Timeline;

                dragBreak = BreakToDrag(out dragBreakEnd);
                if (hoveredObject != null)
                {
                    editor.UndoPush(true);
                    IsDragUndoPushed = true;

                    dragStartTime = editor.TimelinePosToTime(InputManager.CursorPoint.X) - hoveredObject.StartTime;
                }
                else if (dragBreak != null)
                {
                    editor.UndoPush(true);
                    IsDragUndoPushed = true;

                    dragStartTime = editor.TimelinePosToTime(InputManager.CursorPoint.X) - (dragBreakEnd ? dragBreak.EndTime : dragBreak.StartTime);
                }
                else
                {
                    timelineDrag1 = timelineDrag2 = editor.TimelinePosToTime(InputManager.CursorPoint.X);
                    dragSelection = true;
                    SelectNone();
                }

                ignoreDragReset = false;
                return true;
            }
            else if (selectedSliderIndex >= 0 || hoveredObject != null || (editor.InGamefield && !editor.InSeekbar))
            {
                editor.DragCaptured = DragCapture.Gamefield;

                dragSelection = true;
                if (sliderPlacementCircle != null)
                {
                    dragSelection = false;
                }
                else if (selectedSliderIndex >= 0)
                {
                    dragSelection = false;

                    editor.UndoPush(true);
                    IsDragUndoPushed = true;
                }
                else if (spinnerPlacementCircle == null)
                {
                    dragSelection = hoveredObject == null || hoveredObject.IsType(HitObjectType.Spinner);

                    if (SoundAdditionStatus[SoundAdditions.NoteLock])
                        return true;

                    if (hoveredObject != null && !hoveredObject.IsType(HitObjectType.Spinner) &&
                        hoveredObject.Selected)
                    {
                        if (selectedObjects.Find(h => !h.IsType(HitObjectType.Spinner)) != null)
                        {
                            dragStartPosition = GameBase.DisplayToGamefield(InputManager.CursorPosition) - hoveredObject.Position;

                            editor.UndoPush(true);
                            IsDragUndoPushed = true;
                        }
                    }
                }

                ignoreDragReset = false;
                return true;
            }

            ignoreDragReset = false;
            return false;
        }

        internal override void OnDragEnd()
        {
            ignoreDragReset = true;

            dragSelection = false;
            dragStartPosition = Vector2.Zero;
            dragStartTime = editor.TimelinePosToTime(InputManager.CursorPoint.X);

            selectedSliderIndex = -1;

            if (IsDragModified)
            {
                editor.BreakTimeFix(true);

                if (editor.DragCaptured == DragCapture.Gamefield)
                {
                    hitObjectManager.Sort(true);

                    if (selectedSlider != null && selectedSlider.sliderEndCircles.Count > 0)
                        selectedSlider.sliderEndCircles[0].Select();
                }
            }
            else
            {
                if (IsDragUndoPushed)
                    editor.UndoPop(true);
            }

            dragBreak = null;
            if (!resetDrag)
                ignoreDrag = false;
            ignoreDragReset = false;

            base.OnDragEnd();

            return;
        }

        protected override void InitializeSprites()
        {
            beatDivisorSlider =
                new pSliderBar(spriteManager, 1, 16, 1, new Vector2(GameBase.WindowWidthScaled - 190, 25), 140);

            sliderBarTitle =
                new pText(LocalisationManager.GetString(OsuString.EditorModeCompose_BeatSnapDivisor) + @":", 14, new Vector2(GameBase.WindowWidthScaled - 200, 0), 1, true, Color.White);
            sliderBarTitle.Tag = EditorModes.Compose;
            spriteManager.Add(sliderBarTitle);

            beatDivisorText =
                new pText(string.Empty, 14, new Vector2(GameBase.WindowWidthScaled - 40, 15), 1, true,
                          Color.White);
            beatDivisorText.Tag = EditorModes.Compose;
            spriteManager.Add(beatDivisorText);

            pButton p = new pButton(LocalisationManager.GetString(OsuString.EditorModeCompose_InsertBreakTime), new Vector2(GameBase.WindowWidthScaled - 190, 40), new Vector2(130, 22.5f), 1, new Color(112, 152, 252), insertBreakTime_OnClick);
            p.OnClick += insertBreakTime_OnClick;
            p.ToolTip =
                LocalisationManager.GetString(OsuString.EditorModeCompose_InsertBreakTooltip);
            spriteManager.Add(p.SpriteCollection);

            float scale = DpiHelper.DPI(GameBase.Form) / 96.0f;
            warningMessage =
                new pText(string.Empty,
                          10 * scale, new Vector2(70, 120),
                          new Vector2(GameBase.WindowWidth - 140 * GameBase.WindowRatio, GameBase.GamefieldHeight), 1, true,
                          Color.TransparentWhite, true);
            warningMessage.InitialColour = new Color(255, 255, 255, 180);
            warningMessage.ExactCoordinates = true;
            warningMessage.TextRenderSpecific = false;
            warningMessage.TextAlignment = TextAlignment.Centre;
            warningMessage.TextAa = false;
            spriteManager.Add(warningMessage);

            composeToolSprites = new[]
                                     {
                                         new pSprite(SkinManager.Load(@"editor-draw-select", SkinSource.Osu),
                                                     Fields.TopLeft,
                                                     Origins.TopLeft,
                                                     Clocks.Game, new Vector2(0, 140), 0.98f, true,
                                                     Color.TransparentWhite),
                                         new pSprite(SkinManager.Load(@"editor-draw-normal", SkinSource.Osu),
                                                     Fields.TopLeft,
                                                     Origins.TopLeft,
                                                     Clocks.Game, new Vector2(0, 200), 0.98f, true,
                                                     Color.TransparentWhite),
                                         new pSprite(SkinManager.Load(@"editor-draw-slider", SkinSource.Osu),
                                                     Fields.TopLeft,
                                                     Origins.TopLeft,
                                                     Clocks.Game, new Vector2(0, 260), 0.98f, true,
                                                     Color.TransparentWhite),
                                         new pSprite(SkinManager.Load(@"editor-draw-spinner", SkinSource.Osu),
                                                     Fields.TopLeft,
                                                     Origins.TopLeft,
                                                     Clocks.Game, new Vector2(0, 320), 0.98f, true,
                                                     Color.TransparentWhite),
                                        new pSprite(SkinManager.Load(@"editor-draw-hold", SkinSource.Osu),
                                                     Fields.TopLeft,
                                                     Origins.TopLeft,
                                                     Clocks.Game, new Vector2(0, 260), 0.98f, true,
                                                     Color.TransparentWhite)
                                     };

            composeToolSprites[0].ToolTip =
                LocalisationManager.GetString(OsuString.EditorModeCompose_ComposeToolSelectTooltip);
            composeToolSprites[1].ToolTip =
                LocalisationManager.GetString(OsuString.EditorModeCompose_ComposeToolCircleTooltip);
            composeToolSprites[2].ToolTip =
                LocalisationManager.GetString(OsuString.EditorModeCompose_ComposeToolSliderTooltip);
            composeToolSprites[3].ToolTip =
                LocalisationManager.GetString(OsuString.EditorModeCompose_ComposeToolSpinnerTooltip);
            composeToolSprites[4].ToolTip =
                LocalisationManager.GetString(OsuString.EditorModeCompose_ComposeToolHoldTooltip);

            if (BeatmapManager.Current.PlayMode != PlayModes.OsuMania)
                composeToolSprites[4].Bypass = true;
            else
            {
                composeToolSprites[2].Bypass = true;
                composeToolSprites[3].Bypass = true;
            }

            foreach (pSprite c in composeToolSprites)
            {
                c.HandleInput = true;
                c.OnClick += ChangeComposeTool;
                spriteManager.Add(c);
            }

            if (SoundAdditionStatus == null)
            {
                SoundAdditionStatus = new Dictionary<SoundAdditions, bool>();
                SoundAdditionStatus.Add(SoundAdditions.Finish, false);
                SoundAdditionStatus.Add(SoundAdditions.Clap, false);
                SoundAdditionStatus.Add(SoundAdditions.Whistle, false);
                SoundAdditionStatus.Add(SoundAdditions.NewCombo, false);
                SoundAdditionStatus.Add(SoundAdditions.GridSnap, true);
                SoundAdditionStatus.Add(SoundAdditions.DistSnap, ConfigManager.sDistanceSpacingEnabled);
                SoundAdditionStatus.Add(SoundAdditions.NoteLock, false);
            }

            //Initial beatsnap state to menubar.
            GameBase.EditorControl.BeatSnapChanged(AudioEngine.BeatSyncing);

            comboColour = new pSpriteMenu(spriteManager);
            comboColourExpand = new pSprite(SkinManager.Load(@"editor-draw-newcombo-expand", SkinSource.Osu),
                                            Fields.TopRight, Origins.TopLeft,
                                            Clocks.Game, new Vector2(50, 80),
                                            2, true, Color.TransparentWhite, null);
            comboColourExpand.HandleInput = true;
            comboColourExpand.OnClick += ComboExpandClick;
            comboColour.OnBlur += ComboExpandBlur;
            spriteManager.Add(comboColourExpand);

            soundAdditionSprites = new[]
                                       {
                                           new pSprite(
                                               SkinManager.Load(@"editor-draw-newcombo", SkinSource.Osu),
                                               Fields.TopRight, Origins.TopLeft,
                                               Clocks.Game, new Vector2(50, 80),
                                               1, true, Color.TransparentWhite,
                                               SoundAdditions.NewCombo),
                                           new pSprite(
                                               SkinManager.Load(@"editor-sound-whistle", SkinSource.Osu),
                                               Fields.TopRight, Origins.TopLeft,
                                               Clocks.Game, new Vector2(50, 140),
                                               1, true, Color.TransparentWhite,
                                               SoundAdditions.Whistle),
                                           new pSprite(
                                               SkinManager.Load(@"editor-sound-finish", SkinSource.Osu),
                                               Fields.TopRight, Origins.TopLeft,
                                               Clocks.Game, new Vector2(50, 180),
                                               1, true, Color.TransparentWhite,
                                               SoundAdditions.Finish),
                                           new pSprite(
                                               SkinManager.Load(@"editor-sound-clap", SkinSource.Osu),
                                               Fields.TopRight, Origins.TopLeft,
                                               Clocks.Game, new Vector2(50, 220),
                                               1, true, Color.TransparentWhite,
                                               SoundAdditions.Clap),
                                           new pSprite(
                                               SkinManager.Load(@"editor-draw-beatsnap", SkinSource.Osu),
                                               Fields.TopRight, Origins.TopLeft,
                                               Clocks.Game, new Vector2(50, 260),
                                               1, true, Color.TransparentWhite,
                                               SoundAdditions.GridSnap),
                                           new pSprite(
                                               SkinManager.Load(@"editor-draw-distsnap", SkinSource.Osu),
                                               Fields.TopRight, Origins.TopLeft,
                                               Clocks.Game, new Vector2(50, 320),
                                               1, true, Color.TransparentWhite,
                                               SoundAdditions.DistSnap),
                                           new pSprite(
                                               SkinManager.Load(@"editor-draw-lock", SkinSource.Osu),
                                               Fields.TopRight, Origins.TopLeft,
                                               Clocks.Game, new Vector2(50, 380),
                                               1, true, Color.TransparentWhite,
                                               SoundAdditions.NoteLock)
                                       };

            foreach (pSprite c in soundAdditionSprites)
            {
                c.HandleInput = true;
                c.OnClick += ChangeSoundAddition;
                spriteManager.Add(c);
            }

            soundAdditionSprites[0].ToolTip =
                LocalisationManager.GetString(OsuString.EditorModeCompose_NewComboTooltip);
            soundAdditionSprites[1].ToolTip =
                LocalisationManager.GetString(OsuString.EditorModeCompose_WhistleTooltip);
            soundAdditionSprites[2].ToolTip =
                LocalisationManager.GetString(OsuString.EditorModeCompose_FinishTooltip);
            soundAdditionSprites[3].ToolTip =
                LocalisationManager.GetString(OsuString.EditorModeCompose_ClapTooltip);
            soundAdditionSprites[4].ToolTip =
                LocalisationManager.GetString(OsuString.EditorModeCompose_GridSnapTooltip);
            soundAdditionSprites[5].ToolTip =
                LocalisationManager.GetString(OsuString.EditorModeCompose_DistanceSnapTooltip);
            soundAdditionSprites[6].ToolTip =
                LocalisationManager.GetString(OsuString.EditorModeCompose_NoteLockTooltip);

            coordinateText = new pText(string.Empty, 10, new Vector2(GameBase.WindowWidthScaled - 50, 36), Vector2.Zero, 1, true, Color.White, true);
            coordinateText.TextAa = false;
            coordinateText.TextShadow = true;
            coordinateText.HandleInput = true;
            coordinateText.OnClick += coordinateText_OnClick;
            spriteManager.Add(coordinateText);

            // todo: move these guys to the right side
            txtSampleSet = new pText("Sampleset", 10, new Vector2(2, 84), Vector2.Zero, 0.97f, true, Color.White, false);
            spriteManager.Add(txtSampleSet);

            txtSampleSetAddition = new pText("Additions", 10, new Vector2(2, 109), Vector2.Zero, 0.97f, true, Color.White, false);
            spriteManager.Add(txtSampleSetAddition);

            ddSampleSet = new pDropdown(spriteManager, string.Empty, new Vector2(0, 94), 52, 0.98f, false);
            ddSampleSet.AddOption("Auto", SampleSet.None);
            ddSampleSet.AddOption("Normal", SampleSet.Normal);
            ddSampleSet.AddOption("Soft", SampleSet.Soft);
            ddSampleSet.AddOption("Drum", SampleSet.Drum);
            ddSampleSet.SetSelected(SampleSet.None, true);
            ddSampleSet.OnSelect += ObjectSetChange;

            ddSampleSetAddition = new pDropdown(spriteManager, string.Empty, new Vector2(0, 119), 52, 0.98f, false);
            ddSampleSetAddition.AddOption("Auto", SampleSet.None);
            ddSampleSetAddition.AddOption("Normal", SampleSet.Normal);
            ddSampleSetAddition.AddOption("Soft", SampleSet.Soft);
            ddSampleSetAddition.AddOption("Drum", SampleSet.Drum);
            ddSampleSetAddition.SetSelected(SampleSet.None, true);
            ddSampleSetAddition.OnSelect += ObjectAdditionSetChange;

            base.InitializeSprites();
        }

        void coordinateText_OnClick(object sender, EventArgs e)
        {
            if (selectedObjects.Count == 0)
            {
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.EditorModeCompose_SelectNotesMessage), 1000);
                return;
            }
            if (coordinateForm == null)
            {
                coordinateForm = new Forms.CoordinateForm();
                coordinateForm.Show(GameBase.Form);
                coordinateForm = null;
            }
        }

        internal void BreakTimeInsert(int time, bool alertOnFail)
        {
            int startTime = 0;
            int endTime = 0;

            foreach (HitObject h in hitObjectManager.hitObjects)
            {
                if (h.EndTime < time)
                    startTime = Math.Max(startTime, h.EndTime + 200);
                else if (h.StartTime > time)
                {
                    endTime = h.StartTime - hitObjectManager.PreEmpt;
                    break;
                }
            }

            EventBreak match = editor.eventManager.eventBreaks.Find(e => ((e.StartTime >= startTime &&
                                                                                          e.StartTime <= endTime) ||
                                                                                         (e.EndTime >= startTime &&
                                                                                          e.EndTime <= endTime)));

            if (match != null)
                return;

            if (startTime != 0 && endTime != 0 && startTime < endTime)
            {
                if (alertOnFail)
                {
                    editor.UndoPush();
                }
                editor.eventManager.Add(new EventBreak(startTime, endTime));
                editor.Dirty = true;
            }
            else if (alertOnFail)
                NotificationManager.ShowMessageMassive(
                    LocalisationManager.GetString(OsuString.EditorModeCompose_NotEnoughRoomMessage),
                    3000);
        }

        internal void ChangeSnapDivisor(int div)
        {
            SnapDivisorCheckWarning = true;

            div = ClampDivisor(div);

            editor.beatSnapDivisor = div;

            ConfigManager.sEditorBeatDivisor.Value = div;

            GameBase.EditorControl.BeatSnapDivisorChanged(div);
            if (!KeyboardHandler.AltPressed)
            {
                beatDivisorSlider.SetValue(div);
                beatDivisorText.Text = @"1/" + div;
            }
        }

        internal int ClampDivisor(int div)
        {
            if (div > 16)
                div = 1;
            if (div == 5)
                div = 4;
            if (div == 7)
                div = 6;
            if (div >= 9 && div <= 11)
                div = 12;
            if (div > 12 && div < 16)
                div = 16;
            return div;
        }

        internal void ChangeAltDistance(double dist)
        {
            dist = Math.Round(dist, 2);
            distanceSpacingFactor = dist;
            ConfigManager.sDistanceSpacing.Value = dist;

            if (KeyboardHandler.AltPressed)
            {
                beatDivisorSlider.SetValue(dist);
                beatDivisorText.Text = String.Format(@"{0:0.0#}x", distanceSpacingFactor);
            }
        }


        private void insertBreakTime_OnClick(object sender, EventArgs e)
        {
            if (IgnoreInterfaceClick) return;

            BreakTimeInsert(AudioEngine.Time, true);
        }

        private void ChangeSoundAddition(Object sender, EventArgs e)
        {
            if (IgnoreInterfaceClick) return;

            SoundAdditions upd = (SoundAdditions)((pSprite)sender).Tag;
            ChangeSoundAddition(upd);
        }

        internal void ChangeSoundAddition(SoundAdditions add)
        {
            ChangeSoundAddition(add, !SoundAdditionStatus[add], true);
        }

        protected void ChangeSoundAddition(SoundAdditions add, bool value)
        {
            ChangeSoundAddition(add, value, false);
        }

        private void UpdateWarningMessage()
        {
            if (SnapDivisorCheckWarning)
            {
                warningMessage.TextColour = Color.White;
                warningMessage.InitialColour = new Color(255, 255, 255, 180);

                switch (editor.beatSnapDivisor)
                {
                    case 16:
                    case 12:
                        warningMessage.Text =
                            String.Format(LocalisationManager.GetString(OsuString.EditorModeCompose_16WarningMessage), editor.beatSnapDivisor);
                        warningMessage.TextColour = Color.Red;
                        warningMessage.InitialColour = Color.White;
                        warningMessage.FadeIn(500);
                        break;
                    case 8:
                        warningMessage.Text =
                            LocalisationManager.GetString(OsuString.EditorModeCompose_8WarningMessage);
                        warningMessage.FadeOutFromOne(20000);
                        break;
                    case 3:
                        warningMessage.Text =
                            LocalisationManager.GetString(OsuString.EditorModeCompose_3WarningMessage);
                        warningMessage.FadeOutFromOne(6000);
                        break;
                    case 6:
                        warningMessage.Text =
                            LocalisationManager.GetString(OsuString.EditorModeCompose_6WarningMessage);
                        warningMessage.FadeOutFromOne(6000);
                        break;
                    default:
                        warningMessage.FadeOut(100);
                        break;
                }

                SnapDivisorCheckWarning = false;
            }
        }

        private void ComboColourSelect(object sender, EventArgs e)
        {
            if (selectedObjects.Count != 1) return;
            pSpriteMenuItem item = sender as pSpriteMenuItem;
#if !DEBUG
            if (item == null) return;
#endif

            editor.UndoPush();

            selectedObjects[0].NewCombo = true;
            selectedObjects[0].ComboOffset = item.TagNumeric;

            hitObjectManager.Sort(false);

            // Updating the dropdown is necessary to refresh the selected item,
            // but unfortunately costs us fade-out transitions. :(
            UpdateComboDropdown();
        }

        private void ObjectSetChange(object sender, EventArgs e)
        {
            SampleSet ss = (SampleSet)ddSampleSet.SelectedObject;

            if (selectedSlider != null)
            {
                editor.UndoPush();
                bool startPointSelected = selectedSlider.sliderStartCircle.Selected;
                List<HitCircleSliderEnd> selectedEndCircles =
                    selectedSlider.sliderEndCircles.FindAll(c => c.Selected);

                selectedSlider.EnableNodalSamples();

                if (startPointSelected || selectedEndCircles.Count > 0)
                {
                    // Individual node editing.
                    if (startPointSelected)
                        selectedSlider.SampleSetList[0] = ss;

                    for (int i = 0; i < selectedSlider.sliderEndCircles.Count; i++)
                        if (selectedSlider.sliderEndCircles[i].Selected)
                            selectedSlider.SampleSetList[i + 1] = ss;
                }
                else
                {
                    if (!selectedSlider.BodySelected)
                    {
                        // Complete slider editing.
                        selectedSlider.SampleSetList[0] = ss;
                        for (int i = 0; i < selectedSlider.sliderEndCircles.Count; i++)
                            selectedSlider.SampleSetList[i + 1] = ss;
                    }

                    // Slider body editing.
                    selectedSlider.SampleSet = ss;
                }
            }
            else
            {
                if (selectedObjects.Count > 0)
                {
                    editor.UndoPush();
                }
                foreach (HitObject h in selectedObjects)
                {
                    SliderOsu s = h as SliderOsu;
                    if (s != null)
                    {
                        s.EnableNodalSamples();

                        s.SampleSetList[0] = ss;
                        for (int i = 0; i < s.sliderEndCircles.Count; i++)
                            s.SampleSetList[i + 1] = ss;
                    }

                    h.SampleSet = ss;
                }
            }

            UpdateSamples();
            editor.Dirty = true;
        }

        private void ObjectAdditionSetChange(object sender, EventArgs e)
        {
            SampleSet ss = (SampleSet)ddSampleSetAddition.SelectedObject;

            if (selectedSlider != null)
            {
                editor.UndoPush();
                bool startPointSelected = selectedSlider.sliderStartCircle.Selected;
                List<HitCircleSliderEnd> selectedEndCircles =
                    selectedSlider.sliderEndCircles.FindAll(c => c.Selected);

                selectedSlider.EnableNodalSamples();

                if (startPointSelected || selectedEndCircles.Count > 0)
                {
                    // Individual node editing.
                    if (startPointSelected)
                        selectedSlider.SampleSetAdditionList[0] = ss;

                    for (int i = 0; i < selectedSlider.sliderEndCircles.Count; i++)
                        if (selectedSlider.sliderEndCircles[i].Selected)
                            selectedSlider.SampleSetAdditionList[i + 1] = ss;
                }
                else
                {
                    if (!selectedSlider.BodySelected)
                    {
                        // Complete slider editing.
                        selectedSlider.SampleSetAdditionList[0] = ss;
                        for (int i = 0; i < selectedSlider.sliderEndCircles.Count; i++)
                            selectedSlider.SampleSetAdditionList[i + 1] = ss;
                    }

                    // Slider body editing.
                    selectedSlider.SampleSetAdditions = ss;
                }
            }
            else
            {
                if (selectedObjects.Count > 0)
                {
                    editor.UndoPush();
                }
                foreach (HitObject h in selectedObjects)
                {
                    SliderOsu s = h as SliderOsu;
                    if (s != null)
                    {
                        s.EnableNodalSamples();

                        s.SampleSetAdditionList[0] = ss;
                        for (int i = 0; i < s.sliderEndCircles.Count; i++)
                            s.SampleSetAdditionList[i + 1] = ss;
                    }

                    h.SampleSetAdditions = ss;
                }
            }

            editor.Dirty = true;
        }

        private void ChangeSoundAddition(SoundAdditions add, bool value, bool updateSelection)
        {
            if (SoundAdditionStatus[add] == value)
                return;

            SoundAdditionStatus[add] = value;

            if (spriteManager.GetTagged(add)[0].IsVisible)
                ShowSoundAdditionStatus(add);

            switch (add)
            {
                case SoundAdditions.GridSnap:
                    GameBase.EditorControl.GridSnapChanged(value);
                    return;
                case SoundAdditions.DistSnap:
                    return;
            }

            //update the selection
            if (updateSelection)
            {
                if (selectedObjects.Count == 0)
                    return;

                editor.UndoPush();

                if (selectedSlider != null)
                {
                    bool startPointSelected = selectedSlider.sliderStartCircle.Selected;
                    List<HitCircleSliderEnd> selectedEndCircles =
                        selectedSlider.sliderEndCircles.FindAll(c => c.Selected);

                    if (startPointSelected || selectedEndCircles.Count > 0)
                    {
                        // Individual node editing.
                        selectedSlider.EnableNodalSamples();

                        if (startPointSelected)
                            ApplySliderNodalSoundAddition(selectedSlider, value, add, 0);

                        for (int i = 0; i < selectedSlider.sliderEndCircles.Count; i++)
                            if (selectedSlider.sliderEndCircles[i].Selected)
                                ApplySliderNodalSoundAddition(selectedSlider, value, add, i + 1);
                    }
                    else
                    {
                        if (selectedSlider.BodySelected)
                            selectedSlider.EnableNodalSamples();
                        else if (!selectedSlider.unifiedSoundAddition)
                            ApplySliderNodalSoundAddition(selectedSlider, value, add);

                        switch (add)
                        {
                            case SoundAdditions.Whistle:
                                selectedSlider.Whistle = SoundAdditionStatus[add];
                                break;
                            case SoundAdditions.Finish:
                                selectedSlider.Finish = SoundAdditionStatus[add];
                                break;
                            case SoundAdditions.Clap:
                                selectedSlider.Clap = SoundAdditionStatus[add];
                                break;
                        }
                    }

                    if (add == SoundAdditions.NewCombo)
                    {
                        bool has_nc = selectedSlider.NewCombo = SoundAdditionStatus[add];

                        hitObjectManager.SpinnerForceNewCombo(hitObjectManager.hitObjects.IndexOf(selectedSlider));

                        if (!has_nc) selectedSlider.ComboOffset = 0;
                    }
                }
                else
                {
                    foreach (HitObject h in selectedObjects)
                        if (add == SoundAdditions.NewCombo)
                        {
                            bool has_nc = h.NewCombo = SoundAdditionStatus[add];

                            hitObjectManager.SpinnerForceNewCombo(hitObjectManager.hitObjects.IndexOf(h));

                            if (!has_nc) h.ComboOffset = 0; // Reset the combo offset value when a new combo is taken off.
                        }
                        else
                        {
                            SliderOsu s = h as SliderOsu;
                            if (s != null && !s.unifiedSoundAddition)
                                ApplySliderNodalSoundAddition(s, value, add);

                            switch (add)
                            {
                                case SoundAdditions.Whistle:
                                    h.Whistle = SoundAdditionStatus[add];
                                    break;
                                case SoundAdditions.Finish:
                                    h.Finish = SoundAdditionStatus[add];
                                    break;
                                case SoundAdditions.Clap:
                                    h.Clap = SoundAdditionStatus[add];
                                    break;
                            }
                        }
                }

                if (add == SoundAdditions.NewCombo)
                {
                    hitObjectManager.Sort(true);
                    UpdateComboDropdown();
                }
            }

            editor.Dirty = true;
        }

        private void ApplySliderNodalSoundAddition(SliderOsu slider, bool value, SoundAdditions add)
        {
            ApplySliderNodalSoundAddition(slider, value, add, 0);

            for (int i = 0; i < slider.sliderEndCircles.Count; i++)
                ApplySliderNodalSoundAddition(slider, value, add, i + 1);
        }

        private void ApplySliderNodalSoundAddition(SliderOsu slider, bool value, SoundAdditions add, int nodeIndex)
        {
            HitObjectSoundType addition;
            switch (add)
            {
                case SoundAdditions.Whistle:
                    addition = HitObjectSoundType.Whistle;
                    break;
                case SoundAdditions.Finish:
                    addition = HitObjectSoundType.Finish;
                    break;
                case SoundAdditions.Clap:
                    addition = HitObjectSoundType.Clap;
                    break;
                default:
                    return;
            }
            slider.SoundTypeList[nodeIndex] = value ? (slider.SoundTypeList[nodeIndex] | addition)
                                                    : (slider.SoundTypeList[nodeIndex] ^ addition);
        }

        private void ShowSoundAdditionStatus(SoundAdditions add)
        {
            pDrawable cur = spriteManager.GetTagged(add)[0];

            cur.Transformations.Clear();

            if (SoundAdditionStatus[add])
                cur.Transformations.Add(
                    new Transformation(TransformationType.Fade, 1, 0.9F, GameBase.Time, GameBase.Time + 100));
            else
                cur.Transformations.Add(
                    new Transformation(TransformationType.Fade, cur.Alpha, 0.4F, GameBase.Time,
                                       GameBase.Time + 100));
        }

        private void updateCheckKey()
        {
            if (KeyboardHandler.AltPressed)
            {
                if (!distSnapOverride)
                {
                    ChangeSoundAddition(SoundAdditions.DistSnap);
                    sliderBarTitle.Text = LocalisationManager.GetString(OsuString.EditorModeCompose_DistanceSpacing) + @":";
                    beatDivisorSlider.ChangeBounds(0.1, 2, distanceSpacingFactor);
                    ChangeAltDistance(distanceSpacingFactor);
                    distSnapOverride = true;
                }
            }
            else
            {
                if (distSnapOverride)
                {
                    ChangeSoundAddition(SoundAdditions.DistSnap);
                    sliderBarTitle.Text = LocalisationManager.GetString(OsuString.EditorModeCompose_BeatSnapDivisor) + @":";
                    beatDivisorSlider.ChangeBounds(1, 16, editor.beatSnapDivisor);
                    ChangeSnapDivisor(editor.beatSnapDivisor);
                    distSnapOverride = false;
                }
            }

            //Check modifier keys which affect editor controls.
            if (KeyboardHandler.ShiftPressed)
            {
                if (!gridSnapOverride)
                {
                    ChangeSoundAddition(SoundAdditions.GridSnap);
                    gridSnapOverride = true;
                }
            }
            else
            {
                if (gridSnapOverride)
                {
                    ChangeSoundAddition(SoundAdditions.GridSnap);
                    gridSnapOverride = false;
                }
            }
        }

        internal void HandleDrag(bool fullReset = false)
        {
            if (resetDrag) return;

            if (editor.IsDragging && !ignoreDragReset)
            {
                switch (editor.DragCaptured)
                {
                    case DragCapture.Gamefield:
                    case DragCapture.Timeline:
                        if (!dragSelection || fullReset)
                            resetDrag = true;
                        break;
                }
            }
        }

        private void resetDragValues()
        {
            dragStartPosition = Vector2.Zero;
            dragStartTime = editor.TimelinePosToTime(InputManager.CursorPoint.X);

            timelineDrag1 = timelineDrag2 = dragStartTime;
        }

        private void updateHoverStatus(Vector2 snapVector)
        {
            Vector2 mouseVector = InputManager.CursorPosition;
            int mouseX = (int)GameBase.DisplayToGamefieldX(InputManager.CursorPoint.X);
            int mouseY = (int)GameBase.DisplayToGamefieldY(InputManager.CursorPoint.Y);
            int snapX = (int)snapVector.X;
            int snapY = (int)snapVector.Y;

            HitObject found = null;
            if (editor.InTimeline)
            {
                Event foundEvent = null;
                for (int i = 0; i < editor.eventManager.eventBreaks.Count; i++)
                {
                    EventBreak e = editor.eventManager.eventBreaks[i];
                    if (editor.TimelinePosToTime(InputManager.CursorPoint.X) > e.StartTime &&
                        editor.TimelinePosToTime(InputManager.CursorPoint.X) < e.EndTime)
                    {
                        foundEvent = e;
                        break;
                    }
                }

                hoveredEvent = foundEvent;

                //check for timeline circles too
                if (sliderPlacementCircle == null)
                {
                    double lowestDist = 1000;

                    List<pDrawable> timelineCircles;
                    timelineCircles = editor.timelineManager.SpriteList.FindAll(s => s.Tag != null);

                    foreach (pSprite p in timelineCircles)
                    {
                        HitObject h = (HitObject)p.Tag;

                        double thisDist = Vector2.Distance(mouseVector,
                                                           (p.Position * GameBase.WindowRatio) +
                                                           new Vector2(0, GameBase.WindowOffsetY));

                        //Checks for clicking in the middle of a slider - this should be allowed.
                        bool sliderRangeOk = editor.TimelinePosToTime(InputManager.CursorPoint.X) > h.StartTime &&
                                             editor.TimelinePosToTime(InputManager.CursorPoint.X) < h.EndTime;

                        bool selected = selectedObjects.Contains(h);

                        if ((thisDist < (p.DrawWidth * p.Scale * GameBase.WindowRatio) / 5 &&
                             (thisDist < lowestDist || selected)) || sliderRangeOk)
                        {
                            lowestDist = thisDist;
                            found = h;
                            if (selected)
                                break;
                        }
                    }
                }
            }
            else
            {
                //Make sure there aren't two overlapping circles at current mouse location.  If so, retain current selection.
                for (int i = 0; i < selectedObjects.Count; i++)
                {
                    HitObject h = selectedObjects[i];

                    if (Vector2.Distance(GameBase.DisplayToGamefield(mouseVector), h.Position) <
                        hitObjectManager.HitObjectRadius)
                        found = h;

                    if (h.IsType(HitObjectType.Slider) && h.IsVisible)
                    {
                        SliderOsu s = h as SliderOsu;
                        if (s.sliderCurveSmoothLines != null)
                            foreach (Line l in s.sliderCurveSmoothLines)
                            {
                                if (Vector2.Distance(GameBase.DisplayToGamefield(mouseVector), l.p1) <
                                    hitObjectManager.HitObjectRadius)
                                {
                                    found = h;
                                    break;
                                }
                            }
                    }

                    if (found != null)
                        break;
                }

                if (found == null)
                {
                    if (CurrentComposeTool == ComposeTools.Select)
                        found =
                            hitObjectManager.FindObjectAt(mouseX, mouseY,
                                                          hitObjectManager.HitObjectRadius * 0.8F);
                    else if (SoundAdditionStatus[SoundAdditions.DistSnap])
                        found =
                            hitObjectManager.FindCircleAt(mouseX, mouseY, false, hitObjectManager.HitObjectRadius * 0.5F);
                    else
                        found =
                            hitObjectManager.FindCircleAt(snapX, snapY, false, hitObjectManager.HitObjectRadius * 0.8F);
                }
            }

            hoveredObject = found;
        }

        internal override void Update()
        {
            if (ignoreDrag)
                editor.IsDragging = false;

            editor.BreakTimeFix(false);
            updateCheckKey();
            UpdateWarningMessage();

            int mouseX = (int)GameBase.DisplayToGamefieldX(InputManager.CursorPoint.X);
            int mouseY = (int)GameBase.DisplayToGamefieldY(InputManager.CursorPoint.Y);
            Vector2 mouseVector = InputManager.CursorPosition;
            mouseGamefieldVector = new Vector2(mouseX, mouseY);

            float snapX = mouseX;
            float snapY = mouseY;

            //todo: Check for selected timeline objects if drag finished began in timeline and was dragging objects. Also don't allow drags to be handled outside of the original drag area. 
            if (resetDrag)
            {
                switch (editor.DragCaptured)
                {
                    case DragCapture.Gamefield:
                    case DragCapture.Timeline:
                        InputManager.DragFinish();
                        break;
                }

                resetDragValues();
                updateHoverStatus(mouseVector);

                dragSelection = (hoveredObject == null || (!hoveredObject.Selected && selectedSliderIndex < 0));

                if (ignoreDragSelect && dragSelection)
                {
                    ignoreDrag = true;
                    ignoreDragSelect = false;
                }
                resetDrag = false;
            }

            HitObject firstSelected = null;
            if (selectedObjects.Count > 0)
            {
                // mm: Rounding here seems to fix everything and not cause sliderpoint jumps as the previous fix did.
                snapX -= (int)dragStartPosition.X;
                snapY -= (int)dragStartPosition.Y;

                if (selectionChanged)
                    selectedObjects.Sort();
                firstSelected = selectedObjects[0];

                if (selectedSlider != null && !selectedSlider.Selected)
                {
                    selectedSlider = null;
                }
            }
            else
            {
                selectedSlider = null;
            }

            float autoStackRadius = hitObjectManager.HitObjectRadius * 0.6F;
            applyGridSnap = SoundAdditionStatus[SoundAdditions.GridSnap];

            //Handle distance snapping
            if ((editor.IsDragging || currentPlacementObject != null) &&
                sliderPlacementCircle == null && selectedSliderIndex <= 0 && !editor.InTimeline)
            {
                int selectionStartTime = AudioEngine.Time;
                //Find the time of the first selected circle (used as a base point for the position of snapped notes).
                if (selectedObjects.Count > 0)
                {
                    selectionStartTime = firstSelected.StartTime;
                }

                int lastObjectIndex = hitObjectManager.hitObjects.BinarySearch(new HitObjectDummy(hitObjectManager, selectionStartTime - 1));

                if (lastObjectIndex < 0)
                    lastObjectIndex = ~lastObjectIndex - 1;

                if (lastObjectIndex >= 0 && !(hitObjectManager.hitObjects[lastObjectIndex] is SpinnerOsu))
                {
                    Vector2 lastObjectPosition = hitObjectManager.hitObjects[lastObjectIndex].EndPosition;
                    int lastObjectTime = hitObjectManager.hitObjects[lastObjectIndex].EndTime;

                    Vector2 unitVector = Vector2.Normalize(new Vector2(snapX, snapY) - lastObjectPosition);

                    if (SoundAdditionStatus[SoundAdditions.DistSnap])
                    {
                        Vector2 snap = lastObjectPosition +
                                   (unitVector * (float)(hitObjectManager.SliderVelocityAt(selectionStartTime) * distanceSpacingFactor * (selectionStartTime - lastObjectTime) / 1000));

                        //Check snap position is in range...
                        if (GameBase.IsInGamefieldAlt(snap))
                        {
                            snapX = snap.X;
                            snapY = snap.Y;

                            if (soundAdditionSprites[5].InitialColour == Color.Red)
                                soundAdditionSprites[5].FadeColour(Color.White, 100);

                            if (editor.IsDragging && selectedObjects.Count > 0)
                                hoveredObject = firstSelected;
                        }
                        else if (!dragSelection && editor.DragCaptured == DragCapture.Gamefield)
                        {
                            soundAdditionSprites[5].FadeColour(Color.Red, 100);
                        }

                        //Angle snapping algorithm
                        bool gridSnapTemporaryDisable = !SoundAdditionStatus[SoundAdditions.GridSnap] &&
                        (KeyboardHandler.ShiftPressed && !KeyboardHandler.ControlPressed ||
                        !KeyboardHandler.ShiftPressed && KeyboardHandler.ControlPressed);

                        if (gridSnapTemporaryDisable && GameBase.IsInGamefieldAlt(mouseGamefieldVector))
                        {
                            double angle = Math.Atan2(snap.X - lastObjectPosition.X, snap.Y - lastObjectPosition.Y) * 180 / Math.PI;
                            double angleAbs = Math.Abs(angle);
                            int roundedAngle = (int)(Math.Round(angle / 45) * 45);

                            double angleDiff = Math.Abs(angle - roundedAngle);

                            if (angleDiff < 10)
                            {
                                roundedAngle = -roundedAngle + 90;

                                float distance = Vector2.Distance(new Vector2(snapX, snapY), lastObjectPosition);

                                Vector2 addVector = new Vector2((float)Math.Cos(roundedAngle * Math.PI / 180), (float)Math.Sin(roundedAngle * Math.PI / 180));

                                snapX = (float)(lastObjectPosition.X + addVector.X * distance);
                                snapY = (float)(lastObjectPosition.Y + addVector.Y * distance);

                                snapLine = new Line(GameBase.GamefieldToDisplay(lastObjectPosition), GameBase.GamefieldToDisplay(lastObjectPosition + addVector * distance * 2));

                                applyGridSnap = false;
                            }
                        }

                        if (Vector2.Distance(mouseGamefieldVector, lastObjectPosition) < autoStackRadius)
                        {
                            snapX = lastObjectPosition.X;
                            snapY = lastObjectPosition.Y;

                            //stackSnapTriggered = true;
                            applyGridSnap = false;
                        }

                    }
                    else
                    {
                        autoStackRadius = hitObjectManager.HitObjectRadius * 0.15F;

                        if (selectedObjects.Count > 0)
                        {
                            if (stackSnapTriggered == false)
                            {
                                float objectDistance = Vector2.Distance(firstSelected.Position, lastObjectPosition);
                                //Auto stack when enabled and when cursor is in range
                                if ((!ignoreAutoStack || (ignoreAutoStack && Vector2.Distance(mouseGamefieldVector, lastObjectPosition) < autoStackRadius)) &&
                                     objectDistance < autoStackRadius)
                                {
                                    snapX = lastObjectPosition.X;
                                    snapY = lastObjectPosition.Y;

                                    selectionDragOrigin = new Vector2(mouseX, mouseY);

                                    stackSnapTriggered = true;
                                    applyGridSnap = false;
                                }

                            }
                            else
                            {
                                float dragDistance = Vector2.Distance(mouseGamefieldVector, selectionDragOrigin);
                                float maxDragDistance = 8;
                                if (dragDistance > maxDragDistance)
                                {
                                    //ignoreAutoStack = true;
                                    stackSnapTriggered = false;

                                }
                                else
                                {
                                    snapX = lastObjectPosition.X;
                                    snapY = lastObjectPosition.Y;

                                    applyGridSnap = false;
                                }
                            }
                        }
                        else
                        {
                            ignoreAutoStack = false;
                            stackSnapTriggered = false;
                        }
                    }
                }
                else
                {
                    ignoreAutoStack = false;
                    stackSnapTriggered = false;
                }
            }
            else if (soundAdditionSprites[5].InitialColour == Color.Red)
                soundAdditionSprites[5].FadeColour(Color.White, 100);

            if (applyGridSnap)
            {
                //snap
                snapX = (int)Math.Round(snapX / editor.GridSize) * editor.GridSize;
                snapY = (int)Math.Round(snapY / editor.GridSize) * editor.GridSize;
            }

            if (!KeyboardHandler.AltPressed && beatDivisorSlider.Visible
                && ClampDivisor((int)Math.Round(beatDivisorSlider.current)) != editor.beatSnapDivisor)
            {
                ChangeSnapDivisor((int)Math.Round(beatDivisorSlider.current));
            }
            else if (KeyboardHandler.AltPressed && beatDivisorSlider.Visible &&
                Math.Round(beatDivisorSlider.current, KeyboardHandler.ShiftPressed ? 2 : 1) != distanceSpacingFactor)
                ChangeAltDistance((float)Math.Round(beatDivisorSlider.current, KeyboardHandler.ShiftPressed ? 2 : 1));

            if (editor.IsDragging)
            {
                switch (editor.DragCaptured)
                {
                    case DragCapture.Gamefield:
                        //Single and drag beat selection (in the gamefield)
                        if (dragSelection)
                        {
                            DragPoint2 = mouseVector;

                            Rectangle rect =
                                new Rectangle((int)Math.Min(DragPoint1.X, DragPoint2.X),
                                              (int)Math.Min(DragPoint1.Y, DragPoint2.Y),
                                              (int)Math.Abs(DragPoint2.X - DragPoint1.X),
                                              (int)Math.Abs(DragPoint2.Y - DragPoint1.Y));

                            foreach (HitObject ho in hitObjectManager.hitObjects.FindAll(h => h.IsVisible))
                                if (rect.Contains((int)GameBase.GamefieldToDisplayX((int)ho.Position.X),
                                                  (int)GameBase.GamefieldToDisplayY((int)ho.Position.Y)))
                                    Select(ho);
                                else
                                    Deselect(ho);

                            if (selectedObjects.Count == 0)
                            {
                                ResetSoundAdditions();
                            }
                        }
                        else if (hoveredSlider != null && hoveredSlider != sliderPlacementCircle && selectedSliderIndex >= 0)
                        {
                            Vector2 snapVector = new Vector2(snapX, snapY);
                            Vector2 selectedPoint = hoveredSlider.sliderCurvePoints[selectedSliderIndex];

                            if (snapVector != selectedPoint)
                            {
                                IsDragModified = true;
                                hoveredSlider.MovePoint(snapVector, selectedSliderIndex);
                            }
                        }
                        else if (selectedObjects.Count > 0 && hoveredObject != null)
                        {
                            AttemptMove(snapX, snapY, false);
                        }
                        break;
                    case DragCapture.Timeline:
                        //timeline selection
                        timelineDrag2 = editor.TimelinePosToTime(InputManager.CursorPoint.X);

                        if (AudioEngine.AudioState != AudioStates.Playing)
                        {
                            //slide the timeline if the user is pointing to the left or right of it
                            int seekRate = 0;

                            if (timelineDrag2 < editor.TimelineLeftBound)
                                seekRate = (timelineDrag2 - editor.TimelineLeftBound) / 2;
                            else if (timelineDrag2 > editor.TimelineRightBound)
                                seekRate = (timelineDrag2 - editor.TimelineRightBound) / 2;

                            if (seekRate != 0)
                                AudioEngine.SeekTo(AudioEngine.Time + seekRate);
                        }

                        if (hoveredResizeMode)
                        {
                            if (hoveredObject != null)
                            {
                                EventBreak br;
                                if (hoveredObject.IsType(HitObjectType.Slider))
                                {
                                    SliderOsu current = (SliderOsu)hoveredObject;
                                    int endTime = current.EndTime;
                                    int segments = current.SegmentCount;

                                    current.AdjustRepeats(timelineDrag2);

                                    if (current.EndTime != endTime)
                                    {
                                        IsDragModified = true;

                                        hoveredObject = current;
                                        hitObjectManager.Sort(true);
                                    }
                                }
                                else if (hoveredObject.IsType(HitObjectType.Spinner))
                                {
                                    int newSpinnerEnd = editor.Timing.BeatSnapValue(timelineDrag2);
                                    if (newSpinnerEnd > hoveredObject.StartTime)
                                    {
                                        IsDragModified = true;
                                        hoveredObject.SetEndTime(newSpinnerEnd);
                                    }
                                }

                                hoveredObject.Update();
                            }
                            else if (dragBreak != null)
                            {
                                SelectNone();
                                EventBreak b = dragBreak;
                                if (dragBreakEnd)
                                {
                                    int newEnd, oldEnd;

                                    newEnd = editor.Timing.BeatSnapValue(timelineDrag2);
                                    oldEnd = b.EndTime;

                                    if (newEnd != b.EndTime)
                                    {
                                        b.SetEndTime(newEnd);

                                        IsDragModified = true;
                                        editor.BreakTimeFix(false, true);
                                    }
                                }
                                else
                                {
                                    int newStart = editor.Timing.BeatSnapValue(timelineDrag2);

                                    if (newStart != b.StartTime)
                                    {
                                        b.SetStartTime(newStart);

                                        IsDragModified = true;
                                        editor.BreakTimeFix(false, true);
                                    }
                                }
                            }
                        }
                        else if (dragSelection)
                        {
                            int timelineDrag2Clamped =
                                Math.Min(Math.Max(editor.TimelineLeftBound, timelineDrag2), editor.TimelineRightBound);

                            foreach (HitObject h in hitObjectManager.hitObjects)
                            {
                                if ((h.StartTime >= timelineDrag1 && h.StartTime <= timelineDrag2Clamped) ||
                                   (h.StartTime <= timelineDrag1 && h.StartTime >= timelineDrag2Clamped) ||
                                   (h.EndTime >= timelineDrag1 && h.EndTime <= timelineDrag2Clamped) ||
                                   (h.EndTime <= timelineDrag1 && h.EndTime >= timelineDrag2Clamped))
                                    Select(h);
                                else
                                    Deselect(h);
                            }
                        }
                        else if (selectedObjects.Count > 0 && hoveredObject != null)
                        {
                            //Dragging a number of objects in timeline.
                            int time = editor.TimelinePosToTime(InputManager.CursorPoint.X);

                            if (!KeyboardHandler.ShiftPressed)
                                time = editor.Timing.BeatSnapValue(time - dragStartTime);
                            else
                                time -= dragStartTime;

                            int timeChange = time - hoveredObject.StartTime;

                            bool inSong = true;

                            //check that *every* circle selected will end up inside the song length...
                            for (int i = 0; i < selectedObjects.Count; i++)
                            {
                                HitObject h = selectedObjects[i];
                                if (h.StartTime + timeChange < 0 ||
                                    h.StartTime + timeChange >= AudioEngine.AudioLength)
                                {
                                    inSong = false;
                                    break;
                                }
                            }

                            if (inSong && !SoundAdditionStatus[SoundAdditions.NoteLock] && timeChange != 0)
                            {
                                IsDragModified = true;

                                selectedObjects.ForEach(h => h.ModifyTime(h.StartTime + timeChange));
                                hitObjectManager.Sort(true);
                            }
                        }
                        break;
                }
            }
            else
            {
                HitObject found = null;
                bool timelineFound = false;
                selectedSliderIndex = -1;

                //Event finding code.
                Event foundEvent = null;
                if (hoveredSlider != null)
                {
                    if (CurrentComposeTool == ComposeTools.Select)
                    {
                        for (int i = 0; i < hoveredSlider.sliderCurvePoints.Count; i++)
                        {
                            if (Vector2.Distance(mouseGamefieldVector, hoveredSlider.sliderCurvePoints[i]) < 4)
                            {
                                selectedSliderIndex = i;
                                break;
                            }
                        }
                    }

                    if (selectedSliderIndex >= 0)
                    {
                        editor.InGamefield = true;
                    }
                    else if (editor.InGamefield && editor.lastPosition == Location.Timeline)
                    {
                        hoveredSlider = null;
                    };
                }

                updateHoverStatus(new Vector2(snapX, snapY));

                //always snap to 640 x 480 resolution.
                // MM: Let's try rounding elsewhere to fix this...
                snapX = (int)snapX;
                snapY = (int)snapY;

                switch (CurrentComposeTool)
                {
                    case ComposeTools.Normal:
                    case ComposeTools.Hold:
                    case ComposeTools.Slider:

                        if (CurrentComposeTool != ComposeTools.Slider || sliderPlacementCircle == null)
                        {
                            if (hoveredObject != null)
                            {
                                //autoStackRadius = hitObjectManager.HitObjectRadius * hitObjectManager.HitObjectRadius;
                                if (Vector2.Distance(mouseGamefieldVector, hoveredObject.Position) < autoStackRadius)
                                {
                                    snapX = hoveredObject.Position.X;
                                    snapY = hoveredObject.Position.Y;
                                }
                                else if (Vector2.Distance(mouseGamefieldVector, hoveredObject.Position2) < autoStackRadius)
                                {
                                    snapX = hoveredObject.Position2.X;
                                    snapY = hoveredObject.Position2.Y;
                                }
                            }
                            Vector2 snap = new Vector2(snapX, snapY);
                            if (GameBase.IsInGamefieldAlt(snap)) //Display object only in bounds.
                            {
                                currentPlacementObject = new HitCircleOsu(hitObjectManager, snap, AudioEngine.Time,
                                                                          SoundAdditionStatus[SoundAdditions.NewCombo] || LastWasSpinner());
                                currentPlacementObject.IsHit = true;
                                currentPlacementObject.Sounded = true;

                                if (BeatmapManager.Current.PlayMode == PlayModes.Taiko)
                                {
                                    Color taikoRed = new Color(235, 69, 44);
                                    Color taikoBlue = new Color(67, 142, 172);
                                    Color taikoYellow = new Color(252, 184, 6);

                                    if (CurrentComposeTool == ComposeTools.Normal)
                                    {
                                        if (SoundAdditionStatus[SoundAdditions.Whistle] || SoundAdditionStatus[SoundAdditions.Clap])
                                        {
                                            currentPlacementObject.SpriteCollection[0].Transformations[1].StartColour = taikoBlue;
                                            currentPlacementObject.SpriteCollection[0].Transformations[1].EndColour = taikoBlue;
                                            currentPlacementObject.SpriteCollection[0].InitialColour = taikoBlue;
                                            currentPlacementObject.SpriteCollection[1].InitialColour = taikoBlue;
                                        }
                                        else
                                        {
                                            currentPlacementObject.SpriteCollection[0].Transformations[1].StartColour = taikoRed;
                                            currentPlacementObject.SpriteCollection[0].Transformations[1].EndColour = taikoRed;
                                            currentPlacementObject.SpriteCollection[0].InitialColour = taikoRed;
                                            currentPlacementObject.SpriteCollection[1].InitialColour = taikoRed;
                                        }
                                    }
                                    else if (CurrentComposeTool == ComposeTools.Slider)
                                    {
                                        currentPlacementObject.SpriteCollection[0].Transformations[1].StartColour = taikoYellow;
                                        currentPlacementObject.SpriteCollection[0].Transformations[1].EndColour = taikoYellow;
                                        currentPlacementObject.SpriteCollection[0].InitialColour = taikoYellow;
                                        currentPlacementObject.SpriteCollection[1].InitialColour = taikoYellow;
                                    }

                                }

                                hitObjectManager.Add(currentPlacementObject, false);
                                hitObjectManager.Sort(false);
                            }
                        }
                        else
                        {
                            //Adjusts so the slider is always visible.  Will be corrected when slider is finally placed.
                            sliderPlacementCircle.AdjustRepeats2(AudioEngine.Time);

                            sliderPlacementPoint = new Vector2(snapX, snapY);
                            sliderPlacementCircle.PlacePointNext(sliderPlacementPoint);
                        }
                        break;
                    case ComposeTools.Spinner:
                        if (spinnerPlacementCircle != null)
                        {
                            int newEndTime = (int)(editor.GetCurrentSnappedTime() + AudioEngine.beatLength);
                            if (spinnerPlacementCircle.EndTime != newEndTime && newEndTime > spinnerPlacementCircle.StartTime)
                            {
                                spinnerPlacementCircle.SetEndTime(
                                    Math.Max(spinnerPlacementCircle.StartTime + 1,
                                             newEndTime));
                            }
                        }
                        else
                        {
                            currentPlacementObject = new SpinnerOsu(hitObjectManager, AudioEngine.Time, AudioEngine.Time,
                                        soundAdditionSoundType);
                            for (int i = 0; i < currentPlacementObject.SpriteCollection.Count; i++)
                            {
                                pSprite p = currentPlacementObject.SpriteCollection[i];
                                p.Scale = 0.4F;
                                if (Origins.TopLeft == p.Origin)
                                    p.Transformations.Clear();
                            }

                            currentPlacementObject.IsHit = true;
                            currentPlacementObject.Sounded = true;
                            hitObjectManager.Add(currentPlacementObject, false);
                        }
                        break;
                }

                bool boo;

                if (!hoveredResizeMode || hoveredResizeMode && !editor.IsDragging)
                {
                    hoveredResizeMode = false;
                    if (editor.InTimeline)
                    {
                        if (hoveredObject != null)
                        {
                            if (selectedObjects.Count <= 1 && (hoveredObject.IsType(HitObjectType.Slider) || hoveredObject.IsType(HitObjectType.Spinner)))
                            {
                                hoveredResizeMode =
                                    Math.Abs(InputManager.CursorPoint.X - editor.TimeToTimelinePos(hoveredObject.EndTime)) < 6;
                            }
                        }
                        else
                        {
                            hoveredResizeMode = BreakToDrag(out boo) != null;
                        }
                    }

                    if (hoveredResizeMode)
                    {
                        MouseManager.ChangeCursor(Cursors.SizeWE);
                    }
                    else if (CurrentComposeTool != ComposeTools.Select &&
                        GameBase.IsInGamefieldAlt(mouseGamefieldVector))
                    {
                        MouseManager.ChangeCursor(Cursors.Cross);
                        if (selectedObjects.Count > 0)
                            SelectNone();
                    }
                    else
                    {
                        MouseManager.ChangeCursor();
                    }
                }
            }

            //Gets current slider for point editing functionality
            if (selectedObjects.Count == 1 && selectedObjects[0].IsType(HitObjectType.Slider))
            {
                selectedSlider = (SliderOsu)selectedObjects[0];
                hoveredSlider = selectedSlider;
            }
            else if (sliderPlacementCircle != null)
            {
                selectedSlider = sliderPlacementCircle;
                hoveredSlider = sliderPlacementCircle;
            }
            else if (hoveredObject != null && hoveredObject.IsType(HitObjectType.Slider))
                hoveredSlider = (SliderOsu)hoveredObject;
            else
            {
                selectedSlider = null;
                hoveredSlider = selectedSlider;
            }

            if (selectionChanged)
            {
                selectedObjects.Sort();

                UpdateComboDropdown();
                UpdateSamples();
                UpdateSoundAdditions();
            }

            //Store last known position of the cursor.
            if (editor.InGamefield)
            {
                editor.lastPosition = Location.Gamefield;
            }
            else if (editor.InTimeline)
            {
                editor.lastPosition = Location.Timeline;
            }
            else if (editor.InSeekbar)
            {
                editor.lastPosition = Location.Seekbar;
            }
            else
            {
                editor.lastPosition = Location.None;
            }

            hitObjectManager.Update();

            snapCoordinates = new Vector2(snapX, snapY);
            if (GameBase.SixtyFramesPerSecondFrame)
                UpdateCoordinateTicker();

            selectionChanged = false;

        }

        protected virtual void AttemptMove(float snapX, float snapY, bool keyboard)
        {
            if (SoundAdditionStatus[SoundAdditions.NoteLock]) return;

            if (keyboard && !undoKeysPressed)
            {
                editor.UndoPush(true);
                undoKeysPressed = true;
            }

            bool allowMove = true;
            HitObject ho = (hoveredObject == null || keyboard) ? selectedObjects[0] : hoveredObject;

            if (applyGridSnap)
            {
                snapX = (int)Math.Round(snapX / editor.GridSize) * editor.GridSize;
                snapY = (int)Math.Round(snapY / editor.GridSize) * editor.GridSize;
            }

            float xchange = (Math.Min(512, Math.Max(0, snapX)) - ho.Position.X);
            float ychange = (Math.Min(384, Math.Max(0, snapY)) - ho.Position.Y);

            //check that *every* circle selected will end up inside the gamefield...
            foreach (HitObject h in selectedObjects)
                if (
                    !GameBase.IsInGamefield(
                         new Vector2(h.Position.X + xchange, h.Position.Y + ychange)) ||
                    !GameBase.IsInGamefield(
                         new Vector2(h.Position2.X + xchange, h.Position2.Y + ychange)))
                {
                    allowMove = false;
                    break;
                }

            //and only then move them
            if (allowMove && (xchange != 0 || ychange != 0))
            {
                IsDragModified = true;
                foreach (HitObject h in selectedObjects)
                    h.ModifyPosition(new Vector2((int)(h.Position.X + xchange), (int)(h.Position.Y + ychange)));
            }
        }

        protected virtual void UpdateCoordinateTicker()
        {
            //Show current gamefield coordinates and slider velocity between selected first/last objects
            //and the previous/next objects.

            if ((hoveredSlider != null) && (selectedSliderIndex >= 0))
            {
                if (selectedSliderIndex < hoveredSlider.sliderCurvePoints.Count)
                {
                    // A slider control point is selected/hovered.
                    Vector2 pt = hoveredSlider.sliderCurvePoints[selectedSliderIndex];
                    coordinateText.InitialColour = Color.LightSalmon;
                    coordinateText.Text = String.Format(@"x:{0:0} y:{1:0}", pt.X, pt.Y);
                }
                Vector2 pos = hoveredSlider.Position;
                Vector2 posEnd = hoveredSlider.EndPosition;

                string spacingText = string.Empty;
                HitObject prevObject = hitObjectManager.hitObjects.FindLast(n => n.EndTime < hoveredSlider.StartTime - 1);
                if (prevObject != null && !(prevObject is Spinner))
                {
                    int selectedTime = hoveredSlider.StartTime;
                    int time = selectedTime - prevObject.EndTime;
                    double mul = hitObjectManager.SliderVelocityAt(selectedTime);
                    if (time < 3000)
                    {
                        double distance = 1000.0d * (pos - prevObject.EndPosition).Length() / (mul * time);
                        spacingText += String.Format("\nPrev: {0:f2}x", distance);
                    }
                }
                else
                    spacingText += "\n";


                HitObject nextObject = hitObjectManager.hitObjects.Find(n => n.StartTime > hoveredSlider.EndTime + 1);
                if (nextObject != null && !(nextObject is Spinner))
                {
                    int selectedTime = hoveredSlider.EndTime;
                    int nextSelectedTime = nextObject.StartTime;
                    int time = nextSelectedTime - selectedTime;
                    double mul = hitObjectManager.SliderVelocityAt(nextSelectedTime);
                    if (time < 3000)
                    {
                        double distance = 1000.0d * (nextObject.Position - posEnd).Length() / (mul * time);
                        spacingText += String.Format("\nNext: {0:f2}x", distance);
                    }
                }
                coordinateText.Text += spacingText;
            }
            else if (selectedObjects.Count > 0)
            {
                // A hit object or objects are selected.

                HitObject selectedObject = selectedObjects[0];
                coordinateText.InitialColour = Color.Yellow;

                string spacingText = string.Empty;
                HitObject lastSelectedObject = selectedObjects[selectedObjects.Count - 1];
                int firstSelectedIndex = hitObjectManager.hitObjects.FindIndex(h => h == selectedObject);
                int lastSelectedIndex = hitObjectManager.hitObjects.FindIndex(h => h == lastSelectedObject);

                if ((firstSelectedIndex > 0) && !(selectedObject is Spinner))
                {
                    HitObject prevObject = hitObjectManager.hitObjects[firstSelectedIndex - 1];

                    if (!(prevObject is Spinner))
                    {
                        int selectedTime = selectedObject.StartTime;
                        int time = selectedTime - prevObject.EndTime;
                        double mul = hitObjectManager.SliderVelocityAt(selectedTime);
                        if (time < 3000)
                        {
                            double distance = 1000.0d * (selectedObject.Position - prevObject.EndPosition).Length() / (mul * time);
                            spacingText += String.Format("\nPrev: {0:f2}x", distance);
                        }
                    }
                }

                if ((lastSelectedIndex < hitObjectManager.hitObjectsCount - 1) && (lastSelectedIndex >= 0) && !(lastSelectedObject is Spinner))
                {
                    HitObject nextObject = hitObjectManager.hitObjects[lastSelectedIndex + 1];

                    if (!(nextObject is Spinner))
                    {
                        int lastSelectedTime = lastSelectedObject.EndTime;
                        int nextSelectedTime = nextObject.StartTime;
                        int time = nextSelectedTime - lastSelectedTime;
                        double mul = hitObjectManager.SliderVelocityAt(nextSelectedTime);
                        if (time < 3000)
                        {
                            double distance = 1000.0d * (nextObject.Position - lastSelectedObject.EndPosition).Length() / (mul * time);
                            spacingText += String.Format("\nNext: {0:f2}x", distance);
                        }
                    }
                }

                coordinateText.Text = String.Format(@"x:{0:0} y:{1:0}", selectedObject.Position.X, selectedObject.Position.Y) + spacingText;
            }
            else if (CurrentComposeTool == ComposeTools.Normal || (CurrentComposeTool == ComposeTools.Slider && sliderPlacementCircle == null))
            {
                Vector2 pos = snapCoordinates;
                string spacingText = string.Empty;
                HitObject prevObject = hitObjectManager.hitObjects.FindLast(n => n.EndTime < AudioEngine.Time - 1);
                if (prevObject != null && !(prevObject is Spinner))
                {
                    int selectedTime = AudioEngine.Time;
                    int time = selectedTime - prevObject.EndTime;
                    double mul = hitObjectManager.SliderVelocityAt(selectedTime);
                    if (time < 3000)
                    {
                        double distance = 1000.0d * (pos - prevObject.EndPosition).Length() / (mul * time);
                        spacingText += String.Format("\nPrev: {0:f2}x", distance);
                    }
                }
                else
                    spacingText += "\n";

                HitObject nextObject = hitObjectManager.hitObjects.Find(n => n.StartTime > AudioEngine.Time + 1);
                if (nextObject != null && !(nextObject is Spinner))
                {
                    int selectedTime = AudioEngine.Time;
                    int nextSelectedTime = nextObject.StartTime;
                    int time = nextSelectedTime - selectedTime;
                    double mul = hitObjectManager.SliderVelocityAt(nextSelectedTime);
                    if (time < 3000)
                    {
                        double distance = 1000.0d * (nextObject.Position - pos).Length() / (mul * time);
                        spacingText += String.Format("\nNext: {0:f2}x", distance);
                    }
                }

                coordinateText.Text = String.Format(@"x:{0:0} y:{1:0}", pos.X, pos.Y) + spacingText;
            }
            else if (CurrentComposeTool == ComposeTools.Slider && sliderPlacementCircle != null)
            {
                /*   */
            }
            else
            {
                // Nothing is selected
                coordinateText.InitialColour = Color.White;
                coordinateText.Text = String.Format(@"x:{0:0} y:{1:0}", snapCoordinates.X, snapCoordinates.Y);
            }
        }

        internal void RefreshSelections()
        {
            sliderPlacementCircle = null;

            selectedObjects.Clear();
            selectedObjects = hitObjectManager.hitObjects.FindAll(h => h.Selected);

            selectionChanged = true;

            selectedSlider = null;
            selectedSliderIndex = -1;
            hoveredSlider = null;

        }

        private void ResetHoverVariables()
        {
            hoveredObject = null;
            hoveredSlider = null;
        }

        private void ResetAllSelections()
        {
            SelectNone();
        }

        List<HitObject> timelineVisibleSpanObjects = new List<HitObject>();
        pText timelineBreakText = new pText("Break", 16, Vector2.Zero, 1, true, Color.White);

        internal override void UpdateTimeline()
        {
            timelineVisibleSpanObjects.Clear();
            timelineBreakText.MeasureText(); //only really needs to be called once on init to create the texture. just a safety measure.

            foreach (EventBreak b in editor.eventManager.eventBreaks)
            {
                if (b.StartTime >= editor.TimelineLeftBound && b.StartTime <= editor.TimelineRightBound)
                {
                    float pos = editor.TimeToTimelinePosNoOffset(b.StartTime);

                    pText text = (pText)timelineBreakText.Clone();

                    text.Position = new Vector2(pos + 5, 35);
                    text.Alpha = 1;// Math.Min(1, (float)Math.Min(pos - editor.TimelineX, editor.TimelineWidth - (pos - editor.TimelineX)) / 40);

                    editor.timelineManager.Add(text);
                }
            }

            int lastTime = -60000;
            int multiTouchCount = 0;

            foreach (HitObject h in hitObjectManager.hitObjectsMinimal)
            {
                if (h == currentPlacementObject) continue;

                SliderOsu s = h as SliderOsu;

                bool startInRange = h.StartTime >= editor.TimelineLeftBound && h.StartTime <= editor.TimelineRightBound;
                bool endInRange = h.EndTime != h.StartTime && h.EndTime >= editor.TimelineLeftBound &&
                                  h.EndTime <= editor.TimelineRightBound;

                if (h.Length > 0
                    && (startInRange || endInRange)
                    //For sliders longer than the timeline
                    || (h.StartTime < editor.TimelineLeftBound && h.EndTime > editor.TimelineRightBound))
                {
                    timelineVisibleSpanObjects.Add(h);
                }

                if (h.StartTime == lastTime)
                    multiTouchCount++;
                else
                    multiTouchCount = 0;
                lastTime = h.StartTime;
                int ypos = 45 - 2 * multiTouchCount;
                int mtZBias = 3 * multiTouchCount;
                int delta = 0;
                ypos += delta;

                if (startInRange)
                {
                    float pos = editor.TimeToTimelinePosNoOffset(h.StartTime);
                    byte alpha =
                        (byte)
                        Math.Min(255,
                                 255 *
                                 ((double)
                                  Math.Min(pos - editor.TimelineX, editor.TimelineWidth - (pos - editor.TimelineX))) / 40);
                    Color whiteColour = new Color(255, 255, 255, alpha);
                    Color circleColour = new Color(h.Colour.R, h.Colour.G, h.Colour.B, alpha);
                    Color redColour = new Color(255, 0, 0, alpha);

                    pSprite spriteHitCircle1 =
                        new pSprite(SkinManager.Load(@"hitcircle"), Fields.TopLeft,
                                    Origins.Centre, Clocks.Audio,
                                    new Vector2(pos, ypos), SpriteManager.drawOrderBwd(h.StartTime + mtZBias), true,
                                    circleColour, h);
                    spriteHitCircle1.Scale = 0.4F;
                    pSprite spriteHitCircle2 =
                        new pSprite(SkinManager.Load(@"hitcircleoverlay"), Fields.TopLeft,
                                    Origins.Centre, Clocks.Audio,
                                    new Vector2(pos, ypos), SpriteManager.drawOrderBwd(h.StartTime - 1 + mtZBias), true,
                                    whiteColour);
                    spriteHitCircle2.Scale = 0.4F;
                    pSprite spriteHitCircleText =
                        new pSpriteText(h.ComboNumber.ToString(), SkinManager.Current.FontHitCircle,
                                        SkinManager.Current.FontHitCircleOverlap, Fields.TopLeft,
                                        Origins.Centre, Clocks.Audio,
                                        new Vector2(pos, 45 - 3 * multiTouchCount + delta), SpriteManager.drawOrderBwd(h.StartTime - 2 + mtZBias), true,
                                        whiteColour, false);
                    spriteHitCircleText.Scale = 0.32F;

                    if (h.Selected)
                    {
                        pSprite spriteHitCircleSelect =
                            new pSprite(SkinManager.Load(@"hitcircleselect"),
                                        Fields.TopLeft,
                                        Origins.Centre, Clocks.Audio,
                                        new Vector2(pos, ypos), SpriteManager.drawOrderBwd(h.StartTime - 3 + mtZBias),
                                        true,
                                        s != null && s.sliderStartCircle.Selected
                                            ? redColour
                                            : whiteColour);
                        spriteHitCircleSelect.Scale = 0.4F;
                        editor.timelineManager.Add(spriteHitCircleSelect);
                    }

                    editor.timelineManager.Add(spriteHitCircle1);
                    editor.timelineManager.Add(spriteHitCircle2);
                    editor.timelineManager.Add(spriteHitCircleText);
                }

                if (endInRange)
                {
                    float pos = editor.TimeToTimelinePosNoOffset(h.EndTime);
                    byte alpha =
                        (byte)
                        Math.Min(255,
                                 255 *
                                 ((double)
                                  Math.Min(pos - editor.TimelineX, editor.TimelineWidth - (pos - editor.TimelineX))) /
                                 40);
                    Color whiteColour = new Color(255, 255, 255, alpha);
                    Color circleColour = new Color(h.Colour.R, h.Colour.G, h.Colour.B, alpha);
                    Color redColour = new Color(255, 0, 0, alpha);

                    pSprite spriteHitCircle1 =
                        new pSprite(SkinManager.Load(@"hitcircle"), Fields.TopLeft,
                                    Origins.Centre, Clocks.Audio,
                                    new Vector2(pos, ypos), SpriteManager.drawOrderBwd(h.EndTime + mtZBias), true,
                                    circleColour, h);
                    spriteHitCircle1.Scale = 0.4F;
                    pSprite spriteHitCircle2 =
                        new pSprite(SkinManager.Load(@"hitcircleoverlay"), Fields.TopLeft,
                                    Origins.Centre, Clocks.Audio,
                                    new Vector2(pos, ypos), SpriteManager.drawOrderBwd(h.EndTime - 1 + mtZBias), true,
                                    whiteColour);
                    spriteHitCircle2.Scale = 0.4F;

                    if (h.Selected)
                    {
                        pSprite spriteHitCircleSelect =
                            new pSprite(SkinManager.Load(@"hitcircleselect"),
                                        Fields.TopLeft,
                                        Origins.Centre, Clocks.Audio,
                                        new Vector2(pos, ypos), SpriteManager.drawOrderBwd(h.EndTime - 3 + mtZBias),
                                        true,
                                        s != null && s.sliderEndCircles.Count > 0 &&
                                        s.sliderEndCircles[s.sliderEndCircles.Count - 1].Selected
                                            ? redColour
                                            : whiteColour);
                        spriteHitCircleSelect.Scale = 0.4F;
                        editor.timelineManager.Add(spriteHitCircleSelect);
                    }

                    editor.timelineManager.Add(spriteHitCircle1);
                    editor.timelineManager.Add(spriteHitCircle2);
                }

                if (s != null &&
                    (startInRange || endInRange ||
                     (h.StartTime < editor.TimelineLeftBound && h.EndTime >= editor.TimelineRightBound)))
                {
                    //Draw reversing circles
                    for (int k = 0; k < s.sliderRepeatPoints.Count; k++)
                    {
                        int j = s.sliderRepeatPoints[k];

                        if (j >= editor.TimelineLeftBound && j <= editor.TimelineRightBound)
                        {
                            float pos = editor.TimeToTimelinePosNoOffset(j);
                            byte alpha = (byte)Math.Min(255, 255 * ((double)Math.Min(pos - editor.TimelineX, editor.TimelineWidth - (pos - editor.TimelineX))) / 40);
                            Color whiteColour = new Color(255, 255, 255, alpha);
                            Color circleColour = new Color(h.Colour.R, h.Colour.G, h.Colour.B, alpha);
                            Color blackColour = new Color(0, 0, 0, alpha);
                            Color redColour = new Color(255, 0, 0, alpha);
                            Color reverseColour;

                            pSprite spriteHitCircle1 =
                                new pSprite(SkinManager.Load(@"hitcircle"), Fields.TopLeft,
                                            Origins.Centre, Clocks.Audio,
                                            new Vector2(pos, ypos), SpriteManager.drawOrderBwd(j + mtZBias), true,
                                            circleColour, h);
                            spriteHitCircle1.Scale = 0.4F;
                            pSprite spriteHitCircle2 =
                                new pSprite(SkinManager.Load(@"hitcircleoverlay"),
                                            Fields.TopLeft,
                                            Origins.Centre, Clocks.Audio,
                                            new Vector2(pos, ypos), SpriteManager.drawOrderBwd(j - 1 + mtZBias), true,
                                            whiteColour);
                            spriteHitCircle2.Scale = 0.4F;
                            if (spriteHitCircle1.Texture.Source == SkinSource.Osu && h.Colour.R + h.Colour.G + h.Colour.B > 600)
                                reverseColour = blackColour;
                            else
                                reverseColour = whiteColour;
                            pSprite spriteHitArrow =
                                new pSprite(SkinManager.Load(@"reversearrow"),
                                            Fields.TopLeft,
                                            Origins.Centre, Clocks.Audio,
                                            new Vector2(pos, ypos), SpriteManager.drawOrderBwd(j - 2 + mtZBias), true,
                                            reverseColour);
                            spriteHitArrow.Scale = 0.4F;

                            editor.timelineManager.Add(spriteHitArrow);

                            if (k < s.sliderEndCircles.Count && s.sliderEndCircles[k].Selected)
                            {
                                pSprite spriteHitCircleSelect =
                                    new pSprite(SkinManager.Load(@"hitcircleselect"),
                                                Fields.TopLeft,
                                                Origins.Centre, Clocks.Audio,
                                                new Vector2(pos, ypos), SpriteManager.drawOrderBwd(j - 3 + mtZBias),
                                                true,
                                                redColour);
                                spriteHitCircleSelect.Scale = 0.4F;
                                editor.timelineManager.Add(spriteHitCircleSelect);
                            }

                            editor.timelineManager.Add(spriteHitCircle1);
                            editor.timelineManager.Add(spriteHitCircle2);
                        }
                    }
                }
            }
        }

        internal override void DrawTimelineLow()
        {
            List<Line> linelist = new List<Line>();

            foreach (HitObject o in timelineVisibleSpanObjects)
            {
                float startPos = editor.TimeToTimelinePosNoOffset(o.StartTime);
                float endPos = editor.TimeToTimelinePosNoOffset(o.EndTime);
                byte alpha = 170;
                if (startPos > (editor.TimelineX + editor.TimelineWidth / 2))
                {
                    alpha =
                            (byte)
                            Math.Min(170,
                                     170 *
                                     ((double)
                                     Math.Min(startPos - editor.TimelineX, editor.TimelineWidth - (startPos - editor.TimelineX))) / 40);
                }
                else if (endPos < (editor.TimelineX + editor.TimelineWidth / 2))
                {
                    alpha =
                            (byte)
                            Math.Min(170,
                                     170 *
                                     ((double)
                                     Math.Min(endPos - editor.TimelineX, editor.TimelineWidth - (endPos - editor.TimelineX))) / 40);
                }

                //Convert to real coordinates
                float radius = (HitObjectManager.GamefieldSpriteRes / 8f) * GameBase.WindowRatio - 1;
                startPos = Math.Max(startPos * GameBase.WindowRatio, editor.TimelineRectangle.Left + radius);
                endPos = Math.Min(endPos * GameBase.WindowRatio, editor.TimelineRectangle.Right - radius);
                if (startPos > endPos)
                    continue;

                linelist.Clear();
                linelist.Add(new Line(new Vector2(startPos
                                                  , editor.TimelineRectangle.Top + (3 * GameBase.WindowRatio)
                                                  + editor.TimelineRectangle.Height / 2f)
                                      , new Vector2(endPos
                                                    , editor.TimelineRectangle.Top + (3 * GameBase.WindowRatio)
                                                    + editor.TimelineRectangle.Height / 2f)));

                GameBase.LineManager.Draw(linelist, radius,
                                          new Color(o.Colour.R, o.Colour.G, o.Colour.B,
                                                    (byte)(o.Selected ? 255 : alpha)), 0, @"Standard",
                                          true);
            }


            foreach (EventBreak o in editor.eventManager.eventBreaks)
            {
                int before = 0, after = 0;

                try
                {
                    int index = hitObjectManager.BreakObjectAfter(o);
                    before = hitObjectManager.hitObjects[index - 1].EndTime;
                    if (index < hitObjectManager.hitObjectsCount)
                        after = hitObjectManager.hitObjects[index].StartTime;
                    else
                        after = o.EndTime + hitObjectManager.PreEmpt;
                }
                catch (ArgumentOutOfRangeException)
                {
                    // thrown if the map contains broken breaks.
                    before = o.StartTime;
                    after = o.EndTime + hitObjectManager.PreEmpt;
                }

                if ((editor.TimelineLeftBound <= before && editor.TimelineRightBound >= before) ||
                    (editor.TimelineLeftBound <= after && editor.TimelineRightBound >= after) ||
                    (editor.TimelineLeftBound >= before && editor.TimelineRightBound <= after))
                {
                    linelist.Clear();

                    Color colour_start, colour_end;

                    if (o.EndTime - o.StartTime < 50)
                    {
                        colour_start = new Color(255, 0, 0, 150);
                        colour_end = new Color(255, 0, 0, 150);
                    }
                    else
                    {
                        colour_start = o.CustomStart ? new Color(40, 100, 100, 100) : new Color(100, 100, 100, 100);
                        colour_end = o.CustomEnd ? new Color(40, 100, 100, 100) : new Color(100, 100, 100, 100);
                    }

                    // Main break region
                    linelist.Add(
                        new Line(
                            new Vector2(editor.TimeToTimelinePos(o.StartTime),
                                        editor.TimelineRectangle.Top + (3 * GameBase.WindowRatio) +
                                        editor.TimelineRectangle.Height / 2),
                            new Vector2(editor.TimeToTimelinePos(o.EndTime),
                                        editor.TimelineRectangle.Top + (3 * GameBase.WindowRatio) +
                                        editor.TimelineRectangle.Height / 2)));

                    // Break end region
                    linelist.Add(
                        new ColouredLine(
                            new Vector2(editor.TimeToTimelinePos(o.EndTime),
                                        editor.TimelineRectangle.Top + (3 * GameBase.WindowRatio) +
                                        editor.TimelineRectangle.Height / 2),
                            new Vector2(editor.TimeToTimelinePos(after),
                                        editor.TimelineRectangle.Top + (3 * GameBase.WindowRatio) +
                                        editor.TimelineRectangle.Height / 2), colour_end));

                    // Break start region
                    linelist.Add(
                        new ColouredLine(
                            new Vector2(editor.TimeToTimelinePos(before),
                                        editor.TimelineRectangle.Top + (3 * GameBase.WindowRatio) +
                                        editor.TimelineRectangle.Height / 2),
                            new Vector2(editor.TimeToTimelinePos(o.StartTime),
                                        editor.TimelineRectangle.Top + (3 * GameBase.WindowRatio) +
                                        editor.TimelineRectangle.Height / 2), colour_start));

                    GameBase.LineManager.Draw(linelist,
                                              (HitObjectManager.GamefieldSpriteRes / 8) * GameBase.WindowRatio - 1,
                                              new Color(100, 100, 100, 150), 0, @"NoBlur",
                                              false);
                }
            }
        }

        internal override void Enter()
        {
            GameBase.ResizeGamefield(1, false);
            editor.TimelineTickHeight = 12;
            //editor.TimelineMagnification = 1;
            beatDivisorSlider.Visible = true;

            //We move the storyboard here to align with the new gamefield location.
            //editor.eventManager.spriteManagerBGWide.ViewOffset = new Vector2(0, -13);
            //editor.eventManager.spriteManagerBG.ViewOffset = new Vector2(0, -13);
            //editor.eventManager.spriteManagerFG.ViewOffset = new Vector2(0, -13);

            beatDivisorSlider.Visible = true;

            base.Enter();
        }

        internal override void Leave()
        {
            SliderPlacementEnd(true);
            SpinnerPlacementEnd();
            ResetAllSelections();

            InputManager.DragFinish();

            beatDivisorSlider.Visible = false;

            base.Leave();
        }

        internal override void Dispose()
        {
            ConfigManager.sDistanceSpacing.Value = distanceSpacingFactor;
            if (SoundAdditionStatus != null)
                ConfigManager.sDistanceSpacingEnabled.Value = SoundAdditionStatus[SoundAdditions.DistSnap];

            if (comboColour != null)
                comboColour.Dispose();

            KeyboardHandler.OnKeyReleased -= KeyboardHandler_OnKeyReleased;

            base.Dispose();
        }

        internal override bool OnDoubleClick()
        {
            if (!Enabled)
                return false;

            if (selectedObjects.Count > 0 &&
                ((editor.InGamefield && ComposeTools.Select == CurrentComposeTool) || editor.InTimeline))
            {
                AudioEngine.SeekTo(selectedObjects[0].StartTime);
                return true;
            }

            return false;
        }


        internal override void Draw()
        {
            base.Draw();

            if (snapLine != null)
            {
                GameBase.primitiveBatch.Begin();
                Color startColour = Color.White;

                int amount = Math.Abs((GameBase.Time % 600) - 300);

                byte bamount = (byte)(255 * (amount / 300f));

                GameBase.primitiveBatch.AddVertex(snapLine.p1, new Color(startColour.R, startColour.G, startColour.B, bamount));
                GameBase.primitiveBatch.AddVertex(snapLine.p2, new Color(startColour.R, startColour.G, startColour.B, 0));
                GameBase.primitiveBatch.End();

                snapLine = null;
            }

            if (hoveredSlider != null && (CurrentComposeTool == ComposeTools.Select || sliderPlacementCircle != null))
            {
                for (int i = 0; i < hoveredSlider.sliderCurvePoints.Count; i++)
                {
                    bool special = i > 0 &&
                                   hoveredSlider.sliderCurvePoints[i] == hoveredSlider.sliderCurvePoints[i - 1];
                    Vector2 scrPos = GameBase.GamefieldToDisplay(hoveredSlider.sliderCurvePoints[i]);
                    GameBase.LineManager.DrawQuad(scrPos - Vector2.One * 4,
                                                  scrPos + Vector2.One * 4,
                                                  fillColour2);
                    GameBase.LineManager.DrawQuad(scrPos - Vector2.One * 3,
                                                  scrPos + Vector2.One * 3,
                                                  (special
                                                       ? fillColour4
                                                       : i == selectedSliderIndex ? fillColour3 : fillColour));
                }

                //Need a second loop so the primitiveBatch doesnt get interference.
                GameBase.primitiveBatch.Begin();
                for (int i = 1; i < hoveredSlider.sliderCurvePoints.Count; i++)
                {
                    GameBase.primitiveBatch.AddVertex(
                        GameBase.GamefieldToDisplay(hoveredSlider.sliderCurvePoints[i - 1]),
                        (i - 1 == selectedSliderIndex ? fillColour3 : fillColour));
                    GameBase.primitiveBatch.AddVertex(
                        GameBase.GamefieldToDisplay(hoveredSlider.sliderCurvePoints[i]),
                        (i == selectedSliderIndex ? fillColour3 : fillColour));
                }
                GameBase.primitiveBatch.End();
            }
        }


        internal override void DrawTimeline()
        {
            if (dragSelection)
            {
                if (editor.DragCaptured == DragCapture.Gamefield)
                {
                    if (DragPoint1 != DragPoint2)
                    {
                        //Draw the drag box
                        Color lineColourLocal = new Color(255, 255, 255, 180);
                        Vector2 dragPoint3 = new Vector2(DragPoint2.X, DragPoint1.Y);
                        Vector2 dragPoint4 = new Vector2(DragPoint1.X, DragPoint2.Y);

                        GameBase.primitiveBatch.Begin();

                        GameBase.primitiveBatch.AddVertex(DragPoint1, lineColourLocal);
                        GameBase.primitiveBatch.AddVertex(dragPoint3, lineColourLocal);
                        GameBase.primitiveBatch.AddVertex(dragPoint3, lineColourLocal);
                        GameBase.primitiveBatch.AddVertex(DragPoint2, lineColourLocal);
                        GameBase.primitiveBatch.AddVertex(DragPoint2, lineColourLocal);
                        GameBase.primitiveBatch.AddVertex(dragPoint4, lineColourLocal);
                        GameBase.primitiveBatch.AddVertex(dragPoint4, lineColourLocal);
                        GameBase.primitiveBatch.AddVertex(DragPoint1, lineColourLocal);

                        GameBase.primitiveBatch.End();

                        GameBase.LineManager.DrawQuad(DragPoint1, DragPoint2, colourDragBoxFill);
                    }
                }
                else if (editor.DragCaptured == DragCapture.Timeline)
                {
                    if (timelineDrag1 != timelineDrag2)
                    {
                        float x1 = Math.Max(editor.TimelineRectangle.Left,
                                            editor.TimeToTimelinePos(Math.Min(timelineDrag1, timelineDrag2)));
                        float x2 = Math.Min(editor.TimelineRectangle.Right,
                                            editor.TimeToTimelinePos(Math.Max(timelineDrag1, timelineDrag2)));

                        if (x1 != x2)
                            GameBase.LineManager.DrawQuad(new Vector2(x1, editor.TimelineRectangle.Top),
                                                          new Vector2(x2, editor.TimelineRectangle.Bottom),
                                                          colourDragBoxTimeline);
                    }
                }
            }

            base.DrawTimeline();
        }

        internal void addLiveMapObject(bool isClap)
        {
            editor.UndoPush();
            HitCircleOsu h = new HitCircleOsu(hitObjectManager, snapCoordinates, editor.GetCurrentSnappedTime(), false, isClap ? HitObjectSoundType.Clap : HitObjectSoundType.Normal, 0);
            hitObjectManager.Add(h, true);
            hitObjectManager.Sort(true);
        }

        internal void toggleLiveMapping()
        {
            editor.liveMapping = !editor.liveMapping;
            if (editor.liveMapping) ChangeComposeTool(ComposeTools.Normal);
            NotificationManager.ShowMessageMassive(editor.liveMapping ? LocalisationManager.GetString(OsuString.EditorModeCompose_LiveMappingEnabled) : LocalisationManager.GetString(OsuString.EditorModeCompose_LiveMappingDisabled), 3000);
        }

        internal override bool OnKey(Keys keys, bool first)
        {
            if (ChatEngine.IsVisible)
                return false;

            if (first)
            {
                Bindings taikoKey = BindingManager.For(keys);

                if (BeatmapManager.Current.PlayMode != PlayModes.OsuMania && editor.liveMapping && editor.InGamefield && AudioEngine.AudioState != AudioStates.Stopped)
                {

                    switch (taikoKey)
                    {
                        case Bindings.TaikoInnerLeft:
                            addLiveMapObject(false);
                            return true;
                        case Bindings.TaikoInnerRight:
                            addLiveMapObject(false);
                            return true;
                        case Bindings.TaikoOuterLeft:
                            addLiveMapObject(true);
                            return true;
                        case Bindings.TaikoOuterRight:
                            addLiveMapObject(true);
                            return true;
                    }
                }

                if (KeyboardHandler.ShiftPressed)
                {
                    switch (keys)
                    {
                        //Universal Shift+Key
                        case Keys.D1:
                            ChangeSnapDivisor(1);
                            return true;
                        case Keys.D2:
                            ChangeSnapDivisor(2);
                            return true;
                        case Keys.D3:
                            ChangeSnapDivisor(3);
                            return true;
                        case Keys.D4:
                            ChangeSnapDivisor(4);
                            return true;
                        case Keys.D6:
                            ChangeSnapDivisor(6);
                            return true;
                        case Keys.D8:
                            ChangeSnapDivisor(8);
                            return true;
                        case Keys.Q:
                            ddSampleSet.SetSelected(SampleSet.None, false);
                            return true;
                        case Keys.W:
                            ddSampleSet.SetSelected(SampleSet.Normal, false);
                            return true;
                        case Keys.E:
                            ddSampleSet.SetSelected(SampleSet.Soft, false);
                            return true;
                        case Keys.R:
                            if (KeyboardHandler.ControlPressed)
                                editor.ShowRotateBy();
                            else
                                ddSampleSet.SetSelected(SampleSet.Drum, false);
                            return true;
                    }
                }
                else if (KeyboardHandler.ControlPressed)
                {
                    switch (keys)
                    {
                        case Keys.M:
                            ChangeSnapDivisor((editor.beatSnapDivisor * 2) % 15);
                            return true;
                        case Keys.G:
                            ReverseSelection();
                            return true;
                        case Keys.OemComma:
                            RotateSelectionAnticlockwise();
                            return true;
                        case Keys.OemPeriod:
                            RotateSelectionClockwise();
                            return true;
                        case Keys.H:
                            FlipSelectionHorizontal();
                            return true;
                        case Keys.J:
                            FlipSelectionVertical();
                            return true;
                        case Keys.D:
                            CloneSelection();
                            return true;
                        case Keys.V:
                            PasteSelection();
                            return true;
                        case Keys.X:
                            CutSelection();
                            return true;
                        case Keys.A:
                            SelectAll();
                            return true;
                        case Keys.Q:
                            ddSampleSetAddition.SetSelected(SampleSet.None, false);
                            return true;
                        case Keys.W:
                            ddSampleSetAddition.SetSelected(SampleSet.Normal, false);
                            return true;
                        case Keys.E:
                            ddSampleSetAddition.SetSelected(SampleSet.Soft, false);
                            return true;
                        case Keys.R:
                            ddSampleSetAddition.SetSelected(SampleSet.Drum, false);
                            return true;
                        case Keys.Tab:
                            toggleLiveMapping();
                            return true;
                    }
                }
                else
                {
                    Bindings b = BindingManager.For(keys, BindingTarget.Editor);
                    switch (b)
                    {
                        case Bindings.NoteLockToggle:
                            ChangeSoundAddition(SoundAdditions.NoteLock);
                            return true;
                        case Bindings.DistSnapToggle:
                            ChangeSoundAddition(SoundAdditions.DistSnap);
                            return true;
                        case Bindings.SelectTool:
                            ChangeComposeTool(ComposeTools.Select);
                            return true;
                        case Bindings.NormalTool:
                            ChangeComposeTool(ComposeTools.Normal);
                            return true;
                        case Bindings.SliderTool:
                            ChangeComposeTool(ComposeTools.Slider);
                            return true;
                        case Bindings.SpinnerTool:
                            ChangeComposeTool(ComposeTools.Spinner);
                            return true;
                        case Bindings.NewComboToggle:
                            ChangeSoundAddition(SoundAdditions.NewCombo);
                            return true;
                        case Bindings.WhistleToggle:
                            ChangeSoundAddition(SoundAdditions.Whistle);
                            return true;
                        case Bindings.FinishToggle:
                            ChangeSoundAddition(SoundAdditions.Finish);
                            return true;
                        case Bindings.ClapToggle:
                            ChangeSoundAddition(SoundAdditions.Clap);
                            return true;
                        case Bindings.GridSnapToggle:
                            ChangeSoundAddition(SoundAdditions.GridSnap);
                            return true;
                        case Bindings.NudgeLeft:
                            NudgeLeft();
                            return true;
                        case Bindings.NudgeRight:
                            NudgeRight();
                            return true;
                    }
                    switch (keys)
                    {
                        case Keys.Delete:
                            DeleteSelection();
                            return true;
                        case Keys.Add:
                            NudgeRightMultitouch();
                            return true;
                        case Keys.Subtract:
                            NudgeLeftMultitouch();
                            return true;
                    }
                }
            }

            // using keyboard to move notes, we want this to be repeatable
            if (selectedObjects != null && selectedObjects.Count != 0 && KeyboardHandler.ControlPressed && !isTaiko)
            {
                float move = 1;
                float thisX = selectedObjects[0].Position.X;
                float thisY = selectedObjects[0].Position.Y;

                if (applyGridSnap)
                {
                    move = (editor.GridSize / 2) + 0.1f; // rough aim towards nearest grid, snapped later
                }

                switch (keys)
                {
                    case Keys.Up:
                        AttemptMove(thisX, thisY - move, true);
                        return true;
                    case Keys.Down:
                        AttemptMove(thisX, thisY + move, true);
                        return true;
                    case Keys.Left:
                        AttemptMove(thisX - move, thisY, true);
                        return true;
                    case Keys.Right:
                        AttemptMove(thisX + move, thisY, true);
                        return true;
                }
            }

            return base.OnKey(keys, first);
        }

        private bool KeyboardHandler_OnKeyReleased(object sender, Keys k)
        {
            if (undoKeysPressed)
            {
                // if arrow keys are being released, make sure no other arrows are still active.
                switch (k)
                {
                    case Keys.Up:
                    case Keys.Down:
                    case Keys.Left:
                    case Keys.Right:
                        if (!KeyboardHandler.IsKeyDown(Keys.Up) && !KeyboardHandler.IsKeyDown(Keys.Down) && !KeyboardHandler.IsKeyDown(Keys.Left) && !KeyboardHandler.IsKeyDown(Keys.Right))
                        {
                            undoKeysPressed = false;
                        }
                        return true;
                }
            }

            return false;
        }

        private void NudgeLeftMultitouch()
        {
            List<HitObject> hitObjects = hitObjectManager.hitObjects;
            for (int x = 1; x < hitObjects.Count; x++)
            {
                if (hitObjects[x].Selected && hitObjects[x - 1].CompareTo(hitObjects[x]) == 0)
                {
                    HitObject h = hitObjects[x];
                    hitObjects[x] = hitObjects[x - 1];
                    hitObjects[x - 1] = h;
                }
            }
            hitObjectManager.Sort(false);
        }

        private void NudgeRightMultitouch()
        {
            List<HitObject> hitObjects = hitObjectManager.hitObjects;
            for (int x = 0; x < hitObjects.Count - 1; x++)
            {
                if (hitObjects[x].Selected && hitObjects[x].CompareTo(hitObjects[x + 1]) == 0)
                {
                    HitObject h = hitObjects[x + 1];
                    hitObjects[x + 1] = hitObjects[x];
                    hitObjects[x] = h;
                }
            }
            hitObjectManager.Sort(false);
        }

        internal virtual void CutSelection()
        {
            if (selectedObjects.Count == 0)
                return;

            if (editor.IsDragging)
            {
                HandleDrag();
                ignoreDrag = resetDrag;
            }

            CopySelection();
            DeleteSelection();
        }

        internal void NudgeLeft()
        {
            if (selectedObjects.Count == 0)
                return;
            editor.UndoPush();
            editor.Timing.beatShiftCircle(-1);
        }

        internal void NudgeRight()
        {
            if (selectedObjects.Count == 0)
                return;
            editor.UndoPush();
            editor.Timing.beatShiftCircle(1);
        }

        internal void ReverseSelection()
        {
            if (selectedObjects.Count == 0)
                return;

            selectedObjects.Sort();

            editor.UndoPush();

            HandleDrag();
            ignoreDragSelect = resetDrag;
            int highestEndTime = selectedObjects[0].EndTime;
            int lowestStartTime = selectedObjects[0].StartTime;

            bool[] newCombo = new bool[selectedObjects.Count];

            for (int i = 0; i < selectedObjects.Count; i++)
            {
                newCombo[i] = selectedObjects[i].NewCombo;

                if (selectedObjects[i].EndTime > highestEndTime)
                    highestEndTime = selectedObjects[i].EndTime;
                if (selectedObjects[i].StartTime < lowestStartTime)
                    lowestStartTime = selectedObjects[i].StartTime;
            }

            bool isMania = BeatmapManager.Current.PlayMode == PlayModes.OsuMania;
            int beatSnapStart, beatSnapEnd;
            int count = 1;
            foreach (HitObject h in selectedObjects)
            {
                h.NewCombo = newCombo[selectedObjects.Count - count];
                count++;

                int newStart = lowestStartTime + (highestEndTime - h.EndTime);
                if (!isMania)
                {
                    h.ModifyTime(newStart);
                    if (h.IsType(HitObjectType.Slider))
                    {
                        ((SliderOsu)h).Reverse();
                    }
                }
                else
                {
                    beatSnapStart = editor.Timing.BeatSnapClosestValue(newStart);

                    if (Math.Abs(beatSnapStart - newStart) == 1)
                        newStart = beatSnapStart;

                    h.ModifyTime(newStart);

                    if (h.IsType(HitObjectType.Hold))
                    {
                        beatSnapEnd = editor.Timing.BeatSnapClosestValue(h.EndTime);

                        if (Math.Abs(beatSnapEnd - h.EndTime) == 1)
                            h.SetEndTime(beatSnapEnd);
                    }
                }
            }

            selectionChanged = true;
            hitObjectManager.Sort(true);
        }

        internal void CloneSelection()
        {
            if (selectedObjects.Count == 0)
                return;

            HandleDrag();
            ignoreDragSelect = resetDrag;
            List<HitObject> clonedObjects = new List<HitObject>();

            int highestEndTime = selectedObjects[0].EndTime;
            int lowestStartTime = selectedObjects[0].StartTime;

            for (int i = 0; i < selectedObjects.Count; i++)
            {
                if (selectedObjects[i].EndTime > highestEndTime)
                    highestEndTime = selectedObjects[i].EndTime;
                if (selectedObjects[i].StartTime < lowestStartTime)
                    lowestStartTime = selectedObjects[i].StartTime;
            }

            highestEndTime = editor.Timing.BeatSnapValue(highestEndTime + AudioEngine.beatLength);

            foreach (HitObject h in selectedObjects)
            {
                HitObject clone = h.Clone();

                if (h is SliderOsu)
                    ((SliderOsu)clone).ModifySliderTime(highestEndTime + (h.StartTime - lowestStartTime), false);
                else
                    clone.ModifyTime(highestEndTime + (h.StartTime - lowestStartTime));

                clonedObjects.Add(clone);
                hitObjectManager.Add(clone, true);
            }

            SelectNone();

            selectedObjects = clonedObjects;

            DoSelection();

            hitObjectManager.Sort(false);
        }


        internal virtual void FlipSelectionHorizontal()
        {
            if (selectedObjects.Count == 0)
                return;
            editor.UndoPush();

            HandleDrag();
            ignoreDragSelect = resetDrag;
            foreach (HitObject h in selectedObjects)
            {
                if (h.IsType(HitObjectType.Slider))
                {
                    SliderOsu s = (SliderOsu)h;
                    for (int i = 0; i < s.sliderCurvePoints.Count; i++)
                        s.sliderCurvePoints[i] =
                            new Vector2(GameBase.GamefieldDefaultWidth - s.sliderCurvePoints[i].X,
                                        s.sliderCurvePoints[i].Y);
                    s.sliderStartCircle.ModifyPosition(new Vector2(GameBase.GamefieldDefaultWidth - h.Position.X,
                                                                   h.Position.Y));
                    s.Position = new Vector2(GameBase.GamefieldDefaultWidth - h.Position.X, h.Position.Y);
                    s.UpdateCalculations();
                }
                else
                    h.ModifyPosition(new Vector2(GameBase.GamefieldDefaultWidth - h.Position.X, h.Position.Y));
            }
        }

        internal void RotateSelectionAnticlockwise()
        {
            if (selectedObjects.Count == 0)
                return;
            editor.UndoPush();

            HandleDrag();
            ignoreDragSelect = resetDrag;
            Vector2 origin = new Vector2(GameBase.GamefieldDefaultWidth / 2, GameBase.GamefieldDefaultHeight / 2);

            foreach (HitObject h in selectedObjects)
            {
                Vector2 newPos =
                    new Vector2((h.Position.Y - origin.Y) + origin.X, -(h.Position.X - origin.X) + origin.Y);
                if (h.IsType(HitObjectType.Slider))
                {
                    SliderOsu s = (SliderOsu)h;
                    for (int i = 0; i < s.sliderCurvePoints.Count; i++)
                        s.sliderCurvePoints[i] =
                            new Vector2((s.sliderCurvePoints[i].Y - origin.Y) + origin.X,
                                        -(s.sliderCurvePoints[i].X - origin.X) + origin.Y);
                    s.sliderStartCircle.ModifyPosition(newPos);
                    s.Position = newPos;
                    s.UpdateCalculations();
                }
                else
                    h.ModifyPosition(newPos);
            }
        }

        internal void RotateSelectionClockwise()
        {
            if (selectedObjects.Count == 0)
                return;
            editor.UndoPush();

            HandleDrag();
            ignoreDragSelect = resetDrag;
            Vector2 origin = new Vector2(GameBase.GamefieldDefaultWidth / 2, GameBase.GamefieldDefaultHeight / 2);

            foreach (HitObject h in selectedObjects)
            {
                Vector2 newPos =
                    new Vector2(-(h.Position.Y - origin.Y) + origin.X, (h.Position.X - origin.X) + origin.Y);
                if (h.IsType(HitObjectType.Slider))
                {
                    SliderOsu s = (SliderOsu)h;
                    for (int i = 0; i < s.sliderCurvePoints.Count; i++)
                        s.sliderCurvePoints[i] =
                            new Vector2(-(s.sliderCurvePoints[i].Y - origin.Y) + origin.X,
                                        (s.sliderCurvePoints[i].X - origin.X) + origin.Y);
                    s.sliderStartCircle.ModifyPosition(newPos);
                    s.Position = newPos;
                    s.UpdateCalculations();
                }
                else
                    h.ModifyPosition(newPos);
            }
        }

        internal void RotateSelectionArbitrary(int angle, bool playfieldOrigin)
        {
            if (selectedObjects.Count == 0)
                return;

            Vector2 origin = GetOrigin(playfieldOrigin);

            foreach (HitObject h in selectedObjects)
            {
                Vector2 newPos = rotate(h.Position, origin, angle);

                if (h.IsType(HitObjectType.Slider))
                {
                    SliderOsu s = (SliderOsu)h;
                    for (int i = 0; i < s.sliderCurvePoints.Count; i++)
                        s.sliderCurvePoints[i] = rotate(s.sliderCurvePoints[i], origin, angle);
                    s.sliderStartCircle.ModifyPosition(newPos);
                    s.Position = newPos;
                    s.UpdateCalculations();
                }
                else
                    h.ModifyPosition(newPos);
            }
        }

        private static Vector2 rotate(Vector2 p, Vector2 center, int angle)
        {
            angle = -angle;

            p.X -= center.X;
            p.Y -= center.Y;

            Vector2 ret;
            ret.X = (float)(p.X * Math.Cos(angle / 180f * Math.PI) + p.Y * Math.Sin(angle / 180f * Math.PI));
            ret.Y = (float)(p.X * -Math.Sin(angle / 180f * Math.PI) + p.Y * Math.Cos(angle / 180f * Math.PI));

            ret.X += center.X;
            ret.Y += center.Y;

            return ret;
        }

        internal void ScaleSelection(Vector2 factor, bool playfieldOrigin)
        {
            if (selectedObjects.Count == 0)
                return;

            Vector2 origin = GetOrigin(playfieldOrigin);

            foreach (HitObject h in selectedObjects)
            {
                Vector2 newPos = scale(h.Position, origin, factor);

                if (h.IsType(HitObjectType.Slider))
                {
                    SliderOsu s = (SliderOsu)h;

                    for (int i = 0; i < s.sliderCurvePoints.Count; i++)
                        s.sliderCurvePoints[i] = scale(s.sliderCurvePoints[i], origin, factor);

                    s.sliderStartCircle.ModifyPosition(newPos);
                    s.Position = newPos;
                    s.SpatialLength = 0;

                    s.UpdateCalculations();
                }
                else
                    h.ModifyPosition(newPos);
            }
        }
        /// <summary>
        /// return the max allow count of notes in certain ds.avoid error.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        internal int MaxCountAt(double ds)
        {
            int offset = editor.GetCurrentSnappedTime();
            double delta = AudioEngine.beatLengthAt(offset, false) / ((double)base.editor.beatSnapDivisor);
            double dsLength = (((hitObjectManager.SliderVelocityAt(AudioEngine.Time) * ds) * delta) / 1000.0);
            return (int)Math.Floor(OsuMathHelper.Pi / Math.Asin(dsLength / 180 / 2));
        }

        internal bool AddPolygon(int count, int repeat, double angle, double ds)
        {
            if (count < 3)
                return false;
            int offset = editor.GetCurrentSnappedTime();
            double delta = AudioEngine.beatLengthAt(offset, false) / ((double)base.editor.beatSnapDivisor);
            double dsLength = (((hitObjectManager.SliderVelocityAt(AudioEngine.Time) * ds) * delta) / 1000.0);
            double radius = dsLength / (2 * Math.Sin(OsuMathHelper.Pi / count));

            if (radius > 180)
            {
                //this may not appear due to pre-check
                NotificationManager.MessageBox(LocalisationManager.GetString(OsuString.EditorModeCompose_DistanceSnapTooLarge), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (this.selectedObjects.Count != 0)
            {
                hitObjectManager.Remove(selectedObjects.ToArray());
                selectedObjects.Clear();
            }

            bool first = true;
            for (int i = 1; i <= count * repeat; i++)
            {
                Vector2 point = new Vector2((float)(radius * Math.Cos(OsuMathHelper.TwoPi / count * i + angle) + 256f), (float)(radius * Math.Sin(OsuMathHelper.TwoPi / count * i + angle) + 192f));
                HitCircleOsu h = new HitCircleOsu(hitObjectManager, point, offset, this.LastWasSpinner(), SoundAdditionStatus[SoundAdditions.Whistle], SoundAdditionStatus[SoundAdditions.Finish], SoundAdditionStatus[SoundAdditions.Clap], 0);
                if (first && SoundAdditionStatus[SoundAdditions.NewCombo])
                    h.NewCombo = true;
                first = false;
                hitObjectManager.Add(h, true);
                Select(h);
                offset = editor.Timing.BeatSnapValue(offset + delta);

            }
            hitObjectManager.Sort(true);
            return true;
        }

        internal void ChangeNotesPosition(int x, int y, bool relative)
        {
            if (selectedObjects.Count == 0)
                return;
            foreach (HitObject h in selectedObjects)
            {
                h.ModifyPosition(relative ? new Vector2(h.Position.X + x, h.Position.Y + y) : new Vector2(x, y));
            }
        }

        private static Vector2 scale(Vector2 p, Vector2 center, float factor)
        {
            return factor * p + (1 - factor) * center;
        }

        private static Vector2 scale(Vector2 p, Vector2 center, Vector2 factor)
        {
            return new Vector2(factor.X * p.X + (1 - factor.X) * center.X,
                               factor.Y * p.Y + (1 - factor.Y) * center.Y);
        }

        private Vector2 GetOrigin(bool playfieldOrigin)
        {
            if (playfieldOrigin)
                return new Vector2(GameBase.GamefieldDefaultWidth / 2, GameBase.GamefieldDefaultHeight / 2);
            else
            {
                Vector2 origin;

                origin = Vector2.Zero;
                foreach (HitObject o in selectedObjects)
                    origin += o.Position;

                origin.X /= selectedObjects.Count;
                origin.Y /= selectedObjects.Count;

                return origin;
            }
        }

        internal float GetMaxScale(bool playfieldOrigin)
        {
            if (selectedObjects.Count == 0)
                return -1.0f;

            Vector2 origin = GetOrigin(playfieldOrigin);

            float min_x = origin.X, min_y = origin.Y, max_x = origin.X, max_y = origin.Y;

            foreach (HitObject h in selectedObjects)
            {
                min_x = Math.Min(h.Position.X, min_x);
                min_y = Math.Min(h.Position.Y, min_y);
                max_x = Math.Max(h.Position.X, max_x);
                max_y = Math.Max(h.Position.Y, max_y);

                if (h.IsType(HitObjectType.Slider))
                {
                    SliderOsu s = (SliderOsu)h;

                    min_x = Math.Min(s.EndPosition.X, min_x);
                    min_y = Math.Min(s.EndPosition.Y, min_y);
                    max_x = Math.Max(s.EndPosition.X, max_x);
                    max_y = Math.Max(s.EndPosition.Y, max_y);
                }
            }

            min_x -= origin.X;
            min_y -= origin.Y;
            max_x -= origin.X;
            max_y -= origin.Y;

            float left_ratio = (-origin.X) / min_x;
            float top_ratio = (-origin.Y) / min_y;
            float right_ratio = (max_x == 0) ? left_ratio : (512.0f - origin.X) / max_x;
            float bottom_ratio = (max_y == 0) ? top_ratio : (384.0f - origin.Y) / max_y;

            if (min_x == 0)
                left_ratio = right_ratio;
            if (min_y == 0)
                top_ratio = bottom_ratio;

            if ((min_x == 0) && (max_x == 0) && (min_y == 0) && (max_y == 0))
                return -1.0f;
            else if ((min_x == 0) && (max_x == 0))
                return Math.Min(top_ratio, bottom_ratio);
            else if ((min_y == 0) && (max_y == 0))
                return Math.Min(left_ratio, right_ratio);
            else
                return Math.Min(Math.Min(left_ratio, right_ratio), Math.Min(top_ratio, bottom_ratio));
        }

        internal virtual void FlipSelectionVertical()
        {
            if (selectedObjects.Count == 0)
                return;
            editor.UndoPush();

            HandleDrag();
            ignoreDragSelect = resetDrag;
            foreach (HitObject h in selectedObjects)
            {
                if (h.IsType(HitObjectType.Slider))
                {
                    SliderOsu s = (SliderOsu)h;
                    for (int i = 0; i < s.sliderCurvePoints.Count; i++)
                        s.sliderCurvePoints[i] =
                            new Vector2(s.sliderCurvePoints[i].X,
                                        GameBase.GamefieldDefaultHeight - s.sliderCurvePoints[i].Y);
                    s.sliderStartCircle.ModifyPosition(
                        new Vector2(h.Position.X, GameBase.GamefieldDefaultHeight - h.Position.Y));
                    s.Position = new Vector2(h.Position.X, GameBase.GamefieldDefaultHeight - h.Position.Y);
                    s.UpdateCalculations();
                }
                else
                    h.ModifyPosition(new Vector2(h.Position.X, GameBase.GamefieldDefaultHeight - h.Position.Y));
            }
        }

        internal void ResetSelectedObjectSamples()
        {
            ResetSamples(selectedObjects);
        }

        private void ResetSamples(List<HitObject> objects)
        {
            int count = 0;
            editor.UndoPush();
            foreach (HitObject ho in objects)
            {

                SliderOsu s = ho as SliderOsu;

                if (s != null)
                    s.DisableNodalSamples();
                ho.CustomSampleSet = CustomSampleSet.Default;
                ho.SampleSet = SampleSet.None;
                ho.SampleSetAdditions = SampleSet.None;
                ho.SampleVolume = 0;
                ho.SampleFile = string.Empty;
                ho.SampleAddr = -1;
                ho.SoundType = HitObjectSoundType.None;
            }
            if (count > 0)
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.EditorModeCompose_SelectedSampleReset), 2000);
        }

        private bool LastWasSpinner()
        {
            return hitObjectManager.hitObjects.FindLast(h => (h.EndTime < AudioEngine.Time) && (h != currentPlacementObject)) is Spinner;
        }

        internal override bool OnClick()
        {
            if (!Enabled)
                return false;

            if (IgnoreInterfaceClick)
                beatDivisorSlider.ReadOnly = true;

            bool leftButton = InputManager.leftButton == ButtonState.Pressed;
            bool rightButton = InputManager.rightButton == ButtonState.Pressed;

            ignoreDragReset = true;

            if (leftButton)
            {
                HitObject placedObject = null;

                if (SoundAdditionStatus[SoundAdditions.NoteLock] && KeyboardHandler.ControlPressed && KeyboardHandler.ShiftPressed && hoveredObject != null)
                {
                    PasteSample(hoveredObject);
                    return false;
                }

                allowPlacement = !(hoveredResizeMode && hoveredObject == spinnerPlacementCircle);
                if (editor.InTimeline && sliderPlacementCircle == null)
                    ChangeComposeTool(ComposeTools.Select);

                allowPlacement = true;
                if (CurrentComposeTool != ComposeTools.Select)
                {
                    RemovePlacementObject(); //TVO: This may be unnecessary

                    int time = editor.GetCurrentSnappedTime();

                    switch (CurrentComposeTool)
                    {
                        case ComposeTools.Normal:
                            if (GameBase.IsInGamefieldAlt(snapCoordinates) &&
                                GameBase.IsInGamefieldAlt(mouseGamefieldVector))
                            {
                                editor.UndoPush();

                                HitCircleOsu h = new HitCircleOsu(hitObjectManager,
                                                                  snapCoordinates,
                                                                  time,
                                                                  SoundAdditionStatus[
                                                                      SoundAdditions.NewCombo] || LastWasSpinner(),
                                                                  SoundAdditionStatus[SoundAdditions.Whistle
                                                                      ],
                                                                  SoundAdditionStatus[SoundAdditions.Finish],
                                                                  SoundAdditionStatus[SoundAdditions.Clap], 0);

                                placedObject = h;
                            }
                            break;
                        case ComposeTools.Slider:
                            ignoreDrag = true;
                            if (sliderPlacementCircle == null)
                            {
                                if (GameBase.IsInGamefieldAlt(snapCoordinates) &&
                                    GameBase.IsInGamefieldAlt(mouseGamefieldVector))
                                {
                                    editor.UndoPush();

                                    sliderPlacementCircle = new SliderOsu(hitObjectManager,
                                                                snapCoordinates,
                                                                time,
                                                                SoundAdditionStatus[SoundAdditions.NewCombo] || LastWasSpinner(),
                                                                soundAdditionSoundType, SliderCurveType, 0,
                                                                0, null, null, 0) { AllowNewPlacing = true };

                                    placedObject = sliderPlacementCircle;
                                }
                            }
                            else
                            {
                                editor.UndoPush();
                                sliderPlacementCircle.PlacePoint(false, false);
                            }
                            break;
                        case ComposeTools.Spinner:
                            if (spinnerPlacementCircle == null)
                            {
                                if (!editor.InTimeline)
                                {
                                    editor.UndoPush();

                                    spinnerPlacementCircle =
                                        new SpinnerOsu(hitObjectManager, time, (int)(time + AudioEngine.beatLength),
                                                       soundAdditionSoundType);
                                    spinnerPlacementCircle.NewCombo = true;

                                    placedObject = spinnerPlacementCircle;
                                }
                            }
                            break;
                    }

                    if (placedObject != null)
                    {
                        hitObjectManager.Add(placedObject, true);

                        if (placedObject == spinnerPlacementCircle)
                        {
                            if (placedObject != hitObjectManager.hitObjects[hitObjectManager.hitObjectsCount - 1])
                            {
                                // automatically add a new combo on the next object after placing a spinner
                                int index = hitObjectManager.hitObjects.IndexOf(placedObject) + 1;
                                hitObjectManager.hitObjects[index].NewCombo = true;
                            }
                        }
                        else
                        {
                            if (SoundAdditionStatus[SoundAdditions.NewCombo])
                                ChangeSoundAddition(SoundAdditions.NewCombo);
                        }

                        placedObject.SampleSet = (SampleSet)ddSampleSet.SelectedObject;
                        placedObject.SampleSetAdditions = (SampleSet)ddSampleSetAddition.SelectedObject;

                        hitObjectManager.Sort(true);

                        return false;
                    }
                }
                else
                {
                    if (KeyboardHandler.ControlPressed)
                    {
                        if (selectedSlider != null)
                        {
                            SliderOsu s = selectedSlider;

                            if (hoveredObject == null || (hoveredObject == selectedSlider && selectedObjects.Count == 1 && !editor.InTimeline))
                            {
                                if (selectedSliderIndex < 0)
                                {
                                    //No point is selected - add a new point anywhere.
                                    float foundDistance = 0;
                                    int foundIndex = 0;

                                    Vector2 p3 = GameBase.DisplayToGamefield(InputManager.CursorPosition);

                                    //see http://local.wasp.uwa.edu.au/~pbourke/geometry/pointline/ for implementation details
                                    for (int i = 1; i < s.sliderCurvePoints.Count; i++)
                                    {
                                        Vector2 p2 = s.sliderCurvePoints[i];
                                        Vector2 p1 = s.sliderCurvePoints[i - 1];

                                        if (p1 == p2)
                                            continue;

                                        float u = OsuMathHelper.Clamp(((p3.X - p1.X) * (p2.X - p1.X) + (p3.Y - p1.Y) * (p2.Y - p1.Y)) / Vector2.DistanceSquared(p1, p2), 0, 1);
                                        Vector2 tangentIntersection = new Vector2(
                                            p1.X + u * (p2.X - p1.X),
                                            p1.Y + u * (p2.Y - p1.Y));

                                        float dist = Vector2.DistanceSquared(tangentIntersection, p3);

                                        if (foundDistance == 0 || dist <= foundDistance)
                                        {
                                            foundIndex = i;
                                            foundDistance = dist;
                                        }
                                    }

                                    editor.UndoPush();
                                    s.PlacePoint(foundIndex, p3);

                                    return false;
                                }

                                Vector2 selectedPoint = s.sliderCurvePoints[selectedSliderIndex];

                                //Avoid adding a point at the start or end or where a red point already exists
                                if (selectedSliderIndex != 0 &&
                                   selectedSliderIndex != s.sliderCurvePoints.Count - 1 &&
                                   selectedPoint != s.sliderCurvePoints[selectedSliderIndex - 1] &&
                                   selectedPoint != s.sliderCurvePoints[selectedSliderIndex + 1])
                                {
                                    editor.UndoPush();
                                    s.PlacePoint(selectedSliderIndex, s.sliderCurvePoints[selectedSliderIndex]);

                                    return false;
                                }
                            }
                        }

                        if (hoveredObject != null)
                        {
                            if (hoveredObject.Selected && selectedObjects.Count > 1)
                            {
                                Deselect(hoveredObject);
                                ignoreDrag = true;
                            }
                            else
                                Select(hoveredObject);
                            return false;
                        }
                    }

                    //After this point is selection logic.
                    if (selectedSliderIndex >= 0)
                    {
                        if (selectedSlider == null && hoveredSlider != null)
                        {
                            selectedSlider = hoveredSlider;
                            Select(selectedSlider);
                        }
                        return false;
                    }

                    if (hoveredObject != null)
                    {
                        if (hoveredResizeMode && hoveredObject == spinnerPlacementCircle)
                        {
                            //This is a special case. We want to place the spinner as shown if we click on the end.
                            //This is needed so that the length can be changed without snapping back to audio time.
                            SelectNone();
                            Select(spinnerPlacementCircle);
                            SpinnerPlacementEnd(spinnerPlacementCircle.EndTime);
                            return false;
                        }
                        else if (!hoveredResizeMode && hoveredObject == selectedSlider)
                        {
                            //Handle slider circle selection
                            bool found = false;
                            for (int i = 0; i < selectedSlider.sliderAllCircles.Count; i++)
                            {
                                HitCircleOsu h = selectedSlider.sliderAllCircles[i];
                                bool val = !found && editor.InTimeline
                                               ? Math.Abs(editor.TimeToTimelinePos(h.StartTime) -
                                                          InputManager.CursorPoint.X) <
                                                 hitObjectManager.HitObjectRadius * 0.5f
                                               : Vector2.DistanceSquared(mouseGamefieldVector, h.Position) <
                                                 hitObjectManager.HitObjectRadius * hitObjectManager.HitObjectRadius;
                                if (h.Selected != val)
                                {
                                    h.Selected = val;

                                    if (val)
                                        h.SpriteSelectionCircle.InitialColour = Color.Red;
                                    else
                                    {
                                        if (i < 2)
                                        {
                                            h.Select();
                                            h.SpriteSelectionCircle.InitialColour = Color.White;
                                        }
                                    }

                                    selectionChanged = true;
                                }

                                if (val) found = true;
                            }

                            selectedSlider.BodySelected = !found;
                        }
                    }
                    else
                    {
                        if (!hoveredResizeMode)
                            SelectNone();
                        return false;
                    }

                    //Selected objects need to stay selected in order to be dragged.
                    if (hoveredObject.Selected)
                        return false;

                    //Otherwise we can swap select the hovered object
                    SelectNone();
                    Select(hoveredObject);
                }
            }
            else if (rightButton)
            {
                if (editor.InTimeline)
                {
                    if (hoveredEvent != null && hoveredEvent.Type == EventTypes.Break)
                    {
                        if (editor.IsUnderNoBreakTime(hoveredEvent))
                        {
                            editor.UndoPush();
                            editor.eventManager.Remove(hoveredEvent);
                        }
                        return false;
                    }
                }

                if (sliderPlacementCircle != null)
                {
                    SliderOsu placedSlider = sliderPlacementCircle;

                    if (!placedSlider.placingValid &&
                             placedSlider.sliderCurvePoints.Count > 1)
                        SliderPlacementEnd(false); //case where the current point is invalid, so we don't want to include it.
                    else
                        SliderPlacementEnd(true);
                }
                else if (spinnerPlacementCircle != null)
                    SpinnerPlacementEnd();
                else if (selectedSliderIndex >= 0 && hoveredSlider != null && hoveredSlider.sliderCurvePoints.Count > 2)
                {
                    editor.UndoPush();
                    hoveredSlider.sliderCurvePoints.RemoveAt(selectedSliderIndex);
                    if (selectedSliderIndex == 0)
                    {
                        hoveredSlider.Position = hoveredSlider.sliderCurvePoints[0];
                        hoveredSlider.ModifyPosition(hoveredSlider.sliderCurvePoints[0]);
                    }
                    hoveredSlider.SpatialLength = 0;
                    hoveredSlider.UpdateCalculations();
                }
                else if (hoveredObject != null)
                {
                    if (hoveredObject.Selected)
                    {
                        DeleteSelection();
                        ResetHoverVariables();
                    }
                    else if (AudioEngine.AudioState == AudioStates.Stopped ||
                             CurrentComposeTool == ComposeTools.Select)
                    {
                        editor.UndoPush();
                        hitObjectManager.Remove(hoveredObject, true);
                        ResetHoverVariables();
                    }
                    else
                        ChangeSoundAddition(SoundAdditions.NewCombo);
                }
                else if (CurrentComposeTool == ComposeTools.Normal ||
                         CurrentComposeTool == ComposeTools.Slider)
                    ChangeSoundAddition(SoundAdditions.NewCombo);
            }

            editor.BreakTimeFix(true);

            return base.OnClick();
        }

        internal override bool OnClickUp()
        {
            if (InputManager.leftButtonLastBool)
            {
                ignoreDrag = false;
                ignoreDragSelect = false;
            }

            //Reenable sliderbar interaction
            beatDivisorSlider.ReadOnly = false;
            return base.OnClickUp();
        }

        protected void PasteSample(HitObject h)
        {
            if (sampleNote != null)
            {
                h.SoundType = sampleNote.SoundType;
                h.CustomSampleSet = sampleNote.CustomSampleSet;
                h.SampleSet = sampleNote.SampleSet;
                h.SampleSetAdditions = sampleNote.SampleSetAdditions;
                h.SampleFile = sampleNote.SampleFile;
                h.SampleAddr = sampleNote.SampleAddr;
                h.SampleVolume = sampleNote.SampleVolume;
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.EditorModeCompose_SampleApplied), Color.YellowGreen, 700);
            }
            else
            {
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.EditorModeCompose_SelectNoteBeforeSampleCopy), 1500);
            }
        }

        internal virtual void DeleteSelection()
        {
            if (selectedObjects.Count == 0)
                return;

            editor.UndoPush();
            hitObjectManager.Remove(selectedObjects.ToArray());
            selectedObjects.Clear();

            hitObjectManager.Sort(true);
        }

        internal void ChangeComposeTool(ComposeTools tool)
        {
            SliderPlacementEnd(true);
            SpinnerPlacementEnd();

            if (CurrentComposeTool == ComposeTools.Select && tool != CurrentComposeTool)
            {
                SelectNone();
            }

            CurrentComposeTool = tool;
            for (int i = 0; i < composeToolSprites.Length; i++)
                if (i != (int)CurrentComposeTool)
                    composeToolSprites[i].Transformations.Add(
                        new Transformation(TransformationType.Fade, composeToolSprites[i].Alpha,
                                           0.4F, GameBase.Time, GameBase.Time + 100));
                else
                    composeToolSprites[i].Transformations.Add(
                        new Transformation(TransformationType.Fade, 1, 0.9F, GameBase.Time, GameBase.Time + 100));
        }

        private void ResetSoundAdditions()
        {
            ChangeSoundAddition(SoundAdditions.Whistle, false);
            ChangeSoundAddition(SoundAdditions.Finish, false);
            ChangeSoundAddition(SoundAdditions.Clap, false);
            ChangeSoundAddition(SoundAdditions.NewCombo, false);
        }

        private void ShowSoundAddition(SoundAdditions add)
        {
            foreach (SoundAdditions e in Enum.GetValues(typeof(SoundAdditions)))
            {
                if ((e & add) > 0)
                    ShowSoundAdditionStatus(e);
                else
                    spriteManager.GetTagged(e)[0].FadeOut(300);
            }
        }

        private void ChangeComposeTool(Object sender, EventArgs e)
        {
            if (IgnoreInterfaceClick)
                return;

            for (int i = 0; i < composeToolSprites.Length; i++)
                if (composeToolSprites[i].Equals(sender))
                {
                    ComposeTools upd = (ComposeTools)i;

                    //update the interface
                    ChangeComposeTool(upd);

                    return;
                }
        }

        /// <summary>
        /// Removes the hitobject currently being placed.  Usually used before saving to ensure it doesn't get saved.
        /// </summary>
        internal void RemovePlacementObject()
        {
            if (currentPlacementObject != null)
                hitObjectManager.Remove(currentPlacementObject, true);
        }

        private void SpinnerPlacementEnd()
        {
            SpinnerPlacementEnd(editor.GetCurrentSnappedTime());
        }

        private void SpinnerPlacementEnd(int endTime)
        {
            if (spinnerPlacementCircle == null || !allowPlacement) return;

            if (endTime != spinnerPlacementCircle.EndTime)
            {
                editor.UndoPush();
                spinnerPlacementCircle.SetEndTime(Math.Max(spinnerPlacementCircle.StartTime,
                                                           endTime));
            }

            if (spinnerPlacementCircle.EndTime <= spinnerPlacementCircle.StartTime)
                hitObjectManager.Remove(spinnerPlacementCircle, true);
            spinnerPlacementCircle = null;
        }

        private void SliderPlacementEnd(bool placeLastPoint)
        {
            if (sliderPlacementCircle == null || !allowPlacement) return;

            if (sliderPlacementCircle.sliderCurvePoints.Count == 1 ||
                sliderPlacementCircle.SpatialLength == 0)
                hitObjectManager.Remove(sliderPlacementCircle, true);
            else
            {
                editor.UndoPush();
                sliderPlacementCircle.AdjustRepeats(AudioEngine.Time);
                if (placeLastPoint)
                    sliderPlacementCircle.PlacePoint(true, false);
            }

            sliderPlacementCircle = null;
        }


        internal override bool ExitEditor()
        {

            if (sliderPlacementCircle != null)
            {
                hitObjectManager.Remove(sliderPlacementCircle, true);
                sliderPlacementCircle = null;
                return false;
            }
            else if (spinnerPlacementCircle != null)
            {
                hitObjectManager.Remove(spinnerPlacementCircle, true);
                spinnerPlacementCircle = null;
                return false;
            }
            else if (selectedObjects.Count > 0)
            {
                SelectNone();
                return false;
            }

            return true;
        }

        internal virtual void Select(params HitObjectBase[] objects)
        {
            if (dragBreak != null) return;

            SelectNone();

            selectedObjects = new List<HitObject>();
            foreach (HitObjectBase o in objects)
                selectedObjects.Add(o as HitObject);
            selectedObjects.ForEach(h => h.Selected = true);
            selectionChanged = true;
        }

        internal virtual void Select(HitObject h)
        {
            if (dragBreak != null) return;

            if (CurrentComposeTool != ComposeTools.Select)
                ChangeComposeTool(ComposeTools.Select);

            if (!selectedObjects.Contains(h))
            {
                h.Selected = true;
                selectedObjects.Add(h);

                selectionChanged = true;
            }

        }

        internal void Deselect(HitObject h)
        {
            if (selectedObjects.Contains(h))
            {
                h.Selected = false;
                selectedObjects.Remove(h);

                selectionChanged = true;
            }

        }

        internal override void SelectAll()
        {
            if (dragBreak != null) return;

            ChangeComposeTool(ComposeTools.Select);

            HandleDrag(true);

            SelectNone();
            selectedObjects.AddRange(hitObjectManager.hitObjects);
            selectionChanged = true;

            DoSelection();
        }

        internal override void SelectNone()
        {
            ResetSoundAdditions();

            if (selectedObjects.Count == 0) return;

            HandleDrag();

            selectedObjects.ForEach(h => h.Selected = false);

            if (selectedSlider != null)
                selectedSlider.UpdateNodalSamples();

            selectionChanged = true;
            selectedObjects.Clear();
        }

        internal void DoSelection()
        {
            selectedObjects.ForEach(h => h.Selected = true);
        }

        internal override void CopySelection()
        {
            copiedObjects.Clear();
            selectedObjects.Sort();

            string selectedList = string.Empty;
            int startTime = AudioEngine.Time;
            if (selectedObjects.Count > 0)
            {
                selectedList = @" (";
                foreach (HitObject o in selectedObjects)
                    selectedList += o.ComboNumber + @",";
                selectedList = selectedList.Trim(',') + @")";
                startTime = selectedObjects[0].StartTime;
            }

            string time = String.Format(@"{0:00}:{1:00}:{2:000}",
                   startTime / 60000, startTime % 60000 / 1000, startTime % 1000);

            try
            {
                Clipboard.SetText(time + selectedList + @" - ");
            }
            catch (Exception)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.General_Editor_ClipboardNotAvailable));
            }

            foreach (HitObject h in selectedObjects)
            {
                HitObject clone = h.Clone();
                copiedObjects.Add(clone);
            }
            if (selectedObjects.Count == 1 && SoundAdditionStatus[SoundAdditions.NoteLock])
                sampleNote = selectedObjects[0];
        }

        internal override void PasteSelection()
        {
            if (copiedObjects == null || copiedObjects.Count == 0)
                return;

            editor.UndoPush();

            SelectNone();

            HandleDrag();
            ignoreDrag = resetDrag;
            //Sort so the difference we get is for the earliest object.
            copiedObjects.Sort((h1, h2) => h1.StartTime.CompareTo(h2.StartTime));

            int diff = (AudioEngine.BeatSyncing
                            ? editor.Timing.BeatSnapValue(AudioEngine.Time)
                            : AudioEngine.Time) - copiedObjects[0].StartTime;

            foreach (HitObject h in copiedObjects)
            {
                HitObject clone = h.Clone();

                if (h is SliderOsu)
                    ((SliderOsu)clone).ModifySliderTime(clone.StartTime + diff, false);
                else
                    clone.ModifyTime(clone.StartTime + diff);

                hitObjectManager.Add(clone, true);

                Select(clone);
            }

            hitObjectManager.Sort(true);
        }

        #region Nested type: ComposeTools

        internal enum ComposeTools
        {
            Select,
            Normal,
            Slider,
            Spinner,
            Hold
        } ;

        #endregion

        internal void ResetAllSamples()
        {
            ResetSamples(hitObjectManager.hitObjects);
        }

        private void ComboExpandHover(object sender, EventArgs e)
        {
            comboColourExpand.Transformations.Clear();
            comboColourExpand.FadeIn(200);
        }

        private void ComboExpandHoverLost(object sender, EventArgs e)
        {
            if (comboColour.Visible)
            {
                comboColourExpand.Transformations.Clear();
                comboColourExpand.FadeIn(200);
            }
            else
            {
                comboColourExpand.Transformations.Clear();
                comboColourExpand.FadeTo(1.0f / 255.0f, 50, EasingTypes.None);
            }
        }

        private void ComboExpandClick(object sender, EventArgs e)
        {
            if (comboColour.Visible)
                comboColour.Hide();
            else
                comboColour.Show();
        }

        private void DisableComboDropdown()
        {
            comboColourExpand.HandleInput = false;
            comboColourExpand.Transformations.Clear();
            comboColourExpand.FadeOut(50);

            comboColourExpand.OnHover -= ComboExpandHover;
            comboColourExpand.OnHoverLost -= ComboExpandHoverLost;

            soundAdditionSprites[0].Texture = SkinManager.Load(@"editor-draw-newcombo", SkinSource.Osu);
        }

        private void ComboExpandBlur(object sender, EventArgs e)
        {
            ComboExpandHoverLost(this, EventArgs.Empty);
        }

        private void AddComboItem(Color colour, int value, int count, bool selected, EventHandler onClick)
        {
            pSpriteMenuItem item = new pSpriteMenuItem(comboColour, spriteManager, new Vector2(32, 32), new Vector2(GameBase.WindowWidthScaled - 70, 80 + 20 * count), selected);

            pSprite hitCirclePng = new pSprite(SkinManager.Load(@"hitcircle", SkinSource.Osu),
                                               Fields.Storyboard, Origins.TopLeft,
                                               Clocks.Game, new Vector2(GameBase.WindowWidthScaled - 70, 80 + 20 * count),
                                               2, true, new Color(colour.R, colour.G, colour.B, 0));
            pSprite hitCircleOverlay = new pSprite(SkinManager.Load(@"hitcircleoverlay", SkinSource.Osu),
                                                   Fields.Storyboard, Origins.TopLeft,
                                                   Clocks.Game, new Vector2(GameBase.WindowWidthScaled - 70, 80 + 20 * count),
                                                   3, true, Color.TransparentWhite);

            hitCirclePng.Scale = 0.15625f;
            hitCircleOverlay.Scale = 0.15625f;

            spriteManager.Add(hitCirclePng);
            spriteManager.Add(hitCircleOverlay);

            item.Sprites.Add(hitCirclePng);
            item.Sprites.Add(hitCircleOverlay);
            item.TagNumeric = value;
            item.OnClick += ComboColourSelect;

            comboColour.Items.Add(item);
        }

        /// <summary>
        /// Updates new combo button
        /// </summary>
        private void UpdateComboDropdown()
        {
            if (selectedObjects.Count != 1)
                DisableComboDropdown();
            else
            {
                HitObject obj = selectedObjects[0];
                if (!obj.NewCombo)
                {
                    DisableComboDropdown();
                    return;
                }

                soundAdditionSprites[0].Texture = SkinManager.Load(@"editor-draw-newcombo-expandable", SkinSource.Osu);

                comboColourExpand.HandleInput = true;
                comboColourExpand.OnHover += ComboExpandHover;
                comboColourExpand.OnHoverLost += ComboExpandHoverLost;
                comboColourExpand.FadeTo(1.0f / 255.0f, 0, EasingTypes.None);

                comboColour.ClearItems();

                int ComboCount = hitObjectManager.ComboColours.Count;
                int ColourIndex = (obj.ComboColourIndex - obj.ComboOffset);
                if (ColourIndex < 0) // ensure positive
                {
                    ColourIndex -= (ColourIndex / hitObjectManager.ComboColours.Count) * hitObjectManager.ComboColours.Count;
                    ColourIndex += hitObjectManager.ComboColours.Count;
                }

                // Allow us to use the same colour as last time if this is the first object after a break.
                int CombosShow = editor.hitObjectManager.IsFirstObjectAfterBreak(obj, true) ? ComboCount : ComboCount - 1;

                for (int x = 0; x < CombosShow; x++)
                {
                    Color colour = hitObjectManager.ComboColours[(ColourIndex + x) % ComboCount];
                    AddComboItem(colour, x, x, x == obj.ComboOffset, ComboColourSelect);
                }
            }
        }

        /// <summary>
        /// Checks Sampleset specific data and updates sampleset dropdowns.
        /// </summary>
        internal void UpdateSamples()
        {
            if (selectedObjects.Count == 0)
            {
                if (selectionChanged)
                {
                    ddSampleSet.SetSelected(SampleSet.None, true);
                    ddSampleSetAddition.SetSelected(SampleSet.None, true);
                }
            }
            else if (selectedSlider != null)
            {
                SampleSet ss = SampleSet.None;
                SampleSet ssa = SampleSet.None;
                int x;
                bool endpointsSelected = false;

                if (selectedSlider.unifiedSoundAddition)
                {
                    ss = selectedSlider.SampleSet;
                    ssa = selectedSlider.SampleSetAdditions;
                }
                else
                {
                    if (selectedSlider.sliderStartCircle.Selected)
                    {
                        ss = selectedSlider.SampleSetList[0];
                        ssa = selectedSlider.SampleSetAdditionList[0];
                        x = 0;
                        endpointsSelected = true;
                    }
                    else
                    {
                        x = selectedSlider.sliderEndCircles.FindIndex(delegate(HitCircleSliderEnd ho)
                        {
                            return ho.Selected;
                        });

                        if (x >= 0)
                        {
                            ss = selectedSlider.SampleSetList[x + 1];
                            ssa = selectedSlider.SampleSetAdditionList[x + 1];
                            endpointsSelected = true;
                        }
                        else
                        {
                            endpointsSelected = false;
                        }
                    }

                    if (endpointsSelected)
                    {
                        HitCircleSliderEnd obj;
                        bool ssMatches = true;
                        bool ssaMatches = true;
                        while (ssMatches || ssaMatches)
                        {
                            obj = selectedSlider.sliderEndCircles[x];
                            if (obj.Selected)
                            {
                                if (ssMatches)
                                    ssMatches = selectedSlider.SampleSetList[x + 1] == ss;
                                if (ssaMatches)
                                    ssaMatches = selectedSlider.SampleSetAdditionList[x + 1] == ssa;
                            }
                            x++;
                            if (x >= selectedSlider.sliderEndCircles.Count)
                            {
                                break;
                            }
                        }
                        if (!ssMatches)
                            ss = SampleSet.None;
                        if (!ssaMatches)
                            ssa = SampleSet.None;
                    }
                    else
                    {
                        ss = selectedSlider.SampleSet;
                        ssa = selectedSlider.SampleSetAdditions;
                    }
                }

                ddSampleSet.SetSelected(ss, true);
                ddSampleSetAddition.SetSelected(ssa, true);
            }
            else
            {
                SampleSet ss = selectedObjects[0].SampleSet;
                SampleSet ssa = selectedObjects[0].SampleSetAdditions;

                bool ssMatches = true;
                bool ssaMatches = true;
                foreach (HitObject h in selectedObjects)
                {
                    if (ssMatches)
                        ssMatches = h.SampleSet == ss;
                    if (ssaMatches)
                        ssaMatches = h.SampleSetAdditions == ssa;

                    if (!ssMatches && !ssaMatches)
                        break;
                }
                if (!ssMatches)
                    ss = SampleSet.None;
                if (!ssaMatches)
                    ssa = SampleSet.None;

                ddSampleSet.SetSelected(ss, true);
                ddSampleSetAddition.SetSelected(ssa, true);
            }
        }

        /// <summary>
        /// Checks selected objects and updates SoundAddition buttons to reflect current selected data.
        /// </summary>
        internal void UpdateSoundAdditions()
        {
            if (selectedObjects.Count == 0) return;

            if (editor.DragCaptured != DragCapture.Timeline &&
                CurrentComposeTool != ComposeTools.Select && editor.InGamefield)
            {
                ResetAllSelections();
            }
            else
            {
                //Update toggles for the current selection
                bool finish, whistle, clap, newcombo;

                finish =
                whistle =
                clap =
                newcombo = true;

                if (selectedSlider != null)
                {
                    newcombo = selectedSlider.NewCombo;
                    if (selectedSlider.unifiedSoundAddition)
                    {
                        //Unified slider samples - no need to check any endcircles.
                        whistle &= selectedSlider.Whistle;
                        finish &= selectedSlider.Finish;
                        clap &= selectedSlider.Clap;
                    }
                    else
                    {
                        int count = 0;
                        for (int i = 0; i < selectedSlider.sliderAllCircles.Count; i++)
                        {
                            HitCircleOsu h = selectedSlider.sliderAllCircles[i];
                            //Check the endcircles.
                            if (!h.Selected) continue;

                            whistle &= selectedSlider.SoundTypeList[i].IsType(HitObjectSoundType.Whistle);
                            finish &= selectedSlider.SoundTypeList[i].IsType(HitObjectSoundType.Finish);
                            clap &= selectedSlider.SoundTypeList[i].IsType(HitObjectSoundType.Clap);
                            count++;
                        }

                        if (count == 0)
                        {
                            //Editing the slider path itself (currently just toggle sliderslider for whistle)
                            whistle &= selectedSlider.Whistle;
                            finish &= selectedSlider.Finish;
                            clap &= selectedSlider.Clap;
                        }
                    }
                }
                else
                {
                    foreach (HitObject h in selectedObjects)
                    {
                        newcombo &= h.NewCombo;
                        whistle &= h.Whistle;
                        finish &= h.Finish;
                        clap &= h.Clap;
                    }
                }

                ChangeSoundAddition(SoundAdditions.Whistle, whistle);
                ChangeSoundAddition(SoundAdditions.Finish, finish);
                ChangeSoundAddition(SoundAdditions.Clap, clap);
                ChangeSoundAddition(SoundAdditions.NewCombo, newcombo);
            }
        }

        internal void changeSelectedSample(string file, int volume)
        {
            foreach (HitObject h in selectedObjects)
            {
                h.SampleVolume = volume;
                h.ProcessSampleFile(file);
            }
        }

        internal void changeSelectedSample(int custom, int volume)
        {
            foreach (HitObject h in selectedObjects)
            {
                h.SampleVolume = volume;
                h.CustomSampleSet = (CustomSampleSet)custom;
            }
        }

        internal void autoVolumeBalance()
        {
            if (BeatmapManager.Current.PlayMode != PlayModes.OsuMania)
                return;
            int start = 0;
            List<HitObject> notes = hitObjectManager.hitObjects;
            for (int i = 0; i < notes.Count; i++)
            {
                if (notes[i].StartTime != notes[start].StartTime)
                {
                    int count = i - start;
                    if (count < 1)
                        count = 1;
                    int volume = AudioEngine.controlPointAt(notes[start].StartTime).volume;
                    for (int j = start; j < i; j++)
                        notes[j].SampleVolume = volume * 4 / 3 / count;
                    start = i;
                }
            }
        }

        internal virtual void ReferenceChanged() { }

    }
}
