﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
using osu.Audio;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Mania;
using osu.GameplayElements.HitObjects.Osu;
using osu.Graphics.Notifications;
using osu.Graphics.Primitives;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;
using osu_common;
using osu_common.Helpers;
using osu.GameplayElements.Events;
using osu.GameModes.Edit.Forms;
using osu.Online;

namespace osu.GameModes.Edit.Modes
{
    class EditorModeComposeMania : EditorModeCompose
    {
        private int timeShifted;

        private int timebarStart = 80;
        private int columnStart = 160;
        private float columnEnd = 160;
        private int columnTop = 64;
        private int columnBottom = 458;

        private int column = 7;
        private Vector2 mousePosition;
        private pSprite cursorNote;
        internal pText beatDisplay;
        private pSprite timebarWindow;

        private SpriteManager columnManager;
        private int columnLastTime = -1;
        private float columnLastScale = -1;

        private int timeRangeStart = 0;
        private int timeRangeEnd = 0;

        private HitCircleHold pendingHold;
        private HitObject selectedRefNote;
        private float dragStartX;
        private int dragStartTime;
        private List<EventSample> selectedSamples;
        private List<EventSample> copiedSamples;
        private HitCircleOsu[] pendingLivingNotes;

        protected EventSample hoveredSample;
        private int hoveredColumn = -1;
        private int lastHoveredColumn = -1;

        private bool inColumnArea = false;

        private int currBeat;
        private int currStanza;

        private bool sampleColumnMode;

        internal override bool Enabled
        {
            get { return base.Enabled; }
            set
            {
                base.Enabled = value;
                columnManager.HandleInput = value;
            }
        }


        private float columnWidth
        {
            get
            {
                float displayWidth = GameBase.WindowWidthScaled - columnStart - 70f;
                return Math.Min(30f, displayWidth / column);
            }
        }

        private float columnRatio { get { return columnWidth / 30f; } }

        internal override bool IgnoreInterfaceClick
        {
            get { return false; }
        }

        public EditorModeComposeMania(Editor editor)
            : base(editor)
        { }

        internal override void Initialize()
        {
            column = (int)Math.Round(BeatmapManager.Current.DifficultyCircleSize);
            hitObjectManager.ManiaStage = new StageMania(SkinManager.LoadManiaSkin(column), true);
            columnEnd = columnStart + columnWidth * column;

            columnManager = new SpriteManager(true);
            copiedObjects = new List<HitObject>();
            copiedSamples = new List<EventSample>();
            selectedSamples = new List<EventSample>();
            pendingLivingNotes = new HitCircleOsu[column];
            base.Initialize();
        }

        protected override void InitializeSprites()
        {
            spriteManager.Clear();

            base.InitializeSprites();
            pSprite p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(columnStart - 1, columnTop), 0.9F, true, Color.Black);
            p.VectorScale = new Vector2(column * columnWidth + 1, 398) * 1.6F;
            spriteManager.Add(p);

            //timebar bg
            p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(timebarStart, columnTop), 0.9F, true, Color.Black);
            p.VectorScale = new Vector2(60, 398) * 1.6F;
            p.Alpha = 0.95f;
            spriteManager.Add(p);

            timebarWindow = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.BottomLeft, Clocks.Game, new Vector2(timebarStart - 2, columnBottom), 0.901f, true, Color.White);
            timebarWindow.VectorScale = new Vector2(62, 0) * 1.6F;
            timebarWindow.Alpha = 0.5f;
            spriteManager.Add(timebarWindow);
            for (int i = 0; i <= column; i++)
            {
                p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(columnStart + columnWidth * i, columnTop), 0.91F, true, Color.White);
                p.VectorScale = new Vector2(1, 398) * 1.6F;
                if (i > 0 && i < column)
                    p.Tag = @"vertical";
                spriteManager.Add(p);
            }
            //timebar line
            p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(timebarStart, columnTop), 0.91F, true, Color.White);
            p.VectorScale = new Vector2(1, 398) * 1.6F;
            spriteManager.Add(p);

            p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(timebarStart + 60, columnTop), 0.91F, true, Color.White);
            p.VectorScale = new Vector2(1, 398) * 1.6F;
            spriteManager.Add(p);

            //judgement line
            p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(columnStart, columnBottom - (columnBottom - columnTop) / 5), 0.96F, true, Color.GreenYellow);
            p.VectorScale = new Vector2(column * columnWidth, 4) * 1.6F;
            spriteManager.Add(p);

            cursorNote = new pSprite(SkinManager.Load("mania-note1", SkinSource.Osu), Fields.TopLeft, Origins.BottomCentre, Clocks.Game, new Vector2(0, 0), 0.95F, true, Color.White);
            cursorNote.Bypass = true;
            cursorNote.VectorScale.X = columnRatio;
            cursorNote.Scale = 0.1875F;
            spriteManager.Add(cursorNote);

            beatDisplay = new pText(string.Empty, 13, new Vector2(200, 20), 1, true, Color.White);
            beatDisplay.Field = Fields.TopRight;
            beatDisplay.Origin = Origins.TopRight;
            spriteManager.Add(beatDisplay);

            if (ReferenceHitObjectManager != null)
                ReferenceChanged();
        }

        internal override void ReferenceChanged()
        {
            spriteManager.SpriteList.RemoveAll(p => p.TagNumeric == -10086);
            if (ReferenceHitObjectManager != null)
            {
                int col = (int)Math.Round(ReferenceHitObjectManager.Beatmap.DifficultyCircleSize);
                float start = columnStart + columnWidth * column + columnWidth;
                pSprite p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(start, columnTop), 0.9F, true, new Color(50, 50, 50));
                p.VectorScale = new Vector2(col * columnWidth + 1, 398) * 1.6F;
                p.TagNumeric = -10086;
                spriteManager.Add(p);

                for (int i = 0; i <= col; i++)
                {
                    p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(start + columnWidth * i, columnTop), 0.91F, true, Color.White);
                    p.VectorScale = new Vector2(1, 398) * 1.6F;
                    p.TagNumeric = -10086;
                    spriteManager.Add(p);
                }
            }
        }

        internal override void Dispose()
        {
            cursorNote.Dispose();
            columnManager.Dispose();
            base.Dispose();
        }

        private void getEdgeColumns(out int leftMostColumn, out int rightMostColumn)
        {
            leftMostColumn = -1;
            rightMostColumn = -1;
            foreach (HitObject h in selectedObjects)
            {
                int col = hitObjectManager.ManiaStage.ColumnAt(h.Position);

                if (leftMostColumn == -1)
                {
                    leftMostColumn = col;
                    rightMostColumn = col;
                }
                else if (col <= leftMostColumn)
                {
                    leftMostColumn = col;
                }
                else if (col >= rightMostColumn)
                {
                    rightMostColumn = col;
                }
            }
        }

        private void updateTimeRange()
        {
            timeRangeStart = AudioEngine.Time - editor.TimelineDisplayRange / 12;
            timeRangeEnd = AudioEngine.Time + editor.TimelineDisplayRange / 3;
        }

        private void updateDragSelection()
        {
            DragPoint2 = InputManager.CursorPosition;

            DragPoint2.X = Math.Min(DragPoint2.X, (columnStart + column * columnWidth + columnWidth) * GameBase.WindowRatio);
            RectangleF rect = new RectangleF(Math.Min(DragPoint1.X, DragPoint2.X), Math.Min(DragPoint1.Y, DragPoint2.Y),
                                            Math.Abs(DragPoint1.X - DragPoint2.X), Math.Abs(DragPoint1.Y - DragPoint2.Y));

            Vector2 mouse1 = DragPoint1 / GameBase.WindowRatio;
            Vector2 mouse2 = DragPoint2 / GameBase.WindowRatio;
            mouse1.Y -= GameBase.WindowOffsetY / GameBase.WindowRatio;
            mouse2.Y -= GameBase.WindowOffsetY / GameBase.WindowRatio;

            SelectNone();

            if (mouse1.X > timebarStart && mouse1.X < timebarStart + 60 && mouse2.X > timebarStart && mouse2.X < timebarStart + 60)
            {
                int time1 = (int)((columnBottom - mouse1.Y) / (columnBottom - columnTop) * AudioEngine.AudioLength);
                int time2 = (int)((columnBottom - mouse2.Y) / (columnBottom - columnTop) * AudioEngine.AudioLength);
                int timeStart = Math.Min(time1, time2);
                int timeEnd = Math.Max(time1, time2);

                if (editor.DragCaptured == DragCapture.Gamefield)
                {
                    hitObjectManager.hitObjects.ForEach(h =>
                    {
                        if (h.EndTime < timeStart || h.StartTime > timeEnd)
                            return;
                        Select(h);
                    });
                }

                AudioEngine.SeekTo(time2);
                return;
            }

            if (editor.DragCaptured == DragCapture.Gamefield)
            {
                for (int i = 0; i < columnManager.SpriteList.Count; i++)
                {
                    pSprite p = (pSprite)columnManager.SpriteList[i];
                    if (rect.Contains(p.drawRectangle))
                    {
                        if (p.Tag == null)
                            continue;
                        if (sampleColumnMode)
                            Select((EventSample)p.Tag);
                        else
                            Select((HitObject)p.Tag);
                    }
                }
            }
        }

        private void updateDrag()
        {
            if (dragSelection)
            {
                updateDragSelection();
                return;
            }

            if (SoundAdditionStatus[SoundAdditions.NoteLock] || !IsDragModified) return;

            int colOffset = (int)((mousePosition.X - dragStartX) / columnWidth); // old way of calculating colOffset
            int timeOffset = yToTime(mousePosition.Y) - dragStartTime;
            timeShifted += timeOffset;

            int moveUnits = (int)(editor.TickSpacing * Math.Round((double)(timeShifted / editor.TickSpacing)));

            bool allowOffsetChange = Math.Abs(moveUnits) > 0;

            if (allowOffsetChange)
                timeShifted -= moveUnits;

            allowOffsetChange = true; //Remove this and commented out moveUnits adds to snap notes to column area 

            if (sampleColumnMode)
            {
                if (KeyboardHandler.ShiftPressed)
                    moveUnits = timeOffset;

                if (allowOffsetChange)
                {
                    foreach (EventSample e in selectedSamples)
                        e.StartTime += moveUnits;
                }
            }
            else
            {
                int leftMostCol, rightMostCol;
                getEdgeColumns(out leftMostCol, out rightMostCol);

                //New colOffset finding code.
                if (hoveredColumn != -1 && lastHoveredColumn != -1)
                {
                    colOffset = hoveredColumn - lastHoveredColumn;
                }
                else if (mousePosition.X < columnStart)
                {
                    colOffset = leftMostCol * -1;
                }
                else if (mousePosition.X > columnEnd)
                {
                    colOffset = hitObjectManager.ManiaStage.Columns.Count - rightMostCol - 1;
                }
                else
                {
                    colOffset = 0;
                }

                bool allowLeftMovement = leftMostCol + colOffset >= 0 && mousePosition.X < columnEnd;
                bool allowRightMovement = rightMostCol + colOffset <= hitObjectManager.ManiaStage.Columns.Count - 1 && mousePosition.X > columnStart; //-1 because ColumnAt is zero based.

                foreach (HitObject h in selectedObjects)
                {
                    if (allowOffsetChange)
                    {
                        h.StartTime += timeOffset;//moveUnits;
                        h.EndTime += timeOffset;//moveUnits;
                    }

                    //We want to avoid moving the selection if part of the selection cannot be moved.
                    if (allowLeftMovement && colOffset < 0 || allowRightMovement && colOffset > 0)
                    {
                        int col = hitObjectManager.ManiaStage.ColumnAt(h.Position);
                        col += colOffset;
                        col = Math.Min(column, Math.Max(0, col));
                        h.Position.X = hitObjectManager.ManiaStage.Columns[col].SnapPositionX;
                    }
                }
            }
            if (colOffset != 0)
                dragStartX = mousePosition.X;
            if (timeOffset != 0)
                dragStartTime = yToTime(mousePosition.Y);
        }

        internal override void Update()
        {
            hitObjectManager.ManiaStage.UpdateInput();

            mousePosition = new Vector2(InputManager.CursorPosition.X, InputManager.CursorPosition.Y - GameBase.WindowOffsetY) / GameBase.WindowRatio;
            updateTimeRange();

            inColumnArea = mouseInColumnArea();

            if (inColumnArea)
            {
                if (sampleColumnMode)
                {
                    hoveredSample = getSampleAtMouse();
                }
                else
                {
                    hoveredObject = getObjectAtMouse();
                    hoveredColumn = columnAtMousePos();
                }
            }
            else
            {
                hoveredColumn = -1;
                hoveredObject = null;
                hoveredSample = null;
            }

            if (!KeyboardHandler.AltPressed && beatDivisorSlider.Visible
                && ClampDivisor((int)Math.Round(beatDivisorSlider.current)) != editor.beatSnapDivisor)
                ChangeSnapDivisor((int)Math.Round(beatDivisorSlider.current));
            //adjust note sprite position
            if (CurrentComposeTool != ComposeTools.Select && mousePosition.X > columnStart && mousePosition.X < columnEnd)
            {
                cursorNote.Bypass = false;
                cursorNote.Position = mousePosition;
            }
            else
                cursorNote.Bypass = true;

            if (pendingHold != null)
            {
                int time = yToTime(mousePosition.Y);
                if (time > pendingHold.StartTime)
                    pendingHold.EndTime = time;
                else
                    pendingHold.StartTime = time;
            }

            if (editor.IsDragging)
            {
                updateDrag();
            }

            if (AudioEngine.BeatSyncing)
            {
                currBeat = (int)((AudioEngine.Time - AudioEngine.CurrentOffset) / AudioEngine.beatLength) %
                       (int)AudioEngine.timeSignature;
                currStanza = (int)((AudioEngine.Time - AudioEngine.CurrentOffset) / AudioEngine.beatLength) /
                             ((int)AudioEngine.timeSignature);
            }
            checkLivingMapping();
            hitObjectManager.Update();

            if (selectionChanged)
            {
                UpdateSamples();
                selectionChanged = false;
            }
            lastHoveredColumn = hoveredColumn;
        }

        internal void checkLivingMapping()
        {
            if (editor.liveMapping && AudioEngine.AudioState == AudioStates.Playing && !sampleColumnMode)
            {
                bool hasNote = false;
                for (int i = 0; i < hitObjectManager.ManiaStage.Columns.Count; i++)
                {
                    if (hitObjectManager.ManiaStage.Columns[i].KeyState && !hitObjectManager.ManiaStage.Columns[i].KeyStateLast)
                    {
                        HitCircleOsu h = new HitCircleOsu(hitObjectManager, new Vector2(hitObjectManager.ManiaStage.Columns[i].SnapPositionX, 192), editor.Timing.BeatSnapValue(AudioEngine.Time), false,
                                                                false, false, false, 0);
                        addSpecial(h);
                        pendingLivingNotes[i] = h;
                        hasNote = true;
                    }
                    else if (!hitObjectManager.ManiaStage.Columns[i].KeyState && hitObjectManager.ManiaStage.Columns[i].KeyStateLast)
                    {
                        int end = editor.Timing.BeatSnapValue(AudioEngine.Time);
                        HitCircleOsu h = pendingLivingNotes[i];
                        if (h != null && end - h.StartTime > 300)
                        {
                            hitObjectManager.Remove(h, false);
                            HitCircleHold hh = new HitCircleHold(hitObjectManager, h.Position, h.StartTime, false, false, false, false, 0);
                            hh.EndTime = end;
                            addSpecial(hh);
                            pendingLivingNotes[i] = null;
                            hasNote = true;
                        }
                    }
                }
                if (hasNote)
                {
                    hitObjectManager.Sort(true);
                }
            }
        }

        private int columnAtMousePos()
        {
            if (!inColumnArea) return -1;

            float widthTotal = columnStart;
            for (int i = 0; i < hitObjectManager.ManiaStage.Columns.Count; i++)
            {
                widthTotal += columnWidth;
                if (mousePosition.X <= widthTotal) return i;
            }
            return -1;
        }

        internal override void Draw()
        {
            spriteManager.Draw();
            columnManager.Draw();

            if (dragSelection && editor.DragCaptured == DragCapture.Gamefield)
            {
                //Draw the drag box
                Color lineColourLocal = new Color(255, 255, 255, 180);
                Vector2 dragPoint3 = new Vector2(DragPoint2.X, DragPoint1.Y);
                Vector2 dragPoint4 = new Vector2(DragPoint1.X, DragPoint2.Y);

                GameBase.primitiveBatch.Begin();

                GameBase.primitiveBatch.AddVertex(DragPoint1, lineColourLocal);
                GameBase.primitiveBatch.AddVertex(dragPoint3, lineColourLocal);
                GameBase.primitiveBatch.AddVertex(dragPoint3, lineColourLocal);
                GameBase.primitiveBatch.AddVertex(DragPoint2, lineColourLocal);
                GameBase.primitiveBatch.AddVertex(DragPoint2, lineColourLocal);
                GameBase.primitiveBatch.AddVertex(dragPoint4, lineColourLocal);
                GameBase.primitiveBatch.AddVertex(dragPoint4, lineColourLocal);
                GameBase.primitiveBatch.AddVertex(DragPoint1, lineColourLocal);

                GameBase.primitiveBatch.End();

                GameBase.LineManager.DrawQuad(DragPoint1, DragPoint2, colourDragBoxFill);
            }

            if (AudioEngine.BeatSyncing)
                beatDisplay.Text = String.Format(@"{0}:{1}", currStanza, currBeat);
            else
                beatDisplay.Text = @"-";
        }

        protected override void UpdateCoordinateTicker()
        {
        }

        private pSprite getSpriteAtMouse()
        {
            for (int i = 0; i < columnManager.SpriteList.Count; i++)
            {
                pSprite p = columnManager.SpriteList[i] as pSprite;
                if (p.drawRectangle.Contains(InputManager.CursorPosition.X, InputManager.CursorPosition.Y))
                {
                    if (p.Tag == null)
                        continue;
                    return p;
                }
            }
            return null;
        }

        private HitObject getObjectAtMouse()
        {
            pSprite p = getSpriteAtMouse();
            if (p == null)
                return null;
            return p.Tag as HitObject;
        }

        private EventSample getSampleAtMouse()
        {
            pSprite p = getSpriteAtMouse();
            if (p == null || p.TagNumeric != -10010)
                return null;
            return p.Tag as EventSample;
        }

        internal override bool OnClick()
        {
            if (!Enabled)
                return false;

            bool rightButton = InputManager.rightButton == ButtonState.Pressed;

            if (ReferenceHitObjectManager != null && !inColumnArea)
            {
                SelectNone();
                if (hoveredObject != null)
                {
                    if (selectedRefNote != null)
                        selectedRefNote.Selected = false;
                    selectedRefNote = hoveredObject;
                    selectedRefNote.Selected = true;
                    return false;
                }
            }

            if (!rightButton)
            {
                if (KeyboardHandler.AltPressed)
                {
                    bool showSampleDialog = false;
                    if (sampleColumnMode)
                    {
                        if (selectedSamples.Count > 0)
                        {
                            showSampleDialog = true;
                        }
                        else if (hoveredSample != null)
                        {
                            Select(hoveredSample);
                            showSampleDialog = true;
                        }
                    }
                    else
                    {
                        if (selectedObjects.Count > 0)
                        {
                            showSampleDialog = true;
                        }
                        if (hoveredObject != null)
                        {
                            Select(hoveredObject);
                            showSampleDialog = true;
                        }
                    }

                    if (showSampleDialog)
                        editor.ShowSampleImport();
                    return false;
                }

                if (sampleColumnMode)
                {
                    if (hoveredSample != null)
                    {
                        if (!hoveredSample.Selected)
                        {
                            if (!KeyboardHandler.ControlPressed)
                                SelectNone();
                            Select(hoveredSample);
                        }
                        else if (hoveredSample.Selected) //ctrl+click for adding, click for adding/removing
                        {
                            if (KeyboardHandler.ControlPressed && selectedSamples.Count > 1)
                            {
                                Deselect(hoveredSample);
                                hoveredSample.Selected = false;
                                selectedSamples.Remove(hoveredSample);
                            }
                        }
                    }
                    else
                        SelectNone();
                }
                else
                {
                    int col = (int)((mousePosition.X - columnStart) / columnWidth);

                    switch (CurrentComposeTool)
                    {
                        case ComposeTools.Normal:
                            {
                                if (inColumnArea)
                                {
                                    SelectNone();
                                    editor.UndoPush();
                                    int time = yToTime(mousePosition.Y);
                                    time = editor.Timing.BeatSnapValue(time);
                                    HitCircleOsu h = new HitCircleOsu(hitObjectManager, new Vector2(hitObjectManager.ManiaStage.Columns[col].SnapPositionX, 192), time, false,
                                                                        false, false, false, 0);
                                    addSpecial(h);
                                    hitObjectManager.Sort(true);
                                }
                            }
                            return false;
                        case ComposeTools.Hold:
                            {
                                if (inColumnArea)
                                {
                                    SelectNone();
                                    editor.UndoPush();
                                    int time = yToTime(mousePosition.Y);
                                    time = editor.Timing.BeatSnapValue(time);
                                    HitCircleHold h = new HitCircleHold(hitObjectManager, new Vector2(hitObjectManager.ManiaStage.Columns[col].SnapPositionX, 192), time, false,
                                                                        false, false, false, 0);
                                    h.ActiveEnding = true;
                                    addSpecial(h);
                                    hitObjectManager.Sort(true);
                                    pendingHold = h;
                                }
                            }
                            return false;
                        case ComposeTools.Select:
                            {
                                pSprite p = getSpriteAtMouse();
                                if (p != null)
                                {
                                    HitObject h = (HitObject)p.Tag;
                                    int time = p.TagNumeric;

                                    if (KeyboardHandler.ShiftPressed)
                                    {
                                        if (SoundAdditionStatus[SoundAdditions.NoteLock])
                                        {
                                            if (KeyboardHandler.ControlPressed)
                                                PasteSample(h);
                                            return false;
                                        }

                                        if (h.IsType(HitObjectType.Normal) && time == h.StartTime)
                                        {
                                            //extend normal note to hold
                                            SelectNone();
                                            editor.UndoPush();
                                            hitObjectManager.Remove(h, false);
                                            HitCircleHold hh = new HitCircleHold(hitObjectManager, h.Position, h.StartTime, false, h.Whistle, h.Finish, h.Clap, 0);
                                            hh.EndTime = h.StartTime;
                                            hh.SampleSet = h.SampleSet;
                                            hh.SampleSetAdditions = h.SampleSetAdditions;
                                            hh.CustomSampleSet = h.CustomSampleSet;
                                            hh.SampleFile = h.SampleFile;
                                            hh.SampleAddr = h.SampleAddr;
                                            hh.SampleVolume = h.SampleVolume;
                                            hh.ActiveEnding = true;
                                            addSpecial(hh);
                                            pendingHold = hh;
                                        }
                                    }

                                    if (!h.IsType(HitObjectType.Normal))
                                    {
                                        if (time == h.EndTime)
                                        {
                                            editor.UndoPush();
                                            //active ending
                                            SelectNone();
                                            HitCircleHold hh = (HitCircleHold)h;
                                            hh.ActiveEnding = true;
                                            pendingHold = hh;

                                            return false;
                                        }
                                        else //We could use this to create a modifiable start.
                                        {

                                        }
                                    }
                                }

                                if (hoveredObject != null)
                                {
                                    if (!hoveredObject.Selected)
                                    {
                                        if (!KeyboardHandler.ControlPressed)
                                            SelectNone();
                                        Select(hoveredObject);
                                    }
                                    else if (hoveredObject.Selected) //ctrl+click for adding or removing
                                    {
                                        if (KeyboardHandler.ControlPressed && selectedObjects.Count > 1)
                                        {
                                            Deselect(hoveredObject);
                                        }
                                    }
                                }
                                else
                                    SelectNone();
                                return false;
                            }
                            break;
                    }
                }
            }
            else
            {
                if (sampleColumnMode)
                {
                    if (hoveredSample != null)
                    {
                        editor.UndoPush();
                        if (hoveredSample.Selected)
                        {
                            DeleteSelection();
                        }
                        else
                        {
                            selectedSamples.Remove(hoveredSample);
                            editor.eventManager.Remove(hoveredSample);
                        }
                        hoveredSample = null;
                    }
                }
                else
                {
                    if (hoveredObject != null)
                    {
                        editor.UndoPush();
                        if (hoveredObject.Selected)
                        {
                            DeleteSelection();
                        }
                        else
                        {
                            hitObjectManager.Remove(hoveredObject, false);
                        }
                        hoveredObject = null;
                    }
                }
            }

            return base.OnClick();
        }

        private bool mouseInColumnArea()
        {
            return mousePosition.X > columnStart && mousePosition.X < columnEnd && mousePosition.Y > columnTop && mousePosition.Y < columnBottom;
        }

        internal override bool OnClickUp()
        {
            /*if (dragSelection)
            {
                if (Math.Abs(DragPoint1.X - DragPoint2.X) < 10 && Math.Abs(DragPoint1.Y - DragPoint2.Y) < 10)
                    dragSelection = false;
            }*/
            if (pendingHold != null)
            {
                pendingHold.ActiveEnding = false;
                pendingHold.EndTime = editor.Timing.BeatSnapValue(pendingHold.EndTime);
                pendingHold.StartTime = editor.Timing.BeatSnapValue(pendingHold.StartTime);
                if (pendingHold.EndTime == pendingHold.StartTime)
                {
                    hitObjectManager.Remove(pendingHold, false);
                    //change into a normal note.
                    HitCircleOsu h = (HitCircleOsu)pendingHold.CloneAsNormal();
                    hitObjectManager.AddSpecial(h);
                }
                pendingHold = null;
            }

            return false;
        }

        /// <summary>
        /// Double click on note:  sample preview
        /// ctrl + double click : note/sample toggle
        /// double click on reference : note copy
        /// </summary>
        internal override bool OnDoubleClick()
        {
            if (!Enabled)
                return false;

            if (sampleColumnMode)
            {
                if (hoveredSample == null) return false;
                EventSample e = hoveredSample;

                //convert sample to note
                if (KeyboardHandler.ControlPressed)
                {
                    editor.UndoPush();
                    HitCircleOsu h = new HitCircleOsu(hitObjectManager, new Vector2(hitObjectManager.ManiaStage.Columns[0].SnapPositionX, 192), editor.Timing.BeatSnapValue(e.StartTime), false,
                                                                    false, false, false, 0);
                    h.SampleFile = e.Filename;
                    h.SampleVolume = e.Volume;
                    h.SampleAddr = e.SampleId;
                    addSpecial(h);
                    hitObjectManager.Sort(true);
                    editor.eventManager.Remove(e);
                }
                else
                    AudioEngine.PlaySamplePositional(e.Filename, 1, SkinSource.All);

                return true;
            }
            else
            {
                if (hoveredObject == null) return false;
                HitObject h = hoveredObject;

                if (inColumnArea)
                {
                    //convert note to sample(only if note has an assigned sample file)
                    if (KeyboardHandler.ControlPressed && !string.IsNullOrEmpty(h.SampleFile))
                    {
                        editor.UndoPush();
                        EventSample e = new EventSample(h.SampleAddr, h.SampleFile, h.StartTime, StoryLayer.Background, h.SampleVolume);
                        if (e.Volume == 0)
                            e.Volume = 70;
                        editor.eventManager.Add(e);
                        hitObjectManager.Remove(h, true);
                    }
                    else
                        h.PlaySound();
                }
                else
                {
                    editor.UndoPush();
                    HitObject clone = h.Clone();
                    clone.Selected = false;
                    addSpecial(clone);
                }

                return true;
            }

            return false;
        }

        private void addSpecial(HitObject h)
        {
            //h.ProcessSampleFile(SampleImport.CurrentSample);
            hitObjectManager.AddSpecial(h);
        }

        internal override bool OnMouseWheelDown()
        {
            if (KeyboardHandler.ControlPressed)
            {
                ChangeSnapDivisor(Math.Max(1, editor.beatSnapDivisor / 2));
                return true;
            }

            return false;
        }

        internal override bool OnMouseWheelUp()
        {
            if (KeyboardHandler.ControlPressed)
            {
                ChangeSnapDivisor(Math.Min(16, editor.beatSnapDivisor * 2));
                return true;
            }

            return false;
        }

        internal override void OnDragEnd()
        {
            dragSelection = false;

            if (!SoundAdditionStatus[SoundAdditions.NoteLock])
            {
                if (IsDragModified)
                {
                    if (sampleColumnMode)
                    {
                        if (selectedSamples.Count > 0)
                        {
                            int moveDistance = selectedSamples[0].StartTime - editor.Timing.BeatSnapValue(selectedSamples[0].StartTime);
                            foreach (EventSample e in selectedSamples)
                                e.StartTime = editor.Timing.BeatSnapClosestValue(e.StartTime - moveDistance);
                            editor.eventManager.eventSamples.Sort();
                        }
                    }
                    else
                    {
                        selectedObjects.Sort();
                        if (selectedObjects.Count > 0)
                        {
                            int moveDistance = selectedObjects[0].StartTime - editor.Timing.BeatSnapValue(selectedObjects[0].StartTime);

                            if (selectedObjects.Count == 1)
                            {
                                HitObject h = selectedObjects[0];

                                h.StartTime = editor.Timing.BeatSnapValue(h.StartTime);
                                h.EndTime = editor.Timing.BeatSnapValue(h.EndTime);
                            }
                            else
                            {
                                foreach (HitObject h in selectedObjects)
                                {
                                    h.StartTime = editor.Timing.BeatSnapClosestValue(h.StartTime - moveDistance);
                                    h.EndTime = editor.Timing.BeatSnapClosestValue(h.EndTime - moveDistance);
                                }
                            }
                            hitObjectManager.Sort(true);
                        }
                    }
                }
            }
            IsDragModified = false;
            editor.DragCaptured = DragCapture.None;
        }

        internal override bool OnDragStart()
        {
            int colStart = columnStart - 100;
            float colEnd = columnEnd + columnWidth;

            if (inColumnArea || (mousePosition.X >= columnStart - 20 && mousePosition.X <= colEnd + 20))
                editor.DragCaptured = DragCapture.Gamefield;

            if (mousePosition.X >= colStart && mousePosition.X <= colEnd && mousePosition.Y > columnTop)
            {
                DragPoint1 = InputManager.CursorPosition;
                DragPoint2 = DragPoint1;

                if (sampleColumnMode)
                {
                    if (hoveredSample != null)
                    {
                        dragStartTime = yToTime(mousePosition.Y);
                        dragStartX = mousePosition.X;
                        editor.UndoPush();

                        IsDragModified = true;
                        return false;
                    }
                }
                else
                {
                    if (hoveredObject != null)
                    {
                        dragStartTime = yToTime(mousePosition.Y);
                        dragStartX = mousePosition.X;

                        //State is handled OnClick in this case.
                        if (pendingHold == null || !pendingHold.ActiveEnding)
                            editor.UndoPush();

                        IsDragModified = true;
                        return false;
                    }
                }

                dragSelection = CurrentComposeTool == ComposeTools.Select;
            }
            return false;
        }

        internal override bool OnKey(Keys keys, bool first)
        {
            Bindings b = BindingManager.For(keys, BindingTarget.Editor);

            if (first)
            {
                switch (b)
                {
                    case Bindings.SelectTool:
                        {
                            ChangeComposeTool(ComposeTools.Select);
                            return true;
                        }
                    case Bindings.NormalTool:
                        {
                            ChangeComposeTool(ComposeTools.Normal);
                            return true;
                        }
                    case Bindings.SliderTool:
                        {
                            ChangeComposeTool(ComposeTools.Hold);
                            return true;
                        }
                }
            }

            if (editor.liveMapping && AudioEngine.AudioState == AudioStates.Playing)
            {
                if (keys == Keys.Escape)
                    editor.Pause();
                return true;
            }

            if (KeyboardHandler.ControlPressed && first)
            {
                if (sampleColumnMode)
                {
                    if (selectedSamples.Count > 0)
                    {
                        switch (keys)
                        {
                            case Keys.Up:
                                shiftSelectedSamples(true);
                                return true;
                            case Keys.Down:
                                shiftSelectedSamples(false);
                                return true;
                        }
                    }
                }
                else
                {
                    if (selectedObjects.Count > 0)
                    {
                        switch (keys)
                        {
                            case Keys.Up:
                                shiftSelectedNotes(0, 1);
                                return true;
                            case Keys.Down:
                                shiftSelectedNotes(0, -1);
                                return true;
                            case Keys.Left:
                                shiftSelectedNotes(-1, 0);
                                return true;
                            case Keys.Right:
                                shiftSelectedNotes(1, 0);
                                return true;
                        }
                    }
                }
            }
            else
            {
                switch (keys)
                {
                    case Keys.Tab:
                        sampleColumnMode = !sampleColumnMode;

                        if (sampleColumnMode)
                            hoveredObject = null;
                        else
                            hoveredSample = null;

                        //hide vertical line in sample editing mode
                        spriteManager.SpriteList.ForEach(p =>
                        {
                            if (p.Tag == null || p.Tag != @"vertical")
                                return;
                            if (sampleColumnMode)
                                p.Hide();
                            else
                                p.Show();
                        });
                        return true;
                }
            }
            return base.OnKey(keys, first);
        }

        private void shiftSelectedSamples(bool moveFwd)
        {
            if (selectedSamples.Count == 0) return;

            editor.UndoPush();

            if (moveFwd)
                selectedSamples.ForEach(s => s.StartTime += (int)editor.TickSpacing);
            else
                selectedSamples.ForEach(s => s.StartTime -= (int)editor.TickSpacing);
        }

        private void shiftSelectedNotes(int dirColumn, int dirTime)
        {
            if (selectedObjects.Count == 0) return;

            editor.UndoPush();

            foreach (HitObject h in selectedObjects)
            {
                if (dirColumn != 0)
                {
                    int col = hitObjectManager.ManiaStage.ColumnAt(h.Position);
                    col += dirColumn;
                    col = Math.Min(column - 1, Math.Max(0, col));
                    h.Position.X = hitObjectManager.ManiaStage.Columns[col].SnapPositionX;
                }
                if (dirTime != 0)
                {
                    double increment = AudioEngine.beatLengthAt(h.StartTime, false) / editor.beatSnapDivisor;
                    h.StartTime += (int)increment * dirTime;
                    h.EndTime += (int)increment * dirTime;
                }
            }
        }

        internal override void Enter()
        {
            if (column != (int)Math.Round(BeatmapManager.Current.DifficultyCircleSize))
                Initialize();
            else
                base.Enter();
        }

        internal override void Leave()
        {
            base.Leave();
        }

        private float timeToY(int time)
        {
            return columnBottom - (time - timeRangeStart) * (columnBottom - columnTop) * 1F / (timeRangeEnd - timeRangeStart);
        }

        private int yToTime(float y)
        {
            return (int)((timeRangeEnd - timeRangeStart) * (columnBottom - y) * 1F / (columnBottom - columnTop) + timeRangeStart);
        }

        internal override void DrawTimeline()
        {
            //if (!columnDirty && AudioEngine.Time == columnLastTime && editor.TimelineMagnification == columnLastScale)
            //    return;

            updateTimeRange();
            columnLastTime = AudioEngine.Time;
            columnLastScale = editor.TimelineMagnification;

            columnManager.Clear();
            //draw beat
            DrawTimelineBeat();
            //draw notes
            if (sampleColumnMode)
            {
                List<EventSample> list = editor.eventManager.eventSamples;
                //do we need sort here?
                lastSampleOffset = -1;
                for (int i = 0; i < list.Count; i++)
                {
                    EventSample e = list[i];
                    if (e.StartTime < timeRangeStart || e.StartTime > timeRangeEnd)
                        continue;
                    drawSampleAt(e, e.StartTime);
                }
            }
            else
            {
                int size = 100;
                int[] timebar = new int[size];
                int totalLen = AudioEngine.AudioLength;
                int delta = (int)Math.Ceiling(totalLen * 1.0 / size);
                for (int i = 0; i < hitObjectManager.hitObjects.Count; i++)
                {
                    HitObject h = hitObjectManager.hitObjects[i];
                    //mapping to timebar
                    int start = h.StartTime / delta;
                    int end = h.EndTime / delta;
                    if (start >= size)
                        continue;
                    timebar[start]++;
                    if (start != end)
                    {
                        while (++start <= end && start < size)
                        {
                            timebar[start]++;
                        }
                    }

                    if (h.EndTime < timeRangeStart)
                        continue;
                    if (h.StartTime > timeRangeEnd)
                        continue;
                    drawNoteAt(h, column, 0, editor.showSampleName);
                }

                //add timebar sprite
                //maximun note density
                double normal = (60000 / BeatmapManager.Current.BeatsPerMinuteRange.Z) / 2;  // use 1/2 beat as normal level
                int max = (int)(delta / normal * column * 0.6f);  // equals place notes on each 1/2 beat each column.
                for (int i = 0; i < size; i++)
                {
                    if (timebar[i] == 0)
                        continue;
                    Color c = new Color(247, 236, 0);
                    if (timebar[i] > max)
                        c = new Color(250, 170, 212);
                    pSprite p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.BottomLeft, Clocks.Game,
                                            new Vector2(timebarStart, columnBottom - (columnBottom - columnTop) * 1.0f / size * i),
                                            0.93F, true, c);
                    p.VectorScale = new Vector2(60.0f / max * Math.Min(max, timebar[i]), (columnBottom - columnTop) * 1.0f / size - 1) * 1.6f;
                    columnManager.Add(p);
                }
            }
            if (ReferenceHitObjectManager != null)
            {
                for (int i = 0; i < ReferenceHitObjectManager.hitObjects.Count; i++)
                {
                    HitObject h = ReferenceHitObjectManager.hitObjects[i];
                    if (h.EndTime < timeRangeStart)
                        continue;
                    if (h.StartTime > timeRangeEnd)
                        break;
                    drawNoteAt(h, (int)Math.Round(ReferenceHitObjectManager.Beatmap.DifficultyCircleSize), column * columnWidth + columnWidth, editor.showSampleName);
                }
            }
            //move timebar window
            float barStart = columnBottom - (timeRangeStart * 1.0f / AudioEngine.AudioLength) * (columnBottom - columnTop);
            float barEnd = columnBottom - (timeRangeEnd * 1.0f / AudioEngine.AudioLength) * (columnBottom - columnTop);
            timebarWindow.Position.Y = barStart;
            timebarWindow.VectorScale.Y = (barStart - barEnd);
        }

        private int lastSampleOffset = -1;
        private float lastSampleLeft;
        private void drawSampleAt(EventSample e, int time)
        {
            if (time != lastSampleOffset)
                lastSampleLeft = columnStart;
            else
                lastSampleLeft += column * columnWidth / 8;
            lastSampleOffset = time;
            Color c = e.Selected ? Color.LightSkyBlue : Color.Orange;
            pSprite p =
                    new pSprite(SkinManager.Load("mania-note1", SkinSource.Osu), Fields.TopLeft, Origins.BottomLeft, Clocks.Game,
                        new Vector2(lastSampleLeft, timeToY(time)), 0.92F, true, c, e);
            p.TagNumeric = -10010;
            p.VectorScale = new Vector2(column / 8.0F * columnRatio, 1) * 0.1875F;
            columnManager.Add(p);
        }

        private void drawNoteAt(HitObject h, int column, float offset = 0, bool showSample = false)
        {
            int col = h.hitObjectManager.ManiaStage.ColumnAt(h.Position, false);
            Color c = h.Selected ? Color.LightSkyBlue : h.hitObjectManager.ManiaStage.Columns[col].Colour;
            pSprite p;
            if (h.StartTime == h.EndTime)
            {
                //normal note
                p =
                    new pSprite(SkinManager.Load("mania-note1", SkinSource.Osu), Fields.TopLeft, Origins.BottomLeft, Clocks.Game,
                        new Vector2(columnStart + columnWidth * col + offset, timeToY(h.StartTime)), 0.92F, true, c, h);
                p.TagNumeric = h.StartTime;
                p.VectorScale.X = columnRatio;
                p.Scale = 0.1875F;
                columnManager.Add(p);
            }
            else
            {
                //hold note
                if (h.StartTime >= timeRangeStart && h.StartTime < timeRangeEnd)
                {
                    p =
                    new pSprite(SkinManager.Load("mania-note1", SkinSource.Osu), Fields.TopLeft, Origins.BottomLeft, Clocks.Game,
                        new Vector2(columnStart + columnWidth * col + offset, timeToY(h.StartTime)), 0.92F, true, c, h);
                    p.TagNumeric = h.StartTime;
                    p.VectorScale.X = columnRatio;
                    p.Scale = 0.1875F;
                    columnManager.Add(p);
                }
                if (h.EndTime >= timeRangeStart && h.EndTime < timeRangeEnd)
                {
                    p =
                    new pSprite(SkinManager.Load("mania-note1", SkinSource.Osu), Fields.TopLeft, Origins.BottomLeft, Clocks.Game,
                        new Vector2(columnStart + columnWidth * col + offset, timeToY(h.EndTime)), 0.92F, true, c, h);
                    p.TagNumeric = h.EndTime;
                    p.VectorScale.X = columnRatio;
                    p.Scale = 0.1875F;
                    columnManager.Add(p);
                }
                /*
                 * ----Top > endY
                 * |  |
                 * |  |
                 * ----Bottom > startY
                 * */
                float startY = Math.Min(columnBottom, timeToY(h.StartTime));
                float endY = Math.Max(columnTop, timeToY(h.EndTime));
                p =
                    new pSprite(SkinManager.LoadAll("mania-note1L", SkinSource.Osu)[0], Fields.TopLeft, Origins.BottomLeft, Clocks.Game,
                        new Vector2(columnStart + columnWidth * col + offset, startY), 0.918F, true, c, h);
                p.TagNumeric = h.StartTime;
                p.VectorScale = new Vector2(0.1875F * columnRatio, (startY - endY) / 82 * 1.6F);
                columnManager.Add(p);
            }
            if (showSample && h.StartTime > timeRangeStart && h.StartTime < timeRangeEnd)
            {
                string display = string.Empty;
                if (!string.IsNullOrEmpty(h.SampleFile))
                    display = shortFileName(h.SampleFile);
                else if (h.SoundType != HitObjectSoundType.None)
                {
                    if (h.Whistle)
                        display += @"W|";
                    if (h.Finish)
                        display += @"F|";
                    if (h.Clap)
                        display += @"C";
                    if (display.EndsWith(@"|"))
                        display = display.Substring(0, display.Length - 1);
                }
                if (display != string.Empty)
                {
                    pText pt = new pText(display, 10f, new Vector2(columnStart + columnWidth * col + offset, timeToY(h.StartTime) - 10), 0.93f, true, Color.White);
                    columnManager.Add(pt);
                }
            }
        }

        private string shortFileName(string org)
        {
            return org.Substring(0, org.LastIndexOf(@"."));
        }
        /// <summary>
        /// copy from editor.drawTimeline
        ///
        /// </summary>
        internal void DrawTimelineBeat()
        {
            double TickSpacing = AudioEngine.beatLengthAt(AudioEngine.Time, false) / editor.beatSnapDivisor;

            double firstVisibleTick;
            int firstTickCount;

            if (AudioEngine.BeatSyncing)
            {
                firstTickCount = (int)((timeRangeStart - AudioEngine.CurrentOffset) / AudioEngine.beatLength);
                firstVisibleTick = firstTickCount * AudioEngine.beatLength + AudioEngine.CurrentOffset;
            }
            else
            {
                firstVisibleTick = timeRangeStart / TickSpacing + TickSpacing;
                firstTickCount = 0;
            }

            int timeSig = AudioEngine.ActiveInheritedTimingPointIndex < 0 ? 4 : (int)AudioEngine.ControlPoints[AudioEngine.ActiveInheritedTimingPointIndex].timeSignature;

            if (TickSpacing > 1.0e-5)
            {
                int count = 0;
                for (double i = firstVisibleTick; i < timeRangeEnd; i += TickSpacing)
                {
                    int divr = count % editor.beatSnapDivisor;
                    Color tickCol = editor.lineColourAlphaWhite;
                    if (editor.beatSnapDivisor == 1)
                        tickCol = editor.lineColourAlphaWhite;
                    else if (editor.beatSnapDivisor == 2)
                        tickCol = (divr == 0 ? editor.lineColourAlphaWhite : editor.lineColourAlphaRed);
                    else if (editor.beatSnapDivisor == 4)
                    {
                        if (divr == 0)
                            tickCol = editor.lineColourAlphaWhite;
                        else if (divr == 1 || divr == 3)
                            tickCol = editor.lineColourAlphaBlue;
                        else
                            tickCol = editor.lineColourAlphaRed;
                    }
                    else if (editor.beatSnapDivisor == 8 || editor.beatSnapDivisor == 16)
                    {
                        if (divr == 0)
                            tickCol = editor.lineColourAlphaWhite;
                        else if ((divr - 1) % 2 == 0)
                            tickCol = editor.lineColourAlphaOrange;
                        else if (divr % 4 == 0)
                            tickCol = editor.lineColourAlphaRed;
                        else
                            tickCol = editor.lineColourAlphaBlue;
                    }
                    else if (editor.beatSnapDivisor == 3 || editor.beatSnapDivisor == 6 || editor.beatSnapDivisor == 12)
                    {
                        if (divr % 3 == 0)
                            tickCol = editor.lineColourAlphaRed;
                        else if (divr == 0)
                            tickCol = editor.lineColourAlphaWhite;
                        else
                            tickCol = editor.lineColourAlphaViolet;
                    }

                    if (i >= timeRangeStart)
                    {
                        float y = timeToY((int)i);
                        pSprite p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(columnStart, y), 0.916F, true, tickCol, null);
                        p.VectorScale = new Vector2(column * columnWidth, 1) * 1.6F;
                        columnManager.Add(p);

                        if (ReferenceHitObjectManager != null)
                        {
                            p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(columnStart + columnWidth * column + columnWidth, y), 0.916F, true, tickCol, null);
                            p.VectorScale = new Vector2((int)Math.Round(ReferenceHitObjectManager.Beatmap.DifficultyCircleSize) * columnWidth, 1) * 1.6F;
                            p.TagNumeric = -10086;
                            columnManager.Add(p);
                        }
                        if (firstTickCount % timeSig == 0 && count % editor.beatSnapDivisor == 0)
                        {
                            p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(columnStart, y - 2), 0.916F, true, tickCol, null);
                            p.VectorScale = new Vector2(column * columnWidth, 1) * 1.6F;
                            columnManager.Add(p);

                            if (ReferenceHitObjectManager != null)
                            {
                                p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(columnStart + columnWidth * column + columnWidth, y - 2), 0.916F, true, tickCol, null);
                                p.VectorScale = new Vector2((int)Math.Round(ReferenceHitObjectManager.Beatmap.DifficultyCircleSize) * columnWidth, 1) * 1.6F;
                                p.TagNumeric = -10086;
                                columnManager.Add(p);
                            }
                        }
                    }

                    if (count % editor.beatSnapDivisor == 0)
                        firstTickCount++;

                    count++;

                }
            }
        }

        internal override void DrawTimelineLow()
        {

        }

        internal override void UpdateTimeline()
        {
        }

        internal void UpdateSamples()
        {
            SampleSet ss = SampleSet.None;
            SampleSet ssa = SampleSet.None;

            bool whistle, finish, clap;
            if (sampleColumnMode || selectedObjects.Count == 0)
            {
                whistle =
                finish =
                clap = false;
            }
            else
            {
                ss = selectedObjects[0].SampleSet;
                ssa = selectedObjects[0].SampleSetAdditions;

                finish =
                whistle =
                clap = true;
                bool checkSS = true, checkSSA = true;
                foreach (HitObject h in selectedObjects)
                {
                    if (checkSS && ss != h.SampleSet)
                    {
                        ss = SampleSet.None;
                        checkSS = false;
                    }

                    if (checkSSA && ssa != h.SampleSetAdditions)
                    {
                        ssa = SampleSet.None;
                        checkSSA = false;
                    }

                    whistle &= h.Whistle;
                    finish &= h.Finish;
                    clap &= h.Clap;
                }
            }

            ChangeSoundAddition(SoundAdditions.Whistle, whistle);
            ChangeSoundAddition(SoundAdditions.Finish, finish);
            ChangeSoundAddition(SoundAdditions.Clap, clap);

            if (ss != (SampleSet)ddSampleSet.SelectedObject)
                ddSampleSet.SetSelected(ss, true);
            if (ssa != (SampleSet)ddSampleSetAddition.SelectedObject)
                ddSampleSetAddition.SetSelected(ssa, true);
        }

        internal override void Select(params HitObjectBase[] objects)
        {
            base.Select(objects);
        }

        internal override void Select(HitObject h)
        {
            if (selectedObjects.Contains(h)) return;

            selectedObjects.Add(h);
            h.Selected = true;
            coordinateText.Text = h.SampleFile + "\n";
            coordinateText.ToolTip = h.SampleFile;
            if (h.SampleVolume != 0)
                coordinateText.Text += h.SampleVolume.ToString() + @"%";

            selectionChanged = true;
        }

        internal void Select(EventSample e)
        {
            if (selectedSamples.Contains(e)) return;

            selectedSamples.Add(e);
            e.Selected = true;
            coordinateText.Text = shortFileName(e.Filename) + "\n";
            coordinateText.ToolTip = shortFileName(e.Filename);
            if (e.Volume != 0)
                coordinateText.Text += e.Volume.ToString() + @"%";

            selectionChanged = true;
        }

        internal void Deselect(EventSample e)
        {
            if (!e.Selected) return;

            e.Selected = false;
            selectedSamples.Remove(e);
            selectionChanged = true;
        }

        internal override void SelectAll()
        {
            SelectNone();
            if (sampleColumnMode)
            {
                foreach (EventSample e in editor.eventManager.eventSamples)
                {
                    Select(e);
                }
            }
            else
            {
                foreach (HitObject h in hitObjectManager.hitObjects)
                {
                    Select(h);
                }
            }

        }

        internal override void SelectNone()
        {
            if (sampleColumnMode)
            {
                if (selectedSamples.Count == 0) return;

                for (int i = 0; i < selectedSamples.Count; i++)
                    selectedSamples[i].Selected = false;
                selectedSamples.Clear();
                selectionChanged = true;
            }
            else
            {
                if (selectedObjects.Count == 0) return;

                for (int i = 0; i < selectedObjects.Count; i++)
                    selectedObjects[i].Selected = false;
                selectedObjects.Clear();
                selectionChanged = true;
            }
            if (selectedRefNote != null)
                selectedRefNote.Selected = false;
            selectedRefNote = null;
            coordinateText.Text = string.Empty;
            coordinateText.ToolTip = string.Empty;
        }

        internal override void Reset()
        {

        }

        /// <summary>
        ///
        /// </summary>
        internal override void FlipSelectionHorizontal()
        {
            if (selectedObjects.Count == 0)
                return;
            editor.UndoPush();
            foreach (HitObject h in selectedObjects)
            {
                int col = hitObjectManager.ManiaStage.ColumnAt(h.Position);
                col = column - col - 1;
                h.Position.X = hitObjectManager.ManiaStage.Columns[col].SnapPositionX;
            }
        }

        /// <summary>
        ///
        /// </summary>
        internal override void FlipSelectionVertical()
        {
            if (selectedObjects.Count == 0)
                return;
            editor.UndoPush();
            //find earliest and latest time
            int start = int.MaxValue;
            int end = int.MinValue;
            foreach (HitObject h in selectedObjects)
            {
                if (h.StartTime < start)
                    start = h.StartTime;
                if (h.EndTime > end)
                    end = h.EndTime;
            }

            int beatSnapStart, beatSnapEnd;
            //reverse
            foreach (HitObject h in selectedObjects)
            {
                int _start = end - (h.StartTime - start);
                int _end = end - (h.EndTime - start);

                if (h.IsType(HitObjectType.Hold))
                {
                    h.StartTime = _end;
                    h.EndTime = _start;
                }
                else
                {
                    h.StartTime = _start;
                    h.EndTime = _end;
                }

                beatSnapStart = editor.Timing.BeatSnapClosestValue(h.StartTime);
                beatSnapEnd = editor.Timing.BeatSnapClosestValue(h.EndTime);

                if (Math.Abs(beatSnapStart - h.StartTime) == 1)
                    h.StartTime = beatSnapStart;
                if (Math.Abs(beatSnapEnd - h.EndTime) == 1)
                    h.EndTime = beatSnapEnd;
            }
        }

        internal override void CutSelection()
        {
            if (sampleColumnMode)
            {
                if (selectedSamples.Count == 0)
                    return;

                CopySelection();
                DeleteSelection();
            }
            else
            {
                base.CutSelection();
            }
        }

        internal override void PasteSelection()
        {
            if (sampleColumnMode)
            {
                if (copiedSamples.Count == 0)
                    return;

                editor.UndoPush();
                SelectNone();

                int diff = (AudioEngine.BeatSyncing ? editor.Timing.BeatSnapValue(AudioEngine.Time)
                    : AudioEngine.Time) - copiedSamples[0].StartTime;

                int newStart, newEnd;
                foreach (EventSample s in copiedSamples)
                {
                    EventSample clone = (EventSample)s.Clone();

                    clone.StartTime += diff;
                    clone.EndTime += diff;

                    newStart = editor.Timing.BeatSnapClosestValue(clone.StartTime);
                    newEnd = editor.Timing.BeatSnapClosestValue(clone.EndTime);

                    if (Math.Abs(newStart - clone.StartTime) == 1)
                        clone.StartTime = newStart;
                    if (Math.Abs(newEnd - clone.EndTime) == 1)
                        clone.EndTime = newEnd;

                    Select(clone);
                    editor.eventManager.Add(clone);
                }
            }
            else
            {
                if (copiedObjects.Count == 0)
                    return;

                editor.UndoPush();
                SelectNone();

                //Sort so the difference we get is for the earliest object.
                copiedObjects.Sort((h1, h2) => h1.StartTime.CompareTo(h2.StartTime));

                int diff = (AudioEngine.BeatSyncing ? editor.Timing.BeatSnapValue(AudioEngine.Time)
                      : AudioEngine.Time) - copiedObjects[0].StartTime;

                int newStart, newEnd;
                foreach (HitObject h in copiedObjects)
                {
                    HitObject clone = h.Clone();

                    clone.StartTime += diff;
                    clone.EndTime += diff;

                    newStart = editor.Timing.BeatSnapClosestValue(clone.StartTime);
                    newEnd = editor.Timing.BeatSnapClosestValue(clone.EndTime);

                    //We might be pasting into a different bpm/offset. Only resnap if object is minimally unsnapped.
                    if (Math.Abs(newStart - clone.StartTime) == 1)
                        clone.StartTime = newStart;
                    if (Math.Abs(newEnd - clone.EndTime) == 1)
                        clone.EndTime = newEnd;

                    clone.Selected = false;
                    addSpecial(clone);
                }

                hitObjectManager.Sort(true);
            }
        }

        internal override void CopySelection()
        {
            string selectedList = string.Empty;
            int startTime = AudioEngine.Time;

            if (sampleColumnMode)
            {
                copiedSamples.Clear();
                selectedSamples.Sort();

                if (selectedSamples.Count > 0)
                {
                    foreach (EventSample e in selectedSamples)
                    {
                        copiedSamples.Add((EventSample)e.Clone());
                        selectedList += e.ToString() + "\n";
                    }
                    selectedList.Remove(selectedList.Length - 1);
                }
            }
            else
            {
                copiedObjects.Clear();
                selectedObjects.Sort();

                if (selectedObjects.Count > 0)
                {
                    if (SoundAdditionStatus[SoundAdditions.NoteLock])
                    {
                        if (selectedObjects.Count == 1)
                            sampleNote = selectedObjects[0];

                        if (selectedRefNote != null)
                            sampleNote = selectedRefNote;
                    }

                    selectedList = @" (";
                    foreach (HitObject h in selectedObjects)
                    {
                        copiedObjects.Add(h.Clone());
                        selectedList += string.Format(@"{0}|{1},", h.StartTime, hitObjectManager.ManiaStage.ColumnAt(h.Position));
                    }

                    selectedList = selectedList.Trim(',') + @")";
                    startTime = selectedObjects[0].StartTime;
                }
            }

            if (!sampleColumnMode || sampleColumnMode && copiedSamples.Count == 0)
            {
                string time = String.Format(@"{0:00}:{1:00}:{2:000}",
                       startTime / 60000, startTime % 60000 / 1000, startTime % 1000);
                selectedList = time + selectedList + @" - ";
            }

            try
            {
                Clipboard.SetText(selectedList);
            }
            catch (Exception)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.General_Editor_ClipboardNotAvailable));
            }
        }

        internal override void DeleteSelection()
        {
            base.DeleteSelection();
            if (sampleColumnMode)
            {
                if (selectedSamples.Count == 0) return;

                editor.UndoPush();

                selectedSamples.ForEach(e => editor.eventManager.Remove(e));
                SelectNone();
            }
        }

        internal override bool ExitEditor()
        {
            if (sampleColumnMode)
            {
                if (selectedSamples.Count > 0)
                {
                    SelectNone();
                    return false;
                }
                return true;
            }
            return base.ExitEditor();
        }
    }
}
