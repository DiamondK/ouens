using System;
using System.Collections.Generic;
using System.Diagnostics;
using osu.Audio;
using osu.Graphics.Notifications;
using osu_common.Helpers;

namespace osu.GameModes.Edit.Modes
{
    partial class EditorModeTiming
    {
        private int lastBeat; //Not used
        private List<int> taps = new List<int>();
        private int metronomeDisableCount;

        internal void TimingReset()
        {
            if (changeInProgress) return;

            if (AudioEngine.ActiveTimingPointIndex >= 0)
            {
                ControlPoint active = AudioEngine.ControlPoints[AudioEngine.ActiveTimingPointIndex];

                //Default to 120BPM
                active.beatLength = 0;
                active.offset = AudioEngine.ActiveTimingPointIndex == 0 ?
                    0 : AudioEngine.ControlPoints[AudioEngine.ActiveTimingPointIndex - 1].offset + 1000; //Seems like sketchy behavior
                active.timingChange = true;
                
                lastBeat = 0;  
                AudioEngine.SeekTo((int)active.offset);
            }
            else
            {
                AudioEngine.Play();
            }

            taps.Clear();

            AudioEngine.UpdateActiveTimingPoint(true);           
        }

        private void TimingTap()
        {
            if (changeInProgress) return;

            if (AudioEngine.AudioState != AudioStates.Playing)
                AudioEngine.Play();

            metronomeDisableCount = 3;

            if (taps.Count > 0 && AudioEngine.Time == taps[taps.Count - 1])
                return;

            if (taps.Count > 0 && taps[taps.Count - 1] > AudioEngine.Time)
                taps.Clear();

            if (taps.Count == 1 && AudioEngine.Time - taps[0] > 1000)
                taps.Clear();

            if (taps.Count == 0)
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.EditorModeTiming_Tap_TapInTimeToTheBeat), 5000);
            else if (taps.Count == 2)
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.EditorModeTiming_Tap_KeepGoing), 5000);
            else if (taps.Count < 4)
            {
            }
            else if (taps.Count < 8)
                NotificationManager.ShowMessageMassive(string.Format(LocalisationManager.GetString(OsuString.EditorModeTiming_Tap_TapMoreTimes), 7 - taps.Count), 5000);
            else if (taps.Count == 8)
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.EditorModeTiming_Tap_TapToIncreaseAccuracy), 5000);
            else if (taps.Count == 16)
                NotificationManager.ShowMessageMassive(string.Format(LocalisationManager.GetString(OsuString.EditorModeTiming_Tap_BPMSensitivity), 0), 5000);
            else if (taps.Count == 32)
                NotificationManager.ShowMessageMassive(string.Format(LocalisationManager.GetString(OsuString.EditorModeTiming_Tap_BPMSensitivity), 1), 5000);
            else if (taps.Count == 64)
                NotificationManager.ShowMessageMassive(string.Format(LocalisationManager.GetString(OsuString.EditorModeTiming_Tap_BPMSensitivity), 2), 5000);
         
            taps.Add(AudioEngine.Time);

            int removeCount = 0;


            if (taps.Count >= 8)
            {
            restart:
                if (taps.Count < 2) return;
                //AudioEngine.Stop();

                double averageBeatLength = (taps[taps.Count - 1] - taps[1]) / (taps.Count - 2f);

                for (int i = 0; i < taps.Count - 1; i++)
                {
                    int removeLength = taps[i + 1] - taps[i];

                    if (Math.Abs(removeLength) > averageBeatLength * 1.4)
                    {
                        taps.RemoveAt(i+1);

                        if (i < taps.Count - 1)
                        {
                            removeLength = taps[i + 1] - taps[i];
                            taps.RemoveAt(i + 1);
                        }

                        for (int j = i + 1; j < taps.Count; j++)
                            taps[j] -= removeLength;

                        removeCount++;
                        goto restart;
                    }
                }

                double averageDeviance = 0;
                for (int i = 1; i < taps.Count - 1; i++)
                    averageDeviance += Math.Abs(taps[i + 1] - (taps[i] + averageBeatLength)) * (1 + i/64d);
                averageDeviance /= (taps.Count - 1f);

                double averageBeatLengthHigh = (taps[taps.Count - 1] - taps[1] + averageDeviance) / (taps.Count - 2f);
                double averageBeatLengthLow = (taps[taps.Count - 1] - taps[1] - averageDeviance) / (taps.Count - 2f);

                double thisBeatLength = averageBeatLengthLow;
                double bestBpm = 0;
                double bestBeatLength = 0;
                double bestBeatLengthSeparation = 10000;
                while (thisBeatLength < averageBeatLengthHigh)
                {
                    double thisBpm = Math.Round(60000 / thisBeatLength,taps.Count >= 64 ? 2 : (taps.Count < 32 ? 0 : 1));
                    if (Math.Abs(60000 / thisBpm - averageBeatLength) < bestBeatLengthSeparation)
                    {
                        bestBeatLengthSeparation = Math.Abs(60000 / bestBpm - averageBeatLength);
                        bestBpm = thisBpm;
                    }
                    
                    thisBeatLength += taps.Count >= 64 ? 0.05 : (taps.Count < 32 ? 0.1 : 0.5);
                }

                bestBeatLength = 60000d / bestBpm;

                double bestOffset = -1;
                double bestOffsetDeviance = 1000;

                for (int i = 0; i < taps.Count - 1; i++)
                {
                    double thisOffset = taps[i] - bestBeatLength * i;
                    double thisDevianceTotal = 0;
                    double maxDeviance = 0;

                    int usedCount = 0;
                    for (int j = 1; j < taps.Count - 1; j++)
                    {
                        if (i == j) continue;
                        usedCount++;
                        thisDevianceTotal += (thisOffset + bestBeatLength * j) - taps[j];
                        maxDeviance = Math.Max(maxDeviance, Math.Abs((thisOffset + bestBeatLength * j) - taps[j]));
                    }

                    double thisOffsetDeviance = Math.Abs(thisDevianceTotal) / usedCount;

                    //Remove offsets that have deviance of a certain amount or more.
                    if (thisOffsetDeviance > 200)
                    {
                        //int removed = taps[i];
                        //taps.RemoveAt(i);

                        if (i == 0)
                        {
                            taps.RemoveAt(i);
                            for (int j = 0; j < taps.Count; j++)
                                taps[j] -= (int)Math.Round(averageBeatLength);

                            removeCount++;
                            goto restart;
                        }

                        i--; //Removing from the previous to current section.

                        int removeLength = taps[i + 1] - taps[i];
                        taps.RemoveAt(i + 1);
                        
                        if (i < taps.Count - 1)
                        {
                            removeLength = taps[i + 1] - taps[i];
                            taps.RemoveAt(i + 1);
                        }

                        for (int j = i + 1; j < taps.Count; j++)
                            taps[j] -= removeLength;

                        removeCount++;

                        goto restart;
                    }

                    if (maxDeviance < bestOffsetDeviance)
                    {
                        bestOffsetDeviance = maxDeviance;
                        bestOffset = thisOffset;
                    }
                }

#if DEBUG
                Debug.Print(@"---");
                Debug.Print(@"Sample Count         : " + (taps.Count + removeCount));
                Debug.Print(@"Revised Count        : " + taps.Count);
                
                Debug.Print(@"Average BeatLngth    : " + averageBeatLength);

                Debug.Print(@"Average Deviance     : " + averageDeviance);

                Debug.Print(@"Adjusted Range       : " + averageBeatLengthLow + @" ~ " + averageBeatLengthHigh);
#endif
                
                double lastHitOffset = taps[taps.Count - 1] - (taps.Count - 1) * bestBeatLength;

                AudioEngine.beatLength = 60000/bestBpm;
                if (AudioEngine.CurrentOffset == 0)
                    AudioEngine.CurrentOffset = bestOffset;
                else
                    AudioEngine.CurrentOffset = AudioEngine.CurrentOffset - (AudioEngine.CurrentOffset - lastHitOffset) / 2;

                editor.Dirty = true;


                if (AudioEngine.Time != taps[taps.Count - 1])
                    AudioEngine.SeekTo(taps[taps.Count - 1]);

#if DEBUG
                Debug.Print(@"Best BPM             : " + bestBpm);

                Debug.Print(@"First Offset         : " + taps[0]);
                Debug.Print(@"Best Offset          : " + bestOffset);
                Debug.Print(@"Best Offset Deviance : " + bestOffsetDeviance);

                

                Debug.Print(@"Last Hit Offset      : " + lastHitOffset);
#endif

                averageBeatLength -= averageDeviance;
            }

            lastBeat = AudioEngine.Time;

            if (double.IsInfinity(AudioEngine.beatLength))
                TimingReset();
        }
    }
}
