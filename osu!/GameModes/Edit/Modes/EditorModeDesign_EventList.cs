using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameplayElements.Events;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input.Handlers;
using osu.Helpers;

namespace osu.GameModes.Edit.Modes
{
    internal partial class EditorModeDesign
    {
        private pScrollableArea eventList = new pScrollableArea(new Rectangle(GameBase.WindowWidthScaled - 66, 80, 66, 290), Vector2.Zero, true) { AllowDragScrolling = false };

        private readonly Dictionary<Event, pText> eventListDictionary = new Dictionary<Event, pText>();
        private float eventDrawHeight;
        private pSprite eventListDragging;
        private Vector2 eventListDraggingStart;
        private bool eventListUpdateRequired = true;

        private void DrawEventList()
        {
            eventList.Draw();
        }

        private void UpdateEventList()
        {
            eventList.Update();

            if (eventListDragging != null)
            {
                eventListDragging.Position = eventListDragging.InitialPosition +
                                                    (MouseManager.MousePosition - eventListDraggingStart) /
                                                    GameBase.WindowRatio;

                foreach (pSprite p in eventList.SpriteManager.SpriteList.FindAll(t => t.TagNumeric > 0))
                {
                    pText pt = p as pText;
                    if (eventListDragging == p || (pt != null && pt.Text == @"Background"))
                        continue;

                    if (p.InitialPosition.Y > eventListDragging.Position.Y - eventDrawHeight / 2)
                    {
                        if (p.Position == p.InitialPosition)
                            p.MoveTo(p.InitialPosition + new Vector2(0, eventDrawHeight), 80);
                    }
                    else if (p.InitialPosition.Y < eventListDragging.Position.Y)
                    {
                        if (p.Position != p.InitialPosition)
                            p.MoveTo(p.InitialPosition, 80);
                    }
                }
            }


            if (eventListUpdateRequired)
            {
                eventList.ClearSprites();
                eventListDictionary.Clear();

                float y = 0;

                float drawDepth = 0.5f;

                float scale = DpiHelper.DPI(GameBase.Form) / 96.0f;
                for (int i = 0; i < eventManager.storyLayerSprites.Length; i++)
                {
                    bool layerIsEnabled = eventManager.storyLayerEnabled[i];

                    List<Event> l = eventManager.storyLayerSprites[i];

                    StoryLayer layer = (StoryLayer)i;

                    pText pt = new pText(layer.ToString(), 8 * scale, new Vector2(0, y), drawDepth, true, layerIsEnabled ? Color.Orange : Color.LightGray);
                    pt.TextRenderSpecific = false;
                    pt.TextAa = false;
                    pt.TextShadow = true;
                    pt.TagNumeric = 1;
                    pt.HandleInput = true;
                    pt.OnHover += delegate
                                      {
                                          if (selectedEvent != null)
                                          {
                                              pt.TextBold = true;
                                              pt.TextChanged = true;
                                          }
                                      };
                    pt.OnHoverLost += delegate
                                          {
                                              pt.TextBold = false;
                                              pt.TextChanged = true;
                                          };
                    pt.OnClick += delegate
                                      {
                                          if (selectedEvent != null)
                                          {
                                              moveEventToLayer(selectedEvent, layer);
                                          }
                                          else
                                          {
                                              ToggleLayer(layer);
                                          }
                                      };
                    eventList.SpriteManager.Add(pt);
                    if (eventDrawHeight == 0)
                        eventDrawHeight = (pt.MeasureText().Y + 4 * scale) / GameBase.WindowRatio;
                    y += eventDrawHeight;

                    drawDepth += 0.0001f;

                    if (!layerIsEnabled)
                        continue; //don't display the events if this layer is hidden.

                    foreach (Event e in l)
                    {
                        if (e.Sprite == null) continue;

                        pText pt1 = new pText(e.Filename.Length >= 4 ? e.Filename.Remove(e.Filename.Length - 4) : e.Filename + @" ", 8 * scale, new Vector2(2, y), drawDepth,
                                              true, Color.White);
                        pt1.TextRenderSpecific = false;
                        pt1.TextAa = false;
                        pt1.HandleInput = true;
                        pt1.TextShadow = true;
                        pt1.Tag = e;
                        pt1.TagNumeric = 2;
                        pt1.OnHover += delegate
                                           {
                                               pt1.BackgroundColour = new Color(92, 92, 92);
                                               pt1.TextChanged = true;
                                           };
                        pt1.OnHoverLost += delegate
                                               {
                                                   pt1.BackgroundColour = Color.TransparentWhite;
                                                   pt1.TextChanged = true;
                                               };
                        if (e == selectedEvent)
                        {
                            pt1.TextBold = true;
                            pt1.TextUnderline = true;
                        }
                        Event e1 = e;
                        pt1.OnClick += delegate
                                           {
                                               if (editor.CurrentMode != EditorModes.Design)
                                                   return;

                                               if ((!e1.Sprite.IsVisible || MouseManager.DoubleClick) &&
                                                   e1.Sprite.Transformations.Count > 0)
                                                   AudioEngine.SeekTo(e1.Sprite.Transformations[0].Time1);
                                               SetSelectedEvent(e1, false);
                                           };

                        eventList.SpriteManager.Add(pt1);
                        y += (pt1.MeasureText().Y + 4 * scale) / GameBase.WindowRatio;

                        eventListDictionary.Add(e, pt1);

                        drawDepth += 0.0001f;
                    }
                }

                eventList.SetContentDimensions(new Vector2(66, y));
                eventListUpdateRequired = false;
            }
        }
    }
}