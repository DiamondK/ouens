﻿using System;
using Microsoft.Xna.Framework.Input;
using osu.GameplayElements;
using osu.Graphics.Sprites;

namespace osu.GameModes.Edit.Modes
{
    internal abstract class EditorMode
    {
        internal bool Active;
        private bool enabled = true;
        internal virtual bool Enabled
        {
            get { return enabled; }
            set 
            {
                enabled = value;
                spriteManager.HandleInput = value;
            }
        }
        internal Editor editor;
        internal HitObjectManager hitObjectManager;
        internal SpriteManager spriteManager;

        internal EditorMode(Editor editor)
        {
            this.editor = editor;
            hitObjectManager = editor.hitObjectManager;
            spriteManager = new SpriteManager(true);
        }

        internal virtual void Initialize()
        {
            InitializeSprites();
            InitializeSettings();

            spriteManager.HandleFirstDraw();
            //set to false here, because we don't know when the first draw is going to be
            //and don't want to delay animations that may be queued.
        }

        protected virtual void InitializeSprites()
        {
        }

        /// <summary>
        /// Standard graphical draw operations specific to this mode.
        /// </summary>
        internal virtual void Draw()
        {
            spriteManager.Draw();
        }

        /// <summary>
        /// Standard update operations called as many times per second as possible.
        /// </summary>
        internal abstract void Update();

        /// <summary>
        /// Timeline-specific update operations.
        /// </summary>
        internal abstract void UpdateTimeline();

        /// <summary>
        /// Timeline-specific draw operations.
        /// </summary>
        internal virtual void DrawTimeline()
        {
        }

        internal virtual bool OnClick()
        {
            return false;
        }

        internal virtual bool OnClickUp()
        {
            return false;
        }

        /// <returns>true if handled</returns>
        internal virtual bool OnDragStart()
        {
            return false;
        }

        internal virtual void OnDragEnd()
        {
        }

        internal virtual void Reset()
        {
        }

        internal virtual bool OnKey(Keys keys, bool first)
        {
            return false;
        }

        /// <summary>
        /// This mode has got focus, and become the active mode.
        /// </summary>
        internal virtual void Enter()
        {
            Active = true;
            spriteManager.Alpha = 1;
        }

        /// <summary>
        /// This mode has lost focus.  Tidy up anything left over.
        /// </summary>
        internal virtual void Leave()
        {
            Active = false;
            spriteManager.Alpha = 0;
        }

        internal virtual void Dispose()
        {
            spriteManager.Dispose();
        }

        internal virtual void DrawTimelineLow()
        {
        }

        internal virtual bool IgnoreInterfaceClick
        {
            get { return false; }
        }

        internal virtual bool ExitEditor()
        {
            return true;
        }

        internal virtual void SelectAll()
        {

        }

        internal virtual void SelectNone()
        {

        }

        internal virtual void CopySelection()
        {

        }

        internal virtual void PasteSelection()
        {
        }

        internal virtual bool OnDoubleClick()
        {
            return false;
        }

        protected abstract void InitializeSettings();

        /// <summary>
        /// Called when [mouse wheel down].
        /// </summary>
        /// <returns>True if the event has been handled.</returns>
        internal virtual bool OnMouseWheelDown()
        {
            return false;
        }

        /// <summary>
        /// Called when [mouse wheel up].
        /// </summary>
        /// <returns>True if the event has been handled.</returns>
        internal virtual bool OnMouseWheelUp()
        {
            return false;
        }
    }
}