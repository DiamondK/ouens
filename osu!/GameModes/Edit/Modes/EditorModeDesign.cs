﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Edit.Forms;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.Graphics.Primitives;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Helpers;
using osu.Input.Handlers;
using System.IO;
using osu.Graphics.Notifications;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using osu.Online;
using osu.Input;
using osu_common.Helpers;
using osu.Online.Social;

namespace osu.GameModes.Edit.Modes
{
    internal partial class EditorModeDesign : EditorMode
    {
        private pText positionText;
        private pText storyBoardText;
        private pText spriteInfo;
        private SpriteManager spriteManagerLeftHidden;
        private pCheckbox checkDiffSpecific;
        private pCheckbox checkHitObjects;
        private pCheckbox checkLayerBg;
        private pCheckbox checkLayerFailing;
        private pCheckbox checkLayerFg;
        private pCheckbox checkLayerPassing;
        private pCheckbox checkOriginCentre;
        private pCheckbox checkOriginTopLeft;
        private pCheckbox checkTransFade;
        private pCheckbox checkTransFlipHorizontal;
        private pCheckbox checkTransFlipVertical;
        private pCheckbox checkTransMove;
        private pCheckbox checkTransRotate;
        private pCheckbox checkTransScale;
        private pCheckbox checkTransVectorScale;
        private pCheckbox checkTransColour;
        private pCheckbox checkTweenEaseIn;
        private pCheckbox checkTweenEaseOut;
        private pCheckbox checkTweenEnable;
        private bool currentDiffSpecific;
        private bool isDragTimeline;
        private EasingTypes currentEasing;
        private Origins currentOrigin;
        private TransformationType currentTransformType;
        private TransformationType dragTransformType;
        private Vector2 dragOffset;
        private Vector2 dragStart;
        private float dragStartFloat;
        private float dragStartFloat2;
        private EventManager eventManager;
        private Event hoveredEvent;
        private List<Event> otherHoveredEvents = new List<Event>();
        private bool isDragging;
        private Vector2 mousePosition;
        internal Event selectedEvent;
        private Event copiedEvent = null;
        private List<Transformation> selectedEventTransformations;
        internal int selectedIndex;
        private int dragTime;//the time on timeline where drag happend.
        public bool ShowHitObjects;
        private List<Event> visibleEvents;
        private int timelineStart = 0;
        private pText timelineStr;

        const int event_minimal_selection_size = 4;

        internal override bool Enabled
        {
            get { return base.Enabled; }
            set
            {
                base.Enabled = value;
                spriteManagerLeftHidden.HandleInput = value;
            }
        }

        public EditorModeDesign(Editor editor)
            : base(editor)
        {

        }

        protected override void InitializeSettings()
        {
            Reset();
        }

        pText videoOffsetLabel;

        private void insertBackground_OnClick(object sender, EventArgs e)
        {
            InsertBackground();
        }

        internal override void PasteSelection()
        {
            if (copiedEvent != null)
            {
                Event evt = copiedEvent.Clone();
                editor.UndoPush();
                if (evt.Sprite != null && evt.Sprite.Transformations.Count > 0)
                {
                    int delta = AudioEngine.Time - evt.Sprite.Transformations[0].Time1;
                    evt.Sprite.TimeWarp(delta);
                }
                eventManager.Add(evt);
            }
        }

        internal override void CopySelection()
        {
            if (selectedEvent != null)
                copiedEvent = selectedEvent;
            try
            {
                System.Windows.Forms.Clipboard.SetText(AudioEngine.TimeRaw.ToString());
            }
            catch (Exception)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.General_Editor_ClipboardNotAvailable));
            }
        }

        internal void InsertBackground(string filename = null)
        {
            if (filename == null)
            {
                OpenFileDialog d = new OpenFileDialog();
                d.InitialDirectory = Path.GetFullPath(BeatmapManager.Current.ContainingFolder);
                d.RestoreDirectory = true;

                if (DialogResult.Cancel != d.ShowDialog(GameBase.Form))
                    InsertBackground(d.FileName);
                return;
            }

            try
            {
                string destination = BeatmapManager.Current.ContainingFolder + @"\" + Path.GetFileName(filename);

                if (
                    filename.IndexOf(BeatmapManager.Current.ContainingFolder,
                                       StringComparison.CurrentCultureIgnoreCase) < 0)
                {
                    try
                    {
                        File.Delete(destination);
                        File.Copy(filename, destination);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(LocalisationManager.GetString(OsuString.EditorModeDesign_ErrorCopyingBackgroundFile) + e.Message);
                    }
                }

                destination = ImageHelper.ResizeIfLarger(destination, 1366, 768);

                Event ev = eventManager.Add(Path.GetFileName(destination), 0);

                editor.Dirty = true;

                for (int i = 0; i < eventManager.events.Count; i++)
                {
                    if (eventManager.events[i] != ev && eventManager.events[i].Type == ev.Type)
                    {
                        eventManager.Remove(eventManager.events[i]);
                        i--;
                    }
                }

                if (ev != null && ev.Type == EventTypes.Video)
                {
                    video = ev;
                    videoOffset.SetValue(ev.StartTime);
                }
                else if (ev != null && ev.Type == EventTypes.Background)
                {
                    BeatmapManager.Current.BackgroundImage = Path.GetFileName(destination);
                }

                editor.Dirty = true;
                eventManager.PostProcessing();
                eventManager.UpdateBackground();
            }
            catch (Exception)
            {
                MessageBox.Show(
                    LocalisationManager.GetString(OsuString.EditorModeDesign_ErrorDuringImportProcess));
            }
        }


        protected override void InitializeSprites()
        {
            spriteManagerLeftHidden = new SpriteManager(true);
            spriteManagerLeftHidden.Alpha = 0;

            positionText = new pText(string.Empty, 10, new Vector2(GameBase.WindowWidthScaled - 200, 28), Vector2.Zero, 1, true, Color.White, true);
            positionText.TextRenderSpecific = false;
            positionText.TextAa = false;
            positionText.TextShadow = true;

            storyBoardText = new pText(string.Empty, 10, new Vector2(GameBase.WindowWidthScaled - 200, 56), Vector2.Zero, 1, true, Color.White, true);
            storyBoardText.TextRenderSpecific = false;
            storyBoardText.TextAa = false;
            storyBoardText.TextShadow = true;

            spriteManager.Add(positionText);
            spriteManager.Add(storyBoardText);

            InitializeLeftOptions1();

            InitializeLeftOptions2();

            pButton pb = new pButton(LocalisationManager.GetString(OsuString.Editor_Design_BackgroundImageVideo), new Vector2(GameBase.WindowWidthScaled - 180, 6), new Vector2(170, 22.5f), 1, new Color(112, 152, 252), insertBackground_OnClick);
            spriteManager.Add(pb.SpriteCollection);

            videoOffset = new pSliderBar(spriteManager, -3000, 3000, video != null ? video.StartTime : 0, new Vector2(GameBase.WindowWidthScaled - 130, 52), 125);

            videoOffsetLabel = new pText(LocalisationManager.GetString(OsuString.EditorModeDesign_VideoOffset), 12, new Vector2(videoOffset.length / 2f + videoOffset.position.X, 32), 1, true, Color.White);
            videoOffsetLabel.Tag = EditorModes.Design;
            videoOffsetLabel.Origin = Origins.TopCentre;

            spriteManager.Add(videoOffsetLabel);

            videoOffset.Visible = false;
            videoOffset.KeyboardControl = true;
            videoOffsetLabel.Alpha = 0;

            const int KEYFRAME_HEIGHT = 160;

            pText pt = new pText(LocalisationManager.GetString(OsuString.EditorModeDesign_KeyframeControl), 10, new Vector2(KEYFRAME_HEIGHT, 64), 1, true, Color.White);
            pt.TextBold = true;
            pt.TextShadow = true;
            spriteManager.Add(pt);

            pSprite p =
                new pSprite(SkinManager.Load(@"editor-timeline-plus", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(KEYFRAME_HEIGHT + 90, 70), 1, true,
                            Color.White);
            p.Scale = 0.8f;
            p.HandleInput = true;
            p.ClickEffect = new Transformation(TransformationType.Scale, 0.9F, 0.8f, 0, 200);
            p.OnClick += delegate
            {
                editor.UndoPush();
                if (currentTransformType != TransformationType.Colour)
                {
                    Transformation t;
                    AddKeyframe(AudioEngine.Time, out t, true);
                }
                else
                {
                    ColorDialog cd = new ColorDialog();
                    if (cd.ShowDialog() == DialogResult.OK)
                        AddColorFrame(new Color(cd.Color.R, cd.Color.G, cd.Color.B));
                }
            };
            spriteManager.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-timeline-minus", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(KEYFRAME_HEIGHT + 105, 70), 1, true,
                            Color.White);
            p.Scale = 0.8f;
            p.HandleInput = true;
            p.ClickEffect = new Transformation(TransformationType.Scale, 0.9F, 0.8f, 0, 200);
            p.OnClick += delegate { RemoveKeyframe(AudioEngine.Time); };
            spriteManager.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-timeline-left", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(KEYFRAME_HEIGHT + 120, 70), 1, true,
                            Color.White);
            p.Scale = 0.8f;
            p.HandleInput = true;
            p.ClickEffect = new Transformation(TransformationType.Scale, 0.9F, 0.8f, 0, 200);
            p.OnClick += delegate { PrevKeyframe(); };
            spriteManager.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-timeline-right", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(KEYFRAME_HEIGHT + 135, 70), 1, true,
                            Color.White);
            p.Scale = 0.8f;
            p.HandleInput = true;
            p.ClickEffect = new Transformation(TransformationType.Scale, 0.9F, 0.8f, 0, 200);
            p.OnClick += delegate { NextKeyframe(); };
            spriteManager.Add(p);

            pb = new pButton("Sprite\nLibrary", new Vector2(GameBase.WindowWidthScaled - 70, 372), new Vector2(66, 40), 1, Color.YellowGreen,
                                     ShowSpriteLibrary);
            spriteManager.Add(pb.SpriteCollection);

            spriteInfo = new pText(string.Empty, 8, new Vector2(-1, -1), Vector2.Zero, 1, true, Color.White, true);
            spriteInfo.TextRenderSpecific = false;
            spriteInfo.TextAa = false;
            spriteInfo.Field = Fields.Native;
            spriteManager.Add(spriteInfo);

            timelineStr = new pText("Movement\nScale\nFade\nRotation", 8.4f, new Vector2(editor.TimelineX, editor.TimelineY), 1,
                           true, Color.White);
            timelineStr.TextBold = true;
            timelineStr.TextShadow = true;
            spriteManager.Add(timelineStr);

            p =
                new pSprite(SkinManager.Load(@"editor-button-play", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre, Clocks.Game, new Vector2(editor.TimelineX + 5, 70), 1, true, Color.White);
            p.Rotation = OsuMathHelper.PiOver2;
            p.Scale = 0.7f;
            p.HandleInput = true;
            p.ClickEffect = new Transformation(TransformationType.Scale, 0.8F, 0.7f, 0, 200);
            p.OnClick += delegate { ChangeTimeline(false); };
            spriteManager.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-button-play", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre, Clocks.Game, new Vector2(editor.TimelineX + 20, 70), 1, true, Color.White);
            p.Rotation = -OsuMathHelper.PiOver2;
            p.Scale = 0.7f;
            p.HandleInput = true;
            p.ClickEffect = new Transformation(TransformationType.Scale, 0.8F, 0.7f, 0, 200);
            p.OnClick += delegate { ChangeTimeline(true); };
            spriteManager.Add(p);

            base.InitializeSprites();
        }

        private void ChangeTimeline(bool up)
        {
            if (timelineStart == 0 && !up)
            {
                timelineStart = 1;
                timelineStr.Text = "Scale\nFade\nRotation\nColour";
            }
            else if (timelineStart == 1 && up)
            {
                timelineStart = 0;
                timelineStr.Text = "Movement\nScale\nFade\nRotation";
            }

        }

        private void changeColour_OnClick(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            cd.Color = OsuMathHelper.CConvert(EventManager.backgroundColour);

            if (DialogResult.OK == cd.ShowDialog(GameBase.Form))
                hitObjectManager.eventManager.ChangeColour(new Color(cd.Color.R, cd.Color.G, cd.Color.B));
        }

        private void InitializeLeftOptions1()
        {
            int vLeft = 80;
            int hLeft = 0;

            pText pt = new pText(LocalisationManager.GetString(OsuString.EditorModeDesign_LayerToggles), 10, new Vector2(hLeft, vLeft), 1, true, Color.White);
            pt.TextBold = true;
            pt.TextShadow = true;
            spriteManager.Add(pt);

            vLeft += 15;

            checkLayerBg = new pCheckbox(LocalisationManager.GetString(OsuString.General_Background), 0.7f, new Vector2(hLeft + 2, vLeft), 1, true);
            spriteManager.Add(checkLayerBg.SpriteCollection);

            checkLayerBg.OnCheckChanged += delegate { ToggleLayer(StoryLayer.Background); };

            vLeft += 15;

            checkLayerFailing = new pCheckbox(LocalisationManager.GetString(OsuString.Editor_Design_Failing), 0.7f, new Vector2(hLeft + 2, vLeft), 1, true);
            spriteManager.Add(checkLayerFailing.SpriteCollection);

            checkLayerFailing.OnCheckChanged += delegate { ToggleLayer(StoryLayer.Fail); };

            vLeft += 15;

            checkLayerPassing = new pCheckbox(LocalisationManager.GetString(OsuString.Editor_Design_Passing), 0.7f, new Vector2(hLeft + 2, vLeft), 1, true);
            spriteManager.Add(checkLayerPassing.SpriteCollection);

            checkLayerPassing.OnCheckChanged += delegate { ToggleLayer(StoryLayer.Pass); };

            vLeft += 15;

            checkLayerFg = new pCheckbox(LocalisationManager.GetString(OsuString.General_Foreground), 0.7f, new Vector2(hLeft + 2, vLeft), 1, true);
            spriteManager.Add(checkLayerFg.SpriteCollection);

            checkLayerFg.OnCheckChanged += delegate { ToggleLayer(StoryLayer.Foreground); };

            vLeft += 15;

            checkHitObjects = new pCheckbox(LocalisationManager.GetString(OsuString.Editor_Design_HitObjects), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
            spriteManager.Add(checkHitObjects.SpriteCollection);

            checkHitObjects.OnCheckChanged += delegate(object sender, bool status) { ToggleHitObjects(status); };

            vLeft += 30;

            pt = new pText(LocalisationManager.GetString(OsuString.Editor_Design_Transformation), 10, new Vector2(hLeft, vLeft), 1, true, Color.White);
            pt.TextBold = true;
            pt.TextShadow = true;
            spriteManager.Add(pt);

            currentTransformType = TransformationType.Movement;

            vLeft += 15;

            checkTransMove = new pCheckbox(LocalisationManager.GetString(OsuString.General_Move), 0.7f, new Vector2(hLeft + 2, vLeft), 1, true);
            spriteManager.Add(checkTransMove.SpriteCollection);

            checkTransMove.OnCheckChanged += delegate { SetTransformType(TransformationType.Movement); };

            vLeft += 15;

            checkTransScale = new pCheckbox(LocalisationManager.GetString(OsuString.General_Scale), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
            spriteManager.Add(checkTransScale.SpriteCollection);

            checkTransScale.OnCheckChanged += delegate { SetTransformType(TransformationType.Scale); };

            vLeft += 15;

            checkTransFade = new pCheckbox(LocalisationManager.GetString(OsuString.General_Fade), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
            spriteManager.Add(checkTransFade.SpriteCollection);

            checkTransFade.OnCheckChanged += delegate { SetTransformType(TransformationType.Fade); };

            vLeft += 15;

            checkTransRotate = new pCheckbox(LocalisationManager.GetString(OsuString.General_Rotate), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
            spriteManager.Add(checkTransRotate.SpriteCollection);

            checkTransRotate.OnCheckChanged += delegate { SetTransformType(TransformationType.Rotation); };

            vLeft += 15;
            checkTransColour = new pCheckbox(LocalisationManager.GetString(OsuString.General_Colour), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
            spriteManager.Add(checkTransColour.SpriteCollection);

            checkTransColour.OnCheckChanged += delegate { SetTransformType(TransformationType.Colour); };
            vLeft += 15;

            pt = new pText(LocalisationManager.GetString(OsuString.Editor_Design_Tweening), 10, new Vector2(hLeft, vLeft), 1, true, Color.White);
            pt.TextBold = true;
            pt.TextShadow = true;
            spriteManager.Add(pt);

            vLeft += 15;

            checkTweenEnable = new pCheckbox(LocalisationManager.GetString(OsuString.General_Enabled), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
            spriteManager.Add(checkTweenEnable.SpriteCollection);

            checkTweenEnable.OnCheckChanged += delegate(object sender, bool status) { SetTweening(status); };

            vLeft += 15;

            pt = new pText(LocalisationManager.GetString(OsuString.Editor_Design_Easing), 10, new Vector2(hLeft, vLeft), 1, true, Color.White);
            pt.TextBold = true;
            pt.TextShadow = true;
            spriteManager.Add(pt);

            vLeft += 15;

            checkTweenEaseIn = new pCheckbox(LocalisationManager.GetString(OsuString.General_In), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
            spriteManager.Add(checkTweenEaseIn.SpriteCollection);

            checkTweenEaseIn.OnCheckChanged +=
                delegate(object sender, bool status) { SetEasing(status ? EasingTypes.In : EasingTypes.None); };

            vLeft += 15;

            checkTweenEaseOut = new pCheckbox(LocalisationManager.GetString(OsuString.General_Out), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
            spriteManager.Add(checkTweenEaseOut.SpriteCollection);

            checkTweenEaseOut.OnCheckChanged +=
                delegate(object sender, bool status) { SetEasing(status ? EasingTypes.Out : EasingTypes.None); };

            vLeft += 30;

            pt = new pText(LocalisationManager.GetString(OsuString.General_Origin), 10, new Vector2(hLeft, vLeft), 1, true, Color.White);
            pt.TextBold = true;
            pt.TextShadow = true;
            spriteManager.Add(pt);

            vLeft += 15;

            checkOriginTopLeft = new pCheckbox(LocalisationManager.GetString(OsuString.General_TopLeft), 0.7f, new Vector2(hLeft + 2, vLeft), 1, true);
            checkOriginTopLeft.OnCheckChanged += delegate { SetOrigin(Origins.TopLeft); };
            spriteManager.Add(checkOriginTopLeft.SpriteCollection);


            vLeft += 15;

            checkOriginCentre = new pCheckbox(LocalisationManager.GetString(OsuString.General_Centre), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
            checkOriginCentre.OnCheckChanged += delegate { SetOrigin(Origins.Centre); };
            spriteManager.Add(checkOriginCentre.SpriteCollection);

            vLeft += 30;

            pt = new pText(LocalisationManager.GetString(OsuString.General_Advanced), 10, new Vector2(hLeft, vLeft), 1, true, Color.White);
            pt.TextBold = true;
            pt.TextShadow = true;
            spriteManager.Add(pt);

            vLeft += 15;

            checkDiffSpecific = new pCheckbox(LocalisationManager.GetString(OsuString.Editor_Design_DiffSpecific), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
            spriteManager.Add(checkDiffSpecific.SpriteCollection);

            checkDiffSpecific.OnCheckChanged += delegate(object sender, bool status) { SetDiffSpecific(status); };
        }

        private void InitializeLeftOptions2()
        {
            int vLeft = 80;
            int hLeft = 70;

            pSprite ps = new pSprite(GameBase.WhitePixel, new Vector2(0, 66), 0, true, new Color(0, 0, 0, 130));
            ps.Scale = 1.6f;
            ps.VectorScale = new Vector2(160, 396);
            spriteManagerLeftHidden.Add(ps);

            vLeft += 15;

            /*
                        checkLayerBg = new pCheckbox(spriteManagerLeftHidden, LocalisationManager.GetString(OsuString.EditorModeDesign_Background), 0.7f, new Vector2(hLeft + 2, vLeft), 1, true);
                        checkLayerBg.OnCheckChanged += delegate { ToggleLayer(StoryLayer.Background); };
            */

            vLeft += 15;

            /*
                        checkLayerFailing = new pCheckbox(spriteManagerLeftHidden, LocalisationManager.GetString(OsuString.EditorModeDesign_Failing), 0.7f, new Vector2(hLeft + 2, vLeft), 1, true);
                        checkLayerFailing.OnCheckChanged += delegate { ToggleLayer(StoryLayer.Fail); };
            */

            vLeft += 15;

            /*
                        checkLayerPassing = new pCheckbox(spriteManagerLeftHidden, LocalisationManager.GetString(OsuString.EditorModeDesign_Passing), 0.7f, new Vector2(hLeft + 2, vLeft), 1, true);
                        checkLayerPassing.OnCheckChanged += delegate { ToggleLayer(StoryLayer.Pass); };
            */

            vLeft += 15;

            /*
                        checkLayerFg = new pCheckbox(spriteManagerLeftHidden, LocalisationManager.GetString(OsuString.EditorModeDesign_Foreground), 0.7f, new Vector2(hLeft + 2, vLeft), 1, true);
                        checkLayerFg.OnCheckChanged += delegate { ToggleLayer(StoryLayer.Foreground); };
            */

            vLeft += 15;

            /*
                        checkHitObjects = new pCheckbox(spriteManagerLeftHidden, LocalisationManager.GetString(OsuString.EditorModeDesign_HitObjects), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
                        checkHitObjects.OnCheckChanged += delegate(object sender, bool status) { ToggleHitObjects(status); };
            */

            vLeft += 30;


            vLeft += 15;


            //checkTransMove = new pCheckbox(spriteManagerLeftHidden, LocalisationManager.GetString(OsuString.EditorModeDesign_Move), 0.7f, new Vector2(hLeft + 2, vLeft), 1, true);
            //checkTransMove.OnCheckChanged += delegate(object sender, bool status) { SetTransformType(TransformationType.Movement); };


            vLeft += 15;

            checkTransVectorScale = new pCheckbox(LocalisationManager.GetString(OsuString.Editor_Design_VectorScale), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
            spriteManagerLeftHidden.Add(checkTransVectorScale.SpriteCollection);

            checkTransVectorScale.OnCheckChanged += delegate { SetTransformType(TransformationType.VectorScale); };


            vLeft += 15;

            checkTransFlipHorizontal = new pCheckbox(LocalisationManager.GetString(OsuString.Editor_Design_HorizontalFlip), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
            spriteManagerLeftHidden.Add(checkTransFlipHorizontal.SpriteCollection);

            checkTransFlipHorizontal.OnCheckChanged +=
                delegate(object sender, bool status) { SetParameter(TransformationType.ParameterFlipHorizontal, status); };

            /*
                        checkTransFade = new pCheckbox( LocalisationManager.GetString(OsuString.EditorModeDesign_Fade), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
spriteManagerLeftHidden.Add(heckTransFade.SpriteCollection);

                        checkTransFade.OnCheckChanged += delegate(object sender, bool status) { SetTransformType(TransformationType.Fade); };
            */

            vLeft += 15;

            checkTransFlipVertical = new pCheckbox(LocalisationManager.GetString(OsuString.Editor_Design_VerticalFlip), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
            spriteManagerLeftHidden.Add(checkTransFlipVertical.SpriteCollection);

            checkTransFlipVertical.OnCheckChanged +=
                delegate(object sender, bool status) { SetParameter(TransformationType.ParameterFlipVertical, status); };

            /*
                        checkTransRotate = new pCheckbox(spriteManagerLeftHidden, LocalisationManager.GetString(OsuString.EditorModeDesign_Rotate), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
                        checkTransRotate.OnCheckChanged += delegate(object sender, bool status) { SetTransformType(TransformationType.Rotation); };
            */

            vLeft += 30;


            vLeft += 15;

            /*
                        checkTweenEnable = new pCheckbox(spriteManagerLeftHidden, LocalisationManager.GetString(OsuString.EditorModeDesign_Enabled), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
                        checkTweenEnable.OnCheckChanged += delegate(object sender, bool status) { SetTweening(status); };
            */

            vLeft += 15;


            vLeft += 15;

            /*
                        checkTweenEaseIn = new pCheckbox(spriteManagerLeftHidden, LocalisationManager.GetString(OsuString.EditorModeDesign_In), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
                        checkTweenEaseIn.OnCheckChanged += delegate(object sender, bool status) { SetEasing(status ? EasingTypes.Out : EasingTypes.None); };
            */

            vLeft += 15;

            /*
                        checkTweenEaseOut = new pCheckbox(spriteManagerLeftHidden, LocalisationManager.GetString(OsuString.EditorModeDesign_Out), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
                        checkTweenEaseOut.OnCheckChanged += delegate(object sender, bool status) { SetEasing(status ? EasingTypes.In : EasingTypes.None); };
            */

            vLeft += 30;

            vLeft += 15;

            /*
                        checkOriginTopLeft = new pCheckbox(spriteManagerLeftHidden, LocalisationManager.GetString(OsuString.EditorModeDesign_TopLeft), 0.7f, new Vector2(hLeft + 2, vLeft), 1, true);
                        checkOriginTopLeft.OnCheckChanged += delegate(object sender, bool status) { SetOrigin(Origins.TopLeft); };
            */

            vLeft += 15;

            /*
                        checkOriginCentre = new pCheckbox(spriteManagerLeftHidden, LocalisationManager.GetString(OsuString.EditorModeDesign_Centre), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
                        checkOriginCentre.OnCheckChanged += delegate(object sender, bool status) { SetOrigin(Origins.Centre); };
            */

            vLeft += 30;


            vLeft += 15;

            /*
                        checkDiffSpecific = new pCheckbox(spriteManagerLeftHidden, LocalisationManager.GetString(OsuString.EditorModeDesign_DiffSpecific), 0.7f, new Vector2(hLeft + 2, vLeft), 1, false);
                        checkDiffSpecific.OnCheckChanged += delegate(object sender, bool status) { SetDiffSpecific(status); };
            */
        }

        private void ShowSpriteLibrary(object sender, EventArgs e)
        {
            ShowSpriteLibrary();
        }

        private void ShowSpriteLibrary()
        {
            SpriteImportDialog sid = new SpriteImportDialog();
            sid.Closed += delegate
            {
                if (sid.SelectedFilename != null)
                {
                    Event e;
                    if (sid.IsAnimation)
                    {
                        e = new EventAnimation(sid.SelectedFilename, new Vector2(320, 240),
                                               Origins.Centre,
                                               StoryLayer.Foreground, sid.AnimationFrameCount, sid.SelectedFrameDelay,
                                               false, LoopTypes.LoopForever);
                    }
                    else
                    {
                        e = new EventSprite(sid.SelectedFilename, new Vector2(320, 240),
                                            Origins.Centre,
                                            StoryLayer.Foreground, false);
                    }

                    eventManager.Add(e);

                    Transformation t;

                    SetSelectedEvent(e, true);

                    AddKeyframe(AudioEngine.Time, out t, true);

                    eventListUpdateRequired = true;
                }
            };

            GameBase.ShowDialog(sid);
        }

        private void SetDiffSpecific(bool status)
        {
            if (currentDiffSpecific == status) return;

            checkDiffSpecific.SetStatusQuietly(status);
            currentDiffSpecific = status;

            if (selectedEvent != null)
                selectedEvent.WriteToOsu = status;
        }

        private void SetOrigin(Origins origin)
        {
            if (currentOrigin == origin) return;

            currentOrigin = origin;
            checkOriginTopLeft.SetStatusQuietly(origin == Origins.TopLeft);
            checkOriginCentre.SetStatusQuietly(origin == Origins.Centre);

            if (selectedEvent != null)
            {
                editor.UndoPush();

                selectedEvent.Sprite.Origin = origin;
                selectedEvent.Sprite.UpdateTextureAlignment();
            }
        }

        private void SetParameter(TransformationType type, bool status)
        {
            if (selectedEvent != null)
            {
                TransformationType temp = currentTransformType;
                currentTransformType = type;

                selectedEventTransformations = selectedEvent.Sprite.Transformations.FindAll(t => t.Type == currentTransformType);

                Transformation tr;
                AddKeyframe(AudioEngine.Time, out tr, true);

                if (!status)
                    selectedEvent.Sprite.Transformations.Remove(tr);

                currentTransformType = temp;
            }
        }

        private void SetTransformType(TransformationType type)
        {
            if (currentTransformType == type) return;

            checkTransFade.SetStatusQuietly(type == TransformationType.Fade);
            checkTransScale.SetStatusQuietly(type == TransformationType.Scale);
            checkTransVectorScale.SetStatusQuietly(type == TransformationType.VectorScale);
            checkTransMove.SetStatusQuietly(type == TransformationType.Movement);
            checkTransRotate.SetStatusQuietly(type == TransformationType.Rotation);
            checkTransColour.SetStatusQuietly(type == TransformationType.Colour);
            currentTransformType = type;
        }

        internal void ToggleLayer(StoryLayer layer)
        {
            eventManager.ToggleLayer(layer);

            checkLayerBg.SetStatusQuietly(eventManager.storyLayerEnabled[(int)StoryLayer.Background]);
            checkLayerFg.SetStatusQuietly(eventManager.storyLayerEnabled[(int)StoryLayer.Foreground]);
            checkLayerFailing.SetStatusQuietly(eventManager.storyLayerEnabled[(int)StoryLayer.Fail]);
            checkLayerPassing.SetStatusQuietly(eventManager.storyLayerEnabled[(int)StoryLayer.Pass]);

            eventListUpdateRequired = true;
        }

        private Event video;
        private pSliderBar videoOffset;


        internal override void Update()
        {
            hitObjectManager.Update();

            mousePosition = MouseManager.MousePosition;

            float width = GameBase.WindowWidth * GameBase.GamefieldRatioWindowIndependent;
            float height = GameBase.WindowHeight * GameBase.GamefieldRatioWindowIndependent;
            Vector2 offset = new Vector2((GameBase.WindowWidth - width) / 2 + GameBase.WindowOffsetX,
                                         (GameBase.WindowHeight - height) / 4 * 3 + GameBase.WindowOffsetY);

            mousePosition -= offset;
            mousePosition = mousePosition / height * 480;

            UpdateLeftControls();

            if (GameBase.SixtyFramesPerSecondFrame)
            {
                float load = GameBase.PixelsToLoadRatio * (GameBase.TransitionManager.BGPixelsDrawn
                    + eventManager.spriteManagerBGWide.PixelsDrawn
                    + eventManager.spriteManagerBG.PixelsDrawn
                    + eventManager.spriteManagerFG.PixelsDrawn);

                storyBoardText.InitialColour = load > 5 ? Color.Red : Color.White;
                storyBoardText.TextBold = load > 5;

                positionText.Text = String.Format("x:{0:0} y:{1:0}\n{2:#,0}ms", mousePosition.X, mousePosition.Y, AudioEngine.Time);
                storyBoardText.Text = String.Format("SB Load:{0:0.00}x", load);
            }

            if (video != null)
            {
                videoOffset.Visible = true;
                videoOffsetLabel.Alpha = 1;

                if (videoOffset.Hovering)
                {
                    int offsetI = (int)videoOffset.current;
                    video.SetStartTime(offsetI);
                    videoOffset.SpriteCollection.ForEach(s =>
                    {
                        s.ToolTip = String.Format(LocalisationManager.GetString(OsuString.EditorModeDesign_VideoOffsetTooltip), video.StartTime);
                    });
                }
            }
            else
            {
                videoOffset.Visible = false;
                videoOffsetLabel.Alpha = 0;
            }

            if (selectedEvent != null)
            {
                selectedEventTransformations =
                    selectedEvent.Sprite.Transformations.FindAll(t => t.Type == currentTransformType);

                Transformation tween =
                    selectedEventTransformations.Find(t => t.Time1 <= AudioEngine.Time && t.Time2 >= AudioEngine.Time);

                currentDiffSpecific = selectedEvent.WriteToOsu;
                checkDiffSpecific.SetStatusQuietly(currentDiffSpecific);

                currentOrigin = selectedEvent.Sprite.Origin;
                checkOriginTopLeft.SetStatusQuietly(currentOrigin == Origins.TopLeft);
                checkOriginCentre.SetStatusQuietly(currentOrigin == Origins.Centre);

                checkTweenEnable.SetStatusQuietly(tween != null);

                currentEasing = tween == null ? EasingTypes.None : tween.Easing;
                checkTweenEaseIn.SetStatusQuietly(currentEasing == EasingTypes.In);
                checkTweenEaseOut.SetStatusQuietly(currentEasing == EasingTypes.Out);

                checkTransFlipHorizontal.SetStatusQuietly(
                    selectedEvent.Sprite.Transformations.FindAll(t => t.Time1 <= AudioEngine.Time && t.Time2 >= AudioEngine.Time && t.Type == TransformationType.ParameterFlipHorizontal).Count > 0);
                checkTransFlipVertical.SetStatusQuietly(
                    selectedEvent.Sprite.Transformations.FindAll(t => t.Time1 <= AudioEngine.Time && t.Time2 >= AudioEngine.Time && t.Type == TransformationType.ParameterFlipVertical).Count > 0);

                // Place the text inside for unrotated sprites or place it at the rotation center
                spriteInfo.Position = selectedEvent.Sprite.Rotation == 0 ?
                    new Vector2(selectedEvent.Sprite.drawRectangle.Left, selectedEvent.Sprite.drawRectangle.Top)
                    : new Vector2(selectedEvent.Sprite.drawPosition.X, selectedEvent.Sprite.drawPosition.Y);
                spriteInfo.Text = selectedEvent.Filename + @" (" + selectedEvent.Layer + (selectedEvent.LineNumber > 0 ? @" - line " + selectedEvent.LineNumber : string.Empty) + ")\n";
                spriteInfo.Alpha = 1;
            }
            else
            {
                spriteInfo.Alpha = 0;

                checkTweenEnable.SetStatusQuietly(false);
                checkTweenEaseIn.SetStatusQuietly(false);
                checkTweenEaseOut.SetStatusQuietly(false);
            }

            if (isDragging)
            {
                if (KeyboardHandler.ShiftPressed)
                {
                    if (Math.Abs(dragStart.X - mousePosition.X) > Math.Abs(dragStart.Y - mousePosition.Y))
                        mousePosition.Y = dragStart.Y;
                    else
                        mousePosition.X = dragStart.X;
                }

                pSprite p = selectedEvent.Sprite;

                Transformation found = null;
                if (currentTransformType != TransformationType.Colour)
                    AddKeyframe(AudioEngine.Time, out found, true);
            }
            else
            {
                hoveredEvent = null;
                otherHoveredEvents.Clear();

                if (!editor.InSeekbar && spriteManagerLeftHidden.Alpha < 1)
                {
                    visibleEvents = eventManager.visibleEvents;

                    if (visibleEvents.Count > 0)
                    {
                        bool showOtherHoveredEvents = KeyboardHandler.ShiftPressed;

                        int count = visibleEvents.Count;
                        int start = (selectedIndex - 1 + count) % count;

                        bool first = true;

                        for (int i = start; i != start || first; i = (i - 1 + count) % count)
                        {
                            first = false;

                            Event e = visibleEvents[i];

                            if (e.Sprite.IsVisible && checkEventContainsMouse(e))
                            {
                                if (hoveredEvent == null)
                                {
                                    hoveredEvent = e;
                                    if (!showOtherHoveredEvents)
                                        break;
                                }
                                else if (showOtherHoveredEvents)
                                {
                                    otherHoveredEvents.Add(e);
                                }
                            }
                        }
                    }
                }
            }
            if (isDragTimeline && dragTransformType != TransformationType.None)
            {
                int dragNewPos = editor.TimelinePosToTime(InputManager.CursorPoint.X);
                if (!KeyboardHandler.ShiftPressed)
                {
                    dragNewPos = editor.Timing.BeatSnapValue((double)(dragNewPos - dragTime));
                }
                int delta = dragNewPos - selectedEvent.Sprite.Transformations.Find(x => x.Type == dragTransformType).Time1;
                bool canDrag = true;
                for (int k = 0; k < selectedEvent.Sprite.Transformations.Count; k++)
                {
                    Transformation trans = selectedEvent.Sprite.Transformations[k];
                    if (trans.Type != dragTransformType)
                        continue;
                    if (((trans.Time1 + delta) < 0) || ((trans.Time1 + delta) >= AudioEngine.AudioLength))
                    {
                        canDrag = false;
                        break;
                    }
                }
                if (canDrag)
                {
                    for (int k = 0; k < selectedEvent.Sprite.Transformations.Count; k++)
                    {
                        Transformation trans = selectedEvent.Sprite.Transformations[k];
                        if (trans.Type != dragTransformType)
                            continue;
                        trans.Time1 += delta;
                        trans.Time2 += delta;
                    }
                    selectedEvent.Sprite.Transformations.Sort();
                }
            }
            UpdateEventList();
        }

        private void UpdateLeftControls()
        {
            if (!GameBase.SixtyFramesPerSecondFrame)
                return;

            if (!isDragging)
            {
                if (MouseManager.MousePosition.X < 40 * GameBase.WindowRatio && Enabled)
                    spriteManagerLeftHidden.Alpha = Math.Min(1, spriteManagerLeftHidden.Alpha + 0.08f);
                else if (MouseManager.MousePosition.X > 160 * GameBase.WindowRatio || !Enabled)
                    spriteManagerLeftHidden.Alpha = Math.Max(0, spriteManagerLeftHidden.Alpha - 0.08f);
                else if (spriteManagerLeftHidden.Alpha < 1 && spriteManagerLeftHidden.Alpha > 0.5f)
                    spriteManagerLeftHidden.Alpha = Math.Min(1, spriteManagerLeftHidden.Alpha + 0.08f);
            }
        }

        private void AddColorFrame(Color c)
        {
            Transformation tr = null;
            AddKeyframe(AudioEngine.Time, out tr, true);
            if (tr == null)
                return;

            if (tr.Time1 == AudioEngine.Time)
                tr.StartColour = c;

            if (tr.Time2 == AudioEngine.Time)
                tr.EndColour = c;

            Transformation trBehind = selectedEventTransformations.Find(t => t.Time2 == AudioEngine.Time);
            if (trBehind != null && trBehind != tr)
            {
                trBehind.EndColour = c;
            }
        }

        private void SetSlightMovement(float x, float y)
        {
            Transformation tr = null;
            //    editor.UndoPush(); useless
            bool changed = false;
            TransformationType temp = TransformationType.None;
            if (currentTransformType != TransformationType.Movement)
            {
                temp = currentTransformType;
                currentTransformType = TransformationType.Movement;

                selectedEventTransformations =
                    selectedEvent.Sprite.Transformations.FindAll(t => t.Type == TransformationType.Movement);
            }
            AddKeyframe(AudioEngine.Time, out tr, true);
            if (tr == null)
                return;

            if (tr.Time1 == AudioEngine.Time)
            {
                tr.StartVector.X += x;
                tr.StartVector.Y += y;
            }
            if (tr.Time2 == AudioEngine.Time)
            {
                tr.EndVector.X += x;
                tr.EndVector.Y += y;
            }

            Transformation trBehind = selectedEventTransformations.Find(t => t.Time2 == AudioEngine.Time);
            if (trBehind != null && trBehind != tr)
            {
                trBehind.EndVector.X += x;
                trBehind.EndVector.Y += y;
            }

            if (changed)
            {
                currentTransformType = temp;
                selectedEventTransformations =
                    selectedEvent.Sprite.Transformations.FindAll(t => t.Type == temp);
            }
        }

        private void SetEndTransformation(Transformation transformation)
        {
            if (isDragging)
            {
                switch (currentTransformType)
                {
                    case TransformationType.Movement:
                        transformation.EndVector = mousePosition + dragOffset;
                        break;
                    case TransformationType.VectorScale:
                        transformation.EndVector = new Vector2(
                            OsuMathHelper.Clamp((dragStartFloat * 100 + dragStart.X - mousePosition.X) / 100, 0.1f, 5),
                            OsuMathHelper.Clamp((dragStartFloat2 * 100 + dragStart.Y - mousePosition.Y) / 100, 0.1f, 5)
                            );
                        break;
                    case TransformationType.Scale:
                        transformation.EndFloat =
                            OsuMathHelper.Clamp((dragStartFloat * 100 + dragStart.Y - mousePosition.Y) / 100, 0.1f, 5);
                        break;
                    case TransformationType.Rotation:
                        transformation.EndFloat =
                            OsuMathHelper.Clamp((dragStartFloat * 50 + dragStart.Y - mousePosition.Y) / 50, -50, 50);
                        break;
                    case TransformationType.Fade:
                        transformation.EndFloat =
                            OsuMathHelper.Clamp((dragStartFloat * 200 + dragStart.Y - mousePosition.Y) / 200, 0, 1);
                        break;
                }
            }
            else
            {
                switch (currentTransformType)
                {
                    case TransformationType.Movement:
                        transformation.EndVector = selectedEvent.Sprite.Position;
                        break;
                    case TransformationType.VectorScale:
                        transformation.EndVector = selectedEvent.Sprite.VectorScale;
                        break;
                    case TransformationType.Scale:
                        transformation.EndFloat = selectedEvent.Sprite.Scale;
                        break;
                    case TransformationType.Rotation:
                        transformation.EndFloat = selectedEvent.Sprite.Rotation;
                        break;
                    case TransformationType.Fade:
                        transformation.EndFloat = selectedEvent.Sprite.Alpha;
                        break;
                }
            }

            editor.Dirty = true;
        }

        private void SetStartTransformation(Transformation transformation)
        {
            if (isDragging)
            {
                switch (currentTransformType)
                {
                    case TransformationType.Movement:
                        transformation.StartVector = mousePosition + dragOffset;
                        break;
                    case TransformationType.VectorScale:
                        transformation.StartVector = new Vector2(
                            OsuMathHelper.Clamp((dragStartFloat * 100 + dragStart.X - mousePosition.X) / 100, 0.1f, 5),
                            OsuMathHelper.Clamp((dragStartFloat2 * 100 + dragStart.Y - mousePosition.Y) / 100, 0.1f, 5));
                        break;
                    case TransformationType.Scale:
                        transformation.StartFloat =
                            OsuMathHelper.Clamp((dragStartFloat * 100 + dragStart.Y - mousePosition.Y) / 100, 0.1f, 5);
                        break;
                    case TransformationType.Rotation:
                        transformation.StartFloat =
                            OsuMathHelper.Clamp((dragStartFloat * 50 + dragStart.Y - mousePosition.Y) / 50, -50, 50);
                        break;
                    case TransformationType.Fade:
                        transformation.StartFloat =
                            OsuMathHelper.Clamp((dragStartFloat * 200 + dragStart.Y - mousePosition.Y) / 200, 0, 1);
                        break;
                }
            }
            else
            {
                switch (currentTransformType)
                {
                    case TransformationType.Movement:
                        transformation.StartVector = selectedEvent.Sprite.Position;
                        break;
                    case TransformationType.VectorScale:
                        transformation.StartVector = selectedEvent.Sprite.VectorScale;
                        break;
                    case TransformationType.Scale:
                        transformation.StartFloat = selectedEvent.Sprite.Scale;
                        break;
                    case TransformationType.Rotation:
                        transformation.StartFloat = selectedEvent.Sprite.Rotation;
                        break;
                    case TransformationType.Fade:
                        transformation.StartFloat = selectedEvent.Sprite.Alpha;
                        break;
                }
            }

            editor.Dirty = true;
        }

        private static bool checkEventContainsMouse(Event e)
        {
            return e.Sprite.CheckHover(MouseManager.MousePosition, event_minimal_selection_size);
        }

        internal override bool OnClick()
        {
            if (!Enabled)
                return false;

            if (editor.uiManager.CurrentHoverSprite != null || spriteManager.CurrentHoverSprite != null || eventList.SpriteManager.CurrentHoverSprite != null)
                return false;

            if (editor.InTimeline || editor.InSeekbar)
                return false;//don't need any click in timeline

            if (!clickUpPass &&
                selectedEvent != null &&
                selectedEvent != hoveredEvent &&
                checkEventContainsMouse(selectedEvent))
            {
                clickUpPass = true;
                //Don't do anything until the second pass on mouseup
                return false;
            }

            if (hoveredEvent != null)
            {
                SetSelectedEvent(hoveredEvent, false);
                return true;
            }
            else if (editor.InGamefield)
            {
                SetSelectedEvent(null, false);
                return true;
            }

            return base.OnClick();
        }

        internal override bool OnClickUp()
        {
            if (!isDragging && clickUpPass)
            {
                OnClick();
                return true;
            }

            clickUpPass = false;

            return base.OnClickUp();
        }

        /// <summary>
        /// If a video is present, make a local reference to it for adjusting the offset in F2 view.
        /// </summary>
        internal void LoadVideoOffset()
        {
            //todo: get rid of this method
            Event e = eventManager.events.Find(ev => ev.Type == EventTypes.Video);
            if (e != null)
            {
                video = e;
                if (videoOffset != null)
                    videoOffset.SetValue(e.StartTime);
            }
        }

        internal override bool OnDragStart()
        {
            if (editor.InSeekbar)
                return false;

            editor.UndoPush();
            if (editor.InTimeline)
            {
                if (selectedEvent == null || selectedEvent.Sprite == null)
                    return false;
                float lineHeight = ((float)editor.TimelineRectangle.Height) / 4f;

                for (int i = 0; i < selectedEvent.Sprite.Transformations.Count; i++)
                {
                    float top = editor.TimelineRectangle.Top - timelineStart * lineHeight;
                    Transformation trans = selectedEvent.Sprite.Transformations[i];
                    switch (trans.Type)
                    {
                        case TransformationType.Scale:
                            top += lineHeight;
                            break;
                        case TransformationType.Fade:
                            top += lineHeight * 2;
                            break;
                        case TransformationType.Rotation:
                            top += lineHeight * 3;
                            break;
                        case TransformationType.Colour:
                            top += lineHeight * 4;
                            break;
                    }
                    RectangleF rect = new RectangleF(editor.TimeToTimelinePos(trans.Time1), top, editor.TimeToTimelinePos(trans.Time2) - editor.TimeToTimelinePos(trans.Time1), lineHeight);
                    if (rect.Contains(new Point(InputManager.CursorPoint.X, InputManager.CursorPoint.Y)))
                    {
                        dragTransformType = trans.Type;
                        dragTime = editor.TimelinePosToTime((int)MouseManager.MousePosition.X) - trans.Time1;
                        break;
                    }
                }
                if (dragTransformType != TransformationType.None)
                {
                    dragStart = new Vector2(InputManager.CursorPoint.X, InputManager.CursorPoint.Y);
                    isDragTimeline = true;
                    base.OnDragStart();
                    return true;
                }
                return false;
            }
            else
            {
                if (eventList.SpriteManager.CurrentHoverSprite != null &&
                    eventList.SpriteManager.CurrentHoverSprite.TagNumeric == 2)
                {
                    editor.Dirty = true;
                    eventListDragging = eventList.SpriteManager.CurrentHoverSprite;

                    eventListDragging.Depth = 1;
                    eventList.SpriteManager.ResortDepth();

                    eventListDraggingStart = MouseManager.MousePosition;
                    return true;
                }
                else if (eventList.DraggerHovered)
                {
                    return true;
                }

                if (editor.DragCaptured != DragCapture.Gamefield && editor.DragCaptured != DragCapture.None)
                    return false;

                if (hoveredEvent == null &&
                    (selectedEvent == null || !checkEventContainsMouse(selectedEvent)))
                    return false;

                if (editor.uiManager.CurrentHoverSprite != null || spriteManager.CurrentHoverSprite != null)
                    return false;

                if (selectedEvent == null)
                    return false;

                switch (currentTransformType)
                {
                    case TransformationType.Movement:
                        dragOffset = selectedEvent.Sprite.Position - mousePosition;
                        break;
                    case TransformationType.Scale:
                        dragStartFloat = selectedEvent.Sprite.Scale;
                        break;
                    case TransformationType.VectorScale:
                        dragStartFloat = selectedEvent.Sprite.VectorScale.X;
                        dragStartFloat2 = selectedEvent.Sprite.VectorScale.Y;
                        break;
                    case TransformationType.Fade:
                        dragStartFloat = selectedEvent.Sprite.Alpha;
                        break;
                    case TransformationType.Rotation:
                        dragStartFloat = selectedEvent.Sprite.Rotation;
                        break;
                }

                dragStart = mousePosition;
                isDragging = true;
                editor.Dirty = true;

                base.OnDragStart();

                return true;
            }
        }

        internal override void OnDragEnd()
        {
            isDragging = false;
            isDragTimeline = false;
            dragTransformType = TransformationType.None;
            if (eventListDragging != null)
            {
                editor.Dirty = true;
                eventListDragging.OriginPosition = Vector2.Zero;

                int countLayer = 0;
                int countSprite = 0;

                foreach (pSprite p in eventList.SpriteManager.SpriteList.FindAll(t => t.TagNumeric > 0))
                {
                    if (p.Transformations.Count > 0)
                    {
                        p.Position = p.Transformations[0].EndVector;
                        p.Transformations.Clear();
                    }

                    if (p.InitialPosition == p.Position)
                    {
                        if (p.TagNumeric == 1)
                        {
                            countLayer++;
                            countSprite = 0;
                        }
                        else
                            countSprite++;
                    }
                }

                Event e = (Event)eventListDragging.Tag;

                moveEventToLayer(e, (StoryLayer)(countLayer - 1), countSprite);

                eventListDragging = null;
                eventListUpdateRequired = true;
            }

            base.OnDragEnd();
        }

        private void moveEventToLayer(Event e, StoryLayer layer)
        {
            moveEventToLayer(e, layer, -1);
        }

        private void moveEventToLayer(Event e, StoryLayer layer, int position)
        {
            eventManager.Remove(e);

            if (position < 0)
            {
                //simply insert at the highest depth
                selectedEvent.Layer = layer;
                eventManager.Add(e);
            }
            else
            {
                eventManager.Add(e, layer, position);
            }

            if (!eventManager.storyLayerEnabled[(int)layer])
                SelectNone();

            eventListUpdateRequired = true;
        }

        internal override bool OnKey(Keys keys, bool first)
        {
            if (ChatEngine.IsVisible)
                return false;

            if (first)
            {
                if (KeyboardHandler.ControlPressed)
                {
                    switch (keys)
                    {
                        case Keys.D1:
                            ToggleLayer(StoryLayer.Background);
                            return true;
                        case Keys.D2:
                            ToggleLayer(StoryLayer.Fail);
                            return true;
                        case Keys.D3:
                            ToggleLayer(StoryLayer.Pass);
                            return true;
                        case Keys.D4:
                            ToggleLayer(StoryLayer.Foreground);
                            return true;
                        case Keys.V:
                            PasteSelection();
                            return true;
                        case Keys.X:
                            if (selectedEvent != null)
                                copiedEvent = selectedEvent.Clone();
                            DeleteSelectedEvent();
                            return true;
                    }
                }
                else if (KeyboardHandler.AltPressed)
                {
                    //none yet
                }
                else
                {
                    switch (keys)
                    {
                        case Keys.I:
                            ShowSpriteLibrary();
                            return true;
                        case Keys.Delete:
                            DeleteSelectedEvent();
                            return true;
                        case Keys.W:
                            SetSlightMovement(0, -1);
                            return true;
                        case Keys.A:
                            SetSlightMovement(-1, 0);
                            return true;
                        case Keys.S:
                            SetSlightMovement(0, 1);
                            return true;
                        case Keys.D:
                            SetSlightMovement(1, 0);
                            return true;
                    }
                }
            }

            if (videoOffset.Hovering == true)
            {
                if (KeyboardHandler.ShiftPressed)
                    videoOffset.KeyboardControlIncrement = 1;
                else
                    videoOffset.KeyboardControlIncrement = 10;

                return true;
            }

            return base.OnKey(keys, first);
        }

        private void DeleteSelectedEvent()
        {
            if (selectedEvent != null)
            {
                editor.UndoPush();
                eventManager.Remove(selectedEvent);

                selectedEvent = null;
                selectedIndex = -1;
                eventListUpdateRequired = true;
            }
        }

        internal override void SelectNone()
        {
            SetSelectedEvent(null, true);

            base.SelectNone();
        }

        private void SetSelectedEvent(Event e, bool forceUpdate)
        {
            if (selectedEvent != null)
            {
                pText pt;
                if (eventListDictionary.TryGetValue(selectedEvent, out pt))
                {
                    pt.TextBold = false;
                    pt.TextUnderline = false;
                    pt.TextChanged = true;
                }
            }

            selectedEvent = e;

            if (forceUpdate)
                Update(); //force an update

            if (e != null)
                selectedIndex = visibleEvents.IndexOf(selectedEvent);
            else
                selectedIndex = -1;

            if (selectedEvent != null)
            {
                pText pt;
                if (eventListDictionary.TryGetValue(selectedEvent, out pt))
                {
                    pt.TextBold = true;
                    pt.TextUnderline = true;
                    pt.TextChanged = true;
                }
            }
        }

        internal override void UpdateTimeline()
        {
        }

        internal override void DrawTimeline()
        {
            GameBase.primitiveBatch.Begin();

            float height = (float)editor.TimelineRectangle.Height / 4;

            GameBase.primitiveBatch.AddVertex(
                new Vector2(editor.TimelineRectangle.Left, editor.TimelineRectangle.Top + height), Color.Gray);
            GameBase.primitiveBatch.AddVertex(
                new Vector2(editor.TimelineRectangle.Right, editor.TimelineRectangle.Top + height), Color.Gray);

            GameBase.primitiveBatch.AddVertex(
                new Vector2(editor.TimelineRectangle.Left, editor.TimelineRectangle.Top + height * 2), Color.Gray);
            GameBase.primitiveBatch.AddVertex(
                new Vector2(editor.TimelineRectangle.Right, editor.TimelineRectangle.Top + height * 2), Color.Gray);

            GameBase.primitiveBatch.AddVertex(
                new Vector2(editor.TimelineRectangle.Left, editor.TimelineRectangle.Top + height * 3), Color.Gray);
            GameBase.primitiveBatch.AddVertex(
                new Vector2(editor.TimelineRectangle.Right, editor.TimelineRectangle.Top + height * 3), Color.Gray);

            if (selectedEvent != null)
            {
                foreach (Transformation t in selectedEvent.Sprite.Transformations)
                {
                    if (timelineStart == 0 && t.Type == TransformationType.Colour) continue;
                    else if (timelineStart == 1 && t.Type == TransformationType.Movement) continue;

                    bool time1inrange = false;
                    bool time2inrange = false;

                    float pos1 = 0, pos2 = 0;

                    float top = editor.TimelineRectangle.Top - height * timelineStart;

                    Color col;
                    switch (t.Type)
                    {
                        default:
                        case TransformationType.Movement:
                            col = Color.YellowGreen;
                            break;
                        case TransformationType.Scale:
                            col = Color.OrangeRed;
                            top += height;
                            break;
                        case TransformationType.VectorScale:
                            col = Color.Yellow;
                            top += height;
                            break;
                        case TransformationType.Fade:
                            col = Color.Pink;
                            top += height * 2;
                            break;
                        case TransformationType.Rotation:
                            col = Color.Yellow;
                            top += height * 3;
                            break;
                        case TransformationType.ParameterFlipHorizontal:
                        case TransformationType.ParameterFlipVertical:
                            top += height * 3;
                            col = Color.LightBlue;
                            break;
                        case TransformationType.Colour:
                            top += height * 4;
                            col = Color.BlanchedAlmond;
                            break;
                    }

                    if (t.Time1 >= editor.TimelineLeftBound && t.Time1 <= editor.TimelineRightBound)
                    {
                        time1inrange = true;

                        pos1 = editor.TimeToTimelinePos(t.Time1);
                        GameBase.primitiveBatch.AddVertex(new Vector2(pos1, top), Color.Beige);
                        GameBase.primitiveBatch.AddVertex(new Vector2(pos1, top + 0.49f * height), Color.Beige);
                    }

                    if (t.Time2 >= editor.TimelineLeftBound && t.Time2 <= editor.TimelineRightBound)
                    {
                        time2inrange = true;

                        pos2 = editor.TimeToTimelinePos(t.Time2);
                        GameBase.primitiveBatch.AddVertex(new Vector2(pos2, top + 0.51f * height), Color.Beige);
                        GameBase.primitiveBatch.AddVertex(new Vector2(pos2, top + height), Color.Beige);
                    }

                    if (time1inrange || time2inrange || (t.Time1 < editor.TimelineLeftBound && t.Time2 > editor.TimelineRightBound))
                    {
                        if (time1inrange)
                            GameBase.primitiveBatch.AddVertex(new Vector2(pos1, top + 0.5f * height), col);
                        else
                            GameBase.primitiveBatch.AddVertex(
                                new Vector2(editor.TimelineRectangle.Left, top + 0.5f * height), col);

                        if (time2inrange)
                            GameBase.primitiveBatch.AddVertex(new Vector2(pos2, top + 0.5f * height), col);
                        else
                            GameBase.primitiveBatch.AddVertex(
                                new Vector2(editor.TimelineRectangle.Right, top + 0.5f * height), col);
                    }
                }
            }

            GameBase.primitiveBatch.End();
        }

        internal override void Enter()
        {
            GameBase.ResizeGamefield(0.78125f, true);
            editor.SelectNone();
            editor.TimelineTickHeight = 6;

            //Storyboard gets moved back up to neutral position, and gamefield moves instead.
            GameBase.GamefieldOffsetVector1.Y -= 16;

            editor.eventManager.spriteManagerBGWide.ViewOffset = new Vector2(0, 0);
            editor.eventManager.spriteManagerBG.ViewOffset = new Vector2(0, 0);
            editor.eventManager.spriteManagerFG.ViewOffset = new Vector2(0, 0);

            ToggleHitObjects(ShowHitObjects);

            eventManager.spriteManagerBGWide.CalculateInvisibleUpdates = true;
            eventManager.spriteManagerFG.CalculateInvisibleUpdates = true;
            eventManager.spriteManagerBG.CalculateInvisibleUpdates = true;

            AudioEngine.AutomaticVirtualTime = true;
            eventList.Alpha = 1;

            base.Enter();
        }

        internal override void Leave()
        {
            eventList.Alpha = 0;
            hitObjectManager.spriteManager.Alpha = 1;
            spriteManagerLeftHidden.Alpha = 0;

            AudioEngine.AutomaticVirtualTime = false;

            eventManager.spriteManagerFG.CalculateInvisibleUpdates = false;
            eventManager.spriteManagerBGWide.CalculateInvisibleUpdates = false;
            eventManager.spriteManagerBG.CalculateInvisibleUpdates = false;

            InputManager.DragFinish();
            base.Leave();
        }

        internal override void Draw()
        {
            if (hoveredEvent != null)
            {
                foreach (Event e in otherHoveredEvents)
                {
                    if (e != selectedEvent)
                        BorderEvent(e, Color.Silver);
                }
                BorderEvent(hoveredEvent, hoveredEvent == selectedEvent ? Color.White : Color.Silver);
            }

            DrawEventList();

            if (selectedEvent != null && hoveredEvent != selectedEvent)
                BorderEvent(selectedEvent, Color.White);

            spriteManagerLeftHidden.Draw();

            base.Draw();
        }

        private void BorderEvent(Event e, Color colour)
        {
            e.Sprite.DrawBorder(colour, event_minimal_selection_size);
        }

        internal override void Dispose()
        {
            if (spriteManagerLeftHidden != null) spriteManagerLeftHidden.Dispose();
            if (eventList != null) eventList.Dispose();

            base.Dispose();
        }

        public void ToggleHitObjects(bool show)
        {
            ShowHitObjects = show;
            hitObjectManager.spriteManager.Alpha = ShowHitObjects ? 1 : 0.05f;
        }

        internal override void Reset()
        {
            if (checkLayerBg == null) return; //Not yet initialized (not the best check)

            checkLayerBg.SetStatusQuietly(true);
            checkLayerFailing.SetStatusQuietly(true);
            checkLayerPassing.SetStatusQuietly(true);
            checkLayerFg.SetStatusQuietly(true);
            SetSelectedEvent(null, false);
            selectedIndex = -1;
            hoveredEvent = null;
            otherHoveredEvents.Clear();

            eventListUpdateRequired = true;

            eventManager = hitObjectManager.eventManager;
            if (eventManager != null) LoadVideoOffset();

            if (Active)
            {
                eventManager.spriteManagerFG.CalculateInvisibleUpdates = true;
                eventManager.spriteManagerBG.CalculateInvisibleUpdates = true;
                eventManager.spriteManagerBGWide.CalculateInvisibleUpdates = true;
            }
        }

        internal void MoveAllEvents(int amountInt)
        {
            if (amountInt == 0) return;

            foreach (Event e in eventManager.events)
            {
                if (e is EventSample || e is EventSprite || e is EventAnimation || e is EventVideo)
                {
                    e.StartTime += amountInt;
                    e.EndTime += amountInt;
                    if (e.Sprite != null)
                    {
                        foreach (Transformation t in e.Sprite.Transformations)
                        {
                            t.Time1 += amountInt;
                            t.Time2 += amountInt;
                        }

                        if (e.Sprite.Loops != null)
                        {
                            foreach (TransformationLoop tl in e.Sprite.Loops)
                            {
                                tl.StartTime += amountInt;
                            }
                        }
                    }

                    foreach (List<TriggerLoop> ll in e.EventLoopTriggers.Values)
                    {
                        foreach (TriggerLoop l in ll)
                        {
                            l.StartTime += amountInt;
                            l.TriggerStartTime += amountInt;
                            l.TriggerEndTime += amountInt;
                        }
                    }
                }
            }
        }
    }
}