﻿using System;
using System.Collections.Generic;
using osu.Audio;
using osu.GameplayElements.HitObjects;

namespace osu.GameModes.Edit.Modes
{
    internal partial class EditorModeTiming
    {
        internal void beatSnapSong(bool currentTimingSectionOnly = false, int? snapDivisor = null)
        {
            if (AudioEngine.TimingPoints.Count == 0 || changeInProgress)
                return;

            if (!snapDivisor.HasValue) snapDivisor = editor.beatSnapDivisor;

            editor.UndoPush();

            List<ControlPoint> timingPoints = AudioEngine.TimingPoints;

            int start = 0;
            int end;
            if (currentTimingSectionOnly)
            {
                //start = AudioEngine.ActiveTimingPointIndex;
                start = timingPoints.IndexOf(AudioEngine.ControlPoints[start]);

                end = start + 1;
            }
            else
                end = timingPoints.Count;

            double startTime = timingPoints[start].offset;
            double endTime = end == timingPoints.Count ? AudioEngine.AudioLength : timingPoints[end].offset;

            foreach (HitObject h in editor.hitObjectManager.hitObjects)
            {
                if (h.StartTime < startTime || h.StartTime > endTime)
                    continue;
                ControlPoint head;
                if (currentTimingSectionOnly)
                    head = timingPoints[start];
                else
                    head = AudioEngine.customPointAtBin(h.StartTime, timingPoints);
                h.ModifyTime((int)findSnapTime(h.StartTime, head, snapDivisor));
                if (h.IsType(HitObjectType.Spinner) || h.IsType(HitObjectType.Hold))
                    h.SetEndTime((int)findSnapTime(h.EndTime, head, snapDivisor));
            }

            hitObjectManager.Sort(true);
            hitObjectManager.UpdateSlidersAll(false);
        }

        private double findSnapTime(double org, ControlPoint cp, int? snapDivisor = null)
        {
            double length = org - cp.offset;

            if (snapDivisor.HasValue)
            {
                //use a snap divisor if provided.
                double div = length / (cp.beatLength / snapDivisor.Value);
                return cp.offset + Math.Round(div) * (cp.beatLength / snapDivisor.Value);
            }
            else
            {
                //1/16 and 1/12 contains all kind of beats.
                double divied16 = length / (cp.beatLength / 16);
                double divied12 = length / (cp.beatLength / 12);
                double remind16 = Math.Abs(Math.Round(divied16) - divied16);
                double remind12 = Math.Abs(Math.Round(divied12) - divied12);
                if (remind16 < remind12)
                {
                    return cp.offset + Math.Round(divied16) * (cp.beatLength / 16);
                }
                else
                {
                    return cp.offset + Math.Round(divied12) * (cp.beatLength / 12);
                }
            }
        }

        internal void MoveNotesBy(int offset)
        {
            editor.UndoPush();
            foreach (HitObject h in editor.hitObjectManager.hitObjects)
            {
                h.ModifyTime(Math.Min(AudioEngine.AudioLength, Math.Max(0, h.StartTime + offset)));
            }
            hitObjectManager.Sort(true);
            hitObjectManager.UpdateSlidersAll(false);
        }

        internal void beatShiftCircle(int direction)
        {
            if (editor.Compose.selectedObjects.Count == 0) return;

            double increment = AudioEngine.beatLengthAt(editor.Compose.selectedObjects[0].StartTime, false) / editor.beatSnapDivisor;

            foreach (HitObject h in editor.Compose.selectedObjects)
                h.ModifyTime(BeatSnapValue(h.StartTime) + (int)(direction * increment));

            hitObjectManager.Sort(true);
        }

        internal int BeatSnapClosestValue(double time)
        {
            int snapValue, snapValueSixth;

            snapValue = BeatSnapValue(time, 16, time);
            snapValueSixth = BeatSnapValue(time, 12, time);

            return (Math.Abs(snapValue - time) <= Math.Abs(snapValueSixth - time) ? snapValue : snapValueSixth);
        }

        internal int BeatSnapValue(double time)
        {
            return BeatSnapValue(time, editor.beatSnapDivisor, time);
        }

        internal int BeatSnapValue(double time, double original)
        {
            return BeatSnapValue(time, editor.beatSnapDivisor, original);
        }

        /// <summary>
        /// Beats the snap value.
        /// </summary>
        /// <param name="time">The time which should be snapped.</param>
        /// <param name="snapDivisor">The snap divisor (the bottom number of a fraction).</param>
        /// <param name="original">Assuming we want to move *away* from an old timing (ie. jump left 1 notch) set this to the original.</param>
        /// <returns></returns>
        internal int BeatSnapValue(double time, int snapDivisor, double original)
        {
            if (AudioEngine.ControlPoints.Count == 0)
                return (int)time;

            double offset = AudioEngine.beatOffsetCloseToZeroAt(original);
            double increment = AudioEngine.beatLengthAt(original, false) / snapDivisor;

            int flooredCount;
            if (time - offset < 0)
                flooredCount = (int)((time - offset) / increment) - 1;
            else
                flooredCount = (int)((time - offset) / increment);

            int leftVal = (int)(flooredCount * increment + offset);
            int rightVal = (int)((flooredCount + 1) * increment + offset);

            if (original != time)
            {
                if (leftVal == original) return rightVal;
                if (rightVal == original) return leftVal;
            }

            return time - leftVal < rightVal - time ? leftVal : rightVal;
        }

        protected override void InitializeSettings()
        {

        }
    }
}