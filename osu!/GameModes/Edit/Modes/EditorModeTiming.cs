﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.GameModes.Edit.Forms;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu.Graphics;
using osu.Graphics.Primitives;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input;
using osu.Input.Handlers;
using Un4seen.Bass;
using System.Text;
using osu.Graphics.Notifications;
using osu_common.Helpers;

namespace osu.GameModes.Edit.Modes
{
    internal partial class EditorModeTiming : EditorMode
    {
        private bool changeInProgress = false;
        private readonly AudioSample s_metronomeHigh;
        private readonly AudioSample s_metronomeLow;
        internal pText beatBpmDisplay;
        internal pText beatDisplay;
        internal pText beatOffsetDisplay;
        internal pSprite[] beatSquares;
        internal double bpmUpdateOrig;
        internal bool bpmUpdatePending;
        private pCheckbox checkboxOffsetMove;
        private ControlPoint currentActivePoint;
        private int currBeat;
        private int currBeatTotal;
        private int currStanza;
        private int currTimingPoint;

        private int lastMetronomeBeat;
        private int lastMetronomeBeat2x;
        private int lastMetronomeBeat3x;
        internal bool MetronomeEnabled = true;

        internal double offsetUpdateOrig;
        internal bool offsetUpdatePending;
        private pText sliderTickDisplay;

        internal bool sliderUpdatePending;
        private pText sliderVelocityDisplay;
        private pText timingText;
        internal bool timingWindowUpdatePending;
        private pSprite tapButton;

        private Dictionary<ControlPoint, List<pDrawable>> timingInfos = new Dictionary<ControlPoint, List<pDrawable>>();

        private float SLIDER_SPEED_MIN = 0.4f;
        private float SLIDER_SPEED_MAX = 3.6f;

        internal override bool Enabled
        {
            get { return base.Enabled; }
            set
            {
                base.Enabled = value;
                spriteManagerCentre.HandleInput = value;
            }
        }

        internal EditorModeTiming(Editor editor)
            : base(editor)
        {
            s_metronomeHigh = new AudioSample(AudioEngine.LoadSample(@"metronomehigh", false, true, SkinSource.Osu));
            s_metronomeLow = new AudioSample(AudioEngine.LoadSample(@"metronomelow", false, true, SkinSource.Osu));

            spriteManagerCentre = new SpriteManager();
        }

        internal override void CopySelection()
        {
            StringBuilder timingInfo = new StringBuilder();
            for (int i = 0; i < AudioEngine.ControlPoints.Count; i++)
            {
                ControlPoint p = AudioEngine.ControlPoints[i];
                if (p.timingChange)
                    timingInfo.AppendFormat("{2}. Offset: {0:#,0}ms\tBPM: {1:#.00#}\n", p.offset, p.bpm, i + 1);
                else
                    timingInfo.AppendFormat("{2}. Offset: {0:#,0}ms\tBPM: Inherited ({1}x)\n", p.offset, Math.Round(-100f / p.beatLength, 1), i + 1);
            }

            try
            {
                System.Windows.Forms.Clipboard.SetText(timingInfo.ToString());
            }
            catch (Exception)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.General_Editor_ClipboardNotAvailable));
            }
        }

        protected override void InitializeSprites()
        {
            //Beat Sync EditorControl

            spriteManagerCentre.Add(
                new pSprite(SkinManager.Load(@"editor-timing-window-bg", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                            Clocks.Game, new Vector2(120, 85), 0, true, Color.White,
                            EditorModes.Timing));

            spriteManager.Add(
                new pSprite(SkinManager.Load(@"editor-metronome-bg", SkinSource.Osu), Fields.TopRight,
                            Origins.TopLeft,
                            Clocks.Game, new Vector2(200, 0), 0, true, Color.White,
                            EditorModes.Timing));

            int timingOffset = 120;
            int timingOffsetChange = 30;

            //BPM Control
            spriteManagerCentre.Add(
                new pSprite(SkinManager.Load(@"editor-beat-selector-back", SkinSource.Osu), Fields.TopLeft,
                            Origins.TopLeft,
                            Clocks.Game, new Vector2(258, timingOffset), 0.2F, true,
                            Color.White,
                            EditorModes.Timing));


            pSprite p =
                new pSprite(SkinManager.Load(@"editor-beat-selector-buttonleft", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(270, timingOffset + 10), 0.9f, true,
                            Color.White, EditorModes.Timing);
            p.HandleInput = true;
            p.HoverEffect = new Transformation(Color.White, Color.Orange, 0, 140);
            p.ClickEffect = new Transformation(TransformationType.Scale, 1.2F, 1, 0, 200);
            p.ToolTip =
                LocalisationManager.GetString(OsuString.Editor_Timing_DecreaseBPM) + "\n" +
                LocalisationManager.GetString(OsuString.EditorModeTiming_HoldShiftForLargerChanges) + "\n" +
                LocalisationManager.GetString(OsuString.EditorModeTiming_HoldCtrlForSmallerChanges);
            p.OnClick += bpmDown_OnClick;
            spriteManagerCentre.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-beat-selector-buttonright", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(382, timingOffset + 10), 0.9f, true,
                            Color.White, EditorModes.Timing);
            p.HandleInput = true;
            p.ClickEffect = new Transformation(TransformationType.Scale, 1.2F, 1, 0, 200);
            p.HoverEffect = new Transformation(Color.White, Color.Orange, 0, 140);
            p.ToolTip =
                LocalisationManager.GetString(OsuString.Editor_Timing_IncreaseBPM) + "\n" +
                LocalisationManager.GetString(OsuString.EditorModeTiming_HoldShiftForLargerChanges) + "\n" +
                LocalisationManager.GetString(OsuString.EditorModeTiming_HoldCtrlForSmallerChanges);
            p.OnClick += bpmUp_OnClick;
            spriteManagerCentre.Add(p);


            beatBpmDisplay =
                new pText(string.Empty, 14, new Vector2(324, timingOffset + 10), 0.9f, true, Color.White);
            beatBpmDisplay.Tag = EditorModes.Timing;
            beatBpmDisplay.Origin = Origins.Centre;
            beatBpmDisplay.HandleInput = true;
            beatBpmDisplay.HoverEffect = new Transformation(Color.White, Color.YellowGreen, 0, 200);
            beatBpmDisplay.OnClick += delegate { ShowTimingWindow(TimingFocus.BPM, false); };
            beatBpmDisplay.ToolTip = LocalisationManager.GetString(OsuString.EditorModeTiming_ClickToFineTuneTooltip);
            spriteManagerCentre.Add(beatBpmDisplay);

            pText pt = new pText(@"BPM:", 14, new Vector2(155, timingOffset + 3), 0.9f, true, Color.White);
            pt.TextAlignment = TextAlignment.Right;
            pt.TextBounds = new Vector2(100, 20);
            pt.Tag = EditorModes.Timing;
            spriteManagerCentre.Add(pt);

            timingOffset += timingOffsetChange;

            timingText =
                new pText(LocalisationManager.GetString(OsuString.EditorModeTiming_TimingMenuNote), 14,
                          new Vector2(145, timingOffset), 0.9f, true, Color.White);
            timingText.Tag = EditorModes.Timing;
            spriteManagerCentre.Add(timingText);

            timingOffset += (int)(timingOffsetChange * 1.2);

            //Offset Control
            spriteManagerCentre.Add(
                new pSprite(SkinManager.Load(@"editor-beat-selector-back", SkinSource.Osu), Fields.TopLeft,
                            Origins.TopLeft,
                            Clocks.Game, new Vector2(258, timingOffset), 0.2F, true,
                            Color.White,
                            EditorModes.Timing));

            p =
                new pSprite(SkinManager.Load(@"editor-beat-selector-buttonleft", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(270, timingOffset + 10), 0.9f, true,
                            Color.White, EditorModes.Timing);
            p.HandleInput = true;
            p.ClickEffect = new Transformation(TransformationType.Scale, 1.2F, 1, 0, 200);
            p.OnClick += offsetLeft_OnClick;
            p.HoverEffect = new Transformation(Color.White, Color.Orange, 0, 140);
            p.ToolTip =
                LocalisationManager.GetString(OsuString.General_DecreaseOffset) + "\n" +
                LocalisationManager.GetString(OsuString.EditorModeTiming_HoldShiftForLargerChanges) + "\n" +
                LocalisationManager.GetString(OsuString.EditorModeTiming_HoldCtrlForSmallerChanges);
            spriteManagerCentre.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-beat-selector-buttonright", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(382, timingOffset + 10), 0.9f, true,
                            Color.White, EditorModes.Timing);
            p.HandleInput = true;
            p.ClickEffect = new Transformation(TransformationType.Scale, 1.2F, 1, 0, 200);
            p.HoverEffect = new Transformation(Color.White, Color.Orange, 0, 140);
            p.ToolTip =
                LocalisationManager.GetString(OsuString.General_IncreaseOffset) + "\n" +
                LocalisationManager.GetString(OsuString.EditorModeTiming_HoldShiftForLargerChanges) + "\n" +
                LocalisationManager.GetString(OsuString.EditorModeTiming_HoldCtrlForSmallerChanges);
            p.OnClick += offsetRight_OnClick;
            spriteManagerCentre.Add(p);

            beatOffsetDisplay =
                new pText(string.Empty, 14, new Vector2(324, timingOffset + 10), 0.9f, true, Color.White);
            beatOffsetDisplay.Tag = EditorModes.Timing;
            beatOffsetDisplay.Origin = Origins.Centre;
            beatOffsetDisplay.HandleInput = true;
            beatOffsetDisplay.HoverEffect = new Transformation(Color.White, Color.YellowGreen, 0, 200);
            beatOffsetDisplay.OnClick += delegate { ShowTimingWindow(TimingFocus.Offset, false); };
            beatOffsetDisplay.ToolTip = LocalisationManager.GetString(OsuString.EditorModeTiming_ClickToFineTuneTooltip);
            spriteManagerCentre.Add(beatOffsetDisplay);

            pt = new pText(LocalisationManager.GetString(OsuString.General_Offset) + @":", 14, new Vector2(155, timingOffset + 3), 0.9f, true, Color.White);
            pt.TextAlignment = TextAlignment.Right;
            pt.TextBounds = new Vector2(100, 20);
            pt.Tag = EditorModes.Timing;
            spriteManagerCentre.Add(pt);

            p =
                new pSprite(SkinManager.Load(@"editor-timing-manual", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(455, 146), 0.9f, true,
                            Color.White, EditorModes.Timing);
            p.HandleInput = true;
            p.HoverEffect = new Transformation(TransformationType.Scale, 1, 1.1F, 0, 200);
            p.OnClick += delegate { ShowTimingWindow(); };
            p.ToolTip = LocalisationManager.GetString(OsuString.EditorModeTiming_TimingSetupTooltip);
            spriteManagerCentre.Add(p);

            timingOffset += timingOffsetChange;

            checkboxOffsetMove =
                new pCheckbox(LocalisationManager.GetString(OsuString.EditorModeTiming_MoveAlreadyPlacedNotes), new Vector2(145, timingOffset),
                              0.9f, true);
            checkboxOffsetMove.Tooltip =
                LocalisationManager.GetString(OsuString.EditorModeTiming_MoveAlreadyPlacedTooltip);
            foreach (pSprite ps in checkboxOffsetMove.SpriteCollection)
                ps.Tag = EditorModes.Timing;

            spriteManagerCentre.Add(checkboxOffsetMove.SpriteCollection);

            timingOffset += 60;

            //Slider Velocity Control
            pt =
                new pText(LocalisationManager.GetString(OsuString.Editor_Timing_SliderVelocity) + @":", 14, new Vector2(155, timingOffset + 3), 0.9f, true, Color.White);
            pt.TextAlignment = TextAlignment.Right;
            pt.TextBounds = new Vector2(100, 20);
            pt.Tag = EditorModes.Timing;
            spriteManagerCentre.Add(pt);

            spriteManagerCentre.Add(
                new pSprite(SkinManager.Load(@"editor-beat-selector-back", SkinSource.Osu), Fields.TopLeft,
                            Origins.TopLeft,
                            Clocks.Game, new Vector2(258, timingOffset), 0.2F, true,
                            Color.White,
                            EditorModes.Timing));

            p =
                new pSprite(SkinManager.Load(@"editor-beat-selector-buttonleft", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(270, timingOffset + 10), 0.9f, true,
                            Color.White, EditorModes.Timing);
            p.HandleInput = true;
            p.HoverEffect = new Transformation(Color.White, Color.Orange, 0, 140);
            p.ClickEffect = new Transformation(TransformationType.Scale, 1.2F, 1, 0, 200);
            p.OnClick += sliderVelocityLeft_OnClick;
            spriteManagerCentre.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-beat-selector-buttonright", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(382, timingOffset + 10), 0.9f, true,
                            Color.White, EditorModes.Timing);
            p.HandleInput = true;
            p.HoverEffect = new Transformation(Color.White, Color.Orange, 0, 140);
            p.ClickEffect = new Transformation(TransformationType.Scale, 1.2F, 1, 0, 200);
            p.OnClick += sliderVelocityRight_OnClick;
            spriteManagerCentre.Add(p);

            sliderVelocityDisplay =
                new pText(string.Empty, 14, new Vector2(324, timingOffset + 10), 0.9f, true, Color.White);
            sliderVelocityDisplay.Tag = EditorModes.Timing;
            sliderVelocityDisplay.Origin = Origins.Centre;
            spriteManagerCentre.Add(sliderVelocityDisplay);

            timingOffset += timingOffsetChange;

            //Slider Tick Spacing
            pt =
                new pText(LocalisationManager.GetString(OsuString.Editor_Timing_SliderTickRate) + @":", 14, new Vector2(155, timingOffset + 3), 0.9f, true,
                          Color.White);
            pt.TextAlignment = TextAlignment.Right;
            pt.TextBounds = new Vector2(100, 20);
            pt.Tag = EditorModes.Timing;
            spriteManagerCentre.Add(pt);

            spriteManagerCentre.Add(
                new pSprite(SkinManager.Load(@"editor-beat-selector-back", SkinSource.Osu), Fields.TopLeft,
                            Origins.TopLeft,
                            Clocks.Game, new Vector2(258, timingOffset), 0.2F, true,
                            Color.White,
                            EditorModes.Timing));

            p =
                new pSprite(SkinManager.Load(@"editor-beat-selector-buttonleft", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(270, timingOffset + 10), 0.9f, true,
                            Color.White, EditorModes.Timing);
            p.HoverEffect = new Transformation(Color.White, Color.Orange, 0, 140);
            p.HandleInput = true;
            p.ClickEffect = new Transformation(TransformationType.Scale, 1.2F, 1, 0, 200);
            p.OnClick += sliderTickLeft_OnClick;
            spriteManagerCentre.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-beat-selector-buttonright", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(382, timingOffset + 10), 0.9f, true,
                            Color.White, EditorModes.Timing);
            p.HandleInput = true;
            p.HoverEffect = new Transformation(Color.White, Color.Orange, 0, 140);
            p.ClickEffect = new Transformation(TransformationType.Scale, 1.2F, 1, 0, 200);
            p.OnClick += sliderTickRight_OnClick;
            spriteManagerCentre.Add(p);

            sliderTickDisplay =
                new pText(string.Empty, 14, new Vector2(324, timingOffset + 10), 0.9f, true, Color.White);
            sliderTickDisplay.Tag = EditorModes.Timing;
            sliderTickDisplay.Origin = Origins.Centre;
            spriteManagerCentre.Add(sliderTickDisplay);

            beatSquares = new[]
                              {
                                  new pSprite(SkinManager.Load(@"editor-metronome-beat-lighting", SkinSource.Osu), Fields.TopRight,
                                              Origins.Centre,
                                              Clocks.Game, new Vector2(123, 18), 0.9F, true,
                                              Color.White),
                                  new pSprite(SkinManager.Load(@"editor-metronome-beat-lighting", SkinSource.Osu), Fields.TopRight,
                                              Origins.Centre,
                                              Clocks.Game, new Vector2(89, 18), 0.9F, true,
                                              Color.White),
                                  new pSprite(SkinManager.Load(@"editor-metronome-beat-lighting", SkinSource.Osu), Fields.TopRight,
                                              Origins.Centre,
                                              Clocks.Game, new Vector2(55, 18), 0.9F, true,
                                              Color.White),
                                  new pSprite(SkinManager.Load(@"editor-metronome-beat-lighting", SkinSource.Osu), Fields.TopRight,
                                              Origins.Centre,
                                              Clocks.Game, new Vector2(21, 18), 0.9F, true,
                                              Color.White),
                              };
            // make the left blinky light a bit taller since it carries special meaning ^^
            //beatSquares[0].VectorScale = new Vector2(1, 1.15f);

            foreach (pSprite bs in beatSquares)
                bs.Tag = EditorModes.Timing;


            spriteManager.Add(beatSquares);

            pButton tapButton = new pButton(LocalisationManager.GetString(OsuString.EditorModeTiming_TapHere), new Vector2(140, 34), new Vector2(136, 25), 1, Color.SkyBlue, tapButton_OnClick, true, true, 12);
            tapButton.SoundsEnabled = false;
            tapButton.ToolTip = LocalisationManager.GetString(OsuString.EditorModeTiming_TapHereTooltip);
            spriteManager.Add(tapButton.SpriteCollection);


            pButton resetButton = new pButton(LocalisationManager.GetString(OsuString.General_Reset), new Vector2(197, 4), new Vector2(58, 58), 1, Color.Orange, resetButton_OnClick, true, true, 12);
            spriteManager.Add(resetButton.SpriteCollection);

            beatDisplay =
                new pText(string.Empty, 13, new Vector2(200, 20), 1, true, Color.White);
            beatDisplay.Field = Fields.TopRight;
            beatDisplay.Origin = Origins.TopRight;
            beatDisplay.Tag = EditorModes.Timing;
            spriteManager.Add(beatDisplay);
        }

        private void DrawMetronomeSlider()
        {
            List<Line> lineList = new List<Line>();
            Color c = new Color(250, 250, 250, 140);
            Color c2 = new Color(50, 50, 50, 140);
            Vector2 vCentre = new Vector2(330, 360);

            ControlPoint active = AudioEngine.ActiveControlPoint;
            float mul = active != null ? active.bpmMultiplier : 1; // Avoid nullref when no sections exist.


            float dist = (float)(100 * BeatmapManager.Current.DifficultySliderMultiplier / mul); // Length of one beat
            float count = (int)(300 / dist); // Number of beats

            // Fix for very fast slider speeds: make the slider half a beat long.
            // This fix should hold for speeds up to 6.0x
            if (count == 0) count = 0.5f;

            float totalLength = count * dist;
            Vector2 off = new Vector2(totalLength * 0.5f, 0);

            lineList.Add(
                new Line(GameBase.WindowRatioApply(vCentre - off), GameBase.WindowRatioApply(vCentre + off)));
            GameBase.LineManager.Draw(lineList, hitObjectManager.HitObjectRadius * GameBase.WindowRatioInverse, c, 0,
                                      @"Standard", true);
            lineList.Clear();

            Vector2 cPos = vCentre - off;

            // fix tick spacing for slider (BPM) multiplier sections
            Vector2 pointDist = new Vector2((float)hitObjectManager.SliderScoringPointDistance / mul, 0);

            Vector2 cp = vCentre - off;

            // Fix rounding error causing a missing tick.
            // In a perfect world, we could use tick RATE here, not distance.
            while (cp.X <= vCentre.X + off.X + 1.0f)
            {
                lineList.Add(new Line(GameBase.WindowRatioApply(cp), GameBase.WindowRatioApply(cp)));
                cp += pointDist;
            }

            GameBase.LineManager.Draw(lineList, hitObjectManager.HitObjectRadius * 0.2F * hitObjectManager.SpriteRatio,
                                      Color.White, 0, @"Standard",
                                      true);
            lineList.Clear();


            double first = AudioEngine.beatOffsetAt(AudioEngine.Time);

            // AllowMultiplier: FALSE, otherwise we compensate for slider multipliers TWICE and get the wrong value.
            double f = (AudioEngine.Time - first) / AudioEngine.beatLengthAt(AudioEngine.Time, false);
            f *= dist;
            f = f % (totalLength * 2);

            while (f < 0)
                f += totalLength;

            if (f >= totalLength)
                f = (totalLength * 2) - f;

            cPos += new Vector2((float)f, 0);

            lineList.Add(new Line(GameBase.WindowRatioApply(cPos), GameBase.WindowRatioApply(cPos)));
            GameBase.LineManager.Draw(lineList, hitObjectManager.HitObjectRadius * GameBase.WindowRatioInverse, c2, 0, @"Standard", true);
        }

        private void UpdatePendingChanges()
        {
            //After timing changes are complete, we finally do an update or vars.
            if (MouseManager.MouseLeft == ButtonState.Released)
            {
                changeInProgress = false;

                if (sliderUpdatePending)
                {
                    editor.Dirty = true;
                    hitObjectManager.UpdateVariables(false);
                    hitObjectManager.UpdateSliders(false, true);
                }

                if (offsetUpdatePending)
                {
                    editor.Dirty = true;

                    if (checkboxOffsetMove.Checked || timingWindowUpdatePending)
                    {
                        // Catch for a non-ineriting section being moved past an inheriting section,
                        // and ActiveTimingPointIndex becoming stale as a result. (Thanks Cyclone)
                        AudioEngine.UpdateActiveTimingPoint(true);

                        List<ControlPoint> timingPoints = AudioEngine.TimingPoints;
                        ControlPoint activePoint = currentActivePoint != null ? currentActivePoint : AudioEngine.ControlPoints[AudioEngine.ActiveTimingPointIndex];

                        int i = timingPoints.IndexOf(activePoint);

                        double start = activePoint.offset;
                        double end = (i < timingPoints.Count - 1 ? timingPoints[i + 1].offset : AudioEngine.AudioLength);

                        int diff = (int)(start - offsetUpdateOrig);

                        foreach (HitObject h in hitObjectManager.hitObjects)
                        {
                            if (h.StartTime < end && h.StartTime >= offsetUpdateOrig)
                                h.ModifyTime(h.StartTime + diff);
                        }
                    }
                }

                if (bpmUpdatePending)
                {
                    editor.Dirty = true;

                    bool autoMove = checkboxOffsetMove.Checked;

                    List<ControlPoint> timingPoints = AudioEngine.TimingPoints;
                    ControlPoint activePoint = currentActivePoint != null ? currentActivePoint : AudioEngine.ControlPoints[AudioEngine.ActiveTimingPointIndex];

                    int i = timingPoints.IndexOf(activePoint);

                    double start = activePoint.offset;
                    double end = (i < timingPoints.Count - 1 ? timingPoints[i + 1].offset : AudioEngine.AudioLength);

                    double change = activePoint.beatLength / bpmUpdateOrig;

                    foreach (HitObject h in hitObjectManager.hitObjects)
                    {
                        if (autoMove && h.StartTime < end && h.StartTime >= start)
                            h.ModifyTime(BeatSnapValue((h.StartTime - start) * change + start, 8, 0));
                    }
                }

                if (bpmUpdatePending || offsetUpdatePending)
                    hitObjectManager.Sort(true);

                currentActivePoint = null;
                bpmUpdatePending = false;
                offsetUpdatePending = false;
                sliderUpdatePending = false;
                timingWindowUpdatePending = false;

            }

        }

        internal override void Update()
        {
            UpdatePendingChanges();

            if (AudioEngine.BeatSyncing)
                UpdateMetronome();

            if (AudioEngine.ActiveTimingPointIndex != currTimingPoint) //This could be an event-delegate
            {
                currTimingPoint = AudioEngine.ActiveTimingPointIndex;
                GameBase.EditorControl.TimeSignatureChanged(AudioEngine.timeSignature);
            }
        }

        internal override void UpdateTimeline()
        {
            foreach (ControlPoint t in AudioEngine.ControlPoints)
            {
                if (t.offset < editor.TimelineLeftBound || t.offset > editor.TimelineRightBound) continue;

                float pos = editor.TimeToTimelinePosNoOffset(t.offset) - 4;
                byte alpha = (byte)Math.Min(255, 255 * ((double)Math.Max(0, Math.Min(pos - editor.TimelineX, editor.TimelineWidth - (pos - editor.TimelineX))) / 40));

                Color whiteColour = new Color(255, 255, 255, alpha);

                string text;
                if (t.timingChange)
                {
                    if (t.beatLength == 0)
                        text = @"-";
                    else
                        text = string.Format(@"{0:0}bpm", 1000 / t.beatLength * 60);
                }
                else
                    text = string.Format(@"{0}x", t.beatLength < 0 ? Math.Round(-100f / t.beatLength, 1) : 1);

                Vector2 backgroundPos = new Vector2(pos, editor.TimelineY + editor.TimelineHeight - 37);
                float backgroundDepth = SpriteManager.drawOrderBwd((int)t.offset);
                Color backgroundColour = t.timingChange ? whiteColour : new Color(180, 210, 254, alpha);
                Vector2 textPos = new Vector2(pos + 6, editor.TimelineY + editor.TimelineHeight - 27);
                float textDepth = SpriteManager.drawOrderBwd((int)t.offset - 2);

                List<pDrawable> tempInfos;
                if (!timingInfos.TryGetValue(t, out tempInfos))
                {
                    tempInfos = new List<pDrawable>();
                    tempInfos.Add(new pSprite(SkinManager.Load(@"editor-timeline-timinginfo"), Fields.TopLeft, Origins.TopLeft, Clocks.Game,
                                              backgroundPos, backgroundDepth, true, backgroundColour)
                    {
                        HandleInput = true
                    });

                    tempInfos.Add(new pText(text, 14, textPos, textDepth, true, Color.White)
                    {
                        TextBorder = true,
                        BorderColour = Color.Black
                    });

                    int thisOffset = (int)t.offset;
                    tempInfos[0].OnClick += delegate { AudioEngine.SeekTo(thisOffset); };
                    timingInfos.Add(t, tempInfos);
                }
                else
                {
                    //We already have textures,
                    //update any moving variables
                    tempInfos[0].Position = backgroundPos;
                    tempInfos[0].Depth = backgroundDepth;
                    tempInfos[0].Alpha = alpha / 255f;
                    tempInfos[0].InitialColour = backgroundColour;
                    tempInfos[1].Position = textPos;
                    tempInfos[1].Depth = textDepth;
                    ((pText)tempInfos[1]).Text = text;
                }

                editor.timelineManager.Add(tempInfos[0]);
                editor.timelineManager.Add(tempInfos[1]);
            }
        }

        internal override void DrawTimeline()
        {
        }


        private bool LastModeWasTiming = false;
        private SpriteManager spriteManagerCentre;
        internal override void Enter()
        {
            spriteManagerCentre.Alpha = 1;

            GameBase.ResizeGamefield(1, false);
            editor.SelectNone();
            editor.TimelineTickHeight = 6;
            if (!LastModeWasTiming)
                editor.TimelineMagnification *= 0.4F;

            LastModeWasTiming = true;
            base.Enter();
        }

        internal override void Leave()
        {
            UpdatePendingChanges();
            spriteManagerCentre.Alpha = 0;

            editor.TimelineMagnification /= 0.4F;
            LastModeWasTiming = false;
            currentActivePoint = null;

            InputManager.DragFinish();
            base.Leave();
        }

        internal override void Dispose()
        {
            if (spriteManagerCentre != null) spriteManagerCentre.Dispose();
            foreach (KeyValuePair<ControlPoint, List<pDrawable>> kvp in timingInfos)
            {
                for (int i = 0; i < kvp.Value.Count; i++)
                    kvp.Value[i].Dispose();
            }
            base.Dispose();
        }

        internal override void Draw()
        {
            byte byte1R = 0, byte1G = 0, byte1B = 0, byte2 = 0, byte3 = 0, byte4 = 0;
            bool activePointSet = currentActivePoint != null;
            bool beatSyncing = (!activePointSet && AudioEngine.BeatSyncing) || (activePointSet && currentActivePoint.beatLength > 0);

            if (beatSyncing)
            {
                beatBpmDisplay.Text = String.Format(KeyboardHandler.ControlPressed ? @"{0:0.000}" : @"{0:0.00}",
                    60000 / (activePointSet ? currentActivePoint.beatLength : AudioEngine.beatLength));
                beatOffsetDisplay.Text = String.Format(@"{0:#,0}", (activePointSet ? currentActivePoint.offset : AudioEngine.CurrentOffset));
                sliderVelocityDisplay.Text =
                    String.Format(@"{0:N2}", BeatmapManager.Current.DifficultySliderMultiplier);
                sliderTickDisplay.Text = String.Format(@"{0}", BeatmapManager.Current.DifficultySliderTickRate);
                beatDisplay.Text = String.Format(@"{0}:{1}", currStanza, currBeat);

                if (AudioEngine.Time >= AudioEngine.CurrentOffset)
                {
                    double amt = ((AudioEngine.Time - AudioEngine.CurrentOffset) %
                                  (AudioEngine.beatLength * (int)AudioEngine.timeSignature)) /
                                 AudioEngine.beatLength;

                    if (amt < 1)
                    {
                        byte1R = (byte)(187 * (1 - amt)); // flashy square #1 is reserved for the downbeat.
                        byte1G = (byte)(255 * (1 - amt));
                        byte1B = (byte)(51 * (1 - amt));
                    }

                    else
                    {
                        // for very long time signatures, we repeat 2, 3, and 4 in a loop.
                        amt %= 3.0d;

                        if (amt < 1)
                            byte4 = (byte)(255 * (1 - amt));
                        else if (amt < 2)
                            byte2 = (byte)(255 * (2 - amt));
                        else if (amt < 3)
                            byte3 = (byte)(255 * (3 - amt));
                    }
                }
            }
            else
            {
                beatDisplay.Text = LocalisationManager.GetString(OsuString.EditorModeTiming_TimingSectionNotTimed);
                beatBpmDisplay.Text = @"-";
                beatOffsetDisplay.Text = @"-";
            }

            beatSquares[0].InitialColour = new Color(byte1R, byte1G, byte1B, 0);
            beatSquares[1].InitialColour = new Color(byte2, byte2, byte2, 0);
            beatSquares[2].InitialColour = new Color(byte3, byte3, byte3, 0);
            beatSquares[3].InitialColour = new Color(byte4, byte4, byte4, 0);

            spriteManagerCentre.Draw();

            base.Draw();

            DrawMetronomeSlider();
        }

        private void UpdateMetronome()
        {
            //Metronome sounds in timing mode
            currBeat = (int)((AudioEngine.Time - AudioEngine.CurrentOffset) / AudioEngine.beatLength) %
                       (int)AudioEngine.timeSignature;
            currBeatTotal = (int)Math.Floor((AudioEngine.Time - AudioEngine.CurrentOffset) / AudioEngine.beatLength);
            currStanza = (int)((AudioEngine.Time - AudioEngine.CurrentOffset) / AudioEngine.beatLength) /
                         ((int)AudioEngine.timeSignature);

            int currBeat2x = (int)Math.Floor(2.0f * (AudioEngine.Time - AudioEngine.CurrentOffset) / AudioEngine.beatLength);
            int currBeat3x = (int)Math.Floor(3.0f * (AudioEngine.Time - AudioEngine.CurrentOffset) / AudioEngine.beatLength);

            bool extraBleep = ((editor.beatSnapDivisor == 3) || (editor.beatSnapDivisor == 6))
                              ? (currBeat3x > lastMetronomeBeat3x)
                              : (currBeat2x > lastMetronomeBeat2x);

            if (AudioEngine.Time - AudioEngine.CurrentOffset < 0)
            {
                currBeat = 3 + currBeat;
                currStanza -= 1;
            }

            if ((lastMetronomeBeat == currBeatTotal - 1 || lastMetronomeBeat > currBeatTotal) &&
                AudioEngine.AudioState == AudioStates.Playing && MetronomeEnabled)
            {
                if (metronomeDisableCount > 0)
                    metronomeDisableCount--;
                else if (AudioEngine.timeSignature == TimeSignatures.SimpleQuadruple)
                {
                    switch (currBeat)
                    {
                        case 0:
                            s_metronomeHigh.Play(true, true);
                            break;
                        case 1:
                        case 3:
                            s_metronomeLow.Play(true, true);
                            s_metronomeLow.Volume = 0.8f;
                            break;
                        case 2:
                            s_metronomeLow.Play(true, true);
                            s_metronomeLow.Volume = 0.6f;
                            break;
                    }
                }
                else// if (AudioEngine.timeSignature == TimeSignatures.SimpleTriple)
                {
                    switch (currBeat)
                    {
                        case 0:
                            s_metronomeHigh.Play(true, true);
                            break;
                        default:
                            s_metronomeLow.Play(true, true);
                            s_metronomeLow.Volume = 0.8f;
                            break;
                    }
                }
            }
            // 2x metronome bleeps when Ctrl is held down.
            else if ((extraBleep) && (AudioEngine.AudioState == AudioStates.Playing) && MetronomeEnabled && KeyboardHandler.ControlPressed)
            {
                s_metronomeLow.Play(true, true);
                s_metronomeLow.Volume = 0.8f;
            }

            lastMetronomeBeat = currBeatTotal;
            lastMetronomeBeat2x = currBeat2x;
            lastMetronomeBeat3x = currBeat3x;
        }

        internal void ShowTimingWindow()
        {
            ShowTimingWindow(TimingFocus.None);
        }

        internal void ShowTimingWindow(TimingFocus tf)
        {
            ShowTimingWindow(tf, true);
        }

        internal void ShowTimingWindow(TimingFocus tf, bool AllowInherit)
        {
            UpdatePendingChanges();
            editor.UndoPush(true);

            GameBase.MenuActive = true;

            offsetUpdateOrig = AudioEngine.CurrentOffset;
            bpmUpdateOrig = AudioEngine.beatLength;

            TimingEntry t = new TimingEntry(editor, tf, AllowInherit, editor.CurrentMode == EditorModes.Timing);
            t.ShowDialog(GameBase.Form);

            GameBase.MenuActive = false;

            AudioEngine.UpdateActiveTimingPoint(true);

            hitObjectManager.UpdateSlidersAll(false);

            if (editor.CurrentMode != EditorModes.Timing)
                Update(); //We might be calling this from another mode, so force a single update.
        }

        private void offsetLeft_OnClick(object sender, EventArgs e)
        {
            if (!changeInProgress && AudioEngine.BeatSyncing)
            {
                changeInProgress = true;
                currentActivePoint = AudioEngine.ControlPoints[AudioEngine.ActiveTimingPointIndex];
                editor.UndoPush();
            }

            offsetDecrease();
            spriteManagerCentre.ResetClick();
        }

        private void offsetRight_OnClick(object sender, EventArgs e)
        {
            if (!changeInProgress && AudioEngine.BeatSyncing)
            {
                changeInProgress = true;
                currentActivePoint = AudioEngine.ControlPoints[AudioEngine.ActiveTimingPointIndex];
                editor.UndoPush();
            }

            offsetIncrease();
            spriteManagerCentre.ResetClick();
        }

        private void bpmDown_OnClick(object sender, EventArgs e)
        {
            if (!changeInProgress && AudioEngine.BeatSyncing)
            {
                changeInProgress = true;
                currentActivePoint = AudioEngine.ControlPoints[AudioEngine.ActiveTimingPointIndex];
                editor.UndoPush();
            }

            bpmDecrease();
            spriteManagerCentre.ResetClick();
        }

        private void bpmUp_OnClick(object sender, EventArgs e)
        {
            if (!changeInProgress && AudioEngine.BeatSyncing)
            {
                changeInProgress = true;
                currentActivePoint = AudioEngine.ControlPoints[AudioEngine.ActiveTimingPointIndex];
                editor.UndoPush();
            }

            bpmIncrease();
            spriteManagerCentre.ResetClick();
        }

        internal void TimingResetAll()
        {
            if (AudioEngine.ControlPoints.Count == 0) return;

            editor.UndoPush();
            AudioEngine.ControlPoints.Clear();

            lastBeat = 0;
        }

        private void bpmIncrease()
        {
            bool activePointSet = currentActivePoint != null;
            bool beatSyncing = (!activePointSet && AudioEngine.BeatSyncing) || (activePointSet && currentActivePoint.beatLength > 0);

            if (beatSyncing)
            {
                if (!bpmUpdatePending)
                {
                    bpmUpdateOrig = activePointSet ? currentActivePoint.beatLength : AudioEngine.beatLength;
                    bpmUpdatePending = true;
                }

                ControlPoint activePoint = currentActivePoint != null ? currentActivePoint : AudioEngine.ControlPoints[AudioEngine.ActiveTimingPointIndex];

                if (KeyboardHandler.ShiftPressed)
                    activePoint.beatLength -= 1;
                else if (KeyboardHandler.ControlPressed)
                    activePoint.beatLength -= 0.01;
                else
                    activePoint.beatLength -= 0.1;
            }
        }

        private void bpmDecrease()
        {
            bool activePointSet = currentActivePoint != null;
            bool beatSyncing = (!activePointSet && AudioEngine.BeatSyncing) || (activePointSet && currentActivePoint.beatLength > 0);

            if (beatSyncing)
            {
                if (!bpmUpdatePending)
                {
                    bpmUpdateOrig = activePointSet ? currentActivePoint.beatLength : AudioEngine.beatLength;
                    bpmUpdatePending = true;
                }

                ControlPoint activePoint = currentActivePoint != null ? currentActivePoint : AudioEngine.ControlPoints[AudioEngine.ActiveTimingPointIndex];

                if (KeyboardHandler.ShiftPressed)
                    activePoint.beatLength += 1;
                else if (KeyboardHandler.ControlPressed)
                    activePoint.beatLength += 0.01;
                else
                    activePoint.beatLength += 0.1;
            }
        }

        private void offsetIncrease()
        {
            bool activePointSet = currentActivePoint != null;
            bool beatSyncing = (!activePointSet && AudioEngine.BeatSyncing) || (activePointSet && currentActivePoint.beatLength > 0);

            if (beatSyncing)
            {
                if (!offsetUpdatePending)
                {
                    offsetUpdateOrig = activePointSet ? currentActivePoint.offset : AudioEngine.CurrentOffset;
                    offsetUpdatePending = true;
                }

                if (activePointSet)
                {
                    if (KeyboardHandler.ShiftPressed)
                        currentActivePoint.offset += 10;
                    else if (KeyboardHandler.ControlPressed)
                        currentActivePoint.offset += 1;
                    else
                        currentActivePoint.offset += 2;
                }
                else
                {
                    if (KeyboardHandler.ShiftPressed)
                        AudioEngine.CurrentOffset += 10;
                    else if (KeyboardHandler.ControlPressed)
                        AudioEngine.CurrentOffset += 1;
                    else
                        AudioEngine.CurrentOffset += 2;
                }
            }
        }

        private void offsetDecrease()
        {
            bool activePointSet = currentActivePoint != null;
            bool beatSyncing = (!activePointSet && AudioEngine.BeatSyncing) || (activePointSet && currentActivePoint.beatLength > 0);

            if (beatSyncing)
            {
                if (!offsetUpdatePending)
                {
                    offsetUpdateOrig = activePointSet ? currentActivePoint.offset : AudioEngine.CurrentOffset;
                    offsetUpdatePending = true;
                }

                if (activePointSet)
                {
                    if (KeyboardHandler.ShiftPressed)
                        currentActivePoint.offset -= 10;
                    else if (KeyboardHandler.ControlPressed)
                        currentActivePoint.offset -= 1;
                    else
                        currentActivePoint.offset -= 2;
                }
                else
                {
                    if (KeyboardHandler.ShiftPressed)
                        AudioEngine.CurrentOffset -= 10;
                    else if (KeyboardHandler.ControlPressed)
                        AudioEngine.CurrentOffset -= 1;
                    else
                        AudioEngine.CurrentOffset -= 2;
                }
            }
        }

        private void resetButton_OnClick(object sender, EventArgs e)
        {
            TimingReset();
        }

        private void sliderTickRight_OnClick(object sender, EventArgs e)
        {
            if (BeatmapManager.Current.DifficultySliderTickRate < 4)
            {
                if (!changeInProgress)
                {
                    changeInProgress = true;
                }

                BeatmapManager.Current.DifficultySliderTickRate = (int)BeatmapManager.Current.DifficultySliderTickRate + 1;
                sliderUpdatePending = true;
            }
        }

        private void sliderTickLeft_OnClick(object sender, EventArgs e)
        {
            if (BeatmapManager.Current.DifficultySliderTickRate > 1)
            {
                if (!changeInProgress)
                {
                    changeInProgress = true;
                }

                BeatmapManager.Current.DifficultySliderTickRate = (int)BeatmapManager.Current.DifficultySliderTickRate - 1;
                sliderUpdatePending = true;
            }
        }

        private void sliderVelocityRight_OnClick(object sender, EventArgs e)
        {
            if (!changeInProgress)
            {
                changeInProgress = true;
            }

            if (KeyboardHandler.ControlPressed) // Hi DiamondCrash
            {
                BeatmapManager.Current.DifficultySliderMultiplier =
                    Math.Min(SLIDER_SPEED_MAX, BeatmapManager.Current.DifficultySliderMultiplier + 0.01);
            }
            else
            {
                BeatmapManager.Current.DifficultySliderMultiplier =
                    Math.Min(SLIDER_SPEED_MAX, BeatmapManager.Current.DifficultySliderMultiplier + 0.1);
            }

            sliderUpdatePending = true;
            spriteManagerCentre.ResetClick();
        }

        private void sliderVelocityLeft_OnClick(object sender, EventArgs e)
        {
            if (!changeInProgress)
            {
                changeInProgress = true;
            }

            if (KeyboardHandler.ControlPressed) // Hi DiamondCrash
            {
                BeatmapManager.Current.DifficultySliderMultiplier =
                    Math.Max(SLIDER_SPEED_MIN, BeatmapManager.Current.DifficultySliderMultiplier - 0.01);
            }
            else
            {
                BeatmapManager.Current.DifficultySliderMultiplier =
                    Math.Max(SLIDER_SPEED_MIN, BeatmapManager.Current.DifficultySliderMultiplier - 0.1);
            }

            sliderUpdatePending = true;
            spriteManagerCentre.ResetClick();
        }

        private void tapButton_OnClick(object sender, EventArgs e)
        {
            TimingTap();
        }

        internal override bool OnKey(Keys k, bool first)
        {
            if (k == Keys.M && KeyboardHandler.ControlPressed)
            {
                beatSnapSong();
                return true;
            }

            if (k == Keys.L && !KeyboardHandler.ControlPressed)
            {
                beatShiftCircle(0);
                return true;
            }

            if (k == Keys.T && first)
            {
                TimingTap();
                return true;
            }

            if (k == Keys.R)
            {
                TimingReset();
                return true;
            }

            return base.OnKey(k, first);
        }

        internal void ChangeTimeSignature(TimeSignatures ts)
        {
            if (AudioEngine.ControlPoints.Count == 0)
                return;

            AudioEngine.timeSignature = ts;
            GameBase.EditorControl.TimeSignatureChanged(ts);
            editor.Timing.FixInheritSignatures();
        }

        internal void RecalculateSliderLengths()
        {
            editor.UndoPush();

            foreach (HitObject h in hitObjectManager.hitObjects)
            {
                SliderOsu s = h as SliderOsu;

                if (s != null)
                {
                    s.SpatialLength = 0;
                    s.UpdateCalculations();
                }
            }
        }

        internal void TimingAdd(bool timingChange)
        {
            if (AudioEngine.ActiveTimingPointIndex >= 0 && AudioEngine.beatLength > 0)
            {

                if (!timingChange && AudioEngine.Time < AudioEngine.ControlPoints[0].offset) return;

                double currBeatLength = timingChange ? AudioEngine.beatLength : AudioEngine.ActiveControlPoint.beatLength;

                ControlPoint tp = new ControlPoint(AudioEngine.Time, currBeatLength);
                tp.timingChange = timingChange;

                if (AudioEngine.ActiveInheritedTimingPointIndex >= 0)
                {
                    ControlPoint active = AudioEngine.ActiveControlPoint;
                    tp.volume = active.volume;
                    tp.timeSignature = active.timeSignature;
                    tp.kiaiMode = AudioEngine.KiaiEnabled;
                }

                if (!editor.ignoreTimingUpdates)
                    editor.UndoPush();
                editor.Dirty = true;

                AudioEngine.ControlPoints.Add(tp);
            }

            lastBeat = 0;
            AudioEngine.UpdateActiveTimingPoint(true);

            if (!timingChange)
                ShowTimingWindow(TimingFocus.Audio);
        }

        internal void TimingDelete()
        {
            if (AudioEngine.beatLength <= 0 || AudioEngine.ActiveInheritedTimingPointIndex < 0)
                return;

#if !DEBUG
              if (AudioEngine.TimingPoints.Count == 1 && AudioEngine.ActiveInheritedTimingPointIndex == AudioEngine.ActiveTimingPointIndex)
                  return;
#endif

            if (!editor.ignoreTimingUpdates)
                editor.UndoPush();

            AudioEngine.ControlPoints.RemoveAt(AudioEngine.ActiveInheritedTimingPointIndex);

            lastBeat = 0;
            AudioEngine.ResetControlPoints();
            AudioEngine.UpdateActiveTimingPoint(true);
        }

        internal void FixInheritSignatures()
        {
            AudioEngine.TimingPoints.Sort();

            ControlPoint tp = null;

            for (int i = AudioEngine.ActiveTimingPointIndex; i <= AudioEngine.ActiveInheritedTimingPointIndex; i++)
            {
                tp = AudioEngine.ControlPoints[i];
                tp.timeSignature = AudioEngine.timeSignature;
            }
        }
    }

    internal enum TimingFocus
    {
        None,
        BPM,
        Offset,
        Audio
    }
}