﻿using osu.GameModes.Edit.AiMod.Rulesets.Fruits;
using osu.GameModes.Edit.AiMod.Rulesets.Mania;
using osu.GameModes.Edit.AiMod.Rulesets.Osu;
using osu.GameModes.Edit.AiMod.Rulesets.Taiko;
using osu.GameplayElements.Beatmaps;
using osu_common;
using System;
using System.Collections.Generic;
using System.Text;

namespace osu.GameModes.Edit.AiMod.Rulesets
{
    internal static class AiFactory
    {
        internal static AiModRuleset AICompose
        {
            get
            {
                switch (BeatmapManager.Current.PlayMode)
                {
                    case PlayModes.Taiko:
                        return new AiComposeTaiko();
                    case PlayModes.CatchTheBeat:
                        return new AiComposeFruits();
                    case PlayModes.OsuMania:
                        return new AiComposeMania();
                    default:
                        return new AiCompose();
                }
            }
        }

        internal static AiModRuleset AITiming
        {
            get
            {
                switch (BeatmapManager.Current.PlayMode)
                {
                    case PlayModes.Taiko:
                        return new AiTimingTaiko();
                    case PlayModes.OsuMania:
                        return new AiTimingMania();
                    default:
                        return new AiTiming();
                }
            }
        }

        internal static AiModRuleset AIMeta
        {
            get
            {
                switch (BeatmapManager.Current.PlayMode)
                {
                    case PlayModes.Taiko:
                        return new AiMetaTaiko();
                    case PlayModes.OsuMania:
                        return new AiMetaMania();
                    case PlayModes.CatchTheBeat:
                        return new AiMetaFruits();
                    default:
                        return new AiMeta();
                }
            }
        }

        internal static AiModRuleset AIDesign
        {
            get
            {
                switch (BeatmapManager.Current.PlayMode)
                {
                    //   case PlayModes.OsuMania:
                    //       return new AISnappingMania();
                    default:
                        return new AiDesign();
                }
            }
        }

        internal static AiModRuleset AIMapset
        {
            get
            {
                return new AiMapset();
            }
        }
    }
}
