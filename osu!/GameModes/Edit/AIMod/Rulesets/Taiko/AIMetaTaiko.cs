﻿using System.Collections.Generic;
using osu.Audio;
using osu_common.Helpers;
using osu.GameModes.Edit.AiMod.Reports;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.GameModes.Edit.AiMod.Rulesets.Osu;

namespace osu.GameModes.Edit.AiMod.Rulesets.Taiko
{
    internal class AiMetaTaiko : AiMeta
    {
        public AiMetaTaiko()
        {
        }

        protected override void RunAllRules(HitObjectManagerBase hitObjectManager)
        {

            Beatmap map = BeatmapManager.Current;
            if (map.LetterboxInBreaks)
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIMetaTaiko_NoLetterboxTaiko)));
            if (map.EpilepsyWarning)
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIMetaTaiko_NoEpilepsyTaiko)));
            if (map.Countdown != Countdown.Disabled)
            {
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIMetaTaiko_NoCountdownTaiko)));
            }
            if (map.DifficultySliderMultiplier != 1.4 && map.DifficultySliderMultiplier != 1.6)
                Reports.Add(new AiReport(Severity.Info, LocalisationManager.GetString(OsuString.AIMetaTaiko_BadSvTaiko)));
            runMetaCheck(false);
        }
    }
}