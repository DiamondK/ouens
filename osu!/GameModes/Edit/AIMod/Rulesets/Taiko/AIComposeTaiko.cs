﻿using osu.GameplayElements;
using System;
using System.Collections.Generic;
using System.Text;
using osu.GameModes.Edit.AiMod.Rulesets.Osu;

namespace osu.GameModes.Edit.AiMod.Rulesets.Taiko
{
    internal class AiComposeTaiko : AiCompose
    {
        public override AiModType Type { get { return AiModType.Compose; } }
        public AiComposeTaiko()
        {
        }

        protected override void RunAllRules(HitObjectManagerBase hitObjectManager)
        {
            RunSanityRules(hitObjectManager);
        }
    }
}
