using osu.Audio;
using osu_common.Helpers;
using osu.GameModes.Edit.AiMod.Rulesets.Osu;
using osu.GameplayElements.Events;
using osu.GameplayElements;
using System;
using System.Collections.Generic;
using System.Text;

namespace osu.GameModes.Edit.AiMod.Rulesets.Taiko
{
    internal class AiTimingTaiko:AiTiming
    {
        internal AiTimingTaiko() { }

        protected override void RunAllRules(HitObjectManagerBase hitObjectManager)
        {
            EventManager manager = Editor.Instance.eventManager;
            
            List<ControlPoint> points = AudioEngine.ControlPoints.FindAll(t => t.kiaiMode == true);
            for (int i = 0; i < points.Count; i++)
            {
                if (i < points.Count - 1 && (points[i + 1].offset - points[i].offset) < AudioEngine.beatLengthAt(points[i].offset,false) * 4)
                    Reports.Add(new AiReport(Severity.Error, LocalisationManager.GetString(OsuString.AITimingTaiko_KiaiToggledTooOften)));
            }

            base.RunAllRules(hitObjectManager);
        }
    }
}
