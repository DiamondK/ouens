﻿using System.Collections.Generic;
using osu.Audio;
using osu_common.Helpers;
using osu.GameModes.Edit.AiMod.Reports;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using Un4seen.Bass.AddOn.Tags;
using osu.GameplayElements.Events;

namespace osu.GameModes.Edit.AiMod.Rulesets.Osu
{
    internal class AiTiming : AiModRuleset
    {
        public override AiModType Type { get { return AiModType.Timing; } }

        public AiTiming()
        {
        }

        protected override void RunAllRules(HitObjectManagerBase hitObjectManager)
        {
            List<HitObjectBase> hitObjects = hitObjectManager.GetHitObjects();

            int mapEndTime = 0;
            int drainLength = 0;

            if (hitObjects.Count > 0)
            {
                mapEndTime = hitObjects[hitObjects.Count - 1].EndTime;
                drainLength = mapEndTime - hitObjects[0].StartTime;

                if (drainLength > 6 * 60 * 1000)
                    Reports.Add(new AiReport(-1, Severity.Warning, LocalisationManager.GetString(OsuString.AITiming_BeatmapTooLong), -1, null));
                else if (drainLength < 45000)
                    Reports.Add(new AiReport(-1, Severity.Warning, LocalisationManager.GetString(OsuString.AITiming_BeatmapTooShort), -1, null));
                if (drainLength * 1.0 / (AudioEngine.AudioLength - hitObjects[0].StartTime) < 0.8)
                    Reports.Add(new AiReport(-1, Severity.Warning, LocalisationManager.GetString(OsuString.AITiming_Mp3LongerThanMap), -1, null));
            }

            List<ControlPoint> points = AudioEngine.ControlPoints;

            int kiaiToggleCount = 0;
            int kiaiLength = 0;
            List<float> SliderMultipliers = new List<float>();
            bool kiaiOn = false;
            double lastKiaiEndTime = 0;
            int lowVolumeCount = 0;
            int stackedPointCount = 0;

            for (int i = 0; i < points.Count; i++)
            {
                ControlPoint p = points[i];

                //overlap
                int end = i == points.Count - 1 ? mapEndTime : (int)points[i + 1].offset;
                int start = (int)p.offset;

                if (end - start > 0)
                {
                    if (p.kiaiMode)
                    {
                        kiaiLength += (end - start);
                        if (i > 0 && !points[i - 1].kiaiMode)
                            kiaiToggleCount++;
                    }
                }
                if (i > 0 && p.offset == points[i - 1].offset)
                {
                    stackedPointCount++;
                    for (int z = 0; z < stackedPointCount; z++)
                    {
                        if (points[i - z].timingChange && points[i].timingChange)
                        {
                            Reports.Add(new AiReport((int)p.offset, Severity.Warning, LocalisationManager.GetString(OsuString.AITiming_OverlappingTimingPoints), 0, null));
                            break;
                        }
                    }
                }
                else
                    stackedPointCount = 0;

                //snap
                if (p.kiaiMode && !kiaiOn)
                {
                    kiaiOn = true;
                    if (AiModWindow.TestBeatSnap((int)p.offset, false))
                        Reports.Add(new AiReport((int)p.offset, Severity.Warning, LocalisationManager.GetString(OsuString.AITiming_UnsnappedKiai), 0, null));
                    if (p.offset - lastKiaiEndTime < 15000 && p.offset - lastKiaiEndTime > 10)
                        Reports.Add(new AiReport((int)p.offset, Severity.Warning, LocalisationManager.GetString(OsuString.AITiming_KiaiTooShort), -1, null));
                }
                else if (!p.kiaiMode && kiaiOn)
                {
                    kiaiOn = false;
                    if (AiModWindow.TestBeatSnap((int)p.offset, false))
                        Reports.Add(new AiReport((int)p.offset, Severity.Warning, LocalisationManager.GetString(OsuString.AITiming_UnsnappedKiaiEnd), 0, null));
                    lastKiaiEndTime = p.offset;
                }

                //volume
                if (p.volume < 5)
                    lowVolumeCount++;
            }

            //zero volume
            if (lowVolumeCount == points.Count)
                Reports.Add(new AiReport(Severity.Error, LocalisationManager.GetString(OsuString.AITiming_AllTimingSectionsQuiet)));
            else if (lowVolumeCount > 0)
                Reports.Add(new AiReport(Severity.Warning, string.Format(LocalisationManager.GetString(OsuString.AITiming_SomeTimingSectionsQuiet), lowVolumeCount, points.Count)));

            //KIAI
            if (drainLength > 3 * 60 * 1000 && kiaiLength > drainLength / 3)
                Reports.Add(new AiReport(-1, Severity.Warning, LocalisationManager.GetString(OsuString.AITiming_TooMuchKiai), -1, null));
            else if (drainLength < 90 * 1000 && kiaiLength > drainLength / 2)
                Reports.Add(new AiReport(-1, Severity.Warning, LocalisationManager.GetString(OsuString.AITiming_TooMuchKiaiTvSize), -1, null));

            if (BeatmapManager.Current.PreviewTime < 0)
                Reports.Add(new AiReport(-1, Severity.Warning, LocalisationManager.GetString(OsuString.AITiming_NoPreviewPoint), -1, null));
            if (kiaiOn)
                Reports.Add(new AiReport(-1, Severity.Warning, LocalisationManager.GetString(OsuString.AITiming_NoKiaiEnd), -1, null));

            //audio quality
            TAG_INFO info = AudioEngine.GetAudioTagInfo();
            if (info != null)
            {
                if (info.bitrate < 128)
                    Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AITiming_LowAudioBitrate)));
                else if (info.bitrate > 192)
                    Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AITiming_HighAudioBitrate)));
            }
        }
    }
}
