using System.Collections.Generic;
using osu.Audio;
using osu.GameModes.Edit.AiMod.Reports;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu_common;
using osu_common.Helpers;
using osu.GameplayElements.Events;
using System.IO;
using System.Text.RegularExpressions;
using osu.Graphics.Sprites;
using osu.Graphics;
using osu.Graphics.Skinning;

namespace osu.GameModes.Edit.AiMod.Rulesets.Osu
{
    internal class AiDesign : AiModRuleset
    {
        public override AiModType Type { get { return AiModType.Design; } }

        public AiDesign()
        {
        }

        protected override void RunAllRules(HitObjectManagerBase hitObjectManager)
        {
            EventManager manager = Editor.Instance.eventManager;
            if (manager.backgroundEvent == null)
                Reports.Add(new AiReport(-1, Severity.Error, LocalisationManager.GetString(OsuString.AIDesign_NoBackgroundImage), -1, delegate { return Editor.Instance.eventManager.backgroundEvent != null; }));
            else if (manager.backgroundEvent.Sprite != null)
            {
                int w = manager.backgroundEvent.Sprite.Width;
                int h = manager.backgroundEvent.Sprite.Height;
                if (w > 1366 || h > 768)
                    Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIDesign_BackgroundTooLarge)));
                /*if (w * 1.0 / h == 16/9 && (w > 1366 || h > 768))
                    Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIDesign_BackgroundTooLarge)));
                else if (w * 1.0 / h == 16 / 10 && (w > 1366 || h > 850))
                    Reports.Add(new AiReport(Severity.Warning, @"Background image is larger than 1360*850"));
                else if (w> 1024 || h> 768)
                    Reports.Add(new AiReport(Severity.Warning, @"Background image is larger than 1024*768"));*/
            }

            if (manager.videoSprites.Count > 0)
            {
                foreach (pVideo p in manager.videoSprites)
                {
                    if (p.Width > 1280 && p.Height > 720)
                        Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIDesign_VideoDimensionsTooLargeWidescreen)));
                    else if (p.Width > 1024 && p.Height > 768)
                        Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIDesign_VideoDimensionsTooLarge)));
                }
            }
            if (!BeatmapManager.Current.EpilepsyWarning && manager.events.FindAll(e => e.EndTime - e.StartTime < 100).Count > 5)
            {
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIDesign_EpilepsyWarning)));
            }
            checkImageElements();
        }

        private void checkImageElements()
        {
            string[] files = Directory.GetFiles(BeatmapManager.Current.ContainingFolder, @"*", SearchOption.AllDirectories);

            Dictionary<string, bool> fileDict = new Dictionary<string, bool>(files.Length);
            foreach (string file in files)
            {
                switch (Path.GetExtension(file))
                {
                    case @".png":
                    case @".jpg":
                    case @".jpeg":
                        break;
                    default:
                        continue;
                }

                string strippedFilename = Path.GetFileNameWithoutExtension(file).ToLower();

                fileDict[strippedFilename] = true;

                int scale = 1;

                string name = strippedFilename;
                if (name.IndexOf(@"@2x") >= 0)
                {
                    scale = 2;
                    name = name.Substring(0, name.IndexOf('@'));
                }

                switch (name)
                {
                    case @"hitcircle":
                    case @"hitcircleoverlay":
                    case @"approachcircle":
                    case @"reversearrow":
                        pTexture p = SkinManager.Load(strippedFilename, SkinSource.Beatmap);
                        if (p != null && (p.Width != 128 * scale || p.Height != 128 * scale))
                            addSkinReport(strippedFilename, 128 * scale);
                        break;
                }
            }

            //sprite events check
            EventManager manager = Editor.Instance.eventManager;

            for (int i = 0; i < manager.storyLayerSprites.Length; i++)
            {
                for (int j = 0; j < manager.storyLayerSprites[i].Count; j++)
                {
                    Event e = manager.storyLayerSprites[i][j];
                    string name = e.Filename.ToLower();
                    if (!fileDict.ContainsKey(name))
                    {
                        if (BeatmapManager.Current.UseSkinSpritesInSB)
                        {
                            string tmpName = name.Substring(0, name.LastIndexOf('.')).ToLower();
                            pTexture p = SkinManager.Load(tmpName);
                            if (p != null)
                            {
                                continue;
                            }
                        }
                        else // check for sprite animation
                        {
                            int animationSuffix = name.LastIndexOf('.');
                            if (animationSuffix >= 0 &&
                                fileDict.ContainsKey(name.Insert(animationSuffix, "0")) && fileDict.ContainsKey(name.Insert(animationSuffix, "1")))
                            {
                                continue; // at least two frames of sprite animation detected, don't display missing file warning
                            }
                        }
                        fileDict[name] = true; // add fake file,ensure file missing just report once.
                        Reports.Add(new AiReport(Severity.Error, LocalisationManager.GetString(OsuString.AIDesign_FileMissing) + name));
                    }
                }
            }
        }

        private void addSkinReport(string name, int size)
        {
            Reports.Add(new AiReport(Severity.Error, string.Format(LocalisationManager.GetString(OsuString.AIDesign_IncorrectDimensions), name, size)));
        }

    }
}
