﻿using System.Collections.Generic;
using osu.Audio;
using osu.GameModes.Edit.AiMod.Reports;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu_common;
using osu_common.Helpers;

namespace osu.GameModes.Edit.AiMod.Rulesets.Osu
{
    internal class AiMeta : AiModRuleset
    {
        public override AiModType Type { get { return AiModType.Meta; } }

        public AiMeta()
        {
        }

        protected override void RunAllRules(HitObjectManagerBase hitObjectManager)
        {
            runMetaCheck();
            runDifficultyCheck();
        }

        private bool asciiOnly(string str)
        {
            foreach (char c in str)
            {
                if (c > 256)
                    return false;
            }
            return true;
        }

        protected void runMetaCheck(bool checkStacking = true)
        {
            //check unicode meta
            Beatmap map = BeatmapManager.Current;
            if (!asciiOnly(map.Artist))
                Reports.Add(new AiReport(Severity.Error, LocalisationManager.GetString(OsuString.AIMeta_RomanizedArtistUnicode)));
            if (!asciiOnly(map.Title))
                Reports.Add(new AiReport(Severity.Error, LocalisationManager.GetString(OsuString.AIMeta_RomanizedTitleUnicode)));
            if (checkStacking && (map.StackLeniency < 0.3F || map.StackLeniency > 0.9F))
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIMeta_IncorrectStackLeniency)));
        }

        private void runDifficultyCheck()
        {
        //    BeatmapStarCalculator bsc = new BeatmapStarCalculator(BeatmapManager.Current, true);
        //    Difficulty = bsc.GetStars();
        //    Reports.AddRange(bsc.Reports);
            double star = BeatmapManager.Current.DifficultyEyupStars;
            if (star < 2)
                Difficulty = BeatmapDifficulty.Easy;
            else if (star < 3.5)
                Difficulty = BeatmapDifficulty.Normal;
            else if (star < 4.5)
                Difficulty = BeatmapDifficulty.Hard;
            else if (star < 5)
                Difficulty = BeatmapDifficulty.Insane;
            else
                Difficulty = BeatmapDifficulty.Expert;
        }

        public BeatmapDifficulty Difficulty;
    }
}
