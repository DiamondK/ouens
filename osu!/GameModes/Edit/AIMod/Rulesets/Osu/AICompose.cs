using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using osu.GameModes.Edit.AiMod.Reports;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.Beatmaps;
using osu_common;
using osu_common.Helpers;
using osu.GameplayElements;
using osu.GameplayElements.HitObjects.Osu;
using osu.Audio;

namespace osu.GameModes.Edit.AiMod.Rulesets.Osu
{
    internal class AiCompose : AiModRuleset
    {
        public override AiModType Type { get { return AiModType.Compose; } }

        private const float ERROR_FACTOR = 0.05f;
        private const float MIN_SPACING_FACTOR = 0.01f;
        private float spacingFactor = 1;
        float circleRadius = 40;

        public AiCompose()
        {
            if (BeatmapManager.Current.DifficultyEyupStars > 4.5)
                spacingFactor = 2.5f;
            else if (BeatmapManager.Current.DifficultyEyupStars > 3.5)
                spacingFactor = 1.5f;
            else if (BeatmapManager.Current.DifficultyEyupStars < 2)
                spacingFactor = 0.5f;
        }

        protected void RunSanityRules(HitObjectManagerBase hitObjectManager)
        {
            List<HitObjectBase> hitObjects = hitObjectManager.GetHitObjects();

            for (int i = 0; i < hitObjects.Count; i++)
            {
                bool isLast = i == hitObjects.Count - 1;
                HitObjectBase h = hitObjects[i];
                HitObjectBase h1 = isLast ? null : hitObjects[i + 1];
                //Check for hitobjects which share the same startTime...
                if (i > 0 && !isLast)
                {
                    if (hitObjects[i].StartTime != hitObjects[i + 1].StartTime && hitObjects[i].StartTime < hitObjects[i - 1].EndTime + 10)
                    {
                        HitObjectBase last = hitObjects[i - 1];
                        Reports.Add(new AiReportTwoObjects(last, h, delegate { return Math.Abs(last.StartTime - h.StartTime) > 10; }, Severity.Error, LocalisationManager.GetString(OsuString.AICompose_ObjectsTooClose), -1));
                    }
                }

                //check snap
                if (AiModWindow.TestBeatSnap(h.StartTime, true))
                {
                    int index = i;
                    AiReport report = new AiReportOneObject(h, h.StartTime, delegate { return AiModWindow.TestBeatSnap(h.StartTime, true); }, Severity.Warning, LocalisationManager.GetString(OsuString.AICompose_UnsnappedObject), 0);
                    Reports.Add(report);
                }

                if (h.EndTime != h.StartTime)
                {
                    if (AiModWindow.TestBeatSnap(h.EndTime, true)) //Need to account for slider velocity buffer.
                    {
                        int index = i;
                        AiReport report = new AiReportOneObject(h, h.EndTime, delegate { return AiModWindow.TestBeatSnap(h.EndTime, true); }, Severity.Warning, LocalisationManager.GetString(OsuString.AICompose_UnsnappedObjectEnd), 0);
                        Reports.Add(report);
                    }
                }
            }
        }

        protected override void RunAllRules(HitObjectManagerBase hitObjectManager)
        {
            circleRadius = (float)(512 / 8f * (1f - 0.7f * hitObjectManager.AdjustDifficulty(BeatmapManager.Current.DifficultyCircleSize)) / 2f) - 1;

            RunSanityRules(hitObjectManager);

            List<HitObjectBase> hitObjects = hitObjectManager.GetHitObjects();

            bool lastWasSpinner = false;
            double slowestBeatlength = 0;
            float comboSpacingFactor = 0;
            HitObjectBase comboSpacingObject1 = null;
            HitObjectBase comboSpacingObject2 = null;

            for (int i = 0; i < hitObjects.Count; i++)
            {
                bool isLast = i == hitObjects.Count - 1;
                HitObjectBase h = hitObjects[i];
                HitObjectBase h1 = isLast ? null : hitObjects[i + 1];

                //check offscreen
                if (testOffScreen(h.Position))
                    Reports.Add(new AiReportOneObject(h, h.StartTime, delegate { return testOffScreen(h.Position); }, Severity.Warning, LocalisationManager.GetString(OsuString.AICompose_ObjectOffscreen), 0));
                if (h.EndPosition != h.Position && testOffScreen(h.EndPosition))
                    Reports.Add(new AiReportOneObject(h, h.EndTime, delegate { return testOffScreen(h.EndPosition); }, Severity.Warning, LocalisationManager.GetString(OsuString.AICompose_ObjectEndOffscreen), 0));

                //Check for combos over 24
                if (h.ComboNumber > 24)
                    Reports.Add(new AiReportOneObject(h, h.EndTime, delegate { return h.ComboNumber <= 24; }, Severity.Warning, LocalisationManager.GetString(OsuString.AICompose_LongCombo), -1));

                if (i > 0 && !isLast)
                {
                    if (hitObjects[i].StartTime == hitObjects[i + 1].StartTime)
                    {
                        HitObjectBase last = hitObjects[i - 1];
                        Reports.Add(new AiReportTwoObjects(last, h, delegate { return last.StartTime != h.StartTime; }, Severity.Error, LocalisationManager.GetString(OsuString.AICompose_SimultaneousObjects), -1));
                    }
                }


                if (lastWasSpinner && !(h is Spinner))
                {
                    Spinner spinner = (Spinner)hitObjects[i - 1];
                    if (spinner.StartTime > h.StartTime - hitObjectManager.PreEmpt + HitObjectManager.FadeIn)
                        Reports.Add(new AiReportTwoObjects(spinner, h, delegate() { return spinner.StartTime <= h.StartTime - hitObjectManager.PreEmpt + HitObjectManager.FadeIn; }, Severity.Error, LocalisationManager.GetString(OsuString.AICompose_NinjaSpinner), -1));
                }

                lastWasSpinner = (h is Spinner);

                if (h is SliderOsu)
                {
                    // curve stuff is only defined for osu!mode, but happily enough, the editor is always in osu!mode.
                    SliderOsu s2 = h as SliderOsu;
                    if (s2.IsRetarded())
                        Reports.Add(new AiReportOneObject(h, h.StartTime, null, Severity.Error, LocalisationManager.GetString(OsuString.AICompose_AbnormalSlider), -1));

                    // if the mapper sets a broken tick rate but never has long enough sliders to notice it, don't warn
                    if (s2.EndTime - 1000 > s2.StartTime)
                        slowestBeatlength = Math.Max(slowestBeatlength, AudioEngine.beatLengthAt(s2.StartTime));
                }

                //spinner length
                if (h.IsType(HitObjectType.Spinner))
                {
                    if (!h.IsType(HitObjectType.NewCombo))
                        Reports.Add(new AiReportOneObject(h, h.StartTime, null, Severity.Warning, LocalisationManager.GetString(OsuString.AICompose_SpinnerNoNewCombo), -1));

                    //look at SpinnerOsu.cs for these numbers.
                    double autoCanSpin = 0.05 * (double)(h.EndTime - h.StartTime) / Math.PI;
                    double require = ((double)(h.EndTime - h.StartTime) / 1000 * hitObjectManager.SpinnerRotationRatio);
                    if (autoCanSpin - require < 5)
                        Reports.Add(new AiReportOneObject(h, h.StartTime, null, Severity.Warning, LocalisationManager.GetString(OsuString.AICompose_ShortSpinner), -1));
                }

                //check combo distance
                if (!AiModWindow.CheckDistanceSnap) continue;

                if (isLast || h1.NewCombo || h1 is Spinner) continue;

                float thisSpacingFactor = getSpacingFactor(h, h1, hitObjectManager);

                //If the current hitobject is a new combo, get a new distance factor from the current and next object.

                if (h.NewCombo || comboSpacingFactor < MIN_SPACING_FACTOR)
                {
                    comboSpacingFactor = thisSpacingFactor;
                    comboSpacingObject1 = h;
                    comboSpacingObject2 = h1;
                    continue;
                }

                if (comboSpacingFactor > MIN_SPACING_FACTOR &&
                    thisSpacingFactor > MIN_SPACING_FACTOR &&
                    (thisSpacingFactor - comboSpacingFactor > comboSpacingFactor * 0.1 * spacingFactor ||
                    comboSpacingFactor - thisSpacingFactor > comboSpacingFactor * 0.2 * spacingFactor))
                {
                    HitObjectBase object1 = comboSpacingObject1;
                    HitObjectBase object2 = comboSpacingObject2;

                    AiReportTwoObjects report = new AiReportTwoObjects(
                        h,
                        h1,
                        delegate { return Math.Abs(getSpacingFactor(h, h1, hitObjectManager) - getSpacingFactor(object1, object2, hitObjectManager)) < ERROR_FACTOR; },
                        Severity.Warning,
                        thisSpacingFactor < comboSpacingFactor ? LocalisationManager.GetString(OsuString.AICompose_ObjectTooClose) : LocalisationManager.GetString(OsuString.AICompose_ObjectTooFar), 82809);
                    Reports.Add(report);
                }
            }
        }

        // (640 - 512) / 2 = 64
        // (480 - 384) / 2 = 48
        private bool testOffScreen(Vector2 position)
        {
            if (position.X - circleRadius < -64 || position.X + circleRadius > 512 + 64 || position.Y - circleRadius < -48 || position.Y + circleRadius > 384 + 48)
                return true;
            return false;
        }

        private static float getSpacingFactor(HitObjectBase h1, HitObjectBase h2, HitObjectManagerBase hitObjectManager)
        {
            int lastSelectedTime = h1.EndTime;
            int nextSelectedTime = h2.StartTime;
            int time = nextSelectedTime - lastSelectedTime;
            float mul = (float)hitObjectManager.SliderVelocityAt(nextSelectedTime);
            return 1000.0f * (h2.Position - h1.EndPosition).Length() / (mul * time);
        }
    }
}