using osu.Audio;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu_common;
using osu_common.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace osu.GameModes.Edit.AiMod.Rulesets.Osu
{
    internal class AiMapset:AiModRuleset
    {
        public override AiModType Type { get { return AiModType.Mapset; } }
        internal AiMapset() { }

        protected override void RunAllRules(HitObjectManagerBase hitObjectManager)
        {
            Beatmap map = BeatmapManager.Current;
            //group meta check
            //Notice: Don't use group title, it fails to get right mapset in some rare cases.
            List<Beatmap> maps = BeatmapManager.Beatmaps.FindAll(m => m.ContainingFolder == map.ContainingFolder);
            int taikoDiff = 0;
            int maniaDiff = 0;
            int osuDiff = 0;
            double osuStarMin = 5;
            for (int i = 0; i < maps.Count; i++)
            {
                Beatmap diff = maps[i];

                if (diff.PlayMode == PlayModes.Taiko)
                    taikoDiff++;
                if (diff.PlayMode == PlayModes.OsuMania)
                    maniaDiff++;
                if (diff.PlayMode == PlayModes.Osu)
                {
                    osuDiff++;
                    if (diff.DifficultyEyupStars < osuStarMin)
                        osuStarMin = diff.DifficultyEyupStars;
                }
                if (diff.BeatmapChecksum == map.BeatmapChecksum)
                    continue;

                diff.ProcessHeaders();

                if (diff.Artist != map.Artist)
                    Reports.Add(new AiReport(Severity.Warning, string.Format(LocalisationManager.GetString(OsuString.AIMapset_ConflictingArtist), diff.Version)));
                if (diff.Title != map.Title)
                    Reports.Add(new AiReport(Severity.Warning, string.Format(LocalisationManager.GetString(OsuString.AIMapset_ConflictingTitle), diff.Version)));
                if (diff.ArtistUnicode != map.ArtistUnicode)
                    Reports.Add(new AiReport(Severity.Warning, string.Format(LocalisationManager.GetString(OsuString.AIMapset_ConflictingUnicodeArtist), diff.Version)));
                if (diff.TitleUnicode != map.TitleUnicode)
                    Reports.Add(new AiReport(Severity.Warning, string.Format(LocalisationManager.GetString(OsuString.AIMapset_ConflictingUnicodeTitle), diff.Version)));
                if (diff.Source != map.Source)
                    Reports.Add(new AiReport(Severity.Warning, string.Format(LocalisationManager.GetString(OsuString.AIMapset_ConflictingSource), diff.Version)));
                if (diff.Tags != map.Tags)
                    Reports.Add(new AiReport(Severity.Warning, string.Format(LocalisationManager.GetString(OsuString.AIMapset_ConflictingTags), diff.Version)));
                if (diff.AudioFilename != map.AudioFilename)
                    Reports.Add(new AiReport(Severity.Warning, string.Format(LocalisationManager.GetString(OsuString.AIMapset_ConflictingAudioFile), diff.Version)));
                if (diff.AudioLeadIn != map.AudioLeadIn)
                    Reports.Add(new AiReport(Severity.Warning, string.Format(LocalisationManager.GetString(OsuString.AIMapset_ConflictingLeadin), diff.Version)));
                if (diff.PreviewTime != map.PreviewTime)
                    Reports.Add(new AiReport(Severity.Warning, string.Format(LocalisationManager.GetString(OsuString.AIMapset_ConflictingPreviewTime), diff.Version)));
                if (diff.Countdown != map.Countdown)
                    Reports.Add(new AiReport(Severity.Warning, string.Format(LocalisationManager.GetString(OsuString.AIMapset_ConflictingCountdown), diff.Version)));
                if (diff.LetterboxInBreaks != map.LetterboxInBreaks)
                    Reports.Add(new AiReport(Severity.Warning, string.Format(LocalisationManager.GetString(OsuString.AIMapset_ConflictingLetterbox), diff.Version)));

                checkTimingPoints(map, diff);
                
            }
            if (osuDiff == 1)
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIMapset_MissingStandardDiff)));
            if (osuStarMin > 3.25)
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIMapset_MissingEasierDiff)));
            if (taikoDiff == 1)
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIMapset_MissingTaikoDiff)));
            if (maniaDiff == 1)
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIMapset_MissingManiaDiff)));
            checkFiles();
        }

        private void checkTimingPoints(Beatmap origin, Beatmap diff)
        {
            if (origin.AudioFilename != diff.AudioFilename)
                return;
            Dictionary<int, double> timingOrg = new Dictionary<int, double>();
         //   Dictionary<int, double> timingDiff = new Dictionary<int, double>();
            List<ControlPoint> orgList = origin.ControlPoints.FindAll(t => t.timingChange == true);
            List<ControlPoint> diffList = diff.ControlPoints.FindAll(t => t.timingChange == true);
            for (int i = 0; i < orgList.Count; i++)
                timingOrg[(int)orgList[i].offset] = orgList[i].beatLength;
            int fitCount = 0;
            for (int i = 0; i < diffList.Count; i++)
            {
                int time = (int)diffList[i].offset;
                if (timingOrg.ContainsKey(time) && timingOrg[time] == diffList[i].beatLength)
                {
                    timingOrg.Remove(time);
                    fitCount++;
                }
            }
            if (fitCount != diffList.Count || fitCount != orgList.Count)
                Reports.Add(new AiReport(Severity.Warning, "Uninherited timing points conflict with " + diff.Version + " diff."));
        }

        private void checkFiles()
        {
            DirectoryInfo di = new DirectoryInfo(BeatmapManager.Current.ContainingFolder);
            FileInfo[] files = di.GetFiles(@"*", SearchOption.AllDirectories);
            long totalSize = 0;
            bool hasVideo = false;
            Regex isVideo = new Regex(@"\.(avi|wmv|flv|mpg)");
            foreach (FileInfo fi in files)
            {
                totalSize += fi.Length;
                if (isVideo.IsMatch(fi.Name))
                    hasVideo = true;
            }
            totalSize /= 1024; //Kb
            if (!hasVideo && totalSize > 10*1024)
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIMapset_LargeFilesize)));
            else if (hasVideo && totalSize > 24*1024)
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIMapset_LargeFilesizeVideo)));
        }
    }
}
