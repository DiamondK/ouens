﻿using osu.GameplayElements;
using System;
using System.Collections.Generic;
using System.Text;
using osu.GameModes.Edit.AiMod.Rulesets.Osu;

namespace osu.GameModes.Edit.AiMod.Rulesets.Fruits
{
    internal class AiMetaFruits : AiMeta
    {
        public override AiModType Type { get { return AiModType.Meta; } }
        public AiMetaFruits()
        {
        }

        protected override void RunAllRules(HitObjectManagerBase hitObjectManager)
        {
            runMetaCheck(false);
        }
    }
}
