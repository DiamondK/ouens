using osu.Audio;
using osu_common.Helpers;
using osu.GameModes.Edit.AiMod.Rulesets.Osu;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements;
using System;
using System.Collections.Generic;
using System.Text;

namespace osu.GameModes.Edit.AiMod.Rulesets.Mania
{
    internal class AiTimingMania : AiTiming
    {
        internal AiTimingMania() { }

        protected override void RunAllRules(HitObjectManagerBase hitObjectManager)
        {
            EventManager manager = Editor.Instance.eventManager;
            if (manager.eventBreaks.Count > 0)
            {
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AITimingMania_NoManiaBreaktime)));
            }
            if (BeatmapManager.Current.DifficultBemaniStarDisplay < 2.5)
            {
                List<ControlPoint> points = AudioEngine.ControlPoints.FindAll(cp => cp.beatLength < 0 && cp.beatLength != -100);
                if (points.Count > 0 && BeatmapManager.Current.TotalLength / points.Count > 60000 / 4)
                {
                    Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AITimingMania_TooManySpeedChanges)));
                }

            }

            base.RunAllRules(hitObjectManager);
        }
    }
}
