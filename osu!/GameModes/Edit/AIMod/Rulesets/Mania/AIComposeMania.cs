﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using osu_common.Helpers;
using osu.GameModes.Edit.AiMod.Reports;
using osu.GameplayElements;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.Beatmaps;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameModes.Edit.AiMod.Rulesets.Osu;

namespace osu.GameModes.Edit.AiMod.Rulesets.Mania
{
    internal class AiComposeMania : AiCompose
    {
        public override AiModType Type { get { return AiModType.Compose; } }

        public AiComposeMania()
        {
        }

        protected override void RunAllRules(HitObjectManagerBase hitObjectManager)
        {
            List<HitObjectBase> hitObjects = hitObjectManager.GetHitObjects();
            HitObjectManager hom = hitObjectManager as HitObjectManager;

            int column = (int)Math.Round(BeatmapManager.Current.DifficultyCircleSize);
            List<Dictionary<int, int>> columns = new List<Dictionary<int, int>>(column);
            int[] pressArr = new int[column];
            int[] lastNoteInColumn = new int[column]; // used to check distance between notes on the same column
            for (int i = 0; i < column; i++)
                columns.Add(new Dictionary<int, int>());

            for (int i = 0; i < hitObjects.Count; i++)
            {
                HitObjectBase h = hitObjects[i];
                int col = hom.ManiaStage.ColumnAt(h.Position);

                //check snap
                if (AiModWindow.TestBeatSnap(h.StartTime, false))
                {
                    AiReport report = new AiReportOneObject(h, h.StartTime, delegate { return AiModWindow.TestBeatSnap(h.StartTime, false); }, Severity.Warning, LocalisationManager.GetString(OsuString.AICompose_UnsnappedObject), 0);
                    Reports.Add(report);
                }

                if (h.IsType(HitObjectType.Hold))
                {
                    if (h.EndTime - h.StartTime < 10)
                    {
                        Reports.Add(new AiReportOneObject(h, h.StartTime, null, Severity.Error, LocalisationManager.GetString(OsuString.AIComposeMania_HoldNoteTooShort), 0));
                    }

                    if (AiModWindow.TestBeatSnap(h.EndTime, false))
                    {
                        AiReport report = new AiReportOneObject(h, h.EndTime, delegate { return AiModWindow.TestBeatSnap(h.EndTime, false); }, Severity.Warning, LocalisationManager.GetString(OsuString.AICompose_UnsnappedObjectEnd), 0);
                        Reports.Add(report);
                    }
                }

                if (h.StartTime != lastNoteInColumn[col] && lastNoteInColumn[col] != 0 && h.StartTime < lastNoteInColumn[col] + 10)
                {
                    Reports.Add(new AiReportOneObject(h, h.StartTime, delegate { return Math.Abs(lastNoteInColumn[col] - h.StartTime) > 10; }, Severity.Error, LocalisationManager.GetString(OsuString.AICompose_ObjectsTooClose), -1));
                }

                //check for 7 notes simultaneously
                int existCount = 1;
                for (int j = 0; j < column; j++)
                {
                    if (pressArr[j] >= h.StartTime)
                        existCount++;
                }
                //pressArr[col] = h.EndTime;
                if (h.StartTime == h.EndTime)
                    pressArr[col] = h.StartTime;
                else
                    pressArr[col] = h.EndTime - 2;//hack, holds' end are not counted here.
                if (existCount >= 7)
                {
                    AiReportOneObject report = new AiReportOneObject(h, h.StartTime, null, Severity.Error, LocalisationManager.GetString(OsuString.AIComposeMania_TooManyNotes), 0);
                    Reports.Add(report);
                }
                //note stack on another note's startTime
                if (columns[col].ContainsKey(h.StartTime) || columns[col].ContainsKey(h.EndTime))
                {
                    AiReportOneObject report = new AiReportOneObject(h, h.StartTime, null, Severity.Error, LocalisationManager.GetString(OsuString.AIComposeMania_StackedObjects), 0);
                    Reports.Add(report);
                    continue;
                }
                bool needReport = false;
                foreach (KeyValuePair<int, int> pair in columns[col])
                {
                    //normal note, already checked above
                    if (pair.Key == pair.Value)
                        continue;
                    if (h.StartTime <= pair.Key && h.EndTime >= pair.Key)
                    {
                        needReport = true;
                        break;
                    }
                    if (h.StartTime <= pair.Value && h.EndTime >= pair.Value)
                    {
                        needReport = true;
                        break;
                    }
                    if (h.StartTime >= pair.Key && h.EndTime <= pair.Value)
                    {
                        needReport = true;
                        break;
                    }
                    if (h.StartTime < pair.Key && h.EndTime > pair.Value)
                    {
                        needReport = true;
                        break;
                    }
                }
                if (needReport)
                {
                    AiReportOneObject report = new AiReportOneObject(h, h.StartTime, null, Severity.Error, LocalisationManager.GetString(OsuString.AIComposeMania_OverlappingObject), 0);
                    Reports.Add(report);
                    continue;
                }
                else
                    columns[col][h.StartTime] = h.EndTime;

                // we don't want to mark this note as the previous note in the column until it's been processed
                lastNoteInColumn[col] = h.StartTime;
            }
        }
    }
}