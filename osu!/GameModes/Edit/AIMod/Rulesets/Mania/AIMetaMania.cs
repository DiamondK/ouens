﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using osu_common.Helpers;
using osu.GameModes.Edit.AiMod.Reports;
using osu.GameplayElements;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.Beatmaps;
using osu.GameModes.Edit.AiMod.Rulesets.Osu;

namespace osu.GameModes.Edit.AiMod.Rulesets.Mania
{
    internal class AiMetaMania : AiMeta
    {
        public AiMetaMania()
        {
        }

        protected override void RunAllRules(HitObjectManagerBase hitObjectManager)
        {
            Beatmap map = BeatmapManager.Current;
            double star = map.DifficultBemaniStarDisplay;
            if (star < 1.5)
            {
                Difficulty = BeatmapDifficulty.Easy;
            }
            else if (star < 2.5)
            {
                Difficulty = BeatmapDifficulty.Normal;
            }
            else if (star < 4)
            {
                Difficulty = BeatmapDifficulty.Hard;
            }
            else if (star < 4.5)
            {
                Difficulty = BeatmapDifficulty.Insane;
            }
            else
            {
                Difficulty = BeatmapDifficulty.Expert;
            }
            //HP
            if (Math.Round(map.DifficultyHpDrainRate) < 4 && Difficulty < BeatmapDifficulty.Hard)
            {
                Reports.Add(new AiReport(Severity.Info, LocalisationManager.GetString(OsuString.AIMetaMania_EasyHpBelow4)));
            }
            else if (Math.Round(map.DifficultyHpDrainRate) < 7 && Difficulty >= BeatmapDifficulty.Hard)
            {
                Reports.Add(new AiReport(Severity.Info, LocalisationManager.GetString(OsuString.AIMetaMania_HardHpBelow7)));
            }
            //OD
            double holdRate = map.countSlider * 1.0 / map.ObjectCount;
            if (holdRate < 0.05 && Math.Round(map.DifficultyOverall) < 8)
            {
                Reports.Add(new AiReport(Severity.Info, LocalisationManager.GetString(OsuString.AIMetaMania_VeryLowSliderOdBelow8)));
            }
            else if (holdRate < 0.25 && Math.Round(map.DifficultyOverall) < 7)
            {
                Reports.Add(new AiReport(Severity.Info, LocalisationManager.GetString(OsuString.AIMetaMania_LowSliderOdBelow7)));
            }
            else if (Math.Round(map.DifficultyOverall) < 5)
            {
                Reports.Add(new AiReport(Severity.Info, LocalisationManager.GetString(OsuString.AIMetaMania_OdBelow5)));
            }

            if (map.LetterboxInBreaks)
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIMetaMania_NoManiaLetterbox)));
            if (map.Countdown != Countdown.Disabled)
            {
                Reports.Add(new AiReport(Severity.Warning, LocalisationManager.GetString(OsuString.AIMetaMania_NoManiaCountdown)));
            }
            runMetaCheck(false);
        }

    }
}