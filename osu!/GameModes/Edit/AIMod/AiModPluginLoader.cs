﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Policy;
using System.IO;

namespace osu.GameModes.Edit.AiMod
{
    internal class AiModPluginLoader : IDisposable
    {
        const string AIMOD_PLUGIN_DIRECTORY = "Plugins/AiMod";

        internal List<AiModRuleset> Rulesets = new List<AiModRuleset>();
        Dictionary<AiModRulesetAttribute, AppDomain> appDomains = new Dictionary<AiModRulesetAttribute, AppDomain>();

        public AiModPluginLoader()
        {
            LoadRulesets();
        }

        internal void LoadRulesets()
        {
            UnloadRulesets();

            AppDomain tempDomain = AppDomain.CreateDomain("AiModPluginInterface", AppDomain.CurrentDomain.Evidence, new AppDomainSetup()
            {
                ApplicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase,
                PrivateBinPath = AIMOD_PLUGIN_DIRECTORY,
                DisallowCodeDownload = true,
                ApplicationName = "AiModPluginInterface"
            });

            if (Directory.Exists(AIMOD_PLUGIN_DIRECTORY))
            {
                foreach (string file in Directory.GetFiles(AIMOD_PLUGIN_DIRECTORY, "*.dll"))
                {
                    LoadAssemblyAttributesProxy proxy = tempDomain.CreateInstanceAndUnwrap(
                                  "osu!framework",
                                  typeof(LoadAssemblyAttributesProxy).FullName)
                                   as LoadAssemblyAttributesProxy;
                    AiModRulesetAttribute[] attribs = proxy.LoadAssemblyAttributes(file);
                    if (attribs.Length > 0)
                        Rulesets.Add(InstantiateRuleset(attribs[0]));
                }
            }

            AppDomain.Unload(tempDomain);
        }

        internal void UnloadRulesets()
        {
            if (appDomains != null)
            {
                foreach (AppDomain domain in appDomains.Values)
                    AppDomain.Unload(domain);
                appDomains.Clear();
            }
        }

        private AppDomain CreateAppDomain(AiModRulesetAttribute info)
        {
            AppDomainSetup domainSetup = new AppDomainSetup()
            {
                ApplicationName = info.RulesetName,
                ConfigurationFile = info.RulesetName + ".dll.config",
                ApplicationBase = AppDomain.CurrentDomain.BaseDirectory,
                DisallowCodeDownload = true,
                PrivateBinPath = AIMOD_PLUGIN_DIRECTORY
            };

            AppDomain appDomain = AppDomain.CreateDomain(domainSetup.ApplicationName, AppDomain.CurrentDomain.Evidence, domainSetup);

            return appDomain;
        }

        private AiModRuleset InstantiateRuleset(AiModRulesetAttribute info)
        {
            AppDomain appDomain;

            if (!appDomains.TryGetValue(info, out appDomain))
            {
                appDomain = CreateAppDomain(info);
                appDomains[info] = appDomain;
            }
            
            return (AiModRuleset)appDomain.CreateInstanceAndUnwrap(info.RulesetName, info.EntryType);
        }

        #region IDisposable Members
        public void Dispose()
        {
            UnloadRulesets();
        }
        #endregion
    }
}