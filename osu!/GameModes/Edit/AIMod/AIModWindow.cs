using System;
using System.Drawing;
using System.Windows.Forms;
using osu.Audio;
using osu.GameModes.Edit.AiMod.Rulesets;
using osu.GameplayElements.HitObjects;
using osu_common.Libraries.NetLib;
using System.Collections.Generic;
using osu.Properties;
using osu_common;
using osu_common.Helpers;
using osu.Helpers;
using osu.GameModes.Edit.AiMod.Reports;
using osu.Graphics.Primitives;
using System.IO;
using System.Security.Policy;
using System.Reflection;
using osu.GameModes.Edit.AiMod.Rulesets.Osu;

namespace osu.GameModes.Edit.AiMod
{
    internal partial class AiModWindow : pForm
    {
        internal static bool CheckDistanceSnap = false;
        AiModPluginLoader pluginLoader = new AiModPluginLoader();

        private readonly Editor editor;
        bool drawing_with_glass = false;

        internal AiModWindow(Editor editor)
        {
            this.editor = editor;
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;

            // designer doesn't give custom events hnrrgh
            this.NonClientHitTest += AiModWindow_NonClientTest;
            this.DwmCompositionChanged += AiModWindow_DwmCompositionChanged;
            pnlTop.ClickThrough = true;

            AiModWindow_DwmCompositionChanged(this, EventArgs.Empty);

            dataGridView1.ColumnHeadersHeight = (int)(23 * DpiHelper.DPI(this) / 96.0f);
        }

        private int glassOffsetX;
        private int glassOffsetY;

        private void AiModWindow_Load(object sender, EventArgs e)
        {
        }

        internal new void Close()
        {
            allowClose = true;
            base.Close();
        }

        bool allowClose;
        private AiReport selectedReport;

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            GameBase.Scheduler.Add(delegate { editor.aiMod = null; });

            e.Cancel = !allowClose;

            pluginLoader.Dispose();

            Hide();

            base.OnClosing(e);
        }

        internal void Reset()
        {
            rerunModding(null, null);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);


        }

        bool isReporting = false;
        private void Report(List<AiReport> reports)
        {
            isReporting = true;
            DataGridViewRow[] rows = new DataGridViewRow[reports.Count];

            dataGridView1.Rows.Clear();

            int i = 0;
            foreach (AiReport report in reports)
            {
                DataGridViewRow row = (DataGridViewRow)dataGridView1.RowTemplate.Clone();

                DataGridViewImageCell iconCell = new DataGridViewImageCell();
                switch (report.Severity)
                {
                    case Severity.Error:
                        iconCell.Value = Resources.aimod_error;
                        break;
                    case Severity.Info:
                        iconCell.Value = Resources.aimod_info;
                        break;
                    case Severity.Warning:
                        iconCell.Value = Resources.aimod_warning;
                        break;
                }

                row.Cells.Add(iconCell);
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells.Add(new DataGridViewLinkCell());

                row.Cells[2].Value = report.Time != -1 ? String.Format(@"{0:00}:{1:00}:{2:000}", (report.Time / 60000), report.Time % 60000 / 1000, Math.Max(0, report.Time) % 1000) : string.Empty;
                row.Cells[1].Value = report.Information;
                row.Tag = report;

                rows[i++] = row;
            }

            dataGridView1.Rows.AddRange(rows);

            dataGridView1.ClearSelection();
            isReporting = false;
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0 || isReporting || !hasDrawn)
            {
                selectedReport = null;
                return;
            }

            selectedReport = dataGridView1.SelectedRows[0].Tag as AiReport;

            if (selectedReport.RelatedHitObjects.Count > 0)
                editor.Compose.Select(selectedReport.RelatedHitObjects.ToArray());

            if (selectedReport.Time != -1)
                AudioEngine.SeekTo(selectedReport.Time);

        }

        bool hasDrawn;
        internal void Draw()
        {
            if (!Visible)
                return;

            if (!hasDrawn)
            {
                hasDrawn = true;
                dataGridView1.ClearSelection(); //ensure we don't make a selection before the first draw.
            }

            if (selectedReport != null)
            {
                AiReportOneObject oneObject = selectedReport as AiReportOneObject;
                AiReportTwoObjects twoObjects = selectedReport as AiReportTwoObjects;

                selectedReport.Draw();

                HitObject h1 = null;
                HitObject h2 = null;
                bool corrected = selectedReport.Check();

                if (twoObjects != null)
                {
                    h1 = matchObject(twoObjects.h1);
                    h2 = matchObject(twoObjects.h2);
                }
                else if (oneObject != null)
                    h1 = matchObject(oneObject.h1);

                if (h1 != null && h2 != null)
                {
                    if (h1.IsVisible && h2.IsVisible)
                    {
                        Line line = new Line(GameBase.GamefieldToDisplay(h1.EndPosition), GameBase.GamefieldToDisplay(h2.Position));

                        bool correct = twoObjects.Check();

                        GameBase.LineRenderer.Draw(line, 8, GameBase.Time % 1000 > 500 || correct ? Microsoft.Xna.Framework.Graphics.Color.Black : Microsoft.Xna.Framework.Graphics.Color.White);
                        GameBase.LineRenderer.Draw(line, 5, correct ? Microsoft.Xna.Framework.Graphics.Color.YellowGreen : Microsoft.Xna.Framework.Graphics.Color.Red);
                    }
                }
                else if (h1 != null)
                {
                    if (h1.IsVisible)
                        h1.Select();
                }
            }
        }

        /// <summary>
        /// naive matching of cloned HitObjectBase to HitObject
        /// </summary>
        private HitObject matchObject(HitObjectBase match)
        {
            return editor.hitObjectManager.hitObjects.Find(h => h.StartTime == match.StartTime);
        }

        internal Dictionary<AiModType, List<AiReport>> Reports = new Dictionary<AiModType, List<AiReport>>();

        List<AiModRuleset> Rulesets = new List<AiModRuleset>();

        private void rerunModding(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();

            Reports.Clear();

            if (Rulesets.Count == 0)
            {
                Rulesets.Add(AiFactory.AICompose);
                Rulesets.Add(AiFactory.AIDesign);
                Rulesets.Add(AiFactory.AITiming);
                Rulesets.Add(AiFactory.AIMeta);
                Rulesets.Add(AiFactory.AIMapset);
                //   Rulesets.Add(new AISpacing());
                //   Rulesets.Add(AIFactory.AIDifficulty);
                //    Rulesets.Add(AIFactory.AIStoryboard);
                Rulesets.AddRange(pluginLoader.Rulesets);
            }

            foreach (AiModRuleset r in Rulesets)
            {
                List<AiReport> reports = r.Run(Editor.Instance.hitObjectManager);
                if (reports != null)
                {
                    if (!Reports.ContainsKey(r.Type))
                        Reports[r.Type] = new List<AiReport>();
                    Reports[r.Type].AddRange(reports);
                }
            }

            m_difficulty = ((AiMeta)Rulesets.Find(r => r.Type == AiModType.Meta)).Difficulty;

            int totalReports = 0;

            Reports[AiModType.All] = new List<AiReport>();

            foreach (KeyValuePair<AiModType, List<AiReport>> l in Reports)
            {
                tabControl1.TabPages[(int)l.Key].Text = l.Key.ToString() + @" (" + l.Value.Count + @")";

                if (l.Key != AiModType.All)
                {
                    totalReports += l.Value.Count;
                    Reports[AiModType.All].AddRange(l.Value);
                }
            }

            if (totalReports == 0)
                Reports[AiModType.All].Add(new AiReport(-1, Severity.Info, LocalisationManager.GetString(OsuString.AIModWindow_NoProblemsFound), 0, null));

            tabControl1_SelectedIndexChanged(null, null);
            UpdateRight();
        }

        Assembly aiModDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            throw new NotImplementedException();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Report(Reports[(AiModType)tabControl1.SelectedIndex]);
        }

        internal void Run()
        {
            rerunModding(null, null);
        }

        private Color green = Color.FromArgb(0, 16, 112, 16);
        private Color yellow = Color.FromArgb(0, 120, 80, 16);
        private Color red = Color.FromArgb(0, 136, 16, 16);
        private BeatmapDifficulty m_difficulty;

        private void UpdateRight()
        {
            if (Reports.Count == 0) return;

            int errors = Reports[AiModType.All].FindAll(r => r.Severity == Severity.Error).Count; //Reports[AiModType.Snapping].Count + Reports[AiModType.Errors].Count;
            int warnings = Reports[AiModType.All].FindAll(r => r.Severity == Severity.Warning).Count; //errors + Reports[AiModType.Spacing].Count + Reports[AiModType.Style].Count;

            monoDifficulty.Text = m_difficulty.ToString();
            monoWarnings.Text = warnings.ToString();
            monoErrors.Text = errors.ToString();
            lblDifficulty.Text = m_difficulty.ToString();
            lblWarnings.Text = warnings.ToString();
            lblErrors.Text = errors.ToString();

            monoWarnings.ForeColor = warnings > 0 ? yellow : Color.Black;
            monoErrors.ForeColor = errors > 0 ? red : Color.Black;
            lblWarnings.ForeColor = warnings > 0 ? yellow : Color.Black;
            lblErrors.ForeColor = errors > 0 ? red : Color.Black;
        }

        private void AiModWindow_NonClientTest(object sender, NonClientTestEventArgs e)
        {
            if (PointToClient(e.Location).Y < pnlTop.Height && e.Region == NonClientRegions.Client) e.Region = NonClientRegions.Caption;
        }

        private void GlassOn()
        {
            if (!drawing_with_glass)
            {
                glassOffsetX = buttonRun.Left;
                glassOffsetY = buttonRun.Bottom - pnlGlass.Bottom;

                pnlGlass.Visible = true;
                grpRight.Visible = false;

                pnlTop.BackColor = Color.Transparent;
                panel2.BackColor = Color.Black;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                buttonRun.Top -= glassOffsetY;
                buttonRun.Left -= glassOffsetX;
                pnlTop.Height -= glassOffsetY;
                panel2.Top -= glassOffsetY;
            }

            AeroGlassHelper.ExtendFrame(this, new Margins(0, 0, pnlTop.Height, 0));
            drawing_with_glass = true;
        }

        private void GlassOff()
        {
            if (drawing_with_glass)
            {
                grpRight.Visible = true;
                pnlGlass.Visible = false;

                pnlTop.BackColor = Color.FromKnownColor(KnownColor.Control);
                panel2.BackColor = Panel.DefaultBackColor;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
                buttonRun.Top += glassOffsetY;
                buttonRun.Left += glassOffsetX;
                pnlTop.Height += glassOffsetY;
                panel2.Top += glassOffsetY;
            }

            AeroGlassHelper.ExtendFrame(this, new Margins(0, 0, 0, 0));
            drawing_with_glass = false;
        }

        private void AiModWindow_DwmCompositionChanged(object sender, EventArgs e)
        {
            if (AeroGlassHelper.GlassEnabled) GlassOn();
            else GlassOff();
        }

        private void AiModWindow_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\u001b')
            {
                e.Handled = true;
                Close();
            }
        }

        private void cb_ds_CheckedChanged(object sender, EventArgs e)
        {
            CheckDistanceSnap = cb_ds.Checked;
            rerunModding(null, null);
        }

        //Some helpers
        internal static bool TestBeatSnap(int time, bool includeBuffer)
        {
            int snapped = Editor.Instance.Timing.BeatSnapValue(time, 16, time);
            int snappedSixth = Editor.Instance.Timing.BeatSnapValue(time, 12, time);

            bool beatIsSnapped;
            if (includeBuffer)
            {
                beatIsSnapped = (Math.Abs(snapped - time) <= 1 || Math.Abs(snappedSixth - time) <= 1);
            }
            else
            {
                beatIsSnapped = (Math.Abs(snapped - time) < 1 || Math.Abs(snappedSixth - time) < 1);
            }

            return !beatIsSnapped;
        }
    }
}