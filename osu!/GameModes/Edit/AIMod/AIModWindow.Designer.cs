﻿namespace osu.GameModes.Edit.AiMod
{
    partial class AiModWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.pnlTop = new osu.Helpers.Forms.SeeThroughPanel();
            this.cb_ds = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpRight = new System.Windows.Forms.GroupBox();
            this.lblErrors = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblWarnings = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDifficulty = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlGlass = new osu.Helpers.Forms.GlassGroupPanel();
            this.monoErrors = new osu.Helpers.Forms.MonochromeLabel();
            this.monochromeLabel4 = new osu.Helpers.Forms.MonochromeLabel();
            this.monoWarnings = new osu.Helpers.Forms.MonochromeLabel();
            this.monochromeLabel3 = new osu.Helpers.Forms.MonochromeLabel();
            this.monoDifficulty = new osu.Helpers.Forms.MonochromeLabel();
            this.monochromeLabel1 = new osu.Helpers.Forms.MonochromeLabel();
            this.buttonRun = new System.Windows.Forms.Button();
            this.SeverityColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.Information = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewLinkColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.pnlTop.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.grpRight.SuspendLayout();
            this.pnlGlass.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SeverityColumn,
            this.Information,
            this.Time});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.NavajoWhite;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 103);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ShowCellErrors = false;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.ShowRowErrors = false;
            this.dataGridView1.Size = new System.Drawing.Size(424, 343);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // pnlTop
            // 
            this.pnlTop.ClickThrough = false;
            this.pnlTop.Controls.Add(this.cb_ds);
            this.pnlTop.Controls.Add(this.panel2);
            this.pnlTop.Controls.Add(this.panel1);
            this.pnlTop.Controls.Add(this.buttonRun);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Padding = new System.Windows.Forms.Padding(1);
            this.pnlTop.Size = new System.Drawing.Size(424, 103);
            this.pnlTop.TabIndex = 0;
            // 
            // cb_ds
            // 
            this.cb_ds.AutoSize = true;
            this.cb_ds.Location = new System.Drawing.Point(116, 13);
            this.cb_ds.Name = "cb_ds";
            this.cb_ds.Size = new System.Drawing.Size(145, 19);
            this.cb_ds.TabIndex = 13;
            this.cb_ds.Text = "Check distance snap";
            this.cb_ds.UseVisualStyleBackColor = true;
            this.cb_ds.CheckedChanged += new System.EventHandler(this.cb_ds_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Location = new System.Drawing.Point(2, 83);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(440, 31);
            this.panel2.TabIndex = 12;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(440, 31);
            this.tabControl1.TabIndex = 10;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(432, 5);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "All";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(432, 5);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Compose";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(432, 5);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Design";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(432, 5);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Timing";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(432, 5);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Meta";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(432, 5);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Mapset";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grpRight);
            this.panel1.Controls.Add(this.pnlGlass);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(274, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(149, 101);
            this.panel1.TabIndex = 11;
            // 
            // grpRight
            // 
            this.grpRight.Controls.Add(this.lblErrors);
            this.grpRight.Controls.Add(this.label3);
            this.grpRight.Controls.Add(this.lblWarnings);
            this.grpRight.Controls.Add(this.label2);
            this.grpRight.Controls.Add(this.lblDifficulty);
            this.grpRight.Controls.Add(this.label1);
            this.grpRight.Location = new System.Drawing.Point(4, 6);
            this.grpRight.Name = "grpRight";
            this.grpRight.Size = new System.Drawing.Size(133, 69);
            this.grpRight.TabIndex = 1;
            this.grpRight.TabStop = false;
            // 
            // lblErrors
            // 
            this.lblErrors.AutoSize = true;
            this.lblErrors.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrors.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.lblErrors.Location = new System.Drawing.Point(63, 46);
            this.lblErrors.Name = "lblErrors";
            this.lblErrors.Size = new System.Drawing.Size(39, 13);
            this.lblErrors.TabIndex = 5;
            this.lblErrors.Text = "errors";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Errors:";
            // 
            // lblWarnings
            // 
            this.lblWarnings.AutoSize = true;
            this.lblWarnings.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarnings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(80)))), ((int)(((byte)(16)))));
            this.lblWarnings.Location = new System.Drawing.Point(63, 31);
            this.lblWarnings.Name = "lblWarnings";
            this.lblWarnings.Size = new System.Drawing.Size(57, 13);
            this.lblWarnings.TabIndex = 3;
            this.lblWarnings.Text = "warnings";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Warnings:";
            // 
            // lblDifficulty
            // 
            this.lblDifficulty.AutoSize = true;
            this.lblDifficulty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDifficulty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(112)))), ((int)(((byte)(16)))));
            this.lblDifficulty.Location = new System.Drawing.Point(63, 16);
            this.lblDifficulty.Name = "lblDifficulty";
            this.lblDifficulty.Size = new System.Drawing.Size(55, 13);
            this.lblDifficulty.TabIndex = 1;
            this.lblDifficulty.Text = "difficulty";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Difficulty:";
            // 
            // pnlGlass
            // 
            this.pnlGlass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pnlGlass.Controls.Add(this.monoErrors);
            this.pnlGlass.Controls.Add(this.monochromeLabel4);
            this.pnlGlass.Controls.Add(this.monoWarnings);
            this.pnlGlass.Controls.Add(this.monochromeLabel3);
            this.pnlGlass.Controls.Add(this.monoDifficulty);
            this.pnlGlass.Controls.Add(this.monochromeLabel1);
            this.pnlGlass.Location = new System.Drawing.Point(12, 0);
            this.pnlGlass.Name = "pnlGlass";
            this.pnlGlass.Size = new System.Drawing.Size(135, 65);
            this.pnlGlass.TabIndex = 0;
            this.pnlGlass.Visible = false;
            // 
            // monoErrors
            // 
            this.monoErrors.AutoSize = true;
            this.monoErrors.BackColor = System.Drawing.Color.Transparent;
            this.monoErrors.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monoErrors.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.monoErrors.Location = new System.Drawing.Point(63, 41);
            this.monoErrors.Name = "monoErrors";
            this.monoErrors.Size = new System.Drawing.Size(39, 13);
            this.monoErrors.TabIndex = 5;
            this.monoErrors.Text = "errors";
            // 
            // monochromeLabel4
            // 
            this.monochromeLabel4.AutoSize = true;
            this.monochromeLabel4.BackColor = System.Drawing.Color.Transparent;
            this.monochromeLabel4.Location = new System.Drawing.Point(3, 40);
            this.monochromeLabel4.Name = "monochromeLabel4";
            this.monochromeLabel4.Size = new System.Drawing.Size(46, 15);
            this.monochromeLabel4.TabIndex = 4;
            this.monochromeLabel4.Text = "Errors:";
            // 
            // monoWarnings
            // 
            this.monoWarnings.AutoSize = true;
            this.monoWarnings.BackColor = System.Drawing.Color.Transparent;
            this.monoWarnings.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monoWarnings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(80)))), ((int)(((byte)(16)))));
            this.monoWarnings.Location = new System.Drawing.Point(63, 26);
            this.monoWarnings.Name = "monoWarnings";
            this.monoWarnings.Size = new System.Drawing.Size(57, 13);
            this.monoWarnings.TabIndex = 3;
            this.monoWarnings.Text = "warnings";
            // 
            // monochromeLabel3
            // 
            this.monochromeLabel3.AutoSize = true;
            this.monochromeLabel3.BackColor = System.Drawing.Color.Transparent;
            this.monochromeLabel3.Location = new System.Drawing.Point(3, 25);
            this.monochromeLabel3.Name = "monochromeLabel3";
            this.monochromeLabel3.Size = new System.Drawing.Size(65, 15);
            this.monochromeLabel3.TabIndex = 2;
            this.monochromeLabel3.Text = "Warnings:";
            // 
            // monoDifficulty
            // 
            this.monoDifficulty.AutoSize = true;
            this.monoDifficulty.BackColor = System.Drawing.Color.Transparent;
            this.monoDifficulty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monoDifficulty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(112)))), ((int)(((byte)(16)))));
            this.monoDifficulty.Location = new System.Drawing.Point(63, 11);
            this.monoDifficulty.Name = "monoDifficulty";
            this.monoDifficulty.Size = new System.Drawing.Size(55, 13);
            this.monoDifficulty.TabIndex = 1;
            this.monoDifficulty.Text = "difficulty";
            // 
            // monochromeLabel1
            // 
            this.monochromeLabel1.AutoSize = true;
            this.monochromeLabel1.BackColor = System.Drawing.Color.Transparent;
            this.monochromeLabel1.Location = new System.Drawing.Point(3, 10);
            this.monochromeLabel1.Name = "monochromeLabel1";
            this.monochromeLabel1.Size = new System.Drawing.Size(59, 15);
            this.monochromeLabel1.TabIndex = 0;
            this.monochromeLabel1.Text = "Difficulty:";
            // 
            // buttonRun
            // 
            this.buttonRun.BackColor = System.Drawing.Color.Transparent;
            this.buttonRun.Location = new System.Drawing.Point(9, 29);
            this.buttonRun.Margin = new System.Windows.Forms.Padding(8);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(91, 43);
            this.buttonRun.TabIndex = 3;
            this.buttonRun.Text = "Refresh";
            this.buttonRun.UseVisualStyleBackColor = false;
            this.buttonRun.Click += new System.EventHandler(this.rerunModding);
            // 
            // SeverityColumn
            // 
            this.SeverityColumn.FillWeight = 25.29443F;
            this.SeverityColumn.HeaderText = "";
            this.SeverityColumn.MinimumWidth = 14;
            this.SeverityColumn.Name = "SeverityColumn";
            // 
            // Information
            // 
            this.Information.FillWeight = 251.94F;
            this.Information.HeaderText = "Information";
            this.Information.Name = "Information";
            this.Information.ReadOnly = true;
            // 
            // Time
            // 
            this.Time.FillWeight = 78.6944F;
            this.Time.HeaderText = "Time";
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            this.Time.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // AiModWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(424, 446);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.pnlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(440, 320);
            this.Name = "AiModWindow";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AiMod";
            this.Load += new System.EventHandler(this.AiModWindow_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AiModWindow_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.grpRight.ResumeLayout(false);
            this.grpRight.PerformLayout();
            this.pnlGlass.ResumeLayout(false);
            this.pnlGlass.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private Helpers.Forms.SeeThroughPanel pnlTop;
        private System.Windows.Forms.Panel panel1;
        private Helpers.Forms.GlassGroupPanel pnlGlass;
        private Helpers.Forms.MonochromeLabel monoErrors;
        private Helpers.Forms.MonochromeLabel monochromeLabel4;
        private Helpers.Forms.MonochromeLabel monoWarnings;
        private Helpers.Forms.MonochromeLabel monochromeLabel3;
        private Helpers.Forms.MonochromeLabel monoDifficulty;
        private Helpers.Forms.MonochromeLabel monochromeLabel1;
        private System.Windows.Forms.GroupBox grpRight;
        private System.Windows.Forms.Label lblErrors;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblWarnings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblDifficulty;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.CheckBox cb_ds;
        private System.Windows.Forms.DataGridViewImageColumn SeverityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Information;
        private System.Windows.Forms.DataGridViewLinkColumn Time;
    }
}