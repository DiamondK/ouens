#region Using Statements

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Edit.AiMod;
using osu.GameModes.Edit.Forms;
using osu.GameModes.Edit.Modes;
using osu.GameModes.Play;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu_common;
using osu_common.Helpers;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using osu.Graphics.Primitives;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements.HitObjects.Osu;
using osu.Helpers;
using osu.Helpers.Forms;
using osu.Online.Social;

#endregion

namespace osu.GameModes.Edit
{
    internal partial class Editor : DrawableGameComponent
    {
        #region General

        internal static Editor Instance;

        private bool bookmarkMenuActive;
        internal bool Dirty;

        internal EventManager eventManager;
        internal bool ExitedProperly;


        internal HitObjectManager hitObjectManager;
        internal bool IsDragging;

        private pSprite seekAdd;

        private pSprite seekNext;
        private pSprite seekPrev;
        private pSprite seekReset;

        private PlayModes LoadedMode;

        internal Editor(Game game)
            : base(game)
        {
            if (GameBase.EditorControl == null)
                GameBase.EditorControl = new EditorControl();
            Instance = this;
        }

        internal static bool isBeatSnapping
        {
            get { return AudioEngine.ControlPoints.Count > 0; }
        }

        public override void Initialize()
        {
            StreamingManager.StopSpectating(false);

            Beatmap beatmap = BeatmapManager.Current;
            BeatmapManager.GetOnlineBeatmapInfo(beatmap);

            EventManager.UserDimLevel = 30;

            HistoryBackward = new List<EditorState>();
            HistoryForward = new Stack<EditorState>();
            HistoryBackup = new Stack<EditorState>();
            timelineManager = new SpriteManager(true);


            hitObjectManager = new HitObjectManagerEditor();

            //Make sure we don't have any mods interfering with the editor.
            ModManager.Reset();
            ModManager.ModStatus &= ~Mods.DoubleTime; // make sure nightcore is disabled in the editor
            Player.currentScore = null;

            GameBase.EditorControl.RegisterEditor(this);

            OnScreenSizeChanged();

            try
            {
                if (beatmap.Title.Length == 0)
                {
                    AudioEngine.FreeMusic();

                    string asciiOnly = GeneralHelper.AsciiOnly(beatmap.AudioFilename);

                    if (asciiOnly != beatmap.AudioFilename)
                    {
                        if (asciiOnly.Length < 4)
                            asciiOnly = "music.mp3";


                        if (GeneralHelper.FileMove(beatmap.AudioFilename, asciiOnly))
                            beatmap.AudioFilename = asciiOnly;
                    }

                    AudioEngine.LoadAudio(beatmap, true, false);
                    beatmap.Title = GeneralHelper.AsciiOnly(AudioEngine.AudioTitle);
                    beatmap.TitleUnicode = AudioEngine.AudioTitle;
                    beatmap.Artist = GeneralHelper.AsciiOnly(AudioEngine.AudioArtist);
                    beatmap.ArtistUnicode = AudioEngine.AudioArtist;
                }
                else
                    AudioEngine.LoadAudio(beatmap, false, false);
            }
            catch (AudioNotLoadedException)
            {
                NotificationManager.ShowMessageMassive("Could not load audio file.", 2000);
                Exit(false);
                return;
            }

            if (!LoadFile(beatmap, false, false))
                return;

            TimelineMagnification = OsuMathHelper.Clamp(beatmap.TimelineZoom, 0.1F, 8);

            InitializeMenus();

            switch (beatmap.PlayMode)
            {
                default:
                    Compose = new EditorModeCompose(this);
                    break;
                case PlayModes.Taiko:
                    Compose = new EditorModeComposeTaiko(this);
                    break;
                case PlayModes.OsuMania:
                    Compose = new EditorModeComposeMania(this);
                    break;
            }

            //Check if the bookmarks have changed inside test mode
            if (PlayerTest.BookmarksCache != null && PlayerTest.BookmarksCache != hitObjectManager.Bookmarks)
            {
                hitObjectManager.Bookmarks = PlayerTest.BookmarksCache;
                Instance.Dirty = true;
            }
            PlayerTest.BookmarksCache = null;

            LoadedMode = beatmap.PlayMode;

            Timing = new EditorModeTiming(this);
            Design = new EditorModeDesign(this);

            InputManager.Bind(InputEventType.OnClick, onClick);
            InputManager.Bind(InputEventType.OnClickUp, onClickUp);
            InputManager.Bind(InputEventType.OnDoubleClick, onDoubleClick);
            InputManager.Bind(InputEventType.OnDrag, onDrag);

            KeyboardHandler.OnKeyRepeat += Game1_OnKeyRepeat;

            InputManager.Bind(InputEventType.OnEndDrag, onEndDrag);
            InputManager.Bind(InputEventType.OnMouseWheelUp, onMouseWheelUp);
            InputManager.Bind(InputEventType.OnMouseWheelDown, onMouseWheelDown);

            InputManager.Bind(InputEventType.OnMouseWheelUp, onMouseWheelUpAlt, InputTargetType.AltOverrides);
            InputManager.Bind(InputEventType.OnMouseWheelDown, onMouseWheelDownAlt, InputTargetType.AltOverrides);


            GameBase.OnFocusLost += GameBase_OnFocusLost;

            AudioEngine.ActiveInheritedTimingPointChanged += AudioEngine_ActiveInheritedTimingPointChanged;

            Compose.Initialize();
            Timing.Initialize();
            Design.Initialize();

            GameBase.EditorControl.AudioRateChanged(100);
            GameBase.EditorControl.GridSnapChanged(EditorModeCompose.SoundAdditionStatus[SoundAdditions.GridSnap]);

            ChangeGridSize(ConfigManager.sEditorGridSize, false);

            base.Initialize();

            AudioEngine.SetVolumeMusicFade(100);

            if (AudioEngine.ControlPoints.Count == 0)
                ChangeMode(EditorModes.Timing);
            else
                ChangeMode(lastMode != EditorModes.None ? lastMode : EditorModes.Compose);

            GameBase.EditorControl.MenuChanged(CurrentMode);

            if (GameBase.TestMode || GameBase.TestTime > 0)
            {
                AudioEngine.SeekTo(GameBase.TestTime);

                GameBase.TestTime = 0;
                GameBase.TestMode = false;
            }
            else if (beatmap.LastEditTime > 0)
                AudioEngine.SeekTo(beatmap.LastEditTime);
            else
                AudioEngine.SeekTo(0);

            ChangeAudioRate(AudioEngine.CurrentPlaybackRate);
            GameBase.EditorControl.TimeSignatureChanged(AudioEngine.timeSignature); //This needs to be placed after ChangeAudioRate

            NotificationManager.ClearMessageMassive(null, null);

            GameBase.TransitionManager.AllowParallax = false;

            GameBase.LoadComplete();
        }

        void GameBase_OnFocusLost()
        {
            if (IsDragging)
                onEndDrag(null, null);
        }

        private void AudioEngine_ActiveInheritedTimingPointChanged(ControlPoint tp)
        {
        }

        protected override void Dispose(bool disposing)
        {
            AudioEngine.ResetAudioTrack();

            Beatmap beatmap = hitObjectManager.Beatmap;
            if (beatmap != null)
            {
                //Force the rechecking of current map after exiting editor (since this generally isn't done at the song selections screen).
                List<Beatmap> beatmaps = BeatmapManager.Beatmaps.FindAll(b => beatmap.RelativeContainingFolder == b.RelativeContainingFolder);

                foreach (Beatmap b in beatmaps)
                    b.HeadersLoaded = false;

                //Have to process current here else it is too late when we actually do process it (HoverLoad).
                beatmap.ProcessHeaders();

                //don't change the submission status until after we have processed headers.
                //if we do it before-hand, it will cause folder names to be read for beatmapSetIds in cases they shouldn't.
                foreach (Beatmap b in beatmaps)
                {
                    b.SubmissionStatus = SubmissionStatus.Unknown;
                }

                // Re-gain information about this beatmap set
                BeatmapManager.GetOnlineBeatmapInfo(beatmaps);
            }

            if (currentMode != null)
                currentMode.Leave();

            Instance = null;

            if (timelineManager != null)
                timelineManager.Dispose();
            if (uiManager != null)
                uiManager.Dispose();
            if (spriteManagerSeekbar != null)
                spriteManagerSeekbar.Dispose();
            if (hitObjectManager != null)
                hitObjectManager.Dispose();

            GameBase.ResizeGamefield(1, false);

            MouseManager.ChangeCursor();

            if (Timing != null)
                Timing.Dispose();
            if (Compose != null)
                Compose.Dispose();
            if (Design != null)
                Design.Dispose();

            SampleImport.Clear();

            ConfigManager.sEditorBeatDivisor.Value = beatSnapDivisor;
            ConfigManager.sEditorGridSize.Value = GridSize;

            AiModClose();

            GameBase.OnFocusLost -= GameBase_OnFocusLost;

            AudioEngine.ActiveInheritedTimingPointChanged -= AudioEngine_ActiveInheritedTimingPointChanged;

            KeyboardHandler.OnKeyRepeat -= Game1_OnKeyRepeat;

            base.Dispose(disposing);
        }

        private bool onClickUp(object sender, EventArgs e)
        {
            toolWindowOpening = false;
            return currentMode.OnClickUp();
        }

        internal void BreakTimeFix(bool remove)
        {
            BreakTimeFix(remove, false);
        }

        private const int CUSTOM_BREAK_TIME = 5000; // maximum a break can be offset from its usual timing
        private const int PREEMPT_START = 200; // Do not change

        internal void BreakTimeFix(bool remove, bool find_custom)
        {
            if (hitObjectManager.hitObjects.Count == 0 && eventManager.eventBreaks.Count > 0)
            {
                //The case where there are no hitobjects.  We should get rid of all breaks.
                eventManager.RemoveAllBreaks();
                return;
            }

            List<EventBreak> invalidBreaks = new List<EventBreak>();

            int lastBreakEnd = 0;

            foreach (EventBreak b in eventManager.eventBreaks)
            {
                int hitCircleAfterMiddle = hitObjectManager.BreakObjectAfter(b);

                int calcStartTime = b.StartTime;
                int calcEndTime = b.EndTime;

                if (hitCircleAfterMiddle > 0)
                {
                    // For modes with concurrent hitobjects, find the actual hitobject that ends before that break.
                    int previousHitCircleEndTime = hitObjectManager.hitObjects[hitCircleAfterMiddle - 1].EndTime;
                    for (int i = hitCircleAfterMiddle - 1; i >= 0; i--)
                    {
                        int endTime = hitObjectManager.hitObjects[i].EndTime;
                        if (endTime > previousHitCircleEndTime)
                            previousHitCircleEndTime = endTime;
                    }

                    calcStartTime = previousHitCircleEndTime + PREEMPT_START;
                }

                if (hitCircleAfterMiddle < hitObjectManager.hitObjectsCount)
                {
                    calcEndTime = hitObjectManager.hitObjects[hitCircleAfterMiddle].StartTime - hitObjectManager.PreEmpt;
                }

                if (remove)
                {
                    if (b.EndTime < hitObjectManager.hitObjects[0].StartTime || b.StartTime > hitObjectManager.hitObjects[hitObjectManager.hitObjectsCount - 1].EndTime)
                        invalidBreaks.Add(b);
                }

                if (remove && (b.StartTime < lastBreakEnd || calcEndTime < calcStartTime))
                    invalidBreaks.Add(b);
                else
                {
                    if (find_custom)
                    {
                        if (b.StartTime != calcStartTime)
                            b.CustomStart = true;
                        if (b.EndTime != Math.Max(calcStartTime, calcEndTime))
                            b.CustomEnd = true;
                    }

                    if (!b.CustomStart)
                        b.StartTime = calcStartTime;
                    else if (b.StartTime < calcStartTime)
                    {
                        // The break start collides with an object: make it sticky again.
                        b.StartTime = calcStartTime;
                        b.CustomStart = false;
                    }
                    else if (b.StartTime - CUSTOM_BREAK_TIME + PREEMPT_START > calcStartTime)
                    {
                        // The break is delayed too much so start dragging it along anyway.
                        b.StartTime = calcStartTime + CUSTOM_BREAK_TIME - PREEMPT_START;
                    }

                    if (!b.CustomEnd)
                        b.EndTime = Math.Max(b.StartTime, calcEndTime);
                    else if (b.EndTime > calcEndTime)
                    {
                        // The break end collides with an object: make it sticky again.
                        // todo: Allow a break to include a bit of PreEmpt time??
                        b.EndTime = Math.Max(b.StartTime, calcEndTime);
                        b.CustomEnd = false;
                    }
                    else if (b.EndTime + CUSTOM_BREAK_TIME - hitObjectManager.PreEmpt < calcEndTime)
                    {
                        // The break is delayed too much so start dragging it along anyway.
                        b.EndTime = Math.Max(b.StartTime, calcEndTime) - CUSTOM_BREAK_TIME + hitObjectManager.PreEmpt;
                    }
                    //Prevents break from getting too short and exceeding past bordering hitobjects
                    if (calcEndTime + hitObjectManager.PreEmpt - b.StartTime - PREEMPT_START < 50)
                    {
                        remove = true;
                        invalidBreaks.Add(b);
                    }
                    else if (b.EndTime - b.StartTime < 50)
                    {
                        // if only one end is custom, stop shrinking the break right here.
                        if (!b.CustomStart && b.CustomEnd)
                            b.EndTime = b.StartTime + 50;
                        else if (b.CustomStart && !b.CustomEnd)
                            b.StartTime = b.EndTime - 50;
                    }

                    //TVO: This was causing the start of breaks to escape the gap when it got too short
                    /*if (b.EndTime - b.StartTime < 50)
                    {
                        // still unacceptably short so reset both sides.
                        // this also activates when both sides are custom, causing the side the user's not dragging
                        // to snap back to its default position, offering a little more room to drag.
                        b.StartTime = calcStartTime;
                        b.EndTime = calcEndTime;
                        b.CustomStart = false;
                        b.CustomEnd = false;
                    } */

                    lastBreakEnd = b.EndTime;
                }
            }

            if (remove)
            {
                foreach (EventBreak e in invalidBreaks)
                    eventManager.Remove(e);

                if (Compose != null)
                {
                    for (int i = hitObjectManager.hitObjectsCount - 1; i > 0; i--)
                    {
                        int breakStartTime = 0;
                        int breakEndTime = hitObjectManager.hitObjects[i].StartTime;
                        bool insertBreak = false;

                        for (int j = i - 1; j >= 0; j--)
                        {
                            int endTime = hitObjectManager.hitObjects[j].EndTime;
                            if (endTime > breakStartTime)
                            {
                                breakStartTime = endTime;
                                insertBreak = breakEndTime - breakStartTime > MAX_NO_BREAK_TIME;
                                if (!insertBreak)
                                    break;
                            }
                        }

                        if (insertBreak && eventManager.eventBreaks.Find(e => ((e.StartTime >= breakStartTime && e.StartTime <= breakEndTime) || (e.EndTime >= breakStartTime && e.EndTime <= breakEndTime))) == null)
                            Compose.BreakTimeInsert(breakStartTime + (breakEndTime - breakStartTime) / 2, false);
                    }
                }
            }
        }

        public bool IsUnderNoBreakTime(Event breakEvent)
        {
            if (breakEvent == null) return false;

            return (breakEvent.EndTime + CUSTOM_BREAK_TIME - (hitObjectManager.PreEmpt * 2) - breakEvent.StartTime <= MAX_NO_BREAK_TIME);
        }

        public override void Update()
        {
            if (eventManager == null)
                return; //We haven't initialized properly

            base.Update();

            eventManager.Update();

            if (!GameBase.IsActive)
                IsDragging = false;

            Compose.RemovePlacementObject();
            currentMode.Update();

            //Perform updates to the timeline contents.
            UpdateTimeline();

            InGamefield = GameBase.IsDisplayInGamefield(InputManager.CursorPosition) && GameBase.ActiveDialog == null;
            InTimeline = TimelineRectangle.Contains(new Point(InputManager.CursorPoint.X, InputManager.CursorPoint.Y)) && GameBase.ActiveDialog == null;
            InSeekbar = seekbarRectangle.Contains(InputManager.CursorPoint);

            if (!IsDragging)
            {
                //Not dragging
                DragCaptured = DragCapture.None;
            }

            if (DragCaptured == DragCapture.SeekbarFollow && !ChatEngine.IsVisible &&
                (GameBase.Time % 125 < 16 || AudioEngine.AudioState == AudioStates.Stopped))
                AudioEngine.SeekTo((int)(AudioEngine.AudioLength * ((float)(InputManager.CursorPoint.X - seekbarRectangle.Left) / seekbarRectangle.Width)));



            //Update the Kiai alert.
            if (GameBase.SixtyFramesPerSecondFrame)
            {
                bool status = AudioEngine.KiaiEnabled;

                if (status != kiaiAlertStatus)
                {
                    kiaiAlertStatus = status;
                    if (status)
                    {
                        kiaiAlert.Transformations.RemoveAll(t => t.Type == TransformationType.MovementX);
                        kiaiAlert.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                                         GameBase.Time, GameBase.Time + 160,
                                                                         EasingTypes.Out));
                        kiaiAlert.Transformations.Add(new Transformation(TransformationType.MovementX, -40, 0,
                                                                         GameBase.Time, GameBase.Time + 160,
                                                                         EasingTypes.Out));

                        kiaiAlertAdd.FadeTo(0.4f, 100);
                        kiaiAlertAdd.Transformations.RemoveAll(t => t.Type == TransformationType.MovementX);
                        kiaiAlertAdd.Transformations.Add(new Transformation(TransformationType.MovementX, -40, 0,
                                                                         GameBase.Time, GameBase.Time + 160,
                                                                         EasingTypes.Out));

                    }
                    else
                    {
                        kiaiAlert.FadeOut(100);
                        kiaiAlert.Transformations.RemoveAll(t => t.Type == TransformationType.MovementX);
                        kiaiAlert.Transformations.Add(new Transformation(TransformationType.MovementX, 0, -40,
                                                                         GameBase.Time, GameBase.Time + 160,
                                                                         EasingTypes.Out));
                        kiaiAlertAdd.FadeOut(100);
                        kiaiAlertAdd.Transformations.RemoveAll(t => t.Type == TransformationType.MovementX);
                        kiaiAlertAdd.Transformations.Add(new Transformation(TransformationType.MovementX, 0, -40,
                                                                             GameBase.Time, GameBase.Time + 160,
                                                                             EasingTypes.Out));
                    }
                }

                if (status && kiaiAlertAdd.Transformations.Count == 0)
                {
                    kiaiAlertAdd.Alpha = 0.4f - (float)AudioEngine.SyncBeatProgress * 0.6f;
                }

            }

            //Bookmark buttons
            if (seekbarRectangle.Contains(InputManager.CursorPoint) && !IsSubmitting)
            {
                if (seekAdd.Transformations.Count == 0)
                {
                    seekAdd.FadeIn(200);
                    seekDel.FadeIn(200);
                    seekPrev.FadeIn(200);
                    seekNext.FadeIn(200);
                    seekReset.FadeIn(200);
                }
                bookmarkMenuActive = false;
            }
            else if (!seekbarRectangle2.Contains(InputManager.CursorPoint) && seekAdd.Alpha == 1)
            {
                seekAdd.FadeOut(200);
                seekDel.FadeOut(200);
                seekPrev.FadeOut(200);
                seekNext.FadeOut(200);
                seekReset.FadeOut(200);

                bookmarkMenuActive = false;
            }
            else if (seekAdd.IsVisible)
            {
                Compose.RemovePlacementObject();
                bookmarkMenuActive = true;
            }
            else
                bookmarkMenuActive = false;

            //Update bottom controls
            if (GameBase.SixtyFramesPerSecondFrame)
            {
                if (AudioEngine.Time >= 0)
                {
                    timeText.Text =
                        String.Format("{0:00}:{1:00}:{2:000}", (AudioEngine.Time / 60000), AudioEngine.Time % 60000 / 1000,
                                      Math.Max(0, AudioEngine.Time) % 1000);

                }
                else
                {
                    timeText.Text =
                        String.Format("-{0:00}:{1:00}:{2:000}", (-AudioEngine.Time / 60000), -AudioEngine.Time % 60000 / 1000,
                                      Math.Max(0, -AudioEngine.Time) % 1000);
                    timeText2.Text = "intro";
                }

                if (AudioEngine.Time < 0)
                    timeText2.Text = "intro";
                else if (AudioEngine.Time > AudioEngine.AudioLength)
                    timeText2.Text = "outro";
                else
                    timeText2.Text = string.Format("{0:0.0}%", (float)AudioEngine.Time / AudioEngine.AudioLength * 100);

            }

            spriteSeekbarDragger.Position = new Vector2(
                seekbarRectangle.Left +
                ((float)AudioEngine.Time / AudioEngine.AudioLength) * seekbarRectangle.Width,
                seekbarRectangle.Top + seekbarRectangle.Height / 2);
        }

        private void UpdateTimeline()
        {
            //Fill circles into the timeline
            //Because NativeText is expensive, disposing is
            //handled internally in EditorModeTiming
            timelineManager.Clear(false);

            //At a magnification of 1, we will show 6seconds total
            TimelineDisplayRange = (int)(6000 / TimelineMagnification);
            TimelineLeftBound = AudioEngine.Time - TimelineDisplayRange / 2;
            TimelineRightBound = AudioEngine.Time + TimelineDisplayRange / 2;

            currentMode.UpdateTimeline();
        }


        internal void SeekNextTimingPoint()
        {
            if (AudioEngine.ActiveInheritedTimingPointIndex == AudioEngine.ControlPoints.Count - 1)
                return;
            AudioEngine.SeekTo((int)AudioEngine.ControlPoints[AudioEngine.ActiveInheritedTimingPointIndex + 1].offset);
        }

        internal void SeekLastTimingPoint()
        {
            if (AudioEngine.Time - AudioEngine.ControlPoints[AudioEngine.ActiveInheritedTimingPointIndex].offset > 1000)
            {
                AudioEngine.SeekTo((int)AudioEngine.ControlPoints[AudioEngine.ActiveInheritedTimingPointIndex].offset);
                return;
            }
            if (AudioEngine.ActiveInheritedTimingPointIndex == 0)
                return;
            AudioEngine.SeekTo((int)AudioEngine.ControlPoints[AudioEngine.ActiveInheritedTimingPointIndex - 1].offset);
        }


        public override void Draw()
        {
            if (eventManager == null)
                return;

            base.Draw();

            //song background scripting
            eventManager.DrawBG();
            eventManager.DrawFG();

            if (CurrentMode != EditorModes.Timing && LoadedMode != PlayModes.OsuMania)
                hitObjectManager.Draw();

            DrawGrid();

            if (aiMod != null)
                aiMod.Draw();

            uiManager.Draw();

            //Draw the timeline
            DrawTimeline();

            DrawSeekbar();

            spriteManagerSeekbar.Draw();

            currentMode.Draw();
        }

        private float seekbarPositionFromTime(double time)
        {
            return seekbarRectangle.X + seekbarRectangle.Width * ((float)time / AudioEngine.AudioLength);
        }

        static Color colourSeekbarKiai = new Color(255, 146, 18, 140);
        static Color colourSeekbarBreak = new Color(255, 255, 255, 140);
        static Color colourSeekbarBookmark = new Color(58, 110, 170, 240);
        static Color colourSeekbarMainLine = new Color(255, 255, 255, 128);
        static Color colourSeekbarTimingChange = new Color(180, 20, 20, 150);
        static Color colourSeekbarTimingNoChange = new Color(156, 255, 0, 100);
        static Color colourPreviewTime = new Color(255, 247, 17, 150);
        private void DrawSeekbar()
        {
            List<ControlPoint> tps = AudioEngine.ControlPoints;

            if (tps == null)
                return;

            //Kiai Time...
            for (int i = 0; i < tps.Count; i++)
            {
                ControlPoint tp = tps[i];
                if (tp.kiaiMode)
                {
                    float start = seekbarPositionFromTime(tp.offset);
                    float end = seekbarPositionFromTime(i == tps.Count - 1 ? AudioEngine.AudioLength : tps[i + 1].offset);

                    GameBase.LineManager.DrawQuad(
                        new Vector2(start, seekbarRectangle.Top + seekbarRectangle.Height * 0.36f),
                        new Vector2(end, seekbarRectangle.Top + seekbarRectangle.Height * 0.64f),
                        colourSeekbarKiai);
                }
            }

            //Breaks...
            foreach (EventBreak e in eventManager.eventBreaks)
            {
                float start = seekbarPositionFromTime(e.StartTime);
                float end = seekbarPositionFromTime(e.EndTime);

                GameBase.LineManager.DrawQuad(
                    new Vector2(start, seekbarRectangle.Top + seekbarRectangle.Height * 0.4f),
                    new Vector2(end, seekbarRectangle.Top + seekbarRectangle.Height * 0.6f),
                    colourSeekbarBreak);
            }

            float middleLine = seekbarRectangle.Top + seekbarRectangle.Height / 2;
            //seekbar line
            GameBase.primitiveBatch.Begin();
            GameBase.primitiveBatch.AddVertex(
                new Vector2(seekbarRectangle.Left, middleLine), colourSeekbarMainLine);
            GameBase.primitiveBatch.AddVertex(
                new Vector2(seekbarRectangle.Right, middleLine), colourSeekbarMainLine);


            //Preview Point...
            Beatmap beatmap = hitObjectManager.Beatmap;
            if (beatmap.PreviewTime >= 0)
            {
                float start = seekbarPositionFromTime(beatmap.PreviewTime);

                GameBase.primitiveBatch.AddVertex(new Vector2(start, seekbarRectangle.Top), colourPreviewTime);
                GameBase.primitiveBatch.AddVertex(new Vector2(start, seekbarRectangle.Top + seekbarRectangle.Height), colourPreviewTime);
            }

            foreach (int i in hitObjectManager.Bookmarks)
            {
                float start = seekbarPositionFromTime(i);

                GameBase.primitiveBatch.AddVertex(new Vector2(start, seekbarRectangle.Top + seekbarRectangle.Height / 2), colourSeekbarBookmark);
                GameBase.primitiveBatch.AddVertex(new Vector2(start, seekbarRectangle.Top + seekbarRectangle.Height), colourSeekbarBookmark);
            }

            //Control Points
            for (int i = 0; i < tps.Count; i++)
            {
                ControlPoint tp = tps[i];
                float start = seekbarPositionFromTime(tp.offset);
                Color c = tp.timingChange ? colourSeekbarTimingChange : colourSeekbarTimingNoChange;
                GameBase.primitiveBatch.AddVertex(new Vector2(start, seekbarRectangle.Top), c);
                GameBase.primitiveBatch.AddVertex(new Vector2(start, seekbarRectangle.Top + seekbarRectangle.Height / 2), c);
            }

            GameBase.primitiveBatch.End();
        }

        static Color colourGridLine1 = new Color(255, 255, 255, 25);
        static Color colourGridLine2 = new Color(255, 255, 255, 75);

        private void DrawGrid()
        {
            //Draw the grid
            if (GridSize < 1)
                return;

            float gridIncrement = GameBase.GamefieldRatio * GridSize;
            Vector2 offset1 = GameBase.GamefieldOffsetVector1;
            GameBase.primitiveBatch.Begin();
            if (hitObjectManager.Beatmap.PlayMode != PlayModes.OsuMania)
            {
                bool midPoint = false;
                for (float i = 0; i <= GameBase.GamefieldWidth + 1; i += gridIncrement)
                {
                    if (!midPoint && i >= GameBase.GamefieldWidth / 2 - 1)
                    {
                        midPoint = true;
                        GameBase.primitiveBatch.AddVertex(new Vector2(i, 0) + offset1, colourGridLine2);
                        GameBase.primitiveBatch.AddVertex(new Vector2(i, GameBase.GamefieldHeight) + offset1, colourGridLine2);
                    }
                    else
                    {
                        GameBase.primitiveBatch.AddVertex(new Vector2(i, 0) + offset1, colourGridLine1);
                        GameBase.primitiveBatch.AddVertex(new Vector2(i, GameBase.GamefieldHeight) + offset1, colourGridLine1);
                    }
                }
                midPoint = false;
                for (float i = 0; i <= GameBase.GamefieldHeight + 1; i += gridIncrement)
                {
                    if (!midPoint && i >= GameBase.GamefieldHeight / 2 - 1)
                    {
                        midPoint = true;
                        GameBase.primitiveBatch.AddVertex(new Vector2(0, i) + offset1, colourGridLine2);
                        GameBase.primitiveBatch.AddVertex(new Vector2(GameBase.GamefieldWidth, i) + offset1, colourGridLine2);
                    }
                    else
                    {
                        GameBase.primitiveBatch.AddVertex(new Vector2(0, i) + offset1, colourGridLine1);
                        GameBase.primitiveBatch.AddVertex(new Vector2(GameBase.GamefieldWidth, i) + offset1, colourGridLine1);
                    }
                }
            }

            if (CurrentMode == EditorModes.Design)
            {
                float width = (int)(GameBase.WindowWidth * GameBase.GamefieldRatioWindowIndependent);
                float height = (int)(GameBase.WindowHeight * GameBase.GamefieldRatioWindowIndependent);
                float offx = (GameBase.WindowWidth - width) / 2 + GameBase.WindowOffsetX;
                float offy = (GameBase.WindowHeight - height) / 4 * 3 + GameBase.WindowOffsetY;
                width = 640 * GameBase.GamefieldRatio;
                height = 480 * GameBase.GamefieldRatio;

                GameBase.primitiveBatch.AddVertex(new Vector2(offx, offy), colourGridLine1);
                GameBase.primitiveBatch.AddVertex(new Vector2(offx + width, offy), colourGridLine1);
                GameBase.primitiveBatch.AddVertex(new Vector2(offx + width, offy), colourGridLine1);
                GameBase.primitiveBatch.AddVertex(new Vector2(offx + width, offy + height), colourGridLine1);
                GameBase.primitiveBatch.AddVertex(new Vector2(offx + width, offy + height), colourGridLine1);
                GameBase.primitiveBatch.AddVertex(new Vector2(offx, offy + height), colourGridLine1);
                GameBase.primitiveBatch.AddVertex(new Vector2(offx, offy + height), colourGridLine1);
                GameBase.primitiveBatch.AddVertex(new Vector2(offx, offy), colourGridLine1);
            }

            GameBase.primitiveBatch.End();
        }

        internal void Clear()
        {
            DialogResult r = MessageBox.Show("Are you sure you want to clear all objects in the current difficulty?", "osu!",
                                             MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                                             MessageBoxDefaultButton.Button1);
            switch (r)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.OK:
                    hitObjectManager.Clear();
                    HistoryClear();
                    break;
            }

            Dirty = true;
        }

        #endregion

        private static Rectangle seekbarRectangle2;
        internal EditorModes CurrentMode = EditorModes.None;
        internal int GridSize;
        private bool HelpDisplayed;
        private pSprite[] helpScreens;
        private pSprite kiaiAlert;
        private pSprite kiaiAlertAdd;
        private bool kiaiAlertStatus;
        internal bool liveMapping = false;
        internal bool showSampleName = false;

        private pSprite rateArrow;

        private pSprite spriteSeekbarDragger;
        private Rectangle seekbarRectangle;
        private pSprite seekDel;
        private bool signal;


        private pText timeText;
        private pText timeText2;

        private pSprite topmenuSelection;
        internal SpriteManager uiManager;
        internal SpriteManager spriteManagerSeekbar;

        #region Placement

        #endregion

        #region Storyboarding

        internal AiModWindow aiMod;

        private static bool isSubmitting;
        internal static bool IsSubmitting
        {
            get { return isSubmitting; }
            set
            {
                if (isSubmitting == value)
                    return;

                isSubmitting = value;
                if (IsSubmitting && !AudioEngine.Paused)
                    AudioEngine.TogglePause();
                GameBase.EditorControl.Enabled = !value;
                Editor.Instance.currentMode.Enabled = !value;
                Editor.Instance.uiManager.HandleInput = !value;
                Editor.Instance.spriteManagerSeekbar.HandleInput = !value;
                Editor.Instance.timelineManager.HandleInput = !value;
            }
        }

        #endregion

        //todo: what the fucK!

        internal void ChangeAudioRate(int i)
        {
            if (i < 25 || i > 100)
                return;

            bool largeJump = Math.Abs(AudioEngine.CurrentPlaybackRate - i) > 10;

            AudioEngine.CurrentPlaybackRate = i;
            if (rateArrow != null)
            {
                float xpos = (float)(i - 25) / 75 * 66 + 567;
                rateArrow.MoveTo(new Vector2(640 - xpos, 473), largeJump ? 300 : 80); //adjusted to 640-xpos to account for Right(starts at 640X)
                GameBase.EditorControl.AudioRateChanged(i);
            }
        }

        internal void ChangeGridSize(GridSizes size)
        {
            ChangeGridSize(size, true);
        }

        internal void ChangeGridSize(GridSizes size, bool notifyUser)
        {
            ChangeGridSize((int)size, notifyUser);
        }


        internal void ChangeGridSize(int size)
        {
            ChangeGridSize(size, true);
        }

        internal void ChangeGridSize(int size, bool notifyUser)
        {
            GridSize = size;
            ConfigManager.sEditorGridSize.Value = GridSize;
            if (notifyUser)
                NotificationManager.ShowMessageMassive("Grid Size: " + ((GridSizes)size), 1000);
            GameBase.EditorControl.GridSizeChanged((GridSizes)size);
        }

        private void InitializeMenus()
        {
            uiManager = new SpriteManager(true);
            spriteManagerSeekbar = new SpriteManager(true);

            timeText = new pText(string.Empty, 14, new Vector2(10, 463), 1, true, Color.White);
            timeText.Origin = Origins.TopLeft;
            timeText.HandleInput = true;
            timeText.ToolTip = "Jump to specific time.";
            timeText.OnClick += timeText_OnClick;


            uiManager.Add(timeText);

            kiaiAlert = new pSprite(SkinManager.Load(@"editor-kiai", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                    Clocks.Game, new Vector2(0, 66), 0.98F, true, Color.White, null);
            kiaiAlert.Alpha = 0;
            uiManager.Add(kiaiAlert);

            kiaiAlertAdd = new pSprite(SkinManager.Load(@"editor-kiai", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                    Clocks.Game, new Vector2(0, 66), 1F, true, Color.White, null);
            kiaiAlertAdd.Alpha = 0;
            kiaiAlertAdd.Additive = true;
            uiManager.Add(kiaiAlertAdd);


            timeText2 = new pText("", 12, new Vector2(70, 465f), 1, true, Color.White);
            uiManager.Add(timeText2);

            uiManager.Add(new pSprite(SkinManager.Load(@"editor-menu-bg-top", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                            Clocks.Game, Vector2.Zero, 0, true, Color.White, null)
            {
                VectorScale = new Vector2(1024f * GameBase.WindowWidthScaled / GameBase.WindowDefaultWidth, 1)
            });

            uiManager.Add(new pSprite(SkinManager.Load(@"editor-menu-bg-bottom", SkinSource.Osu), Fields.BottomLeft, Origins.BottomLeft,
                            Clocks.Game, Vector2.Zero, 0, true, Color.White, null)
            {
                VectorScale = new Vector2(1024f * GameBase.WindowWidthScaled / GameBase.WindowDefaultWidth, 1)
            });

            uiManager.Add(
                new pSprite(SkinManager.Load(@"topmenu-bg", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                            Clocks.Game,
                            Vector2.Zero, 0.1F, true, Color.White, null));

            topmenuSelection =
                new pSprite(SkinManager.Load(@"topmenu-selection", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                            Clocks.Game, Vector2.Zero, 0.8F, true, Color.White, null);
            uiManager.Add(topmenuSelection);

            if (GameBase.NewGraphicsAvailable)
                topmenuSelection.Additive = true;

            //Help Screen
            helpScreens = new[]
                              {
                                  null,
                                  new pSprite(SkinManager.Load(@"editor-compose-help", SkinSource.Osu), Fields.TopLeft,
                                              Origins.Centre,
                                              Clocks.Game, new Vector2(320+GameBase.WindowOffsetXScaled,240), 1, true, Color.TransparentWhite), null,
                                  null
                              };
            uiManager.Add(helpScreens);

            pText pt;

            //Timeline Adjustment Controls
            pSprite p =
                new pSprite(SkinManager.Load(@"editor-timeline-plus", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(10, 37), 1, true,
                            Color.White, null);
            p.HandleInput = true;
            p.ClickEffect = new Transformation(TransformationType.Scale, 1.1F, 1, 0, 200);
            p.OnClick += timelinePlus_OnClick;
            p.ToolTip = "Increase timeline zoom";
            uiManager.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-timeline-minus", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(10, 53), 1, true,
                            Color.White, null);
            p.HandleInput = true;
            p.ClickEffect = new Transformation(TransformationType.Scale, 1.1F, 1, 0, 200);
            p.ToolTip = "Decrease timeline zoom";
            p.OnClick += timelineMinus_OnClick;
            uiManager.Add(p);

            float scale = DpiHelper.DPI(GameBase.Form) / 96.0f;
            pt =
                new pText(GetTip(), 10 * scale, new Vector2(70, 70),
                          new Vector2(GameBase.WindowWidth - 140 * GameBase.WindowRatio, GameBase.GamefieldHeight), 1, false,
                          Color.TransparentWhite, true);
            pt.ExactCoordinates = true;
            pt.TextRenderSpecific = false;
            pt.TextAlignment = TextAlignment.Centre;
            pt.TextAa = false;
            pt.Transformations.Add(
                new Transformation(TransformationType.Fade, 1, 1, GameBase.Time, GameBase.Time + 10000));
            pt.Transformations.Add(
                new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + 10000, GameBase.Time + 10500));
            uiManager.Add(pt);

            spriteSeekbarDragger =
                new pSprite(SkinManager.Load(@"seekbar-over", SkinSource.Osu), Fields.NativeStandardScale, Origins.Centre,
                            Clocks.Game,
                            new Vector2(seekbarRectangle.Left, seekbarRectangle.Top + seekbarRectangle.Height / 2), 1,
                            true, Color.White, null);
            spriteSeekbarDragger.Additive = true;
            spriteManagerSeekbar.Add(spriteSeekbarDragger);

            int bookmarkOffset = 0;
            if (hitObjectManager.Beatmap.PlayMode == PlayModes.OsuMania)
            {
                // lanes are a fixed width in mania editor, so no need to account for column size/offset
                bookmarkOffset = (int)(25 + (30 * hitObjectManager.Beatmap.DifficultyCircleSize));
                seekbarRectangle2.Width = Math.Max(400 + bookmarkOffset, seekbarRectangle2.Width);
            }

            seekAdd =
                new pSprite(SkinManager.Load(@"editor-timeline-plus", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(150 + bookmarkOffset, 450), 1, true,
                            Color.TransparentWhite, "bookmark");
            seekAdd.HandleInput = true;
            seekAdd.ClickEffect = new Transformation(TransformationType.Scale, 1.1F, 1, 0, 200);
            seekAdd.OnClick += bookmarkAdd_OnClick;
            seekAdd.ToolTip = "Add new bookmark at current location (Ctrl+B)";
            uiManager.Add(seekAdd);

            seekDel =
                new pSprite(SkinManager.Load(@"editor-timeline-minus", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(170 + bookmarkOffset, 450), 1, true,
                            Color.TransparentWhite, "bookmark");
            seekDel.HandleInput = true;
            seekDel.ClickEffect = new Transformation(TransformationType.Scale, 1.1F, 1, 0, 200);
            seekDel.ToolTip = "Remove bookmark at current location (Ctrl+Shift+B)";
            seekDel.OnClick += bookmarkDelete_OnClick;
            uiManager.Add(seekDel);

            seekPrev =
                new pSprite(SkinManager.Load(@"editor-timeline-left", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(190 + bookmarkOffset, 450), 1, true,
                            Color.TransparentWhite, "bookmark");
            seekPrev.HandleInput = true;
            seekPrev.ClickEffect = new Transformation(TransformationType.Scale, 1.1F, 1, 0, 200);
            seekPrev.ToolTip = "Seek to previous bookmark (Alt-LeftArrow)";
            seekPrev.OnClick += bookmarkPrev_OnClick;
            uiManager.Add(seekPrev);

            seekNext =
                new pSprite(SkinManager.Load(@"editor-timeline-right", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(210 + bookmarkOffset, 450), 1, true,
                            Color.TransparentWhite, "bookmark");
            seekNext.HandleInput = true;
            seekNext.ToolTip = "Seek to next bookmark (Alt-RightArrow)";
            seekNext.ClickEffect = new Transformation(TransformationType.Scale, 1.1F, 1, 0, 200);
            seekNext.OnClick += bookmarkNext_OnClick;
            uiManager.Add(seekNext);

            seekReset =
                new pSprite(SkinManager.Load(@"editor-timeline-reset", SkinSource.Osu), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game, new Vector2(242 + bookmarkOffset, 450), 1, true,
                            Color.TransparentWhite, "bookmark");
            seekReset.HandleInput = true;
            seekReset.ToolTip = "Reset all bookmarks";
            seekReset.ClickEffect = new Transformation(TransformationType.Scale, 1.1F, 1, 0, 200);
            seekReset.OnClick += bookmarkReset_OnClick;
            uiManager.Add(seekReset);

            rateArrow =
                new pSprite(SkinManager.Load(@"editor-rate-arrow", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                            Clocks.Game, new Vector2(18, 483), 1, true, Color.White, null);
            uiManager.Add(rateArrow);

            Transformation t = new Transformation(TransformationType.Fade, 0.5f, 1, 0, 100);
            t.Easing = EasingTypes.Out;

            p =
                new pSprite(SkinManager.Load(@"editor-rate-25", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                            Clocks.Game, new Vector2(85, 470), 1, true, new Color(255, 255, 255, 128), null);
            p.HandleInput = true;
            p.HoverEffect = t;
            p.Tag = 25;
            p.OnClick += rateChangeButton;
            uiManager.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-rate-50", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                            Clocks.Game, new Vector2(62, 470), 1, true, new Color(255, 255, 255, 128), null);
            p.HandleInput = true;
            p.Tag = 50;
            p.HoverEffect = t;
            p.OnClick += rateChangeButton;
            uiManager.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-rate-75", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                            Clocks.Game, new Vector2(41, 470), 1, true, new Color(255, 255, 255, 128), null);
            p.HandleInput = true;
            p.Tag = 75;
            p.OnClick += rateChangeButton;
            p.HoverEffect = t;
            uiManager.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-rate-100", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                            Clocks.Game, new Vector2(18, 470), 1, true, new Color(255, 255, 255, 128), null);
            p.HandleInput = true;
            p.Tag = 100;
            p.OnClick += rateChangeButton;
            p.HoverEffect = t;
            uiManager.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-rate-bg", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                            Clocks.Game, new Vector2(77, 470), 1, true, Color.White, null);
            uiManager.Add(p);

            t = new Transformation(TransformationType.Scale, 1, 1.2F, 0, 100);
            t.Easing = EasingTypes.Out;

            Transformation t2 = new Transformation(TransformationType.Scale, 1.1F, 1.2F, 0, 100);
            t2.Easing = EasingTypes.Out;

            p =
                new pSprite(SkinManager.Load(@"editor-button-play", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                            Clocks.Game, new Vector2(245, 470), 1, true, Color.White, null);
            p.HandleInput = true;
            p.ClickEffect = t2;
            p.HoverEffect = t;
            p.OnClick += buttonPlay_OnClick;
            uiManager.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-button-pause", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                            Clocks.Game, new Vector2(225, 470), 1, true, Color.White, null);
            p.HandleInput = true;
            p.ClickEffect = t2;
            p.HoverEffect = t;
            p.OnClick += buttonPause_OnClick;
            uiManager.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-button-stop", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                            Clocks.Game, new Vector2(205, 470), 1, true, Color.White, null);
            p.HandleInput = true;
            p.ClickEffect = t2;
            p.HoverEffect = t;
            p.OnClick += buttonStop_OnClick;
            uiManager.Add(p);

            p =
                new pSprite(SkinManager.Load(@"editor-button-test", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                            Clocks.Game, new Vector2(175, 470), 1, true, Color.White, null);
            p.HandleInput = true;
            p.ToolTip =
                "Enter test mode, where you can play through from the current location.\nThis requires saving your beatmap.";
            p.ClickEffect = t2;
            p.HoverEffect = t;
            p.OnClick += buttonTest_OnClick;
            uiManager.Add(p);
        }

        private GotoTime gotoTime;
        private bool toolWindowOpening = false;

        private void timeText_OnClick(object sender, EventArgs e)
        {
            if (signal || (gotoTime != null))
                return;

            toolWindowOpening = true;
            gotoTime = new GotoTime(timeText.Text);
            gotoTime.FormClosed += delegate(object sender2, FormClosedEventArgs e2) { gotoTime = null; };
            gotoTime.ShowAtCursor(Origins.BottomLeft);
        }

        private void bookmarkPrev_OnClick(object sender, EventArgs e)
        {
            int f = -1;
            foreach (int i in hitObjectManager.Bookmarks)
            {
                if (i >= AudioEngine.Time - 500)
                    break;
                f = i;
            }
            if (f >= 0)
                AudioEngine.SeekTo(f);

        }

        private void bookmarkNext_OnClick(object sender, EventArgs e)
        {
            foreach (int i in hitObjectManager.Bookmarks)
                if (i > AudioEngine.Time + 50)
                {
                    AudioEngine.SeekTo(i);
                    break;
                }

        }

        private void bookmarkReset_OnClick(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(GameBase.Form, "Remove all bookmarks?", "osu!", MessageBoxButtons.OKCancel,
                                MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.OK)
            {
                hitObjectManager.Bookmarks.Clear();
                Instance.Dirty = true;
            }
        }

        private void bookmarkDelete_OnClick(object sender, EventArgs e)
        {
            int closest = -1;
            int distance = -1;

            for (int i = 0; i < Instance.hitObjectManager.Bookmarks.Count; i++)
            {
                int thisDistance = Math.Abs(Instance.hitObjectManager.Bookmarks[i] - AudioEngine.Time);
                if (thisDistance < 2000 && (thisDistance < distance || distance == -1))
                {
                    closest = i;
                    distance = thisDistance;
                }
            }

            if (closest > -1)
            {
                Instance.hitObjectManager.Bookmarks.RemoveAt(closest);
                Instance.Dirty = true;
            }
            Instance.hitObjectManager.Bookmarks.Sort();
        }

        private void bookmarkAdd_OnClick(object sender, EventArgs e)
        {
            if (!hitObjectManager.Bookmarks.Contains(Timing.BeatSnapValue(AudioEngine.Time)))
            {
                hitObjectManager.Bookmarks.Add(Timing.BeatSnapValue(AudioEngine.Time));
                Instance.Dirty = true;
            }
            hitObjectManager.Bookmarks.Sort();
        }

        private void buttonPlay_OnClick(object sender, EventArgs e)
        {
            if (AudioEngine.AudioState == AudioStates.Playing)
                AudioEngine.Play();
            else
                Pause();
        }

        private void buttonPause_OnClick(object sender, EventArgs e)
        {
            Pause();
        }

        private void buttonStop_OnClick(object sender, EventArgs e)
        {
            AudioEngine.Stop();
            foreach (HitObject h in hitObjectManager.hitObjects)
                h.StopSound();
        }


        private void buttonTest_OnClick(object sender, EventArgs e)
        {
            TestMode();
        }

        private void timelinePlus_OnClick(object sender, EventArgs e)
        {
            hitObjectManager.Beatmap.TimelineZoom = TimelineMagnification = OsuMathHelper.Clamp(TimelineMagnification + 0.1F, 0.1F, 8);
        }

        private void timelineMinus_OnClick(object sender, EventArgs e)
        {
            hitObjectManager.Beatmap.TimelineZoom = TimelineMagnification = OsuMathHelper.Clamp(TimelineMagnification - 0.1F, 0.1F, 8);
        }

        internal void ToggleHelp()
        {
            if (CurrentMode != EditorModes.Compose || hitObjectManager.Beatmap.PlayMode == PlayModes.OsuMania)
                return;

            if (!HelpDisplayed)
                helpScreens[1].FadeIn(100);
            else
                helpScreens[1].FadeOut(100);

            HelpDisplayed = !HelpDisplayed;
        }

        internal void ChangeMode(EditorModes menu)
        {
            if (HelpDisplayed)
                ToggleHelp();

            MouseManager.ChangeCursor();

            if (AudioEngine.ControlPoints.Count == 0 && menu != EditorModes.Timing)
            {
                MessageBox.Show(
                    "Please time the song first!  This is imperative to creating a good beatmap.  Read the guide on the site if you don't understand timing, or ask in the Beatmap Help forum.",
                    "denied", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (menu == CurrentMode)
                return;

            CurrentMode = menu;

            topmenuSelection.MoveTo(new Vector2(((int)CurrentMode - 1) * 100, 0), 300, EasingTypes.Out);

            if (currentMode != null)
                currentMode.Leave();

            //We do this here to stop sliders from continuing to play their sliding
            //sample after swapping to a mode where hitsounds are no longer playing.
            hitObjectManager.hitObjectsMinimal.ForEach(h => h.StopSound());

            switch (menu)
            {
                case EditorModes.Compose:
                    currentMode = Compose;
                    break;

                case EditorModes.Timing:
                    currentMode = Timing;
                    break;

                case EditorModes.Design:
                    currentMode = Design;
                    break;
            }

            currentMode.Enter();

            UpdateTimelineRectangle();

            GameBase.EditorControl.MenuChanged(menu);
        }


        private void rateChangeButton(object sender, EventArgs e)
        {
            ChangeAudioRate((int)((pSprite)sender).Tag);
            ((pSprite)sender).Transformations.Add(
                new Transformation(TransformationType.Scale, 1.2F, 1, GameBase.Time, GameBase.Time + 100));
        }

        /// <summary>
        /// Stops the mp3 from playing, but also silences any active hitobjects.
        /// </summary>
        internal void Pause()
        {
            AudioEngine.TogglePause();
            foreach (HitObject h in hitObjectManager.hitObjects)
                h.StopSound();
        }

        private static string GetTip()
        {
            int tipCount = (int)OsuString.EditorTip_LAST_DONT_TRANSLATE - (int)OsuString.EditorTip_1;
            int thisTip = ConfigManager.sEditorTip % tipCount;

            ConfigManager.sEditorTip.Value = (thisTip + 1) % tipCount;

            return LocalisationManager.GetString(OsuString.EditorTip_1 + thisTip);
        }

        internal void OnScreenSizeChanged()
        {
            UpdateTimelineRectangle();

            seekbarRectangle = new Rectangle((int)(110 * GameBase.WindowRatio),
                                             (int)(460 * GameBase.WindowRatio + GameBase.WindowOffsetY),
                                             (int)(GameBase.WindowRatio * (GameBase.WindowWidthScaled - 370)), (int)(GameBase.WindowRatio * 20));
            seekbarRectangle2 = new Rectangle((int)(110 * GameBase.WindowRatio),
                                              (int)(430 * GameBase.WindowRatio + GameBase.WindowOffsetY),
                                              (int)(GameBase.WindowRatio * (GameBase.WindowWidthScaled - 440)), (int)(GameBase.WindowRatio * 40));
        }

        private void UpdateTimelineRectangle()
        {
            TimelineRectangle = new Rectangle((int)(GameBase.WindowRatio * TimelineX),
                                              (int)(GameBase.WindowRatio * TimelineY + GameBase.WindowOffsetY),
                                              (int)(GameBase.WindowRatio * TimelineWidth),
                                              (int)(GameBase.WindowRatio * TimelineHeight));
        }

        #region Nested type: GridSizes

        internal enum GridSizes
        {
            Tiny = 4,
            Small = 8,
            Medium = 16,
            Large = 32
        } ;

        #endregion

        #region Selection Manipulation

        internal void CopySelection()
        {
            currentMode.CopySelection();
        }

        internal void PasteSelection()
        {
            currentMode.PasteSelection();
        }


        internal void SelectAll()
        {
            currentMode.SelectAll();
        }

        internal void SelectNone()
        {
            if (currentMode != null)
                currentMode.SelectNone();
        }

        #endregion

        #region Timeline

        internal readonly Color lineColourAlphaBlue = new Color(50, 128, 255, 225);
        internal readonly Color lineColourAlphaOrange = new Color(255, 255, 0, 225);
        internal readonly Color lineColourAlphaRed = new Color(255, 0, 0, 225);
        internal readonly Color lineColourAlphaViolet = new Color(200, 0, 200, 225);
        internal readonly Color lineColourAlphaWhite = new Color(255, 255, 255, 220);
        internal readonly Color lineColourAlphaMagenta = new Color(144, 64, 144, 225);
        internal readonly Color lineColourAlphaGrey = new Color(160, 160, 160, 225);
        internal readonly Color lineColourWhite = new Color(255, 255, 255, 255);
        internal readonly int TimelineHeight = 35;
        internal readonly int TimelineX = 20;
        internal readonly int TimelineY = 25;
        internal int beatSnapDivisor;
        internal DragCapture DragCaptured;

        internal double TickSpacing = 250;
        internal int TimelineDisplayRange;

        internal int TimelineLeftBound;
        internal int TimelineRightBound;

        private static float timelineMagnification = 1;

        internal SpriteManager timelineManager;
        internal Rectangle TimelineRectangle;
        internal int TimelineTickHeight = 10;
        internal int TimelineWidth = GameBase.WindowWidthScaled - 220;

        internal float TimelineMagnification
        {
            get { return timelineMagnification; }
            set
            {
                timelineMagnification = value;
                OnScreenSizeChanged();
            }
        }

        private void DrawTimeline()
        {
            if (AudioEngine.BeatSyncing && beatSnapDivisor > 0)
                TickSpacing = AudioEngine.beatLength / beatSnapDivisor;
            else
                TickSpacing = 250;

            currentMode.DrawTimelineLow();

            timelineManager.Draw();

            GameBase.primitiveBatch.Begin();

            if (CurrentMode != EditorModes.Timing && AudioEngine.ControlPoints != null)
            {
                int pt = hitObjectManager.Beatmap.PreviewTime;
                if (pt >= 0 && pt >= TimelineLeftBound && pt <= TimelineRightBound)
                {
                    float start = TimeToTimelinePos(pt);

                    GameBase.primitiveBatch.AddVertex(new Vector2(start, TimelineRectangle.Top), colourPreviewTime);
                    GameBase.primitiveBatch.AddVertex(new Vector2(start, TimelineRectangle.Bottom), colourPreviewTime);
                }

                foreach (ControlPoint t in AudioEngine.ControlPoints)
                {
                    if (t.offset >= TimelineLeftBound && t.offset <= TimelineRightBound)
                    {
                        Color c = t.timingChange ? new Color(180, 20, 20, 150) : new Color(156, 255, 0, 150);
                        float pos = TimeToTimelinePos(t.offset);
                        GameBase.primitiveBatch.AddVertex(
                            new Vector2(pos, TimelineRectangle.Top),
                            c);
                        GameBase.primitiveBatch.AddVertex(
                            new Vector2(pos, TimelineRectangle.Bottom),
                            c);
                    }
                }
            }


            //Base line
            GameBase.primitiveBatch.AddVertex(new Vector2(TimelineRectangle.Left, TimelineRectangle.Bottom),
                                              lineColourWhite);
            GameBase.primitiveBatch.AddVertex(new Vector2(TimelineRectangle.Right, TimelineRectangle.Bottom),
                                              lineColourWhite);

            GameBase.primitiveBatch.AddVertex(
                new Vector2(TimelineRectangle.Left + TimelineRectangle.Width / 2 - 1, TimelineRectangle.Top),
                lineColourWhite);
            GameBase.primitiveBatch.AddVertex(
                new Vector2(TimelineRectangle.Left + TimelineRectangle.Width / 2 - 1, TimelineRectangle.Bottom),
                lineColourWhite);

            GameBase.primitiveBatch.AddVertex(
                new Vector2(TimelineRectangle.Left + TimelineRectangle.Width / 2 + 1, TimelineRectangle.Top),
                lineColourWhite);
            GameBase.primitiveBatch.AddVertex(
                new Vector2(TimelineRectangle.Left + TimelineRectangle.Width / 2 + 1, TimelineRectangle.Bottom),
                lineColourWhite);

            //Ticks
            double FirstVisibleTick;
            int firstTickCount;

            if (AudioEngine.BeatSyncing && AudioEngine.beatLength > 0)
            {
                firstTickCount = (int)Math.Floor((TimelineLeftBound - AudioEngine.CurrentOffset) / AudioEngine.beatLength);
                FirstVisibleTick = firstTickCount * AudioEngine.beatLength + AudioEngine.CurrentOffset;
            }
            else
            {
                firstTickCount = 0;
                FirstVisibleTick = TimelineLeftBound / TickSpacing;
            }


            int timeSig = AudioEngine.ActiveInheritedTimingPointIndex < 0 ? 4 : (int)AudioEngine.ControlPoints[AudioEngine.ActiveInheritedTimingPointIndex].timeSignature;

            if (TickSpacing > 1.0e-5)
            {
                int count = 0;
                for (double i = FirstVisibleTick; i < TimelineRightBound; i += TickSpacing)
                {
                    int divr = count % beatSnapDivisor;
                    Color tickCol;

                    if (divr == 0) tickCol = lineColourAlphaWhite;
                    else if ((divr * 2) % beatSnapDivisor == 0) tickCol = lineColourAlphaRed;
                    else if ((divr * 3) % beatSnapDivisor == 0) tickCol = lineColourAlphaViolet;
                    else if ((divr * 4) % beatSnapDivisor == 0) tickCol = lineColourAlphaBlue;
                    else if ((divr * 6) % beatSnapDivisor == 0) tickCol = lineColourAlphaMagenta;
                    else if ((divr * 8) % beatSnapDivisor == 0) tickCol = lineColourAlphaOrange;
                    else tickCol = lineColourAlphaGrey;

                    int endPointY = TimelineRectangle.Bottom;

                    if (i >= TimelineLeftBound)
                    {
                        if (count % beatSnapDivisor == 0)
                        {
                            GameBase.primitiveBatch.AddVertex(
                                new Vector2(TimeToTimelinePos(i), TimelineRectangle.Bottom - TimelineTickHeight * (firstTickCount % timeSig == 0 ? 2 : 1)),
                                tickCol);
                            GameBase.primitiveBatch.AddVertex(new Vector2(TimeToTimelinePos(i), endPointY),
                                                              tickCol);
                        }
                        else
                        {
                            GameBase.primitiveBatch.AddVertex(
                                new Vector2(TimeToTimelinePos(i), TimelineRectangle.Bottom - TimelineTickHeight / 8 * 7),
                                tickCol);
                            GameBase.primitiveBatch.AddVertex(new Vector2(TimeToTimelinePos(i), TimelineRectangle.Bottom),
                                                              tickCol);
                        }
                    }

                    if (count % beatSnapDivisor == 0)
                        firstTickCount++;

                    count++;

                }
            }

            Color lineColour = new Color(58, 110, 170, 240);

            foreach (int i in hitObjectManager.Bookmarks)
            {
                if (i >= TimelineLeftBound && i <= TimelineRightBound)
                {
                    GameBase.primitiveBatch.AddVertex(new Vector2(TimeToTimelinePos(i), TimelineRectangle.Top), lineColour);
                    GameBase.primitiveBatch.AddVertex(new Vector2(TimeToTimelinePos(i), TimelineRectangle.Bottom), lineColour);
                }
            }


            GameBase.primitiveBatch.End();

            currentMode.DrawTimeline();
        }

        internal float TimeToTimelinePosNoOffset(double audioTime)
        {
            return
                (float)Math.Min(
                            Math.Max(
                                (TimelineX +
                                 ((audioTime - TimelineLeftBound) / TimelineDisplayRange) * TimelineWidth),
                                TimelineX), TimelineX + TimelineWidth);
        }

        internal float TimeToTimelinePos(double audioTime)
        {
            return
                (float)
                Math.Min(
                    Math.Max(
                        (TimelineRectangle.Left +
                         ((audioTime - TimelineLeftBound) / TimelineDisplayRange) * TimelineRectangle.Width),
                        TimelineRectangle.Left), TimelineRectangle.Right);
        }

        internal int TimelinePosToTime(int xPos)
        {
            return
                (int)
                (TimelineLeftBound +
                 ((double)(xPos - TimelineRectangle.Left)) / TimelineRectangle.Width * TimelineDisplayRange);
        }

        #endregion

        #region User Input Handling

        private List<EditorState> HistoryBackward;
        private Stack<EditorState> HistoryForward;
        private Stack<EditorState> HistoryBackup;

        internal bool InGamefield;
        internal bool InTimeline;
        internal Location lastPosition = Location.None;


        private bool onMouseWheelUpAlt(object sender, EventArgs e)
        {
            if (KeyboardHandler.AltPressed)
            {
                if (InTimeline)
                {
                    timelinePlus_OnClick(null, null);
                    return true;
                }
            }

            return false;
        }

        private bool onMouseWheelDownAlt(object sender, EventArgs e)
        {
            if (KeyboardHandler.AltPressed)
            {
                if (InTimeline)
                {
                    timelineMinus_OnClick(null, null);
                    return true;
                }
            }

            return false;
        }


        private bool onMouseWheelUp(object sender, EventArgs e)
        {
            if (currentMode.OnMouseWheelUp())
                return true;

            SeekLeft(KeyboardHandler.ShiftPressed ? 4 : 1);
            return true;
        }

        private bool onMouseWheelDown(object sender, EventArgs e)
        {
            if (currentMode.OnMouseWheelDown())
                return true;

            SeekRight(KeyboardHandler.ShiftPressed ? 4 : 1);
            return true;
        }

        private void SeekLeft(int rate)
        {
            int seekTime;
            int skip;
            if (AudioEngine.BeatSyncing)
            {
                if (AudioEngine.AudioState == AudioStates.Playing)
                    rate *= (250 / (int)AudioEngine.beatLength + 1);
                int snap = (AudioEngine.AudioState != AudioStates.Playing ? beatSnapDivisor : 1);
                skip = (int)(AudioEngine.beatLength / snap) * rate;
                seekTime = Timing.BeatSnapValue(AudioEngine.Time - skip, AudioEngine.Time);
                AudioEngine.SeekTo(seekTime,
                                   CurrentMode == EditorModes.Design);
            }
            else
            {
                skip = (1000 / beatSnapDivisor) * rate; //1000 ms = length of a beat at 120 BPM.

                seekTime = AudioEngine.Time - skip;
                seekTime -= seekTime - (1000 / beatSnapDivisor) * Math.DivRem(seekTime, (1000 / beatSnapDivisor), out skip);

                if (seekTime < 0)
                    seekTime = 0;

                AudioEngine.SeekTo(seekTime);
            }
        }

        private void SeekRight(int rate)
        {
            int skip;
            int seekTime;
            if (AudioEngine.BeatSyncing)
            {
                if (AudioEngine.AudioState == AudioStates.Playing)
                    rate *= (250 / (int)AudioEngine.beatLength + 1);
                int snap = (AudioEngine.AudioState != AudioStates.Playing ? beatSnapDivisor : 1);
                skip = (int)(AudioEngine.beatLength / snap) * rate;
                seekTime = Timing.BeatSnapValue(AudioEngine.Time + skip, AudioEngine.Time);
                AudioEngine.SeekTo(seekTime,
                                   CurrentMode == EditorModes.Design);
            }
            else
            {
                skip = (1000 / beatSnapDivisor) * rate;

                seekTime = AudioEngine.Time + skip;
                seekTime += (1000 / beatSnapDivisor) * Math.DivRem(seekTime, (1000 / beatSnapDivisor), out skip) - seekTime;

                AudioEngine.SeekTo(seekTime);
            }
        }

        private bool Game1_OnKeyRepeat(object sender, Keys k, bool first)
        {
            if (isSubmitting)
                return false;

            if (currentMode.OnKey(k, first))
                return false;

            if (KeyboardHandler.ControlPressed)
            {
                switch (k)
                {
                    case Keys.Down:
                        ChangeAudioRate(Math.Max(25, AudioEngine.CurrentPlaybackRate - (KeyboardHandler.ShiftPressed ? 3 : 25)));
                        return true;
                    case Keys.Up:
                        ChangeAudioRate(Math.Min(100, AudioEngine.CurrentPlaybackRate + (KeyboardHandler.ShiftPressed ? 3 : 25)));
                        return true;
                }
            }
            else
            {
                switch (k)
                {
                    case Keys.Left:
                        SeekLeft(KeyboardHandler.ShiftPressed ? 4 : 1);
                        return true;
                    case Keys.Right:
                        SeekRight(KeyboardHandler.ShiftPressed ? 4 : 1);
                        return true;
                    case Keys.Down:
                        SeekLastTimingPoint();
                        return true;
                    case Keys.Up:
                        SeekNextTimingPoint();
                        return true;
                }
            }

            if (first)
            {
                if (KeyboardHandler.ControlPressed)
                {
                    switch (k)
                    {
                        case Keys.Left:
                            bookmarkPrev_OnClick(null, null);
                            return true;
                        case Keys.Right:
                            bookmarkNext_OnClick(null, null);
                            return true;
                        case Keys.U:
                            if (KeyboardHandler.ShiftPressed)
                            {
                                Submit();
                                return true;
                            }
                            break;
                        case Keys.P:
                            if (KeyboardHandler.ShiftPressed)
                                Timing.TimingAdd(false);
                            else
                                Timing.TimingAdd(true);
                            return true;
                        case Keys.I:
                            if (KeyboardHandler.ShiftPressed)
                            {
                                ShowSampleImport();
                                return true;
                            }
                            Timing.TimingDelete();
                            return true;
                        case Keys.B:
                            if (KeyboardHandler.ShiftPressed)
                                bookmarkDelete_OnClick(null, null);
                            else
                                bookmarkAdd_OnClick(null, null);
                            return true;
                        case Keys.N:
                            Clear();
                            return true;
                        case Keys.S:
                            if (KeyboardHandler.ShiftPressed)
                            {
                                ShowScaleBy();
                                return true;
                            }

                            SaveFile();
                            return true;
                        case Keys.D:  //seems should be ctrl+shift+P
                            if (KeyboardHandler.ShiftPressed)
                            {
                                ShowPolygon();
                                return true;
                            }
                            break;
                        case Keys.F:
                            if (KeyboardHandler.ShiftPressed)
                            {
                                ShowConvertToStream();
                                return true;
                            }
                            break;
                        case Keys.O:
                            ShowDifficultySwitch();
                            return true;
                        case Keys.L:
                            LoadFile(hitObjectManager.Beatmap, true, KeyboardHandler.ShiftPressed);
                            return true;
                        case Keys.D1:
                            ChangeGridSize(GridSizes.Tiny);
                            return true;
                        case Keys.D2:
                            ChangeGridSize(GridSizes.Small);
                            return true;
                        case Keys.D3:
                            ChangeGridSize(GridSizes.Medium);
                            return true;
                        case Keys.D4:
                            ChangeGridSize(GridSizes.Large);
                            return true;
                        case Keys.C:
                            CopySelection();
                            return true;
                        case Keys.Z:
                            Undo();
                            return true;
                        case Keys.Y:
                            Redo();
                            return true;
                        case Keys.A:
                            if (KeyboardHandler.ShiftPressed)
                            {
                                AiModOpen();
                                return true;
                            }
                            break;
                    }
                }


                //Univerasl key
                if (!KeyboardHandler.ControlPressed)
                {
                    Bindings b = BindingManager.For(k, BindingTarget.Editor);
                    switch (b)
                    {
                        case Bindings.HelpToggle:
                            ToggleHelp();
                            break;
                        case Bindings.JumpToBegin:
                            Compose.RemovePlacementObject();

                            if (hitObjectManager.hitObjectsCount == 0)
                            {
                                buttonStop_OnClick(null, null);
                                return true;
                            }
                            if (hitObjectManager.hitObjects[0].StartTime == AudioEngine.Time)
                                buttonStop_OnClick(null, null);
                            else
                                AudioEngine.SeekTo(hitObjectManager.hitObjects[0].StartTime);
                            return true;
                        case Bindings.PlayFromBegin:
                            AudioEngine.Play();
                            return true;
                        case Bindings.JumpToEnd:
                            if (hitObjectManager.hitObjects.Count > 0 &&
                                AudioEngine.Time <
                                hitObjectManager.hitObjects[hitObjectManager.hitObjects.Count - 1].EndTime)
                                AudioEngine.SeekTo(
                                    hitObjectManager.hitObjects[hitObjectManager.hitObjects.Count - 1].EndTime);
                            else
                                AudioEngine.SeekTo(Math.Max(0, AudioEngine.AudioLength - 500));
                            return true;
                        case Bindings.AudioPause:
                            Pause();
                            return true;
                        case Bindings.GridChange:
                            ChangeGridSize(GridSize * 2 <= (int)GridSizes.Large ? GridSize * 2 : (int)GridSizes.Tiny);
                            return true;
                        case Bindings.TimingSection:
                            Timing.TimingAdd(true);
                            return true;
                        case Bindings.InheritingSection:
                            Timing.TimingAdd(false);
                            return true;
                        case Bindings.RemoveSection:
                            Timing.TimingDelete();
                            return true;
                    }
                    if (k == Keys.Space)
                    {
                        Pause();
                        return true;
                    }
                }

                //Mode Swapping
                if (!KeyboardHandler.AltPressed)
                {
                    switch (k)
                    {
                        case Keys.F1:
                            ChangeMode(EditorModes.Compose);
                            return true;
                        case Keys.F2:
                            ChangeMode(EditorModes.Design);
                            return true;
                        case Keys.F3:
                            ChangeMode(EditorModes.Timing);
                            return true;
                        case Keys.F4:
                            ShowSongSetup();
                            return true;
                        case Keys.F5:
                            TestMode();
                            return true;
                        case Keys.F6:
                            Timing.ShowTimingWindow(TimingFocus.BPM);
                            return true;
                        case Keys.Escape:
                            if (HelpDisplayed)
                            {
                                ToggleHelp();
                                return true;
                            }
                            else if (currentMode.ExitEditor())
                            {
                                Exit();
                                return true;
                            }
                            break;
                    }
                }
            }

            return false;
        }

        internal void AiModOpen()
        {
            if (aiMod == null || aiMod.IsDisposed)
            {
                aiMod = new AiModWindow(this);
            }
            aiMod.Run();

            if (!aiMod.Visible)
                aiMod.Show(GameBase.Form);

            return;
        }

        private void AiModClose()
        {
            if (aiMod == null)
                return;

            aiMod.Close();
            aiMod = null;
        }

        internal bool Exit()
        {
            return Exit(true);
        }

        internal bool Exit(bool requireConfirmation)
        {

            ExitedProperly = true;

            lastMode = EditorModes.None;

            HideDifficultySwitch();
            if (gotoTime != null)
            {
                gotoTime.Close();
                gotoTime = null;
            }

            GameBase.MenuActive = true;

            if (Compose != null)
            {
                Compose.RemovePlacementObject();
            }
            CloseImportDialog();

            //special case to ensure we don't accidentally overwrite someone's hard work mapping.
            if (ErrorDialog.IsHandlingError && hitObjectManager != null && hitObjectManager.hitObjects.Count == 0)
                return false;

            if (!Dirty || hitObjectManager.Save(requireConfirmation, false, false))
                GameBase.ChangeMode(OsuModes.SelectEdit);
            else
            {
                GameBase.MenuActive = false;
                ExitedProperly = false;
                return false;
            }

            Beatmap beatmap = hitObjectManager.Beatmap;

            //make sure that the current beatmap is not a temporary beatmap
            if (beatmap.IsTemporary)
            {
                if (beatmap.Package != null && beatmap.Package.MapFiles.Length > 0)
                    BeatmapManager.SetCurrentFromFilename(beatmap.Package.MapFiles[0]);
                else
                    BeatmapManager.Current = BeatmapManager.Beatmaps.Find((b) => !b.IsTemporary && b.AudioPresent);
            }


            //remove beatmaps created from editing a osz2 package, ae using save as.
            //these beatmaps may not showup at play.
            List<Beatmap> temporaryBeatmaps = BeatmapManager.Beatmaps.FindAll((b) => (b.IsTemporary));
            temporaryBeatmaps.ForEach(b => { BeatmapManager.Remove(b); });

            beatmap.LastEditTime = AudioEngine.Time;

            GameBase.TestTime = 0;
            GameBase.TestMode = false;

            GameBase.MenuActive = false;

            return true;
        }

        private bool onDoubleClick(object sender, EventArgs e)
        {
            if (bookmarkMenuActive)
                return false;

            return currentMode.OnDoubleClick();
        }

        private bool onClick(object sender, EventArgs e)
        {
            if (bookmarkMenuActive || (ChatEngine.IsVisible && ChatEngine.IsFullVisible) || GameBase.ActiveDialog != null || GameBase.FadeState != FadeStates.Idle || IsSubmitting)
                return false;

            if (!toolWindowOpening)
            {
                if (gotoTime != null)
                    gotoTime.Close();
                gotoTime = null;
            }

            if ((uiManager.CurrentHoverSprite == null && currentMode.spriteManager.CurrentHoverSprite == null) || currentMode.IgnoreInterfaceClick)
            {
                currentMode.OnClick();
            }

            if (!currentMode.IgnoreInterfaceClick)
            {
                int actualX = InputManager.CursorPoint.X;

                if (InputManager.CursorPoint.Y <= GameBase.WindowRatio * 16 + GameBase.WindowOffsetY &&
                    InputManager.CursorPoint.Y >= GameBase.WindowOffsetY &&
                    actualX <= GameBase.WindowRatio * 400 && actualX >= 0)
                {

                    if (actualX / (GameBase.WindowRatio * 100) >= 3)
                        ShowSongSetup();
                    else
                        ChangeMode((EditorModes)Math.Max(0, actualX / (GameBase.WindowRatio * 100) + 1));
                    return true;
                }
            }

            if (InSeekbar && !ChatEngine.IsVisible)
            {
                AudioEngine.SeekTo((int)(AudioEngine.AudioLength * ((float)(InputManager.CursorPoint.X - seekbarRectangle.Left) / seekbarRectangle.Width)));
                return true;
            }

            return false;
        }

        internal int GetCurrentSnappedTime()
        {
            return (AudioEngine.BeatSyncing
                        ? Timing.BeatSnapValue(AudioEngine.Time)
                        : (int)AudioEngine.Time);
        }

        internal bool InSeekbar;

        private bool onDrag(object sender, EventArgs e)
        {
            if (GameBase.ActiveDialog != null || IsSubmitting)
                return false;

            if (bookmarkMenuActive)
                return false;

            if (!IsDragging)
            {
                IsDragging = true;

                if (currentMode.OnDragStart())
                    return true;

                if (InSeekbar)
                {
                    //DragCaptured = spriteSeekbarDragger.Rectangle.Contains(InputManager.CursorPoint) ? DragCapture.SeekbarFollow : DragCapture.Seekbar;
                    DragCaptured = DragCapture.SeekbarFollow;
                    return true;
                }
            }

            return false;
        }

        private bool onEndDrag(object sender, EventArgs e)
        {
            currentMode.OnDragEnd();
            DragCaptured = DragCapture.None;
            IsDragging = false;
            return false;
        }

        #endregion

        #region Editor Modes

        private EditorMode currentMode;
        internal EditorModeCompose Compose;
        internal EditorModeDesign Design;
        internal EditorModeTiming Timing;
        private RotateBy rotateBy;
        private ScaleBy scaleBy;
        private Polygon polygon;
        private ConvertToStream ctoStream;
        private SampleImport sampleImport;
        private const int MAX_NO_BREAK_TIME = 5000;

        /// <summary>
        /// Used to restore editor mode after returning from test mode.
        /// </summary>
        private static EditorModes lastMode;


        #endregion

        public void ShowSampleImport()
        {
            if (sampleImport == null || sampleImport.IsDisposed)
            {
                sampleImport = new SampleImport();
                sampleImport.Show(GameBase.Form);
            }
        }
        internal void CloseImportDialog()
        {
            if (sampleImport != null && !sampleImport.IsDisposed)
            {
                sampleImport.Close();
                sampleImport = null;
            }
        }
        public void ShowRotateBy()
        {
            if (currentMode != Compose || Compose.selectedObjects.Count == 0 || rotateBy != null)
                return;

            rotateBy = new RotateBy();
            rotateBy.ShowDialog(GameBase.Form);
            rotateBy = null;
        }

        public void ShowScaleBy()
        {
            if (currentMode != Compose || Compose.selectedObjects.Count == 0 || scaleBy != null)
                return;

            float max = Compose.GetMaxScale(true);
            if (max == -1)
                max = 5.0f;
            scaleBy = new ScaleBy();
            scaleBy.MinScale = 0.5f;
            scaleBy.MaxScale = Math.Max(max, 1.0f);
            scaleBy.ShowDialog(GameBase.Form);
            scaleBy = null;
        }

        public void ShowPolygon()
        {
            if (currentMode != Compose || polygon != null)
                return;

            polygon = new Polygon();
            polygon.ShowDialog(GameBase.Form);
            polygon = null;
        }

        public void ShowConvertToStream()
        {
            if (currentMode != Compose || Compose.selectedSlider == null || ctoStream != null)
                return;

            ctoStream = new ConvertToStream((SliderOsu)(Compose.selectedObjects[0]));
            ctoStream.ShowDialog(GameBase.Form);
            ctoStream = null;
        }
    }

    internal enum EditorModes
    {
        None = 0,
        Compose = 1,
        Design = 2,
        Timing = 3
    } ;

    [Flags]
    internal enum SoundAdditions
    {
        NewCombo = 1,
        Whistle = 2,
        Finish = 4,
        GridSnap = 8,
        DistSnap = 16,
        NoteLock = 32,
        Clap = 64
    } ;
    internal enum Location
    {
        None,
        Timeline,
        Gamefield,
        Seekbar
    }
    internal enum DragCapture
    {
        None,
        Timeline,
        Gamefield,
        SeekbarFollow
    }
}