﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Edit.Forms;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.Graphics.Notifications;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers.Forms;
using osu.Online;
using osu_common.Helpers;
using osu.GameplayElements;
using osu_common.Libraries.Osz2;
using Microsoft.Xna.Framework;
using osu_common;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameModes.Play;
using osu.GameplayElements.HitObjects;

namespace osu.GameModes.Edit
{
    internal partial class Editor
    {
        internal void SaveFile()
        {
            Compose.RemovePlacementObject();
            BreakTimeFix(true);

            hitObjectManager.Save(false, false, false);

            BeatmapManager.Add(hitObjectManager.Beatmap);
            //make sure this map is added to the beatmap listing (needed for BSS).

            NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.Editor_FileOperations_SavedBeatmap), 1000);

            Dirty = false;
        }

        internal bool LoadReferenceFile(Beatmap map)
        {
            if (Compose.ReferenceHitObjectManager != null)
            {
                Compose.ReferenceHitObjectManager.Dispose();
                Compose.ReferenceHitObjectManager = null;
            }

            try
            {
                if (map == null)
                {
                    //Clear the reference sprites
                    Compose.ReferenceChanged();
                    return false;
                }

                Compose.ReferenceHitObjectManager = new HitObjectManagerEditor();
                Compose.ReferenceHitObjectManager.SetBeatmap(map, Mods.None);
                Compose.ReferenceHitObjectManager.Load(true);
                Compose.ReferenceHitObjectManager.ManiaStage = new StageMania(Compose.ReferenceHitObjectManager);
                Compose.ReferenceChanged();
                return true;
            }
            catch (Exception e)
            {
                Compose.ReferenceChanged();
                return false;
            }
        }

        internal bool LoadFile(Beatmap beatmap, bool warnOnLostChanges, bool clearCache)
        {
            if (warnOnLostChanges && CheckForChanges())
            {
                DialogResult r = NotificationManager.MessageBox(LocalisationManager.GetString(OsuString.Editor_FileOperations_RevertFileToLastSavedState), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
                switch (r)
                {
                    case DialogResult.Cancel:
                        return false;
                    case DialogResult.No:
                        return false;
                }
            }



            GameBase.SetTitle(Path.GetFileName(beatmap.Filename));
            Instance.SelectNone();

            SkinManager.ResetColoursToSkin();

            if (clearCache)
                SkinManager.ClearBeatmapCache(true);

            AudioEngine.LoadAudio(beatmap, false, false);

            ChangeAudioRate(100);

            bool existingMap;

            try
            {
                hitObjectManager.SetBeatmap(beatmap, Mods.None);
                existingMap = hitObjectManager.LoadWithEvents();
            }
            catch (OsuFileUnreadableException)
            {
                GameBase.ChangeModeInstant(OsuModes.SelectEdit, true);
                return false;
            }

            if (Compose != null)
            {
                if (beatmap.PlayMode != LoadedMode)
                {
                    GameBase.ChangeMode(OsuModes.Edit, true);
                    return false;
                }
            }

            eventManager = hitObjectManager.eventManager;

            eventManager.spriteManagerBGWide.EnableProfiling = true;
            eventManager.spriteManagerBG.EnableProfiling = true;
            eventManager.spriteManagerFG.EnableProfiling = true;

            if (!existingMap || beatmap.Creator.Length == 0)
            {
                if (Compose != null) Compose.ChangeAltDistance(0.8f);
                ShowSongSetup(false);
            }


            beatmap.BeatmapVersion = Beatmap.BEATMAP_VERSION;

            hitObjectManager.UpdateSlidersAll(false);

            if (Design != null)
                Design.Reset();

            if (currentMode != null)
                currentMode.Enter();

            UndoRedoClear();

            if (aiMod != null) aiMod.Reset();

            BreakTimeFix(false, true);

            GameBase.EditorControl.LoadMenu();

            Dirty = false;

            return true;
        }

        internal void ShowSongSetup()
        {
            ShowSongSetup(true);
        }

        internal void ShowSongSetup(bool confirmSave)
        {
            HideDifficultySwitch();

            GameBase.MenuActive = true;
            SongSetup m = new SongSetup(false, confirmSave);
            m.ShowDialog(GameBase.Form);
            GameBase.MenuActive = false;

            if (!hitObjectManager.Beatmap.CustomColours)
                SkinManager.ResetColoursToSkin();

            hitObjectManager.ComboColoursReset();


            hitObjectManager.Sort(true);
        }

        internal bool CheckForChanges()
        {
            Compose.RemovePlacementObject();
            return hitObjectManager.Save(false, false, true);
        }

        /// <summary>
        /// Initialises Test Mode.
        /// </summary>
        internal void TestMode()
        {
            lastMode = CurrentMode;

            if (hitObjectManager.hitObjects.Count == 0)
                return;
            if (AudioEngine.AudioState == AudioStates.Playing)
                Pause();

            Compose.RemovePlacementObject();
            if (AudioEngine.beatLength <= 0)
            {
                MessageBox.Show(LocalisationManager.GetString(OsuString.EditorControl_OneSectionRequiredWarning), @"osu!", MessageBoxButtons.OK);
                return;
            }

            ignoreUndoPush = true;
            while (!AudioEngine.ControlPoints[0].timingChange)
            {
                AudioEngine.ControlPoints.RemoveAt(0);
                AudioEngine.UpdateActiveTimingPoint(false);
            }
            ignoreUndoPush = false;
            AiModClose();

            CloseImportDialog();

            GameBase.TestMode = true;
            GameBase.TestTime = AudioEngine.Time;

            Beatmap b = hitObjectManager.Beatmap;
            Player.Mode = b.PlayMode;

            if (hitObjectManager.hitObjects.FindAll(h => h.EndTime > AudioEngine.Time).Count == 0)
                GameBase.TestTime = 0; //There are no hitcircles after the current point.


            if (!Dirty || hitObjectManager.Save(true, false, false))
            {
                if (b.DifficultyTomStars(b.PlayMode) < 0)
                {
                    b.ComputeAndSetDifficultiesTomStars(b.PlayMode);
                }
                HideDifficultySwitch();
                GameBase.ChangeModeInstant(OsuModes.Play);
            }
            else
                GameBase.TestMode = false;
        }

        internal bool SaveIfDirty(bool prompt = false)
        {
            if (Dirty)
            {
                Compose.RemovePlacementObject();
                if (!hitObjectManager.Save(prompt, false, false))
                    return false;
                Dirty = false;
            }

            return true;
        }

        internal void HideDifficultySwitch()
        {
            if (openDialog != null)
            {
                openDialog.Close();
                openDialog = null;
            }
        }

        OpenDialog openDialog;
        internal void ShowDifficultySwitch(bool reference = false)
        {
            if (openDialog != null && !openDialog.IsDisposed && openDialog.Visible)
            {
                openDialog.Focus();
                return;
            }

            openDialog = new OpenDialog(reference);
            openDialog.Location = new System.Drawing.Point(GameBase.Form.Location.X + 10, GameBase.Form.Location.Y + 55);
            openDialog.Show(GameBase.Form);
        }


        internal void SaveAs()
        {
            HideDifficultySwitch();

            GameBase.MenuActive = true;

            SongSetup m = new SongSetup(true, false);
            if (m.ShowDialog(GameBase.Form) == DialogResult.OK)
            {
                BeatmapManager.Beatmaps.Sort(); //sorting by filename as it just changed.

                //ensure we reset the beatmap IDs.
                hitObjectManager.Beatmap.BeatmapId = 0;

                BeatmapManager.ProcessFolder(hitObjectManager.Beatmap.InOszContainer ?
                    hitObjectManager.Beatmap.ExtractionFolder : hitObjectManager.Beatmap.ContainingFolder);

                DialogResult r = MessageBox.Show(LocalisationManager.GetString(OsuString.Editor_FileOperations_SaveAsDialog), @"osu!",
                                             MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation,
                                             MessageBoxDefaultButton.Button1);
                if (r == DialogResult.Yes)
                {
                    hitObjectManager.Clear();
                    hitObjectManager.Save(false, false, false);
                }
            }

            GameBase.MenuActive = false;
        }

        internal void Submit()
        {
            bool success = true;

            if (!GameBase.HasLogin)
            {
                GameBase.Options.OnLoginResult += delegate(bool loginSuccess)
                {
                    GameBase.Scheduler.AddDelayed(delegate
                    {
                        if (loginSuccess)
                            GameBase.Scheduler.AddDelayed(Submit, 500);
                        else
                            MessageBox.Show(GameBase.Form, LocalisationManager.GetString(OsuString.Editor_FileOperations_SubmitDialog_1));
                    }, 500);
                };
                GameBase.ShowLogin();
                return;
            }

            if (hitObjectManager.hitObjectsCount == 0)
            {
                MessageBox.Show(GameBase.Form, LocalisationManager.GetString(OsuString.Editor_FileOperations_SubmitDialog_2));
                success = false;
            }

            if (success)
            {
                SaveFile();

                DeleteUnnecessaryFiles();

                BeatmapSubmissionSystem s = new BeatmapSubmissionSystem(this, eventManager.events.FindAll(e => e is EventVideo).Count > 0, eventManager.events.FindAll(e => e.Type == EventTypes.Sprite).Count > 0);
                s.StartPosition = FormStartPosition.CenterScreen;
                if (BanchoClient.Connected)
                    s.Show(GameBase.Form);
                else
                    s.ShowDialog(GameBase.Form);
            }
            else
            {
                GameBase.MenuActive = false;
            }
        }


        internal void PackageOsz2()
        {
            if (hitObjectManager.Beatmap.InOszContainer)
                return;

            SaveFile();

            GameBase.MenuActive = true;

            SaveFileDialog s = new SaveFileDialog();
            s.RestoreDirectory = true;
            s.AddExtension = true;
            s.Filter = @"Packaged Beatmaps|*.osz2";
            s.DefaultExt = @".osz2";
            s.FileName = GeneralHelper.WindowsFilenameStrip(hitObjectManager.Beatmap.SortTitle);

            DialogResult r = s.ShowDialog(GameBase.Form);

            if (r == DialogResult.OK)
            {
                MapPackage pkg = hitObjectManager.Beatmap.ConvertToOsz2(s.FileName, false);
                pkg.Close();
            }

            GameBase.MenuActive = false;

        }

        internal void PackageOsz()
        {
            Beatmap b = hitObjectManager.Beatmap;
            if (b.InOszContainer)
                return;

            SaveFile();

            DeleteUnnecessaryFiles();

            GameBase.PackageFile(b.SortTitle + @".osz", b.ContainingFolder);

#if DEBUG
            b.ConvertToOsz2(@"Exports\" + GeneralHelper.WindowsFilenameStrip(b.SortTitle) + @".osz2", false);
#endif
        }

        internal void DeleteUnnecessaryFiles()
        {
            Beatmap b = hitObjectManager.Beatmap;
            if (b.InOszContainer)
                return;

            try
            {
                GeneralHelper.FileDelete(b.ContainingFolder + "/Thumbs.db");
            }
            catch { }

            List<string> unnecessaryFiles = new List<string>();

            foreach (string file in Directory.GetFiles(b.ContainingFolder))
            {
                string ext = Path.GetExtension(file).TrimStart('.').ToLower();
                string filename = Path.GetFileName(file);

                switch (ext)
                {
                    case @"osu":
                    case @"osb":
                    case @"wav":
                    case @"mp3":
                    case @"avi":
                    case @"mpg":
                    case @"wmv":
                    case @"flv":
                    case @"ogg":
                    case @"png":
                    case @"osz2":
                        continue;
                    case @"jpg":
                        if ((filename == @"pause-overlay.jpg") || (filename == @"spinner-background.jpg"))
                            continue;
                        else if (Instance.eventManager.events.Find(e => e.Filename == filename) != null)
                            continue;
                        break;
                }

                unnecessaryFiles.Add(file);
            }

            if (unnecessaryFiles.Count > 0)
            {
                string files = string.Empty;
                foreach (string f in unnecessaryFiles)
                    files += f + '\n';
                files = files.Trim('\n');
                if (DialogResult.Yes == MessageBox.Show(LocalisationManager.GetString(OsuString.Editor_FileOperations_UnusedFilesInDirectoryDialog) + '\n' + '\n' + files, @"osu!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
                {
                    foreach (string sf in unnecessaryFiles)
                    {
                        try
                        {
                            GeneralHelper.FileDelete(sf);
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }

        internal static bool LoadSelection(string link)
        {
            if (Instance == null || GameBase.Mode != OsuModes.Edit)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Editor_FileOperations_LoadSelectionRequirement));
                return false;
            }

            if (!AudioEngine.Paused)
                AudioEngine.TogglePause();

            int ms = 0;
            if (Int32.TryParse(link, out ms))
            {
                AudioEngine.SeekTo(ms);
                return true;
            }

            try
            {
                if (link.IndexOf('(') > 0)
                {
                    //Assume this is a well-formed copypaste link.

                    int time = Int32.Parse(link.Substring(0, 2)) * 60000 + Int32.Parse(link.Substring(3, 2)) * 1000 +
                               Int32.Parse(link.Substring(6, 3)) - 20;

                    int index = Instance.hitObjectManager.hitObjects.FindIndex(h => h.StartTime > time);

                    if (index < 0 || link.IndexOf(')') < 0)
                    {
                        //GameBase.ShowMessage("Can't find specific hit objects mentioned - only seeking to the time");
                        AudioEngine.SeekTo(time + 20);
                        return true;
                    }

                    AudioEngine.SeekTo(Instance.hitObjectManager.hitObjects[index].StartTime);

                    Instance.SelectNone();

                    Instance.ChangeMode(EditorModes.Compose);
                    string[] numPart = link.Substring(11, link.IndexOf(')') - 11).Split(',');
                    if (numPart.Length > 0 && numPart[0].IndexOf('|') > 0)
                    {
                        //mania format
                        foreach (string s in numPart)
                        {
                            string[] arr = s.Split('|');
                            int start = 0, col = 0;
                            Int32.TryParse(arr[0], out start);
                            Int32.TryParse(arr[1], out col);
                            for (int i = index; i < Instance.hitObjectManager.hitObjectsCount; i++)
                            {
                                HitObject h = Instance.hitObjectManager.hitObjects[i];
                                if (h.StartTime == start && Instance.hitObjectManager.ManiaStage.ColumnAt(h.Position) == col)
                                {
                                    Instance.Compose.Select(h);
                                }

                            }
                        }
                    }
                    else
                    {
                        //normal format
                        List<int> comboNumbers = new List<int>();
                        foreach (string s in numPart)
                            comboNumbers.Add(Int32.Parse(s));
                        if (comboNumbers.Count == 1)
                            Instance.Compose.Select(Instance.hitObjectManager.hitObjects[index]);
                        else
                        {
                            for (int i = index; i < Instance.hitObjectManager.hitObjectsCount; i++)
                            {
                                if (Instance.hitObjectManager.hitObjects[i].ComboNumber == comboNumbers[0])
                                {
                                    Instance.Compose.Select(Instance.hitObjectManager.hitObjects[i]);
                                    comboNumbers.RemoveAt(0);
                                }

                                if (comboNumbers.Count == 0)
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    string[] parts = link.Replace(@"-", string.Empty).Trim().Split(':');

                    int mins = 0;
                    int secs = 0;
                    int msecs = 0;

                    switch (parts.Length)
                    {
                        default:
                            return false;
                        case 2:
                            mins = Int32.Parse(parts[0]);
                            secs = Int32.Parse(parts[1]);
                            break;
                        case 3:
                            mins = Int32.Parse(parts[0]);
                            secs = Int32.Parse(parts[1]);
                            msecs = Int32.Parse(parts[2]);
                            break;
                    }

                    int time = mins * 60000 + secs * 1000 + msecs;
                    AudioEngine.SeekTo(time);
                    return true;
                }
            }
            catch (Exception)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Editor_FileOperations_InvalidFormat));
                return false;
            }

            return true;
        }

        public void SubmissionComplete()
        {
            GameBase.MenuActive = false;
            IsSubmitting = false;
        }
    }
}
