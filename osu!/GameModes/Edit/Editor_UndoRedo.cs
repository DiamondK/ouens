using System;
using System.Collections.Generic;
using osu.Audio;
using osu.Configuration;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu.Graphics.Notifications;
using osu.Input;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace osu.GameModes.Edit
{
    partial class Editor
    {
        internal bool overrideIgnoreState = false;
        internal bool ignoreUndoPush = false; //Prevents undo states from being created
        internal bool ignoreTimingUpdates = false; //Use this to disable Timing changes from being pushed 
        internal class EditorState
        {
            internal int preview_point;
            internal List<int> marks; //bookmarks
            internal List<ControlPoint> timings;
            internal List<HitObject> objects;
            internal List<Event> events;

            internal EditorState(List<HitObject> objects, List<ControlPoint> timings, List<Event> events)
            {
                preview_point = Editor.Instance.hitObjectManager.Beatmap.PreviewTime;
                this.timings = new List<ControlPoint>(timings);
                this.objects = new List<HitObject>(objects);
                this.events = new List<Event>(events);
                //this.marks = new List<int>(marks);
            } 
        }

        /// <summary>
        /// Push a new undo state.
        /// </summary>
        internal void UndoPush(bool backupState = false)
        {
            if (hitObjectManager.hitObjects == null)
                return;

            Dirty = true; 
            if (!overrideIgnoreState && (ConfigManager.sFastEditor || ignoreUndoPush))
                return;

            Compose.RemovePlacementObject();

            Debug.Print("Saving undo state #" + HistoryBackward.Count);

            //Changes that can be stored.
            List<ControlPoint> listTiming;
            List<HitObject>    listObject;
            List<Event>        listEvent;
            List<int>          listMarks; //Not used yet

            listTiming = new List<ControlPoint>();
            if (!ignoreTimingUpdates)
                foreach (ControlPoint cp in AudioEngine.ControlPoints)
                    listTiming.Add((ControlPoint)cp.Clone());

            /*foreach (int mark in hitObjectManager.Bookmarks)
                listMarks.Add(mark);*/

            listObject = hitObjectManager.Clone();
            listEvent = eventManager.Clone();

            if (HistoryBackward.Count >= 20)
            {
                Debug.Print("Max states reached");
                while (HistoryBackward.Count >= 20)
                    HistoryBackward.RemoveAt(0);
            }
            HistoryBackward.Add(new EditorState(listObject, listTiming, listEvent));
            HistoryBackup.Clear();
            if (!backupState)
                RedoClear();
            else
            {
                //Keep backup in case stack needs to be restored;
                while(HistoryForward.Count > 0)
                    HistoryBackup.Push(HistoryForward.Pop());
            }

            GameBase.EditorControl.menuItemRedo.Enabled = false;
            GameBase.EditorControl.menuItemUndo.Enabled = true;

        }

        internal EditorState UndoPop(bool killState = false)
        {
            if ((!overrideIgnoreState && ConfigManager.sFastEditor) || HistoryBackward.Count == 0)
                return null;

            Debug.Print("Loading undo state #" + HistoryBackward.Count);

#if DEBUG
            NotificationManager.ShowMessage("Undo pop", Color.YellowGreen, 700);
#endif

            EditorState state = HistoryBackward[HistoryBackward.Count - 1];
            HistoryBackward.RemoveAt(HistoryBackward.Count - 1);
            if (killState)
            {
                //Restore backup states
                while (HistoryBackup.Count > 0)
                    HistoryForward.Push(HistoryBackup.Pop());

                GameBase.EditorControl.menuItemRedo.Enabled = HistoryForward.Count > 0;
                GameBase.EditorControl.menuItemUndo.Enabled = HistoryBackward.Count > 0;

            }
            HistoryBackup.Clear();
            return state;
        }

        internal void HistoryClear()
        {
            HistoryBackup.Clear();
            HistoryBackward.Clear();
            HistoryForward.Clear();
            GameBase.EditorControl.menuItemRedo.Enabled = false;
            GameBase.EditorControl.menuItemUndo.Enabled = false;
        }

        internal void Undo()
        {
            if (HistoryBackward.Count == 0) return;

            if (currentMode == Compose)
                Compose.HandleDrag();

            Editor.Instance.Dirty = true;
            //BreakTimeFix();

            PushRedoStateNoClone();

            RestoreState(UndoPop());

            if (HistoryBackward.Count == 0)
                GameBase.EditorControl.menuItemUndo.Enabled = false;
            GameBase.EditorControl.menuItemRedo.Enabled = true;
        }

        private void PushRedoStateNoClone()
        {
            Compose.RemovePlacementObject();
            HistoryForward.Push(new EditorState(hitObjectManager.hitObjects, AudioEngine.ControlPoints, eventManager.events));
        }

        internal void Redo()
        {
            if (HistoryForward.Count <= 0) return;

            Editor.Instance.Dirty = true;
            //BreakTimeFix();

            PushUndoStateNoClone();

            RestoreState(HistoryForward.Pop());

            if (HistoryForward.Count == 0)
                GameBase.EditorControl.menuItemRedo.Enabled = false;
            GameBase.EditorControl.menuItemUndo.Enabled = true;
        }

        private void PushUndoStateNoClone()
        {
            Compose.RemovePlacementObject();

            HistoryBackward.Add(new EditorState(hitObjectManager.hitObjects, AudioEngine.ControlPoints, eventManager.events));

        }

        EditorState currentState;

        private void RestoreState(EditorState state)
        {    
            if (state == null || state.objects == null || (!ignoreTimingUpdates && state.timings == null) || state.events == null)
            {
                return;
            }

            currentState = state;

            //Handle HitObjects
            Compose.RemovePlacementObject();

            hitObjectManager.LoadObjects(state.objects);
            Compose.RefreshSelections();

            eventManager.LoadEvents(state.events);

            int selectedIndex = Design.selectedIndex;

            Design.Reset();

            if (!ignoreTimingUpdates)
            {
                bool overrideSet = overrideIgnoreState; //Make sure no state is set here even if override is enabled.
                overrideIgnoreState = false;
                ignoreUndoPush = true;
                Timing.TimingResetAll();

                foreach (ControlPoint cp in state.timings)
                    AudioEngine.ControlPoints.Add(cp);
                AudioEngine.UpdateActiveTimingPoint(false);
                ignoreUndoPush = false;
                overrideIgnoreState = overrideSet;
            }

            Editor.Instance.hitObjectManager.Beatmap.PreviewTime = state.preview_point;

            currentMode.Enter();

            Compose.selectedObjects.Clear();
            foreach (HitObject h in hitObjectManager.hitObjects)
                if (h.Selected)
                    Compose.selectedObjects.Add(h);

            if (currentMode == Design)
            {
                //handle reselection of selected event if we are in design mode.
                if (selectedIndex >= 0 && selectedIndex < eventManager.visibleEvents.Count)
                {
                    Design.selectedIndex = selectedIndex;
                    Design.selectedEvent = eventManager.visibleEvents[selectedIndex];
                }
            }


            //hitObjectManager.UpdateSlidersAll(false);
            hitObjectManager.UpdateBasic();
        }

        internal void UndoRedoClear()
        {
            HistoryBackup.Clear();
            HistoryForward.Clear();
            HistoryBackward.Clear();

            GameBase.EditorControl.menuItemUndo.Enabled = false;
            GameBase.EditorControl.menuItemRedo.Enabled = false;
        }

        internal void RedoClear()
        {
            HistoryForward.Clear();
            GameBase.EditorControl.menuItemRedo.Enabled = false;
        }

    }
}
