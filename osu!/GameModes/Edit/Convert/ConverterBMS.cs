﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using osu.Audio;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu_common.Helpers;

namespace osu.GameModes.Edit.Convert
{
    internal class TimingBMS : IComparer<TimingBMS>, IComparable<TimingBMS>
    {
        internal int Section = 0;
        internal double Offset = 0;
        internal double Time;
        internal double BPM = -1;
        internal bool Changed = true;
        internal double TimeSignature = 1;
        internal TimingBMS() { }

        public int Compare(TimingBMS x, TimingBMS y)
        {
            return y.CompareTo(x);
        }

        public int CompareTo(TimingBMS x)
        {
            if (x.Section < Section)
                return 1;
            else if (x.Section > Section)
                return -1;
            else
            {
                if (x.Offset < Offset)
                    return 1;
                else if (x.Offset > Offset)
                    return -1;
                else
                    return 0;
            }
        }

        public TimingBMS Clone()
        {
            TimingBMS t = new TimingBMS();
            t.TimeSignature = TimeSignature;
            t.BPM = BPM;
            t.Changed = Changed;
            t.Offset = Offset;
            t.Section = Section;
            t.Time = Time;
            return t;
        }
    }

    internal class ConverterBMS
    {
        private Editor editor;
        private HitObjectManagerEditor hitObjectManager;
        private EventManager eventManager;

        internal bool IgnoreSample = false;
        internal bool IgnoreEffect = false;
        internal bool CopyImage = false;
        internal bool OverrideMeta = false;
        internal bool IgnoreSpecial = false;

        private Dictionary<string, string> wavDict;
        private Dictionary<string, double> bpmDict;
        private Dictionary<string, bool> noteDict;
        private Dictionary<string, bool> effectDict;
        private List<TimingBMS> timingList;
        private HitCircle[] pendingNotes;
        private List<string> pendingLines;
        private string directory = "";
        private string longObj = "";
        private int longType = 1;


        internal ConverterBMS(Editor editor)
        {
            this.editor = editor;
            hitObjectManager = editor.hitObjectManager as HitObjectManagerEditor;
            eventManager = editor.eventManager;
            wavDict = new Dictionary<string, string>();
            bpmDict = new Dictionary<string, double>();
            noteDict = new Dictionary<string, bool>();
            effectDict = new Dictionary<string, bool>();
            timingList = new List<TimingBMS>();
            pendingLines = new List<string>();
            pendingNotes = new HitCircle[10];
        }

        public void Clear()
        {
            wavDict.Clear();
            bpmDict.Clear();
            noteDict.Clear();
            effectDict.Clear();
            timingList.Clear();
            pendingLines.Clear();
        }

        internal bool ProcessFile(string file, string encode)
        {
            file = GeneralHelper.PathSanitise(file);
            directory = Path.GetDirectoryName(file);

            BeatmapManager.Current.DifficultyCircleSize = 1;
            try
            {
                using (FileStream fs = new FileStream(file, FileMode.Open))
                {
                    using (StreamReader sr = new StreamReader(fs, Encoding.GetEncoding(encode)))
                    {
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine();
                            if (line.IndexOf('#') < 0)
                                continue;
                            line = line.Substring(1);
                            //try split with :
                            if (line.IndexOf(':') > 0)
                            {
                                string[] arr = line.Split(':');
                                if (arr.Length == 2)
                                {
                                    arr[0] = arr[0].Trim();
                                    arr[1] = arr[1].Trim();
                                    if (arr[0].Length == 5)
                                    {
                                        if (!parseTimingField(arr))
                                        {
                                            pendingLines.Add(line);
                                        }
                                        continue;
                                    }
                                }
                            }
                            //try split with space
                            int spaceInx = line.IndexOf(' ');
                            if (spaceInx > 0)
                            {
                                string[] arr = new string[2];
                                arr[0] = line.Substring(0, spaceInx).Trim().ToUpper();
                                arr[1] = line.Substring(spaceInx).Trim();
                                if (arr[0].StartsWith("BPM") && arr[0].Length == 5)
                                {
                                    string key = arr[0].Substring(3, 2);
                                    bpmDict.Add(key, double.Parse(arr[1], GameBase.nfi));
                                    continue;
                                }
                                if (arr[0].StartsWith("WAV") && arr[0].Length == 5)
                                {
                                    string key = arr[0].Substring(3, 2);
                                    wavDict.Add(key, getFullFilename(arr[1]));
                                    continue;
                                }
                                switch (arr[0])
                                {
                                    case "PLAYER":
                                        if (int.Parse(arr[1]) != 1)
                                        {
                                            MessageBox.Show("Can not convert 2 player style bms file.");
                                            return false;
                                        }
                                        break;
                                    case "TITLE":
                                        if (OverrideMeta)
                                        {
                                            BeatmapManager.Current.Title = arr[1];
                                            BeatmapManager.Current.TitleUnicode = arr[1];
                                        }
                                        break;
                                    case "ARTIST":
                                        if (OverrideMeta)
                                        {
                                            BeatmapManager.Current.Artist = arr[1];
                                            BeatmapManager.Current.ArtistUnicode = arr[1];
                                        }
                                        break;
                                    case "BPM":
                                        TimingBMS t = new TimingBMS();
                                        t.BPM = double.Parse(arr[1], GameBase.nfi);
                                        timingList.Add(t);
                                        break;
                                    case "STAGEFILE":
                                        if (CopyImage)
                                        {
                                            BeatmapManager.Current.BackgroundImage = arr[1];
                                            copyFile(arr[1]);
                                        }
                                        break;
                                    case "PLAYLEVEL":
                                        if (OverrideMeta)
                                        {
                                            BeatmapManager.Current.Version = "Lv." + arr[1];
                                        }
                                        break;
                                    case "RANK":
                                        //rank = 0~3  / insane~easy
                                        if (OverrideMeta)
                                        {
                                            int rank = int.Parse(arr[1], GameBase.nfi);
                                            BeatmapManager.Current.DifficultyHpDrainRate = (byte)Math.Max(4, Math.Min(9, 9 - rank * 2)); // 4~9
                                            BeatmapManager.Current.DifficultyOverall = BeatmapManager.Current.DifficultyHpDrainRate;
                                        }
                                        break;
                                    case "LNTYPE":
                                        longType = int.Parse(arr[1], GameBase.nfi);
                                        break;
                                    case "LNOBJ":
                                        longObj = arr[1];
                                        break;
                                }
                            }
                        }
                    }
                }
                //process timing list
                timingList.Sort();
                calculateTime();
                //not n+1 style, remove 0 column
                byte col = (byte)Math.Round(BeatmapManager.Current.DifficultyCircleSize);
                if ((col == 6 || col == 8) && !BeatmapManager.Current.SpecialStyle)
                    BeatmapManager.Current.DifficultyCircleSize = col - 1;

                IgnoreSpecial = !BeatmapManager.Current.SpecialStyle;
                for (int i = 0; i < pendingLines.Count; i++)
                {
                    string[] arr = pendingLines[i].Split(':');
                    if (arr.Length == 2)
                    {
                        arr[0] = arr[0].Trim();
                        arr[1] = arr[1].Trim();
                        if (arr[0].Length == 5)
                        {
                            parseDataField(arr);
                        }
                    }
                }
                for (int j = 0; j < pendingNotes.Length; j++)
                {
                    if (pendingNotes[j] != null)
                        throw new Exception("Something wrong with the bms");
                }
                //add timing point
                List<ControlPoint> list = new List<ControlPoint>(timingList.Count);
                for (int i = 0; i < timingList.Count; i++)
                {
                    TimingBMS t = timingList[i];
                    ControlPoint cp = new ControlPoint((int)t.Time, t.Changed ? 60000 / t.BPM : -100, (TimeSignatures)(t.TimeSignature * 4), SampleSet.None, CustomSampleSet.Default, 70, t.Changed, EffectFlags.None);
                    list.Add(cp);
                }
                AudioEngine.ControlPoints = list;
                //copy samples
                if (!IgnoreSample)
                {
                    foreach (KeyValuePair<string, bool> pair in noteDict)
                        copyFile(pair.Key);
                }
                if (!IgnoreEffect)
                {
                    foreach (KeyValuePair<string, bool> pair in effectDict)
                        copyFile(pair.Key);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Convert failed");
                return false;
            }
            return true;
        }

        private void calculateTime()
        {
            TimingBMS current = timingList[0].Clone();
            for (int i = 0; i < timingList.Count; i++)
            {
                TimingBMS t = timingList[i];
                t.Time = ((t.Section + t.Offset) - (current.Section + current.Offset)) * (60000 * 4 / current.BPM) * current.TimeSignature + current.Time;
                current.Time = t.Time;
                current.Section = t.Section;
                current.Offset = t.Offset;
                if (t.BPM != -1)
                    current.BPM = t.BPM;
                else
                    t.BPM = current.BPM;
                if (t.TimeSignature != current.TimeSignature)
                    current.TimeSignature = t.TimeSignature;
            }
        }

        /// <summary>
        /// For sample file
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private string getFullFilename(string name)
        {
            if (Regex.IsMatch(name, @"\.(mp3|wav|ogg)$"))
                return name;
            string ext = ".ogg";
            for (int i = 0; i < 3; i++)
            {
                if (i == 1)
                    ext = ".wav";
                else if (i == 2)
                    ext = ".mp3";
                if (File.Exists(directory + name + ext))
                    return name + ext;
            }
            return "";
        }

        private void copyFile(string filename)
        {
            string org = Path.Combine(directory, filename);
            string dest = Path.Combine(BeatmapManager.Current.ContainingFolder, filename);
            if (File.Exists(dest))
                return;
            if (File.Exists(org))
                File.Copy(org, dest, true);
        }

        private int[] line2key(string line)
        {
            int[] arr = new int[2];
            arr[0] = int.Parse(line.Substring(0, 3));
            arr[1] = int.Parse(line.Substring(3, 2));
            return arr;
        }

        /// <summary>
        /// 00000100-> 00 00 01 00
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private List<string> line2arr(string line)
        {
            int index = 0;
            List<string> result = new List<string>(line.Length / 2);
            while (index < line.Length - 1)
            {
                result.Add(line.Substring(index, 2));
                index += 2;
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="line">[00111:00000100]</param>
        private bool parseTimingField(string[] arr)
        {
            int[] key = line2key(arr[0]);// 001 00
            List<string> val = line2arr(arr[1]);// 00 01 AW 00
            switch (key[1])
            {
                case 2:  //beat change
                    {
                        int Section = key[0];
                        TimingBMS target = timingList.Find(t => t.Section == Section && t.Offset == 0 && t.Changed == false);
                        if (target == null)
                        {
                            TimingBMS t = new TimingBMS();
                            t.TimeSignature = double.Parse(arr[1], GameBase.nfi);
                            t.Section = Section;
                            t.Offset = 0;
                            t.Changed = false;
                            timingList.Add(t);
                        }
                        else
                            target.TimeSignature = double.Parse(arr[1], GameBase.nfi);
                        //one more timing for next section
                        TimingBMS nt = new TimingBMS();
                        //    nt.TimeSignature = 1;
                        nt.Section = Section + 1;
                        nt.Offset = 0;
                        nt.Changed = false;
                        timingList.Add(nt);
                    }
                    return true;
                case 3:   //hardcode bpm
                    for (int i = 0; i < val.Count; i++)
                    {
                        if (val[i] == "00")
                            continue;
                        TimingBMS t = new TimingBMS();
                        t.Section = key[0];
                        t.Offset = 1.0 * i / val.Count;
                        t.BPM = System.Convert.ToInt32(val[i], 16);
                        timingList.Add(t);
                    }
                    return true;
                case 8:  //pre-defined bpm
                    for (int i = 0; i < val.Count; i++)
                    {
                        if (val[i] == "00")
                            continue;
                        double bpm;
                        if (bpmDict.TryGetValue(val[i], out bpm))
                        {
                            TimingBMS t = new TimingBMS();
                            t.Section = key[0];
                            t.Offset = 1.0 * i / val.Count;
                            t.BPM = bpm;
                            timingList.Add(t);
                        }
                    }
                    return true;
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 18:
                case 19:
                    {
                        int col = key[1] == 16 ? 0 : (key[1] < 16 ? key[1] - 10 : key[1] - 12);
                        if (col >= Math.Round(BeatmapManager.Current.DifficultyCircleSize))
                            BeatmapManager.Current.DifficultyCircleSize = (byte)(col + 1);
                        if (col == 0)
                            BeatmapManager.Current.SpecialStyle = true; // n+1
                    }
                    return false;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 58:
                case 59:
                    {
                        int col = key[1] == 56 ? 0 : (key[1] < 56 ? key[1] - 50 : key[1] - 52);
                        if (col >= Math.Round(BeatmapManager.Current.DifficultyCircleSize))
                            BeatmapManager.Current.DifficultyCircleSize = (byte)(col + 1);
                        if (col == 0)
                            BeatmapManager.Current.SpecialStyle = true; // n+1
                    }
                    return false;
            }
            return false;
        }

        private double getTime(int section, double offset)
        {
            TimingBMS t = new TimingBMS();
            t.Section = section;
            t.Offset = offset;
            int index = timingList.BinarySearch(t);
            if (index < 0)
                index = ~index;
            if (index > timingList.Count)
                index = timingList.Count;
            TimingBMS target = timingList[index == 0 ? 0 : index - 1];
            return ((t.Section + t.Offset) - (target.Section + target.Offset)) * (60000 * 4 / target.BPM) * target.TimeSignature + target.Time;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="line">[00111:00000100]</param>
        private void parseDataField(string[] arr)
        {
            int[] key = line2key(arr[0]);// 001 00
            List<string> val = line2arr(arr[1]);// 00 01 AW 00
            switch (key[1])
            {
                case 1:  //BGM
                    if (IgnoreEffect)
                        return;
                    for (int i = 0; i < val.Count; i++)
                    {
                        if (val[i] == "00")
                            continue;
                        string fn;
                        if (wavDict.TryGetValue(val[i], out fn))
                        {
                            EventSample e = new EventSample(-1, fn, (int)getTime(key[0], 1.0 * i / val.Count), StoryLayer.Background, 70);
                            e.WriteToOsu = true;
                            eventManager.Add(e);
                            effectDict[fn] = true;
                        }
                    }
                    break;

                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 18:
                case 19:
                    {
                        for (int i = 0; i < val.Count; i++)
                        {
                            if (val[i] == "00")
                                continue;
                            int col = key[1] == 16 ? 0 : (key[1] < 16 ? key[1] - 10 : key[1] - 12);
                            if (IgnoreSpecial)
                                col--;
                            if (longType == 2 && val[i] == longObj && pendingNotes[col] != null)
                            {
                                pendingNotes[col].EndTime = (int)getTime(key[0], 1.0 * i / val.Count);
                                pendingNotes[col].Type = HitObjectType.Hold;
                                pendingNotes[col] = null;
                                continue;
                            }
                            HitCircleOsu h = new HitCircleOsu(hitObjectManager, new Vector2(hitObjectManager.ManiaStage.Columns[col].SnapPositionX, 192), (int)getTime(key[0], 1.0 * i / val.Count), false);
                            string wav = "";
                            if (wavDict.TryGetValue(val[i], out wav) && !IgnoreSample)
                            {
                                noteDict[wav] = true;
                                h.SampleFile = wav;
                            }
                            else
                                h.SampleVolume = 1; //mute sample volume if file doesn't exist in meta 
                            hitObjectManager.Add(h, true);
                            if (longType == 2)
                                pendingNotes[col] = h;
                        }
                    }
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 58:
                case 59:
                    {
                        if (longType != 1)
                            break;
                        for (int i = 0; i < val.Count; i++)
                        {
                            if (val[i] == "00")
                                continue;
                            int col = key[1] == 56 ? 0 : (key[1] < 56 ? key[1] - 50 : key[1] - 52);
                            if (IgnoreSpecial)
                                col--;
                            if (pendingNotes[col] != null)
                            {
                                pendingNotes[col].EndTime = (int)getTime(key[0], 1.0 * i / val.Count);
                                pendingNotes[col] = null;
                                continue;
                            }
                            else
                            {
                                HitCircleHold h = new HitCircleHold(hitObjectManager, new Vector2(hitObjectManager.ManiaStage.Columns[col].SnapPositionX, 192),
                                                                                                  (int)getTime(key[0], 1.0 * i / val.Count), false, false, false, false, 0);
                                string wav = "";
                                if (wavDict.TryGetValue(val[i], out wav) && !IgnoreSample)
                                {
                                    noteDict[wav] = true;
                                    h.SampleFile = wav;
                                }
                                else
                                    h.SampleVolume = 1;
                                hitObjectManager.Add(h, true);
                                pendingNotes[col] = h;
                            }
                        }
                    }
                    break;
            }
        }
    }
}
