﻿namespace osu.GameModes.Edit.Forms
{
    partial class DesignToolbox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkLayer1 = new System.Windows.Forms.CheckBox();
            this.checkLayer2 = new System.Windows.Forms.CheckBox();
            this.checkLayer3 = new System.Windows.Forms.CheckBox();
            this.checkLayer4 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // checkLayer1
            // 
            this.checkLayer1.AutoSize = true;
            this.checkLayer1.Checked = true;
            this.checkLayer1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkLayer1.Location = new System.Drawing.Point(12, 28);
            this.checkLayer1.Name = "checkLayer1";
            this.checkLayer1.Size = new System.Drawing.Size(84, 17);
            this.checkLayer1.TabIndex = 0;
            this.checkLayer1.Text = "Background";
            this.checkLayer1.UseVisualStyleBackColor = true;
            this.checkLayer1.CheckedChanged += new System.EventHandler(this.checkLayer1_CheckedChanged);
            // 
            // checkLayer2
            // 
            this.checkLayer2.AutoSize = true;
            this.checkLayer2.Checked = true;
            this.checkLayer2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkLayer2.Location = new System.Drawing.Point(12, 51);
            this.checkLayer2.Name = "checkLayer2";
            this.checkLayer2.Size = new System.Drawing.Size(56, 17);
            this.checkLayer2.TabIndex = 1;
            this.checkLayer2.Text = "Failing";
            this.checkLayer2.UseVisualStyleBackColor = true;
            this.checkLayer2.CheckedChanged += new System.EventHandler(this.checkLayer2_CheckedChanged);
            // 
            // checkLayer3
            // 
            this.checkLayer3.AutoSize = true;
            this.checkLayer3.Checked = true;
            this.checkLayer3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkLayer3.Location = new System.Drawing.Point(12, 74);
            this.checkLayer3.Name = "checkLayer3";
            this.checkLayer3.Size = new System.Drawing.Size(63, 17);
            this.checkLayer3.TabIndex = 2;
            this.checkLayer3.Text = "Passing";
            this.checkLayer3.UseVisualStyleBackColor = true;
            this.checkLayer3.CheckedChanged += new System.EventHandler(this.checkLayer3_CheckedChanged);
            // 
            // checkLayer4
            // 
            this.checkLayer4.AutoSize = true;
            this.checkLayer4.Checked = true;
            this.checkLayer4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkLayer4.Location = new System.Drawing.Point(12, 97);
            this.checkLayer4.Name = "checkLayer4";
            this.checkLayer4.Size = new System.Drawing.Size(80, 17);
            this.checkLayer4.TabIndex = 3;
            this.checkLayer4.Text = "Foreground";
            this.checkLayer4.UseVisualStyleBackColor = true;
            this.checkLayer4.CheckedChanged += new System.EventHandler(this.checkLayer4_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Layer Visibility";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(12, 120);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(78, 17);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "Hit Objects";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // DesignToolbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(129, 161);
            this.ControlBox = false;
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkLayer4);
            this.Controls.Add(this.checkLayer3);
            this.Controls.Add(this.checkLayer2);
            this.Controls.Add(this.checkLayer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "DesignToolbox";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Design Toolbox";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkLayer1;
        private System.Windows.Forms.CheckBox checkLayer2;
        private System.Windows.Forms.CheckBox checkLayer3;
        private System.Windows.Forms.CheckBox checkLayer4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}