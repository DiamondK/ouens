﻿namespace osu.GameModes.Edit.Forms
{
    partial class CoordinateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nud_x = new System.Windows.Forms.NumericUpDown();
            this.nud_y = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_relative = new System.Windows.Forms.CheckBox();
            this.bt_ok = new System.Windows.Forms.Button();
            this.bt_cancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nud_x)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_y)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Move note(s) to...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "X:";
            // 
            // nud_x
            // 
            this.nud_x.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nud_x.Location = new System.Drawing.Point(33, 33);
            this.nud_x.Maximum = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.nud_x.Name = "nud_x";
            this.nud_x.Size = new System.Drawing.Size(98, 21);
            this.nud_x.TabIndex = 2;
            this.nud_x.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nud_x.ValueChanged += new System.EventHandler(this.nud_x_ValueChanged);
            this.nud_x.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nud_x_KeyPress);
            // 
            // nud_y
            // 
            this.nud_y.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nud_y.Location = new System.Drawing.Point(171, 33);
            this.nud_y.Maximum = new decimal(new int[] {
            384,
            0,
            0,
            0});
            this.nud_y.Name = "nud_y";
            this.nud_y.Size = new System.Drawing.Size(98, 21);
            this.nud_y.TabIndex = 4;
            this.nud_y.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nud_y.ValueChanged += new System.EventHandler(this.nud_y_ValueChanged);
            this.nud_y.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nud_y_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(150, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Y:";
            // 
            // cb_relative
            // 
            this.cb_relative.AutoSize = true;
            this.cb_relative.Location = new System.Drawing.Point(15, 63);
            this.cb_relative.Name = "cb_relative";
            this.cb_relative.Size = new System.Drawing.Size(136, 19);
            this.cb_relative.TabIndex = 5;
            this.cb_relative.Text = "Relative movement";
            this.cb_relative.UseVisualStyleBackColor = true;
            this.cb_relative.CheckedChanged += new System.EventHandler(this.cb_relative_CheckedChanged);
            // 
            // bt_ok
            // 
            this.bt_ok.Location = new System.Drawing.Point(13, 90);
            this.bt_ok.Name = "bt_ok";
            this.bt_ok.Size = new System.Drawing.Size(152, 32);
            this.bt_ok.TabIndex = 6;
            this.bt_ok.Text = "OK";
            this.bt_ok.UseVisualStyleBackColor = true;
            this.bt_ok.Click += new System.EventHandler(this.bt_ok_Click);
            // 
            // bt_cancel
            // 
            this.bt_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bt_cancel.Location = new System.Drawing.Point(171, 90);
            this.bt_cancel.Name = "bt_cancel";
            this.bt_cancel.Size = new System.Drawing.Size(98, 32);
            this.bt_cancel.TabIndex = 7;
            this.bt_cancel.Text = "Cancel";
            this.bt_cancel.UseVisualStyleBackColor = true;
            this.bt_cancel.Click += new System.EventHandler(this.bt_cancel_Click);
            // 
            // CoordinateForm
            // 
            this.AcceptButton = this.bt_ok;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.bt_cancel;
            this.ClientSize = new System.Drawing.Size(282, 128);
            this.Controls.Add(this.bt_cancel);
            this.Controls.Add(this.bt_ok);
            this.Controls.Add(this.cb_relative);
            this.Controls.Add(this.nud_y);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nud_x);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CoordinateForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Coordinate ";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CoordinateForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.nud_x)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_y)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nud_x;
        private System.Windows.Forms.NumericUpDown nud_y;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cb_relative;
        private System.Windows.Forms.Button bt_ok;
        private System.Windows.Forms.Button bt_cancel;
    }
}