﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace osu.GameModes.Edit.Forms
{


    public partial class FileColissionDialog : Form
    {
        public enum DialogOption
        {
            None,
            Yes,
            YesToAll,
            No,
            NoToAll
        }

        public DialogOption Result {get; private set;}

        public FileColissionDialog(string currentFilePath, int currentFileSize, DateTime currentFiledate,
                                   string newFilePath,     int newFileSize,     DateTime newFiledate)
        {
            InitializeComponent();
            this.LblLocationExtracted.Text = currentFilePath;
            this.LblFilesizeExtracted.Text = String.Format("{0} KB", currentFileSize / 1024);
            this.LblModifiedExtracted.Text = currentFiledate.ToString();

            this.LblLocationOsz2.Text = newFilePath;
            this.LblFilesizeOsz2.Text = String.Format("{0} KB", newFileSize / 1024);
            this.LblModifiedOsz2.Text = newFiledate.ToString();

            this.DialogResult = DialogResult.Ignore;
            

        }

        private void BtnEventHandler(object sender, EventArgs e)
        {

            if (!(sender is Button))
                return;
            else if (sender.Equals(BtnYes))
                Result = DialogOption.Yes;
            else if (sender.Equals(BtnYesToAll))
                Result = DialogOption.YesToAll;
            else if (sender.Equals(BtnNo))
                Result = DialogOption.No;
            else if (sender.Equals(BtnNoToAll))
                Result = DialogOption.NoToAll;

            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void LblFilesizeExtracted_Click(object sender, EventArgs e)
        {

        }





    }
}
