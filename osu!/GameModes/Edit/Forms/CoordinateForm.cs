﻿using Microsoft.Xna.Framework;
using osu.GameplayElements.HitObjects;
using osu_common.Helpers;
using osu.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace osu.GameModes.Edit.Forms
{
    public partial class CoordinateForm : pForm
    {
        private Vector2 relativePos;

        public CoordinateForm()
        {
            InitializeComponent();
        }

        protected override void OnShown(EventArgs e)
        {
            if (ConfigManager.sFastEditor)
                Editor.Instance.overrideIgnoreState = true;
            Editor.Instance.UndoPush();
            if (Editor.Instance.Compose.selectedObjects.Count == 1)
            {
                HitObject h = Editor.Instance.Compose.selectedObjects[0];
                float x = h.Position.X;
                float y = h.Position.Y;
                nud_y.Value = (decimal)y;
                nud_x.Value = (decimal)x;
                cb_relative.Checked = false;
            }
            else
            {
                cb_relative.Checked = true;
                relativePos = new Vector2();
            }
            base.OnShown(e);
        }

        private void bt_ok_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bt_cancel_Click(object sender, EventArgs e)
        {
            Editor.Instance.Undo();
            Editor.Instance.RedoClear();

            if (ConfigManager.sFastEditor)
                Editor.Instance.overrideIgnoreState = false;
            Close();
        }

        private void changeCoordinate()
        {
            int x = (int)nud_x.Value;
            int y = (int)nud_y.Value;
            bool relative = cb_relative.Checked;
            if (relative)
                Editor.Instance.Compose.ChangeNotesPosition(x - (int)relativePos.X, y- (int)relativePos.Y, relative);
            else
                Editor.Instance.Compose.ChangeNotesPosition(x, y, relative);
            relativePos.X = x;
            relativePos.Y = y;
        }

        private void nud_x_ValueChanged(object sender, EventArgs e)
        {
            changeCoordinate();
        }

        private void nud_y_ValueChanged(object sender, EventArgs e)
        {
            changeCoordinate();
        }

        private void cb_relative_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_relative.Checked)
            {
                nud_x.Minimum = -512;
                nud_y.Minimum = -384;
            }
            else
            {
                nud_x.Minimum = 0;
                nud_y.Minimum = 0;
            }
        }

        private void nud_x_KeyPress(object sender, KeyEventArgs e)
        {
            changeCoordinate();
        }

        private void nud_y_KeyPress(object sender, KeyEventArgs e)
        {
            changeCoordinate();
        }

        private void CoordinateForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Editor.Instance.overrideIgnoreState && ConfigManager.sFastEditor.Value)
            {
                Editor.Instance.overrideIgnoreState = false;
                Editor.Instance.UndoRedoClear();
            }
        }
    }
}
