﻿namespace osu.GameModes.Edit.Forms
{
    partial class FileColissionDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label9;
            this.BtnYes = new System.Windows.Forms.Button();
            this.BtnNo = new System.Windows.Forms.Button();
            this.BtnYesToAll = new System.Windows.Forms.Button();
            this.BtnNoToAll = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LblModifiedExtracted = new System.Windows.Forms.Label();
            this.LblFilesizeExtracted = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LblLocationExtracted = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LblModifiedOsz2 = new System.Windows.Forms.Label();
            this.LblFilesizeOsz2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LblLocationOsz2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(4, 33);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(44, 13);
            label4.TabIndex = 2;
            label4.Text = "Filesize:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(4, 29);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(44, 13);
            label9.TabIndex = 2;
            label9.Text = "Filesize:";
            // 
            // BtnYes
            // 
            this.BtnYes.Location = new System.Drawing.Point(80, 241);
            this.BtnYes.Name = "BtnYes";
            this.BtnYes.Size = new System.Drawing.Size(75, 23);
            this.BtnYes.TabIndex = 0;
            this.BtnYes.Text = "Yes";
            this.BtnYes.UseVisualStyleBackColor = true;
            this.BtnYes.Click += new System.EventHandler(this.BtnEventHandler);
            // 
            // BtnNo
            // 
            this.BtnNo.Location = new System.Drawing.Point(176, 241);
            this.BtnNo.Name = "BtnNo";
            this.BtnNo.Size = new System.Drawing.Size(75, 23);
            this.BtnNo.TabIndex = 1;
            this.BtnNo.Text = "No";
            this.BtnNo.UseVisualStyleBackColor = true;
            this.BtnNo.Click += new System.EventHandler(this.BtnEventHandler);
            // 
            // BtnYesToAll
            // 
            this.BtnYesToAll.Location = new System.Drawing.Point(276, 241);
            this.BtnYesToAll.Name = "BtnYesToAll";
            this.BtnYesToAll.Size = new System.Drawing.Size(75, 23);
            this.BtnYesToAll.TabIndex = 2;
            this.BtnYesToAll.Text = "Yes to all";
            this.BtnYesToAll.UseVisualStyleBackColor = true;
            this.BtnYesToAll.Click += new System.EventHandler(this.BtnEventHandler);
            // 
            // BtnNoToAll
            // 
            this.BtnNoToAll.Location = new System.Drawing.Point(373, 241);
            this.BtnNoToAll.Name = "BtnNoToAll";
            this.BtnNoToAll.Size = new System.Drawing.Size(75, 23);
            this.BtnNoToAll.TabIndex = 3;
            this.BtnNoToAll.Text = "No to all";
            this.BtnNoToAll.UseVisualStyleBackColor = true;
            this.BtnNoToAll.Click += new System.EventHandler(this.BtnEventHandler);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(34, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(249, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Do you want to replace the following file?";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.LblModifiedExtracted);
            this.panel1.Controls.Add(this.LblFilesizeExtracted);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(label4);
            this.panel1.Controls.Add(this.LblLocationExtracted);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(40, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(470, 66);
            this.panel1.TabIndex = 5;
            // 
            // LblModifiedExtracted
            // 
            this.LblModifiedExtracted.Location = new System.Drawing.Point(87, 46);
            this.LblModifiedExtracted.Name = "LblModifiedExtracted";
            this.LblModifiedExtracted.Size = new System.Drawing.Size(378, 13);
            this.LblModifiedExtracted.TabIndex = 5;
            // 
            // LblFilesizeExtracted
            // 
            this.LblFilesizeExtracted.Location = new System.Drawing.Point(87, 33);
            this.LblFilesizeExtracted.Name = "LblFilesizeExtracted";
            this.LblFilesizeExtracted.Size = new System.Drawing.Size(378, 13);
            this.LblFilesizeExtracted.TabIndex = 4;
            this.LblFilesizeExtracted.Click += new System.EventHandler(this.LblFilesizeExtracted_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Date modified:";
            // 
            // LblLocationExtracted
            // 
            this.LblLocationExtracted.Location = new System.Drawing.Point(84, 4);
            this.LblLocationExtracted.Name = "LblLocationExtracted";
            this.LblLocationExtracted.Size = new System.Drawing.Size(381, 26);
            this.LblLocationExtracted.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Location:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "With";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.LblModifiedOsz2);
            this.panel2.Controls.Add(this.LblFilesizeOsz2);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(label9);
            this.panel2.Controls.Add(this.LblLocationOsz2);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Location = new System.Drawing.Point(40, 153);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(470, 67);
            this.panel2.TabIndex = 7;
            // 
            // LblModifiedOsz2
            // 
            this.LblModifiedOsz2.Location = new System.Drawing.Point(87, 42);
            this.LblModifiedOsz2.Name = "LblModifiedOsz2";
            this.LblModifiedOsz2.Size = new System.Drawing.Size(378, 13);
            this.LblModifiedOsz2.TabIndex = 5;
            // 
            // LblFilesizeOsz2
            // 
            this.LblFilesizeOsz2.Location = new System.Drawing.Point(87, 29);
            this.LblFilesizeOsz2.Name = "LblFilesizeOsz2";
            this.LblFilesizeOsz2.Size = new System.Drawing.Size(382, 13);
            this.LblFilesizeOsz2.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Date modified:";
            // 
            // LblLocationOsz2
            // 
            this.LblLocationOsz2.Location = new System.Drawing.Point(84, 4);
            this.LblLocationOsz2.Name = "LblLocationOsz2";
            this.LblLocationOsz2.Size = new System.Drawing.Size(381, 25);
            this.LblLocationOsz2.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Location:";
            // 
            // FileColissionDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(557, 288);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnNoToAll);
            this.Controls.Add(this.BtnYesToAll);
            this.Controls.Add(this.BtnNo);
            this.Controls.Add(this.BtnYes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FileColissionDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Replace File?";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnYes;
        private System.Windows.Forms.Button BtnNo;
        private System.Windows.Forms.Button BtnYesToAll;
        private System.Windows.Forms.Button BtnNoToAll;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LblModifiedExtracted;
        private System.Windows.Forms.Label LblFilesizeExtracted;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LblLocationExtracted;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label LblModifiedOsz2;
        private System.Windows.Forms.Label LblFilesizeOsz2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label LblLocationOsz2;
        private System.Windows.Forms.Label label11;
    }
}