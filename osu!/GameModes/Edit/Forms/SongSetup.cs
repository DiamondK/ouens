﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using osu.Audio;
using osu.Configuration;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.Graphics.Skinning;
using osu.Helpers;
using osu_common;
using osu_common.Helpers;
using Color = Microsoft.Xna.Framework.Graphics.Color;
using osu.Helpers.Forms;
using System.Collections.Generic;
using System.IO;
using osu.GameModes.Play.Rulesets.Mania;
using osu.Input.Handlers;

namespace osu.GameModes.Edit.Forms
{
    internal partial class SongSetup : pForm
    {
        private readonly bool newDifficulty;
        private readonly bool _confirmSave;

        private bool resetSampleSettings = true;
        private bool resetVolumeSettings = true;

        private bool shiftPressed = false;

        private bool changesMade;

        internal SongSetup()
            : this(false, true)
        {
        }

        internal SongSetup(bool restrict)
            : this(restrict, true)
        {
        }

        private SwatchButton[] combos;

        private const float REAL_MIN = 0.0f;
        private const float REAL_MAX = 10.0f;

        private const float DRAIN_MIN = 0.0f;
        private const float DRAIN_MAX = 10.0f;
        private const float CS_MIN = 2.0f;
        private const float CS_MAX = 7.0f;
        private const float CS_MIN_MANIA = 1.0f;
        private const float CS_MAX_MANIA = 9.0f;
        private const float AR_MIN = 0.0f;
        private const float AR_MAX = 10.0f;
        private const float OD_MIN = 0.0f;
        private const float OD_MAX = 10.0f;


        private PlayModes originMode;

        internal SongSetup(bool newDifficulty, bool confirmSave)
        {
            this.newDifficulty = newDifficulty;
            _confirmSave = confirmSave;

            InitializeComponent();

            if (Application.RenderWithVisualStyles)
            {
                // This fixes some white boxes around controls
                foreach (TabPage tab in tabControl1.TabPages)
                {
                    tab.BackColor = System.Drawing.SystemColors.ControlLightLight;
                }
                foreach (Control c in this.Controls)
                {
                    // trackbars are retarded and don't support transparent backgrounds
                    if (c is TrackBar)
                    {
                        TrackBar t = (TrackBar)c;
                        t.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    }
                }
            }

            DialogResult = DialogResult.Cancel;
        }

        private void SongSetup_Load(object sender, EventArgs e)
        {
            combos = new[] {combo1, combo2, combo3, combo4,
                            combo5, combo6, combo7, combo8};

            List<String> files = (BeatmapManager.Current.InOszContainer) ?
                                 new List<string>(BeatmapManager.Current.Package.MapFiles) : null;

            //Restrict anything that can alter the filename of the difficulty when we are in the editor with a overridden difficulty
            //unless we are using the save as dialog.
            bool fRestrict = files != null && !newDifficulty &&
                files.Exists((f) => f.EndsWith(Path.GetFileName(BeatmapManager.Current.Filename)));

            version.Enabled = !fRestrict;

            title.Text = !string.IsNullOrEmpty(BeatmapManager.Current.TitleUnicode) ? BeatmapManager.Current.TitleUnicode : BeatmapManager.Current.Title;
            titleRomanised.Text = string.IsNullOrEmpty(BeatmapManager.Current.Title) ? GeneralHelper.AsciiOnly(BeatmapManager.Current.TitleUnicode) : BeatmapManager.Current.Title;

            artist.Text = !string.IsNullOrEmpty(BeatmapManager.Current.ArtistUnicode) ? BeatmapManager.Current.ArtistUnicode : BeatmapManager.Current.Artist;
            artistRomanised.Text = string.IsNullOrEmpty(BeatmapManager.Current.Artist) ? GeneralHelper.AsciiOnly(BeatmapManager.Current.ArtistUnicode) : BeatmapManager.Current.Artist;

            creator.Text = (BeatmapManager.Current.Creator.Length > 0
                                ? BeatmapManager.Current.Creator
                                : ConfigManager.sUsername.Value);
            source.Text = BeatmapManager.Current.Source;
            if (creator.Text == ConfigManager.sUsername || fRestrict)
                creator.Enabled = false;
            version.Text = BeatmapManager.Current.Version;

            coopmode.Checked = BeatmapManager.Current.PlayMode == PlayModes.OsuMania && BeatmapManager.Current.DifficultyCircleSize >= 10;

            // extend the trackbar a little to avoid resetting .osuhaxed values
            setDifficulty(hpDrainRate, BeatmapManager.Current.DifficultyHpDrainRate, DRAIN_MIN, DRAIN_MAX, 10.0);
            
            if (allowedModes.SelectedIndex == 3)
                setDifficulty(circleSize, BeatmapManager.Current.DifficultyCircleSize / (coopmode.Checked ? 2 : 1), CS_MIN_MANIA, CS_MAX_MANIA, 1.0); // Mania doesn't support any decimal circle size
            else
                setDifficulty(circleSize, BeatmapManager.Current.DifficultyCircleSize, CS_MIN, CS_MAX, 10.0);

            setDifficulty(approachRate, BeatmapManager.Current.DifficultyApproachRate, AR_MIN, AR_MAX, 10.0);
            setDifficulty(overallDifficulty, BeatmapManager.Current.DifficultyOverall, OD_MIN, OD_MAX, 10.0);

            // in the case where there's only one sampleset, this works.
            // when there isn't, the control is hidden anyway.
            listSampleset.SelectedItem = AudioEngine.CurrentSampleSet.ToString();
            tags.Text = BeatmapManager.Current.Tags;
            source.Text = BeatmapManager.Current.Source;
            sampleCustom.Checked = AudioEngine.CustomSamples == CustomSampleSet.Custom1;
            checkEpilepsy.Checked = BeatmapManager.Current.EpilepsyWarning;
            udCountdownOffset.Value = BeatmapManager.Current.CountdownOffset;

            checkLetterbox.Enabled = !EventManager.Instance.HasStoryboard;

            stackLeniency.Value = (int)Math.Max(2, Math.Round(BeatmapManager.Current.StackLeniency * 10));

            allowedModes.SelectedIndex = (int)BeatmapManager.Current.PlayMode;
            originMode = BeatmapManager.Current.PlayMode;

            volume1.Value = BeatmapManager.Current.SampleVolume;

            if (BeatmapManager.Current.AudioPresent)
            {
                checkSamplesMatchPlaybackRate.Checked = BeatmapManager.Current.SamplesMatchPlaybackRate;
            }
            else
            {
                checkSamplesMatchPlaybackRate.Checked = true;
                checkSamplesMatchPlaybackRate.Enabled = false;
            }

            checkCountdown.Checked = BeatmapManager.Current.Countdown != Countdown.Disabled;
            checkLetterbox.Checked = BeatmapManager.Current.LetterboxInBreaks;
            checkWidescreen.Checked = BeatmapManager.Current.WidescreenStoryboard || newDifficulty;
            checkStoryOverFire.Checked = !BeatmapManager.Current.StoryFireInFront;

            if (BeatmapManager.Current.Countdown == Countdown.Disabled)
            {
                countdownNormal.Checked = true;
            }
            else
            {
                countdownDouble.Checked = BeatmapManager.Current.Countdown == Countdown.DoubleSpeed;
                countdownHalf.Checked = BeatmapManager.Current.Countdown == Countdown.HalfSpeed;
                countdownNormal.Checked = BeatmapManager.Current.Countdown == Countdown.Normal;
            }

            SkinManager.InitializeSkinList();
            foreach (string s in SkinManager.Skins)
                skinPreference.Items.Add(s);

            if (!string.IsNullOrEmpty(BeatmapManager.Current.SkinPreference))
                skinPreference.Text = BeatmapManager.Current.SkinPreference;
            else
                skinPreference.SelectedIndex = 0;



            panelCountdownRate.Enabled = checkCountdown.Checked;

            if (AudioEngine.ControlPoints.Count > 0)
            {
                SampleSet def = AudioEngine.ControlPoints[0].sampleSet;
                CustomSampleSet cus = AudioEngine.ControlPoints[0].customSamples;
                int volume = AudioEngine.ControlPoints[0].volume;


                foreach (ControlPoint p in AudioEngine.ControlPoints)
                {
                    if (p.sampleSet != def || p.customSamples != cus || p.customSamples >= CustomSampleSet.Custom2)
                    {
                        hideSampleSettings.Visible = true;
                        resetSampleSettings = false;
                    }
                    if (p.volume != volume)
                    {
                        hideSampleVolume.Visible = true;
                        resetVolumeSettings = false;
                    }
                }

                volume1.Value = volume;
            }

            List<System.Drawing.Color> colours = new List<System.Drawing.Color>(4);

            for (int x = 0; x < SkinManager.MAX_COLOUR_COUNT; x++)
            {
                System.Drawing.Color c = OsuMathHelper.CConvert(SkinManager.LoadColour("Combo" + (x + 1).ToString()));
                if (x < 2 || c.A > 0) colours.Add(c);
            }

            while (colours.Count < 2) colours.Add(System.Drawing.Color.FromArgb(240, 240, 240));

            colours.Add(colours[0]);
            colours.RemoveAt(0);

            int y;

            for (y = 0; y < colours.Count; y++)
            {
                combos[y].SwatchColor = colours[y];
                combos[y].Visible = true;
                combos[y].Visible2 = true;
            }

            for (; y < 8; y++)
            {
                combos[y].Visible = false;
                combos[y].Visible2 = false;
            }

            backgroundColour.SwatchColor = OsuMathHelper.CConvert(EventManager.backgroundColour);

            updateCheckedColours();
            customColour.Checked = BeatmapManager.Current.CustomColours;
            cb_maniaSpecial.Checked = BeatmapManager.Current.SpecialStyle;

            if (newDifficulty || fRestrict)
            {
                version.Text = string.Empty;
                version.Select();

                artist.Enabled = false;
                title.Enabled = false;
            }
            else
            {
                artist.Select();
            }

            changesMade = false;
        }

        void SongSetup_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey || e.KeyCode == Keys.LShiftKey || e.KeyCode == Keys.RShiftKey)
            {
                shiftPressed = true;
                e.Handled = true;
            }
        }


        void SongSetup_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey || e.KeyCode == Keys.LShiftKey || e.KeyCode == Keys.RShiftKey)
            {
                shiftPressed = false;
                e.Handled = true;
            }
        }

        private void updateCheckedColours()
        {
            int x = ColourCount();

            // keep add button in the spot of the next combo. Hide it if all 8 are in use.
            if (buttonAddCombo.Visible = (x < SkinManager.MAX_COLOUR_COUNT)) buttonAddCombo.Location = combos[x].Location;

            buttonRemoveCombo.Enabled = (x > 2);
            buttonAddCombo.Enabled = (x < SkinManager.MAX_COLOUR_COUNT);
        }

        private int ColourCount()
        {
            int x;
            for (x = 0; x < SkinManager.MAX_COLOUR_COUNT; x++)
            {
                if (!combos[x].Visible2) break; // x == index of first absent combo
            }

            return x;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (!changesMade)
            {
                DialogResult = DialogResult.OK;
                Close();
                return;
            }
            //currently for mania only
            PlayModes newMode = (PlayModes)allowedModes.SelectedIndex;
            if (newMode == PlayModes.OsuMania && originMode != newMode)
            {
                if (MessageBox.Show("You changed play mode, do you want to save changes and clear all notes?", "Song Setup", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
                    return;
                else
                    Editor.Instance.hitObjectManager.Clear();
            }
            else
            {
                if (_confirmSave && (MessageBox.Show("Do you want to save changes to this beatmap?", "Song Setup", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK))
                    return;
            }

            HitObjectManager hom = Editor.Instance.hitObjectManager;
            Beatmap beatmap = hom.Beatmap;

            beatmap.Title = titleRomanised.Text;
            beatmap.TitleUnicode = title.Text;

            beatmap.Artist = artistRomanised.Text;
            beatmap.ArtistUnicode = artist.Text;

            beatmap.Creator = creator.Text;
            beatmap.Version = version.Text;
            beatmap.Source = source.Text;
            beatmap.Tags = tags.Text;
            beatmap.DifficultyHpDrainRate = hpDrainRate.Value / 10.0f;
            beatmap.DifficultyOverall = overallDifficulty.Value / 10.0f;
            beatmap.DifficultyCircleSize = allowedModes.SelectedIndex == 3 ? circleSize.Value * (coopmode.Checked ? 2 : 1) : circleSize.Value / 10.0f;
            beatmap.DifficultyApproachRate = approachRate.Value / 10.0f;
            beatmap.StackLeniency = stackLeniency.Value / 10f;
            beatmap.EpilepsyWarning = checkEpilepsy.Checked;
            beatmap.CountdownOffset = (int)udCountdownOffset.Value;
            if (newDifficulty) beatmap.BeatmapId = 0;

            beatmap.PlayMode = newMode;
            if (beatmap.PlayMode == PlayModes.OsuMania)
            {
                //Account for coop mod here
                beatmap.DifficultyCircleSize = Math.Min(CS_MAX_MANIA * 2, Math.Max(beatmap.DifficultyCircleSize, CS_MIN_MANIA));
                beatmap.SpecialStyle = cb_maniaSpecial.Checked;
            }

            SkinManager.ComputeColours(hom);
            beatmap.SkinPreference = skinPreference.SelectedIndex > 0 ? skinPreference.Text : "";

            beatmap.LetterboxInBreaks = checkLetterbox.Checked;
            beatmap.WidescreenStoryboard = checkWidescreen.Checked;
            beatmap.StoryFireInFront = !checkStoryOverFire.Checked;

            if (!checkCountdown.Checked)
                beatmap.Countdown = Countdown.Disabled;
            else if (countdownNormal.Checked)
                beatmap.Countdown = Countdown.Normal;
            else if (countdownHalf.Checked)
                beatmap.Countdown = Countdown.HalfSpeed;
            else if (countdownDouble.Checked)
                beatmap.Countdown = Countdown.DoubleSpeed;

            beatmap.SampleVolume = volume1.Value;
            beatmap.CustomColours = customColour.Checked;

            if (checkSamplesMatchPlaybackRate.Enabled)
                beatmap.SamplesMatchPlaybackRate = checkSamplesMatchPlaybackRate.Checked;

            if (customColour.Checked)
            {
                int count = ColourCount();
                int x;
                List<System.Drawing.Color> colours = new List<System.Drawing.Color>(count);

                for (x = 0; x < SkinManager.MAX_COLOUR_COUNT; x++)
                {
                    if (!combos[x].Visible2) break;
                    colours.Add(combos[x].SwatchColor);
                }

                colours.Insert(0, colours[colours.Count - 1]);
                colours.RemoveAt(colours.Count - 1);

                for (x = 0; x < colours.Count; x++)
                {
                    SkinManager.BeatmapColours["Combo" + (x + 1).ToString()] =
                        new Color(colours[x].R, colours[x].G, colours[x].B);
                }

                for (; x < SkinManager.MAX_COLOUR_COUNT; x++)
                {
                    SkinManager.BeatmapColours["Combo" + (x + 1).ToString()] =
                        Color.TransparentBlack;
                }
            }

            if (EventManager.Instance != null)
                EventManager.Instance.ChangeColour(new Color(backgroundColour.SwatchColor.R, backgroundColour.SwatchColor.G, backgroundColour.SwatchColor.B));

            HitSoundInfo info = GetSampleset();

            if (AudioEngine.ControlPoints.Count > 0)
            {
                if (resetSampleSettings)
                {
                    foreach (ControlPoint p in AudioEngine.ControlPoints)
                    {
                        p.sampleSet = info.SampleSet;
                        p.customSamples = info.CustomSampleSet;
                    }
                }

                if (resetVolumeSettings)
                {
                    foreach (ControlPoint p in AudioEngine.ControlPoints)
                    {
                        p.volume = info.Volume;
                    }
                }
            }

            Enabled = false;
            Invalidate();

            // Update all breaks to the new PreEmpt value so that they aren't mistaken for custom.
            hom.UpdateVariables(false, true);
            Editor.Instance.BreakTimeFix(true, false);

            bool result = hom.Save(false, newDifficulty, false);
            if (!result)
                DialogResult = DialogResult.Cancel;
            else
                DialogResult = DialogResult.OK;
            Editor.Instance.LoadFile(beatmap, false, false);

            Close();
        }


        private void hpDrainRate_Scroll(object sender, EventArgs e)
        {
            TrackBar tb = sender as TrackBar;
            if (!shiftPressed)
            {
                tb.Value = (int)Math.Round((double)tb.Value / 10) * 10;
            }

            UpdateTooltip(tb, 0.1);
            changesMade = true;
        }

        private void overallDifficulty_Scroll(object sender, EventArgs e)
        {
            TrackBar tb = sender as TrackBar;
            if (!shiftPressed)
            {
                tb.Value = (int)Math.Round((double)tb.Value / 10) * 10;
            }

            UpdateTooltip(tb, 0.1);
            changesMade = true;
        }

        private void circleSize_Scroll(object sender, EventArgs e)
        {
            TrackBar tb = sender as TrackBar;

            // We ignore mania for snapping to integers, because it only allows integers anyways
            if (allowedModes.SelectedIndex != 3 && !shiftPressed)
            {
                tb.Value = (int)Math.Round((double)tb.Value / 10) * 10;
            }

            UpdateTooltip(tb, allowedModes.SelectedIndex == 3 ? 1.0 : 0.1);
            changesMade = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void buttonHit_Click(object sender, EventArgs e)
        {
            HitSoundInfo info = GetSampleset();
            info.SoundType = HitObjectSoundType.Normal;
            AudioEngine.PlayHitSamples(info, 0, false);
        }

        private void buttonWhistle_Click(object sender, EventArgs e)
        {
            HitSoundInfo info = GetSampleset();
            info.SoundType = HitObjectSoundType.Whistle;
            AudioEngine.PlayHitSamples(info, 0, false);
        }

        private void buttonFinish_Click(object sender, EventArgs e)
        {
            HitSoundInfo info = GetSampleset();
            info.SoundType = HitObjectSoundType.Finish;
            AudioEngine.PlayHitSamples(info, 0, false);
        }

        private void buttonClap_Click(object sender, EventArgs e)
        {
            HitSoundInfo info = GetSampleset();
            info.SoundType = HitObjectSoundType.Clap;
            AudioEngine.PlayHitSamples(info, 0, false);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GameBase.ProcessStart(Urls.HELP_FAQ);
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GameBase.ProcessStart(General.WEB_ROOT + "/wiki/Ranking_Criteria");
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GameBase.ProcessStart(General.WEB_ROOT + "/forum/10");
        }

        private void customColour_CheckedChanged(object sender, EventArgs e)
        {
            updateCheckedColours();
            changesMade = true;
        }

        private void combo_Click(object sender, EventArgs e)
        {
            SwatchButton seNder = (SwatchButton)sender;
            colorDialog1.Color = seNder.SwatchColor;
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                seNder.SwatchColor = colorDialog1.Color;
                customColour.Checked = true;
                changesMade = true;
            }
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GameBase.ProcessStart(Urls.CUSTOM_SAMPLES);
        }

        private void backgroundColour_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = backgroundColour.BackColor;
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                backgroundColour.SwatchColor = colorDialog1.Color;
                changesMade = true;
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 3)
                updateCheckedColours();
        }

        private void volume1_Scroll(object sender, EventArgs e)
        {
            UpdateTooltip((TrackBar)sender, 1.0);
            changesMade = true;
        }

        private void sampleCustom_CheckedChanged(object sender, EventArgs e)
        {
            changesMade = true;
        }

        private void buttonAddCombo_Click(object sender, EventArgs e)
        {
            int count = ColourCount();

            colorDialog1.Color = System.Drawing.Color.FromArgb(240, 240, 240);
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                combos[count].SwatchColor = colorDialog1.Color;
                combos[count].Visible = true;
                combos[count].Visible2 = true;

                updateCheckedColours();
                customColour.Checked = true;
                changesMade = true;
            }
        }

        private void buttonRemoveCombo_Click(object sender, EventArgs e)
        {
            int count = ColourCount();
            combos[count - 1].Visible = false;
            combos[count - 1].Visible2 = false;

            updateCheckedColours();
            customColour.Checked = true;
            changesMade = true;
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GameBase.ProcessStart(Urls.CUSTOM_COUNTDOWN);
        }

        private void buttonSampleReset_Click(object sender, EventArgs e)
        {
            hideSampleSettings.Visible = false;
            resetSampleSettings = true;
            changesMade = true;
        }

        private void buttonVolumeReset_Click(object sender, EventArgs e)
        {
            hideSampleVolume.Visible = false;
            resetVolumeSettings = true;
            changesMade = true;
        }

        private void checkCountdown_CheckedChanged(object sender, EventArgs e)
        {
            panelCountdownRate.Enabled = checkCountdown.Checked;
            changesMade = true;
        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.youtube.com/watch?v=zZDozyWPtbs");
        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.youtube.com/watch?v=AGBN_9nTVzI");
        }

        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.youtube.com/watch?v=4eygAtmkCIk");
        }

        private void approachRate_Scroll(object sender, EventArgs e)
        {
            TrackBar tb = sender as TrackBar;
            if (!shiftPressed)
            {
                tb.Value = (int)Math.Round((double)tb.Value / 10) * 10;
            }

            UpdateTooltip(tb, 0.1);
            changesMade = true;
        }

        private void title_TextChanged(object sender, EventArgs e)
        {
            string ascii = GeneralHelper.AsciiOnly(title.Text);
            bool isRoman = ascii == title.Text;

            if (isRoman)
                titleRomanised.Text = title.Text;
            panelRomanisedTitle.Enabled = !isRoman;

            titleRomanised.Text = GeneralHelper.AsciiOnly(titleRomanised.Text);

            changesMade = true;
        }

        private void artist_TextChanged(object sender, EventArgs e)
        {
            string ascii = GeneralHelper.AsciiOnly(artist.Text);
            bool isRoman = ascii == artist.Text;

            if (isRoman)
                artistRomanised.Text = artist.Text;
            panelRomanisedArtist.Enabled = !isRoman;

            artistRomanised.Text = GeneralHelper.AsciiOnly(artistRomanised.Text);

            changesMade = true;
        }

        private void setChangesMade(object sender, EventArgs e)
        {
            changesMade = true;
        }

        private void setDifficulty(TrackBar control, float value, float min, float max, double factor)
        {
            value = Math.Max(REAL_MIN, Math.Min(REAL_MAX, value));

            if (value <= min) control.Minimum = (int)Math.Round(value * factor);
            else control.Minimum = (int)Math.Round(min * factor);

            if (value >= max) control.Maximum = (int)Math.Round(value * factor);
            else control.Maximum = (int)Math.Round(max * factor);

            control.Value = (int)Math.Round(value * factor);
        }

        const int TIP_X = 2;
        const int TIP_Y = 4;

        private void hpDrainRate_MouseDown(object sender, MouseEventArgs e)
        {
            if ((sender == circleSize && allowedModes.SelectedIndex == 3) || sender == stackLeniency)
                UpdateTooltip((TrackBar)sender, 1.0);
            else
                UpdateTooltip((TrackBar)sender, 0.1);
        }

        private void UpdateTooltip(TrackBar tb, double factor)
        {
            toolTip1.Show((tb.Value * factor).ToString(), tabPage4, tb.Right + TIP_X, tb.Top + TIP_Y);
        }

        private void hpDrainRate_MouseCaptureChanged(object sender, EventArgs e)
        {
            TrackBar tb = (TrackBar)sender;
            toolTip1.Hide(tabPage4);
            toolTip1.RemoveAll();
        }

        private void stackLeniency_Scroll(object sender, EventArgs e)
        {
            UpdateTooltip((TrackBar)sender, 1.0);
            changesMade = true;
        }

        private HitSoundInfo GetSampleset()
        {
            SampleSet ss = SampleSet.None;
            CustomSampleSet css = CustomSampleSet.Default;
            int volume = 100;

            if (resetSampleSettings)
            {
                if (listSampleset.SelectedItem == null) ss = SampleSet.None;
                else ss = (SampleSet)Enum.Parse(typeof(SampleSet), listSampleset.SelectedItem.ToString());
                if (sampleCustom.Checked) css = CustomSampleSet.Custom1;
            }
            else
            {
                ss = AudioEngine.ActiveControlPoint.sampleSet;
                css = AudioEngine.ActiveControlPoint.customSamples;
            }

            if (resetVolumeSettings)
            {
                volume = volume1.Value;
            }
            else
            {
                volume = AudioEngine.ActiveControlPoint.volume;
            }

            return new HitSoundInfo(HitObjectSoundType.None, ss, css, volume);
        }

        private void listSampleset_SelectedValueChanged(object sender, EventArgs e)
        {
            changesMade = true;
        }

        private void tabPage4_Enter(object sender, EventArgs e)
        {
            if (allowedModes.SelectedIndex == 3)
            {
                label13.Text = "Key Count";
                label40.Text = "The column count";
                circleSize.Minimum = (int)Math.Round(CS_MIN_MANIA);
                circleSize.Maximum = (int)Math.Round(CS_MAX_MANIA);
                circleSize.TickFrequency = 1;
                label12.Text = "1";
                label10.Text = "";
                label11.Text = "      9";
                approachRate.Enabled = false;
            }
            else
            {
                label13.Text = "Circle Size";
                label40.Text = "The radial size of hit circles and sliders";
                circleSize.Minimum = (int)Math.Round(CS_MIN * 10.0f);
                circleSize.Maximum = (int)Math.Round(CS_MAX * 10.0f);
                circleSize.TickFrequency = 10;
                label12.Text = "Large";
                label10.Text = "Normal";
                label11.Text = "Small";
                approachRate.Enabled = true;
            }
        }

        private void cb_maniaSpecial_CheckedChanged(object sender, EventArgs e)
        {
            changesMade = true;
        }

        private void allowedModes_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Reload circle size from the map - we don't want inconsistent states.
            if (allowedModes.SelectedIndex == 3)
            {
                setDifficulty(circleSize, BeatmapManager.Current.DifficultyCircleSize / (coopmode.Checked ? 2 : 1), CS_MIN_MANIA, CS_MAX_MANIA, 1.0);
                coopmode.Visible = true;
            }
            else
            {
                setDifficulty(circleSize, BeatmapManager.Current.DifficultyCircleSize, CS_MIN, CS_MAX, 10.0);
                coopmode.Visible = false;
            }
            changesMade = true;
        }

        private void circleSize_ValueChanged(object sender, EventArgs e)
        {
            coopmode.Enabled = BeatmapManager.Current.PlayMode == PlayModes.OsuMania && circleSize.Value >= 5;
            if (!coopmode.Enabled)
                coopmode.Checked = false;
        }

        private void coopmode_CheckedChanged(object sender, EventArgs e)
        {
            changesMade = true;
        }
    }
}