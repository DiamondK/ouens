﻿using System;
using osu_common.Helpers;

namespace osu.GameModes.Edit.Forms
{
    public partial class MoveEvents : pForm
    {
        public MoveEvents()
        {
            InitializeComponent();
            
        }

        protected override void OnShown(EventArgs e)
        {
            Editor.Instance.UndoPush();

            amount.SelectAll();
            
            base.OnShown(e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DoOperation();
            Close();
        }

        int lastAngle;
        bool lastPlayfield;

        private void DoOperation()
        {
            int amountInt;

            
            
            if (!Int32.TryParse(amount.Text,out amountInt))
                return;

            Editor.Instance.Design.MoveAllEvents(amountInt);
        }

        private void angle_TextChanged(object sender, EventArgs e)
        {
            int parseTest = 0;
            
            bool success = Int32.TryParse(amount.Text, out parseTest);
            button1.Enabled = success;

            if (success)
                trackBar1.Value = Math.Max(-60000,Math.Min(60000,parseTest));
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            amount.Text = trackBar1.Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GameBase.Scheduler.Add(Editor.Instance.Undo);
        }
    }
}