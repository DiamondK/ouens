﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using osu.GameModes.Edit.EditorModes;
using osu.GameplayElements.Events;

namespace osu.GameModes.Edit.Forms
{
    internal partial class DesignToolbox : Form
    {
        private readonly EditorModeDesign design;

        internal DesignToolbox(EditorModeDesign design)
        {
            this.design = design;
            InitializeComponent();
        }

        private void checkLayer1_CheckedChanged(object sender, EventArgs e)
        {
            design.ToggleLayer(StoryLayer.Background);
        }

        private void checkLayer2_CheckedChanged(object sender, EventArgs e)
        {
            design.ToggleLayer(StoryLayer.Failing);
        }

        private void checkLayer3_CheckedChanged(object sender, EventArgs e)
        {
            design.ToggleLayer(StoryLayer.Passing);
        }

        private void checkLayer4_CheckedChanged(object sender, EventArgs e)
        {
            design.ToggleLayer(StoryLayer.Foreground);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            design.ToggleHitObjects(checkBox1.Checked);
        }

        private void DesignToolbox_Load(object sender, EventArgs e)
        {

        }

        private void DesignToolbox_MouseLeave(object sender, EventArgs e)
        {
            
        }



    }
}
