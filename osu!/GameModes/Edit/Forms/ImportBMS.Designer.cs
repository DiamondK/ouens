﻿namespace osu.GameModes.Edit.Forms
{
    partial class ImportBMS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.b_file = new System.Windows.Forms.Button();
            this.tb_file = new System.Windows.Forms.TextBox();
            this.cb_meta = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cb_encode = new System.Windows.Forms.ComboBox();
            this.cb_image = new System.Windows.Forms.CheckBox();
            this.cb_sample = new System.Windows.Forms.CheckBox();
            this.cb_clear = new System.Windows.Forms.CheckBox();
            this.b_start = new System.Windows.Forms.Button();
            this.b_cancel = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.cb_effect = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // b_file
            // 
            this.b_file.Location = new System.Drawing.Point(11, 11);
            this.b_file.Name = "b_file";
            this.b_file.Size = new System.Drawing.Size(75, 23);
            this.b_file.TabIndex = 0;
            this.b_file.Text = "Select file";
            this.b_file.UseVisualStyleBackColor = true;
            this.b_file.Click += new System.EventHandler(this.b_file_Click);
            // 
            // tb_file
            // 
            this.tb_file.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_file.Location = new System.Drawing.Point(92, 12);
            this.tb_file.Name = "tb_file";
            this.tb_file.ReadOnly = true;
            this.tb_file.Size = new System.Drawing.Size(244, 21);
            this.tb_file.TabIndex = 1;
            // 
            // cb_meta
            // 
            this.cb_meta.AutoSize = true;
            this.cb_meta.Location = new System.Drawing.Point(12, 48);
            this.cb_meta.Name = "cb_meta";
            this.cb_meta.Size = new System.Drawing.Size(115, 21);
            this.cb_meta.TabIndex = 2;
            this.cb_meta.Text = "Override meta ";
            this.cb_meta.UseVisualStyleBackColor = true;
            this.cb_meta.CheckedChanged += new System.EventHandler(this.cb_meta_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(153, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "File encode";
            // 
            // cb_encode
            // 
            this.cb_encode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_encode.Enabled = false;
            this.cb_encode.FormattingEnabled = true;
            this.cb_encode.Items.AddRange(new object[] {
            "utf-8",
            "gb2312",
            "big5",
            "shift_jis"});
            this.cb_encode.Location = new System.Drawing.Point(233, 49);
            this.cb_encode.Name = "cb_encode";
            this.cb_encode.Size = new System.Drawing.Size(103, 20);
            this.cb_encode.TabIndex = 4;
            // 
            // cb_image
            // 
            this.cb_image.AutoSize = true;
            this.cb_image.Checked = true;
            this.cb_image.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_image.Location = new System.Drawing.Point(12, 79);
            this.cb_image.Name = "cb_image";
            this.cb_image.Size = new System.Drawing.Size(193, 21);
            this.cb_image.TabIndex = 5;
            this.cb_image.Text = "Override background image";
            this.cb_image.UseVisualStyleBackColor = true;
            // 
            // cb_sample
            // 
            this.cb_sample.AutoSize = true;
            this.cb_sample.Location = new System.Drawing.Point(12, 112);
            this.cb_sample.Name = "cb_sample";
            this.cb_sample.Size = new System.Drawing.Size(148, 21);
            this.cb_sample.TabIndex = 6;
            this.cb_sample.Text = "Ignore note samples";
            this.cb_sample.UseVisualStyleBackColor = true;
            // 
            // cb_clear
            // 
            this.cb_clear.AutoSize = true;
            this.cb_clear.Checked = true;
            this.cb_clear.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_clear.Location = new System.Drawing.Point(12, 183);
            this.cb_clear.Name = "cb_clear";
            this.cb_clear.Size = new System.Drawing.Size(196, 21);
            this.cb_clear.TabIndex = 7;
            this.cb_clear.Text = "Clear all notes before import";
            this.cb_clear.UseVisualStyleBackColor = true;
            // 
            // b_start
            // 
            this.b_start.Location = new System.Drawing.Point(12, 226);
            this.b_start.Name = "b_start";
            this.b_start.Size = new System.Drawing.Size(215, 38);
            this.b_start.TabIndex = 8;
            this.b_start.Text = "Start";
            this.b_start.UseVisualStyleBackColor = true;
            this.b_start.Click += new System.EventHandler(this.b_start_Click);
            // 
            // b_cancel
            // 
            this.b_cancel.Location = new System.Drawing.Point(244, 226);
            this.b_cancel.Name = "b_cancel";
            this.b_cancel.Size = new System.Drawing.Size(93, 38);
            this.b_cancel.TabIndex = 9;
            this.b_cancel.Text = "Cancel";
            this.b_cancel.UseVisualStyleBackColor = true;
            this.b_cancel.Click += new System.EventHandler(this.b_cancel_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            this.openFileDialog.Filter = "BMS file|*.bms;*.bme";
            // 
            // cb_effect
            // 
            this.cb_effect.AutoSize = true;
            this.cb_effect.Location = new System.Drawing.Point(12, 144);
            this.cb_effect.Name = "cb_effect";
            this.cb_effect.Size = new System.Drawing.Size(154, 21);
            this.cb_effect.TabIndex = 10;
            this.cb_effect.Text = "Ignore effect samples";
            this.cb_effect.UseVisualStyleBackColor = true;
            // 
            // ImportBMS
            // 
            this.AcceptButton = this.b_start;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(349, 282);
            this.Controls.Add(this.cb_effect);
            this.Controls.Add(this.b_cancel);
            this.Controls.Add(this.b_start);
            this.Controls.Add(this.cb_clear);
            this.Controls.Add(this.cb_sample);
            this.Controls.Add(this.cb_image);
            this.Controls.Add(this.cb_encode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cb_meta);
            this.Controls.Add(this.tb_file);
            this.Controls.Add(this.b_file);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportBMS";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Import from bms";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b_file;
        private System.Windows.Forms.TextBox tb_file;
        private System.Windows.Forms.CheckBox cb_meta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_encode;
        private System.Windows.Forms.CheckBox cb_image;
        private System.Windows.Forms.CheckBox cb_sample;
        private System.Windows.Forms.CheckBox cb_clear;
        private System.Windows.Forms.Button b_start;
        private System.Windows.Forms.Button b_cancel;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.CheckBox cb_effect;
    }
}