﻿namespace osu.GameModes.Edit.Forms
{
    partial class ConvertToStream
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.domainBeatSnap = new System.Windows.Forms.DomainUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numericDistanceSnap = new System.Windows.Forms.NumericUpDown();
            this.radCount = new System.Windows.Forms.RadioButton();
            this.numericCount = new System.Windows.Forms.NumericUpDown();
            this.radDistanceSnap = new System.Windows.Forms.RadioButton();
            this.trackDistanceSnap = new System.Windows.Forms.TrackBar();
            this.trackCount = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.numericDistanceSnap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackDistanceSnap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackCount)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOk.Location = new System.Drawing.Point(12, 138);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(170, 34);
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Location = new System.Drawing.Point(188, 138);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(110, 34);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // domainBeatSnap
            // 
            this.domainBeatSnap.Items.Add("1/1");
            this.domainBeatSnap.Items.Add("1/2");
            this.domainBeatSnap.Items.Add("1/3");
            this.domainBeatSnap.Items.Add("1/4");
            this.domainBeatSnap.Items.Add("1/6");
            this.domainBeatSnap.Items.Add("1/8");
            this.domainBeatSnap.Location = new System.Drawing.Point(236, 102);
            this.domainBeatSnap.Name = "domainBeatSnap";
            this.domainBeatSnap.Size = new System.Drawing.Size(50, 20);
            this.domainBeatSnap.TabIndex = 7;
            this.domainBeatSnap.Text = "fixme";
            this.domainBeatSnap.SelectedItemChanged += new System.EventHandler(this.domainBeatSnap_SelectedItemChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(130, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "Beat snap divisor:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(286, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "x";
            // 
            // numericDistanceSnap
            // 
            this.numericDistanceSnap.DecimalPlaces = 2;
            this.numericDistanceSnap.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericDistanceSnap.Location = new System.Drawing.Point(236, 57);
            this.numericDistanceSnap.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericDistanceSnap.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericDistanceSnap.Name = "numericDistanceSnap";
            this.numericDistanceSnap.Size = new System.Drawing.Size(50, 20);
            this.numericDistanceSnap.TabIndex = 6;
            this.numericDistanceSnap.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            this.numericDistanceSnap.ValueChanged += new System.EventHandler(this.numericDistanceSnap_ValueChanged);
            // 
            // radCount
            // 
            this.radCount.AutoSize = true;
            this.radCount.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radCount.Location = new System.Drawing.Point(12, 12);
            this.radCount.Name = "radCount";
            this.radCount.Size = new System.Drawing.Size(114, 20);
            this.radCount.TabIndex = 0;
            this.radCount.Text = "By object count";
            this.radCount.UseVisualStyleBackColor = true;
            this.radCount.CheckedChanged += new System.EventHandler(this.radCount_CheckedChanged);
            // 
            // numericCount
            // 
            this.numericCount.Enabled = false;
            this.numericCount.Location = new System.Drawing.Point(236, 12);
            this.numericCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericCount.Name = "numericCount";
            this.numericCount.Size = new System.Drawing.Size(50, 20);
            this.numericCount.TabIndex = 4;
            this.numericCount.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericCount.ValueChanged += new System.EventHandler(this.numericCount_ValueChanged);
            // 
            // radDistanceSnap
            // 
            this.radDistanceSnap.AutoSize = true;
            this.radDistanceSnap.Checked = true;
            this.radDistanceSnap.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radDistanceSnap.Location = new System.Drawing.Point(12, 57);
            this.radDistanceSnap.Name = "radDistanceSnap";
            this.radDistanceSnap.Size = new System.Drawing.Size(119, 20);
            this.radDistanceSnap.TabIndex = 1;
            this.radDistanceSnap.TabStop = true;
            this.radDistanceSnap.Text = "By distance snap";
            this.radDistanceSnap.UseVisualStyleBackColor = true;
            this.radDistanceSnap.CheckedChanged += new System.EventHandler(this.radDistanceSnap_CheckedChanged);
            // 
            // trackDistanceSnap
            // 
            this.trackDistanceSnap.Location = new System.Drawing.Point(125, 52);
            this.trackDistanceSnap.Maximum = 500;
            this.trackDistanceSnap.Minimum = 10;
            this.trackDistanceSnap.Name = "trackDistanceSnap";
            this.trackDistanceSnap.Size = new System.Drawing.Size(104, 45);
            this.trackDistanceSnap.TabIndex = 5;
            this.trackDistanceSnap.TickFrequency = 50;
            this.trackDistanceSnap.Value = 100;
            this.trackDistanceSnap.Scroll += new System.EventHandler(this.trackDistanceSnap_Scroll);
            // 
            // trackCount
            // 
            this.trackCount.Enabled = false;
            this.trackCount.LargeChange = 4;
            this.trackCount.Location = new System.Drawing.Point(125, 7);
            this.trackCount.Maximum = 17;
            this.trackCount.Minimum = 1;
            this.trackCount.Name = "trackCount";
            this.trackCount.Size = new System.Drawing.Size(104, 45);
            this.trackCount.TabIndex = 3;
            this.trackCount.TickFrequency = 4;
            this.trackCount.Value = 5;
            this.trackCount.ValueChanged += new System.EventHandler(this.trackCount_ValueChanged);
            // 
            // ConvertToStream
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(310, 184);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.domainBeatSnap);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericDistanceSnap);
            this.Controls.Add(this.radCount);
            this.Controls.Add(this.numericCount);
            this.Controls.Add(this.radDistanceSnap);
            this.Controls.Add(this.trackDistanceSnap);
            this.Controls.Add(this.trackCount);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConvertToStream";
            this.Text = "Convert slider to stream";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConvertToStream_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numericDistanceSnap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackDistanceSnap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radCount;
        private System.Windows.Forms.RadioButton radDistanceSnap;
        private System.Windows.Forms.TrackBar trackCount;
        private System.Windows.Forms.TrackBar trackDistanceSnap;
        private System.Windows.Forms.NumericUpDown numericCount;
        private System.Windows.Forms.NumericUpDown numericDistanceSnap;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DomainUpDown domainBeatSnap;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
    }
}