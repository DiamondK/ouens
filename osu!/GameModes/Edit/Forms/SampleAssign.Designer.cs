﻿namespace osu.GameModes.Edit.Forms
{
    partial class SampleImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.b_cancel = new System.Windows.Forms.Button();
            this.b_apply = new System.Windows.Forms.Button();
            this.b_open = new System.Windows.Forms.Button();
            this.b_reset = new System.Windows.Forms.Button();
            this.b_play = new System.Windows.Forms.Button();
            this.lb_sample = new System.Windows.Forms.ListBox();
            this.nud_vol = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.b_del = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nud_custom = new System.Windows.Forms.NumericUpDown();
            this.cb_basic = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.b_sample = new System.Windows.Forms.Button();
            this.b_list = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nud_vol)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_custom)).BeginInit();
            this.SuspendLayout();
            // 
            // b_cancel
            // 
            this.b_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.b_cancel.Location = new System.Drawing.Point(333, 305);
            this.b_cancel.Name = "b_cancel";
            this.b_cancel.Size = new System.Drawing.Size(86, 30);
            this.b_cancel.TabIndex = 4;
            this.b_cancel.Text = "Cancel";
            this.b_cancel.UseVisualStyleBackColor = true;
            this.b_cancel.Click += new System.EventHandler(b_cancel_Click);
            // 
            // b_apply
            // 
            this.b_apply.Location = new System.Drawing.Point(283, 219);
            this.b_apply.Name = "b_apply";
            this.b_apply.Size = new System.Drawing.Size(86, 30);
            this.b_apply.TabIndex = 3;
            this.b_apply.Text = "Apply";
            this.toolTip1.SetToolTip(this.b_apply, "Apply to selected notes");
            this.b_apply.UseVisualStyleBackColor = true;
            this.b_apply.Click += new System.EventHandler(b_apply_Click);
            // 
            // b_open
            // 
            this.b_open.Location = new System.Drawing.Point(232, 178);
            this.b_open.Name = "b_open";
            this.b_open.Size = new System.Drawing.Size(86, 30);
            this.b_open.TabIndex = 6;
            this.b_open.Text = "Import";
            this.b_open.UseVisualStyleBackColor = true;
            this.b_open.Click += new System.EventHandler(this.b_open_Click);
            // 
            // b_reset
            // 
            this.b_reset.Location = new System.Drawing.Point(232, 305);
            this.b_reset.Name = "b_reset";
            this.b_reset.Size = new System.Drawing.Size(86, 30);
            this.b_reset.TabIndex = 14;
            this.b_reset.Text = "Reset";
            this.toolTip1.SetToolTip(this.b_reset, "Reset selected notes\' sample");
            this.b_reset.UseVisualStyleBackColor = true;
            this.b_reset.Click += new System.EventHandler(this.b_reset_Click);
            // 
            // b_play
            // 
            this.b_play.Location = new System.Drawing.Point(284, 136);
            this.b_play.Name = "b_play";
            this.b_play.Size = new System.Drawing.Size(86, 30);
            this.b_play.TabIndex = 15;
            this.b_play.Text = "Play";
            this.b_play.UseVisualStyleBackColor = true;
            this.b_play.Click += new System.EventHandler(this.b_play_Click);
            // 
            // lb_sample
            // 
            this.lb_sample.FormattingEnabled = true;
            this.lb_sample.ItemHeight = 12;
            this.lb_sample.Location = new System.Drawing.Point(8, 11);
            this.lb_sample.Name = "lb_sample";
            this.lb_sample.Size = new System.Drawing.Size(204, 328);
            this.lb_sample.TabIndex = 16;
            this.lb_sample.SelectedIndexChanged += new System.EventHandler(this.lb_sample_SelectedIndexChanged);
            this.lb_sample.DoubleClick += new System.EventHandler(this.lb_sample_DoubleClick);
            // 
            // nud_vol
            // 
            this.nud_vol.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nud_vol.Location = new System.Drawing.Point(95, 58);
            this.nud_vol.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.nud_vol.Name = "nud_vol";
            this.nud_vol.Size = new System.Drawing.Size(85, 21);
            this.nud_vol.TabIndex = 17;
            this.nud_vol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nud_vol.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 15);
            this.label1.TabIndex = 18;
            this.label1.Text = "Volume";
            // 
            // b_del
            // 
            this.b_del.Location = new System.Drawing.Point(334, 178);
            this.b_del.Name = "b_del";
            this.b_del.Size = new System.Drawing.Size(85, 29);
            this.b_del.TabIndex = 19;
            this.b_del.Text = "Delete";
            this.b_del.UseVisualStyleBackColor = true;
            this.b_del.Click += new System.EventHandler(this.b_del_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nud_vol);
            this.groupBox1.Controls.Add(this.nud_custom);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cb_basic);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(232, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(187, 119);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Basic";
            // 
            // nud_custom
            // 
            this.nud_custom.Location = new System.Drawing.Point(95, 26);
            this.nud_custom.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nud_custom.Name = "nud_custom";
            this.nud_custom.Size = new System.Drawing.Size(85, 21);
            this.nud_custom.TabIndex = 21;
            this.nud_custom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toolTip1.SetToolTip(this.nud_custom, "0: Default\r\n1-1000: Custom");
            // 
            // cb_basic
            // 
            this.cb_basic.AutoSize = true;
            this.cb_basic.Location = new System.Drawing.Point(15, 92);
            this.cb_basic.Name = "cb_basic";
            this.cb_basic.Size = new System.Drawing.Size(128, 19);
            this.cb_basic.TabIndex = 2;
            this.cb_basic.Text = "Use basic sample";
            this.cb_basic.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "CustomSet";
            // 
            // b_sample
            // 
            this.b_sample.Location = new System.Drawing.Point(232, 262);
            this.b_sample.Name = "b_sample";
            this.b_sample.Size = new System.Drawing.Size(86, 30);
            this.b_sample.TabIndex = 21;
            this.b_sample.Text = "Sample";
            this.toolTip1.SetToolTip(this.b_sample, "Add as a sample event");
            this.b_sample.UseVisualStyleBackColor = true;
            this.b_sample.Click += new System.EventHandler(this.b_sample_Click);
            // 
            // b_list
            // 
            this.b_list.Location = new System.Drawing.Point(334, 262);
            this.b_list.Name = "b_list";
            this.b_list.Size = new System.Drawing.Size(86, 30);
            this.b_list.TabIndex = 22;
            this.b_list.Text = "Sample List";
            this.b_list.UseVisualStyleBackColor = true;
            this.b_list.Click += new System.EventHandler(this.b_list_Click);
            // 
            // SampleImport
            // 
            this.AcceptButton = this.b_apply;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.b_cancel;
            this.ClientSize = new System.Drawing.Size(431, 351);
            this.Controls.Add(this.b_list);
            this.Controls.Add(this.b_sample);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.b_del);
            this.Controls.Add(this.lb_sample);
            this.Controls.Add(this.b_play);
            this.Controls.Add(this.b_reset);
            this.Controls.Add(this.b_open);
            this.Controls.Add(this.b_cancel);
            this.Controls.Add(this.b_apply);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SampleImport";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sample import";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SampleImport_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.nud_vol)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_custom)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button b_cancel;
        private System.Windows.Forms.Button b_apply;
        private System.Windows.Forms.Button b_open;
        private System.Windows.Forms.Button b_reset;
        private System.Windows.Forms.Button b_play;
        private System.Windows.Forms.ListBox lb_sample;
        private System.Windows.Forms.NumericUpDown nud_vol;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button b_del;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nud_custom;
        private System.Windows.Forms.CheckBox cb_basic;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button b_sample;
        private System.Windows.Forms.Button b_list;
    }
}