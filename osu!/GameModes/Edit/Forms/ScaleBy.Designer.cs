﻿namespace osu.GameModes.Edit.Forms
{
    partial class ScaleBy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbOrigin = new System.Windows.Forms.GroupBox();
            this.radPlayfield = new System.Windows.Forms.RadioButton();
            this.radSelection = new System.Windows.Forms.RadioButton();
            this.btnCancel = new System.Windows.Forms.Button();
            this.trScale = new System.Windows.Forms.TrackBar();
            this.lblMul = new System.Windows.Forms.Label();
            this.txtScale = new System.Windows.Forms.TextBox();
            this.lblScale = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.gbApplyTo = new System.Windows.Forms.GroupBox();
            this.chkAxisX = new System.Windows.Forms.CheckBox();
            this.chkAxisY = new System.Windows.Forms.CheckBox();
            this.gbOrigin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trScale)).BeginInit();
            this.gbApplyTo.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbOrigin
            // 
            this.gbOrigin.Controls.Add(this.radPlayfield);
            this.gbOrigin.Controls.Add(this.radSelection);
            this.gbOrigin.Location = new System.Drawing.Point(12, 73);
            this.gbOrigin.Name = "gbOrigin";
            this.gbOrigin.Size = new System.Drawing.Size(144, 73);
            this.gbOrigin.TabIndex = 19;
            this.gbOrigin.TabStop = false;
            this.gbOrigin.Text = "Origin";
            // 
            // radPlayfield
            // 
            this.radPlayfield.AutoSize = true;
            this.radPlayfield.Checked = true;
            this.radPlayfield.Location = new System.Drawing.Point(10, 20);
            this.radPlayfield.Name = "radPlayfield";
            this.radPlayfield.Size = new System.Drawing.Size(108, 19);
            this.radPlayfield.TabIndex = 8;
            this.radPlayfield.TabStop = true;
            this.radPlayfield.Text = "Playfield Centre";
            this.radPlayfield.UseVisualStyleBackColor = true;
            // 
            // radSelection
            // 
            this.radSelection.AutoSize = true;
            this.radSelection.Location = new System.Drawing.Point(10, 41);
            this.radSelection.Name = "radSelection";
            this.radSelection.Size = new System.Drawing.Size(111, 19);
            this.radSelection.TabIndex = 9;
            this.radSelection.Text = "Selection Centre";
            this.radSelection.UseVisualStyleBackColor = true;
            this.radSelection.CheckedChanged += new System.EventHandler(this.radSelection_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(162, 152);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(95, 34);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // trScale
            // 
            this.trScale.LargeChange = 100;
            this.trScale.Location = new System.Drawing.Point(12, 28);
            this.trScale.Maximum = 2000;
            this.trScale.Minimum = 100;
            this.trScale.Name = "trScale";
            this.trScale.Size = new System.Drawing.Size(174, 45);
            this.trScale.SmallChange = 10;
            this.trScale.TabIndex = 20;
            this.trScale.TickFrequency = 100;
            this.trScale.Value = 100;
            this.trScale.Scroll += new System.EventHandler(this.trScale_Scroll);
            // 
            // lblMul
            // 
            this.lblMul.AutoSize = true;
            this.lblMul.Location = new System.Drawing.Point(242, 30);
            this.lblMul.Name = "lblMul";
            this.lblMul.Size = new System.Drawing.Size(15, 15);
            this.lblMul.TabIndex = 17;
            this.lblMul.Text = "×";
            // 
            // txtScale
            // 
            this.txtScale.Location = new System.Drawing.Point(192, 29);
            this.txtScale.Name = "txtScale";
            this.txtScale.Size = new System.Drawing.Size(48, 23);
            this.txtScale.TabIndex = 13;
            this.txtScale.Text = "0";
            this.txtScale.TextChanged += new System.EventHandler(this.txtScale_TextChanged);
            // 
            // lblScale
            // 
            this.lblScale.AutoSize = true;
            this.lblScale.Location = new System.Drawing.Point(14, 9);
            this.lblScale.Name = "lblScale";
            this.lblScale.Size = new System.Drawing.Size(207, 15);
            this.lblScale.TabIndex = 16;
            this.lblScale.Text = "Scale the selected objects’ spacing by:";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(12, 152);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(144, 34);
            this.btnOk.TabIndex = 14;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // gbApplyTo
            // 
            this.gbApplyTo.Controls.Add(this.chkAxisY);
            this.gbApplyTo.Controls.Add(this.chkAxisX);
            this.gbApplyTo.Location = new System.Drawing.Point(162, 73);
            this.gbApplyTo.Name = "gbApplyTo";
            this.gbApplyTo.Size = new System.Drawing.Size(94, 73);
            this.gbApplyTo.TabIndex = 21;
            this.gbApplyTo.TabStop = false;
            this.gbApplyTo.Text = "Apply to";
            // 
            // chkAxisX
            // 
            this.chkAxisX.AutoSize = true;
            this.chkAxisX.Checked = true;
            this.chkAxisX.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAxisX.Location = new System.Drawing.Point(9, 20);
            this.chkAxisX.Name = "chkAxisX";
            this.chkAxisX.Size = new System.Drawing.Size(57, 19);
            this.chkAxisX.TabIndex = 0;
            this.chkAxisX.Text = "X-axis";
            this.chkAxisX.UseVisualStyleBackColor = true;
            this.chkAxisX.CheckedChanged += new System.EventHandler(this.chkAxisX_CheckedChanged);
            // 
            // chkAxisY
            // 
            this.chkAxisY.AutoSize = true;
            this.chkAxisY.Checked = true;
            this.chkAxisY.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAxisY.Location = new System.Drawing.Point(9, 42);
            this.chkAxisY.Name = "chkAxisY";
            this.chkAxisY.Size = new System.Drawing.Size(57, 19);
            this.chkAxisY.TabIndex = 1;
            this.chkAxisY.Text = "Y-axis";
            this.chkAxisY.UseVisualStyleBackColor = true;
            this.chkAxisY.CheckedChanged += new System.EventHandler(this.chkAxisY_CheckedChanged);
            // 
            // ScaleBy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(268, 198);
            this.Controls.Add(this.gbApplyTo);
            this.Controls.Add(this.gbOrigin);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.trScale);
            this.Controls.Add(this.lblMul);
            this.Controls.Add(this.txtScale);
            this.Controls.Add(this.lblScale);
            this.Controls.Add(this.btnOk);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ScaleBy";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Scale by";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScaleBy_FormClosing);
            this.gbOrigin.ResumeLayout(false);
            this.gbOrigin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trScale)).EndInit();
            this.gbApplyTo.ResumeLayout(false);
            this.gbApplyTo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbOrigin;
        private System.Windows.Forms.GroupBox gbApplyTo;
        private System.Windows.Forms.RadioButton radPlayfield;
        private System.Windows.Forms.RadioButton radSelection;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TrackBar trScale;
        private System.Windows.Forms.Label lblMul;
        private System.Windows.Forms.TextBox txtScale;
        private System.Windows.Forms.Label lblScale;
        private System.Windows.Forms.CheckBox chkAxisX;
        private System.Windows.Forms.CheckBox chkAxisY;


    }
}