﻿namespace osu.GameModes.Edit.Forms
{
    partial class Polygon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.trDS = new System.Windows.Forms.TrackBar();
            this.btnOk = new System.Windows.Forms.Button();
            this.lblDS = new System.Windows.Forms.Label();
            this.lblOffset = new System.Windows.Forms.Label();
            this.trAngle = new System.Windows.Forms.TrackBar();
            this.lblDSVal = new System.Windows.Forms.Label();
            this.lblAngleVal = new System.Windows.Forms.Label();
            this.lblRepeat = new System.Windows.Forms.Label();
            this.lblEdge = new System.Windows.Forms.Label();
            this.nudRepeat = new System.Windows.Forms.NumericUpDown();
            this.nudCount = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRepeat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCount)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(242, 163);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(95, 34);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // trDS
            // 
            this.trDS.Location = new System.Drawing.Point(138, 40);
            this.trDS.Maximum = 20;
            this.trDS.Minimum = 1;
            this.trDS.Name = "trDS";
            this.trDS.Size = new System.Drawing.Size(205, 45);
            this.trDS.TabIndex = 1;
            this.trDS.Value = 1;
            this.trDS.Scroll += new System.EventHandler(this.trDS_Scroll);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(10, 163);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(226, 34);
            this.btnOk.TabIndex = 14;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblDS
            // 
            this.lblDS.AutoSize = true;
            this.lblDS.Location = new System.Drawing.Point(12, 45);
            this.lblDS.Name = "lblDS";
            this.lblDS.Size = new System.Drawing.Size(89, 17);
            this.lblDS.TabIndex = 22;
            this.lblDS.Text = "Distance snap";
            // 
            // lblOffset
            // 
            this.lblOffset.AutoSize = true;
            this.lblOffset.Location = new System.Drawing.Point(12, 81);
            this.lblOffset.Name = "lblOffset";
            this.lblOffset.Size = new System.Drawing.Size(79, 17);
            this.lblOffset.TabIndex = 23;
            this.lblOffset.Text = "Offset angle";
            // 
            // trAngle
            // 
            this.trAngle.Location = new System.Drawing.Point(138, 77);
            this.trAngle.Maximum = 180;
            this.trAngle.Name = "trAngle";
            this.trAngle.Size = new System.Drawing.Size(205, 45);
            this.trAngle.TabIndex = 24;
            this.trAngle.TickFrequency = 45;
            this.trAngle.Scroll += new System.EventHandler(this.trAngle_Scroll);
            // 
            // lblDSVal
            // 
            this.lblDSVal.AutoSize = true;
            this.lblDSVal.Location = new System.Drawing.Point(110, 46);
            this.lblDSVal.Name = "lblDSVal";
            this.lblDSVal.Size = new System.Drawing.Size(15, 17);
            this.lblDSVal.TabIndex = 25;
            this.lblDSVal.Text = "0";
            // 
            // lblAngleVal
            // 
            this.lblAngleVal.AutoSize = true;
            this.lblAngleVal.Location = new System.Drawing.Point(110, 81);
            this.lblAngleVal.Name = "lblAngleVal";
            this.lblAngleVal.Size = new System.Drawing.Size(20, 17);
            this.lblAngleVal.TabIndex = 26;
            this.lblAngleVal.Text = "0°";
            // 
            // lblRepeat
            // 
            this.lblRepeat.AutoSize = true;
            this.lblRepeat.Location = new System.Drawing.Point(27, 127);
            this.lblRepeat.Name = "lblRepeat";
            this.lblRepeat.Size = new System.Drawing.Size(49, 17);
            this.lblRepeat.TabIndex = 27;
            this.lblRepeat.Text = "Repeat";
            // 
            // lblEdge
            // 
            this.lblEdge.AutoSize = true;
            this.lblEdge.Location = new System.Drawing.Point(186, 127);
            this.lblEdge.Name = "lblEdge";
            this.lblEdge.Size = new System.Drawing.Size(43, 17);
            this.lblEdge.TabIndex = 28;
            this.lblEdge.Text = "Points";
            // 
            // nudRepeat
            // 
            this.nudRepeat.Location = new System.Drawing.Point(76, 124);
            this.nudRepeat.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudRepeat.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudRepeat.Name = "nudRepeat";
            this.nudRepeat.Size = new System.Drawing.Size(62, 21);
            this.nudRepeat.TabIndex = 29;
            this.nudRepeat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudRepeat.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudRepeat.ValueChanged += new System.EventHandler(this.nudRepeat_ValueChanged);
            // 
            // nudCount
            // 
            this.nudCount.Location = new System.Drawing.Point(253, 125);
            this.nudCount.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nudCount.Name = "nudCount";
            this.nudCount.Size = new System.Drawing.Size(77, 21);
            this.nudCount.TabIndex = 30;
            this.nudCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudCount.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nudCount.ValueChanged += new System.EventHandler(this.nudEdge_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(300, 17);
            this.label1.TabIndex = 31;
            this.label1.Text = "Try smaller distance snap if you need more points";
            // 
            // Polygon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(349, 213);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nudCount);
            this.Controls.Add(this.nudRepeat);
            this.Controls.Add(this.lblEdge);
            this.Controls.Add(this.lblRepeat);
            this.Controls.Add(this.lblAngleVal);
            this.Controls.Add(this.lblDSVal);
            this.Controls.Add(this.lblOffset);
            this.Controls.Add(this.lblDS);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.trAngle);
            this.Controls.Add(this.trDS);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Polygon";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Polygonal Circle Creation";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Polygon_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.trDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRepeat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TrackBar trDS;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label lblDS;
        private System.Windows.Forms.Label lblOffset;
        private System.Windows.Forms.TrackBar trAngle;
        private System.Windows.Forms.Label lblDSVal;
        private System.Windows.Forms.Label lblAngleVal;
        private System.Windows.Forms.Label lblRepeat;
        private System.Windows.Forms.Label lblEdge;
        private System.Windows.Forms.NumericUpDown nudRepeat;
        private System.Windows.Forms.NumericUpDown nudCount;
        private System.Windows.Forms.Label label1;


    }
}