﻿using System;
using System.Windows.Forms;
using osu_common.Helpers;
using osu.Configuration;

namespace osu.GameModes.Edit.Forms
{
    public partial class RotateBy : pForm
    {
        public RotateBy()
        {
            InitializeComponent();           
        }

        protected override void OnShown(EventArgs e)
        {
            if(ConfigManager.sFastEditor)
                Editor.Instance.overrideIgnoreState = true;
            Editor.Instance.UndoPush();

            angle.SelectAll();

            base.OnShown(e);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        int lastAngle;
        bool lastPlayfield;

        private void UpdateRotation()
        {
            int angleInt;

            
            
            if (!Int32.TryParse(angle.Text,out angleInt))
                return;

            Editor.Instance.Compose.RotateSelectionArbitrary(-lastAngle, lastPlayfield);

            lastAngle = (radAnticlockwise.Checked ? -1 : 1) * angleInt;
            lastPlayfield = radPlayfield.Checked;

            Editor.Instance.Compose.RotateSelectionArbitrary(lastAngle, lastPlayfield);
        }

        private void radDirection_CheckedChanged(object sender, EventArgs e)
        {
            radAnticlockwise.Checked = !radClockwise.Checked;
            UpdateRotation();
        }

        private void angle_TextChanged(object sender, EventArgs e)
        {
            int parseTest = 0;
            
            bool success = Int32.TryParse(angle.Text, out parseTest);
            btnOk.Enabled = success;

            if (success && parseTest >= -180 && parseTest <= 180)
                trRotate.Value = parseTest;
            
            UpdateRotation();
        }

        private void radOrigin_CheckedChanged(object sender, EventArgs e)
        {
            radSelection.Checked = !radPlayfield.Checked;
            UpdateRotation();
        }

        private void trRotate_Scroll(object sender, EventArgs e)
        {
            angle.Text = trRotate.Value.ToString();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Editor.Instance.Undo();
            Editor.Instance.RedoClear();

            if (ConfigManager.sFastEditor)
                Editor.Instance.overrideIgnoreState = false;
        }

        private void RotateBy_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Editor.Instance.overrideIgnoreState && ConfigManager.sFastEditor.Value)
            {
                Editor.Instance.overrideIgnoreState = false;
                Editor.Instance.UndoRedoClear();
            }
        }
    }
}