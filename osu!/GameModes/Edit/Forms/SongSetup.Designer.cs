﻿using osu_common;
namespace osu.GameModes.Edit.Forms
{
    partial class SongSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SongSetup));
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.source = new System.Windows.Forms.TextBox();
            this.version = new System.Windows.Forms.ComboBox();
            this.tags = new System.Windows.Forms.TextBox();
            this.cb_maniaSpecial = new System.Windows.Forms.CheckBox();
            this.buttonOk = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panelRomanisedArtist = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.artistRomanised = new System.Windows.Forms.TextBox();
            this.panelRomanisedTitle = new System.Windows.Forms.Panel();
            this.label52 = new System.Windows.Forms.Label();
            this.titleRomanised = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.artistLabel = new System.Windows.Forms.Label();
            this.creator = new System.Windows.Forms.TextBox();
            this.artist = new System.Windows.Forms.TextBox();
            this.tagsLabel = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.coopmode = new System.Windows.Forms.CheckBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.approachRate = new System.Windows.Forms.TrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.circleSize = new System.Windows.Forms.TrackBar();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.overallDifficulty = new System.Windows.Forms.TrackBar();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.hpDrainRate = new System.Windows.Forms.TrackBar();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkSamplesMatchPlaybackRate = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.hideSampleSettings = new System.Windows.Forms.Panel();
            this.buttonSampleReset = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.listSampleset = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.buttonClap = new System.Windows.Forms.Button();
            this.hideSampleVolume = new System.Windows.Forms.Panel();
            this.buttonVolumeReset = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.volume1 = new System.Windows.Forms.TrackBar();
            this.buttonHit = new System.Windows.Forms.Button();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.buttonWhistle = new System.Windows.Forms.Button();
            this.sampleCustom = new System.Windows.Forms.CheckBox();
            this.buttonFinish = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.backgroundColour = new osu.Helpers.Forms.SwatchButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.combo8 = new osu.Helpers.Forms.SwatchButton();
            this.combo7 = new osu.Helpers.Forms.SwatchButton();
            this.combo6 = new osu.Helpers.Forms.SwatchButton();
            this.buttonRemoveCombo = new System.Windows.Forms.Button();
            this.buttonAddCombo = new System.Windows.Forms.Button();
            this.combo5 = new osu.Helpers.Forms.SwatchButton();
            this.combo4 = new osu.Helpers.Forms.SwatchButton();
            this.combo3 = new osu.Helpers.Forms.SwatchButton();
            this.combo2 = new osu.Helpers.Forms.SwatchButton();
            this.combo1 = new osu.Helpers.Forms.SwatchButton();
            this.customColour = new System.Windows.Forms.CheckBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label44 = new System.Windows.Forms.Label();
            this.skinPreference = new System.Windows.Forms.ComboBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.checkEpilepsy = new System.Windows.Forms.CheckBox();
            this.checkStoryOverFire = new System.Windows.Forms.CheckBox();
            this.checkLetterbox = new System.Windows.Forms.CheckBox();
            this.checkWidescreen = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label36 = new System.Windows.Forms.Label();
            this.panelCountdownRate = new System.Windows.Forms.Panel();
            this.udCountdownOffset = new System.Windows.Forms.NumericUpDown();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.countdownDouble = new System.Windows.Forms.RadioButton();
            this.countdownNormal = new System.Windows.Forms.RadioButton();
            this.countdownHalf = new System.Windows.Forms.RadioButton();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.checkCountdown = new System.Windows.Forms.CheckBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.allowedModes = new System.Windows.Forms.ComboBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.stackLeniency = new System.Windows.Forms.TrackBar();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panelRomanisedArtist.SuspendLayout();
            this.panelRomanisedTitle.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.approachRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.circleSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.overallDifficulty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hpDrainRate)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.hideSampleSettings.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel1.SuspendLayout();
            this.hideSampleVolume.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.volume1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelCountdownRate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udCountdownOffset)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stackLeniency)).BeginInit();
            this.SuspendLayout();
            // 
            // colorDialog1
            // 
            this.colorDialog1.FullOpen = true;
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Easy",
            "Normal",
            "Hard",
            "Insane"});
            this.comboBox1.Location = new System.Drawing.Point(108, 96);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(240, 21);
            this.comboBox1.TabIndex = 36;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(55, 99);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "Difficulty";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(107, 44);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(241, 20);
            this.textBox7.TabIndex = 1;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(76, 47);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(27, 13);
            this.label23.TabIndex = 8;
            this.label23.Text = "Title";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(17, 73);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(86, 13);
            this.label24.TabIndex = 13;
            this.label24.Text = "Beatmap Creator";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(73, 21);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(30, 13);
            this.label25.TabIndex = 10;
            this.label25.Text = "Artist";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(107, 70);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(241, 20);
            this.textBox8.TabIndex = 2;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(107, 18);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(241, 20);
            this.textBox9.TabIndex = 0;
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 200;
            // 
            // source
            // 
            this.source.Location = new System.Drawing.Point(141, 170);
            this.source.Name = "source";
            this.source.Size = new System.Drawing.Size(218, 20);
            this.source.TabIndex = 6;
            this.toolTip1.SetToolTip(this.source, "If this song is from a series/game, name it here.");
            this.source.TextChanged += new System.EventHandler(this.setChangesMade);
            // 
            // version
            // 
            this.version.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.version.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.version.FormattingEnabled = true;
            this.version.Items.AddRange(new object[] {
            "Easy",
            "Normal",
            "Hard",
            "Insane"});
            this.version.Location = new System.Drawing.Point(141, 145);
            this.version.Name = "version";
            this.version.Size = new System.Drawing.Size(218, 21);
            this.version.TabIndex = 5;
            this.toolTip1.SetToolTip(this.version, "While defaults in the drop-down are well-known, you may use any difficulty name y" +
        "ou like.");
            this.version.TextChanged += new System.EventHandler(this.setChangesMade);
            // 
            // tags
            // 
            this.tags.Location = new System.Drawing.Point(141, 195);
            this.tags.Name = "tags";
            this.tags.Size = new System.Drawing.Size(218, 20);
            this.tags.TabIndex = 7;
            this.toolTip1.SetToolTip(this.tags, "Any words which can be used to identify this beatmap.  Separate with spaces.");
            this.tags.TextChanged += new System.EventHandler(this.setChangesMade);
            // 
            // cb_maniaSpecial
            // 
            this.cb_maniaSpecial.AutoSize = true;
            this.cb_maniaSpecial.Location = new System.Drawing.Point(8, 45);
            this.cb_maniaSpecial.Name = "cb_maniaSpecial";
            this.cb_maniaSpecial.Size = new System.Drawing.Size(223, 19);
            this.cb_maniaSpecial.TabIndex = 53;
            this.cb_maniaSpecial.Text = "Use special style(N+1 style) for mania";
            this.toolTip1.SetToolTip(this.cb_maniaSpecial, "Notice: The left most column is used as special column");
            this.cb_maniaSpecial.UseVisualStyleBackColor = true;
            this.cb_maniaSpecial.CheckedChanged += new System.EventHandler(this.cb_maniaSpecial_CheckedChanged);
            // 
            // buttonOk
            // 
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonOk.Location = new System.Drawing.Point(4, 392);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(232, 34);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Location = new System.Drawing.Point(242, 392);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(139, 34);
            this.button1.TabIndex = 2;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(385, 386);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(377, 360);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panelRomanisedArtist);
            this.groupBox1.Controls.Add(this.panelRomanisedTitle);
            this.groupBox1.Controls.Add(this.source);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.version);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.title);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.artistLabel);
            this.groupBox1.Controls.Add(this.creator);
            this.groupBox1.Controls.Add(this.artist);
            this.groupBox1.Controls.Add(this.tags);
            this.groupBox1.Controls.Add(this.tagsLabel);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(365, 223);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Song and Map Metadata";
            // 
            // panelRomanisedArtist
            // 
            this.panelRomanisedArtist.Controls.Add(this.label31);
            this.panelRomanisedArtist.Controls.Add(this.artistRomanised);
            this.panelRomanisedArtist.Enabled = false;
            this.panelRomanisedArtist.Location = new System.Drawing.Point(5, 45);
            this.panelRomanisedArtist.Name = "panelRomanisedArtist";
            this.panelRomanisedArtist.Size = new System.Drawing.Size(356, 23);
            this.panelRomanisedArtist.TabIndex = 1;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(1, 2);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(131, 16);
            this.label31.TabIndex = 46;
            this.label31.Text = "Romanised Artist";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // artistRomanised
            // 
            this.artistRomanised.Location = new System.Drawing.Point(136, 0);
            this.artistRomanised.Name = "artistRomanised";
            this.artistRomanised.Size = new System.Drawing.Size(218, 20);
            this.artistRomanised.TabIndex = 1;
            this.artistRomanised.TextChanged += new System.EventHandler(this.artist_TextChanged);
            // 
            // panelRomanisedTitle
            // 
            this.panelRomanisedTitle.Controls.Add(this.label52);
            this.panelRomanisedTitle.Controls.Add(this.titleRomanised);
            this.panelRomanisedTitle.Enabled = false;
            this.panelRomanisedTitle.Location = new System.Drawing.Point(6, 95);
            this.panelRomanisedTitle.Name = "panelRomanisedTitle";
            this.panelRomanisedTitle.Size = new System.Drawing.Size(355, 23);
            this.panelRomanisedTitle.TabIndex = 3;
            // 
            // label52
            // 
            this.label52.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label52.Location = new System.Drawing.Point(0, 2);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(131, 16);
            this.label52.TabIndex = 47;
            this.label52.Text = "Romanised Title";
            this.label52.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // titleRomanised
            // 
            this.titleRomanised.Location = new System.Drawing.Point(135, 0);
            this.titleRomanised.Name = "titleRomanised";
            this.titleRomanised.Size = new System.Drawing.Size(218, 20);
            this.titleRomanised.TabIndex = 3;
            this.titleRomanised.TextChanged += new System.EventHandler(this.title_TextChanged);
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label29.Location = new System.Drawing.Point(32, 172);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(105, 16);
            this.label29.TabIndex = 37;
            this.label29.Text = "Source";
            this.label29.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(32, 147);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 16);
            this.label14.TabIndex = 15;
            this.label14.Text = "Difficulty";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // title
            // 
            this.title.Location = new System.Drawing.Point(141, 70);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(218, 20);
            this.title.TabIndex = 2;
            this.title.TextChanged += new System.EventHandler(this.title_TextChanged);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(32, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Title";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(5, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 16);
            this.label1.TabIndex = 13;
            this.label1.Text = "Beatmap Creator";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // artistLabel
            // 
            this.artistLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.artistLabel.Location = new System.Drawing.Point(32, 22);
            this.artistLabel.Name = "artistLabel";
            this.artistLabel.Size = new System.Drawing.Size(105, 16);
            this.artistLabel.TabIndex = 10;
            this.artistLabel.Text = "Artist";
            this.artistLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // creator
            // 
            this.creator.Location = new System.Drawing.Point(141, 120);
            this.creator.Name = "creator";
            this.creator.Size = new System.Drawing.Size(218, 20);
            this.creator.TabIndex = 4;
            this.creator.TextChanged += new System.EventHandler(this.setChangesMade);
            // 
            // artist
            // 
            this.artist.Location = new System.Drawing.Point(141, 20);
            this.artist.Name = "artist";
            this.artist.Size = new System.Drawing.Size(218, 20);
            this.artist.TabIndex = 0;
            this.artist.TextChanged += new System.EventHandler(this.artist_TextChanged);
            // 
            // tagsLabel
            // 
            this.tagsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tagsLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tagsLabel.Location = new System.Drawing.Point(32, 197);
            this.tagsLabel.Name = "tagsLabel";
            this.tagsLabel.Size = new System.Drawing.Size(105, 16);
            this.tagsLabel.TabIndex = 39;
            this.tagsLabel.Text = "Tags";
            this.tagsLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.panel10);
            this.groupBox5.Controls.Add(this.linkLabel2);
            this.groupBox5.Controls.Add(this.linkLabel1);
            this.groupBox5.Location = new System.Drawing.Point(6, 234);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(365, 118);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "A quick note";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label30);
            this.panel10.Location = new System.Drawing.Point(6, 18);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(353, 70);
            this.panel10.TabIndex = 42;
            // 
            // label30
            // 
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(353, 70);
            this.label30.TabIndex = 41;
            this.label30.Text = "Due to the large number of beatmap submissions, the standard of approval is relat" +
    "ively high.  Please ensure your beatmap is at least timed properly, or it will l" +
    "ikely be ignored!";
            // 
            // linkLabel2
            // 
            this.linkLabel2.ActiveLinkColor = System.Drawing.SystemColors.Highlight;
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.linkLabel2.Location = new System.Drawing.Point(182, 91);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(150, 15);
            this.linkLabel2.TabIndex = 34;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Official Submission Criteria";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.SystemColors.Highlight;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.linkLabel1.Location = new System.Drawing.Point(31, 91);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(100, 15);
            this.linkLabel1.TabIndex = 33;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Editor Guide/FAQ";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox2);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(377, 360);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Difficulty";
            this.tabPage4.Enter += new System.EventHandler(this.tabPage4_Enter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.coopmode);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.label48);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label45);
            this.groupBox2.Controls.Add(this.label46);
            this.groupBox2.Controls.Add(this.label47);
            this.groupBox2.Controls.Add(this.approachRate);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.circleSize);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.overallDifficulty);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.hpDrainRate);
            this.groupBox2.Location = new System.Drawing.Point(4, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(365, 351);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Difficulty";
            // 
            // coopmode
            // 
            this.coopmode.AutoSize = true;
            this.coopmode.Location = new System.Drawing.Point(12, 124);
            this.coopmode.Name = "coopmode";
            this.coopmode.Size = new System.Drawing.Size(94, 19);
            this.coopmode.TabIndex = 60;
            this.coopmode.Text = "Co-op mode";
            this.coopmode.UseVisualStyleBackColor = true;
            this.coopmode.CheckedChanged += new System.EventHandler(this.coopmode_CheckedChanged);
            // 
            // label41
            // 
            this.label41.Location = new System.Drawing.Point(9, 293);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(350, 25);
            this.label41.TabIndex = 49;
            this.label41.Text = "The harshness of the hit window and the difficulty of spinners";
            // 
            // label40
            // 
            this.label40.Location = new System.Drawing.Point(9, 145);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(350, 19);
            this.label40.TabIndex = 48;
            this.label40.Text = "The radial size of hit circles and sliders";
            // 
            // label39
            // 
            this.label39.Location = new System.Drawing.Point(9, 69);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(350, 21);
            this.label39.TabIndex = 47;
            this.label39.Text = "The constant rate of health-bar drain throughout the song";
            // 
            // label48
            // 
            this.label48.Location = new System.Drawing.Point(9, 218);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(350, 19);
            this.label48.TabIndex = 54;
            this.label48.Text = "The speed at which objects appear";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic);
            this.label17.Location = new System.Drawing.Point(66, 331);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(229, 15);
            this.label17.TabIndex = 59;
            this.label17.Text = "Hold shift for precise difficulty adjustment.";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(327, 196);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(28, 15);
            this.label45.TabIndex = 53;
            this.label45.Text = "Fast";
            this.label45.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(122, 196);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(32, 15);
            this.label46.TabIndex = 52;
            this.label46.Text = "Slow";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label47.Location = new System.Drawing.Point(10, 176);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(89, 15);
            this.label47.TabIndex = 51;
            this.label47.Text = "Approach Rate";
            // 
            // approachRate
            // 
            this.approachRate.Location = new System.Drawing.Point(118, 168);
            this.approachRate.Name = "approachRate";
            this.approachRate.Size = new System.Drawing.Size(241, 45);
            this.approachRate.TabIndex = 2;
            this.approachRate.TickFrequency = 10;
            this.approachRate.Value = 10;
            this.approachRate.Scroll += new System.EventHandler(this.approachRate_Scroll);
            this.approachRate.MouseCaptureChanged += new System.EventHandler(this.hpDrainRate_MouseCaptureChanged);
            this.approachRate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.hpDrainRate_MouseDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(216, 124);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 15);
            this.label10.TabIndex = 31;
            this.label10.Text = "Normal";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(319, 124);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 15);
            this.label11.TabIndex = 30;
            this.label11.Text = "Small";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(122, 124);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 15);
            this.label12.TabIndex = 29;
            this.label12.Text = "Large";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(10, 102);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 15);
            this.label13.TabIndex = 27;
            this.label13.Text = "Circle Size";
            // 
            // circleSize
            // 
            this.circleSize.LargeChange = 1;
            this.circleSize.Location = new System.Drawing.Point(118, 93);
            this.circleSize.Name = "circleSize";
            this.circleSize.Size = new System.Drawing.Size(241, 45);
            this.circleSize.TabIndex = 1;
            this.circleSize.TickFrequency = 10;
            this.circleSize.Scroll += new System.EventHandler(this.circleSize_Scroll);
            this.circleSize.ValueChanged += new System.EventHandler(this.circleSize_ValueChanged);
            this.circleSize.MouseCaptureChanged += new System.EventHandler(this.hpDrainRate_MouseCaptureChanged);
            this.circleSize.MouseDown += new System.Windows.Forms.MouseEventHandler(this.hpDrainRate_MouseDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(314, 270);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 15);
            this.label6.TabIndex = 20;
            this.label6.Text = "Insane";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(122, 270);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 15);
            this.label7.TabIndex = 19;
            this.label7.Text = "Easy";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(10, 250);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 15);
            this.label8.TabIndex = 17;
            this.label8.Text = "Overall Difficulty";
            // 
            // overallDifficulty
            // 
            this.overallDifficulty.Location = new System.Drawing.Point(118, 241);
            this.overallDifficulty.Name = "overallDifficulty";
            this.overallDifficulty.Size = new System.Drawing.Size(241, 45);
            this.overallDifficulty.TabIndex = 3;
            this.overallDifficulty.TickFrequency = 10;
            this.overallDifficulty.Value = 10;
            this.overallDifficulty.Scroll += new System.EventHandler(this.overallDifficulty_Scroll);
            this.overallDifficulty.MouseCaptureChanged += new System.EventHandler(this.hpDrainRate_MouseCaptureChanged);
            this.overallDifficulty.MouseDown += new System.Windows.Forms.MouseEventHandler(this.hpDrainRate_MouseDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(314, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 15);
            this.label5.TabIndex = 16;
            this.label5.Text = "Insane";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(122, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 15);
            this.label4.TabIndex = 15;
            this.label4.Text = "Easy";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(10, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 15);
            this.label2.TabIndex = 14;
            this.label2.Text = "HP Drain Rate";
            // 
            // hpDrainRate
            // 
            this.hpDrainRate.Location = new System.Drawing.Point(118, 19);
            this.hpDrainRate.Name = "hpDrainRate";
            this.hpDrainRate.Size = new System.Drawing.Size(241, 45);
            this.hpDrainRate.TabIndex = 0;
            this.hpDrainRate.TickFrequency = 10;
            this.hpDrainRate.Value = 10;
            this.hpDrainRate.Scroll += new System.EventHandler(this.hpDrainRate_Scroll);
            this.hpDrainRate.MouseCaptureChanged += new System.EventHandler(this.hpDrainRate_MouseCaptureChanged);
            this.hpDrainRate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.hpDrainRate_MouseDown);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(377, 360);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Audio";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkSamplesMatchPlaybackRate);
            this.groupBox4.Location = new System.Drawing.Point(6, 282);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(365, 44);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Misc. Toggles";
            // 
            // checkSamplesMatchPlaybackRate
            // 
            this.checkSamplesMatchPlaybackRate.AutoSize = true;
            this.checkSamplesMatchPlaybackRate.Location = new System.Drawing.Point(8, 21);
            this.checkSamplesMatchPlaybackRate.Name = "checkSamplesMatchPlaybackRate";
            this.checkSamplesMatchPlaybackRate.Size = new System.Drawing.Size(329, 19);
            this.checkSamplesMatchPlaybackRate.TabIndex = 0;
            this.checkSamplesMatchPlaybackRate.Text = "Samples match playback rate (for fully-hitsounded maps)";
            this.checkSamplesMatchPlaybackRate.UseVisualStyleBackColor = true;
            this.checkSamplesMatchPlaybackRate.CheckedChanged += new System.EventHandler(this.setChangesMade);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.hideSampleSettings);
            this.groupBox3.Controls.Add(this.panel11);
            this.groupBox3.Controls.Add(this.panel1);
            this.groupBox3.Controls.Add(this.buttonClap);
            this.groupBox3.Controls.Add(this.hideSampleVolume);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.volume1);
            this.groupBox3.Controls.Add(this.buttonHit);
            this.groupBox3.Controls.Add(this.linkLabel4);
            this.groupBox3.Controls.Add(this.buttonWhistle);
            this.groupBox3.Controls.Add(this.sampleCustom);
            this.groupBox3.Controls.Add(this.buttonFinish);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(365, 270);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Default Sample Settings";
            // 
            // hideSampleSettings
            // 
            this.hideSampleSettings.Controls.Add(this.buttonSampleReset);
            this.hideSampleSettings.Controls.Add(this.label34);
            this.hideSampleSettings.Location = new System.Drawing.Point(8, 22);
            this.hideSampleSettings.Name = "hideSampleSettings";
            this.hideSampleSettings.Size = new System.Drawing.Size(353, 73);
            this.hideSampleSettings.TabIndex = 40;
            this.hideSampleSettings.Visible = false;
            // 
            // buttonSampleReset
            // 
            this.buttonSampleReset.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSampleReset.Location = new System.Drawing.Point(226, 40);
            this.buttonSampleReset.Name = "buttonSampleReset";
            this.buttonSampleReset.Size = new System.Drawing.Size(123, 23);
            this.buttonSampleReset.TabIndex = 0;
            this.buttonSampleReset.Text = "Reset Settings";
            this.buttonSampleReset.UseVisualStyleBackColor = true;
            this.buttonSampleReset.Click += new System.EventHandler(this.buttonSampleReset_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(5, 4);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(337, 30);
            this.label34.TabIndex = 46;
            this.label34.Text = "This beatmap has timing-section dependent volume settings.  \r\nYou can therefore n" +
    "ot set beatmap-wide settings here.";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.listSampleset);
            this.panel11.Location = new System.Drawing.Point(11, 22);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(133, 73);
            this.panel11.TabIndex = 44;
            // 
            // listSampleset
            // 
            this.listSampleset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listSampleset.FormattingEnabled = true;
            this.listSampleset.Items.AddRange(new object[] {
            "Normal",
            "Soft",
            "Drum"});
            this.listSampleset.Location = new System.Drawing.Point(0, 0);
            this.listSampleset.Name = "listSampleset";
            this.listSampleset.Size = new System.Drawing.Size(133, 73);
            this.listSampleset.TabIndex = 42;
            this.listSampleset.SelectedValueChanged += new System.EventHandler(this.listSampleset_SelectedValueChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label32);
            this.panel1.Location = new System.Drawing.Point(6, 207);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(353, 57);
            this.panel1.TabIndex = 43;
            // 
            // label32
            // 
            this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label32.Location = new System.Drawing.Point(0, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(353, 57);
            this.label32.TabIndex = 37;
            this.label32.Text = "Sample settings (sets and volume) can also be assigned independently for each tim" +
    "ing section via the timing setup panel (F6 in the editor).";
            // 
            // buttonClap
            // 
            this.buttonClap.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonClap.Location = new System.Drawing.Point(280, 170);
            this.buttonClap.Name = "buttonClap";
            this.buttonClap.Size = new System.Drawing.Size(79, 23);
            this.buttonClap.TabIndex = 5;
            this.buttonClap.Text = "Clap";
            this.buttonClap.UseVisualStyleBackColor = true;
            this.buttonClap.Click += new System.EventHandler(this.buttonClap_Click);
            // 
            // hideSampleVolume
            // 
            this.hideSampleVolume.Controls.Add(this.buttonVolumeReset);
            this.hideSampleVolume.Controls.Add(this.label33);
            this.hideSampleVolume.Location = new System.Drawing.Point(8, 101);
            this.hideSampleVolume.Name = "hideSampleVolume";
            this.hideSampleVolume.Size = new System.Drawing.Size(353, 65);
            this.hideSampleVolume.TabIndex = 41;
            this.hideSampleVolume.Visible = false;
            // 
            // buttonVolumeReset
            // 
            this.buttonVolumeReset.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonVolumeReset.Location = new System.Drawing.Point(226, 40);
            this.buttonVolumeReset.Name = "buttonVolumeReset";
            this.buttonVolumeReset.Size = new System.Drawing.Size(123, 23);
            this.buttonVolumeReset.TabIndex = 1;
            this.buttonVolumeReset.Text = "Reset Settings";
            this.buttonVolumeReset.UseVisualStyleBackColor = true;
            this.buttonVolumeReset.Click += new System.EventHandler(this.buttonVolumeReset_Click);
            // 
            // label33
            // 
            this.label33.Location = new System.Drawing.Point(4, 4);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(345, 41);
            this.label33.TabIndex = 45;
            this.label33.Text = "This beatmap has timing-section dependent volume settings.  \r\nYou can therefore n" +
    "ot set beatmap-wide settings here.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 101);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(90, 15);
            this.label15.TabIndex = 39;
            this.label15.Text = "Sample Volume";
            // 
            // volume1
            // 
            this.volume1.LargeChange = 500;
            this.volume1.Location = new System.Drawing.Point(6, 119);
            this.volume1.Maximum = 100;
            this.volume1.Name = "volume1";
            this.volume1.Size = new System.Drawing.Size(353, 45);
            this.volume1.SmallChange = 5;
            this.volume1.TabIndex = 37;
            this.volume1.TickFrequency = 10;
            this.volume1.Value = 100;
            this.volume1.Scroll += new System.EventHandler(this.volume1_Scroll);
            this.volume1.MouseCaptureChanged += new System.EventHandler(this.hpDrainRate_MouseCaptureChanged);
            this.volume1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.hpDrainRate_MouseDown);
            // 
            // buttonHit
            // 
            this.buttonHit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonHit.Location = new System.Drawing.Point(5, 170);
            this.buttonHit.Name = "buttonHit";
            this.buttonHit.Size = new System.Drawing.Size(75, 23);
            this.buttonHit.TabIndex = 2;
            this.buttonHit.Text = "Normal";
            this.buttonHit.UseVisualStyleBackColor = true;
            this.buttonHit.Click += new System.EventHandler(this.buttonHit_Click);
            // 
            // linkLabel4
            // 
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.linkLabel4.Location = new System.Drawing.Point(150, 45);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(204, 15);
            this.linkLabel4.TabIndex = 38;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "How to add custom sample overrides";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // buttonWhistle
            // 
            this.buttonWhistle.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonWhistle.Location = new System.Drawing.Point(98, 170);
            this.buttonWhistle.Name = "buttonWhistle";
            this.buttonWhistle.Size = new System.Drawing.Size(75, 23);
            this.buttonWhistle.TabIndex = 3;
            this.buttonWhistle.Text = "Whistle";
            this.buttonWhistle.UseVisualStyleBackColor = true;
            this.buttonWhistle.Click += new System.EventHandler(this.buttonWhistle_Click);
            // 
            // sampleCustom
            // 
            this.sampleCustom.AutoSize = true;
            this.sampleCustom.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.sampleCustom.Location = new System.Drawing.Point(153, 22);
            this.sampleCustom.Name = "sampleCustom";
            this.sampleCustom.Size = new System.Drawing.Size(161, 20);
            this.sampleCustom.TabIndex = 37;
            this.sampleCustom.Text = "Enable custom overrides";
            this.sampleCustom.UseVisualStyleBackColor = true;
            this.sampleCustom.CheckedChanged += new System.EventHandler(this.sampleCustom_CheckedChanged);
            // 
            // buttonFinish
            // 
            this.buttonFinish.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonFinish.Location = new System.Drawing.Point(190, 170);
            this.buttonFinish.Name = "buttonFinish";
            this.buttonFinish.Size = new System.Drawing.Size(75, 23);
            this.buttonFinish.TabIndex = 4;
            this.buttonFinish.Text = "Finish";
            this.buttonFinish.UseVisualStyleBackColor = true;
            this.buttonFinish.Click += new System.EventHandler(this.buttonFinish_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Controls.Add(this.groupBox6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(377, 360);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Colours";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.panel2);
            this.groupBox8.Controls.Add(this.backgroundColour);
            this.groupBox8.Location = new System.Drawing.Point(3, 208);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(365, 115);
            this.groupBox8.TabIndex = 39;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Playfield Background";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label35);
            this.panel2.Location = new System.Drawing.Point(6, 57);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(353, 52);
            this.panel2.TabIndex = 48;
            // 
            // label35
            // 
            this.label35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label35.Location = new System.Drawing.Point(0, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(353, 52);
            this.label35.TabIndex = 47;
            this.label35.Text = "Please note that this colour is dimmed in both editor compose mode and in gamepla" +
    "y during playtime.  It will be the exact shade of the chosen colour during break" +
    "s.";
            // 
            // backgroundColour
            // 
            this.backgroundColour.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.backgroundColour.FlatAppearance.BorderSize = 2;
            this.backgroundColour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backgroundColour.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backgroundColour.ForeColor = System.Drawing.Color.White;
            this.backgroundColour.Location = new System.Drawing.Point(7, 19);
            this.backgroundColour.Name = "backgroundColour";
            this.backgroundColour.Size = new System.Drawing.Size(352, 32);
            this.backgroundColour.SwatchColor = System.Drawing.Color.Empty;
            this.backgroundColour.TabIndex = 2;
            this.backgroundColour.Text = "Background Colour";
            this.backgroundColour.UseVisualStyleBackColor = false;
            this.backgroundColour.Click += new System.EventHandler(this.backgroundColour_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.combo8);
            this.groupBox6.Controls.Add(this.combo7);
            this.groupBox6.Controls.Add(this.combo6);
            this.groupBox6.Controls.Add(this.buttonRemoveCombo);
            this.groupBox6.Controls.Add(this.buttonAddCombo);
            this.groupBox6.Controls.Add(this.combo5);
            this.groupBox6.Controls.Add(this.combo4);
            this.groupBox6.Controls.Add(this.combo3);
            this.groupBox6.Controls.Add(this.combo2);
            this.groupBox6.Controls.Add(this.combo1);
            this.groupBox6.Controls.Add(this.customColour);
            this.groupBox6.Location = new System.Drawing.Point(3, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(365, 202);
            this.groupBox6.TabIndex = 20;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Hitcircle/Slider Combos";
            // 
            // combo8
            // 
            this.combo8.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.combo8.FlatAppearance.BorderSize = 2;
            this.combo8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.combo8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo8.ForeColor = System.Drawing.Color.White;
            this.combo8.Location = new System.Drawing.Point(185, 121);
            this.combo8.Name = "combo8";
            this.combo8.Size = new System.Drawing.Size(172, 32);
            this.combo8.SwatchColor = System.Drawing.Color.Empty;
            this.combo8.TabIndex = 46;
            this.combo8.Text = "Combo 8";
            this.combo8.UseVisualStyleBackColor = false;
            this.combo8.Click += new System.EventHandler(this.combo_Click);
            // 
            // combo7
            // 
            this.combo7.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.combo7.FlatAppearance.BorderSize = 2;
            this.combo7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.combo7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo7.ForeColor = System.Drawing.Color.White;
            this.combo7.Location = new System.Drawing.Point(7, 121);
            this.combo7.Name = "combo7";
            this.combo7.Size = new System.Drawing.Size(172, 32);
            this.combo7.SwatchColor = System.Drawing.Color.Empty;
            this.combo7.TabIndex = 45;
            this.combo7.Text = "Combo 7";
            this.combo7.UseVisualStyleBackColor = false;
            this.combo7.Click += new System.EventHandler(this.combo_Click);
            // 
            // combo6
            // 
            this.combo6.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.combo6.FlatAppearance.BorderSize = 2;
            this.combo6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.combo6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo6.ForeColor = System.Drawing.Color.White;
            this.combo6.Location = new System.Drawing.Point(185, 87);
            this.combo6.Name = "combo6";
            this.combo6.Size = new System.Drawing.Size(172, 32);
            this.combo6.SwatchColor = System.Drawing.Color.Empty;
            this.combo6.TabIndex = 44;
            this.combo6.Text = "Combo 6";
            this.combo6.UseVisualStyleBackColor = false;
            this.combo6.Click += new System.EventHandler(this.combo_Click);
            // 
            // buttonRemoveCombo
            // 
            this.buttonRemoveCombo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonRemoveCombo.Location = new System.Drawing.Point(185, 164);
            this.buttonRemoveCombo.Name = "buttonRemoveCombo";
            this.buttonRemoveCombo.Size = new System.Drawing.Size(172, 32);
            this.buttonRemoveCombo.TabIndex = 43;
            this.buttonRemoveCombo.Text = "Remove Combo Colour";
            this.buttonRemoveCombo.UseVisualStyleBackColor = true;
            this.buttonRemoveCombo.Click += new System.EventHandler(this.buttonRemoveCombo_Click);
            // 
            // buttonAddCombo
            // 
            this.buttonAddCombo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonAddCombo.Location = new System.Drawing.Point(136, 186);
            this.buttonAddCombo.Name = "buttonAddCombo";
            this.buttonAddCombo.Size = new System.Drawing.Size(172, 32);
            this.buttonAddCombo.TabIndex = 42;
            this.buttonAddCombo.Text = "New Combo Colour";
            this.buttonAddCombo.UseVisualStyleBackColor = true;
            this.buttonAddCombo.Click += new System.EventHandler(this.buttonAddCombo_Click);
            // 
            // combo5
            // 
            this.combo5.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.combo5.FlatAppearance.BorderSize = 2;
            this.combo5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.combo5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo5.ForeColor = System.Drawing.Color.White;
            this.combo5.Location = new System.Drawing.Point(7, 87);
            this.combo5.Name = "combo5";
            this.combo5.Size = new System.Drawing.Size(172, 32);
            this.combo5.SwatchColor = System.Drawing.Color.Empty;
            this.combo5.TabIndex = 41;
            this.combo5.Text = "Combo 5";
            this.combo5.UseVisualStyleBackColor = false;
            this.combo5.Click += new System.EventHandler(this.combo_Click);
            // 
            // combo4
            // 
            this.combo4.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.combo4.FlatAppearance.BorderSize = 2;
            this.combo4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.combo4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo4.ForeColor = System.Drawing.Color.White;
            this.combo4.Location = new System.Drawing.Point(185, 53);
            this.combo4.Name = "combo4";
            this.combo4.Size = new System.Drawing.Size(172, 32);
            this.combo4.SwatchColor = System.Drawing.Color.Empty;
            this.combo4.TabIndex = 4;
            this.combo4.Text = "Combo 4";
            this.combo4.UseVisualStyleBackColor = false;
            this.combo4.Click += new System.EventHandler(this.combo_Click);
            // 
            // combo3
            // 
            this.combo3.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.combo3.FlatAppearance.BorderSize = 2;
            this.combo3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.combo3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo3.ForeColor = System.Drawing.Color.White;
            this.combo3.Location = new System.Drawing.Point(7, 53);
            this.combo3.Name = "combo3";
            this.combo3.Size = new System.Drawing.Size(172, 32);
            this.combo3.SwatchColor = System.Drawing.Color.Empty;
            this.combo3.TabIndex = 3;
            this.combo3.Text = "Combo 3";
            this.combo3.UseVisualStyleBackColor = false;
            this.combo3.Click += new System.EventHandler(this.combo_Click);
            // 
            // combo2
            // 
            this.combo2.BackColor = System.Drawing.Color.Transparent;
            this.combo2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.combo2.FlatAppearance.BorderSize = 2;
            this.combo2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.combo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo2.ForeColor = System.Drawing.Color.White;
            this.combo2.Location = new System.Drawing.Point(185, 19);
            this.combo2.Name = "combo2";
            this.combo2.Size = new System.Drawing.Size(172, 32);
            this.combo2.SwatchColor = System.Drawing.Color.Empty;
            this.combo2.TabIndex = 2;
            this.combo2.Text = "Combo 2";
            this.combo2.UseVisualStyleBackColor = false;
            this.combo2.Click += new System.EventHandler(this.combo_Click);
            // 
            // combo1
            // 
            this.combo1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.combo1.FlatAppearance.BorderSize = 2;
            this.combo1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.combo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo1.ForeColor = System.Drawing.Color.White;
            this.combo1.Location = new System.Drawing.Point(7, 19);
            this.combo1.Name = "combo1";
            this.combo1.Size = new System.Drawing.Size(172, 32);
            this.combo1.SwatchColor = System.Drawing.Color.Empty;
            this.combo1.TabIndex = 1;
            this.combo1.Text = "Combo 1";
            this.combo1.UseVisualStyleBackColor = false;
            this.combo1.Click += new System.EventHandler(this.combo_Click);
            // 
            // customColour
            // 
            this.customColour.AutoSize = true;
            this.customColour.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.customColour.Location = new System.Drawing.Point(12, 171);
            this.customColour.Name = "customColour";
            this.customColour.Size = new System.Drawing.Size(156, 20);
            this.customColour.TabIndex = 0;
            this.customColour.Text = "Enable Custom Colours";
            this.customColour.UseVisualStyleBackColor = true;
            this.customColour.CheckedChanged += new System.EventHandler(this.customColour_CheckedChanged);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox12);
            this.tabPage5.Controls.Add(this.groupBox11);
            this.tabPage5.Controls.Add(this.groupBox7);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(377, 360);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Design";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label44);
            this.groupBox12.Controls.Add(this.skinPreference);
            this.groupBox12.Location = new System.Drawing.Point(6, 311);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(365, 41);
            this.groupBox12.TabIndex = 41;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Skinning";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 19);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(83, 15);
            this.label44.TabIndex = 50;
            this.label44.Text = "Preferred Skin:";
            // 
            // skinPreference
            // 
            this.skinPreference.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.skinPreference.FormattingEnabled = true;
            this.skinPreference.Items.AddRange(new object[] {
            "User\'s preference (No Change)"});
            this.skinPreference.Location = new System.Drawing.Point(95, 16);
            this.skinPreference.Name = "skinPreference";
            this.skinPreference.Size = new System.Drawing.Size(263, 21);
            this.skinPreference.TabIndex = 55;
            this.skinPreference.TextChanged += new System.EventHandler(this.setChangesMade);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.checkEpilepsy);
            this.groupBox11.Controls.Add(this.checkStoryOverFire);
            this.groupBox11.Controls.Add(this.checkLetterbox);
            this.groupBox11.Controls.Add(this.checkWidescreen);
            this.groupBox11.Location = new System.Drawing.Point(6, 189);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(365, 116);
            this.groupBox11.TabIndex = 40;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Misc. Toggles";
            // 
            // checkEpilepsy
            // 
            this.checkEpilepsy.AutoSize = true;
            this.checkEpilepsy.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkEpilepsy.Location = new System.Drawing.Point(10, 66);
            this.checkEpilepsy.Name = "checkEpilepsy";
            this.checkEpilepsy.Size = new System.Drawing.Size(330, 20);
            this.checkEpilepsy.TabIndex = 54;
            this.checkEpilepsy.Text = "Display epilepsy warning (storyboard has quick strobing)";
            this.checkEpilepsy.UseVisualStyleBackColor = true;
            this.checkEpilepsy.CheckedChanged += new System.EventHandler(this.setChangesMade);
            // 
            // checkStoryOverFire
            // 
            this.checkStoryOverFire.AutoSize = true;
            this.checkStoryOverFire.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkStoryOverFire.Location = new System.Drawing.Point(10, 44);
            this.checkStoryOverFire.Name = "checkStoryOverFire";
            this.checkStoryOverFire.Size = new System.Drawing.Size(247, 20);
            this.checkStoryOverFire.TabIndex = 53;
            this.checkStoryOverFire.Text = "Display storyboard in front of combo fire";
            this.checkStoryOverFire.UseVisualStyleBackColor = true;
            this.checkStoryOverFire.CheckedChanged += new System.EventHandler(this.setChangesMade);
            // 
            // checkLetterbox
            // 
            this.checkLetterbox.AutoSize = true;
            this.checkLetterbox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkLetterbox.Location = new System.Drawing.Point(10, 88);
            this.checkLetterbox.Name = "checkLetterbox";
            this.checkLetterbox.Size = new System.Drawing.Size(156, 20);
            this.checkLetterbox.TabIndex = 52;
            this.checkLetterbox.Text = "Letterbox during breaks";
            this.checkLetterbox.UseVisualStyleBackColor = true;
            this.checkLetterbox.CheckedChanged += new System.EventHandler(this.setChangesMade);
            // 
            // checkWidescreen
            // 
            this.checkWidescreen.AutoSize = true;
            this.checkWidescreen.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkWidescreen.Location = new System.Drawing.Point(10, 22);
            this.checkWidescreen.Margin = new System.Windows.Forms.Padding(6);
            this.checkWidescreen.Name = "checkWidescreen";
            this.checkWidescreen.Size = new System.Drawing.Size(138, 20);
            this.checkWidescreen.TabIndex = 55;
            this.checkWidescreen.Text = "Widescreen Support";
            this.checkWidescreen.UseVisualStyleBackColor = true;
            this.checkWidescreen.CheckedChanged += new System.EventHandler(this.setChangesMade);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.panel3);
            this.groupBox7.Controls.Add(this.panelCountdownRate);
            this.groupBox7.Controls.Add(this.linkLabel5);
            this.groupBox7.Controls.Add(this.checkCountdown);
            this.groupBox7.Location = new System.Drawing.Point(6, 4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(365, 179);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Countdown";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label36);
            this.panel3.Location = new System.Drawing.Point(3, 40);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(356, 50);
            this.panel3.TabIndex = 49;
            // 
            // label36
            // 
            this.label36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label36.Location = new System.Drawing.Point(0, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(356, 50);
            this.label36.TabIndex = 48;
            this.label36.Text = "If this is checked, an \"Are you ready? 3, 2, 1, GO!\" countdown will be inserted a" +
    "t the beginning of the beatmap, assuming there is enough time to do so.";
            // 
            // panelCountdownRate
            // 
            this.panelCountdownRate.Controls.Add(this.udCountdownOffset);
            this.panelCountdownRate.Controls.Add(this.label49);
            this.panelCountdownRate.Controls.Add(this.label50);
            this.panelCountdownRate.Controls.Add(this.label43);
            this.panelCountdownRate.Controls.Add(this.countdownDouble);
            this.panelCountdownRate.Controls.Add(this.countdownNormal);
            this.panelCountdownRate.Controls.Add(this.countdownHalf);
            this.panelCountdownRate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelCountdownRate.Location = new System.Drawing.Point(3, 90);
            this.panelCountdownRate.Name = "panelCountdownRate";
            this.panelCountdownRate.Size = new System.Drawing.Size(359, 86);
            this.panelCountdownRate.TabIndex = 2;
            // 
            // udCountdownOffset
            // 
            this.udCountdownOffset.Location = new System.Drawing.Point(137, 27);
            this.udCountdownOffset.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.udCountdownOffset.Name = "udCountdownOffset";
            this.udCountdownOffset.Size = new System.Drawing.Size(49, 20);
            this.udCountdownOffset.TabIndex = 51;
            this.udCountdownOffset.ValueChanged += new System.EventHandler(this.setChangesMade);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label49.Location = new System.Drawing.Point(3, 29);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(113, 15);
            this.label49.TabIndex = 50;
            this.label49.Text = "Countdown Offset:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(3, 50);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(327, 30);
            this.label50.TabIndex = 52;
            this.label50.Text = "If the countdown sounds off-time, use this to make it appear\r\none or more beats e" +
    "arly.";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label43.Location = new System.Drawing.Point(3, 3);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(112, 15);
            this.label43.TabIndex = 49;
            this.label43.Text = "Countdown Speed:";
            // 
            // countdownDouble
            // 
            this.countdownDouble.AutoSize = true;
            this.countdownDouble.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.countdownDouble.Location = new System.Drawing.Point(275, 1);
            this.countdownDouble.Name = "countdownDouble";
            this.countdownDouble.Size = new System.Drawing.Size(69, 20);
            this.countdownDouble.TabIndex = 43;
            this.countdownDouble.Text = "Double";
            this.countdownDouble.UseVisualStyleBackColor = true;
            this.countdownDouble.CheckedChanged += new System.EventHandler(this.setChangesMade);
            // 
            // countdownNormal
            // 
            this.countdownNormal.AutoSize = true;
            this.countdownNormal.Checked = true;
            this.countdownNormal.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.countdownNormal.Location = new System.Drawing.Point(206, 1);
            this.countdownNormal.Name = "countdownNormal";
            this.countdownNormal.Size = new System.Drawing.Size(71, 20);
            this.countdownNormal.TabIndex = 42;
            this.countdownNormal.TabStop = true;
            this.countdownNormal.Text = "Normal";
            this.countdownNormal.UseVisualStyleBackColor = true;
            this.countdownNormal.CheckedChanged += new System.EventHandler(this.setChangesMade);
            // 
            // countdownHalf
            // 
            this.countdownHalf.AutoSize = true;
            this.countdownHalf.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.countdownHalf.Location = new System.Drawing.Point(137, 1);
            this.countdownHalf.Name = "countdownHalf";
            this.countdownHalf.Size = new System.Drawing.Size(53, 20);
            this.countdownHalf.TabIndex = 40;
            this.countdownHalf.Text = "Half";
            this.countdownHalf.UseVisualStyleBackColor = true;
            this.countdownHalf.CheckedChanged += new System.EventHandler(this.setChangesMade);
            // 
            // linkLabel5
            // 
            this.linkLabel5.ActiveLinkColor = System.Drawing.SystemColors.Highlight;
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.linkLabel5.Location = new System.Drawing.Point(201, 19);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(158, 15);
            this.linkLabel5.TabIndex = 39;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "Customising the countdown";
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel5_LinkClicked);
            // 
            // checkCountdown
            // 
            this.checkCountdown.AutoSize = true;
            this.checkCountdown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkCountdown.Location = new System.Drawing.Point(10, 19);
            this.checkCountdown.Name = "checkCountdown";
            this.checkCountdown.Size = new System.Drawing.Size(131, 20);
            this.checkCountdown.TabIndex = 0;
            this.checkCountdown.Text = "Enable countdown";
            this.checkCountdown.UseVisualStyleBackColor = true;
            this.checkCountdown.CheckedChanged += new System.EventHandler(this.checkCountdown_CheckedChanged);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBox10);
            this.tabPage6.Controls.Add(this.groupBox9);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(377, 360);
            this.tabPage6.TabIndex = 4;
            this.tabPage6.Text = "Advanced";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.cb_maniaSpecial);
            this.groupBox10.Controls.Add(this.panel5);
            this.groupBox10.Controls.Add(this.label42);
            this.groupBox10.Controls.Add(this.allowedModes);
            this.groupBox10.Location = new System.Drawing.Point(6, 141);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(365, 158);
            this.groupBox10.TabIndex = 40;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Mode Specific";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label38);
            this.panel5.Location = new System.Drawing.Point(1, 77);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(353, 66);
            this.panel5.TabIndex = 52;
            // 
            // label38
            // 
            this.label38.Dock = System.Windows.Forms.DockStyle.Top;
            this.label38.Location = new System.Drawing.Point(0, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(353, 77);
            this.label38.TabIndex = 50;
            this.label38.Text = resources.GetString("label38.Text");
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label42.Location = new System.Drawing.Point(6, 22);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(95, 15);
            this.label42.TabIndex = 51;
            this.label42.Text = "Allowed Modes:";
            // 
            // allowedModes
            // 
            this.allowedModes.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.allowedModes.FormattingEnabled = true;
            this.allowedModes.Items.AddRange(new object[] {
            "All",
            "Taiko",
            "Catch the Beat",
            "osu!mania"});
            this.allowedModes.Location = new System.Drawing.Point(118, 19);
            this.allowedModes.Name = "allowedModes";
            this.allowedModes.Size = new System.Drawing.Size(236, 21);
            this.allowedModes.TabIndex = 41;
            this.allowedModes.SelectedIndexChanged += new System.EventHandler(this.allowedModes_SelectedIndexChanged);
            this.allowedModes.TextChanged += new System.EventHandler(this.setChangesMade);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.panel4);
            this.groupBox9.Controls.Add(this.label26);
            this.groupBox9.Controls.Add(this.label27);
            this.groupBox9.Controls.Add(this.label28);
            this.groupBox9.Controls.Add(this.stackLeniency);
            this.groupBox9.Location = new System.Drawing.Point(6, 4);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(365, 131);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Stacking";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label37);
            this.panel4.Location = new System.Drawing.Point(7, 69);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(351, 56);
            this.panel4.TabIndex = 50;
            // 
            // label37
            // 
            this.label37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label37.Location = new System.Drawing.Point(0, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(351, 56);
            this.label37.TabIndex = 49;
            this.label37.Text = "In play mode, osu! automatically stacks notes which occur at the same location.  " +
    "Increasing this value means it is more likely to snap notes of further time-dist" +
    "ance.";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(279, 51);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(75, 15);
            this.label26.TabIndex = 20;
            this.label26.Text = "Always Stack";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(122, 51);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(70, 15);
            this.label27.TabIndex = 19;
            this.label27.Text = "Rarely Stack";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(6, 24);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(93, 15);
            this.label28.TabIndex = 18;
            this.label28.Text = "Stack Leniency:";
            // 
            // stackLeniency
            // 
            this.stackLeniency.Location = new System.Drawing.Point(118, 21);
            this.stackLeniency.Minimum = 2;
            this.stackLeniency.Name = "stackLeniency";
            this.stackLeniency.Size = new System.Drawing.Size(241, 45);
            this.stackLeniency.TabIndex = 17;
            this.stackLeniency.Value = 7;
            this.stackLeniency.Scroll += new System.EventHandler(this.stackLeniency_Scroll);
            this.stackLeniency.MouseCaptureChanged += new System.EventHandler(this.hpDrainRate_MouseCaptureChanged);
            this.stackLeniency.MouseDown += new System.Windows.Forms.MouseEventHandler(this.hpDrainRate_MouseDown);
            // 
            // SongSetup
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(385, 430);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SongSetup";
            this.Opacity = 1D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Song Setup";
            this.Load += new System.EventHandler(this.SongSetup_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SongSetup_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SongSetup_KeyUp);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelRomanisedArtist.ResumeLayout(false);
            this.panelRomanisedArtist.PerformLayout();
            this.panelRomanisedTitle.ResumeLayout(false);
            this.panelRomanisedTitle.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.approachRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.circleSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.overallDifficulty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hpDrainRate)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.hideSampleSettings.ResumeLayout(false);
            this.hideSampleSettings.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.hideSampleVolume.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.volume1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panelCountdownRate.ResumeLayout(false);
            this.panelCountdownRate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udCountdownOffset)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stackLeniency)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox title;
        private System.Windows.Forms.TextBox artist;
        private System.Windows.Forms.Label artistLabel;
        private System.Windows.Forms.TextBox creator;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar hpDrainRate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TrackBar overallDifficulty;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TrackBar circleSize;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonFinish;
        private System.Windows.Forms.Button buttonWhistle;
        private System.Windows.Forms.Button buttonHit;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.CheckBox sampleCustom;
        private System.Windows.Forms.GroupBox groupBox6;
        private osu.Helpers.Forms.SwatchButton combo1;
        private System.Windows.Forms.CheckBox customColour;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private osu.Helpers.Forms.SwatchButton combo4;
        private osu.Helpers.Forms.SwatchButton combo3;
        private osu.Helpers.Forms.SwatchButton combo2;
        private osu.Helpers.Forms.SwatchButton combo5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TrackBar volume1;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.GroupBox groupBox8;
        private osu.Helpers.Forms.SwatchButton backgroundColour;
        private System.Windows.Forms.ComboBox version;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button buttonAddCombo;
        private System.Windows.Forms.Button buttonRemoveCombo;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.CheckBox checkCountdown;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.Panel hideSampleVolume;
        private System.Windows.Forms.Panel hideSampleSettings;
        private System.Windows.Forms.Button buttonSampleReset;
        private System.Windows.Forms.Button buttonVolumeReset;
        private System.Windows.Forms.RadioButton countdownHalf;
        private System.Windows.Forms.RadioButton countdownDouble;
        private System.Windows.Forms.RadioButton countdownNormal;
        private System.Windows.Forms.Panel panelCountdownRate;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TrackBar stackLeniency;
        private System.Windows.Forms.TextBox source;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox tags;
        private System.Windows.Forms.Label tagsLabel;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.CheckBox checkLetterbox;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.ComboBox skinPreference;
        private System.Windows.Forms.Button buttonClap;
        private System.Windows.Forms.CheckBox checkStoryOverFire;
        private System.Windows.Forms.ComboBox allowedModes;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.CheckBox checkEpilepsy;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TrackBar approachRate;
        private System.Windows.Forms.NumericUpDown udCountdownOffset;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private Helpers.Forms.SwatchButton combo6;
        private Helpers.Forms.SwatchButton combo8;
        private Helpers.Forms.SwatchButton combo7;
        private System.Windows.Forms.Panel panelRomanisedArtist;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox artistRomanised;
        private System.Windows.Forms.Panel panelRomanisedTitle;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox titleRomanised;
        private System.Windows.Forms.ListBox listSampleset;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.CheckBox cb_maniaSpecial;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.CheckBox checkWidescreen;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox coopmode;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkSamplesMatchPlaybackRate;
    }
}
