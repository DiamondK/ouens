﻿using osu_common.Helpers;

namespace osu.GameModes.Edit.Forms
{
    partial class BeatmapSubmissionSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.status = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.buttonModdingQueues = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonSubmissionFAQ = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonHelpForum = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonRankingCriteria = new System.Windows.Forms.Button();
            this.labelPreSubmissionInfo = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonUpload = new System.Windows.Forms.Button();
            this.panelMain = new System.Windows.Forms.Panel();
            this.checkNotify = new System.Windows.Forms.CheckBox();
            this.checkLoad = new System.Windows.Forms.CheckBox();
            this.textMessage = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panelForum = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.panelForum.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Enabled = false;
            this.buttonSubmit.Location = new System.Drawing.Point(356, 341);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(144, 37);
            this.buttonSubmit.TabIndex = 3;
            this.buttonSubmit.Text = LocalisationManager.GetString(OsuString.General_Submit);
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(506, 341);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 37);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = LocalisationManager.GetString(OsuString.General_Cancel);
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(5, 356);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(345, 22);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 4;
            // 
            // status
            // 
            this.status.AutoEllipsis = true;
            this.status.Location = new System.Drawing.Point(4, 337);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(346, 17);
            this.status.TabIndex = 9;
            this.status.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_CheckingBeatmapStatus);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.labelPreSubmissionInfo);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.buttonUpload);
            this.panel1.Location = new System.Drawing.Point(1, -3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(591, 335);
            this.panel1.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Location = new System.Drawing.Point(10, 37);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(566, 202);
            this.panel2.TabIndex = 23;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.OldLace;
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.buttonModdingQueues);
            this.panel6.Location = new System.Drawing.Point(0, 150);
            this.panel6.Margin = new System.Windows.Forms.Padding(0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(566, 51);
            this.panel6.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(20, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(289, 40);
            this.label10.TabIndex = 26;
            this.label10.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_AskModQueue);
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonModdingQueues
            // 
            this.buttonModdingQueues.BackColor = System.Drawing.Color.White;
            this.buttonModdingQueues.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonModdingQueues.Location = new System.Drawing.Point(315, 10);
            this.buttonModdingQueues.Name = "buttonModdingQueues";
            this.buttonModdingQueues.Size = new System.Drawing.Size(232, 30);
            this.buttonModdingQueues.TabIndex = 27;
            this.buttonModdingQueues.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_ModQueueForum);
            this.buttonModdingQueues.UseVisualStyleBackColor = false;
            this.buttonModdingQueues.Click += new System.EventHandler(this.buttonModdingQueues_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.OldLace;
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.buttonSubmissionFAQ);
            this.panel5.Location = new System.Drawing.Point(0, 50);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(566, 51);
            this.panel5.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(20, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(289, 41);
            this.label8.TabIndex = 23;
            this.label8.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_SubmissionWiki);
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonSubmissionFAQ
            // 
            this.buttonSubmissionFAQ.BackColor = System.Drawing.Color.White;
            this.buttonSubmissionFAQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSubmissionFAQ.Location = new System.Drawing.Point(315, 10);
            this.buttonSubmissionFAQ.Name = "buttonSubmissionFAQ";
            this.buttonSubmissionFAQ.Size = new System.Drawing.Size(232, 30);
            this.buttonSubmissionFAQ.TabIndex = 22;
            this.buttonSubmissionFAQ.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_SubmissionProcess);
            this.buttonSubmissionFAQ.UseVisualStyleBackColor = false;
            this.buttonSubmissionFAQ.Click += new System.EventHandler(this.buttonSubmissionFAQ_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FloralWhite;
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.buttonHelpForum);
            this.panel4.Location = new System.Drawing.Point(0, 100);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(566, 51);
            this.panel4.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(20, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(289, 40);
            this.label9.TabIndex = 25;
            this.label9.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_GotQuestions);
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonHelpForum
            // 
            this.buttonHelpForum.BackColor = System.Drawing.Color.White;
            this.buttonHelpForum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHelpForum.Location = new System.Drawing.Point(315, 10);
            this.buttonHelpForum.Name = "buttonHelpForum";
            this.buttonHelpForum.Size = new System.Drawing.Size(232, 30);
            this.buttonHelpForum.TabIndex = 24;
            this.buttonHelpForum.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_MappingHelpForum);
            this.buttonHelpForum.UseVisualStyleBackColor = false;
            this.buttonHelpForum.Click += new System.EventHandler(this.buttonHelpForum_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FloralWhite;
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.buttonRankingCriteria);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(566, 51);
            this.panel3.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(20, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(289, 41);
            this.label7.TabIndex = 23;
            this.label7.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_CheckGuidelines);
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonRankingCriteria
            // 
            this.buttonRankingCriteria.BackColor = System.Drawing.Color.White;
            this.buttonRankingCriteria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRankingCriteria.Location = new System.Drawing.Point(315, 9);
            this.buttonRankingCriteria.Name = "buttonRankingCriteria";
            this.buttonRankingCriteria.Size = new System.Drawing.Size(232, 30);
            this.buttonRankingCriteria.TabIndex = 22;
            this.buttonRankingCriteria.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_BeatmapRankingCriteria);
            this.buttonRankingCriteria.UseVisualStyleBackColor = false;
            this.buttonRankingCriteria.Click += new System.EventHandler(this.buttonRankingCriteria_Click);
            // 
            // labelPreSubmissionInfo
            // 
            this.labelPreSubmissionInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelPreSubmissionInfo.Location = new System.Drawing.Point(10, 260);
            this.labelPreSubmissionInfo.Name = "labelPreSubmissionInfo";
            this.labelPreSubmissionInfo.Size = new System.Drawing.Size(563, 19);
            this.labelPreSubmissionInfo.TabIndex = 20;
            this.labelPreSubmissionInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(12, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(563, 19);
            this.label5.TabIndex = 17;
            this.label5.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_OneStepFromSharing);
            // 
            // buttonUpload
            // 
            this.buttonUpload.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonUpload.Enabled = false;
            this.buttonUpload.Location = new System.Drawing.Point(5, 284);
            this.buttonUpload.Name = "buttonUpload";
            this.buttonUpload.Size = new System.Drawing.Size(576, 37);
            this.buttonUpload.TabIndex = 10;
            this.buttonUpload.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_PleaseWait);
            this.buttonUpload.UseVisualStyleBackColor = false;
            this.buttonUpload.Click += new System.EventHandler(this.buttonUpload_Click);
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelMain.Controls.Add(this.checkNotify);
            this.panelMain.Controls.Add(this.checkLoad);
            this.panelMain.Controls.Add(this.textMessage);
            this.panelMain.Controls.Add(this.label1);
            this.panelMain.Controls.Add(this.panelForum);
            this.panelMain.Enabled = false;
            this.panelMain.Location = new System.Drawing.Point(1, -3);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(591, 335);
            this.panelMain.TabIndex = 8;
            // 
            // checkNotify
            // 
            this.checkNotify.AutoSize = true;
            this.checkNotify.Checked = true;
            this.checkNotify.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkNotify.Location = new System.Drawing.Point(28, 295);
            this.checkNotify.Name = "checkNotify";
            this.checkNotify.Size = new System.Drawing.Size(208, 19);
            this.checkNotify.TabIndex = 13;
            this.checkNotify.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_EmailNotificationOnReply);
            this.checkNotify.UseVisualStyleBackColor = true;
            this.checkNotify.CheckedChanged += new System.EventHandler(this.checkLoad_CheckedChanged);
            // 
            // checkLoad
            // 
            this.checkLoad.AutoSize = true;
            this.checkLoad.Location = new System.Drawing.Point(344, 295);
            this.checkLoad.Name = "checkLoad";
            this.checkLoad.Size = new System.Drawing.Size(200, 19);
            this.checkLoad.TabIndex = 9;
            this.checkLoad.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_LoadInBrowserAfterSubmission);
            this.checkLoad.UseVisualStyleBackColor = true;
            this.checkLoad.CheckedChanged += new System.EventHandler(this.checkLoad_CheckedChanged);
            // 
            // textMessage
            // 
            this.textMessage.AcceptsReturn = true;
            this.textMessage.Location = new System.Drawing.Point(8, 121);
            this.textMessage.Multiline = true;
            this.textMessage.Name = "textMessage";
            this.textMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textMessage.Size = new System.Drawing.Size(579, 161);
            this.textMessage.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_CreatorsWords);
            // 
            // panelForum
            // 
            this.panelForum.Controls.Add(this.label6);
            this.panelForum.Controls.Add(this.radioButton2);
            this.panelForum.Controls.Add(this.radioButton1);
            this.panelForum.Location = new System.Drawing.Point(8, 12);
            this.panelForum.Name = "panelForum";
            this.panelForum.Size = new System.Drawing.Size(579, 83);
            this.panelForum.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoEllipsis = true;
            this.label6.Location = new System.Drawing.Point(4, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(571, 19);
            this.label6.TabIndex = 10;
            this.label6.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_WhereToPostBeatmap);
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radioButton2
            // 
            this.radioButton2.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton2.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.radioButton2.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.radioButton2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AntiqueWhite;
            this.radioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton2.Location = new System.Drawing.Point(297, 26);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(246, 48);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_PendingBeatmaps);
            this.radioButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton1.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.radioButton1.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.radioButton1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AntiqueWhite;
            this.radioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton1.Location = new System.Drawing.Point(35, 26);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(246, 48);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Designer_WorkInProgress);
            this.radioButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.radioButton1_MouseUp);
            // 
            // BeatmapSubmissionSystem
            // 
            this.AcceptButton = this.buttonSubmit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(593, 387);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.status);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BeatmapSubmissionSystem";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Beatmap Submission System";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.panelForum.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textMessage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelForum;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Label status;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonUpload;
        private System.Windows.Forms.CheckBox checkLoad;
        private System.Windows.Forms.CheckBox checkNotify;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonHelpForum;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonSubmissionFAQ;
        private System.Windows.Forms.Label labelPreSubmissionInfo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button buttonModdingQueues;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonRankingCriteria;
    }
}