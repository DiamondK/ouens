﻿namespace osu.GameModes.Edit.Forms
{
    partial class VolumeDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tr_music = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.lb_music = new System.Windows.Forms.Label();
            this.lb_sample = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tr_sample = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.tr_music)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tr_sample)).BeginInit();
            this.SuspendLayout();
            // 
            // tr_music
            // 
            this.tr_music.Location = new System.Drawing.Point(51, 12);
            this.tr_music.Maximum = 100;
            this.tr_music.Name = "tr_music";
            this.tr_music.Size = new System.Drawing.Size(240, 45);
            this.tr_music.TabIndex = 0;
            this.tr_music.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tr_music.Scroll += new System.EventHandler(this.tr_music_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "Music";
            // 
            // lb_music
            // 
            this.lb_music.AutoSize = true;
            this.lb_music.Location = new System.Drawing.Point(296, 17);
            this.lb_music.Name = "lb_music";
            this.lb_music.Size = new System.Drawing.Size(11, 12);
            this.lb_music.TabIndex = 2;
            this.lb_music.Text = "0";
            // 
            // lb_sample
            // 
            this.lb_sample.AutoSize = true;
            this.lb_sample.Location = new System.Drawing.Point(296, 52);
            this.lb_sample.Name = "lb_sample";
            this.lb_sample.Size = new System.Drawing.Size(11, 12);
            this.lb_sample.TabIndex = 5;
            this.lb_sample.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "Sample";
            // 
            // tr_sample
            // 
            this.tr_sample.Location = new System.Drawing.Point(51, 47);
            this.tr_sample.Maximum = 100;
            this.tr_sample.Name = "tr_sample";
            this.tr_sample.Size = new System.Drawing.Size(240, 45);
            this.tr_sample.TabIndex = 3;
            this.tr_sample.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tr_sample.Scroll += new System.EventHandler(this.tb_sample_Scroll);
            // 
            // VolumeDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(324, 89);
            this.Controls.Add(this.lb_sample);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tr_sample);
            this.Controls.Add(this.lb_music);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tr_music);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VolumeDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Volume";
            ((System.ComponentModel.ISupportInitialize)(this.tr_music)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tr_sample)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar tr_music;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lb_music;
        private System.Windows.Forms.Label lb_sample;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TrackBar tr_sample;
    }
}