﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using osu_common.Helpers;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements;
using osu.Configuration;
using osu.Audio;
using Microsoft.Xna.Framework;

namespace osu.GameModes.Edit.Forms
{
    public partial class ConvertToStream : pForm
    {
        internal ConvertToStream(SliderOsu slider)
        {
            InitializeComponent();
            m_slider = slider;
            suppress_events = true;
            switch (Editor.Instance.beatSnapDivisor)
            {
                case 3:
                case 6:
                    domainBeatSnap.SelectedIndex = 2;
                    break;
                default:
                    domainBeatSnap.SelectedIndex = 3;
                    break;
            }

            suppress_events = false;
        }

        private SliderOsu m_slider;

        protected override void OnShown(EventArgs e)
        {
            if (ConfigManager.sFastEditor)
                Editor.Instance.overrideIgnoreState = true;
            Editor.Instance.UndoPush();
            Editor.Instance.SelectNone();
            Editor.Instance.hitObjectManager.Remove(m_slider, false);
            UpdateStream();
            base.OnShown(e);
        }

        private void radCount_CheckedChanged(object sender, EventArgs e)
        {
            if (radCount.Checked)
            {
                radDistanceSnap.Checked = false;
                trackCount.Enabled = true;
                numericCount.Enabled = true;
                trackDistanceSnap.Enabled = false;
                numericDistanceSnap.Enabled = false;

                UpdateStream();
            }
        }

        private void radDistanceSnap_CheckedChanged(object sender, EventArgs e)
        {
            if (radDistanceSnap.Checked)
            {
                radCount.Checked = false;
                trackCount.Enabled = false;
                numericCount.Enabled = false;
                trackDistanceSnap.Enabled = true;
                numericDistanceSnap.Enabled = true;

                UpdateStream();
            }
        }

        private bool suppress_events = false;

        private void SetWithConstraints(TrackBar control, int value)
        {
            if (value <= control.Minimum) control.Value = control.Minimum;
            else if (value >= control.Maximum) control.Value = control.Maximum;
            else control.Value = value;
        }

        private void SetWithConstraints(NumericUpDown control, decimal value)
        {
            if (value <= control.Minimum) control.Value = control.Minimum;
            else if (value >= control.Maximum) control.Value = control.Maximum;
            else control.Value = value;
        }

        private void trackCount_ValueChanged(object sender, EventArgs e)
        {
            if (suppress_events) return;
            suppress_events = true;
            SetWithConstraints(numericCount, (decimal)(trackCount.Value));
            suppress_events = false;

            UpdateStream();
        }

        private void trackDistanceSnap_Scroll(object sender, EventArgs e)
        {
            if (suppress_events) return;
            suppress_events = true;
            SetWithConstraints(numericDistanceSnap, (decimal)(trackDistanceSnap.Value) * 0.01m);
            suppress_events = false;

            UpdateStream();
        }

        private void numericCount_ValueChanged(object sender, EventArgs e)
        {
            if (suppress_events) return;
            suppress_events = true;
            SetWithConstraints(trackCount, (int)(numericCount.Value));
            suppress_events = false;

            UpdateStream();
        }

        private void numericDistanceSnap_ValueChanged(object sender, EventArgs e)
        {
            if (suppress_events) return;
            suppress_events = true;
            SetWithConstraints(trackDistanceSnap, (int)(numericDistanceSnap.Value * 100.0m));
            suppress_events = false;

            UpdateStream();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Editor.Instance.Undo();
            Editor.Instance.RedoClear();

            if (ConfigManager.sFastEditor)
                Editor.Instance.overrideIgnoreState = false;
        }

        double lastSpacing = 0.0d;

        private void UpdateStream()
        {
            int start = m_slider.StartTime;
            double length = m_slider.SpatialLength;
            double beatsnap = (double)GetBeatSnap();

            double spacing;
            int count;

            if (radCount.Checked)
            {
                count = (int)(numericCount.Value);
                spacing = length / (double)Math.Max(1, count - 1);
            }
            else
            {
                spacing = m_slider.Velocity * AudioEngine.beatLengthAt(start, false) * (double)(numericDistanceSnap.Value) / (beatsnap * 1000);
                count = (int)((length + 1.0d) / spacing) + 1;
            }

            if (spacing == lastSpacing) return;

            double progress = 0.0d;
            double increment = AudioEngine.beatLengthAt(start, false) / beatsnap;
            double time = start;

            if (Editor.Instance.Compose.selectedObjects.Count > 0)
            {
                Editor.Instance.hitObjectManager.Remove(Editor.Instance.Compose.selectedObjects.ToArray());
                Editor.Instance.Compose.selectedObjects.Clear();
            }

            for (int x = 0; x < count; x++)
            {
                Vector2 position = m_slider.positionAtLength((float)progress);

                HitCircleOsu h = new HitCircleOsu(Editor.Instance.hitObjectManager, position, (int)time, x == 0 && m_slider.NewCombo, false, false, false, 0);
                Editor.Instance.hitObjectManager.Add(h, true);
                Editor.Instance.Compose.Select(h);

                time += increment;
                progress += spacing;
            }
            Editor.Instance.hitObjectManager.Sort(true);
        }

        private int GetBeatSnap()
        {
            switch (domainBeatSnap.SelectedIndex)
            {
                case 0:
                    return 1;
                case 1:
                    return 2;
                case 2:
                    return 3;
                case 3:
                    return 4;
                case 4:
                    return 6;
                case 5:
                    return 8;
                default:
                    return 1; // unreachable
            }
        }

        private void domainBeatSnap_SelectedItemChanged(object sender, EventArgs e)
        {
            if (suppress_events) return;
            UpdateStream();
        }

        private void ConvertToStream_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Editor.Instance.overrideIgnoreState && ConfigManager.sFastEditor.Value)
            {
                Editor.Instance.overrideIgnoreState = false;
                Editor.Instance.UndoRedoClear();
            }
        }
    }
}
