﻿using osu.Audio;
using osu.GameplayElements.Events;
using osu_common.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace osu.GameModes.Edit.Forms
{
    public partial class SampleList : pForm
    {
        public SampleList()
        {
            InitializeComponent();
        }

        protected override void OnShown(EventArgs e)
        {
            List<EventSample> list = Editor.Instance.eventManager.eventSamples;
            foreach (EventSample evt in list)
            {
                lb_sample.Items.Add(evt);
                if (evt.StartTime == AudioEngine.Time)
                    lb_sample.SetSelected(lb_sample.Items.Count - 1, true);
            }
            
            base.OnShown(e);
        }

        private void b_close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void b_del_Click(object sender, EventArgs e)
        {
            if (lb_sample.SelectedItems == null || lb_sample.SelectedItems.Count == 0)
                return;
            Editor.Instance.UndoPush();
            foreach (EventSample evt in lb_sample.SelectedItems)
            {
                Editor.Instance.eventManager.Remove(evt);
            }
            
        }
    }
}
