﻿using System;
using osu_common.Helpers;
using osu.Helpers;
using System.Windows.Forms;
using System.Drawing;

namespace osu.GameModes.Edit.Forms
{
    public partial class GotoTime : pForm
    {
        public GotoTime(string currTime)
        {
            InitializeComponent();

            time.Text = currTime;
            time.SelectAll();

            if (AeroGlassHelper.AeroEnabled)
            {
                this.NonClientHitTest += GotoTime_NonClientTest;
                this.Text = "";
            }
            else
            {
                FormBorderStyle = FormBorderStyle.FixedToolWindow;
                label1.Visible = false;
                time.Location = new Point(12, 12);
                this.ClientSize = new Size(this.ClientSize.Width, time.Height + 24);

                if (!Application.RenderWithVisualStyles) this.BackColor = System.Drawing.SystemColors.Control;
            }
        }

        private void time_TextChanged(object sender, EventArgs e)
        {
            Editor.LoadSelection(time.Text.Trim());
        }

        private void GotoTime_NonClientTest(object sender, NonClientTestEventArgs e)
        {
            // disable window resizing without losing the pretty border
            e.Region = NonClientRegions.Client;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void GotoTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((short)(e.KeyChar) == 27) || ((short)(e.KeyChar) == 13)) Close();
        }
    }
}