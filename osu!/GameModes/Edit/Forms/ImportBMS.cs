﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using osu.GameModes.Edit.Convert;
using osu_common.Helpers;

namespace osu.GameModes.Edit.Forms
{
    public partial class ImportBMS : pForm
    {
        public ImportBMS()
        {
            InitializeComponent();
        }

        protected override void OnShown(EventArgs e)
        {
            cb_encode.SelectedIndex = 0;
            base.OnShown(e);
        }

        private void b_file_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tb_file.Text = openFileDialog.FileName;
            }
        }

        private void cb_meta_CheckedChanged(object sender, EventArgs e)
        {
            cb_encode.Enabled = cb_meta.Checked;
        }

        private void b_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void b_start_Click(object sender, EventArgs e)
        {
            if (tb_file.Text == "")
            {
                MessageBox.Show("Select a bms file first!");
                return;
            }
            Editor editor = Editor.Instance;
            editor.UndoPush();
            if (cb_clear.Checked)
                editor.hitObjectManager.Clear();
            if (!cb_effect.Checked)
            {
                editor.eventManager.eventSamples.Clear();
            }
            ConverterBMS cvt = new ConverterBMS(Editor.Instance);
            cvt.CopyImage = cb_image.Checked;
            cvt.IgnoreSample = cb_sample.Checked;
            cvt.IgnoreEffect = cb_effect.Checked;
            cvt.OverrideMeta = cb_meta.Checked;
            if (cvt.ProcessFile(tb_file.Text, cb_encode.SelectedItem.ToString()))
            {
                cvt.Clear();
                Close();
                editor.hitObjectManager.Save(false, false, false);
                GameBase.ChangeMode(OsuModes.Edit, true);             
            }
            else
                Close();
        }
    }
}
