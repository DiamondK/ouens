﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using osu.Configuration;

namespace osu.GameModes.Edit.Forms
{
    public partial class ScaleBy : Form
    {
        public ScaleBy()
        {
            InitializeComponent();
        }

        protected override void OnShown(EventArgs e)
        {
            if (ConfigManager.sFastEditor)
                Editor.Instance.overrideIgnoreState = true;
            Editor.Instance.UndoPush();
            m_last_scale = 1.0f;
            SetScale(1.0f, true);
            // m_last_playfield irrelevant

            txtScale.SelectAll();

            base.OnShown(e);
        }

        private float m_last_scale;
        private bool m_last_playfield;
        private bool m_last_x, m_last_y;

        private float m_min_scale = 0.5f, m_max_scale = 2.0f;

        public float MinScale
        {
            get
            {
                return m_min_scale;
            }
            set
            {
                int i = (int)(value * 1000);

                if (i >= m_max_scale * 1000) throw new ArgumentOutOfRangeException();

                m_min_scale = (float)i * 0.001f;
                trScale.Minimum = i;
            }
        }

        public float MaxScale
        {
            get
            {
                return m_max_scale;
            }
            set
            {
                int i = (int)(value * 1000);

                if (i <= m_min_scale * 1000) throw new ArgumentOutOfRangeException();

                m_max_scale = (float)i * 0.001f;
                trScale.Maximum = i;
            }
        }

        private void UpdateScale()
        {
            float scale = trScale.Value * 0.001f;

            Editor.Instance.Compose.ScaleSelection(new Vector2(m_last_x ? 1.0f / m_last_scale : 1.0f,
                                                               m_last_y ? 1.0f / m_last_scale : 1.0f), m_last_playfield);

            m_last_scale = scale;
            m_last_playfield = radPlayfield.Checked;
            m_last_x = chkAxisX.Checked;
            m_last_y = chkAxisY.Checked;

            Editor.Instance.Compose.ScaleSelection(new Vector2(m_last_x ? scale : 1.0f, m_last_y ? scale : 1.0f), radPlayfield.Checked);
        }

        private void SetScale(float scale, bool force)
        {
            float actual = Math.Max(Math.Min(scale, m_max_scale), m_min_scale);
            int i = (int)(actual * 1000);

            if (!trScale.Focused || force) trScale.Value = i;
            if (!txtScale.Focused || force) txtScale.Text = actual.ToString("0.000");

            UpdateScale();
        }

        private void radSelection_CheckedChanged(object sender, EventArgs e)
        {
            radPlayfield.Checked = !radSelection.Checked;
            UpdateScale();
        }

        private void txtScale_TextChanged(object sender, EventArgs e)
        {
            float d;

            if (!Single.TryParse(txtScale.Text, out d)) return;

            SetScale(d, false);
        }

        private void trScale_Scroll(object sender, EventArgs e)
        {
            float d = trScale.Value * 0.001f;
            SetScale(d, false);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Editor.Instance.Undo();
            Editor.Instance.RedoClear();

            if (ConfigManager.sFastEditor)
                Editor.Instance.overrideIgnoreState = false;

            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chkAxisX_CheckedChanged(object sender, EventArgs e)
        {
            UpdateScale();
            btnOk.Enabled = chkAxisX.Checked || chkAxisY.Checked;
        }

        private void chkAxisY_CheckedChanged(object sender, EventArgs e)
        {
            UpdateScale();
            btnOk.Enabled = chkAxisX.Checked || chkAxisY.Checked;
        }

        private void ScaleBy_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Editor.Instance.overrideIgnoreState && ConfigManager.sFastEditor.Value)
            {
                Editor.Instance.overrideIgnoreState = false;
                Editor.Instance.UndoRedoClear();
            }
        }
    }
}
