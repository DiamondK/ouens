﻿using System;
using osu_common.Helpers;

namespace osu.GameModes.Edit.Forms
{
    public partial class MoveNotes : pForm
    {
        public MoveNotes()
        {
            InitializeComponent();
        }

        protected override void OnShown(EventArgs e)
        {
            amount.SelectAll();
            
            base.OnShown(e);
        }

        int lastAngle;
        bool lastPlayfield;

        private void DoOperation()
        {
            int amountInt;
            
            if (!Int32.TryParse(amount.Text,out amountInt))
                return;
        //    Editor.Instance.UndoPush(); do this in moveNotesBy
            Editor.Instance.Timing.MoveNotesBy(amountInt);
        }

        private void angle_TextChanged(object sender, EventArgs e)
        {
            int parseTest = 0;
            
            bool success = Int32.TryParse(amount.Text, out parseTest);
            btnOk.Enabled = success;

            if (success)
                trOffset.Value = Math.Max(-60000,Math.Min(60000,parseTest));
        }

        private void trOffset_Scroll(object sender, EventArgs e)
        {
            amount.Text = trOffset.Value.ToString();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DoOperation();
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
        }
    }
}