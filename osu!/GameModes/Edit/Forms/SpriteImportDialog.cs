﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameplayElements.Beatmaps;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu_common;
using osu.Audio;

namespace osu.GameModes.Edit.Forms
{
    internal class SpriteImportDialog : pDialog
    {
        private readonly pScrollableArea spriteList;

        float x = 0, y = 0;
        List<string> files;
        int count = 0;
        private bool loadingSprites = true;
        private int lastLoad;
        internal string SelectedFilename;
        internal bool IsAnimation;
        internal int AnimationFrameCount;
        public double SelectedFrameDelay;

        internal SpriteImportDialog()
            : base("Sprite Library", true)
        {
            spriteList = new pScrollableArea(new Rectangle(0, 50, 640, 360), new Vector2(640, 480), false);

            if (BeatmapManager.Current.InOszContainer)
            {
                BeatmapManager.Current.Osz2ExtractSafe();
                Folder = BeatmapManager.Current.ExtractionFolder;
            }
            else
                Folder = BeatmapManager.Current.ContainingFolder;

            files = new List<string>();

            pButton pb = new pButton("Cancel", new Vector2(40, 440), new Vector2(240, 30), 1, Color.Gray, delegate { Close(); });
            spriteManager.Add(pb.SpriteCollection);

            pb = new pButton("Browse...", new Vector2(360, 440), new Vector2(240, 30), 1, Color.Orange, delegate { BrowseForFile(); });
            spriteManager.Add(pb.SpriteCollection);

            string[] pngFiles = Directory.GetFiles(Folder, "*.png",
                                                   SearchOption.AllDirectories);
            files.AddRange(pngFiles);
            string[] jpgFiles = Directory.GetFiles(Folder, "*.jpg",
                                                   SearchOption.AllDirectories);
            files.AddRange(jpgFiles);
        }

        string Folder;

        private void BrowseForFile()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Images (jpg/png)|*.jpg;*.png";
            ofd.InitialDirectory = Path.GetFullPath(Folder);
            ofd.RestoreDirectory = true;

            IsAnimation = false;

            if (DialogResult.OK == ofd.ShowDialog(GameBase.Form))
            {
                string filename = ofd.FileName;

                if (filename.StartsWith(Folder))
                    return;

                string destination = Path.Combine(Folder, Path.GetFileName(filename));

                if (filename.Contains("0."))
                {
                    //possible animation
                    AnimationFrameCount = 0;
                    while (true)
                    {
                        string testFile = filename.Replace("0.", AnimationFrameCount + ".");

                        if (!File.Exists(testFile))
                            break;
                        try
                        {
                            File.Delete(Path.Combine(Folder, Path.GetFileName(testFile)));
                            File.Copy(testFile, Path.Combine(Folder, Path.GetFileName(testFile)));
                        }
                        catch (Exception)
                        {
                        }

                        if (AnimationFrameCount > 0) IsAnimation = true;

                        AnimationFrameCount++;
                    }
                }
                else
                {
                    try
                    {
                        File.Delete(destination);
                        File.Copy(filename, destination);
                    }
                    catch
                    {
                    }
                }


                SelectedFilename = destination;
                GameBase.Scheduler.Add(delegate { Close(); });
            }
        }

        internal override void Draw()
        {
            base.Draw();

            spriteList.Draw();
        }

        public override void Update()
        {
            if (loadingSprites && GameBase.Time - lastLoad > 40)
            {
                if (files.Count > 0)
                {
                    x = 80 + 100 * (count % 6);
                    y = 50 + (count / 6) * 100;

                    count++;
                    string filename = files[0].Replace(Folder + Path.DirectorySeparatorChar, string.Empty);

                    pSprite p = null;
                    int frameDelay = 0;

                    if (files.Count > 1 && filename[filename.Length - 5] == '0')
                    {
                        List<pTexture> textures = new List<pTexture>();

                        int j = 0;
                        while (true)
                        {
                            string testFilename = filename.Replace("0.", j++ + ".");
                            pTexture test = SkinManager.Load(testFilename, SkinSource.Beatmap);
                            if (test == null) break;
                            files.Remove(testFilename);
                            textures.Add(test);
                        }

                        if (textures.Count > 1)
                        {

                            pAnimation a =
                                new
                                    pAnimation(textures.ToArray(), Fields.StandardGamefieldScale, Origins.Centre,
                                               Clocks.Game, new Vector2(x, y),
                                               0.98f, true, Color.White, null);

                            a.Scale = 98f / Math.Max(a.Width, a.Height);
                            spriteList.SpriteManager.Add(a);

                            p = a;

                            filename = filename.Replace("0.", ".");

                            frameDelay = (int)(AudioEngine.ActiveControlPoint != null ? (AudioEngine.ActiveControlPoint.beatLength * 2) / textures.Count : 500);
                            a.FrameDelay = frameDelay;
                        }
                    }

                    if (p == null)
                    {
                        p = new pSprite(SkinManager.Load(filename, SkinSource.Beatmap), Fields.StandardGamefieldScale,
                                                Origins.Centre, Clocks.Game,
                                                new Vector2(x, y), 0.98f, true, Color.White);
                        p.Scale = 98f / Math.Max(p.Width, p.Height);
                        spriteList.SpriteManager.Add(p);
                    }

                    pSprite p2 = new pSprite(GameBase.WhitePixel, Fields.StandardGamefieldScale, Origins.TopLeft,
                                             Clocks.Game,
                                             new Vector2(x - 39, y - 39), 0.9f, true, new Color(15, 15, 15, 255));
                    p2.HoverEffect = new Transformation(p2.InitialColour, new Color(50, 50, 50, 255), 0, 100);
                    p2.Tag = p;
                    p2.OnClick += delegate
                    {
                        IsAnimation = p is pAnimation;
                        SelectedFilename = filename;

                        if (IsAnimation)
                        {
                            SelectedFrameDelay = frameDelay;
                            AnimationFrameCount = ((pAnimation)p).TextureCount;
                        }
                        Close();
                    };

                    p2.HandleInput = true;
                    p2.VectorScale = new Vector2(100, 100);
                    spriteList.SpriteManager.Add(p2);

                    pText pt = new pText(filename, 8, new Vector2(x - 39, y - 50), new Vector2(100, 100), 1, true, Color.White, true);
                    pt.TextRenderSpecific = false;
                    pt.TextAa = false;
                    pt.TextShadow = true;
                    spriteList.SpriteManager.Add(pt);

                    files.RemoveAt(0);

                }

                spriteList.SetContentDimensions(new Vector2(x + 100, y + 100));

                lastLoad = GameBase.Time;

                if (files.Count == 0) loadingSprites = false;
            }

            spriteList.Update();

            base.Update();
        }

        protected override void Dispose(bool disposing)
        {
            spriteList.Dispose();
            base.Dispose(disposing);
        }
    }
}
