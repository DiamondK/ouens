﻿using System;
using System.IO;
using System.Windows.Forms;
using osu.Configuration;
using osu.GameplayElements.Beatmaps;
using osu_common.Helpers;
using osu.Online;
using System.Collections.Generic;
using osu_common;

namespace osu.GameModes.Edit.Forms
{
    internal partial class OpenDialog : pForm
    {
        private pList<OpenDialogItem> sortedItems;
        private bool reference = false;

        internal OpenDialog(bool reference)
        {
            InitializeComponent();
            this.reference = reference;
            if (reference)
                checkBoxAutosave.Visible = false;

            TopLevel = true;

            sortedItems = new pList<OpenDialogItem>();
            List<String> files = new List<string> (BeatmapManager.Current.InOszContainer ? 
                                BeatmapManager.Current.Package.MapFiles : 
                                Directory.GetFiles(BeatmapManager.Current.ContainingFolder, "*.osu"));

            if (BeatmapManager.Current.ExtractionFolder != null)
            {
                string[] fileOverrides = Directory.GetFiles(BeatmapManager.Current.ExtractionFolder, "*.osu");
                foreach(string file in fileOverrides)
                    if (!files.Exists( (f) => f.EndsWith(Path.GetFileName(file))))
                        files.Add(file);
            }

            if (BeatmapManager.Current.ExtractionFolder != null)
                BeatmapManager.ProcessFolder(BeatmapManager.Current.ExtractionFolder);

            foreach (string f in files)
            {
                OpenDialogItem di = new OpenDialogItem();
                Beatmap b = BeatmapManager.GetBeatmapByFilename(Path.GetFileName(f));
                if (b == null)
                {
                    di.displayString = "?NEW? " + Path.GetFileName(f);
                    di.filename = Path.GetFileName(f);
                    di.sort = 10;
                }
                else
                {
                    di.beatmap = b;
                    di.filename = b.Filename;
                    di.displayString = string.Format("{0}", b.Version);
                    di.sort = b.StarDisplayEyup;
                }

                sortedItems.AddInPlace(di);
            }

            foreach (OpenDialogItem i in sortedItems)
                listBox1.Items.Add(i);

            if (reference)
            {
                OpenDialogItem di = new OpenDialogItem();
                di.displayString = @"None";
                sortedItems.Insert(0, di);
                listBox1.Items.Insert(0, di);

                if (Editor.Instance.Compose.ReferenceHitObjectManager == null)
                    listBox1.SelectedIndex = 0;
                else
                    SetTo(Editor.Instance.Compose.ReferenceHitObjectManager.Beatmap);
            }
            else
                SetTo(BeatmapManager.Current);
        }

        private void SetTo(Beatmap beatmap)
        {
            int select = sortedItems.FindIndex(p => p.filename == beatmap.Filename);
            if (select >= 0)
                listBox1.SelectedIndex = select;
        }

        internal string SelectedFilename
        {
            get { return ((OpenDialogItem)listBox1.SelectedItem).filename; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Editor.Instance.Compose.ChangeAltDistance((float)ConfigManager.sDistanceSpacing);
            Editor.Instance.Compose.ChangeSnapDivisor(ConfigManager.sEditorBeatDivisor);
            Editor.Instance.ChangeGridSize(ConfigManager.sEditorGridSize,false);
            BanchoClient.UpdateStatus();
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            //button1_Click(null, null);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string oldFilename = BeatmapManager.Current.Filename;
            string newFilename = Path.GetFileName(SelectedFilename);

            if (oldFilename == newFilename)
                return;

            if (reference)
            {
                if (newFilename == null)
                {
                    Editor.Instance.LoadReferenceFile(null);
                    return;
                }

                Beatmap newMap = BeatmapManager.GetBeatmapByFilename(newFilename);
                if (newMap == null || newMap == BeatmapManager.Current || newMap.PlayMode != PlayModes.OsuMania)
                    return;
                if (!Editor.Instance.LoadReferenceFile(newMap))
                {
                    MessageBox.Show("Cannot load this file!");
                }
                return;
            }

            if (checkBoxAutosave.Checked)
            {
                Editor.Instance.SaveIfDirty(false);
            }
            else if (!Editor.Instance.SaveIfDirty(true))
            {
                SetTo(BeatmapManager.Current);
                return;
            }

            if (!BeatmapManager.SetCurrentFromFilename(newFilename))
            {
                BeatmapManager.ProcessBeatmaps();
                BeatmapManager.SetCurrentFromFilename(newFilename);
            }

            Beatmap b = BeatmapManager.Current;
            if (b != null)
            {
                if (b.Filename != oldFilename)
                {
                    Editor.Instance.LoadFile(b, false, false);
                    Editor.Instance.HistoryClear();
                }
            }
            else
            {
                BeatmapManager.SetCurrentFromFilename(oldFilename);
                MessageBox.Show("Cannot load this file!");
            }
        }

        private void OpenDialog_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }
    }

    internal class OpenDialogItem : IComparable<OpenDialogItem>
    {
        internal OpenDialogItem()
        {

        }

        internal Beatmap beatmap;
        internal string filename;
        internal string displayString;
        internal double sort;

        public override string ToString()
        {
            return displayString;
        }

        #region IComparable<OpenDialogItem> Members

        public int CompareTo(OpenDialogItem other)
        {
            return this.sort.CompareTo(other.sort);
        }

        #endregion
    }
}