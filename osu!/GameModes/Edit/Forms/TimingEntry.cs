﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using osu.Audio;
using osu.GameModes.Edit.Modes;
using osu_common.Helpers;
using osu.Properties;
using osu.GameplayElements.HitObjects;
using osu.Helpers;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu_common;

namespace osu.GameModes.Edit.Forms
{
    internal partial class TimingEntry : pForm
    {
        private readonly Editor editor;
        private readonly TimingFocus timingFocus;

        private List<ControlPoint> controlPoints;
        private List<ControlPoint> hiddenPoints;
        private List<ControlPoint> selectedPoints;
        private ControlPoint tp;
        private ControlPoint tp_timing;
        private ControlPoint tp_inherit;

        private const double SLIDER_75 = -133.333333333333d;
        private const double SLIDER_150 = -66.6666666666667d;

        private const int METRONOME_MIN = 5;
        private const int METRONOME_MAX = 7;

        private const decimal SLIDER_MIN = 0.5m;
        private const decimal SLIDER_MAX = 2.0m;

        private const decimal SLIDER_MIN_TAIKO = 0.1m;
        private const decimal SLIDER_MAX_TAIKO = 4.0m;

        private const decimal SLIDER_MIN_MANIA = 0.1m;
        private const decimal SLIDER_MAX_MANIA = 10.0m;

        private const int COLUMN_COUNT = 6;
        private const int ROW_COUNT = 11;

        /// <summary>
        /// Stops winforms events from firing when changing the selected point
        /// </summary>
        private bool suppressEvents = false;

        /// <summary>
        /// Becomes true once the user has changed the selected point
        /// </summary>
        private bool selectionChanged = false;

        /// <summary>
        /// Determines whether changes are kept upon form close.
        /// </summary>
        private bool updateChanges = false;

        internal TimingEntry(Editor editor, TimingFocus tf)
            : this(editor, tf, true, false)
        {
        }

        internal TimingEntry(Editor editor, TimingFocus tf, bool AllowInherit)
            : this(editor, tf, AllowInherit, false)
        {
        }

        internal TimingEntry(Editor editor, TimingFocus tf, bool AllowInherit, bool timing_tab)
        {
            this.editor = editor;
            timingFocus = tf;

            InitializeComponent();
            imageList1.ColorDepth = Application.RenderWithVisualStyles ? ColorDepth.Depth32Bit : ColorDepth.Depth24Bit;
            imageList1.Images.Clear();
            imageList1.Images.Add(Resources.redbullet);
            imageList1.Images.Add(Resources.greenbullet);

            if (Application.RenderWithVisualStyles) // don't make the left pane completely white in Classic
            {
                foreach (TabPage tab in tabsLeft.TabPages)
                {
                    tab.BackColor = System.Drawing.SystemColors.ControlLightLight;

                    // trackbars are retarded and don't support transparent backgrounds
                    foreach (Control c in tab.Controls) if (c is TrackBar)
                    {
                        TrackBar t = (TrackBar)c;
                        t.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    }
                }
            }

            AeroGlassHelper.SetWindowTheme(listTimingPoints, "explorer");

            tabsLeft.TabPages.Remove(tabGameplay);
            tabsLeft.TabPages.Remove(tabBulk);
            tabsLeft.SelectedIndex = tf == TimingFocus.Audio ? 1 : 0;

            controlPoints = new List<ControlPoint>();
            hiddenPoints = new List<ControlPoint>();

            foreach (ControlPoint t in AudioEngine.ControlPoints)
                controlPoints.Add((ControlPoint) t.Clone());

            switch (editor.beatSnapDivisor)
            {
                case 1:
                    udBeatSnap.SelectedIndex = 0;
                    break;
                case 2:
                    udBeatSnap.SelectedIndex = 1;
                    break;
                case 3:
                    udBeatSnap.SelectedIndex = 2;
                    break;
                case 4:
                    udBeatSnap.SelectedIndex = 3;
                    break;
                case 6:
                    udBeatSnap.SelectedIndex = 4;
                    break;
                case 8:
                    udBeatSnap.SelectedIndex = 5;
                    break;
            }

            SuspendDrawing(listTimingPoints);
            UpdateAll(false);

            if (timing_tab)
                starting_index = AllowInherit ? AudioEngine.ActiveInheritedTimingPointIndex : AudioEngine.ActiveTimingPointIndex;
            else
                starting_index = AllowInherit ? AudioEngine.controlPointIndexAt(AudioEngine.Time + 50, true) : AudioEngine.controlPointIndexAt(AudioEngine.Time + 50, false);

        }

        private int starting_index = -1;

        private void TimingEntry_Load(object sender, EventArgs e)
        {
            suppressEvents = true;
            if (starting_index >= 0) listTimingPoints.SelectedIndices.Add(starting_index);
            GetSelectedPoints();
            suppressEvents = false;
            ResumeDrawing(listTimingPoints);

            UpdateLeft(false);

            if (listTimingPoints.Items.Count > 0)
                ScrollToItem(listTimingPoints.SelectedIndices[0]);

            //listTimingPoints.Focus();

            if (tabsLeft.TabPages.Contains(tabTiming))
            {
                switch (timingFocus)
                {
                    case TimingFocus.None:
                        ActiveControl = listTimingPoints;
                        return;
                    case TimingFocus.BPM:
                        ActiveControl = udBpm;
                        udBpm.Select(0, udBpm.Text.Length);
                        return;
                    case TimingFocus.Offset:
                        ActiveControl = udTimingOffset;
                        udTimingOffset.Select(0, udTimingOffset.Text.Length);
                        return;
                    case TimingFocus.Audio:
                        tabsLeft.SelectedIndex = 1;
                        ActiveControl = listSampleset;
                        return;
                }
            }
            else if (tabsLeft.TabPages.Contains(tabGameplay))
            {
                switch (timingFocus)
                {
                    case TimingFocus.None:
                        ActiveControl = listTimingPoints;
                        return;
                    case TimingFocus.BPM:
                    case TimingFocus.Offset:
                        ActiveControl = udGameplayOffset;
                        udGameplayOffset.Select(0, udGameplayOffset.Text.Length);
                        return;
                    case TimingFocus.Audio:
                        tabsLeft.SelectedIndex = 1;
                        ActiveControl = listSampleset;
                        return;
                }
            }
            else if (tabsLeft.TabPages.Contains(tabBulk))
            {
                switch (timingFocus)
                {
                    case TimingFocus.None:
                        ActiveControl = listTimingPoints;
                        return;
                    case TimingFocus.BPM:
                    case TimingFocus.Offset:
                        ActiveControl = udBulkMove;
                        udBulkMove.Select(0, udBulkMove.Text.Length);
                        return;
                    case TimingFocus.Audio:
                        tabsLeft.SelectedIndex = 1;
                        ActiveControl = listSampleset;
                        return;
                }
            }
        }

        private void UpdateSelected()
        {
            suppressEvents = true;

            if (listTimingPoints.SelectedIndices.Count == 0) return;
            System.Drawing.Color EvenColour = ColourHelper.ColourLerp(System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.Control),
                                                          System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.Window), 0.5f);

            foreach (int x in listTimingPoints.SelectedIndices)
            {
                ListViewItem newItem = CreateItem(controlPoints[x]);
                if (x % 2 == 0) newItem.BackColor = EvenColour;

                for (int y = 0; y < COLUMN_COUNT; y++)
                {
                    listTimingPoints.Items[x].SubItems[y].Text = newItem.SubItems[y].Text;
                    listTimingPoints.Items[x].SubItems[y].ForeColor = newItem.SubItems[y].ForeColor;
                }
                listTimingPoints.Items[x].ImageIndex = newItem.ImageIndex;
            }

            suppressEvents = false;
        }

        private void UpdateAll(bool keepSelected)
        {
            suppressEvents = true;

            int top_index = listTimingPoints.Items.IndexOf(listTimingPoints.TopItem);

            listTimingPoints.Items.Clear();

            if (selectedPoints == null) keepSelected = false;
            int lastindex = 0;
            bool even = false;
            System.Drawing.Color EvenColour = ColourHelper.ColourLerp(System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.Control),
                                                                      System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.Window), 0.5f);

            foreach (ControlPoint p in controlPoints)
            {
                ListViewItem item = CreateItem(p);
                if (even) item.BackColor = EvenColour;
                even ^= true;

                if (keepSelected)
                {
                    int newindex = selectedPoints.IndexOf(p, lastindex);
                    if (newindex >= 0)
                    {
                        item.Selected = true;
                        lastindex = newindex;
                    }
                }

                listTimingPoints.Items.Add(item);
            }

            ScrollToItem(top_index);

            suppressEvents = false;
        }

        private void FixAlternatingColours()
        {
            System.Drawing.Color EvenColour = ColourHelper.ColourLerp(System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.Control),
                                                                      System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.Window), 0.5f);
            System.Drawing.Color OddColour = System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.Window);
            bool even = false;

            foreach (ListViewItem item in listTimingPoints.Items)
            {
                item.BackColor = even ? EvenColour : OddColour;
                even ^= true;
            }
        }

        private void Sort()
        {
            int top_index = listTimingPoints.Items.IndexOf(listTimingPoints.TopItem);

            controlPoints.Sort();
            selectedPoints.Sort();

            SuspendDrawing(listTimingPoints);
            UpdateAll(true);

            ScrollToIndex(top_index);
            ResumeDrawing(listTimingPoints);

            suppressEvents = false;
        }

        private void UpdateLeft(bool allowSuspend = true)
        {
            dirty = false;
            if (tp == null)
            {
                tabsLeft.Enabled = false;
                chkInherit.Enabled = false;
                toolRemove.Enabled = false;

                return;
            }

            tabsLeft.Enabled = true;
            chkInherit.Enabled = true;
            toolRemove.Enabled = true;

            if (allowSuspend) SuspendDrawing(tabsLeft);


            suppressEvents = true;
            bool multi = false, timing_change = false;

            // pick which left tab to offer
            if (multi = selectedPoints.Count > 1)
            {
                bool timingActive = tabsLeft.SelectedIndex == 0;
                if (!tabsLeft.TabPages.Contains(tabBulk)) tabsLeft.TabPages.Insert(0, tabBulk);
                if (timingActive) tabsLeft.SelectedIndex = 0;
                tabsLeft.TabPages.Remove(tabTiming);
                tabsLeft.TabPages.Remove(tabGameplay);
            }
            else if (timing_change = tp.timingChange)
            {
                bool timingActive = tabsLeft.SelectedIndex == 0;
                if (!tabsLeft.TabPages.Contains(tabTiming)) tabsLeft.TabPages.Insert(0, tabTiming);
                if (timingActive) tabsLeft.SelectedIndex = 0;
                tabsLeft.TabPages.Remove(tabGameplay);
                tabsLeft.TabPages.Remove(tabBulk);
            }
            else
            {
                bool timingActive = tabsLeft.SelectedIndex == 0;
                if (!tabsLeft.TabPages.Contains(tabGameplay)) tabsLeft.TabPages.Insert(0, tabGameplay);
                if (timingActive) tabsLeft.SelectedIndex = 0;
                tabsLeft.TabPages.Remove(tabTiming);
                tabsLeft.TabPages.Remove(tabBulk);
            }

            if (multi)
            {
                // populate bulk timing tab
                udBulkMove.Value = 0;

                bool timing = tp_timing != null;
                bool inherit = tp_inherit != null;

                grpBulkTimeSignature.Enabled = timing;
                radBulkTimingTriple.Enabled = timing;
                radBulkTimingQuad.Enabled = timing;
                radBulkCustomSignature.Enabled = timing;
                udBulkMetronome.Enabled = timing;

                grpBulkDifficulty.Enabled = inherit;
                radBulkSlider75.Enabled = inherit;
                radBulkSlider100.Enabled = inherit;
                radBulkSlider150.Enabled = inherit;
                radBulkSliderCustom.Enabled = inherit;
                udBulkSliderMultiplier.Enabled = inherit;

                if (timing)
                {
                    if (selectedPoints.TrueForAll(p => !p.timingChange || p.timeSignature == tp_timing.timeSignature))
                        setTimeSignature(tp_timing.timeSignature, radBulkTimingTriple, radBulkTimingQuad,
                                         radBulkCustomSignature, udBulkMetronome);
                    else
                        clearTimeSignature(radBulkTimingTriple, radBulkTimingQuad,
                                           radBulkCustomSignature, udBulkMetronome);
                }

                if (inherit)
                {
                    if (selectedPoints.TrueForAll(p => p.timingChange || p.beatLength == tp_inherit.beatLength))
                        setSliderVelocity(tp_inherit.beatLength, radBulkSlider75, radBulkSlider100,
                                      radBulkSlider150, radBulkSliderCustom, udBulkSliderMultiplier);
                    else
                        clearSliderVelocity(radBulkSlider75, radBulkSlider100,
                                            radBulkSlider150, radBulkSliderCustom, udBulkSliderMultiplier);
                }
            }
            else if (timing_change)
            {
                // populate uninherit timing tab
                udTimingOffset.Value = NanClamp(tp.offset, udTimingOffset.Minimum, udTimingOffset.Maximum);
                udBpm.Value = NanClamp(tp.bpm, udBpm.Minimum, udBpm.Maximum);

                setTimeSignature(tp.timeSignature, radTimingTriple, radTimingQuad,
                                 radCustomSignature, udMetronome);
            }
            else
            {
                // populate inherit timing tab
                udGameplayOffset.Value = NanClamp(tp.offset, udGameplayOffset.Minimum, udGameplayOffset.Maximum);

                setSliderVelocity(tp.beatLength, radSlider75, radSlider100,
                                  radSlider150, radSliderCustom, udSliderMultiplier);
            }

            // populate audio tab
            if (selectedPoints.TrueForAll(p => p.sampleSet == tp.sampleSet))
                try
                {
                    listSampleset.SelectedItem = tp.sampleSet.ToString();
                }
                catch (Exception)
                {
                    listSampleset.SelectedItems.Clear();
                }
            else
                listSampleset.SelectedItems.Clear();

            if (selectedPoints.TrueForAll(p => p.customSamples == tp.customSamples))
            {
                radSamplesetDefault.Checked = tp.customSamples == CustomSampleSet.Default;
                radSamplesetCustom1.Checked = tp.customSamples == CustomSampleSet.Custom1;
                nudCustom.Enabled = radSamplesetCustom2.Checked = (int)tp.customSamples >= 2;
                nudCustom.Value = Math.Max(2, Math.Min((int)tp.customSamples,100));
            }
            else
            {
                radSamplesetDefault.Checked = false;
                radSamplesetCustom1.Checked = false;
                nudCustom.Enabled = radSamplesetCustom2.Checked = false;
            }

            tbVolume.Value = Math.Min(100,Math.Max(5,tp.volume));
            udVolume.Value = tp.volume;

            // populate effects tab
            if (selectedPoints.TrueForAll(p => p.kiaiMode == tp.kiaiMode))
            {
                chkKiai.CheckState = tp.kiaiMode ? CheckState.Checked : CheckState.Unchecked;
                chkKiai.ThreeState = false;
            }
            else
                chkKiai.CheckState = CheckState.Indeterminate;

            // disable kiai checkbox if only the first control point is selected.
            chkKiai.Enabled = (chkKiai.CheckState != CheckState.Unchecked) || // allow it to be UNchecked in case of .osuhax
                              ((selectedPoints.Count == 1) && !first(selectedPoints[0])) ||
                              (selectedPoints.Count > 1);
            // only allow omitting the barline on non-inheriting points
            chkOmitBarline.Enabled = selectedPoints.Exists(p => p.timingChange);

            if (selectedPoints.TrueForAll(p => (p.effectFlags & EffectFlags.OmitFirstBarLine) == (tp.effectFlags & EffectFlags.OmitFirstBarLine)))
            {
                chkOmitBarline.CheckState = (tp.effectFlags & EffectFlags.OmitFirstBarLine) > 0 ? CheckState.Checked : CheckState.Unchecked;
                chkOmitBarline.ThreeState = false;
            }
            else
                chkOmitBarline.CheckState = CheckState.Indeterminate;

            if (selectedPoints.TrueForAll(p => p.timingChange == tp.timingChange))
            {
                chkInherit.CheckState = tp.timingChange ? CheckState.Unchecked : CheckState.Checked;
                chkInherit.ThreeState = false;
            }
            else
            {
                chkInherit.CheckState = CheckState.Indeterminate;
            }

            suppressEvents = false;
            if (allowSuspend) ResumeDrawing(tabsLeft);
        }

        private decimal NanClamp(double value, decimal min, decimal max)
        {
            if (Double.IsNaN(value)) return min;
            if (Double.IsNegativeInfinity(value)) return min;
            if (Double.IsPositiveInfinity(value)) return max;
            return (decimal)Math.Min((double)max, Math.Max((double)min, value));
        }

        private void ScrollToItem(int index)
        {
            int count = listTimingPoints.Items.Count;
            if (count == 0) return;
            if (index < 0) index = 0;
            if (index >= count) index = count - 1;

            try
            {
                listTimingPoints.EnsureVisible(index);
            }
            catch (ArgumentOutOfRangeException) { }
        }

        private void ScrollToIndex(int index)
        {
            // do not touch TopItem no matter what; it's horribly broken.
            int count = listTimingPoints.Items.Count;
            if (count == 0) return;
            if (index < 0) index = 0;
            if (index >= count) index = count - 1;

            try
            {
                listTimingPoints.EnsureVisible(count - 1);
                listTimingPoints.EnsureVisible(index);
            }
            catch (ArgumentOutOfRangeException) { }
        }

        private void setSliderVelocity(double bpm, RadioButton rad75, RadioButton rad100, RadioButton rad150, RadioButton radCustom, NumericUpDown ud)
        {
            rad75.Checked = bpm == SLIDER_75;
            rad100.Checked = bpm == -100.0d;
            rad150.Checked = bpm == SLIDER_150;

            radCustom.Checked = bpm != SLIDER_75 && bpm != -100.0d && bpm != SLIDER_150;
            ud.Enabled = radCustom.Checked;

            decimal vel = (decimal)(-100.0d / bpm);
            switch (BeatmapManager.Current.PlayMode)
            {
                case PlayModes.OsuMania:
                    ud.Minimum = Math.Min(SLIDER_MIN_MANIA, vel);
                    ud.Maximum = Math.Max(SLIDER_MAX_MANIA, vel);
                    break;
                case PlayModes.Taiko:
                    ud.Minimum = Math.Min(SLIDER_MIN_TAIKO, vel);
                    ud.Maximum = Math.Max(SLIDER_MAX_TAIKO, vel);
                    break;
                case PlayModes.Osu:
                default:
                    ud.Minimum = Math.Min(SLIDER_MIN, vel);
                    ud.Maximum = Math.Max(SLIDER_MAX, vel);
                    break;
            }
            ud.Value = vel;
        }

        private void clearSliderVelocity(RadioButton rad75, RadioButton rad100, RadioButton rad150, RadioButton radCustom, NumericUpDown ud)
        {
            rad75.Checked = false;
            rad100.Checked = false;
            rad150.Checked = false;
            radCustom.Checked = false;
            ud.Enabled = false;
            switch (BeatmapManager.Current.PlayMode)
            {
                case PlayModes.OsuMania:
                    ud.Minimum = SLIDER_MIN_MANIA;
                    ud.Maximum = SLIDER_MAX_MANIA;
                    break;
                case PlayModes.Taiko:
                    ud.Minimum = SLIDER_MIN_TAIKO;
                    ud.Maximum = SLIDER_MAX_TAIKO;
                    break;
                case PlayModes.Osu:
                default:
                    ud.Minimum = SLIDER_MIN;
                    ud.Maximum = SLIDER_MAX;
                    break;
            }
            ud.Value = 1.0m;
        }

        private void setTimeSignature(TimeSignatures sig, RadioButton radTriple, RadioButton radQuad, RadioButton radCustom, NumericUpDown ud)
        {
            int sig2 = METRONOME_MIN;
            switch (sig)
            {
                case TimeSignatures.SimpleTriple:
                    radTriple.Checked = true;
                    radQuad.Checked = false;
                    radCustom.Checked = false;
                    ud.Enabled = false;
                    break;
                case TimeSignatures.SimpleQuadruple:
                    radQuad.Checked = true;
                    radTriple.Checked = false;
                    radCustom.Checked = false;
                    ud.Enabled = false;
                    break;
                default:
                    sig2 = (int)sig;
                    radCustom.Checked = true;
                    ud.Enabled = true;
                    radTriple.Checked = false;
                    radQuad.Checked = false;
                    break;
            }
            ud.Minimum = Math.Min(METRONOME_MIN, sig2);
            ud.Maximum = Math.Max(METRONOME_MAX, sig2);
            ud.Value = sig2;
        }

        private void clearTimeSignature(RadioButton radTriple, RadioButton radQuad, RadioButton radCustom, NumericUpDown ud)
        {
            radTriple.Checked = false;
            radQuad.Checked = false;
            radCustom.Checked = false;
            ud.Enabled = false;
            ud.Minimum = METRONOME_MIN;
            ud.Maximum = METRONOME_MAX;
            ud.Value = METRONOME_MIN;
        }

        private ListViewItem CreateItem(ControlPoint tp)
        {
            if (tp.timingChange)
            {
                return new ListViewItem(new[]{
                    String.Format("{0:00}:{1:00}:{2:000}", ((int) tp.offset/60000), (int) tp.offset%60000/1000, (int) tp.offset%1000),
                    tp.bpm.ToString("0.000"),
                    ((int)tp.timeSignature).ToString() + "/4",
                    (SamplesetLetter(tp)) + (tp.customSamples > CustomSampleSet.Default ? (":C" + ((int)tp.customSamples).ToString()) : ""),
                    tp.volume + "%", tp.kiaiMode ? "\u2606" : ""}, 0);
            }
            else
            {
                return new ListViewItem(new[]{
                    String.Format("{0:00}:{1:00}:{2:000}", ((int) tp.offset/60000), (int) tp.offset%60000/1000, (int) tp.offset%1000),
                    "x" + (-100.0d / tp.beatLength).ToString("0.00"),
                    "",
                    (SamplesetLetter(tp)) + (tp.customSamples > CustomSampleSet.Default ? (":C" + ((int)tp.customSamples).ToString()) : ""),
                    tp.volume + "%", tp.kiaiMode ? "\u2606" : ""}, 1);
            }
        }

        private string SamplesetLetter(ControlPoint tp)
        {
            switch (tp.sampleSet)
            {
                case SampleSet.Normal:
                    return "N";
                case SampleSet.Soft:
                    return "S";
                case SampleSet.Drum:
                    return "D";
                default:
                    return "?";
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            updateChanges = true;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            updateChanges = false;
            Close();
        }

        private void listTimingPoints_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
        }

        private void GetSelectedPoints()
        {
            selectedPoints = new List<ControlPoint>();
            tp_timing = null;
            tp_inherit = null;

            if (listTimingPoints.SelectedIndices.Count == 0)
            {
                tp = null;
                return;
            }

            foreach (int x in listTimingPoints.SelectedIndices)
            {
                selectedPoints.Add(controlPoints[x]);

                if (tp_timing != null && tp_inherit != null) continue;
                if (controlPoints[x].timingChange)
                {
                    if (tp_timing == null) tp_timing = controlPoints[x];
                }
                else
                {
                    if (tp_inherit == null) tp_inherit = controlPoints[x];
                }
            }

            tp = selectedPoints[0];
        }

        private void radTimingTriple_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radTimingTriple.Checked)
            {
                radTimingQuad.Checked = false;
                radCustomSignature.Checked = false;
                udMetronome.Enabled = false;
                udMetronome.Value = METRONOME_MIN;

                tp.timeSignature = TimeSignatures.SimpleTriple;

                UpdateSelected();
            }
        }

        private void radTimingQuad_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radTimingQuad.Checked)
            {
                radTimingTriple.Checked = false;
                radCustomSignature.Checked = false;
                udMetronome.Enabled = false;
                udMetronome.Value = METRONOME_MIN;

                tp.timeSignature = TimeSignatures.SimpleQuadruple;

                UpdateSelected();
            }
        }

        private void radCustomSignature_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radCustomSignature.Checked)
            {
                radTimingTriple.Checked = false;
                radTimingQuad.Checked = false;
                udMetronome.Enabled = true;

                tp.timeSignature = (TimeSignatures)udMetronome.Value;

                UpdateSelected();
            }
        }

        private void udMetronome_ValueChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            tp.timeSignature = (TimeSignatures)udMetronome.Value;

            UpdateSelected();
        }

        private void radSlider75_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radSlider75.Checked)
            {
                radSlider100.Checked = false;
                radSlider150.Checked = false;
                radSliderCustom.Checked = false;
                udSliderMultiplier.Enabled = false;
                suppressEvents = true;
                udSliderMultiplier.Value = (decimal)(-100.0d / SLIDER_75);
                suppressEvents = false;

                tp.beatLength = SLIDER_75;

                UpdateSelected();
            }
        }

        private void radSlider100_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radSlider100.Checked)
            {
                radSlider75.Checked = false;
                radSlider150.Checked = false;
                radSliderCustom.Checked = false;
                udSliderMultiplier.Enabled = false;
                suppressEvents = true;
                udSliderMultiplier.Value = 1.0m;
                suppressEvents = false;

                tp.beatLength = -100.0d;

                UpdateSelected();
            }
        }

        private void radSlider150_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radSlider150.Checked)
            {
                radSlider75.Checked = false;
                radSlider100.Checked = false;
                radSliderCustom.Checked = false;
                udSliderMultiplier.Enabled = false;
                suppressEvents = true;
                udSliderMultiplier.Value = (decimal)(-100.0d / SLIDER_150);
                suppressEvents = false;

                tp.beatLength = SLIDER_150;

                UpdateSelected();
            }
        }

        private void radSliderCustom_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radSliderCustom.Checked)
            {
                radSlider75.Checked = false;
                radSlider100.Checked = false;
                radSlider150.Checked = false;
                udSliderMultiplier.Enabled = true;

                UpdateSelected();
            }
        }

        private void udSliderMultiplier_ValueChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            tp.beatLength = (double)(-100.0m / udSliderMultiplier.Value);

            UpdateSelected();
        }

        private void radBulkTimingTriple_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radBulkTimingTriple.Checked)
            {
                radBulkTimingQuad.Checked = false;
                radBulkCustomSignature.Checked = false;
                udBulkMetronome.Enabled = false;
                suppressEvents = true;
                udBulkMetronome.Value = METRONOME_MIN;
                suppressEvents = false;

                foreach (ControlPoint p in selectedPoints)
                {
                    p.timeSignature = TimeSignatures.SimpleTriple;
                }

                UpdateSelected();
            }
        }

        private void radBulkTimingQuad_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radBulkTimingQuad.Checked)
            {
                radBulkTimingTriple.Checked = false;
                radBulkCustomSignature.Checked = false;
                udBulkMetronome.Enabled = false;
                suppressEvents = true;
                udBulkMetronome.Value = METRONOME_MIN;
                suppressEvents = false;

                foreach (ControlPoint p in selectedPoints)
                {
                    p.timeSignature = TimeSignatures.SimpleQuadruple;
                }

                UpdateSelected();
            }
        }

        private void radBulkCustomSignature_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radBulkCustomSignature.Checked)
            {
                radBulkTimingTriple.Checked = false;
                radBulkTimingQuad.Checked = false;
                udBulkMetronome.Enabled = true;

                UpdateSelected();
            }
        }

        private void udBulkMetronome_ValueChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            foreach (ControlPoint p in selectedPoints)
            {
                p.timeSignature = (TimeSignatures)udBulkMetronome.Value;
            }

            UpdateSelected();
        }

        private void radBulkSlider75_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radBulkSlider75.Checked)
            {
                radBulkSlider100.Checked = false;
                radBulkSlider150.Checked = false;
                radBulkSliderCustom.Checked = false;
                udBulkSliderMultiplier.Enabled = false;
                suppressEvents = true;
                udBulkSliderMultiplier.Value = (decimal)(-100.0d / SLIDER_75);
                suppressEvents = false;

                foreach (ControlPoint p in selectedPoints)
                {
                    if (!p.timingChange) p.beatLength = SLIDER_75;
                }

                UpdateSelected();
            }
        }

        private void radBulkSlider100_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radBulkSlider100.Checked)
            {
                radBulkSlider75.Checked = false;
                radBulkSlider150.Checked = false;
                radBulkSliderCustom.Checked = false;
                udBulkSliderMultiplier.Enabled = false;
                suppressEvents = true;
                udBulkSliderMultiplier.Value = 1.0m;
                suppressEvents = false;

                foreach (ControlPoint p in selectedPoints)
                {
                    if (!p.timingChange) p.beatLength = -100.0d;
                }

                UpdateSelected();
            }
        }

        private void radBulkSlider150_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radBulkSlider150.Checked)
            {
                radBulkSlider75.Checked = false;
                radBulkSlider100.Checked = false;
                radBulkSliderCustom.Checked = false;
                udBulkSliderMultiplier.Enabled = false;
                suppressEvents = true;
                udBulkSliderMultiplier.Value = (decimal)(-100.0d / SLIDER_150);
                suppressEvents = false;

                foreach (ControlPoint p in selectedPoints)
                {
                    if (!p.timingChange) p.beatLength = SLIDER_150;
                }

                UpdateSelected();
            }
        }

        private void radBulkSliderCustom_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radBulkSliderCustom.Checked)
            {
                radBulkSlider75.Checked = false;
                radBulkSlider100.Checked = false;
                radBulkSlider150.Checked = false;
                udBulkSliderMultiplier.Enabled = true;

                UpdateSelected();
            }
        }

        private void udBulkSliderMultiplier_ValueChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            foreach (ControlPoint p in selectedPoints)
            {
                if (!p.timingChange) p.beatLength = (double)(-100.0m / udBulkSliderMultiplier.Value);
            }

            UpdateSelected();
        }

        private void radSamplesetDefault_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radSamplesetDefault.Checked)
            {
                radSamplesetCustom1.Checked = false;
                radSamplesetCustom2.Checked = false;

                foreach (ControlPoint p in selectedPoints)
                {
                    p.customSamples = CustomSampleSet.Default;
                }

                UpdateSelected();
            }
        }

        private void radSamplesetCustom1_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radSamplesetCustom1.Checked)
            {
                radSamplesetDefault.Checked = false;
                radSamplesetCustom2.Checked = false;

                foreach (ControlPoint p in selectedPoints)
                {
                    p.customSamples = CustomSampleSet.Custom1;
                }

                UpdateSelected();
            }
        }

        private void radSamplesetCustom2_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            if (radSamplesetCustom2.Checked)
            {
                nudCustom.Enabled = true;

                radSamplesetDefault.Checked = false;
                radSamplesetCustom1.Checked = false;

                foreach (ControlPoint p in selectedPoints)
                {
                    p.customSamples = (CustomSampleSet)nudCustom.Value;
                }

                UpdateSelected();
            }
            else
            {
                nudCustom.Enabled = false;
            }
        }

        private void nudCustom_ValueChanged(object sender, EventArgs e)
        {
            if (!radSamplesetCustom2.Checked)
                return;
            foreach (ControlPoint p in selectedPoints)
            {
                p.customSamples = (CustomSampleSet)nudCustom.Value;
            }

            UpdateSelected();
        }

        private void tbVolume_ValueChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            suppressEvents = true;
            udVolume.Value = tbVolume.Value;
            suppressEvents = false;

            foreach (ControlPoint p in selectedPoints)
            {
                p.volume = tbVolume.Value;
            }

            UpdateSelected();
        }

        private void udVolume_ValueChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            suppressEvents = true;

            udVolume.Value = Math.Min(100,Math.Max(5,udVolume.Value));
            tbVolume.Value = (int)udVolume.Value;
            suppressEvents = false;

            foreach (ControlPoint p in selectedPoints)
            {
                p.volume = (int)udVolume.Value;
            }

            UpdateSelected();
        }

        private void udBpm_ValueChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            tp.beatLength = 60000.0d / (double)udBpm.Value;

            UpdateSelected();
        }

        private void udTimingOffset_ValueChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            tp.offset = (double)udTimingOffset.Value;

            Sort();
        }

        private void btnTimingUseCurrent_Click(object sender, EventArgs e)
        {
            tp.offset = (AudioEngine.Time < 0) ? 0 : (int)AudioEngine.Time;
            udTimingOffset.Value = (decimal)tp.offset;

            Sort();
        }

        private void udGameplayOffset_ValueChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            tp.offset = (double)udGameplayOffset.Value;

            Sort();
        }

        private void btnGameplayUseCurrent_Click(object sender, EventArgs e)
        {
            tp.offset = AudioEngine.Time;
            udGameplayOffset.Value = (decimal)tp.offset;

            Sort();
        }

        private void btnBulkMove_Click(object sender, EventArgs e)
        {
            foreach (ControlPoint p in selectedPoints)
            {
                p.offset += (int)udBulkMove.Value;
            }
            udBulkMove.Value = 0;

            Sort();
        }

        private void listSampleset_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;
            if (listSampleset.SelectedItem == null) return;

            SampleSet ss = SampleSet.None;
            ss = (SampleSet)Enum.Parse(typeof(SampleSet), listSampleset.SelectedItem.ToString());

            foreach (ControlPoint p in selectedPoints)
            {
                p.sampleSet = ss;
            }

            UpdateSelected();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GameBase.ProcessStart(Urls.CUSTOM_SAMPLES);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AudioEngine.PlayHitSamples(new HitSoundInfo(HitObjectSoundType.Normal, tp.sampleSet, tp.customSamples, tp.volume), 0.0f, false);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AudioEngine.PlayHitSamples(new HitSoundInfo(HitObjectSoundType.Whistle, tp.sampleSet, tp.customSamples, tp.volume), 0.0f, false);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AudioEngine.PlayHitSamples(new HitSoundInfo(HitObjectSoundType.Finish, tp.sampleSet, tp.customSamples, tp.volume), 0.0f, false);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AudioEngine.PlayHitSamples(new HitSoundInfo(HitObjectSoundType.Clap, tp.sampleSet, tp.customSamples, tp.volume), 0.0f, false);
        }

        private void chkKiai_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;
            bool checkFirst = true;

            foreach (ControlPoint p in selectedPoints)
            {
                if (checkFirst && first(p))
                {
                    checkFirst = false;
                    continue;
                }
                checkFirst = false;
                p.kiaiMode = chkKiai.Checked;
            }

            chkKiai.ThreeState = false;

            UpdateSelected();
        }

        private void chkInherit_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;
            if (tp == null) return;

            foreach (ControlPoint p in selectedPoints)
            {
                if (chkInherit.Checked)
                {
                    p.timingChange = false;
                    if (p.beatLength>0)
                        p.beatLength = -100;
                }
                else
                {
                    p.beatLength = LastBpm(p);
                    p.timingChange = true;
                }
            }

            chkInherit.ThreeState = false;

            UpdateSelected();
            UpdateLeft();
        }

        private void toolAdd_Click(object sender, EventArgs e)
        {

            ControlPoint c;

            int top_index = listTimingPoints.Items.IndexOf(listTimingPoints.TopItem);
            bool timing_change = tabsRight.SelectedIndex == 1 || (tabsRight.SelectedIndex == 0 && listTimingPoints.SelectedIndices.Count == 0);
            if (listTimingPoints.SelectedIndices.Count > 0 && tp != null)
            {
                double bpm;

                if (timing_change) bpm = LastBpm(tp);
                else if (tp.timingChange) bpm = tp.timingChange ? tp.beatLength : -100;
                else bpm = tp.beatLength;

                c = new ControlPoint(selectionChanged ? tp.offset + 100.0f : AudioEngine.Time, bpm, tp.timeSignature, tp.sampleSet, tp.customSamples, tp.volume, timing_change, tp.effectFlags);
            }
            else
            {
                c = new ControlPoint(0, timing_change ? 500 : -100);
                c.timingChange = timing_change;
            }

            controlPoints.Add(c);
            controlPoints.Sort();

            SuspendDrawing(listTimingPoints);
            UpdateAll(false);

            suppressEvents = true;
            int index = controlPoints.IndexOf(c);
            listTimingPoints.SelectedIndices.Add(index);
            suppressEvents = false;

            ScrollToIndex(top_index);
            ScrollToItem(index);
            listTimingPoints.Focus();
            ResumeDrawing(listTimingPoints);

            GetSelectedPoints();
            UpdateLeft();

            selectionChanged = true;
        }

        private void toolRemove_Click(object sender, EventArgs e)
        {
#if !DEBUG
            if (listTimingPoints.SelectedIndices.Count == 0) return;
#endif
            int index = Math.Max(listTimingPoints.SelectedIndices[0] - 1, 0);

            foreach (ControlPoint p in selectedPoints)
            {
                controlPoints.Remove(p);
            }

            SuspendDrawing(listTimingPoints);
            suppressEvents = true;
            foreach (ListViewItem item in listTimingPoints.Items)
            {
                if (item.Selected) listTimingPoints.Items.Remove(item);
            }
            suppressEvents = false;
            FixAlternatingColours();

            if (index < controlPoints.Count)
                listTimingPoints.SelectedIndices.Add(index);
            else if (controlPoints.Count > 0)
                listTimingPoints.SelectedIndices.Add(controlPoints.Count - 1);
            ResumeDrawing(listTimingPoints);

            GetSelectedPoints();
            UpdateLeft();

            ScrollToItem(index);
            listTimingPoints.Focus();
            selectionChanged = true;
            ResumeDrawing(listTimingPoints);
        }

        private bool dirty;

        private void listTimingPoints_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            GetSelectedPoints();

            selectionChanged = true;
            dirty = true;
        }

        private double LastBpm(ControlPoint p)
        {
            int index = controlPoints.IndexOf(p);

            for (int x = index; x >= 0; x--)
            {
                ControlPoint q = controlPoints[x];
                if (q.timingChange) return q.beatLength;
            }

            return 500;
        }

        private void tabsRight_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabsRight.SelectedIndex)
            {
                case 0:
                    Filter(true, true);
                    break;
                case 1:
                    Filter(true, false);
                    break;
                case 2:
                    Filter(false, true);
                    break;
            }

            SuspendDrawing(listTimingPoints);

            ScrollToIndex(0);
            UpdateAll(false);
            if (listTimingPoints.Items.Count > 0)
                listTimingPoints.SelectedIndices.Add(0);
            GetSelectedPoints();
            ResumeDrawing(listTimingPoints);

            UpdateLeft();
            tabsRight.Focus();
        }

        private void Filter(bool timing, bool inherit)
        {
            List<ControlPoint> Showing = new List<ControlPoint>();
            List<ControlPoint> Hidden = new List<ControlPoint>();

            foreach (ControlPoint p in controlPoints)
            {
                if (p.timingChange)
                {
                    if (timing) Showing.Add(p);
                    else Hidden.Add(p);
                }
                else
                {
                    if (inherit) Showing.Add(p);
                    else Hidden.Add(p);
                }
            }

            foreach (ControlPoint p in hiddenPoints)
            {
                if (p.timingChange)
                {
                    if (timing) Showing.Add(p);
                    else Hidden.Add(p);
                }
                else
                {
                    if (inherit) Showing.Add(p);
                    else Hidden.Add(p);
                }
            }

            Showing.Sort();
            Hidden.Sort();

            controlPoints = Showing;
            hiddenPoints = Hidden;
        }

        private void listTimingPoints_ItemActivate(object sender, EventArgs e)
        {
            if (tp != null) AudioEngine.SeekTo((int)tp.offset);
        }

        private void listTimingPoints_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Delete:
                    if (listTimingPoints.SelectedIndices.Count > 0)
                        toolRemove_Click(sender, EventArgs.Empty);
                    break;
                case Keys.A:
                    if (e.Control) SelectAll();
                    break;
                case Keys.C:
                    if (e.Control) Copy();
                    break;
                case Keys.V:
                    if (e.Control) Paste();
                    break;
                case Keys.X:
                    if (e.Control)
                    {
                        Copy();
                        toolRemove_Click(sender, EventArgs.Empty);
                    }
                    break;
            }
            if (dirty) UpdateLeft();
            listTimingPoints.Focus();
        }

        private void listTimingPoints_Click(object sender, EventArgs e)
        {
            if (dirty) UpdateLeft();
            listTimingPoints.Focus();
        }

        private void listTimingPoints_KeyUp(object sender, KeyEventArgs e)
        {
            if (dirty) UpdateLeft();
            listTimingPoints.Focus();
        }

        /// <summary>
        /// true if this is the first control point in the collection
        /// </summary>
        private bool first(ControlPoint p)
        {
            ControlPoint first;
            if (hiddenPoints.Count == 0) first = controlPoints[0];
            else if (controlPoints.Count == 0) first = hiddenPoints[0];
            else
                first = (controlPoints[0].offset < hiddenPoints[0].offset) ? controlPoints[0] : hiddenPoints[0];
            return p == first;
        }

        private void SelectAll()
        {
            listTimingPoints.SelectedIndices.Clear();
            for (int index = 0; index < listTimingPoints.Items.Count; index++)
            {
                listTimingPoints.SelectedIndices.Add(index);
            }
        }

        private void Copy()
        {
            if (selectedPoints.Count == 0) return;
            string s = HitObjectManager.OutputTimingPoints(selectedPoints);
            Clipboard.Clear();
            Clipboard.SetData(DataFormats.Text, s);
        }

        private bool Paste()
        {
            if (!Clipboard.ContainsData(DataFormats.Text)) return false;
            bool success;
            List<ControlPoint> data = HitObjectManager.ParseTimingPoints(BeatmapManager.Current, (string)Clipboard.GetData(DataFormats.Text), out success, false);
            if (!success) return false;
            listTimingPoints.SelectedIndices.Clear();
            selectedPoints.Clear();
            controlPoints.AddRange(data);
            selectedPoints.AddRange(data);
            Sort();
            GetSelectedPoints();
            UpdateLeft();
            listTimingPoints.Focus();
            if (listTimingPoints.SelectedIndices.Count > 0) ScrollToItem(listTimingPoints.SelectedIndices[0]);
            return true;
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            cutToolStripMenuItem.Enabled = copyToolStripMenuItem.Enabled = deleteToolStripMenuItem.Enabled = selectedPoints.Count > 0;
            bool canPaste = false;
            if (Clipboard.ContainsData(DataFormats.Text)) HitObjectManager.ParseTimingPoints(BeatmapManager.Current, (string)Clipboard.GetData(DataFormats.Text), out canPaste, true);
            pasteToolStripMenuItem.Enabled = canPaste;
            selectAllToolStripMenuItem.Enabled = controlPoints.Count > 0;
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Copy();
            toolRemove_Click(sender, EventArgs.Empty);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Paste();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolRemove_Click(sender, EventArgs.Empty);
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectAll();
        }

        private void addControlPointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolAdd_Click(sender, EventArgs.Empty);
        }

        private void listTimingPoints_MouseUp(object sender, MouseEventArgs e)
        {
            if (dirty) UpdateLeft();
        }

        private void listTimingPoints_Leave(object sender, EventArgs e)
        {
            if (dirty) UpdateLeft();
        }

        private void udBulkMove_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnBulkMove;
        }

        private void udBulkMove_Leave(object sender, EventArgs e)
        {
            AcceptButton = buttonOk;
        }

        private void chkOmitBarline_CheckedChanged(object sender, EventArgs e)
        {
            if (suppressEvents) return;

            foreach (ControlPoint p in selectedPoints)
            {
                if (!p.timingChange) continue;

                if (chkOmitBarline.Checked) p.effectFlags |= EffectFlags.OmitFirstBarLine;
                else p.effectFlags &= ~EffectFlags.OmitFirstBarLine;
            }

            chkOmitBarline.ThreeState = false;

            UpdateSelected();
        }

        private void TimingEntry_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (updateChanges)
            {
                bool isChanged = controlPoints.Count != AudioEngine.ControlPoints.Count;
                Filter(true, true);

                //Look for differences between the two lists, and whether there exists an uninherited section
                bool hasTiming = false;
                bool continueLoop = true;
                int i = 0;
                foreach (ControlPoint p in controlPoints)
                {
                    if (p.timingChange)
                    {
                        hasTiming = true;
                    }
                    continueLoop = i < AudioEngine.ControlPoints.Count;
                    if (hasTiming && (isChanged || !continueLoop))
                    {
                        break;
                    }
                    if (continueLoop)
                    {
                        isChanged = !(p.Equals(AudioEngine.ControlPoints[i]));
                        i++;
                    }

                }
                if (isChanged)
                {
                    editor.Dirty = true;
                }
                else
                {
                    editor.UndoPop(true);
                }

                if (!hasTiming)
                {
                    MessageBox.Show("At least one timing point is required.", "osu!", MessageBoxButtons.OK);
                    this.DialogResult = System.Windows.Forms.DialogResult.None;
                    return;
                }

                GameBase.Scheduler.Add(delegate
                {
                    if (chkAdjustBookmarkAndPreview.Checked)
                    {
                        if (isChanged)
                            editor.UndoPush();

                        //Adjust bookmarks and previews based on the difference in offset of the first control point.
                        int adjustment = (int)(controlPoints[0].offset - AudioEngine.ControlPoints[0].offset);

                        for (i = 0; i < editor.hitObjectManager.Bookmarks.Count; i++)
                            editor.hitObjectManager.Bookmarks[i] += adjustment;

                        BeatmapManager.Current.PreviewTime += adjustment;

                        isChanged = adjustment > 0;
                        if (isChanged)
                            editor.Dirty = true;
                    }

                    AudioEngine.ControlPoints = controlPoints;

                    int snapDivisor = 0;
                    switch (udBeatSnap.SelectedIndex)
                    {
                        case 0:
                            snapDivisor = 1;
                            break;
                        case 1:
                            snapDivisor = 2;
                            break;
                        case 2:
                            snapDivisor = 3;
                            break;
                        case 3:
                            snapDivisor = 4;
                            break;
                        case 4:
                            snapDivisor = 5;
                            break;
                        case 5:
                            snapDivisor = 6;
                            break;
                        case 6:
                            snapDivisor = 12;
                            break;
                        case 7:
                            snapDivisor = 24;
                            break;
                    }

                    if (chkScaleObjects.Checked) // scale to timing
                    {
                        // todo: fix this for multiple sections
                        editor.Timing.bpmUpdatePending = true;
                        editor.Timing.offsetUpdatePending = true;
                    }
                    else if (chkSnapObjects.Checked) // snap to timing
                        editor.Timing.beatSnapSong(false, snapDivisor); //todo: Turn this into boolean like the rest

                    if (chkResnapSliders.Checked) // recalculate slider lengths
                    {
                        int tmp = editor.beatSnapDivisor;
                        editor.beatSnapDivisor = snapDivisor;
                        editor.hitObjectManager.UpdateVariables(false);
                        AudioEngine.UpdateActiveTimingPoint(true);
                        editor.hitObjectManager.UpdateSliders(false, true);
                        editor.beatSnapDivisor = tmp;
                    }

                    editor.Timing.FixInheritSignatures();
                    editor.Timing.timingWindowUpdatePending = true;

                    editor.Dirty = true;

                    AudioEngine.UpdateActiveTimingPoint(true);
                });
            }
            else
            {
                Editor.Instance.UndoPop(true);
            }
        }
    }
}