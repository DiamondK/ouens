﻿using osu.GameplayElements.Beatmaps;
using osu_common;

namespace osu.GameModes.Edit.Forms
{
    using osu_common.Helpers;
    partial class EditorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            editor = null;
            GameBase.EditorControl = null;

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>       
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.totalLoadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asReferenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.loadSoftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.testBeatmapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openAiModToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.submitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.packageForDistributionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extractMapPackageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importBMSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bmsbmeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.openSongFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.openOsuInNotepad = new System.Windows.Forms.ToolStripMenuItem();
            this.openosbInNotepadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemRedo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cloneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.reverseSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flipHorizontallyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flipVerticallyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateClockwiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateAnticlockwiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotationByToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scaleByToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.resetSliderStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetAllSamplesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetComboColoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetBreaksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.nudgeBackwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nudgeOneMeasureRightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.composeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.designToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.songSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timingSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.volumeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gridLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mostSparseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gridSize2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gridSize3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mostPreciseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showVideoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showSampleNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.snakingSlidersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hitAnimationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.followPointsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disableUndoStatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.composeToolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.snapDivisorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fullBeatsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.halfBeatsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quarterBeatsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eighthBeatsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tripletsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doubleTripletsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.audioRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.audioRate25ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.audioRate50ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.audioRate75ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.audioRate100ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beatSnappingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.polygonStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertSliderToStreamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableLiveMappingModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sampleImportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.designToolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.moveAllElementsInTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timingToolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.timingSignatureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.signature44 = new System.Windows.Forms.ToolStripMenuItem();
            this.signature34 = new System.Windows.Forms.ToolStripMenuItem();
            this.metronomeClicksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.addTimingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addTimingPointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.retimeCurrentPointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteTimingPointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.snapAllNotesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.bPMOffsetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.resnapAllNotesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveAllNotesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recalculateSlidersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetAllTimingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.setPreviewPointToCurrentPositionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.webToolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.thisBeatmapsInformationPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thisBeatmapsThreadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickReplyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.displayInlineHlpeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewFAQToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenu,
            this.editToolStripMenu,
            this.viewToolStripMenu,
            this.composeToolStripMenu,
            this.designToolStripMenu,
            this.timingToolStripMenu,
            this.webToolStripMenu,
            this.helpToolStripMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.MenuActivate += new System.EventHandler(this.menuStrip1_MenuActivate);
            this.menuStrip1.MenuDeactivate += new System.EventHandler(this.menuStrip1_MenuDeactivate);
            // 
            // fileToolStripMenu
            // 
            this.fileToolStripMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator15,
            this.loadSoftToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.toolStripSeparator12,
            this.testBeatmapToolStripMenuItem,
            this.openAiModToolStripMenuItem,
            this.toolStripSeparator17,
            this.submitToolStripMenuItem,
            this.packageForDistributionToolStripMenuItem,
            this.extractMapPackageToolStripMenuItem,
            this.importBMSToolStripMenuItem,
            this.toolStripSeparator13,
            this.openSongFolder,
            this.openOsuInNotepad,
            this.openosbInNotepadToolStripMenuItem,
            this.toolStripSeparator11,
            this.exitToolStripMenuItem});
            this.fileToolStripMenu.Name = "fileToolStripMenu";
            this.fileToolStripMenu.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenu.Text = "&File";
            this.fileToolStripMenu.DropDownOpening += new System.EventHandler(this.fileToolStripMenu_DropDownOpening);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+N";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.newToolStripMenuItem.Text = "Clear All &Notes";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.totalLoadToolStripMenuItem,
            this.asReferenceToolStripMenuItem});
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+O";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.openToolStripMenuItem.Text = "&Open Difficulty...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // totalLoadToolStripMenuItem
            // 
            this.totalLoadToolStripMenuItem.Name = "totalLoadToolStripMenuItem";
            this.totalLoadToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.totalLoadToolStripMenuItem.Text = "For Editing";
            this.totalLoadToolStripMenuItem.Click += new System.EventHandler(this.forEditingToolStripMenuItem_Click);
            // 
            // asReferenceToolStripMenuItem
            // 
            this.asReferenceToolStripMenuItem.Enabled = false;
            this.asReferenceToolStripMenuItem.Name = "asReferenceToolStripMenuItem";
            this.asReferenceToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.asReferenceToolStripMenuItem.Text = "For Reference";
            this.asReferenceToolStripMenuItem.Click += new System.EventHandler(this.asReferenceToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+S";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.saveAsToolStripMenuItem.Text = "Save &as New Difficulty...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(254, 6);
            // 
            // loadSoftToolStripMenuItem
            // 
            this.loadSoftToolStripMenuItem.Name = "loadSoftToolStripMenuItem";
            this.loadSoftToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+L";
            this.loadSoftToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.loadSoftToolStripMenuItem.Text = "&Revert to Saved";
            this.loadSoftToolStripMenuItem.Click += new System.EventHandler(this.loadSoftToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+L";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.loadToolStripMenuItem.Text = "Revert to Saved (Full)";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(254, 6);
            // 
            // testBeatmapToolStripMenuItem
            // 
            this.testBeatmapToolStripMenuItem.Name = "testBeatmapToolStripMenuItem";
            this.testBeatmapToolStripMenuItem.ShortcutKeyDisplayString = "F5";
            this.testBeatmapToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.testBeatmapToolStripMenuItem.Text = "&Test Beatmap";
            this.testBeatmapToolStripMenuItem.Click += new System.EventHandler(this.testBeatmapToolStripMenuItem_Click);
            // 
            // openAiModToolStripMenuItem
            // 
            this.openAiModToolStripMenuItem.Name = "openAiModToolStripMenuItem";
            this.openAiModToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+A";
            this.openAiModToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.openAiModToolStripMenuItem.Text = "Open Ai&Mod";
            this.openAiModToolStripMenuItem.Click += new System.EventHandler(this.openAiModToolStripMenuItem_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(254, 6);
            // 
            // submitToolStripMenuItem
            // 
            this.submitToolStripMenuItem.Name = "submitToolStripMenuItem";
            this.submitToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+U";
            this.submitToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.submitToolStripMenuItem.Text = "&Upload Beatmap...";
            this.submitToolStripMenuItem.Click += new System.EventHandler(this.submitToolStripMenuItem_Click);
            // 
            // packageForDistributionToolStripMenuItem
            // 
            this.packageForDistributionToolStripMenuItem.Name = "packageForDistributionToolStripMenuItem";
            this.packageForDistributionToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.packageForDistributionToolStripMenuItem.Text = "&Export Package...";
            this.packageForDistributionToolStripMenuItem.Click += new System.EventHandler(this.packageForDistributionToolStripMenuItem_Click);
            // 
            // extractMapPackageToolStripMenuItem
            // 
            this.extractMapPackageToolStripMenuItem.Name = "extractMapPackageToolStripMenuItem";
            this.extractMapPackageToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.extractMapPackageToolStripMenuItem.Text = "Extract Map Package";
            this.extractMapPackageToolStripMenuItem.Click += new System.EventHandler(this.extractMapPackageToolStripMenuItem_Click);
            // 
            // importBMSToolStripMenuItem
            // 
            this.importBMSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bmsbmeToolStripMenuItem});
            this.importBMSToolStripMenuItem.Name = "importBMSToolStripMenuItem";
            this.importBMSToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.importBMSToolStripMenuItem.Text = "Import from...";
            // 
            // bmsbmeToolStripMenuItem
            // 
            this.bmsbmeToolStripMenuItem.Name = "bmsbmeToolStripMenuItem";
            this.bmsbmeToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.bmsbmeToolStripMenuItem.Text = "bms/bme";
            this.bmsbmeToolStripMenuItem.Click += new System.EventHandler(this.bmsbmeToolStripMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(254, 6);
            // 
            // openSongFolder
            // 
            this.openSongFolder.Name = "openSongFolder";
            this.openSongFolder.Size = new System.Drawing.Size(257, 22);
            this.openSongFolder.Text = "Open Song Fold&er";
            this.openSongFolder.Click += new System.EventHandler(this.openSongFolderToolStripMenuItem_Click);
            // 
            // openOsuInNotepad
            // 
            this.openOsuInNotepad.Name = "openOsuInNotepad";
            this.openOsuInNotepad.Size = new System.Drawing.Size(257, 22);
            this.openOsuInNotepad.Text = "Open .osu in Notepad";
            this.openOsuInNotepad.Click += new System.EventHandler(this.openNotepadToolStripMenuItem_Click);
            // 
            // openosbInNotepadToolStripMenuItem
            // 
            this.openosbInNotepadToolStripMenuItem.Name = "openosbInNotepadToolStripMenuItem";
            this.openosbInNotepadToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.openosbInNotepadToolStripMenuItem.Text = "Open .osb in Notepad";
            this.openosbInNotepadToolStripMenuItem.Click += new System.EventHandler(this.openosbInNotepadToolStripMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(254, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeyDisplayString = "Esc";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.exitToolStripMenuItem.Text = "E&xit...";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenu
            // 
            this.editToolStripMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemUndo,
            this.menuItemRedo,
            this.toolStripSeparator9,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.toolStripSeparator1,
            this.selectAllToolStripMenuItem,
            this.cloneToolStripMenuItem,
            this.toolStripSeparator3,
            this.reverseSelectionToolStripMenuItem,
            this.flipHorizontallyToolStripMenuItem,
            this.flipVerticallyToolStripMenuItem,
            this.rotateClockwiseToolStripMenuItem,
            this.rotateAnticlockwiseToolStripMenuItem,
            this.rotationByToolStripMenuItem,
            this.scaleByToolStripMenuItem,
            this.toolStripSeparator14,
            this.resetSliderStripMenuItem,
            this.resetAllSamplesToolStripMenuItem,
            this.resetComboColoursToolStripMenuItem,
            this.resetBreaksToolStripMenuItem,
            this.toolStripSeparator2,
            this.nudgeBackwardToolStripMenuItem,
            this.nudgeOneMeasureRightToolStripMenuItem});
            this.editToolStripMenu.Name = "editToolStripMenu";
            this.editToolStripMenu.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenu.Text = "&Edit";
            this.editToolStripMenu.DropDownOpening += new System.EventHandler(this.editToolStripMenu_DropDownOpening);
            // 
            // menuItemUndo
            // 
            this.menuItemUndo.Enabled = false;
            this.menuItemUndo.Name = "menuItemUndo";
            this.menuItemUndo.ShortcutKeyDisplayString = "Ctrl+Z";
            this.menuItemUndo.Size = new System.Drawing.Size(246, 22);
            this.menuItemUndo.Text = "Undo";
            this.menuItemUndo.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // menuItemRedo
            // 
            this.menuItemRedo.Enabled = false;
            this.menuItemRedo.Name = "menuItemRedo";
            this.menuItemRedo.ShortcutKeyDisplayString = "Ctrl+Y";
            this.menuItemRedo.Size = new System.Drawing.Size(246, 22);
            this.menuItemRedo.Text = "Redo";
            this.menuItemRedo.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(243, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+X";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.cutToolStripMenuItem.Text = "Cu&t";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+C";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.copyToolStripMenuItem.Text = "&Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+V";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.pasteToolStripMenuItem.Text = "&Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeyDisplayString = "Del";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.deleteToolStripMenuItem.Text = "&Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(243, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+A";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.selectAllToolStripMenuItem.Text = "Select &All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // cloneToolStripMenuItem
            // 
            this.cloneToolStripMenuItem.Name = "cloneToolStripMenuItem";
            this.cloneToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+D";
            this.cloneToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.cloneToolStripMenuItem.Text = "C&lone";
            this.cloneToolStripMenuItem.Click += new System.EventHandler(this.cloneToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(243, 6);
            // 
            // reverseSelectionToolStripMenuItem
            // 
            this.reverseSelectionToolStripMenuItem.Name = "reverseSelectionToolStripMenuItem";
            this.reverseSelectionToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+G";
            this.reverseSelectionToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.reverseSelectionToolStripMenuItem.Text = "&Reverse Selection";
            this.reverseSelectionToolStripMenuItem.Click += new System.EventHandler(this.reverseSelectionToolStripMenuItem_Click);
            // 
            // flipHorizontallyToolStripMenuItem
            // 
            this.flipHorizontallyToolStripMenuItem.Name = "flipHorizontallyToolStripMenuItem";
            this.flipHorizontallyToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+H";
            this.flipHorizontallyToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.flipHorizontallyToolStripMenuItem.Text = "Flip &Horizontally";
            this.flipHorizontallyToolStripMenuItem.Click += new System.EventHandler(this.flipHorizontalToolStripMenuItem_Click);
            // 
            // flipVerticallyToolStripMenuItem
            // 
            this.flipVerticallyToolStripMenuItem.Name = "flipVerticallyToolStripMenuItem";
            this.flipVerticallyToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+J";
            this.flipVerticallyToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.flipVerticallyToolStripMenuItem.Text = "Flip &Vertically";
            this.flipVerticallyToolStripMenuItem.Click += new System.EventHandler(this.flipVerticalToolStripMenuItem_Click);
            // 
            // rotateClockwiseToolStripMenuItem
            // 
            this.rotateClockwiseToolStripMenuItem.Name = "rotateClockwiseToolStripMenuItem";
            this.rotateClockwiseToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+>";
            this.rotateClockwiseToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.rotateClockwiseToolStripMenuItem.Text = "Rotate 90° Cl&ockwise";
            this.rotateClockwiseToolStripMenuItem.Click += new System.EventHandler(this.rotateClockwiseToolStripMenuItem_Click);
            // 
            // rotateAnticlockwiseToolStripMenuItem
            // 
            this.rotateAnticlockwiseToolStripMenuItem.Name = "rotateAnticlockwiseToolStripMenuItem";
            this.rotateAnticlockwiseToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+<";
            this.rotateAnticlockwiseToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.rotateAnticlockwiseToolStripMenuItem.Text = "Rotate 90° Antic&lockwise";
            this.rotateAnticlockwiseToolStripMenuItem.Click += new System.EventHandler(this.rotateAnticlockwiseToolStripMenuItem_Click);
            // 
            // rotationByToolStripMenuItem
            // 
            this.rotationByToolStripMenuItem.Name = "rotationByToolStripMenuItem";
            this.rotationByToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+R";
            this.rotationByToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.rotationByToolStripMenuItem.Text = "Rotate by...";
            this.rotationByToolStripMenuItem.Click += new System.EventHandler(this.rotationByToolStripMenuItem_Click);
            // 
            // scaleByToolStripMenuItem
            // 
            this.scaleByToolStripMenuItem.Name = "scaleByToolStripMenuItem";
            this.scaleByToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+S";
            this.scaleByToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.scaleByToolStripMenuItem.Text = "Scale by...";
            this.scaleByToolStripMenuItem.Click += new System.EventHandler(this.scaleByToolStripMenuItem_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(243, 6);
            // 
            // resetSliderStripMenuItem
            // 
            this.resetSliderStripMenuItem.Name = "resetSliderStripMenuItem";
            this.resetSliderStripMenuItem.ShortcutKeyDisplayString = "";
            this.resetSliderStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.resetSliderStripMenuItem.Text = "Reset selected objects\' samples";
            this.resetSliderStripMenuItem.Click += new System.EventHandler(this.resetSliderStripMenuItem_Click);
            // 
            // resetAllSamplesToolStripMenuItem
            // 
            this.resetAllSamplesToolStripMenuItem.ForeColor = System.Drawing.Color.Red;
            this.resetAllSamplesToolStripMenuItem.Name = "resetAllSamplesToolStripMenuItem";
            this.resetAllSamplesToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.resetAllSamplesToolStripMenuItem.Text = "Reset all samples";
            this.resetAllSamplesToolStripMenuItem.Click += new System.EventHandler(this.resetAllSamplesToolStripMenuItem_Click);
            // 
            // resetComboColoursToolStripMenuItem
            // 
            this.resetComboColoursToolStripMenuItem.ForeColor = System.Drawing.Color.Red;
            this.resetComboColoursToolStripMenuItem.Name = "resetComboColoursToolStripMenuItem";
            this.resetComboColoursToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.resetComboColoursToolStripMenuItem.Text = "Reset combo colours";
            this.resetComboColoursToolStripMenuItem.Click += new System.EventHandler(this.resetComboColoursToolStripMenuItem_Click);
            // 
            // resetBreaksToolStripMenuItem
            // 
            this.resetBreaksToolStripMenuItem.ForeColor = System.Drawing.Color.Red;
            this.resetBreaksToolStripMenuItem.Name = "resetBreaksToolStripMenuItem";
            this.resetBreaksToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.resetBreaksToolStripMenuItem.Text = "Reset breaks";
            this.resetBreaksToolStripMenuItem.Click += new System.EventHandler(this.resetBreaksToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(243, 6);
            // 
            // nudgeBackwardToolStripMenuItem
            // 
            this.nudgeBackwardToolStripMenuItem.Name = "nudgeBackwardToolStripMenuItem";
            this.nudgeBackwardToolStripMenuItem.ShortcutKeyDisplayString = "J";
            this.nudgeBackwardToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.nudgeBackwardToolStripMenuItem.Text = "Nudge &Backward";
            this.nudgeBackwardToolStripMenuItem.Click += new System.EventHandler(this.nudgeBackwardToolStripMenuItem_Click);
            // 
            // nudgeOneMeasureRightToolStripMenuItem
            // 
            this.nudgeOneMeasureRightToolStripMenuItem.Name = "nudgeOneMeasureRightToolStripMenuItem";
            this.nudgeOneMeasureRightToolStripMenuItem.ShortcutKeyDisplayString = "K";
            this.nudgeOneMeasureRightToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.nudgeOneMeasureRightToolStripMenuItem.Text = "Nudge &Forward";
            this.nudgeOneMeasureRightToolStripMenuItem.Click += new System.EventHandler(this.nudgeOneMeasureRightToolStripMenuItem_Click);
            // 
            // viewToolStripMenu
            // 
            this.viewToolStripMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.composeToolStripMenuItem,
            this.designToolStripMenuItem,
            this.timingToolStripMenuItem,
            this.toolStripSeparator4,
            this.songSetupToolStripMenuItem,
            this.timingSetupToolStripMenuItem,
            this.toolStripSeparator16,
            this.volumeToolStripMenuItem,
            this.gridLevelToolStripMenuItem,
            this.showVideoToolStripMenuItem,
            this.showSampleNameToolStripMenuItem,
            this.snakingSlidersToolStripMenuItem,
            this.hitAnimationsToolStripMenuItem,
            this.followPointsToolStripMenuItem,
            this.disableUndoStatesToolStripMenuItem});
            this.viewToolStripMenu.Name = "viewToolStripMenu";
            this.viewToolStripMenu.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenu.Text = "&View";
            // 
            // composeToolStripMenuItem
            // 
            this.composeToolStripMenuItem.Checked = true;
            this.composeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.composeToolStripMenuItem.Name = "composeToolStripMenuItem";
            this.composeToolStripMenuItem.ShortcutKeyDisplayString = "F1";
            this.composeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.composeToolStripMenuItem.Text = "&Compose";
            this.composeToolStripMenuItem.Click += new System.EventHandler(this.composeToolStripMenuItem_Click);
            // 
            // designToolStripMenuItem
            // 
            this.designToolStripMenuItem.Name = "designToolStripMenuItem";
            this.designToolStripMenuItem.ShortcutKeyDisplayString = "F2";
            this.designToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.designToolStripMenuItem.Text = "&Design";
            this.designToolStripMenuItem.Click += new System.EventHandler(this.designToolStripMenuItem_Click);
            // 
            // timingToolStripMenuItem
            // 
            this.timingToolStripMenuItem.Name = "timingToolStripMenuItem";
            this.timingToolStripMenuItem.ShortcutKeyDisplayString = "F3";
            this.timingToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.timingToolStripMenuItem.Text = "&Timing";
            this.timingToolStripMenuItem.Click += new System.EventHandler(this.timingToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(177, 6);
            // 
            // songSetupToolStripMenuItem
            // 
            this.songSetupToolStripMenuItem.Name = "songSetupToolStripMenuItem";
            this.songSetupToolStripMenuItem.ShortcutKeyDisplayString = "F4";
            this.songSetupToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.songSetupToolStripMenuItem.Text = "Song &Setup...";
            this.songSetupToolStripMenuItem.Click += new System.EventHandler(this.songSetupToolStripMenuItem_Click);
            // 
            // timingSetupToolStripMenuItem
            // 
            this.timingSetupToolStripMenuItem.Name = "timingSetupToolStripMenuItem";
            this.timingSetupToolStripMenuItem.ShortcutKeyDisplayString = "F6";
            this.timingSetupToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.timingSetupToolStripMenuItem.Text = "Timing Setup...";
            this.timingSetupToolStripMenuItem.Click += new System.EventHandler(this.timingSetupToolStripMenuItem_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(177, 6);
            // 
            // volumeToolStripMenuItem
            // 
            this.volumeToolStripMenuItem.Name = "volumeToolStripMenuItem";
            this.volumeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.volumeToolStripMenuItem.Text = "Volume";
            this.volumeToolStripMenuItem.Click += new System.EventHandler(this.volumeToolStripMenuItem_Click);
            // 
            // gridLevelToolStripMenuItem
            // 
            this.gridLevelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mostSparseToolStripMenuItem,
            this.gridSize2ToolStripMenuItem,
            this.gridSize3ToolStripMenuItem,
            this.mostPreciseToolStripMenuItem});
            this.gridLevelToolStripMenuItem.Name = "gridLevelToolStripMenuItem";
            this.gridLevelToolStripMenuItem.ShortcutKeyDisplayString = "G";
            this.gridLevelToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.gridLevelToolStripMenuItem.Text = "&Grid Level";
            // 
            // mostSparseToolStripMenuItem
            // 
            this.mostSparseToolStripMenuItem.Name = "mostSparseToolStripMenuItem";
            this.mostSparseToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.mostSparseToolStripMenuItem.Text = "&1 (Most sparse)";
            this.mostSparseToolStripMenuItem.Click += new System.EventHandler(this.mostSparseToolStripMenuItem_Click);
            // 
            // gridSize2ToolStripMenuItem
            // 
            this.gridSize2ToolStripMenuItem.Checked = true;
            this.gridSize2ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.gridSize2ToolStripMenuItem.Name = "gridSize2ToolStripMenuItem";
            this.gridSize2ToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.gridSize2ToolStripMenuItem.Text = "&2";
            this.gridSize2ToolStripMenuItem.Click += new System.EventHandler(this.gridMediumToolStripMenuItem_Click);
            // 
            // gridSize3ToolStripMenuItem
            // 
            this.gridSize3ToolStripMenuItem.Name = "gridSize3ToolStripMenuItem";
            this.gridSize3ToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.gridSize3ToolStripMenuItem.Text = "&3";
            this.gridSize3ToolStripMenuItem.Click += new System.EventHandler(this.gridSmallToolStripMenuItem_Click);
            // 
            // mostPreciseToolStripMenuItem
            // 
            this.mostPreciseToolStripMenuItem.Name = "mostPreciseToolStripMenuItem";
            this.mostPreciseToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.mostPreciseToolStripMenuItem.Text = "&4 (Most precise)";
            this.mostPreciseToolStripMenuItem.Click += new System.EventHandler(this.mostPreciseToolStripMenuItem_Click);
            // 
            // showVideoToolStripMenuItem
            // 
            this.showVideoToolStripMenuItem.Name = "showVideoToolStripMenuItem";
            this.showVideoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.showVideoToolStripMenuItem.Text = "Show Video";
            this.showVideoToolStripMenuItem.Click += new System.EventHandler(this.showVideoToolStripMenuItem_Click);
            // 
            // showSampleNameToolStripMenuItem
            // 
            this.showSampleNameToolStripMenuItem.Name = "showSampleNameToolStripMenuItem";
            this.showSampleNameToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.showSampleNameToolStripMenuItem.Text = "Show Sample Name";
            this.showSampleNameToolStripMenuItem.Click += new System.EventHandler(this.showSampleNameToolStripMenuItem_Click);
            // 
            // snakingSlidersToolStripMenuItem
            // 
            this.snakingSlidersToolStripMenuItem.Name = "snakingSlidersToolStripMenuItem";
            this.snakingSlidersToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.snakingSlidersToolStripMenuItem.Text = "Snaking Sliders";
            this.snakingSlidersToolStripMenuItem.Click += new System.EventHandler(this.snakingSlidersToolStripMenuItem_Click);
            // 
            // hitAnimationsToolStripMenuItem
            // 
            this.hitAnimationsToolStripMenuItem.Name = "hitAnimationsToolStripMenuItem";
            this.hitAnimationsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.hitAnimationsToolStripMenuItem.Text = "Hit Animations";
            this.hitAnimationsToolStripMenuItem.Click += new System.EventHandler(this.hitAnimationsToolStripMenuItem_Click);
            // 
            // followPointsToolStripMenuItem
            // 
            this.followPointsToolStripMenuItem.Name = "followPointsToolStripMenuItem";
            this.followPointsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.followPointsToolStripMenuItem.Text = "Follow Points";
            this.followPointsToolStripMenuItem.Click += new System.EventHandler(this.followPointsToolStripMenuItem_Click);
            // 
            // disableUndoStatesToolStripMenuItem
            // 
            this.disableUndoStatesToolStripMenuItem.Name = "disableUndoStatesToolStripMenuItem";
            this.disableUndoStatesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.disableUndoStatesToolStripMenuItem.Text = "Disable Undo States";
            this.disableUndoStatesToolStripMenuItem.Click += new System.EventHandler(this.disableUndoStatesToolStripMenuItem_Click);
            // 
            // composeToolStripMenu
            // 
            this.composeToolStripMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.snapDivisorToolStripMenuItem,
            this.audioRateToolStripMenuItem,
            this.beatSnappingToolStripMenuItem,
            this.toolStripSeparator5,
            this.polygonStripMenuItem,
            this.convertSliderToStreamToolStripMenuItem,
            this.enableLiveMappingModeToolStripMenuItem,
            this.sampleImportToolStripMenuItem,
            this.autoToolStripMenuItem});
            this.composeToolStripMenu.Name = "composeToolStripMenu";
            this.composeToolStripMenu.Size = new System.Drawing.Size(70, 20);
            this.composeToolStripMenu.Text = "&Compose";
            this.composeToolStripMenu.DropDownOpening += new System.EventHandler(this.composeToolStripMenu_DropDownOpening);
            // 
            // snapDivisorToolStripMenuItem
            // 
            this.snapDivisorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fullBeatsToolStripMenuItem,
            this.halfBeatsToolStripMenuItem,
            this.quarterBeatsToolStripMenuItem,
            this.eighthBeatsToolStripMenuItem,
            this.tripletsToolStripMenuItem,
            this.doubleTripletsToolStripMenuItem});
            this.snapDivisorToolStripMenuItem.Name = "snapDivisorToolStripMenuItem";
            this.snapDivisorToolStripMenuItem.ShortcutKeyDisplayString = "M";
            this.snapDivisorToolStripMenuItem.Size = new System.Drawing.Size(269, 22);
            this.snapDivisorToolStripMenuItem.Text = "Snap Di&visor";
            // 
            // fullBeatsToolStripMenuItem
            // 
            this.fullBeatsToolStripMenuItem.Name = "fullBeatsToolStripMenuItem";
            this.fullBeatsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.fullBeatsToolStripMenuItem.Text = "1/1 Full Beats";
            this.fullBeatsToolStripMenuItem.Click += new System.EventHandler(this.fullBeatsToolStripMenuItem_Click);
            // 
            // halfBeatsToolStripMenuItem
            // 
            this.halfBeatsToolStripMenuItem.Checked = true;
            this.halfBeatsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.halfBeatsToolStripMenuItem.Name = "halfBeatsToolStripMenuItem";
            this.halfBeatsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.halfBeatsToolStripMenuItem.Text = "1/2 Half Beats";
            this.halfBeatsToolStripMenuItem.Click += new System.EventHandler(this.halfBeatsToolStripMenuItem_Click);
            // 
            // quarterBeatsToolStripMenuItem
            // 
            this.quarterBeatsToolStripMenuItem.Name = "quarterBeatsToolStripMenuItem";
            this.quarterBeatsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.quarterBeatsToolStripMenuItem.Text = "1/4 Quarter Beats";
            this.quarterBeatsToolStripMenuItem.Click += new System.EventHandler(this.quarterBeatsToolStripMenuItem_Click);
            // 
            // eighthBeatsToolStripMenuItem
            // 
            this.eighthBeatsToolStripMenuItem.Name = "eighthBeatsToolStripMenuItem";
            this.eighthBeatsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.eighthBeatsToolStripMenuItem.Text = "1/8 Eighth Beats";
            this.eighthBeatsToolStripMenuItem.Click += new System.EventHandler(this.eighthBeatsToolStripMenuItem_Click);
            // 
            // tripletsToolStripMenuItem
            // 
            this.tripletsToolStripMenuItem.Name = "tripletsToolStripMenuItem";
            this.tripletsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.tripletsToolStripMenuItem.Text = "1/3 Triplets*";
            this.tripletsToolStripMenuItem.Click += new System.EventHandler(this.tripletsToolStripMenuItem_Click);
            // 
            // doubleTripletsToolStripMenuItem
            // 
            this.doubleTripletsToolStripMenuItem.Name = "doubleTripletsToolStripMenuItem";
            this.doubleTripletsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.doubleTripletsToolStripMenuItem.Text = "1/6 Double Triplets*";
            this.doubleTripletsToolStripMenuItem.Click += new System.EventHandler(this.doubleTripletsToolStripMenuItem_Click);
            // 
            // audioRateToolStripMenuItem
            // 
            this.audioRateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.audioRate25ToolStripMenuItem,
            this.audioRate50ToolStripMenuItem,
            this.audioRate75ToolStripMenuItem,
            this.audioRate100ToolStripMenuItem});
            this.audioRateToolStripMenuItem.Name = "audioRateToolStripMenuItem";
            this.audioRateToolStripMenuItem.Size = new System.Drawing.Size(269, 22);
            this.audioRateToolStripMenuItem.Text = "Audio &Rate";
            // 
            // audioRate25ToolStripMenuItem
            // 
            this.audioRate25ToolStripMenuItem.Name = "audioRate25ToolStripMenuItem";
            this.audioRate25ToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.audioRate25ToolStripMenuItem.Text = "25%";
            this.audioRate25ToolStripMenuItem.Click += new System.EventHandler(this.audioRate25ToolStripMenuItem_Click);
            // 
            // audioRate50ToolStripMenuItem
            // 
            this.audioRate50ToolStripMenuItem.Name = "audioRate50ToolStripMenuItem";
            this.audioRate50ToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.audioRate50ToolStripMenuItem.Text = "50%";
            this.audioRate50ToolStripMenuItem.Click += new System.EventHandler(this.audioRate50ToolStripMenuItem_Click);
            // 
            // audioRate75ToolStripMenuItem
            // 
            this.audioRate75ToolStripMenuItem.Name = "audioRate75ToolStripMenuItem";
            this.audioRate75ToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.audioRate75ToolStripMenuItem.Text = "75%";
            this.audioRate75ToolStripMenuItem.Click += new System.EventHandler(this.audioRate75ToolStripMenuItem_Click);
            // 
            // audioRate100ToolStripMenuItem
            // 
            this.audioRate100ToolStripMenuItem.Checked = true;
            this.audioRate100ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.audioRate100ToolStripMenuItem.Name = "audioRate100ToolStripMenuItem";
            this.audioRate100ToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.audioRate100ToolStripMenuItem.Text = "100%";
            this.audioRate100ToolStripMenuItem.Click += new System.EventHandler(this.audioRate100ToolStripMenuItem_Click);
            // 
            // beatSnappingToolStripMenuItem
            // 
            this.beatSnappingToolStripMenuItem.Name = "beatSnappingToolStripMenuItem";
            this.beatSnappingToolStripMenuItem.ShortcutKeyDisplayString = "T";
            this.beatSnappingToolStripMenuItem.Size = new System.Drawing.Size(269, 22);
            this.beatSnappingToolStripMenuItem.Text = "&Grid Snapping";
            this.beatSnappingToolStripMenuItem.Click += new System.EventHandler(this.beatSnappingToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(266, 6);
            // 
            // polygonStripMenuItem
            // 
            this.polygonStripMenuItem.Name = "polygonStripMenuItem";
            this.polygonStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+D";
            this.polygonStripMenuItem.Size = new System.Drawing.Size(269, 22);
            this.polygonStripMenuItem.Text = "Create &Polygon Circles";
            this.polygonStripMenuItem.Click += new System.EventHandler(this.polygonStripMenuItem_Click);
            // 
            // convertSliderToStreamToolStripMenuItem
            // 
            this.convertSliderToStreamToolStripMenuItem.Name = "convertSliderToStreamToolStripMenuItem";
            this.convertSliderToStreamToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+F";
            this.convertSliderToStreamToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.convertSliderToStreamToolStripMenuItem.Text = "Convert slider to &stream";
            this.convertSliderToStreamToolStripMenuItem.Click += new System.EventHandler(this.convertSliderToStreamToolStripMenuItem_Click);
            // 
            // enableLiveMappingModeToolStripMenuItem
            // 
            this.enableLiveMappingModeToolStripMenuItem.Name = "enableLiveMappingModeToolStripMenuItem";
            this.enableLiveMappingModeToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Tab";
            this.enableLiveMappingModeToolStripMenuItem.Size = new System.Drawing.Size(269, 22);
            this.enableLiveMappingModeToolStripMenuItem.Text = "Enable live mapping mode";
            this.enableLiveMappingModeToolStripMenuItem.Click += new System.EventHandler(this.enableLiveMappingModeToolStripMenuItem_Click);
            // 
            // sampleImportToolStripMenuItem
            // 
            this.sampleImportToolStripMenuItem.Name = "sampleImportToolStripMenuItem";
            this.sampleImportToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+I";
            this.sampleImportToolStripMenuItem.Size = new System.Drawing.Size(269, 22);
            this.sampleImportToolStripMenuItem.Text = "Sample import";
            this.sampleImportToolStripMenuItem.Click += new System.EventHandler(this.sampleImportToolStripMenuItem_Click);
            // 
            // autoToolStripMenuItem
            // 
            this.autoToolStripMenuItem.Name = "autoToolStripMenuItem";
            this.autoToolStripMenuItem.Size = new System.Drawing.Size(269, 22);
            this.autoToolStripMenuItem.Text = "Auto volume balance";
            this.autoToolStripMenuItem.Visible = false;
            this.autoToolStripMenuItem.Click += new System.EventHandler(this.autoToolStripMenuItem_Click);
            // 
            // designToolStripMenu
            // 
            this.designToolStripMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moveAllElementsInTimeToolStripMenuItem});
            this.designToolStripMenu.Name = "designToolStripMenu";
            this.designToolStripMenu.Size = new System.Drawing.Size(55, 20);
            this.designToolStripMenu.Text = "&Design";
            this.designToolStripMenu.DropDownOpening += new System.EventHandler(this.designToolStripMenu_DropDownOpening);
            // 
            // moveAllElementsInTimeToolStripMenuItem
            // 
            this.moveAllElementsInTimeToolStripMenuItem.Name = "moveAllElementsInTimeToolStripMenuItem";
            this.moveAllElementsInTimeToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.moveAllElementsInTimeToolStripMenuItem.Text = "Move all elements in time...";
            this.moveAllElementsInTimeToolStripMenuItem.Click += new System.EventHandler(this.moveAllElementsInTimeToolStripMenuItem_Click);
            // 
            // timingToolStripMenu
            // 
            this.timingToolStripMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.timingSignatureToolStripMenuItem,
            this.metronomeClicksToolStripMenuItem,
            this.toolStripSeparator6,
            this.addTimingToolStripMenuItem,
            this.addTimingPointToolStripMenuItem,
            this.retimeCurrentPointToolStripMenuItem,
            this.deleteTimingPointToolStripMenuItem,
            this.snapAllNotesToolStripMenuItem,
            this.toolStripSeparator8,
            this.bPMOffsetToolStripMenuItem,
            this.toolStripSeparator7,
            this.resnapAllNotesToolStripMenuItem,
            this.moveAllNotesToolStripMenuItem,
            this.recalculateSlidersToolStripMenuItem,
            this.resetAllTimingsToolStripMenuItem,
            this.toolStripSeparator10,
            this.setPreviewPointToCurrentPositionToolStripMenuItem});
            this.timingToolStripMenu.Name = "timingToolStripMenu";
            this.timingToolStripMenu.Size = new System.Drawing.Size(57, 20);
            this.timingToolStripMenu.Text = "&Timing";
            // 
            // timingSignatureToolStripMenuItem
            // 
            this.timingSignatureToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.signature44,
            this.signature34});
            this.timingSignatureToolStripMenuItem.Name = "timingSignatureToolStripMenuItem";
            this.timingSignatureToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.timingSignatureToolStripMenuItem.Text = "Time Signature";
            // 
            // signature44
            // 
            this.signature44.Name = "signature44";
            this.signature44.Size = new System.Drawing.Size(183, 22);
            this.signature44.Text = "4/4 (Common Time)";
            this.signature44.Click += new System.EventHandler(this.signature44_Click);
            // 
            // signature34
            // 
            this.signature34.Name = "signature34";
            this.signature34.Size = new System.Drawing.Size(183, 22);
            this.signature34.Text = "3/4 (Waltz/Triple)";
            this.signature34.Click += new System.EventHandler(this.signature34_Click);
            // 
            // metronomeClicksToolStripMenuItem
            // 
            this.metronomeClicksToolStripMenuItem.Checked = true;
            this.metronomeClicksToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metronomeClicksToolStripMenuItem.Name = "metronomeClicksToolStripMenuItem";
            this.metronomeClicksToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.metronomeClicksToolStripMenuItem.Text = "&Metronome Clicks";
            this.metronomeClicksToolStripMenuItem.Click += new System.EventHandler(this.metronomeClicksToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(265, 6);
            // 
            // addTimingToolStripMenuItem
            // 
            this.addTimingToolStripMenuItem.Name = "addTimingToolStripMenuItem";
            this.addTimingToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+P";
            this.addTimingToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.addTimingToolStripMenuItem.Text = "&Add Timing Section";
            this.addTimingToolStripMenuItem.Click += new System.EventHandler(this.timingAddToolStripMenuItem_Click);
            // 
            // addTimingPointToolStripMenuItem
            // 
            this.addTimingPointToolStripMenuItem.Name = "addTimingPointToolStripMenuItem";
            this.addTimingPointToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+P";
            this.addTimingPointToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.addTimingPointToolStripMenuItem.Text = "Add &Inheriting Section";
            this.addTimingPointToolStripMenuItem.Click += new System.EventHandler(this.addTimingPointToolStripMenuItem_Click);
            // 
            // retimeCurrentPointToolStripMenuItem
            // 
            this.retimeCurrentPointToolStripMenuItem.Name = "retimeCurrentPointToolStripMenuItem";
            this.retimeCurrentPointToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.retimeCurrentPointToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.retimeCurrentPointToolStripMenuItem.Text = "&Reset Current Section";
            this.retimeCurrentPointToolStripMenuItem.Click += new System.EventHandler(this.retimeCurrentPointToolStripMenuItem_Click);
            // 
            // deleteTimingPointToolStripMenuItem
            // 
            this.deleteTimingPointToolStripMenuItem.Name = "deleteTimingPointToolStripMenuItem";
            this.deleteTimingPointToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+I";
            this.deleteTimingPointToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.deleteTimingPointToolStripMenuItem.Text = "&Delete Timing Section";
            this.deleteTimingPointToolStripMenuItem.Click += new System.EventHandler(this.deleteTimingPointToolStripMenuItem_Click);
            // 
            // snapAllNotesToolStripMenuItem
            // 
            this.snapAllNotesToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.snapAllNotesToolStripMenuItem.Name = "snapAllNotesToolStripMenuItem";
            this.snapAllNotesToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.snapAllNotesToolStripMenuItem.Text = "Re&snap Current Section";
            this.snapAllNotesToolStripMenuItem.Click += new System.EventHandler(this.snapAllNotesToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(265, 6);
            // 
            // bPMOffsetToolStripMenuItem
            // 
            this.bPMOffsetToolStripMenuItem.Name = "bPMOffsetToolStripMenuItem";
            this.bPMOffsetToolStripMenuItem.ShortcutKeyDisplayString = "F6";
            this.bPMOffsetToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.bPMOffsetToolStripMenuItem.Text = "Timing &Setup...";
            this.bPMOffsetToolStripMenuItem.Click += new System.EventHandler(this.bPMOffsetToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(265, 6);
            // 
            // resnapAllNotesToolStripMenuItem
            // 
            this.resnapAllNotesToolStripMenuItem.ForeColor = System.Drawing.Color.Red;
            this.resnapAllNotesToolStripMenuItem.Name = "resnapAllNotesToolStripMenuItem";
            this.resnapAllNotesToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.resnapAllNotesToolStripMenuItem.Text = "Res&nap All Notes";
            this.resnapAllNotesToolStripMenuItem.Click += new System.EventHandler(this.resnapAllNotesToolStripMenuItem_Click);
            // 
            // moveAllNotesToolStripMenuItem
            // 
            this.moveAllNotesToolStripMenuItem.ForeColor = System.Drawing.Color.Red;
            this.moveAllNotesToolStripMenuItem.Name = "moveAllNotesToolStripMenuItem";
            this.moveAllNotesToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.moveAllNotesToolStripMenuItem.Text = "Move all notes in time...";
            this.moveAllNotesToolStripMenuItem.Click += new System.EventHandler(this.moveAllNotesToolStripMenuItem_Click);
            // 
            // recalculateSlidersToolStripMenuItem
            // 
            this.recalculateSlidersToolStripMenuItem.ForeColor = System.Drawing.Color.Red;
            this.recalculateSlidersToolStripMenuItem.Name = "recalculateSlidersToolStripMenuItem";
            this.recalculateSlidersToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.recalculateSlidersToolStripMenuItem.Text = "Re&calculate Slider Lengths";
            this.recalculateSlidersToolStripMenuItem.Click += new System.EventHandler(this.recalculateSlidersToolStripMenuItem_Click);
            // 
            // resetAllTimingsToolStripMenuItem
            // 
            this.resetAllTimingsToolStripMenuItem.ForeColor = System.Drawing.Color.Red;
            this.resetAllTimingsToolStripMenuItem.Name = "resetAllTimingsToolStripMenuItem";
            this.resetAllTimingsToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.resetAllTimingsToolStripMenuItem.Text = "Delete All Timing Sections";
            this.resetAllTimingsToolStripMenuItem.Click += new System.EventHandler(this.resetAllTimingsToolStripMenuItem_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(265, 6);
            // 
            // setPreviewPointToCurrentPositionToolStripMenuItem
            // 
            this.setPreviewPointToCurrentPositionToolStripMenuItem.Name = "setPreviewPointToCurrentPositionToolStripMenuItem";
            this.setPreviewPointToCurrentPositionToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.setPreviewPointToCurrentPositionToolStripMenuItem.Text = "Set Current Position as &Preview Point";
            this.setPreviewPointToCurrentPositionToolStripMenuItem.Click += new System.EventHandler(this.setPreviewPointToCurrentPositionToolStripMenuItem_Click);
            // 
            // webToolStripMenu
            // 
            this.webToolStripMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thisBeatmapsInformationPageToolStripMenuItem,
            this.thisBeatmapsThreadToolStripMenuItem,
            this.quickReplyToolStripMenuItem});
            this.webToolStripMenu.Name = "webToolStripMenu";
            this.webToolStripMenu.Size = new System.Drawing.Size(43, 20);
            this.webToolStripMenu.Text = "&Web";
            // 
            // thisBeatmapsInformationPageToolStripMenuItem
            // 
            this.thisBeatmapsInformationPageToolStripMenuItem.Name = "thisBeatmapsInformationPageToolStripMenuItem";
            this.thisBeatmapsInformationPageToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.thisBeatmapsInformationPageToolStripMenuItem.Text = "This Beatmap\'s &Information Page";
            this.thisBeatmapsInformationPageToolStripMenuItem.Click += new System.EventHandler(this.thisBeatmapsInformationPageToolStripMenuItem_Click);
            // 
            // thisBeatmapsThreadToolStripMenuItem
            // 
            this.thisBeatmapsThreadToolStripMenuItem.Name = "thisBeatmapsThreadToolStripMenuItem";
            this.thisBeatmapsThreadToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.thisBeatmapsThreadToolStripMenuItem.Text = "This Beatmap\'s &Thread";
            this.thisBeatmapsThreadToolStripMenuItem.Click += new System.EventHandler(this.thisBeatmapsThreadToolStripMenuItem_Click);
            // 
            // quickReplyToolStripMenuItem
            // 
            this.quickReplyToolStripMenuItem.Name = "quickReplyToolStripMenuItem";
            this.quickReplyToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.quickReplyToolStripMenuItem.Text = "Quick &Reply";
            this.quickReplyToolStripMenuItem.Click += new System.EventHandler(this.quickReplyToolStripMenuItem_Click);
            // 
            // helpToolStripMenu
            // 
            this.helpToolStripMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayInlineHlpeToolStripMenuItem,
            this.viewFAQToolStripMenuItem});
            this.helpToolStripMenu.Name = "helpToolStripMenu";
            this.helpToolStripMenu.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenu.Text = "&Help";
            // 
            // displayInlineHlpeToolStripMenuItem
            // 
            this.displayInlineHlpeToolStripMenuItem.Name = "displayInlineHlpeToolStripMenuItem";
            this.displayInlineHlpeToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.displayInlineHlpeToolStripMenuItem.Text = "Show &in-game help";
            this.displayInlineHlpeToolStripMenuItem.Click += new System.EventHandler(this.displayInlineHlpeToolStripMenuItem_Click);
            // 
            // viewFAQToolStripMenuItem
            // 
            this.viewFAQToolStripMenuItem.Name = "viewFAQToolStripMenuItem";
            this.viewFAQToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.viewFAQToolStripMenuItem.Text = "View &FAQ...";
            this.viewFAQToolStripMenuItem.Click += new System.EventHandler(this.viewFAQToolStripMenuItem_Click);
            // 
            // EditorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.menuStrip1);
            this.Name = "EditorControl";
            this.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void TranslateComponents()
        {
            //Menu tabs
            this.fileToolStripMenu.Text = LocalisationManager.GetString(OsuString.General_File);
            this.editToolStripMenu.Text = LocalisationManager.GetString(OsuString.SongSelection_Edit);
            this.viewToolStripMenu.Text = LocalisationManager.GetString(OsuString.General_View);
            this.composeToolStripMenu.Text = LocalisationManager.GetString(OsuString.Editor_Compose);
            this.designToolStripMenu.Text = LocalisationManager.GetString(OsuString.Editor_Design);
            this.timingToolStripMenu.Text = LocalisationManager.GetString(OsuString.Editor_Timing);
            this.webToolStripMenu.Text = LocalisationManager.GetString(OsuString.General_Web);
            this.helpToolStripMenu.Text = LocalisationManager.GetString(OsuString.General_Help);

            //File
            this.newToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_ClearAllNotes);
            this.openToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_OpenDifficulty);
            //Open Difficulty menu options
            this.totalLoadToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_ForEditing);
            this.asReferenceToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_ForReference);

            this.saveToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.General_Save);
            this.saveAsToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_SaveAsNewDifficulty);
            this.loadSoftToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_RevertToSaved);
            this.loadToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_RevertToSavedFull);
            this.testBeatmapToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_TestBeatmap);
            this.openAiModToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_OpenAiMod);
            this.submitToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_UploadBeatmap);
            this.packageForDistributionToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_ExportPackage);
            this.extractMapPackageToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_ExtractMapPackage);
            this.importBMSToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_ImportFrom);
            this.bmsbmeToolStripMenuItem.Text = @"bms/bme";
            this.openSongFolder.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_OpenSongFolder);
            this.openOsuInNotepad.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_OpenOsuInNotepad);
            this.openosbInNotepadToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_OpenOsbInNotepad);
            this.exitToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_File_Exit);

            //Edit
            this.menuItemUndo.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_Undo);
            this.menuItemRedo.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_Redo);
            this.cutToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_Cut);
            this.copyToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_Copy);
            this.pasteToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_Paste);
            this.deleteToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.General_Delete);
            this.selectAllToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_SelectAll);
            this.cloneToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_Clone);
            this.reverseSelectionToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_ReverseSelection);
            this.flipHorizontallyToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_FlipHorizontally);
            this.flipVerticallyToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_FlipVertically);
            this.rotateClockwiseToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_Rotate90Clockwise);
            this.rotateAnticlockwiseToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_Rotate90Anticlockwise);
            this.rotationByToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_RotateBy);
            this.scaleByToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_ScaleBy);
            this.resetSliderStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_ResetSelectedObjectsSamples);
            this.resetAllSamplesToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_ResetAllSamples);
            this.resetComboColoursToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_ResetComboColours);
            this.resetBreaksToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_ResetBreaks);
            this.nudgeBackwardToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_NudgeBackward);
            this.nudgeOneMeasureRightToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Edit_NudgeForward);

            //View
            this.composeToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.Editor_Compose);
            this.designToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.Editor_Design);
            this.timingToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.Editor_Timing);
            this.songSetupToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_View_SongSetup);
            this.timingSetupToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_View_TimingSetup);
            this.volumeToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.Options_Audio_Volume);
            this.gridLevelToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_View_GridLevel);
            //Grid level menu options
            this.mostSparseToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_View_MostSparse);
            this.gridSize2ToolStripMenuItem.Text = @"2";
            this.gridSize3ToolStripMenuItem.Text = @"3";
            this.mostPreciseToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_View_MostPrecise);

            this.showVideoToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_View_ShowVideo);
            this.showSampleNameToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_View_ShowSampleName);
            this.snakingSlidersToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.Options_Editor_Snaking_Sliders);
            this.hitAnimationsToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.Options_Editor_Hit_Animations);
            this.followPointsToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.Options_Editor_Follow_Points);
            this.disableUndoStatesToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_View_DisableUndoStates);

            //Compose
            this.snapDivisorToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_SnapDivisor);
            //Snap divisor menu options
            this.fullBeatsToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_FullBeats);
            this.halfBeatsToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_HalfBeats);
            this.quarterBeatsToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_QuarterBeats);
            this.eighthBeatsToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_EighthBeats);
            this.tripletsToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_Triplets);
            this.doubleTripletsToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_DoubleTriplets);

            this.audioRateToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_AudioRate);
            //Audio rate menu options
            this.audioRate25ToolStripMenuItem.Text = @"25%";
            this.audioRate50ToolStripMenuItem.Text = @"50%";
            this.audioRate75ToolStripMenuItem.Text = @"75%";
            this.audioRate100ToolStripMenuItem.Text = @"100%";

            this.beatSnappingToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_GridSnapping);
            this.polygonStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_CreatePolygonCircles);
            this.convertSliderToStreamToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_ConvertSliderToStream);
            this.enableLiveMappingModeToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_EnableLiveMappingMode);
            this.sampleImportToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_SampleImport);
            this.autoToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Compose_AutoVolumeBalance);

            //Design
            this.moveAllElementsInTimeToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Design_MoveAllElementsInTime);

            //Timing
            this.timingSignatureToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_TimeSignature);
            //Time signature menu options
            this.signature44.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_44Time);
            this.signature34.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_34Time);

            this.metronomeClicksToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_MetronomeClicks);
            this.addTimingToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_AddTimingSection);
            this.addTimingPointToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_AddInheritingSection);
            this.retimeCurrentPointToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_ResetCurrentSection);
            this.deleteTimingPointToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_DeleteTimingSection);
            this.snapAllNotesToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_ResnapCurrentSection);
            this.bPMOffsetToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_View_TimingSetup);
            this.resnapAllNotesToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_ResnapAllNotes);
            this.moveAllNotesToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_MoveAllNotesInTime);
            this.recalculateSlidersToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_RecalculateSliderLengths);
            this.resetAllTimingsToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_DeleteAllTimingSections);
            this.setPreviewPointToCurrentPositionToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Timing_SetCurrentPositionAsPreviewPoint);

            //Web
            this.thisBeatmapsInformationPageToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Web_ThisBeatmapsInformationPage);
            this.thisBeatmapsThreadToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Web_ThisBeatmapsThread);
            this.quickReplyToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.SongSelection_Reply);

            //Help
            this.displayInlineHlpeToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Help_ShowInGameHelp);
            this.viewFAQToolStripMenuItem.Text = LocalisationManager.GetString(OsuString.EditorMenuItem_Help_ViewFAQ);

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem composeToolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem cloneToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem displayInlineHlpeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem composeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem designToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem audioRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem audioRate25ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem audioRate50ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem audioRate75ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem audioRate100ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flipHorizontallyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flipVerticallyToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem nudgeBackwardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nudgeOneMeasureRightToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gridLevelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mostSparseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gridSize2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gridSize3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mostPreciseToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem beatSnappingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timingToolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem bPMOffsetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem snapDivisorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fullBeatsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem halfBeatsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quarterBeatsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eighthBeatsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tripletsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem addTimingPointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem retimeCurrentPointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteTimingPointToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem resetAllTimingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem snapAllNotesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem reverseSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem packageForDistributionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doubleTripletsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateClockwiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateAnticlockwiseToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem menuItemUndo;
        internal System.Windows.Forms.ToolStripMenuItem menuItemRedo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem resnapAllNotesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem setPreviewPointToCurrentPositionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recalculateSlidersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem metronomeClicksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem submitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timingSignatureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem signature44;
        private System.Windows.Forms.ToolStripMenuItem signature34;
        private System.Windows.Forms.ToolStripMenuItem testBeatmapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem songSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem viewFAQToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem webToolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem thisBeatmapsThreadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickReplyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thisBeatmapsInformationPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openSongFolder;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem openOsuInNotepad;
        private System.Windows.Forms.ToolStripMenuItem resetSliderStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotationByToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addTimingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timingSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem resetAllSamplesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openAiModToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem loadSoftToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem designToolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem moveAllElementsInTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem extractMapPackageToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripMenuItem resetComboColoursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetBreaksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scaleByToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openosbInNotepadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showVideoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem polygonStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripMenuItem convertSliderToStreamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableLiveMappingModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sampleImportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveAllNotesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importBMSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bmsbmeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem totalLoadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asReferenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showSampleNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem volumeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disableUndoStatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem snakingSlidersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hitAnimationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem followPointsToolStripMenuItem;
    }
}