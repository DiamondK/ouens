﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using osu.Audio;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects.Osu;
using osu_common.Helpers;
using osu.GameModes.Edit.Modes;
using System.IO;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.Events;
using System.Drawing;
using System.Collections.Generic;
using osu.Helpers;
using osu_common;
using osu.Configuration;
using osu.Graphics.Notifications;

namespace osu.GameModes.Edit.Forms
{
    internal partial class EditorControl : UserControl
    {
        private Editor editor;

        internal EditorControl()
        {
            InitializeComponent();
            TranslateComponents();
        }

        internal void Dispose()
        {
            UnregisterEditor();
            Parent = null;

            base.Dispose();
        }

        internal void LoadMenu()
        {
            Beatmap b = editor.hitObjectManager.Beatmap;

            thisBeatmapsInformationPageToolStripMenuItem.Enabled = b.BeatmapId != 0;
            thisBeatmapsThreadToolStripMenuItem.Enabled = b.BeatmapId != 0;
            quickReplyToolStripMenuItem.Enabled = b.BeatmapId != 0;

            packageForDistributionToolStripMenuItem.Enabled = !b.InOszContainer;

            openSongFolder.Enabled = !b.InOszContainer || b.ExtractionFolder != null;

            submitToolStripMenuItem.Enabled = !b.InOszContainer;
            extractMapPackageToolStripMenuItem.Enabled = b.InOszContainer;
            openosbInNotepadToolStripMenuItem.Enabled = b.CheckFileExists(b.StoryboardFilename);

            showVideoToolStripMenuItem.Checked = ConfigManager.sEditorVideo;
            snakingSlidersToolStripMenuItem.Checked = ConfigManager.sEditorSnakingSliders;
            hitAnimationsToolStripMenuItem.Checked = ConfigManager.sEditorHitAnimations;
            followPointsToolStripMenuItem.Checked = ConfigManager.sEditorFollowPoints;
            disableUndoStatesToolStripMenuItem.Checked = ConfigManager.sFastEditor;

            if (b.PlayMode == osu_common.PlayModes.OsuMania)
            {
                autoToolStripMenuItem.Visible = true;
            }
            SpecialChanged();

            if (EditorModeCompose.SoundAdditionStatus != null) beatSnappingToolStripMenuItem.Checked = EditorModeCompose.SoundAdditionStatus[SoundAdditions.GridSnap];
        }

        private void SpecialChanged()
        {
            //    Beatmap b = editor.hitObjectManager.Beatmap;
            //    leftColumnToolStripMenuItem.Checked = b.SpecialLeft;
            //    rightLastToolStripMenuItem.Checked = !b.SpecialLeft;
        }

        private void menuStrip1_MenuActivate(object sender, EventArgs e)
        {
            GameBase.MenuActive = true;
            enableLiveMappingModeToolStripMenuItem.Checked = editor.liveMapping;
        }

        private void menuStrip1_MenuDeactivate(object sender, EventArgs e)
        {
            GameBase.MenuActive = false;
        }

        internal void MenuChanged(EditorModes menu)
        {
            composeToolStripMenuItem.Checked = menu == EditorModes.Compose;
            designToolStripMenuItem.Checked = menu == EditorModes.Design;
            timingToolStripMenuItem.Checked = menu == EditorModes.Timing;
        }

        internal void AudioRateChanged(int rate)
        {
            audioRate25ToolStripMenuItem.Checked = rate == 25;
            audioRate50ToolStripMenuItem.Checked = rate == 50;
            audioRate75ToolStripMenuItem.Checked = rate == 75;
            audioRate100ToolStripMenuItem.Checked = rate == 100;
        }

        internal void GridSizeChanged(Editor.GridSizes size)
        {
            mostSparseToolStripMenuItem.Checked = size == Editor.GridSizes.Large;
            gridSize2ToolStripMenuItem.Checked = size == Editor.GridSizes.Medium;
            gridSize3ToolStripMenuItem.Checked = size == Editor.GridSizes.Small;
            mostPreciseToolStripMenuItem.Checked = size == Editor.GridSizes.Tiny;
        }

        internal void BeatSnapChanged(bool status)
        {
            beatSnappingToolStripMenuItem.Checked = status;
        }

        internal void GridSnapChanged(bool value)
        {
            beatSnappingToolStripMenuItem.Checked = value;
        }

        internal void RegisterEditor(Editor e)
        {
            editor = e;
        }

        internal void UnregisterEditor()
        {
            editor = null;
        }

        private void WithBeatmapReload(VoidDelegate action)
        {
            DialogResult r;
            if (editor.Dirty)
            {
                r = MessageBox.Show(LocalisationManager.GetString(OsuString.EditorControl_BeatmapMustBeSavedDialog), @"osu!",
                                                 MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation,
                                                 MessageBoxDefaultButton.Button1);

                if (r == DialogResult.Yes)
                {
                    editor.SaveFile();
                }
                else if (r == DialogResult.Cancel)
                {
                    return;
                }
            }
            else
                r = DialogResult.Yes;

            action();

            editor.LoadFile(editor.hitObjectManager.Beatmap, r == DialogResult.No, true);
        }

        #region File

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Clear();
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.LoadFile(editor.hitObjectManager.Beatmap, true, true);
        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.SaveFile();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Exit();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.SaveAs();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //editor.ShowDifficultySwitch();
        }

        private void packageForDistributionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.PackageOsz();
        }

        private void submitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Submit();
        }

        private void testBeatmapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.TestMode();
        }

        private void openSongFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Beatmap b = editor.hitObjectManager.Beatmap;
            GameBase.OpenFolder(b.InOszContainer ? b.ExtractionFolder : b.ContainingFolder);
        }

        private void openNotepadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Beatmap b = editor.hitObjectManager.Beatmap;
            if (!b.InOszContainer)
            {
                GameBase.ProcessStart(@"notepad.exe", b.FilenameFull);
                return;
            }

            string path = b.Osz2ExtractSingleFile(b.Filename, false);
            if (path != null)
                GameBase.ProcessStart(@"notepad.exe", path);
        }

        private void openAiModToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.AiModOpen();
        }

        private void loadSoftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.LoadFile(editor.hitObjectManager.Beatmap, true, false);
        }

        private void extractMapPackageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Beatmap b = editor.hitObjectManager.Beatmap;
            if (!b.InOszContainer)
            {
                MessageBox.Show(LocalisationManager.GetString(OsuString.EditorControl_BeatmapAlreadyExtracted));
                return;
            }

            if (!b.AllowExtraction)
            {
                MessageBox.Show(LocalisationManager.GetString(OsuString.EditorControl_BeatmapProtectedFromExtraction));
                return;
            }

            /*FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            folderDialog.RootFolder = System.Environment.SpecialFolder.MyDocuments;
            DialogResult result = folderDialog.ShowDialog();

            if (result != DialogResult.OK)
                return;*/

            if (!b.Osz2ExtractSafe(Beatmap.CopyColissionMode.Ask, false))
            {
                MessageBox.Show(LocalisationManager.GetString(OsuString.EditorControl_ExtractionFailed));
                return;
            }

            //open explorer to extracted folder
            string explorerEXE = String.Format(@"{0}\explorer.exe", Environment.GetEnvironmentVariable(@"windir"));
            try
            {
                ProcessStartInfo info = new ProcessStartInfo(explorerEXE);
                info.Arguments = "/n," + b.ExtractionFolder;
                Process.Start(info);
            }
            catch
            {
            }
        }

        private void openosbInNotepadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Beatmap b = editor.hitObjectManager.Beatmap;

            if (!b.CheckFileExists(b.StoryboardFilename))
                return;

            if (!b.InOszContainer)
            {
                GameBase.ProcessStart(@"notepad.exe", (b.ContainingFolder + @"\")
                                      + b.StoryboardFilename);
                return;
            }

            string path = b.Osz2ExtractSingleFile(b.StoryboardFilename, false);
            if (path != null)
                GameBase.ProcessStart(@"notepad.exe", path);
        }

        private void bmsbmeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (editor.hitObjectManager.Beatmap.PlayMode != osu_common.PlayModes.OsuMania)
            {
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.EditorControl_OnlyAvailableForMania), 2000);
                return;
            }
            GameBase.MenuActive = true;
            ImportBMS m = new ImportBMS();
            m.ShowDialog();
            GameBase.MenuActive = false;
        }

        private void forEditingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ShowDifficultySwitch();
        }

        private void asReferenceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ShowDifficultySwitch(true);
        }

        private void fileToolStripMenu_DropDownOpening(object sender, EventArgs e)
        {
            asReferenceToolStripMenuItem.Enabled = BeatmapManager.Current.PlayMode == PlayModes.OsuMania;
        }

        #endregion
        #region Edit

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.CutSelection();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.CopySelection();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.PasteSelection();
        }

        private void cloneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.CloneSelection();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.DeleteSelection();
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.SelectAll();
        }

        private void flipHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.FlipSelectionHorizontal();
        }

        private void flipVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.FlipSelectionVertical();
        }

        private void reverseSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.ReverseSelection();
        }

        private void rotateClockwiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.RotateSelectionClockwise();
        }

        private void rotateAnticlockwiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.RotateSelectionAnticlockwise();
        }

        private void rotationByToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ShowRotateBy();
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Undo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Redo();
        }

        private void nudgeBackwardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.NudgeLeft();
        }

        private void nudgeOneMeasureRightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.NudgeRight();
        }

        private void resetSliderStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.ResetSelectedObjectSamples();
        }

        private void resetAllSamplesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK ==
                MessageBox.Show(
                    LocalisationManager.GetString(OsuString.EditorControl_ResetSamplesWarning),
                    @"osu!", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation))
                editor.Compose.ResetAllSamples();
        }

        private void resetComboColoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.UndoPush();
            foreach (HitObject h in editor.hitObjectManager.hitObjects)
                h.ComboOffset = 0;
            editor.hitObjectManager.Sort(false);
        }

        private void resetBreaksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.UndoPush();
            foreach (EventBreak b in editor.eventManager.eventBreaks)
            {
                b.CustomStart = false;
                b.CustomEnd = false;
            }
            editor.BreakTimeFix(true, false);
        }

        private void scaleByToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ShowScaleBy();
        }

        private void editToolStripMenu_DropDownOpening(object sender, EventArgs e)
        {
            bool enableItems;

            if (Editor.Instance.Compose.selectedObjects.Count == 0 || Editor.Instance.CurrentMode != EditorModes.Compose)
            {
                enableItems = false;
            }
            else
            {
                enableItems = true;
            }

            reverseSelectionToolStripMenuItem.Enabled = enableItems;
            flipHorizontallyToolStripMenuItem.Enabled = enableItems;
            flipVerticallyToolStripMenuItem.Enabled = enableItems;
            rotateClockwiseToolStripMenuItem.Enabled = enableItems;
            rotateAnticlockwiseToolStripMenuItem.Enabled = enableItems;
            rotationByToolStripMenuItem.Enabled = enableItems;
            scaleByToolStripMenuItem.Enabled = enableItems;

            if (enableItems)
            {
                enableItems = Editor.Instance.CurrentMode == EditorModes.Compose;
            }

            cloneToolStripMenuItem.Enabled = enableItems;
            nudgeBackwardToolStripMenuItem.Enabled = enableItems;
            nudgeOneMeasureRightToolStripMenuItem.Enabled = enableItems;
        }

        #endregion
        #region View

        private void composeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ChangeMode(EditorModes.Compose);
        }

        private void designToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ChangeMode(EditorModes.Design);
        }

        private void timingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ChangeMode(EditorModes.Timing);
        }

        private void mostSparseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ChangeGridSize(Editor.GridSizes.Large);
        }

        private void gridMediumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ChangeGridSize(Editor.GridSizes.Medium);
        }

        private void gridSmallToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ChangeGridSize(Editor.GridSizes.Small);
        }

        private void mostPreciseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ChangeGridSize(Editor.GridSizes.Tiny);
        }

        private void songSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ShowSongSetup();
        }

        private void showVideoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WithBeatmapReload(delegate
            {
                showVideoToolStripMenuItem.Checked = !showVideoToolStripMenuItem.Checked;
                ConfigManager.sEditorVideo.Value = showVideoToolStripMenuItem.Checked;
            });
        }

        private void showSampleNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showSampleNameToolStripMenuItem.Checked = !showSampleNameToolStripMenuItem.Checked;
            editor.showSampleName = showSampleNameToolStripMenuItem.Checked;
        }

        private void snakingSlidersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            snakingSlidersToolStripMenuItem.Checked = !snakingSlidersToolStripMenuItem.Checked;
            ConfigManager.sEditorSnakingSliders.Value = snakingSlidersToolStripMenuItem.Checked;
        }

        private void hitAnimationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WithBeatmapReload(delegate
            {
                hitAnimationsToolStripMenuItem.Checked = !hitAnimationsToolStripMenuItem.Checked;
                ConfigManager.sEditorHitAnimations.Value = hitAnimationsToolStripMenuItem.Checked;
            });
        }

        private void followPointsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            followPointsToolStripMenuItem.Checked = !followPointsToolStripMenuItem.Checked;
            ConfigManager.sEditorFollowPoints.Value = followPointsToolStripMenuItem.Checked;
        }

        private void volumeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GameBase.MenuActive = true;
            VolumeDialog m = new VolumeDialog();
            m.Show(GameBase.Form);
            GameBase.MenuActive = false;
        }

        private void disableUndoStatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            disableUndoStatesToolStripMenuItem.Checked = !disableUndoStatesToolStripMenuItem.Checked;
            ConfigManager.sFastEditor.Value = disableUndoStatesToolStripMenuItem.Checked;

            if (ConfigManager.sFastEditor)
                Editor.Instance.HistoryClear();
        }

        #endregion
        #region Compose

        private void audioRate25ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ChangeAudioRate(25);
        }

        private void audioRate50ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ChangeAudioRate(50);
        }

        private void audioRate75ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ChangeAudioRate(75);
        }

        private void audioRate100ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ChangeAudioRate(100);
        }

        private void beatSnappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.ChangeSoundAddition(SoundAdditions.GridSnap);
        }

        private void fullBeatsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.ChangeSnapDivisor(1);
        }

        private void halfBeatsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.ChangeSnapDivisor(2);
        }

        private void quarterBeatsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.ChangeSnapDivisor(4);
        }

        private void eighthBeatsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.ChangeSnapDivisor(8);
        }

        private void doubleEighthBeatsToolStrip_Click(object sender, EventArgs e)
        {
            editor.Compose.ChangeSnapDivisor(16);
        }

        private void tripletsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.ChangeSnapDivisor(3);
        }

        private void doubleTripletsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.ChangeSnapDivisor(6);
        }

        private void quadrupleTripletstoolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.ChangeSnapDivisor(12);
        }

        internal void BeatSnapDivisorChanged(int div)
        {
            fullBeatsToolStripMenuItem.Checked = div == 1;
            halfBeatsToolStripMenuItem.Checked = div == 2;
            quarterBeatsToolStripMenuItem.Checked = div == 4;
            eighthBeatsToolStripMenuItem.Checked = div == 8;
            tripletsToolStripMenuItem.Checked = div == 3;
            doubleTripletsToolStripMenuItem.Checked = div == 6;
            //    quadrupleTripletstoolStripMenuItem.Checked = div == 12;
            //    doubleEighthBeatsToolStrip.Checked = div == 16;
        }

        private void polygonStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ShowPolygon();
        }

        private void convertSliderToStreamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ShowConvertToStream();
        }

        private void enableLiveMappingModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.liveMapping = !editor.liveMapping;
            enableLiveMappingModeToolStripMenuItem.Checked = editor.liveMapping;
            NotificationManager.ShowMessageMassive(editor.liveMapping ? LocalisationManager.GetString(OsuString.EditorModeCompose_LiveMappingEnabled) : LocalisationManager.GetString(OsuString.EditorModeCompose_LiveMappingDisabled), 3000);
        }

        private void sampleImportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ShowSampleImport();
        }

        private void autoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Compose.autoVolumeBalance();
        }

        private void composeToolStripMenu_DropDownOpening(object sender, EventArgs e)
        {
            if (Editor.Instance.CurrentMode != EditorModes.Compose)
            {
                polygonStripMenuItem.Enabled = false;
                convertSliderToStreamToolStripMenuItem.Enabled = false;
            }
            else
            {
                polygonStripMenuItem.Enabled = true;
                if (Editor.Instance.Compose.selectedSlider != null)
                    convertSliderToStreamToolStripMenuItem.Enabled = true;
                else
                    convertSliderToStreamToolStripMenuItem.Enabled = false;
            }
        }

        #endregion
        #region Design

        private void moveAllElementsInTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GameBase.MenuActive = true;
            MoveEvents m = new MoveEvents();
            m.ShowDialog();
            GameBase.MenuActive = false;
        }

        private void designToolStripMenu_DropDownOpening(object sender, EventArgs e)
        {
            if (Editor.Instance.CurrentMode != EditorModes.Design)
            {
                moveAllElementsInTimeToolStripMenuItem.Enabled = false;
            }
            else
            {
                moveAllElementsInTimeToolStripMenuItem.Enabled = true;
            }
        }

        #endregion
        #region Timing

        private void resetAllTimingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes ==
                MessageBox.Show(LocalisationManager.GetString(OsuString.EditorControl_ResetTimingsWarning), @"osu!", MessageBoxButtons.YesNo,
                                MessageBoxIcon.Exclamation))
                GameBase.Scheduler.Add(editor.Timing.TimingResetAll);
        }

        private void addTimingPointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GameBase.Scheduler.Add(delegate { editor.Timing.TimingAdd(false); });
        }

        private void retimeCurrentPointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes ==
                MessageBox.Show(LocalisationManager.GetString(OsuString.EditorControl_ResetCurrentTimingWarning), @"osu!",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
                GameBase.Scheduler.Add(editor.Timing.TimingReset);
        }

        private void deleteTimingPointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GameBase.Scheduler.Add(editor.Timing.TimingDelete);
        }

        private void snapAllNotesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (AudioEngine.beatLength > 0)
            {
                if (DialogResult.Yes ==
                    MessageBox.Show(LocalisationManager.GetString(OsuString.EditorControl_SnapNotesCurrentTimingWarning), @"osu!",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
                    editor.Timing.beatSnapSong(true);
            }
            else
                MessageBox.Show(LocalisationManager.GetString(OsuString.EditorControl_OneSectionRequiredWarning), @"osu!", MessageBoxButtons.OK);
        }

        private void bPMOffsetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GameBase.Scheduler.Add(delegate() { editor.Timing.ShowTimingWindow(TimingFocus.BPM); });
        }

        private void resnapAllNotesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes ==
                MessageBox.Show(LocalisationManager.GetString(OsuString.EditorControl_SnapAllNotesWarning), @"osu!",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
                editor.Timing.beatSnapSong();
        }

        private void setPreviewPointToCurrentPositionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (AudioEngine.Time >= 0 && AudioEngine.Time != editor.hitObjectManager.Beatmap.PreviewTime)
            {
                editor.UndoPush();
                editor.hitObjectManager.Beatmap.PreviewTime = AudioEngine.Time;
            }

        }

        private void recalculateSlidersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK ==
                MessageBox.Show(
                    LocalisationManager.GetString(OsuString.EditorControl_RecalculateSlidersMessage),
                    @"osu!", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation))
                editor.Timing.RecalculateSliderLengths();
        }

        private void metronomeClicksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            metronomeClicksToolStripMenuItem.Checked = !metronomeClicksToolStripMenuItem.Checked;
            editor.Timing.MetronomeEnabled = metronomeClicksToolStripMenuItem.Checked;
        }

        private void signature44_Click(object sender, EventArgs e)
        {
            editor.Timing.ChangeTimeSignature(TimeSignatures.SimpleQuadruple);
        }

        private void signature34_Click(object sender, EventArgs e)
        {
            editor.Timing.ChangeTimeSignature(TimeSignatures.SimpleTriple);
        }

        internal void TimeSignatureChanged(TimeSignatures ts)
        {
            signature34.Checked = ts == TimeSignatures.SimpleTriple;
            signature44.Checked = ts == TimeSignatures.SimpleQuadruple;
        }

        private void timingAddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Timing.TimingAdd(true);
        }

        private void timingSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Timing.ShowTimingWindow();
        }

        private void moveAllNotesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GameBase.MenuActive = true;
            MoveNotes m = new MoveNotes();
            m.ShowDialog();
            GameBase.MenuActive = false;
        }

        #endregion
        #region Web

        private void thisBeatmapsInformationPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.hitObjectManager.Beatmap.LoadUrlListing();
        }

        private void thisBeatmapsThreadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.hitObjectManager.Beatmap.LoadUrlTopic();
        }

        private void quickReplyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.hitObjectManager.Beatmap.LoadUrlReply();
        }

        #endregion
        #region Help

        private void displayInlineHlpeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.ToggleHelp();
        }

        private void viewFAQToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GameBase.ProcessStart(Urls.HELP_FAQ);
        }

        #endregion

        //Unused
        private void showSongSelect_Click(object sender, EventArgs e)
        {
            GameBase.MenuActive = true;
            SongSetup m = new SongSetup();
            m.ShowDialog();
            GameBase.MenuActive = false;
            editor.hitObjectManager.UpdateVariables();
        }
    }
}