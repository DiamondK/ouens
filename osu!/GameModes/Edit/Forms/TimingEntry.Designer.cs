﻿namespace osu.GameModes.Edit.Forms
{
    partial class TimingEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.chkAdjustBookmarkAndPreview = new System.Windows.Forms.CheckBox();
            this.nudCustom = new System.Windows.Forms.NumericUpDown();
            this.buttonOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tabsLeft = new System.Windows.Forms.TabControl();
            this.tabTiming = new System.Windows.Forms.TabPage();
            this.grpTimingSignature = new System.Windows.Forms.GroupBox();
            this.lblCustomSignature = new System.Windows.Forms.Label();
            this.udMetronome = new System.Windows.Forms.NumericUpDown();
            this.radCustomSignature = new System.Windows.Forms.RadioButton();
            this.radTimingQuad = new System.Windows.Forms.RadioButton();
            this.radTimingTriple = new System.Windows.Forms.RadioButton();
            this.grpTimingGeneral = new System.Windows.Forms.GroupBox();
            this.udTimingOffset = new System.Windows.Forms.NumericUpDown();
            this.udBpm = new System.Windows.Forms.NumericUpDown();
            this.btnTimingUseCurrent = new System.Windows.Forms.Button();
            this.lblTimingOffset = new System.Windows.Forms.Label();
            this.lblBpm = new System.Windows.Forms.Label();
            this.tabGameplay = new System.Windows.Forms.TabPage();
            this.grpDifficulty = new System.Windows.Forms.GroupBox();
            this.udSliderMultiplier = new System.Windows.Forms.NumericUpDown();
            this.radSliderCustom = new System.Windows.Forms.RadioButton();
            this.radSlider150 = new System.Windows.Forms.RadioButton();
            this.radSlider100 = new System.Windows.Forms.RadioButton();
            this.radSlider75 = new System.Windows.Forms.RadioButton();
            this.lblSliderMultiplier = new System.Windows.Forms.Label();
            this.grpGameplayGeneral = new System.Windows.Forms.GroupBox();
            this.udGameplayOffset = new System.Windows.Forms.NumericUpDown();
            this.btnGameplayUseCurrent = new System.Windows.Forms.Button();
            this.lblGameplayOffset = new System.Windows.Forms.Label();
            this.tabBulk = new System.Windows.Forms.TabPage();
            this.grpBulkDifficulty = new System.Windows.Forms.GroupBox();
            this.udBulkSliderMultiplier = new System.Windows.Forms.NumericUpDown();
            this.radBulkSliderCustom = new System.Windows.Forms.RadioButton();
            this.radBulkSlider150 = new System.Windows.Forms.RadioButton();
            this.radBulkSlider100 = new System.Windows.Forms.RadioButton();
            this.radBulkSlider75 = new System.Windows.Forms.RadioButton();
            this.grpBulkTimeSignature = new System.Windows.Forms.GroupBox();
            this.lblBulkCustomSignature = new System.Windows.Forms.Label();
            this.udBulkMetronome = new System.Windows.Forms.NumericUpDown();
            this.radBulkCustomSignature = new System.Windows.Forms.RadioButton();
            this.radBulkTimingQuad = new System.Windows.Forms.RadioButton();
            this.radBulkTimingTriple = new System.Windows.Forms.RadioButton();
            this.grpBulkGeneral = new System.Windows.Forms.GroupBox();
            this.udBulkMove = new System.Windows.Forms.NumericUpDown();
            this.btnBulkMove = new System.Windows.Forms.Button();
            this.lblBulkMove = new System.Windows.Forms.Label();
            this.tabAudio = new System.Windows.Forms.TabPage();
            this.grpVolume = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lblVolume = new System.Windows.Forms.Label();
            this.udVolume = new System.Windows.Forms.NumericUpDown();
            this.tbVolume = new System.Windows.Forms.TrackBar();
            this.grpSampleset = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.listSampleset = new System.Windows.Forms.ListBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.radSamplesetCustom2 = new System.Windows.Forms.RadioButton();
            this.radSamplesetCustom1 = new System.Windows.Forms.RadioButton();
            this.radSamplesetDefault = new System.Windows.Forms.RadioButton();
            this.tabStyle = new System.Windows.Forms.TabPage();
            this.grpEffects = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelOmitBarline = new System.Windows.Forms.Label();
            this.chkOmitBarline = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblKiai = new System.Windows.Forms.Label();
            this.chkKiai = new System.Windows.Forms.CheckBox();
            this.tabsRight = new System.Windows.Forms.TabControl();
            this.tabAll = new System.Windows.Forms.TabPage();
            this.tabReds = new System.Windows.Forms.TabPage();
            this.tabGreens = new System.Windows.Forms.TabPage();
            this.pnlTimingPoints = new System.Windows.Forms.Panel();
            this.listTimingPoints = new osu.Helpers.Forms.pListView();
            this.colOffset = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBpm = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSlider = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSampleset = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colVolume = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colKiai = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addControlPointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.grpTimingScale = new System.Windows.Forms.GroupBox();
            this.udBeatSnap = new System.Windows.Forms.DomainUpDown();
            this.lblBeatSnap = new System.Windows.Forms.Label();
            this.chkResnapSliders = new System.Windows.Forms.CheckBox();
            this.chkSnapObjects = new System.Windows.Forms.CheckBox();
            this.chkScaleObjects = new System.Windows.Forms.CheckBox();
            this.chkInherit = new System.Windows.Forms.CheckBox();
            this.pnlToolStrip = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolAdd = new System.Windows.Forms.ToolStripButton();
            this.toolRemove = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.nudCustom)).BeginInit();
            this.tabsLeft.SuspendLayout();
            this.tabTiming.SuspendLayout();
            this.grpTimingSignature.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udMetronome)).BeginInit();
            this.grpTimingGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udTimingOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udBpm)).BeginInit();
            this.tabGameplay.SuspendLayout();
            this.grpDifficulty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udSliderMultiplier)).BeginInit();
            this.grpGameplayGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udGameplayOffset)).BeginInit();
            this.tabBulk.SuspendLayout();
            this.grpBulkDifficulty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udBulkSliderMultiplier)).BeginInit();
            this.grpBulkTimeSignature.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udBulkMetronome)).BeginInit();
            this.grpBulkGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udBulkMove)).BeginInit();
            this.tabAudio.SuspendLayout();
            this.grpVolume.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbVolume)).BeginInit();
            this.grpSampleset.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabStyle.SuspendLayout();
            this.grpEffects.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabsRight.SuspendLayout();
            this.pnlTimingPoints.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.grpTimingScale.SuspendLayout();
            this.pnlToolStrip.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 300;
            this.toolTip1.AutoPopDelay = 10000;
            this.toolTip1.InitialDelay = 300;
            this.toolTip1.ReshowDelay = 60;
            // 
            // chkAdjustBookmarkAndPreview
            // 
            this.chkAdjustBookmarkAndPreview.AutoSize = true;
            this.chkAdjustBookmarkAndPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkAdjustBookmarkAndPreview.Location = new System.Drawing.Point(191, 43);
            this.chkAdjustBookmarkAndPreview.Name = "chkAdjustBookmarkAndPreview";
            this.chkAdjustBookmarkAndPreview.Size = new System.Drawing.Size(228, 20);
            this.chkAdjustBookmarkAndPreview.TabIndex = 5;
            this.chkAdjustBookmarkAndPreview.Text = "Adjust bookmarks and audio preview";
            this.toolTip1.SetToolTip(this.chkAdjustBookmarkAndPreview, "Will be adjusted based on the offset change of the first control point only.");
            this.chkAdjustBookmarkAndPreview.UseVisualStyleBackColor = true;
            // 
            // nudCustom
            // 
            this.nudCustom.Location = new System.Drawing.Point(133, 64);
            this.nudCustom.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudCustom.Name = "nudCustom";
            this.nudCustom.Size = new System.Drawing.Size(54, 20);
            this.nudCustom.TabIndex = 6;
            this.toolTip1.SetToolTip(this.nudCustom, "Custom sample set number");
            this.nudCustom.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudCustom.ValueChanged += new System.EventHandler(this.nudCustom_ValueChanged);
            // 
            // buttonOk
            // 
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonOk.Location = new System.Drawing.Point(4, 383);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(397, 35);
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Location = new System.Drawing.Point(407, 383);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(171, 35);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tabsLeft
            // 
            this.tabsLeft.Controls.Add(this.tabTiming);
            this.tabsLeft.Controls.Add(this.tabGameplay);
            this.tabsLeft.Controls.Add(this.tabBulk);
            this.tabsLeft.Controls.Add(this.tabAudio);
            this.tabsLeft.Controls.Add(this.tabStyle);
            this.tabsLeft.Location = new System.Drawing.Point(4, 4);
            this.tabsLeft.Name = "tabsLeft";
            this.tabsLeft.SelectedIndex = 0;
            this.tabsLeft.Size = new System.Drawing.Size(238, 297);
            this.tabsLeft.TabIndex = 10;
            // 
            // tabTiming
            // 
            this.tabTiming.Controls.Add(this.grpTimingSignature);
            this.tabTiming.Controls.Add(this.grpTimingGeneral);
            this.tabTiming.Location = new System.Drawing.Point(4, 22);
            this.tabTiming.Name = "tabTiming";
            this.tabTiming.Padding = new System.Windows.Forms.Padding(3);
            this.tabTiming.Size = new System.Drawing.Size(230, 271);
            this.tabTiming.TabIndex = 0;
            this.tabTiming.Text = "Timing";
            // 
            // grpTimingSignature
            // 
            this.grpTimingSignature.Controls.Add(this.lblCustomSignature);
            this.grpTimingSignature.Controls.Add(this.udMetronome);
            this.grpTimingSignature.Controls.Add(this.radCustomSignature);
            this.grpTimingSignature.Controls.Add(this.radTimingQuad);
            this.grpTimingSignature.Controls.Add(this.radTimingTriple);
            this.grpTimingSignature.Location = new System.Drawing.Point(6, 141);
            this.grpTimingSignature.Name = "grpTimingSignature";
            this.grpTimingSignature.Size = new System.Drawing.Size(218, 124);
            this.grpTimingSignature.TabIndex = 1;
            this.grpTimingSignature.TabStop = false;
            this.grpTimingSignature.Text = "Time Signature (metronome)";
            // 
            // lblCustomSignature
            // 
            this.lblCustomSignature.AutoSize = true;
            this.lblCustomSignature.Location = new System.Drawing.Point(123, 75);
            this.lblCustomSignature.Name = "lblCustomSignature";
            this.lblCustomSignature.Size = new System.Drawing.Size(21, 15);
            this.lblCustomSignature.TabIndex = 4;
            this.lblCustomSignature.Text = "/ 4";
            // 
            // udMetronome
            // 
            this.udMetronome.Location = new System.Drawing.Point(73, 73);
            this.udMetronome.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.udMetronome.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.udMetronome.Name = "udMetronome";
            this.udMetronome.Size = new System.Drawing.Size(44, 20);
            this.udMetronome.TabIndex = 3;
            this.udMetronome.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.udMetronome.ValueChanged += new System.EventHandler(this.udMetronome_ValueChanged);
            // 
            // radCustomSignature
            // 
            this.radCustomSignature.AutoSize = true;
            this.radCustomSignature.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radCustomSignature.Location = new System.Drawing.Point(9, 73);
            this.radCustomSignature.Name = "radCustomSignature";
            this.radCustomSignature.Size = new System.Drawing.Size(64, 20);
            this.radCustomSignature.TabIndex = 2;
            this.radCustomSignature.TabStop = true;
            this.radCustomSignature.Text = "Other:";
            this.radCustomSignature.UseVisualStyleBackColor = true;
            this.radCustomSignature.CheckedChanged += new System.EventHandler(this.radCustomSignature_CheckedChanged);
            // 
            // radTimingQuad
            // 
            this.radTimingQuad.AutoSize = true;
            this.radTimingQuad.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radTimingQuad.Location = new System.Drawing.Point(9, 52);
            this.radTimingQuad.Name = "radTimingQuad";
            this.radTimingQuad.Size = new System.Drawing.Size(114, 20);
            this.radTimingQuad.TabIndex = 1;
            this.radTimingQuad.TabStop = true;
            this.radTimingQuad.Text = "4 / 4 (common)";
            this.radTimingQuad.UseVisualStyleBackColor = true;
            this.radTimingQuad.CheckedChanged += new System.EventHandler(this.radTimingQuad_CheckedChanged);
            // 
            // radTimingTriple
            // 
            this.radTimingTriple.AutoSize = true;
            this.radTimingTriple.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radTimingTriple.Location = new System.Drawing.Point(9, 31);
            this.radTimingTriple.Name = "radTimingTriple";
            this.radTimingTriple.Size = new System.Drawing.Size(92, 20);
            this.radTimingTriple.TabIndex = 0;
            this.radTimingTriple.TabStop = true;
            this.radTimingTriple.Text = "3 / 4 (waltz)";
            this.radTimingTriple.UseVisualStyleBackColor = true;
            this.radTimingTriple.CheckedChanged += new System.EventHandler(this.radTimingTriple_CheckedChanged);
            // 
            // grpTimingGeneral
            // 
            this.grpTimingGeneral.Controls.Add(this.udTimingOffset);
            this.grpTimingGeneral.Controls.Add(this.udBpm);
            this.grpTimingGeneral.Controls.Add(this.btnTimingUseCurrent);
            this.grpTimingGeneral.Controls.Add(this.lblTimingOffset);
            this.grpTimingGeneral.Controls.Add(this.lblBpm);
            this.grpTimingGeneral.Location = new System.Drawing.Point(6, 6);
            this.grpTimingGeneral.Name = "grpTimingGeneral";
            this.grpTimingGeneral.Size = new System.Drawing.Size(218, 129);
            this.grpTimingGeneral.TabIndex = 0;
            this.grpTimingGeneral.TabStop = false;
            this.grpTimingGeneral.Text = "General";
            // 
            // udTimingOffset
            // 
            this.udTimingOffset.Location = new System.Drawing.Point(65, 47);
            this.udTimingOffset.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.udTimingOffset.Minimum = new decimal(new int[] {
            100000000,
            0,
            0,
            -2147483648});
            this.udTimingOffset.Name = "udTimingOffset";
            this.udTimingOffset.Size = new System.Drawing.Size(147, 20);
            this.udTimingOffset.TabIndex = 6;
            this.udTimingOffset.ValueChanged += new System.EventHandler(this.udTimingOffset_ValueChanged);
            // 
            // udBpm
            // 
            this.udBpm.DecimalPlaces = 3;
            this.udBpm.Location = new System.Drawing.Point(65, 17);
            this.udBpm.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.udBpm.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.udBpm.Name = "udBpm";
            this.udBpm.Size = new System.Drawing.Size(147, 20);
            this.udBpm.TabIndex = 5;
            this.udBpm.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.udBpm.ValueChanged += new System.EventHandler(this.udBpm_ValueChanged);
            // 
            // btnTimingUseCurrent
            // 
            this.btnTimingUseCurrent.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnTimingUseCurrent.Location = new System.Drawing.Point(6, 81);
            this.btnTimingUseCurrent.Name = "btnTimingUseCurrent";
            this.btnTimingUseCurrent.Size = new System.Drawing.Size(206, 42);
            this.btnTimingUseCurrent.TabIndex = 4;
            this.btnTimingUseCurrent.Text = "Use Current Time";
            this.btnTimingUseCurrent.UseVisualStyleBackColor = true;
            this.btnTimingUseCurrent.Click += new System.EventHandler(this.btnTimingUseCurrent_Click);
            // 
            // lblTimingOffset
            // 
            this.lblTimingOffset.AutoSize = true;
            this.lblTimingOffset.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblTimingOffset.Location = new System.Drawing.Point(6, 49);
            this.lblTimingOffset.Name = "lblTimingOffset";
            this.lblTimingOffset.Size = new System.Drawing.Size(43, 15);
            this.lblTimingOffset.TabIndex = 1;
            this.lblTimingOffset.Text = "Offset";
            // 
            // lblBpm
            // 
            this.lblBpm.AutoSize = true;
            this.lblBpm.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblBpm.Location = new System.Drawing.Point(6, 19);
            this.lblBpm.Name = "lblBpm";
            this.lblBpm.Size = new System.Drawing.Size(33, 15);
            this.lblBpm.TabIndex = 0;
            this.lblBpm.Text = "BPM";
            // 
            // tabGameplay
            // 
            this.tabGameplay.Controls.Add(this.grpDifficulty);
            this.tabGameplay.Controls.Add(this.grpGameplayGeneral);
            this.tabGameplay.Location = new System.Drawing.Point(4, 22);
            this.tabGameplay.Name = "tabGameplay";
            this.tabGameplay.Padding = new System.Windows.Forms.Padding(3);
            this.tabGameplay.Size = new System.Drawing.Size(230, 271);
            this.tabGameplay.TabIndex = 1;
            this.tabGameplay.Text = "Timing";
            // 
            // grpDifficulty
            // 
            this.grpDifficulty.Controls.Add(this.udSliderMultiplier);
            this.grpDifficulty.Controls.Add(this.radSliderCustom);
            this.grpDifficulty.Controls.Add(this.radSlider150);
            this.grpDifficulty.Controls.Add(this.radSlider100);
            this.grpDifficulty.Controls.Add(this.radSlider75);
            this.grpDifficulty.Controls.Add(this.lblSliderMultiplier);
            this.grpDifficulty.Location = new System.Drawing.Point(6, 141);
            this.grpDifficulty.Name = "grpDifficulty";
            this.grpDifficulty.Size = new System.Drawing.Size(218, 124);
            this.grpDifficulty.TabIndex = 3;
            this.grpDifficulty.TabStop = false;
            this.grpDifficulty.Text = "Difficulty";
            // 
            // udSliderMultiplier
            // 
            this.udSliderMultiplier.DecimalPlaces = 2;
            this.udSliderMultiplier.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.udSliderMultiplier.Location = new System.Drawing.Point(91, 62);
            this.udSliderMultiplier.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.udSliderMultiplier.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.udSliderMultiplier.Name = "udSliderMultiplier";
            this.udSliderMultiplier.Size = new System.Drawing.Size(61, 20);
            this.udSliderMultiplier.TabIndex = 5;
            this.udSliderMultiplier.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udSliderMultiplier.ValueChanged += new System.EventHandler(this.udSliderMultiplier_ValueChanged);
            // 
            // radSliderCustom
            // 
            this.radSliderCustom.AutoSize = true;
            this.radSliderCustom.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radSliderCustom.Location = new System.Drawing.Point(9, 63);
            this.radSliderCustom.Name = "radSliderCustom";
            this.radSliderCustom.Size = new System.Drawing.Size(76, 20);
            this.radSliderCustom.TabIndex = 4;
            this.radSliderCustom.TabStop = true;
            this.radSliderCustom.Text = "Custom:";
            this.radSliderCustom.UseVisualStyleBackColor = true;
            this.radSliderCustom.CheckedChanged += new System.EventHandler(this.radSliderCustom_CheckedChanged);
            // 
            // radSlider150
            // 
            this.radSlider150.AutoSize = true;
            this.radSlider150.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radSlider150.Location = new System.Drawing.Point(135, 37);
            this.radSlider150.Name = "radSlider150";
            this.radSlider150.Size = new System.Drawing.Size(51, 20);
            this.radSlider150.TabIndex = 3;
            this.radSlider150.TabStop = true;
            this.radSlider150.Text = "1.5x";
            this.radSlider150.UseVisualStyleBackColor = true;
            this.radSlider150.CheckedChanged += new System.EventHandler(this.radSlider150_CheckedChanged);
            // 
            // radSlider100
            // 
            this.radSlider100.AutoSize = true;
            this.radSlider100.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radSlider100.Location = new System.Drawing.Point(72, 37);
            this.radSlider100.Name = "radSlider100";
            this.radSlider100.Size = new System.Drawing.Size(51, 20);
            this.radSlider100.TabIndex = 2;
            this.radSlider100.TabStop = true;
            this.radSlider100.Text = "1.0x";
            this.radSlider100.UseVisualStyleBackColor = true;
            this.radSlider100.CheckedChanged += new System.EventHandler(this.radSlider100_CheckedChanged);
            // 
            // radSlider75
            // 
            this.radSlider75.AutoSize = true;
            this.radSlider75.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radSlider75.Location = new System.Drawing.Point(9, 37);
            this.radSlider75.Name = "radSlider75";
            this.radSlider75.Size = new System.Drawing.Size(57, 20);
            this.radSlider75.TabIndex = 1;
            this.radSlider75.TabStop = true;
            this.radSlider75.Text = "0.75x";
            this.radSlider75.UseVisualStyleBackColor = true;
            this.radSlider75.CheckedChanged += new System.EventHandler(this.radSlider75_CheckedChanged);
            // 
            // lblSliderMultiplier
            // 
            this.lblSliderMultiplier.AutoSize = true;
            this.lblSliderMultiplier.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblSliderMultiplier.Location = new System.Drawing.Point(6, 19);
            this.lblSliderMultiplier.Name = "lblSliderMultiplier";
            this.lblSliderMultiplier.Size = new System.Drawing.Size(146, 15);
            this.lblSliderMultiplier.TabIndex = 0;
            this.lblSliderMultiplier.Text = "Slider velocity multiplier:";
            // 
            // grpGameplayGeneral
            // 
            this.grpGameplayGeneral.Controls.Add(this.udGameplayOffset);
            this.grpGameplayGeneral.Controls.Add(this.btnGameplayUseCurrent);
            this.grpGameplayGeneral.Controls.Add(this.lblGameplayOffset);
            this.grpGameplayGeneral.Location = new System.Drawing.Point(6, 6);
            this.grpGameplayGeneral.Name = "grpGameplayGeneral";
            this.grpGameplayGeneral.Size = new System.Drawing.Size(218, 129);
            this.grpGameplayGeneral.TabIndex = 2;
            this.grpGameplayGeneral.TabStop = false;
            this.grpGameplayGeneral.Text = "General";
            // 
            // udGameplayOffset
            // 
            this.udGameplayOffset.Location = new System.Drawing.Point(65, 34);
            this.udGameplayOffset.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.udGameplayOffset.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.udGameplayOffset.Name = "udGameplayOffset";
            this.udGameplayOffset.Size = new System.Drawing.Size(147, 20);
            this.udGameplayOffset.TabIndex = 7;
            this.udGameplayOffset.ValueChanged += new System.EventHandler(this.udGameplayOffset_ValueChanged);
            // 
            // btnGameplayUseCurrent
            // 
            this.btnGameplayUseCurrent.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnGameplayUseCurrent.Location = new System.Drawing.Point(6, 81);
            this.btnGameplayUseCurrent.Name = "btnGameplayUseCurrent";
            this.btnGameplayUseCurrent.Size = new System.Drawing.Size(206, 42);
            this.btnGameplayUseCurrent.TabIndex = 4;
            this.btnGameplayUseCurrent.Text = "Use Current Time";
            this.btnGameplayUseCurrent.UseVisualStyleBackColor = true;
            this.btnGameplayUseCurrent.Click += new System.EventHandler(this.btnGameplayUseCurrent_Click);
            // 
            // lblGameplayOffset
            // 
            this.lblGameplayOffset.AutoSize = true;
            this.lblGameplayOffset.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblGameplayOffset.Location = new System.Drawing.Point(6, 36);
            this.lblGameplayOffset.Name = "lblGameplayOffset";
            this.lblGameplayOffset.Size = new System.Drawing.Size(43, 15);
            this.lblGameplayOffset.TabIndex = 1;
            this.lblGameplayOffset.Text = "Offset";
            // 
            // tabBulk
            // 
            this.tabBulk.Controls.Add(this.grpBulkDifficulty);
            this.tabBulk.Controls.Add(this.grpBulkTimeSignature);
            this.tabBulk.Controls.Add(this.grpBulkGeneral);
            this.tabBulk.Location = new System.Drawing.Point(4, 22);
            this.tabBulk.Name = "tabBulk";
            this.tabBulk.Size = new System.Drawing.Size(230, 271);
            this.tabBulk.TabIndex = 4;
            this.tabBulk.Text = "Timing";
            // 
            // grpBulkDifficulty
            // 
            this.grpBulkDifficulty.Controls.Add(this.udBulkSliderMultiplier);
            this.grpBulkDifficulty.Controls.Add(this.radBulkSliderCustom);
            this.grpBulkDifficulty.Controls.Add(this.radBulkSlider150);
            this.grpBulkDifficulty.Controls.Add(this.radBulkSlider100);
            this.grpBulkDifficulty.Controls.Add(this.radBulkSlider75);
            this.grpBulkDifficulty.Location = new System.Drawing.Point(6, 186);
            this.grpBulkDifficulty.Name = "grpBulkDifficulty";
            this.grpBulkDifficulty.Size = new System.Drawing.Size(218, 77);
            this.grpBulkDifficulty.TabIndex = 4;
            this.grpBulkDifficulty.TabStop = false;
            this.grpBulkDifficulty.Text = "Slider velocity multiplier";
            // 
            // udBulkSliderMultiplier
            // 
            this.udBulkSliderMultiplier.DecimalPlaces = 2;
            this.udBulkSliderMultiplier.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.udBulkSliderMultiplier.Location = new System.Drawing.Point(91, 47);
            this.udBulkSliderMultiplier.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.udBulkSliderMultiplier.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.udBulkSliderMultiplier.Name = "udBulkSliderMultiplier";
            this.udBulkSliderMultiplier.Size = new System.Drawing.Size(61, 20);
            this.udBulkSliderMultiplier.TabIndex = 5;
            this.udBulkSliderMultiplier.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udBulkSliderMultiplier.ValueChanged += new System.EventHandler(this.udBulkSliderMultiplier_ValueChanged);
            // 
            // radBulkSliderCustom
            // 
            this.radBulkSliderCustom.AutoSize = true;
            this.radBulkSliderCustom.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radBulkSliderCustom.Location = new System.Drawing.Point(9, 48);
            this.radBulkSliderCustom.Name = "radBulkSliderCustom";
            this.radBulkSliderCustom.Size = new System.Drawing.Size(76, 20);
            this.radBulkSliderCustom.TabIndex = 4;
            this.radBulkSliderCustom.TabStop = true;
            this.radBulkSliderCustom.Text = "Custom:";
            this.radBulkSliderCustom.UseVisualStyleBackColor = true;
            this.radBulkSliderCustom.CheckedChanged += new System.EventHandler(this.radBulkSliderCustom_CheckedChanged);
            // 
            // radBulkSlider150
            // 
            this.radBulkSlider150.AutoSize = true;
            this.radBulkSlider150.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radBulkSlider150.Location = new System.Drawing.Point(135, 22);
            this.radBulkSlider150.Name = "radBulkSlider150";
            this.radBulkSlider150.Size = new System.Drawing.Size(51, 20);
            this.radBulkSlider150.TabIndex = 3;
            this.radBulkSlider150.TabStop = true;
            this.radBulkSlider150.Text = "1.5x";
            this.radBulkSlider150.UseVisualStyleBackColor = true;
            this.radBulkSlider150.CheckedChanged += new System.EventHandler(this.radBulkSlider150_CheckedChanged);
            // 
            // radBulkSlider100
            // 
            this.radBulkSlider100.AutoSize = true;
            this.radBulkSlider100.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radBulkSlider100.Location = new System.Drawing.Point(72, 22);
            this.radBulkSlider100.Name = "radBulkSlider100";
            this.radBulkSlider100.Size = new System.Drawing.Size(51, 20);
            this.radBulkSlider100.TabIndex = 2;
            this.radBulkSlider100.TabStop = true;
            this.radBulkSlider100.Text = "1.0x";
            this.radBulkSlider100.UseVisualStyleBackColor = true;
            this.radBulkSlider100.CheckedChanged += new System.EventHandler(this.radBulkSlider100_CheckedChanged);
            // 
            // radBulkSlider75
            // 
            this.radBulkSlider75.AutoSize = true;
            this.radBulkSlider75.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radBulkSlider75.Location = new System.Drawing.Point(9, 22);
            this.radBulkSlider75.Name = "radBulkSlider75";
            this.radBulkSlider75.Size = new System.Drawing.Size(57, 20);
            this.radBulkSlider75.TabIndex = 1;
            this.radBulkSlider75.TabStop = true;
            this.radBulkSlider75.Text = "0.75x";
            this.radBulkSlider75.UseVisualStyleBackColor = true;
            this.radBulkSlider75.CheckedChanged += new System.EventHandler(this.radBulkSlider75_CheckedChanged);
            // 
            // grpBulkTimeSignature
            // 
            this.grpBulkTimeSignature.Controls.Add(this.lblBulkCustomSignature);
            this.grpBulkTimeSignature.Controls.Add(this.udBulkMetronome);
            this.grpBulkTimeSignature.Controls.Add(this.radBulkCustomSignature);
            this.grpBulkTimeSignature.Controls.Add(this.radBulkTimingQuad);
            this.grpBulkTimeSignature.Controls.Add(this.radBulkTimingTriple);
            this.grpBulkTimeSignature.Location = new System.Drawing.Point(6, 83);
            this.grpBulkTimeSignature.Name = "grpBulkTimeSignature";
            this.grpBulkTimeSignature.Size = new System.Drawing.Size(218, 97);
            this.grpBulkTimeSignature.TabIndex = 2;
            this.grpBulkTimeSignature.TabStop = false;
            this.grpBulkTimeSignature.Text = "Time Signature (metronome)";
            // 
            // lblBulkCustomSignature
            // 
            this.lblBulkCustomSignature.AutoSize = true;
            this.lblBulkCustomSignature.Location = new System.Drawing.Point(123, 66);
            this.lblBulkCustomSignature.Name = "lblBulkCustomSignature";
            this.lblBulkCustomSignature.Size = new System.Drawing.Size(21, 15);
            this.lblBulkCustomSignature.TabIndex = 4;
            this.lblBulkCustomSignature.Text = "/ 4";
            // 
            // udBulkMetronome
            // 
            this.udBulkMetronome.Location = new System.Drawing.Point(73, 64);
            this.udBulkMetronome.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.udBulkMetronome.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.udBulkMetronome.Name = "udBulkMetronome";
            this.udBulkMetronome.Size = new System.Drawing.Size(44, 20);
            this.udBulkMetronome.TabIndex = 3;
            this.udBulkMetronome.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.udBulkMetronome.ValueChanged += new System.EventHandler(this.udBulkMetronome_ValueChanged);
            // 
            // radBulkCustomSignature
            // 
            this.radBulkCustomSignature.AutoSize = true;
            this.radBulkCustomSignature.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radBulkCustomSignature.Location = new System.Drawing.Point(9, 64);
            this.radBulkCustomSignature.Name = "radBulkCustomSignature";
            this.radBulkCustomSignature.Size = new System.Drawing.Size(64, 20);
            this.radBulkCustomSignature.TabIndex = 2;
            this.radBulkCustomSignature.TabStop = true;
            this.radBulkCustomSignature.Text = "Other:";
            this.radBulkCustomSignature.UseVisualStyleBackColor = true;
            this.radBulkCustomSignature.CheckedChanged += new System.EventHandler(this.radBulkCustomSignature_CheckedChanged);
            // 
            // radBulkTimingQuad
            // 
            this.radBulkTimingQuad.AutoSize = true;
            this.radBulkTimingQuad.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radBulkTimingQuad.Location = new System.Drawing.Point(9, 43);
            this.radBulkTimingQuad.Name = "radBulkTimingQuad";
            this.radBulkTimingQuad.Size = new System.Drawing.Size(114, 20);
            this.radBulkTimingQuad.TabIndex = 1;
            this.radBulkTimingQuad.TabStop = true;
            this.radBulkTimingQuad.Text = "4 / 4 (common)";
            this.radBulkTimingQuad.UseVisualStyleBackColor = true;
            this.radBulkTimingQuad.CheckedChanged += new System.EventHandler(this.radBulkTimingQuad_CheckedChanged);
            // 
            // radBulkTimingTriple
            // 
            this.radBulkTimingTriple.AutoSize = true;
            this.radBulkTimingTriple.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radBulkTimingTriple.Location = new System.Drawing.Point(9, 22);
            this.radBulkTimingTriple.Name = "radBulkTimingTriple";
            this.radBulkTimingTriple.Size = new System.Drawing.Size(92, 20);
            this.radBulkTimingTriple.TabIndex = 0;
            this.radBulkTimingTriple.TabStop = true;
            this.radBulkTimingTriple.Text = "3 / 4 (waltz)";
            this.radBulkTimingTriple.UseVisualStyleBackColor = true;
            this.radBulkTimingTriple.CheckedChanged += new System.EventHandler(this.radBulkTimingTriple_CheckedChanged);
            // 
            // grpBulkGeneral
            // 
            this.grpBulkGeneral.Controls.Add(this.udBulkMove);
            this.grpBulkGeneral.Controls.Add(this.btnBulkMove);
            this.grpBulkGeneral.Controls.Add(this.lblBulkMove);
            this.grpBulkGeneral.Location = new System.Drawing.Point(6, 6);
            this.grpBulkGeneral.Name = "grpBulkGeneral";
            this.grpBulkGeneral.Size = new System.Drawing.Size(218, 71);
            this.grpBulkGeneral.TabIndex = 0;
            this.grpBulkGeneral.TabStop = false;
            this.grpBulkGeneral.Text = "General";
            // 
            // udBulkMove
            // 
            this.udBulkMove.Location = new System.Drawing.Point(9, 39);
            this.udBulkMove.Maximum = new decimal(new int[] {
            300000,
            0,
            0,
            0});
            this.udBulkMove.Minimum = new decimal(new int[] {
            300000,
            0,
            0,
            -2147483648});
            this.udBulkMove.Name = "udBulkMove";
            this.udBulkMove.Size = new System.Drawing.Size(120, 20);
            this.udBulkMove.TabIndex = 3;
            this.udBulkMove.Enter += new System.EventHandler(this.udBulkMove_Enter);
            this.udBulkMove.Leave += new System.EventHandler(this.udBulkMove_Leave);
            // 
            // btnBulkMove
            // 
            this.btnBulkMove.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnBulkMove.Location = new System.Drawing.Point(137, 37);
            this.btnBulkMove.Name = "btnBulkMove";
            this.btnBulkMove.Size = new System.Drawing.Size(75, 23);
            this.btnBulkMove.TabIndex = 2;
            this.btnBulkMove.Text = "Move";
            this.btnBulkMove.UseVisualStyleBackColor = true;
            this.btnBulkMove.Click += new System.EventHandler(this.btnBulkMove_Click);
            // 
            // lblBulkMove
            // 
            this.lblBulkMove.AutoSize = true;
            this.lblBulkMove.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblBulkMove.Location = new System.Drawing.Point(6, 19);
            this.lblBulkMove.Name = "lblBulkMove";
            this.lblBulkMove.Size = new System.Drawing.Size(150, 15);
            this.lblBulkMove.TabIndex = 0;
            this.lblBulkMove.Text = "Move selected offsets by:";
            // 
            // tabAudio
            // 
            this.tabAudio.Controls.Add(this.grpVolume);
            this.tabAudio.Controls.Add(this.grpSampleset);
            this.tabAudio.Location = new System.Drawing.Point(4, 22);
            this.tabAudio.Name = "tabAudio";
            this.tabAudio.Size = new System.Drawing.Size(230, 271);
            this.tabAudio.TabIndex = 2;
            this.tabAudio.Text = "Audio";
            // 
            // grpVolume
            // 
            this.grpVolume.Controls.Add(this.button4);
            this.grpVolume.Controls.Add(this.button3);
            this.grpVolume.Controls.Add(this.button2);
            this.grpVolume.Controls.Add(this.button1);
            this.grpVolume.Controls.Add(this.lblVolume);
            this.grpVolume.Controls.Add(this.udVolume);
            this.grpVolume.Controls.Add(this.tbVolume);
            this.grpVolume.Location = new System.Drawing.Point(6, 143);
            this.grpVolume.Name = "grpVolume";
            this.grpVolume.Size = new System.Drawing.Size(218, 123);
            this.grpVolume.TabIndex = 4;
            this.grpVolume.TabStop = false;
            this.grpVolume.Text = "Volume";
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button4.Location = new System.Drawing.Point(114, 89);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(96, 25);
            this.button4.TabIndex = 7;
            this.button4.Text = "Clap";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button3.Location = new System.Drawing.Point(114, 58);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(96, 25);
            this.button3.TabIndex = 6;
            this.button3.Text = "Finish";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button2.Location = new System.Drawing.Point(9, 89);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 25);
            this.button2.TabIndex = 5;
            this.button2.Text = "Whistle";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Location = new System.Drawing.Point(9, 58);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 25);
            this.button1.TabIndex = 4;
            this.button1.Text = "Hit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblVolume
            // 
            this.lblVolume.AutoSize = true;
            this.lblVolume.Location = new System.Drawing.Point(199, 27);
            this.lblVolume.Name = "lblVolume";
            this.lblVolume.Size = new System.Drawing.Size(17, 15);
            this.lblVolume.TabIndex = 2;
            this.lblVolume.Text = "%";
            // 
            // udVolume
            // 
            this.udVolume.Location = new System.Drawing.Point(156, 23);
            this.udVolume.Name = "udVolume";
            this.udVolume.Size = new System.Drawing.Size(43, 20);
            this.udVolume.TabIndex = 1;
            this.udVolume.ValueChanged += new System.EventHandler(this.udVolume_ValueChanged);
            // 
            // tbVolume
            // 
            this.tbVolume.LargeChange = 10;
            this.tbVolume.Location = new System.Drawing.Point(2, 20);
            this.tbVolume.Maximum = 100;
            this.tbVolume.Minimum = 5;
            this.tbVolume.Name = "tbVolume";
            this.tbVolume.Size = new System.Drawing.Size(147, 45);
            this.tbVolume.TabIndex = 0;
            this.tbVolume.TickFrequency = 5;
            this.tbVolume.Value = 5;
            this.tbVolume.ValueChanged += new System.EventHandler(this.tbVolume_ValueChanged);
            // 
            // grpSampleset
            // 
            this.grpSampleset.Controls.Add(this.nudCustom);
            this.grpSampleset.Controls.Add(this.panel2);
            this.grpSampleset.Controls.Add(this.linkLabel1);
            this.grpSampleset.Controls.Add(this.radSamplesetCustom2);
            this.grpSampleset.Controls.Add(this.radSamplesetCustom1);
            this.grpSampleset.Controls.Add(this.radSamplesetDefault);
            this.grpSampleset.Location = new System.Drawing.Point(6, 6);
            this.grpSampleset.Name = "grpSampleset";
            this.grpSampleset.Size = new System.Drawing.Size(218, 117);
            this.grpSampleset.TabIndex = 3;
            this.grpSampleset.TabStop = false;
            this.grpSampleset.Text = "Sampleset";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.listSampleset);
            this.panel2.Location = new System.Drawing.Point(8, 22);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(103, 62);
            this.panel2.TabIndex = 5;
            // 
            // listSampleset
            // 
            this.listSampleset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listSampleset.FormattingEnabled = true;
            this.listSampleset.Items.AddRange(new object[] {
            "Normal",
            "Soft",
            "Drum"});
            this.listSampleset.Location = new System.Drawing.Point(0, 0);
            this.listSampleset.Name = "listSampleset";
            this.listSampleset.Size = new System.Drawing.Size(103, 62);
            this.listSampleset.TabIndex = 3;
            this.listSampleset.SelectedIndexChanged += new System.EventHandler(this.listSampleset_SelectedIndexChanged);
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.SystemColors.Highlight;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.linkLabel1.Location = new System.Drawing.Point(6, 93);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(204, 15);
            this.linkLabel1.TabIndex = 4;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "How to add custom sample overrides";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // radSamplesetCustom2
            // 
            this.radSamplesetCustom2.AutoSize = true;
            this.radSamplesetCustom2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radSamplesetCustom2.Location = new System.Drawing.Point(117, 64);
            this.radSamplesetCustom2.Name = "radSamplesetCustom2";
            this.radSamplesetCustom2.Size = new System.Drawing.Size(34, 20);
            this.radSamplesetCustom2.TabIndex = 2;
            this.radSamplesetCustom2.TabStop = true;
            this.radSamplesetCustom2.Text = " ";
            this.radSamplesetCustom2.UseVisualStyleBackColor = true;
            this.radSamplesetCustom2.CheckedChanged += new System.EventHandler(this.radSamplesetCustom2_CheckedChanged);
            // 
            // radSamplesetCustom1
            // 
            this.radSamplesetCustom1.AutoSize = true;
            this.radSamplesetCustom1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radSamplesetCustom1.Location = new System.Drawing.Point(117, 43);
            this.radSamplesetCustom1.Name = "radSamplesetCustom1";
            this.radSamplesetCustom1.Size = new System.Drawing.Size(82, 20);
            this.radSamplesetCustom1.TabIndex = 1;
            this.radSamplesetCustom1.TabStop = true;
            this.radSamplesetCustom1.Text = "Custom 1";
            this.radSamplesetCustom1.UseVisualStyleBackColor = true;
            this.radSamplesetCustom1.CheckedChanged += new System.EventHandler(this.radSamplesetCustom1_CheckedChanged);
            // 
            // radSamplesetDefault
            // 
            this.radSamplesetDefault.AutoSize = true;
            this.radSamplesetDefault.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radSamplesetDefault.Location = new System.Drawing.Point(117, 22);
            this.radSamplesetDefault.Name = "radSamplesetDefault";
            this.radSamplesetDefault.Size = new System.Drawing.Size(69, 20);
            this.radSamplesetDefault.TabIndex = 0;
            this.radSamplesetDefault.TabStop = true;
            this.radSamplesetDefault.Text = "Default";
            this.radSamplesetDefault.UseVisualStyleBackColor = true;
            this.radSamplesetDefault.CheckedChanged += new System.EventHandler(this.radSamplesetDefault_CheckedChanged);
            // 
            // tabStyle
            // 
            this.tabStyle.Controls.Add(this.grpEffects);
            this.tabStyle.Location = new System.Drawing.Point(4, 22);
            this.tabStyle.Name = "tabStyle";
            this.tabStyle.Size = new System.Drawing.Size(230, 271);
            this.tabStyle.TabIndex = 3;
            this.tabStyle.Text = "Style";
            // 
            // grpEffects
            // 
            this.grpEffects.Controls.Add(this.panel3);
            this.grpEffects.Controls.Add(this.chkOmitBarline);
            this.grpEffects.Controls.Add(this.panel1);
            this.grpEffects.Controls.Add(this.chkKiai);
            this.grpEffects.Location = new System.Drawing.Point(6, 6);
            this.grpEffects.Name = "grpEffects";
            this.grpEffects.Size = new System.Drawing.Size(218, 222);
            this.grpEffects.TabIndex = 0;
            this.grpEffects.TabStop = false;
            this.grpEffects.Text = "Effects";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.labelOmitBarline);
            this.panel3.Location = new System.Drawing.Point(6, 162);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(206, 49);
            this.panel3.TabIndex = 4;
            // 
            // labelOmitBarline
            // 
            this.labelOmitBarline.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelOmitBarline.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.labelOmitBarline.Location = new System.Drawing.Point(0, 0);
            this.labelOmitBarline.Name = "labelOmitBarline";
            this.labelOmitBarline.Size = new System.Drawing.Size(206, 49);
            this.labelOmitBarline.TabIndex = 0;
            this.labelOmitBarline.Text = "Use this to remove the first bar line from this section in Taiko mode.";
            // 
            // chkOmitBarline
            // 
            this.chkOmitBarline.AutoSize = true;
            this.chkOmitBarline.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkOmitBarline.Location = new System.Drawing.Point(9, 135);
            this.chkOmitBarline.Name = "chkOmitBarline";
            this.chkOmitBarline.Size = new System.Drawing.Size(124, 20);
            this.chkOmitBarline.TabIndex = 3;
            this.chkOmitBarline.Text = "Omit first bar line";
            this.chkOmitBarline.UseVisualStyleBackColor = true;
            this.chkOmitBarline.CheckedChanged += new System.EventHandler(this.chkOmitBarline_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblKiai);
            this.panel1.Location = new System.Drawing.Point(6, 48);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(206, 83);
            this.panel1.TabIndex = 2;
            // 
            // lblKiai
            // 
            this.lblKiai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKiai.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblKiai.Location = new System.Drawing.Point(0, 0);
            this.lblKiai.Name = "lblKiai";
            this.lblKiai.Size = new System.Drawing.Size(206, 83);
            this.lblKiai.TabIndex = 1;
            this.lblKiai.Text = "Fountains, flying stars, flashing beats. This is what you could call the \"epic ch" +
    "orus\" mode in osu!. It will emphasize a particular part of a song. Please don\'t " +
    "overuse it.";
            // 
            // chkKiai
            // 
            this.chkKiai.AutoSize = true;
            this.chkKiai.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkKiai.Location = new System.Drawing.Point(9, 22);
            this.chkKiai.Name = "chkKiai";
            this.chkKiai.Size = new System.Drawing.Size(85, 20);
            this.chkKiai.TabIndex = 0;
            this.chkKiai.Text = "Kiai Mode";
            this.chkKiai.UseVisualStyleBackColor = true;
            this.chkKiai.CheckedChanged += new System.EventHandler(this.chkKiai_CheckedChanged);
            // 
            // tabsRight
            // 
            this.tabsRight.Controls.Add(this.tabAll);
            this.tabsRight.Controls.Add(this.tabReds);
            this.tabsRight.Controls.Add(this.tabGreens);
            this.tabsRight.Location = new System.Drawing.Point(248, 4);
            this.tabsRight.Name = "tabsRight";
            this.tabsRight.SelectedIndex = 0;
            this.tabsRight.Size = new System.Drawing.Size(330, 272);
            this.tabsRight.TabIndex = 11;
            this.tabsRight.SelectedIndexChanged += new System.EventHandler(this.tabsRight_SelectedIndexChanged);
            // 
            // tabAll
            // 
            this.tabAll.BackColor = System.Drawing.Color.Transparent;
            this.tabAll.Location = new System.Drawing.Point(4, 22);
            this.tabAll.Name = "tabAll";
            this.tabAll.Padding = new System.Windows.Forms.Padding(3);
            this.tabAll.Size = new System.Drawing.Size(322, 246);
            this.tabAll.TabIndex = 0;
            this.tabAll.Text = "All";
            this.tabAll.UseVisualStyleBackColor = true;
            // 
            // tabReds
            // 
            this.tabReds.BackColor = System.Drawing.Color.Transparent;
            this.tabReds.Location = new System.Drawing.Point(4, 22);
            this.tabReds.Name = "tabReds";
            this.tabReds.Padding = new System.Windows.Forms.Padding(3);
            this.tabReds.Size = new System.Drawing.Size(322, 246);
            this.tabReds.TabIndex = 1;
            this.tabReds.Text = "Timing Points";
            this.tabReds.UseVisualStyleBackColor = true;
            // 
            // tabGreens
            // 
            this.tabGreens.BackColor = System.Drawing.Color.Transparent;
            this.tabGreens.Location = new System.Drawing.Point(4, 22);
            this.tabGreens.Name = "tabGreens";
            this.tabGreens.Size = new System.Drawing.Size(322, 246);
            this.tabGreens.TabIndex = 2;
            this.tabGreens.Text = "Inherited Points";
            this.tabGreens.UseVisualStyleBackColor = true;
            // 
            // pnlTimingPoints
            // 
            this.pnlTimingPoints.BackColor = System.Drawing.Color.Transparent;
            this.pnlTimingPoints.Controls.Add(this.listTimingPoints);
            this.pnlTimingPoints.Location = new System.Drawing.Point(251, 28);
            this.pnlTimingPoints.Name = "pnlTimingPoints";
            this.pnlTimingPoints.Size = new System.Drawing.Size(323, 245);
            this.pnlTimingPoints.TabIndex = 12;
            // 
            // listTimingPoints
            // 
            this.listTimingPoints.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listTimingPoints.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colOffset,
            this.colBpm,
            this.colSlider,
            this.colSampleset,
            this.colVolume,
            this.colKiai});
            this.listTimingPoints.ContextMenuStrip = this.contextMenuStrip1;
            this.listTimingPoints.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listTimingPoints.FullRowSelect = true;
            this.listTimingPoints.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listTimingPoints.HideSelection = false;
            this.listTimingPoints.Location = new System.Drawing.Point(0, 0);
            this.listTimingPoints.Name = "listTimingPoints";
            this.listTimingPoints.Size = new System.Drawing.Size(323, 245);
            this.listTimingPoints.SmallImageList = this.imageList1;
            this.listTimingPoints.TabIndex = 0;
            this.listTimingPoints.UseCompatibleStateImageBehavior = false;
            this.listTimingPoints.View = System.Windows.Forms.View.Details;
            this.listTimingPoints.ItemActivate += new System.EventHandler(this.listTimingPoints_ItemActivate);
            this.listTimingPoints.SelectedIndexChanged += new System.EventHandler(this.listTimingPoints_SelectedIndexChanged);
            this.listTimingPoints.Click += new System.EventHandler(this.listTimingPoints_Click);
            this.listTimingPoints.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listTimingPoints_KeyDown);
            this.listTimingPoints.KeyUp += new System.Windows.Forms.KeyEventHandler(this.listTimingPoints_KeyUp);
            this.listTimingPoints.Leave += new System.EventHandler(this.listTimingPoints_Leave);
            this.listTimingPoints.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listTimingPoints_MouseUp);
            // 
            // colOffset
            // 
            this.colOffset.Text = "Offset";
            this.colOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colOffset.Width = 81;
            // 
            // colBpm
            // 
            this.colBpm.Text = "BPM";
            this.colBpm.Width = 58;
            // 
            // colSlider
            // 
            this.colSlider.Text = "Meter";
            this.colSlider.Width = 44;
            // 
            // colSampleset
            // 
            this.colSampleset.Text = "Sample";
            this.colSampleset.Width = 51;
            // 
            // colVolume
            // 
            this.colVolume.Text = "Vol.";
            this.colVolume.Width = 45;
            // 
            // colKiai
            // 
            this.colKiai.Text = "Ki";
            this.colKiai.Width = 25;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addControlPointToolStripMenuItem,
            this.toolStripMenuItem2,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.toolStripMenuItem1,
            this.selectAllToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(171, 148);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // addControlPointToolStripMenuItem
            // 
            this.addControlPointToolStripMenuItem.Name = "addControlPointToolStripMenuItem";
            this.addControlPointToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.addControlPointToolStripMenuItem.Text = "A&dd Control Point";
            this.addControlPointToolStripMenuItem.Click += new System.EventHandler(this.addControlPointToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(167, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+X";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.cutToolStripMenuItem.Text = "Cu&t";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+C";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.copyToolStripMenuItem.Text = "&Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+V";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.pasteToolStripMenuItem.Text = "&Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeyDisplayString = "Del";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.deleteToolStripMenuItem.Text = "De&lete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(167, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+A";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.selectAllToolStripMenuItem.Text = "Select &All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // grpTimingScale
            // 
            this.grpTimingScale.Controls.Add(this.chkAdjustBookmarkAndPreview);
            this.grpTimingScale.Controls.Add(this.udBeatSnap);
            this.grpTimingScale.Controls.Add(this.lblBeatSnap);
            this.grpTimingScale.Controls.Add(this.chkResnapSliders);
            this.grpTimingScale.Controls.Add(this.chkSnapObjects);
            this.grpTimingScale.Controls.Add(this.chkScaleObjects);
            this.grpTimingScale.Location = new System.Drawing.Point(4, 303);
            this.grpTimingScale.Name = "grpTimingScale";
            this.grpTimingScale.Size = new System.Drawing.Size(574, 74);
            this.grpTimingScale.TabIndex = 12;
            this.grpTimingScale.TabStop = false;
            this.grpTimingScale.Text = "When applying timing changes...";
            // 
            // udBeatSnap
            // 
            this.udBeatSnap.Items.Add("1/1");
            this.udBeatSnap.Items.Add("1/2");
            this.udBeatSnap.Items.Add("1/3");
            this.udBeatSnap.Items.Add("1/4");
            this.udBeatSnap.Items.Add("1/6");
            this.udBeatSnap.Items.Add("1/8");
            this.udBeatSnap.Items.Add("1/12");
            this.udBeatSnap.Items.Add("1/24");
            this.udBeatSnap.Location = new System.Drawing.Point(478, 22);
            this.udBeatSnap.Name = "udBeatSnap";
            this.udBeatSnap.Size = new System.Drawing.Size(48, 20);
            this.udBeatSnap.TabIndex = 4;
            this.udBeatSnap.Text = "1/4";
            // 
            // lblBeatSnap
            // 
            this.lblBeatSnap.AutoSize = true;
            this.lblBeatSnap.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblBeatSnap.Location = new System.Drawing.Point(340, 24);
            this.lblBeatSnap.Name = "lblBeatSnap";
            this.lblBeatSnap.Size = new System.Drawing.Size(132, 15);
            this.lblBeatSnap.TabIndex = 3;
            this.lblBeatSnap.Text = "Use beat snap divisor of";
            // 
            // chkResnapSliders
            // 
            this.chkResnapSliders.AutoSize = true;
            this.chkResnapSliders.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkResnapSliders.Location = new System.Drawing.Point(191, 22);
            this.chkResnapSliders.Name = "chkResnapSliders";
            this.chkResnapSliders.Size = new System.Drawing.Size(143, 20);
            this.chkResnapSliders.TabIndex = 2;
            this.chkResnapSliders.Text = "Resnap slider lengths";
            this.chkResnapSliders.UseVisualStyleBackColor = true;
            // 
            // chkSnapObjects
            // 
            this.chkSnapObjects.AutoSize = true;
            this.chkSnapObjects.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkSnapObjects.Location = new System.Drawing.Point(8, 43);
            this.chkSnapObjects.Name = "chkSnapObjects";
            this.chkSnapObjects.Size = new System.Drawing.Size(176, 20);
            this.chkSnapObjects.TabIndex = 1;
            this.chkSnapObjects.Text = "Snap objects to new timing";
            this.chkSnapObjects.UseVisualStyleBackColor = true;
            // 
            // chkScaleObjects
            // 
            this.chkScaleObjects.AutoSize = true;
            this.chkScaleObjects.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkScaleObjects.Location = new System.Drawing.Point(8, 22);
            this.chkScaleObjects.Name = "chkScaleObjects";
            this.chkScaleObjects.Size = new System.Drawing.Size(177, 20);
            this.chkScaleObjects.TabIndex = 0;
            this.chkScaleObjects.Text = "Scale objects to new timing";
            this.chkScaleObjects.UseVisualStyleBackColor = true;
            // 
            // chkInherit
            // 
            this.chkInherit.AutoSize = true;
            this.chkInherit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkInherit.Location = new System.Drawing.Point(252, 282);
            this.chkInherit.Name = "chkInherit";
            this.chkInherit.Size = new System.Drawing.Size(196, 20);
            this.chkInherit.TabIndex = 15;
            this.chkInherit.Text = "Inherit previous timing settings";
            this.chkInherit.UseVisualStyleBackColor = true;
            this.chkInherit.CheckedChanged += new System.EventHandler(this.chkInherit_CheckedChanged);
            // 
            // pnlToolStrip
            // 
            this.pnlToolStrip.Controls.Add(this.toolStrip1);
            this.pnlToolStrip.Location = new System.Drawing.Point(509, 278);
            this.pnlToolStrip.Name = "pnlToolStrip";
            this.pnlToolStrip.Size = new System.Drawing.Size(64, 25);
            this.pnlToolStrip.TabIndex = 16;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolAdd,
            this.toolRemove});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(64, 27);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolAdd
            // 
            this.toolAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolAdd.Image = global::osu.Properties.Resources.add;
            this.toolAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolAdd.Margin = new System.Windows.Forms.Padding(0, 1, 8, 2);
            this.toolAdd.Name = "toolAdd";
            this.toolAdd.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolAdd.Padding = new System.Windows.Forms.Padding(2);
            this.toolAdd.Size = new System.Drawing.Size(24, 24);
            this.toolAdd.Text = "Add Timing Section";
            this.toolAdd.Click += new System.EventHandler(this.toolAdd_Click);
            // 
            // toolRemove
            // 
            this.toolRemove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolRemove.Image = global::osu.Properties.Resources.sub;
            this.toolRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolRemove.Name = "toolRemove";
            this.toolRemove.Padding = new System.Windows.Forms.Padding(2);
            this.toolRemove.Size = new System.Drawing.Size(24, 24);
            this.toolRemove.Text = "Remove Timing Section";
            this.toolRemove.Click += new System.EventHandler(this.toolRemove_Click);
            // 
            // TimingEntry
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(582, 422);
            this.Controls.Add(this.pnlTimingPoints);
            this.Controls.Add(this.chkInherit);
            this.Controls.Add(this.grpTimingScale);
            this.Controls.Add(this.pnlToolStrip);
            this.Controls.Add(this.tabsRight);
            this.Controls.Add(this.tabsLeft);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TimingEntry";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Timing and Control Points";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TimingEntry_FormClosing);
            this.Load += new System.EventHandler(this.TimingEntry_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudCustom)).EndInit();
            this.tabsLeft.ResumeLayout(false);
            this.tabTiming.ResumeLayout(false);
            this.grpTimingSignature.ResumeLayout(false);
            this.grpTimingSignature.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udMetronome)).EndInit();
            this.grpTimingGeneral.ResumeLayout(false);
            this.grpTimingGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udTimingOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udBpm)).EndInit();
            this.tabGameplay.ResumeLayout(false);
            this.grpDifficulty.ResumeLayout(false);
            this.grpDifficulty.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udSliderMultiplier)).EndInit();
            this.grpGameplayGeneral.ResumeLayout(false);
            this.grpGameplayGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udGameplayOffset)).EndInit();
            this.tabBulk.ResumeLayout(false);
            this.grpBulkDifficulty.ResumeLayout(false);
            this.grpBulkDifficulty.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udBulkSliderMultiplier)).EndInit();
            this.grpBulkTimeSignature.ResumeLayout(false);
            this.grpBulkTimeSignature.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udBulkMetronome)).EndInit();
            this.grpBulkGeneral.ResumeLayout(false);
            this.grpBulkGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udBulkMove)).EndInit();
            this.tabAudio.ResumeLayout(false);
            this.grpVolume.ResumeLayout(false);
            this.grpVolume.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbVolume)).EndInit();
            this.grpSampleset.ResumeLayout(false);
            this.grpSampleset.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabStyle.ResumeLayout(false);
            this.grpEffects.ResumeLayout(false);
            this.grpEffects.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tabsRight.ResumeLayout(false);
            this.pnlTimingPoints.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.grpTimingScale.ResumeLayout(false);
            this.grpTimingScale.PerformLayout();
            this.pnlToolStrip.ResumeLayout(false);
            this.pnlToolStrip.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TabControl tabsLeft;
        private System.Windows.Forms.TabPage tabTiming;
        private System.Windows.Forms.TabPage tabGameplay;
        private System.Windows.Forms.TabPage tabAudio;
        private System.Windows.Forms.TabPage tabStyle;
        private System.Windows.Forms.TabControl tabsRight;
        private System.Windows.Forms.TabPage tabAll;
        private System.Windows.Forms.Panel pnlTimingPoints;
        private System.Windows.Forms.TabPage tabReds;
        private System.Windows.Forms.TabPage tabGreens;
        private System.Windows.Forms.GroupBox grpTimingScale;
        private System.Windows.Forms.CheckBox chkInherit;
        private System.Windows.Forms.Panel pnlToolStrip;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolAdd;
        private System.Windows.Forms.ToolStripButton toolRemove;
        private osu.Helpers.Forms.pListView listTimingPoints;
        private System.Windows.Forms.ColumnHeader colOffset;
        private System.Windows.Forms.ColumnHeader colBpm;
        private System.Windows.Forms.ColumnHeader colSlider;
        private System.Windows.Forms.ColumnHeader colSampleset;
        private System.Windows.Forms.ColumnHeader colVolume;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.GroupBox grpTimingGeneral;
        private System.Windows.Forms.GroupBox grpTimingSignature;
        private System.Windows.Forms.Button btnTimingUseCurrent;
        private System.Windows.Forms.Label lblTimingOffset;
        private System.Windows.Forms.Label lblBpm;
        private System.Windows.Forms.Label lblCustomSignature;
        private System.Windows.Forms.NumericUpDown udMetronome;
        private System.Windows.Forms.RadioButton radCustomSignature;
        private System.Windows.Forms.RadioButton radTimingQuad;
        private System.Windows.Forms.RadioButton radTimingTriple;
        private System.Windows.Forms.GroupBox grpDifficulty;
        private System.Windows.Forms.GroupBox grpGameplayGeneral;
        private System.Windows.Forms.Button btnGameplayUseCurrent;
        private System.Windows.Forms.Label lblGameplayOffset;
        private System.Windows.Forms.NumericUpDown udSliderMultiplier;
        private System.Windows.Forms.RadioButton radSliderCustom;
        private System.Windows.Forms.RadioButton radSlider150;
        private System.Windows.Forms.RadioButton radSlider100;
        private System.Windows.Forms.RadioButton radSlider75;
        private System.Windows.Forms.Label lblSliderMultiplier;
        private System.Windows.Forms.ColumnHeader colKiai;
        private System.Windows.Forms.TabPage tabBulk;
        private System.Windows.Forms.GroupBox grpBulkGeneral;
        private System.Windows.Forms.NumericUpDown udBulkMove;
        private System.Windows.Forms.Button btnBulkMove;
        private System.Windows.Forms.Label lblBulkMove;
        private System.Windows.Forms.GroupBox grpBulkTimeSignature;
        private System.Windows.Forms.Label lblBulkCustomSignature;
        private System.Windows.Forms.NumericUpDown udBulkMetronome;
        private System.Windows.Forms.RadioButton radBulkCustomSignature;
        private System.Windows.Forms.RadioButton radBulkTimingQuad;
        private System.Windows.Forms.RadioButton radBulkTimingTriple;
        private System.Windows.Forms.GroupBox grpBulkDifficulty;
        private System.Windows.Forms.NumericUpDown udBulkSliderMultiplier;
        private System.Windows.Forms.RadioButton radBulkSliderCustom;
        private System.Windows.Forms.RadioButton radBulkSlider150;
        private System.Windows.Forms.RadioButton radBulkSlider100;
        private System.Windows.Forms.RadioButton radBulkSlider75;
        private System.Windows.Forms.GroupBox grpSampleset;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton radSamplesetCustom1;
        private System.Windows.Forms.RadioButton radSamplesetDefault;
        private System.Windows.Forms.GroupBox grpVolume;
        private System.Windows.Forms.Label lblVolume;
        private System.Windows.Forms.NumericUpDown udVolume;
        private System.Windows.Forms.TrackBar tbVolume;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.ListBox listSampleset;
        private System.Windows.Forms.GroupBox grpEffects;
        private System.Windows.Forms.Label lblKiai;
        private System.Windows.Forms.CheckBox chkKiai;
        private System.Windows.Forms.CheckBox chkResnapSliders;
        private System.Windows.Forms.CheckBox chkSnapObjects;
        private System.Windows.Forms.CheckBox chkScaleObjects;
        private System.Windows.Forms.DomainUpDown udBeatSnap;
        private System.Windows.Forms.Label lblBeatSnap;
        private System.Windows.Forms.NumericUpDown udTimingOffset;
        private System.Windows.Forms.NumericUpDown udBpm;
        private System.Windows.Forms.NumericUpDown udGameplayOffset;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addControlPointToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.CheckBox chkAdjustBookmarkAndPreview;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelOmitBarline;
        private System.Windows.Forms.NumericUpDown nudCustom;
        private System.Windows.Forms.RadioButton radSamplesetCustom2;
        private System.Windows.Forms.CheckBox chkOmitBarline;
    }
}