﻿namespace osu.GameModes.Edit.Forms
{
    partial class SampleList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_sample = new System.Windows.Forms.ListBox();
            this.b_close = new System.Windows.Forms.Button();
            this.b_del = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lb_sample
            // 
            this.lb_sample.FormattingEnabled = true;
            this.lb_sample.ItemHeight = 12;
            this.lb_sample.Location = new System.Drawing.Point(12, 12);
            this.lb_sample.Name = "lb_sample";
            this.lb_sample.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lb_sample.Size = new System.Drawing.Size(262, 328);
            this.lb_sample.TabIndex = 17;
            // 
            // b_close
            // 
            this.b_close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.b_close.Location = new System.Drawing.Point(187, 350);
            this.b_close.Name = "b_close";
            this.b_close.Size = new System.Drawing.Size(87, 30);
            this.b_close.TabIndex = 18;
            this.b_close.Text = "Close";
            this.b_close.UseVisualStyleBackColor = true;
            this.b_close.Click += new System.EventHandler(this.b_close_Click);
            // 
            // b_del
            // 
            this.b_del.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.b_del.Location = new System.Drawing.Point(12, 350);
            this.b_del.Name = "b_del";
            this.b_del.Size = new System.Drawing.Size(159, 30);
            this.b_del.TabIndex = 19;
            this.b_del.Text = "Delete";
            this.b_del.UseVisualStyleBackColor = true;
            this.b_del.Click += new System.EventHandler(this.b_del_Click);
            // 
            // SampleList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.b_close;
            this.ClientSize = new System.Drawing.Size(286, 392);
            this.Controls.Add(this.b_del);
            this.Controls.Add(this.b_close);
            this.Controls.Add(this.lb_sample);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SampleList";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sample events list";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lb_sample;
        private System.Windows.Forms.Button b_close;
        private System.Windows.Forms.Button b_del;
    }
}