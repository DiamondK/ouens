﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using osu.Audio;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.Graphics.Notifications;
using osu_common.Helpers;

namespace osu.GameModes.Edit.Forms
{
    public partial class SampleImport : pForm
    {
        internal static string CurrentSample = "";
        private static List<string> sampleList;
        private bool exist;
        private AudioSample lastPlaySample;

        public SampleImport()
        {
            InitializeComponent();
        }

        internal static void Clear()
        {
            if (sampleList != null)
                sampleList.Clear();
            sampleList = null;
        }

        protected override void OnShown(EventArgs e)
        {
            if (sampleList == null)
            {
                sampleList = AudioEngine.GetAllBeatmapSamples();
            }
            lb_sample.Items.Add("None");
            lb_sample.Items.AddRange(sampleList.ToArray());
            base.OnShown(e);
        }

        private void b_open_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Sample file|*.wav;*.ogg;*.mp3";
            if (ofd.ShowDialog(GameBase.Form) == System.Windows.Forms.DialogResult.OK)
            {
                importSample(ofd.FileName);
            }
        }

        private void b_play_Click(object sender, EventArgs e)
        {
            if (lb_sample.SelectedItem == null)
                return;
            string key = lb_sample.SelectedItem.ToString();
            if (key == "None")
                return;
            if (lastPlaySample != null)
            {
                lastPlaySample.Stop();
                lastPlaySample = null;
            }

            int addr = AudioEngine.LoadBeatmapSample(key, false);

            //ignore slider and tick, just for preview
            //HitObject.ProcessSampleFile reset sample to proper loop config.
            if (addr != -1)
                lastPlaySample = AudioEngine.PlaySample(addr, (int)nud_vol.Value);
        }

        private void importSample(string file)
        {
            //copy to song dir
            try
            {
                string name = file.Substring(file.LastIndexOf(Path.DirectorySeparatorChar) + 1);
                string destination = BeatmapManager.Current.ContainingFolder + Path.DirectorySeparatorChar + name;
                if (File.Exists(destination))
                    File.Delete(destination);
                File.Copy(file, destination);
                int addr = AudioEngine.LoadBeatmapSample(name, false, false, true);//force reload
                MessageBox.Show("Loaded!");
                lb_sample.Items.Add(name);
                sampleList.Add(name);
                if (addr == 0)
                {
                    NotificationManager.ShowMessageMassive("You have added an invaild sample", 2000);
                }
            }
            catch (Exception)
            {
                MessageBox.Show(
                    "An error occurred during the import process.  This could mean the wav/mp3/ogg file is unreadable by osu!");
            }
        }

        private void b_apply_Click(object sender, EventArgs e)
        {
            if (cb_basic.Checked)
            {
                Editor.Instance.UndoPush();
                Editor.Instance.Compose.changeSelectedSample((int)nud_custom.Value, (int)nud_vol.Value);
            }
            else
            {
                if (lb_sample.SelectedItem == null)
                {
                    NotificationManager.ShowMessageMassive("You havn't selected a sample yet.", 2000);
                    return;
                }
                string name = lb_sample.SelectedItem.ToString();
                Editor.Instance.UndoPush();
                if (name == "None")
                {
                    Editor.Instance.Compose.changeSelectedSample("", (int)nud_vol.Value);
                }
                else
                {
                    Editor.Instance.Compose.changeSelectedSample(name, (int)nud_vol.Value);
                }

            }
            NotificationManager.ShowMessageMassive("Done", 1000);
        }

        private void b_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void b_reset_Click(object sender, EventArgs e)
        {
            Editor.Instance.UndoPush();
            if (cb_basic.Checked)
                Editor.Instance.Compose.changeSelectedSample(0, 0);
            else
                Editor.Instance.Compose.changeSelectedSample(string.Empty, 0);
            NotificationManager.ShowMessageMassive("Selected objects' hit samples have been reset.", 2000);
        }

        private void b_del_Click(object sender, EventArgs e)
        {
            if (lb_sample.SelectedItem == null)
                return;
            string key = lb_sample.SelectedItem.ToString();
            if (key == "None")
                return;
            AudioEngine.DeleteBeatmapSample(key.Substring(0, key.LastIndexOf('.')));
            lb_sample.Items.RemoveAt(lb_sample.SelectedIndex);
            sampleList.Remove(key);
            string destination = Path.Combine(BeatmapManager.Current.ContainingFolder, key);
            if (File.Exists(destination))
                File.Delete(destination);
        }

        private void lb_sample_DoubleClick(object sender, EventArgs e)
        {
            b_play_Click(null, null);
        }

        private void b_sample_Click(object sender, EventArgs e)
        {
            if (lb_sample.SelectedItem == null)
                return;
            string name = lb_sample.SelectedItem.ToString();
            if (name == "None")
                return;
            int addr = AudioEngine.LoadBeatmapSample(name);
            EventSample es = new EventSample(addr, name, AudioEngine.Time, StoryLayer.Background, (int)nud_vol.Value);
            es.WriteToOsu = true;
            Editor.Instance.UndoPush();
            Editor.Instance.eventManager.Add(es);
            NotificationManager.ShowMessageMassive("Done", 1000);
        }

        private void SampleImport_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (lastPlaySample != null)
                lastPlaySample.Stop();
            CurrentSample = "";
        }

        private void b_list_Click(object sender, EventArgs e)
        {
            SampleList sl = new SampleList();
            sl.ShowDialog(GameBase.Form);
        }

        private void lb_sample_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lb_sample.SelectedItem == null)
                return;
            string name = lb_sample.SelectedItem.ToString();
            if (name == "None")
                CurrentSample = "";
            else
                CurrentSample = name;
        }

    }
}
