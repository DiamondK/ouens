﻿using osu.Audio;
using osu_common.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace osu.GameModes.Edit.Forms
{
    public partial class VolumeDialog : pForm
    {
        public VolumeDialog()
        {
            InitializeComponent();
        }

        protected override void OnShown(EventArgs e)
        {
            tr_music.Value = AudioEngine.VolumeMusic.Value;
            tr_sample.Value = AudioEngine.VolumeEffect.Value;
            lb_music.Text = AudioEngine.VolumeMusic.Value.ToString();
            lb_sample.Text = AudioEngine.VolumeEffect.Value.ToString();
            base.OnShown(e);
        }

        private void tr_music_Scroll(object sender, EventArgs e)
        {
            lb_music.Text = tr_music.Value.ToString();
            AudioEngine.VolumeMusic.Value = tr_music.Value;
        }

        private void tb_sample_Scroll(object sender, EventArgs e)
        {
            lb_sample.Text = tr_sample.Value.ToString();
            AudioEngine.VolumeEffect.Value = tr_sample.Value;
        }
    }
}
