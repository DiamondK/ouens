using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;
using osu.Configuration;
using osu.GameplayElements.Beatmaps;
using osu.Online;
using osu_common;
using osu_common.Helpers;
using osu_common.Libraries.NetLib;
using osu.Audio;
using osu_common.Libraries.Osz2;
using osu.GameplayElements;
using osu_common.Libraries;
using System.Diagnostics;

namespace osu.GameModes.Edit.Forms
{
    internal partial class BeatmapSubmissionSystem : pForm
    {
        private readonly List<Beatmap> beatmaps;
        private readonly BackgroundWorker backgroundWorker = new BackgroundWorker() { WorkerSupportsCancellation = true };
        private readonly bool hasVideo;
        private readonly bool hasStoryboard;

        private string error;
        private long beatmapFilesize;
        private FileUploadNetRequest FileUploadRequest;
        private bool formClosing;

        //todo: these variables aren't being set
        private bool isNewSubmission = true;
        private int approved;

        private string newMessage;
        private string oldMessage;
        private int threadId;
        private bool uploadError;
        private DateTime progressStartTime;

        //used for initialsubmission:
        private int newBeatmapSetID;
        private int[] newBeatmapIDs;
        private bool fullSubmit;
        private bool bubblePop;
        private MapPackage packagePreviousUpload;
        private MapPackage packageCurrentUpload;

        private Editor editor;
        private int submissionQuotaRemaining;

        internal BeatmapSubmissionSystem(Editor editor, bool hasVideo, bool hasStoryboard)
        {
            this.editor = editor;
            this.hasVideo = hasVideo;
            this.hasStoryboard = hasStoryboard;
            InitializeComponent();

            Editor.IsSubmitting = true;

            checkLoad.Checked = ConfigManager.sLoadSubmittedThread;
            checkNotify.Checked = ConfigManager.sNotifySubmittedThread;

            //Quite naive matching here...
            beatmaps = BeatmapManager.Beatmaps.FindAll(b => b.ContainingFolder == BeatmapManager.Current.ContainingFolder);

            BanchoClient.UpdateStatus(bStatus.Submitting);

            backgroundWorker.RunWorkerCompleted += submission_InitialRequest_Complete;
            backgroundWorker.DoWork += submission_InitialRequest;
            backgroundWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Tries to get the current beatmapsetID by trying all available methods
        /// </summary>
        /// <returns>The beatmapSetID</returns>
        private int getBeatmapSetID()
        {
            //first we check the current beatmap
            if (BeatmapManager.Current.BeatmapSetId > 0)
                return BeatmapManager.Current.BeatmapSetId;

            //secondly we check the osz2 file
            if (packagePreviousUpload != null)
                return System.Convert.ToInt32(packagePreviousUpload.GetMetadata(MapMetaType.BeatmapSetID));

            //thirdly we check other beatmaps
            foreach (Beatmap b in beatmaps)
                if (b.BeatmapSetId > 0)
                    return b.BeatmapSetId;

            return -1;
        }

        /// <summary>
        /// Returns a comma-separated list of beatmap IDs.
        /// </summary>
        /// <returns></returns>
        private string getBeatmapIdList()
        {
            string commaList = String.Empty;
            foreach (Beatmap b in beatmaps)
                commaList += b.BeatmapId + ",";
            return commaList.TrimEnd(',');
        }

        private bool handleErrorCode(string[] resultSplit, int expectedCount)
        {
            //if (resultSplit.Length != expectedCount)
            //    error = "Invalid return from the submission handshake. Please try to resubmit";

            try
            {
                int errorCode = System.Convert.ToInt32(resultSplit[0]);
                switch (errorCode)
                {
                    case 0:
                        return false;
                    case 1:
                        error = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_OwnershipError);
                        break;
                    case 2:
                        error = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_NoLongerAvailable);
                        break;
                    case 3:
                        error = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_AlreadyRanked);
                        break;
                    case 4:
                        error = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_InGraveyard) +
                        LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_UngraveyardMap);
                        break;
                    default:
                        if (errorCode > 4 || errorCode < 0)
                            error = resultSplit[1];
                        break;
                }
            }
            catch
            {
            }

            return true;
        }

        /// <summary>
        /// Called when dialog window opens. Does preliminary checks in background.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submission_InitialRequest(object sender, DoWorkEventArgs e)
        {
            string osz2Hash = string.Empty;
            string osz2FileHash = string.Empty;

            if (BeatmapManager.Current.Creator != GameBase.User.Name && (GameBase.User.Permission & Permissions.BAT) == 0)
            {
                error = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_OwnershipError);
                return;
            }

            string lastUpload = lastUploadFilename;

            if (lastUpload != null && File.Exists(lastUpload))
            {
                try
                {
                    packagePreviousUpload = new MapPackage(lastUpload);
                    osz2Hash = (BitConverter.ToString(packagePreviousUpload.hash_body) +
                        BitConverter.ToString(packagePreviousUpload.hash_meta)).Replace("-", "");
                }
                catch
                {
                }

                osz2FileHash = CryptoHelper.GetMd5(lastUpload);
            }

            int beatmapSetID = getBeatmapSetID();
            string beatmapIDs = getBeatmapIdList();

            string url =
                string.Format(Urls.OSZ2_SUBMIT_GET_ID,
                ConfigManager.sUsername, ConfigManager.sPassword, beatmapSetID, beatmapIDs, osz2FileHash);

            string result = String.Empty;

            try
            {
                StringNetRequest submitGetID = new StringNetRequest(url);
                result = submitGetID.BlockingPerform();
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                //something has gone wrong, but we handle this in the next few lines.
            }

            if (checkCancel(e)) return;

            if (string.IsNullOrEmpty(result))
            {
                error = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_BSSConnectFailed) +
                         LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_CheckConnection);
                return;
            }

            string[] resultSplit = result.Split('\n');

            if (handleErrorCode(resultSplit, 6))
                return;

            newBeatmapSetID = System.Convert.ToInt32(resultSplit[1]);
            newBeatmapIDs = Array.ConvertAll<string, int>(resultSplit[2].Split(','), (s) => System.Convert.ToInt32(s));
            fullSubmit = resultSplit[3] == "1" || packagePreviousUpload == null;
            Int32.TryParse(resultSplit[4], out submissionQuotaRemaining);
            bubblePop = resultSplit.Length > 5 && resultSplit[5] == "1";
        }

        /// <summary>
        /// Preliminary checks are complete. This is called on the main form thread and either enables the user interface or displays an error.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submission_InitialRequest_Complete(object sender, RunWorkerCompletedEventArgs e)
        {
#if DISABLE_BSS
            error = "BSS is disabled for this osz2 test build";
#endif

            if (error != null)
            {
                if (!formClosing)
                    MessageBox.Show(this, error);
                Close();
                return;
            }

            isNewSubmission = newBeatmapSetID != getBeatmapSetID();

            buttonUpload.Enabled = true;
            buttonUpload.Text = !isNewSubmission ? LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_UpdateBeatmap) : LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_UploadNewBeatmap);
            if (!isNewSubmission)
                labelPreSubmissionInfo.Text = bubblePop ? LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_ResetBubbleStatus) : "";
            else
                labelPreSubmissionInfo.Text = string.Format(LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_AvailableUploadCount), submissionQuotaRemaining, (submissionQuotaRemaining != 1 ? "s" : ""));

            UpdateStatus(isNewSubmission 
                ? LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_ReadyToUpload)
                : LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_ReadyToUpdate));

            backgroundWorker.DoWork -= submission_InitialRequest;
            backgroundWorker.RunWorkerCompleted -= submission_InitialRequest_Complete;
        }

        private void buttonUpload_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;

            backgroundWorker.DoWork += submission_PackageAndUpload;
            backgroundWorker.RunWorkerAsync();
        }

        private void submission_PackageAndUpload(object sender, DoWorkEventArgs e)
        {
            bool workDone = false;

            UpdateStatus(LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_SynchronisingMaps));

            //Run this on main thread to avoid threading issues (the editor is still being displayed in the background).
            GameBase.Scheduler.Add(delegate
            {
                using (HitObjectManager hitObjectManager = new HitObjectManagerEditor())
                {
                    Beatmap current = BeatmapManager.Current;

                    bool requiresReload = false;

                    try
                    {
                        //overwrite beatmap id's from the webservice and save them to disk.
                        for (int i = 0; i < beatmaps.Count; i++)
                        {
                            Beatmap b = beatmaps[i];
                            if (b.BeatmapSetId != newBeatmapSetID || b.BeatmapId != newBeatmapIDs[i])
                            {
                                AudioEngine.LoadedBeatmap = BeatmapManager.Current = b;
                                hitObjectManager.SetBeatmap(b, Mods.None);
                                hitObjectManager.LoadWithEvents();
                                b.BeatmapSetId = newBeatmapSetID;
                                b.BeatmapId = newBeatmapIDs[i];
                                hitObjectManager.Save(false, false, false);

                                requiresReload = true;
                            }

                            b.ComputeAndSetDifficultiesTomStars(b.PlayMode);
                        }
                    }
                    catch { }

                    if (requiresReload)
                    {
                        AudioEngine.LoadedBeatmap = BeatmapManager.Current = current;
                        editor.LoadFile(current, false, false);
                    }

                    workDone = true;
                }
            });

            if (checkCancel(e)) return;

            //get beatmap topic contents
            string url = String.Format(Urls.FORUM_GET_BEATMAP_TOPIC_INFO, ConfigManager.sUsername, ConfigManager.sPassword, newBeatmapSetID);
            StringNetRequest submitGetTopicContents = new StringNetRequest(url);
            string result = string.Empty;
            try
            {
                result = submitGetTopicContents.BlockingPerform();
                if (!string.IsNullOrEmpty(result) && result[0] == '0')
                {
                    string[] results = result.Split((char)3);
                    threadId = int.Parse(results[1]);

                    Invoke(() => oldMessage = results[3]);
                }

            }
            catch
            {
            }

            while (!workDone)
                Thread.Sleep(100);

            if (checkCancel(e)) return;

            UpdateStatus(LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_CreatingPackage));

            //create a new Osz2 file
            string osz2Temp = Path.GetTempPath() + newBeatmapSetID + ".osz2";
            //todo: add to progress bar
            packageCurrentUpload = BeatmapManager.Current.ConvertToOsz2(osz2Temp, false);
            string osz2NewHash = (BitConverter.ToString(packageCurrentUpload.hash_body) +
                    BitConverter.ToString(packageCurrentUpload.hash_meta)).Replace("-", "");

            if (packageCurrentUpload == null)
            {
                error = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_CouldntCreatePackage) +
                LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_ExtraDiskSpace);
                submission_PackageAndUpload_Complete("-1", null);
                return;
            }

            beatmapFilesize = new System.IO.FileInfo(packageCurrentUpload.Filename).Length;

            if (checkCancel(e)) return;

            Invoke(delegate
            {

                panelMain.Enabled = true;

                if (beatmaps.Count > 1)
                {
                    radioButton1.Checked = approved == -1;
                    radioButton2.Checked = approved == 0;
                }
                else
                {
                    radioButton1.Checked = true;
                    radioButton2.Checked = false;
                }

                newMessage = "[size=85]This beatmap was submitted using in-game submission on " +
                             DateTime.Now.ToLongDateString() + " at " + DateTime.Now.ToLongTimeString() + "[/size]\n";
                newMessage += "\n[b]Artist:[/b] " + beatmaps[0].Artist;
                newMessage += "\n[b]Title:[/b] " + beatmaps[0].Title;
                if (beatmaps[0].Source.Length > 0)
                    newMessage += "\n[b]Source:[/b] " + beatmaps[0].Source;
                if (beatmaps[0].Tags.Length > 0)
                    newMessage += "\n[b]Tags:[/b] " + beatmaps[0].Tags;
                newMessage += "\n[b]BPM:[/b] " + Math.Round(1000 / beatmaps[0].ControlPoints[0].beatLength * 60, 2);
                newMessage += "\n[b]Filesize:[/b] " + (beatmapFilesize / 1024).ToString(GameBase.nfi) + "kb";
                newMessage +=
                    String.Format("\n[b]Play Time:[/b] {0:00}:{1:00}", beatmaps[0].TotalLength / 60000,
                                  (beatmaps[0].TotalLength % 60000) / 1000);
                newMessage += "\n[b]Difficulties Available:[/b]\n[list]";
                foreach (Beatmap b in beatmaps)
                {
                    newMessage += string.Format("[*][url={0}]{1}{2}[/url] ({3} stars, {4} notes)\n", Urls.PATH_MAPS + GeneralHelper.UrlEncode(Path.GetFileName(b.Filename)),
                        (b.Version.Length > 0 ? b.Version : "Normal"), (b.PlayMode == PlayModes.OsuMania ? " - " + (int)Math.Round(b.DifficultyCircleSize) + "Key" : ""), Math.Round(b.DifficultyTomStars(b.PlayMode), 2), b.ObjectCount);
                }
                newMessage += "[/list]\n";
                newMessage += "\n[size=150][b]Download: [url=" + General.WEB_ROOT + "/d/" + newBeatmapSetID + "]" +
                              beatmaps[0].SortTitle + "[/url][/b][/size]";
                if (hasVideo)
                    newMessage += "\n[size=120][b]Download: [url=" + General.WEB_ROOT + "/d/" + newBeatmapSetID + "n]" +
                                  beatmaps[0].SortTitle + " (no video)[/url][/b][/size]";
                newMessage += "\n[b]Information:[/b] [url=" + General.WEB_ROOT + "/s/" + newBeatmapSetID +
                              "]Scores/Beatmap Listing[/url]";
                newMessage += "\n---------------\n";

                textMessage.Text =
                    LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_CreatorsWords);

                if (!string.IsNullOrEmpty(oldMessage))
                {
                    if (oldMessage.IndexOf("---------------\n") > 0)
                        textMessage.Text = oldMessage.Remove(0, oldMessage.IndexOf("---------------") + 16).Trim('\n', '\r').Replace("\n", "\r\n");
                    else
                        textMessage.Text = oldMessage.Replace("\n", "\r\n");
                }

                textMessage.Focus();
                textMessage.SelectAll();

            });

            bool marathonException = beatmaps[0].Version.Contains("Marathon");

            if (beatmapFilesize > 32 * 1024 * 1024 && !marathonException) //special marathon exception
            {
                error = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_UploadFailed) +
                LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_BeatmapTooLarge);
                submission_PackageAndUpload_Complete("-1", null);
                return;
            }

            if (beatmaps.Count < 2 && !marathonException)
            {
                Invoke(delegate
                {
                    //don't allow submission to pending
                    radioButton2.Enabled = false;
                    radioButton2.Text = LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_PendingBeatmaps);
                });
            }

            string uploadUrl = string.Format(Urls.OSZ2_SUBMIT_UPLOAD, ConfigManager.sUsername, ConfigManager.sPassword,
                fullSubmit ? "1" : "2", "", newBeatmapSetID);

            byte[] uploadBytes;

            if (!fullSubmit)
            {
                UpdateStatus(LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_PreparingChanges));
                progressStartTime = DateTime.Now;

                BSDiffer patchCreator = new BSDiffer();
                patchCreator.OnProgress += upload_onUpdate;

                using (MemoryStream ms = new MemoryStream())
                {
                    patchCreator.Diff(packagePreviousUpload.Filename, osz2Temp, ms, Compression.GZip);
                    uploadBytes = ms.ToArray();
                }
            }
            else
            {
                uploadBytes = File.ReadAllBytes(osz2Temp);
            }

            if (checkCancel(e)) return;

            UpdateStatus(!isNewSubmission ? LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_SendingChanges) : LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Uploading));

            //Start the main upload process...
            FileUploadRequest = new FileUploadNetRequest(uploadUrl, uploadBytes, "osz2", "0");
            FileUploadRequest.ReceiveTimeout = 3 * 60000;
            FileUploadRequest.onUpdate += delegate(object s, long current, long total)
            {
                if (total == 0) return;
                if (current == total)
                    UpdateStatus(LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Distributing));
                else
                    UpdateProgress(current, total);
            };
            FileUploadRequest.onFinish += submission_PackageAndUpload_Complete;

            NetManager.AddRequest(FileUploadRequest);
            progressStartTime = DateTime.Now;
        }

        private bool checkCancel(DoWorkEventArgs e)
        {
            if (!backgroundWorker.CancellationPending)
                return false;

            e.Cancel = true;
            return true;
        }


        private void upload_onUpdate(object sender, long current, long total)
        {
            UpdateProgress(current, total);
        }

        private void submission_PackageAndUpload_Complete(string _result, Exception e)
        {
            Debug.Print(_result);
            if (uploadError || e != null || _result != "0")
            {
                Invoke(delegate
                {
                    if (_result != null)
                        handleErrorCode(_result.Split('\n'), 1);
                    if (!formClosing)
                        MessageBox.Show(this,
                                        string.Format(LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_ErrorDuringUpload), (error ?? _result ?? e.Message).Trim('\n', ' ')),
                                        "osu!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    Close();
                });

                if (packageCurrentUpload != null)
                {
                    packageCurrentUpload.Dispose();
                    File.Delete(packageCurrentUpload.Filename);
                    packageCurrentUpload = null;
                }

                return;
            }


            backgroundWorker.DoWork -= submission_PackageAndUpload;

            //Replace/create submissionCache for this map...
            try
            {
                packageCurrentUpload.Close();
                string lastUpload = lastUploadFilename;

                if (!Directory.Exists(Path.GetDirectoryName(lastUpload)))
                    Directory.CreateDirectory(Path.GetDirectoryName(lastUpload));

                if (packagePreviousUpload != null)
                    packagePreviousUpload.Close();

                File.Delete(lastUpload);
                File.Move(packageCurrentUpload.Filename, lastUpload);
            }
            catch { }

            //Finished uploading. Alert the user!

            UpdateStatus(isNewSubmission
            ? LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Uploaded)
            : LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_Updated));

            Invoke(delegate
            {
                if (!formClosing)
                    progressBar1.Value = 100;

                buttonSubmit.Enabled = true;

                AudioEngine.PlaySample(@"notify1");
                GameBase.FlashWindow(Handle, false);
            });

        }

        protected override void OnClosing(CancelEventArgs e)
        {
            editor.SubmissionComplete();

            formClosing = true;

            if (FileUploadRequest != null)
                FileUploadRequest.Close();

            if (backgroundWorker != null)
                backgroundWorker.CancelAsync();

            base.OnClosing(e);

            GameBase.MenuActive = false;

            BanchoClient.UpdateStatus(bStatus.Editing);

            if (packagePreviousUpload != null)
                packagePreviousUpload.Dispose();
            if (packageCurrentUpload != null)
                packageCurrentUpload.Dispose();

            GameBase.Form.Focus();
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            backgroundWorker.DoWork += submission_ForumSubmission;
            backgroundWorker.RunWorkerCompleted += submission_ForumSubmission_Complete;
            backgroundWorker.RunWorkerAsync();
            buttonSubmit.Enabled = false;
        }

        private void submission_ForumSubmission(object sender, DoWorkEventArgs e)
        {
            string textSubject;

            if (beatmaps[0].Artist.Length == 0)
                textSubject = beatmaps[0].Title;
            else
                textSubject = beatmaps[0].SortTitle;

            List<PlayModes> modes = new List<PlayModes>();
            foreach (Beatmap b in beatmaps)
                if (!modes.Contains(b.PlayMode))
                    modes.Add(b.PlayMode);
            if (modes.Count > 1 || modes[0] != PlayModes.Osu)
            {
                string modeString = string.Empty;
                foreach (PlayModes p in modes)
                    modeString = modeString + p + "|";
                modeString = modeString.TrimEnd('|');
                textSubject += " [" + modeString + "]";
            }

            UpdateStatus(LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_PostingToForums));
            Http h = new Http();
            HttpRequest r = new HttpRequest();
            h.Request = r;
            r.Items.AddFormField("u", ConfigManager.sUsername);
            r.Items.AddFormField("p", ConfigManager.sPassword);
            r.Items.AddFormField("b", newBeatmapSetID.ToString());
            r.Items.AddFormField("subject", textSubject);
            r.Items.AddFormField("message", newMessage + textMessage.Text.Replace("\r", ""));
            r.Items.AddFormField("complete", (radioButton2.Checked ? "1" : "0"));
            if (checkNotify.Checked)
                r.Items.AddFormField("notify", "1");
            string[] response = h.Post(General.WEB_ROOT + "/web/osu-osz2-bmsubmit-post.php");
            h.Close();
            threadId = Int32.Parse(response[0]);
        }

        private void submission_ForumSubmission_Complete(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!formClosing)
                progressBar1.Value = 100;
            UpdateStatus(LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_PostedToForums));
            beatmaps.ForEach(b => b.UpdateAvailable = false);//ensure don't show update status after quitting editor
            if (checkLoad.Checked)
                GameBase.ProcessStart(General.WEB_ROOT + "/forum/t/" + threadId);
            Close();
        }

        private void UpdateStatus(string str)
        {
            if (formClosing)
                return;

            Invoke(delegate
            {
                status.Text = str;
            });
        }

        double averageSpeed = 0;

        private void UpdateProgress(long current, long total)
        {
            double secondsPassed = DateTime.Now.Subtract(progressStartTime).TotalSeconds;

            double instantaneousSpeed = secondsPassed > 5 ? current / secondsPassed : 0;

            averageSpeed = averageSpeed == 0 ? instantaneousSpeed : (instantaneousSpeed * 0.01f + averageSpeed * 0.99f);

            if (formClosing)
                return;

            Invoke(delegate
            {
                float percent = (float)current / total * 100;
                progressBar1.Value = (int)percent;

                int seconds = (int)((total - current) / instantaneousSpeed) % 60;
                int mins = (int)((total - current) / instantaneousSpeed) / 60 + (seconds > 20 ? 1 : 0);

                string progress = "";
                if (percent > 0)
                {
                    if (mins >= 1)
                        progress = string.Format("{0:0.0}% ({1} minute{2} remaining)", percent, mins, mins != 1 ? "s" : "");
                    else if (seconds > 0)
                        progress = string.Format("{0:0.0}% ({1} second{2} remaining)", percent, seconds, seconds != 1 ? "s" : "");
                    else
                        progress = string.Format("{0:0.0}%", percent);
                }

                string newStatusText = status.Text;
                int dotLocation = newStatusText.IndexOf("...");
                if (dotLocation > 0)
                {
                    newStatusText = newStatusText.Remove(dotLocation) + "... " + progress;
                    UpdateStatus(newStatusText);
                }
            });
        }

        private void checkLoad_CheckedChanged(object sender, EventArgs e)
        {
            ConfigManager.sLoadSubmittedThread.Value = checkLoad.Checked;
            ConfigManager.sNotifySubmittedThread.Value = checkNotify.Checked;

            textMessage.Focus();
        }

        private void radioButton1_MouseUp(object sender, MouseEventArgs e)
        {
            textMessage.Focus();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (beatmaps.Count < 2 && radioButton2.Checked)
            {
                MessageBox.Show(LocalisationManager.GetString(OsuString.BeatmapSubmissionSystem_SubmittingRequiresTwoDiffs), "Sorry");
                radioButton2.Checked = false;
                radioButton1.Checked = true;
            }

            textMessage.Focus();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonRankingCriteria_Click(object sender, EventArgs e)
        {
            GameBase.ProcessStart(Urls.RANKING_CRITERIA);
        }

        private void buttonHelpForum_Click(object sender, EventArgs e)
        {
            GameBase.ProcessStart(Urls.FORUM_MAPPING_HELP);
        }

        private void buttonSubmissionFAQ_Click(object sender, EventArgs e)
        {
            GameBase.ProcessStart(Urls.HELP_SUBMISSION);
        }

        private void buttonModdingQueues_Click(object sender, EventArgs e)
        {
            GameBase.ProcessStart(Urls.FORUM_MODDING_QUEUES);
        }

        public string lastUploadFilename { get { return BeatmapManager.Current.BeatmapSetId <= 0 ? null : "Data/SubmissionCache/" + BeatmapManager.Current.BeatmapSetId + ".osz2"; } }
    }
}