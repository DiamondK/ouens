﻿namespace osu.GameModes.Edit.Forms
{
    partial class RotateBy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.lblRotate = new System.Windows.Forms.Label();
            this.angle = new System.Windows.Forms.TextBox();
            this.lblDegree = new System.Windows.Forms.Label();
            this.radClockwise = new System.Windows.Forms.RadioButton();
            this.radAnticlockwise = new System.Windows.Forms.RadioButton();
            this.gbDirection = new System.Windows.Forms.GroupBox();
            this.gbOrigin = new System.Windows.Forms.GroupBox();
            this.radPlayfield = new System.Windows.Forms.RadioButton();
            this.radSelection = new System.Windows.Forms.RadioButton();
            this.trRotate = new System.Windows.Forms.TrackBar();
            this.gbDirection.SuspendLayout();
            this.gbOrigin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trRotate)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(202, 155);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(86, 34);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(12, 155);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(182, 34);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblRotate
            // 
            this.lblRotate.AutoSize = true;
            this.lblRotate.Location = new System.Drawing.Point(22, 11);
            this.lblRotate.Name = "lblRotate";
            this.lblRotate.Size = new System.Drawing.Size(162, 14);
            this.lblRotate.TabIndex = 5;
            this.lblRotate.Text = "Enter an angle to rotate by:";
            // 
            // angle
            // 
            this.angle.Location = new System.Drawing.Point(200, 31);
            this.angle.Name = "angle";
            this.angle.Size = new System.Drawing.Size(48, 20);
            this.angle.TabIndex = 0;
            this.angle.Text = "0";
            this.angle.TextChanged += new System.EventHandler(this.angle_TextChanged);
            // 
            // lblDegree
            // 
            this.lblDegree.AutoSize = true;
            this.lblDegree.Location = new System.Drawing.Point(250, 32);
            this.lblDegree.Name = "lblDegree";
            this.lblDegree.Size = new System.Drawing.Size(13, 14);
            this.lblDegree.TabIndex = 7;
            this.lblDegree.Text = "°";
            // 
            // radClockwise
            // 
            this.radClockwise.AutoSize = true;
            this.radClockwise.Checked = true;
            this.radClockwise.Location = new System.Drawing.Point(15, 20);
            this.radClockwise.Name = "radClockwise";
            this.radClockwise.Size = new System.Drawing.Size(77, 18);
            this.radClockwise.TabIndex = 8;
            this.radClockwise.TabStop = true;
            this.radClockwise.Text = "Clockwise";
            this.radClockwise.UseVisualStyleBackColor = true;
            this.radClockwise.CheckedChanged += new System.EventHandler(this.radDirection_CheckedChanged);
            // 
            // radAnticlockwise
            // 
            this.radAnticlockwise.AutoSize = true;
            this.radAnticlockwise.Location = new System.Drawing.Point(15, 41);
            this.radAnticlockwise.Name = "radAnticlockwise";
            this.radAnticlockwise.Size = new System.Drawing.Size(103, 18);
            this.radAnticlockwise.TabIndex = 9;
            this.radAnticlockwise.Text = "Anti-Clockwise";
            this.radAnticlockwise.UseVisualStyleBackColor = true;
            // 
            // gbDirection
            // 
            this.gbDirection.Controls.Add(this.radClockwise);
            this.gbDirection.Controls.Add(this.radAnticlockwise);
            this.gbDirection.Location = new System.Drawing.Point(12, 76);
            this.gbDirection.Name = "gbDirection";
            this.gbDirection.Size = new System.Drawing.Size(132, 73);
            this.gbDirection.TabIndex = 10;
            this.gbDirection.TabStop = false;
            this.gbDirection.Text = "Direction";
            // 
            // gbOrigin
            // 
            this.gbOrigin.Controls.Add(this.radPlayfield);
            this.gbOrigin.Controls.Add(this.radSelection);
            this.gbOrigin.Location = new System.Drawing.Point(154, 76);
            this.gbOrigin.Name = "gbOrigin";
            this.gbOrigin.Size = new System.Drawing.Size(134, 73);
            this.gbOrigin.TabIndex = 11;
            this.gbOrigin.TabStop = false;
            this.gbOrigin.Text = "Origin";
            // 
            // radPlayfield
            // 
            this.radPlayfield.AutoSize = true;
            this.radPlayfield.Checked = true;
            this.radPlayfield.Location = new System.Drawing.Point(15, 20);
            this.radPlayfield.Name = "radPlayfield";
            this.radPlayfield.Size = new System.Drawing.Size(109, 18);
            this.radPlayfield.TabIndex = 8;
            this.radPlayfield.TabStop = true;
            this.radPlayfield.Text = "Playfield Centre";
            this.radPlayfield.UseVisualStyleBackColor = true;
            this.radPlayfield.CheckedChanged += new System.EventHandler(this.radOrigin_CheckedChanged);
            // 
            // radSelection
            // 
            this.radSelection.AutoSize = true;
            this.radSelection.Location = new System.Drawing.Point(15, 41);
            this.radSelection.Name = "radSelection";
            this.radSelection.Size = new System.Drawing.Size(116, 18);
            this.radSelection.TabIndex = 9;
            this.radSelection.Text = "Selection Centre";
            this.radSelection.UseVisualStyleBackColor = true;
            // 
            // trRotate
            // 
            this.trRotate.LargeChange = 45;
            this.trRotate.Location = new System.Drawing.Point(20, 30);
            this.trRotate.Maximum = 180;
            this.trRotate.Minimum = -180;
            this.trRotate.Name = "trRotate";
            this.trRotate.Size = new System.Drawing.Size(174, 42);
            this.trRotate.TabIndex = 12;
            this.trRotate.TickFrequency = 45;
            this.trRotate.Scroll += new System.EventHandler(this.trRotate_Scroll);
            // 
            // RotateBy
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(297, 201);
            this.Controls.Add(this.trRotate);
            this.Controls.Add(this.gbOrigin);
            this.Controls.Add(this.gbDirection);
            this.Controls.Add(this.lblDegree);
            this.Controls.Add(this.angle);
            this.Controls.Add(this.lblRotate);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RotateBy";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Rotate by";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RotateBy_FormClosing);
            this.gbDirection.ResumeLayout(false);
            this.gbDirection.PerformLayout();
            this.gbOrigin.ResumeLayout(false);
            this.gbOrigin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trRotate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label lblRotate;
        private System.Windows.Forms.TextBox angle;
        private System.Windows.Forms.Label lblDegree;
        private System.Windows.Forms.RadioButton radClockwise;
        private System.Windows.Forms.RadioButton radAnticlockwise;
        private System.Windows.Forms.GroupBox gbDirection;
        private System.Windows.Forms.GroupBox gbOrigin;
        private System.Windows.Forms.RadioButton radPlayfield;
        private System.Windows.Forms.RadioButton radSelection;
        private System.Windows.Forms.TrackBar trRotate;
    }
}