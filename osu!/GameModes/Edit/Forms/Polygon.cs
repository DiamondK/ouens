﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using osu_common.Helpers;
using osu.Configuration;

namespace osu.GameModes.Edit.Forms
{
    public partial class Polygon : pForm
    {
        public Polygon()
        {
            InitializeComponent();
        }
        
        protected override void OnShown(EventArgs e)
        {
            if (ConfigManager.sFastEditor)
                Editor.Instance.overrideIgnoreState = true;
            Editor.Instance.UndoPush();
            Editor.Instance.ignoreUndoPush = true;
            nudCount.Select();
            trDS.Value = (int)(Configuration.ConfigManager.sDistanceSpacing*10);
            if (trDS.Value < 1)
                trDS.Value = 1;
            nudCount.Maximum = Editor.Instance.Compose.MaxCountAt(trDS.Value / 10.0);
            lblDSVal.Text = string.Format("{0:0.0#}×", Configuration.ConfigManager.sDistanceSpacing);
            Editor.Instance.SelectNone();
            Editor.Instance.Compose.ChangeComposeTool(osu.GameModes.Edit.Modes.EditorModeCompose.ComposeTools.Select);
            Editor.Instance.ignoreUndoPush = false;
            base.OnShown(e);
            ChangePolygon();
        }


        private void ChangePolygon() {
            Editor.Instance.Compose.AddPolygon((int)nudCount.Value, (int)nudRepeat.Value, (double)(trAngle.Value * 3.14159 / 180), (double)trDS.Value / 10.0);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Editor.Instance.Undo();
            Editor.Instance.RedoClear();

            if (ConfigManager.sFastEditor)
                Editor.Instance.overrideIgnoreState = false;

            this.Close();
        }

        private void trDS_Scroll(object sender, EventArgs e) {
            lblDSVal.Text = string.Format("{0:0.0#}×", (double)trDS.Value / 10);
            nudCount.Maximum = Editor.Instance.Compose.MaxCountAt(trDS.Value / 10.0);
            ChangePolygon();
        }

        private void trAngle_Scroll(object sender, EventArgs e) {
            lblAngleVal.Text = string.Format("{0}°", (int)trAngle.Value);
            ChangePolygon();
        }

        private void nudRepeat_ValueChanged(object sender, EventArgs e) {
            ChangePolygon();
        }

        private void nudEdge_ValueChanged(object sender, EventArgs e) {
            ChangePolygon();
        }

        private void Polygon_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Editor.Instance.overrideIgnoreState && ConfigManager.sFastEditor.Value)
            {
                Editor.Instance.overrideIgnoreState = false;
                Editor.Instance.UndoRedoClear();
            }
        }

    }
}
