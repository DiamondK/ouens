﻿namespace osu.GameModes.Edit.Forms
{
    partial class GotoTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(259, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "Jump to time: [mm:ss:ms] (combonumbers,...)";
            // 
            // time
            // 
            this.time.Location = new System.Drawing.Point(12, 30);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(287, 20);
            this.time.TabIndex = 0;
            this.time.TextChanged += new System.EventHandler(this.time_TextChanged);
            this.time.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GotoTime_KeyPress);
            // 
            // GotoTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(311, 43);
            this.ControlBox = false;
            this.Controls.Add(this.time);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GotoTime";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Jump to time: [mm:ss:ms] (combonumbers,...)";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GotoTime_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox time;
        private System.Windows.Forms.Label label1;
    }
}