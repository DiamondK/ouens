﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Notifications;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input;
using osu_common;
using osu_common.Libraries.NetLib;
using osu_common.Helpers;
using osu.Helpers;
using osu.Online;
using osu.GameModes.Play;
using osu.Online.Social;

namespace osu.GameModes.Ranking
{
    internal class RankingDialog : pDialog
    {
        private readonly pCheckbox favouriteOnline;

        private readonly int localRankingPosition;
        private readonly pTextBox localScoreName;
        private readonly pText onlineRanking2;
        private readonly pText onlineRankingLoading;
        private readonly bool OnlineRetrival;
        private readonly pText ratingText;
        private readonly pText recordingEntry;
        private readonly Score score;
        private readonly pSprite[] star = new pSprite[10];
        private readonly int vspacing;
        private bool acceptRating;
        internal pCheckbox exportReplay;
        private bool pendingOnlineScores;
        private pButton ratingConfirm;
        private int userRating = -1;

        protected override bool hideChatOnDisplay { get { return false; } }

        public RankingDialog(Ranking ranking)
            : base("", true)
        {
            score = ranking.score;

            int recordingStartTime = GameBase.Time;

            localRankingPosition = ScoreManager.InsertScore(score, true);

            if (ConfigManager.sUsername.Value.Length > 0)
            {
                string url = General.WEB_ROOT + "/web/osu-rate.php?u=" + ConfigManager.sUsername + "&p=" +
                             ConfigManager.sPassword + "&c=" + BeatmapManager.Current.BeatmapChecksum;
                StringNetRequest request = new StringNetRequest(url);
                request.onFinish += ratingCheckCallback;
                NetManager.AddRequest(request);
            }

            vspacing = 20;

            //Local Ranking...

            int widescreenBacktrack = GameBase.WindowDefaultWidthWidescreen - GameBase.WindowWidthScaled;
            int leftCentre = (GameBase.WindowWidthScaled - widescreenBacktrack) / 2;

            bool localHighScore = localRankingPosition > -1;
            pSprite p =
                new pText(
                    localHighScore
                        ? "You achieved the #" + localRankingPosition + " score on local rankings!"
                        : "You failed to get a high score on the local rankings.\nBetter luck next time!",
#if ARCADE
                    26, new Vector2(leftCentre + 20, vspacing + 150), 0.95f, true,
#else
 localHighScore ? 22 : 18, new Vector2(leftCentre, vspacing), 0.95f, true,
#endif
 localHighScore ? new Color(255, 203, 33) : new Color(64, 51, 8));
            p.Origin = Origins.Centre;
            Transformation t = new Transformation(TransformationType.Fade, 0, 1, 0, 400);
            p.Transformations.Add(t);
            AddSprite(p);

            vspacing += 20;

            p =
                new pText("Local player name:", 12, new Vector2(leftCentre - 110, vspacing), 0.95f, true,
                          Color.White);

            localScoreName =
                new pTextBox(ConfigManager.sUsername, 12, new Vector2(leftCentre - 10, vspacing), 130, 0.95f);
            localScoreName.LengthLimit = 20;

            int checkboxX = GameBase.WindowWidthScaled - 160;
            int checkboxY = 10;
            //Checkbox options...
            exportReplay = new pCheckbox("Export replay as .osr", new Vector2(checkboxX, checkboxY), 0.95f, false);

            checkboxY += 25;

            favouriteOnline =
                new pCheckbox("Online Favourite", new Vector2(checkboxX, checkboxY), 0.95f, BeatmapManager.Current.OnlineFavourite);

#if !ARCADE
            AddSprite(p);
            spriteManager.Add(localScoreName.SpriteCollection);
            spriteManager.Add(exportReplay.SpriteCollection);

            if (BeatmapManager.Current.BeatmapSetId > 0 && ConfigManager.sUsername.Value.Length > 0)
                spriteManager.Add(favouriteOnline.SpriteCollection);
#endif

            OnlineRetrival = (BeatmapManager.Current.SubmissionStatus == SubmissionStatus.Ranked ||
                              BeatmapManager.Current.SubmissionStatus == SubmissionStatus.Approved) &&
                             ConfigManager.sUsername.Value.Length > 0 && score.AllowSubmission;

            if (OnlineRetrival)
            {
                vspacing += 20;
                GameBase.User.DrawAt(new Vector2(leftCentre - 100, vspacing), false, 0);
                GameBase.User.Clickable = false;
                spriteManager.Add(GameBase.User.Sprites);

                if (score.submissionStatus < ScoreSubmissionStatus.Complete)
                {
                    score.SubmissionComplete += onlineResponseCallback;
                    onlineRankingLoading =
                        new pText("Retrieving online ranking statistics...", 22,
                                  new Vector2(GameBase.WindowWidthScaled / 2, 180),
                                  Vector2.Zero, 1, true, Color.White, false);
                    onlineRankingLoading.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, 0, 200,
                                                                                EasingTypes.Out));
                    onlineRankingLoading.Transformations.Add(new Transformation(TransformationType.Scale, 0, 1.2f, 0,
                                                                                200, EasingTypes.Out));
                    onlineRankingLoading.Transformations.Add(new Transformation(TransformationType.Scale, 1.2f, 1f, 200,
                                                                                1200, EasingTypes.Out));
                    onlineRankingLoading.Transformations.Add(new Transformation(TransformationType.Scale, 1f, 1.1f, 1200,
                                                                                2400, EasingTypes.Out));
                    onlineRankingLoading.Transformations[onlineRankingLoading.Transformations.Count - 1].Loop = true;
                    onlineRankingLoading.Transformations[onlineRankingLoading.Transformations.Count - 1].LoopDelay =
                        1200;
                    onlineRankingLoading.Transformations.Add(new Transformation(TransformationType.Scale, 1.1f, 1f, 2400,
                                                                                3600, EasingTypes.In));
                    onlineRankingLoading.Transformations[onlineRankingLoading.Transformations.Count - 1].Loop = true;
                    onlineRankingLoading.Transformations[onlineRankingLoading.Transformations.Count - 1].LoopDelay =
                        1200;

                    onlineRankingLoading.Origin = Origins.Centre;
                    spriteManager.Add(onlineRankingLoading);
                }
                else
                {
                    //already have response.
                    onlineResponseCallback(score);
                }
            }


            /*
             *      L           M         R
             * <- 168.75 -><-   x   -><- 175 ->
             * M = GameBase.WindowWidthScaled - 343.75
             */

            Background.Bypass = true;

            Color bgColour = new Color(255, 255, 255, 220);

            pSprite bg = new pSprite(SkinManager.Load(@"ranking-dialog-middle", SkinSource.Osu), new Vector2(168.75f, 0),
                                     0.5f, true, bgColour);
            bg.VectorScale = new Vector2((GameBase.WindowWidthScaled - 343.75f) * 1.6f, 1);
            AddSprite(bg);

            bg = new pSprite(SkinManager.Load(@"ranking-dialog-left", SkinSource.Osu), new Vector2(0, 0),
                                     0.5f, true, bgColour);
            AddSprite(bg);

            bg = new pSprite(SkinManager.Load(@"ranking-dialog-right", SkinSource.Osu), Fields.TopRight, Origins.TopRight, Clocks.Game, new Vector2(0, 0),
                                     0.5f, true, bgColour);
            AddSprite(bg);


#if ARCADE
            ratingText =
                new pText("Online ranking/rating not available in arcade mode!", 16, new Vector2(GameBase.WindowWidthScaled / 2, 375), 0.98F, true, Color.OrangeRed);

            const int button_pos = 255;
#else
            ratingText =
    new pText("Checking rating status...", 16, new Vector2(GameBase.WindowWidthScaled / 2, 355), 0.98F, true, Color.White);
            const int button_pos = 455;
#endif

            ratingText.Origin = Origins.Centre;

            ratingText.TextColour = Color.OrangeRed;

            spriteManager.Add(ratingText);

            int offset = 25;

            for (int i = 0; i < 10; i++)
            {
                star[i] =
                    new pSprite(SkinManager.Load(@"star3", SkinSource.Osu), Fields.TopLeft, Origins.Centre,
                                Clocks.Game,
                                new Vector2(GameBase.WindowWidthScaled / 2 - 144 + 32 * i,
                                            ratingText.Position.Y + offset),
                                0.98F, true, new Color(0, 0, 0, 50));
                star[i].TagNumeric = i + 1;

#if !ARCADE
                spriteManager.Add(star[i]);
#endif

                star[i] =
                    new pSprite(SkinManager.Load(@"star3", SkinSource.Osu), Fields.TopLeft, Origins.Centre,
                                Clocks.Game,
                                new Vector2(GameBase.WindowWidthScaled / 2 - 144 + 32 * i,
                                            ratingText.Position.Y + offset),
                                0.99F, true, Color.TransparentWhite);
                star[i].TagNumeric = i + 1;

#if !ARCADE
                spriteManager.Add(star[i]);
#endif
            }
        }

        private void onlineResponseCallback(object sender)
        {
            GameBase.Scheduler.Add(displayOnlineRanking, true);
        }

        private bool didUserRefresh = false;

        internal override void Display()
        {
            if (score.submissionStatus == ScoreSubmissionStatus.Complete)
            {
                GameBase.User.Refresh();
                didUserRefresh = true;
            }

            base.Display();
        }

        protected override void Dispose(bool disposing)
        {
            score.SubmissionComplete -= onlineResponseCallback;
            base.Dispose(disposing);
        }

        private void displayOnlineRanking()
        {
            switch (GameBase.Mode)
            {
                case OsuModes.Rank:
                case OsuModes.RankingTagCoop:
                case OsuModes.RankingTeam:
                case OsuModes.RankingVs:
                    break;
                default:
                    pendingOnlineScores = false;
                    return;
            }

            if (BeatmapManager.Current == null || score == null || score.WaitingResponse == null || !score.WaitingResponse.StartsWith("beatmapId"))
            {
                pendingOnlineScores = false;
                return;
            }

            Dictionary<string, string> beatmapInfo = new Dictionary<string, string>();
            Dictionary<string, string> chartInfo = new Dictionary<string, string>();

            try
            {
                int timeOffset = GetTimeOffset();

                //we haven't requested a refresh yet because the score was still submitting when this dialog appeared.
                if (IsDisplayed && !didUserRefresh) GameBase.User.Refresh();

                if (onlineRankingLoading != null)
                {
                    onlineRankingLoading.Transformations.Clear();
                    onlineRankingLoading.Scale = 1;
                    onlineRankingLoading.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, timeOffset, timeOffset + 500));
                }

                pTexture modeTexture;
                switch (score.playMode)
                {
                    case PlayModes.CatchTheBeat:
                        modeTexture = SkinManager.Load(@"mode-fruits");
                        break;
                    case PlayModes.Taiko:
                        modeTexture = SkinManager.Load(@"mode-taiko");
                        break;
                    case PlayModes.OsuMania:
                        modeTexture = SkinManager.Load(@"mode-mania");
                        break;
                    default:
                        modeTexture = SkinManager.Load(@"mode-osu");
                        break;
                }

                pSprite s_ModeLogo = new pSprite(modeTexture, Fields.TopRight, Origins.Centre, Clocks.Game,
                                         new Vector2(100, 230), 0.94f, true, Color.TransparentWhite) { Scale = 0.8f };
                s_ModeLogo.Transformations.Add(new Transformation(TransformationType.Rotation, 0, 0.1f, timeOffset,
                                                                  timeOffset + 5000, EasingTypes.Out));
                s_ModeLogo.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.1f, timeOffset,
                                                                  timeOffset + 2000));
                s_ModeLogo.Additive = true;

                AddSprite(s_ModeLogo);

                Color colourHeaderBg = new Color(42, 48, 48, 0);
                Color colourIncrease = new Color(103, 157, 17, 255);
                Color colourDecrease = new Color(203, 35, 18, 255);
                Color colourWin = new Color(246, 233, 25, 255);

                const int COL_WIDTH_CHART = 100;
                const int COL_WIDTH_MAP_RANK = 70;
                const int COL_WIDTH_OVERALL = 70;
                const int COL_WIDTH_ACCURACY = 70;
                const int COL_WIDTH_RANKED = 110;
                const int COL_WIDTH_TOTAL = 110;
                const int COL_WIDTH_NEXT_RANK = 100;
                int LEFT_PADDING = 3 + (GameBase.WindowWidthScaled - GameBase.WindowDefaultWidth) / 2;

                int y = 170;
                int x = LEFT_PADDING;

                Transformation hoverEffect = new Transformation(TransformationType.Fade, 200 / 255f, 1, 0, 100, EasingTypes.Out);

                x += COL_WIDTH_CHART;

                const int HEADER_FONT_SIZE = 9;

                pText pt = new pText("Map Rank", HEADER_FONT_SIZE, new Vector2(x, y),
                                     new Vector2(COL_WIDTH_MAP_RANK - 1, 0), 0.95f, true, Color.White,
                                     false);
                pt.BackgroundColour = colourHeaderBg;
                pt.Alpha = 0;
                pt.TextBold = true;
                pt.TextAlignment = TextAlignment.Centre;
                pt.HandleInput = true;
                pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                          timeOffset + 200 + (x + y - 150) / 4,
                                                          timeOffset + 200 + (x + y - 150) * 2));
                AddSprite(pt);

                x += COL_WIDTH_MAP_RANK;

                pt = new pText("Overall", HEADER_FONT_SIZE, new Vector2(x, y), new Vector2(COL_WIDTH_OVERALL - 1, 0),
                               0.95f, true, Color.White,
                               false);
                pt.BackgroundColour = colourHeaderBg;
                pt.TextBold = true;
                pt.Alpha = 0;
                pt.TextAlignment = TextAlignment.Centre;
                pt.HandleInput = true;
                pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                          timeOffset + 200 + (x + y - 150) / 4,
                                                          timeOffset + 200 + (x + y - 150) * 2));
                AddSprite(pt);

                x += COL_WIDTH_OVERALL;

                pt = new pText("Accuracy", HEADER_FONT_SIZE, new Vector2(x, y), new Vector2(COL_WIDTH_ACCURACY - 1, 0),
                               0.95f, true, Color.White,
                               false);
                pt.BackgroundColour = colourHeaderBg;
                pt.TextBold = true;
                pt.Alpha = 0;
                pt.TextAlignment = TextAlignment.Centre;
                pt.HandleInput = true;
                pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                          timeOffset + 200 + (x + y - 150) / 4,
                                                          timeOffset + 200 + (x + y - 150) * 2));
                AddSprite(pt);

                x += COL_WIDTH_ACCURACY;

                pt = new pText("Ranked Score", HEADER_FONT_SIZE, new Vector2(x, y), new Vector2(COL_WIDTH_RANKED - 1, 0),
                               0.95f, true,
                               Color.White, false);
                pt.BackgroundColour = colourHeaderBg;
                pt.TextBold = true;
                pt.Alpha = 0;
                pt.TextAlignment = TextAlignment.Centre;
                pt.HandleInput = true;
                pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                          timeOffset + 200 + (x + y - 150) / 4,
                                                          timeOffset + 200 + (x + y - 150) * 2));
                AddSprite(pt);

                x += COL_WIDTH_RANKED;

                pt = new pText("Total Score", HEADER_FONT_SIZE, new Vector2(x, y), new Vector2(COL_WIDTH_TOTAL - 1, 0),
                               0.95f, true,
                               Color.White, false);
                pt.BackgroundColour = colourHeaderBg;
                pt.TextBold = true;
                pt.Alpha = 0;
                pt.TextAlignment = TextAlignment.Centre;
                pt.HandleInput = true;
                pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                          timeOffset + 200 + (x + y - 150) / 4,
                                                          timeOffset + 200 + (x + y - 150) * 2));
                AddSprite(pt);

                x += COL_WIDTH_TOTAL;

                pt = new pText("To Next Rank", HEADER_FONT_SIZE, new Vector2(x, y),
                               new Vector2(COL_WIDTH_NEXT_RANK - 1, 0), 0.95f, true, Color.White,
                               false);
                pt.BackgroundColour = colourHeaderBg;
                pt.TextBold = true;
                pt.Alpha = 0;
                pt.TextAlignment = TextAlignment.Centre;
                pt.HandleInput = true;
                pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                          timeOffset + 200 + (x + y - 150) / 4,
                                                          timeOffset + 200 + (x + y - 150) * 2));
                AddSprite(pt);

                y += 10;

                List<string> achievements = new List<string>();

                //each one of these lines is for a different ranking chart.
                string[] chartLines = score.WaitingResponse.Split('\n');

                foreach (string s in chartLines[0].Split('|'))
                {
                    if (s.Length == 0 || s.Contains("error:")) continue;

                    int index = s.IndexOf(':');

                    if (index < 0) continue;

                    beatmapInfo[s.Remove(index).Trim()] = s.Substring(index + 1).Trim();
                }

                if (beatmapInfo.Count > 0)
                {
                    DateTime approved = DateTime.Now;
                    string approvedDateString = "unknown";

                    if (!string.IsNullOrEmpty(beatmapInfo["approvedDate"]))
                    {
                        try
                        {
                            approved = DateTime.Parse(beatmapInfo["approvedDate"], GameBase.nfi);
                            approvedDateString = approved.ToString("MMM, yyyy", GameBase.nfi);
                        }
                        catch { }
                    }

                    int passRate = (int)((float)Int32.Parse(beatmapInfo["beatmapPasscount"]) / Int32.Parse(beatmapInfo["beatmapPlaycount"]) * 100);

                    pt =
                        new pText(
                            string.Format("{2} by {3}\n{0:#,0} plays since {1} | {4}% pass rate",
                                          Int32.Parse(beatmapInfo["beatmapPlaycount"]), approvedDateString,
                                          BeatmapManager.Current.DisplayTitle, BeatmapManager.Current.Creator, passRate),
                            14, new Vector2(170, 301), 0.951f, true, Color.White);
                    pt.TextColour = new Color(255, 255, 255, 71);
                    pt.TextAlignment = TextAlignment.Centre;
                    AddSprite(pt);
                }

                for (int i = 1; i < chartLines.Length; i++)
                {
                    int thisChart = i;

                    string line = chartLines[i];
                    if (line.Length == 0) continue;

                    chartInfo = new Dictionary<string, string>();
                    foreach (string s in line.Split('|'))
                    {
                        if (s.Length == 0) continue;

                        int index = s.IndexOf(':');
                        if (index < 0) continue;
                        chartInfo[s.Remove(index).Trim()] = s.Substring(index + 1).Trim();
                    }

                    if (chartInfo.Count == 0) continue;

                    //achievements
                    if (chartInfo["achievements"].Length > 0)
                        achievements.AddRange(chartInfo["achievements"].Split(' '));

                    Color colourNoChange = i % 2 == 0 ? new Color(20, 20, 20) : new Color(40, 40, 40);

                    x = LEFT_PADDING;

                    EventHandler rowHover =
                        delegate(object sender, EventArgs e)
                        {
                            spriteManager.GetTagged("chart" + thisChart).ForEach(
                                s => s.FadeTo(1, 100, EasingTypes.Out));
                            pText ss = sender as pText;
                            ss.BorderWidth = 1;
                            ss.BorderColour = Color.White;
                            ss.TextChanged = true;
                        };
                    EventHandler rowLostHover =
                        delegate(object sender, EventArgs e)
                        {
                            spriteManager.GetTagged("chart" + thisChart).ForEach(
                                s => s.FadeTo(0.9f, 100, EasingTypes.In));
                            pText ss = sender as pText;
                            ss.BorderWidth = 0;
                            ss.TextChanged = true;
                        };



                    if (chartInfo["chartEndDate"].Length > 0)
                    {
                        DateTime end = DateTime.Parse(chartInfo["chartEndDate"], GameBase.nfi);
                        int days = (end - DateTime.Today).Days;
                        int chartLength = chartInfo["chartName"].Split(new[] { '\n' }).Length;

                        pt = new pText("(" + days + (days != 1 ? " days" : " day") + " left)", 8,
                                       new Vector2(x + 2, y + (chartLength + 1) * 12),
                                       new Vector2(COL_WIDTH_CHART - 1 - 4, 0),
                                       0.951f, true, Color.White, false);
                        pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                                  timeOffset + 200 + (x + y - 150) / 4,
                                                                  timeOffset + 200 + (x + y - 150) * 2));
                        pt.Alpha = 0;
                        pt.TextAlignment = TextAlignment.Centre;
                        pt.Tag = "chart" + i;
                        AddSprite(pt);
                    }

                    pt = new pText(chartInfo["chartName"] + "\n\t", 11, new Vector2(x, y),
                                   new Vector2(COL_WIDTH_CHART - 1, 0), 0.95f, true, Color.Orange, false);
                    pt.BackgroundColour = colourNoChange;
                    pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                              timeOffset + 200 + (x + y - 150) / 4,
                                                              timeOffset + 200 + (x + y - 150) * 2));
                    pt.TextBold = true;
                    pt.Alpha = 0;
                    pt.TextAlignment = TextAlignment.Centre;
                    pt.OnHover += rowHover;
                    pt.OnHoverLost += rowLostHover;
                    pt.HandleInput = true;
                    pt.ToolTip = "View this ranking chart's info/ranking page.";

                    if (chartInfo["chartId"] == "overall")
                    {
                        if (score != null && chartInfo.ContainsKey("onlineScoreId")) score.onlineId = Int64.Parse(chartInfo["onlineScoreId"]);
                        pt.OnClick += delegate { GameBase.ProcessStart(General.WEB_ROOT + "/p/playerranking"); };
                    }
                    else
                        pt.OnClick +=
                            delegate { GameBase.ProcessStart(General.WEB_ROOT + "/p/chart/?ch=" + chartInfo["chartId"]); };


                    pt.Tag = "chart" + i;
                    AddSprite(pt);

                    x += COL_WIDTH_CHART;

                    long after = Int32.Parse(chartInfo["beatmapRankingAfter"]);
                    long before = Int32.Parse(chartInfo["beatmapRankingBefore"]);

                    long change = before - after;
                    if (chartInfo["chartId"] == "overall" && after <= 100 && (before == 0 || after <= before))
                    {
                        ChatEngine.AddUserLog(string.Format(LocalisationManager.GetString(OsuString.Userlog_TopRank), after, BeatmapManager.Current.DisplayTitleFull, OsuCommon.PlayModeString(score.playMode)));
                    }
                    if (before == 0 || change == 0)
                    {
                        //first time playing this map (and got a new record).
                        pt = new pText(string.Format("#{0:#,0}", after), 17, new Vector2(x, y),
                                       new Vector2(COL_WIDTH_MAP_RANK - 1, 0),
                                       0.95f, true, Color.White, change != 0);
                        pt.BackgroundColour = change == 0 ? colourNoChange : colourWin;
                        pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                                  timeOffset + 200 + (x + y - 150) / 4,
                                                                  timeOffset + 200 + (x + y - 150) * 2));
                        pt.Alpha = 0;
                        pt.TextAlignment = TextAlignment.Centre;
                        pt.OnHover += rowHover;
                        pt.OnHoverLost += rowLostHover;
                        pt.Tag = "chart" + i;
                        pt.HandleInput = true;
                        AddSprite(pt);
                    }
                    else if (after < before)
                    {
                        //Improving on previous rank (or not).
                        pt =
                            new pText(
                                string.Format("#{0:#,0}\n" + (change != 0 ? " ({1}{2:#,0})" : "-"), after,
                                              change > 0 ? "+" : "", before - after), 11, new Vector2(x, y),
                                new Vector2(COL_WIDTH_MAP_RANK - 1, 0), 0.95f, true, Color.White, false);
                        pt.BackgroundColour = change > 0
                                                  ? colourIncrease
                                                  : (change < 0 ? colourDecrease : colourNoChange);
                        pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                                  timeOffset + 200 + (x + y - 150) / 4,
                                                                  timeOffset + 200 + (x + y - 150) * 2));
                        pt.Alpha = 0;
                        pt.TextAlignment = TextAlignment.Centre;
                        pt.OnHover += rowHover;
                        pt.OnHoverLost += rowLostHover;
                        pt.Tag = "chart" + i;
                        pt.HandleInput = true;
                        AddSprite(pt);
                    }
                    else
                    {
                        pt = new pText(string.Format("your best: #{0:#,0}", before), 8, new Vector2(x + 2, y + 12),
                                       new Vector2(COL_WIDTH_MAP_RANK - 1 - 4, 0),
                                       0.951f, true, Color.White, false);
                        pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                                  timeOffset + 200 + (x + y - 150) / 4,
                                                                  timeOffset + 200 + (x + y - 150) * 2));
                        pt.Alpha = 0;
                        pt.TextAlignment = TextAlignment.Centre;
                        pt.Tag = "chart" + i;
                        AddSprite(pt);

                        pt =
                            new pText(
                                string.Format("#{0:#,0}\n\t", after), 11, new Vector2(x, y),
                                new Vector2(COL_WIDTH_MAP_RANK - 1, 0), 0.95f, true, Color.White, false);
                        pt.BackgroundColour = colourNoChange;
                        pt.TextColour = Color.Gray;
                        pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                                  timeOffset + 200 + (x + y - 150) / 4,
                                                                  timeOffset + 200 + (x + y - 150) * 2));
                        pt.Alpha = 0;
                        pt.TextAlignment = TextAlignment.Centre;
                        pt.OnHover += rowHover;
                        pt.OnHoverLost += rowLostHover;
                        pt.Tag = "chart" + i;
                        pt.HandleInput = true;
                        AddSprite(pt);
                    }

                    pt.ToolTip = "Open beatmap's info/ranking page.";
                    pt.OnClick +=
                        delegate
                        {
                            GameBase.ProcessStart(General.WEB_ROOT + "/b/" + beatmapInfo["beatmapId"] +
                                                  "&ch=" + chartInfo["chartId"]);
                        };

                    x += COL_WIDTH_MAP_RANK;

                    after = Int32.Parse(chartInfo["rankAfter"]);
                    before = Int32.Parse(chartInfo["rankBefore"]);

                    change = before == 0 ? 0 : before - after;


                    pt =
                        new pText(
                            string.Format("#{0:#,0}\n" + (change != 0 ? " ({1}{2:#,0})" : "-"), after,
                                          change > 0 ? "+" : "", before - after), 11, new Vector2(x, y),
                            new Vector2(COL_WIDTH_OVERALL - 1, 0), 0.95f, true, Color.White, false);
                    pt.BackgroundColour = change > 0 ? colourIncrease : (change < 0 ? colourDecrease : colourNoChange);
                    pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                              timeOffset + 200 + (x + y - 150) / 4,
                                                              timeOffset + 200 + (x + y - 150) * 2));
                    pt.Alpha = 0;
                    pt.TextAlignment = TextAlignment.Centre;
                    pt.OnHover += rowHover;
                    pt.OnHoverLost += rowLostHover;
                    pt.Tag = "chart" + i;
                    pt.HandleInput = true;
                    AddSprite(pt);

                    x += COL_WIDTH_OVERALL;

                    float fAfter = float.Parse(chartInfo["accuracyAfter"], GameBase.nfi) * 100;
                    float fBefore = float.Parse(chartInfo["accuracyBefore"], GameBase.nfi) * 100;
                    float fChange = (float)Math.Round(fAfter - fBefore, 2);

                    pt =
                        new pText(
                            string.Format("{0:n}%\n" + (fChange != 0 && fBefore != 0 ? " ({1}{2:n}%)" : "-"), fAfter,
                                          fChange > 0 ? "+" : "",
                                          fAfter - fBefore), 11, new Vector2(x, y),
                            new Vector2(COL_WIDTH_ACCURACY - 1, 0), 0.95f,
                            true, Color.White, false);
                    pt.BackgroundColour = fChange > 0 ? colourIncrease : (fChange < 0 ? colourDecrease : colourNoChange);
                    pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                              timeOffset + 200 + (x + y - 150) / 4,
                                                              timeOffset + 200 + (x + y - 150) * 2));
                    pt.Alpha = 0;
                    pt.TextAlignment = TextAlignment.Centre;
                    pt.OnHover += rowHover;
                    pt.OnHoverLost += rowLostHover;
                    pt.Tag = "chart" + i;
                    pt.HandleInput = true;
                    AddSprite(pt);

                    x += COL_WIDTH_ACCURACY;

                    after = Int64.Parse(chartInfo["rankedScoreAfter"]);
                    before = Int64.Parse(chartInfo["rankedScoreBefore"]);

                    change = after - before;

                    pt =
                        new pText(
                            string.Format("{0:#,0}\n" + (change != 0 ? " ({1}{2:#,0})" : "-"), after,
                                          change > 0 ? "+" : "",
                                          after - before), 11, new Vector2(x, y), new Vector2(COL_WIDTH_RANKED - 1, 0),
                            0.95f,
                            true, Color.White, false);
                    pt.BackgroundColour = change > 0 ? colourIncrease : (change < 0 ? colourDecrease : colourNoChange);
                    pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                              timeOffset + 200 + (x + y - 150) / 4,
                                                              timeOffset + 200 + (x + y - 150) * 2));
                    pt.Alpha = 0;
                    pt.TextAlignment = TextAlignment.Centre;
                    pt.OnHover += rowHover;
                    pt.OnHoverLost += rowLostHover;
                    pt.Tag = "chart" + i;
                    pt.HandleInput = true;
                    AddSprite(pt);

                    x += COL_WIDTH_RANKED;

                    after = Int64.Parse(chartInfo["totalScoreAfter"]);
                    before = Int64.Parse(chartInfo["totalScoreBefore"]);

                    change = after - before;

                    pt =
                        new pText(
                            string.Format("{0:#,0}\n" + (change != 0 ? " ({1}{2:#,0})" : "-"), after,
                                          change > 0 ? "+" : "",
                                          after - before), 11, new Vector2(x, y), new Vector2(COL_WIDTH_TOTAL - 1, 0),
                            0.95f,
                            true, Color.White, false);
                    pt.BackgroundColour = change > 0 ? colourIncrease : (change < 0 ? colourDecrease : colourNoChange);
                    pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                              timeOffset + 200 + (x + y - 150) / 4,
                                                              timeOffset + 200 + (x + y - 150) * 2));
                    pt.Alpha = 0;
                    pt.TextAlignment = TextAlignment.Centre;
                    pt.OnHover += rowHover;
                    pt.OnHoverLost += rowLostHover;
                    pt.Tag = "chart" + i;
                    pt.HandleInput = true;
                    AddSprite(pt);

                    x += COL_WIDTH_TOTAL;

                    after = Int64.Parse(chartInfo["toNextRank"]);

                    string user = chartInfo["toNextRankUser"];


                    pt = new pText(after > 0 ? string.Format("@{0:#,0}\n\t", after) : "-\n\t", 11, new Vector2(x, y),
                                   new Vector2(COL_WIDTH_NEXT_RANK - 1, 0),
                                   0.95f, true, Color.White, false);
                    pt.BackgroundColour = colourNoChange;
                    pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                              timeOffset + 200 + (x + y - 150) / 4,
                                                              timeOffset + 200 + (x + y - 150) * 2));
                    pt.Alpha = 0;
                    pt.TextAlignment = TextAlignment.Centre;
                    pt.OnHover += rowHover;
                    pt.OnHoverLost += rowLostHover;
                    pt.Tag = "chart" + i;
                    pt.HandleInput = true;
                    AddSprite(pt);

                    if (user.Length > 0)
                    {
                        pt = new pText(user.Length > 0 ? "to pass " + user : "-", 8, new Vector2(x + 2, y + 12),
                                       new Vector2(COL_WIDTH_NEXT_RANK - 1 - 4, 0),
                                       0.951f, true, Color.White, false);
                        pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9f,
                                                                  timeOffset + 200 + (x + y - 150) / 4,
                                                                  timeOffset + 200 + (x + y - 150) * 2));
                        pt.Alpha = 0;
                        pt.TextAlignment = TextAlignment.Centre;
                        pt.Tag = "chart" + i;
                        AddSprite(pt);
                    }

                    y += 24;
                }

                LoadAchievements(achievements.ToArray());
            }
            catch (Exception e)
            {
                NotificationManager.ShowMessage("An error occurred when displaying the new ranking screen!\nThis has been reported and will be fixed soon!", Color.Red, 5000);
                OsuError err = new OsuError(e);

                string feedback = "response: \"" + score.WaitingResponse + "\"\n";

                if (beatmapInfo != null)
                    foreach (KeyValuePair<string, string> kvp in beatmapInfo)
                        feedback += kvp.Key + " - " + kvp.Value + "\n";

                feedback += "\n\n";

                if (chartInfo != null)
                    foreach (KeyValuePair<string, string> kvp in chartInfo)
                        feedback += kvp.Key + " - " + kvp.Value + "\n";

                err.Feedback = feedback;

                ErrorSubmission.Submit(err);
            }

            score.WaitingResponse = null;
        }

        private void closeWindow(object sender, EventArgs e)
        {
            if (IsDisplayed)
                Close();
        }

        internal override void Close(bool isCancel = false)
        {
            if (!IsDisplayed)
                return;

#if !DEBUG
            //do we need this?  i think it is already done earlier.
            if (score != null) score.WaitingResponse = null;
#endif

            //AudioEngine.PlaySample(@"menuback");

            if (OnlineRetrival)
            {
                if (favouriteOnline != null && favouriteOnline.Checked && !BeatmapManager.Current.OnlineFavourite)
                {
                    BeatmapManager.Current.OnlineFavourite = true;
                    StringNetRequest dnr =
                        new StringNetRequest(string.Format(Urls.ADD_FAVOURITE, ConfigManager.sUsername,
                                                           ConfigManager.sPassword, BeatmapManager.Current.BeatmapSetId));
                    dnr.onFinish +=
                        delegate(string result, Exception e)
                        {
                            NotificationManager.ShowMessageMassive(
                                result.Length > 0 ? result : "Online favourite added!",
                                result.Length > 0 ? 2000 : 1000);
                        };
                    NetManager.AddRequest(dnr);
                }

                if (GameBase.User.Sprites != null)
                {
                    GameBase.User.Clickable = true;
                    if (spriteManager != null)
                        foreach (pSprite p in GameBase.User.Sprites)
                            spriteManager.Remove(p);

                    if (spriteManager != null)
                        spriteManager.RemoveRange(GameBase.User.Sprites);
                }
            }

            WriteScore();

            base.Close(isCancel);
        }

        protected void star_OnClick(object sender, EventArgs e)
        {
            userRating = ((pSprite)sender).TagNumeric;

            foreach (pSprite s in star)
            {
                //s.IsClickable = false;

                if (s.TagNumeric > userRating)
                    s.Tag = Color.Black;
                else
                    s.Tag = Color.YellowGreen;
                if (s.Scale > 1)
                    s.Transformations.Add(
                        new Transformation(TransformationType.Scale, s.Scale, 1, GameBase.Time,
                                           GameBase.Time + 500));
            }

            string vText = "Vote (" + userRating + " star" + (userRating > 1 ? "s" : "") + ")";

            if (ratingConfirm == null)
            {
                ratingConfirm = new pButton(vText,
                                            new Vector2(star[9].Position.X + 32, star[0].Position.Y - 11),
                                            new Vector2(140, 24), 0.98f, Color.YellowGreen, confirmrating);
                spriteManager.Add(ratingConfirm.SpriteCollection);
                spriteManager.Add(ratingConfirm.SpriteCollection);
            }
            else
            {
                ratingConfirm.Text.Text = vText;
            }

            ratingConfirm.SpriteCollection.ForEach(f => f.FlashColour(Color.White, 500));

            return;
        }

        private void confirmrating(object sender, EventArgs e)
        {
            string url = General.WEB_ROOT + "/web/osu-rate.php?u=" + ConfigManager.sUsername + "&p=" +
                         ConfigManager.sPassword + "&c=" + BeatmapManager.Current.BeatmapChecksum + "&v=" +
                         userRating;
            StringNetRequest request = new StringNetRequest(url);
            request.onFinish += submitSuccess;

            acceptRating = false;
            foreach (pSprite s in star)
            {
                s.HandleInput = false;

                if (s.TagNumeric > userRating)
                    s.FadeColour(Color.Black, 500);
                else
                    s.FadeColour(Color.YellowGreen, 500);
                if (s.Scale > 1)
                    s.Transformations.Add(
                        new Transformation(TransformationType.Scale, s.Scale, 1, GameBase.Time,
                                           GameBase.Time + 500));
            }
            ratingText.Text = "Submitting rating...";

            ratingConfirm.Hide();

            NetManager.AddRequest(request);
        }

        protected void WriteScore()
        {
            if (localRankingPosition > -1)
            {
                if (localScoreName.Box.Text.Length > 0)
                    score.playerName = localScoreName.Box.Text;
                if (exportReplay.Checked) ScoreManager.ExportReplay(score);
                ScoreManager.InsertScore(score, false);
            }
        }

        internal override bool HandleKey(Keys k)
        {
            if (!IsDisplayed || InputManager.FullTextEnabled)
                return false;

            if (k == Keys.Enter || k == Keys.Escape)
                Close();

            return true;
        }

        protected void submitSuccess(string _result, Exception e)
        {
            //exception !
            if (e != null)
                return;

            if (_result.IndexOf('\n') > 0)
                _result = _result.Substring(_result.IndexOf('\n') + 1);

            ratingText.Text = "Thanks for your input!  This beatmap now has an average rating of " + _result + ".";

            ShowRating(float.Parse(_result, GameBase.nfi));
        }

        public override void Update()
        {
            if (IsDisplayed && pendingOnlineScores && OnlineRetrival)
            {
                if (score.onlineRank > 0)
                {
                    onlineRankingLoading.Text = "Congratulations - you achieved online rank #" + score.onlineRank + "!";
                    onlineRankingLoading.Transformations.Add(new Transformation(TransformationType.Scale, 1, 1.1f,
                                                                                GameBase.Time, GameBase.Time + 100,
                                                                                EasingTypes.Out));
                    onlineRankingLoading.Transformations.Add(new Transformation(TransformationType.Scale, 1.1f, 1,
                                                                                GameBase.Time + 100, GameBase.Time + 200,
                                                                                EasingTypes.In));
                    onlineRankingLoading.TextColour = Color.YellowGreen;
                }
                else
                {
                    onlineRankingLoading.Text = "You have previously set a higher online record.";
                    onlineRankingLoading.FlashColour(Color.OrangeRed, 400);
                    onlineRankingLoading.Transformations.Add(new Transformation(TransformationType.Rotation, 0, 0.1f,
                                                                                GameBase.Time, GameBase.Time + 50));
                    onlineRankingLoading.Transformations.Add(new Transformation(TransformationType.Rotation, 0.1f, -0.1f,
                                                                                GameBase.Time + 50, GameBase.Time + 150));
                    onlineRankingLoading.Transformations.Add(new Transformation(TransformationType.Rotation, -0.1f, 0,
                                                                                GameBase.Time + 150, GameBase.Time + 200));
                }

                if (!didUserRefresh)
                {
                    GameBase.User.Refresh();
                    didUserRefresh = true;
                }

                pendingOnlineScores = false;
            }

            if (acceptRating)
            {
                int found = -1;
                for (int i = 0; i < 10; i++)
                    if (star[i].Hovering)
                        found = i;
                for (int i = 0; i < 10; i++)
                    star[i].FadeColour(
                        i <= found ? Color.White : (star[i].Tag == null ? new Color(35, 162, 255) : (Color)star[i].Tag),
                        100);
            }
        }

        private List<pSprite> achievementHolders;

        private void LoadAchievements(string[] achievements)
        {
            if (achievements.Length == 0)
                return;

            if (IsDisplayed)
                NotificationManager.ShowMessageMassive("Loading new achievements...", 1000);

            achievementHolders = new List<pSprite>();

            for (int i = 0; i < achievements.Length; i++)
            {
                if (achievements[i].Length == 0)
                    continue;

                int timeOffset = GetTimeOffset();

                Vector2 position = new Vector2(GameBase.WindowWidthScaled / 2 - 24 - (achievements.Length * 42) / 2f + i * 84, 265);

                pSprite p = new pSprite(SkinManager.Load(@"achievement-unknown"), Origins.Centre, position, 0.95f + i * 0.001f, true, Color.TransparentWhite);
                p.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1,
                                                         timeOffset,
                                                         timeOffset + 300));
                AddSprite(p);
                achievementHolders.Add(p);

                int locali = i;

                DataNetRequest dnr =
                    new DataNetRequest(General.STATIC_WEB_ROOT + "/images/achievements/" +
                                       achievements[i]);
                dnr.onFinish += delegate(byte[] data, Exception e)
                                    {
                                        if (e != null) return;

                                        int timeOffsetLocal = GetTimeOffset();

                                        MemoryStream stream = new MemoryStream(data);
                                        stream.Position = 0;
                                        pTexture tex = pTexture.FromStream(stream, achievements[locali]);
                                        SkinManager.RegisterUnrecoverableTexture(tex);
                                        achievementHolders[locali].Texture = tex;

                                        p =
                                            new pSprite(tex, Origins.Centre, position, 0.96f + locali * 0.001f, true, Color.TransparentWhite);
                                        p.Additive = true;
                                        p.Transformations.Add(new Transformation(TransformationType.Scale, 1, 2,
                                                                                 timeOffsetLocal + locali * 200,
                                                                                 timeOffsetLocal + locali * 200 + 400));
                                        p.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0,
                                                                                 timeOffsetLocal + locali * 200,
                                                                                 timeOffsetLocal + locali * 200 + 400));
                                        AddSprite(p);
                                    };

                NetManager.AddRequest(dnr);
            }
        }

        protected void ratingCheckCallback(string _result, Exception e)
        {
            //exception !
            if (e != null)
            {
                return;
            }

            string[] result = _result.Split('\n');

            int timeOffset = GetTimeOffset();

            acceptRating = result[0] == "ok";

            if (acceptRating && score.playMode == BeatmapManager.Current.PlayMode)
            {
                ratingText.Text =
                    "Please take a second to rate this beatmap by clicking a star below!";

                for (int i = 0; i < 10; i++)
                {
                    star[i].OnClick += star_OnClick;
                    star[i].FadeColour(new Color(66, 173, 255, 255), 100);
                    star[i].Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, timeOffset + i * 50,
                                                                   timeOffset + i * 200, EasingTypes.Out));
                    star[i].HandleInput = true;
                    Transformation t = new Transformation(TransformationType.Scale, 1, 1.5F, 0, 300);
                    t.Easing = EasingTypes.Out;
                    star[i].HoverEffect = t;
                }
            }
            else
            {
#if !ARCADE
                ratingText.Text = "Rating is not available ";
                switch (result[0])
                {
                    default:
                    case "alreadyvoted":
                    case "owner":
                        break;
                    case "auth fail":
                        ratingText.Text += "because you are not logged in.";
                        break;
                    case "no exist":
                        ratingText.Text += "because this beatmap has not been submitted yet.";
                        break;
                    case "not ranked":
                        ratingText.Text += "because this beatmap is not a ranked map.";
                        break;
                }

                if (result.Length > 1)
                {
                    ratingText.Text = "This map has an average rating of ";

                    float rating = float.Parse(result[1], GameBase.nfi);

                    ShowRating(rating);

                    ratingText.Text += Math.Round(rating, 2).ToString();
                }

                ratingText.TextColour = Color.OrangeRed;
#endif
            }
        }

        private void ShowRating(float rating)
        {
            int timeOffset = GetTimeOffset();

            for (int i = 0; i < 10; i++)
            {
                if (rating > i)
                {
                    Color newColor = new Color(253, 222, 48, 255);

                    star[i].Alpha = 0;

                    if (rating < i + 1)
                    {
                        newColor = new Color(253, 183, 43, 255);
                        star[i].Transformations.Add(new Transformation(TransformationType.Fade, 0, rating - (int)rating,
                                                                       timeOffset + i * 50, timeOffset + i * 200,
                                                                       EasingTypes.Out));
                    }
                    else
                    {
                        star[i].Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, timeOffset + i * 50,
                                                                       timeOffset + i * 200, EasingTypes.Out));
                    }

                    star[i].Transformations.Add(new Transformation(TransformationType.Scale, 1.5f, 1, timeOffset + i * 50,
                                                                   timeOffset + i * 200, EasingTypes.Out));

                    if (star[i].IsVisible)
                        star[i].FadeColour(newColor, 100);
                    else
                        star[i].InitialColour = newColor;
                }
                else
                {
                    if (star[i].IsVisible)
                        star[i].FadeOut(100);
                    else
                    {
                        star[i].InitialColour = Color.TransparentWhite;
                        star[i].Alpha = 1;
                        star[i].DrawWidth = 0;
                    }
                }
            }
        }
    }
}