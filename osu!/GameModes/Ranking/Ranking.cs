﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Notifications;
using osu.Graphics.Primitives;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu_common;
using osudata;
using osu_common.Helpers;
using osu.Graphics.UserInterface;
using osu.Configuration;
using osu.GameModes.Select;

namespace osu.GameModes.Ranking
{
    internal class Ranking : DrawableGameComponent
    {
        protected readonly List<pSprite> leftPanel = new List<pSprite>();
        protected List<RankingElement> rankingElements = new List<RankingElement>();
        private AudioSample ApplauseChannel;
        protected pSprite b_back;
        protected pSprite b_replay;
        protected pSprite b_retry;
        protected bool forceDisplay;
        protected bool LoadingReplay;
        private RankingDialog rankingDialog;
        protected bool ReplayMode;
        protected HealthGraph healthGraph;
        protected pSpriteText s_score1;
        protected pSprite s_white;
        protected internal Score score;

        protected SpriteManager spriteManagerWideBelow;
        protected SpriteManager spriteManagerWideScrolling;
        protected SpriteManager spriteManagerWideAbove;
        protected pSprite[] star = new pSprite[10];
        protected int startTime;
        protected string stringTotalScore;
        private pSprite backgroundOverlay;
        private pSprite rankingLetter;
        private pScrollableArea scrollableArea;
        private pButton onlineRankingButton;
        private pSprite spriteRankingGraph;

        internal Ranking(Game game)
            : base(game)
        {
        }


        protected bool GameBase_OnKeyPressed(object sender, Keys k)
        {
            if (k == Keys.Escape)
            {
                Exit();
                return true;
            }

            if (k == Keys.F2)
            {
                if (score.isOnline)
                {
                    OnlineLoadReplay(score_GotReplayDataExport);
                    return true;
                }
                else if (score != null)
                {
                    ScoreManager.ExportReplay(score.replay == null && Player.currentScore != null ? Player.currentScore : score);
                    return true;
                }
            }

#if DEBUG
            if (k == Keys.S)
                score.Submit();
            if (k == Keys.E)
            {
                Player.currentScore = score;
                GameBase.ChangeMode(GameBase.Mode, true);
            }
            if (k == Keys.R)
                PopupRecord();
#endif

            if (k == Keys.Space || k == Keys.Enter)
            {
                Progress(false);
                return true;
            }

            return false;
        }

        private void score_GotReplayDataExport(object sender)
        {
            score.GotReplayData -= score_GotReplayDataExport;
            ScoreManager.ExportReplay(score);
            LoadingReplay = false;
            spriteManagerWideScrolling.BlacknessTarget = 0;
            NotificationManager.ClearMessageMassive();
        }

        protected override void Dispose(bool disposing)
        {
            if (ApplauseChannel != null) ApplauseChannel.Stop();

            AudioEngine.StopAllSampleEvents();

            if (scrollableArea != null) scrollableArea.Dispose();

            KeyboardHandler.OnKeyPressed -= GameBase_OnKeyPressed;

            if (BeatmapManager.Current != null) BeatmapManager.Current.Scores.Clear();

            if (rankingDialog != null)
            {
                rankingDialog.Close();
                rankingDialog.Dispose();
            }

            if (score != null)
            {
                if (score.hpGraph != null)
                    score.DisposeHpGraph();

                if (LoadingReplay)
                    score.GotReplayData -= score_GotReplayData;

                if (!InputManager.ReplayMode)
                    score.ClearReplayData();
            }

            switch (GameBase.GameQueuedState)
            {
                case OsuModes.Rank:
                case OsuModes.RankingTagCoop:
                case OsuModes.RankingTeam:
                case OsuModes.RankingVs:
                    break;
                default:
                    Player.currentScore = null;
                    break;
            }

            GameBase.spriteManager.Remove(s_white);

            if (spriteManagerWideScrolling != null) spriteManagerWideScrolling.Dispose();
            if (spriteManagerWideAbove != null) spriteManagerWideAbove.Dispose();
            if (spriteManagerWideBelow != null) spriteManagerWideBelow.Dispose();

            base.Dispose(disposing);
        }

        public override void Update()
        {
            if (scrollableArea == null) return;

#if ARCADE
            IdleHandler.Check(10000);
#endif
            scrollableArea.Update();

            if (onlineRankingButton != null)
            {
                if (scrollableArea.ScrollPosition.Y > 20)
                    onlineRankingButton.Hide();
                else
                    onlineRankingButton.Show();
            }

            int currentTime = GameBase.Time - startTime;

            int digitsVisible = currentTime / 500;

            if (s_score1 != null)
            {
                string built = string.Empty;
                for (int i = 0; i < Math.Max(8, stringTotalScore.Length); i++)
                {
                    if (i >= digitsVisible)
                        built += GameBase.random.Next(0, 9).ToString()[0];
                    else
                        built += stringTotalScore[i];
                }

                s_score1.Text = built;
            }

            if (currentTime > 4400 && !ReplayMode)
                PopupRecord();

            base.Update();
        }

        public override void Draw()
        {
            if (scrollableArea == null) return;

            base.Draw();

            spriteManagerWideBelow.Draw();
            scrollableArea.Draw();
            spriteManagerWideAbove.Draw();
        }

        protected virtual int AddRankingSprites()
        {
            const int length = 300;
            int delay = length - (forceDisplay ? 7000 : 0);

            foreach (RankingElement e in rankingElements)
            {
                // delay each transformation based on its order of creation.
                if (e.image != null)
                {
                    foreach (Transformation t in e.image.Transformations)
                    {
                        t.Time1 += delay;
                        t.Time2 += delay;
                    }
                    spriteManagerWideScrolling.Add(e.image);
                    leftPanel.Add(e.image);
                }

                if (e.text != null)
                {
                    foreach (Transformation t in e.text.Transformations)
                    {
                        t.Time1 += delay;
                        t.Time2 += delay;
                    }
                    spriteManagerWideScrolling.Add(e.text);
                    leftPanel.Add(e.text);
                }

                // overlays are part of existing elements, don't add extra delay.
                if (!e.isOverlay) delay += length;
            }

            return delay;
        }

        protected virtual void LoadScore(Score score)
        {
            if (score == null)
                return;

            foreach (pSprite ps in leftPanel)
            {
                ps.FadeOut(100);
                ps.AlwaysDraw = false;
            }
            leftPanel.Clear();
            rankingElements.Clear();

            this.score = score;

            stringTotalScore = String.Format("{0:00000000}", score.totalScore);

            s_score1 =
                new pSpriteText("", SkinManager.Current.FontScore, SkinManager.Current.FontScoreOverlap,
                                Fields.TopLeft, Origins.Centre, Clocks.Game,
                                new Vector2(220, 94), 0.8F, true, Color.White);
            s_score1.Scale = SkinManager.UseNewLayout ? 1.3f : 1.05f;
            if (SkinManager.UseNewLayout)
                s_score1.SpacingOverlap = -2;
            s_score1.TextConstantSpacing = true;
            spriteManagerWideScrolling.Add(s_score1);
            leftPanel.Add(s_score1);

            const int textx1 = 80;
            const int imgx1 = 40;
            const int textx2 = 280;
            const int imgx2 = 240;

            const int row1 = 160;
            const int row2 = 220;
            const int row3 = 280;
            const int row4 = 320;

            RankingElement e;

            switch (score.playMode)
            {
                case PlayModes.Osu:
                    {
                        rankingElements.Add(new RankingElement(@"hit300", new Vector2(imgx1, row1), String.Format("{0}x", score.count300)));

                        rankingElements.Add(new RankingElement(@"hit100", new Vector2(imgx1, row2), String.Format("{0}x", score.count100)));

                        rankingElements.Add(new RankingElement(@"hit50", new Vector2(imgx1, row3), String.Format("{0}x", score.count50)));

                        rankingElements.Add(new RankingElement(@"hit300g", new Vector2(imgx2, row1), String.Format("{0}x", score.countGeki)));

                        rankingElements.Add(new RankingElement(@"hit100k", new Vector2(imgx2, row2), String.Format("{0}x", score.countKatu)));

                        rankingElements.Add(new RankingElement(@"hit0", new Vector2(imgx2, row3), String.Format("{0}x", score.countMiss)));

                        break;
                    }
                case PlayModes.Taiko:
                    {
                        rankingElements.Add(new RankingElement(@"taiko-hit300", new Vector2(imgx1, row1),
                                                               String.Format("{0}x", score.count300 - score.countGeki)));

                        rankingElements.Add(new RankingElement(@"taiko-hit100", new Vector2(imgx1, row2),
                                                               String.Format("{0}x", score.count100 - score.countKatu)));

                        rankingElements.Add(new RankingElement(@"taiko-hit0", new Vector2(imgx1, row3), String.Format("{0}x", score.countMiss)));

                        rankingElements.Add(new RankingElement(@"taiko-hit300g", new Vector2(imgx2, row1), String.Format("{0}x", score.countGeki)));

                        rankingElements.Add(new RankingElement(@"taiko-hit100k", new Vector2(imgx2, row2), String.Format("{0}x", score.countKatu)));

                        break;
                    }
                case PlayModes.CatchTheBeat:
                    {
                        // overlays are added first to prevent a minor display bug
                        rankingElements.Add(new RankingElement(@"fruit-orange-overlay", new Vector2(imgx1, row1), null, true));

                        e = new RankingElement(@"fruit-orange", new Vector2(imgx1, row1), String.Format("{0}x", score.count300));
                        e.image.InitialColour = Color.Orange;
                        rankingElements.Add(e);

                        e = new RankingElement(@"fruit-drop", new Vector2(imgx1, row2), String.Format("{0}x", score.count100));
                        e.ChangeScale(1F, 0.6F);
                        e.image.InitialColour = Color.YellowGreen;
                        rankingElements.Add(e);

                        e = new RankingElement(@"fruit-drop", new Vector2(imgx1, row3), String.Format("{0}x", score.count50));
                        e.ChangeScale(1F, 0.4F);
                        e.ApplyRotation(4f);
                        e.image.InitialColour = Color.LightBlue;
                        rankingElements.Add(e);

                        e = new RankingElement(@"fruit-orange-overlay", new Vector2(imgx2, row1), null, true);
                        e.ChangeScale(1f, 0.4f);
                        e.ApplyRotation(0.8f);
                        rankingElements.Add(e);

                        e = new RankingElement(@"hit0", new Vector2(imgx2, row1) - new Vector2(20, 20), null, true);
                        e.ChangeScale(0.7f, 0.2f);
                        rankingElements.Add(e);

                        e = new RankingElement(@"fruit-orange", new Vector2(imgx2, row1), String.Format("{0}x", score.countMiss));
                        e.ChangeScale(1f, 0.4f);
                        e.ApplyRotation(0.8f);
                        e.image.InitialColour = Color.LightGray;
                        rankingElements.Add(e);

                        break;
                    }
                case PlayModes.OsuMania:
                    {
                        rankingElements.Add(new RankingElement(@"mania-hit300", new Vector2(imgx1, row1), String.Format("{0}x", score.count300)));

                        rankingElements.Add(new RankingElement(@"mania-hit200", new Vector2(imgx1, row2), String.Format("{0}x", score.countKatu)));

                        rankingElements.Add(new RankingElement(@"mania-hit50", new Vector2(imgx1, row3), String.Format("{0}x", score.count50)));

                        rankingElements.Add(new RankingElement(@"mania-hit300g", new Vector2(imgx2, row1), String.Format("{0}x", score.countGeki)));

                        rankingElements.Add(new RankingElement(@"mania-hit100", new Vector2(imgx2, row2), String.Format("{0}x", score.count100)));

                        rankingElements.Add(new RankingElement(@"mania-hit0", new Vector2(imgx2, row3), String.Format("{0}x", score.countMiss)));

                        break;
                    }
            }

            int row4Offset = (SkinManager.UseNewLayout ? 20 : 8);

            e = new RankingElement(@"ranking-maxcombo", new Vector2(imgx1 - 35, row4 - row4Offset), null, false, 0, Origins.TopLeft);
            e.ChangeScale(1F, 1F);
            e.image.Depth = 0.81F;

            // we're adding the text separately here because of the unusual offset.
            e.LoadScoreText(String.Format("{0}x", score.maxCombo), new Vector2(textx1 - 65, row4 + 10));
            e.text.Depth = 0.8F;
            rankingElements.Add(e);

            e = new RankingElement(@"ranking-accuracy", new Vector2(imgx2 - 58, row4 - row4Offset), null, false, 0, Origins.TopLeft);
            e.ChangeScale(1F, 1F);
            e.image.Depth = 0.81F;

            e.LoadScoreText(String.Format("{0:0.00}%", score.accuracy * 100), new Vector2(textx2 - 86, row4 + 10));
            e.text.Depth = 0.8F;
            rankingElements.Add(e);

            // show average error as hint

            string hint = "";
            if (score.errorList != null && score.errorList.Count != 0)
            {
                List<double> result = score.ErrorStatics(score.errorList);
                if (result != null)
                {
                    hint = String.Format("Accuracy:\nError: {0:0.00}ms ~ +{1:0.00}ms\nUnstable Rate: {2:0.00}\n", result[0], result[1], result[4] * 10);
                }
            }
            if (score.spinSpeedList != null && score.spinSpeedList.Count != 0)
            {
                List<double> result = score.ErrorStatics(score.spinSpeedList);
                if (result != null)
                {
                    hint += String.Format("Spin:\nSpeed: {0:000}(Max:{1:000}rpm)\nUnstable Rate: {2:0.00}\n", result[2], result[5], result[4] * 2);
                }
            }
            if (hint != "")
            {
                spriteRankingGraph.HandleInput = true;
                spriteRankingGraph.ToolTip = hint;
            }

            if (score.perfect)
            {
                e = new RankingElement(@"ranking-perfect", new Vector2(SkinManager.UseNewLayout ? 260 : 200, 430), null, false, 300);
                e.ChangeScale(1.1F, 1F);
                e.image.Depth = 0.99F;
                rankingElements.Add(e);
            }

            int timeOffset = AddRankingSprites();

            LoadRanking(timeOffset);

            if (healthGraph != null)
            {
                spriteManagerWideScrolling.Remove(healthGraph);
                healthGraph.Dispose();
            }

            healthGraph = new HealthGraph(startTime, 4000, score, new RectangleF(165, SkinManager.UseNewLayout ? 385 : 370, 186, 86));
            spriteManagerWideScrolling.Add(healthGraph);
            LoadMods();

            s_white =
                new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game,
                            Vector2.Zero, 1, false,
                            Color.TransparentWhite);
            s_white.ScaleToWindowRatio = false;
            s_white.Additive = true;
            s_white.VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight);
            Transformation t = new Transformation(TransformationType.Fade, 0.5f, 0, startTime + timeOffset + 1000, startTime + timeOffset + 2400);
            s_white.Transformations.Add(t);
            spriteManagerWideAbove.Add(s_white);
            leftPanel.Add(s_white);
        }

        protected virtual void LoadMods()
        {
            int time = 0;
            float dep = 0;
            int x = 600;
            foreach (Mods m in Enum.GetValues(typeof(Mods)))
            {
                if (ModManager.CheckActive(score.enabledMods, m))
                {
                    if (m == Mods.DoubleTime && ModManager.CheckActive(score.enabledMods, Mods.Nightcore))
                        continue;
                    if (m == Mods.SuddenDeath && ModManager.CheckActive(score.enabledMods, Mods.Perfect))
                        continue;

                    Transformation t2 =
                        new Transformation(TransformationType.Scale, 2, 1, GameBase.Time + time,
                                           GameBase.Time + time + 400);
                    Transformation t3 =
                        new Transformation(TransformationType.Fade, 0, 1, GameBase.Time + time,
                                           GameBase.Time + time + 400);
                    t2.Easing = EasingTypes.Out;
                    t3.Easing = EasingTypes.Out;

                    pSprite p =
                        new pSprite(SkinManager.Load(@"selection-mod-" + m.ToString().ToLower()),
                                    Fields.TopLeft,
                                    Origins.Centre,
                                    Clocks.Game,
                                    new Vector2(x, 260), 0.92F + dep, true,
                                    Color.TransparentWhite);
                    p.Transformations.Add(t2);
                    p.Transformations.Add(t3);
                    spriteManagerWideScrolling.Add(p);

                    time += 500;
                    dep += 0.00001f;
                    x -= 20;
                }
            }
        }

        protected virtual void LoadRanking(int timeOffset)
        {
            rankingLetter =
                new pSprite(SkinManager.Load(@"ranking-" + score.ranking), Fields.TopRight,
                            Origins.Centre,
                            Clocks.Game, new Vector2(120, SkinManager.UseNewLayout ? 200 : 170), 0.9F, true, Color.TransparentWhite);
            Transformation t =
                new Transformation(TransformationType.Scale, 2, 1, startTime + timeOffset,
                                   startTime + timeOffset + 1000);
            t.Easing = EasingTypes.In;
            rankingLetter.Transformations.Add(t);
            t =
                new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                   startTime + timeOffset + 1000);
            t.Easing = EasingTypes.In;
            rankingLetter.Transformations.Add(t);
            spriteManagerWideScrolling.Add(rankingLetter);

            if (score.ranking < Rankings.C)
            {
                pSprite p =
                    new pSprite(SkinManager.Load(@"ranking-" + score.ranking), Fields.TopRight,
                                Origins.Centre,
                                Clocks.Game, new Vector2(120, SkinManager.UseNewLayout ? 200 : 170), 0.91F, true, Color.TransparentWhite);
                p.Additive = true;

                t =
                    new Transformation(TransformationType.Scale, 1, 1.05f, startTime + timeOffset + 1000,
                                       startTime + timeOffset + 3400);
                t.Easing = EasingTypes.Out;
                p.Transformations.Add(t);
                t =
                    new Transformation(TransformationType.Fade, 1, 0, startTime + timeOffset + 1000,
                                       startTime + timeOffset + 3400);
                t.Easing = EasingTypes.Out;
                p.Transformations.Add(t);
                spriteManagerWideScrolling.Add(p);
            }
        }

        public override void Initialize()
        {
            if (Player.currentScore == null && InputManager.ReplayScore == null)
            {
                GameBase.ChangeModeInstant(OsuModes.Menu, true);
                return;
            }

            spriteManagerWideBelow = new SpriteManager(true);
            spriteManagerWideAbove = new SpriteManager(true);


            InitializeLocalScore();

            if (score == null)
            {
                GameBase.ChangeModeInstant(OsuModes.Menu, true);
                return;
            }

            if (BeatmapManager.Current == null || (!string.IsNullOrEmpty(score.fileChecksum) && BeatmapManager.Current.BeatmapChecksum != score.fileChecksum))
            {
                Beatmap lookup = BeatmapManager.GetBeatmapByChecksum(score.fileChecksum);
                if (lookup != null) BeatmapManager.Current = lookup;
            }

            if (BeatmapManager.Current == null)
            {
                GameBase.ChangeModeInstant(OsuModes.Menu, true);
                return;
            }

            scrollableArea = new pScrollableArea(new Rectangle(0, 0, GameBase.WindowWidthScaled, GameBase.WindowHeightScaled), new Vector2(GameBase.WindowWidthScaled, GameBase.WindowHeightScaled * (rankingDialog != null ? 1.875f : 1) - 60), true, 60);
            spriteManagerWideScrolling = scrollableArea.SpriteManager;
            //if (rankingDialog != null) rankingDialog.spriteManager = spriteManagerWide;

            BackButton back = new BackButton(delegate { Exit(); });
            spriteManagerWideAbove.Add(back.SpriteCollection);

            bool Spectating = InputManager.ReplayStreaming;

            lock (StreamingManager.LockReplayScore)
            {
                InputManager.ReplayMode = false;
                InputManager.ReplayStreaming = false;
                InputManager.ReplayStartTime = 0;
            }

            startTime = GameBase.Time + 300;

            KeyboardHandler.OnKeyPressed += GameBase_OnKeyPressed;
            InputManager.Bind(InputEventType.OnClick, onClick);

            backgroundOverlay =
                new pSprite(SkinManager.Load(@"ranking-background-overlay", SkinSource.Osu), Fields.TopRight,
                            Origins.Centre,
                            Clocks.Game, new Vector2(180, SkinManager.UseNewLayout ? 200 : 170), 0.05f, true, Color.White);
            backgroundOverlay.Additive = true;
            backgroundOverlay.Transformations.Add(new Transformation(TransformationType.Rotation, 0, (float)(2 * Math.PI), GameBase.Time, GameBase.Time + 20000));
            backgroundOverlay.Transformations[0].Loop = true;
            spriteManagerWideBelow.Add(backgroundOverlay);

            pSprite p =
                new pSprite(SkinManager.Load(@"ranking-title"), Fields.TopRight, Origins.TopRight,
                            Clocks.Game, new Vector2(20, 0), 0.991F, true, Color.White);
            p.ViewOffsetImmune = SkinManager.UseNewLayout;
            spriteManagerWideScrolling.Add(p);

            InitializeRankingPanel();

            stringTotalScore = String.Format(@"{0:00000000}", score.totalScore);

            pSprite detailsBack =
                 new pSprite(GameBase.WhitePixel, Fields.TopLeft,
                             Origins.TopLeft,
                             Clocks.Game,
                             new Vector2(0, 0), 0.99F, true, Color.Black);
            detailsBack.Alpha = 0.8f;
            detailsBack.VectorScale = new Vector2(GameBase.WindowWidthScaled * 1.6f, 60 * 1.6f);
            detailsBack.ViewOffsetImmune = true;
            spriteManagerWideScrolling.Add(detailsBack);

            InitializeSpecifics();

            LoadScore(score);

            base.Initialize();

            GameBase.LoadComplete();

            if (!ReplayMode) PopupRecord();

            if (ReplayMode && !Spectating)
                Progress(false);
            else if (score.scoringSectionResults.FindAll(t => t).Count > score.scoringSectionResults.FindAll(t => !t).Count)
                ApplauseChannel = AudioEngine.PlaySample(@"applause", 100, SkinSource.All);
        }

        protected virtual void InitializeRankingPanel()
        {
            spriteRankingGraph =
                new pSprite(SkinManager.Load(@"ranking-graph"), Fields.TopLeft, Origins.TopLeft,
                            Clocks.Game, new Vector2(160, SkinManager.UseNewLayout ? 380 : 360), 0.8F, true, Color.White);
            spriteManagerWideScrolling.Add(spriteRankingGraph);

            pSprite p =
                new pSprite(SkinManager.Load(@"ranking-panel"), Fields.TopLeft, Origins.TopLeft,
                            Clocks.Game, new Vector2(0, SkinManager.UseNewLayout ? 64 : 46), 0.2F, true, Color.White);
            spriteManagerWideScrolling.Add(p);
        }

        protected virtual void InitializeLocalScore()
        {
            ReplayMode = InputManager.ReplayMode;

            if (ReplayMode && (Player.currentScore == null || !ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Autoplay)))
            {
                //We are watching a replay that wasn't played by auto mod.

                if (InputManager.ReplayStreaming && Player.currentScore != null)
                {
                    score = Player.currentScore;
                    if (score.playerName.Length == 0)
                        score.playerName = StreamingManager.CurrentlySpectating.Name;
                }
                else
                {
                    score = InputManager.ReplayScore;
                    if (Player.currentScore != null && Player.currentScore.errorList != null)
                    {
                        score.errorList = new List<int>();
                        score.errorList.AddRange(Player.currentScore.errorList);
                    }
                    if (score.isOnline)
                    {
                        //check for a matching local replay based on online score id (allow watching replays based on online ranking.
                        List<Score> localScores = ScoreManager.FindScores(score.fileChecksum, score.playMode);
                        if (localScores != null)
                        {
                            Score match = localScores.Find(s => s.onlineId == score.onlineId);
                            if (match != null && match.replayLocalPresent)
                                score = match;
                        }
                    }

                    score.LoadLocalData();

                    if (score.rawGraph != null)
                        score.ReadGraphData(score.rawGraph);
                }
            }
            else
                loadLocalUserScore(true);
        }

        protected void loadLocalUserScore(bool showRankingDialog)
        {
            score = Player.currentScore;
            if (score.pass && !ReplayMode && !ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Relax) && !ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Relax2))
            {
                if (BeatmapManager.Current.onlinePersonalScore != null)
                {
                    //global ranking
                    if (score.totalScore > BeatmapManager.Current.onlinePersonalScore.totalScore)
                        BeatmapManager.Current.PlayerRank = score.ranking;
                }
                else
                {
                    //local ranking
                    if (BeatmapManager.Current.PlayerRank == Rankings.N)
                        BeatmapManager.Current.PlayerRank = score.ranking;
                    else
                    {
                        List<Score> s = ScoreManager.FindScores(BeatmapManager.Current.BeatmapChecksum, score.playMode);
                        bool noLocalScores = s == null || s.Count == 0;

                        if (noLocalScores)
                        {
                            if (score.ranking < BeatmapManager.Current.PlayerRank)
                                BeatmapManager.Current.PlayerRank = score.ranking;
                        }
                        else if (s.FindAll(sc => sc.totalScore >= score.totalScore).Count == 0)
                            BeatmapManager.Current.PlayerRank = score.ranking;
                    }
                }

                if (showRankingDialog)
                    rankingDialog = new RankingDialog(this);
            }
        }


        protected virtual void InitializeSpecifics()
        {
            pText pt =
                new pText(BeatmapManager.Current.DisplayTitle, 22, new Vector2(0, 0), Vector2.Zero, 0.991F, true,
                          Color.White, false);
            pt.ViewOffsetImmune = true;
            spriteManagerWideScrolling.Add(pt);

            pt =
                new pText("Beatmap by " + BeatmapManager.Current.Creator, 16, new Vector2(1, 20), Vector2.Zero, 0.991F,
                          true, Color.White, false);
            pt.ViewOffsetImmune = true;
            spriteManagerWideScrolling.Add(pt);

            pt = new pText(string.Format("Played by {0} on {1}.", score.playerName, score.date.ToLocalTime()), 16, new Vector2(1, 34), Vector2.Zero,
                           0.991F, true, Color.White, false);
            pt.ViewOffsetImmune = true;
            spriteManagerWideScrolling.Add(pt);

            float buttonPosition = 360;

            if (!ReplayMode)
            {
                if (SkinManager.UseNewLayout)
                {
                    b_retry = new pSprite(SkinManager.Load(@"pause-retry"), Fields.TopRight,
                                    Origins.CentreRight,
                                    Clocks.Game,
                                    new Vector2(0, buttonPosition), 0.94F, true,
                                    new Color(255, 255, 255, 178));
                }
                else
                {
                    b_retry =
                        new pSprite(SkinManager.Load(@"ranking-retry"), Fields.TopRight,
                                    Origins.CentreRight,
                                    Clocks.Game,
                                    new Vector2(0, buttonPosition), 0.94F, true,
                                    new Color(255, 255, 255, 178));
                }
                b_retry.OnClick += retry_OnClick;
                b_retry.HandleInput = true;
                b_retry.HoverEffect = new Transformation(TransformationType.Fade, 0.7F, 1, 0, 200);
                spriteManagerWideScrolling.Add(b_retry);

                buttonPosition += 60;
            }

            if ((score.replay != null && score.replay.Count > 0) ||
                score.rawReplayCompressed != null ||
                score.replayLocalPresent ||
                (ConfigManager.sRankType == Select.RankingType.Top && (score.onlineRank <= 40 || (score.onlineRank <= 50 && score.date > new DateTime(2013, 07, 01))) && score.isOnline))
            {
                if (!GameBase.Tournament)
                {
                    if (SkinManager.UseNewLayout)
                    {

                        b_replay =
            new pSprite(SkinManager.Load(@"pause-replay"), Fields.TopRight,
                        Origins.CentreRight,
                        Clocks.Game,
                        new Vector2(0, buttonPosition), 0.94F, true,
                        new Color(255, 255, 255, 178));
                    }
                    else
                    {
                        b_replay =
            new pSprite(SkinManager.Load(@"ranking-replay"), Fields.TopRight,
                        Origins.CentreRight,
                        Clocks.Game,
                        new Vector2(0, buttonPosition), 0.94F, true,
                        new Color(255, 255, 255, 178));
                    }


                    b_replay.OnClick += replay_OnClick;
                    b_replay.HandleInput = true;
                    b_replay.HoverEffect = new Transformation(TransformationType.Fade, 0.7F, 1, 0, 200);
                    spriteManagerWideScrolling.Add(b_replay);

                    buttonPosition += 60;
                }
            }

            if (rankingDialog != null)
            {
                onlineRankingButton = new pButton("▼ Online Ranking ▼", new Vector2(GameBase.WindowWidthScaled / 2 - 100, GameBase.WindowHeightScaled - 26), new Vector2(200, 30), 0.95f, Color.BlueViolet, delegate { scrollableArea.ScrollToPosition(GameBase.WindowHeightScaled * 0.875f, -0.99f); }); spriteManagerWideScrolling.Add(onlineRankingButton.SpriteCollection);
            }
        }

        protected void replay_OnClick(object sender, EventArgs e)
        {
            if (!LoadingReplay && Progress(false))
                LoadReplay();
        }

        protected void retry_OnClick(object sender, EventArgs e)
        {
            Progress(false);

            if (!LoadingReplay)
            {
                AudioEngine.PlaySamplePositional(@"menuhit");
                GameBase.ChangeMode(OsuModes.Play);
            }
        }

        protected void return_OnClick(object sender, EventArgs e)
        {
            Progress(true);
        }

        protected void LoadReplay()
        {
            if (score.isOnline)
            {
                OnlineLoadReplay(score_GotReplayData);
            }
            else
            {
                if (score.replay == null || score.replay.Count == 0)
                {
                    if (score.rawReplayCompressed == null) //SaveToDisk nulls this when it activates.
                        score.LoadLocalData();

                    score.ReadReplayData();
                }

                if (score.replay != null && score.replay.Count > 0)
                {
                    AudioEngine.PlaySamplePositional(@"menuhit");

                    StreamingManager.StopSpectating(false);

                    InputManager.ReplayMode = true;
                    InputManager.ReplayScore = score;
                    Player.Seed = score.Seed;
                    GameBase.ChangeMode(OsuModes.Play);
                }
                else
                {
                    NotificationManager.ShowMessage("This score has no replay data saved!", Color.Red, 5000);
                }
            }
        }

        private void OnlineLoadReplay(GotReplayDataHandler callback)
        {
            NotificationManager.ShowMessageMassive("Downloading replay data...", 10000);
            spriteManagerWideScrolling.BlacknessTarget = 0.5f;
            LoadingReplay = true;
            score.GetReplayData();
            score.GotReplayData += callback;
        }

        protected void score_GotReplayData(object sender)
        {
            score.GotReplayData -= score_GotReplayData;


            LoadingReplay = false;

            if (score.replay != null && score.replay.Count > 0)
            {
                NotificationManager.ClearMessageMassive();

                StreamingManager.StopSpectating(false);

                AudioEngine.PlaySamplePositional(@"menuhit");
                InputManager.ReplayMode = true;
                InputManager.ReplayScore = score;
                Player.Seed = score.Seed;
                GameBase.ChangeMode(OsuModes.Play);
            }
            else
            {
                NotificationManager.ShowMessageMassive("Replay data is not available for this play!", 2000);
                spriteManagerWideScrolling.BlacknessTarget = 0;
                b_replay.FadeOut(100);
                b_replay.HandleInput = false;
            }
        }

        protected virtual bool onClick(object sender, EventArgs e)
        {
            Progress(false);
            return false;
        }


        protected bool Progress(bool allowExit)
        {
            if (GameBase.Time - startTime < 5000 && !forceDisplay)
            {
                forceDisplay = true;
                startTime = GameBase.Time - 7000;
                lock (spriteManagerWideScrolling.SpriteList)
                {
                    foreach (pDrawable p in spriteManagerWideScrolling.SpriteList)
                    {
                        foreach (Transformation t in p.Transformations)
                        {
                            if (!t.Loop)
                            {
                                t.Time1 = startTime - 200;
                                t.Time2 = startTime - 100;
                            }
                        }
                    }
                }

                spriteManagerWideAbove.Remove(s_white);

                return false;
            }

            if (allowExit) Exit();

            return true;
        }

        protected virtual void Exit()
        {
            AudioEngine.PlaySamplePositional(@"menuback");

#if ARCADE
            GameBase.ChangeMode(OsuModes.Menu);
#else
            GameBase.ChangeMode(OsuModes.SelectPlay);
#endif
        }

        protected virtual void CloseRecord()
        {
            if (rankingDialog != null)
                rankingDialog.Close();
        }

        protected virtual void PopupRecord()
        {
            if (rankingDialog == null || rankingDialog.IsDisplayed) return;

            rankingDialog.Display();
            rankingDialog.DisplayAfter();

            rankingDialog.spriteManager.BaseOffset = new Vector2(0, GameBase.WindowHeightScaled);
            scrollableArea.AddChild(rankingDialog.spriteManager);


        }
    }

    class HealthGraph : pDrawable
    {
        List<ColouredLine> graphLine = new List<ColouredLine>();
        List<Line> drawList = new List<Line>();

        Score score;
        int startTime;
        int lengthMs;
        double graphLength;
        internal bool Finished = false;

        internal RectangleF initialRegion;

        internal HealthGraph(int startTime, int lengthMS, Score score, RectangleF region)
            : base(true)
        {
            Depth = 0.85f;
            this.score = score;
            this.startTime = startTime;
            this.lengthMs = lengthMS;
            this.initialRegion = region;

            if (score.hpGraph != null && score.hpGraph.Count > 0)
            {
                float time1 = score.hpGraph[0].X;
                float time2 = score.hpGraph[score.hpGraph.Count - 1].X;
                float ratio = (float)(region.Width) / (time2 - time1);

                List<Vector2> lowResGraph = new List<Vector2>();
                lowResGraph.AddRange(score.hpGraph);

                while (lowResGraph.Count > 100)
                {
                    for (int i = lowResGraph.Count - 1; i >= 0 && i < lowResGraph.Count; i -= 2)
                        lowResGraph.RemoveAt(i);
                }

                Vector2[] va = lowResGraph.ToArray();

                for (int i = 0; i < va.Length; i++)
                {

                    va[i].X = GameBase.WindowRatio * ((va[i].X - time1) * ratio + region.X);
                    bool ok = va[i].Y > 0.5;
                    va[i].Y = GameBase.WindowRatio * ((1 - va[i].Y) * region.Height + region.Y);

                    if (i > 0)
                    {
                        ColouredLine l = new ColouredLine(va[i - 1], va[i], (ok ? Color.YellowGreen : Color.Red));
                        graphLine.Add(l);
                        graphLength += l.rho;
                    }
                }
            }
        }

        public override UpdateResult Update()
        {
            return base.Update();
        }

        public override bool Draw()
        {
            if (!base.Draw())
                return false;

            int currentTime = GameBase.Time - startTime;
            if (graphLine.Count > 0 && (currentTime <= lengthMs || !Finished))
            {
                drawList.Clear();
                double lengthRequired = graphLength * (Math.Min(lengthMs, (float)currentTime / lengthMs));

                Vector2 drawOffset = (SpriteManager.Current.ScrollOffset - SpriteManager.Current.ViewOffset) * GameBase.WindowRatio;

                foreach (ColouredLine l in graphLine)
                {
                    if (l.rho < lengthRequired)
                    {
                        ColouredLine c = l.Clone() as ColouredLine;

                        c.p1 += drawOffset;
                        c.p2 += drawOffset;

                        drawList.Add(c);
                        lengthRequired -= l.rho;
                    }
                    else
                    {
                        drawList.Add(new ColouredLine(l.p1 + drawOffset, l.p1 + Vector2.Normalize(l.p2 - l.p1) * (float)lengthRequired + drawOffset, l.colour));
                        break;
                    }
                }
            }

            if (drawList.Count > 0)
                GameBase.LineManager.Draw(drawList, GameBase.Tournament ? 1 : 2, Color.Green, 0, "StandardNoGradient", true);

            return true;
        }
    }
}