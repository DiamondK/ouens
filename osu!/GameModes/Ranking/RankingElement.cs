﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Graphics.Skinning;
using osu.Helpers;
using osu_common.Helpers;
using osu.Graphics;

namespace osu.GameModes.Ranking
{
    class RankingElement
    {
        internal pSprite image;
        internal pSprite text;
        internal bool isOverlay;
        internal int startTime = GameBase.Time + 300;

        const int length = 300;

        public RankingElement(string elementName, Vector2 location, string scoreText = null, 
                              bool isOverlay = false, int timeOffset = 0, Origins origin = Origins.Centre)
        {
            pSprite p;
            Transformation t;

            int textoffset = SkinManager.UseNewLayout ? -16 : -25;

            p =
                new pSprite(SkinManager.LoadFirst(elementName), Fields.TopLeft, origin, Clocks.Game, 
                            location, isOverlay ? 0.91F : 0.9F, true, Color.TransparentWhite);
            p.Alpha = 0;

            t =
                new Transformation(TransformationType.Scale, 1F, 0.5F,
                                   startTime + timeOffset, startTime + timeOffset + length);
            t.Easing = EasingTypes.In;
            p.Transformations.Add(t);
            t =
                new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                   startTime + timeOffset + length);
            t.Easing = EasingTypes.In;
            p.Transformations.Add(t);

            image = p;
            this.isOverlay = isOverlay;

            if (scoreText == null) return;

            LoadScoreText(scoreText, new Vector2(location.X + 40, location.Y + textoffset));
        }

        internal virtual void LoadScoreText(string scoreText, Vector2 location)
        {
            pSprite p;
            Transformation t;

            const int timeOffset = 200;

            Vector2 tMove = new Vector2(40, 0);

            p =
                    new pSpriteText(scoreText, SkinManager.Current.FontScore,
                                    SkinManager.Current.FontScoreOverlap, Fields.TopLeft,
                                    Origins.TopLeft, Clocks.Game,
                                    location, 0.8F, true, Color.TransparentWhite);
            p.Scale = 1.12F;
            t =
                new Transformation(p.Position - tMove, p.Position, startTime + timeOffset,
                                   startTime + timeOffset + length);
            t.Easing = EasingTypes.Out;
            p.Transformations.Add(t);
            t =
                new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                   startTime + timeOffset + length);
            t.Easing = EasingTypes.Out;
            p.Transformations.Add(t);

            text = p;
        }

        internal virtual void ApplyRotation(float amount)
        {
            if (image == null) return;

            image.Rotation = amount;
        }

        internal virtual void ChangeScale(float scaleStart, float scaleEnd)
        {
            if (image == null) return;

            Transformation p = image.Transformations.Find(r => r.Type == TransformationType.Scale);
            if (p == null) return;

            p.StartFloat = scaleStart;
            p.EndFloat = scaleEnd;
        }
    }
}
