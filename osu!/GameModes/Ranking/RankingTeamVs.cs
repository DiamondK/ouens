﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.GameModes.Online;
using osu.GameModes.Play;
using osu.GameModes.Play.Components;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Primitives;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu.Online.Drawable;
using osu_common;

namespace osu.GameModes.Ranking
{
    internal class RankingTeamVs : RankingVs
    {
        private readonly List<pSprite> scoreSpritesBlue = new List<pSprite>();
        private readonly List<pSprite> scoreSpritesRed = new List<pSprite>();
        private readonly List<pSprite> staticSprites = new List<pSprite>();

        private PlayModes playMode;

        private HealthGraph team1HPgraph;
        //private int startTime;

        private Score team1Score;
        private pTabCollection team1Tabs;
        private HealthGraph team2HPgraph;
        private Score team2Score;
        private pTabCollection team2Tabs;

        internal RankingTeamVs(Game game)
            : base(game)
        {
        }

        public override void Initialize()
        {

            bool Spectating = InputManager.ReplayStreaming;

#if DEBUG
            if (GameBase.PREDEFINED_TEST)
            {
                PlayerVs.Match = new ClientSideMatch(MatchTypes.Standard, MatchScoringTypes.Score, MatchTeamTypes.TeamVs, PlayModes.Osu, "poop game", null, 8, "poop map", "poop", 1, Mods.Easy, 1, osu_common.Bancho.Objects.MultiSpecialModes.None, 1);

                Player.ScoreBoard = new Scoreboard(5, null, new Vector2(0, 100), true);
                Player.currentScore = new Score();
                PlayerVs.lastScoreEntries = new List<ScoreboardEntryExtended>();
                for (int i = 1; i <= 8; i++)
                {
                    ScoreboardEntryExtended sbe = new ScoreboardEntryExtended(i, new User(i, "player" + i), MatchScoringTypes.Score, PlayModes.Osu, i < 5 ? SlotTeams.Blue : SlotTeams.Red);

                    sbe.Score = new Score();

                    Player.ScoreBoard.Entries.Add(sbe);
                    PlayerVs.lastScoreEntries.Add(sbe);
                }

                PlayerVs.TeamOverlay = new Play.Components.Multiplayer.TeamplayOverlay(null, new SpriteManager());
                PlayerVs.TeamOverlay.team1display.currentScore = 1000000;
                PlayerVs.TeamOverlay.team2display.currentScore = 4000000;
            }
#endif

            base.Initialize();

            Player.ScoreBoard.spriteManager.WidescreenAutoOffset = true;
            Player.ScoreBoard.spriteManager.Alpha = 1;

            lock (StreamingManager.LockReplayScore)
            {
                InputManager.ReplayMode = false;
                InputManager.ReplayStreaming = false;
                InputManager.ReplayStartTime = 0;
            }

            pText text = null;
            SlotTeams winner = PlayerVs.TeamOverlay.winner;
            if (PlayerVs.TeamFailed(SlotTeams.Blue))
            {
                text = new pText("Blue team failed!", 20, new Vector2(GameBase.WindowWidthScaled / 2, 85), Vector2.Zero, 1, true, Color.Blue, false);
            }
            else if (PlayerVs.TeamFailed(SlotTeams.Red))
            {
                text = new pText("Red team failed!", 20, new Vector2(GameBase.WindowWidthScaled / 2, 85), Vector2.Zero, 1, true, Color.Red, false);
            }
            else
            {
                switch (winner)
                {
                    case SlotTeams.Blue:
                        text = new pText("Blue team wins!", 20, new Vector2(GameBase.WindowWidthScaled / 2, 85), Vector2.Zero, 1, true, Color.Blue,
                                         false);
                        break;
                    case SlotTeams.Red:
                        text = new pText("Red team wins!", 20, new Vector2(GameBase.WindowWidthScaled / 2, 85), Vector2.Zero, 1, true, Color.Red,
                                         false);
                        break;
                }
            }

            text.TextBold = true;
            text.Origin = Origins.Centre;
            spriteManagerWideScrolling.Add(text);

            int tabHeight = 113;

            //add
            team1Tabs = new pTabCollection(spriteManagerWideScrolling, 3, new Vector2(56, tabHeight), 0.8f, true, Color.BlueViolet, 40);
            team2Tabs = new pTabCollection(spriteManagerWideScrolling, 3, new Vector2(GameBase.WindowWidthScaled - 224, tabHeight), 0.8f, true, Color.BlueViolet, 40);
            team1Tabs.Add("Total", 0);
            team2Tabs.Add("Total", 0);
            team1Tabs.SetSelected(0);
            team2Tabs.SetSelected(0);
            team1Tabs.OnTabChanged +=
                delegate(object sender, EventArgs e) { loadScores(SlotTeams.Blue, (int)sender, false); };
            team2Tabs.OnTabChanged +=
                delegate(object sender, EventArgs e) { loadScores(SlotTeams.Red, (int)sender, false); };

            int count = PlayerVs.lastScoreEntries.Count;
            for (int i = 0; i < count; i++)
            {
                ScoreboardEntry entry = PlayerVs.lastScoreEntries[i];
                if (entry != null)
                    (entry.Team == SlotTeams.Blue ? team1Tabs : team2Tabs).Add(entry.name, i + 1);
            }

            pSprite scoreBackground1 = new pSprite(SkinManager.Load(@"ranking-panel"), Fields.TopLeft,
                                                   Origins.TopLeft,
                                                   Clocks.Game,
                                                   new Vector2(1, SkinManager.UseNewLayout ? 107 : 97), 0.79f, true, Color.White);
            scoreBackground1.Alpha = 1.0f;
            scoreBackground1.VectorScale = new Vector2(0.715f, 0.77f);

            pSprite scoreBackground2 = new pSprite(SkinManager.Load(@"ranking-panel"), Fields.TopLeft,
                                                   Origins.TopLeft,
                                                   Clocks.Game,
                                                   new Vector2(GameBase.WindowWidthScaled - 279, SkinManager.UseNewLayout ? 107 : 97), 0.79f, true, Color.White);
            scoreBackground2.Alpha = 1.0f;
            scoreBackground2.VectorScale = new Vector2(0.715f, 0.77f);
            spriteManagerWideScrolling.Add(scoreBackground1);
            spriteManagerWideScrolling.Add(scoreBackground2);
            initializeStatics();

            //cache scores for more efficiency
            team1Score = getTeamScore(SlotTeams.Blue);
            team2Score = getTeamScore(SlotTeams.Red);

            loadScores(SlotTeams.Blue, 0, true);
            loadScores(SlotTeams.Red, 0, true);

            playMode = Player.Mode;

            GameBase.LoadComplete();
        }

        protected override void InitializeRankingPanel()
        {
            //base.InitializeRankingPanel();
        }

        protected override void InitializeLocalScore()
        {
            base.InitializeLocalScore();
        }

        protected override void InitializeSpecifics()
        {
            base.InitializeSpecifics();
        }

        protected override void LoadRanking(int timeOffset)
        {

        }

        protected override void LoadScore(Score score)
        {

        }

        private Score getTeamScore(SlotTeams teamId)
        {
            Score teamScore = ScoreFactory.Create(playMode, BeatmapManager.Current);

            int teamCount = 0;

            teamScore.hpGraph = new List<Vector2>();
            //we need the combined hpgaphs to be sorted
            //because it's probably out of order due to latency
            SortedDictionary<float, float> timeHealth = new SortedDictionary<float, float>();

            for (int i = 0; i < PlayerVs.lastScoreEntries.Count; i++)
            {
                ScoreboardEntry entry = PlayerVs.lastScoreEntries[i];
                if (entry != null)
                {
                    Score score = entry.Score;
                    if (score != null)
                    {
                        if (entry.Team == teamId)
                        {
                            teamScore.maxCombo += score.maxCombo;
                            teamScore.totalScore += score.totalScore;
                            teamScore.count300 += score.count300;
                            teamScore.countGeki += score.countGeki;
                            teamScore.count100 += score.count100;
                            teamScore.countKatu += score.countKatu;
                            teamScore.count50 += score.count50;
                            teamScore.countMiss += score.countMiss;

                            if (score.hpGraph != null)
                            {
                                if (PlayerVs.Match.matchTeamType == MatchTeamTypes.TeamVs)
                                {
                                    if (teamCount == 0)
                                        teamScore.hpGraph.AddRange(score.hpGraph);
                                    else // calculate average hpGraph
                                        for (int j = Math.Min(teamScore.hpGraph.Count, score.hpGraph.Count) - 1;
                                             j >= 0;
                                             j--)
                                            teamScore.hpGraph[j] = new Vector2(teamScore.hpGraph[j].X,
                                                                               (teamScore.hpGraph[j].Y * teamCount +
                                                                                score.hpGraph[j].Y) /
                                                                               (teamCount + 1));
                                }
                                else
                                {
                                    foreach (Vector2 TimeHealth in score.hpGraph)
                                        // Will be sorted automaticlly
                                        if (!timeHealth.ContainsKey(TimeHealth.X))
                                            timeHealth.Add(TimeHealth.X, TimeHealth.Y);
                                }
                            }

                            teamCount++;
                        }
                    }
                }
            }

            if (PlayerVs.Match.matchTeamType == MatchTeamTypes.TagTeamVs)
                foreach (KeyValuePair<float, float> th in timeHealth)
                    teamScore.hpGraph.Add(new Vector2(th.Key, th.Value));


            return teamScore;
        }

        private void loadScores(SlotTeams teamId, int tabId, bool slowAnimations)
        {
            bool isFruits = playMode == PlayModes.CatchTheBeat;

            int startTime = GameBase.Time;

            Score scoreEntry = tabId != 0 ? PlayerVs.lastScoreEntries[tabId - 1].Score : (teamId == SlotTeams.Blue ? team1Score : team2Score);

            //load the right scoreSprites we are about to replace
            List<pSprite> scoreSprites = teamId == SlotTeams.Blue ? scoreSpritesBlue : scoreSpritesRed;
            HealthGraph currentHpGraph;
            spriteManagerWideScrolling.RemoveRange(scoreSprites);
            scoreSprites.Clear();
            //create new scoresprites
            const int textx1 = 80;
            const int imgx1 = 40;
            const int textx2 = 280;
            //const int imgx2 = 240;

            const int row1 = 160;
            const int row2 = 220;
            const int row3 = 280;
            const int row4 = 320;

            int textoffset = SkinManager.UseNewLayout ? -16 : -25;

            Vector2 tMove = new Vector2(0, 20);

            int timeOffset = slowAnimations ? 1000 : 0;
            int timeOffset2 = slowAnimations ? 1300 : 0;
            int length = slowAnimations ? 400 : 20;
            pSprite p;
            Transformation t;
            p =
                new pSpriteText(String.Format("{0}x", scoreEntry.count300), SkinManager.Current.FontScore,
                                SkinManager.Current.FontScoreOverlap, Fields.TopLeft,
                                Origins.TopLeft, Clocks.Game,
                                new Vector2(textx1, row1 + textoffset), 0.8F, true, Color.TransparentWhite);
            p.Scale = 1.12F;
            t =
                new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset2,
                                   startTime + timeOffset2 + length);
            timeOffset2 += length;
            t.Easing = EasingTypes.Out;
            p.Transformations.Add(t);
            scoreSprites.Add(p);

            p =
                new pSpriteText(String.Format("{0}x", scoreEntry.count100), SkinManager.Current.FontScore,
                                SkinManager.Current.FontScoreOverlap, Fields.TopLeft,
                                Origins.TopLeft, Clocks.Game,
                                new Vector2(textx1, row2 + textoffset), 0.8F, true, Color.TransparentWhite);
            p.Scale = 1.12F;

            t =
                new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset2,
                                   startTime + timeOffset2 + length);
            timeOffset2 += length;
            t.Easing = EasingTypes.Out;
            p.Transformations.Add(t);
            scoreSprites.Add(p);

            p =
                new pSpriteText(String.Format("{0}x", scoreEntry.count50), SkinManager.Current.FontScore,
                                SkinManager.Current.FontScoreOverlap, Fields.TopLeft,
                                Origins.TopLeft, Clocks.Game,
                                new Vector2(textx1, row3 + textoffset), 0.8F, true, Color.TransparentWhite);
            p.Scale = 1.12F;
            t =
                new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset2,
                                   startTime + timeOffset2 + length);
            timeOffset2 += length;
            t.Easing = EasingTypes.Out;
            p.Transformations.Add(t);
            scoreSprites.Add(p);

            if (!isFruits)
            {
                p =
                    new pSpriteText(String.Format("{0}x", scoreEntry.countGeki), SkinManager.Current.FontScore,
                                    SkinManager.Current.FontScoreOverlap, Fields.TopLeft,
                                    Origins.TopLeft, Clocks.Game,
                                    new Vector2(textx2, row1 + textoffset), 0.8F, true, Color.TransparentWhite);
                p.Scale = 1.12F;

                t =
                    new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset2,
                                       startTime + timeOffset2 + length);
                timeOffset2 += length;
                t.Easing = EasingTypes.Out;
                p.Transformations.Add(t);
                scoreSprites.Add(p);

                p =
                    new pSpriteText(String.Format("{0}x", scoreEntry.countKatu), SkinManager.Current.FontScore,
                                    SkinManager.Current.FontScoreOverlap, Fields.TopLeft,
                                    Origins.TopLeft, Clocks.Game,
                                    new Vector2(textx2, row2 + textoffset), 0.8F, true, Color.TransparentWhite);
                p.Scale = 1.12F;
                t =
                    new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset2,
                                       startTime + timeOffset2 + length);
                timeOffset2 += length;
                t.Easing = EasingTypes.Out;
                p.Transformations.Add(t);
                scoreSprites.Add(p);
            }

            p =
                new pSpriteText(String.Format("{0}x", scoreEntry.countMiss), SkinManager.Current.FontScore,
                                SkinManager.Current.FontScoreOverlap, Fields.TopLeft,
                                Origins.TopLeft, Clocks.Game,
                                new Vector2(textx2, (isFruits ? row1 : row3) + textoffset), 0.8F, true,
                                Color.TransparentWhite);
            p.Scale = 1.12F;
            t =
                new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset2,
                                   startTime + timeOffset2 + length);
            timeOffset2 += length;
            t.Easing = EasingTypes.Out;
            p.Transformations.Add(t);
            scoreSprites.Add(p);

            p =
                new pSpriteText(tabId == 0 ? "xxxx" : String.Format("{0}x", scoreEntry.maxCombo),
                                SkinManager.Current.FontScore,
                                SkinManager.Current.FontScoreOverlap, Fields.TopLeft,
                                Origins.TopLeft, Clocks.Game,
                                new Vector2(textx1 - 65, row4 + 10), 0.8F, true, Color.TransparentWhite);
            p.Scale = 1.12F;
            t =
                new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset2,
                                   startTime + timeOffset2 + length);
            timeOffset2 += length;
            t.Easing = EasingTypes.Out;
            p.Transformations.Add(t);
            scoreSprites.Add(p);

            p =
                new pSpriteText(String.Format("{0:0.00}%", scoreEntry.accuracy * 100), SkinManager.Current.FontScore,
                                SkinManager.Current.FontScoreOverlap, Fields.TopLeft,
                                Origins.TopLeft, Clocks.Game,
                                new Vector2(textx2 - 86, row4 + 10), 0.8F, true, Color.TransparentWhite);
            p.Scale = 1.12F;
            t =
                new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset2,
                                   startTime + timeOffset2 + length);
            t.Easing = EasingTypes.Out;
            p.Transformations.Add(t);
            scoreSprites.Add(p);
            timeOffset2 += length;

            string stringTotalScore = String.Format("{0:00000000}", scoreEntry.totalScore);

            pSpriteText ps =
                new pSpriteText(stringTotalScore, SkinManager.Current.FontScore, SkinManager.Current.FontScoreOverlap,
                                Fields.TopLeft, Origins.Centre, Clocks.Game,
                /*new Vector2(180, row4 + 110)*/ new Vector2(180, 94), 0.8F, true, Color.White);
            ps.Scale = SkinManager.UseNewLayout ? 1.3f : 1.2f;
            if (SkinManager.UseNewLayout)
                ps.SpacingOverlap = -2;
            ps.Alpha = 0;
            t =
                new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset2,
                                   startTime + timeOffset2 + length * 2);
            t.Easing = EasingTypes.Out;
            ps.Transformations.Add(t);
            scoreSprites.Add(ps);


            int offsetX = 0;
            switch (teamId)
            {
                case SlotTeams.Blue:
                    break;
                case SlotTeams.Red:
                    offsetX = GameBase.WindowWidthScaled - 278;
                    break;
            }
            //scale and move the sprites to the correct panel
            float scale = 0.6f * 1.10f;
            for (int j = 0; j < scoreSprites.Count; j++)
            {
                pSprite pp = scoreSprites[j];
                pp.Scale *= scale;
                pp.Position.X = imgx1 + (pp.Position.X - imgx1) * scale + offsetX;
                pp.Position.Y = row1 + (pp.Position.Y - row1) * scale + 18;
                spriteManagerWideScrolling.Add(pp);
            }

            //finally create the hpGraph

            currentHpGraph = new HealthGraph(startTime, slowAnimations ? 2000 : 4000, scoreEntry, new RectangleF(60 + offsetX,  SkinManager.UseNewLayout ? 346 : 331, 215 - 65, 420 - 352));

            if (teamId == SlotTeams.Blue)
                team1HPgraph = currentHpGraph;
            else
                team2HPgraph = currentHpGraph;

            spriteManagerWideScrolling.Add(currentHpGraph);
        }

        private void initializeStatics()
        {
            //const int textx1 = 80;
            const int imgx1 = 40;
            //const int textx2 = 280;
            const int imgx2 = 240;

            const int row1 = 160;
            const int row2 = 220;
            const int row3 = 280;
            const int row4 = 320;

            int timeOffset = 1000;

            float staticScale = 0.65f;
            Vector2 tMove = new Vector2(40, 0);
            const int length = 400;
            int startTime = GameBase.Time;

            pSprite p;
            Transformation t;

            bool isFruits = playMode == PlayModes.CatchTheBeat;
            //offset for the first panel
            int offsetX = 0;
            int secondSpritesOffset = 0;
            //draw statics for both panels
            for (int i = 0; i < 2; i++)
            {
                timeOffset = 1000;

                if (isFruits)
                {
                    p =
                        new pSprite(SkinManager.Load(@"fruit-orange"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Game, new Vector2(imgx1, row1), 0.9F, true, Color.Orange);
                    p.Alpha = 0;

                    t =
                        new Transformation(TransformationType.Scale, 1.2f * staticScale, 0.65F * staticScale,
                                           startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    t =
                        new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    staticSprites.Add(p);

                    p =
                        new pSprite(SkinManager.Load(@"fruit-orange-overlay"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Game, new Vector2(imgx1, row1), 0.91F, true, Color.TransparentWhite);
                    t =
                        new Transformation(TransformationType.Scale, 1.2f * staticScale, 0.65F * staticScale,
                                           startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    t =
                        new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                           startTime + timeOffset + length);
                    timeOffset += length;
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    staticSprites.Add(p);


                    p =
                        new pSprite(SkinManager.Load(@"fruit-drop"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Game, new Vector2(imgx1, row2), 0.9F, true, Color.YellowGreen);
                    p.Alpha = 0;

                    t =
                        new Transformation(TransformationType.Scale, 1.2f * staticScale, 0.77F * staticScale,
                                           startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    t =
                        new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                           startTime + timeOffset + length);
                    timeOffset += length;
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    staticSprites.Add(p);


                    p =
                        new pSprite(SkinManager.Load(@"fruit-drop"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Game, new Vector2(imgx1, row3), 0.9F, true, Color.LightBlue);
                    p.Alpha = 0;
                    p.Rotation = 4;

                    t =
                        new Transformation(TransformationType.Scale, 1.15f * staticScale, 0.57F * staticScale,
                                           startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    t =
                        new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                           startTime + timeOffset + length);
                    timeOffset += length;
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    staticSprites.Add(p);


                    p =
                        new pSprite(SkinManager.Load(@"fruit-orange"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Game, new Vector2(imgx2, row1), 0.9F, true, Color.LightGray);
                    p.Alpha = 0;
                    p.Rotation = 0.8f;

                    t =
                        new Transformation(TransformationType.Scale, 1.15f * staticScale, 0.57F * staticScale,
                                           startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    t =
                        new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    staticSprites.Add(p);

                    p =
                        new pSprite(SkinManager.Load(@"fruit-orange-overlay"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Game, new Vector2(imgx2, row1), 0.91F, true, Color.TransparentWhite);
                    p.Rotation = 0.8f;
                    t =
                        new Transformation(TransformationType.Scale, 1.15f * staticScale, 0.57F * staticScale,
                                           startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    t =
                        new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    staticSprites.Add(p);

                    p =
                        new pSprite(SkinManager.Load(@"hit0"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Game, new Vector2(imgx2, row1) - new Vector2(20, 20), 0.9F, true,
                                    Color.TransparentWhite);

                    t =
                        new Transformation(TransformationType.Scale, 0.9f * staticScale, 0.35F * staticScale,
                                           startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    t =
                        new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                           startTime + timeOffset + length);
                    timeOffset += length;
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    staticSprites.Add(p);
                }
                else
                {
                    p = new pSprite(SkinManager.Load(@"hit300"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Game, new Vector2(imgx1, row1), 0.9F, true, Color.TransparentWhite);
                    t =
                        new Transformation(TransformationType.Scale, 1 * staticScale, 0.5F * staticScale,
                                           startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    t =
                        new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                           startTime + timeOffset + length);
                    timeOffset += length;
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    staticSprites.Add(p);


                    p =
                        new pSprite(SkinManager.Load(@"hit100"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Game, new Vector2(imgx1, row2), 0.9F, true, Color.TransparentWhite);
                    t =
                        new Transformation(TransformationType.Scale, 1 * staticScale, 0.5F * staticScale,
                                           startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    t =
                        new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                           startTime + timeOffset + length);
                    timeOffset += length;
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    staticSprites.Add(p);

                    p =
                        new pSprite(SkinManager.Load(@"hit50"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Game, new Vector2(imgx1, row3), 0.9F, true, Color.TransparentWhite);
                    t =
                        new Transformation(TransformationType.Scale, 1 * staticScale, 0.5F * staticScale,
                                           startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    t =
                        new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                           startTime + timeOffset + length);
                    timeOffset += length;
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    staticSprites.Add(p);


                    p =
                        new pSprite(SkinManager.Load(@"hit300g"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Game, new Vector2(imgx2, row1), 0.9F, true, Color.TransparentWhite);
                    t =
                        new Transformation(TransformationType.Scale, 1 * staticScale, 0.5F * staticScale,
                                           startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    t =
                        new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                           startTime + timeOffset + length);
                    timeOffset += length;
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    staticSprites.Add(p);


                    p =
                        new pSprite(SkinManager.Load(@"hit100k"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Game, new Vector2(imgx2, row2), 0.9F, true, Color.TransparentWhite);
                    t =
                        new Transformation(TransformationType.Scale, 1 * staticScale, 0.5F * staticScale,
                                           startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    t =
                        new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                           startTime + timeOffset + length);
                    timeOffset += length;
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    staticSprites.Add(p);


                    p =
                        new pSprite(SkinManager.Load(@"hit0"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Game, new Vector2(imgx2, row3), 0.9F, true, Color.TransparentWhite);
                    t =
                        new Transformation(TransformationType.Scale, 1 * staticScale, 0.5F * staticScale,
                                           startTime + timeOffset,
                                           startTime + timeOffset + length);
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    t =
                        new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                           startTime + timeOffset + length);
                    timeOffset += length;
                    t.Easing = EasingTypes.In;
                    p.Transformations.Add(t);
                    staticSprites.Add(p);
                }


                p =
                    new pSprite(SkinManager.Load(@"ranking-maxcombo"), Fields.TopLeft, Origins.TopLeft,
                                Clocks.Game, new Vector2(imgx1 - 35, row4 - (SkinManager.UseNewLayout ? 20 : 8)), 0.81F, true, Color.TransparentWhite);
                t =
                    new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                       startTime + timeOffset + length);
                timeOffset += length;
                t.Easing = EasingTypes.In;
                p.Transformations.Add(t);
                staticSprites.Add(p);


                p =
                    new pSprite(SkinManager.Load(@"ranking-accuracy"), Fields.TopLeft, Origins.TopLeft,
                                Clocks.Game, new Vector2(imgx2 - 58, row4 - (SkinManager.UseNewLayout ? 20 : 8)), 0.81F, true, Color.TransparentWhite);
                t =
                    new Transformation(TransformationType.Fade, 0, 1, startTime + timeOffset,
                                       startTime + timeOffset + length);
                timeOffset += length;
                t.Easing = EasingTypes.In;
                p.Transformations.Add(t);
                staticSprites.Add(p);

                p =
                    new pSprite(SkinManager.Load(@"ranking-graph"), Fields.TopLeft, Origins.TopLeft,
                                Clocks.Game, new Vector2(56, SkinManager.UseNewLayout ? 415 : 375), 0.8F, true, Color.White);
                p.Scale = 1.22f;
                staticSprites.Add(p);


                float scale = 0.60f * 1.10f;
                //the lazy way to get the positions and scale right
                int j = 0;
                for (j = secondSpritesOffset; j < staticSprites.Count; j++)
                {
                    pSprite pp = staticSprites[j];
                    pp.Scale *= scale;
                    pp.Position.X = imgx1 + (pp.Position.X - imgx1) * scale + offsetX;
                    pp.Position.Y = row1 + (pp.Position.Y - row1) * scale + 18;
                    spriteManagerWideScrolling.Add(pp);
                }
                secondSpritesOffset = j;
                //offset for the second panel
                offsetX = GameBase.WindowWidthScaled - 279;
            }
        }

        public override void Draw()
        {
            base.Draw();



            Player.ScoreBoard.Draw();
        }

        public override void Update()
        {
            base.Update();
        }

        protected override void Dispose(bool disposing)
        {
            if (Player.ScoreBoard != null)
                Player.ScoreBoard.Dispose();

            PlayerVs.DisposeStatic();

            base.Dispose(disposing);
        }

        protected void Exit()
        {
            AudioEngine.PlaySamplePositional(@"menuback");
            MatchSetup.PendingScoreUpdate = true;
            GameBase.ChangeMode(OsuModes.MatchSetup);
        }
    }
}