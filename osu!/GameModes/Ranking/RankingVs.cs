﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Components;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu.GameModes.Online;
using osu.Online;
using osu_common.Helpers;
using System.Collections.Generic;
using osu.Online.Social;

namespace osu.GameModes.Ranking
{
    internal class RankingVs : Ranking
    {
        private List<pSprite> modSprites;
        internal RankingVs(Game game)
            : base(game)
        {
        }

        public override void Draw()
        {
            base.Draw();

            Player.ScoreBoard.Draw();
        }

        protected override void Dispose(bool disposing)
        {
            if (GameBase.GameQueuedState != OsuModes.RankingVs)
            {
                PlayerVs.DisposeStatic();
                base.Dispose(disposing);
            }
        }

        protected override void LoadMods()
        {
            if (modSprites != null)
                modSprites.ForEach(s => { s.FadeOut(200); s.AlwaysDraw = false; });

            modSprites = ModManager.ShowModSprites(score.enabledMods, new Vector2(305, 440), 1, @"mods");
            spriteManagerWideScrolling.Add(modSprites);
        }

        protected void LoadScore(string name, Score score)
        {
            LoadScore(score);
        }

        private void spriteBackground_OnClick(object sender, EventArgs e)
        {
            ScoreboardEntry sbe = (ScoreboardEntry)((pSprite)sender).Tag;
            Player.ScoreBoard.Reposition(true);
            sbe.MoveTo(sbe.spriteBackground.Position - new Vector2(30, 0), true, false);
            LoadScore(sbe.name, sbe.Score);
        }

        protected override void LoadRanking(int timeOffset)
        {
            if (score.ranking < Rankings.F)
            {
                pSprite p =
                    new pSprite(SkinManager.Load(@"ranking-" + score.ranking), Fields.TopLeft,
                                Origins.Centre,
                                Clocks.Game, new Vector2(180, 250), 0.3F, true, Color.TransparentWhite);
                Transformation t =
                    new Transformation(TransformationType.Scale, 1.4f, 1f, startTime + timeOffset,
                                       startTime + timeOffset + 1000);
                t.Easing = EasingTypes.In;
                p.Transformations.Add(t);
                t =
                    new Transformation(TransformationType.Fade, 0, 0.4f, startTime + timeOffset,
                                       startTime + timeOffset + 1000);
                t.Easing = EasingTypes.In;
                p.Transformations.Add(t);
                spriteManagerWideScrolling.Add(p);
                leftPanel.Add(p);
                if (score.ranking < Rankings.B)
                {
                    p =
                        new pSprite(SkinManager.Load(@"ranking-" + score.ranking), Fields.TopLeft,
                                    Origins.Centre,
                                    Clocks.Game, new Vector2(180, 270), 0.9F, true, Color.TransparentWhite);
                    t =
                        new Transformation(TransformationType.Scale, 1f, 2f, startTime + timeOffset + 1000,
                                           startTime + timeOffset + 1400);
                    t.Easing = EasingTypes.Out;
                    p.Transformations.Add(t);
                    leftPanel.Add(p);
                    t =
                        new Transformation(TransformationType.Fade, 0.4f, 0, startTime + timeOffset + 1000,
                                           startTime + timeOffset + 1400);
                    t.Easing = EasingTypes.Out;
                    p.Transformations.Add(t);
                    spriteManagerWideScrolling.Add(p);
                    leftPanel.Add(p);
                }
            }
        }


        protected override void InitializeSpecifics()
        {
            Player.ScoreBoard.topLeft = new Vector2(GameBase.WindowWidthScaled - 125, 120);

            foreach (ScoreboardEntry sbe in Player.ScoreBoard.Entries)
            {
                sbe.spriteBackground.Texture = SkinManager.Load(@"menu-button-background");
                sbe.spriteBackground.OriginPosition = new Vector2(26, 20);
                sbe.spriteBackground.OnClick += spriteBackground_OnClick;
                sbe.spriteBackground.HandleInput = true;
                sbe.spriteBackground.Tag = sbe;
                sbe.spriteBackground.HoverEffect = new Transformation(sbe.spriteBackground.InitialColour,
                                                                      Color.YellowGreen, 0, 100);
                if (sbe.name == GameBase.User.Name)
                    ChatEngine.AddUserLog(string.Format(LocalisationManager.GetString(OsuString.Userlog_Multiplayer), sbe.rank, Player.ScoreBoard.Entries.Count, BeatmapManager.Current.DisplayTitleFull, OsuCommon.PlayModeString(Player.Mode)));
            }

            Player.ScoreBoard.spriteManager.Alpha = 1;

            pText noMatches = new pText("Click a player tab to view their score! Hit F2 to export your replay.", 14
                                        , new Vector2(GameBase.WindowWidthScaled, 60), Vector2.Zero, 1, true, Color.White, false)
                                        {
                                            TextAlignment = Graphics.Renderers.TextAlignment.Right,
                                            Origin = Origins.TopRight,
                                            ViewOffsetImmune = true
                                        };
            noMatches.TextBold = true;
            spriteManagerWideScrolling.Add(noMatches);

            startTime = GameBase.Time + 300;

            if (Player.ScoreBoard.trackingEntry != null)
                Player.ScoreBoard.trackingEntry.spriteBackground.Click();

            pText pt =
     new pText(BeatmapManager.Current.DisplayTitle, 22, new Vector2(0, 0), Vector2.Zero, 0.991F, true,
               Color.White, false);
            pt.ViewOffsetImmune = true;
            spriteManagerWideScrolling.Add(pt);

            pt =
                new pText("Beatmap by " + BeatmapManager.Current.Creator, 16, new Vector2(1, 20), Vector2.Zero, 0.991F,
                          true, Color.White, false);
            pt.ViewOffsetImmune = true;
            spriteManagerWideScrolling.Add(pt);

            pt = new pText("Match played at " + score.date.ToString("yy/MM/dd hh:mm:ss", GameBase.nfi), 16, new Vector2(1, 34), Vector2.Zero,
                           0.991F, true, Color.White, false);
            pt.ViewOffsetImmune = true;
            spriteManagerWideScrolling.Add(pt);

            if (!MatchSetup.Match.TeamMode && Player.ScoreBoard.Entries.Count > 1 && Player.ScoreBoard.Entries[0].score != Player.ScoreBoard.Entries[1].score) //don't display in a tied situation.
            {
                pTexture winner = ((ScoreboardEntryExtended)Player.ScoreBoard.Entries[0]).spriteAvatar.Texture;
                pSprite winnerBg = new pSprite(SkinManager.Load(@"ranking-winner"), new Vector2(395, 10), 0.4f, true, Color.White);
                spriteManagerWideScrolling.Add(winnerBg);
                if (winner != null)
                {
                    pSprite largeAvatar = new pSprite(winner, Origins.Centre, new Vector2(457.5f, 89f), 0.41f, true, Color.White);
                    largeAvatar.Scale = 185F / Math.Max(largeAvatar.Width, largeAvatar.Height);
                    spriteManagerWideScrolling.Add(largeAvatar);
                    largeAvatar.MoveToRelative(new Vector2(0, 200), 2000, EasingTypes.Out);
                    largeAvatar.FadeInFromZero(1500);
                }

                winnerBg.MoveToRelative(new Vector2(0, 200), 2000, EasingTypes.Out);
                winnerBg.FadeInFromZero(1500);
            }
        }

        protected override void InitializeLocalScore()
        {
            loadLocalUserScore(false);
        }

        protected override bool onClick(object sender, EventArgs e)
        {
            Progress(false);
            return false;
        }

        protected override void Exit()
        {
            AudioEngine.PlaySamplePositional(@"menuback");
            MatchSetup.PendingScoreUpdate = true;
            GameBase.ChangeMode(OsuModes.MatchSetup);
        }

        protected override void CloseRecord()
        {
        }

        protected override void PopupRecord()
        {
            if (score.AllowSubmission)
                base.PopupRecord();
        }
    }
}