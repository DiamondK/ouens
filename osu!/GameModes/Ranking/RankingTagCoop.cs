﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Components;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Primitives;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Online;
using osu_common;
using osu_common.Bancho.Objects;
using osudata;

namespace osu.GameModes.Ranking
{
    internal class RankingTagCoop : Ranking
    {
        internal RankingTagCoop(Game game)
            : base(game)
        {
        }

        public override void Draw()
        {
            base.Draw();

            Player.ScoreBoard.Draw();
        }

        protected override void LoadMods()
        {
            int time = 0;
            float dep = 0;
            int x = 360;
            foreach (Mods m in Enum.GetValues(typeof(Mods)))
            {
                if (ModManager.CheckActive(score.enabledMods, m))
                {
                    if (m == Mods.DoubleTime && ModManager.CheckActive(score.enabledMods, Mods.Nightcore))
                        continue;
                    if (m == Mods.SuddenDeath && ModManager.CheckActive(score.enabledMods, Mods.Perfect))
                        continue;

                    Transformation t2 =
                        new Transformation(TransformationType.Scale, 2, 1, GameBase.Time + time,
                                           GameBase.Time + time + 400);
                    Transformation t3 =
                        new Transformation(TransformationType.Fade, 0, 1, GameBase.Time + time,
                                           GameBase.Time + time + 400);
                    t2.Easing = EasingTypes.Out;
                    t3.Easing = EasingTypes.Out;

                    pSprite p =
                        new pSprite(SkinManager.Load(@"selection-mod-" + m.ToString().ToLower()),
                                    Fields.TopLeft,
                                    Origins.Centre,
                                    Clocks.Game,
                                    new Vector2(x, 440), 0.92F + dep, true,
                                    Color.TransparentWhite);
                    p.Transformations.Add(t2);
                    p.Transformations.Add(t3);
                    spriteManagerWideScrolling.Add(p);

                    time += 500;
                    dep += 0.00001f;
                    x -= 20;
                }
            }
        }

        protected override void InitializeSpecifics()
        {
            Player.ScoreBoard.topLeft = new Vector2(515, 120);
            Player.ScoreBoard.spriteManager.WidescreenAutoOffset = true;

            foreach (ScoreboardEntry sbe in Player.ScoreBoard.Entries)
            {
                sbe.spriteBackground.Texture = SkinManager.Load(@"menu-button-background");
                sbe.spriteBackground.OriginPosition = new Vector2(26, 20);
                sbe.spriteBackground.Tag = sbe;
                sbe.spriteRank.Text = string.Empty;
                sbe.spriteBackground.HoverEffect = new Transformation(sbe.spriteBackground.InitialColour,
                                                                      Color.YellowGreen, 0, 100);
            }

            Player.ScoreBoard.spriteManager.RemoveTagged("extra");
            Player.ScoreBoard.Reposition(true);

            Player.ScoreBoard.spriteManager.Alpha = 1;

            score.pass = true; //force a pass.

            LoadScore(score);

            startTime = GameBase.Time + 300;

            pSprite detailsBack =
                new pSprite(GameBase.WhitePixel, Fields.TopLeft,
                            Origins.TopLeft,
                            Clocks.Game,
                            new Vector2(0, 0), 0.7F, true, Color.Black);

            detailsBack.VectorScale = new Vector2(GameBase.WindowWidthScaled * 1.6f, 83);
            spriteManagerWideScrolling.Add(detailsBack);

            pText pt =
                new pText("Multiplayer Results", 22, new Vector2(0, 0), Vector2.Zero, 0.79F, true,
                          Color.White, false);
            spriteManagerWideScrolling.Add(pt);

            pt =
                new pText("Beatmap: " + BeatmapManager.Current.DisplayTitle, 16, new Vector2(1, 20), Vector2.Zero, 0.79F,
                          true, Color.White, false);
            spriteManagerWideScrolling.Add(pt);
            pt = new pText("Match played at " +
                           score.date.ToString("yy/MM/dd hh:mm:ss", GameBase.nfi), 16, new Vector2(1, 34), Vector2.Zero,
                           0.79F, true, Color.White, false);
            spriteManagerWideScrolling.Add(pt);

            b_back =
                new pSprite(SkinManager.Load(@"ranking-back"), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Game,
                            new Vector2(520, 430), 0.94F, true,
                            new Color(255, 255, 255, 178));
            b_back.OnClick += return_OnClick;
            b_back.HandleInput = true;
            b_back.HoverEffect = new Transformation(TransformationType.Fade, 0.7F, 1, 0, 200);
            if (!GameBase.Tournament)
                spriteManagerWideScrolling.Add(b_back);
        }

        protected override void InitializeLocalScore()
        {
            score = Player.currentScore;
        }

        protected override bool onClick(object sender, EventArgs e)
        {
            Progress(false);
            return false;
        }

        protected override void Exit()
        {
            AudioEngine.PlaySamplePositional(@"menuback");
            GameBase.ChangeMode(OsuModes.MatchSetup);
        }

        protected override void CloseRecord()
        {
        }
        protected override void Dispose(bool disposing)
        {
            PlayerVs.DisposeStatic();
            base.Dispose(disposing);
        }

        protected override void PopupRecord()
        {
        }
    }
}