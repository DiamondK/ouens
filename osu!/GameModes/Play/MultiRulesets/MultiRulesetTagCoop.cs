﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play.Components;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Fruits;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Online;
using osu_common;
using osu_common.Bancho.Objects;
using osu.GameModes.Online;
using osu_common.Bancho.Requests;
using osu.Graphics.Skinning;
using osu.Helpers;
using osu.GameplayElements.Beatmaps;
using System.Diagnostics;

namespace osu.GameModes.Play.TeamRulesets
{
    internal class MultiRulesetTagCoop : MultiRuleset
    {
        /// <summary>
        /// Contains the combined score for all players in the cooperative mode.
        /// </summary>
        private readonly Score combinedScore = new Score();

        private int lastRewindTime;

        /// <summary>
        /// A list of "break" periods during which the local player has control over the mouse.
        /// </summary>
        private List<EventBreak> localUserActiveTime;

        /// <summary>
        /// Special scoreboard which hides a lot of per-player details.
        /// </summary>
        private ScoreboardTagCoop scoreboard;

        private int scoreboardActiveUser = -3;

        /// <summary>
        /// A list of the used slot indexes.
        /// </summary>
        private List<int> usedPlayerSlots;

        internal MultiRulesetTagCoop(PlayerVs player)
            : base(player)
        {
            InputManager.ReplayCursorFollow = false;

            combinedScore.AllowSubmission = false;
            combinedScore.pass = true;
        }

        internal override bool ContinuousPlay
        {
            get { return false; }
        }

        internal override bool CheckAllNotesHit
        {
            get { return false; }
        }

        /// <summary>
        /// Is the local player in control at the moment?
        /// </summary>
        private bool isLocalUserActive
        {
            get { return isLocalUserActiveAt(AudioEngine.Time); }
        }

        internal override Score visibleScore
        {
            get { return combinedScore; }
        }

        /// <summary>
        /// Gets the slotId with the active player (or -1 if there is none in the minimal range).
        /// </summary>
        private int activePlayerSlotId
        {
            get
            {
                if (EventManager.BreakMode) return -1; //We are in a break.

                HitObject ho = player.hitObjectManager.hitObjectsMinimal.Find(h => h.EndTime >= AudioEngine.Time);

                if (ho == null) return -1; //Couldn't find a matching hitobject in the minimal range.

                return ho.TagNumeric;
            }
        }

        public override void Dispose()
        {
            InputManager.s_Cursor.InitialColour = Color.White;
            InputManager.ReplayCursorFollow = true;
            InputManager.ReplayMode = false;
            hideWarningArrows();
            scoreboard.SetActivePlayer(null);
            base.Dispose();
        }

        internal override void OnFail()
        {
            player.SendMapComplete();

            if (PlayerVs.TeamMode)
            {
                GameBase.ChangeMode(OsuModes.RankingTeam);
                NotificationManager.ClearMessageMassive(null, null);
            }
            else /*if (PlayerVs.AllPlayersCompleted)*/
                GameBase.ChangeMode(OsuModes.MatchSetup);
        }


        /// <summary>
        /// Is the local user active at [time]
        /// </summary>
        private bool isLocalUserActiveAt(int time)
        {
            if (EventManager.BreakMode || player.Passed || Player.Failed || player.Status != PlayerStatus.Playing)
                return true;

            if (AudioEngine.Time >
                player.hitObjectManager.hitObjects[player.hitObjectManager.hitObjectsCount - 1].EndTime)
                return true;

            EventBreak br = localUserActiveTime.Find(b => b.StartTime <= time && b.EndTime >= time);

            if (br == null) return false;

            return player.hitObjectManager.hitObjectsMinimal.FindIndex(
                       ho => (ho.TagNumeric == player.localPlayerMatchId || ho.TagNumeric == -2) &&
                             ho.EndTime <= br.EndTime + player.hitObjectManager.HitWindow50 && !ho.IsHit) >= 0;
        }

        internal override void HandlePlayerFailed(int i)
        {
            //mark score as failed in scoreboard
            base.HandlePlayerFailed(i);

            if (Player.Failed) return;

            if (PlayerVs.TeamMode)
            {
                GameBase.ChangeMode(OsuModes.RankingTeam);
                player.SendMapComplete();
                NotificationManager.ClearMessageMassive(null, null);
            }
            else
            {
                player.Ruleset.HpBar.SetCurrentHp(0);
                player.OnFailableHp();
            }


        }


        internal override Scoreboard CreateScoreboard()
        {
            scoreboard = new ScoreboardTagCoop(PlayerVs.Match.slotUsedCount, null,
                                               new Vector2(0, Player.Mode == PlayModes.Taiko ? 40 : 150), true, PlayerVs.TeamMode ? PlayerVs.Match.slotTeam[player.localPlayerMatchId] : SlotTeams.Blue);
            return scoreboard;
        }

        internal override void HandleFrame(bScoreFrame frame)
        {
            ScoreboardEntry match = player.scoreEntries[frame.id];

            match.HandleUpdateUnsafe(frame);
            //PlayerVs.scoreboardUpdatePending = true;

            if (match.Team != player.scoreEntry.Team)
            {
                //Other team's play.
                return;
            }

            UpdateVisibleScore(match);
        }

        /// <summary>
        /// Updates the visible (combined) score and triggers any replay alterations required.
        /// </summary>
        /// <param name="trigger">The player that triggered this update.</param>
        private void UpdateVisibleScore(ScoreboardEntry trigger)
        {
            int count300 = combinedScore.count300;
            int count100 = combinedScore.count100;
            int count50 = combinedScore.count50;
            int countMiss = combinedScore.countMiss;
            int totalScore = combinedScore.totalScore;

            combinedScore.hpGraph = Player.currentScore.hpGraph;

            combinedScore.Reset();

            foreach (int i in usedPlayerSlots)
            {
                Score thisScore = player.scoreEntries[i].Score;

                if (thisScore == null) continue;

                combinedScore.totalScore += thisScore.totalScore;

                combinedScore.count50 += thisScore.count50;
                combinedScore.count100 += thisScore.count100;
                combinedScore.count300 += thisScore.count300;
                combinedScore.countMiss += thisScore.countMiss;
                combinedScore.countGeki += thisScore.countGeki;
                combinedScore.countKatu += thisScore.countKatu;

                combinedScore.currentHp = Math.Max(combinedScore.currentHp, thisScore.currentHp);
                combinedScore.maxCombo = Math.Max(combinedScore.maxCombo, thisScore.maxCombo);
            }

            if (trigger != player.scoreEntry)
            {
                HitObjectType type = (HitObjectType)trigger.Frame.tagByte;

                if (type.IsType(HitObjectType.Normal)) //HitCircle
                {
                    //Wasn't a result of the active player.
                    if (combinedScore.count50 > count50)
                        AlterReplay(IncreaseScoreType.Hit50);
                    else if (combinedScore.count100 > count100)
                        AlterReplay(IncreaseScoreType.Hit100);
                    else if (combinedScore.countMiss > countMiss)
                        AlterReplay(IncreaseScoreType.Miss);
                }
                else if (type.IsType(HitObjectType.Slider))
                {
                    if (combinedScore.count50 > count50)
                        AlterReplaySlider(IncreaseScoreType.Hit50);
                    else if (combinedScore.count100 > count100)
                        AlterReplaySlider(IncreaseScoreType.Hit100);
                    else if (combinedScore.count300 > count300)
                        AlterReplaySlider(IncreaseScoreType.Hit300);
                    else if (combinedScore.countMiss > countMiss)
                        AlterReplaySlider(IncreaseScoreType.Miss);
                }
            }

            player.Ruleset.ScoreDisplay.Update(combinedScore.totalScore);
        }

        /// <summary>
        /// When we have latency involved, we need to look at changing the autoplay replay
        /// to act as if a player was hitting notes.  So we should check for any non-300s
        /// and apply then to the replay by warping some frames.
        /// </summary>
        private void AlterReplay(IncreaseScoreType type)
        {
            List<bReplayFrame> replay = autoScore.replay;
            int currentFrame = InputManager.ReplayFrame + 1;

            while (currentFrame < replay.Count - 2)
            {
                bReplayFrame prev = replay[currentFrame - 1];
                bReplayFrame curr = replay[currentFrame];
                bReplayFrame next = replay[currentFrame + 1];

                if (curr.mouseLeft)
                {
                    if (prev.mouseLeft || next.mouseLeft || isLocalUserActiveAt(curr.time))
                    {
                        currentFrame++;
                        continue; //slider or spinner.
                    }

                    switch (type)
                    {
                        case IncreaseScoreType.Miss:
                            curr.SetButtonStates(pButtonState.None);
                            return;
                        case IncreaseScoreType.Hit50:
                            {
                                int aim = (int)(player.hitObjectManager.HitWindow100 * 1.1f);
                                if (curr.time - prev.time > aim)
                                {
                                    curr.time -= aim;
                                    return;
                                }
                            }
                            break;
                        case IncreaseScoreType.Hit100:
                            {
                                int aim = (int)(player.hitObjectManager.HitWindow300 * 1.1f);
                                if (curr.time - prev.time > aim)
                                {
                                    curr.time -= aim;
                                    return;
                                }
                            }
                            break;
                    }
                }

                currentFrame++;
            }
        }

        int safeNoClickFrame;

        /// <summary>
        /// When we have latency involved, we need to look at changing the autoplay replay
        /// to act as if a player was hitting notes.  So we should check for any non-300s
        /// and apply then to the replay by warping some frames.
        /// </summary>
        private void AlterReplaySlider(IncreaseScoreType type)
        {
            //First check if we just want to increase the tick count...

            List<bReplayFrame> replay = InputManager.ReplayScore.replay;
            int currentFrame = InputManager.ReplayFrame + 1;

            int replayCount = replay.Count;

            if (currentFrame > replayCount - 1)
                return;

            int sliderCheckFrame = Math.Max(safeNoClickFrame, currentFrame);
            //Even if we already are in the middle of a slider, we should
            //rewind back to the start and utilise the rest of it.
            while (sliderCheckFrame < replayCount - 2 && replay[sliderCheckFrame + 1].mouseLeft)
                sliderCheckFrame++;

            safeNoClickFrame = sliderCheckFrame;

            while (currentFrame < replayCount - 2)
            {
                bReplayFrame prev = replay[currentFrame - 1];
                bReplayFrame curr = replay[currentFrame];
                bReplayFrame next = replay[currentFrame + 1];

                if (curr.mouseLeft && next.mouseLeft && !prev.mouseLeft && !isLocalUserActiveAt(curr.time))
                {
                    //Found the start of a slider or spinner.
                    //We need to confirm this is actually a slider though.

                    //bool firstMatchRewindFound = sliderCheckFrame != currentFrame && first;
                    //Is the first check in this loop and rewinding has been utilised.

                    //if (!firstMatchRewindFound) //Rewinding current slider not used.  Use currentFrame.
                    //    sliderCheckFrame = currentFrame;

                    sliderCheckFrame = currentFrame;

                    int searchTime = replay[sliderCheckFrame].time;

                    SliderOsu found =
                        player.hitObjectManager.hitObjects.Find(ho => ho.StartTime == searchTime) as SliderOsu;

                    if (found == null)
                    {
                        currentFrame++;
                        continue; //No slider or spinner.
                    }

                    //At this point we know we have a slider.
                    //Now we need to choose how to change the replay depending on the type of
                    //penalty we are planning to deal.

                    switch (type)
                    {
                        case IncreaseScoreType.Miss:
                            //The slider should be TOTALLY missed.

                            //We can't do this if we have already started on a slider that
                            //has already had a non-miss...

                            /*if (firstMatchRewindFound && found.sliderTicksMissed == 0)
                            {
                                currentFrame++;
                                continue;
                            }*/

                            while (curr.mouseLeft)
                            {
                                //No mouse control over the whole slider is required.
                                curr.SetButtonStates(pButtonState.None);

                                if (currentFrame == replayCount - 1)
                                    return;

                                curr = replay[++currentFrame];
                            }

                            return;
                        case IncreaseScoreType.Hit50:
                        case IncreaseScoreType.Hit100:
                        case IncreaseScoreType.Hit300:
                            {
                                int possibleCount;

                                if (type == IncreaseScoreType.Hit50)
                                {
                                    //In case of a 50, we can miss a minimum of 50%.
                                    possibleCount = (found.sliderScoreTimingPoints.Count + 1) / 2 ==
                                                    (found.sliderScoreTimingPoints.Count + 1) / 2f
                                                        ?
                                                            (found.sliderScoreTimingPoints.Count + 1) / 2
                                                        : (found.sliderScoreTimingPoints.Count + 1) / 2 + 1;

                                    //possibleCount = found.sliderScoreTimingPoints.Count;
                                }
                                else if (type == IncreaseScoreType.Hit100)
                                {
                                    //In case of a 100, we can miss a maximum of 50%.
                                    possibleCount = 1; //TOO LENIANT
                                }
                                else
                                {
                                    return;
                                }

                                //If the slider is already partway through, we should remove any ticks which have already been consumed.
                                possibleCount -= found.sliderTicksHit;

                                if (possibleCount <= 0)
                                {
                                    currentFrame++;
                                    continue;
                                }

                                bool firstTick = true;

                                int firstTickEndTime = curr.time +
                                                       Math.Min(found.sliderScoreTimingPoints[0],
                                                                player.hitObjectManager.HitWindow50);

                                while (curr.time <= found.EndTime && possibleCount > 0)
                                {
                                    if (firstTick)
                                    {
                                        if (curr.time < firstTickEndTime)
                                            curr.SetButtonStates(pButtonState.None);
                                        else
                                        {
                                            firstTick = false;
                                            possibleCount--;
                                        }
                                    }

                                    if (!firstTick)
                                    {
                                        bool containsTimingPoint =
                                            found.sliderScoreTimingPoints.FindIndex(
                                                tp => curr.time <= tp && next.time > tp) >= 0;

                                        if (containsTimingPoint)
                                        {
                                            if (possibleCount > 0)
                                            {
                                                possibleCount--;
                                                curr.SetButtonStates(pButtonState.None);
                                                next.SetButtonStates(pButtonState.None);
                                                Debug.Print("  Consumed 1 tick (remaining to consume " + possibleCount + ")");
                                            }
                                            else
                                                return;
                                        }
                                    }

                                    if (currentFrame == replayCount - 1)
                                        break;

                                    curr = replay[++currentFrame];
                                    next = replay[currentFrame + 1];
                                }
                            }

                            Debug.Print("  Slider exhausted...");
                            return;
                    }
                }

                currentFrame++;
            }
        }

        Score autoScore;

        internal override void Initialize()
        {
            InputManager.ReplayScore = autoScore = ScoreFactory.Create(Player.Mode, "osu!", player.hitObjectManager.Beatmap); player.Ruleset.CreateAutoplayReplay();

            Player.currentScore.AllowSubmission = false;
            //This mode is totally unranked.

            base.Initialize();
        }

        /// <summary>
        /// Handle splitting up and labelling the hitObjects into different player's scopes.
        /// </summary>
        internal override void InitializeHitObjectPostProcessing()
        {
            bMatch match = PlayerVs.Match;

            usedPlayerSlots = new List<int>();

            int playerCount = 0;

            for (int i = 0; i < bMatch.MAX_PLAYERS; i++)
                if ((match.slotStatus[i] & SlotStatus.Playing) > 0 &&
                    match.slotTeam[i] == match.slotTeam[player.localPlayerMatchId])
                {
                    usedPlayerSlots.Add(i);
                    playerCount++;
                }

            HitObjectManager hitObjectManager = player.hitObjectManager;

            int currentPlayer = -1;

            localUserActiveTime = new List<EventBreak>();

            EventBreak currentBreak = null;

            bool firstCombo = true;
            bool customTagColor;

            if (customTagColor = MatchSetup.TagComboColour != Color.TransparentWhite)
            {
                SkinManager.SliderRenderer.Tag = MatchSetup.TagComboColour;
            }
            for (int i = 0; i < hitObjectManager.hitObjectsCount; i++)
            {
                HitObject h = hitObjectManager.hitObjects[i];
                //HitObject hLast = i > 0 ? hitObjectManager.hitObjects[i-1] : hitObjectManager.hitObjects[i];

                bool firstInCombo = h.NewCombo || firstCombo;

                bool spinner = h.IsType(HitObjectType.Spinner) || h is HitCircleFruitsSpin;

                if (firstInCombo)
                {
                    if (!spinner)
                    {
                        currentPlayer = (currentPlayer + 1) % playerCount;
                        firstCombo = false;
                    }

                    if (spinner || usedPlayerSlots[currentPlayer] == player.localPlayerMatchId)
                    {
                        //The local player starts playing at this point.
                        int st = (i > 0
                                      ? Math.Max(h.StartTime - hitObjectManager.HitWindow50,
                                                 hitObjectManager.hitObjects[i - 1].EndTime + 1)
                                      : h.StartTime - hitObjectManager.HitWindow50);
                        int ed = h.EndTime;
                        currentBreak = new EventBreak(st, ed);
                        localUserActiveTime.Add(currentBreak);
                    }
                    else
                    {
                        //Another play has taken over.
                        currentBreak = null;
                    }
                }


                if (spinner || usedPlayerSlots[currentPlayer] == player.localPlayerMatchId)
                {
                    if (currentBreak != null)
                    {
                        //The local player finishes playing at this point (or further).
                        currentBreak.SetEndTime(h.EndTime + hitObjectManager.HitWindow50);
                    }

                    if (customTagColor)
                        h.SetColour(MatchSetup.TagComboColour);
                }
                else
                {
                    h.IsScorable = false;
                    h.SetColour(Color.Gray);
                }

                h.TagNumeric = spinner ? -2 : usedPlayerSlots[currentPlayer];
            }

            InitializeWarningArrows();
        }

        /// <summary>
        /// Initializes the warning arrows.  These appear before each tag combo start.
        /// </summary>
        private void InitializeWarningArrows()
        {
            //Clear defaults.
            player.s_arrows.ForEach(a => a.Transformations.Clear());

            //Populate new.
            foreach (EventBreak b in localUserActiveTime)
            {
                for (int i = 0; i < 1300; i += 200)
                {
                    Transformation t1 =
                        new Transformation(TransformationType.Fade, 0, 1, b.StartTime - 1000 + i, b.StartTime - 1000 + i);
                    Transformation t2 =
                        new Transformation(TransformationType.Fade, 1, 0, b.StartTime - 900 + i, b.StartTime - 900 + i);

                    player.s_arrows.ForEach(a =>
                                                {
                                                    a.Transformations.Add(t1);
                                                    a.Transformations.Add(t2);
                                                });
                }
            }
        }
        private void hideWarningArrows()
        {
            player.s_arrows.ForEach(a => { a.Transformations.Clear(); a.FadeOutFromOne(1000); });
        }
        internal override void Update()
        {
            HitObjectManager hitObjectManager = player.hitObjectManager;
            if (InputManager.ReplayMode && !player.Passed && AudioEngine.Time < lastRewindTime + hitObjectManager.HitWindow50)
                return;

            int activePlayerSlot = activePlayerSlotId;

            bool localUserActive = activePlayerSlot == player.localPlayerMatchId || isLocalUserActive;

            if (scoreboardActiveUser != activePlayerSlot || InputManager.ReplayMode == localUserActive)
            {
                scoreboardActiveUser = activePlayerSlot;

                UpdateRankingText();

                InputManager.s_Cursor.FadeColour(localUserActive ? Color.White : Color.Gray, 50);

                if (activePlayerSlot == -2) //all players (spinner)
                    scoreboard.SetActiveAll();
                else
                    scoreboard.SetActivePlayer(activePlayerSlot == -1 ? null : player.scoreEntries[activePlayerSlot]);

                if (InputManager.ReplayMode == localUserActive)
                {
                    InputManager.ReplayMode = !localUserActive;
                    player.UpdateChecksum(); //Must update checksum here else it will think we are hacking.

                    if (InputManager.ReplayMode)
                    {
                        InputManager.ReplayScore = autoScore;

                        lastRewindTime = AudioEngine.Time;

                        HitObject h =
                            player.hitObjectManager.hitObjectsMinimal.Find(
                                ho =>
                                ho.StartTime >= AudioEngine.Time - hitObjectManager.HitWindow50 &&
                                ho.TagNumeric == scoreboardActiveUser
                                && !ho.IsHit);
                        //If we accidentally bypassed a possible hit/miss situation, we don't want to skip directly to the current time.
                        //We need to let the replay start a few frames earlier (in practice this shouldn't cause any problems with repeated
                        //events... I hope).

                        int rewindTime = h != null ? h.StartTime - hitObjectManager.HitWindow50 : (int)AudioEngine.Time;

                        //Bypass any unnecessary frames.
                        while (InputManager.ReplayFrame < InputManager.ReplayScore.replay.Count - 1 &&
                               InputManager.ReplayScore.replay[InputManager.ReplayFrame].time < rewindTime)
                            InputManager.ReplayFrame++;

                        //Then go back to check for any clicks we may have missed..
                        if (InputManager.ReplayScore.replay[InputManager.ReplayFrame].mouseLeft)
                        {
                            while (InputManager.ReplayScore.replay[InputManager.ReplayFrame].time > rewindTime &&
                                   InputManager.ReplayScore.replay[InputManager.ReplayFrame - 1].mouseLeft)
                                InputManager.ReplayFrame--;
                        }
                        else
                        {
                            while (InputManager.ReplayScore.replay[InputManager.ReplayFrame].time > rewindTime &&
                                   !InputManager.ReplayScore.replay[InputManager.ReplayFrame].mouseLeft)
                                InputManager.ReplayFrame--;
                        }

                        rewindTime = InputManager.ReplayScore.replay[InputManager.ReplayFrame].time;

                        h = player.hitObjectManager.hitObjectsMinimal.Find(ho => ho.StartTime == rewindTime);
                        if (h != null && h.TagNumeric != scoreboardActiveUser)
                        {
                            //Found a frame for a circle that isn't owned by this user - skip forward a frame and play.
                            InputManager.ReplayFrame++;
                            //lastRewindTime = InputManager.ReplayScore.replay[InputManager.ReplayFrame].time;
                        }

                        //Insert a dud frame to clear the mouse buttons.
                        bReplayFrame cf = InputManager.ReplayScore.replay[InputManager.ReplayFrame];

                        bReplayFrame frame = new bReplayFrame(cf.time - 1, cf.mouseX, cf.mouseY, pButtonState.None);
                        InputManager.ReplayScore.replay.Insert(InputManager.ReplayFrame, frame);
                        InputManager.ReplayScore.replay.Insert(InputManager.ReplayFrame, frame);
                    }
                    else
                    {
                        //Force an update to make sure we dont accidentally accept a false positive click.
                        InputManager.UpdateButtonsLocalPlayer();
                    }
                }
            }

            base.Update();
        }

        /// <summary>
        /// Populates the scoreboard with ScoreboardEntries.
        /// </summary>
        internal override void PopulateScoreboard()
        {
            Player.ScoreBoard.inputCount = PlayerVs.Match.slotUsedCount;

            for (int i = 0; i < bMatch.MAX_PLAYERS; i++)
            {
                if (PlayerVs.Match.slotStatus[i] != SlotStatus.Playing)
                {
                    player.scoreEntries.Add(null);
                    continue;
                }

                ScoreboardEntryCoop se = new ScoreboardEntryCoop(i, PlayerVs.Match.UserSlots[i], PlayerVs.Match.matchScoringType, Player.Mode,
                                                             PlayerVs.Match.slotTeam[i]);
                player.scoreEntries.Add(se);
                Player.ScoreBoard.Add(se);
            }

            player.scoreEntry = player.scoreEntries[player.localPlayerMatchId];
            if (player.scoreEntry != null)
                Player.ScoreBoard.SetTrackingEntry(player.scoreEntry);

            scoreboard.SetActivePlayer(null);

            Player.ScoreBoard.Reposition(false);
        }

        internal override void LoadRanking()
        {
            GameBase.ChangeMode(OsuModes.RankingTagCoop);
            NotificationManager.ClearMessageMassive(null, null);
        }

        internal override void UpdateRankingText()
        {
            if (player == null || player.multiplayerRankingText == null || player.scoreEntries == null)
                return;

            player.multiplayerRankingText.Text = scoreboardActiveUser < 0 ? "" : player.scoreEntries[scoreboardActiveUser].name;
            player.multiplayerRankingText.FlashColour(Color.White, 300);
        }
    }
}
