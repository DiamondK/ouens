﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Play.Components;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Online;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho.Objects;
using osu.GameModes.Play.Rulesets.Taiko;
using osu.GameModes.Play.Rulesets;

namespace osu.GameModes.Play.TeamRulesets
{
    internal abstract class MultiRuleset : IDisposable
    {
        protected readonly PlayerVs player;

        internal MultiRuleset(PlayerVs player)
        {
            this.player = player;
        }

        internal virtual Score visibleScore
        {
            get { return Player.currentScore; }
        }

        internal virtual bool ContinuousPlay
        {
            get { return true; }
        }

        internal virtual bool CheckAllNotesHit
        {
            get { return true; }
        }

        public virtual void Dispose()
        {
            if (Player.ScoreBoard != null)
                ScoreboardVisible = Player.ScoreBoard.AlwaysVisible;
        }

        internal virtual void Initialize()
        {
            InitializeHitObjectPostProcessing();
        }

        internal virtual void InitializeHitObjectPostProcessing()
        {

        }

        internal virtual void Update()
        {
        }

        internal static bool ScoreboardVisible = true;

        internal virtual Scoreboard CreateScoreboard()
        {
            switch (Player.Mode)
            {
                case PlayModes.Taiko:
                    return new Scoreboard(6, null, new Vector2(0, RulesetTaiko.TAIKO_BAR_Y + 130), ScoreboardVisible);
                default:
                    return new Scoreboard(PlayerVs.Match.slotUsedCount, null, new Vector2(0, 150), ScoreboardVisible);
            }

        }

        internal virtual void HandleFrame(bScoreFrame frame)
        {
            ScoreboardEntry playerEntry = player.scoreEntries[frame.id];

            if (playerEntry == null) return;

            playerEntry.HandleUpdate(frame);
            PlayerVs.scoreboardUpdatePending = true;
        }

        internal virtual void PopulateScoreboard()
        {
            Player.ScoreBoard.inputCount = PlayerVs.Match.slotUsedCount;

            for (int i = 0; i < bMatch.MAX_PLAYERS; i++)
            {
                //todo: WTF AT THIS

                if (PlayerVs.Match.slotStatus[i] != SlotStatus.Playing)
                {
                    player.scoreEntries.Add(null);
                    continue;
                }

                /*User u = BanchoClient.GetUserById(PlayerVs.Match.slotId[i]);
                
                if (u == null)
                {
                    //todo: dump some debug info here
                    /*
                        System.ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument argument,
                                          ExceptionResource resource)M
                        09:18 <BanchoBot>    at System.ThrowHelper.ThrowArgumentOutOfRangeException()M
                        09:18 <BanchoBot>    at osu.GameModes.Play.TeamRulesets.MultiRuleset.PopulateScoreboard()M
                        09:18 <BanchoBot>    at osu.GameModes.Play.PlayerVs.InitializeScoreboard()M
                        09:18 <BanchoBot>    at osu.GameModes.Play.Player.Initialize()M
                        09:18 <BanchoBot>    at osu.GameModes.Play.PlayerVs.Initialize()M
                        09:18 <BanchoBot>    at Microsoft.Xna.Framework.Game.GameComponentAdded(Object
                                          sender, GameComponentCollectionEventArgs e)M
                        09:18 <BanchoBot>    at
                        Microsoft.Xna.Framework.GameComponentCollection.OnComponentAdded(GameComponentCollectionEventArgs eventArgs)M
                        09:18 <BanchoBot>    at
                        Microsoft.Xna.Framework.GameComponentCollection.InsertItem(Int32 index,
                                          IGameComponent item)M
                     
                    player.scoreEntries.Add(null);
                    continue;
                }*/

                ScoreboardEntryExtended se = new ScoreboardEntryExtended(i, PlayerVs.Match.UserSlots[i], PlayerVs.Match.matchScoringType, Player.Mode, PlayerVs.Match.slotTeam[i], PlayerVs.Match.slotMods[i]);

                se.spriteBackground.InitialColour = new Color(0, 0, 0, 180);

                player.scoreEntries.Add(se);

                Player.ScoreBoard.Add(se);
            }

            player.scoreEntry = player.scoreEntries[player.localPlayerMatchId];

            //if (player.scoreEntry != null) ScoreEntry should never be null?
            Player.ScoreBoard.SetTrackingEntry(player.scoreEntry);

            Player.ScoreBoard.Reorder(false);
        }

        internal virtual void OnFail()
        {
        }

        internal virtual void OnCompletion()
        {
            player.multiplayerRankingText.FadeOut(100);
        }

        internal virtual void HandlePlayerFailed(int i)
        {
            if (player.scoreEntries == null || player.scoreEntries[i] == null) return;
            player.scoreEntries[i].spriteRank.Text = "Failed";
            player.scoreEntries[i].Passing = false;
        }

        internal virtual void HandlePlayerSkipped(int i)
        {
            if (player.scoreEntries == null || player.scoreEntries[i] == null) return;
            player.scoreEntries[i].spriteSkip.Bypass = false;
            player.scoreEntries[i].spriteSkip.FadeInFromZero(100);
        }

        internal virtual void LoadRanking()
        {
            GameBase.ChangeMode(OsuModes.RankingVs);
            NotificationManager.ClearMessageMassive(null, null);
        }

        internal virtual void UpdateRankingText()
        {
            if (PlayerVs.TeamMode)
                return;

            if (!player.MultiFailed)
            {
                string ext;
                switch (player.scoreEntry.rank)
                {
                    case 1:
                        ext = "st";
                        break;
                    case 2:
                        ext = "nd";
                        break;
                    case 3:
                        ext = "rd";
                        break;
                    default:
                        ext = "th";
                        break;
                }
                player.multiplayerRankingText.Text = player.scoreEntry.rank + ext;
            }
            else
                player.multiplayerRankingText.Text = "Failed";
        }

        internal virtual void HandlePlayerLeft(int i)
        {
            if (player.scoreEntries == null || player.scoreEntries[i] == null)
                return;

            player.scoreEntries[i].spriteRank.Text = "Quit";
            player.scoreEntries[i].Passing = false;
        }

        internal virtual void Recover()
        {
            if (player.Ruleset.HpBar.CurrentHp < Ruleset.HP_BAR_MAXIMUM * 0.95)
                return;

            player.MultiFailed = false;
            player.Ruleset.DoRecovery(true);
            player.ResetRankingText();
        }
    }
}
