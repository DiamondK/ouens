﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.GameModes.Play.Rulesets;

namespace osu.GameModes.Play.TeamRulesets
{
    internal class MultiRulesetVs : MultiRuleset
    {
        public MultiRulesetVs(PlayerVs player)
            : base(player)
        {
        }

        internal override bool ContinuousPlay { get { return true; } }
    }
}
