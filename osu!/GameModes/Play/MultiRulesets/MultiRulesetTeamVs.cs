﻿using System;
using System.Collections.Generic;
using System.Text;
using osu_common;
using osu.GameModes.Play.Components;
using osu.Graphics.Notifications;
using osu.Online;
using osu_common.Bancho.Requests;

namespace osu.GameModes.Play.TeamRulesets
{
    internal class MultiRulesetTeamVs : MultiRuleset
    {
        public MultiRulesetTeamVs(PlayerVs player)
            : base(player)
        {
        }

        internal override bool ContinuousPlay { get { return !ourTeamFailed; } }
        bool ourTeamFailed = false;
        //this stuff below should be done in a new MultiRuleset for TeamVs.

        internal override void HandlePlayerFailed(int i)
        {
            //mark slot [i] as failed.
            base.HandlePlayerFailed(i);

            SlotTeams failTeam = player.scoreEntries[i].Team;
            //check each slot for the team [i] to see if all failed
            bool teamFailed = true;

            foreach (ScoreboardEntry s in player.scoreEntries)
                if (s != null && s.Team == failTeam && s.Passing == true)
                {
                    teamFailed = false;
                    break;
                }

            if (teamFailed)
            {
                if (player.scoreEntry.Team == failTeam)
                {
                    player.Ruleset.HpBar.SetCurrentHp(0);
                    player.OnFailableHp();
                    ourTeamFailed = true;
                }
                else
                {
                    BanchoClient.SendRequest(RequestType.Osu_MatchComplete, null);
                    player.InitializeScoreGraphs();
                    GameBase.ChangeMode(OsuModes.RankingTeam);
                    NotificationManager.ClearMessageMassive(null, null);
                }
            }



        }

        internal override void OnFail()
        {
            //After the fail animation, the failing team members will reach here.
            //change mode to ranking screen.
            BanchoClient.SendRequest(RequestType.Osu_MatchComplete, null);
            GameBase.ChangeMode(OsuModes.RankingTeam);
            player.InitializeScoreGraphs();
            NotificationManager.ClearMessageMassive(null, null);
        }
    }
}
