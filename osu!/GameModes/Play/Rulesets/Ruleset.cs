﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play.Components;
using osu.GameModes.Play.Rulesets.Taiko;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Notifications;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Online;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu_common.Helpers;
using osudata;
using osu.Input.Handlers;
using System.Threading;
using osu.GameplayElements.HitObjects.Taiko;
using osu.GameModes.Play.Rulesets.Mania;
using osu.Helpers;
using System.Text;

namespace osu.GameModes.Play.Rulesets
{
    internal abstract partial class Ruleset : IDisposable
    {
        /// <summary>
        /// SpriteManager borrowed from the base instance of Player
        /// </summary>
        protected readonly SpriteManager spriteManager;

        protected readonly SpriteManager spriteManagerWidescreen;

        /// <summary>
        /// Combo Counter display
        /// </summary>
        internal ComboCounter ComboCounter;

        /// <summary>
        /// Current Score instance.
        /// </summary>
        internal Score CurrentScore;

        /// <summary>
        /// HitObjectManager from Player instance
        /// </summary>
        internal HitObjectManager hitObjectManager;

        /// <summary>
        /// Hp Bar display
        /// </summary>
        internal HpBar HpBar;

        /// <summary>
        /// Semaphore to make sure the Ruleset is initialized before executing Update/Draw calls.
        /// </summary>
        internal bool IsInitialized;

        /// <summary>
        /// Player instance.
        /// </summary>
        protected Player player;

        /// <summary>
        /// Number of recoveries which can postpone the point of failure.
        /// </summary>
        internal int Recoveries;

        /// <summary>
        /// Score Display
        /// </summary>
        protected internal ScoreDisplay ScoreDisplay;

        internal ScoreMeter ScoreMeter;

        /// <summary>
        /// Additive SpriteManager borrowed from the base instance of Player
        /// </summary>
        protected SpriteManager spriteManagerAdd;

        /// <summary>
        /// SpriteManager specifically for handling the flashlight mod overlay.
        /// todo: move this to its own class.
        /// </summary>
        protected SpriteManager spriteManagerFlashlight;

        internal int TotalHitsPossible;

        #region Sprites

        //Flashlight Mod
        protected int comboBurstNumber;
        protected pTexture[] comboBurstSprites;
        internal pSprite s_Flashlight;
        protected pSprite s_LightsOut;

        #endregion

        #region Sound Channels

        private AudioSample failSound;

        #endregion

        #region Scoring

        /// <summary>
        /// Multiplier for hp increase at the end of a numbered-combo set.
        /// </summary>
        internal double HpMultiplierComboEnd = 1;

        /// <summary>
        /// Multiplier for hp increase on a normal hit.
        /// </summary>
        internal double HpMultiplierNormal = 1;
        internal double ScoreMultiplier = 1;

        /// <summary>
        /// Used to judge combo gain in mania.
        /// </summary>
        private double comboAddition = JudgementMania.ComboMulti;

        public static Ruleset Instance;

        internal virtual Vector2 ScoreboardPosition
        {
            get { return new Vector2(0, 205); }
        }

        internal virtual bool ScoreboardOnRight
        {
            get { return false; }
        }

        #endregion

        internal Ruleset(Player player)
        {
            Instance = this;

            this.player = player;
            spriteManager = player.spriteManagerInterface;
            spriteManagerWidescreen = player.spriteManagerInterfaceWidescreen;
            spriteManagerAdd = player.spriteManagerAdd;

            hitObjectManager = CreateHitObjectManager();

            player.OnClick += OnClick;

            GameBase.OnReload += GameBase_OnReload;
        }

        protected virtual void GameBase_OnReload(bool loadAll)
        {
            loadFlashlight();
        }

        internal virtual bool IsPassing
        {
            get { return HpBar == null || HpBar.CurrentHp >= HP_BAR_MAXIMUM * 0.5; }
        }

        internal virtual bool AllowMetadataDisplay
        {
            get { return true; }
        }

        internal virtual bool AllowScoreboardDisplay
        {
            get { return true; }
        }

        internal virtual Vector2 MousePosition
        {
            get { return GameBase.DisplayToGamefield(InputManager.CursorPosition); }
        }

        internal virtual bool AllowSubmission
        {
            get { return true; }
        }

        internal virtual bool AllowInstantUnpause
        {
            get { return false; }
        }

        internal virtual bool AllNotesHit
        {
            get
            {
                return CurrentScore.count300 + CurrentScore.count100 + CurrentScore.count50 +
                       CurrentScore.countMiss == hitObjectManager.hitObjectsCount;
            }
        }

        internal virtual bool AllowCountdown { get { return true; } }
        internal virtual bool AllowWarningArrows { get { return true; } }

        internal virtual bool AllowHideSpinnerTop
        {
            get { return true; }
        }

        protected double MouseIdleTime;
        protected Vector2 MouseLastPosition;

        internal virtual bool AllowMouseHide
        {
            get
            {
                if (Player.Playing) return false;

                if (Vector2.Distance(MouseManager.MousePosition, MouseLastPosition) < 10)
                    MouseIdleTime += GameBase.ElapsedMilliseconds;
                else
                    MouseIdleTime = 0;
                MouseLastPosition = MouseManager.MousePosition;
                return MouseIdleTime > 1000;
            }
        }

        internal virtual bool AllowFailTint
        {
            get { return true; }
        }

        internal virtual bool IsMilestoneCombo
        {
            get
            {
                return ComboCounter.HitCombo == 30 || ComboCounter.HitCombo == 60 ||
                       (ComboCounter.HitCombo > 99 && ComboCounter.HitCombo % 50 == 0);
            }
        }

        #region IDisposable Members

        public virtual void Dispose()
        {
            Instance = null;

            if (failSound != null) failSound.Stop();

            player.OnClick -= OnClick;

            if (ScoreMeter != null) ScoreMeter.Dispose();

            if (s_Flashlight != null) s_Flashlight.IsDisposable = true;
            if (spriteManagerFlashlight != null) spriteManagerFlashlight.Dispose();

            GameBase.OnReload -= GameBase_OnReload;
        }

        #endregion

        internal virtual void OnClick(HitObject h)
        {
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        internal virtual void Initialize()
        {
            InitializeModSettings();

            InitializeSprites();

            InitializeHpBar();
            InitializeComboCounter();
            InitializeScoreDisplay();
            InitializeScoreMeter();
            InitializeCalculations();

            InitializeSpriteManagers();

            IsInitialized = true;
        }

        internal virtual void InitializeSpriteManagers()
        {
            float gamefieldSpriteRatio = hitObjectManager.spriteManager.GamefieldSpriteRatio;

            if (spriteManager != null)
            {
                spriteManager.GamefieldSpriteRatio = gamefieldSpriteRatio;
            }

            if (spriteManagerAdd != null)
            {
                spriteManagerAdd.GamefieldSpriteRatio = gamefieldSpriteRatio;
            }

            if (spriteManagerFlashlight != null)
            {
                spriteManagerFlashlight.GamefieldSpriteRatio = gamefieldSpriteRatio;
            }

            if (spriteManagerWidescreen != null)
            {
                spriteManagerWidescreen.GamefieldSpriteRatio = gamefieldSpriteRatio;
            }
        }

        protected virtual void InitializeScoreMeter()
        {
            if (ConfigManager.sScoreMeter == ScoreMeterType.Colour)
                ScoreMeter = new ScoreMeterColour(GameBase.Instance, hitObjectManager);
            else if (ConfigManager.sScoreMeter == ScoreMeterType.Error || ModManager.CheckActive(Mods.Target))
            {
                if (Player.IsTargetPracticeMode)
                    ScoreMeter = new ScoreMeterErrorTarget(GameBase.Instance, hitObjectManager);
                else
                    ScoreMeter = new ScoreMeterError(GameBase.Instance, hitObjectManager);
            }
        }

        protected virtual void InitializeScoreDisplay()
        {
            ScoreDisplay = new ScoreDisplay(spriteManagerWidescreen);
        }

        internal virtual void CacheFutureSprites()
        {
            SkinManager.LoadAll(@"hit0");

            SkinManager.LoadAll(@"hit50");

            SkinManager.LoadAll(@"hit100");
            SkinManager.LoadAll(@"hit100k");

            SkinManager.LoadAll(@"hit300");
            SkinManager.LoadAll(@"hit300g");
            SkinManager.LoadAll(@"hit300k");
        }

        internal virtual void InitializeHpBar()
        {
            HpBar = new HpBar(spriteManagerWidescreen);
        }

        internal virtual void SetAlpha(float alpha)
        {
            spriteManagerAdd.Alpha = alpha;
            hitObjectManager.spriteManager.Alpha = alpha;
        }

        internal virtual void InitializeCalculations()
        {
            ResetScore();

            if (BeatmapManager.Current.AudioLeadIn > 0)
                HpBar.InitialIncreaseStartTime = -BeatmapManager.Current.AudioLeadIn;

            if (hitObjectManager.hitObjects[0].StartTime * HpBar.InitialIncreaseRate < HP_BAR_MAXIMUM)
                HpBar.InitialIncreaseRate = HP_BAR_MAXIMUM /
                                            (hitObjectManager.hitObjects[0].StartTime +
                                             BeatmapManager.Current.AudioLeadIn);
            else if (hitObjectManager.hitObjects[0].StartTime > 10000)
                HpBar.InitialIncreaseStartTime = hitObjectManager.hitObjects[0].StartTime - 10000;

            if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.DoubleTime))
                HpBar.InitialIncreaseRate *= 1.5f;
        }

        internal virtual void InitializeModSettings()
        {
            Player.Relaxing = ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Relax);
            Player.Relaxing2 = ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Relax2);
            ScoreMultiplier = BeatmapManager.Current.DifficultyPeppyStars *
                                ModManager.ScoreMultiplier(CurrentScore.enabledMods);
            if (!ModManager.CheckActive(Player.currentScore.enabledMods, Mods.SuddenDeath))
            {
                if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Easy))
                    Recoveries = 2;
            }

            if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Flashlight))
            {
                spriteManagerFlashlight = new SpriteManager(true);

                s_Flashlight = new pSprite(null, Fields.Storyboard,
                                           Origins.Centre, Clocks.Game, Vector2.Zero, 1, true, Color.Black);
                loadFlashlight();
                spriteManagerFlashlight.Add(s_Flashlight);

                s_LightsOut =
                    new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game,
                                Vector2.Zero, 0.989f, true,
                                Color.TransparentBlack);
                s_LightsOut.ScaleToWindowRatio = false;
                s_LightsOut.VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight);
                spriteManagerFlashlight.Add(s_LightsOut);
            }
        }

        protected virtual void loadFlashlight()
        {
            if (s_Flashlight == null) return;

            string filename = @"flashlightv2";
            newFlashlightOverlay = true;
            byte[] bytes = osu_gameplay.ResourcesStore.ResourceManager.GetObject(filename) as byte[];

            if (bytes == null)
            {
                //use old flashlight for now
                newFlashlightOverlay = false;
                filename = @"flashlight";
                bytes = ResourcesStore.ResourceManager.GetObject(filename) as byte[];
            }

            byte[] hash = CryptoHelper.GetMd5ByteArray(bytes);

            byte[] expected = newFlashlightOverlay ?
                new byte[] { 213, 93, 96, 252, 7, 242, 209, 88, 2, 29, 38, 126, 115, 243, 173, 82 } :
                new byte[] { 10, 176, 7, 248, 35, 235, 20, 80, 98, 129, 47, 153, 198, 132, 182, 81 };

            bool notmatching = false;

            for (int i = 0; i < 16; i++)
            {
                if (hash[i] != expected[i])
                {
                    notmatching = true;
                    break;
                }
            }

            if (notmatching)
            {
                GameBase.Scheduler.Add(delegate
                {
                    Player.flag |= BadFlags.FlashlightChecksumIncorrect;
                    Player.wasAllowingSubmission = false;
                }, true);
            }

            s_Flashlight.Texture = pTexture.FromBytes(bytes);
        }

        internal int haxCheckCount;
        protected bool newFlashlightOverlay;

        /// <summary>
        /// Changes skin if necessary.
        /// </summary>
        internal virtual void InitializeSkin()
        {
            if (Player.Retrying)
                return;

            //If we need a preferred skin, we have to do a load here.
            if (!string.IsNullOrEmpty(BeatmapManager.Current.SkinPreference) && !BeatmapManager.Current.DisableSkin && !ConfigManager.sIgnoreBeatmapSkins)
            {
                if (!SkinManager.LoadSkin(BeatmapManager.Current.SkinPreference) && !Player.Retrying)
                    NotificationManager.ShowMessage(
            "This beatmap is requesting a skin preference of '" + BeatmapManager.Current.SkinPreference + "'.  Please download this skin for the intended experience",
            Color.OrangeRed, 6000);
            }
            else
                SkinManager.LoadSkin();
        }

        internal virtual void InitializeSprites()
        {
            if (AllowSubmission && !ModManager.AllowRanking(Player.currentScore.enabledMods))
            {
                pSprite unranked = new pSprite(SkinManager.Load(@"play-unranked"), Origins.Centre,
                                               new Vector2(320, 45), 0, true, Color.White);
                unranked.FadeInFromZero(2000);
                player.spriteManagerInterface.Add(unranked);
            }
        }

        internal abstract HitObjectManager CreateHitObjectManager();


        internal virtual void InitializeComboCounter()
        {
            ComboCounter = new ComboCounter(spriteManagerWidescreen);
        }

        internal abstract void CreateAutoplayReplay();

        internal void BreakStart()
        {
            if (HpBar != null) HpBar.SlideOut();
            if (ComboCounter != null) ComboCounter.SlideOut();
        }

        internal void BreakStop()
        {
            if (HpBar != null) HpBar.SlideIn();
            if (ComboCounter != null) ComboCounter.SlideIn();
        }

        //anything draw before hitobjectmanager.
        internal virtual void DrawBefore()
        {

        }

        internal virtual void Draw()
        {
            if (!IsInitialized)
                return;

            ScoreDisplay.Draw();

            if (HpBar != null) HpBar.Draw();

            DrawFlashlight();

            if (ScoreMeter != null) ScoreMeter.Draw();
        }

        internal virtual void DrawFlashlight()
        {
            if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Flashlight))
            {
                if (s_Flashlight.Texture == null ||
                    spriteManagerFlashlight.SpriteList.Count == 0 ||
                    (GameBase.OGL && !s_Flashlight.Texture.TextureGl.Loaded) ||
                    (GameBase.D3D && s_Flashlight.Texture.TextureXna == null))
                {
                    if (AllowSubmission)
                    {
                        CurrentScore.AllowSubmission = false;
                        player.AddFlashlightFlag();
                    }
                    return;
                }

                if (!Player.Paused)
                {
                    if (Player.Unpausing && player.pauseCursor != null)
                        s_Flashlight.Position =
                            new Vector2(
                                Math.Max(0, Math.Min(640, player.pauseCursor.Position.X / GameBase.WindowRatio)),
                                Math.Max(0, Math.Min(480, player.pauseCursor.Position.Y / GameBase.WindowRatio)));

                    SliderOsu s = player.ActiveHitObject as SliderOsu;
                    if (s != null && s.IsSliding)
                    {
                        if (!s_LightsOut.IsVisible)
                            s_LightsOut.Alpha = 0.8f;
                    }
                    else if (s_LightsOut.IsVisible)
                        s_LightsOut.Alpha = 0;

                    if (GameBase.SixtyFramesPerSecondFrame)
                    {
                        if (EventManager.BreakMode)
                        {
                            if (s_Flashlight.Scale < 8)
                                s_Flashlight.Scale += 0.1f;
                        }
                        else
                        {
                            if (ComboCounter.HitCombo < 100)
                            {
                                if (s_Flashlight.Scale > 3.2f)
                                    s_Flashlight.Scale -= 0.1f;
                                if (s_Flashlight.Scale < 3.2f)
                                    s_Flashlight.Scale += 0.1f;
                            }
                            else if (ComboCounter.HitCombo < 200)
                            {
                                if (s_Flashlight.Scale > 2.6f)
                                    s_Flashlight.Scale -= 0.1f;
                                if (s_Flashlight.Scale < 2.6f)
                                    s_Flashlight.Scale += 0.1f;
                            }
                            else
                            {
                                if (s_Flashlight.Scale > 2f)
                                    s_Flashlight.Scale -= 0.1f;
                                if (s_Flashlight.Scale < 2f)
                                    s_Flashlight.Scale += 0.1f;
                            }
                        }
                    }
                }

                spriteManagerFlashlight.Draw();
            }
        }

        internal void Update()
        {
            if (!IsInitialized)// || Player.Paused) doing this causes strange stuff to happen (gametime based animations etc. not updating)
                return;

            //General updates - main place for misc mode-specific updates.
            UpdateGeneral();

            UpdateHp();

            if (HpBar != null) HpBar.Update();

            UpdateCombo();
            if (ScoreMeter != null)
                ScoreMeter.Update();
        }

        /// <summary>
        /// Updates the scoring.  Must be called separate to Update for the time being. (conflict with hitObjectManager update)
        /// </summary>
        internal virtual void UpdateScoring()
        {
            //Update any active slider/spinner.
            if (player.ActiveHitObject != null)
            {
                bool activeBefore = player.ActiveHitObject is SliderOsu && ((SliderOsu)player.ActiveHitObject).IsSliding;
                IncreaseScoreHit(player.ActiveHitObject.GetScorePoints(InputManager.CursorPosition), player.ActiveHitObject);
                bool activeAfter = player.ActiveHitObject is SliderOsu && ((SliderOsu)player.ActiveHitObject).IsSliding;

                player.forceReplayFrame |= activeBefore != activeAfter;
            }
        }

        internal virtual void UpdateGeneral()
        {
        }

        internal virtual void UpdateHp()
        {
            if (HpBar != null && ModManager.CheckActive(CurrentScore.enabledMods, Mods.SuddenDeath) && CurrentScore.countMiss > 0)
                HpBar.SetCurrentHp(0);
        }

        internal virtual void UpdateCombo()
        {
            ComboCounter.Update();
        }

        internal virtual void ResetCombo()
        {
            ComboCounter.Reset();
        }

        internal virtual void UpdateInput()
        {
            if (!IsInitialized || Player.Paused)
                return;
        }

        internal virtual bool Fail(bool continuousPlay = false)
        {
            if (!continuousPlay)
            {
                if (Recoveries > 0)
                {
                    DoRecovery(false);
                    return false;
                }

                if (!Player.Failed)
                {
                    //Player has not yet failed, so lets do that now.
                    foreach (HitObject h in hitObjectManager.hitObjectsMinimal)
                        h.StopSound();
                    AudioEngine.StopAllSampleEvents();
                    Player.Failed = true;
                    Player.FailTime = AudioEngine.Time;
                    if (Player.Mode != PlayModes.OsuMania)
                        Player.currentScore.replay.Add(new bReplayFrame(AudioEngine.Time + 5, MousePosition.X, MousePosition.Y, pButtonState.None));

                    if (ModManager.CheckActive(CurrentScore.enabledMods, Mods.Perfect))
                        Player.Retry();
                    else
                        failSound = AudioEngine.PlaySample(@"failsound", 100, SkinSource.All);
                }
                return true;
            }

            if (Recoveries > 0)
            {
                DoRecovery(true);
                return false;
            }

            if (Player.Mode == PlayModes.OsuMania || Player.Mode == PlayModes.Taiko)
                return true;

            player.SetFailedState(true);

            return true;
        }

        internal void DoRecovery(bool continuousPlay)
        {
            if (HpBar.CurrentHp < 160)
            {
                HpBar.IncreaseCurrentHp(160);
                HpBar.InitialIncrease = true;
                HpBar.InitialIncreaseRate = 0.08f;
                HpBar.SetKiColour(Color.Red);
                HpBar.SetKiColour(Color.White);
            }

            if (!continuousPlay)
            {
                Player.Recovering = true;
                AudioEngine.TogglePause();
                AudioEngine.PlaySample(@"readys", 100, SkinSource.All);
            }

            player.SetFailedState(false);

            Recoveries--;
        }

        internal virtual void OnCompletion()
        {
            player.Passed = true;
            CurrentScore.pass = true;

            CurrentScore.perfect = TotalHitsPossible - CurrentScore.maxCombo <= 0;

            if (HpBar != null && HpBar.Visible)
            {
                lock (spriteManager.SpriteList)
                    foreach (pSprite p in spriteManager.SpriteList)
                        p.FadeOut(700);
                lock (spriteManagerAdd.SpriteList)
                    foreach (pSprite p in spriteManagerAdd.SpriteList)
                        p.FadeOut(700);
                HpBar.Visible = false;
            }
        }

        internal void ResetScore()
        {
            CurrentScore = Player.currentScore;

            if (!IsInitialized) return;

            if (ComboCounter != null) ComboCounter.Reset();
            if (HpBar != null) HpBar.Reset();
        }


        internal virtual void OnIncreaseScoreHit(IncreaseScoreType ist, double hpIncrease, HitObject h)
        {
            if (hpIncrease <= 0) return;

            if (HpBar != null)
            {
                HpBar.KiBulge();

                if (HpBar.CurrentHp > 180)
                    HpBar.KiExplode();
            }
        }

        internal virtual bool IsImportantFrame()
        {
            return (!EventManager.BreakMode &&
                    ((Player.IsSliding && Player.Relaxing) ||
                     (GameBase.SixtyFramesPerSecondFrame &&
                      (InputManager.leftButton == ButtonState.Pressed || //Mouse button changed
                       InputManager.rightButton == ButtonState.Pressed)) ||
                     InputManager.leftButton != InputManager.leftButtonLast ||
                     InputManager.rightButton != InputManager.rightButtonLast));
        }

        internal virtual void IncreaseScoreHit(IncreaseScoreType value, HitObject ho)
        {
            if (value == IncreaseScoreType.Ignore) return;

            player.PendingScoreChangeObject = ho;
            bool addCounters = ho.IsScorable;

            if (IsInitialized)
                player.HaxCheck();

            double hpIncrease;
            bool increaseCombo = true;

            int scoreIncrease = 0;
            double scoreIncreaseD = 0;

            bool addComboMultiplier = false;

            IncreaseScoreType scoringValue = value;
            if (scoringValue > 0) scoringValue &= ~IncreaseScoreType.NonScoreModifiers;

            switch (scoringValue)
            {
                case IncreaseScoreType.MissHpOnlyNoCombo:
                    hpIncrease =
                        hitObjectManager.MapDifficultyRange(BeatmapManager.Current.DifficultyHpDrainRate, -4, -15, -28);
                    increaseCombo = false;
                    break;
                case IncreaseScoreType.MissHpOnly:
                    hpIncrease =
                        hitObjectManager.MapDifficultyRange(BeatmapManager.Current.DifficultyHpDrainRate, -4, -15, -28);
                    break;
                case IncreaseScoreType.Miss:
                    hpIncrease =
                        hitObjectManager.MapDifficultyRange(BeatmapManager.Current.DifficultyHpDrainRate, -6, -25, -40);
                    if (addCounters)
                        CurrentScore.countMiss++;
                    break;
                case IncreaseScoreType.MissManiaHoldBreak:
                    hpIncrease = 0;
                    break;
                case IncreaseScoreType.MissMania:
                    hpIncrease = -(BeatmapManager.Current.DifficultyHpDrainRate + 1) * 1.5;
                    if (addCounters)
                        CurrentScore.countMiss++;
                    break;
                case IncreaseScoreType.ComboAddition:
                    hpIncrease = (0.5 - BeatmapManager.Current.DifficultyHpDrainRate * 0.05);
                    scoreIncrease = 0;
                    break;
                case IncreaseScoreType.SliderTick:
                    scoreIncrease = 10;
                    hpIncrease = HpMultiplierNormal * HP_SLIDER_TICK;
                    {
                        SliderOsu s = player.ActiveHitObject as SliderOsu;
                        if (s != null && !SkinManager.UseNewLayout)
                        {
                            pSprite p = new pSprite(SkinManager.Load(@"sliderpoint10"), Fields.Gamefield,
                                                    Origins.Centre, Clocks.Audio, s.sliderBall.Position,
                                                    1, false, Color.White);
                            p.MoveToRelative(new Vector2(0, -10), 300, EasingTypes.Out);
                            p.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0,
                                                                     AudioEngine.Time + 300, AudioEngine.Time + 360));
                            hitObjectManager.spriteManager.Add(p);
                        }
                    }

                    break;
                case IncreaseScoreType.SliderRepeat:
                    scoreIncrease = 30;
                    hpIncrease = HpMultiplierNormal * HP_SLIDER_REPEAT;
                    {
                        SliderOsu s = player.ActiveHitObject as SliderOsu;
                        if (s != null && !SkinManager.UseNewLayout)
                        {
                            pSprite p = new pSprite(SkinManager.Load(@"sliderpoint30"), Fields.Gamefield,
                                                    Origins.Centre, Clocks.Audio, s.sliderBall.Position,
                                                    1, false, Color.White);
                            p.MoveToRelative(new Vector2(0, -10), 300, EasingTypes.Out);
                            p.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0,
                                                                     AudioEngine.Time + 300, AudioEngine.Time + 360));
                            hitObjectManager.spriteManager.Add(p);
                        }
                    }
                    break;
                case IncreaseScoreType.SliderEnd:
                    scoreIncrease = 30;
                    hpIncrease = HpMultiplierNormal * HP_SLIDER_REPEAT;
                    break;
                case IncreaseScoreType.Hit50:
                    scoreIncrease = 50;
                    increaseCombo = !ho.IsType(HitObjectType.Slider);
                    addComboMultiplier = true;

                    hpIncrease = HpMultiplierNormal *
                                 hitObjectManager.MapDifficultyRange(BeatmapManager.Current.DifficultyHpDrainRate,
                                                                   HP_HIT_50 * 8, HP_HIT_50, HP_HIT_50);
                    if (addCounters)
                        CurrentScore.count50++;
                    break;
                case IncreaseScoreType.Hit100:
                    scoreIncrease = this is RulesetTaiko ? 150 : 100;
                    increaseCombo = !ho.IsType(HitObjectType.Slider);
                    addComboMultiplier = true;
                    hpIncrease = HpMultiplierNormal *
                                 hitObjectManager.MapDifficultyRange(BeatmapManager.Current.DifficultyHpDrainRate,
                                                                   HP_HIT_100 * 8, HP_HIT_100, HP_HIT_100);
                    if (addCounters)
                        CurrentScore.count100++;
                    break;
                case IncreaseScoreType.Hit300:
                    scoreIncrease = 300;
                    increaseCombo = !ho.IsType(HitObjectType.Slider);
                    addComboMultiplier = true;
                    hpIncrease = HpMultiplierNormal * HP_HIT_300;
                    if (addCounters)
                        CurrentScore.count300++;
                    break;
                case IncreaseScoreType.Hit50m:
                    scoreIncrease = 50;
                    increaseCombo = !ho.IsType(HitObjectType.Slider);
                    addComboMultiplier = true;
                    hpIncrease = (HpMultiplierComboEnd * HP_COMBO_MU) +
                                 (HpMultiplierNormal *
                                  hitObjectManager.MapDifficultyRange(BeatmapManager.Current.DifficultyHpDrainRate,
                                                                    HP_HIT_50 * 8, HP_HIT_50, HP_HIT_50));
                    if (addCounters)
                        CurrentScore.count50++;
                    break;
                case IncreaseScoreType.Hit100m:
                    scoreIncrease = 100;
                    increaseCombo = !ho.IsType(HitObjectType.Slider);
                    addComboMultiplier = true;
                    hpIncrease = (HpMultiplierComboEnd * HP_COMBO_MU) +
                                 (HpMultiplierNormal *
                                  hitObjectManager.MapDifficultyRange(BeatmapManager.Current.DifficultyHpDrainRate,
                                                                    HP_HIT_100 * 8, HP_HIT_100, HP_HIT_100));
                    if (addCounters)
                        CurrentScore.count100++;
                    break;
                case IncreaseScoreType.Hit300m:
                    scoreIncrease = 300;
                    increaseCombo = !ho.IsType(HitObjectType.Slider);
                    addComboMultiplier = true;
                    hpIncrease = (HpMultiplierComboEnd * HP_COMBO_MU) + (HpMultiplierNormal * HP_HIT_300);
                    if (addCounters)
                        CurrentScore.count300++;
                    break;
                case IncreaseScoreType.Hit100k:
                    scoreIncrease = 100;
                    increaseCombo = !ho.IsType(HitObjectType.Slider);
                    addComboMultiplier = true;
                    hpIncrease = (HpMultiplierComboEnd * HP_COMBO_KATU) +
                                 (HpMultiplierNormal *
                                  hitObjectManager.MapDifficultyRange(BeatmapManager.Current.DifficultyHpDrainRate,
                                                                    HP_HIT_100 * 8, HP_HIT_100, HP_HIT_100));
                    if (addCounters)
                    {
                        CurrentScore.count100++;
                        CurrentScore.countKatu++;
                    }
                    break;
                case IncreaseScoreType.Hit300k:
                    scoreIncrease = 300;
                    increaseCombo = !ho.IsType(HitObjectType.Slider);
                    addComboMultiplier = true;
                    hpIncrease = (HpMultiplierComboEnd * HP_COMBO_KATU) + (HpMultiplierNormal * HP_HIT_300);
                    if (addCounters)
                    {
                        CurrentScore.count300++;
                        CurrentScore.countKatu++;
                    }
                    break;
                case IncreaseScoreType.Hit300g:
                    scoreIncrease = 300;
                    increaseCombo = !ho.IsType(HitObjectType.Slider);
                    addComboMultiplier = true;
                    hpIncrease = (HpMultiplierComboEnd * HP_COMBO_GEKI) + (HpMultiplierNormal * HP_HIT_300);
                    if (addCounters)
                    {
                        CurrentScore.count300++;
                        CurrentScore.countGeki++;
                    }
                    break;
                case IncreaseScoreType.SpinnerSpin:
                    scoreIncrease = 0;
                    hpIncrease = HpMultiplierNormal * 1.7;
                    increaseCombo = false;
                    break;
                case IncreaseScoreType.SpinnerSpinPoints:
                    scoreIncrease = 100;
                    hpIncrease = HpMultiplierNormal * 1.7;
                    increaseCombo = false;
                    break;
                case IncreaseScoreType.SpinnerBonus:
                    scoreIncrease = 1100;
                    hpIncrease = HpMultiplierNormal * 2;
                    increaseCombo = false;
                    break;
                case IncreaseScoreType.FruitTick:
                    scoreIncrease = 100;
                    hpIncrease = HpMultiplierNormal * HP_SLIDER_TICK;
                    if (addCounters)
                        CurrentScore.count100++;
                    break;
                case IncreaseScoreType.FruitTickTiny:
                    scoreIncrease = 10;
                    increaseCombo = false;
                    hpIncrease = HpMultiplierNormal * HP_SLIDER_TICK * 0.1f;
                    if (addCounters)
                        CurrentScore.count50++;
                    break;
                case IncreaseScoreType.FruitTickTinyMiss:
                    if (addCounters)
                        CurrentScore.countKatu++;
                    if (IsInitialized)
                        haxCheckCount++;
                    return;
                case IncreaseScoreType.TaikoDrumRoll:
                    scoreIncrease = 300;
                    increaseCombo = false;
                    hpIncrease = 0.0001;
                    if (ho.Kiai)
                        scoreIncrease = (int)(scoreIncrease * 1.2f);
                    break;
                case IncreaseScoreType.TaikoDenDenHit:
                    scoreIncrease = 300;
                    //hpIncrease = HpMultiplierNormal * 1.7;
                    hpIncrease = 0;
                    increaseCombo = false;
                    break;
                case IncreaseScoreType.TaikoDenDenComplete:
                    increaseCombo = false;
                    scoreIncrease = 300;
                    addComboMultiplier = true;
                    hpIncrease = 0;
                    break;
                case IncreaseScoreType.ManiaHit300g:
                    scoreIncrease = JudgementMania.Score300g;
                    addComboMultiplier = true;
                    hpIncrease = HpMultiplierNormal * (1.1 - BeatmapManager.Current.DifficultyHpDrainRate * 0.1);
                    if (addCounters)
                        CurrentScore.countGeki++;
                    break;
                case IncreaseScoreType.ManiaHit300:
                    scoreIncrease = 300;
                    addComboMultiplier = true;
                    hpIncrease = HpMultiplierNormal * (1.0 - BeatmapManager.Current.DifficultyHpDrainRate * 0.1);
                    if (addCounters)
                        CurrentScore.count300++;
                    break;
                case IncreaseScoreType.ManiaHit200:
                    scoreIncrease = 200;
                    addComboMultiplier = true;
                    hpIncrease = HpMultiplierNormal * (0.8 - BeatmapManager.Current.DifficultyHpDrainRate * 0.08);
                    if (addCounters)
                        CurrentScore.countKatu++;
                    break;
                case IncreaseScoreType.ManiaHit100:
                    scoreIncrease = 100;
                    addComboMultiplier = true;

                    hpIncrease = 0;
                    if (addCounters)
                        CurrentScore.count100++;
                    break;
                case IncreaseScoreType.ManiaHit50:
                    scoreIncrease = 50;
                    addComboMultiplier = true;

                    hpIncrease = -(BeatmapManager.Current.DifficultyHpDrainRate + 1) * 0.32;
                    if (addCounters)
                        CurrentScore.count50++;
                    break;
                default:
                    hpIncrease = 0;
                    increaseCombo = false;
                    break;
            }

            if (CurrentScore.playMode == PlayModes.OsuMania)
            {
                int delta = 0;

                double multiplier = ModManager.ScoreMultiplier(Player.currentScore.enabledMods & Mods.ScoreIncreaseMods);
                switch (scoringValue)
                {
                    case IncreaseScoreType.MissMania:
                        comboAddition = -56 / multiplier;
                        delta = 0;
                        break;
                    case IncreaseScoreType.ManiaHit50:
                        comboAddition -= 44 / multiplier;
                        delta = 4;
                        break;
                    case IncreaseScoreType.ManiaHit100:
                        comboAddition -= 24 / multiplier;
                        delta = 8;
                        break;
                    case IncreaseScoreType.ManiaHit200:
                        comboAddition -= 8 / multiplier;
                        delta = 16;
                        break;
                    case IncreaseScoreType.ManiaHit300:
                        comboAddition += 1;
                        delta = 32;
                        break;
                    case IncreaseScoreType.ManiaHit300g:
                        comboAddition += 2;
                        delta = 32;
                        break;
                }
                if (CurrentScore.currentCombo != 0 && CurrentScore.currentCombo % JudgementMania.ComboBonus == 0)
                    comboAddition = JudgementMania.ComboMulti;
                comboAddition = Math.Max(0, Math.Min(comboAddition, JudgementMania.ComboMulti));
                scoreIncreaseD = Math.Sqrt(comboAddition) * delta * ScoreMultiplier / JudgementMania.Score300g;
            }

            int offset = 0; //special combo offset for taiko double-hits.

            //First times both hits by two, then halve below if need be (so we can get the correct score increase).
            if (value > 0 && (value & IncreaseScoreType.NonScoreModifiers) > 0)
            {
                if (scoreIncrease > 0)
                {
                    if (!increaseCombo)
                        scoreIncrease += (scoreIncrease / 5); //Case for drum roll hits.
                }
                else
                    scoreIncrease = ho.scoreValue / 50 * 50;

                if ((value & IncreaseScoreType.TaikoLargeHitSecond) > 0)
                    offset = -1;
            }

            ho.scoreValue = scoreIncrease;

            if (addComboMultiplier)
            {
                switch (Player.Mode)
                {
                    case PlayModes.Taiko:
                        scoreIncrease +=
                            (int)
                            ((scoreIncrease / 35) * 2 * (BeatmapManager.Current.DifficultyPeppyStars + 1) *
                             ModManager.ScoreMultiplier(CurrentScore.enabledMods)) *
                            (Math.Min(100, ComboCounter.HitCombo + offset) / 10);

                        // this should fix dvorak's taiko scoring issue:
                        // https://osu.sifterapp.com/projects/4151/issues/329867/comments
                        // roll ticks and spinner completions use the old method, but circles
                        // and slider starts receive kiai bonus based on whether they're inside
                        // a kiai section or not, not the current kiai status.
                        if ((scoringValue == IncreaseScoreType.TaikoDenDenComplete && Player.KiaiActive) || ho.Kiai)
                            scoreIncrease = (int)(scoreIncrease * 1.2f);
                        break;
                    case PlayModes.OsuMania:
                        //Clear any inconsistencies caused by int rounding
                        CurrentScore.totalScore = (int)CurrentScore.totalScoreD;

                        //reset scoreIncrease to a relative value.
                        scoreIncreaseD += scoreIncrease * ScoreMultiplier / JudgementMania.Score300g;
                        scoreIncrease = (int)scoreIncreaseD;
                        break;
                    default:
                        scoreIncrease += (int)(Math.Max(0, ComboCounter.HitCombo - 1) *
                                                (scoreIncrease / 25 * ScoreMultiplier));
                        break;
                }
            }

            if (value > 0)
            {
                if ((value & (IncreaseScoreType.TaikoLargeHitFirst | IncreaseScoreType.TaikoLargeHitSecond)) > 0)
                {
                    if (scoreIncrease > 0)
                    {
                        if (!increaseCombo)
                            scoreIncrease += (scoreIncrease / 5); //Case for drum roll hits.
                    }
                    else
                        scoreIncrease = ho.scoreValue / 50 * 50;

                    if ((value & IncreaseScoreType.TaikoLargeHitSecond) > 0)
                        increaseCombo = false;
                }

                //This is the only case that deals with big notes in taiko
                if ((value & (IncreaseScoreType.TaikoLargeHitBoth | IncreaseScoreType.TaikoDenDenComplete)) > 0)
                {
                    if (increaseCombo)
                    {
                        if (ho.scoreValue == 300)
                        {
                            //Perfect Timing
                            CurrentScore.countGeki++;
                        }
                        else if (ho.scoreValue == 150)
                        {
                            //Bad Timing
                            CurrentScore.countKatu++;
                        }
                        else
                        {

                        }
                    }
                    scoreIncrease *= 2;
                }
            }


            if (increaseCombo)
            {
                if (value > 0)
                {
                    CurrentScore.maxCombo = Math.Max(CurrentScore.maxCombo, ++ComboCounter.HitCombo);
                    CurrentScore.currentCombo = (ushort)ComboCounter.HitCombo;
                }
                else
                {
                    if (ComboCounter.HitCombo > 20 && !Player.Relaxing && !Player.Relaxing2)
                    {
                        AudioEngine.PlaySample(AudioEngine.LoadSample(@"combobreak"));
                        if (GameBase.Tournament)
                        {
                            GameBase.TourneySpectatorName.FlashColour(Color.Red, 2000);
                            GameBase.TourneySpectatorName.Scale = 0.8f;
                            GameBase.TourneySpectatorName.ScaleTo(1, 2000, EasingTypes.OutElastic);
                        }
                    }

                    CurrentScore.currentCombo = (ushort)(ComboCounter.HitCombo = 0);

                    if (ModManager.CheckActive(CurrentScore.enabledMods, Mods.SuddenDeath))
                        HpBar.SetCurrentHp(0); //Set HP to 0 on a slider miss too.
                }
            }

            if (player.Status != PlayerStatus.Busy)
                OnIncreaseScoreHit(value, hpIncrease, ho);

            if (hpIncrease > 0)
                HpBar.IncreaseCurrentHp(hpIncrease);
            else
                HpBar.ReduceCurrentHp(-hpIncrease);

            if (addCounters || (value & IncreaseScoreType.TaikoLargeHitSecond) > 0)
            {
                CurrentScore.totalScore += scoreIncrease;
                CurrentScore.totalScoreD += scoreIncreaseD;
            }

            if (player.Status == PlayerStatus.Playing && ScoreMeter != null)
                ScoreMeter.AddMeter(value);

            if (IsInitialized)
            {
                haxCheckCount++;
                player.UpdateChecksum();
            }
        }

        internal virtual void OnSkip()
        {
            if (HpBar != null) HpBar.SetDisplayHp(190);
        }

        internal virtual void comboBurstStars()
        {
            double r = GameBase.random.NextDouble();

            for (int i = 0; i < Math.Min(16, ComboCounter.HitCombo / 6); i++)
            {
                int time2 = GameBase.Time + GameBase.random.Next(300, 1000);

                if (r > 0.5 || ComboCounter.HitCombo >= 100)
                {
                    Vector2 startVector = new Vector2(-10, GameBase.random.Next(0, 480));

                    pSprite p2 = new pSprite(player.t_star2, Fields.TopLeft,
                                             Origins.Centre,
                                             Clocks.Game,
                                             startVector, 0.05F, false, Color.White);

                    Transformation ts =
                        new Transformation(TransformationType.Scale, 1, (float)(GameBase.random.NextDouble() * 2 + 1),
                                           GameBase.Time, time2);

                    //Transformation tm = new Transformation(startVector, endVector, GameBase.Time, time2);
                    Transformation tf = new Transformation(TransformationType.Fade, 1, 0, GameBase.Time, time2);
                    Transformation tr =
                        new Transformation(TransformationType.Rotation, (float)(GameBase.random.NextDouble() * 4 - 2),
                                           (float)(GameBase.random.NextDouble() * 4 - 2), GameBase.Time, time2);

                    ts.Easing = EasingTypes.Out;
                    tf.Easing = EasingTypes.Out;
                    tr.Easing = EasingTypes.Out;

                    p2.Transformations.Add(ts);
                    p2.Transformations.Add(tf);
                    p2.Transformations.Add(tr);

                    spriteManagerAdd.Add(p2);
                    GravitySprite gs = GameBase.PhysicsManager.Add(p2);
                    gs.gravity = new Vector2(GameBase.random.Next(100, 500), GameBase.random.Next(-120, 0));
                }

                if (r <= 0.5 || ComboCounter.HitCombo >= 100)
                {
                    Vector2 startVector = new Vector2(GameBase.WindowWidthScaled + 10, GameBase.random.Next(0, 480));

                    pSprite p2 = new pSprite(player.t_star2, Fields.TopLeft,
                                             Origins.Centre,
                                             Clocks.Game,
                                             startVector, 0.05F, false, Color.White);

                    Transformation ts =
                        new Transformation(TransformationType.Scale, 1,
                                           (float)(GameBase.random.NextDouble() * 1.5 + 1),
                                           GameBase.Time, time2);

                    Transformation tf = new Transformation(TransformationType.Fade, 1, 0, GameBase.Time, time2);
                    Transformation tr =
                        new Transformation(TransformationType.Rotation, (float)(GameBase.random.NextDouble() * 4 - 2),
                                           (float)(GameBase.random.NextDouble() * 4 - 2), GameBase.Time, time2);

                    ts.Easing = EasingTypes.Out;
                    tf.Easing = EasingTypes.Out;
                    tr.Easing = EasingTypes.Out;

                    p2.Transformations.Add(ts);
                    p2.Transformations.Add(tf);
                    p2.Transformations.Add(tr);

                    spriteManagerAdd.Add(p2);
                    GravitySprite gs = GameBase.PhysicsManager.Add(p2);
                    gs.gravity = new Vector2(GameBase.random.Next(-500, -100), GameBase.random.Next(-120, 0));
                }
            }
        }

        internal virtual void ComboBurst()
        {
            double r = GameBase.random.NextDouble();

            if (comboBurstSprites != null && comboBurstSprites.Length > 0)
            {
                if (SkinManager.Current.ComboBurstRandom) //random combo burst
                    comboBurstNumber = GameBase.random.Next(0, comboBurstSprites.Length);

                pSprite cb = new pSprite(comboBurstSprites[comboBurstNumber], Fields.TopLeft, Origins.BottomLeft,
                                         Clocks.Game, Vector2.Zero, 1, false, Color.White);
                Vector2 startPosition = r > 0.5
                                            ? new Vector2(GameBase.WindowWidthScaled, 480)
                                            : new Vector2(-comboBurstSprites[comboBurstNumber].Width * 0.625f, 480);
                Vector2 endPosition = r > 0.5
                                          ? new Vector2(GameBase.WindowWidthScaled - comboBurstSprites[comboBurstNumber].Width * 0.625f, 480)
                                          : new Vector2(0, 480);
                if (r > 0.5)
                    cb.FlipHorizontal = true;
                Transformation move = new Transformation(startPosition, endPosition, GameBase.Time,
                                                         GameBase.Time + 700, EasingTypes.Out);
                cb.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + 200,
                                                          GameBase.Time + 1200, EasingTypes.In));
                cb.Transformations.Add(move);
                player.spriteManagerBelowHitObjectsWidescreen.Add(cb);

                comboBurstNumber = (comboBurstNumber + 1) % comboBurstSprites.Length;
            }

            comboBurstStars();
        }

        internal void KiaiFountain()
        {
            player.bloomBurst = 1;
            player.eventManager.Flash();

            for (int i = 0; i < 50; i++)
            {
                int time1 = AudioEngine.Time + i * 10;
                int time2 = time1 + GameBase.random.Next(300, 1000);

                Transformation ts =
                    new Transformation(TransformationType.Scale, 1,
                                       (float)(GameBase.random.NextDouble() * (ConfigManager.sMyPcSucks ? 1.1 : 1.8) + 1),
                                       time1, time2);
                Transformation tf = new Transformation(TransformationType.Fade, 1, 0, time1, time2);
                Transformation tr =
                    new Transformation(TransformationType.Rotation, (float)(GameBase.random.NextDouble() * 4 - 2),
                                       (float)(GameBase.random.NextDouble() * 4 - 2), time1, time2);

                ts.Easing = EasingTypes.Out;
                tf.Easing = EasingTypes.Out;
                tr.Easing = EasingTypes.Out;

                //Left Fountain
                Vector2 startVector = new Vector2(30, 490);
                pSprite p1 = new pSprite(player.t_star2, Fields.TopLeft,
                                         Origins.Centre,
                                         Clocks.Audio,
                                         startVector, 0.05F, false, Color.White);

                p1.Transformations.Add(ts);
                p1.Transformations.Add(tf);
                p1.Transformations.Add(tr);


                spriteManagerAdd.Add(p1);
                GameBase.PhysicsManager.Add(p1, new Vector2(GameBase.random.Next(300, 450), GameBase.random.Next(-800, -700))).weight = 60;

                //Right Fountain
                startVector = new Vector2(GameBase.WindowWidthScaled - 30, 490);
                pSprite p2 = new pSprite(player.t_star2, Fields.TopLeft,
                                         Origins.Centre,
                                         Clocks.Audio,
                                         startVector, 0.05F, false, Color.White);

                p2.Transformations.Add(ts);
                p2.Transformations.Add(tf);
                p2.Transformations.Add(tr);

                spriteManagerAdd.Add(p2);
                GameBase.PhysicsManager.Add(p2, new Vector2(GameBase.random.Next(-450, -300), GameBase.random.Next(-800, -700))).weight = 60;
            }
        }

        internal virtual void UpdateKiai()
        {
            if (!player.NewSyncBeatWaiting) return;

            int len = (int)(AudioEngine.beatLength * 0.75f * 100f / AudioEngine.CurrentPlaybackRate);

            foreach (HitObject h in player.hitObjectManager.hitObjectsMinimal)
            {
                int thisLen = len;

                if (thisLen + AudioEngine.Time > h.EndTime)
                    thisLen = h.EndTime - AudioEngine.Time;

                if (thisLen < 0) continue;

                if (h.IsHit || !h.IsVisible) continue;

                float finalFade = 0;// 1 - ((float)thisLen / len);

                int add = Math.Max(25, 300 - h.Colour.R - h.Colour.G - h.Colour.B);

                Color c = new Color(
                    (byte)Math.Min(h.Colour.R + add, 255),
                    (byte)Math.Min(h.Colour.G + add, 255),
                    (byte)Math.Min(h.Colour.B + add, 255));

                if (ConfigManager.sHitLighting)
                {
                    pSprite plight =
                        new pSprite(SkinManager.Load(@"lighting"),
                                    h.SpriteCollection[0].Field,
                                    Origins.Centre,
                                    Clocks.AudioOnce, h.Position, 0, false, h.Colour);
                    plight.Scale = 1.2f;
                    plight.Transformations.Add(
                        new Transformation(TransformationType.Fade, 0, 0.5f, AudioEngine.Time,
                                           AudioEngine.Time + 50));
                    plight.Transformations.Add(
                        new Transformation(TransformationType.Fade, 0.5f, finalFade, AudioEngine.Time + 50,
                                           AudioEngine.Time + thisLen * 2));
                    plight.Additive = true;
                    player.hitObjectManager.spriteManager.Add(plight);
                }

                foreach (pSprite s in h.KiaiSprites)
                {
                    if (s == null || !s.IsVisible) continue;

                    pSprite s2 = s.Clone();
                    s2.Clock = Clocks.AudioOnce;
                    s2.Depth += 0.1f;
                    s2.Additive = true;
                    s2.AlwaysDraw = false;
                    s2.InitialColour = c;
                    s2.Transformations.RemoveAll(t => t.Type == TransformationType.Fade);
                    s2.Transformations.Add(
                            new Transformation(TransformationType.Fade, s.Alpha * 0.3f, finalFade * s.Alpha, AudioEngine.Time,
                                               AudioEngine.Time + thisLen));

                    player.hitObjectManager.spriteManager.Add(s2);
                }
            }
        }

        internal virtual void OnKiaiToggle(bool active)
        {
            if (active) KiaiFountain();
        }

        internal virtual ClickAction CheckClickAction(HitObject h)
        {
            if (h.IsType(HitObjectType.Normal))
            {
                //check stack - ignore clicks on circles lower in stack.
                int index = hitObjectManager.hitObjectsMinimal.IndexOf(h);

                if (index > 0 && hitObjectManager.hitObjectsMinimal[index - 1].StackCount > 0 &&
                    hitObjectManager.hitObjectsMinimal[index - 1].IsVisible &&
                    !hitObjectManager.hitObjectsMinimal[index - 1].IsHit)
                    return ClickAction.Ignore;
            }

            bool isNextCircle = true;
            foreach (HitObject t in hitObjectManager.hitObjectsMinimal)
            {
                if (t.StartTime + hitObjectManager.HitWindow50 <= AudioEngine.Time || t.IsHit) continue;

                if (t.StartTime < h.StartTime && t != h)
                    isNextCircle = false;
                break;
            }

            int apLeniency = Player.Relaxing2 ? 200 : 0;
            return (isNextCircle && Math.Abs(h.StartTime - AudioEngine.Time) < HitObjectManager.HITTABLE_RANGE - apLeniency)
                       ? ClickAction.Hit
                       : ClickAction.Shake;
        }

        internal virtual void UpdatePassing(bool value)
        {

        }

        internal virtual void LoadComplete()
        {
        }

        internal virtual void AdjustBackgroundSprite(pSprite backgroundSprite)
        {
        }

        internal virtual bool UsesMouse { get { return false; } }
    }

    [Flags]
    internal enum IncreaseScoreType
    {
        MissManiaHoldBreak = -1048576,
        MissHpOnlyNoCombo = -524288,
        MissHpOnly = -262144,
        Miss = -131072,
        MissMania = -65536,
        Ignore = 0,
        MuAddition = 1,
        KatuAddition = 2,
        GekiAddition = 4,
        SliderTick = 8,
        FruitTickTiny = 16,
        FruitTickTinyMiss = 32,
        SliderRepeat = 64,
        SliderEnd = 128,
        Hit50 = 256,
        Hit100 = 512,
        Hit300 = 1024,
        Hit50m = Hit50 | MuAddition,
        Hit100m = Hit100 | MuAddition,
        Hit300m = Hit300 | MuAddition,
        Hit100k = Hit100 | KatuAddition,
        Hit300k = Hit300 | KatuAddition,
        Hit300g = Hit300 | GekiAddition,
        FruitTick = 2048,
        SpinnerSpin = 4096,
        SpinnerSpinPoints = 8192,
        SpinnerBonus = 16384,
        TaikoDrumRoll = 32768,
        TaikoLargeHitBoth = 65536,
        TaikoDenDenHit = 1048576,
        TaikoDenDenComplete = 2097152,
        TaikoLargeHitFirst = 4194304,
        TaikoLargeHitSecond = 8388608,
        ManiaHit50 = 16777216,
        ManiaHit100 = 33554432,
        ManiaHit200 = 67108864,
        ManiaHit300 = 134217728,
        ManiaHit300g = 268435456,    //g means glow, so there's no GekiAddition
        BaseHitValuesOnly = Hit50 | Hit100 | Hit300,
        HitValuesOnly = Hit50 | Hit100 | Hit300 | GekiAddition | KatuAddition | ManiaHit300g | ManiaHit300 | ManiaHit200 | ManiaHit100 | ManiaHit50,
        ComboAddition = MuAddition | KatuAddition | GekiAddition,
        NonScoreModifiers = TaikoLargeHitBoth | TaikoLargeHitFirst | TaikoLargeHitSecond
    }
}