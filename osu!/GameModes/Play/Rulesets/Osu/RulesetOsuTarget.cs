﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Audio;
using osu.GameplayElements;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.Scoring;
using osu.Input;
using osu_common;

namespace osu.GameModes.Play.Rulesets.Osu
{
    class RulesetOsuTarget : RulesetOsu
    {
        public RulesetOsuTarget(Player player)
            : base(player)
        {

        }

        internal override double CalculateHpDropRate()
        {
            return 0;
        }

        internal override void InitializeHpBar()
        {
            
        }

        internal override void UpdateGeneral()
        {
            if (AudioEngine.SyncNewBeat && AudioEngine.Time > player.CountdownTime)
                AudioEngine.PlaySample(@"count");
            base.UpdateGeneral();
        }

        internal override void IncreaseScoreHit(IncreaseScoreType value, HitObject h)
        {
            IncreaseScoreType scoringValue = value;
            if (scoringValue > 0) scoringValue &= IncreaseScoreType.HitValuesOnly;

            if (value < 0)
            {
                if (ModManager.CheckActive(Mods.NoFail))
                {
                    CurrentScore.currentCombo = (ushort)(ComboCounter.HitCombo = 0);
                    CurrentScore.countMiss++;
                    return;
                }
                else
                {
                    player.DoPass();
                    return;
                }
            }

            CurrentScore.maxCombo = Math.Max(CurrentScore.maxCombo, ++ComboCounter.HitCombo);
            CurrentScore.currentCombo = (ushort)ComboCounter.HitCombo;

            ScoreTarget ts = CurrentScore as ScoreTarget;

            float hitAccuracy = ((HitCircleOsuTarget)h).TargetAccuracy;

            ts.PositionTotal += hitAccuracy;

            int scoreMax = 0;

            switch (scoringValue)
            {
                case IncreaseScoreType.Hit300:
                case IncreaseScoreType.Hit300m:
                case IncreaseScoreType.Hit300k:
                case IncreaseScoreType.Hit300g:
                    scoreMax = 300;
                    CurrentScore.count300++;
                    break;
                case IncreaseScoreType.Hit100:
                case IncreaseScoreType.Hit100m:
                case IncreaseScoreType.Hit100k:
                    scoreMax = 100;
                    CurrentScore.count100++;
                    break;
                case IncreaseScoreType.Hit50:
                    scoreMax = 50;
                    CurrentScore.count50++;
                    break;
            }

            h.scoreValue = scoreMax;

            //todo: this should include some equation of distance over time, rather than current bpm.
            CurrentScore.totalScore += (int)(hitAccuracy * scoreMax * AudioEngine.CurrentBPM / 120);
        }
    }
}
