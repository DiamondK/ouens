﻿using System;
using Microsoft.Xna.Framework;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play.Components;
using osu.GameplayElements;
using osu.GameplayElements.Scoring;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu_common;
using osu.GameplayElements.HitObjects;
using osu.Input.Handlers;
using osu.Helpers;
using Microsoft.Xna.Framework.Graphics;
using osu_common.Bancho.Objects;
using osu.Graphics;
using System.Collections.Generic;

namespace osu.GameModes.Play.Rulesets.Osu
{
    internal partial class RulesetOsu : Ruleset
    {
        public RulesetOsu(Player player)
            : base(player)
        {
        }

        internal override void Initialize()
        {
            InputManager.MouseButtonInstantRelease = false;
            t_smoke = SkinManager.Load(@"cursor-smoke");
            base.Initialize();
        }

        internal override HitObjectManager CreateHitObjectManager()
        {
            if (Player.IsTargetPracticeMode)
            {
                return new HitObjectManagerOsuTarget();
            }
            else
                return new HitObjectManagerOsu();
        }

        internal override void InitializeCalculations()
        {
            if (HpBar == null) return;

            HpBar.HpDropRate = CalculateHpDropRate();

            if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.HalfTime))
                HpBar.HpDropRate *= 0.75;

            base.InitializeCalculations();
        }

        internal override void CacheFutureSprites()
        {
            base.CacheFutureSprites();

            SkinManager.LoadAll(@"particle50");
            SkinManager.LoadAll(@"particle100");
            SkinManager.LoadAll(@"particle300");

            SkinManager.LoadAll(@"sliderpoint30");
            SkinManager.LoadAll(@"sliderpoint10");
        }

        internal override bool AllowMouseHide
        {
            get
            {
                if (!Player.Playing
                    && (hitObjectManager.currentHitObjectIndex + 1 >= hitObjectManager.hitObjectsCount || Math.Abs(hitObjectManager.hitObjects[hitObjectManager.currentHitObjectIndex + 1].StartTime - AudioEngine.Time) > 3000)
                    && Vector2.Distance(MouseManager.MousePosition, MouseLastPosition) < 3 && !InputManager.leftCond && !InputManager.rightCond)
                    MouseIdleTime += GameBase.ElapsedMilliseconds;
                else
                    MouseIdleTime = 0;
                MouseLastPosition = MouseManager.MousePosition;
                return MouseIdleTime > 3000 && !InputManager.ReplayMode;
            }
        }

        internal override void InitializeSprites()
        {
            if (ConfigManager.sComboBurst)
                comboBurstSprites = SkinManager.LoadAll(@"comboburst");

            base.InitializeSprites();
        }

#if ARCADE
        const int PASSIVE_DROP_LENIENCE = 2;
#else
        const int PASSIVE_DROP_LENIENCE = 1;
#endif
        internal override void UpdateHp()
        {
            if (HpBar != null && player.DrainTime)
                HpBar.ReduceCurrentHp((HpBar.HpDropRate / PASSIVE_DROP_LENIENCE) * (AudioEngine.Time - AudioEngine.TimeLastFrame));

            base.UpdateHp();
        }

        internal override void DrawFlashlight()
        {
            if (s_Flashlight != null)
            {
                Vector2 destination = new Vector2(
                                Math.Max(0, Math.Min(640, (InputManager.s_Cursor.Position.X - GameBase.WindowOffsetX) / GameBase.WindowRatio)) + GameBase.WindowOffsetXScaled,
                                Math.Max(0, Math.Min(480, InputManager.s_Cursor.Position.Y / GameBase.WindowRatio)));
                if (s_Flashlight != null)
                    s_Flashlight.Position = OsuMathHelper.TweenValues(s_Flashlight.Position, destination,
                                         (float)GameBase.ElapsedMilliseconds, 0, 120, EasingTypes.Out);
            }

            base.DrawFlashlight();
        }

        pSprite lastSmokeSprite;
        List<pSprite> lastSmokeTrailLength = new List<pSprite>();
        pTexture t_smoke;
        internal override void UpdateInput()
        {
            if (GameBase.SixtyFramesPerSecondFrame)
            {
                if ((!InputManager.ReplayMode && KeyboardHandler.IsKeyDown(BindingManager.For(Bindings.OsuSmoke))) ||
                    (InputManager.ReplayMode && InputManager.CurrentReplayFrame != null && (InputManager.CurrentReplayFrame.buttonState & pButtonState.Smoke) > 0))
                {
                    float distance = lastSmokeSprite != null ? Vector2.Distance(lastSmokeSprite.Position, InputManager.CursorPosition) : 0;
                    float acceptedWidth = Math.Max(2, lastSmokeSprite != null ? (lastSmokeSprite.DrawWidth * 0.625f * GameBase.WindowRatio) / 4f : 0);
                    int steps = Math.Max(1, (int)(distance / acceptedWidth));

                    Vector2 end = InputManager.CursorPosition;
                    if (lastSmokeSprite == null || lastSmokeSprite.Position != end)
                    {
                        Vector2 start = lastSmokeSprite != null ? lastSmokeSprite.Position + Vector2.Normalize(end - lastSmokeSprite.Position) * Math.Min(distance, acceptedWidth) : InputManager.CursorPosition;

                        for (int j = 0; j < steps; j++)
                        {
                            pSprite smoke = new pSprite(t_smoke, Fields.NativeStandardScale, Origins.Centre, Clocks.Game, Vector2.Lerp(start, end, (float)j / steps), 1, false, Color.White);
                            smoke.Additive = true;
                            smoke.Alpha = 0.6f;
                            smoke.Scale = 0.5f;
                            smoke.Rotation = (float)GameBase.random.NextDouble() * 10;
                            smoke.RotateTo(smoke.Rotation + (float)(GameBase.random.NextDouble() - 0.5f) * 0.5f, 500, EasingTypes.Out);
                            smoke.ScaleTo(smoke.Scale * 1f, 2000, EasingTypes.Out);
                            smoke.FadeOut(4000);

                            player.spriteManagerBelowHitObjectsWidescreen.Add(smoke);
                            lastSmokeTrailLength.Add(smoke);
                            lastSmokeSprite = smoke;
                        }
                    }

                    if (lastSmokeTrailLength.Count > 50 && Vector2.Distance(lastSmokeSprite.Position, lastSmokeTrailLength[0].Position) < 10)
                        endTrail();
                }
                else
                {
                    endTrail();
                    lastSmokeSprite = null;
                }
            }

            base.UpdateInput();
        }

        private void endTrail()
        {
            if (lastSmokeTrailLength.Count > 0)
            {
                int offset = 0;
                foreach (pSprite s in lastSmokeTrailLength)
                {
                    if (s.Alpha == 0) continue;

                    Transformation f = s.Transformations.Find(t => t.Type == TransformationType.Fade);
                    f.StartFloat = 1;
                    f.Time1 = GameBase.Time + offset;
                    f.Time2 = GameBase.Time + offset + 8000;
                    f.Easing = EasingTypes.In;

                    offset += 2;
                }

                lastSmokeTrailLength.Clear();
            }
        }

        internal override void IncreaseScoreHit(IncreaseScoreType value, HitObject ho)
        {
            if (ho.LastInCombo && ho.IsHit && value != IncreaseScoreType.Ignore)
                player.UpdatePassing(value);

            base.IncreaseScoreHit(value, ho);
        }

        internal override bool UsesMouse { get { return true; } }
    }
}