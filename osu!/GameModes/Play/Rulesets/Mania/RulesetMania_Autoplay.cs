﻿using System;
using System.Collections.Generic;
using osu.GameplayElements;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu.Graphics.Sprites;
using osu.Input;
using osu_common;
using osu_common.Bancho.Objects;
using osu.GameplayElements.Scoring;
using Microsoft.Xna.Framework;
using osu.GameplayElements.HitObjects.Mania;
using osu.GameplayElements.Beatmaps;

namespace osu.GameModes.Play.Rulesets.Mania
{
    internal partial class RulesetMania
    {
        private int speed = 10;
        internal override void CreateAutoplayReplay()
        {
            Vector3 range = BeatmapManager.Current.BeatsPerMinuteRange;
            speed = (int)(2000 / range.Z);

            InputManager.ReplayScore.replay = new List<bReplayFrame>();
            List<bReplayFrame> replay = InputManager.ReplayScore.replay;

            HitObjectManagerMania homMania = hitObjectManager as HitObjectManagerMania;

            replay.Add(new bReplayFrame(0, 0, speed, pButtonState.None));
            for (int i = 0; i < hitObjectManager.hitObjectsCount; i++)
            {
                HitCircleMania note = (HitCircleMania)hitObjectManager.hitObjects[i];
                int val = 0;
                //inputmanager adjust hit position based on NeedAdjust
                //Change columns back to thier original to fit the input require.
                if (hitObjectManager.ManiaStage.SpecialStyleRight)
                    val = hitObjectManager.ManiaStage.Columns[note.Column == hitObjectManager.ManiaStage.Columns.Count - 1 ? 0 : note.Column + 1].KeyValue;
                else
                    val = note.Column.KeyValue;
                InsertReplay(replay, note.StartTime, val);
                if (note.ManiaType == ManiaNoteType.Long)
                {
                    //find exist frames
                    replay.ForEach(f =>
                    {
                        if (f.time <= note.StartTime)
                            return;
                        if (f.time >= note.EndTime)
                            return;
                        f.mouseX = MergeValue((int)f.mouseX, val);
                    });
                    InsertReplay(replay, note.EndTime - 1, -val);
                }
                else
                    InsertReplay(replay, note.StartTime + 1, -val);
            }

            Player.currentScore.replay = InputManager.ReplayScore.replay;
            Player.currentScore.playerName = "osu!topus!";
        }

        private int MergeValue(int m, int v)
        {
            if (v > 0)
            {
                if ((m & v) > 0)
                    m &= ~v;
                else
                    m |= v;
            }
            else
                if ((m & (-v)) > 0)
                    m &= ~(-v);
            return m;
        }

        private void InsertReplay(List<bReplayFrame> replay, int time, int val)
        {
            int index = replay.FindLastIndex(b => b.time <= time);
            if (index < 0)
                replay.Add(new bReplayFrame(time, MergeValue(0, val), speed, pButtonState.None));
            else if (time == 0)
                replay.Insert(index + 1, new bReplayFrame(0, MergeValue((int)replay[index].mouseX, val), speed, pButtonState.None));
            else if (replay[index].time == time)
                replay[index].mouseX = MergeValue((int)replay[index].mouseX, val);
            else
                replay.Insert(index + 1, new bReplayFrame(time, MergeValue((int)replay[index].mouseX, val), speed, pButtonState.None));
        }
    }
}