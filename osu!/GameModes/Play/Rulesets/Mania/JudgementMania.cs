﻿using System;
using System.Collections.Generic;
using System.Text;

namespace osu.GameModes.Play.Rulesets.Mania
{
    internal static class JudgementMania
    {
        internal static readonly int ScoreMax = 500000;
        internal static readonly int Score300g = 320;
        internal static readonly int ComboIntv = 100;
        internal static readonly int ComboMulti = 100;
        internal static readonly int ComboBonus = 384;//magic number
    }
}
