﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.GameplayElements.Beatmaps;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;
using osu_common;
using System;
using System.Collections.Generic;
using System.Text;

namespace osu.GameModes.Play.Rulesets.Mania
{
    internal partial class ColumnMania
    {
        private static readonly Color color2 = new Color(250, 170, 212);
        private static readonly Color colorS = new Color(247, 236, 0);

        internal StageMania Host { get; private set; }

        private pSprite keyImageUp;
        private pSprite keyImageDown;
        private pText keyWarning;
        private pAnimation lightImage;

        private pTexture[] hitLightFrames;
        private pAnimation hitLightLong;

        private int column;

        internal float Left { get; private set; }
        internal float Width { get; private set; }
        internal int SnapPositionX { get { return (int)((column >= Host.Skin.Columns ? column - 0.5f : 0.5f + column) * (512f / Host.Skin.Columns)); } }

        internal ManiaKeyType KeyType
        {
            get
            {
                int adjustedColumn = column - Host.ColumnOffset;

                //4K, 5K, 7K, 9K don't have special styles
                bool useSpecialStyle = Host.SpecialStyle != ManiaSpecialStyle.None && Host.AllowSpecialStyle;
                ManiaSpecialStyle specialStyle = Host.SpecialStyle;

                int specialColumn = Host.SpecialColumn;

                if (useSpecialStyle && specialStyle == ManiaSpecialStyle.Right)
                    specialColumn = Host.PrimaryStageColumns - 1;
                else if (useSpecialStyle)
                    specialColumn = 0;

                if (adjustedColumn == specialColumn)
                    return ManiaKeyType.SpecialKey;

                if (useSpecialStyle)
                {
                    if ((adjustedColumn % 2 == 1 && specialStyle == ManiaSpecialStyle.Right) || (adjustedColumn % 2 == 0 && specialStyle == ManiaSpecialStyle.Left))
                        return ManiaKeyType.NormalKey2;
                }
                else
                {
                    //Check if the key is a pink key
                    int midCol = (int)Math.Floor(Host.PrimaryStageColumns / 2f);
                    //Odd number of columns
                    if ((Host.PrimaryStageColumns % 2 == 1 && (adjustedColumn < midCol || adjustedColumn > midCol) && adjustedColumn % 2 == 1)
                        //Even number of columns. Here we have to adjust by 1 for the right side to compensate
                        //for one less column. This is done by checking if the column is an even index.
                        || (Host.PrimaryStageColumns % 2 == 0 && ((adjustedColumn < midCol && adjustedColumn % 2 == 1) || (adjustedColumn >= midCol && adjustedColumn % 2 == 0))))
                    {
                        return ManiaKeyType.NormalKey2;
                    }
                }

                return ManiaKeyType.NormalKey1;
            }
        }

        internal Color Colour
        {
            get
            {
                switch (KeyType)
                {
                    case ManiaKeyType.NormalKey2:
                        return color2;
                    case ManiaKeyType.SpecialKey:
                        return colorS;
                    case ManiaKeyType.NormalKey1:
                    default:
                        return Color.White;
                }
            }
        }

        internal string NoteKeySuffix
        {
            get
            {
                switch (KeyType)
                {
                    case ManiaKeyType.NormalKey2:
                        return SkinMania.ManiaNormal2;
                    case ManiaKeyType.SpecialKey:
                        return SkinMania.ManiaSpecial;
                    case ManiaKeyType.NormalKey1:
                    default:
                        return SkinMania.ManiaNormal1;
                }
            }
        }

        internal string KeyString { get { return SkinMania.ManiaKeyPrefix + NoteKeySuffix; } }
        internal string NoteString { get { return SkinMania.ManiaNotePrefix + NoteKeySuffix; } }

        internal ColumnMania(StageMania stage, int column, float widthScale)
        {
            this.Host = stage;
            this.column = column;

            float spacing = column > 0 ? stage.Skin.ColumnSpacing[column - 1] * widthScale : 0;

            Left = stage.Left + stage.Width + spacing;
            Width = stage.Skin.ColumnWidth[column] * widthScale;
            stage.Width += Width + spacing;

            if (!stage.Minimal)
            {
                //Left column line
                if (stage.Skin.ColumnLineWidth[column] > 0)
                {
                    add(new pSprite(GameBase.WhitePixel, Fields.TopLeft, stage.FlipOrigin(Origins.BottomLeft), Clocks.Audio, new Vector2(0, stage.HitPosition), 0.61f, true, stage.Skin.GetColour("ColumnLine", Color.White))
                    {
                        VectorScale = new Vector2(0.740f * stage.Skin.ColumnLineWidth[column], 1.34f * stage.Height)
                    }, stage.SpriteManager);
                }

                //Column background
                add(new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Audio, new Vector2(0, stage.Top), 0.6f, true, stage.Skin.GetColour((column + 1).ToString(), Color.Black))
                {
                    VectorScale = new Vector2(Width, stage.Height),
                    Scale = 1.6f
                }, stage.SpriteManager);

                //Right column line
                if ((stage.Skin.ColumnLineWidth[column + 1] > 0 && SkinManager.Current.Version >= 2.4) || column == stage.Skin.Columns - 1)
                {
                    add(new pSprite(GameBase.WhitePixel, Fields.TopLeft, stage.FlipOrigin(Origins.BottomLeft), Clocks.Audio,
                                    new Vector2(Width - (column == stage.Skin.Columns - 1 ? 0.10f : 0), stage.HitPosition), 0.61f, true, stage.Skin.GetColour("ColumnLine", Color.White))
                    {
                        VectorScale = new Vector2(0.740f * stage.Skin.ColumnLineWidth[column + 1], 1.34f * stage.Height)
                    }, stage.SpriteManager);
                }

                //Key sprites
                SpriteManager keySpriteManager = stage.Skin.KeysUnderNotes ? stage.SpriteManager : stage.SpriteManagerAbove;

                pTexture p = stage.Skin.Load("KeyImage" + column, KeyString, stage.SkinSource);
                keyImageUp = new pSprite(p, Fields.TopLeft, stage.FlipOrigin(Origins.BottomCentre), Clocks.Audio, new Vector2(Width / 2f, stage.ActualBottom), 0.92f, true, Color.White)
                {
                    VectorScale = new Vector2(Width * 1.6f / p.Width, stage.HeightRatio),
                    FlipVertical = stage.UpsideDown
                };
                add(keyImageUp, keySpriteManager);

                p = stage.Skin.Load("KeyImage" + column + "D", KeyString + "D", stage.SkinSource);
                keyImageDown = new pSprite(p, Fields.TopLeft, stage.FlipOrigin(Origins.BottomCentre), Clocks.Audio, new Vector2(Width / 2f, stage.ActualBottom), 0.925f, true, Color.White)
                {
                    VectorScale = new Vector2(Width * 1.6f / p.Width, stage.HeightRatio),
                    FlipVertical = stage.UpsideDown,
                    Alpha = 0
                };
                add(keyImageDown, keySpriteManager);

                //Lights
                lightImage = new pAnimation(stage.Skin.LoadAll("StageLight", "mania-stage-light", stage.SkinSource), Fields.TopLeft, stage.FlipOrigin(Origins.BottomLeft), Clocks.Audio,
                                            new Vector2(0, stage.ScaleFlipPosition(Host.Skin.LightPosition)), 0.72f, true, stage.Skin.GetColour("Light" + (column + 1), Color.White));
                lightImage.Alpha = 0;
                lightImage.FrameDelay = 1000f / stage.Skin.LightFramePerSecond;
                lightImage.RunAnimation = false;
                lightImage.LoopType = LoopTypes.LoopForever;
                lightImage.VectorScale = new Vector2(Width * 1.6f / lightImage.Width, stage.HeightRatio);
                add(lightImage, stage.SpriteManager);

                //Key warning
                keyWarning = new pText("", Width / 2f, new Vector2(Width / 2f, stage.ScaleFlipPosition(480 - keyImageUp.DrawHeight / 1.6f / 2)), 0.93f, true, stage.Skin.GetColour("KeyWarning", Color.Black))
                {
                    TextBold = true,
                    Origin = Origins.Centre,
                    Scale = stage.HeightRatio
                };
                add(keyWarning, stage.SpriteManagerAbove);

                //Hit lights
                hitLightFrames = stage.Skin.LoadAll("LightingN", "lightingN", stage.SkinSource);
                pTexture[] lightLong = stage.Skin.LoadAll("LightingL", "lightingL", stage.SkinSource);

                if (lightLong == null || lightLong.Length == 0)
                {
                    lightLong = new pTexture[hitLightFrames.Length];
                    hitLightFrames.CopyTo(lightLong, 0);
                }

                hitLightLong = new pAnimation(lightLong, Fields.TopLeft, Origins.Centre, Clocks.Audio, new Vector2(Width / 2f, stage.HitPosition), 0.98f, true, Color.White);
                hitLightLong.Scale = SkinManager.Current.Version >= 2.5 ? stage.WidthRatio : 1f;
                hitLightLong.Alpha = 0;
                hitLightLong.Additive = true;
                if (lightLong != null)
                    hitLightLong.FrameDelay = 170f / lightLong.Length;
                add(hitLightLong, stage.SpriteManagerAbove);
            }
        }

        public static implicit operator int(ColumnMania column)
        {
            return column.column;
        }

        private void add(pSprite p, SpriteManager spriteManager)
        {
            spriteManager.Add(p);
            p.Position.X = Left + p.Position.X;
        }

        internal void ResetWarning()
        {
            if (Host.Minimal) return;

            keyWarning.Text = BindingManager.NiceName(Key) + (AlternateKey != Keys.None ? "\n" + BindingManager.NiceName(AlternateKey) : string.Empty);
            keyWarning.Transformations.Clear();
            keyWarning.Alpha = 1;
            if (!Host.AlwaysShowKeyWarnings)
                keyWarning.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + 500, GameBase.Time + 3500));
        }

        internal void AddHitLight()
        {
            pAnimation hitLight = new pAnimation(hitLightFrames, Fields.TopLeft, Origins.Centre, Clocks.AudioOnce, new Vector2(Width / 2f, Host.HitPosition), 0.98f, false, Color.White);
            hitLight.Scale = SkinManager.Current.Version >= 2.5 ? Host.WidthRatio : 1f;
            hitLight.Additive = true;
            hitLight.LoopType = LoopTypes.LoopOnce;
            if (hitLightFrames != null)
                hitLight.FrameDelay = 170f / hitLightFrames.Length;
            hitLight.FadeInFromZero(80);
            hitLight.Transformations.Add(new Transformation(TransformationType.Fade, 1f, 0f, AudioEngine.Time + 80, AudioEngine.Time + 200));
            add(hitLight, Host.SpriteManagerAbove);
        }
    }

    internal enum ManiaKeyType
    {
        /// <summary>
        /// White key.
        /// </summary>
        NormalKey1,
        /// <summary>
        /// Pink key.
        /// </summary>
        NormalKey2,
        /// <summary>
        /// Yellow key.
        /// </summary>
        SpecialKey
    }
}
