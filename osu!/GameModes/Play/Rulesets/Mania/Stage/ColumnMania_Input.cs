﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace osu.GameModes.Play.Rulesets.Mania
{
    internal partial class ColumnMania
    {
        private Keys key = Keys.None;
        internal Keys Key
        {
            get { return key; }
            set
            {
                key = value;
                ResetWarning();
            }
        }
        private Keys alternateKey = Keys.None;
        internal Keys AlternateKey
        {
            get { return alternateKey; }
            set
            {
                alternateKey = value;
                ResetWarning();
            }
        }

        internal int KeyValue { get { return (int)Math.Pow(2, column); } }

        private bool hasLongHitLight;
        internal bool HasLongHitLight
        {
            get { return hasLongHitLight; }
            set
            {
                if (hasLongHitLight == value)
                    return;
                hasLongHitLight = value;

                //Match fade in/out of normal hitlights
                if (HasLongHitLight)
                {
                    hitLightLong.ResetAnimation();
                    hitLightLong.FadeIn(80);
                }
                else
                    hitLightLong.FadeOut(120);
            }
        }

        private bool touchDownState;
        internal bool KeyStateLast;
        private bool keyState;
        internal bool KeyState
        {
            get { return keyState; }
            set
            {
                KeyStateLast = keyState;
                if (keyState == value)
                    return;
                keyState = value;

                if (Host.Minimal)
                    return;

                if (keyState)
                {
                    keyImageDown.FadeIn(0);
                    keyImageUp.FadeOut(0);

                    lightImage.ResetAnimation();
                    lightImage.RunAnimation = true;
                    lightImage.Texture = lightImage.TextureArray[0];
                    lightImage.Transformations.Clear();
                    lightImage.Alpha = 1f;
                    lightImage.VectorScale.Y = Host.UpsideDown ? -Host.HeightRatio : Host.HeightRatio;
                }
                else
                {
                    //Hit lights fade in for 80ms
                    keyImageDown.Transformations.Clear();
                    keyImageDown.Transformations.Add(new Transformation(TransformationType.Fade, 1f, 0f, AudioEngine.Time + 80, AudioEngine.Time + 80));
                    keyImageUp.Transformations.Clear();
                    keyImageUp.Transformations.Add(new Transformation(TransformationType.Fade, 0f, 1f, AudioEngine.Time + 80, AudioEngine.Time + 80));

                    int lightAnimationLength = (int)(400 * (100 / AudioEngine.CurrentBPM));
                    lightImage.Transformations.Add(new Transformation(TransformationType.Fade, 1f, 0f, AudioEngine.Time, AudioEngine.Time + lightAnimationLength));
                    lightImage.Transformations.Add(new Transformation(TransformationType.VectorScale,
                                                                      new Vector2(lightImage.VectorScale.X, Host.UpsideDown ? -Host.HeightRatio : Host.HeightRatio),
                                                                      new Vector2(lightImage.VectorScale.X, 0),
                                                                      AudioEngine.Time, AudioEngine.Time + lightAnimationLength));
                }
            }
        }

        internal void UpdateInput()
        {
            bool newKeyState = false;

            if (!touchDownState)
            {
                //Check for normal key presses
                if ((int)Key > 300)
                    newKeyState = JoystickHandler.KeyDown.Contains(Key);
                else
                    newKeyState = KeyboardHandler.IsKeyDown(Key);

                //Check for alternative special key
                if (Host.SpecialColumn == this)
                {
                    if ((int)AlternateKey > 300)
                        newKeyState |= JoystickHandler.KeyDown.Contains(AlternateKey);
                    else
                        newKeyState |= KeyboardHandler.IsKeyDown(AlternateKey);
                }
            }

            if (InputManager.TouchDevice && InputManager.AllClickPositions.Count > 0)
            {
                for (int i = 0; i < InputManager.AllClickPositions.Count; i++)
                {
                    Vector2 pos = InputManager.AllClickPositions[i];
                    if (pos.X > Left && pos.X < Left + Width && pos.Y > Host.Top && pos.Y < Host.Bottom)
                        newKeyState = touchDownState = true;
                    else
                        touchDownState = false;
                }
            }
            else
                touchDownState = false;

            KeyState = newKeyState;
        }
    }
}
