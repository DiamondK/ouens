﻿using Microsoft.Xna.Framework;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.Scoring;
using osu.Graphics.Primitives;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu.Input;
using osu_common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace osu.GameModes.Play.Rulesets.Mania
{
    internal partial class StageMania
    {
        internal float Height { get; private set; }

        private float top = 0;
        internal float Top
        {
            get { return top; }
            set
            {
                if (!Minimal)
                {
                    SpriteManager.SpriteList.ForEach(s => s.Position.Y -= top - value);
                    SpriteManagerAbove.SpriteList.ForEach(s => s.Position.Y -= top - value);
                    SpriteManagerBelow.SpriteList.ForEach(s => s.Position.Y -= top - value);
                }
                top = value;
            }
        }

        private float left = 0;
        internal float Left
        {
            get { return left; }
            set
            {
                if (!Minimal)
                {
                    SpriteManager.SpriteList.ForEach(s => s.Position.X -= left - value);
                    SpriteManagerAbove.SpriteList.ForEach(s => s.Position.X -= left - value);
                    SpriteManagerBelow.SpriteList.ForEach(s => s.Position.X -= left - value);
                }
                left = value;
            }
        }
        internal float Bottom { get { return Top + Height; } }

        internal bool UpsideDown { get { return Skin.UpsideDown; } }
        internal float ActualTop { get { return UpsideDown ? Bottom : Top; } }
        internal float ActualBottom { get { return UpsideDown ? Top : Bottom; } }

        internal float Alpha
        {
            get { return Minimal ? 0f : SpriteManager.Alpha; }
            set
            {
                if (!Minimal) SpriteManagerBelow.Alpha = SpriteManagerAbove.Alpha = SpriteManager.Alpha = SpriteManagerNotes.Alpha = value;
                if (SecondaryStage != null)
                    SecondaryStage.Alpha = Alpha;
            }
        }

        internal float HitPosition { get { return ScaleFlipPosition(Skin.HitPosition); } }

        internal float HeightRatio { get { return Height / GameBase.WindowDefaultHeight; } }
        internal float WidthRatio
        {
            get
            {
                if (Skin.Columns == 0)
                    return 0;

                float width = 0;
                Skin.ColumnWidth.ForEach(c => width += c);
                return width * widthScale / (30 * Skin.Columns);
            }
        }
        internal float MinimumRatio { get { return Math.Min(WidthRatio, HeightRatio); } }

        internal float MinimumColumnWidth
        {
            get
            {
                float ret = float.MaxValue;
                Skin.ColumnWidth.ForEach(c => ret = Math.Min(ret, c));
                return ret * widthScale;
            }
        }

        internal bool AllowSpecialStyle { get { return (PrimaryStageColumns > 4 && PrimaryStageColumns % 2 == 0); } }
        internal bool SpecialStyleRight { get { return AllowSpecialStyle && Skin != null && SpecialStyle == ManiaSpecialStyle.Right; } }
        internal int RandomStart { get { return Columns.Count == 8 ? ColumnOffset + 1 : ColumnOffset; } }
        internal int SpecialColumn
        {
            get
            {
                if (PrimaryStageColumns % 2 == 0)
                {
                    if (SpecialStyle != ManiaSpecialStyle.None && AllowSpecialStyle)
                        return SpecialStyle == ManiaSpecialStyle.Left ? 0 : PrimaryStageColumns - 1;
                    return -1;
                }
                else
                {
                    //First special column is shown at position 2 when column == 5
                    return 2 + (PrimaryStageColumns - 5) / 2;
                }
            }
        }

        internal int PrimaryStageColumns
        {
            get
            {
                if (!Skin.SplitStages || stageIndex > 0)
                    return availableColumns;
                return (availableColumns + 1) / 2;
            }
        }

        internal int SecondaryStageColumns
        {
            get
            {
                if (!Skin.SplitStages || stageIndex > 0 || availableColumns <= PrimaryStageColumns)
                    return -1;
                return availableColumns - PrimaryStageColumns;
            }
        }

        internal float ScaleFlipPosition(float position)
        {
            position = UpsideDown ? GameBase.WindowDefaultHeight - position : position;
            return Top + position * HeightRatio;
        }

        internal RectangleF ScaleFlipRectangleFromTB(float t, float b)
        {
            t = ScaleFlipPosition(t);
            b = ScaleFlipPosition(b);
            return RectangleF.FromLTRB(Left, Math.Min(t, b), Left + Width, Math.Max(t, b));
        }

        internal Vector2 SnapPosition(Vector2 org)
        {
            double col = Math.Floor(org.X * Columns.Count / 512);
            return new Vector2((int)((col >= Columns.Count ? col - 0.5 : 0.5 + col) * (512.0 / Columns.Count)), org.Y);
        }

        /// <summary>
        /// Converts gamefield mouse position to column number.
        /// </summary>
        /// <param name="allowSpecial">If true, 7+1K will be considered</param>
        internal int ColumnAt(Vector2 pos, bool allowSpecial = false)
        {
            if (allowSpecial && Columns.Count == 8)
            {
                float localWDivisor = 512.0f / 7;
                return Math.Min((int)Math.Floor(Math.Max(0, pos.X) / localWDivisor), 6) + 1;
            }
            else
            {
                float localWDivisor = 512.0f / Columns.Count;
                return Math.Min((int)Math.Floor(pos.X / localWDivisor), Columns.Count - 1);
            }
        }

        private static Mods?[] canonicalKeyMods;

        static StageMania()
        {
            InitialiseCanonicalKeyMods();
        }

        /// <summary>
        /// In osu! beatmaps, more sliders means and low OD means absolutely easy.
        /// </summary>
        private static int getOriginKey(Beatmap map)
        {
            //Percentage of the map that's a slider or spinner
            float percent = (float)(map.countSlider + map.countSpinner) / map.ObjectCount;
            if (percent < 0.2)
                return 7;
            if (percent < 0.3 || Math.Round(map.DifficultyCircleSize) >= 5)
                return Math.Round(map.DifficultyOverall) > 5 ? 7 : 6;
            if (percent > 0.6)
                return Math.Round(map.DifficultyOverall) > 4 ? 5 : 4;
            return Math.Max(4, Math.Min((int)Math.Round(map.DifficultyOverall) + 1, 7));
        }

        internal static int ColumnsWithoutMods(Beatmap map)
        {
            if (map.PlayMode == PlayModes.OsuMania)
                return (int)Math.Round(map.DifficultyCircleSize);
            return getOriginKey(map);
        }

        /// <summary>
        /// Notice: For mania specific beatmaps, use CS as column count.
        /// For osu! beatmaps, use AR in the same way but clamp it in range 4~7 
        /// 8K, 7+1K and above left for osu!mania specific maps
        /// </summary>
        internal static int ColumnsWithMods(Beatmap map)
        {
            if (map == null)
                return 4;

            return ColumnsWithMods(map, InputManager.ReplayMode ? InputManager.ReplayScore.enabledMods : ModManager.ModStatus);
        }

        internal static int ColumnsWithMods(Beatmap map, Mods allMods)
        {
            if (map == null)
                return 4;

            if (map.PlayMode == PlayModes.OsuMania)
                return (int)Math.Round(map.DifficultyCircleSize);

            Mods keyMod = allMods & Mods.KeyMod & ~Mods.KeyCoop;

            int keys;
            if (keyMod > 0 && (keyMod & (keyMod - 1)) == 0)
                keys = Convert.ToInt32(keyMod.ToString().Substring(3));
            else
            {
                Debug.Assert((keyMod & (keyMod - 1)) == 0);
                keys = getOriginKey(map);
            }

            if ((allMods & Mods.KeyCoop) > 0)
                return keys * 2;
            return keys;
        }

        private static void InitialiseCanonicalKeyMods()
        {
            canonicalKeyMods = new Mods?[StageMania.MAX_COLUMNS + 1];
            for (int i = 1; i <= StageMania.MAX_COLUMNS; i++)
            {
                Mods mod = Mods.None;
                int k = i;
                if (i > 9)
                {
                    if (i % 2 != 0)
                        continue;

                    k /= 2;
                    mod = Mods.KeyCoop;
                }
                canonicalKeyMods[i] = mod | (Mods)Enum.Parse(typeof(Mods), @"Key" + k);
            }
        }

        internal static Mods CanonicalKeyMods(Beatmap map, Mods allMods)
        {
            Mods canonMods = allMods & ~Mods.KeyMod;

            int columnsWithMods = StageMania.ColumnsWithMods(map, allMods);
            if (StageMania.ColumnsWithoutMods(map) == columnsWithMods)
                return canonMods;

            return canonMods | (Mods)canonicalKeyMods[columnsWithMods];
        }

        private void updateHiddenMask()
        {
            if (hasHiddenSprites)
            {
                if (hiddenMask == null || hiddenFade == null)
                {
                    Player.Instance.AddFlashlightFlag();
                    return;
                }

                if (!Player.Paused)
                {
                    if (GameBase.SixtyFramesPerSecondFrame)
                    {
                        if (EventManager.BreakMode)
                        {
                            if (hiddenMask.Alpha > 0)
                                hiddenMask.Alpha -= 0.016667f;  //60fps
                            else if (hiddenMask.Alpha < 0)
                                hiddenMask.Alpha = 0;

                            if (hiddenFade.Alpha > 0)
                                hiddenFade.Alpha -= 0.016667f;  //60fps
                            else if (hiddenFade.Alpha < 0)
                                hiddenFade.Alpha = 0;
                        }
                        else
                        {
                            if (hiddenMask.Alpha < 1)
                                hiddenMask.Alpha += 0.026667f;
                            else if (hiddenMask.Alpha > 1)
                                hiddenMask.Alpha = 1;  //Bug fix: when currentalpha > 1, there's a graphic bug

                            if (hiddenFade.Alpha < 1)
                                hiddenFade.Alpha += 0.026667f;
                            else if (hiddenFade.Alpha > 1)
                                hiddenFade.Alpha = 1;

                            float height = Math.Min(400, 160 + Player.Instance.Ruleset.ComboCounter.HitCombo / 2);
                            if (hiddenMask.VectorScale.Y > height)
                                hiddenMask.VectorScale.Y -= 2f;
                            else
                                hiddenMask.VectorScale.Y = height;

                            if (UpsideDown)
                            {
                                if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Hidden))
                                    hiddenFade.Position.Y = HitPosition + hiddenMask.VectorScale.Y / 1.6f;
                                else
                                    hiddenFade.Position.Y = GameBase.WindowDefaultHeight - hiddenMask.VectorScale.Y / 1.6f;
                            }
                            else
                            {
                                if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Hidden))
                                    hiddenFade.Position.Y = HitPosition - hiddenMask.VectorScale.Y / 1.6f;
                                else
                                    hiddenFade.Position.Y = hiddenMask.VectorScale.Y / 1.6f;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Vertically flips an origin if
        /// the stage is upside down.
        /// </summary>
        internal Origins FlipOrigin(Origins origin)
        {
            if (!UpsideDown)
                return origin;
            switch (origin)
            {
                case Origins.TopLeft:
                    return Origins.BottomLeft;
                case Origins.TopCentre:
                    return Origins.BottomCentre;
                case Origins.TopRight:
                    return Origins.BottomRight;
                case Origins.BottomLeft:
                    return Origins.TopLeft;
                case Origins.BottomCentre:
                    return Origins.TopCentre;
                case Origins.BottomRight:
                    return Origins.TopRight;
                default:
                    return origin;
            }
        }
    }
}
