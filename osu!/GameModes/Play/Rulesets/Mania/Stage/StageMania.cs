﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameplayElements.Beatmaps;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu_common;
using osu.GameModes.Play.Components;
using Microsoft.Xna.Framework.Input;
using osu.GameplayElements;
using System.Collections;
using osu.Helpers;
using osu.GameplayElements.Scoring;
using osu.Graphics.Primitives;

namespace osu.GameModes.Play.Rulesets.Mania
{
    internal partial class StageMania : IEnumerable<StageMania>, IDisposable
    {
        internal const int MAX_COLUMNS = 18;

        internal int ColumnOffset { get; private set; }
        private int availableColumns { get { return Skin.Columns - ColumnOffset; } }
        private int stageIndex;
        internal StageMania SecondaryStage { get; private set; }

        internal SkinMania Skin;

        internal SpriteManager SpriteManager;
        internal SpriteManager SpriteManagerBelow;
        internal SpriteManager SpriteManagerAbove;
        internal SpriteManager SpriteManagerNotes;
        internal HpBarMania HPBar;

        private pSprite stageRight;
        private pSprite stageHint;
        private pAnimation stageBottom;
        private pSprite judgementLine;
        private pSprite waveRight;
        private pAnimation hitScore;
        private pSprite hiddenMask;
        private pSprite hiddenFade;

        private Dictionary<string, pTexture[]> hitTextures = new Dictionary<string, pTexture[]>(6);

        internal List<ColumnMania> Columns = new List<ColumnMania>();
        internal float Width;
        private float widthScale;

        internal LayoutListMania LayoutList
        {
            get { return BindingManagerMania.GetLayoutList(Skin.Columns, Skin.SplitStages ? ManiaLayoutsFor.Split : ManiaLayoutsFor.Normal); }
        }

        private bool alwaysShowKeyWarnings = false;
        internal bool AlwaysShowKeyWarnings
        {
            get { return alwaysShowKeyWarnings; }
            set
            {
                alwaysShowKeyWarnings = value;
                Columns.ForEach(c => c.ResetWarning());
            }
        }

        internal SkinSource SkinSource { get { return BeatmapManager.Current.PlayMode == PlayModes.OsuMania ? SkinSource.All : (SkinSource.Skin | SkinSource.Osu); } }
        internal ManiaSpecialStyle SpecialStyle
        {
            get
            {
                if (!AllowSpecialStyle)
                    return ManiaSpecialStyle.None;
                if (stageIndex % 2 == 0)
                    return Skin.SpecialStyle;
                if (Skin.SpecialStyle == ManiaSpecialStyle.Left)
                    return ManiaSpecialStyle.Right;
                if (Skin.SpecialStyle == ManiaSpecialStyle.Right)
                    return ManiaSpecialStyle.Left;
                return ManiaSpecialStyle.None;
            }
        }

        private bool hasHiddenSprites { get { return Player.currentScore != null && ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Hidden | Mods.FadeIn); } }

        internal bool Minimal { get; private set; }

        internal bool Disposed { get; private set; }

        /// <summary>
        /// Creates a new stage with start position and columns defined by the skin.
        /// </summary>
        /// <param name="skin">The skin to use.</param>
        /// <param name="posY">The vertical position of the stage.</param>
        /// <param name="height">The height of the stage.</param>
        /// <param name="minimal">Should we draw sprites?</param>
        /// <param name="columnOffset">The skin column offset for this stage (used for coop)</param>
        internal StageMania(SkinMania skin, bool minimal = false, float posY = 0f, float height = 480f, float posX = float.NaN, float widthScale = 1f, int stageIndex = 0, int columnOffset = 0)
        {
            Minimal = minimal;
            Height = height;
            Skin = skin;
            this.stageIndex = stageIndex;
            this.ColumnOffset = columnOffset;

            if (!minimal)
            {
                SpriteManager = new SpriteManager(true);
                SpriteManagerBelow = new SpriteManager(true);
                SpriteManagerAbove = new SpriteManager(true);
                SpriteManagerNotes = new SpriteManager(true);

                stageHint = new pSprite(Skin.Load("StageHint", "mania-stage-hint", SkinSource), Fields.TopLeft, Origins.CentreLeft, Clocks.Audio, new Vector2(0, HitPosition), 0.62f, true, Color.White);
                stageHint.Alpha = 0.9f;
                stageHint.VectorScale = new Vector2(0, 0.9f);
                stageHint.Scale = 1.6026f;
                stageHint.FlipVertical = UpsideDown;
                SpriteManager.Add(stageHint);

                pSprite p = new pSprite(Skin.Load("StageLeft", "mania-stage-left", SkinSource), Fields.TopLeft, Origins.TopRight, Clocks.Audio, new Vector2(0.05f, Top), 0.935f, true, Color.White);
                p.VectorScale = new Vector2(HeightRatio, Height / (float)p.Height * 1.6f);
                SpriteManager.Add(p);

                stageRight = new pSprite(Skin.Load("StageRight", "mania-stage-right", SkinSource), Fields.TopLeft, Origins.TopLeft, Clocks.Audio, new Vector2(0.05f, Top), 0.935f, true, Color.White);
                stageRight.VectorScale = new Vector2(HeightRatio, Height / (float)stageRight.Height * 1.6f);
                SpriteManager.Add(stageRight);

                pTexture[] pt = Skin.LoadAll("StageBottom", "mania-stage-bottom", SkinSource);
                if (pt != null && pt.Length > 0)
                {
                    stageBottom = new pAnimation(pt, Fields.TopLeft, FlipOrigin(Origins.BottomCentre), Clocks.Audio, new Vector2(0, ActualBottom), 0.94f, true, Color.White);
                    stageBottom.FrameDelay = 1000 / 60f;
                    stageBottom.Scale = 1.6f;
                    SpriteManagerAbove.Add(stageBottom);
                }

                if (Skin.JudgementLine)
                {
                    judgementLine = new pSprite(GameBase.WhitePixel, Fields.TopLeft, FlipOrigin(Origins.TopLeft), Clocks.Audio, new Vector2(0, HitPosition), 0.62f, true, Skin.GetColour("JudgementLine", Color.White));
                    judgementLine.Alpha = 0.9f;
                    judgementLine.VectorScale = new Vector2(0, 0.7f);
                    judgementLine.Scale = 1.6026f;
                    SpriteManager.Add(judgementLine);
                }

                p = new pSprite(SkinManager.Load("mania-stage-light", SkinSource.Osu), Fields.TopLeft, Origins.BottomRight, Clocks.Audio, new Vector2(0, Top), 0.5f, true, Color.TransparentWhite);
                p.Rotation = -(float)(Math.PI / 2);
                p.VectorScale = new Vector2(Height / (float)p.Width, 0.27f) * 1.6f;
                SpriteManagerBelow.Add(p);

                waveRight = new pSprite(SkinManager.Load("mania-stage-light", SkinSource.Osu), Fields.TopLeft, Origins.BottomLeft, Clocks.Audio, new Vector2(0, Top), 0.5f, true, Color.TransparentWhite);
                waveRight.Rotation = (float)(Math.PI / 2);
                waveRight.VectorScale = new Vector2(Height / (float)waveRight.Width, 0.27f) * 1.6f;
                SpriteManagerBelow.Add(waveRight);

                hitScore = new pAnimation(null, Fields.TopLeft, Origins.Centre, Clocks.Audio, new Vector2(0, ScaleFlipPosition(Skin.ScorePosition)), 0.998f, true, Color.White);
                hitScore.FrameDelay = 1000 / 20f;
                SpriteManagerAbove.Add(hitScore);

                foreach (string hitType in new[] { "0", "50", "100", "200", "300", "300g" })
                    hitTextures.Add(hitType, Skin.LoadAll("Hit" + hitType, "mania-hit" + hitType, SkinSource));

                if (hasHiddenSprites)
                {
                    bool fromKeyLayer = ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Hidden);

                    pTexture hiddenSprite = SkinManager.Load(@"mania-stage-hidden", SkinSource.Osu);

                    if (hiddenSprite == null)
                        GameBase.ChangeMode(OsuModes.Menu);

                    if (UpsideDown)
                    {
                        hiddenMask = new pSprite(GameBase.WhitePixel, Fields.TopLeft, fromKeyLayer ? Origins.TopLeft : Origins.BottomLeft, Clocks.Audio,
                                                 new Vector2(0, fromKeyLayer ? HitPosition : 480), 0.81f, true, Color.Black);
                        hiddenFade = new pSprite(hiddenSprite, Fields.TopLeft, fromKeyLayer ? Origins.TopLeft : Origins.BottomLeft, Clocks.Audio,
                                                 new Vector2(0, fromKeyLayer ? (HitPosition + 160 / 1.6f) : (320 / 1.6f)), 0.81f, true, Color.TransparentWhite);
                    }
                    else
                    {
                        hiddenMask = new pSprite(GameBase.WhitePixel, Fields.TopLeft, fromKeyLayer ? Origins.BottomLeft : Origins.TopLeft, Clocks.Audio,
                                                 new Vector2(0, fromKeyLayer ? HitPosition : 0), 0.81f, true, Color.Black);
                        hiddenFade = new pSprite(hiddenSprite, Fields.TopLeft, fromKeyLayer ? Origins.BottomLeft : Origins.TopLeft, Clocks.Audio,
                                                 new Vector2(0, fromKeyLayer ? (HitPosition - 160 / 1.6f) : (160 / 1.6f)), 0.81f, true, Color.TransparentWhite);
                    }

                    SpriteManagerAbove.Add(hiddenMask);
                    SpriteManagerAbove.Add(hiddenFade);
                }
            }

            //We must set these later as they position the spritemanagers
            Left = float.IsNaN(posX) ? Skin.ColumnStart : posX;
            Top = posY;

            //Scale the width of columns, column spacing, and stage separation to fit everything on the screen
            //NOTE: This logic assumes there are a maximim of two stages
            if (!minimal && columnOffset == 0)
            {
                //Flip left and right offsets if they overlap
                float left = Math.Min(Left, GameBase.WindowWidthScaled - Skin.ColumnRight);
                float right = Math.Min(Skin.ColumnRight, GameBase.WindowWidthScaled - Left);
                Left = left;

                //resizeWidth is the width of everything we're scaling that is to the right of "Left"
                float resizeWidth = 0;
                for (int i = 0; i < Skin.Columns; i++)
                    resizeWidth += Skin.ColumnWidth[i] + (i > 0 ? Skin.ColumnSpacing[i - 1] : 0);
                resizeWidth += Skin.SplitStages && SecondaryStageColumns != -1 ? Skin.StageSeparation : 0;

                float totalWidth = Left + resizeWidth + right;
                float screenOverflow = Math.Max(0, totalWidth - GameBase.WindowWidthScaled);

                widthScale = (resizeWidth - screenOverflow) / resizeWidth;
            }
            this.widthScale = widthScale;

            for (int i = columnOffset; i < columnOffset + PrimaryStageColumns; i++)
                Columns.Add(new ColumnMania(this, i, widthScale));

            if (!minimal)
            {
                //Set the visible area of the notes now that we know the width of the columns
                SpriteManagerNotes.SetVisibleArea(new RectangleF(Left, Top, Width, Height));
                SpriteManagerNotes.ForwardPlayOptimisations = true;
            }

            if (!minimal)
            {
                stageHint.VectorScale.X = Width / stageHint.Width;
                stageRight.Position.X += Width;
                if (stageBottom != null)
                    stageBottom.Position.X += Width / 2f;
                if (judgementLine != null)
                    judgementLine.VectorScale.X = Width;
                waveRight.Position.X += Width;
                hitScore.Position.X += Width / 2f;

                //Scale is used for sprite transformations, so we use vectorscale instead
                if (SkinManager.Current.Version >= 2.4 || HeightRatio < 1f)
                    hitScore.VectorScale = Vector2.One * MinimumRatio;

                //Hidden masks
                if (hasHiddenSprites)
                {
                    bool fromKeyLayer = ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Hidden);
                    hiddenMask.VectorScale = new Vector2(Width * 1.6f, 160);
                    hiddenMask.Alpha = 0;
                    hiddenFade.VectorScale = new Vector2(Width * 1.6f, 180 / 250f);
                    if ((!fromKeyLayer && !UpsideDown) || (fromKeyLayer && UpsideDown))
                        hiddenFade.FlipVertical = true;
                    hiddenFade.Alpha = 0;
                }

                if (SecondaryStageColumns == -1)
                    HPBar = new HpBarMania(this);
            }

            if (SecondaryStageColumns != -1)
            {
                posX = Left + Width + Skin.StageSeparation * widthScale;
                SecondaryStage = new StageMania(Skin, minimal, posY, height, posX, widthScale, stageIndex + 1, columnOffset + PrimaryStageColumns);
                Columns.AddRange(SecondaryStage.Columns);
                HPBar = SecondaryStage.HPBar;
            }

            bindColumnKeys();
        }

        /// <summary>
        /// Creates a default minimal stage with columns defined
        /// by the beatmap.
        /// </summary>
        /// <param name="hom">The hitobjectmanager.</param>
        internal StageMania(HitObjectManager hom)
            : this(new SkinMania() { Columns = ColumnsWithMods(hom.Beatmap, hom.ActiveMods) }, true)
        { }

        private void bindColumnKeys()
        {
            int bindingIndex = Math.Min(stageIndex, 1);
            int columns = PrimaryStageColumns;
            bool useSpecialColumn = AllowSpecialStyle && SpecialStyle != ManiaSpecialStyle.None;

            //Use a layout if we have one instead of the default
            LayoutMania layout = LayoutList.Layout;
            if (layout != null)
            {
                foreach (ColumnMania col in Columns)
                    col.Key = layout[col];
                if (useSpecialColumn)
                    Columns[SpecialColumn].AlternateKey = layout[bindingIndex, ManiaLayoutAccess.AlternateKeys];
                return;
            }

            int specialOffset = 0;
            if (useSpecialColumn)
            {
                columns--;
                specialOffset = SpecialColumn == 0 ? 1 : 0;
                Columns[SpecialColumn].Key = BindingManager.ManiaDefaultSpecialKeys[bindingIndex];
                Columns[SpecialColumn].AlternateKey = BindingManager.ManiaDefaultAlternateSpecialKeys[bindingIndex];
            }

            //The leftmost key to start from
            int key = Math.Max(0, (BindingManager.ManiaMiddleKeys[bindingIndex]) - columns / 2);

            //If we should skip the binding for the middle key
            bool skipMiddle = columns % 2 == 0 && columns < 10;

            for (int i = 0; i < columns; i++)
            {
                if (skipMiddle && (key == BindingManager.ManiaMiddleKeys[0] || key == BindingManager.ManiaMiddleKeys[1]))
                    key++;
                Columns[i + specialOffset].Key = BindingManager.ManiaDefaultKeys[key++];
            }
        }

        internal void RebindColumnKeys()
        {
            foreach (StageMania stage in this)
                stage.bindColumnKeys();
        }

        internal void CopyBindingToLayout(LayoutMania layout)
        {
            if (layout == null) return;

            foreach (StageMania stage in this)
            {
                foreach (ColumnMania col in stage.Columns)
                    layout[col] = col.Key;

                if (stage.AllowSpecialStyle && stage.SpecialStyle != ManiaSpecialStyle.None)
                    layout[Math.Min(stage.stageIndex, 1), ManiaLayoutAccess.AlternateKeys] = stage.Columns[stage.SpecialColumn].AlternateKey;
            }
        }

        private pTexture[] hitScoreTextures(IncreaseScoreType hitType)
        {
            switch (hitType)
            {
                case IncreaseScoreType.ManiaHit300g:
                    return hitTextures["300g"];
                case IncreaseScoreType.ManiaHit300:
                    return hitTextures["300"];
                case IncreaseScoreType.ManiaHit200:
                    return hitTextures["200"];
                case IncreaseScoreType.ManiaHit100:
                    return hitTextures["100"];
                case IncreaseScoreType.ManiaHit50:
                    return hitTextures["50"];
                case IncreaseScoreType.MissMania:
                default:
                    return hitTextures["0"];
            }
        }

        internal void TriggerScoreIncrease(IncreaseScoreType hitType)
        {
            if (hitType != IncreaseScoreType.MissManiaHoldBreak)
            {
                hitScore.TextureArray = hitScoreTextures(hitType);
                hitScore.Transformations.Clear();
                hitScore.Rotation = 0;

                float scale = 1;

                hitScore.Transformations.Add(new Transformation(TransformationType.Fade, hitScore.Alpha, 1, AudioEngine.Time, AudioEngine.Time + 20, EasingTypes.Out));

                if (hitType == IncreaseScoreType.MissMania)
                {
                    hitScore.Transformations.Add(new Transformation(TransformationType.Scale, scale * 1.2f, scale, AudioEngine.Time, AudioEngine.Time + 100, EasingTypes.Out));
                    hitScore.Transformations.Add(new Transformation(TransformationType.Rotation, 0, ((float)GameBase.random.NextDouble() - 0.5f) * 0.2f, AudioEngine.Time, AudioEngine.Time + 100, EasingTypes.Out));
                }
                else
                {
                    hitScore.Transformations.Add(new Transformation(TransformationType.Scale, scale * 0.8f, scale, AudioEngine.Time + 0, AudioEngine.Time + 40, EasingTypes.None));
                    hitScore.Transformations.Add(new Transformation(TransformationType.Scale, scale, scale * 0.70f, AudioEngine.Time + 0, AudioEngine.Time + 80, EasingTypes.None));
                    hitScore.Transformations.Add(new Transformation(TransformationType.Scale, scale * 0.70f, scale * 0.4f, AudioEngine.Time + 180, AudioEngine.Time + 220, EasingTypes.In));
                }

                hitScore.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, AudioEngine.Time + 180, AudioEngine.Time + 220, EasingTypes.In));
            }
        }

        internal void DrawBelow()
        {
            SpriteManagerBelow.Draw();
            SpriteManager.Draw();
            if (SecondaryStage != null) SecondaryStage.DrawBelow();
        }

        internal void Draw()
        {
            SpriteManagerNotes.Draw();
            updateHiddenMask();
            SpriteManagerAbove.Draw();
            if (SecondaryStage != null) SecondaryStage.Draw();
        }

        internal void UpdateInput()
        {
            if (InputManager.ReplayMode)
                return;

            Columns.ForEach(c => c.UpdateInput());
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (!Minimal)
                {
                    SpriteManager.Dispose();
                    SpriteManagerBelow.Dispose();
                    SpriteManagerAbove.Dispose();
                }

                if (SecondaryStage != null)
                    SecondaryStage.Dispose();
                Disposed = true;
            }
        }

        public IEnumerator<StageMania> GetEnumerator()
        {
            StageMania stage = this;
            while (stage != null)
            {
                yield return stage;
                stage = stage.SecondaryStage;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}