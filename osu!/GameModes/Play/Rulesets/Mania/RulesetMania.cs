﻿using System;
using Microsoft.Xna.Framework;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play.Components;
using osu.GameplayElements;
using osu.GameplayElements.Scoring;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu_common;
using osu.GameplayElements.HitObjects;
using osu.Input.Handlers;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.Helpers;
using osu_common.Helpers;
using osudata;
using osu.Graphics.Notifications;
using osu_common.Bancho.Objects;
using osu.Online;
using osu_common.Bancho.Requests;
using osu.Graphics;
using System.Collections.Generic;
using osu.GameplayElements.HitObjects.Mania;
using System.Diagnostics;

namespace osu.GameModes.Play.Rulesets.Mania
{

    internal partial class RulesetMania : Ruleset
    {
        public RulesetMania(Player player)
            : base(player)
        { }

        internal override void CacheFutureSprites()
        {
            base.CacheFutureSprites();
        }

        public override void Dispose()
        {
            base.Dispose();
        }

        internal override bool AllowMouseHide
        {
            get
            {
                if (Player.Playing) return !InputManager.TouchDevice;

                return base.AllowMouseHide;
            }
        }

        //notice: notes' drawDepth is 0.8F
        internal override void InitializeSprites()
        {
            base.InitializeSprites();
            HpBar = hitObjectManager.ManiaStage.HPBar;

            if (!Configuration.ConfigManager.GetTipStatus(Tips.ManiaSpeedChange))
            {
                NotificationManager.ShowMessageMassive(string.Format(LocalisationManager.GetString(OsuString.Tips_ManiaSpeedChange),
                    BindingManager.NiceName(Bindings.IncreaseSpeed), BindingManager.NiceName(Bindings.DecreaseSpeed)), 2500);
                Configuration.ConfigManager.SetTipStatus(Tips.ManiaSpeedChange, true);
            }
        }

        internal override void UpdateGeneral()
        {
            if (!IsInitialized || Player.Paused)
                return;

            /*  if (Player.KiaiActive && AudioEngine.beatSync && AudioEngine.Time - AudioEngine.beatOffset > 0)
              {
                  int current = (int)((AudioEngine.Time - AudioEngine.beatOffset) % AudioEngine.beatLength);
                  int length = (int)AudioEngine.beatLength;
                  waveLeft.CurrentAlpha = SpriteManager.easeOutVal(current, 0.8F, 0.2F, length);
                  waveRight.CurrentAlpha = waveLeft.CurrentAlpha;
              }
              else
              {
                  waveLeft.CurrentAlpha = 0;
                  waveRight.CurrentAlpha = 0;
              }*/
        }

     /*   protected override void InitializeScoreMeter()
        {
            if (ConfigManager.sScoreMeter == ScoreMeterType.Colour)
                ScoreMeter = new ScoreMeterColourMania(GameBase.Instance);
            else if (ConfigManager.sScoreMeter == ScoreMeterType.Error)
                ScoreMeter = new ScoreMeterErrorMania(GameBase.Instance);
        }*/

        internal override void InitializeCalculations()
        {
            hitObjectManager.ManiaStage.HPBar.HpDropRate = CalculateHpDropRate();

            if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.HalfTime))
                hitObjectManager.ManiaStage.HPBar.HpDropRate *= 0.75;

            base.InitializeCalculations();
        }

        internal override void InitializeModSettings()
        {
            HitObjectManagerMania manager = (HitObjectManagerMania)hitObjectManager;

            //Don't consider score increase mods to not go over mania max
            double multiplier = ModManager.ScoreMultiplier(Player.currentScore.enabledMods & ~(Mods.ScoreIncreaseMods));
            ScoreMultiplier = (double)JudgementMania.ScoreMax / manager.CountTotal * multiplier;

            if (!ModManager.CheckActive(Player.currentScore.enabledMods, Mods.SuddenDeath))
            {
                if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Easy))
                    Recoveries = 2;
            }

            if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Flashlight))
            {
                spriteManagerFlashlight = new SpriteManager(true);

                s_Flashlight = new pSprite(null, Fields.Storyboard, Origins.CentreLeft, Clocks.Game, new Vector2(0, 220), 1, true, Color.White);
                s_Flashlight.Scale = 1.4F;

                spriteManagerFlashlight.Add(s_Flashlight);
                loadFlashlight();
            }
        }

        protected override void loadFlashlight()
        {
            if (s_Flashlight == null) return;

            newFlashlightOverlay = true;
            byte[] bytes = osu_gameplay.ResourcesStore.ResourceManager.GetObject("flashlightv2_mania") as byte[];

            if (bytes == null)
            {
                newFlashlightOverlay = false;
                bytes = ResourcesStore.ResourceManager.GetObject("flashlight2") as byte[];
                //use old flashlight for now
            }

            byte[] hash = CryptoHelper.GetMd5ByteArray(bytes);

            byte[] expected = newFlashlightOverlay ?
                new byte[] { 238, 208, 143, 44, 243, 39, 228, 31, 59, 30, 20, 241, 91, 126, 248, 201 } :
                new byte[] { 170, 142, 53, 203, 205, 131, 29, 55, 53, 91, 207, 196, 60, 73, 12, 190 };

            bool notmatching = false;

            for (int i = 0; i < 16; i++)
            {
                if (hash[i] != expected[i])
                {
                    notmatching = true;
                    break;
                }
            }

            if (notmatching)
            {
                GameBase.Scheduler.Add(delegate
                {
                    Player.flag |= BadFlags.FlashlightChecksumIncorrect;
                    Player.wasAllowingSubmission = false;
                }, true);
            }

            s_Flashlight.Texture = pTexture.FromBytes(bytes);
        }

        internal override void DrawFlashlight()
        {
            if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Flashlight))
            {
                if (s_Flashlight.Texture == null ||
                    (GameBase.OGL && !s_Flashlight.Texture.TextureGl.Loaded) ||
                    (GameBase.D3D && s_Flashlight.Texture.TextureXna == null))
                {
                    if (CurrentScore.AllowSubmission)
                    {
                        CurrentScore.AllowSubmission = false;
                        NotificationManager.ShowMessageMassive("Couldn't draw Flashlight texture - disabling ranking.", 2000);

                        ConfigManager.PurgeHashCache();
                        GameBase.ChangeMode(OsuModes.Update);
                    }

                    return;
                }
                if (spriteManagerFlashlight.SpriteList.Count == 0)
                {
                    player.AddFlashlightFlag();
                }

                spriteManagerFlashlight.Draw();
            }
        }

        internal override void UpdateInput()
        {
            if (!IsInitialized || Player.Paused)
                return;
            hitObjectManager.ManiaStage.UpdateInput();
        }

        internal override void UpdateScoring()
        {
        }

        internal override Vector2 ScoreboardPosition
        {
            get { return new Vector2(0, 135); }
        }

        internal override bool ScoreboardOnRight
        {
            get { return false; }
        }

        internal override void OnCompletion()
        {
            base.OnCompletion();
            HitObjectManagerMania manager = (HitObjectManagerMania)hitObjectManager;
            player.HaxCheck();
            CurrentScore.totalScore = (int)Math.Round(CurrentScore.totalScoreD);
            if (CurrentScore.totalScore == 999999)
                CurrentScore.totalScore = 1000000;//some round issue.
            if ((CurrentScore.countMiss + CurrentScore.count50 + CurrentScore.count100) == 0 && ((float)CurrentScore.countKatu / CurrentScore.totalHits) < 0.1)
                CurrentScore.perfect = true;
            else
                CurrentScore.perfect = false;

            //remove invalid mods
            CurrentScore.enabledMods = StageMania.CanonicalKeyMods(BeatmapManager.Current, CurrentScore.enabledMods);

            if (manager.PressHaxCount > manager.CountNormal * 2 / 3) // 67% normal notes
                Player.flag |= BadFlags.FastPress;

            haxCheckCount++;
            player.UpdateChecksum();

            Debug.Print("Fast press: {0}", manager.PressHaxCount);
        }

        internal override void DrawBefore()
        {
            if (!IsInitialized)
                return;

            hitObjectManager.ManiaStage.DrawBelow();
        }

        internal override void Draw()
        {
            if (!IsInitialized)
                return;

            hitObjectManager.ManiaStage.Draw();

            ScoreDisplay.Draw();
            DrawFlashlight();
            if (ScoreMeter != null)
                ScoreMeter.Draw();
        }

        internal override void SetAlpha(float alpha)
        {
            hitObjectManager.ManiaStage.Alpha = alpha;
            base.SetAlpha(alpha);
        }

        internal override bool IsPassing
        {
            get { return hitObjectManager.ManiaStage.HPBar.CurrentHp >= HP_BAR_MAXIMUM * 0.8; }
        }

        internal override bool AllowSubmission
        {
            get { return true; }
        }

        internal override bool AllNotesHit
        {
            get
            {
                return CurrentScore.totalHits == hitObjectManager.hitObjectsCount;
            }
        }

        internal override bool IsImportantFrame()
        {
            /*    if (EventManager.BreakMode)
                    return false;*/

            StageMania stage = hitObjectManager.ManiaStage;
            for (int i = 0; i < stage.Columns.Count; i++)
            {
                if (stage.Columns[i].KeyState != stage.Columns[i].KeyStateLast)
                    return true;
            }
            return false;
        }

        internal override HitObjectManager CreateHitObjectManager()
        {
            return new HitObjectManagerMania();
        }

        /*   internal override bool AllowMouseHide
           {
               get { return !InputManager.ReplayMode; }
           }*/

        internal override bool AllowInstantUnpause
        {
            get { return true; }
        }

        internal override void InitializeHpBar()
        { }

        internal override void InitializeComboCounter()
        {
            ComboCounter = new ComboCounterMania(spriteManagerWidescreen, hitObjectManager.ManiaStage);
        }

        internal override bool IsMilestoneCombo
        {
            get
            {
                return (ComboCounter.HitCombo > 99 && ComboCounter.HitCombo % 100 == 0);
            }
        }

        internal override bool AllowWarningArrows
        {
            get
            {
                return false;
            }
        }
    }
}