﻿using osu.Audio;
using osu.Configuration;
using osu.GameModes.Edit;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Helpers;
using osu_common;
using osu_common.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace osu.GameModes.Play.Rulesets.Mania
{
    internal class SpeedMania
    {
        internal const int SPEED_MIN = 1;
        internal const int SPEED_MAX = 40;
        internal const int SPEED_DEFAULT = 12;
        internal const double SPEED_FACTOR = 100d;

        private static bool isSpeedOwned;
        internal static int Speed = 12;
        internal static double RelativeSpeed;
        internal static double EffectiveBPM;
        private static double primaryBPM;
        private static Beatmap currentBeatmap;

        // returns whether the speed was set
        internal static bool SetSpeed(int value, ManiaSpeedSet set)
        {
            if ((set & ManiaSpeedSet.Reset) > 0)
            {
                if (BeatmapManager.Current == null)
                {
                    value = ConfigManager.sManiaSpeed.Value;
                    if (primaryBPM <= 0)
                        primaryBPM = SPEED_FACTOR; // sane default
                }
                else
                {
                    value = ConfigManager.sUsePerBeatmapManiaSpeed && BeatmapManager.Current.ManiaSpeed > 0
                        ? BeatmapManager.Current.ManiaSpeed : ConfigManager.sManiaSpeed.Value;
                    if (BeatmapManager.Current != currentBeatmap)
                    {
                        // recalculate these only if the Beatmap changed
                        currentBeatmap = BeatmapManager.Current;
                        primaryBPM = currentBeatmap.BeatsPerMinuteRange.Z;
                    }
                }
                isSpeedOwned = false;
            }
            else if ((set & ManiaSpeedSet.Owned) == 0 && isSpeedOwned)
            {
                // Unowned sets only happen when the speed is not already Owned
                return false;
            }

            value = OsuMathHelper.Clamp(value, SPEED_MIN, SPEED_MAX);

            if ((set & ManiaSpeedSet.Owned) > 0)
            {
                isSpeedOwned = true;

                ConfigManager.sManiaSpeed.Value = value;
                if (BeatmapManager.Current != null && ConfigManager.sUsePerBeatmapManiaSpeed)
                    BeatmapManager.Current.ManiaSpeed = value;
            }

            EffectiveBPM = primaryBPM *
                (ModManager.CheckActive(Mods.DoubleTime) ? 1.5
                : ModManager.CheckActive(Mods.HalfTime) ? 0.75
                : 1.0);

            double oldRelativeSpeed = RelativeSpeed;

            if ((set & ManiaSpeedSet.Legacy) == 0)
            {
                Speed = value;
                RelativeSpeed = Speed * (ConfigManager.sManiaSpeedBPMScale ? 1 : SPEED_FACTOR / Math.Max(EffectiveBPM, 1d));
            }
            else
            {
                // Legacy speed setting from old replays or spectating old clients
                RelativeSpeed = value;
                Speed = OsuMathHelper.Clamp((int)Math.Round(RelativeSpeed * EffectiveBPM / SPEED_FACTOR), SPEED_MIN, SPEED_MAX);
            }

            return RelativeSpeed != oldRelativeSpeed;
        }

        internal static void AdjustSpeed(int delta)
        {
            int speed = Speed + delta;
            SetSpeed(speed, ManiaSpeedSet.Owned);
            NotificationManager.ShowMessageMassive(string.Format(LocalisationManager.GetString(OsuString.ConfigMania_OsuManiaSpeedSet), GetSpeedString()), 1750);
        }

        internal static string GetSpeedString()
        {
            return string.Format(@"{0:0} ({1})", Speed, ConfigManager.sManiaSpeedBPMScale ? LocalisationManager.GetString(OsuString.ConfigMania_BPMscale) : LocalisationManager.GetString(OsuString.ConfigMania_Fixed));
        }

        internal static double TimeFromOffset(int offset)
        {
            HitObjectManager hom = null;
            if (GameBase.Mode == OsuModes.Play)
                hom = Player.Instance.hitObjectManager;
            else if (GameBase.Mode == OsuModes.Edit)
                hom = Editor.Instance.hitObjectManager;
            if (hom == null)
                return AudioEngine.beatLengthAt(offset, BeatmapManager.Current.PlayMode == PlayModes.OsuMania);
            return hom.Beatmap.beatLengthAt(offset, hom.Beatmap.PlayMode == PlayModes.OsuMania);
        }

        //how long should be at the giving time interval
        internal static double DistanceAt(int offset, int intv)
        {
            return 21d * RelativeSpeed * intv / TimeFromOffset(offset);
        }

        internal static double DistanceAt(double beatLen, double intv)
        {
            return 21d * RelativeSpeed * intv / beatLen;
        }

        internal static double TimeAt(int offset, int distance)
        {
            return distance * TimeFromOffset(offset) / 21d / RelativeSpeed;
        }

        internal static double TimeAt(double beatlen, double distance)
        {
            return distance * beatlen / 21d / RelativeSpeed;
        }
    }

    [Flags]
    internal enum ManiaSpeedSet
    {
        Unowned = 0,
        Owned = 1,
        Reset = 2,
        Legacy = 4,
    }
}
