﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Fruits;
using osu.Graphics.Sprites;
using osu.Input;
using osu_common.Bancho.Objects;
using osu.Helpers;

namespace osu.GameModes.Play.Rulesets.Fruits
{
    internal partial class RulesetFruits
    {
        internal override void CreateAutoplayReplay()
        {
            InputManager.ReplayScore.replay = new List<bReplayFrame>();
            List<bReplayFrame> replay = InputManager.ReplayScore.replay;

            replay.Add(new bReplayFrame(-100000, 256, 490, pButtonState.None));
            
            //replay.Add(new bReplayFrame(hitObjectManager.hitObjects[0].StartTime - 1000, 256, 490, pButtonState.None));

            float movementSpeed = baseMovementSpeed * 0.5f;
            float dashSpeed = baseMovementSpeed;

            float lastPosition = 256;
            int lastTime = 0;

            for (int i = 0; i < hitObjectManager.hitObjectsCount; i++)
            {
                HitObject h = hitObjectManager.hitObjects[i];

                float positionChange = Math.Abs(lastPosition - h.Position.X);
                int timeAvailable = h.StartTime - lastTime;

                //So we can either make it there without a dash or not.
                float speedRequired = positionChange/timeAvailable;

                bool dashRequired = speedRequired > movementSpeed && h.StartTime != 0;
                bool hyperDash = speedRequired > dashSpeed && h.StartTime != 0;

                if (lastPosition - catcherWidthHalf + catchMargin < h.Position.X &&
                        lastPosition + catcherWidthHalf - catchMargin > h.Position.X)
                {
                    //we are already in the correct range.
                    lastTime = h.EndTime;
                    replay.Add(new bReplayFrame(h.StartTime, lastPosition, 490, pButtonState.None));
                    continue;
                }
                
                if (h is HitCircleFruitsSpin)
                {
                    //special spinner handling...
                    replay.Add(new bReplayFrame(h.StartTime, h.Position.X, 490, pButtonState.Left1));
                }
                else if (hyperDash)
                {
                    replay.Add(new bReplayFrame(h.StartTime - timeAvailable + 1, lastPosition, 490, pButtonState.Left1));

                    replay.Add(new bReplayFrame(h.StartTime, h.Position.X, 490, pButtonState.None));
                }
                else if (dashRequired)
                {
                    //we do a movement in two parts - the dash part then the normal part...
                    int timeAtNormalSpeed = (int)(positionChange / movementSpeed);

                    int timeWeNeedToSave = timeAtNormalSpeed - timeAvailable;

                    int timeAtDashSpeed = timeWeNeedToSave / 2;
                    timeAtNormalSpeed -= timeAtDashSpeed;

                    float midPosition = OsuMathHelper.Lerp(lastPosition, h.Position.X, (float) timeAtDashSpeed/timeAvailable);

                    //dash movement
                    replay.Add(new bReplayFrame(h.StartTime - timeAvailable + 1, lastPosition, 490, pButtonState.Left1));

                    replay.Add(new bReplayFrame(h.StartTime - timeAvailable + timeAtDashSpeed, midPosition, 490, pButtonState.None));

                    replay.Add(new bReplayFrame(h.StartTime, h.Position.X, 490, pButtonState.None));

                }
                else
                {
                    int timeBefore = (int)(positionChange/movementSpeed);

                    replay.Add(new bReplayFrame(h.StartTime - timeBefore, lastPosition, 490, pButtonState.Right1));
                    replay.Add(new bReplayFrame(h.StartTime, h.Position.X, 490, pButtonState.None));
                }


                lastTime = h.EndTime;
                lastPosition = h.Position.X;

                /*replay.Add(new bReplayFrame(h.StartTime, h.Position.X, 490, pButtonState.Right1));
                if (i < hitObjectManager.hitObjectsCount - 1 && i != 0 && hitObjectManager.hitObjects[i + 1].StartTime - h.StartTime > 2000)
                    replay.Add(new bReplayFrame(hitObjectManager.hitObjects[i + 1].StartTime - 1000, h.Position.X, 490,pButtonState.None));    */
            }


            Player.currentScore.replay = InputManager.ReplayScore.replay;
            Player.currentScore.playerName = "salad!";
        }
    }
}