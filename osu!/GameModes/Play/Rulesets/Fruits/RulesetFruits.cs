﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play.Components;
using osu.GameplayElements;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Fruits;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Notifications;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;

namespace osu.GameModes.Play.Rulesets.Fruits
{
    internal partial class RulesetFruits : Ruleset
    {
        private readonly List<HitCircleFruits> caughtFruits = new List<HitCircleFruits>();
        private readonly List<pSprite> caughtSprites = new List<pSprite>();
        private pAnimation catcher1;
        private float catcherWidth;
        internal float catcherWidthHalf;
        private float catchMargin;
        private ComboCounterFruits comboCounterFruits;


        internal RulesetFruits(Player player)
            : base(player)
        {
        }

        internal override Vector2 ScoreboardPosition
        {
            get
            {
                return new Vector2(0, 100);
            }
        }

        internal override bool AllowMetadataDisplay
        {
            get { return true; }
        }

        internal override Vector2 MousePosition
        {
            get 
            {
                if (catcher1 != null)
                    return new Vector2(catcher1.Position.X, 9999);

                return base.MousePosition;
            }
        }

        internal override bool AllowScoreboardDisplay
        {
            get { return true; }
        }

        internal override bool AllowSubmission
        {
            get { return true; }
        }

        internal override bool AllowInstantUnpause
        {
            get { return true; }
        }

        internal override bool AllowHideSpinnerTop
        {
            get
            {
                return false;
            }
        }

        internal override bool AllowMouseHide
        {
            get
            {
                if (Player.Playing) return true;

                return base.AllowMouseHide || InputManager.ReplayMode;
            }
        }

        internal override bool AllNotesHit
        {
            get
            {
                return CurrentScore.totalHits == hitObjectManager.hitObjects.FindAll(h => !(h is HitCircleFruitsSpin)).Count;
            }
        }

        internal override bool UsesMouse
        {
            get
            {
                if (Player.Relaxing) return true;

                return base.UsesMouse;
            }
        }

        internal override void InitializeCalculations()
        {
            TotalHitsPossible = hitObjectManager.hitObjectsCount;
            baseMovementSpeed = ModManager.CheckActive(CurrentScore.enabledMods, Mods.HalfTime) ? 0.75f :
                ModManager.CheckActive(CurrentScore.enabledMods, Mods.DoubleTime) ? 1.5f : 1;


            HpBar.HpDropRate = CalculateHpDropRate();

            if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.HalfTime))
                HpBar.HpDropRate *= 0.75;

            InitializeHyperDash();

            base.InitializeCalculations();
        }

        protected override void InitializeScoreMeter()
        {
            if (ConfigManager.sScoreMeter != ScoreMeterType.None)
                ScoreMeter = new ScoreMeterColour(GameBase.Instance, hitObjectManager);
        }

        private void InitializeHyperDash()
        {
            HitObjectManagerFruits hom = hitObjectManager as HitObjectManagerFruits;
            hom.InitializeHyperDash(catcherWidth);
        }

        internal override void UpdateHp()
        {
            if (player.DrainTime)
                HpBar.ReduceCurrentHp(HpBar.HpDropRate * (AudioEngine.Time - AudioEngine.TimeLastFrame));

            base.UpdateHp();
        }

        internal override void OnIncreaseScoreHit(IncreaseScoreType ist, double hpIncrease, HitObject h)
        {
            if (h == specialMovementNextFruit)
            {
                ResetMovementSpeed();
                specialMovementNextFruit = null;
            }

            if (ist == IncreaseScoreType.FruitTickTiny || hpIncrease <= 0)
                return;

            ResetMovementSpeed();

            //applies hyperdash
            checkDistanceToNextFruit(h as HitCircleFruits);

            WiimoteManager.Rumble(Player.KiaiActive ? 64 : 32);

            HpBar.KiBulge();

            if (HpBar.CurrentHp > 180)
                HpBar.KiExplode();
        }

        /// <summary>
        /// Checks the distance to next fruit.  If it is too far to reach, we give a temporary speed bonus.
        /// </summary>
        private void checkDistanceToNextFruit(HitCircleFruits currentObject)
        {
            if (currentObject.HyperDash)
            {
                HitObject nextObject = currentObject.HyperDashTarget;

                float distanceToNext = Math.Abs(nextObject.Position.X - catcher1.Position.X);
                // Don't apply 60fps frame buffer if hyperdash lasts less than a frame (catcher will fly backwards)
                float timeToNext = Math.Max(1f, nextObject.StartTime - AudioEngine.Time - (float)(GameBase.SIXTY_FRAME_TIME));

                //time for some ZOOOOOOOM
                GlowCatcherRed();

                specialMovementModifier = distanceToNext / (float)timeToNext;
                specialMovementDirection = nextObject.Position.X > catcher1.Position.X ? 1 : -1;
                specialMovementNextFruit = currentObject.HyperDashTarget;
            }
        }

        private void ResetMovementSpeed()
        {
            specialMovementModifier = 1;
            specialMovementDirection = 0;
            specialWaitingState = false;
        }

        pTexture[] catcherIdle;
        pTexture[] catcherFail;
        pTexture[] catcherKiai;


        internal override void InitializeSprites()
        {
            catcherIdle = SkinManager.LoadAll("fruit-catcher-idle");
            if (catcherIdle != null && SkinManager.Current.Version >= 2.3)
            {
                catcherFail = SkinManager.LoadAll("fruit-catcher-fail");
                catcherKiai = SkinManager.LoadAll("fruit-catcher-kiai");
            }
            else
                catcherIdle = catcherFail = catcherKiai = SkinManager.LoadAll(@"fruit-ryuuta");

            catcher1 = new pAnimation(catcherIdle, Fields.Gamefield, Origins.Custom,
                                   Clocks.Game, new Vector2(256, 340), 0.81f, true, Color.White);
            catcher1.SetFramerateFromSkin();
            catcher1.OriginPosition = new Vector2(catcher1.DrawWidth / 2, 16);
            catcher1.Scale = 0.7f;

            checkPosition = catcher1.Position.X;

            hitObjectManager.spriteManager.Add(catcher1);

            catcherWidth = 305 / GameBase.GamefieldRatio * hitObjectManager.SpriteRatio * 0.7f;
            catcherWidthHalf = catcherWidth / 2;
            catchMargin = catcherWidth * 0.1f;

            if (ConfigManager.sComboBurst)
            {
                if (SkinManager.Current.Version >= 2.3)
                    comboBurstSprites = SkinManager.LoadAll(@"comboburst-fruits");

                if (comboBurstSprites == null)
                    comboBurstSprites = SkinManager.LoadAll(@"comboburst");
            }

            base.InitializeSprites();
        }

        internal float baseMovementSpeed;
        private float specialMovementModifier = 1;
        private int specialMovementDirection;

        bool importantFrame;
        int importantFrameCheck;
        private HitObject specialMovementNextFruit;
        private bool specialWaitingState;

        internal bool Dashing;

        Obfuscated<float> checkPosition;

        internal bool LeftPressed;
        internal bool RightPressed;

        internal override void UpdateInput()
        {
            if (!IsInitialized || Player.Paused)
                return;

            int frameCheck = 0;

            if (!InputManager.ReplayMode)
            {
                InputManager.leftButton1 |= KeyboardHandler.IsKeyDown(BindingManager.For(Bindings.FruitsDash));
                InputManager.leftButton1 |= JoystickHandler.IsKeyDown(BindingManager.For(Bindings.FruitsDash));
                InputManager.leftButton1i |= InputManager.leftButton1;
                if (InputManager.leftButton1i)
                    InputManager.leftButton = ButtonState.Pressed;
            }

            Dashing = InputManager.leftButton == ButtonState.Pressed;

            if (Dashing)
                frameCheck += 4;

            float currentMovementSpeed = baseMovementSpeed * specialMovementModifier;

            if (!Dashing) currentMovementSpeed /= 2;

            if (InputManager.ReplayMode || Player.Relaxing)
            {
                float newPos = InputManager.ReplayGamefieldCursor.X;

                if (InputManager.NewReplayFrame)
                {
                    LeftPressed = catcher1.Position.X > newPos;
                    RightPressed = catcher1.Position.X < newPos;
                }

                if (catcher1.Position.X != newPos)
                {


#if DEBUG
                    if (!ModManager.CheckActive(Mods.Autoplay) && (Math.Abs(catcher1.Position.X - newPos) > currentMovementSpeed * GameBase.SIXTY_FRAME_TIME))
                        NotificationManager.ShowMessageMassive("Impossible movement (" + Math.Abs(catcher1.Position.X - newPos) + " vs " + (currentMovementSpeed * GameBase.SIXTY_FRAME_TIME) + ")", 1000);
#endif

                    catcher1.FlipHorizontal = catcher1.Position.X > newPos;

                    catcher1.Position.X = newPos;
                }
            }
            else
            {
                if (GameBase.ElapsedMilliseconds > 33)
                    return;

                if (Math.Abs(catcher1.Position.X - checkPosition) > 0.01f)
                {
                    catcher1.Position.X = checkPosition;
                    CurrentScore.AllowSubmission = false;
                }


                if ((RightPressed = KeyboardHandler.IsKeyDown(BindingManager.For(Bindings.FruitsRight)) || JoystickHandler.IsKeyDown(BindingManager.For(Bindings.FruitsRight))))
                {
                    frameCheck += 1;
                    catcher1.Position.X = (float)(catcher1.Position.X + currentMovementSpeed * GameBase.ElapsedMilliseconds);
                    catcher1.FlipHorizontal = false;
                }

                if ((LeftPressed = KeyboardHandler.IsKeyDown(BindingManager.For(Bindings.FruitsLeft)) || JoystickHandler.IsKeyDown(BindingManager.For(Bindings.FruitsLeft))))
                {
                    frameCheck += 2;
                    catcher1.Position.X = (float)(catcher1.Position.X - currentMovementSpeed * GameBase.ElapsedMilliseconds);
                    catcher1.FlipHorizontal = true;
                }
            }

            checkSpecialWaitingState();

            importantFrame = frameCheck != importantFrameCheck;
            importantFrameCheck = frameCheck;

            catcher1.Position.X = MathHelper.Clamp(catcher1.Position.X, 0, 512);
            checkPosition = catcher1.Position.X;
        }

        private bool specialStateColourChange;
        private Vector2 catcherPositionLastFrame;
        private int lastDashSpriteTime;

        private void checkSpecialWaitingState()
        {
            if (specialStateColourChange != (specialMovementModifier != 1) && !ModManager.CheckActive(Mods.Cinema))
            {
                specialStateColourChange = specialMovementModifier != 1;
                catcher1.FadeColour(specialStateColourChange ? SkinManager.CurrentFruitsSkin.ColourHyperDash : Color.White, 100);
                if (player.eventManager.backgroundEvent != null && player.eventManager.backgroundEvent.Sprite != null)
                    player.eventManager.backgroundEvent.Sprite.FadeColour(specialStateColourChange ? new Color(100, 100, 100) : Color.White, 100);
            }

            if (specialMovementModifier != 1 && specialMovementNextFruit != null)
            {
                if ((specialMovementDirection > 0 && catcher1.Position.X >= specialMovementNextFruit.Position.X) ||
                    (specialMovementDirection < 0 && catcher1.Position.X <= specialMovementNextFruit.Position.X))
                {
                    catcher1.Position.X = specialMovementNextFruit.Position.X;

                    ResetMovementSpeed();
                    specialWaitingState = true;
                    GlowCatcherRed();
                }
            }
        }

        private void GlowCatcherRed()
        {
            pSprite catcher1a = new pSprite(catcher1.Texture, Fields.Gamefield, Origins.Centre,
                       Clocks.Game, catcher1.Position + new Vector2(0, catcher1.DrawHeight * 0.18f), 0.84f, false, SkinManager.CurrentFruitsSkin.ColourHyperDashAfterImage);
            catcher1a.FlipHorizontal = catcher1.FlipHorizontal;
            catcher1a.Additive = true;
            catcher1a.Scale = 0.71f;
            catcher1a.FadeOut(1200);
            catcher1a.Transformations.Add(new Transformation(TransformationType.Scale, catcher1a.Scale, catcher1a.Scale * 1.2f, GameBase.Time,
                                                             GameBase.Time + 1200, EasingTypes.In));
            hitObjectManager.spriteManager.Add(catcher1a);
        }

        internal override bool IsImportantFrame()
        {
            return importantFrame;
        }

        internal override void UpdateScoring()
        {
            if (Player.Failed)
                return;

            foreach (HitObject h in hitObjectManager.hitObjectsMinimal)
            {
                HitCircleFruits hf = h as HitCircleFruits;
                if (hf == null) continue; //should never happen, though.

                if (AudioEngine.Time >= hf.StartTime && !hf.IsHit)
                {
                    if (catcher1.Position.X - catcherWidthHalf + catchMargin < hf.Position.X &&
                        catcher1.Position.X + catcherWidthHalf - catchMargin > hf.Position.X)
                    {
                        Player.Instance.LogHitError(hf, (int)(catcher1.Position.X - hf.Position.X));
                        hf.ValidHit = true;
                    }
                    else
                    {
                        hf.ValidHit = false;
                    }

                    hitObjectManager.Hit(hf);

                    if (hf.ValidHit)
                    {
                        catcher1.TextureArray = hf is HitCircleFruitsSpin || AudioEngine.KiaiEnabled ? catcherKiai : catcherIdle;

                        comboCounterFruits.s_hitCombo2.FlashColour(hf.Colour, 300);

                        //Place the valid fruit on the plate.
                        foreach (pSprite p in hf.SpriteCollection)
                        {
                            p.Transformations.RemoveAll(t => t.Type == TransformationType.Movement || t.Type == TransformationType.Scale);
                            p.Scale *= 0.5f;
                        }

                        hf.CatchOffset.X = hf.Position.X - catcher1.Position.X;
                        hf.CatchOffset.Y = -5;

                        //If there is another fruit, stack it!
                        while (null != caughtFruits.Find(f => Vector2.DistanceSquared(f.CatchOffset, hf.CatchOffset) < 10 * 10))
                        {
                            hf.CatchOffset.X += GameBase.random.Next(-5, 5);
                            hf.CatchOffset.Y -= GameBase.random.Next(0, 5);
                        }

                        if (hf is HitCircleFruitsTick)
                        {
                            Explode(hf);
                            bounceFruit(hf, false);
                        }
                        else if (hf is HitCircleFruitsTickTiny)
                        {
                            bounceFruit(hf, false);
                        }
                        else
                        {
                            Explode(hf);
                            caughtFruits.Add(hf);
                            foreach (pSprite p in hf.SpriteCollection)
                                p.AlwaysDraw = true;
                        }

                        ThrowFruit(true);
                    }
                    else
                    {
                        if (!(hf is HitCircleFruitsSpin))
                            catcher1.TextureArray = catcherFail;

                        //Oops we missed, lets look sad.
                        foreach (pSprite p in hf.SpriteCollection)
                            p.Transformations.Add(new Transformation(TransformationType.Rotation, 0, 2, AudioEngine.Time, AudioEngine.Time + 500));

                        ThrowFruit(false);
                    }
                }
            }

            base.UpdateScoring();
        }

        internal override void UpdateGeneral()
        {
            caughtFruits.ForEach(hf => hf.ModifyPosition(catcher1.Position + hf.CatchOffset));
            caughtSprites.ForEach(s => s.Position = catcher1.Position + s.InitialPosition);
            caughtSprites.RemoveAll(s => !s.IsVisible);

            if (Dashing && GameBase.Time - lastDashSpriteTime > 16)
            {
                pSprite catcher1a = new pSprite(catcher1.Texture, Fields.Gamefield, Origins.TopCentre,
                                       Clocks.Game, catcher1.Position, 0.84f, false, catcher1.InitialColour);
                catcher1a.Alpha = 0.5f;
                catcher1a.FlipHorizontal = catcher1.FlipHorizontal;
                catcher1a.Additive = true;
                catcher1a.OriginPosition.Y += 16;
                catcher1a.Scale = 0.71f;
                catcher1a.FadeOut(catcher1.Position.X != catcherPositionLastFrame.X ? 250 : 100);

                hitObjectManager.spriteManager.Add(catcher1a);

                catcherPositionLastFrame = catcher1.Position;
                lastDashSpriteTime = GameBase.Time;
            }

            if (catcher1.TextureArray != catcherFail)
                catcher1.TextureArray = AudioEngine.KiaiEnabled ? catcherKiai : catcherIdle;
        }

        private void Explode(HitObject h)
        {
            if (ModManager.CheckActive(Mods.Cinema))
                return;

            float explosionOffset = OsuMathHelper.Clamp(((HitCircleFruits)h).CatchOffset.X, -catcherWidthHalf + catchMargin * 3, catcherWidthHalf - catchMargin * 3);

            if (!(h is HitCircleFruitsTick))
            {
                float scale = OsuMathHelper.Clamp(ComboCounter.HitCombo / 200f, 0.35f, 1.125f);

                pSprite exp1 = new pSprite(SkinManager.Load(@"scoreboard-explosion-2", SkinSource.Osu),
                                           Fields.Gamefield, Origins.CentreLeft, Clocks.Game,
                                           h.Position, 0.99f,
                                           false, h.Colour);
                exp1.Rotation = (float)(-Math.PI / 2);
                exp1.FadeOut(300);
                exp1.Transformations.Add(new Transformation(TransformationType.VectorScale, new Vector2(1, 0.9f),
                                                            new Vector2(16 * scale, 1.1f), GameBase.Time, GameBase.Time + 160,
                                                            EasingTypes.Out));
                spriteManagerAdd.Add(exp1);

                exp1.IsVisible = true;
                exp1.InitialPosition = new Vector2(explosionOffset, 0);
                caughtSprites.Add(exp1);
            }
            pSprite exp2 = new pSprite(SkinManager.Load(@"scoreboard-explosion-1", SkinSource.Osu),
                                       Fields.Gamefield, Origins.CentreLeft, Clocks.Game,
                                       h.Position, 1, false,
                                       h.Colour);
            exp2.Rotation = (float)(-Math.PI / 2);
            spriteManagerAdd.Add(exp2);
            exp2.FadeOut(700);
            exp2.Transformations.Add(new Transformation(TransformationType.VectorScale, new Vector2(0.9f, 1),
                                                        new Vector2(0.9f, 1.3f), GameBase.Time, GameBase.Time + 500,
                                                        EasingTypes.Out));

            exp2.IsVisible = true;
            exp2.InitialPosition = new Vector2(explosionOffset, 0);
            caughtSprites.Add(exp2);
        }

        private void ThrowFruit(bool happy)
        {
            if (!hitObjectManager.hitObjects[hitObjectManager.currentHitObjectIndex].LastInCombo)
                return;

            if (hitObjectManager.lastHitObject is HitCircleFruitsSpin)
            {
                //Spinner end
                foreach (HitCircleFruits f in caughtFruits)
                    bounceFruit(f, true);
                ((HitCircleFruitsSpin)hitObjectManager.lastHitObject).PlayActualSound();

                pSprite exp1 = new pSprite(SkinManager.Load(@"scoreboard-explosion-1", SkinSource.Osu),
                           Fields.Gamefield, Origins.CentreLeft, Clocks.Game,
                           Vector2.Zero, 0.99f,
                           false, Color.White);
                exp1.Rotation = (float)(-Math.PI / 2);
                exp1.FadeOut(800);
                exp1.Transformations.Add(new Transformation(TransformationType.VectorScale, new Vector2(0, 1.8f),
                                                            new Vector2(4, 2.2f), GameBase.Time, GameBase.Time + 500,
                                                            EasingTypes.Out));
                exp1.IsVisible = true;

                caughtSprites.Add(exp1);
                spriteManagerAdd.Add(exp1);
            }
            else if (happy)
            {
                //Happy combo
                foreach (HitCircleFruits f in caughtFruits)
                    bounceFruit(f, false);
            }
            else
            {
                //Sad combo
                foreach (HitCircleFruits f in caughtFruits)
                {
                    foreach (pSprite p in f.SpriteCollection)
                    {
                        GameBase.PhysicsManager.Add(p);
                        p.FadeOut(1000);
                        p.AlwaysDraw = false;
                    }
                }
            }

            caughtFruits.Clear();
        }

        private void bounceFruit(HitCircleFruits f, bool highPower)
        {
            Vector2 v = new Vector2(f.CatchOffset.X * 4, highPower ? -400 : -120);

            foreach (pSprite p in f.SpriteCollection)
            {
                p.AlwaysDraw = false;
                p.FadeOut(2000);
                GameBase.PhysicsManager.Add(p, v);
            }
        }

        internal override void UpdateCombo()
        {
            comboCounterFruits.Update(GameBase.GamefieldToDisplayX(catcher1.Position.X) / GameBase.WindowRatio);
        }

        internal override HitObjectManager CreateHitObjectManager()
        {
            return new HitObjectManagerFruits();
        }

        internal override void InitializeComboCounter()
        {
            comboCounterFruits = new ComboCounterFruits(spriteManagerWidescreen);
            ComboCounter = comboCounterFruits;
        }

        internal override void DrawFlashlight()
        {
            if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Flashlight))
            {
                if (s_Flashlight.Texture == null ||
                    spriteManagerFlashlight.SpriteList.Count == 0 ||
                    (GameBase.OGL && !s_Flashlight.Texture.TextureGl.Loaded) ||
                    (GameBase.D3D && s_Flashlight.Texture.TextureXna == null))
                {
                    if (AllowSubmission)
                    {
                        CurrentScore.AllowSubmission = false;
                        player.AddFlashlightFlag();

                        ConfigManager.PurgeHashCache();
                        GameBase.ChangeMode(OsuModes.Update);
                    }
                    return;
                }

                s_Flashlight.Position = GameBase.GamefieldToDisplay(catcher1.Position) / GameBase.WindowRatio;

                if (!Player.Paused)
                {
                    SliderOsu s = player.ActiveHitObject as SliderOsu;
                    if (s != null && s.IsSliding)
                    {
                        if (!s_LightsOut.IsVisible)
                            s_LightsOut.Alpha = 0.8f;
                    }
                    else if (s_LightsOut.IsVisible)
                        s_LightsOut.Alpha = 0;

                    if (GameBase.SixtyFramesPerSecondFrame)
                    {
                        if (EventManager.BreakMode)
                        {
                            if (s_Flashlight.Scale < 8)
                                s_Flashlight.Scale += 0.1f;
                        }
                        else
                        {
                            if (ComboCounter.HitCombo < 100)
                            {
                                if (s_Flashlight.Scale > 5.2f)
                                    s_Flashlight.Scale -= 0.1f;
                                if (s_Flashlight.Scale < 5.2f)
                                    s_Flashlight.Scale += 0.1f;
                            }
                            else if (ComboCounter.HitCombo < 200)
                            {
                                if (s_Flashlight.Scale > 4.6f)
                                    s_Flashlight.Scale -= 0.1f;
                                if (s_Flashlight.Scale < 4.6f)
                                    s_Flashlight.Scale += 0.1f;
                            }
                            else
                            {
                                if (s_Flashlight.Scale > 4.0f)
                                    s_Flashlight.Scale -= 0.1f;
                                if (s_Flashlight.Scale < 4.0f)
                                    s_Flashlight.Scale += 0.1f;
                            }
                        }
                    }
                }

                spriteManagerFlashlight.Draw();
            }
        }
    }
}