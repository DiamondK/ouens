﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Audio;
using osu.Graphics;
using osu.Graphics.Sprites;
using osu.Graphics.Skinning;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu_common;

namespace osu.GameModes.Play.Rulesets.Taiko
{
    enum TaikoMascotStates
    {
        None,
        Idle,
        Fail,
        Kiai,
        Clear
    }

    class TaikoMascot : pDrawableComponent
    {
        private pAnimation mascot;
        Dictionary<TaikoMascotStates, MascotAnimationSequence> animations = new Dictionary<TaikoMascotStates, MascotAnimationSequence>();

        internal override void Initialize()
        {
            spriteManager.WidescreenAutoOffset = false;

            mascot = new pAnimation(null, Fields.TopLeft, Origins.BottomLeft, Clocks.Audio, Vector2.Zero, 1, true, Color.White);
            mascot.AnimationFinished += mascot_AnimationFinished;
            mascot.Scale = 0.6f;

            spriteManager.Add(mascot);

            animations.Add(TaikoMascotStates.Clear, new MascotAnimationSequence(SkinManager.LoadAll(@"pippidonclear", SkinSource.All, false), 
                new[] { 0, 1, 2, 3, 4, 5, 6, 5, 6, 5, 4, 3, 2, 1, 0 }, 1000 / 10F));
            animations.Add(TaikoMascotStates.Fail, new MascotAnimationSequence(SkinManager.LoadAll(@"pippidonfail", SkinSource.All, false), null, 0));
            animations.Add(TaikoMascotStates.Kiai, new MascotAnimationSequence(SkinManager.LoadAll(@"pippidonkiai", SkinSource.All, false), null, 0));
            animations.Add(TaikoMascotStates.Idle, new MascotAnimationSequence(SkinManager.LoadAll(@"pippidonidle", SkinSource.All, false), null, 0));

            SetState(TaikoMascotStates.Idle, false);

            base.Initialize();
        }

        void mascot_AnimationFinished()
        {
            mascot.LoopType = LoopTypes.LoopForever;
            SetState(IdleState, false);
        }

        internal TaikoMascotStates IdleState;
        internal TaikoMascotStates CurrentState;

        internal Vector2 Position
        {
            get { return mascot.Position; }
            set { mascot.Position = value; }
        }

        internal void SetState(TaikoMascotStates state, bool playOnceOnly)
        {
            if (!playOnceOnly)
                IdleState = state;

            CurrentState = state;

            if (mascot.LoopType == LoopTypes.LoopOnce)
                return; //running an animation currently.

            MascotAnimationSequence mas = animations[state];

            mascot.TextureArray = mas.frames;
            mascot.FrameDelay = mas.delay != 0 ? mas.delay : AudioEngine.beatLength;
            mascot.CustomSequence = mas.sequence;
            mascot.LoopType = playOnceOnly ? LoopTypes.LoopOnce : LoopTypes.LoopForever;
        }

        internal override void Update()
        {
            if (mascot.LoopType == LoopTypes.LoopForever)
                mascot.FrameDelay = AudioEngine.beatLength;
            base.Update();
        }
    }

    internal class MascotAnimationSequence
    {
        internal readonly pTexture[] frames;
        internal readonly int[] sequence;
        internal readonly double delay;

        internal MascotAnimationSequence(pTexture[] frames, int[] sequence, double delay)
        {
            this.frames = frames;
            this.sequence = sequence;
            this.delay = delay;
        }
    }
}
