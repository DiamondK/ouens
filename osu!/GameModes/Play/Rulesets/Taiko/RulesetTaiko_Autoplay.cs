﻿using System.Collections.Generic;
using osu.Audio;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.HitObjects.Taiko;
using osu.Input;
using osu_common.Bancho.Objects;

namespace osu.GameModes.Play.Rulesets.Taiko
{
    internal partial class RulesetTaiko
    {
        internal override void CreateAutoplayReplay()
        {
            InputManager.ReplayScore.replay = new List<bReplayFrame>();
            List<bReplayFrame> replay = InputManager.ReplayScore.replay;
            bool hitButton = true;

            replay.Add(new bReplayFrame(-100000, 320, 240, pButtonState.None));
            replay.Add(new bReplayFrame(hitObjectManager.hitObjects[0].StartTime - 1000, 320, 240,
                                            pButtonState.None));

            for (int i = 0; i < hitObjectManager.hitObjectsCount; i++)
            {
                HitObject h = hitObjectManager.hitObjects[i];
                pButtonState button;



                SpinnerTaiko sp = h as SpinnerTaiko;
                if (sp != null)
                {
                    int d = 0;
                    int count = 0;
                    int req = sp.rotationRequirement + 1;
                    int hitRate = h.Length / req;
                    for (int j = h.StartTime; j < h.EndTime; j += hitRate)
                    {
                        switch (d)
                        {
                            default:
                                button = pButtonState.Left1;
                                break;
                            case 1:
                                button = pButtonState.Right1;
                                break;
                            case 2:
                                button = pButtonState.Left2;
                                break;
                            case 3:
                                button = pButtonState.Right2;
                                break;
                        }
                        replay.Add(new bReplayFrame(j, h.Position.X, h.Position.Y, button));
                        d = (d + 1) % 4;
                        if (++count > req)
                            break;
                    }
                }
                else if (h is SliderTaiko)
                {
                    SliderTaiko s = h as SliderTaiko;


                    double delay = s.MinHitDelay;

                    double time = s.StartTime;

                    for (int j = 0; j < s.hittablePoints.Count; j++)
                    {
                        replay.Add(new bReplayFrame((int)time, h.Position.X, h.Position.Y,
                                                    hitButton ? pButtonState.Left1 : pButtonState.Left2));
                        time += delay;
                        hitButton = !hitButton;
                    }
                }
                else
                {
                    if (h.Whistle || h.Clap)
                    {
                        if (h.Finish)
                            button = pButtonState.Right1 | pButtonState.Right2;
                        else
                            button = hitButton ? pButtonState.Right1 : pButtonState.Right2;
                    }
                    else
                    {
                        if (h.Finish)
                            button = pButtonState.Left1 | pButtonState.Left2;
                        else
                            button = hitButton ? pButtonState.Left1 : pButtonState.Left2;
                    }
                    replay.Add(new bReplayFrame(h.StartTime, h.Position.X, h.Position.Y, button));
                }

                replay.Add(new bReplayFrame(h.EndTime + 1, h.EndPosition.X, h.EndPosition.Y, pButtonState.None));

                if (i < hitObjectManager.hitObjectsCount - 1)
                {
                    int waitTime = hitObjectManager.hitObjects[i + 1].StartTime - 1000;
                    if (waitTime > h.EndTime)
                        replay.Add(new bReplayFrame(waitTime, h.EndPosition.X, h.EndPosition.Y, pButtonState.None));
                }

                hitButton = !hitButton;
            }

            Player.currentScore.replay = InputManager.ReplayScore.replay;
            Player.currentScore.playerName = "mekkadosu!";

            InputManager.ReplayScore.replay.ForEach(f =>
                                                        {
                                                            f.mouseX = -150;
                                                            f.mouseY = -150;
                                                        }); //Get rid of the mouse cursor for taiko autoplay.
        }
    }
}