﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play.Components;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Taiko;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Notifications;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;
using osu_common;

namespace osu.GameModes.Play.Rulesets.Taiko
{
    internal partial class RulesetTaiko : Ruleset
    {
        private readonly List<pSprite> flyingCircles = new List<pSprite>();
        private pSprite taikoInnerLeft;
        private pSprite taikoInnerRight;
        private pSprite taikoOuterLeft;
        private pSprite taikoOuterRight;
        private TaikoMascot mascot;

        HitObject firstDonKatu;

        public RulesetTaiko(Player player)
            : base(player)
        {
        }

        internal override bool AllowMetadataDisplay
        {
            get { return false; }
        }

        internal override Vector2 MousePosition
        {
            get { return new Vector2(InputManager.leftButton1 || InputManager.rightButton1 ? 0 : (InputManager.leftButton2 || InputManager.rightButton2 ? 640 : 320), 9999); }
        }

        internal override bool AllowScoreboardDisplay
        {
            get { return true; }
        }

        internal override Vector2 ScoreboardPosition
        {
            get { return new Vector2(0, 264); }
        }

        internal override bool AllowSubmission
        {
            get { return true; }
        }

        internal override bool AllowInstantUnpause
        {
            get { return true; }
        }

        internal override bool AllowFailTint
        {
            get { return false; }
        }

        internal override bool AllowCountdown
        {
            get
            {
                return false;
            }
        }

        internal override bool AllowWarningArrows
        {
            get
            {
                return false;
            }
        }

        internal override bool AllNotesHit
        {
            get
            {

                int taikoHits = hitObjectManager.hitObjects.FindAll(
                           h => (h is HitCircleTaiko)).Count;

                return taikoHits == CurrentScore.totalHits;
            }
        }

        internal override bool AllowMouseHide
        {
            get
            {
                if (Player.Playing) return true;

                return base.AllowMouseHide || InputManager.ReplayMode;
            }
        }

        protected override void InitializeScoreDisplay()
        {
            ScoreDisplay = new ScoreDisplayTaiko(spriteManagerWidescreen);
        }

        internal override void InitializeModSettings()
        {
            base.InitializeModSettings();

            if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Flashlight))
            {
                pSprite extraBlackness = new pSprite(GameBase.WhitePixel, Fields.Storyboard, Origins.TopLeft, Clocks.Game, new Vector2(640,0), 0, true, Color.Black);
                extraBlackness.Scale = 1.6f;
                extraBlackness.VectorScale = new Vector2(GameBase.WindowWidthScaled - 640, GameBase.WindowHeightScaled);
                spriteManagerFlashlight.Add(extraBlackness);
            }
        }

        internal static bool MouthOpen(int multiplier)
        {
            double beatLength = AudioEngine.BeatLengthNotInheritted;
            return ((AudioEngine.Time - AudioEngine.CurrentOffset) % ((beatLength * 2) / multiplier))
                >= beatLength / multiplier;
        }


        int circlesFrame;

        public override void Dispose()
        {
            if (IsInitialized)
                mascot.Dispose(); //not initialized on beatmap-load error
            base.Dispose();
        }

        internal override void Draw()
        {
            if (!IsInitialized) return;

            base.Draw();

            mascot.spriteManager.Alpha = spriteManager.Alpha; //used to fade out during SB intro.
            mascot.Draw();

            updateSlidingBar();
        }

        internal override void UpdateGeneral()
        {
            mascot.Update();

            //remove any old slider sprites that now have no transformations (off-screen to left).
            if (taikoSliderActiveCollection.Count > 1)
                taikoSliderActiveCollection.RemoveAll(s => s.Transformations[0].Time2 < AudioEngine.Time);

            if (GameBase.SixtyFramesPerSecondFrame)
            {
                foreach (pSprite p in flyingCircles)
                {
                    p.Position.X += (p.InitialPosition.X - p.Position.X + 2) * 0.065f;
                    p.Scale *= 0.965f;
                    p.Position.Y -= p.TagNumeric / 11.5f;
                    p.TagNumeric -= 7;
                    if (p.TagNumeric < 70 && p.Transformations.Count < 1)
                    {
                        p.FadeOut(300);
                    }
                    if (p.TagNumeric < -32)
                    {
                        p.AlwaysDraw = false;
                    }
                }

                flyingCircles.RemoveAll(p => !p.AlwaysDraw);
            }

            bool doCircleAnimation = ComboCounter.HitCombo >= 50;

            if (doCircleAnimation)
            {
                circlesFrame = (ComboCounter.HitCombo >= 150 ? MouthOpen(2) : MouthOpen(1)) ? 0 : 1;

                hitObjectManager.hitObjectsMinimal.ForEach(ho =>
                {
                    SliderTaiko s = ho as SliderTaiko;
                    if (s != null)
                        s.sliderStartCircle.SpriteHitCircle2.CurrentFrame = circlesFrame;
                    else
                    {
                        HitCircleTaiko h = ho as HitCircleTaiko;
                        if (h != null)
                            h.SpriteHitCircle2.CurrentFrame = circlesFrame;
                    }
                });
            }


            base.UpdateGeneral();
        }

        private List<pSprite> taikoSliderActiveCollection = new List<pSprite>();

        private void updateSlidingBar()
        {
            if (taikoSlider == null) return;

            float oldXpos = 0;
            while ((oldXpos = (taikoSlider.Position.X + (taikoSlider.DrawWidth * taikoSlider.Scale * 0.625f)) * GameBase.WindowRatio) <= GameBase.WindowWidth)
            {
                pSprite old = taikoSlider;
                taikoSlider = taikoSlider.Clone();

                taikoSliderActiveCollection.Add(taikoSlider);

                float startX = oldXpos / GameBase.WindowRatio;
                float endX = old.Transformations[0].EndVector.X;

                taikoSlider.Position.X = startX;
                taikoSlider.Transformations[0].StartVector.X = startX;
                taikoSlider.Transformations[0].EndVector.X = endX;

                taikoSlider.Transformations[0].Time1 = (int)(AudioEngine.Time);
                taikoSlider.Transformations[0].Time2 = (int)(AudioEngine.Time + (endX - startX) / (old.Transformations[0].EndVector.X - old.Transformations[0].StartVector.X) * old.Transformations[0].Duration);

                player.spriteManagerBelowHitObjectsWidescreen.Add(taikoSlider);
            }
        }

        internal override ClickAction CheckClickAction(HitObject h)
        {
            return ClickAction.Hit; //We don't care in this case - always accept the next in order since we are working in 1 dimension rather than 2.
        }

        internal override void OnIncreaseScoreHit(IncreaseScoreType ist, double hpIncrease, HitObject h)
        {
            if (ist == IncreaseScoreType.TaikoDenDenComplete)
                mascot.SetState(TaikoMascotStates.Clear, true);

            base.OnIncreaseScoreHit(ist, hpIncrease, h);
        }

        internal override void OnClick(HitObject h)
        {
            HitCircleTaiko hc = h as HitCircleTaiko;
            SliderTaiko sl = h as SliderTaiko;
            SpinnerTaiko sp = h as SpinnerTaiko;

            if ((hc != null && hc.scoreValue > 0) || (sl != null && sl.LastHitSuccessful))
            {
                pTexture bg;
                pTexture overlay;

                if (hc != null)
                {
                    bg = hc.SpriteHitCircle1.Texture;
                    overlay = hc.SpriteHitCircle2.Texture;
                }
                else
                {
                    bg = sl.sliderStartCircle.SpriteHitCircle1.Texture;
                    overlay = sl.sliderStartCircle.SpriteHitCircle2.Texture;
                }

                float depth = 1 - 0.03f * SpriteManager.drawOrderBwd(h != null ? h.StartTime : 0.1f) / 0.8f;

                pSprite s =
                    new pSprite(bg, Fields.GamefieldWide, Origins.Centre,
                                Clocks.Audio, HitObjectManagerTaiko.HIT_LOCATION,
                                depth, true,
                                h != null ? h.Colour : new Color(252, 184, 6));
                s.Scale = h != null ? h.SpriteCollection[0].Scale : 0.7f;
                spriteManagerWidescreen.Add(s);
                s.InitialPosition.X = GameBase.DisplayToGamefieldXWide(HpBar.CurrentXPosition);

                s.TagNumeric = 169;
                flyingCircles.Add(s);

                s =
                    new pSprite(overlay, Fields.GamefieldWide, Origins.Centre,
                                Clocks.Audio, HitObjectManagerTaiko.HIT_LOCATION,
                                depth + 0.0001f, true, Color.White);
                s.Scale = h != null ? h.SpriteCollection[0].Scale : 0.7f;
                s.InitialPosition.X = GameBase.DisplayToGamefieldXWide(HpBar.CurrentXPosition);
                s.TagNumeric = 169;
                spriteManagerWidescreen.Add(s);
                flyingCircles.Add(s);

                if (Player.KiaiActive)
                {
                    s_KiaiGlow.Transformations.RemoveAll(t => t.Type == TransformationType.Scale);
                    s_KiaiGlow.Transformations.Add(new Transformation(TransformationType.Scale, 0.85f, 0.7f, GameBase.Time, GameBase.Time + 80, EasingTypes.Out));
                }
            }

            //where we store the old values,
            //we set them to false to make the compiler happy
            bool oldLeftButton1i = false;
            bool oldLeftButton2i = false;

            bool oldRightButton1i = false;
            bool oldRightButton2i = false;


            //temporarilly change the buttons to the right ones if relaxed is on.
            if (Player.Relaxing && hc != null)
            {
                oldLeftButton1i = InputManager.leftButton1i;
                oldLeftButton2i = InputManager.leftButton2i;
                oldRightButton1i = InputManager.rightButton1i;
                oldRightButton2i = InputManager.rightButton2i;

                InputManager.leftButton1i = hc.CorrectButtonIsLeft && (oldRightButton1i || oldLeftButton1i);
                InputManager.leftButton2i = hc.CorrectButtonIsLeft && (oldRightButton2i || oldLeftButton2i);
                InputManager.rightButton1i = !hc.CorrectButtonIsLeft && (oldRightButton1i || oldLeftButton1i);
                InputManager.rightButton2i = !hc.CorrectButtonIsLeft && (oldRightButton2i || oldLeftButton2i);
            }


            if (InputManager.leftButton1i || InputManager.leftButton2i)
            {
                ControlPoint pt = hitObjectManager.Beatmap.controlPointAt(AudioEngine.Time + 2);
                HitSoundInfo hitSoundInfo = new HitSoundInfo(HitObjectSoundType.Normal, AudioEngine.CurrentSampleSet, AudioEngine.CustomSamples,  pt.volume, AudioEngine.CurrentSampleSet);
                AudioEngine.PlayHitSamples(hitSoundInfo, InputManager.leftButton1i ? -0.2f : 0.2f, true);

                if (InputManager.leftButton1i)
                {
                    taikoInnerLeft.Transformations.Clear();
                    taikoInnerLeft.Transformations.Add(new Transformation(TransformationType.Fade,
                                                                          taikoInnerLeft.Alpha, 1,
                                                                          GameBase.Time,
                                                                          GameBase.Time +
                                                                          (int)
                                                                          ((1 - taikoInnerLeft.Alpha) * 80),
                                                                          EasingTypes.Out));
                    taikoInnerLeft.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0,
                                                                          GameBase.Time + 100, GameBase.Time + 150));
                }

                if (InputManager.leftButton2i)
                {
                    taikoInnerRight.Transformations.Clear();
                    taikoInnerRight.Transformations.Add(new Transformation(TransformationType.Fade,
                                                                           taikoInnerRight.Alpha, 1,
                                                                           GameBase.Time,
                                                                           GameBase.Time +
                                                                           (int)
                                                                           ((1 - taikoInnerRight.Alpha) *
                                                                            80), EasingTypes.Out));
                    taikoInnerRight.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0,
                                                                           GameBase.Time + 100, GameBase.Time + 150));
                }
            }

            if (InputManager.rightButton1i || InputManager.rightButton2i)
            {
                ControlPoint pt = hitObjectManager.Beatmap.controlPointAt(AudioEngine.Time + 2);
                HitSoundInfo hitSoundInfo = new HitSoundInfo(HitObjectSoundType.Clap, AudioEngine.CurrentSampleSet, AudioEngine.CustomSamples, pt.volume, AudioEngine.CurrentSampleSet);
                AudioEngine.PlayHitSamples(hitSoundInfo, InputManager.rightButton1i ? -0.2f : 0.2f, true);

                if (InputManager.rightButton1i)
                {
                    taikoOuterLeft.Transformations.Clear();
                    taikoOuterLeft.Transformations.Add(new Transformation(TransformationType.Fade,
                                                                          taikoOuterLeft.Alpha, 1,
                                                                          GameBase.Time,
                                                                          GameBase.Time +
                                                                          (int)
                                                                          ((1 - taikoOuterLeft.Alpha) * 80),
                                                                          EasingTypes.Out));
                    taikoOuterLeft.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0,
                                                                          GameBase.Time + 100, GameBase.Time + 150));
                }

                if (InputManager.rightButton2i)
                {
                    taikoOuterRight.Transformations.Clear();
                    taikoOuterRight.Transformations.Add(new Transformation(TransformationType.Fade,
                                                                           taikoOuterRight.Alpha, 1,
                                                                           GameBase.Time,
                                                                           GameBase.Time +
                                                                           (int)
                                                                           ((1 - taikoOuterRight.Alpha) *
                                                                            80), EasingTypes.Out));
                    taikoOuterRight.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0,
                                                                           GameBase.Time + 100, GameBase.Time + 150));
                }
            }
            //return the input we just changed to the old values
            if (Player.Relaxing && hc != null)
            {
                InputManager.leftButton1i = oldLeftButton1i;
                InputManager.leftButton2i = oldLeftButton2i;
                InputManager.rightButton1i = oldRightButton1i;
                InputManager.rightButton2i = oldRightButton2i;
            }


        }

        internal override HitObjectManager CreateHitObjectManager()
        {
            return new HitObjectManagerTaiko();
        }

        internal override void InitializeCalculations()
        {
            HpMultiplierNormal = 200 / (0.06 * HP_HIT_300 * hitObjectManager.hitObjects.FindAll(h => h is HitCircleTaiko).Count *
                hitObjectManager.MapDifficultyRange(hitObjectManager.Beatmap.DifficultyHpDrainRate, 0.5f, 0.75f, 0.98f));
            HpMultiplierComboEnd = 0; // 200 / (0.06 * HP_HIT_300 * hitObjectManager.hitObjectsCount * 0.94);

            TotalHitsPossible = hitObjectManager.hitObjects.FindAll(h => !(h is SliderTaiko)).Count;

            if (GameBase.TestMode)
            {
                for (int i = 0; i < hitObjectManager.hitObjectsCount; i++)
                {
                    HitObject h = hitObjectManager.hitObjects[i];
                    IncreaseScoreType type;

                    if (h is HitCircleTaiko)
                    {
                        if (h.Finish)
                        {
                            type = IncreaseScoreType.Hit300;
                            type |= IncreaseScoreType.TaikoLargeHitBoth;
                        }
                        else
                        {
                            type = IncreaseScoreType.Hit300;
                        }

                        IncreaseScoreHit(type, h);
                    }
                    else if (h is SliderTaiko)
                    {
                        SliderTaiko slider = (SliderTaiko)h;
                        type = IncreaseScoreType.TaikoDrumRoll;
                        if (slider.Finish)
                            type |= IncreaseScoreType.TaikoLargeHitBoth;

                        for (int j = 0; j < slider.hittablePoints.Count; j++)
                        {
                            IncreaseScoreHit(type, h);
                        }
                        continue;
                    }
                    else if (h is SpinnerTaiko)
                    {
                        SpinnerTaiko spinner = (SpinnerTaiko)h;
                        for (int j = 0; j < spinner.rotationRequirement + 1; j++)
                        {
                            IncreaseScoreHit(IncreaseScoreType.TaikoDenDenHit, h);
                        }
                        IncreaseScoreHit(IncreaseScoreType.TaikoDenDenComplete, h);
                    }
                }

                PlayerTest pt = player as PlayerTest;
                if (pt != null)
                {
                    pt.testMaxScore = Player.currentScore.totalScore;
                    pt.testMaxCombo = ComboCounter.HitCombo;
                }
                Player.currentScore.totalScore = 0;
                ComboCounter.HitCombo = 0;
            }

            hitObjectManager.hitObjects.ForEach(h => h.MaxHp = 200);

            //Make the graph look correct.

            base.InitializeCalculations();
        }


        internal override void InitializeComboCounter()
        {
            ComboCounter = new ComboCounterTaiko(spriteManagerWidescreen);
        }

        internal override void InitializeHpBar()
        {
            HpBar = new HpBarTaiko(spriteManagerWidescreen);
        }

        internal override bool IsImportantFrame()
        {
            return false;
        }

        internal override void OnCompletion()
        {
            CurrentScore.pass = HpBar.CurrentHp > 100;

            if (!CurrentScore.pass && !ModManager.CheckActive(CurrentScore.enabledMods, Mods.NoFail | Mods.Relax) && !GameBase.TestMode)
            {
                HpBar.SetCurrentHp(0);
                //OnFailableHp will handle this exit condition.
                return;
            }

            mascot.SetState(TaikoMascotStates.Clear, true);

            base.OnCompletion();
        }

        internal override bool Fail(bool continuousPlay)
        {
            if ((firstDonKatu != null && !firstDonKatu.IsHit) || (!hitObjectManager.hitObjects[hitObjectManager.hitObjectsCount - 1].IsHit && !ModManager.CheckActive(CurrentScore.enabledMods, Mods.SuddenDeath)))
                return false;

            bool fail = (CurrentScore.totalHits > 0 && CurrentScore.totalSuccessfulHits == 0);
            fail |= CurrentScore.currentHp < 100 && !ModManager.CheckActive(CurrentScore.enabledMods, Mods.NoFail);

            //Taiko can't fail (mostly).
            return fail && base.Fail(continuousPlay);
        }

        internal override void DrawFlashlight()
        {
            if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Flashlight))
                s_Flashlight.Position = new Vector2(208, 208);

            base.DrawFlashlight();
        }

        internal override bool IsPassing
        {
            get
            {
                return CurrentScore.accuracy > 0.8f;
            }
        }

        internal override void OnSkip()
        {

        }

        internal override void Initialize()
        {
            InputManager.MouseButtonInstantRelease = true;

            base.Initialize();

            firstDonKatu = hitObjectManager.hitObjects.Find(h => h.IsType(HitObjectType.Normal));
        }

        internal override void InitializeSkin()
        {
            if (ConfigManager.sUseTaikoSkin)
            {
                SkinManager.LoadSkin("taiko");
            }
            else
            {
                SkinManager.LoadSkin();
            }
        }

        internal const int TAIKO_BAR_Y = 135;
        internal const int TAIKO_BAR_HEIGHT = 118;

        internal const int title_height = 36;

        pSprite s_KiaiGlow;

        pSprite s_BarRight;
        pSprite s_BarRight_Kiai;

        pSprite s_BarLeft;
        private pSprite taikoSlider;
        private pTexture sliderTexturePass;
        private pTexture sliderTextureFail;
        private pText metadataBarTitle;

        internal override void OnKiaiToggle(bool active)
        {
            if (active)
            {
                s_KiaiGlow.Transformations.Clear();
                s_KiaiGlow.FadeIn(100);
                s_KiaiGlow.Transformations.Add(new Transformation(TransformationType.Scale, 0.1f, 0.7f, GameBase.Time, GameBase.Time + 120, EasingTypes.Out));
                s_BarRight_Kiai.FadeIn(150);

                mascot.SetState(TaikoMascotStates.Kiai, false);
            }
            else
            {
                s_KiaiGlow.Transformations.Clear();
                s_KiaiGlow.FadeOut(600);

                s_BarRight_Kiai.FadeOut(150);

                mascot.SetState(TaikoMascotStates.Idle, false);
            }

            base.OnKiaiToggle(active);
        }

        internal override void InitializeSprites()
        {
            hitObjectManager.spriteManager.WidescreenAutoOffset = false;

            mascot = new TaikoMascot();
            mascot.Initialize();
            mascot.Position = new Vector2(0, TAIKO_BAR_Y + 25);


            s_BarRight =
                new pSprite(SkinManager.Load(@"approachcircle", SkinSource.Skin | SkinSource.Osu), Fields.GamefieldWide, Origins.Centre,
                            Clocks.Game, HitObjectManagerTaiko.HIT_LOCATION, 0.021f, true,
                            new Color(255, 255, 255, 180));
            s_BarRight.Scale = 0.73f;
            hitObjectManager.spriteManager.Add(s_BarRight);

            s_BarRight =
                new pSprite(SkinManager.Load(@"taikobigcircle", SkinSource.Skin | SkinSource.Osu), Fields.GamefieldWide, Origins.Centre,
                            Clocks.Game, HitObjectManagerTaiko.HIT_LOCATION, 0.021f, true,
                            new Color(255, 255, 255, 120));
            s_BarRight.Scale = 0.7f;
            hitObjectManager.spriteManager.Add(s_BarRight);

            s_KiaiGlow =
                new pSprite(SkinManager.Load(@"taiko-glow", SkinSource.Skin | SkinSource.Osu), Fields.GamefieldWide, Origins.Centre,
                            Clocks.Game, HitObjectManagerTaiko.HIT_LOCATION, 0.02f, true,
                            new Color(255, 228, 0, 0));
            s_KiaiGlow.Scale = 0.7f;
            hitObjectManager.spriteManager.Add(s_KiaiGlow);

            s_BarLeft =
                new pSprite(SkinManager.Load(@"taiko-bar-left", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                            Clocks.Game, new Vector2(0, TAIKO_BAR_Y), 0.95f, true, Color.White);
            hitObjectManager.spriteManager.Add(s_BarLeft);

            if (SkinManager.Current.Version >= 2.1)
            {
                s_BarRight =
                    new pSprite(SkinManager.Load(@"taiko-bar-right", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                Clocks.Game, new Vector2(0, TAIKO_BAR_Y), 0, true, Color.White);

                s_BarRight_Kiai =
                    new pSprite(SkinManager.Load(@"taiko-bar-right-glow", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                Clocks.Game, new Vector2(0, TAIKO_BAR_Y), 0.0001f, true, Color.TransparentWhite);
            }

            else
            {

                s_BarRight =
                    new pSprite(SkinManager.Load(@"taiko-bar-right", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                Clocks.Game, new Vector2(113, TAIKO_BAR_Y), 0, true, Color.White);

                s_BarRight_Kiai =
                    new pSprite(SkinManager.Load(@"taiko-bar-right-glow", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                Clocks.Game, new Vector2(113, TAIKO_BAR_Y), 0.0001f, true, Color.TransparentWhite);

            }

            if (GameBase.Widescreen)
            {
                s_BarRight_Kiai.VectorScale = s_BarRight.VectorScale = new Vector2(2 + (2 * GameBase.WindowOffsetX / GameBase.WindowRatioInverse) / s_BarRight.Texture.Width, 1);
            }

            hitObjectManager.spriteManager.Add(s_BarRight);
            hitObjectManager.spriteManager.Add(s_BarRight_Kiai);

            if (!ModManager.CheckActive(Mods.Cinema))
            {
                pSprite pb =
                            new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft,
                                        Clocks.Game, new Vector2(0, TAIKO_BAR_Y + TAIKO_BAR_HEIGHT), 0, true, Color.Black);
                pb.ScaleToWindowRatio = false;
                pb.Alpha = 0.9f;
                pb.VectorScale = new Vector2(GameBase.WindowWidth, title_height * GameBase.WindowRatio);
                player.spriteManagerBelowScoreboardWidescreen.Add(pb);

                metadataBarTitle = new pText(BeatmapManager.Current.SortTitleAuto, 19, new Vector2(-20, TAIKO_BAR_Y + TAIKO_BAR_HEIGHT + title_height / 2), 0.00001f, true, Color.TransparentWhite)
                {
                    Origin = Origins.CentreRight,
                    Field = Fields.TopRight,
                    TextBold = true,
                    FontFace = "skin-taiko"
                };
                player.spriteManagerBelowScoreboardWidescreen.Add(metadataBarTitle);
            }

            if (GameBase.Widescreen && ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Hidden))
            {
                pSprite s_BarRightExtra =
                new pSprite(SkinManager.Load(@"taiko-bar-right", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                            Clocks.Game, new Vector2(GameBase.WindowWidthScaled - 2 * GameBase.WindowOffsetX / GameBase.WindowRatio, TAIKO_BAR_Y), 0.1f, true, new Color(0, 0, 0, 255));
                s_BarRightExtra.DrawWidth = (int)(2 * GameBase.WindowOffsetX / GameBase.WindowRatioInverse) + 1;
                player.spriteManagerBelowHitObjectsWidescreen.Add(s_BarRightExtra);
            }


            int bindingOffset = SkinManager.Current.Version >= 2.1 ? 5 : 0;
            pText binding = new pText(BindingManager.NiceName(Bindings.TaikoInnerLeft), 18, new Vector2(25 + bindingOffset, TAIKO_BAR_Y + 50), Vector2.Zero,
                                      0.98f, false, Color.White, true);
            binding.TextBold = true;
            binding.FadeOut(5000);
            hitObjectManager.spriteManager.Add(binding);

            binding = new pText(BindingManager.NiceName(Bindings.TaikoInnerRight), 18, new Vector2(64 + bindingOffset, TAIKO_BAR_Y + 50), Vector2.Zero, 0.98f,
                                    false, Color.White, true);
            binding.TextBold = true;
            binding.FadeOut(5000);
            hitObjectManager.spriteManager.Add(binding);

            binding = new pText(BindingManager.NiceName(Bindings.TaikoOuterLeft), 18, new Vector2(8 + bindingOffset, TAIKO_BAR_Y + 23), Vector2.Zero, 0.98f,
                                    false, Color.White, true);
            binding.TextBold = true;
            binding.FadeOut(5000);
            hitObjectManager.spriteManager.Add(binding);

            binding = new pText(BindingManager.NiceName(Bindings.TaikoOuterRight), 18, new Vector2(75 + bindingOffset, TAIKO_BAR_Y + 23), Vector2.Zero, 0.98f,
                                false, Color.White, true);
            binding.TextBold = true;
            binding.FadeOut(5000);
            hitObjectManager.spriteManager.Add(binding);


            if (SkinManager.Current.Version >= 2.1)
            {
                taikoInnerLeft = new pSprite(SkinManager.Load(@"taiko-drum-inner", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                         Clocks.Game, new Vector2(0, TAIKO_BAR_Y), 0.96f, true, Color.TransparentWhite);
                hitObjectManager.spriteManager.Add(taikoInnerLeft);

                taikoInnerRight = new pSprite(SkinManager.Load(@"taiko-drum-inner", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                              Clocks.Game, new Vector2(56, TAIKO_BAR_Y), 0.96f, true, Color.TransparentWhite);
                taikoInnerRight.FlipHorizontal = true;
                hitObjectManager.spriteManager.Add(taikoInnerRight);

                taikoOuterLeft = new pSprite(SkinManager.Load(@"taiko-drum-outer", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                             Clocks.Game, new Vector2(0, TAIKO_BAR_Y), 0.96f, true, Color.TransparentWhite);
                taikoOuterLeft.FlipHorizontal = true;
                hitObjectManager.spriteManager.Add(taikoOuterLeft);

                taikoOuterRight = new pSprite(SkinManager.Load(@"taiko-drum-outer", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                              Clocks.Game, new Vector2(56, TAIKO_BAR_Y), 0.96f, true, Color.TransparentWhite);
                hitObjectManager.spriteManager.Add(taikoOuterRight);
            }
            else
            {
                taikoInnerLeft = new pSprite(SkinManager.Load(@"taiko-drum-inner", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                             Clocks.Game, new Vector2(18, TAIKO_BAR_Y + 31), 0.96f, true, Color.TransparentWhite);
                hitObjectManager.spriteManager.Add(taikoInnerLeft);

                taikoInnerRight = new pSprite(SkinManager.Load(@"taiko-drum-inner", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                              Clocks.Game, new Vector2(54, TAIKO_BAR_Y + 31), 0.96f, true, Color.TransparentWhite);
                taikoInnerRight.FlipHorizontal = true;
                hitObjectManager.spriteManager.Add(taikoInnerRight);

                taikoOuterLeft = new pSprite(SkinManager.Load(@"taiko-drum-outer", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                             Clocks.Game, new Vector2(8, TAIKO_BAR_Y + 23), 0.96f, true, Color.TransparentWhite);
                taikoOuterLeft.FlipHorizontal = true;
                hitObjectManager.spriteManager.Add(taikoOuterLeft);

                taikoOuterRight = new pSprite(SkinManager.Load(@"taiko-drum-outer", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                              Clocks.Game, new Vector2(53, TAIKO_BAR_Y + 23), 0.96f, true, Color.TransparentWhite);
                hitObjectManager.spriteManager.Add(taikoOuterRight);
            }

            if (!player.eventManager.HasStoryboard || player.eventManager.spriteManagerBG.TotalSpriteCount + player.eventManager.spriteManagerFG.TotalSpriteCount < 10)
            {
                taikoSlider = new pSprite(SkinManager.Load(@"taiko-slider", SkinSource.Skin | SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                              Clocks.Audio, new Vector2(0, 0), 0f, false, Color.White);
                player.spriteManagerBelowHitObjectsWidescreen.Add(taikoSlider);

                taikoSlider.Scale = 1.4f;

                taikoSlider.Transformations.Add(new Transformation(Vector2.Zero, new Vector2(-1400, 0), -player.leadInTime, -player.leadInTime + 34400));

                taikoSliderActiveCollection.Add(taikoSlider);
            }

            if (ConfigManager.sComboBurst)
                comboBurstSprites = SkinManager.LoadAll(@"taiko-flower-group");

            //Load in advance
            SkinManager.Load(@"taiko-bar-right-glow", SkinSource.Skin | SkinSource.Osu);


            sliderTexturePass = SkinManager.Load(@"taiko-slider", SkinSource.Skin | SkinSource.Osu);
            sliderTextureFail = SkinManager.Load(@"taiko-slider-fail", SkinSource.Skin | SkinSource.Osu);

            base.InitializeSprites();
        }

        internal override void LoadComplete()
        {
            base.LoadComplete();
            if (metadataBarTitle != null)
            {
                metadataBarTitle.FadeInFromZero(200);
                metadataBarTitle.MoveTo(metadataBarTitle.Position + new Vector2(25, 0), 500, EasingTypes.Out);
            }
        }

        internal override void AdjustBackgroundSprite(pSprite backgroundSprite)
        {
            if (ModManager.CheckActive(Mods.Cinema))
                return;

            if (player.eventManager.backgroundEvent == null || player.eventManager.backgroundEvent.Sprite == null)
                return;

            pSprite eventSprite = player.eventManager.backgroundEvent.Sprite;

            backgroundSprite.Position.Y += GameBase.WindowHeightScaled - (GameBase.WindowHeightScaled - (TAIKO_BAR_Y + TAIKO_BAR_HEIGHT + title_height)) / 2f - 240;
            backgroundSprite.OriginPosition.Y = eventSprite.OriginPosition.Y + 15 * (((float)backgroundSprite.Width / backgroundSprite.Height) / 1.333f);
        }

        internal override void IncreaseScoreHit(IncreaseScoreType value, HitObject ho)
        {
            if (value != IncreaseScoreType.Ignore)
                player.UpdatePassing(value != IncreaseScoreType.Miss);

            base.IncreaseScoreHit(value, ho);
        }

        internal override bool IsMilestoneCombo
        {
            get
            {
                return ComboCounter.HitCombo == 50 || (ComboCounter.HitCombo > 99 && ComboCounter.HitCombo % 100 == 0);
            }
        }

        internal override void ComboBurst()
        {
            mascot.SetState(TaikoMascotStates.Clear, true);

            if (comboBurstSprites != null && comboBurstSprites.Length > 0)
            {
                pSprite cb = new pSprite(comboBurstSprites[comboBurstNumber], Origins.BottomCentre, new Vector2(54, TAIKO_BAR_Y + 6), 0.05f, false, Color.White);

                cb.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, GameBase.Time + 0,
                                                          GameBase.Time + 200));
                /*
                                cb.Transformations.Add(new Transformation(TransformationType.Rotation, -1, 0, GameBase.Time + 0,
                                                                          GameBase.Time + 600, EasingTypes.In));
                */
                cb.Transformations.Add(new Transformation(TransformationType.Scale, 0.6f, 1f, GameBase.Time + 0,
                                                          GameBase.Time + 800, EasingTypes.Out));

                cb.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + 1600,
                                                          GameBase.Time + 1900, EasingTypes.In));
                player.spriteManagerBelowHitObjectsWidescreen.Add(cb);

                comboBurstNumber = (comboBurstNumber + 1) % comboBurstSprites.Length;
            }

            comboBurstStars();
        }

        internal override void UpdatePassing(bool value)
        {
            if (value)
                mascot.SetState(Player.KiaiActive ? TaikoMascotStates.Kiai : TaikoMascotStates.Idle, false);
            else
                mascot.SetState(TaikoMascotStates.Fail, false);

            List<pSprite> newSliderCollection = new List<pSprite>();

            taikoSliderActiveCollection.ForEach(s =>
            {
                pSprite clone = s.Clone();

                clone.Depth += 0.0001f;

                const int delay = 200;

                clone.FadeInFromZero(delay);
                s.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, AudioEngine.Time + delay, AudioEngine.Time + delay));

                if (value)
                    clone.Texture = sliderTexturePass;
                else
                    clone.Texture = sliderTextureFail;

                newSliderCollection.Add(clone);
            });

            Debug.Print("Cloned {0} sprites from {1} to state: {2}", newSliderCollection.Count, taikoSliderActiveCollection.Count, value);

            player.spriteManagerBelowHitObjectsWidescreen.Add(newSliderCollection);

            taikoSliderActiveCollection = newSliderCollection;
            if (newSliderCollection.Count > 0)
                taikoSlider = newSliderCollection[newSliderCollection.Count - 1];

            base.UpdatePassing(value);
        }


    }
}