﻿using System;
using System.Collections.Generic;
using System.Text;

namespace osu.GameModes.Play.Rulesets
{
        partial class Ruleset
        {
            internal const double HP_COMBO_GEKI = 14;
            internal const double HP_COMBO_KATU = 10;
            internal const double HP_COMBO_MU = 6;
            internal const double HP_HIT_300 = 6;
            internal const double HP_HIT_100 = 2.2;
            internal const double HP_HIT_50 = 0.4;
            internal const double HP_SLIDER_REPEAT = 4;
            internal const double HP_SLIDER_TICK = 3;

            internal const double HP_BAR_MAXIMUM = 200;
        }
}
