using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.GameModes.Edit;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;
using osu_common;
using osu_common.Helpers;

namespace osu.GameModes.Play
{
    /// <summary>
    /// Player modules that gives extra details and cuts back some functionality for
    /// test mode accessed via the editor.
    /// </summary>
    internal class PlayerTest : Player
    {
        private int lastRender = -1;
        private int testBadTimingCount;
        private double testHitdevianceAverage = 0.0d;
        private double testHitDivisor = 0.0d;
        private pText testInfo;
        private pText testInfo2;
        private pText testInfo3;
        internal int testMaxCombo;
        internal int testMaxScore;
        internal int testTotalHits;

        private static bool wasAutoplay;

        internal static List<int> BookmarksCache;

        public override void Update()
        {
            if (!Loader.Loaded || hitObjectManager.hitObjectsCount == 0)
            {
                UpdateLoading();
                return;
            }

            if (wasAutoplay && !Autoplay)
            {
                Autoplay = true;
                wasAutoplay = false;
            }

            float load = GameBase.PixelsToLoadRatio * (GameBase.TransitionManager.BGPixelsDrawn
                + eventManager.spriteManagerBGWide.PixelsDrawn
                + eventManager.spriteManagerBG.PixelsDrawn
                + eventManager.spriteManagerFG.PixelsDrawn);

            if (GameBase.SixtyFramesPerSecondFrame)
            {
                testInfo.InitialColour = load > 5 ? Color.Red : Color.White;
                testInfo.TextBold = load > 5;
                testInfo.Text = string.Format("Current: {0:00}:{1:00}:{2:00} | SB Load: {3:0.00}x",
                    AudioEngine.Time / 1000 / 60,
                    (AudioEngine.Time % 60000) / 1000,
                    (AudioEngine.Time % 1000) / 10,
                    load);
            }

            if (lastRender == -1 ||
                (hitObjectManager.currentHitObjectIndex >= 0 &&
                 AudioEngine.Time - hitObjectManager.hitObjects[hitObjectManager.currentHitObjectIndex].EndTime > 200 &&
                 AudioEngine.Time / 200 > lastRender))
            {
                lastRender = AudioEngine.Time / 200;
                if (Player.Mode == PlayModes.OsuMania)
                {
                    testInfo2.Text =
                    string.Format(
                        "300g x {0}\n300 x {1}\n200 x {2}\n100 x {3}\n50 x {4}\nMiss x {5}\nMistimed Hits: {6:#,0}% (ave {7}ms {8})",
                        currentScore.countGeki, currentScore.count300, currentScore.countKatu, currentScore.count100,
                        currentScore.count50, currentScore.countMiss,
                        testTotalHits > 0 ? (float)testBadTimingCount / testTotalHits * 100 : 0,
                        (testHitDivisor > 0.0d) ? (int)Math.Abs(testHitdevianceAverage / testHitDivisor) : 0,
                        testHitdevianceAverage < 0 ? LocalisationManager.GetString(OsuString.PlayerTest_Early) : LocalisationManager.GetString(OsuString.PlayerTest_Late));
                }
                else
                {
                    testInfo2.Text =
                    string.Format(
                        "300 x {0}\n100 x {1}\n50 x {2}\nMiss x {3}\n\nMistimed Hits: {4:#,0}% (ave {5}ms {6})",
                        currentScore.count300, currentScore.count100, currentScore.count50, currentScore.countMiss,
                        testTotalHits > 0 ? (float)testBadTimingCount / testTotalHits * 100 : 0,
                        (testHitDivisor > 0.0d) ? (int)Math.Abs(testHitdevianceAverage / testHitDivisor) : 0,
                        testHitdevianceAverage < 0 ? LocalisationManager.GetString(OsuString.PlayerTest_Early) : LocalisationManager.GetString(OsuString.PlayerTest_Late));
                }

            }

            base.Update();
        }

        protected override bool exit()
        {
            GameBase.ChangeMode(OsuModes.Edit);
            return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (GameBase.Mode != OsuModes.Edit && GameBase.Mode != OsuModes.Play)
                GameBase.TestMode = false;

            wasAutoplay = Retrying ? InputManager.ReplayMode : false;
            InputManager.ReplayMode = false;

            base.Dispose(disposing);
        }

        Score autoReplay;

        private bool Autoplay
        {
            get { return InputManager.ReplayMode; }
            set
            {
                if (value == InputManager.ReplayMode) return;

                if (!GameBase.TestMode || AudioEngine.Time >= hitObjectManager.hitObjects[hitObjectManager.hitObjectsCount - 1].EndTime || !Loaded)
                    return;

                InputManager.ReplayMode = value;

                if (InputManager.ReplayMode)
                {
                    InputManager.ReplayScore = autoReplay;
                    while (InputManager.ReplayScore.replay[InputManager.ReplayFrame].time < AudioEngine.Time)
                        InputManager.ReplayFrame++;
                    InputManager.ReplayFrame -= 2;
                    if (InputManager.ReplayFrame < 0) InputManager.ReplayFrame = 0;
                    foreach (HitObject h in hitObjectManager.hitObjects)
                    {
                        if (h.EndTime < AudioEngine.Time)
                            h.IsHit = true;
                        else
                            break;
                    }
                }

                NotificationManager.ShowMessageMassive("Autoplay mod " + (InputManager.ReplayMode ? "enabled" : "disabled"), 2000);
            }
        }

        protected override bool onKeyPressed(object sender, Keys k)
        {
            if (Autoplay && BindingManager.CheckValidPlayKey(k, Mode))
                Autoplay = false;

            if (base.onKeyPressed(sender, k)) return true;

            switch (k)
            {
                case Keys.Tab:
                    Autoplay = !Autoplay;
                    return true;
                case Keys.B:
                    if (KeyboardHandler.ControlPressed)
                    {
                        if (BookmarksCache == null && hitObjectManager.Bookmarks != null)
                            BookmarksCache = hitObjectManager.Bookmarks;
                        else if (BookmarksCache == null)
                            BookmarksCache = new List<int>();
                        BookmarksCache.Add(AudioEngine.Time);
                        NotificationManager.ShowMessageMassive("Added new bookmark!", 1000);
                        return true;
                    }
                    return false;
                case Keys.F1:
                    GameBase.ChangeModeInstant(OsuModes.Edit);
                    return true;
                case Keys.F2:
                    GameBase.TestTime = AudioEngine.Time;
                    GameBase.ChangeModeInstant(OsuModes.Edit);
                    return true;
                case Keys.F3:  //conflic with Player.F3 (debug only)
                    if (!InputManager.ReplayMode)
                    {
                        NotificationManager.ShowMessageMassive("Press [Tab] to active autoplay before changing speed.", 1500);
                        return true;
                    }
                    int rate = AudioEngine.CurrentPlaybackRate;
                    if (rate == 150)
                        rate = 100;
                    else
                        rate = 150;
                    AudioEngine.CurrentPlaybackRate = rate;
                    NotificationManager.ShowMessageMassive(string.Format("Speed set to {0:0.0}x", rate / 100.0), 1000);
                    return true;
                case Keys.P:
                    if (KeyboardHandler.ControlPressed)
                    {
                        AudioEngine.TogglePause();
                        Player.Paused = !Player.Paused;
                        return true;
                    }
                    return false;
            }

            return false;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override bool OnLoadComplete(bool success)
        {
            autoReplay = InputManager.ReplayScore;

            testInfo = new pText("\n", 10, new Vector2(1, 44), Vector2.Zero, 1, true, Color.White, true);
            testInfo.TextAa = false;
            testInfo.TextRenderSpecific = false;
            testInfo.ExactCoordinates = true;
            spriteManager.Add(testInfo);

            int yScale = (int)(testInfo.MeasureText().Y * 1 / GameBase.WindowRatio);

            testInfo2 = new pText(string.Empty, 10, new Vector2(1, 50 + yScale), Vector2.Zero, 1, true, Color.White, true);
            testInfo2.TextAa = false;
            testInfo2.TextRenderSpecific = false;
            testInfo2.ExactCoordinates = true;
            spriteManager.Add(testInfo2);

            testInfo3 = new pText(string.Empty, 10, new Vector2(1, testInfo2.Position.Y + yScale * 9 + 6), Vector2.Zero, 1,
                                  true, new Color(231, 255, 208), true);
            testInfo3.TextAa = false;
            testInfo3.TextRenderSpecific = false;
            testInfo3.ExactCoordinates = true;
            spriteManager.Add(testInfo3);

            int maxScore = testMaxScore;
            if (Player.Mode == PlayModes.OsuMania)
                maxScore = 1000000;
            testInfo3.Text =
                string.Format(
                    "Star Rating: {3:0.00}\nTotal Play Length: {0}s\nMaximum Score: {1:0,0}\nMaximum Combo: {2:0,0}\n\nTab: autoplay\nCtrl-B: bookmark\nCtrl-P: Toggle pause\nF1: quick exit\nF2: exit at current pos\nF3: change autoplay speed",
                    BeatmapManager.Current.DrainLength,
                    maxScore, testMaxCombo, BeatmapManager.Current.StarDisplay);

            ControlPoint cp = AudioEngine.ActiveControlPoint;

            if (cp != null)
                KiaiActive = cp.kiaiMode;

            if (KiaiActive) Ruleset.OnKiaiToggle(KiaiActive);

            if (eventManager != null)
            {
                eventManager.spriteManagerBGWide.EnableProfiling = true;
                eventManager.spriteManagerBG.EnableProfiling = true;
                eventManager.spriteManagerFG.EnableProfiling = true;
            }

            return base.OnLoadComplete(success);
        }

        internal override void OnHitSuccess(HitObject h)
        {
            if (Player.Mode == PlayModes.OsuMania && h.EndTime > h.StartTime)
                return; //skip holds
            int diff = Math.Abs(AudioEngine.Time - h.StartTime);

            if (diff > 20)
                testBadTimingCount++;

            testHitdevianceAverage = 0.95 * testHitdevianceAverage +
                                     0.05 * (AudioEngine.Time - h.StartTime);

            testHitDivisor = 1.0d - ((1.0d - testHitDivisor) * 0.95);
            if (testHitDivisor > 0.99d) testHitDivisor = 1.0d;

            testTotalHits++;

            base.OnHitSuccess(h);
        }

        protected override void InitializeScoreboard()
        {
            //Don't do this for testing.
        }

        protected override void InitializeSpectatorList()
        {
            //Nuh-uh
        }
    }
}
