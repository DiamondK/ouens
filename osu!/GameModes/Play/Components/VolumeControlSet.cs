using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;
using osu.Online.Social;

namespace osu.GameModes.Play.Components
{
    internal class VolumeControlSet : DrawableGameComponent
    {
        VolumeControl controlMaster;
        VolumeControl controlEffect;
        VolumeControl controlMusic;

        List<VolumeControl> controls = new List<VolumeControl>();

        VolumeControl activeControl;

        SpriteManager spriteManager = new SpriteManager(true);

        private void onReceivedHover(object sender, EventArgs e)
        {
            setActive(sender as VolumeControl);
        }

        public VolumeControlSet()
            : base(GameBase.Game)
        {
            controls.Add(controlEffect = new VolumeControl(AudioEngine.VolumeEffect, @"effect volume", new Vector2(-100, 170), 0.8f, false, onReceivedHover));
            controls.Add(controlMaster = new VolumeControl(AudioEngine.VolumeMaster, @"master volume", new Vector2(0, 140), 1, true, onReceivedHover));
            controls.Add(controlMusic = new VolumeControl(AudioEngine.VolumeMusic, @"music volume", new Vector2(100, 170), 0.8f, false, onReceivedHover));

            spriteManager.Add(
                background = new pSprite(SkinManager.Load(@"volume-background-gradient", SkinSource.Osu), Fields.BottomLeft, Origins.BottomLeft, Clocks.Game, Vector2.Zero, 0, true, Color.TransparentWhite));

            controlMaster.VisibleChanged += controlMaster_VisibleChanged;

            activeControl = controlMaster;

            InputManager.Bind(InputEventType.OnMouseWheelUp, onMouseWheelUp, InputTargetType.Volume, BindLifetime.Permanent);
            InputManager.Bind(InputEventType.OnMouseWheelDown, onMouseWheelDown, InputTargetType.Volume, BindLifetime.Permanent);

            InputManager.Bind(InputEventType.OnMouseWheelUp, onMouseWheelUpOverride, InputTargetType.AltOverrides, BindLifetime.Permanent);
            InputManager.Bind(InputEventType.OnMouseWheelDown, onMouseWheelDownOverride, InputTargetType.AltOverrides, BindLifetime.Permanent);
        }

        void controlMaster_VisibleChanged(object sender, EventArgs e)
        {
            if (controlMaster.Visible)
                background.FadeIn(300);
            else
                background.FadeOut(300);
        }

        public override void Draw()
        {
            background.VectorScale = new Vector2(GameBase.WindowWidth * 1.6f, 1);
            spriteManager.Draw();

            controlMaster.Draw();
            controlEffect.Draw();
            controlMusic.Draw();
            base.Draw();
        }

        public override void Update()
        {
            controlMaster.Update();
            controlEffect.Update();
            controlMusic.Update();
            base.Update();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            spriteManager.Dispose();

            controlMaster.Dispose();
            controlEffect.Dispose();
            controlMusic.Dispose();

            InputManager.Unbind(InputEventType.OnMouseWheelDown, onMouseWheelDown, InputTargetType.Volume);
            InputManager.Unbind(InputEventType.OnMouseWheelUp, onMouseWheelUp, InputTargetType.Volume);
        }

        /// <returns>true if controls were already visible</returns>
        private bool showControls()
        {
            bool wasVisible = controlMaster.Visible;

            if (!wasVisible)
                setActive(controlMaster);

            controlMaster.Visible = true;
            controlEffect.Visible = true;
            controlMusic.Visible = true;

            return wasVisible;
        }

        private void setActive(VolumeControl newControl)
        {
            if (activeControl == newControl) return;

            activeControl.Active = false;
            activeControl = newControl;
            activeControl.Active = true;

            showControls();
        }

        private bool onMouseWheelUp(object sender, EventArgs e)
        {
            return adjustCurrent(5);
        }

        private bool onMouseWheelDown(object sender, EventArgs e)
        {
            return adjustCurrent(-5);
        }

        private bool onMouseWheelUpOverride(object sender, EventArgs e)
        {
            if (!KeyboardHandler.AltPressed) return false;

            return adjustCurrent(5);
        }

        private bool onMouseWheelDownOverride(object sender, EventArgs e)
        {
            if (!KeyboardHandler.AltPressed) return false;

            return adjustCurrent(-5);
        }

        private bool adjustCurrent(int adjustAmount)
        {
            if (!ValidInCurrentMode) return false;

            if (Player.Playing)
                controlMaster.AdjustVolume(adjustAmount);
            else if (showControls())
                activeControl.AdjustVolume(adjustAmount);
            return true;
        }

        int consecutive_presses;
        private pSprite background;
        internal bool KeyboardHandler_OnKeyRepeat(object sender, Keys k, bool first)
        {
            if (!ValidInCurrentMode || (!KeyboardHandler.AltPressed && GameBase.Mode == OsuModes.Play && Player.Paused))
                return false;

            if (first)
                consecutive_presses = 0;
            else if (++consecutive_presses < 4)
                return true;

            switch (BindingManager.CheckKey(k))
            {
                case Bindings.VolumeDecrease:
                    adjustCurrent(-5);
                    return true;
                case Bindings.VolumeIncrease:
                    adjustCurrent(5);
                    return true;
            }

            if (controlMaster.Visible)
            {
                switch (k)
                {
                    case Keys.Left:
                        setActive(controls[(controls.Count + controls.FindIndex(a => a == activeControl) - 1) % controls.Count]);
                        return true;
                    case Keys.Right:
                        setActive(controls[(controls.Count + controls.FindIndex(a => a == activeControl) + 1) % controls.Count]);
                        return true;
                }
            }

            return false;
        }

        internal bool ValidInCurrentMode
        {
            get
            {
                //todo: this can be removed once we have finished migrating keyboard input events to the new input groups.
                bool volumeOverride = KeyboardHandler.AltPressed;

                if (!volumeOverride && (ChatEngine.CursorWithinChatBounds || (ChatEngine.IsVisible && ChatEngine.IsFullVisible)))
                    return false;

                switch (GameBase.Mode)
                {
                    case OsuModes.Play:
                        //todo: fix up this logic. it currently affects keyboard controls.
                        if (ConfigManager.sMouseDisableWheel) break;
                        return true;
                    case OsuModes.MatchSetup:
                    case OsuModes.Menu:
                        //some modes can be allowed to always have adjustment.
                        return true;
                }

                return volumeOverride;
            }
        }
    }
}
