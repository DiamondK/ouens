﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play.Rulesets;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;

namespace osu.GameModes.Play.Components
{
    internal class HpBarTaiko : HpBar
    {
        internal HpBarTaiko(SpriteManager spriteManager) : base(spriteManager)
        {
            InitialIncrease = false;

            s_kiIcon.Position = new Vector2(s_barFill.Position.X + s_barFill.Width * GameBase.WindowRatioInverse, s_kiIcon.Position.Y);
            s_kiIcon.Transformations.Add(new Transformation(TransformationType.Scale,0,1,GameBase.Time, GameBase.Time + 1000,EasingTypes.Out));
            s_kiIcon.Transformations.Add(new Transformation(TransformationType.Rotation, -4, 0, GameBase.Time, GameBase.Time + 1000, EasingTypes.Out));
        }

        internal override void  SlideIn()
        {
         	
        }

        internal override void SlideOut()
        {
            
        }

        protected override void setVisibility(bool visible)
        {
            if (!visible)
                gleeSprites.ForEach(s => s.FadeOut(100));
            base.setVisibility(visible);
        }

        internal override void IncreaseCurrentHp(double amount)
        {
            base.IncreaseCurrentHp(amount * 0.06);

            CheckFullBar();
        }

        protected override pTexture loadSpriteFromSource(string p)
        {
            return SkinManager.Load(p, SkinSource.Osu | SkinSource.Skin);
        }

        protected override pTexture[] loadSpritesFromSource(string p)
        {
            return SkinManager.LoadAll(p, SkinSource.Skin | SkinSource.Osu, true);
        }

        protected override void UpdateKiPosition()
        {
            
            //base.UpdateKiPosition();
        }

        bool fullBarGlee = false;
        List<pSprite> gleeSprites = new List<pSprite>();

        private void CheckFullBar()
        {
            if (Player.Relaxing)
                return;
            
            if ((CurrentHp == 200) == fullBarGlee)
                return;
            fullBarGlee = CurrentHp == 200;

            if (fullBarGlee)
            {
                pSprite p = s_barFill.Clone();
                p.Additive = true;
                p.Depth += 0.01f;
                
                Transformation t = new Transformation(TransformationType.Fade,0.2f,1f,GameBase.Time, GameBase.Time + 1000);
                t.Loop = true;
                t.LoopDelay = 1000;
                
                p.Transformations.Add(t);

                t = new Transformation(TransformationType.Fade, 1f,0.12f, GameBase.Time + 1000, GameBase.Time + 2000);
                t.Loop = true;
                t.LoopDelay = 1000;

                p.Transformations.Add(t);

                gleeSprites.Add(p);

                s_kiIcon.Additive = true;

                gleeSprites.Add(p);

                spriteManager.Add(gleeSprites);
            }
            else
            {
                gleeSprites.ForEach(s => { s.FadeOut(50); s.AlwaysDraw = false; });
                gleeSprites.Clear();

                s_kiIcon.Additive = false;
            }

        }

        internal override void ReduceCurrentHp(double amount)
        {
            base.ReduceCurrentHp(amount*0.06);

            CheckFullBar();
        }

        internal override void Reset()
        {
            base.Reset();
            SetCurrentHp(0);
        }
    }
}
