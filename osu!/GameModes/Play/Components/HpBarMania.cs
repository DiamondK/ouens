﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play.Rulesets;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu.GameModes.Play.Rulesets.Mania;
using osu.Helpers;

namespace osu.GameModes.Play.Components
{
    internal class HpBarMania : HpBar
    {
        internal HpBarMania(StageMania stage)
            : base(stage.SpriteManagerBelow)
        {
            s_kiIcon.Bypass = true;
            s_barBg.Rotation = -OsuMathHelper.PiOver2;
            s_barFill.Rotation = -OsuMathHelper.PiOver2;
            s_barBg.Scale = 0.7F;
            s_barFill.Scale = 0.7F;
            s_barBg.VectorScale = new Vector2(1, stage.HeightRatio);
            s_barFill.VectorScale = new Vector2(1, stage.HeightRatio);
            s_barBg.Position = new Vector2(stage.Left + stage.Width + 1, stage.Top + stage.Height);
            if (newDefault)
                s_barFill.Position = new Vector2(stage.Left + stage.Width + 6.6f, stage.Top + 474.8f * stage.HeightRatio);
            else
                s_barFill.Position = new Vector2(stage.Left + stage.Width + 8f, stage.Top + 478 * stage.HeightRatio);
            InitialIncrease = false;

            this.spriteManager.Add(SpriteCollection);
        }

        protected override pTexture[] loadSpritesFromSource(string p)
        {
            return SkinManager.LoadAll(p, SkinSource.Skin | SkinSource.Osu, true);
        }

        protected override pTexture loadSpriteFromSource(string p)
        {
            return SkinManager.Load(p, SkinSource.Skin | SkinSource.Osu);
        }

        //We need to draw hpbar under section pass and other things
        internal override void Draw() { }

        internal override void Fade(float alpha)
        {
            s_barBg.Alpha = s_barFill.Alpha = alpha;
        }

        protected override void UpdateKiPosition() { }

        internal override void KiExplode() { }

        internal override void KiBulge() { }

        internal override void SetKiColour(Color colour) { }

        internal override void SlideIn()
        {
            //    Visible = true;
        }

        internal override void SlideOut()
        {
            //   Visible = false;
        }

        internal override void Reset()
        {
            base.Reset();
            SetCurrentHp(200);
        }
    }
}
