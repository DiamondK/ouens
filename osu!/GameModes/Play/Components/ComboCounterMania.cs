﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu.GameModes.Play.Rulesets.Mania;

namespace osu.GameModes.Play.Components
{
    class ComboCounterMania : ComboCounter
    {
        private Color holdColour;
        private Color breakColour;
        private bool isFading;
        private bool applyEffect = true;
        private List<ComboCounterMania> secondaryComboCounters = new List<ComboCounterMania>();
        private StageMania stage;

        internal override int HitCombo
        {
            get { return base.HitCombo; }
            set
            {
                base.HitCombo = value;
                secondaryComboCounters.ForEach(c => c.HitCombo = HitCombo);
            }
        }

        public ComboCounterMania(SpriteManager spriteManager, StageMania maniaStage, bool primary = true)
            : base(spriteManager, SkinManager.Current.FontCombo)
        {
            this.stage = maniaStage;

            s_hitCombo.Position = new Vector2(maniaStage.Left + maniaStage.Width / 2, maniaStage.ScaleFlipPosition(maniaStage.Skin.ComboPosition));
            s_hitCombo.OriginPosition = Vector2.Zero;
            s_hitCombo.Origin = Origins.Centre;
            if (SkinManager.Current.Version >= 2.4 || maniaStage.HeightRatio < 1f)
                s_hitCombo.Scale = maniaStage.MinimumRatio;
            s_hitCombo.TextConstantSpacing = true;
            if (!maniaStage.Skin.Colours.TryGetValue("ColourHold", out holdColour))
                holdColour = new Color(255, 199, 51);
            if (!maniaStage.Skin.Colours.TryGetValue("ColourBreak", out breakColour))
                breakColour = Color.Red;

            if (!primary)
                return;

            if (maniaStage.SecondaryStage != null)
            {
                foreach (StageMania stage in maniaStage.SecondaryStage)
                    secondaryComboCounters.Add(new ComboCounterMania(spriteManager, stage, false));
            }
        }

        protected override string GetComboString()
        {
            return DisplayCombo.ToString();
        }

        internal override void SlideOut()
        {
            s_hitCombo.FadeOut(500);
            secondaryComboCounters.ForEach(c => c.SlideOut());
        }

        internal override void EnsureVisible()
        {
            base.EnsureVisible();
            secondaryComboCounters.ForEach(c => c.EnsureVisible());
        }

        internal override void SlideIn()
        {
            s_hitCombo.FadeIn(500);
            secondaryComboCounters.ForEach(c => c.SlideIn());
        }

        internal override void Update()
        {
            base.Update();
            if (Player.IsSliding && !isFading)
            {
                isFading = true;
                s_hitCombo.Transformations.Add(new Transformation(Color.White, holdColour, GameBase.Time, GameBase.Time + 300));
            }
            else if (!Player.IsSliding && isFading)
            {
                isFading = false;
                s_hitCombo.Transformations.Add(new Transformation(holdColour, Color.White, GameBase.Time, GameBase.Time + 300));
            }

            secondaryComboCounters.ForEach(c => c.Update());
        }

        protected override void OnIncrease(bool hasChanged)
        {
            applyEffect = true;
            s_hitCombo.Transformations.RemoveAll(tr => tr.TagNumeric == 1);
            Transformation t1 = new Transformation(TransformationType.VectorScale, new Vector2(1f, 1.4f), new Vector2(1, 1), GameBase.Time, GameBase.Time + 300);
            t1.Easing = EasingTypes.Out;
            t1.TagNumeric = 1;
            s_hitCombo.Transformations.Add(t1);
        }

        protected override void OnDecrease()
        {
            if (HitCombo == 0 && applyEffect)
            {
                applyEffect = false;
                pSpriteText s_fade =
                    new pSpriteText(s_hitCombo.Text, SkinManager.Current.FontCombo, SkinManager.Current.FontScoreOverlap,
                                Fields.TopLeft, Origins.Centre, Clocks.Game,
                                Vector2.Zero, 0.915F, true, breakColour);
                s_fade.Position = s_hitCombo.Position;
                s_fade.OriginPosition = Vector2.Zero;
                s_fade.Transformations.Add(new Transformation(TransformationType.Fade, 0.8F, 0, GameBase.Time, GameBase.Time + 200));
                s_fade.Transformations.Add(new Transformation(TransformationType.Scale, 1, 4, GameBase.Time, GameBase.Time + 200));
                spriteManager.Add(s_fade);
            }
        }
    }
}
