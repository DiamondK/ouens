﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Graphics.Sprites;
using osu.Graphics.Skinning;

namespace osu.GameModes.Play.Components
{
    internal class ComboCounterTaiko : ComboCounter
    {
        internal ComboCounterTaiko(SpriteManager spriteManager)
            : base(spriteManager)
        {
            if (SkinManager.UseNewLayout)
            {
                s_hitCombo.Position = new Vector2(56, Rulesets.Taiko.RulesetTaiko.TAIKO_BAR_Y + 76);
                s_hitCombo.InitialPosition = new Vector2(56, Rulesets.Taiko.RulesetTaiko.TAIKO_BAR_Y + 76);
                s_hitCombo.OriginPosition = Vector2.Zero;
                s_hitCombo2.Position = new Vector2(56, Rulesets.Taiko.RulesetTaiko.TAIKO_BAR_Y + 76);
                s_hitCombo2.OriginPosition = Vector2.Zero;
                s_hitCombo.TextConstantSpacing = true;
            }

            else
            {
                s_hitCombo.Position = new Vector2(53, Rulesets.Taiko.RulesetTaiko.TAIKO_BAR_Y + 76);
                s_hitCombo.InitialPosition = new Vector2(53, Rulesets.Taiko.RulesetTaiko.TAIKO_BAR_Y + 76);
                s_hitCombo.OriginPosition = Vector2.Zero;
                s_hitCombo2.Position = new Vector2(53, Rulesets.Taiko.RulesetTaiko.TAIKO_BAR_Y + 76);
                s_hitCombo2.OriginPosition = Vector2.Zero;
            }

            s_hitCombo.Scale = 0.8f;
            s_hitCombo2.Scale = 0.8f;

            s_hitCombo.Origin = Origins.BottomCentre;
        }

        protected override string GetComboString()
        {
            return DisplayCombo.ToString();
        }

        internal override void SlideIn() { }

        internal override void SlideOut() { }

        protected override void OnIncrease(bool hasChanged)
        {
            s_hitCombo.Transformations.RemoveAll(tr => tr.TagNumeric == 1);
            Transformation t1 = new Transformation(TransformationType.VectorScale, new Vector2(1f, 1.4f), new Vector2(1, 1), GameBase.Time, GameBase.Time + 300);
            t1.Easing = EasingTypes.Out;
            t1.TagNumeric = 1;
            s_hitCombo.Transformations.Add(t1);
        }
    }
}
