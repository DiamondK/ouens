using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameplayElements;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu_common.Bancho.Objects;

namespace osu.GameModes.Play.Components
{
    internal class ScoreboardEntry : IComparable<ScoreboardEntry>
    {
        protected readonly pText spriteName;
        private static int creationCount = 0;
        private int creationOrder;
        internal string name;
        internal Vector2 position;
        internal int rank;
        internal int score;
        internal pSprite spriteBackground;
        internal List<pSprite> SpriteCollection;
        internal pText spriteInfo;
        internal pText spriteRank;
        internal PlayModes playMode;
        internal Mods mods;
        internal bool visible;
        internal MatchScoringTypes scoreType;
        private SlotTeams team;
        internal SlotTeams Team
        {
            get { return team; }
            set
            {
                team = value;
                updateBackgroundOrigin();
            }
        }

        private bool? alignedToRight;
        internal bool AlignedToRight
        {
            get
            {
                return alignedToRight ?? team == SlotTeams.Red;
            }

            set
            {
                alignedToRight = value;
                updateBackgroundOrigin();
            }
        }

        internal Score Score;

        /// <summary>
        /// Score will display current combo and update based on HP colour if false.
        /// </summary>
        internal bool FixedScore;

        private void updateBackgroundOrigin()
        {
            if (AlignedToRight)
                spriteBackground.OriginPosition = new Vector2(90, 20);
            else
                spriteBackground.OriginPosition = new Vector2(460, 20);
        }


        internal ScoreboardEntry(int rank, Score s, PlayModes playMode)
            : this(rank, s.playerName, playMode)
        {
            FixedScore = true;
            SetScore(s);
        }

        internal ScoreboardEntry(int rank, string name, PlayModes playMode)
        {
            this.rank = rank;
            this.playMode = playMode;
            this.name = name ?? @"-";
            creationOrder = creationCount++;

            SpriteCollection = new List<pSprite>();

            float fdepth = 0.92f;

            spriteBackground = new pSprite(SkinManager.Load(@"menu-button-background"), Fields.TopLeft,
                                           Origins.TopLeft,
                                           Clocks.Game,
                                           position, fdepth, true,
                                           new Color(0, 0, 0, 100));
            spriteBackground.Alpha = 0;
            spriteBackground.Scale = 0.6f;
            updateBackgroundOrigin();
            SpriteCollection.Add(spriteBackground);

            spriteName = new pText(name, 14, position + new Vector2(4, 1), Vector2.Zero, fdepth + 0.000003f, true,
                                   new Color(255, 255, 255, 0), true);
            SpriteCollection.Add(spriteName);

            spriteInfo = new pText(string.Empty, 10, position + new Vector2(4, 15), new Vector2(150, 33), fdepth + 0.000003f,
                                   true,
                                   new Color(255, 255, 255, 0), true);
            spriteInfo.TagNumeric = 2;
            SpriteCollection.Add(spriteInfo);

            spriteRank =
                new pText(rank > 0 ? rank.ToString() : @"--", 30, position + new Vector2(90, 0), Vector2.Zero, fdepth + 0.000002f, true,
                          new Color(255, 255, 255, 120), false);
            spriteRank.Alpha = 0;
            spriteRank.Origin = Origins.TopRight;
            SpriteCollection.Add(spriteRank);
        }

        #region IComparable<ScoreboardEntry> Members

        public int CompareTo(ScoreboardEntry other)
        {
            // list sort sometimes invokes compareTo of an object with itself
            if (ReferenceEquals(other, this))
                return 0;

            if (team != other.team)
                return team.CompareTo(other.team);

            if (Passing != other.Passing)
                return Passing ? -1 : 1;
            int i = 0;

            if (Score != null && other.Score != null)
            {
                switch (scoreType)
                {
                    case MatchScoringTypes.Accuracy:
                        i = other.Score.accuracy.CompareTo(Score.accuracy);
                        break;
                    case MatchScoringTypes.Score:
                        i = other.score.CompareTo(score);
                        break;
                    case MatchScoringTypes.Combo:
                        i = other.Score.currentCombo.CompareTo(Score.currentCombo);
                        break;
                }
            }
            else
                i = other.score.CompareTo(score);
            if (i != 0) return i;
            i = name.CompareTo(other.name);

            return i != 0 ? i : (creationOrder > other.creationOrder ? 1 : -1);
        }

        #endregion

        bool NameLengthFixed;
        internal bool Passing = true;
        internal bool allowUpdateRank = true;
        internal Scoreboard scoreboard;

        internal void MoveTo(Vector2 destination, bool visible, bool instant)
        {
            int duration = instant ? 0 : 600;
            Vector2 moveoffset = destination - SpriteCollection[0].Position;
            foreach (pSprite p in SpriteCollection)
            {
                if (this.visible != visible)
                {
                    if (visible)
                        p.FadeTo(alpha, duration, EasingTypes.None);
                    else
                        p.FadeOut(duration);
                }
                if (instant)
                {
                    p.Transformations.RemoveAll(t => t.Type == TransformationType.Movement);
                    p.Position += moveoffset;
                }
                else
                    p.MoveToRelative(moveoffset, duration, EasingTypes.Out);
            }

            if (visible && !NameLengthFixed)
            {
                bool doDots = false;
                while (spriteName.MeasureText().X > 90)
                {
                    spriteName.Text = spriteName.Text.Remove(spriteName.Text.Length - 1);
                    doDots = true;
                }
                if (doDots) spriteName.Text += @"...";

                NameLengthFixed = true;
            }

            this.visible = visible;
        }

        internal virtual void SetScore(Score score)
        {
            Score = score;

            this.score = score.totalScore;
            switch (scoreType)
            {
                case MatchScoringTypes.Accuracy:
                    spriteInfo.Text = score.totalScore > 0 ? string.Format(@"{0:n}%", score.accuracy * 100) : @"-";
                    break;
                case MatchScoringTypes.Score:
                    spriteInfo.Text = score.totalScore > 0 ? score.totalScore.ToString(@"0,0", GameBase.nfi) : @"-";
                    break;
                case MatchScoringTypes.Combo:
                    spriteInfo.Text = score.currentCombo > 0 ? string.Format(@"{0}x", score.currentCombo) : @"-";
                    break;
            }

            spriteInfo.Text += @" (" + (FixedScore ? score.maxCombo : score.currentCombo) + @"x)";
            if (!FixedScore)
            {
                Color col = spriteBackground.InitialColour;

                if (score.pass)
                {
                    if (score.currentHp > 0)
                        col = new Color((byte)(200 - score.currentHp), 20, (byte)score.currentHp, spriteBackground.InitialColour.A);
                }
                else
                    col = new Color(80, 80, 80, spriteBackground.InitialColour.A);

                spriteBackground.FadeColour(col, 50);
            }
        }

        internal virtual void UpdateRank(string rank)
        {
            if (!allowUpdateRank) return;
            spriteRank.Text = rank;
        }

        pSprite lastHitSprite;
        public bScoreFrame Frame;
        internal float alpha = 1;

        internal void HandleUpdate(bScoreFrame frame)
        {
            GameBase.Scheduler.Add(delegate { HandleUpdateUnsafe(frame); });
        }

        internal void HandleUpdateUnsafe(bScoreFrame frame)
        {
            Score newScore = ScoreFactory.Create(playMode, frame, null);
            newScore.enabledMods = mods;
            Frame = frame;

            Score oldScore = Score;
            SetScore(newScore);

            Passing = frame.pass;

            if (oldScore != null && newScore.totalHits != oldScore.totalHits)
            {
                string value = @"0";
                bool mania = playMode == PlayModes.OsuMania;

                if (mania)
                {
                    if (newScore.count300 > oldScore.count300)
                        value = @"300";
                    else if (newScore.count100 > oldScore.count100)
                        value = @"100";
                    else if (newScore.count50 > oldScore.count50)
                        value = @"50";
                    else if (newScore.countGeki > oldScore.countGeki)
                        value = @"300g";
                    else if (newScore.countKatu > oldScore.countKatu)
                        value = @"200";
                }
                else
                {
                    if (newScore.count300 > oldScore.count300)
                        value = @"300";
                    else if (newScore.count100 > oldScore.count100)
                        value = @"100";
                    else if (newScore.count50 > oldScore.count50)
                        value = @"50";
                    if (newScore.countGeki > oldScore.countGeki)
                        value += @"g";
                    if (newScore.countKatu > oldScore.countKatu)
                        value += @"k";
                }


                int displayTime = newScore.currentCombo < oldScore.currentCombo ? 1000 : 200;

                if (newScore.currentCombo == 0 && oldScore.currentCombo == 0)
                    return; //Don't draw consecutive misses.

                bool miss = value == @"0";

                if (!miss && lastHitSprite != null && lastHitSprite.Transformations.Count > 0 && lastHitSprite.Transformations[0].Time1 == GameBase.Time)
                    return; //Bundled sprites don't need to be drawn unless they are a miss.

                if (lastHitSprite != null && lastHitSprite.TagNumeric == 0)
                {
                    lastHitSprite.Depth -= 0.0001f;

                    lastHitSprite.FadeOut(100); //Fade out any non-miss previously being displayed.

                    scoreboard.spriteManager.Remove(lastHitSprite);
                    scoreboard.spriteManager.Add(lastHitSprite);
                }

                pTexture texture;
                if (mania)
                    texture = SkinManager.LoadAll("mania-hit" + value)[0];
                else
                    texture = SkinManager.Load("hit" + value);
                pSprite p = new pSprite(texture, Origins.Centre, spriteBackground.Position + (AlignedToRight ? new Vector2(-10, 14) : new Vector2(110, 14)), (miss ? 0.95f : 0.949f) - (rank * 0.001f), false, Color.White);
                p.TagNumeric = miss ? 1 : 0;

                lastHitSprite = p;

                if (value == @"0")
                {
                    p.Transformations.Add(
                        new Transformation(TransformationType.Fade, 0, 1, GameBase.Time,
                                           GameBase.Time + HitObjectManager.HitFadeIn));

                    p.Transformations.Add(
                        new Transformation(TransformationType.Scale, 0.6F, 0.4F, GameBase.Time, (int)(GameBase.Time + (HitObjectManager.HitFadeIn * 1.4))));

                    p.Transformations.Add(
                        new Transformation(TransformationType.Fade, 1, 0,
                                           GameBase.Time + HitObjectManager.PostEmpt / 2, GameBase.Time + HitObjectManager.PostEmpt + HitObjectManager.HitFadeOut));
                }
                else
                {
                    p.Transformations.Add(
                        new Transformation(TransformationType.Fade, 0, 1, GameBase.Time,
                                           GameBase.Time + HitObjectManager.HitFadeIn));

                    p.Transformations.Add(
                new Transformation(TransformationType.Scale, 0.2F, 0.4F, GameBase.Time,
                                   (int)(GameBase.Time + (HitObjectManager.HitFadeIn * 0.8))));



                    p.Transformations.Add(
                        new Transformation(TransformationType.Scale, 0.4F, 0.3F, GameBase.Time + HitObjectManager.HitFadeIn,
                                           (int)(GameBase.Time + (HitObjectManager.HitFadeIn * 1.2))));
                    p.Transformations.Add(
                        new Transformation(TransformationType.Scale, 0.3F, 0.35F, GameBase.Time + HitObjectManager.HitFadeIn,
                                           (int)(GameBase.Time + (HitObjectManager.HitFadeIn * 1.4))));

                    p.Transformations.Add(
                        new Transformation(TransformationType.Fade, 1, 0,
                                           GameBase.Time + HitObjectManager.PostEmpt, GameBase.Time + HitObjectManager.PostEmpt / 2 + HitObjectManager.HitFadeOut));
                }

                scoreboard.spriteManager.Add(p);
            }
        }

        internal void Hide()
        {
            foreach (pSprite p in SpriteCollection)
                p.FadeOut(0);

            spriteBackground.FadeOut(0);
            spriteInfo.FadeOut(0);
            spriteName.FadeOut(0);
            spriteRank.FadeOut(0);
        }
    }
}