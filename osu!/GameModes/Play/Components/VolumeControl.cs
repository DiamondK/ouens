﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input.Handlers;
using osu.Online;
using osu.Input;
using osu.Online.Social;
using osu.Helpers;

namespace osu.GameModes.Play.Components
{
    internal class VolumeControl : DrawableGameComponent
    {
        private readonly pSprite background;
        private readonly pSprite foreground;
        private readonly pSprite foreground2;

        Color colourActive = new Color(102, 68, 204);
        Color colourInactive = new Color(68, 17, 136);

        private pText percentageDisplay;

        SpriteManager spriteManager = new SpriteManager(true) { HandleOverlayInput = true };
        SpriteManager spriteManagerFg = new SpriteManager(true) { HandleOverlayInput = true };
        SpriteManager spriteManagerText = new SpriteManager(true) { HandleOverlayInput = true };

        CircularProgress progress = new CircularProgress();

        float speedModifier { get { return Player.Playing ? 10 : 1; } }

        private int lastVisibleTime;

        float activeScaleAdjust = 0;
        bool active;
        internal bool Active
        {
            get
            {
                return active;
            }

            set
            {
                if (value == active && activeScaleAdjust != 0) return;

                active = value;

                if (active)
                {
                    background.FadeColour(colourActive, 100);
                    foreground.FadeColour(colourActive, 100);
                    activeScaleAdjust = 1;
                }
                else
                {
                    background.FadeColour(colourInactive, 100);
                    foreground.FadeColour(colourInactive, 100);
                    activeScaleAdjust = 0.95f;
                }

                if (Visible)
                {
                    spriteManager.SpriteList.ForEach(s => { s.ScaleTo(1 * activeScaleAdjust * Scale, 600, EasingTypes.OutElastic); });
                    spriteManagerFg.SpriteList.ForEach(s => { s.ScaleTo(1 * activeScaleAdjust * Scale, 600, EasingTypes.OutElastic); });
                }
            }
        }

        BindableInt volume;
        event EventHandler receivedHover;
        float Scale;

        internal VolumeControl(BindableInt volume, string text, Vector2 pos, float scale, bool active, EventHandler onReceivedHover)
            : base(GameBase.Game)
        {
            Visible = false;

            this.volume = volume;

            receivedHover += onReceivedHover;
            Scale = scale;

            background = new pSprite(SkinManager.Load(@"volume-circle-bg", SkinSource.Osu), Fields.Centre, Origins.Centre, Clocks.Game, pos, 0.97f, true, colourInactive);
            background.HandleInput = true;
            background.OnHover += background_OnHover;
            spriteManager.Add(background);

            foreground = new pSprite(SkinManager.Load(@"volume-circle-fg", SkinSource.Osu), Fields.Centre, Origins.Centre, Clocks.Game, pos, 0.98f, true, colourInactive);
            spriteManagerFg.Add(foreground);

            foreground2 = new pSprite(SkinManager.Load(@"volume-circle-fg2", SkinSource.Osu), Fields.Centre, Origins.Centre, Clocks.Game, pos, 0.99f, true, Color.White);
            spriteManagerFg.Add(foreground2);

            percentageDisplay = new pText(string.Empty, 14, pos, 0.991f, true, Color.White)
            {
                Field = Fields.Centre,
                Origin = Origins.Centre
            };
            spriteManagerFg.Add(percentageDisplay);

            pText textSprite = new pText(text, 12, new Vector2(pos.X, 220), 0.991f, true, Color.White)
            {
                Field = Fields.Centre,
                Origin = Origins.Centre
            };
            spriteManagerText.Add(textSprite);

            progress.Colour = Color.White;
            progress.StartOffset = 1;

            spriteManagerFg.SpriteList.ForEach(s => s.Alpha = 0);
            spriteManager.SpriteList.ForEach(s => s.Alpha = 0);
            spriteManagerText.SpriteList.ForEach(s => s.Alpha = 0);

            Active = active;
        }

        void background_OnHover(object sender, EventArgs e)
        {
            if (receivedHover != null) receivedHover(this, null);
        }

        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
                if (value) lastVisibleTime = GameBase.Time;

                if (value == base.Visible)
                    return;

                base.Visible = value;

                if (value)
                {
                    spriteManager.SpriteList.ForEach(s =>
                    {
                        s.FadeIn((int)(80 / speedModifier));
                        s.ScaleTo(1 * activeScaleAdjust * Scale, 600, EasingTypes.OutElastic);
                    });

                    spriteManagerFg.SpriteList.ForEach(s =>
                    {
                        s.FadeIn((int)(150 / speedModifier));
                        s.ScaleTo(1 * activeScaleAdjust * Scale, 600, EasingTypes.OutElastic);
                    });

                    spriteManagerText.SpriteList.ForEach(s => { s.FadeIn(250); });

                    progress.Progress = 0;

                }
                else
                {
                    spriteManager.SpriteList.ForEach(s =>
                    {
                        s.FadeOut(200);
                        s.ScaleTo(0.6f * activeScaleAdjust * Scale, 1000, EasingTypes.Out);
                    });

                    spriteManagerFg.SpriteList.ForEach(s =>
                    {
                        s.FadeOut(50);
                        s.ScaleTo(0.6f * activeScaleAdjust * Scale, 1000, EasingTypes.Out);
                    });

                    spriteManagerText.SpriteList.ForEach(s => { s.FadeOut(250); });

                    progress.Progress = 0;
                }
            }
        }

        public override void Update()
        {
            background.HandleInput = !Player.Playing;

            float hideDelay = (InputManager.CursorPosition.Y > GameBase.WindowHeight / 3 * 2 ? 1600 : 800) / speedModifier;

            if (Visible && GameBase.Time - lastVisibleTime > hideDelay)
                Visible = false;

            base.Update();
        }

        public override void Draw()
        {
            if (Visible)
            {
                if (foreground.Alpha > 0.98f)
                {
                    float aimProgress = volume.Value / 100f;
                    if (progress.Progress != aimProgress)
                        progress.Progress += (aimProgress - progress.Progress) / 100 * (float)GameBase.ElapsedMilliseconds * speedModifier;
                    if (GameBase.SixtyFramesPerSecondFrame)
                        percentageDisplay.Text = Math.Round(progress.Progress * 100) + @"%";
                }
            }
            else
            {
                if (percentageDisplay.Alpha == 0)
                    percentageDisplay.Text = string.Empty;
            }

            spriteManager.Draw();

            progress.Alpha = (float)Math.Pow(foreground.Alpha, 8);
            if (progress.Alpha > 0)
            {
                progress.Position = new Vector2(GameBase.WindowWidthScaled / 2, GameBase.WindowHeightScaled / 2) + background.Position;
                progress.Radius = 40.2f * background.Scale;
                progress.Draw();
            }

            spriteManagerFg.Draw();

            spriteManagerText.Draw();

            base.Draw();
        }

        internal void Dispose()
        {
            spriteManager.Dispose();
            spriteManagerFg.Dispose();
            spriteManagerText.Dispose();
        }

        internal void AdjustVolume(int adjust)
        {
            Visible = true;
            int volumeBefore = volume.Value;

            volume.Value += adjust;

            AudioEngine.Click(null, @"sliderbar", 1.1f - (0.5f - (volume.Value / 100f)) * 0.15f * (volume.Value != volumeBefore ? 1 : 4));
            return;
        }
    }
}