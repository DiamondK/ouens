﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Sprites;
using osu.Helpers;

namespace osu.GameModes.Play.Components
{
    internal class ProgressBar : IDisposable
    {
        private const int height = 3;
        private readonly float length;
        private readonly pSprite progressBarBackground;
        private readonly pSprite progressBarForeground;
        internal Vector2 Position;
        protected readonly SpriteManager spriteManager;

        internal ProgressBar(SpriteManager spriteManager, ProgressBarTypes type, float topRightVerticalOffset = 34)
        {
            this.spriteManager = spriteManager;
            Fields field;
            switch (type)
            {
                case ProgressBarTypes.Bottom:
                    Position = new Vector2(0, 477);
                    length = GameBase.WindowWidth/GameBase.WindowRatio;
                    field = Fields.TopLeft;
                    break;
                case ProgressBarTypes.TopRight:
                default:
                    Position = new Vector2(178, topRightVerticalOffset - 2);
                    length = 170;
                    field = Fields.TopRight;
                    break;
                case ProgressBarTypes.BottomRight:
                    Position = new Vector2(118, 460);
                    length = 118;
                    field = Fields.TopRight;
                    break;
                case ProgressBarTypes.Pie:
                    return; //Extended class handles this fully.
            }

            progressBarBackground = new pSprite(GameBase.WhitePixel, field, Origins.TopLeft,
                                                Clocks.Game,
                                                Position, 0.938f, true,
                                                new Color(255, 255, 255, 20));
            progressBarBackground.Scale = 1.6f;
            progressBarBackground.VectorScale = new Vector2(length, height);
            progressBarBackground.Additive = true;

            spriteManager.Add(progressBarBackground);


            progressBarForeground = new pSprite(GameBase.WhitePixel, field, Origins.TopLeft,
                                                Clocks.Game,
                                                Position, 0.94f, true,
                                                new Color(255, 255, 128, 128));
            progressBarForeground.Scale = 1.6f;
            progressBarForeground.VectorScale = new Vector2(0, height);
            progressBarForeground.Additive = true;

            spriteManager.Add(progressBarForeground);
        }

        internal virtual void SetProgress(float progress)
        {
            progressBarForeground.VectorScale = new Vector2(OsuMathHelper.Clamp(progress, 0, 1)*length, height);
        }

        public virtual void Dispose()
        {

        }

        internal virtual void Draw()
        {

        }
    }

    internal enum ProgressBarTypes
    {
        Off,
        Pie,
        TopRight,
        BottomRight,
        Bottom
    }
}