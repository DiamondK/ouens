using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Play.Rulesets;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;

namespace osu.GameModes.Play.Components
{
    class ScoreDisplay
    {
        protected readonly SpriteManager spriteManager;
        protected readonly pSpriteText s_Score;
        protected readonly pSpriteText s_Accuracy;
        private int displayScore;
        private double displayAccuracy;
        internal int currentScore;
        internal double currentAccuracy;
        internal Vector2 leftOfDisplay;
        protected Vector2 textMeasure;
        protected float scale;

        internal float ScoreFontHeight { get { return s_Score == null ? 0 : s_Score.DrawHeight / 1.6f; } }

        internal ScoreDisplay(SpriteManager spriteManager)
            : this(spriteManager, new Vector2(6, 0), true, 0.96f, true, true)
        {
        }

        internal ScoreDisplay(SpriteManager spriteManager, Vector2 position, bool alignRight, float scale, bool showScore, bool showAccuracy)
        {
            this.spriteManager = spriteManager;

            this.scale = scale;
            if (GameBase.Tournament) scale *= 1.4f;

            float vpos = position.Y;

            textMeasure = Vector2.Zero;

            if (showScore)
            {
                s_Score =
                    new pSpriteText(@"00000000", SkinManager.Current.FontScore, SkinManager.Current.FontScoreOverlap,
                        alignRight ? Fields.TopRight : Fields.TopLeft, alignRight ? Origins.TopRight : Origins.TopLeft, Clocks.Game,
                        new Vector2(0, 0), 0.95F, true, Color.White);
                s_Score.TextConstantSpacing = true;
                textMeasure = s_Score.MeasureText() * 0.625f * scale;
                s_Score.Position = new Vector2(position.X, vpos);
                s_Score.Scale = scale;

                vpos += textMeasure.Y;
            }

            if (SkinManager.UseNewLayout)
                vpos += 3;

            if (showAccuracy)
            {
                s_Accuracy =
                        new pSpriteText(@"00.00%", SkinManager.Current.FontScore, SkinManager.Current.FontScoreOverlap,
                            alignRight ? Fields.TopRight : Fields.TopLeft, alignRight ? Origins.TopRight : Origins.TopLeft, Clocks.Game,
                            new Vector2(0, 0), 0.95F, true, Color.White);
                s_Accuracy.Scale = scale * (showScore ? 0.6f : 1);
                s_Accuracy.TextConstantSpacing = true;
                s_Accuracy.Position = new Vector2(position.X, vpos);

                leftOfDisplay =
                s_Accuracy.Position +
                                new Vector2(s_Accuracy.MeasureText().X * 0.625f * s_Accuracy.Scale + 24,
                                    s_Accuracy.MeasureText().Y * 0.625f * s_Accuracy.Scale / 2);
            }


            if ((!Player.Relaxing && !Player.Relaxing2))
            {
                spriteManager.Add(s_Score);
                spriteManager.Add(s_Accuracy);
            }
        }

        internal void Draw()
        {
            if (GameBase.SixtyFramesPerSecondFrame)
            {
                if (s_Accuracy != null)
                {
                    if (displayAccuracy - currentAccuracy <= -0.005)
                    {
                        displayAccuracy = Math.Round(displayAccuracy + Math.Max(0.01, (currentAccuracy - displayAccuracy) / 2), 2);
                        s_Accuracy.Text = String.Format(@"{0:00.00}%", displayAccuracy);
                    }
                    else if (displayAccuracy - currentAccuracy >= 0.005)
                    {
                        displayAccuracy = Math.Round(displayAccuracy - Math.Max(0.01, (displayAccuracy - currentAccuracy) / 2), 2);
                        s_Accuracy.Text = String.Format(@"{0:00.00}%", displayAccuracy);
                    }
                }

                if (s_Score != null)
                {
                    if (displayScore != currentScore)
                    {
                        if (displayScore < currentScore)
                            displayScore += Math.Max(1, (currentScore - displayScore) / 4);
                        else
                            displayScore -= Math.Max(1, (displayScore - currentScore) / 4);

                        s_Score.Text = String.Format(@"{0:00000000}", displayScore);
                    }
                }
            }
        }

        internal virtual void Update(int score)
        {
            currentScore = score;
        }

        internal void Update(float accuracy)
        {
            currentAccuracy = Math.Round(accuracy, 2);
        }

        internal void Hide()
        {
            if (s_Score != null)
                s_Score.FadeOut(0);
            if (s_Accuracy != null)
                s_Accuracy.FadeOut(0);

        }
    }
}
