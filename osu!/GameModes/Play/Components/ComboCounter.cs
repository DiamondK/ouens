﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;

namespace osu.GameModes.Play.Components
{
    class ComboCounter
    {
        protected readonly SpriteManager spriteManager;
        internal pSpriteText s_hitCombo;
        internal pSpriteText s_hitCombo2;
        internal int DisplayCombo;
        internal virtual int HitCombo { get; set; }
        internal int HitComboLast;
        private string font;

        public ComboCounter(SpriteManager spriteManager, string font = "")
        {
            this.spriteManager = spriteManager;
            this.font = font != "" ? font : SkinManager.Current.FontCombo;
            InitializeSprites();
        }

        protected virtual void InitializeSprites()
        {
            s_hitCombo =
                new pSpriteText(GetComboString(), font, SkinManager.Current.FontComboOverlap,
                                Fields.TopLeft, Origins.Custom, Clocks.Game,
                                new Vector2(2, 472), 0.92F, true, Color.TransparentWhite);
            s_hitCombo.OriginPosition = new Vector2(0, s_hitCombo.MeasureText().Y * 0.625f + 9);
            s_hitCombo.Scale = 1.28F;

            s_hitCombo2 =
                new pSpriteText("0x", font, SkinManager.Current.FontComboOverlap,
                                Fields.TopLeft, Origins.Custom, Clocks.Game,
                                new Vector2(2, 472), 0.91F, true, Color.TransparentWhite);
            s_hitCombo2.OriginPosition = new Vector2(3, s_hitCombo2.MeasureText().Y * 0.625f + 9);
            s_hitCombo2.Additive = true;
            s_hitCombo2.Scale = 1.28F;

            if ((!Player.Relaxing && !Player.Relaxing2) || Player.Mode == PlayModes.Taiko)
            {
                spriteManager.Add(s_hitCombo);
                spriteManager.Add(s_hitCombo2);
            }
        }

        protected virtual string GetComboString()
        {
            return DisplayCombo + "x";
        }

        internal virtual void SlideOut()
        {
            s_hitCombo.FadeOut(1000);
            s_hitCombo.MoveTo(s_hitCombo.InitialPosition - new Vector2(80, 0), 1000, EasingTypes.In);
        }

        internal virtual void SlideIn()
        {
            s_hitCombo.FadeIn(1000);
            s_hitCombo.MoveTo(s_hitCombo.InitialPosition, 1000, EasingTypes.Out);
        }

        internal virtual void EnsureVisible()
        {
            if (DisplayCombo != 0 && s_hitCombo.Alpha == 0)
                s_hitCombo.FadeIn(120);
            else if (DisplayCombo == 0 && HitCombo == 0 && s_hitCombo.Alpha == 1)
                s_hitCombo.FadeOut(120);
        }

        internal void Reset()
        {
            HitCombo = 0;
            DisplayCombo = 0;
        }

        internal virtual void Update()
        {
            if (HitCombo < DisplayCombo)
            {
                //Decrease
                if (AudioEngine.IsReversed || HitCombo == 0)
                {
                    if (GameBase.SixtyFramesPerSecondFrame) DisplayCombo--;
                }
                else
                    DisplayCombo = HitCombo;

                OnDecrease();
            }
            else if (HitCombo > DisplayCombo)
            {
                //Increase
                DisplayCombo++;
                bool hasChanged = s_hitCombo.Text != s_hitCombo2.Text;
                s_hitCombo.Text = s_hitCombo2.Text;

                OnIncrease(hasChanged);
            }

            s_hitCombo2.Text = GetComboString();

            if (s_hitCombo2.Transformations.Count > 0)
            {
                if (s_hitCombo2.Transformations[0].Time2 < GameBase.Time + 140 && s_hitCombo.Text != s_hitCombo2.Text)
                    transferToMainCounter();
            }
            else if (s_hitCombo.Text != s_hitCombo2.Text)
                s_hitCombo.Text = s_hitCombo2.Text;
        }

        private void transferToMainCounter()
        {
            s_hitCombo.Text = s_hitCombo2.Text;

            s_hitCombo.Transformations.RemoveAll(tr => tr.Type == TransformationType.Scale);
            Transformation t1 = new Transformation(TransformationType.Scale, 1.28F, 1.4F, GameBase.Time, GameBase.Time + 50);
            t1.Easing = EasingTypes.In;
            s_hitCombo.Transformations.Add(t1);
            t1 = new Transformation(TransformationType.Scale, 1.4f, 1.28F, GameBase.Time + 50, GameBase.Time + 100);
            t1.Easing = EasingTypes.Out;
            s_hitCombo.Transformations.Add(t1);
        }

        protected virtual void OnDecrease() { }

        protected virtual void OnIncrease(bool hasChanged)
        {
            if (hasChanged)
                transferToMainCounter();

            s_hitCombo2.Transformations.RemoveAll(tr => tr.TagNumeric == 1);
            s_hitCombo2.Transformations.Add(new Transformation(TransformationType.Scale, 2F, 1.28F, GameBase.Time, GameBase.Time + 300) { TagNumeric = 1 });
            s_hitCombo2.Transformations.Add(new Transformation(TransformationType.Fade, 0.6F, 0, GameBase.Time, GameBase.Time + 300) { TagNumeric = 1 });
        }

        internal void SetCombo(int newCombo)
        {
            if (newCombo > 0 && Math.Abs(HitCombo - newCombo) > 10)
                DisplayCombo = newCombo;
            HitCombo = newCombo;
        }
    }
}
