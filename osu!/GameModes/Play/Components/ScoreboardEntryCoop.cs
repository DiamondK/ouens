﻿using osu.GameplayElements.Scoring;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho.Objects;

namespace osu.GameModes.Play.Components
{
    internal class ScoreboardEntryCoop : ScoreboardEntryExtended
    {
        internal ScoreboardEntryCoop(int rank, User user, MatchScoringTypes scoreType, PlayModes playMode,
                                     SlotTeams team) : base(rank, user, scoreType, playMode, team)
        {
            spriteRank.Text = string.Empty;
        }

        internal override void UpdateRank(string rank)
        {
            return;
        }

        internal override void SetScore(Score score)
        {
            Score = score;
            this.score = score.totalScore;
        }
    }
}