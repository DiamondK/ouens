﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;

namespace osu.GameModes.Play.Components
{
    internal class ComboCounterFruits : ComboCounter
    {
        internal ComboCounterFruits(SpriteManager spriteManager)
            : base(spriteManager)
        {
        }

        protected override void InitializeSprites()
        {
            s_hitCombo =
                new pSpriteText(GetComboString(), SkinManager.Current.FontCombo, SkinManager.Current.FontComboOverlap,
                                Fields.TopLeft, Origins.Centre, Clocks.Game,
                                new Vector2(2, 240), 0.9F, true, Color.TransparentWhite);
            s_hitCombo2 =
                new pSpriteText("", SkinManager.Current.FontCombo, SkinManager.Current.FontComboOverlap,
                                Fields.TopLeft, Origins.Centre, Clocks.Game,
                                new Vector2(2, 240), 0.91F, true, Color.TransparentWhite);
            s_hitCombo2.Additive = true;
            if (!Player.Relaxing && !Player.Relaxing2)
            {
                spriteManager.Add(s_hitCombo);
                spriteManager.Add(s_hitCombo2);
            }
        }

        protected override string GetComboString()
        {
            return DisplayCombo.ToString();
        }

        internal override void SlideIn() { }

        internal override void SlideOut() { }

        internal override void EnsureVisible() { }

        internal void Update(float position)
        {
            base.Update();

            s_hitCombo.Position.X = position;
            s_hitCombo2.Position.X = position;
        }

        protected override void OnDecrease()
        {
            s_hitCombo.FadeOut(100);
        }

        protected override void OnIncrease(bool hasChanged)
        {
            s_hitCombo.Transformations.Clear();
            s_hitCombo2.Transformations.Clear();

            s_hitCombo.Alpha = 1;

            //Scale the main text
            Transformation t1 = new Transformation(TransformationType.Scale, 2, 1, GameBase.Time, GameBase.Time + 300)
            {
                Easing = EasingTypes.Out,
                TagNumeric = 1
            };
            s_hitCombo.Transformations.Add(t1);

            t1 = new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + 1000, GameBase.Time + 1300)
            {
                TagNumeric = 1
            };
            s_hitCombo.Transformations.Add(t1);

            //Scale the alt text
            t1 = new Transformation(TransformationType.Scale, 2, 2.4f, GameBase.Time, GameBase.Time + 400)
            {
                Easing = EasingTypes.Out,
                TagNumeric = 1
            };
            //Fade the alt text
            Transformation t2 = new Transformation(TransformationType.Fade, 0.7F, 0, GameBase.Time, GameBase.Time + 400)
            {
                TagNumeric = 1
            };
            s_hitCombo2.Transformations.Add(t1);
            s_hitCombo2.Transformations.Add(t2);
        }
    }
}