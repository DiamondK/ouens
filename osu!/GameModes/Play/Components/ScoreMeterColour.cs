﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements;
using osu.Graphics.Sprites;
using System;
using System.Collections.Generic;
using System.Text;

namespace osu.GameModes.Play.Components
{
    internal class ScoreMeterColour : ScoreMeter
    {
        protected float iconWidth { get { return 9 * scale; } }
        protected pSprite[] scoreIcons;
        protected int iconIndex = 0;

        internal ScoreMeterColour(Game game, HitObjectManager hitObjectManager)
            : base(game, hitObjectManager)
        {

        }

        internal override void Initialize()
        {
            float width = GameBase.WindowWidthScaled / 3 * scale;
            pSprite bg = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.BottomLeft, Clocks.Audio, new Vector2((GameBase.WindowWidthScaled - width) / 2, 480), 0.8f, true, Color.Black);
            bg.Alpha = 0.6f;
            bg.VectorScale = new Vector2(width, iconWidth + 1) * 1.6f;
            spriteManager.Add(bg);

            int count = (int)(width / (iconWidth + 1));
            float space = (width - count * iconWidth - (count - 1)) * 1.0f / (count - 1);
            if (GameBase.Tournament) space *= 3;
            scoreIcons = new pSprite[count];
            float left = (GameBase.WindowWidthScaled - width) / 2.0f;
            for (int i = 0; i < count; i++)
            {
                pSprite icon = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.BottomLeft, Clocks.Audio, new Vector2(left, 480), 0.9f, true, Color.Black);
                icon.VectorScale = new Vector2(iconWidth, iconWidth) * 1.6f;
                spriteManager.Add(icon);
                scoreIcons[i] = icon;
                left += iconWidth + 1 + space;
            }
        }

        internal void Reset()
        {
            foreach (pSprite p in scoreIcons)
            {
                p.Alpha -= 0.08f;
            }
        }

        internal override void AddMeter(IncreaseScoreType score)
        {
            nextFadeTime = AudioEngine.Time + fade_delay;
            
            Show();

            Color c;
            if (score < 0)
            {
                c = new Color(255, 9, 9);
            }
            else
            {
                switch (score & ~(IncreaseScoreType.NonScoreModifiers))
                {
                    case IncreaseScoreType.Hit300g:
                    case IncreaseScoreType.Hit300k:
                    case IncreaseScoreType.Hit300m:
                    case IncreaseScoreType.Hit300:
                    case IncreaseScoreType.TaikoDrumRoll:
                    case IncreaseScoreType.TaikoDenDenHit:
                    case IncreaseScoreType.TaikoDenDenComplete:
                        c = colour300;
                        break;
                    case IncreaseScoreType.Hit100k:
                    case IncreaseScoreType.Hit100m:
                    case IncreaseScoreType.Hit100:
                    case IncreaseScoreType.FruitTick:
                        c = colour100;
                        break;
                    case IncreaseScoreType.Hit50m:
                    case IncreaseScoreType.Hit50:
                    case IncreaseScoreType.FruitTickTiny:
                        c = colour50;
                        break;
                    case IncreaseScoreType.ManiaHit300g:
                        c = new Color(255, 133, 0);
                        break;
                    case IncreaseScoreType.ManiaHit300:
                        c = new Color(255, 209, 55);
                        break;
                    case IncreaseScoreType.ManiaHit200:
                        c = new Color(121, 208, 32);
                        break;
                    case IncreaseScoreType.ManiaHit100:
                        c = new Color(31, 104, 198);
                        break;
                    case IncreaseScoreType.ManiaHit50:
                        c = new Color(109, 120, 135);
                        break;
                    default:
                        return;
                }
            }

            if (iconIndex >= scoreIcons.Length)
                iconIndex = 0;
            Reset();
            scoreIcons[iconIndex].InitialColour = c;
            scoreIcons[iconIndex].Alpha = 1;
            iconIndex++;
        }


    }
}
