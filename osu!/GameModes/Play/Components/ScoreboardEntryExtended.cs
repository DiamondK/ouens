using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Sprites;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho.Objects;
using osu.Graphics.Skinning;
using osu.GameplayElements.Scoring;

namespace osu.GameModes.Play.Components
{
    internal class ScoreboardEntryExtended : ScoreboardEntry
    {
        private readonly User user;
        internal pSpriteDynamic spriteAvatar;
        internal pSprite spriteSkip;

        internal ScoreboardEntryExtended(int rank, User user, MatchScoringTypes scoreType, PlayModes playMode, SlotTeams team, Mods mods = Mods.None)
            : base(rank, user.Name, playMode)
        {
            Team = team;
            this.user = user;
            this.scoreType = scoreType;
            this.mods = mods;


            for (int i = 0; i < SpriteCollection.Count; i++)
            {
                pSprite p = SpriteCollection[i];
                if (i == 0)
                    p.OriginPosition -= new Vector2(80, 0);
                else
                    p.Position += new Vector2(32.4f, 0);
            }

            //SpriteCollection.AddRange(ModManager.ShowModSprites(mods, position + new Vector2(34, 6), 0.5f));

            spriteAvatar = new pSpriteDynamic(null, null, 0, position + new Vector2(16.8f, 12.51f), 0.920001f);

            user.LoadAvatarInto(spriteAvatar, 51f);

            spriteAvatar.Alpha = 0;

            spriteSkip = new pSprite(SkinManager.Load(@"multi-skipped"), Fields.TopLeft,
                           Origins.BottomRight,
                           Clocks.Game,
                           position + new Vector2(118, 30), spriteBackground.Depth + 0.000001f, true,
                           Color.White);
            spriteSkip.Alpha = 0;
            spriteSkip.Bypass = true;

            SpriteCollection.Add(spriteSkip);
            SpriteCollection.Add(spriteAvatar);
        }
    }
}