using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu_common;
using osu_common.Bancho.Objects;
using osu.GameplayElements.Scoring;
using osu.GameplayElements.Beatmaps;
using osu.Configuration;
using osu.GameModes.Online;

namespace osu.GameModes.Play.Components
{
    internal class Scoreboard : IDisposable
    {
        internal readonly int displayCount;
        internal readonly List<ScoreboardEntry> Entries = new List<ScoreboardEntry>();
        internal Vector2 topLeft;
        internal bool AlwaysVisible;
        internal bool AutoColour;
        private int lastTrackingPos;
        internal Vector2 offset = new Vector2(0, 36);
        internal SpriteManager spriteManager = new SpriteManager(true);
        private pText statusMessage;
        internal ScoreboardEntry trackingEntry;
        internal int inputCount;


        internal Scoreboard(int displayCount, ScoreboardEntry tracking, Vector2 topLeft, bool startVisible)
        {
            SkinManager.Load(@"scoreboard-explosion-1", SkinSource.Osu);
            SkinManager.Load(@"scoreboard-explosion-2", SkinSource.Osu);

            AlwaysVisible = startVisible;

            this.displayCount = displayCount;

            this.topLeft = topLeft;

            if (tracking != null)
                SetTrackingEntry(tracking);
        }


        internal virtual void SetTrackingEntry(ScoreboardEntry tracking)
        {
            trackingEntry = tracking;
            if (!Entries.Contains(trackingEntry))
                Add(trackingEntry);
            trackingEntry.spriteBackground.InitialColour = new Color(250, 250, 250, 100);
        }

        internal void Add(ScoreboardEntry s)
        {
            Entries.Add(s);
            if (DisplayOnRight) s.AlignedToRight = DisplayOnRight;
            s.scoreboard = this;

            if (s.allowUpdateRank)
            {
                if (s.rank == 0 && ShowLeader)
                    s.spriteBackground.InitialColour = new Color(97, 190, 255, 150);
                else
                    s.spriteBackground.InitialColour = new Color(31, 115, 153, 150);
            }

            spriteManager.Add(s.SpriteCollection);
        }

        internal virtual bool Reorder(bool instant)
        {
            Entries.Sort();

            bool posChange = false;

            if (trackingEntry != null)
            {
                int trackingPos = Entries.IndexOf(trackingEntry);

                bool rightAligned = trackingEntry.AlignedToRight || DisplayOnRight;

                if (trackingPos < lastTrackingPos)
                {
                    trackingEntry.spriteBackground.FlashColour(Color.White, 200);
                    pSprite exp1 = new pSprite(SkinManager.Load(@"scoreboard-explosion-2", SkinSource.Osu),
                                               rightAligned ? Origins.CentreRight : Origins.CentreLeft,
                                               (rightAligned ? trackingEntry.spriteRank.Position + new Vector2(24, 0) : trackingEntry.spriteBackground.Position) + new Vector2(0, 15)
                                               , 0.99f, false, Color.White);
                    exp1.Additive = true;
                    exp1.FadeOut(400);
                    exp1.Transformations.Add(new Transformation(TransformationType.VectorScale, new Vector2(1, 1),
                                                                new Vector2(16, 1.2f), GameBase.Time, GameBase.Time + 200,
                                                                EasingTypes.Out));
                    if (rightAligned)
                    {
                        exp1.FlipHorizontal = true;
                    }
                    spriteManager.Add(exp1);
                    pSprite exp2 = new pSprite(SkinManager.Load(@"scoreboard-explosion-1", SkinSource.Osu),
                                               rightAligned ? Origins.CentreRight : Origins.CentreLeft,
                                               (rightAligned ? trackingEntry.spriteRank.Position + new Vector2(24, 0) : trackingEntry.spriteBackground.Position) + new Vector2(0, 15)
                                               , 1, false, Color.White);
                    spriteManager.Add(exp2);
                    exp2.FadeOut(700);
                    exp2.Additive = true;
                    exp2.Transformations.Add(new Transformation(TransformationType.VectorScale, new Vector2(1, 1),
                                                                new Vector2(1, 1.3f), GameBase.Time, GameBase.Time + 700,
                                                                EasingTypes.Out));
                    if (rightAligned)
                    {
                        exp1.FlipHorizontal = true;
                        exp2.FlipHorizontal = true;
                    }
                }

                posChange = trackingPos != lastTrackingPos;
                lastTrackingPos = trackingPos;

                Reposition(instant);
            }

            return posChange;
        }

        internal Vector2 rightOffset = new Vector2(GameBase.WindowWidthScaled - 115, 0);
        public bool ShowLeader;
        public bool AllowTies;

        bool displayOnRight;
        internal bool AllowRankUpdates = true;
        internal bool DisplayOnRight
        {
            get { return displayOnRight; }
            set
            {
                displayOnRight = value;
                Entries.ForEach(e => e.AlignedToRight = displayOnRight);
            }
        }

        internal void Reposition(bool instant)
        {
            int trackingPos = Entries.IndexOf(trackingEntry);

            int startScore = trackingPos > displayCount - 1 ? trackingPos - (displayCount - 1) : 0;
            if (ShowLeader && startScore > 0) startScore++;

            int leftList = 0;
            int rightList = 0;

            //local score is being displayed.  the rank delimiter should be decreased by one since the local score may pass this one.
            //todo: refactor this stuff.  should be a child class or something to handle these cases.
            int rankDelimiter = InputManager.ReplayMode && !InputManager.ReplayStreaming && !ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Autoplay) ? inputCount - 1 : Entries.Count - 1;
            if (!InputManager.ReplayMode && Entries.Count > 0 && Entries[Entries.Count - 1].Score != null && Entries[Entries.Count - 1].Score == BeatmapManager.Current.onlinePersonalScore)
                rankDelimiter--;

            int rank = 1;

            for (int i = 0; i < Entries.Count; i++)
            {
                ScoreboardEntry sbe = Entries[i];

                bool displayOnRight = Entries[i].AlignedToRight || DisplayOnRight;

                int m = displayOnRight ? rightList : leftList;

                //int m = i - startScore;
                bool displayable = i >= startScore && m < displayCount || (ShowLeader && i == 0);

                if (displayable && AutoColour && sbe != trackingEntry && sbe.allowUpdateRank)
                    sbe.alpha = 0.8f - (float)i / inputCount * 0.3f;

                if (i > 0 && (!AllowTies || Entries[i - 1].score != sbe.score))
                    rank++;

                if (AllowRankUpdates)
                {
                    sbe.rank = rank;

                    if (sbe.Passing)
                        sbe.UpdateRank((ConfigManager.sRankType != Select.RankingType.Friends && ConfigManager.sRankType != Select.RankingType.Local) &&
                            sbe.rank > rankDelimiter && MatchSetup.Match == null ? @"??" : (i + 1).ToString());
                }

                sbe.MoveTo(topLeft + (displayOnRight ? rightOffset : Vector2.Zero) + offset * m, displayable, instant);

                if (displayable)
                {
                    if (displayOnRight)
                        rightList++;
                    else
                        leftList++;
                }
            }
        }

        internal void Draw()
        {
            spriteManager.Draw();
        }

        public void ShowStatus(string s)
        {
            if (statusMessage != null)
                statusMessage.FadeOut(100);

            statusMessage = new pText(s, 12, topLeft + (DisplayOnRight ? rightOffset : Vector2.Zero) - new Vector2(0, 5), 1, false, Color.White);
            statusMessage.Origin = Origins.BottomLeft;
            statusMessage.TextBold = true;
            statusMessage.FadeOut(6000);
            spriteManager.Add(statusMessage);
        }

        public void Dispose()
        {
            if (spriteManager != null)
                spriteManager.Dispose();
        }
    }
}