﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play.Rulesets;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu_common;

namespace osu.GameModes.Play.Components
{
    internal class HpBar
    {
        protected readonly pAnimation s_barFill;
        protected internal readonly pSprite s_barBg;
        protected readonly pSprite s_kiIcon;

        protected readonly pTexture t_kiDanger;
        protected readonly pTexture t_kiDanger2;
        protected readonly pTexture t_kiNormal;

        protected readonly SpriteManager spriteManager;
        protected readonly List<pSprite> SpriteCollection = new List<pSprite>();

        /// <summary>
        /// Are we currently doing the initial "fill" stage?
        /// </summary>
        internal bool InitialIncrease = true;

        /// <summary>
        /// Time in Audio milliseconds to start the initial HP increase.
        /// </summary>
        internal int InitialIncreaseStartTime;

        /// <summary>
        /// Rate of initial HP increase.
        /// </summary>
        internal double InitialIncreaseRate = 0.02;

        /// <summary>
        /// The rate at which HP will naturally drop.
        /// </summary>
        internal double HpDropRate;

        /// <summary>
        /// DisplayHp lags behind current hp due to smooth movement.  Handled internally.
        /// </summary>
        internal double DisplayHp { get; private set; }

        /// <summary>
        /// Current and accurate HP counter.
        /// </summary>
        internal double CurrentHp { get; private set; }

        /// <summary>
        /// Current HP with no upper limiter.
        /// </summary>
        internal double CurrentHpUncapped { get; private set; }

        private bool localVisible = true;

        private pSprite lastExplode;
        protected bool newDefault;

        internal bool Visible
        {
            get { return localVisible; }
            set
            {
                if (localVisible == value) return;

                setVisibility(value);


                localVisible = value;
            }
        }

        protected virtual void setVisibility(bool visible)
        {
            if (visible)
                SpriteCollection.ForEach(s => s.FadeIn(100));
            else
                SpriteCollection.ForEach(s => s.FadeOut(100));
        }

        internal float CurrentXPosition
        {
            get { return (s_barFill.Position.X + s_barFill.DrawWidth / 1.6f) * GameBase.WindowRatio; }
        }

        internal HpBar(SpriteManager spriteManager)
        {
            this.spriteManager = spriteManager;

            pTexture[] fillTextures = loadSpritesFromSource(@"scorebar-colour");
            pTexture[] markerSprite = loadSpritesFromSource(@"scorebar-marker");
            newDefault = (fillTextures[0].Source == SkinSource.Osu || markerSprite[0].Source == SkinSource.Skin);

            if (newDefault)
            {
                if (fillTextures[0].assetName == @"scorebar-colour-0" && fillTextures[0].Source == SkinSource.Osu)
                {
                    fillTextures = new pTexture[] {loadSpriteFromSource(@"scorebar-colour")}; // replace the old ugly bar if osu! tries to load it
                }
                t_kiNormal = markerSprite[0];
                t_kiDanger = markerSprite[0];
                t_kiDanger2 = markerSprite[0];
                s_barFill = new pAnimation(fillTextures, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(7.5f, 7.8f), 0.965F, true, Color.White, null);
                s_kiIcon = new pSprite(t_kiNormal, Fields.NativeStandardScale, Origins.Centre, Clocks.Game, new Vector2(CurrentXPosition, (8.125f + 2.5f) * GameBase.WindowRatio), 0.97F, true, Color.White, null);
                s_kiIcon.Additive = true;
            }
            else
            {
                t_kiNormal = loadSpriteFromSource(@"scorebar-ki");
                t_kiDanger = loadSpriteFromSource(@"scorebar-kidanger");
                t_kiDanger2 = loadSpriteFromSource(@"scorebar-kidanger2");
                s_barFill = new pAnimation(fillTextures, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(3, 10), 0.965F, true, Color.White, null);
                s_kiIcon = new pSprite(t_kiNormal, Fields.NativeStandardScale, Origins.Centre, Clocks.Game, new Vector2(0, 10 * GameBase.WindowRatio), 0.97F, true, Color.White, null);
            }

            s_barFill.DrawWidth = 0;
            s_barFill.SetFramerateFromSkin();
            s_barFill.DrawDimensionsManualOverride = true;



            s_barBg = new pSprite(loadSpriteFromSource(@"scorebar-bg"), Fields.TopLeft, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0.96F, true, Color.White, null);

            SpriteCollection.Add(s_barBg);
            SpriteCollection.Add(s_barFill);
            SpriteCollection.Add(s_kiIcon);

            if (Player.Relaxing || Player.Relaxing2)
                Visible = false;
            else if (spriteManager != null)
                spriteManager.Add(SpriteCollection);

            Update();
        }

        protected virtual pTexture[] loadSpritesFromSource(string p)
        {
            return SkinManager.LoadAll(p);
        }

        protected virtual pTexture loadSpriteFromSource(string p)
        {
            return SkinManager.Load(p);
        }

        internal virtual void SlideOut()
        {
            s_barFill.FadeOut(500);
            s_barBg.FadeOut(500);
            s_kiIcon.FadeOut(500);

            if (lastExplode != null) lastExplode.FadeOut(50);

            Vector2 off = new Vector2(0, 20);

            s_barFill.MoveTo(s_barFill.InitialPosition - off, 500);
            s_barBg.MoveTo(s_barBg.InitialPosition - off, 500);

            s_kiIcon.InitialPosition = new Vector2(s_kiIcon.Position.X, s_kiIcon.InitialPosition.Y);
            s_kiIcon.Transformations.Add(new Transformation(TransformationType.Scale, 1, 1.6f, GameBase.Time, GameBase.Time + 500));
        }

        internal virtual void SlideIn()
        {
            s_barFill.FadeIn(500);
            s_barBg.FadeIn(500);
            s_kiIcon.FadeIn(500);

            s_barFill.MoveTo(s_barFill.InitialPosition, 500);
            s_barBg.MoveTo(s_barBg.InitialPosition, 500);
            s_kiIcon.Transformations.Add(new Transformation(TransformationType.Scale, 1.6f, 1, GameBase.Time, GameBase.Time + 500));
        }

        internal virtual void Update()
        {
            if (Player.Failed)
            {
                s_barFill.DrawWidth = 0;
            }

            if (!InitialIncrease || InitialIncreaseStartTime > 0)
            //i think this is more accurate to ouendan.  someone said otherwise on the forum but..
            {
                if (newDefault)
                {
                    if (Player.Mode == PlayModes.Osu)
                    {
                        if (DisplayHp < Ruleset.HP_BAR_MAXIMUM * 0.2)
                        {
                            s_kiIcon.InitialColour = s_barFill.InitialColour = ColourHelper.ColourLerp(Color.Black, Color.Red, Math.Max(0, (float)((Ruleset.HP_BAR_MAXIMUM * 0.2 - DisplayHp) / (Ruleset.HP_BAR_MAXIMUM * 0.2))));
                            s_kiIcon.Bypass = true;
                        }
                        else if (DisplayHp < Ruleset.HP_BAR_MAXIMUM * 0.5)
                        {
                            s_kiIcon.InitialColour = s_barFill.InitialColour = ColourHelper.ColourLerp(Color.White, Color.Black, Math.Max(0, (float)((Ruleset.HP_BAR_MAXIMUM * 0.5 - DisplayHp) / (Ruleset.HP_BAR_MAXIMUM * 0.5))));
                            s_kiIcon.Additive = false;
                            s_kiIcon.Bypass = false;
                        }
                        else
                        {
                            s_barFill.InitialColour = Color.White;
                            s_kiIcon.InitialColour = Color.White;
                            s_kiIcon.Additive = true;
                            s_kiIcon.Bypass = false;
                        }
                    }
                }

                if (DisplayHp < Ruleset.HP_BAR_MAXIMUM * 0.2)
                {
                    if (s_kiIcon.localTexture != t_kiDanger2)
                        s_kiIcon.localTexture = t_kiDanger2;
                }
                else if (DisplayHp < Ruleset.HP_BAR_MAXIMUM * 0.5)
                {
                    if (s_kiIcon.localTexture != t_kiDanger)
                        s_kiIcon.localTexture = t_kiDanger;
                }
                else if (s_kiIcon.localTexture != t_kiNormal)
                {
                    s_kiIcon.localTexture = t_kiNormal;
                }
            }

            //HP Bar
            if (DisplayHp < CurrentHp)
            {
                if (InitialIncrease)
                {
                    if (InitialIncreaseStartTime < AudioEngine.Time && (Player.Recovering || AudioEngine.AudioState == AudioStates.Playing))
                    {
                        DisplayHp = Math.Min(Ruleset.HP_BAR_MAXIMUM, DisplayHp + InitialIncreaseRate * (Player.Recovering ? GameBase.ElapsedMilliseconds : AudioEngine.ElapsedMilliseconds));
                        if (s_kiIcon.Transformations.Count == 0)
                        {
                            s_kiIcon.Transformations.Add(new Transformation(TransformationType.Scale, 1.2F, 0.8F, GameBase.Time, GameBase.Time + 150));
                        }
                    }
                }
                else
                    DisplayHp = Math.Min(Ruleset.HP_BAR_MAXIMUM, DisplayHp + Math.Abs(CurrentHp - DisplayHp) / 4 * GameBase.FrameRatio);
            }
            else if (DisplayHp > CurrentHp)
            {
                InitialIncrease = false;
                DisplayHp = Math.Max(0, DisplayHp - Math.Abs(DisplayHp - CurrentHp) / 6 * GameBase.FrameRatio);
            }

            s_barFill.DrawWidth = (int)Math.Min(s_barFill.Width, Math.Max(0, (s_barFill.Width * (DisplayHp / 200))));

            UpdateKiPosition();
        }

        internal virtual void Draw()
        {
        }

        internal virtual void Fade(float alpha) { }

        protected virtual void UpdateKiPosition()
        {
            //Sync Ki icon position with the end of the scorebar fill.
            s_kiIcon.Position = new Vector2(CurrentXPosition, s_kiIcon.Position.Y);
        }

        internal virtual void SetKiColour(Color colour)
        {
            //If already changing the colour, set to that colour immediately.
            Transformation tr = s_kiIcon.Transformations.Find(t => t.Type == TransformationType.Colour);
            if (tr != null)
            {
                s_kiIcon.InitialColour = tr.EndColour;
                s_kiIcon.Transformations.Remove(tr);
            }

            //Add new colour fade.
            Transformation newTransformation =
                    new Transformation(s_kiIcon.InitialColour, colour, GameBase.Time, GameBase.Time + 400);
            s_kiIcon.Transformations.Add(newTransformation);
        }

        internal virtual void KiBulge()
        {
            s_kiIcon.Transformations.RemoveAll(t => t.Type == TransformationType.Scale);
            s_kiIcon.Transformations.Add(new Transformation(TransformationType.Scale, 1.2F, 0.8F, GameBase.Time, GameBase.Time + 150));
        }

        internal virtual void KiExplode()
        {
            if (!localVisible) return;

            lastExplode =
                    new pSprite(t_kiNormal, Fields.NativeStandardScale, Origins.Centre, Clocks.Game,
                                s_kiIcon.Position, 1, false, Color.White, null);

            Transformation ts;
            Transformation tf;

            if (s_kiIcon.Additive)
            {
                lastExplode.Additive = s_kiIcon.Additive;
                tf = new Transformation(TransformationType.Fade, 0.5f, 0, GameBase.Time, GameBase.Time + 120);
                ts = new Transformation(TransformationType.Scale, 1, 2F, GameBase.Time, GameBase.Time + 120);
            }
            else
            {
                tf = new Transformation(TransformationType.Fade, 1, 0, GameBase.Time, GameBase.Time + 120);
                ts = new Transformation(TransformationType.Scale, 1, 1.6F, GameBase.Time, GameBase.Time + 120);
            }

            ts.Easing = EasingTypes.Out;
            tf.Easing = EasingTypes.Out;

            lastExplode.Transformations.Add(ts);
            lastExplode.Transformations.Add(tf);

            if (spriteManager != null)
                spriteManager.Add(lastExplode);
        }

        internal virtual void SetCurrentHp(double amount)
        {
            CurrentHp = Math.Max(0, Math.Min(Ruleset.HP_BAR_MAXIMUM, amount));
            CurrentHpUncapped = amount;
        }

        internal virtual void ReduceCurrentHp(double amount)
        {
            if (Player.Playing && InitialIncrease) InitialIncrease = false;

            CurrentHpUncapped = Math.Max(0, CurrentHpUncapped - amount);
            CurrentHp = Math.Max(0, CurrentHp - amount);
        }

        internal virtual void IncreaseCurrentHp(double amount)
        {
            if (Player.Playing && InitialIncrease) InitialIncrease = false;

            CurrentHpUncapped += amount;
            CurrentHp = Math.Max(0, Math.Min(Ruleset.HP_BAR_MAXIMUM, CurrentHp + amount));
        }

        internal void SetDisplayHp(double amount)
        {
            DisplayHp = amount;
        }

        internal virtual void Reset()
        {
            CurrentHp = Ruleset.HP_BAR_MAXIMUM;
            CurrentHpUncapped = Ruleset.HP_BAR_MAXIMUM;
            DisplayHp = 0;
        }
    }
}
