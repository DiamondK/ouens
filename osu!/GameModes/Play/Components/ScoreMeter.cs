﻿using Microsoft.Xna.Framework;
using osu.Audio;
using osu.GameModes.Play.Rulesets;
using osu.Graphics.Sprites;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.GameplayElements;
using osu.GameplayElements.HitObjects;

namespace osu.GameModes.Play.Components
{
    internal enum ScoreMeterType
    {
        None,
        Colour,
        Error
    }

    internal class ScoreMeter : GameComponent
    {
        protected HitObjectManager hitObjectManager;

        protected SpriteManager spriteManager;
        protected int nextFadeTime = 0;
        protected const int fade_delay = 4000;

        protected Color colour300 = new Color(50, 188, 231);
        protected Color colour100 = new Color(87, 227, 19);
        protected Color colour50 = new Color(218, 174, 70);

        protected virtual float scale { get { return (float)ConfigManager.sScoreMeterScale.Value; } }

        internal ScoreMeter(Game game, HitObjectManager hitObjectManager)
            : base(game)
        {
            this.hitObjectManager = hitObjectManager;

            spriteManager = new SpriteManager(true);
            spriteManager.Alpha = 0;
            Initialize();
        }

        internal void Dispose()
        {
            spriteManager.Dispose();
        }

        internal virtual void Initialize(){}

        internal virtual void AddMeter(IncreaseScoreType score){}
        internal virtual void AddMeter(HitObject h, int? customDelta){}

        internal void Draw()
        {
            spriteManager.Draw();
        }

        protected void Show()
        {
            spriteManager.Blackness = 0;
            spriteManager.Alpha = 1;
        }

        public override void Update()
        {
            if ((Player.IsSpinning || AudioEngine.Time > nextFadeTime || (Player.Instance.Status != PlayerStatus.Playing && Player.Instance.Status != PlayerStatus.Resting)) && spriteManager.Alpha > 0)
            {
                spriteManager.Blackness = (float)Math.Max(0, 1 - 4 * spriteManager.Alpha - 0.04f * GameBase.FrameRatio);
                spriteManager.Alpha = (float)Math.Max(0, spriteManager.Alpha - 0.04f * GameBase.FrameRatio);
            }
        }
    }
}
