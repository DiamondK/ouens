﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;

namespace osu.GameModes.Play.Components
{
    class ScoreDisplayTaiko : ScoreDisplay
    {
        internal ScoreDisplayTaiko(SpriteManager spriteManager) : base(spriteManager)
        {
            s_Accuracy.Position.Y += textMeasure.Y * 0.8f;
        }

        internal ScoreDisplayTaiko(SpriteManager spriteManager, Vector2 position, bool alignRight, float scale, bool showScore, bool showAccuracy) : base(spriteManager, position, alignRight, scale, showScore, showAccuracy)
        {
        }

        internal override void Update(int score)
        {
            if (!Player.Relaxing)
            {

                int dif = score - currentScore;
                if (dif > 0)
                {
                    Vector2 startPos = new Vector2(10, s_Score.Position.Y + textMeasure.Y);
                    pSpriteText s =
                        new pSpriteText(dif.ToString(), SkinManager.Current.FontScore, SkinManager.Current.FontScoreOverlap,
                            s_Accuracy.Field, Origins.TopRight, Clocks.Game,
                            startPos, 0.95F, false, Color.Red);
                    s.Scale = scale * 0.8f;
                    spriteManager.Add(s);

                    s.FadeInFromZero(60);                
                    s.Transformations.Add(new Transformation(startPos + new Vector2(GameBase.random.NextDouble() > 0.5 ? 10 : -10, 0), startPos, GameBase.Time + 0, GameBase.Time + 100, EasingTypes.Out));
                    s.Transformations.Add(new Transformation(startPos, startPos + new Vector2(0,-30),GameBase.Time + 300, GameBase.Time + 420, EasingTypes.In));
                    s.Transformations.Add(new Transformation(TransformationType.Fade,1,0, GameBase.Time + 300, GameBase.Time + 420, EasingTypes.In));
                }
            }

            base.Update(score);
        }
    }
}
