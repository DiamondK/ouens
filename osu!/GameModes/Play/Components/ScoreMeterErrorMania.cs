﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using System;
using System.Collections.Generic;
using System.Text;

namespace osu.GameModes.Play.Components
{
    internal class ScoreMeterErrorMania : ScoreMeterError
    {
        internal ScoreMeterErrorMania(Game game, HitObjectManager hitObjectManager)
            : base(game, hitObjectManager)
        {

        }

        internal override Vector2 CentrePosition
        {
            get
            {
                return new Vector2(hitObjectManager.ManiaStage.Left + Width / 2, (hitObjectManager.ManiaStage.Skin.UpsideDown ? GameBase.WindowHeightScaled - barHeight * 2 : barHeight * 4));
            }
        }
        internal override float Width
        {
            get
            {
                return hitObjectManager.ManiaStage.Width;
            }
        }
    }
}
