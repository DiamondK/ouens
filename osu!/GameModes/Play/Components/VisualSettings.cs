﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.Graphics.Notifications;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input.Handlers;
using osu_common;
using osu.Input;
using osu.Configuration;
using osu.Online;
using osu_common.Helpers;
using osu.Online.Social;

namespace osu.GameModes.Play.Components
{
    internal class VisualSettings : GameComponent
    {
        internal int WIDTH { get { return GameBase.WindowWidthScaled; } }
        private int HEIGHT { get { return initial_display; } }

        private int yPosition { get { return GameBase.WindowHeightScaled - (Activated ? extended_display : initial_display); } }

        const int initial_display = 45;
        private int extended_display = 150;

        private readonly SpriteManager spriteManager = new SpriteManager(true);
        private bool alwaysShow;
        private pSprite background;
        private int LastShow;
        internal bool Activated;

        private pSliderBar backgroundTransparency;
        private pCheckbox checkboxStoryboard;
        private pCheckbox checkboxVideo;
        private pCheckbox checkboxSkin;
        private pCheckbox checkboxHitsounds;
        private bool skinChanged;
        private bool samplesetChanged;
        bool dirty;

        Vector2 mousePositionInitial;
        private bool mouseHasMoved;

        public bool CurrentMapHasBeenPlayed
        {
            get
            {
                return true;
                /*return
                    (BanchoClient.Permission & Permissions.Supporter) > 0
                    || PlayerVs.Match != null
                    || InputManager.ReplayMode
                    || BeatmapManager.Current.PlayerRank < Rankings.F
                    || BeatmapManager.Current.SubmissionStatus == SubmissionStatus.NotSubmitted
                    || BeatmapManager.Current.SubmissionStatus == SubmissionStatus.Pending
                    || (BeatmapManager.Current.SubmissionStatus != SubmissionStatus.Ranked && BeatmapManager.Current.UpdateAvailable);*/
            }
        }

        public VisualSettings(Game game)
            : base(game)
        {
            Initialize();
            RestoreSettings();
        }

        private void RestoreSettings()
        {
            bool isPlayed = CurrentMapHasBeenPlayed;

            if (BeatmapManager.Current.VisualSettingsOverride)
            {
                backgroundTransparency.SetValue(EventManager.UserDimLevel = (BeatmapManager.Current.DimLevel ?? ConfigManager.sDimLevel.Value), true);

                checkboxStoryboard.SetStatusQuietly(!(EventManager.ShowStoryboard = (!isPlayed && !BeatmapManager.Current.EpilepsyWarning) || !BeatmapManager.Current.DisableStoryboard));
                checkboxVideo.SetStatusQuietly(!(EventManager.ShowVideo = ConfigManager.sVideo && !BeatmapManager.Current.DisableVideo));
                checkboxSkin.SetStatusQuietly(SkinManager.IgnoreBeatmapSkin = isPlayed && BeatmapManager.Current.DisableSkin);
                checkboxHitsounds.SetStatusQuietly(AudioEngine.IgnoreBeatmapSamples = isPlayed && BeatmapManager.Current.DisableSamples);
            }
            else
            {
                backgroundTransparency.SetValue(EventManager.UserDimLevel = ConfigManager.sDimLevel.Value, true);

                checkboxStoryboard.SetStatusQuietly(!(EventManager.ShowStoryboard = ConfigManager.sShowStoryboard));
                checkboxVideo.SetStatusQuietly(!(EventManager.ShowVideo = ConfigManager.sVideo));
                checkboxSkin.SetStatusQuietly(SkinManager.IgnoreBeatmapSkin = ConfigManager.sIgnoreBeatmapSkins);
                checkboxHitsounds.SetStatusQuietly(AudioEngine.IgnoreBeatmapSamples = ConfigManager.sIgnoreBeatmapSamples);
            }

            updateBackgroundTransparencyTooltip();
        }

        internal void CheckMapContent()
        {
            if (Player.Instance == null) return;

            EventManager em = Player.Instance.eventManager;
            if (em == null) return;

            checkboxStoryboard.Enabled &= checkboxStoryboard.Checked || em.HasStoryboard;
            checkboxVideo.Enabled = ConfigManager.sVideo && (checkboxVideo.Checked || em.videoSprites.Count > 0);
        }

        protected override void Dispose(bool disposing)
        {
            //transfer settings to other maps in this set
            if (dirty)
            {
                BeatmapManager.Beatmaps.FindAll(b => b.RelativeContainingFolder == BeatmapManager.Current.RelativeContainingFolder).ForEach(b =>
                    {
                        b.DisableSamples = checkboxHitsounds.Checked;
                        b.DisableSkin = checkboxSkin.Checked;
                        b.DisableStoryboard = checkboxStoryboard.Checked;
                        if (ConfigManager.sVideo && GameBase.Mode != OsuModes.Edit && !GameBase.TestMode) b.DisableVideo = BeatmapManager.Current.DisableVideo;
                        b.DimLevel = (int)backgroundTransparency.current;
                    });
            }

            spriteManager.Dispose();
            backgroundTransparency.Dispose();
            base.Dispose(disposing);

            if (skinChanged)
            {
                SkinManager.ClearBeatmapCache(true);
                Player.Retrying = false; //force the Player instance into non-retry mode to ensure skin is reloaded.
            }
            if (samplesetChanged)
            {
                AudioEngine.ClearBeatmapCache();
            }

            //restore settings to defaults
            EventManager.ShowStoryboard = true;
            EventManager.ShowVideo = ConfigManager.sVideo;
            SkinManager.IgnoreBeatmapSkin = false;
            AudioEngine.IgnoreBeatmapSamples = false;
            EventManager.UserDimLevel = 30;
        }

        public override void Initialize()
        {
            int vPosition = yPosition + initial_display;
            int hPosition = 0;

            mousePositionInitial = MouseManager.MousePosition;

            background = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft,
                                     Clocks.Game,
                                     new Vector2(0, vPosition), 0.68f, true,
                                     CurrentMapHasBeenPlayed ? new Color(0, 78, 155) : Color.DarkGray);
            background.Scale = 1.6f;
            background.VectorScale = new Vector2(WIDTH, 3);
            spriteManager.Add(background);

            vPosition += 3;

            background = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft,
                                     Clocks.Game,
                                     new Vector2(0, vPosition), 0.68f, true,
                                     new Color(0, 0, 0, 180));
            background.Scale = 1.6f;
            background.VectorScale = new Vector2(WIDTH, extended_display);
            spriteManager.Add(background);

            spriteManager.Add(new pText("Visual", 24, new Vector2(hPosition, vPosition), Vector2.Zero, 0.99f, true, Color.LightBlue, true)
            {
                TextBold = true
            });

            vPosition += 15;

            spriteManager.Add(new pText("Settings", 22, new Vector2(hPosition + 50, vPosition), Vector2.Zero, 1, true, Color.White, true)
            {
                TextBold = true
            });

            const int padding = 20;
            const int col_width = 240;
            bool mapHasBeenPlayed = CurrentMapHasBeenPlayed;

            if (!CurrentMapHasBeenPlayed)
            {
                spriteManager.Add(new pText(LocalisationManager.GetString(OsuString.FunSpoiler_PlayFirst), 15, new Vector2(GameBase.WindowWidthScaled / 2, vPosition), new Vector2(col_width * 2, 0), 1, true, Color.OrangeRed, true)
                {
                    TextBorder = true,
                    Origin = Origins.Centre,
                    TextBold = true,
                    TextAa = false
                });
            }

            vPosition += 30;

            spriteManager.Add(new pText(LocalisationManager.GetString(OsuString.FunSpoiler_BackgroundDim), 10, new Vector2(hPosition + padding, vPosition), new Vector2(WIDTH, 20), 0.99f, true, Color.White, true)
            {
                TextBold = true,
                TextAa = false
            });

            spriteManager.Add(new pText(LocalisationManager.GetString(OsuString.FunSpoiler_Toggles), 10, new Vector2(hPosition + padding + col_width, vPosition), new Vector2(WIDTH, 20), 0.99f, true, mapHasBeenPlayed ? Color.White : Color.DarkGray, true)
            {
                TextBold = true,
                TextAa = false
            });

            vPosition += 20;

            backgroundTransparency = new pSliderBar(spriteManager, 0, mapHasBeenPlayed ? 100 : 60, BeatmapManager.Current.DimLevel ?? ConfigManager.sDimLevel.Value, new Vector2(hPosition + padding, vPosition), col_width - padding * 2);
            backgroundTransparency.ValueChanged += backgroundTransparency_ValueChanged;

            updateBackgroundTransparencyTooltip();

            checkboxStoryboard = new pCheckbox(LocalisationManager.GetString(OsuString.FunSpoiler_Storyboard), 0.8f, new Vector2(hPosition + padding + col_width, vPosition), 0.99f, false);
            checkboxStoryboard.Enabled = mapHasBeenPlayed || BeatmapManager.Current.EpilepsyWarning;
            checkboxStoryboard.OnCheckChanged += checkboxStoryboard_OnCheckChanged;
            spriteManager.Add(checkboxStoryboard.SpriteCollection);

            checkboxVideo = new pCheckbox(LocalisationManager.GetString(OsuString.FunSpoiler_Video), 0.8f, new Vector2(hPosition + padding * 2 + col_width * 2, vPosition), 0.99f, false);
            checkboxVideo.Enabled = false;
            checkboxVideo.OnCheckChanged += checkboxVideo_OnCheckChanged;
            spriteManager.Add(checkboxVideo.SpriteCollection);

            vPosition += 20;

            checkboxSkin = new pCheckbox(LocalisationManager.GetString(OsuString.FunSpoiler_Skin), 0.8f, new Vector2(hPosition + padding + col_width, vPosition), 0.99f, false);
            checkboxSkin.OnCheckChanged += checkboxSkin_OnCheckChanged;
            checkboxSkin.Enabled = mapHasBeenPlayed;
            spriteManager.Add(checkboxSkin.SpriteCollection);

            vPosition += 20;

            checkboxHitsounds = new pCheckbox(LocalisationManager.GetString(OsuString.FunSpoiler_Hitsounds), 0.8f, new Vector2(hPosition + padding + col_width, vPosition), 0.99f, false);
            checkboxHitsounds.OnCheckChanged += checkboxHitsounds_OnCheckChanged;
            checkboxHitsounds.Enabled = mapHasBeenPlayed;
            spriteManager.Add(checkboxHitsounds.SpriteCollection);

            spriteManager.FirstDraw = false;

            base.Initialize();
        }

        void checkboxHitsounds_OnCheckChanged(object sender, bool status)
        {
            startOverriding();
            BeatmapManager.Current.DisableSamples = status;
            samplesetChanged = true;
            dirty = true;
            showRestartNotice();
        }

        void checkboxSkin_OnCheckChanged(object sender, bool status)
        {
            startOverriding();
            BeatmapManager.Current.DisableSkin = status;
            skinChanged = true;
            dirty = true;
            showRestartNotice();
        }

        void checkboxStoryboard_OnCheckChanged(object sender, bool status)
        {
            startOverriding();
            BeatmapManager.Current.DisableStoryboard = status;
            EventManager.ShowStoryboard = !status;
            dirty = true;
        }

        void checkboxVideo_OnCheckChanged(object sender, bool status)
        {
            startOverriding();
            BeatmapManager.Current.DisableVideo = status;
            EventManager.ShowVideo = !status;
            dirty = true;
            showRestartNotice();
        }

        void backgroundTransparency_ValueChanged(object sender, EventArgs e)
        {
            startOverriding();
            EventManager.UserDimLevel = (int)backgroundTransparency.current;
            if (EventManager.UserDimLevel != ConfigManager.sDimLevel.Value)
                BeatmapManager.Current.DimLevel = EventManager.UserDimLevel;
            dirty = true;

            updateBackgroundTransparencyTooltip();
        }

        void updateBackgroundTransparencyTooltip()
        {
            backgroundTransparency.Tooltip = string.Format(LocalisationManager.GetString(OsuString.FunSpoiler_Dim_Tooltip), EventManager.UserDimLevel, backgroundTransparency.max);
        }

        private void startOverriding()
        {
            if (BeatmapManager.Current.VisualSettingsOverride)
                return;

            BeatmapManager.Current.VisualSettingsOverride = true;

            //set beatmap setting to current user defaults.
            BeatmapManager.Current.DisableVideo = !ConfigManager.sVideo;
            BeatmapManager.Current.DisableStoryboard = !ConfigManager.sShowStoryboard;
            BeatmapManager.Current.DisableSkin = ConfigManager.sIgnoreBeatmapSkins;
            BeatmapManager.Current.DisableSamples = ConfigManager.sIgnoreBeatmapSamples;
        }

        private void showRestartNotice()
        {
            if (!Player.Loaded || (PlayerVs.Match != null && !PlayerVs.AllPlayersLoaded))
            {
                //we haven't yet finished loading and started playing, so we can just force a restart here
                Player.PendingRestart = true;
            }
            else
            {
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.FunSpoiler_Restart), 1000);
            }
        }

        bool lastShowExtended;
        internal void Show(bool extended = false, bool instant = false)
        {
            if (GameBase.Tournament)
                return;

            if (LastShow == 0)
                spriteManager.SpriteList.ForEach(s =>
                {
                    pText t = s as pText;
                    if (t != null && t.localTexture == null)
                        t.MeasureText();
                });

            lastShowExtended = extended;

            LastShow = GameBase.Time;
            if (extended) LastShow += 500;

            if (Activated) return;

            if (instant)
                spriteManager.SpriteList.ForEach(s => s.Position.Y = s.InitialPosition.Y - extended_display);
            else
                spriteManager.SpriteList.ForEach(s => s.MoveToY(s.InitialPosition.Y - extended_display, 400, EasingTypes.Out));

            Activated = true;
        }

        bool HideOffscreen = InputManager.ReplayMode || StreamingManager.CurrentlySpectating != null;
        internal void Hide(bool? hideOffscreen = null)
        {
            if (GameBase.Time < LastShow || (hideOffscreen == HideOffscreen && !Activated)) return;

            if (hideOffscreen.HasValue) HideOffscreen = hideOffscreen.Value;
            float offset = HideOffscreen ? 0 : initial_display;

            foreach (pSprite s in spriteManager.SpriteList)
            {
                float target = s.InitialPosition.Y - offset;
                bool upwards = s.Position.Y >= target;
                s.MoveToY(target, upwards ? 400 : 600, upwards ? EasingTypes.In : EasingTypes.OutBounce);
            }

            backgroundTransparency.KillCapture();
            Activated = false;
        }

        internal void Draw()
        {
            if (!Activated && HideOffscreen && background.Position == background.InitialPosition)
                return;

            spriteManager.Draw();

            // We don't want the pause menu flickering during the bounce animation of the menu, so we let the alpha become zero above the highest bounce-back point.
            int alphaZeroOffset = 30;
            Player.Instance.spriteManagerPauseScreen.Alpha = 1 - Math.Max(0, (background.InitialPosition.Y - background.Position.Y - initial_display - alphaZeroOffset) / (extended_display - initial_display - alphaZeroOffset));
        }

        public override void Update()
        {
            if (!mouseHasMoved && Vector2.Distance(mousePositionInitial, MouseManager.MousePosition) > 30)
                mouseHasMoved = true;

            bool mouseInRange =
                !ChatEngine.IsVisible &&
                MouseManager.MousePositionWindow.Y > yPosition && MouseManager.MousePositionWindow.Y < GameBase.WindowHeightScaled + 20 &&
                MouseManager.MousePosition.X > 0 && MouseManager.MousePosition.X < GameBase.WindowWidth &&
                mouseHasMoved;

            if (((!HideOffscreen && (Player.Paused || (EventManager.BreakMode && EventManager.BreakCurrent == null))) || (PlayerVs.Instance == null && InputManager.ReplayMode && !Player.Instance.SkipHovered && ChatEngine.spriteManager.CurrentHoverSprite == null)) && mouseInRange && Game.IsActive)
                Show();
            else if (!mouseInRange && Activated)
                Hide();

            base.Update();
        }

    }
}
