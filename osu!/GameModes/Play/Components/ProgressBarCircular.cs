﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.OpenGl;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;

namespace osu.GameModes.Play.Components
{
    internal class ProgressBarCircular : ProgressBar
    {
        private pSprite overlay;
        private CircularProgress progress = new CircularProgress();

        internal ProgressBarCircular(SpriteManager spriteManager, Vector2 position)
            : base(spriteManager, ProgressBarTypes.Pie)
        {
            Position = position;

            overlay = new pSprite(SkinManager.Load(@"circularmetre", SkinSource.Osu), Fields.TopRight,
                                          Origins.Centre, Clocks.Game, position, 0.95f, true, Color.White);

            spriteManager.Add(overlay);

            progress.Position = new Vector2(GameBase.WindowWidth / GameBase.WindowRatio - position.X, position.Y);
            progress.Radius = 10;
        }

        private void Initialize()
        {
        }

        internal override void Draw()
        {
            progress.Colour = progress.Progress < 0 ?
                new Color(199, 255, 47, (byte)(spriteManager.Alpha * overlay.drawColour.A * 0.6)) :
                new Color(255, 255, 255, (byte)(spriteManager.Alpha * overlay.drawColour.A * 0.6));
            progress.Update();
            progress.Draw();
        }

        internal override void SetProgress(float val)
        {
            progress.Progress = val;
        }
    }
}