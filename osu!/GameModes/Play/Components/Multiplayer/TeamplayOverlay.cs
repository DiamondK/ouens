﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu_common.Bancho.Objects;



namespace osu.GameModes.Play.Components.Multiplayer
{
    internal class TeamplayOverlay : IDisposable
    {
        private readonly PlayerVs player;
        private readonly SpriteManager spriteManager;
        private pSprite metre;
        private pSprite metre2;
        internal ScoreDisplay team1display;
        internal ScoreDisplay team2display;

        pText team1header;
        pText team2header;

        //we store it in a bool because the matchtype will be reset on the winner-screen.
        bool accuracyMatch;

        internal SlotTeams winner
        {
            get
            {
                if (PlayerVs.Match.matchScoringType == MatchScoringTypes.Accuracy)
                    return team1display.currentAccuracy > team2display.currentAccuracy ? SlotTeams.Blue : SlotTeams.Red;
                else
                    return team1display.currentScore > team2display.currentScore ? SlotTeams.Blue : SlotTeams.Red;  //score and combo
            }
        }

        internal TeamplayOverlay(PlayerVs playerVs, SpriteManager spriteManager)
        {
            player = playerVs;
            this.spriteManager = spriteManager;

            team1header = new pText("Blue Team", 16, new Vector2(0,100),1, true, Color.White);
            team1header.TextBold = true;
            team1header.TextShadow = true;
            spriteManager.Add(team1header);

            team2header = new pText("Red Team", 16, new Vector2(GameBase.WindowWidthScaled, 100), 1, true, Color.White);
            team2header.TextShadow = true;
            team2header.TextBold = true;
            team2header.Origin = Origins.TopRight;
            spriteManager.Add(team2header);

            accuracyMatch = PlayerVs.Match.matchScoringType == MatchScoringTypes.Accuracy;

            team1display = new ScoreDisplay(spriteManager, new Vector2(0, 120), false, 0.7f, !accuracyMatch, accuracyMatch);
            team2display = new ScoreDisplay(spriteManager, new Vector2(0, 120), true, 0.7f, !accuracyMatch, accuracyMatch);

            metre = new pSprite(SkinManager.Load(@"lobby-crown", SkinSource.Osu), Origins.Centre,
                new Vector2(GameBase.WindowWidthScaled/2, 90), 0.99f, true, Color.Yellow);
            metre.Scale = 1.2f;
            spriteManager.Add(metre);

            metre2 = new pSprite(SkinManager.Load(@"lobby-crown", SkinSource.Osu),Origins.Centre,
                new Vector2(GameBase.WindowWidthScaled/2, 90), 1, true, Color.Yellow);
            metre2.Additive = true;
            Transformation t = new Transformation(TransformationType.Scale,1.2f,1.3f,GameBase.Time,GameBase.Time + 1000,EasingTypes.In);
            t.Loop = true;
            t.LoopDelay = 1000;
            metre2.Transformations.Add(t);
            t = new Transformation(TransformationType.Scale, 1.3f, 1.2f, GameBase.Time + 1000, GameBase.Time + 2000, EasingTypes.Out);
            t.Loop = true;
            t.LoopDelay = 1000;
            metre2.Transformations.Add(t);
            metre2.Scale = 1.2f;
            spriteManager.Add(metre2);
        }

        internal void Draw()
        {
            team1display.Draw();
            team2display.Draw();
        }

        internal void UpdateScores()
        {
            if (!accuracyMatch)
            {
                bool comboHack = PlayerVs.Match.matchScoringType == MatchScoringTypes.Combo;
                int team1 = comboHack? getTeamCombo(SlotTeams.Blue) : getTeamScore(SlotTeams.Blue);
                int team2 = comboHack? getTeamCombo(SlotTeams.Red) : getTeamScore(SlotTeams.Red);

                team1display.Update(team1);
                team2display.Update(team2);

                //Accuracy is shown too...
                team1display.Update(getTeamAccuracy(SlotTeams.Blue) * 100);
                team2display.Update(getTeamAccuracy(SlotTeams.Red) * 100);

                //team1score.Text = team1.ToString("00000000");
                //team2score.Text = team2.ToString("00000000");
                bool teamBlueWinner = (team1 > team2 && !PlayerVs.TeamFailed(SlotTeams.Blue)) || PlayerVs.TeamFailed(SlotTeams.Red);
                float offset = teamBlueWinner ? -Math.Min(300, ((float)team1 / Math.Max(1, team2) - 1) * 100) : Math.Min(300, ((float)team2 / Math.Max(1, team1) - 1) * 100);
                float x = GameBase.WindowWidthScaled/2 + offset;

                metre.MoveTo(new Vector2(x, 90), 1000, EasingTypes.Out);
                metre2.MoveTo(new Vector2(x, 90), 1000, EasingTypes.Out);
            }
            else
            {
                float team1 = getTeamAccuracy(SlotTeams.Blue);
                float team2 = getTeamAccuracy(SlotTeams.Red);

                team1display.Update(team1*100);
                team2display.Update(team2*100);

                float offset = team1 > team2 ? -Math.Min(300, ((float)team1 / Math.Max(1, team2) - 1) * 300) : Math.Min(300, ((float)team2 / Math.Max(1, team1) - 1) * 300);
                float x = GameBase.WindowWidthScaled/2 + offset;

                metre.MoveTo(new Vector2(x, 90), 1000, EasingTypes.Out);
                metre2.MoveTo(new Vector2(x, 90), 1000, EasingTypes.Out);
            }
                   
            
        }

        private int getTeamScore(SlotTeams team)
        {
            int totalScore = 0;
            int playerCount = 0;

            List<ScoreboardEntryExtended> scores = player.scoreEntries ?? PlayerVs.lastScoreEntries;
            int count = scores.Count;
            for (int i = 0; i < count; i++)
            {
                ScoreboardEntryExtended se = scores[i];

                if (se == null) continue;
                if (se.Team != team) continue;

                playerCount++;

                if (se.Score != null && !se.Score.pass)
                    continue;

                totalScore += se.score;
            }

            if (playerCount == 0) return totalScore;


            //players don't want the balance. they like to do 4 vs 2 matches and get (un)fair results
            //if (PlayerVs.Match.matchTeamType == MatchTeamTypes.TeamVs)
            //    totalScore /= playerCount;
            return totalScore;
        }

        private int getTeamCombo(SlotTeams team)
        {
            int totalCombo = 0;
            int playerCount = 0;

            List<ScoreboardEntryExtended> scores = player.scoreEntries ?? PlayerVs.lastScoreEntries;
            int count = scores.Count;
            for (int i = 0; i < count; i++)
            {
                ScoreboardEntryExtended se = scores[i];

                if (se == null) continue;
                if (se.Team != team) continue;

                playerCount++;

                if (se.Score != null && !se.Score.pass)
                    continue;

                totalCombo += se.Score.currentCombo;
            }

            if (playerCount == 0) return totalCombo;

            return totalCombo;
        }

        private float getTeamAccuracy(SlotTeams team)
        {
            float totalAccuracy = 0;
            int playerCount = 0;

            List<ScoreboardEntryExtended> scores = player.scoreEntries ?? PlayerVs.lastScoreEntries;
            int count = scores.Count;

            for (int i = 0; i < count; i++)
            {
                ScoreboardEntry se = scores[i];

                if (se == null) continue;
                if (se.Team != team) continue;
                if (se.Score != null)
                    totalAccuracy += se.Score.accuracy;
                playerCount++;
            }

            return totalAccuracy/playerCount;
        }
        public void Hide()
        {
            team1display.Hide();
            team2display.Hide();

            metre.MoveTo(metre.Position + new Vector2(0, -20), 1000, EasingTypes.Out);
            metre2.MoveTo(metre2.Position + new Vector2(0, -20), 1000, EasingTypes.Out);
            team1header.Position += new Vector2(0, -20);
            team2header.Position += new Vector2(0, -20);
            // hide all scoreboards
            foreach (ScoreboardEntry s in player.scoreEntries)
                if (s!=null) s.Hide();
        }

        public void Dispose()
        {
            
        }
    }
}
