﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.Graphics.Notifications;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input.Handlers;
using osu_common;
using osu.Input;
using osu.Configuration;
using osu.Online;
using osu_common.Helpers;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameModes.Online;

namespace osu.GameModes.Play.Components
{
    internal class Scrubber : GameComponent
    {
        private readonly SpriteManager spriteManager = new SpriteManager(true);

        const int padding = 320;
        const int height = 130;
        private pSliderBar sliderBar;
        private pSliderBar sliderBarUser;
        private int? newAim;

        public Scrubber(Game game)
            : base(game)
        {
            Initialize();

            sliderBar = new pSliderBar(spriteManager, 0, 1, 0, new Vector2(padding, height), GameBase.WindowWidthScaled - padding * 2);
            sliderBar.ReadOnly = true;
            sliderBarUser = new pSliderBar(spriteManager, 0, 1, 0, new Vector2(padding, height), GameBase.WindowWidthScaled - padding * 2);
            sliderBarUser.ValueChanged += sliderBar_ValueChanged;
            sliderBarUser.seekbar1.InitialColour = Color.SkyBlue;
            sliderBarUser.seekbar1.Additive = true;
        }

        void sliderBar_ValueChanged(object sender, EventArgs e)
        {
            newAim = (int)(sliderBarUser.current * AudioEngine.AudioLength);
            if (newAim < AudioEngine.Time)
            {
                SliderOsu s = Player.Instance.hitObjectManager.hitObjects.Find(h => h is SliderOsu && h.StartTime <= newAim && h.EndTime >= newAim) as SliderOsu;
                if (s != null)
                    newAim = s.StartTime - 100;
            }
        }

        protected override void Dispose(bool disposing)
        {
            spriteManager.Dispose();
            sliderBar.Dispose();
            base.Dispose(disposing);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        internal void Draw()
        {
            if (!InputManager.ReplayMode || MatchSetup.Match != null || Player.Relaxing2)
                return;

            spriteManager.Draw();
        }

        public override void Update()
        {
            base.Update();
            sliderBar.SetValue((float)AudioEngine.Time / AudioEngine.AudioLength, true);

            if (newAim.HasValue)
            {
                int aim = newAim.Value;
                int diff = Math.Abs(aim - AudioEngine.Time);

                if (diff < 100)
                {
                    if (AudioEngine.IsReversed && Player.IsSliding) return;

                    AudioEngine.ChangeFrequencyRate(1);
                    newAim = null;
                }
                else if (aim > AudioEngine.Time)
                    AudioEngine.ChangeFrequencyRate(Math.Min(10, 0.8f + diff / 2000f));
                else
                    AudioEngine.ChangeFrequencyRate(Math.Max(-10, -0.2f - diff / 2000f));
            }
            else
                sliderBarUser.SetValue(sliderBar.current, true);
        }

    }
}
