﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameplayElements;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using System;
using System.Collections.Generic;
using System.Text;
using osu.Configuration;
using osu.GameplayElements.HitObjects;

namespace osu.GameModes.Play.Components
{
    internal class ScoreMeterError : ScoreMeter
    {
        protected float barHeight { get { return 3 * scale; } }

        protected pSprite arrow;

        float floatingError;
        private pSprite centre;

        internal virtual Vector2 CentrePosition { get { return new Vector2(GameBase.WindowWidthScaled / 2, GameBase.WindowHeightScaled - barHeight * 2); } }
        internal virtual float Width { get { return hitObjectManager.HitWindow50 * scale; } }
        internal float ErrorRange { get { return hitObjectManager.HitWindow50; } }

        internal ScoreMeterError(Game game, HitObjectManager hitObjectManager)
            : base(game, hitObjectManager)
        {
        }

        internal override void Initialize()
        {
            float left = CentrePosition.X - Width / 2;

            float piece = Width;

            pSprite bg = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.BottomCentre, Clocks.Audio, CentrePosition + new Vector2(-piece / 2, barHeight * 2), 0.79f, true, Color.Black);
            bg.Alpha = 0.6f;
            bg.VectorScale = new Vector2(piece, barHeight * 4) * 1.6f;
            spriteManager.Add(bg);

            piece = hitObjectManager.HitWindow300 / ErrorRange * Width;

            //left half
            pSprite p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.BottomCentre, Clocks.Audio, CentrePosition + new Vector2(-piece / 2, barHeight / 2), 0.82f, true, colour300);
            p.VectorScale = new Vector2(piece, barHeight) * 1.6f;
            spriteManager.Add(p);

            piece = hitObjectManager.HitWindow100 / ErrorRange * Width;

            p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.BottomCentre, Clocks.Audio, CentrePosition + new Vector2(-piece / 2, barHeight / 2), 0.81f, true, colour100);
            p.VectorScale = new Vector2(piece, barHeight) * 1.6f;
            spriteManager.Add(p);

            piece = hitObjectManager.HitWindow50 / ErrorRange * Width;

            p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.BottomCentre, Clocks.Audio, CentrePosition + new Vector2(-piece / 2, barHeight / 2), 0.80f, true, colour50);
            p.VectorScale = new Vector2(piece, barHeight) * 1.6f;
            spriteManager.Add(p);

            centre = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.BottomCentre, Clocks.Audio, CentrePosition + new Vector2(-0.75f, barHeight * 2), 0.83f, true, Color.White);
            centre.VectorScale = new Vector2(1.5f, barHeight * 4) * 1.6f;
            spriteManager.Add(centre);

            arrow = new pSprite(SkinManager.Load("editor-rate-arrow", SkinSource.Osu), Fields.TopLeft, Origins.BottomCentre, Clocks.Audio, CentrePosition + new Vector2(0, -3), 0.84f, true, Color.White);
            arrow.Scale = 0.6f * scale;
            spriteManager.Add(arrow);
        }

        internal override void AddMeter(HitObject h, int? customDelta)
        {
            float error = customDelta ?? AudioEngine.Time - h.StartTime;

            nextFadeTime = AudioEngine.Time + fade_delay;

            Show();

            if (error < 0)
                error = Math.Max(error, -ErrorRange);
            else
                error = Math.Min(error, ErrorRange);

            float pos = error / ErrorRange * (Width / 2);

            floatingError = floatingError * 0.8f + pos * 0.2f;

            arrow.MoveTo(new Vector2(CentrePosition.X + floatingError, arrow.InitialPosition.Y), 800, EasingTypes.Out);

            error = Math.Abs(error);

            const float width = 3;
            pSprite a = centre.Clone();
            a.Clock = Clocks.AudioOnce;
            a.AlwaysDraw = false;
            a.Depth = 0.85f;
            a.InitialColour = error <= hitObjectManager.HitWindow300 ? colour300 : (error <= hitObjectManager.HitWindow100 ? colour100 : colour50);
            a.Position.X = CentrePosition.X + pos;
            a.VectorScale.X = width;
            a.Alpha = 0.4f;
            a.Additive = true;
            a.FadeOut(10000);
            spriteManager.Add(a);
        }
    }
}
