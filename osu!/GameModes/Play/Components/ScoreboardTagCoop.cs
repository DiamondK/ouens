using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;

namespace osu.GameModes.Play.Components
{
    internal class ScoreboardTagCoop : Scoreboard
    {
        internal ScoreboardTagCoop(int displayCount, ScoreboardEntry tracking, Vector2 topLeft, bool startVisible, SlotTeams trackingTeam)
            : base(displayCount, tracking, topLeft, startVisible)
        {
            bool alignToRight = trackingTeam == SlotTeams.Red;

            arrow = new pSprite(SkinManager.Load(@"play-warningarrow", SkinSource.Osu),
                                alignToRight ? Fields.TopRight : Fields.TopLeft, Origins.Centre,
                                Clocks.Game, new Vector2(topLeft.X + 150, -100), 1, true,
                                GameBase.NewGraphicsAvailable ? Color.YellowGreen : Color.White, @"extra");
            Transformation bounce1 = new Transformation(TransformationType.MovementX, topLeft.X + 170, topLeft.X + 150,
                                                        GameBase.Time, GameBase.Time + 300, EasingTypes.In);
            bounce1.Loop = true;
            bounce1.LoopDelay = 300;
            Transformation bounce2 = new Transformation(TransformationType.MovementX, topLeft.X + 150, topLeft.X + 170,
                                                        GameBase.Time + 300, GameBase.Time + 600, EasingTypes.Out);
            bounce2.Loop = true;
            bounce2.LoopDelay = 300;

            arrow.Transformations.Add(bounce1);
            arrow.Transformations.Add(bounce2);

            if (!alignToRight)
                arrow.FlipHorizontal = true;

            arrow.Scale = 0.5f;

            spriteManager.Add(arrow);
        }

        private pSprite arrow;

        internal override bool Reorder(bool instant)
        {
            return false;
        }

        internal override void SetTrackingEntry(ScoreboardEntry tracking)
        {
            trackingEntry = tracking;
        }

        static Color colourActive = new Color(163, 222, 69, 160);
        static Color colourInactive = new Color(89, 122, 36, 160);

        internal void SetActivePlayer(ScoreboardEntry active)
        {
            foreach (ScoreboardEntry s in Entries)
                s.spriteBackground.FadeColour(active == s ? colourActive : colourInactive, 70);

            if (active != null)
            {
                arrow.Transformations.RemoveAll(t => t.Type == TransformationType.MovementY);
                arrow.Transformations.Add(new Transformation(TransformationType.MovementY, arrow.Position.Y, active.spriteBackground.Position.Y + offset.Y / 2 - 6, GameBase.Time, GameBase.Time + 100, EasingTypes.Out));

                arrow.FadeIn(50);
            }
            else
                arrow.FadeOut(500);
        }

        public void SetActiveAll()
        {
            foreach (ScoreboardEntry s in Entries)
                s.spriteBackground.FadeColour(colourActive, 70);
        }
    }
}