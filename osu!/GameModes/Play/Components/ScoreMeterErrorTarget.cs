﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameplayElements;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;

namespace osu.GameModes.Play.Components
{
    class ScoreMeterErrorTarget : ScoreMeterError
    {
        public ScoreMeterErrorTarget(Game game, HitObjectManager hitObjectManager)
            : base(game, hitObjectManager)
        {

        }

        internal override void Initialize()
        {
            base.Initialize();

            float left = CentrePosition.X - Width / 2;

            float piece = Width;

            pSprite bg = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.BottomCentre, Clocks.Audio, CentrePosition + new Vector2(-piece / 2, -barHeight * 2), 0.79f, true, Color.Black);
            bg.Alpha = 0.4f;
            bg.VectorScale = new Vector2(piece, barHeight * 24) * 1.6f;
            spriteManager.Add(bg);

            HitCircleOsuTarget h = new HitCircleOsuTarget(hitObjectManager, new Vector2(256, 365), 0, false, HitObjectSoundType.None, 0); // Needs to associate this hitobject to a HitObjectManager! NullPointerException otherwise! (HitCircleOsu ctor)
            h.SetColour(Color.Red);
            h.SpriteCollection.ForEach(s =>
            {
                s.AlwaysDraw = true;
                s.Transformations.Clear();
                s.Alpha = 1;
            });

            spriteManager.Add(h.SpriteCollection);
        }

        internal override void AddMeter(HitObject h, int? customDelta)
        {
            HitCircleOsuTarget ht = h as HitCircleOsuTarget;

            pSprite hitPoint =
                        new pSprite(SkinManager.Load(@"particle" + h.scoreValue),
                                    Fields.Gamefield,
                                    Origins.Centre,
                                    Clocks.AudioOnce, new Vector2(256,365) + ht.TargetDistance, 1, false, Color.Black);
            hitPoint.FadeOutFromOne(60000);
            spriteManager.Add(hitPoint);

            base.AddMeter(h, customDelta);
        }
    }
}
