﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play.Rulesets;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements;
using osu.Graphics.Sprites;
using System;
using System.Collections.Generic;
using System.Text;

namespace osu.GameModes.Play.Components
{
    internal class ScoreMeterColourMania : ScoreMeterColour
    {
        protected override float scale
        {
            get
            {
                //mania aligns to the play bar, so scale should be fixed.
                return 1;
            }
        }
        internal ScoreMeterColourMania(Game game, HitObjectManager hitObjectManager)
            : base(game, hitObjectManager)
        {

        }

        internal override void Initialize()
        {
            float width = hitObjectManager.ManiaStage.Width - 2;
            pSprite bg = new pSprite(GameBase.WhitePixel, Fields.TopLeft, hitObjectManager.ManiaStage.Skin.UpsideDown ? Origins.BottomLeft : Origins.TopLeft, Clocks.Audio, 
                                     new Vector2(hitObjectManager.ManiaStage.Left + 1, hitObjectManager.ManiaStage.ScaleFlipPosition(0)), 0.8f, true, Color.Black);
            bg.VectorScale = new Vector2(width, iconWidth + 1) * 1.6f;
            spriteManager.Add(bg);

            int count = (int)(width / (iconWidth + 1));
            float space = (width - count * iconWidth - (count - 1)) * 1.0f / (count - 1);
            scoreIcons = new pSprite[count];
            float left = hitObjectManager.ManiaStage.Left;
            for (int i = 0; i < count; i++)
            {
                pSprite icon = new pSprite(GameBase.WhitePixel, Fields.TopLeft, hitObjectManager.ManiaStage.Skin.UpsideDown ? Origins.BottomLeft : Origins.TopLeft, Clocks.Audio, 
                                           new Vector2(left, hitObjectManager.ManiaStage.ScaleFlipPosition(0)), 0.9f, true, Color.Black);
                icon.VectorScale = new Vector2(iconWidth, iconWidth) * 1.6f;
                spriteManager.Add(icon);
                scoreIcons[i] = icon;
                left += iconWidth + 1 + space;
            }
        }
    }
}
