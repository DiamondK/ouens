﻿using System;
using System.Collections.Generic;
using System.Text;

namespace osu.GameModes.Play.Components
{
    /// <summary>
    /// A backing class for comments which are displayed overlaying replays.
    /// </summary>
    internal class Comment : IComparable<Comment>
    {
        internal int time;
        internal CommentTargets target;
        internal string format;
        internal string text;

        internal Comment(int time, CommentTargets target, string format, string comment)
        {
            this.time = time;
            this.target = target;
            this.format = format;
            this.text = comment;
        }

        internal int DisplayTime
        {
            get
            {
                return 4000 + Math.Min(5000, text.Length * 60);
            }
        }


        #region IComparable<Comment> Members

        public int CompareTo(Comment other)
        {
            return this.time.CompareTo(other.time);
        }

        #endregion
    }
}
