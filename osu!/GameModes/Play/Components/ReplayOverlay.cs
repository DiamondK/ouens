﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Graphics.Sprites;
using osu_common.Libraries.NetLib;
using osu.Configuration;
using osu.Input;
using Microsoft.Xna.Framework.Graphics;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.UserInterface;
using osu.Audio;
using osu.Graphics.Notifications;
using osu.Online;
using osu.GameplayElements.Scoring;
using osu_common;
using osu.Graphics.Skinning;
using osu_common.Helpers;
using System.Windows.Forms;
using osu.Helpers;

namespace osu.GameModes.Play.Components
{
    internal enum CommentTargets
    {
        None,
        Map,
        Song,
        Replay
    }

    internal class ReplayOverlay : DrawableGameComponent
    {
        private SpriteManager spriteManager;
        private pTextBox commentInputTextbox;

        internal pSprite buttonFF;
        private pSprite buttonEndReplay;
        private pSprite buttonComments;

        private bool allowComments { get { return BeatmapManager.Current.BeatmapId > 0; } }

        public ReplayOverlay(Player player)
            : base(GameBase.Game)
        {
            this.player = player;
        }

        const string DEFAULT_MESSAGE = "Click to add a comment at the current time!";
        public override void Initialize()
        {
            spriteManager = new SpriteManager(true);
            spriteManager.FirstDraw = false;

            //We only want to show this stuff if we are watching a replay for the time being.
            if (!InputManager.ReplayMode)
                return;

            InitializeMarquee();

            InitializeButtons();

            if (allowComments)
                InitializeCommentSystem();

            base.Initialize();
        }

        pSprite commentBackgroundTop;
        pSprite commentBackgroundMiddle;
        pSprite commentBackgroundBottom;
        pSprite commentColourPicker;

        private void InitializeCommentSystem()
        {
            commentBackgroundTop = new pSprite(SkinManager.Load(@"commentbox", SkinSource.Osu), Fields.TopRight,
                                Origins.TopRight,
                                Clocks.Game,
                                new Vector2(70, buttonComments.Position.Y - 24), 0.95f, true, Color.TransparentWhite, "play");
            commentBackgroundTop.DrawHeight = 52;
            commentBoxSprites.Add(commentBackgroundTop);
            spriteManager.Add(commentBackgroundTop);

            commentBackgroundMiddle = new pSprite(SkinManager.Load(@"commentbox", SkinSource.Osu), Fields.TopRight,
                                Origins.TopRight,
                                Clocks.Game,
                                new Vector2(70, buttonComments.Position.Y - 24), 0.95f, true, Color.TransparentWhite, "play");
            commentBackgroundMiddle.DrawHeight = 1;
            commentBackgroundMiddle.DrawTop = 52;
            commentBackgroundMiddle.VectorScale = new Vector2(1, 0);
            commentBackgroundMiddle.Position.Y += 52 * 0.625f;
            commentBoxSprites.Add(commentBackgroundMiddle);
            spriteManager.Add(commentBackgroundMiddle);

            commentBackgroundBottom = new pSprite(SkinManager.Load(@"commentbox", SkinSource.Osu), Fields.TopRight,
                                Origins.TopRight,
                                Clocks.Game,
                                new Vector2(70, buttonComments.Position.Y - 24 + 52 * 0.625f), 0.95f, true, Color.TransparentWhite, "play");
            commentBackgroundBottom.DrawHeight = 49;
            commentBackgroundBottom.DrawTop = 53;
            commentBoxSprites.Add(commentBackgroundBottom);
            spriteManager.Add(commentBackgroundBottom);

            float width = commentBackgroundTop.DrawWidth * 0.625f;
            const int padding = 6;

            commentInputTextbox = new pTextBox(DEFAULT_MESSAGE, 12, new Vector2(commentBackgroundTop.CurrentPositionActual.X - width + padding, commentBackgroundTop.Position.Y + 17 + padding), width - padding * 4, 1);

            commentInputTextbox.BackgroundUnfocused = new Color(51, 197, 234, 150);
            commentInputTextbox.Box.Alpha = 0;
            commentInputTextbox.Box.BorderWidth = 0;
            commentInputTextbox.OnGotFocus += textBox_OnGotFocus;
            commentInputTextbox.CommitOnLeave = false;
            commentInputTextbox.OnCommit += textBox_OnCommit;
            commentInputTextbox.LengthLimit = 80;
            commentInputTextbox.OnChange += new pTextBox.OnCommitHandler(commentInputTextbox_OnChange);
            commentBoxSprites.AddRange(commentInputTextbox.SpriteCollection);
            spriteManager.Add(commentInputTextbox.SpriteCollection);

            cMap = new pCheckbox("Difficulty", 0.7f, new Vector2(commentBackgroundTop.CurrentPositionActual.X - 250, commentBackgroundTop.CurrentPositionActual.Y + 5), 1, false);
            spriteManager.Add(cMap.SpriteCollection);

            cMap.OnCheckChanged += new CheckboxCallbackDelegate(choice_OnCheckChanged);
            cMap.Hide(true);
            cMap.Tooltip = "Comment on this particular difficulty";
            commentBoxSprites.AddRange(cMap.SpriteCollection);
            spriteManager.Add(cMap.SpriteCollection);

            cSong = new pCheckbox("Song", 0.7f, new Vector2(commentBackgroundTop.CurrentPositionActual.X - 170, commentBackgroundTop.CurrentPositionActual.Y + 5), 1, false);
            spriteManager.Add(cSong.SpriteCollection);

            cSong.OnCheckChanged += new CheckboxCallbackDelegate(choice_OnCheckChanged);
            cSong.Tooltip = "Comment on this collection of beatmaps or the song";
            cSong.Hide(true);
            commentBoxSprites.AddRange(cSong.SpriteCollection);
            spriteManager.Add(cSong.SpriteCollection);

            cReplay = new pCheckbox("Player", 0.7f, new Vector2(commentBackgroundTop.CurrentPositionActual.X - 90, commentBackgroundTop.CurrentPositionActual.Y + 5), 1, false);
            spriteManager.Add(cReplay.SpriteCollection);

            cReplay.Tooltip = "Comment on the current replay (the specific person playing)";
            cReplay.OnCheckChanged += new CheckboxCallbackDelegate(choice_OnCheckChanged);
            cReplay.Hide(true);

            if (InputManager.ReplayScore.isOnline)
            {
                commentBoxSprites.AddRange(cReplay.SpriteCollection);
                spriteManager.Add(cReplay.SpriteCollection);
            }

            originalCheckboxPosition = new Vector2(commentBackgroundBottom.CurrentPositionActual.X - 250, commentBackgroundBottom.CurrentPositionActual.Y + 5);
            showCommentsCheckbox = new pCheckbox("Show Comments", 0.7f, originalCheckboxPosition, 1, ConfigManager.sShowReplayComments);
            spriteManager.Add(showCommentsCheckbox.SpriteCollection);

            showCommentsCheckbox.OnCheckChanged += showComments_OnCheckChanged;
            showCommentsCheckbox.Hide(true);
            commentBoxSprites.AddRange(showCommentsCheckbox.SpriteCollection);
            spriteManager.Add(showCommentsCheckbox.SpriteCollection);

            commentColourPicker = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.Centre, Clocks.Game, originalCheckboxPosition + new Vector2(124, 1), 0.96f, true, Color.TransparentWhite);
            commentColourPicker.Alpha = 0;
            commentColourPicker.VectorScale = new Vector2(20, 20);
            commentColourPicker.HandleInput = true;
            commentColourPicker.OnClick += (obj, ev) =>
            {
                if (!Player.Paused && !InputManager.ReplayStreaming)
                {
                    AudioEngine.TogglePause();
                    Player.Paused = true;
                    commitStartTime = AudioEngine.Time;
                }
                if (GameBase.graphics.IsFullScreen)
                {
                    NotificationManager.ShowMessage("Exit fullscreen mode to use the colour pick feature.", Color.Red, 2000);
                    return;
                }
                ColorDialog cd = new ColorDialog();
                if (cd.ShowDialog(GameBase.Form) == DialogResult.OK)
                {
                    commentColourPicker.InitialColour = new Color(cd.Color.R, cd.Color.G, cd.Color.B);
                }
                if (!InputManager.ReplayStreaming)
                {
                    AudioEngine.TogglePause();
                    Player.Paused = false;
                }
            };

            if (GameBase.User != null && (GameBase.User.Permission & Permissions.Supporter) > 0)
            {
                commentBoxSprites.Add(commentColourPicker);
                spriteManager.Add(commentColourPicker);
                pText pt = new pText("Colour", 15 * 0.7f, originalCheckboxPosition + new Vector2(94, 2), 0.96f, true, Color.TransparentWhite);
                commentBoxSprites.Add(pt);
                spriteManager.Add(pt);
            }
            if (ConfigManager.sShowReplayComments)
                RequestComments();
        }

        void commentInputTextbox_OnChange(pTextBox sender, bool newText)
        {
            UpdateCommentWindowSizing();
        }

        private void UpdateCommentWindowSizing()
        {
            //fix up sizing...
            float fixupAmount = (commentInputTextbox.Box.MeasureText().Y - 13.6f);
            commentBackgroundMiddle.VectorScale.Y = fixupAmount * 1.6f;
            commentBackgroundBottom.Position.Y = commentBackgroundBottom.InitialPosition.Y + fixupAmount;

            //bug here:showCommentsCheckbox jump upwards much more than fixupAmount
            //showCommentsCheckbox.Position = originalCheckboxPosition + new Vector2(0, fixupAmount);
        }

        void choice_OnCheckChanged(object sender, bool status)
        {
            if (sender == cReplay)
            {
                //check we have an online replay...
                if (!InputManager.ReplayScore.isOnline)
                    NotificationManager.ShowMessage("This replay is not stored online.  You can't comment on local replays.", Color.Red, 4000);
                else
                    commentTarget = CommentTargets.Replay;
            }
            else if (sender == cSong)
            {
                commentTarget = CommentTargets.Song;
            }
            else if (sender == cMap)
            {
                commentTarget = CommentTargets.Map;
            }

            cReplay.SetStatusQuietly(commentTarget == CommentTargets.Replay);
            cSong.SetStatusQuietly(commentTarget == CommentTargets.Song);
            cMap.SetStatusQuietly(commentTarget == CommentTargets.Map);
        }

        private void RequestComments()
        {
            if (commentsRequested) return;

            commentsRequested = true;

            FormNetRequest fnr = new FormNetRequest(General.WEB_ROOT + "/web/osu-comment.php");
            fnr.AddField("u", ConfigManager.sUsername);
            fnr.AddField("p", ConfigManager.sPassword);
            fnr.AddField("b", BeatmapManager.Current.BeatmapId.ToString());
            fnr.AddField("s", BeatmapManager.Current.BeatmapSetId.ToString());
            fnr.AddField("r", InputManager.ReplayScore.onlineId.ToString(GameBase.nfi));
            fnr.AddField("m", ((int)InputManager.ReplayScore.playMode).ToString(GameBase.nfi));
            fnr.AddField("a", "get");

            fnr.onFinish += comment_IncomingCommentInfo;

            NetManager.AddRequest(fnr);
        }

        void showComments_OnCheckChanged(object sender, bool status)
        {
            RequestComments();
            ConfigManager.sShowReplayComments.Value = status;
            commentSprites.ForEach(p => p.Bypass = !status);
        }

        /// <summary>
        /// Overlay buttons which allow controlling of the replay.
        /// </summary>
        private void InitializeButtons()
        {
            Transformation tr = new Transformation(Color.White, new Color(255, 247, 197), 0, 100);


            int overlayY = 85;

            if (allowComments)
            {
                buttonComments =
                        new pSprite(SkinManager.Load(@"overlay-discussion", SkinSource.Osu), Fields.TopRight,
                                    Origins.Centre,
                                    Clocks.Game,
                                    new Vector2(40, overlayY), 1, true, Color.White, "play");
                buttonComments.FadeInFromZero(500);
                buttonComments.HoverEffect = tr;
                buttonComments.HandleInput = true;
                buttonComments.OnClick += buttonComments_OnClick;
                spriteManager.Add(buttonComments);

                overlayY += 20;
            }

            buttonFF =
                new pSprite(SkinManager.Load(@"overlay-1x", SkinSource.Osu), Fields.TopRight,
                            Origins.Centre,
                            Clocks.Game,
                            new Vector2(40, overlayY), 1, true, Color.White, "play");
            buttonFF.FadeInFromZero(500);
            buttonFF.HoverEffect = tr;
            buttonFF.HandleInput = true;
            buttonFF.OnClick += buttonFF_OnClick;
            spriteManager.Add(buttonFF);

            overlayY += 20;

            //Replay overlay controls.
            buttonEndReplay =
                new pSprite(SkinManager.Load(@"overlay-endreplay", SkinSource.Osu), Fields.TopRight,
                            Origins.Centre, Clocks.Game,
                            new Vector2(40, overlayY), 1, true, Color.White, "play");
            buttonEndReplay.FadeInFromZero(500);
            buttonEndReplay.HoverEffect = tr;
            buttonEndReplay.HandleInput = true;
            buttonEndReplay.OnClick += buttonEndReplay_OnClick;
            spriteManager.Add(buttonEndReplay);

            overlayY += 20;
        }

        bool commentBoxShown;
        List<pSprite> commentBoxSprites = new List<pSprite>();
        List<pSprite> commentSprites = new List<pSprite>();

        void buttonComments_OnClick(object sender, EventArgs e)
        {
            commentBoxShown = !commentBoxShown;
            if (commentBoxShown)
            {
                commentBoxSprites.ForEach(p => p.FadeIn(100));
                buttonComments.Texture = SkinManager.Load(@"overlay-discussion2", SkinSource.Osu);
            }
            else
            {
                commentBoxSprites.ForEach(p => p.FadeOut(100));
                buttonComments.Texture = SkinManager.Load(@"overlay-discussion", SkinSource.Osu);
            }
        }

        /// <summary>
        /// Display a looping scrolling marquee with information about the current play.
        /// </summary>
        private void InitializeMarquee()
        {
            //Scrolling spectator marquee
            string info;
            if (InputManager.ReplayStreaming)
                info = "SPECTATOR MODE - Watching " + StreamingManager.CurrentlySpectating.Name + " play " +
                       BeatmapManager.Current.DisplayTitle;
            else
            {
                info = "REPLAY MODE - Watching " + InputManager.ReplayScore.playerName + " play " +
                       BeatmapManager.Current.DisplayTitle;
            }

            pText p = new pText(info, 13, new Vector2(320, 460), Vector2.Zero, 0.4f, true, Color.White, true);
            Transformation t = new Transformation(TransformationType.Fade, 0, 1, GameBase.Time, GameBase.Time + 4000);
            p.Transformations.Add(t);
            t =
                new Transformation(new Vector2(GameBase.WindowWidthScaled, 60), new Vector2(-p.MeasureText().X, 60),
                                   GameBase.Time, GameBase.Time + (int)(p.MeasureText().X * 52));
            t.Loop = true;
            p.Transformations.Add(t);
            spriteManager.Add(p);
        }

        private void buttonEndReplay_OnClick(object sender, EventArgs e)
        {
            StreamingManager.StopSpectating(true);
            player.Status = PlayerStatus.ExitingSpectator;
        }

        private void buttonFF_OnClick(object sender, EventArgs e)
        {
            player.ToggleFF();
            if (InputManager.ReplayModeFF == ReplaySpeed.Double)
                buttonFF.Texture = SkinManager.Load(@"overlay-2x", SkinSource.Osu);
            else if (InputManager.ReplayModeFF == ReplaySpeed.Normal)
                buttonFF.Texture = SkinManager.Load(@"overlay-1x", SkinSource.Osu);
            else if (InputManager.ReplayModeFF == ReplaySpeed.Half)
                buttonFF.Texture = SkinManager.Load(@"overlay-half", SkinSource.Osu);
        }

        CommentTargets commentTarget;

        void textBox_OnGotFocus(pTextBox sender, bool newText)
        {
            if (sender.Text == DEFAULT_MESSAGE)
            {
                sender.Text = string.Empty;
            }

            if (!Player.Paused)
            {
                AudioEngine.TogglePause();
                Player.Paused = true;
                commitStartTime = AudioEngine.Time;
            }
        }

        void textBox_OnCommit(pTextBox sender, bool newText)
        {
            if (commentTarget == CommentTargets.None && newText)
            {
                NotificationManager.ShowMessageMassive("Select a target for your comment first!", 1500);
                commentInputTextbox.Select();
                return;
            }

            if (newText && commentInputTextbox.Text.Length > 0)
            {
                FormNetRequest fnr = new FormNetRequest(General.WEB_ROOT + "/web/osu-comment.php");
                fnr.AddField("u", ConfigManager.sUsername);
                fnr.AddField("p", ConfigManager.sPassword);

                fnr.AddField("s", BeatmapManager.Current.BeatmapSetId.ToString());
                fnr.AddField("b", BeatmapManager.Current.BeatmapId.ToString(GameBase.nfi));
                fnr.AddField("m", ((int)InputManager.ReplayScore.playMode).ToString(GameBase.nfi));
                fnr.AddField("r", InputManager.ReplayScore.onlineId.ToString(GameBase.nfi));
                fnr.AddField("target", commentTarget.ToString().ToLower());
                //append colour here
                if (commentColourPicker.InitialColour != Color.White && commentColourPicker.InitialColour != Color.TransparentWhite)
                {
                    string colour = ColourHelper.Color2Hex(commentColourPicker.InitialColour);
                    fnr.AddField("f", colour);
                }


                fnr.AddField("a", "post");
                fnr.AddField("starttime", commitStartTime.ToString());
                fnr.AddField("comment", commentInputTextbox.Text);

                fnr.onFinish += delegate
                {
                    GameBase.Scheduler.Add(delegate
                    {
                        increaseCount(commentTarget);
                        updateCounts();

                        commentInputTextbox.Text = "Please wait before commenting again...";
                        NotificationManager.ShowMessage("Your comment has been submitted!", Color.Orange, 3000);
                    });
                };

                NetManager.AddRequest(fnr);

                commentInputTextbox.Enabled = false;
                commentInputTextbox.Text = "Sending...";
            }

            if (commentInputTextbox.Text.Length == 0)
                commentInputTextbox.Text = DEFAULT_MESSAGE;

            UpdateCommentWindowSizing();

            if (Player.Paused)
            {
                AudioEngine.TogglePause();
                Player.Paused = false;
            }
        }

        int commitStartTime;

        const int START_HEIGHT_TOP = 80;
        const int START_HEIGHT_BOTTOM = 420;
        private Player player;

        bool commentsRequested;
        private pCheckbox cMap;
        private pCheckbox cSong;
        private pCheckbox cReplay;

        int totalM = 0;
        int totalR = 0;
        int totalS = 0;

        float topHeight = START_HEIGHT_TOP;
        int topLastEndTime = 0;

        float bottomHeight = START_HEIGHT_BOTTOM;
        int bottomLastEndTime = 0;

        pList<Comment> comments = new pList<Comment>();

        int processed = -1;
        void comment_IncomingCommentInfo(string _result, Exception e)
        {
            GameBase.Scheduler.Add(delegate
            {
                if (processed < 0)
                {
                    foreach (string line in _result.Split('\n'))
                    {
                        if (line.Length == 0) continue;

                        string[] split = line.Split('\t');
                        if (split.Length < 4) continue;
                        int time;
                        Int32.TryParse(split[0], System.Globalization.NumberStyles.Integer, GameBase.nfi, out time);
                        if (time == 0) continue;
                        CommentTargets target = (CommentTargets)Enum.Parse(typeof(CommentTargets), split[1], true);
                        string formatString = split[2];
                        string comment = split[3];
                        comments.AddInPlace(new Comment(time, target, formatString, comment));
                    }

                    processed = 0;

                    GameBase.Scheduler.Add(processComments, true);
                }
            });
        }

        private void processComments()
        {
            if (comments == null || comments.Count == 0)
                return;

            Comment c = comments[processed];
            int displayTime = c.DisplayTime;

            increaseCount(c.target);

            bool top = c.target != CommentTargets.Replay;

            if (top)
            {
                if (topLastEndTime == 0 || topLastEndTime < c.time || topHeight > 300)
                {
                    topHeight = START_HEIGHT_TOP;
                    topLastEndTime = c.time + displayTime;
                }
            }
            else
            {
                if (bottomLastEndTime == 0 || bottomLastEndTime < c.time || bottomHeight < 300)
                {
                    bottomHeight = START_HEIGHT_BOTTOM;
                    bottomLastEndTime = c.time + displayTime;
                }
            }

            pText p = new pText(c.text, 14, new Vector2(GameBase.WindowWidthScaled / 2, 240), 0.8f, false, Color.White);
            p.Origin = Origins.TopCentre;
            p.TextBold = true;
            p.TextShadow = true;
            bool scrollIn = true;

            string[] formats = c.format.Split('|');
            switch (formats[0])
            {
                case "player":
                    p.TextColour = new Color(33, 246, 22);
                    scrollIn = false;
                    p.TextSize = 18;
                    p.Position = new Vector2(GameBase.WindowWidthScaled / 2, 440);
                    displayTime = Math.Min(5000, Math.Max(3000, displayTime));
                    break;
                case "creator":
                    p.TextColour = new Color(51, 224, 255);
                    p.TextSize = 18;
                    scrollIn = false;
                    p.Position = new Vector2(GameBase.WindowWidthScaled / 2, 40);
                    displayTime = Math.Min(5000, Math.Max(3000, displayTime));
                    break;
                case "bat":
                    p.TextColour = Color.OrangeRed;
                    p.TextSize = 20;
                    break;
                case "subscriber":
                    p.TextColour = new Color(255, 223, 46);
                    p.TextSize = 17;
                    break;
            }
            if (formats.Length > 1)
            {
                p.TextColour = ColourHelper.Hex2Color(formats[1]);
            }

            Vector2 measured = p.MeasureText();

            p.Clock = Clocks.Audio;

            if (scrollIn)
            {
                Vector2 right = new Vector2(GameBase.WindowWidthScaled + measured.X / 2, top ? topHeight : bottomHeight);
                Vector2 middle = new Vector2(GameBase.WindowWidthScaled / 2, top ? topHeight : bottomHeight);
                Vector2 left = new Vector2(-measured.X / 2, top ? topHeight : bottomHeight);
                //p.Transformations.Add(new Transformation(right, middle, c.time, c.time + displayTime / 3, EasingTypes.In));
                //p.Transformations.Add(new Transformation(middle, left, c.time + displayTime / 3 * 2, c.time + displayTime, EasingTypes.Out));
                p.Transformations.Add(new Transformation(right, left, c.time, c.time + displayTime, EasingTypes.None));
                if (top)
                    topHeight += measured.Y;
                else
                    bottomHeight -= measured.Y;
            }
            else
            {
                p.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, c.time, c.time + 200));
                p.Transformations.Add(new Transformation(TransformationType.Scale, 1.8f, 1f, c.time, c.time + 200, EasingTypes.Out));
                p.Transformations.Add(new Transformation(TransformationType.Fade, 1, 1, c.time + displayTime, c.time + displayTime + 100));
            }

            commentSprites.Add(p);
            spriteManager.Add(p);

            if (!ConfigManager.sShowReplayComments)
                p.Bypass = true;

            processed++;

            if (processed == comments.Count)
                updateCounts();
            else
                GameBase.Scheduler.Add(processComments, true);
        }

        private void updateCounts()
        {
            cMap.Text = string.Format("Difficulty ({0:#,0})", totalM);
            cReplay.Text = string.Format("Player ({0:#,0})", totalR);
            cSong.Text = string.Format("Song ({0:#,0})", totalS);
        }

        private void increaseCount(CommentTargets commentTarget)
        {
            switch (commentTarget)
            {
                case CommentTargets.Map:
                    totalM++;
                    break;
                case CommentTargets.Replay:
                    totalR++;
                    break;
                case CommentTargets.Song:
                    totalS++;
                    break;
            }
        }

        protected override void Dispose(bool disposing)
        {
            spriteManager.Dispose();
            if (commentInputTextbox != null)
            {
                commentInputTextbox.Dispose();
            }

            base.Dispose(disposing);
        }

        public override void Draw()
        {
            if (GameBase.Tournament) return;

            spriteManager.Draw();
            base.Draw();
        }

        const int COMMENT_SPAM_DELAY = 5000;
        private pCheckbox showCommentsCheckbox;
        private Vector2 originalCheckboxPosition;

        public override void Update()
        {
            if (commentInputTextbox != null && !commentInputTextbox.Enabled && AudioEngine.Time - commitStartTime > COMMENT_SPAM_DELAY)
            {
                commentInputTextbox.Text = DEFAULT_MESSAGE;
                commentInputTextbox.Enabled = true;
            }

            base.Update();
        }

        internal void PushButtonsDownwards(int p)
        {
            int amount = 55;

            if (buttonFF != null)
                buttonFF.MoveToRelative(new Vector2(0, amount), 500, EasingTypes.Out);
            if (buttonEndReplay != null)
                buttonEndReplay.MoveToRelative(new Vector2(0, amount), 500, EasingTypes.Out);
            if (buttonComments != null)
                buttonComments.MoveToRelative(new Vector2(0, amount), 500, EasingTypes.Out);

            commentBoxSprites.ForEach(s => { s.MoveToRelative(new Vector2(0, amount), 500, EasingTypes.Out); s.InitialPosition.Y += amount; });
        }
    }
}
