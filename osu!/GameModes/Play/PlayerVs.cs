﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Online;
using osu.GameModes.Play.Components;
using osu.GameModes.Play.Components.Multiplayer;
using osu.GameModes.Play.TeamRulesets;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Online;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu.GameplayElements.Events;
using osu.Helpers;
using osu_common.Helpers;

namespace osu.GameModes.Play
{
    internal class PlayerVs : Player
    {
        internal static bool AllPlayersLoaded;
        internal static bool AllPlayersCompleted;
        internal static ClientSideMatch Match;
        internal static bool scoreboardUpdatePending;
        internal List<ScoreboardEntryExtended> scoreEntries = new List<ScoreboardEntryExtended>();
        internal List<Vector2>[] HpGraphCollection = new List<Vector2>[bMatch.MAX_PLAYERS];
        //this is a hack to get the score entries to the ranking screen
        internal static List<ScoreboardEntryExtended> lastScoreEntries;
        private bool confirm;
        internal bool MultiFailed;
        private bool MultiPass;
        internal pText multiplayerRankingText;
        internal MultiRuleset MultiRuleset;
        internal new static PlayerVs Instance;

        private bool MultiSkipRequested;
        internal int localPlayerMatchId;
        internal static bool TeamMode;
        private bScoreFrame lastValidScoreFrame;
        public static TeamplayOverlay TeamOverlay;

        public override bool HideMouse
        {
            get
            {
                return AllPlayersLoaded && base.HideMouse;
            }
        }

        internal PlayerVs(ClientSideMatch matchInfo)
        {
            Match = matchInfo;
            Mode = Match.playMode;
            TeamMode = matchInfo.TeamMode;

            switch (Match.matchTeamType)
            {
                case MatchTeamTypes.HeadToHead:
                    MultiRuleset = new MultiRulesetVs(this);
                    break;
                case MatchTeamTypes.TeamVs:
                    MultiRuleset = new MultiRulesetTeamVs(this);
                    break;
                case MatchTeamTypes.TagCoop:
                case MatchTeamTypes.TagTeamVs:
                    MultiRuleset = new MultiRulesetTagCoop(this);
                    break;
            }

            Instance = this;

            NotificationManager.ClearMessageMassive(null, null);
        }

        internal override bool DoSkip()
        {
            if (OutroSkippable)
            {
                base.DoSkip();
                return true;
            }

            if (queueSkipCount > 0)
            {
                NotificationManager.ClearMessageMassive(null, null);
                MultiSkipRequested = false;

                clearSkippedStatus();

                base.DoSkip();
                return true;
            }

            if (!MultiSkipRequested && (AudioEngine.Time < SkipBoundary) && Status == PlayerStatus.Intro)
            {
                MultiSkipRequested = true;
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.PlayerVs_SkipRequest), 2000);
                BanchoClient.SendRequest(RequestType.Osu_MatchSkipRequest, null);
            }

            return false;
        }

        private void clearSkippedStatus()
        {
            scoreEntries.ForEach(s =>
            {
                if (s != null)
                {
                    s.spriteSkip.FadeOut(100);
                    s.spriteSkip.AlwaysDraw = false;
                }
            });
        }

        protected override bool AllowFailShaderEffects
        {
            get
            {
                return false;
            }
        }

        protected override void IntroSkipBoundaryEnded()
        {
            clearSkippedStatus();

            base.IntroSkipBoundaryEnded();
        }

        public override void Initialize()
        {
            if (Match == null)
            {
                GameBase.ChangeMode(OsuModes.Lobby);
                return;
            }

            lastScoreEntries = scoreEntries;
            if (GameBase.LastMode != OsuModes.Play)
            {
                AllPlayersLoaded = false;
                AllPlayersCompleted = false;
                queueSkipCount = 0;
            }

            scoreboardUpdatePending = false;

            if ((Match.specialModes & MultiSpecialModes.FreeMod) == 0)
                ModManager.ModStatus = Match.activeMods;

            base.Initialize();
        }

        protected override void OnLoadStart()
        {
            base.OnLoadStart();

            multiplayerRankingText = new pText(string.Empty, 80, new Vector2(638, 410), Vector2.Zero, 0, true,
                                               new Color(255, 255, 255, 100), false);
            multiplayerRankingText.Origin = Origins.TopRight;
            hitObjectManager.spriteManager.Add(multiplayerRankingText);

            if (!AllPlayersLoaded) NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.PlayerVs_WaitingForPlayers), 300000);

            if (Match.beatmapChecksum != BeatmapManager.Current.BeatmapChecksum)
            {
                //getting here when hitting mp invites in spectate mode.
                GameBase.ChangeMode(OsuModes.Lobby);
                return;
            }

            MultiRuleset.Initialize();

            for (int i = 0; i < bMatch.MAX_PLAYERS; i++)
            {
                //initialize HpGraph
                HpGraphCollection[i] = new List<Vector2>();

                if (Match.slotStatus[i] == SlotStatus.Quit)
                    MultiRuleset.HandlePlayerLeft(i);
            }
        }

        protected override void Dispose(bool disposing)
        {
            Instance = null;
            if (TeamOverlay != null) TeamOverlay.Hide();
            InitializeScoreGraphs();
            lastScoreEntries = new List<ScoreboardEntryExtended>(scoreEntries);

            scoreEntries = null;

            MultiRuleset.Dispose();

            if (GameBase.Mode != OsuModes.RankingTagCoop && GameBase.Mode != OsuModes.RankingTeam && GameBase.Mode != OsuModes.RankingVs)
            {
                //Have exited from the game without reaching the ranking screen.
                //Dispose of static items we would be transferring to the ranking screen usually.
                DisposeStatic();
            }

            base.Dispose(disposing);
        }

        public static void DisposeStatic()
        {
            GameBase.Scheduler.Add(delegate
            {
                if (ScoreBoard != null)
                {
                    ScoreBoard.Dispose();
                    ScoreBoard = null;
                }

                if (TeamOverlay != null)
                {
                    TeamOverlay.Dispose();
                    TeamOverlay = null;
                }

                lastScoreEntries = null;

                if (GameBase.Mode != OsuModes.Play)
                    Match = null;
            });
        }

        protected override bool OnLoadComplete(bool success)
        {
            BanchoClient.SendRequest(RequestType.Osu_MatchLoadComplete, null);
            return base.OnLoadComplete(success);
        }

        protected override void OnFailed()
        {
            MultiRuleset.OnFail();
        }

        internal override void OnFailableHp()
        {
            if (ModManager.CheckActive(Mods.NoFail)) return;

            if (MultiRuleset.ContinuousPlay)
            {
                if (!Ruleset.Fail(true))
                    return;

                if (MultiFailed) return;

                MultiFailed = true;
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.PlayerVs_FailedButKeepGoing), 3000);
                BanchoClient.SendRequest(RequestType.Osu_MatchFailed, null);
                currentScore.AllowSubmission = false;
                return;
            }

            if (!Failed)
            {
                BanchoClient.SendRequest(RequestType.Osu_MatchFailed, null);
                //We failed...

                SendMapComplete();
                //And won't be playing anymore.

                Ruleset.Fail(false);
            }
        }

        protected override bool exit()
        {
            bool performExit = false;

            int activeCount = 0;
            for (int i = 0; i < bMatch.MAX_PLAYERS; i++)
            {
                if (Match.slotStatus[i] == SlotStatus.Ready || Match.slotStatus[i] == SlotStatus.Playing || Match.slotStatus[i] == SlotStatus.Complete)
                    activeCount++;
            }

            if (activeCount < 2)
            {
                SendMapComplete();
                currentScore.AllowSubmission = false;
                GameBase.ChangeMode(OsuModes.MatchSetup);
                performExit = true;
            }
            else if (confirm || !Loader.Loaded)
            {
                MatchSetup.LeaveGame();
                performExit = true;
            }

            if (performExit) NotificationManager.ClearMessageMassive();

            return performExit;
        }

        internal override void TogglePause()
        {
            if (exit())
                return;

            NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.PlayerVs_MultiplayerExit));
            confirm = true;
            return;
        }

        protected override void InitializeScoreboard()
        {
            localPlayerMatchId = Match.findPlayerFromId(GameBase.User.Id);

            if (localPlayerMatchId < 0)
            {
                ErrorSubmission.Submit(new OsuError(new Exception("local user id:" + GameBase.User.Id + "\n" + "Match contains:" + Match.slotId[0] + @"," + Match.slotId[1] + @"," + Match.slotId[2] + @"," + Match.slotId[3] + @"," + Match.slotId[4] + @"," + Match.slotId[5] + @"," + Match.slotId[6] + @"," + Match.slotId[7])) { Feedback = "local player wasnt in the mp match" });
                MatchSetup.LeaveGame();
                return;
            }

            scoreEntries.Clear();

            ScoreBoard = MultiRuleset.CreateScoreboard();
            if (Ruleset.ScoreboardOnRight)
                ScoreBoard.DisplayOnRight = true;
            ScoreBoard.AllowTies = true;

            MultiRuleset.PopulateScoreboard();

            if (TeamMode)
                TeamOverlay = new TeamplayOverlay(this, ScoreBoard.spriteManager);

            base.InitializeScoreboard();
        }

        public override void Draw()
        {
            if (Failed) //We don't wanna blur stuff because you can still play after a fail.
                BloomRenderer.ExtraPass = false;

            base.Draw();
        }

        protected override void DrawScoreboardLevelItems()
        {
            base.DrawScoreboardLevelItems();

            if (TeamMode && TeamOverlay != null)
                TeamOverlay.Draw();
        }

        protected override bool CheckAllNotesHit
        {
            get
            {
                return MultiRuleset.CheckAllNotesHit;
            }
        }

        protected override bool AllowLoad
        {
            get
            {
                return base.AllowLoad && AllPlayersLoaded;
            }
        }

        bool hasStarted;

        public override void Update()
        {
            if (!BanchoClient.Connected && GameBase.FadeState == FadeStates.Idle)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Lobby_Bancho_Fail));
                GameBase.ChangeMode(OsuModes.Menu);
            }

            if (!AllPlayersLoaded)
            {
                //still waiting for more players to finish loading.
                UpdateLoading();
                return;
            }
            else
            {
                if (UpdateLoading()) return;
            }

            if (!hasStarted)
            {
                //ensure we clear all messages before starting gameplay.
                hasStarted = true;
                NotificationManager.ClearMessageMassive(null, null);
            }

            if (!Passed && AudioEngine.Paused)
                NotificationManager.ClearMessageMassive(null, null);

            if (MultiFailed)
                MultiRuleset.Recover();

            base.Update();

            if (Passed)
                SendMapComplete();

            /* Even though this *is* called in the base.Update() method, if some players lag behind to the point where those waiting
             * are in a paused state (i think) then the scoreboard updates will not be handled correctly.
             * We want to make sure the scoreboard is updated no matter what so it is in the right order.
             */
            if (scoreboardUpdatePending)
                UpdateScoreboard();

            if (MultiPass && GameBase.FadeState == FadeStates.Idle)
                DoPass(); //Force calling this more often in the case an outro skip has been requested.


            MultiRuleset.Update();

            SendScoreChange();
        }

        int lastScoreChangeSend;
        bool pendingScoreChange;

        protected override void OnScoreChanged()
        {
            if (InputManager.ReplayMode)
                return;

            lastValidScoreFrame = GetScoreFrame();
            lastValidScoreFrame.pass = !MultiFailed;

            lastValidScoreFrame.tagByte = PendingScoreChangeObject != null ? (int)PendingScoreChangeObject.Type : 1;
            PendingScoreChangeObject = null;
            pendingScoreChange = true;

            SendScoreChange();
        }

        private void SendScoreChange()
        {
            if (!pendingScoreChange || GameBase.Time - lastScoreChangeSend < 400) return;

            pendingScoreChange = false;
            lastScoreChangeSend = GameBase.Time;

            BanchoClient.SendRequest(RequestType.Osu_MatchScoreUpdate, lastValidScoreFrame);
        }

        int lastScoreboardUpdate = 0;

        string dotdotdot = @"...";

        protected override void UpdateScoreboard()
        {
            if (!GameBase.SixtyFramesPerSecondFrame)
            {
                return;
            }

            if (Passed || Failed || (!EventManager.BreakMode && !ScoreBoard.AlwaysVisible))
            {
                if (ScoreBoard.spriteManager.Alpha > 0)
                    ScoreBoard.spriteManager.Alpha = Math.Max(0, ScoreBoard.spriteManager.Alpha - 0.04f);
            }
            else if (ScoreBoard.spriteManager.Alpha < 1)
            {
                ScoreBoard.spriteManager.Alpha = Math.Min(1, ScoreBoard.spriteManager.Alpha + 0.04f);
            }

            if (scoreboardUpdatePending)
            {
                if (GameBase.Time - lastScoreboardUpdate < 1000)
                    return; //no updates for the first 5 seconds, and only one per second max.

                if (AudioEngine.Time - hitObjectManager.hitObjects[0].EndTime < 10000)
                {
                    multiplayerRankingText.Text = dotdotdot;
                    return;
                }

                lastScoreboardUpdate = GameBase.Time;

                if (ScoreBoard.Reorder(false) || multiplayerRankingText.Text == dotdotdot)
                    MultiRuleset.UpdateRankingText();
                scoreboardUpdatePending = false;
            }
        }

        internal void ResetRankingText()
        {
            multiplayerRankingText.Text = dotdotdot;
        }

        internal static void MatchScoreUpdate(bScoreFrame frame)
        {
            GameBase.Scheduler.Add(delegate
            {
                if (Instance == null) return;

                Instance.MultiRuleset.HandleFrame(frame);


                if (TeamMode)
                    TeamOverlay.UpdateScores();

                //quick fix for getting hpgraphs into multiplayer
                if (Instance.hitObjectManager != null &&
                    Instance.hitObjectManager.hitObjects != null &&
                    Instance.HpGraphCollection[frame.id] != null
                    )
                {
                    //get the hitobject for the incoming score.
                    HitObject O = Instance.hitObjectManager.hitObjects.FindLast(h => frame.time < h.EndTime);
                    if (O != null)
                    {
                        Instance.HpGraphCollection[frame.id].Add(new Vector2(frame.time,
                                            (float)(Math.Min(1, frame.currentHp / O.MaxHp))));
                    }

                }
            });


        }

        internal static void MatchPlayerLeft(int i)
        {
            Match.slotStatus[i] = SlotStatus.Quit;
            if (Instance != null)
                Instance.MultiRuleset.HandlePlayerLeft(i);
        }

        internal static void MatchPlayerFailed(int i)
        {
            GameBase.Scheduler.Add(delegate
            {
                if (Instance == null) return;
                Instance.MultiRuleset.HandlePlayerFailed(i);
            });
        }

        internal static void MatchPlayerSkipped(int i)
        {
            GameBase.Scheduler.Add(delegate
            {
                if (Instance == null) return;
                Instance.MultiRuleset.HandlePlayerSkipped(i);
            });
        }


        internal void InitializeScoreGraphs()
        {
            //add hpGraphs to all the players scores
            //we dont do it directly when recieving them because
            //the score is reconstructed every scoreframe

            for (int i = scoreEntries.Count - 1; i >= 0; i--)
            {
                ScoreboardEntryExtended entry = scoreEntries[i];
                if (entry != null && entry.Score != null)
                    entry.Score.hpGraph = HpGraphCollection[i];
            }
        }

        internal override void GotoRanking(bool force = false)
        {
            if (AllPlayersCompleted || force)
            {
                //force a reorder before going to ranking screen.
                ScoreBoard.Reorder(false);

                if (TeamOverlay != null)
                {
                    GameBase.ChangeMode(OsuModes.RankingTeam);
                    NotificationManager.ClearMessageMassive(null, null);

                }
                else
                {
                    MultiRuleset.LoadRanking();
                }
            }
            else
            {
                //gotoranking will never be called twice, so when we get here it gets stuck.
                if (!MultiPass)
                {
                    MultiPass = true;
                    SendMapComplete();
                    NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.PlayerVs_WaitingForFinish), 300000);
                }
            }
        }

        private bool MapCompletePacketSent;
        internal void SendMapComplete()
        {
            if (!MapCompletePacketSent)
            {
                BanchoClient.SendRequest(RequestType.Osu_MatchComplete, null);
                MapCompletePacketSent = true;
            }

        }

        protected override void ProcessPassed()
        {
            MultiRuleset.OnCompletion();
            base.ProcessPassed();
        }

        internal static void MatchComplete()
        {
            AllPlayersCompleted = true;
        }

        internal override Score VisibleScore
        {
            get
            {
                return MultiRuleset.visibleScore;
            }
        }

        internal static bool TeamFailed(SlotTeams Team)
        {
            if (lastScoreEntries == null)
                return false;

            foreach (ScoreboardEntry S in lastScoreEntries)
                if (S != null && S.Team == Team && S.Passing == true)
                    return false;
            return true;
        }
    }
}
