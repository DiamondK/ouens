#region Using Statements

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play.Components;
using osu.GameModes.Play.Rulesets;
using osu.GameModes.Play.Rulesets.Fruits;
using osu.GameModes.Play.Rulesets.Osu;
using osu.GameModes.Play.Rulesets.Taiko;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Notifications;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using Un4seen.Bass;
using System.Threading;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.IO;
using osu.GameModes.Play.Rulesets.Mania;
using osu.Input.Drawable;
using osu.Graphics.UserInterface;
using osu_common.Helpers;
using osu.GameModes.Online;
using osu.GameModes.Select;
using osu.Online.Social;

#endregion

namespace osu.GameModes.Play
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    internal partial class Player : DrawableGameComponent
    {
        internal static bool Failed;
        internal static int FailTime;
        internal static bool HasSkipped;
        internal static Player Instance;
        internal static bool IsSliding;
        internal static bool IsSpinning;
        private static PlayModes mode;

        internal static bool Passing = true;
        internal static bool Paused;
        internal static bool Playing;
        protected static int queueSkipCount = 0;
        internal static bool Recovering;
        private bool IsTutorial;
        internal static int RetryCount;
        internal static int PlayCount;
        internal static int Seed = 0;//used for random mod

        public static bool Relaxing;
        public static bool Relaxing2;

        public static pText ReplayBufferText;
        internal static Scoreboard ScoreBoard;
        internal static int StreamSyncPoint;
        internal readonly List<pSprite> s_arrows = new List<pSprite>();
        internal HitObject ActiveHitObject;

        private long datetimeCheckTimeInitial = -1;
        private int dateTimeCheckCount;
        private int datetimeCheckTimeComp;

        internal float bloomBurst;
        internal bool breakAccountedFor;
        private int burstSampleNumber;

        internal static bool wasAllowingSubmission = true;

        private VisualSettings visualSettings;
        private Scrubber scrubber;

        private string currentProgressString;
        private int currentUpdateFrame;
        private bool edgeStartBurstPending;
        internal EventManager eventManager;
        protected bool failedPreviousFrame;
        private FireRenderer fireRenderer;
        private int flameFlashLast;
        internal bool forceReplayFrame;
        private bool HasSkippedOutro;
        internal HitObjectManager hitObjectManager;
        private int LastPause;
        private static bool leadInActive;
        protected int missPreviousFrame;
        private Vector2 mouseVelocity;
        private Vector2 mouseVelocityLastPosition;
        private bool OnlineDataPending;
        internal static bool OutroActive;

        internal bool Passed;
        internal pSprite pauseCursor;
        private pSprite pauseCursorText;
        private Vector2 pauseLocation;
        private List<pSprite> pauseSprites;
        private ProgressBar progressBar;
        private int progressStringLastPosition = -1;
        private bool ReportedEndGame;
        private int resumeAfterBreakTime = -1;
        internal static bool Retrying;
        internal Ruleset Ruleset;
        private pSprite s_breakFail;
        private pSprite s_breakPass;

        /// <summary>
        /// Time before the NEXT break when drain stops.
        /// </summary>
        private Queue<int> breakStopTimes;


        private string scoreChecksum;
        private Int64 scoreChecksumNumerical;
        protected internal ScoreboardEntry scoreEntry;
        protected int scorePreviousFrame;
        private pAnimation skip;
        internal int SkipBoundary;
        internal bool SkipHovered { get { return skip != null && skip.Hovering; } }
        internal int CountdownTime;

        protected pText spectatorList;
        int spectatorListUpdate;
        public static SpriteManager SpriteManagerLoading;
        internal SpriteManager spriteManager;
        internal SpriteManager spriteManagerAdd;
        internal SpriteManager spriteManagerBelowHitObjectsWidescreen;
        internal SpriteManager spriteManagerBelowScoreboardWidescreen;
        internal SpriteManager spriteManagerMetadata;
        internal SpriteManager spriteManagerInterface;
        internal SpriteManager spriteManagerInterfaceWidescreen;
        internal SpriteManager spriteManagerPauseScreen;
        internal SpriteManager spriteManagerHighest;
        internal PlayerStatus Status = PlayerStatus.Busy;
        internal pTexture t_star2;
        private bool UnpauseConfirmed;
        internal static bool Unpausing;

        private int lastHitTime = int.MinValue;
        private int firstHitTime = int.MaxValue;
        protected InputOverlay inputOverlay;

        #region Combo and HP

        internal static Score currentScore;

        public virtual bool HideMouse
        {
            get
            {
                Ruleset r = Ruleset;
                return r != null && r.AllowMouseHide && !Paused && (visualSettings == null || !visualSettings.Activated);
            }
        }

        /// <summary>
        /// We are in the bound of time where HP can be drained (if necessary)
        /// </summary>
        internal bool DrainTime;

        private bool InitialAllowSubmission;

        bool inputReceivedAtLeastOnce;

        #endregion

        internal Player()
            : base(GameBase.Game)
        {
            Instance = this;

        }

        internal static PlayModes Mode
        {
            get { return mode; }
            set
            {
                if (value != mode)
                {
                    mode = value;

                    //todo: holy SHIT this is inefficient.
                    if (BeatmapManager.Beatmaps != null)
                        BeatmapManager.Beatmaps.ForEach(b =>
                            {
                                b.Scores.Clear();
                                b.onlinePersonalScore = null;
                            });

                }
            }
        }

        internal static bool IsTargetPracticeMode
        {
            get
            {
                return (ModManager.CheckActive(Mods.Target) && !InputManager.ReplayMode)
                        || (InputManager.ReplayScore != null && ModManager.CheckActive(InputManager.ReplayScore.enabledMods, Mods.Target));
            }
        }

        /// <summary>
        /// Gets a value indicating whether the skip boundary period is currently active.
        /// Not thread-safe.
        /// </summary>
        public static bool SkipBoundaryActive
        {
            get
            {
                if (Instance == null)
                    return false;
                return Instance.SkipBoundary > 0 && AudioEngine.Time < Instance.SkipBoundary;
            }
        }

        PlayModes ModeOriginal;
        private bool replayModeStable = false;

        protected ASyncLoader Loader;

        private static bool warningMouseButtonsDisabled;
        private static bool warningInterfaceDisabled;

        public override void Initialize()
        {
            InitializeAntiCheat();

            GameBase.Instance.Activated += Game_ActivationChanged;
            GameBase.Instance.Deactivated += Game_ActivationChanged;

            //We have to initially set the correct playMode if watching a replay.
            if (InputManager.ReplayMode)
                Mode = InputManager.ReplayScore != null ? InputManager.ReplayScore.playMode : PlayModes.Osu;
            if (!InputManager.ReplayMode && !ModManager.CheckActive(Mods.Autoplay) && MatchSetup.Match == null)
            {
                //only update Seed here when entering single player.
                Seed = GameBase.Time;
            }
            //The beatmap might want to force us into a specific play mode.
            ModeOriginal = Mode;
            if (BeatmapManager.Current.PlayMode != PlayModes.Osu)
            {
                Mode = BeatmapManager.Current.PlayMode;

                //Because the osu! gamefield was moved, we should run this to set it to the correct location for taiko.
                //todo: Really, all the taiko constants should be modified instead, but this will take a bit of time.
                GameBase.ResizeGamefield(1, true);
            }

            if (ModManager.CheckActive(Mods.Cinema))
            {
                if (InputManager.ReplayMode)
                    ModManager.ModStatus &= ~(Mods.Cinema | Mods.Autoplay);
                else
                {
                    if (BeatmapManager.Current.PlayMode == PlayModes.Osu)
                        Mode = PlayModes.Osu;

                    //use most visually intense settings in all cases when playing with cinema mod.
                    EventManager.UserDimLevel = 0;
                    EventManager.ShowStoryboard = true;
                    EventManager.ShowVideo = true;
                    SkinManager.IgnoreBeatmapSkin = false;
                    AudioEngine.IgnoreBeatmapSamples = false;
                }
            }
            else if (!GameBase.TestMode)
            {
                //this restores defaults for disables for this map, so needs to be run early.
                visualSettings = new VisualSettings(GameBase.Game);
            }

            if (!warningMouseButtonsDisabled && ConfigManager.sMouseDisableButtons)
            {
                warningMouseButtonsDisabled = true;
                NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.InputManager_MouseButtonsDisabledWarning), BindingManager.For(Bindings.DisableMouseButtons)), Color.Beige, 10000);
            }

            if (!warningInterfaceDisabled && !ConfigManager.sShowInterface)
            {
                warningInterfaceDisabled = true;
                NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.Player_InterfaceDisabledWarning), @"Shift + " + BindingManager.For(Bindings.ToggleScoreboard)), Color.Beige, 10000);
            }

            ModManager.CheckForCurrentPlayMode();

            IsAutoplayReplay = false;

            //Set these to local settings that should be used until we are up to initialising the ruleset (using currentScore mods)
            Relaxing = ModManager.CheckActive(Mods.Relax) && !InputManager.ReplayMode;
            Relaxing2 = ModManager.CheckActive(Mods.Relax2) && !InputManager.ReplayMode;

            //Check if we need autoplay enabled, and setup a few prelim settings.
            if (GameBase.TestMode || ((ModManager.CheckActive(Mods.Autoplay) || Relaxing2) && !InputManager.ReplayMode))
            {
                IsAutoplayReplay = true;

                //Make sure we stop spectating here.
                StreamingManager.StopSpectating(false);

                if (!GameBase.TestMode && !Relaxing2)
                    InputManager.ReplayMode = true;
                InputManager.ReplayScore = ScoreFactory.Create(mode, @"osu!", BeatmapManager.Current);
                InputManager.ReplayScore.enabledMods = ModManager.ModStatus;

#if !Public
                //scrubber = new Scrubber(GameBase.Game);
#endif
            }

            //SpriteManager setup.
            spriteManager = new SpriteManager(true) { ForwardPlayOptimisations = true };
            spriteManagerBelowHitObjectsWidescreen = new SpriteManager(true) { ForwardPlayOptimisations = true, Alpha = 0 };
            spriteManagerInterface = new SpriteManager() { ForwardPlayOptimisations = true, Alpha = 0 };
            spriteManagerInterfaceWidescreen = new SpriteManager(true) { ForwardPlayOptimisations = true, Alpha = 0 };
            spriteManagerBelowScoreboardWidescreen = new SpriteManager(true) { Alpha = 0 };
            spriteManagerMetadata = new SpriteManager(true);
            spriteManagerPauseScreen = new SpriteManager(true) { ForwardPlayOptimisations = true, FirstDraw = false };
            spriteManagerHighest = new SpriteManager(true) { ForwardPlayOptimisations = true };
            spriteManagerAdd = new SpriteManager(true) { ForwardPlayOptimisations = true };

            //Reset some static vars.
            Paused = false;
            Failed = false;
            Recovering = false;
            IsSliding = false;
            IsSpinning = false;
            HasSkipped = false;
            leadInActive = true;
            Unpausing = false;
            OutroSkippable = false;
            OutroActive = false;
            KiaiActive = false;
            Loaded = false;
            replayModeStable = false;
            PendingRestart = false;
            IsTutorial = BeatmapManager.Current.Title == @"osu! tutorial";
            StreamSyncPoint = 0;
            FailTime = 0;

            if (GameBase.LastMode != OsuModes.Play)
                queueSkipCount = 0;

            if (SpriteManagerLoading == null)
                SpriteManagerLoading = new SpriteManager(true);

            loadingSpinner = pStatusDialog.CreateSpinner(SkinManager.RandomDefaultColour());
            loadingSpinner.FadeInFromZero(400);
            SpriteManagerLoading.Add(loadingSpinner);

            loadDelayAdded = !Retrying && !InputManager.ReplayMode && !GameBase.TestMode;
            reloadingAfterChange = GameBase.LastMode == OsuModes.Play;
            if (loadDelayAdded && visualSettings != null && visualSettings.CurrentMapHasBeenPlayed) visualSettings.Show(!InputManager.ReplayMode && StreamingManager.CurrentlySpectating == null, reloadingAfterChange);

            Loader = new ASyncLoader(OnLoadStart, OnLoadComplete);

            base.Initialize();

            GameBase.TransitionManager.AllowParallax = false;

            GameBase.LoadComplete();
        }

        private void Game_ActivationChanged(object sender, EventArgs e)
        {
            if (!Paused)
            {
                if (Playing && MatchSetup.Match == null)
                    TogglePause();
                else
                    return;
            }

            if (pauseSprites == null) return;

            foreach (pSprite p in pauseSprites.FindAll(s => s.Tag == @"back" || s.Tag == @"retry"))
            {
                if (GameBase.IsActive)
                {
                    GameBase.Scheduler.AddDelayed(delegate
                    {
                        p.HandleInput = true;
                        p.FadeColour(Color.White, 100);
                    }, 500);
                }
                else
                {
                    p.HandleInput = false;
                    p.FadeColour(Color.DarkGray, 100);
                }
            }
        }

        bool loadDelayAdded;

        List<pSprite> warnings = new List<pSprite>();

        private void InitializeWarnings()
        {
            if (BeatmapManager.Current.EpilepsyWarning && !Retrying && !GameBase.TestMode)
            {
                pSprite warning = new pSprite(SkinManager.Load(@"epilepsy", SkinSource.Osu), Fields.Centre, Origins.Centre, Clocks.Game, Vector2.Zero, 1, false, Color.TransparentWhite);
                warning.FadeIn(200);
                warning.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + 3000, GameBase.Time + 3400));
                spriteManagerMetadata.Add(warning);
                warnings.Add(warning);
            }
        }

        protected virtual void OnLoadStart()
        {
            Beatmap beatmap = BeatmapManager.Current;

            //if (!beatmap.InOszContainer && PlayMode &&
            //    (beatmap.SubmissionStatus == SubmissionStatus.Ranked ||
            //    beatmap.SubmissionStatus == SubmissionStatus.Approved) &&
            //    beatmap.ServerHasOsz2)
            //{
            //    beatmap.ConvertToOsz2(); //forced conversion
            //}

            //Initialize the mod-specific ruleset.
            switch (Mode)
            {
                case PlayModes.Taiko:
                    Ruleset = new RulesetTaiko(this);
                    break;
                case PlayModes.Osu:
                    if (IsTargetPracticeMode)
                        Ruleset = new RulesetOsuTarget(this);
                    else
                        Ruleset = new RulesetOsu(this);
                    break;
                case PlayModes.CatchTheBeat:
                    Ruleset = new RulesetFruits(this);
                    break;
                case PlayModes.OsuMania:
                    Ruleset = new RulesetMania(this);
                    break;
            }

            //Reset the score to make sure nothing odd goes on with it.
            ResetScore(beatmap);

            hitObjectManager = Ruleset.hitObjectManager;

#if OSZ2
            //if (!GameBase.TestMode && BeatmapManager.Current.SubmissionStatus >= SubmissionStatus.Ranked)
                BeatmapManager.Current.ConvertToOsz2();
#endif

            beatmap.ProcessHeaders();

            InitializeWarnings();

            // feel free to move code the if you know a better place
            if (!InputManager.ReplayMode)
                beatmap.DateLastPlayed = DateTime.Now;
            else
                replayModeStable = true; // to give a reliable bool at the dispose method

            Ruleset.InitializeSkin();

            bool audioLoadFailed = false;

            try
            {
                //Loads the audio stream into AudioEngine.
                //This is required to get correct readings from AudioEngine.ControlPoints (and anything else dependent on AudioEngine.loadedBeatmap)
                //so much be run before we load the file itself.
                InitializeAudioEngine();
            }
            catch (AudioNotLoadedException)
            {
                audioLoadFailed = true;
            }

            //We pass it to load the beatmap correctly for the current mode.
            hitObjectManager.SetBeatmap(beatmap, Player.currentScore.enabledMods);
            hitObjectManager.Load(false);

            //Make sure the beatmap loaded is sane.
            if (hitObjectManager.hitObjectsCount == 0 || (AudioEngine.ControlPoints != null && AudioEngine.ControlPoints.Count == 0))
            {
                GameBase.LoadComplete();
                exit();
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Player_CantLoadBeatmap));
                return;
            }
            foreach (HitObject n in hitObjectManager.hitObjects)
            {
                if (n.EndTime > lastHitTime)
                    lastHitTime = n.EndTime;
                if (n.StartTime < firstHitTime)
                    firstHitTime = n.StartTime;
            }

            //The hitObjectManager initializes an eventManager for us.
            eventManager = hitObjectManager.eventManager;

            //Seek points, lead-in etc.
            //Uses eventManager.
            InitializeAudioSpecifics();

            //Large initialization for the specific play mode.
            Ruleset.Initialize();

            Ruleset.SetAlpha(0);

            //Reset the score to make sure nothing odd goes on with it.
            ResetScore(beatmap);

            hitObjectManager.LoadEvents(true);

            //Make sure the audio loading is sane.
            if (AudioEngine.AudioTrack == null || AudioEngine.AudioTrack.Preview || audioLoadFailed)
            {
                GameBase.LoadComplete();
                if (StreamingManager.CurrentlySpectating != null) StreamingManager.StopSpectating(false);
                exit();
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Player_CantLoadAudio));
                return;
            }

            //Preload common sprites to save loading during gameplay.
            CacheFutureSprites();

            //Check at this point whether we are trying to playback an invalid replay.
            if (!IsAutoplayReplay && InputManager.ReplayMode && !InputManager.ReplayStreaming &&
                InputManager.ReplayScore.replay.Count == 0)
            {
                GameBase.LoadComplete();
                if (StreamingManager.CurrentlySpectating != null)
                    GameBase.ChangeMode(OsuModes.Rank);
                else
                    GameBase.ChangeMode(OsuModes.SelectPlay);

                InputManager.ReplayMode = false;

                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Player_CantLoadReplay));
                return;
            }

            //Force an early status update before spectator streaming info is sent.
            //Ensures the beatmap checksum is set correctly.
            BanchoClient.UpdateStatus();

            //Autoplay Replay
            if (IsAutoplayReplay)
            {
                Ruleset.CreateAutoplayReplay();

                if (Relaxing2 && !InputManager.ReplayMode)
                {
                    currentScore.replay = new List<bReplayFrame>();
                    currentScore.playerName = ConfigManager.sUsername;
                }
                else
                    InputManager.ReplayScore = currentScore;
            }

            //Basic replay initialisation.
            InitializeReplay();

            //Progress Bar.
            GameBase.Scheduler.Add(InitializeProgressBar);

            //Combo Fire.
            if (ConfigManager.sComboFire)
                fireRenderer = new FireRenderer();

            //Countdown Sequence.
            InitializeCountdown();

            //Skip Button.
            InitializeSkip();

            //Spectator List.
            InitializeSpectatorList();

            InitialAllowSubmission = AllowSubmissionConstantConditions;

            //Scoreboard
            InitializeScoreboard();

            //Break time arrows etc.
            InitializeBreaks();

            if (ConfigManager.sKeyOverlay || InputManager.ReplayMode)
            {
                switch (Mode)
                {
                    case PlayModes.Osu:
                        inputOverlay = new InputOverlay();
                        break;
                    case PlayModes.CatchTheBeat:
                        inputOverlay = new InputOverlay();
                        break;
                }
            }

            //Adjust the start location in the beatmap depending on certain criteria.
            AdjustPlayStartLocation();

            //Bind delegates to handle input/other events.
            BindEvents();

            if (visualSettings != null) visualSettings.CheckMapContent();

            if (loadDelayAdded && !reloadingAfterChange && Thread.CurrentThread != GameBase.MainThread)
                Thread.Sleep(this is PlayerVs ? 5000 : 950);

            //Send the currently playing map to the spectators
            if (StreamingManager.HasSpectators && !InputManager.ReplayMode && !Retrying
                && (BanchoClient.Permission & Permissions.Supporter) > 0
                && ConfigManager.sAutoSendNowPlaying.Value)
            {
                string m = ChatEngine.GenerateNowPlayingString();
                m = '\x1' + @"ACTION " + m.Substring(4) + '\x1';
                ChatEngine.SendMessage(m, @"#spectator", false);
            }

            Retrying = false;
        }

        protected virtual bool OnLoadComplete(bool success)
        {
            if (!success)
            {
                //something didn't load correctly, but this scenario should already be handled if we let the Update() method free.
                GameBase.ChangeMode(GameBase.LastMode != OsuModes.Play ? GameBase.LastMode : OsuModes.Menu, true);
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Player_CantLoadBeatmap), Color.Red, 5000);
                ErrorSubmission.Submit(new OsuError(Loader.LastException) { Feedback = BeatmapManager.Current.ContainingFolder });

                return true;
            }

            GameBase.SetTitle(BeatmapManager.Current.DisplayTitle);
            SetGCLowLatency(true);
            AsyncLoadComplete = true;

            if (SpriteManagerLoading != null) SpriteManagerLoading.SpriteList.ForEach(s => s.FadeOut(loadDelayAdded || loadingSpinner.Alpha > 0 ? 500 : 0));

            int time = 0;
            float dep = 0;
            int x = 610;
            bool hasMods = false;

            foreach (Mods m in Enum.GetValues(typeof(Mods)))
            {
                if (ModManager.CheckActive(currentScore.enabledMods, m))
                {
                    if (m == Mods.DoubleTime && ModManager.CheckActive(currentScore.enabledMods, Mods.Nightcore))
                        continue;
                    if (m == Mods.SuddenDeath && ModManager.CheckActive(currentScore.enabledMods, Mods.Perfect))
                        continue;

                    hasMods = true;
                    Transformation t2 =
                        new Transformation(TransformationType.Scale, 2, 1, GameBase.Time + time,
                                           GameBase.Time + time + 400);
                    t2.Easing = EasingTypes.Out;
                    Transformation t = new Transformation(TransformationType.Fade, 0, 1, GameBase.Time + time,
                                                          GameBase.Time + time + 400);
                    pSprite p =
                        new pSprite(SkinManager.Load(@"selection-mod-" + m.ToString().ToLower()),
                                    Fields.TopLeft,
                                    Origins.Centre,
                                    Clocks.Game,
                                    new Vector2(x, 94), 0.9F + dep, InputManager.ReplayMode,
                                    Color.TransparentWhite);
                    if (!InputManager.ReplayMode)
                        p.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + time,
                                                                 GameBase.Time + time + 5000));
                    p.Transformations.Add(t);
                    p.Transformations.Add(t2);
                    spriteManagerInterface.Add(p);

                    time += 500;
                    dep += 0.00001f;

                    x -= 10;
                }
            }

            Ruleset.LoadComplete();

            return false;
        }

        private void ClearWarnings()
        {
            foreach (pSprite p in warnings)
                p.FadeOut(200);
            warnings.Clear();
        }

        AudioTrack previewAudioTrack;
        float previewAudioTrackVolume;
        private void InitializeAudioEngine()
        {
            //must be set before loading audio so the samples are loaded correctly.
            AudioEngine.Nightcore = ModManager.CheckActive(currentScore.enabledMods, Mods.Nightcore);

            //setting this here is mainly for checksum matching. it gets re-set at InitializeAudioSpecificsCallback
            if (ModManager.CheckActive(currentScore.enabledMods, Mods.DoubleTime))
                AudioEngine.CurrentPlaybackRate = 150;
            else if (ModManager.CheckActive(currentScore.enabledMods, Mods.HalfTime))
                AudioEngine.CurrentPlaybackRate = 75;

            UpdateChecksum();
            //Update the checksum here (AudioRate has been changed above in the case of half/double time)

            previewAudioTrack = AudioEngine.AudioTrack;
            if (previewAudioTrack != null) previewAudioTrackVolume = previewAudioTrack.Volume;

            if (AudioEngine.LoadAudio(BeatmapManager.Current, false, false, false))
            {
                AudioEngine.Play(); //Starting to play is once helps reduce the lag on lead-in.  The start time gets reset next so this is fine in practice.
                AudioEngine.Stop();
            }
        }

        private void AdjustPlayStartLocation()
        {
            //Fast-forward to the current position and adjust vars.
            if ((GameBase.TestMode && GameBase.TestTime > hitObjectManager.hitObjects[0].StartTime)
                || InputManager.ReplayStartTime > 0)
            {
                AudioEngine.VirtualTimeAccumulated = leadInTime;

                for (int i = 0; i < hitObjectManager.hitObjectsCount; i++)
                    if (hitObjectManager.hitObjects[i].EndTime <= AudioEngine.Time)
                    {
                        hitObjectManager.hitObjects[i].IsHit = true;
                        Ruleset.HpBar.SetCurrentHp(hitObjectManager.hitObjects[i].MaxHp);
                    }

                KiaiActive = AudioEngine.ActiveControlPoint.kiaiMode;
            }
        }

        private void InitializeProgressBar()
        {
            if (ConfigManager.sProgressBarType != ProgressBarTypes.Off && ((!Relaxing && !Relaxing2)))
            {
                switch ((ProgressBarTypes)ConfigManager.sProgressBarType)
                {
                    case ProgressBarTypes.Pie:
                        progressBar = new ProgressBarCircular(spriteManagerInterfaceWidescreen, Ruleset.ScoreDisplay.leftOfDisplay);
                        break;
                    default:
                        progressBar = new ProgressBar(spriteManagerInterfaceWidescreen, ConfigManager.sProgressBarType, Ruleset.ScoreDisplay.ScoreFontHeight);
                        break;
                }
            }
        }

        protected virtual void InitializeBreaks()
        {
            breakStopTimes = new Queue<int>();

            if (eventManager.eventBreaks.Count > 0)
            {
                s_breakPass =
                    new pSprite(SkinManager.Load(@"section-pass"), Fields.Centre, Origins.Centre,
                                Clocks.Audio,
                                Vector2.Zero, 1F, true, Color.TransparentWhite, null);
                s_breakFail =
                    new pSprite(SkinManager.Load(@"section-fail"), Fields.Centre, Origins.Centre,
                                Clocks.Audio,
                                Vector2.Zero, 1F, true, Color.TransparentWhite, null);


                if (Ruleset.AllowWarningArrows)
                {
                    spriteManagerInterfaceWidescreen.Add(s_breakPass);
                    spriteManagerInterfaceWidescreen.Add(s_breakFail);
                    s_arrows.Add(
                        new pSprite(SkinManager.Load(@"play-warningarrow"), Fields.TopLeft, Origins.Centre,
                                    Clocks.Audio, new Vector2(80, 100), 1, false,
                                    SkinManager.UseNewLayout ? Color.Red : Color.White));

                    s_arrows.Add(
                        new pSprite(SkinManager.Load(@"play-warningarrow"), Fields.BottomLeft, Origins.Centre,
                                    Clocks.Audio, new Vector2(80, 100), 1, false,
                                    SkinManager.UseNewLayout ? Color.Red : Color.White));

                    s_arrows.Add(
                        new pSprite(SkinManager.Load(@"play-warningarrow"), Fields.TopRight, Origins.Centre,
                                    Clocks.Audio, new Vector2(80, 100), 1, false,
                                    SkinManager.UseNewLayout ? Color.Red : Color.White));

                    s_arrows.Add(
                        new pSprite(SkinManager.Load(@"play-warningarrow"), Fields.BottomRight, Origins.Centre,
                                    Clocks.Audio, new Vector2(80, 100), 1, false,
                                    SkinManager.UseNewLayout ? Color.Red : Color.White));

                    s_arrows[2].FlipHorizontal = true;
                    s_arrows[3].FlipHorizontal = true;

                    spriteManager.Add(s_arrows);
                }

                foreach (EventBreak b in eventManager.eventBreaks)
                {
                    if (Ruleset.AllowWarningArrows)
                    {
                        for (int i = 0; i < 1300; i += 200)
                        {
                            Transformation t1 =
                                new Transformation(TransformationType.Fade, 0, 1, b.EndTime - 1000 + i, b.EndTime - 1000 + i);
                            Transformation t2 =
                                new Transformation(TransformationType.Fade, 1, 0, b.EndTime - 900 + i, b.EndTime - 900 + i);

                            s_arrows.ForEach(a =>
                            {
                                a.Transformations.Add(t1);
                                a.Transformations.Add(t2);
                            });
                        }
                    }

                    // queue up drain stop times for each break.
                    if ((b.EndTime > AudioEngine.Time) && (b.StartTime > hitObjectManager.hitObjects[0].StartTime))
                        breakStopTimes.Enqueue(hitObjectManager.hitObjects[hitObjectManager.BreakObjectBefore(b)].EndTime);
                }
            }
        }

        private void InitializeMetadataDisplay()
        {
            if (IsTutorial)
                return;

            if (RetryCount > 1)
            {
                pText retryCount = new pText(string.Format(LocalisationManager.GetString(OsuString.Player_RetriesCounting), RetryCount), 14, new Vector2(0, 30), 1, false, Color.White);
                retryCount.TextBold = true;
                retryCount.MeasureText();
                retryCount.Transformations.Add(new Transformation(new Vector2(-20, 30), retryCount.InitialPosition, GameBase.Time + 1000, GameBase.Time + 1500, EasingTypes.Out));
                retryCount.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, GameBase.Time + 1000, GameBase.Time + 1500));
                //retryCount.Transformations.Add(new Transformation(retryCount.StartPosition,new Vector2(-20, 30), GameBase.Time + 3500, GameBase.Time + 4000, EasingTypes.In));
                retryCount.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + 3500, GameBase.Time + 4500));
                spriteManagerInterfaceWidescreen.Add(retryCount);
            }

            if (!InputManager.ReplayMode && RetryCount > 0 && RetryCount % 10 == 0)
                ChatEngine.AddUserLog(string.Format(LocalisationManager.GetString(OsuString.Userlog_RetryCount), RetryCount, BeatmapManager.Current.DisplayTitleFull, OsuCommon.PlayModeString(Mode)));
            if (!InputManager.ReplayMode && PlayCount > 0 && PlayCount % 50 == 0)
                ChatEngine.AddUserLog(string.Format(LocalisationManager.GetString(OsuString.Userlog_PlayCount), PlayCount));

            if (GameBase.TestMode || !Ruleset.AllowMetadataDisplay)
                return;

            if (string.IsNullOrEmpty(BeatmapManager.Current.OnlineDisplayTitle))
                return;

            string[] lines = BeatmapManager.Current.OnlineDisplayTitle.Split('\n');

            List<pText> lineSprites = new List<pText>();

            Vector2 pos = new Vector2(0, -90);

            int t1 = 200;

            int t2 = 3800 + lines.Length * 800;

            float rateAdjust = 1;

            foreach (string s in lines)
            {
                float size = 40;
                bool bold = true;
                Color colour = Color.White;
                int wait = 500;

                string thisline = s;

                if (s.Length == 0)
                    continue;
                if (s[0] == '[')
                {
                    string formatstr = s.Substring(1, s.IndexOf(']') - 1);
                    foreach (string subformat in formatstr.Split(','))
                    {
                        string[] keyval = subformat.Split(':');
                        if (keyval.Length != 2)
                            continue;
                        switch (keyval[0])
                        {
                            case @"size":
                                float.TryParse(keyval[1], out size);
                                break;
                            case @"bold":
                                bold = keyval[1] == @"1";
                                break;
                            case @"colour":
                                string[] col = keyval[1].Split('.');
                                if (col.Length == 3)
                                    colour = new Color(byte.Parse(col[0]), byte.Parse(col[1]), byte.Parse(col[2]));
                                break;
                            case @"wait":
                                wait = Int32.Parse(keyval[1]);
                                break;
                            case @"time":
                                t1 = Int32.Parse(keyval[1]);
                                t2 = t1 + 4000 + lines.Length * 800;
                                break;
                            case @"hold":
                                t2 = t1 + Int32.Parse(keyval[1]);
                                break;
                        }
                    }
                    thisline = s.Substring(s.IndexOf(']') + 1);
                }

                t1 += wait;
                t2 += wait;

                pText pt = new pText(thisline, size, pos, 0.87f, false, colour);
                pt.Clock = Clocks.Audio;
                pt.TextShadow = true;
                pt.TextBold = bold;
                pt.Field = Fields.Centre;
                pt.Origin = Origins.Centre;
                pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, (int)(t1 * rateAdjust), (int)((t1 + 500) * rateAdjust)));
                pt.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, (int)(t2 * rateAdjust), (int)((t2 + 500) * rateAdjust)));

                //draw title above columns

                spriteManagerMetadata.Add(pt);


                lineSprites.Add(pt);

                pt = new pText(thisline, size * 2f, pos, 0.86f, false, colour);
                pt.Clock = Clocks.Audio;
                pt.Additive = true;
                pt.TextShadow = true;
                pt.TextBold = bold;
                pt.Field = Fields.Centre;
                pt.Origin = Origins.Centre;
                pt.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.1f, (int)(t1 * rateAdjust), (int)((t1 + 500) * rateAdjust)));
                pt.Transformations.Add(new Transformation(TransformationType.Fade, 0.1f, 0, (int)(t2 * rateAdjust), (int)((t2 + 500) * rateAdjust)));

                spriteManagerMetadata.Add(pt);

                lineSprites.Add(pt);

                t1 += 200;
                pos.Y += 60 * (size / 40);
            }


            for (int i = 0; i < lineSprites.Count; i++)
            {
                bool right = ((i / 2) % 2 == 0);
                bool bg = (i % 2) == 1;
                pText p = lineSprites[i];

                float magnitude = p.MeasureText().X * 0.625f / 2;

                p.Position.Y -= (50 + (pos.Y / 2));

                if (right)
                    p.Transformations.Add(new Transformation(
                                              p.Position - new Vector2(bg ? magnitude : 50, 0),
                                              p.Position - new Vector2(bg ? -magnitude : -50, 0),
                                              p.Transformations[0].Time1, p.Transformations[1].Time2));
                else
                    p.Transformations.Add(
                        new Transformation(p.Position - new Vector2(bg ? -magnitude : -50, 0),
                                           p.Position - new Vector2(bg ? magnitude : 50, 0),
                                           p.Transformations[0].Time1, p.Transformations[1].Time2));
            }
        }

        protected virtual void InitializeSpectatorList()
        {
            spectatorList = new pText(string.Empty, 11, new Vector2(0, 80), 1, true, Color.White);
            spectatorList.TextShadow = true;

            if (GameBase.Tournament) return;

            if ((!InputManager.ReplayMode && ConfigManager.sShowSpectators) || InputManager.ReplayStreaming)
                spriteManagerHighest.Add(spectatorList);
        }

        private void BindEvents()
        {
            InputManager.Bind(InputEventType.OnClick, onClick);
            InputManager.Bind(InputEventType.OnGamePress, onGamePress);
            KeyboardHandler.OnKeyPressed += onKeyPressed;
            AudioEngine.ActiveInheritedTimingPointChanged += AudioEngine_ActiveInheritedTimingPointChanged;
            AudioEngine.OnNewSyncBeat += AudioEngine_OnNewSyncBeat;

            hitObjectManager.ForcedHit += hitObjectManager_ForcedHit;
        }

        internal bool NewSyncBeatWaiting;
        void AudioEngine_OnNewSyncBeat()
        {
            NewSyncBeatWaiting = true;
        }

        private void UnbindEvents()
        {
            KeyboardHandler.OnKeyPressed -= onKeyPressed;
            AudioEngine.ActiveInheritedTimingPointChanged -= AudioEngine_ActiveInheritedTimingPointChanged;
            AudioEngine.OnNewSyncBeat -= AudioEngine_OnNewSyncBeat;

            if (hitObjectManager != null)
                hitObjectManager.ForcedHit -= hitObjectManager_ForcedHit;
        }

        internal static bool KiaiActive;
        void AudioEngine_ActiveInheritedTimingPointChanged(ControlPoint tp)
        {
            if (KiaiActive != tp.kiaiMode)
            {
                KiaiActive = tp.kiaiMode;
                Ruleset.OnKiaiToggle(KiaiActive);
            }

            //hitObjectManager.hitObjects.ForEach(h => { h.SpriteCollection.ForEach(s => { s.Additive = KiaiActive; }); });
        }

        protected virtual void InitializeReplay()
        {
            if (!GameBase.TestMode && !Relaxing2 && !Relaxing)
                InputManager.ReplayMode = false;
            StreamingManager.Reset();
        }

        private void InitializeSkip()
        {
            skip = new pAnimation(SkinManager.LoadAll(@"play-skip"), Fields.TopRight,
                                  Origins.BottomRight,
                                  Clocks.Game,
                                  new Vector2(0, 480), GameBase.Widescreen ? 1 : 0, true, new Color(255, 255, 255, (byte)(255 * 0.6)));
            skip.SetFramerateFromSkin();
            skip.OnClick += skip_OnClick;
            skip.HandleInput = visualSettings == null;
            skip.HoverEffect = new Transformation(TransformationType.Fade, 0.6F, 1, 0, 300);
            skip.Alpha = 0;

            int proposedBoundary = SkipBoundary;

            //skipBoundary may already be initialised in cases a countdown is being used (see InitializeCountdown()).
            if (SkipBoundary == 0)
            {
                //ensure for cases with lead-in time that the skip boundary is initialised to negative audiotime.
                proposedBoundary = SkipBoundary = AudioEngine.Time;

                if (AudioEngine.BeatSyncing)
                    proposedBoundary = (int)(hitObjectManager.hitObjects[0].StartTime - AudioEngine.beatLength * (AudioEngine.beatLength < 500 ? 8 : 4));
                else
                    proposedBoundary = hitObjectManager.hitObjects[0].StartTime - 2000;
            }

            if (proposedBoundary - Math.Min(audioStartTime, -leadInTime) > 1200 + hitObjectManager.PreEmpt)
            {
                SkipBoundary = proposedBoundary;
                skip.FadeIn(400);

                spriteManagerHighest.Add(skip);

                Status = PlayerStatus.Intro;
            }
            else
            {
                Status = PlayerStatus.Playing;
                SkipBoundary = 0;
            }

            if (!InputManager.ReplayMode || (InputManager.ReplayStreaming && InputManager.ReplayStartTime == 0))
            {
                currentScore.replay.Insert(0, new bReplayFrame(0, 256, -500, pButtonState.None));
                currentScore.replay.Insert(1, new bReplayFrame(SkipBoundary - 1, 256, -500, pButtonState.None));
            }
            else if (queueSkipCount > 0)
            {
                DoSkip();
            }

        }

        protected void InitializeAudioSpecificsCallback(int startTime = 0)
        {
            if (Status == PlayerStatus.Exiting || Status == PlayerStatus.ExitingSpectator)
                return;

            AudioEngine.ResetAudioTrack();

            //Adjust audio speed depending on mods.
            AudioEngine.Nightcore = ModManager.CheckActive(currentScore.enabledMods, Mods.Nightcore);
            if (ModManager.CheckActive(currentScore.enabledMods, Mods.DoubleTime))
                AudioEngine.CurrentPlaybackRate = 150;
            else if (ModManager.CheckActive(currentScore.enabledMods, Mods.HalfTime))
                AudioEngine.CurrentPlaybackRate = 75;

            UpdateChecksum();

            AudioEngine.SetVolumeMusicFade(100);

            if (leadInTime > startTime)
                AudioEngine.StartVirtualTime(-leadInTime);
            else
            {
                AudioEngine.VirtualTimeAccumulated = leadInTime;
                AudioEngine.SeekTo(startTime);
            }

            FinishedInitialAudioSetup = true;
        }

        private void InitializeAudioSpecifics()
        {
            audioStartTime = 0;
            //Decide where to start playing the audio.
            if (GameBase.TestMode)
                audioStartTime = GameBase.TestTime;
            else if (InputManager.ReplayMode && InputManager.ReplayStartTime > 0)
                audioStartTime = InputManager.ReplayStartTime + 5;

            leadInTime = BeatmapManager.Current.AudioLeadIn;

            //Decide if we need a lead-in period (due to virtual storyboard).
            leadInTime = Math.Max(-eventManager.firstEventTime, leadInTime);

            //Provide an arbitrary lead-in time if the map hasn't specified one that is long enough.
            //Ignores PreEmpt in mania because it's ~doing it's own thing~ with it.
            int preempt = (mode == PlayModes.OsuMania) ? 1800 : Math.Max(1800, hitObjectManager.PreEmpt);
            int firstObjectTime = hitObjectManager.hitObjects.Count > 0 ? hitObjectManager.hitObjects[0].StartTime : 0;
            leadInTime = Math.Max(preempt - firstObjectTime, leadInTime);

            //Load the initial sampleset.
            if (BeatmapManager.Current.BeatmapVersion > 3 && AudioEngine.ActiveTimingPointIndex >= 0)
                AudioEngine.LoadSampleSet(AudioEngine.ControlPoints[AudioEngine.ActiveInheritedTimingPointIndex].sampleSet, false);

            //Alert user of local offset setting.
            if (BeatmapManager.Current.PlayerOffset != 0)
                NotificationManager.ShowMessageMassive(string.Format(LocalisationManager.GetString(OsuString.Player_LocalBeatmapOffset), BeatmapManager.Current.PlayerOffset), 1000);

            AudioEngine.UpdateActiveTimingPoint(true);
        }

        protected virtual void InitializeScoreboard()
        {
            rankType = ConfigManager.sRankType;

            if (BeatmapManager.Current.SubmissionStatus >= SubmissionStatus.Ranked && rankType != RankingType.Local)
            {
                if (BeatmapManager.Current.Scores == null || BeatmapManager.Current.Scores.Count == 0)
                {
                    BeatmapManager.Current.GetOnlineScores(true, false, ConfigManager.sRankType);
                    BeatmapManager.Current.OnGotOnlineScores += OnGotOnlineData;
                    return;
                }

                InitializeOnlineData();
                return;
            }

            //force local ranking should this map not be ranked.
            rankType = RankingType.Local;
            InitializeOnlineData();
        }

        private void OnGotOnlineData(object sender)
        {
            BeatmapManager.Current.OnGotOnlineScores -= OnGotOnlineData;
            GameBase.Scheduler.Add(InitializeOnlineData);
        }

        /// <summary>
        /// Much information about this beatmap has now been retrieved.
        /// Includes scores, title metadata etc.
        /// </summary>
        private void InitializeOnlineData()
        {
            //Metadata fly-by.
            InitializeMetadataDisplay();

            if (GameBase.Tournament || !Ruleset.AllowScoreboardDisplay || ((Relaxing || Relaxing2)))
                return;

            if (ScoreBoard == null)
            {
                List<Score> scores = BeatmapManager.Current.Scores != null && BeatmapManager.Current.Scores.Count > 0 ? BeatmapManager.Current.Scores : ScoreManager.FindScores(BeatmapManager.Current.BeatmapChecksum, Mode);

                if (scores == null || scores.Count == 0)
                    return;

                scoreEntry = new ScoreboardEntry(0, currentScore.playerName ?? @"-", Mode);
                scoreEntry.FixedScore = true;

                ScoreBoard = new Scoreboard(6, scoreEntry, Ruleset.ScoreboardPosition,
                                            InputManager.ReplayMode ? true : ConfigManager.sScoreboardVisible.Value);
                if (Ruleset.ScoreboardOnRight)
                    ScoreBoard.DisplayOnRight = true;
                ScoreBoard.ShowLeader = true;


                if (InputManager.ReplayMode || ConfigManager.sScoreboardVisible)
                    ScoreBoard.ShowStatus(string.Format(LocalisationManager.GetString(OsuString.Player_ToggleScoreboard), BindingManager.For(Bindings.ToggleScoreboard).ToString()));
                ScoreBoard.AutoColour = true;
                ScoreBoard.spriteManager.Alpha = 0;

                //ScoreBoard.AllowRankUpdates = false;

                ScoreboardEntry personal = null;

                foreach (Score s in scores)
                {
                    ScoreBoard.inputCount++;

                    bool isMe = s.playerName == scoreEntry.name;

                    if (rankType != RankingType.Local && isMe)
                    {
                        if ((BeatmapManager.Current.onlinePersonalScore != null && BeatmapManager.Current.onlinePersonalScore.totalScore == s.totalScore && rankType == RankingType.Top) || (InputManager.ReplayScore != null && s.date == InputManager.ReplayScore.date))
                            continue;
                        personal = new ScoreboardEntry(s.onlineRank, s, Mode);
                    }
                    else
                    {
                        ScoreboardEntry sbe = new ScoreboardEntry(s.onlineRank, s, Mode);
                        ScoreBoard.Add(sbe);
                    }
                }

                if (BeatmapManager.Current.onlinePersonalScore != null && (!InputManager.ReplayMode || InputManager.ReplayScore.playerName != GameBase.User.Name) && personal == null)
                    personal = new ScoreboardEntry(BeatmapManager.Current.onlinePersonalScore.onlineRank, BeatmapManager.Current.onlinePersonalScore, Mode);

                if (personal != null)
                {
                    personal.FixedScore = true;
                    personal.allowUpdateRank = false;

                    ScoreBoard.Add(personal);
                    personal.spriteBackground.InitialColour = new Color(255, 69, 0, 150);
                }

                foreach (ScoreboardEntry sbe in ScoreBoard.Entries)
                {
                    if (sbe.Score == null || sbe.Score.user == null)
                        continue;
                    if (ChatEngine.Friends.Contains(sbe.Score.user.Id))
                        sbe.spriteBackground.InitialColour = new Color(255, 97, 175, 180);
                }

                ScoreBoard.Reorder(false);
                ScoreBoard.Reposition(false);
            }
        }

        private void InitializeCountdown()
        {
            double firstCircle = hitObjectManager.hitObjects[0].StartTime;

            double offset = AudioEngine.beatOffsetAt(firstCircle);
            double beatLengthOriginal = AudioEngine.beatLengthAt(firstCircle);//in some case, the value is negative
            if (beatLengthOriginal < 0)
                beatLengthOriginal = AudioEngine.beatLengthAt(0);

            double goTime = offset - 5;
            double beatLength = beatLengthOriginal;

            //If the bpm is too fast let's double the length just because it seems sensible.
            if (beatLength <= 333)
                beatLength *= 2;

            //Apply user customisations.
            if (BeatmapManager.Current.Countdown == Countdown.DoubleSpeed)
                beatLength /= 2;
            else if (BeatmapManager.Current.Countdown == Countdown.HalfSpeed)
                beatLength *= 2;

            // Push the countdown back by the mapper's countdown offset before any other adjustments.
            firstCircle -= beatLength * BeatmapManager.Current.CountdownOffset;

            //Skip back until we are at a good place.
            if (goTime >= firstCircle)
                while (goTime > firstCircle - beatLength)
                    goTime -= beatLength;

            int divisor = 1;

            while (goTime < firstCircle - beatLength / divisor)
            {
                goTime += beatLength;
                double beat = ((float)(goTime - offset) / beatLengthOriginal) % 4;
                if (beat > 1.5 &&
                    beat < 3.5)
                    divisor = 2;
                else
                    divisor = 1;
            }
            goTime -= beatLength;

            bool useCountdown = goTime - 4 * beatLength > 0;

            int goTimeInt = (int)goTime;
            int beatLengthInt = (int)beatLength;

            bool playCountdownSounds = !ModManager.CheckActive(Mods.Target);

            if (ModManager.CheckActive(Mods.Target) || (useCountdown && BeatmapManager.Current.Countdown != Countdown.Disabled && Ruleset.AllowCountdown))
            {
                SkipBoundary = goTimeInt - 6 * beatLengthInt;
                CountdownTime = goTimeInt - 3 * beatLengthInt;

                pSprite ready =
                    new pSprite(SkinManager.Load(@"ready"), Fields.Centre, Origins.Centre,
                                Clocks.Audio, new Vector2(0, 0), 0.91f, false,
                                Color.TransparentWhite);

                if (playCountdownSounds)
                    AudioEngine.SampleEvents.Add(new SampleCacheItem((int)(goTimeInt - 5.9 * beatLengthInt), AudioEngine.LoadSample(@"readys"), 100, false));

                ready.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, goTimeInt - 6 * beatLengthInt, goTimeInt - 5 * beatLengthInt));
                if (!SkinManager.UseNewLayout)
                {
                    ready.Transformations.Add(new Transformation(TransformationType.Scale, 3, 1, goTimeInt - 6 * beatLengthInt, goTimeInt - 5 * beatLengthInt));
                    ready.Transformations.Add(new Transformation(TransformationType.Rotation, -0.4f, 0, goTimeInt - 6 * beatLengthInt, goTimeInt - 5 * beatLengthInt));
                }
                ready.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, goTimeInt - 4 * beatLengthInt, goTimeInt - 3 * beatLengthInt));
                ready.Transformations.Add(
                    new Transformation(TransformationType.Scale, 1, SkinManager.UseNewLayout ? 1.2f : 1.8f, goTimeInt - 4 * beatLengthInt,
                                       goTimeInt - 3 * beatLengthInt));

                pSprite count3 =
                    new pSprite(SkinManager.Load(@"count3"), SkinManager.UseNewLayout ? Fields.Centre : Fields.TopLeft, SkinManager.UseNewLayout ? Origins.Centre : Origins.TopLeft,
                                Clocks.Audio, new Vector2(0, 0), 0.9f, false,
                                Color.TransparentWhite);
                pSprite count2 =
                    new pSprite(SkinManager.Load(@"count2"), SkinManager.UseNewLayout ? Fields.Centre : Fields.TopRight, SkinManager.UseNewLayout ? Origins.Centre : Origins.TopRight,
                                Clocks.Audio, new Vector2(0, 0), 0.89f, false,
                                Color.TransparentWhite);
                pSprite count1 =
                    new pSprite(SkinManager.Load(@"count1"), Fields.Centre, Origins.Centre,
                                Clocks.Audio, new Vector2(0, 0), 0.88f, false,
                                Color.TransparentWhite);
                pSprite go =
                    new pSprite(SkinManager.Load(@"go"), Fields.Centre, Origins.Centre,
                                Clocks.Audio, new Vector2(0, 0), 0.91f, false,
                                Color.TransparentWhite);

                if (playCountdownSounds)
                {
                    if (AudioEngine.LoadSample(@"count3s") != -1)
                    {
                        AudioEngine.SampleEvents.Add(new SampleCacheItem(goTimeInt - 3 * beatLengthInt, AudioEngine.LoadSample(@"count3s"), 100, false));
                        AudioEngine.SampleEvents.Add(new SampleCacheItem(goTimeInt - 2 * beatLengthInt, AudioEngine.LoadSample(@"count2s"), 100, false));
                        AudioEngine.SampleEvents.Add(new SampleCacheItem(goTimeInt - 1 * beatLengthInt, AudioEngine.LoadSample(@"count1s"), 100, false));
                        AudioEngine.SampleEvents.Add(new SampleCacheItem(goTimeInt, AudioEngine.LoadSample(@"gos"), 100, false));
                    }
                    else
                    {
                        AudioEngine.SampleEvents.Add(new SampleCacheItem(goTimeInt - 3 * beatLengthInt, AudioEngine.LoadSample(@"count"), 100, false));
                        AudioEngine.SampleEvents.Add(new SampleCacheItem(goTimeInt - 2 * beatLengthInt, AudioEngine.LoadSample(@"count"), 100, false));
                        AudioEngine.SampleEvents.Add(new SampleCacheItem(goTimeInt - 1 * beatLengthInt, AudioEngine.LoadSample(@"count"), 100, false));
                        AudioEngine.SampleEvents.Add(new SampleCacheItem(goTimeInt, AudioEngine.LoadSample(@"count"), 100, false));
                    }
                }

                count3.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, goTimeInt - (int)(3.2 * beatLengthInt), goTimeInt - 3 * beatLengthInt));
                count2.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, goTimeInt - (int)(2.2 * beatLengthInt), goTimeInt - 2 * beatLengthInt));
                count1.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, goTimeInt - (int)(1.2 * beatLengthInt), goTimeInt - 1 * beatLengthInt));

                if (SkinManager.UseNewLayout)
                {
                    count3.Transformations.Add(new Transformation(TransformationType.Scale, 1.4f, 1, goTimeInt - (int)(3.2 * beatLengthInt), goTimeInt - 3 * beatLengthInt));
                    count2.Transformations.Add(new Transformation(TransformationType.Scale, 1.4f, 1, goTimeInt - (int)(2.2 * beatLengthInt), goTimeInt - 2 * beatLengthInt));
                    count1.Transformations.Add(new Transformation(TransformationType.Scale, 1.4f, 1, goTimeInt - (int)(1.2 * beatLengthInt), goTimeInt - 1 * beatLengthInt));

                    count3.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, goTimeInt - (int)(2.2 * beatLengthInt), goTimeInt - (int)(2.0 * beatLengthInt)));
                    count2.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, goTimeInt - (int)(1.2 * beatLengthInt), goTimeInt - (int)(1.0 * beatLengthInt)));
                    count1.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, goTimeInt - (int)(0.2 * beatLengthInt), goTimeInt - (int)(0.0 * beatLengthInt)));
                }
                else
                {
                    count3.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, goTimeInt - (int)(0.2 * beatLengthInt), goTimeInt + (int)(0.2 * beatLengthInt)));
                    count2.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, goTimeInt - (int)(0.2 * beatLengthInt), goTimeInt + (int)(0.2 * beatLengthInt)));
                    count1.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, goTimeInt - (int)(0.2 * beatLengthInt), goTimeInt + (int)(0.2 * beatLengthInt)));
                }

                if (SkinManager.UseNewLayout)
                {
                    go.Transformations.Add(new Transformation(TransformationType.Scale, 1.4f, 1, goTimeInt - (int)(0.6 * beatLengthInt), goTimeInt + (int)(0.2 * beatLengthInt)));
                    go.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, goTimeInt - (int)(0.2 * beatLengthInt), goTimeInt - 0 * beatLengthInt));
                }
                else
                {
                    go.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, goTimeInt - (int)(0.6 * beatLengthInt), goTimeInt));

                    Transformation t = new Transformation(TransformationType.Rotation, -4, 0, goTimeInt - (int)(0.6 * beatLengthInt), goTimeInt + (int)(0.2 * beatLengthInt));
                    t.Easing = EasingTypes.Out;
                    go.Transformations.Add(t);

                    t = new Transformation(TransformationType.Scale, 0.2f, 1, goTimeInt - (int)(0.6 * beatLengthInt), goTimeInt + (int)(0.2 * beatLengthInt));
                    t.Easing = EasingTypes.Out;
                    go.Transformations.Add(t);
                }

                go.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, goTimeInt + (int)(0.3 * beatLengthInt), goTimeInt + beatLengthInt));

                ready.DimImmune = true;
                count3.DimImmune = true;
                count2.DimImmune = true;
                count1.DimImmune = true;
                go.DimImmune = true;


                spriteManagerBelowHitObjectsWidescreen.Add(ready);
                spriteManagerBelowHitObjectsWidescreen.Add(count3);
                spriteManagerBelowHitObjectsWidescreen.Add(count2);
                spriteManagerBelowHitObjectsWidescreen.Add(count1);
                spriteManagerBelowHitObjectsWidescreen.Add(go);
            }
            else if (hitObjectManager.hitObjects[0].StartTime > 6000)
            {
                int t = hitObjectManager.hitObjects[0].StartTime - hitObjectManager.PreEmpt;
                for (int i = 0; i < 1300; i += 200)
                {
                    Transformation t1 =
                        new Transformation(TransformationType.Fade, 0, 1, t - 1000 + i, t - 1000 + i);
                    Transformation t2 =
                        new Transformation(TransformationType.Fade, 1, 0, t - 900 + i, t - 900 + i);
                    s_arrows.ForEach(a =>
                                         {
                                             a.Transformations.Add(t1);
                                             a.Transformations.Add(t2);
                                         });
                }
            }
        }

        /// <summary>
        /// Caches the future sprites.
        /// </summary>
        protected virtual void CacheFutureSprites()
        {
            SkinManager.LoadAll(@"lighting");

            t_star2 = SkinManager.Load(@"star2");
            starBreakAdditive = SkinManager.Current.Colours["StarBreakAdditive"];

            Ruleset.CacheFutureSprites();
        }

        private void skip_OnClick(object sender, EventArgs e)
        {
            GameBase.Scheduler.AddDelayed(delegate { DoSkip(); }, GameBase.random.Next(0, 300));
        }

        internal static void QueueSkip()
        {
            if (Instance == null)
                return;
            queueSkipCount = Math.Max(0, queueSkipCount) + 1;
        }

        internal virtual bool DoSkip()
        {
            if (GameBase.Mode != OsuModes.Play || Paused || Unpausing || !FinishedInitialAudioSetup)
                return false;

            if (AudioEngine.Time < SkipBoundary && !Passed)
            {
                if (HasSkipped || Status != PlayerStatus.Intro)
                    return false;

                GameBase.fadeLevel = 80;
                GameBase.FadeState = FadeStates.FadeIn;

                AudioEngine.PlaySamplePositional(@"menuhit");

                ClearWarnings();

                //Allow skipping twice if there is a long storyboarded intro.
                bool doubleSkip = AllowDoubleSkip;

                if (!doubleSkip)
                {
                    HasSkipped = true;
                    skip.HandleInput = false;
                    Status = PlayerStatus.Playing;
                }

                int skipTo = doubleSkip ? 0 : (SkipBoundary + GameBase.random.Next(-80, 90));

                AudioEngine.StopAllSampleEvents();

                AudioEngine.SeekTo(skipTo);

                audioCheckTime = -1;

                StreamingManager.PurgeFrames(ReplayAction.Skip);
                if (!doubleSkip)
                    Ruleset.OnSkip();

                queueSkipCount--;
            }
            else if (OutroSkippable && !HasSkippedOutro)
            {
                HasSkippedOutro = true;
                AudioEngine.PlaySamplePositional(@"menuhit");
                DoPass();
                Status = PlayerStatus.Exiting;
            }

            return true;
        }

        protected bool AllowDoubleSkip
        {
            get
            {
                int leadIn = leadInTime < 10000 ? -leadInTime : 0;
                return !(this is PlayerVs)
                    && AudioEngine.Time < leadIn - (InputManager.ReplayMode ? 50 : 0)
                    && SkipBoundary > 6000;
            }
        }

        protected void ResetScore(Beatmap beatmap)
        {
            lock (StreamingManager.LockReplayScore)
            {
                string name = string.Empty;

                Score replayScore = InputManager.ReplayScore;
                bool replayAvailable = InputManager.ReplayMode && replayScore != null;

                name = replayAvailable ? (InputManager.ReplayStreaming ? StreamingManager.CurrentlySpectating.Name : replayScore.playerName) : ConfigManager.sUsername.Value;

                if (IsTargetPracticeMode)
                    currentScore = new ScoreTarget(beatmap, name);
                else
                    currentScore = ScoreFactory.Create(mode, name, beatmap);

                currentScore.AllowSubmission &= wasAllowingSubmission;

                currentScore.enabledMods = replayAvailable ? (Mods)replayScore.enabledMods : (Mods)ModManager.ModStatus;
                if (replayAvailable && !InputManager.ReplayStreaming)
                    currentScore.replay = InputManager.ReplayScore.replay;

                if (InputManager.ReplayMode || GameBase.TestMode)
                    currentScore.AllowSubmission = false;
                /*
                currentScore.hpGraph = new List<Vector2>();
                currentScore.errorList = new List<int>(BeatmapManager.Current.ObjectCount);*/
                currentScore.createStaticsList();
                Ruleset.ResetScore();

                if (hitObjectManager != null)
                {
                    hitObjectManager.CurrentComboBad = 0;
                    hitObjectManager.CurrentComboKatu = 0;
                }

                UpdateChecksum();

                Passing = true;
            }
        }

        private string CalcScoreChecksum()
        {
            if (Ruleset.ComboCounter == null)
                return string.Empty;

            return (currentScore.totalScore * 2 + Ruleset.ComboCounter.HitCombo + AudioEngine.CurrentPlaybackRate * 5 + (GameBase.TestMode ? 2 : 3) + (int)(Mods)currentScore.enabledMods)
                   + @"9" + (int)(BeatmapManager.Current.DifficultyPeppyStarsRaw * 4 * (Ruleset.ComboCounter.HitCombo % 11 + 1))
                   + (int)Math.Sqrt(Math.Pow(Ruleset.ComboCounter.HitCombo, 4) % 89) + (InputManager.ReplayMode && !GameBase.TestMode ? @"9" : @"5");
        }

        protected virtual bool exit()
        {
            GameBase.ChangeMode(OsuModes.SelectPlay);
            return true;
        }

        protected virtual bool onKeyPressed(object sender, Keys k)
        {
            if (GameBase.FadeState != FadeStates.Idle)
                return false;

            Bindings b = BindingManager.CheckKey(k);

            if (!Loader.Loaded)
            {
                if (b == Bindings.Pause || k == Keys.Escape)
                    return exit();
                return false;
            }

            inputReceivedAtLeastOnce = true;

            switch (k)
            {
#if DEBUG
                case Keys.F3:
                    if (KeyboardHandler.ControlPressed)
                    {
                        int newTime = AudioEngine.Time - 1000;
                        AudioEngine.SeekTo(newTime);
                        return true;
                    }
                    break;
                case Keys.E:
                    if (KeyboardHandler.ControlPressed)
                    {
                        GameBase.TestMode = true;
                        GameBase.TestTime = AudioEngine.Time;
                        GameBase.ChangeModeInstant(OsuModes.Edit);
                        return true;
                    }
                    break;
#endif
#if !Public
#if DEBUG
                case Keys.D1:
                case Keys.D2:
                case Keys.D3:
                case Keys.D4:
#endif
                case Keys.D5:
                case Keys.D6:
                case Keys.D7:
                case Keys.D8:
                case Keys.D9:
                    if (InputManager.ReplayMode && MatchSetup.Match == null) //We don't want this happening in Tag.
                        AudioEngine.ChangeFrequencyRate((int)(k - Keys.D1 - 4));
                    break;
                case Keys.D0:
                    if (InputManager.ReplayMode && MatchSetup.Match == null)
                        AudioEngine.ChangeFrequencyRate(1);
                    break;
#endif
            }

            if (b == Bindings.Pause || k == Keys.Escape)
            {
                if (Passed)
                    DoPass();
                else if (!(this is PlayerVs) && (InputManager.ReplayMode || Failed))
                {
                    if (GameBase.TestMode)
                    {
                        InputManager.ReplayMode = false;
                        GameBase.ChangeMode(OsuModes.Edit);
                    }
                    else if (InputManager.ReplayMode)
                    {
                        StreamingManager.StopSpectating(true);
                        Status = PlayerStatus.ExitingSpectator;
                    }

                    if (Paused)
                        if (GameBase.TestMode)
                            GameBase.ChangeMode(OsuModes.Edit);
                        else
                            GameBase.ChangeMode(OsuModes.SelectPlay);
                    else
                        AudioEngine.AudioFrequency = 300;
                }
                else
                    TogglePause();

                return true;
            }


            if (k == BindingManager.For(Bindings.Skip))
            {
                skip_OnClick(null, null);
                return true;
            }

            if (mode == PlayModes.OsuMania)
            {
                if ((KeyboardHandler.ControlPressed && k == Keys.OemPlus) || b == Bindings.IncreaseSpeed)
                {
                    ChangeManiaSpeed(1);
                    return true;
                }
                else if ((KeyboardHandler.ControlPressed && k == Keys.OemMinus) || b == Bindings.DecreaseSpeed)
                {
                    ChangeManiaSpeed(-1);
                    return true;
                }
            }

            switch (b)
            {
                case Bindings.ToggleScoreboard:
                    if (KeyboardHandler.ShiftPressed)
                    {
                        ConfigManager.sShowInterface.Toggle();
                        NotificationManager.ShowMessageMassive(LocalisationManager.GetString(ConfigManager.sShowInterface ? OsuString.Player_InterfaceEnabled : OsuString.Player_InterfaceDisabled), 1000);
                    }
                    else
                    {
                        if (ScoreBoard == null) return false;
                        ScoreBoard.AlwaysVisible = !ScoreBoard.AlwaysVisible;

                        if (EventManager.BreakMode)
                        {
                            if (!ScoreBoard.AlwaysVisible)
                                ScoreBoard.ShowStatus(LocalisationManager.GetString(OsuString.Player_ScoreBoardShowStatus));
                            else
                                ScoreBoard.ShowStatus(LocalisationManager.GetString(OsuString.Player_ScoreBoardShowStatus2));
                        }

                        if (!InputManager.ReplayMode)
                            ConfigManager.sScoreboardVisible.Value = ScoreBoard.AlwaysVisible;
                    }

                    return true;
                case Bindings.IncreaseSpeed:
                    if (Mode == PlayModes.OsuMania)
                    {
                        ChangeManiaSpeed(1);
                        return true;
                    }
                    break;
                case Bindings.DecreaseSpeed:
                    if (mode == PlayModes.OsuMania)
                    {
                        ChangeManiaSpeed(-1);
                        return true;
                    }
                    break;
                case Bindings.IncreaseAudioOffset:
                    ChangeCustomOffset(KeyboardHandler.AltPressed ? 1 : 5);
                    return true;
                case Bindings.DecreaseAudioOffset:
                    ChangeCustomOffset(KeyboardHandler.AltPressed ? -1 : -5);
                    return true;
            }

            if (Paused && !InputManager.ReplayMode)
            {
                if (Failed && k == Keys.F1 && currentScore != null)
                {
                    HandleScoreSubmission();
                    InputManager.ReplayScore = currentScore;
                    InputManager.ReplayMode = true;
                    InputManager.ReplayToEnd = true;
                    GameBase.ChangeModeInstant(OsuModes.Play, true);
                    return true;
                }

                switch (Mode)
                {
                    case PlayModes.CatchTheBeat:
                        if (b == Bindings.Skip || k == Keys.Enter || k == Keys.Space || b == Bindings.FruitsDash)
                            return pauseKey(0);
                        else if (k == Keys.Down || b == Bindings.FruitsRight)
                            return pauseKey(1);
                        else if (k == Keys.Up || b == Bindings.FruitsLeft)
                            return pauseKey(-1);
                        break;
                    case PlayModes.Osu:
                        if (b == Bindings.Skip || k == Keys.Enter || k == Keys.Space)
                            return pauseKey(0);
                        else if (k == Keys.Down)
                            return pauseKey(1);
                        else if (k == Keys.Up)
                            return pauseKey(-1);
                        break;
                    case PlayModes.Taiko:
                        if (b == Bindings.Skip || k == Keys.Enter || k == Keys.Space || b == Bindings.TaikoInnerLeft || b == Bindings.TaikoInnerRight)
                            return pauseKey(0);
                        else if (k == Keys.Down || b == Bindings.TaikoOuterRight)
                            return pauseKey(1);
                        else if (k == Keys.Up || b == Bindings.TaikoOuterLeft)
                            return pauseKey(-1);
                        break;
                    case PlayModes.OsuMania:
                        if (b == Bindings.Skip || k == Keys.Enter || k == JoyKey.Right)
                            return pauseKey(0);
                        else if (k == Keys.Down || k == JoyKey.Down)
                            return pauseKey(1);
                        else if (k == Keys.Up || k == JoyKey.Up)
                            return pauseKey(-1);
                        break;
                }

            }

            return false;
        }

        bool failedState;
        internal void SetFailedState(bool failed)
        {
            if (failedState == failed) return;
            failedState = failed;

            hitObjectManager.spriteManager.Blackness = failed ? 0.5f : 0;
            hitObjectManager.spriteManager.BlacknessShade = failed ? 0.65f : 0;
            hitObjectManager.ComboColoursReset(failed);
            SkinManager.ComputeColours(hitObjectManager);
            HitObjectManager.RecycleTargets();
        }

        private void ChangeManiaSpeed(int delta)
        {
            if (AudioEngine.Time > firstHitTime + 5000
                && !InputManager.ReplayMode && !GameBase.TestMode)
            {
                return;
            }

            SpeedMania.AdjustSpeed(delta);
            ((HitObjectManagerMania)hitObjectManager).ReCalculate(AudioEngine.Time);
        }

        private void ChangeCustomOffset(int msChange)
        {
            if (Paused || Unpausing)
            {
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.Player_OffsetChangeUnpause), 1000);
                return;
            }

            if (AudioEngine.Time > firstHitTime + 10000 && !GameBase.TestMode && !EventManager.BreakMode)
                return;

            if (GameBase.Time - lastPlayerOffsetChange < 150)
                return;

            lastPlayerOffsetChange = GameBase.Time;

            BeatmapManager.Current.PlayerOffset = Math.Max(-1000, Math.Min(1000, BeatmapManager.Current.PlayerOffset + msChange));
            NotificationManager.ShowMessageMassive(string.Format(LocalisationManager.GetString(OsuString.Player_LocalBeatmapOffsetSet), BeatmapManager.Current.PlayerOffset), 1000);
        }

        int lastPlayerOffsetChange;

        private bool pauseKey(int i)
        {
            if (i == 0)
            {
                switch (pauseSelected)
                {
                    default:
                        TogglePause();
                        return true;
                    case 0:
                        pauseContinue(null, null);
                        return true;
                    case 1:
                        pauseRetry(null, null);
                        return true;
                    case 2:
                        pauseExit(null, null);
                        return true;
                }
            }
            if (pauseSelected < 0)
            {
                pauseSelected = i == -1 ? 2 : (Failed ? 1 : 0);

                pTexture tex = SkinManager.Load(@"play-warningarrow", SkinSource.Skin | SkinSource.Osu);

                pSprite l = new pSprite(tex, Fields.Centre, Origins.Centre, Clocks.Game, new Vector2(-170, pauseSprites[pauseSelected + 2].Position.Y), 1, true, GameBase.NewGraphicsAvailable ? new Color(0, 114, 255) : Color.White);

                l.Scale = 0.8f;
                l.FadeInFromZero(300);
                l.Transformations.Add(new Transformation(TransformationType.MovementX, l.Position.X - 170, l.Position.X, GameBase.Time, GameBase.Time + 300, EasingTypes.In));

                Transformation loop = new Transformation(TransformationType.MovementX, l.Position.X, l.Position.X - 45, GameBase.Time + 300, GameBase.Time + 600, EasingTypes.Out);
                loop.Loop = true;
                loop.LoopDelay = 300;
                loop.TagNumeric = 101;
                l.Transformations.Add(loop);


                loop = new Transformation(TransformationType.MovementX, l.Position.X - 45, l.Position.X, GameBase.Time + 600, GameBase.Time + 900, EasingTypes.In);
                loop.Loop = true;
                loop.LoopDelay = 300;
                loop.TagNumeric = 102;
                l.Transformations.Add(loop);

                spriteManagerPauseScreen.Add(l);
                pauseArrows.Add(l);

                l = new pSprite(tex, Fields.Centre, Origins.Centre, Clocks.Game, new Vector2(170, pauseSprites[pauseSelected + 2].Position.Y), 1, true, GameBase.NewGraphicsAvailable ? new Color(0, 114, 255) : Color.White);
                l.FlipHorizontal = true;

                l.Scale = 0.8f;
                l.FadeInFromZero(300);
                l.Transformations.Add(new Transformation(TransformationType.MovementX, l.Position.X + 170, l.Position.X, GameBase.Time, GameBase.Time + 300, EasingTypes.In));

                loop = new Transformation(TransformationType.MovementX, l.Position.X, l.Position.X + 45, GameBase.Time + 300, GameBase.Time + 600, EasingTypes.Out);
                loop.Loop = true;
                loop.LoopDelay = 300;
                loop.TagNumeric = 201;
                l.Transformations.Add(loop);


                loop = new Transformation(TransformationType.MovementX, l.Position.X + 45, l.Position.X, GameBase.Time + 600, GameBase.Time + 900, EasingTypes.In);
                loop.Loop = true;
                loop.LoopDelay = 300;
                loop.TagNumeric = 202;
                l.Transformations.Add(loop);

                spriteManagerPauseScreen.Add(l);
                pauseArrows.Add(l);
            }
            else
            {
                AudioEngine.Click();

                pauseSelected = Failed ? ((pauseSelected + i + 1) % 2) + 1 : (pauseSelected + i + 3) % 3;

                pauseArrows.ForEach(p =>
                {
                    p.Transformations.RemoveAll(t => t.Type == TransformationType.MovementY);
                    p.Transformations.Add(new Transformation(TransformationType.MovementY, p.Position.Y, pauseSprites[pauseSelected + 2].Position.Y, GameBase.Time, GameBase.Time + 100, EasingTypes.Out));
                });
            }

            return true;
        }

        internal void ToggleFF()
        {
            //    if (ModManager.CheckActive(currentScore.enabledMods, Mods.DoubleTime))
            //        return;
            bool isDoubleTime = ModManager.CheckActive(currentScore.enabledMods, Mods.DoubleTime);
            bool isHalfTime = ModManager.CheckActive(currentScore.enabledMods, Mods.HalfTime);
            //normal -> double -> half -> double
            switch (InputManager.ReplayModeFF)
            {
                case ReplaySpeed.Normal:
                    if (isDoubleTime)
                        InputManager.ReplayModeFF = ReplaySpeed.Half;
                    else
                        InputManager.ReplayModeFF = ReplaySpeed.Double;
                    break;
                case ReplaySpeed.Double:
                    InputManager.ReplayModeFF = ReplaySpeed.Half;
                    break;
                case ReplaySpeed.Half:
                    InputManager.ReplayModeFF = ReplaySpeed.Normal;
                    break;
            }
            switch (InputManager.ReplayModeFF)
            {
                case ReplaySpeed.Normal:
                    if (isDoubleTime)
                        AudioEngine.CurrentPlaybackRate = 150;
                    else if (isHalfTime)
                        AudioEngine.CurrentPlaybackRate = 75;
                    else
                        AudioEngine.CurrentPlaybackRate = 100;
                    break;
                case ReplaySpeed.Double:
                    if (isHalfTime)
                        AudioEngine.CurrentPlaybackRate = 150;
                    else
                        AudioEngine.CurrentPlaybackRate = 200;
                    break;
                case ReplaySpeed.Half:
                    if (isDoubleTime)
                        AudioEngine.CurrentPlaybackRate = 75;
                    else
                        AudioEngine.CurrentPlaybackRate = 50;
                    break;
            }
        }

        internal virtual void TogglePause()
        {
            if (spriteManager == null || (Failed && Paused) || !FinishedInitialAudioSetup)
                return;

            if (!AudioEngine.ExtendedTime && !GameBase.TestMode && !Failed && !InputManager.ReplayMode && !Unpausing && !Paused && !EventManager.BreakMode && LastPause != 0 &&
                AudioEngine.Time - LastPause < 1000)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Player_WaitBeforePausing));
                return;
            }

            if (Passed)
            {
                DoPass();
                return;
            }

            Paused = !Paused;

            if (visualSettings != null)
                visualSettings.Hide(InputManager.ReplayMode || StreamingManager.CurrentlySpectating != null || !Paused);

            pauseSelected = -1;

            BanchoClient.UpdateStatus();

            SetGCLowLatency(!Paused);

            if (Paused)
            {
                if (pauseCursor == null)
                    pauseLocation = new Vector2(OsuMathHelper.Clamp(InputManager.CursorPosition.X, 1, GameBase.WindowRectangle.Width),
                        OsuMathHelper.Clamp(InputManager.CursorPosition.Y, 1, GameBase.WindowRectangle.Height - 1));

                StreamingManager.PurgeFrames(ReplayAction.Pause);

                HidePauseCursor();

                LastPause = AudioEngine.Time;

                foreach (HitObject h in hitObjectManager.hitObjectsMinimal)
                    h.StopSound();

                skip.HandleInput = false;

                if (!AudioEngine.Paused)
                {
                    AudioEngine.TogglePause();
                }

                if (pauseSprites == null)
                {
                    pauseSprites = new List<pSprite>();

                    pSprite pb =
                        new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft,
                                    Clocks.Game, Vector2.Zero, 0.98F, true, Color.TransparentBlack);
                    pb.VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight);
                    pb.ScaleToWindowRatio = false;
                    pauseSprites.Add(pb);

                    //Check for a jpg background for beatmap-based skins (used to reduce filesize), then fallback to png.
                    pb =
                            new pSprite(SkinManager.Load(@"pause-overlay.jpg", SkinSource.Beatmap) ?? SkinManager.Load(@"pause-overlay"), Fields.Centre,
                                        Origins.Centre,
                                        Clocks.Game,
                                        Vector2.Zero, 0.985f, true, Color.TransparentWhite);
                    pauseSprites.Add(pb);


                    if (InputManager.ReplayMode)
                    {
                        pb =
                            new pSprite(SkinManager.Load(@"menu-osu"), Fields.Centre,
                                        Origins.Centre,
                                        Clocks.Game,
                                        Vector2.Zero, 0.99f, true, new Color(100, 100, 100, 0));
                        if (GameBase.Tournament) pb.Bypass = true;
                        pb.Scale = 0.6f;
                        pauseSprites.Add(pb);

                        ReplayBufferText = new pText(string.Empty, 20, new Vector2(GameBase.WindowWidthScaled / 2, 240), 1, true, Color.TransparentWhite);
                        if (GameBase.Tournament) ReplayBufferText.Bypass = true;
                        ReplayBufferText.Origin = Origins.Centre;
                        pauseSprites.Add(ReplayBufferText);
                    }
                    else
                    {
                        pb =
                            new pSprite(SkinManager.Load(@"pause-continue"), Fields.Centre,
                                        Origins.Centre,
                                        Clocks.Game,
                                        new Vector2(0, -100), 1, true, Color.TransparentWhite);
                        pb.OnClick += pauseContinue;
                        pb.HandleInput = true;
                        pb.HoverEffect = new Transformation(TransformationType.Scale, 1, 1.1F, 0, 300);
                        pb.HoverEffect.Easing = EasingTypes.Out;
                        pauseSprites.Add(pb);

                        pb =
                            new pSprite(SkinManager.Load(@"pause-retry"), Fields.Centre,
                                        Origins.Centre,
                                        Clocks.Game,
                                        new Vector2(0, 10), 1, true, Color.TransparentWhite);
                        pb.Tag = @"retry";
                        pb.OnClick += pauseRetry;
                        pb.HandleInput = true;
                        pb.HoverEffect = new Transformation(TransformationType.Scale, 1, 1.1F, 0, 300);
                        pb.HoverEffect.Easing = EasingTypes.Out;

                        pauseSprites.Add(pb);

                        pb =
                            new pSprite(SkinManager.Load(@"pause-back"), Fields.Centre,
                                        Origins.Centre,
                                        Clocks.Game,
                                        new Vector2(0, 120), 1, true, Color.TransparentWhite);
                        pb.Tag = @"back";
                        pb.OnClick += pauseExit;
                        pb.HandleInput = true;
                        pb.HoverEffect = new Transformation(TransformationType.Scale, 1, 1.1F, 0, 300);
                        pb.HoverEffect.Easing = EasingTypes.Out;
                    }
                    pauseSprites.Add(pb);
                    spriteManagerPauseScreen.Add(pauseSprites);
                }

                if (InputManager.ReplayMode)
                {
                    pauseSprites[0].Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.5F, GameBase.Time, GameBase.Time + 500));
                    pauseSprites[2].FadeIn(500);
                    pauseSprites[3].FadeIn(500);
                }
                else
                {
                    pauseSprites[0].Transformations.Clear();
                    if (SkinManager.UseNewLayout)
                        pauseSprites[0].Transformations.Add(new Transformation(TransformationType.Fade, pauseSprites[0].Alpha, 0.8F, GameBase.Time, GameBase.Time + 300));
                    else
                        pauseSprites[0].Transformations.Add(new Transformation(TransformationType.Fade, pauseSprites[0].Alpha, 0.95F, GameBase.Time, GameBase.Time + 300));

                    if (!Failed)
                    {
                        pauseSprites[1].FadeIn(300);
                        pauseSprites[2].FadeIn(300);
                    }
                    else
                    {
                        pSprite p = new pSprite(SkinManager.Load(@"fail-background"), Fields.Centre,
                                        Origins.Centre,
                                        Clocks.Game,
                                        Vector2.Zero, 0.985f, true, Color.TransparentWhite);

                        p.Scale = 768f / p.Height;
                        p.FadeInFromZero(600);
                        spriteManagerPauseScreen.Add(p);

                        //better to add an image here
                        pText t = new pText(LocalisationManager.GetString(OsuString.Player_PressToViewReplay), 16F, new Vector2(10, 10),
                                        0.987F, true, Color.White);
                        t.TextBold = true;
                        t.TextShadow = true;
                        spriteManagerPauseScreen.Add(t);
                    }
                    pauseSprites[3].FadeIn(300);
                    pauseSprites[4].FadeIn(300);

                    pauseSprites[2].HandleInput = true;
                    pauseSprites[3].HandleInput = true;
                    pauseSprites[4].HandleInput = true;
                }
            }
            else
            {
                Unpausing = true;

                pauseArrows.ForEach(p => { p.FadeOut(200); p.AlwaysDraw = false; });
                pauseArrows.Clear();

                if (AudioEngine.ExtendedTime || InputManager.ReplayMode || (this is PlayerTest) || Ruleset.AllowInstantUnpause || Relaxing2 || Relaxing)
                {
                    StreamingManager.PurgeFrames(ReplayAction.Unpause);
                    UnpauseConfirmed = true;
                }
                else
                {
                    if (pauseCursor == null)
                    {
                        if (!EventManager.BreakMode)
                        {
                            pauseCursor = new pSpriteCircular(SkinManager.Load(@"cursor", SkinSource.Osu), pauseLocation, 0.99f, true, Color.Orange, 20);
                            pauseCursor.Field = Fields.NativeStandardScale;
                            pauseCursor.HandleInput = true;
                            pauseCursor.ToolTip = LocalisationManager.GetString(OsuString.Player_ClickToResume);


                            pauseCursor.Scale = 1.2f;
                            pauseCursor.HoverEffect = new Transformation(Color.Orange, Color.White, 0, 200);
                            pauseCursor.OnClick += pauseCursor_OnClick;
                            pauseCursor.Tag = @"pausecursor";
                            spriteManager.Add(pauseCursor);

                            pauseCursor = new pSprite(SkinManager.Load(@"cursor", SkinSource.Osu), Fields.NativeStandardScale,
                                                      Origins.Centre, Clocks.Game, pauseLocation, 1, true,
                                                      Color.Orange);
                            pauseCursor.Tag = @"pausecursor";
                            pauseCursor.Additive = true;
                            spriteManager.Add(pauseCursor);


                            Transformation t = new Transformation(TransformationType.Scale, 1.2f, 3f, GameBase.Time,
                                                                  GameBase.Time + 1000);
                            t.Loop = true;
                            pauseCursor.Transformations.Add(t);

                            t = new Transformation(TransformationType.Fade, 1, 0, GameBase.Time, GameBase.Time + 1000,
                                                   EasingTypes.Out);
                            t.Loop = true;
                            pauseCursor.Transformations.Add(t);

                            pauseCursorText = new pSprite(SkinManager.Load(@"unpause", SkinSource.Osu), Fields.Centre, Origins.Centre,
                                                          Clocks.Game, Vector2.Zero, 0.9f, true, Color.White);
                            spriteManager.Add(pauseCursorText);
                            pauseCursorText.Tag = @"pausecursor";
                        }
                        else
                        {
                            UnpauseConfirmed = true;
                        }
                    }
                }

                if (pauseSprites != null)
                {
                    pauseSprites[0].FadeOut(GameBase.Tournament ? 0 : 800);
                    pauseSprites[1].FadeOut(GameBase.Tournament ? 0 : 800);

                    pauseSprites[2].FadeOut(GameBase.Tournament ? 0 : 300);
                    pauseSprites[3].FadeOut(GameBase.Tournament ? 0 : 300);

                    if (pauseSprites.Count > 4)
                    {
                        pauseSprites[2].HandleInput = false;
                        pauseSprites[3].HandleInput = false;
                        pauseSprites[4].HandleInput = false;
                        pauseSprites[4].FadeOut(GameBase.Tournament ? 0 : 300);
                    }
                }
            }
        }

        private void pauseContinue(object sender, EventArgs e)
        {
            AudioEngine.PlaySamplePositional(@"menuback");
            TogglePause();
        }

        private void pauseExit(object sender, EventArgs e)
        {
            AudioEngine.PlaySamplePositional(@"menuback");
            if (GameBase.TestMode)
                GameBase.ChangeMode(OsuModes.Edit);
            else
                GameBase.ChangeMode(OsuModes.SelectPlay);
        }

        private void pauseRetry(object sender, EventArgs e)
        {
            AudioEngine.PlaySample(@"menuhit");
            Retry();
        }

        internal static void Retry(bool instant = false)
        {
            Retrying = true;
            if (Instance != null)
                Instance.Status = PlayerStatus.Exiting;

            if (instant)
            {
                QueueSkip();
                GameBase.ChangeMode(OsuModes.Play, true);
                GameBase.fadeLevel = 100;
            }
            else
                GameBase.ChangeMode(OsuModes.Play, true);
        }

        private void pauseCursor_OnClick(object sender, EventArgs e)
        {
            if (pauseSprites[0].IsVisible)
                return;

            ((pSprite)sender).HandleInput = false;
            UnpauseConfirmed = true;
        }

        protected override void Dispose(bool disposing)
        {
            if (ModManager.CheckActive(Mods.Cinema))
                ModManager.ModStatus &= ~(Mods.Cinema | Mods.Autoplay);

            if (previewAudioTrack != null && previewAudioTrack != AudioEngine.AudioTrack && previewAudioTrack.Preview)
                previewAudioTrack.Dispose();

            if (Loader != null) Loader.Dispose();

            if (ConfigManager.sComboFire)
            {
                fireRenderer.Dispose();
                fireRenderer = null;
            }

            SetGCLowLatency(false);

            GameBase.Instance.Activated -= Game_ActivationChanged;
            GameBase.Instance.Deactivated -= Game_ActivationChanged;

            Status = PlayerStatus.Exiting;

            Relaxing = false;
            Relaxing2 = false;
            Paused = false;
            currentScore = VisibleScore;

            Instance = null;
            ReplayBufferText = null;

            HandleScoreSubmission();

            AudioEngine.ResetAudioTrack();
            AudioEngine.EndVirtualTime();

            UnbindEvents();

            if (!AudioEngine.Nightcore)
            {
                AudioEngine.CurrentPlaybackRate = 100;
                Bass.BASS_ChannelSetAttribute(AudioEngine.AudioTrack.audioStream, BASSAttribute.BASS_ATTRIB_FREQ, AudioEngine.AudioFrequency);
                //Reset frequency.
            }

            if (hitObjectManager != null) hitObjectManager.Dispose();

            if (spriteManager != null) spriteManager.Dispose();
            if (spriteManagerBelowHitObjectsWidescreen != null) spriteManagerBelowHitObjectsWidescreen.Dispose();
            if (spriteManagerAdd != null) spriteManagerAdd.Dispose();
            if (spriteManagerInterface != null) spriteManagerInterface.Dispose();
            if (spriteManagerInterfaceWidescreen != null) spriteManagerInterfaceWidescreen.Dispose();
            if (spriteManagerBelowScoreboardWidescreen != null) spriteManagerBelowScoreboardWidescreen.Dispose();
            if (spriteManagerMetadata != null) spriteManagerMetadata.Dispose();
            if (spriteManagerPauseScreen != null) spriteManagerPauseScreen.Dispose();
            if (spriteManagerHighest != null) spriteManagerHighest.Dispose();
            if (SpriteManagerLoading != null) // && GameBase.Mode != OsuModes.SelectPlay) <- use this when we start working on the transition in the other direction back to song select.
            {
                SpriteManagerLoading.SpriteList.ForEach(s =>
                {
                    pSprite sp = s as pSprite;
                    if (sp != null && sp.Texture != null)
                        sp.Texture.Disposable = true;
                });
                SpriteManagerLoading.Dispose();
                SpriteManagerLoading = null;
            }

            if (progressBar != null) progressBar.Dispose();
            if (visualSettings != null) visualSettings.Dispose();
            if (scrubber != null) scrubber.Dispose();
            if (inputOverlay != null) inputOverlay.Dispose();

            if (ScoreBoard != null && !(this is PlayerVs)) //playervs uses the scoreboard in ranking
            {
                ScoreBoard.Dispose();
                ScoreBoard = null;
            }

            //Keep the content loaded if we are playing again.
            //Saves time loading gameplay sprites.
            if (Retrying)
                RetryCount++;
            else
                RetryCount = 0;
            if (!InputManager.ReplayMode)
                PlayCount++;

            if (Ruleset != null)
                Ruleset.Dispose();

            InputManager.ReplayFrame = 0;
            Mode = ModeOriginal;
            base.Dispose(disposing);

            if (GameBase.Tournament)
                GameBase.TourneySpectatorName.Alpha = 1;
        }

        internal void HandleScoreSubmission()
        {
            if (!allowSubmissionHaxCheck)
            {
                crash();
                Environment.Exit(-1);
            }

            if (currentScore == null)
                return;

            if (!InputManager.ReplayMode)
            {
                currentScore.Seed = Seed;
                currentScore.date = DateTime.Now;
            }

            if (ConfigManager.sEloTouchscreen)
                return;


            if (InitialAllowSubmission && AllowSubmissionConstantConditions && AllowSubmissionVariableConditions)
            {
                if (Ruleset.IsInitialized)
                {
                    Ruleset.haxCheckCount++;
                    HaxCheck();
                }

                HaxCheckPass();

                if (!currentScore.pass)
                {
                    if (!Failed)
                        currentScore.exit = true;
                    currentScore.failTime = FailTime == 0 ? (int)AudioEngine.Time : FailTime;
                }

                currentScore.Submit();

                if (BeatmapManager.Current.NewFile) BeatmapManager.Current.NewFile = false;
            }
            else
                currentScore.AllowSubmission = false;
        }

        internal static bool Loaded;

        protected virtual bool AllowLoad { get { return true; } }

        /// <returns>true if still loading</returns>
        internal bool UpdateLoading()
        {
            if (Loader.Loaded) return false;

            Loader.Update();
            if (visualSettings != null) visualSettings.Update();

            if (previewAudioTrack != null)
            {
                if (previewAudioTrackVolume > (AsyncLoadComplete ? 0 : 0.4f))
                    previewAudioTrackVolume -= (float)(0.02 * GameBase.FrameRatio);

                previewAudioTrack.Volume = Math.Max(0, previewAudioTrackVolume);
            }

            if (AsyncLoadComplete && PendingRestart)
            {
                GameBase.ChangeModeInstant(GameBase.Mode, true);
                AsyncLoadComplete = false;
                return true;
            }

            if (AsyncLoadComplete && AllowLoad &&
                (previewAudioTrack == null || previewAudioTrackVolume <= 0) &&
                (SpriteManagerLoading == null || SpriteManagerLoading.SpriteList.Count == 0 || SpriteManagerLoading.SpriteList.TrueForAll(s => s.Alpha == 0)) &&
                (visualSettings == null || !visualSettings.Activated || this is PlayerVs))
            {
                if (visualSettings != null)
                    visualSettings.Hide(true);
                Loader.Complete();

                Loaded = true;
                AudioEngine.Stop();

                if (leadInTime > audioStartTime) AudioEngine.StartVirtualTime(-leadInTime, true);

                eventManager.UpdateBackground();

                GameBase.Scheduler.AddDelayed(delegate
                {
                    if (previewAudioTrack != null && AudioEngine.AudioTrack != previewAudioTrack && previewAudioTrack.Preview) previewAudioTrack.Dispose();

                    InitializeAudioSpecificsCallback(audioStartTime);
                }, GameBase.TestMode || InputManager.ReplayMode || this is PlayerVs ? 0 : GameBase.random.Next(0, 600));
            }
            else
            {
                AudioEngine.Time = -leadInTime;
                AudioEngine.TimeUnedited = -leadInTime;
                if (progressBar != null) progressBar.SetProgress(leadInTime != 0 ? -1 : 0);
            }

            //force update this here in the case background dim has been changed
            if (eventManager != null) eventManager.Update();

            return true;
        }

        public override void Update()
        {
            if (UpdateLoading()) return;

#if ARCADE
            if (Player.Failed)
                IdleHandler.Check(10000);
#endif

            if (inputOverlay != null) inputOverlay.Update();

            float targetInterfaceAlpha = ConfigManager.sShowInterface.Value ? 1 : 0;
            float targetAlpha = 0;

            if (!Loader.Loaded)
                targetAlpha = 0;
            else if (AudioEngine.Time < -1000 && SkipBoundary > 6000)
                targetAlpha = 0;
            else if (Passed)
                targetAlpha = 0;
            else
                targetAlpha = 1;

            if (GameBase.Tournament)
            {
                spriteManagerInterface.Blackness =
                spriteManagerInterfaceWidescreen.Blackness =
                    (EventManager.Instance.spriteManagerBG.Blackness - 0.5f) * 2;

                GameBase.TourneySpectatorName.Alpha = Math.Min(1, 1 - spriteManagerInterface.Blackness);
            }

            if ((Paused || Unpausing) && pauseSprites != null && AudioEngine.Time < SkipBoundary)
                skip.Alpha = (1 - pauseSprites[0].Alpha) * 0.6f;

            if (spriteManagerBelowHitObjectsWidescreen.Alpha != targetAlpha)
            {
                spriteManagerBelowHitObjectsWidescreen.Alpha = MathHelper.Clamp(spriteManagerBelowHitObjectsWidescreen.Alpha + ((targetAlpha < spriteManagerBelowHitObjectsWidescreen.Alpha ? -0.07f : 0.07f) * (float)GameBase.FrameRatio), 0, 1);
                if (hitObjectManager != null) hitObjectManager.spriteManager.Alpha = spriteManagerBelowHitObjectsWidescreen.Alpha;
                if (Ruleset != null) Ruleset.SetAlpha(spriteManagerBelowHitObjectsWidescreen.Alpha);
            }

            if (spriteManagerInterface.Alpha != targetInterfaceAlpha && !Passed)
            {
                spriteManagerInterfaceWidescreen.Alpha = spriteManagerInterface.Alpha =
                    MathHelper.Clamp(spriteManagerInterface.Alpha + ((targetInterfaceAlpha < spriteManagerInterface.Alpha ? -0.07f : 0.07f) * (float)GameBase.FrameRatio), 0, 1);
            }
            else if (Passed)
                spriteManagerInterfaceWidescreen.Alpha = spriteManagerInterface.Alpha = spriteManagerBelowHitObjectsWidescreen.Alpha * targetInterfaceAlpha;

            if (Status == PlayerStatus.Busy || GameBase.FadeState == FadeStates.FadeOut)
                return;

            if (InputManager.ReplayMode && AudioEngine.SeekedBackwards)
            {
                bScoreFrame f = currentScore.Frames.FindLast(s => s.time < AudioEngine.Time);
                LoadFrame(f);
            }

            if (Status == PlayerStatus.Intro && AudioEngine.Time >= SkipBoundary)
            {
                ClearWarnings();
                Status = PlayerStatus.Playing;
            }

            if (!InputManager.ReplayMode && !(this is PlayerTest))
                HaxCheck2();

            if (Failed && StreamingManager.HasSpectators && !InputManager.ReplayMode && !ReportedEndGame)
            {
                StreamingManager.PurgeFrames(ReplayAction.Fail);
                ReportedEndGame = true;
            }

            if (queueSkipCount > 0 && !Paused && !Unpausing)
                DoSkip();

            if (AudioEngine.Time >= SkipBoundary && AudioEngine.AudioState == AudioStates.Playing)
            {
                if (!Passed)
                {
                    if (skip.IsVisible && skip.TagNumeric != -1)
                    {
                        skip.TagNumeric = -1;
                        IntroSkipBoundaryEnded();
                    }
                }
                else
                {
                    if (!skip.IsVisible && skip.Transformations.Count == 0 &&
                        eventManager.lastEventTime - AudioEngine.Time > 3000)
                    {
                        OutroSkippable = true;
                        skip.HandleInput = true;
                        skip.Transformations.Clear();
                        skip.FadeIn(300);
                    }
                }
            }

            if (GameBase.Time - spectatorListUpdate > 1000)
            {
                spectatorListUpdate = GameBase.Time;
                if ((StreamingManager.HasSpectators && !InputManager.ReplayMode && ConfigManager.sShowSpectators) || (InputManager.ReplayStreaming && StreamingManager.FellowSpectators.Count > 0))
                {
                    List<User> spectators = InputManager.ReplayStreaming ? StreamingManager.FellowSpectators : StreamingManager.Spectators;

                    string specString = string.Format(InputManager.ReplayStreaming ? LocalisationManager.GetString(OsuString.Player_OtherSpectators) : LocalisationManager.GetString(OsuString.Player_Spectators), spectators.Count);

                    if (spectators.Count < 30)
                    {
                        specString += @":";
                        string specString2 = "\n\n" + LocalisationManager.GetString(OsuString.Player_SpectatorsMissingBeatmap);

                        int missing = 0;

                        lock (StreamingManager.Spectators)
                        {
                            foreach (User u in spectators)
                            {
                                if (u.CantSpectate)
                                {
                                    missing++;
                                    specString2 += "\n" + u.Name;
                                }
                                else
                                    specString += "\n" + u.Name;
                            }
                        }

                        if (missing > 0)
                            specString += specString2;
                    }

                    if (spectatorList != null && spectatorList.Text != specString)
                        spectatorList.Text = specString;
                }
                else if (spectatorList != null && spectatorList.Text.Length > 0)
                    spectatorList.Text = string.Empty;
            }

            if (visualSettings != null)
            {
                visualSettings.Update();
                spriteManagerPauseScreen.HandleInput = !visualSettings.Activated;

                //If skip button is inactive, input handling has already been disabled
                if (skip.TagNumeric != -1)
                    skip.HandleInput = !visualSettings.Activated && !Paused;
            }

            if (scrubber != null)
                scrubber.Update();

            eventManager.Update();

            if (!GameBase.IsActive && Playing && !Paused)
                Game_ActivationChanged(this, null);

            if ((!InputManager.ReplayMode || StreamingManager.CurrentlySpectating == null) && MatchSetup.Match == null && GameBase.FadeState == FadeStates.Idle)
            {
                if (!ChatEngine.IsVisible && KeyboardHandler.ControlPressed && KeyboardHandler.IsKeyDown(Keys.R))
                {
                    if (GameBase.fadeLevel <= 0)
                    {
                        GameBase.spriteManagerTransition.Clear();
                        GameBase.s_fadeScreen.Bypass = false;
                        GameBase.spriteManagerTransition.Add(GameBase.s_fadeScreen);
                    }

                    GameBase.fadeLevel = Math.Min(100, GameBase.fadeLevel + 10f * (float)GameBase.FrameRatio);
                    if (GameBase.fadeLevel == 100)
                        Retry(true);
                }
                else if (GameBase.fadeLevel > 0)
                {
                    GameBase.fadeLevel = Math.Max(0, GameBase.fadeLevel - 3f * (float)GameBase.FrameRatio);
                }
            }

            if (Paused)
            {
                audioCheckTime = -1;
                audioCheckTimeComp = -1;
                return;
            }

            if (Unpausing)
            {
                if (UnpauseConfirmed && (pauseSprites == null || (pauseSprites.Count > 0 && !pauseSprites[0].IsVisible)))
                {
                    HidePauseCursor();

                    skip.HandleInput = true;

                    if (AudioEngine.Paused && !Recovering)
                        AudioEngine.TogglePause();
                }
                else
                    return;
            }

            if (leadInActive)
            {
                if (AudioEngine.VirtualTimeAccumulated >= leadInTime && GameBase.FadeState == FadeStates.Idle && FinishedInitialAudioSetup)
                {
                    leadInActive = false;
                    if (AudioEngine.ExtendedTime)
                    {
                        AudioEngine.EndVirtualTime();
                        if (AudioEngine.Time > 1000) AudioEngine.SeekTo(AudioEngine.Time, false, true);
                    }
                    AudioEngine.Continue();
                }
            }

            if (AudioEngine.AudioState == AudioStates.Stopped && !Recovering && !leadInActive)
            //The audio isn't playing anymore...
            {
                if (!OutroActive)
                {
                    AudioEngine.StartVirtualTime();
                    OutroActive = true;
                }
            }

            //Always need to run this as it keeps input-specific values up-to-date
            Ruleset.UpdateInput();

            UpdateActive();

            hitObjectManager.UpdateBasic();

            //These functions only need to be run on an actual replay frame (I think).
            //This stops scoring getting noise interpolation frames when playing back replays.
            if (!InputManager.ReplayMode || InputManager.ScorableFrame || IsAutoplayReplay)
            {
                Ruleset.UpdateScoring();
                UpdateScore();
            }

            hitObjectManager.UpdateHitObjects();

            Ruleset.Update();

            UpdateProgress();

            UpdatePhysics();

            UpdateScoreboard();

            UpdateBloomEffects();

            if (spectatorList != null)
            {
                float sbAlpha = ScoreBoard == null ? 1 : ScoreBoard.spriteManager.Alpha;
                spectatorList.Alpha = Paused ? Math.Max(sbAlpha, spriteManagerPauseScreen.Alpha) : sbAlpha;
            }

            if (Ruleset.HpBar != null && Ruleset.HpBar.CurrentHp <= 0 &&
                !(ModManager.CheckActive(currentScore.enabledMods, Mods.NoFail) || Relaxing || Relaxing2) &&
                (!InputManager.ReplayMode || InputManager.ReplayToEnd) && !GameBase.TestMode &&
                ((Mode == PlayModes.Taiko && AudioEngine.Time > hitObjectManager.hitObjects[0].EndTime) || (Mode != PlayModes.Taiko && AudioEngine.Time > hitObjectManager.hitObjects[0].StartTime)))
            {
                OnFailableHp();
            }

            if (scorePreviousFrame != currentScore.totalScore || currentScore.countMiss != missPreviousFrame ||
                Failed != failedPreviousFrame)
            {
                OnScoreChanged();
                failedPreviousFrame = Failed;
                scorePreviousFrame = currentScore.totalScore;
                missPreviousFrame = currentScore.countMiss;
                forceReplayFrame = true;
            }

            if (Failed)
            {
                updateFailed();
                return;
            }

            if (Recovering && Ruleset.HpBar.DisplayHp > Ruleset.HpBar.CurrentHp - 5)
            {
                Recovering = false;
                AudioEngine.PlaySample(AudioEngine.LoadSample(@"gos"));
                AudioEngine.Continue();
            }

            HaxCheckAudio();

            if (Relaxing && Mode != PlayModes.Taiko)
            {
                foreach (HitObject h in hitObjectManager.hitObjectsMinimal)
                    if (h.StartTime - AudioEngine.Time < 12 &&
                        ((h.IsType(HitObjectType.Normal) && !h.IsHit) ||
                         (h.IsType(HitObjectType.Slider) && !((SliderOsu)h).StartIsHit)))
                    {
                        onGamePress(this, null);
                        if (GameBase.SixtyFramesPerSecondFrame)
                        {
                            Transformation t =
                                new Transformation(Color.Pink, Color.White, GameBase.Time, GameBase.Time + 150);
                            InputManager.s_Cursor.Transformations.Add(t);
                        }
                    }
            }

            if ((Relaxing || Relaxing2 || (KiaiActive && (InputManager.leftButton == ButtonState.Pressed || InputManager.rightButton == ButtonState.Pressed)))
                && GameBase.SixtyFramesPerSecondFrame && (IsSliding || IsSpinning))
                CursorBurst();

            if (KiaiActive)
                Ruleset.UpdateKiai();

            updateBreak();


            if (GameBase.FadeState == FadeStates.Idle && !leadInActive)
            {

                DrainTime = !((GameBase.TestMode && AudioEngine.TimeLastFrame <= GameBase.TestTime) ||
                    //Don't drain until we are in the required testing position at least
                              (InputManager.ReplayMode && InputManager.ReplayStartTime >= AudioEngine.TimeLastFrame)) &&
                    //Don't drain before the required replay position
                            AudioEngine.Time >= hitObjectManager.hitObjects[0].StartTime - hitObjectManager.PreEmpt &&
                    //After the first hitobject starts appearing
                            AudioEngine.Time <=
                            hitObjectManager.hitObjects[hitObjectManager.hitObjectsCount - 1].EndTime &&
                    //Before the last hitobject has been hit
                            resumeAfterBreakTime < 0 && AudioEngine.AudioState != AudioStates.Stopped &&
                    //Between a break and the last object before
                            EarlyStopDrainBreak();

                CheckPassed();


                if (hitObjectManager.hitObjects[hitObjectManager.hitObjectsCount - 1].EndTime + 3000 <= AudioEngine.Time &&
                    //More than 3 seconds after the last hitobject
                    (eventManager.events.Count == 0 || eventManager.lastEventTime - 500 <= AudioEngine.Time) &&
                    //More than 0.5 seconds after the last storyboard event.
                    (Passed || this is PlayerVs))
                    DoPass();

                if (EventManager.BreakMode && EventManager.BreakCurrent != null && (AudioEngine.Time > hitObjectManager.hitObjects[0].StartTime) &&
                    !breakAccountedFor && hitObjectManager.hitObjectsMinimal.Find(h => !h.IsHit && Math.Abs(h.EndTime - AudioEngine.Time) < 500) == null)
                {
                    UpdatePassing();

                    Ruleset.BreakStart();
                    breakAccountedFor = true;

                    if (EventManager.BreakCurrent.Length > 5000 && currentScore.totalHits > 5 && !Relaxing && !Relaxing2)
                        ShowBreakInfo();

                    if (EventManager.BreakCurrent.Length > 2880 && !Passed && !Failed)
                        showBreakRanking();
                }
                else if (!EventManager.BreakMode && !Passed && !Failed && breakAccountedFor)
                {
                    breakAccountedFor = false;
                    Ruleset.BreakStop();
                }
            }

            UpdateReplay();

            if (!EventManager.BreakMode && !Passed)
                Ruleset.ComboCounter.EnsureVisible();

            base.Update();

            if (InputManager.ReplayStreaming && InputManager.ReplayFrame == StreamingManager.ScoreSyncNext)
            {
                StreamingScoreSync();
                StreamingManager.ScoreSyncNext = -1;
            }

            NewSyncBeatWaiting = false;
        }

        private void UpdateProgress()
        {
            float pos = 0;

            if (AudioEngine.Time > hitObjectManager.hitObjects[0].StartTime)
                pos = (((float)AudioEngine.Time - hitObjectManager.hitObjects[0].StartTime) / (lastHitTime - hitObjectManager.hitObjects[0].StartTime));
            else
                pos = -1 + ((float)AudioEngine.Time + leadInTime) / (leadInTime + hitObjectManager.hitObjects[0].StartTime);

            if (progressBar != null) progressBar.SetProgress(OsuMathHelper.Clamp(pos, -2, 1));
        }

        private void LoadFrame(bScoreFrame f)
        {
            Score s = ScoreFactory.Create(Mode, f, currentScore.playerName);
            s.hpGraph = currentScore.hpGraph;
            s.replay = currentScore.replay;

            Ruleset.CurrentScore = currentScore = s;

            Ruleset.HpBar.SetCurrentHp(currentScore.currentHp);
            Ruleset.ComboCounter.HitCombo = currentScore.currentCombo;
            Ruleset.ScoreDisplay.currentScore = currentScore.totalScore;
        }



        private bool EarlyStopDrainBreak()
        {
            if (breakStopTimes.Count == 0)
                return true;
            return (AudioEngine.Time <= breakStopTimes.Peek()) || (BeatmapManager.Current.BeatmapVersion < 8);
        }

        protected virtual void IntroSkipBoundaryEnded()
        {
            skip.Hovering = false;
            skip.HandleInput = false;

            skip.Transformations.Clear();
            skip.FadeOut(200);
        }

        private void updateBreak()
        {
            //resumeAfterBreakTime makes sure drain doesn't start again after a break until the next hitcircle is reached.
            if (EventManager.BreakMode)
            {
                resumeAfterBreakTime = AudioEngine.Time;

                if (GameBase.SixtyFramesPerSecondFrame && currentUpdateFrame++ > 1) //Once every 3 frames.
                {
                    currentUpdateFrame = 0;
                    CursorBurst();
                }

                // be sure we've wiped out all breaks past
                if (breakStopTimes.Count > 0)
                {
                    while (breakStopTimes.Peek() < AudioEngine.Time)
                    {
                        // this is mega-paranoid because there should only be one
                        breakStopTimes.Dequeue();
                        if (breakStopTimes.Count == 0)
                            break;
                    }
                }
            }

            if (resumeAfterBreakTime >= 0 && hitObjectManager.hitObjectsMinimal.FindIndex(h => h.StartTime <= AudioEngine.Time && h.StartTime >= resumeAfterBreakTime) >= 0)
                resumeAfterBreakTime = -1;

        }

        private void updateFailed()
        {
            if (AudioEngine.AudioFrequency > 101)
            {
                if (GameBase.SixtyFramesPerSecondFrame)
                {
                    Bass.BASS_ChannelSetAttribute(AudioEngine.AudioTrack.audioStream, BASSAttribute.BASS_ATTRIB_FREQ, AudioEngine.AudioFrequency);
                    if (hitObjectManager.spriteManager.Alpha > 0)
                        hitObjectManager.spriteManager.Alpha =
                            Math.Max(0, hitObjectManager.spriteManager.Alpha - 0.007F);
                    Ruleset.SetAlpha(hitObjectManager.spriteManager.Alpha);
                    foreach (HitObject h in hitObjectManager.hitObjectsMinimal)
                    {
                        foreach (pSprite p in h.SpriteCollection)
                        {
                            p.Transformations.RemoveAll(t => t.Type == TransformationType.Movement);
                            p.AlwaysDraw = true;
                            p.Position = new Vector2(p.Position.X, p.Position.Y < 0 ? p.Position.Y * 0.6F : p.Position.Y * 1.01F);
                            if (p.Rotation == 0)
                                p.Rotation += GameBase.random.Next(-2, 2) * 0.01f;
                            else if (p.Rotation > 0)
                                p.Rotation += 0.01f;
                            else
                                p.Rotation -= 0.01f;
                        }
                    }
                    AudioEngine.AudioFrequency -= 300;
                }
            }
            else if (GameBase.FadeState == FadeStates.Idle)
            {
                //ChatEngine.SendSpectateEnd(this);
                OnFailed();
            }
        }

        int lastCustomCombo;
        private void updateCustomComboBurstSounds()
        {
            if (SkinManager.Current.comboSoundBursts.Count == 0 || lastCustomCombo == Ruleset.ComboCounter.HitCombo)
                return;

            int found = SkinManager.Current.comboSoundBursts.BinarySearch(Ruleset.ComboCounter.HitCombo);
            if (found >= 0)
                AudioEngine.PlaySample(@"comboburst-" + found, 100, SkinSource.All);

            lastCustomCombo = Ruleset.ComboCounter.HitCombo;
        }

        protected void UpdateScore()
        {
            Ruleset.ScoreDisplay.Update(VisibleScore.totalScore);
            Ruleset.ScoreDisplay.Update(VisibleScore.accuracy * 100);
        }

        internal virtual Score VisibleScore
        {
            get { return currentScore; }
        }

        internal void UpdateActive()
        {
            ActiveHitObject = Failed ? null :
                hitObjectManager.hitObjectsMinimal.Find(
                    h => h.StartTime != h.EndTime && h.HittableStartTime <= AudioEngine.Time && (h.HittableEndTime >= AudioEngine.Time || !h.IsHit));

            IsSliding = ActiveHitObject != null && ActiveHitObject.IsType(HitObjectType.Slider);
            IsSpinning = ActiveHitObject != null && ActiveHitObject.IsType(HitObjectType.Spinner);
        }

        protected internal void CheckPassed()
        {
            if (Passed)
                return;


            if (!DrainTime && !Recovering && ((AudioEngine.Time > 0 && AudioEngine.AudioState == AudioStates.Stopped) || AudioEngine.Time > lastHitTime + 200))
            {
                currentScore.scoringSectionResults.Add(Ruleset.IsPassing);
                UpdatePassing(currentScore.scoringSectionResults.FindAll(t => t).Count > currentScore.scoringSectionResults.FindAll(t => !t).Count);

                if (InputManager.ReplayMode || GameBase.TestMode || (!CheckAllNotesHit || Ruleset.AllNotesHit))
                {
                    ProcessPassed();
                }
                else
                {
                    Ruleset.HpBar.SetCurrentHp(0);
                }
            }
        }

        protected virtual void ProcessPassed()
        {
            spriteManager.RemoveRange(s_arrows);
            Ruleset.OnCompletion();
        }

        protected virtual bool CheckAllNotesHit
        {
            get { return true; }
        }

        /// <summary>
        /// Shows the pass/fail sprite during the break.
        /// </summary>
        private void showBreakRanking()
        {
            if (IsTutorial || !Ruleset.AllowWarningArrows)
                return;

            currentScore.scoringSectionResults.Add(Passing);

            int duration = EventManager.BreakCurrent.Length;

            int start = (duration / 2 > 2880
                             ? EventManager.BreakCurrent.StartTime + (duration / 2)
                             : EventManager.BreakCurrent.EndTime - 2880);

            if (Passing)
            {
                AudioEngine.SampleEvents.Add(new SampleCacheItem(start + 20, AudioEngine.LoadSample(@"sectionpass"), 100, false));
                s_breakPass.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, start + 20, start + 20));
                s_breakPass.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, start + 100, start + 100));
                s_breakPass.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, start + 160, start + 160));
                s_breakPass.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, start + 230, start + 230));
                s_breakPass.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, start + 280, start + 280));
                s_breakPass.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, start + 1280, start + 1480));
            }
            else
            {
                AudioEngine.SampleEvents.Add(new SampleCacheItem(start + 130, AudioEngine.LoadSample(@"sectionfail"), 100, false));
                s_breakFail.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, start + 130, start + 130));
                s_breakFail.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, start + 230, start + 230));
                s_breakFail.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, start + 280, start + 280));
                s_breakFail.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, start + 1280, start + 1480));
                spriteManager.Add(s_breakFail);
            }
        }

        private void ShowBreakInfo()
        {
            if (IsTutorial || ModManager.CheckActive(Mods.Cinema))
                return;

            VisibleScore.pass = true; //Temporarily set so we don't get a forced fail.
            Rankings r = VisibleScore.ranking;
            VisibleScore.pass = false;

            if (r < Rankings.F)
            {
                Vector2 pos = ConfigManager.sProgressBarType != ProgressBarTypes.Pie ? Ruleset.ScoreDisplay.leftOfDisplay :
                    progressBar.Position + new Vector2(28, 0);

                pSprite rankLetter =
                    new pSprite(SkinManager.Load(@"ranking-" + r + "-small"), Fields.TopRight,
                                Origins.Centre,
                                Clocks.Audio, pos, 0.9F, false, Color.TransparentWhite);

                if (ConfigManager.sShowInterface.Value)
                {
                    rankLetter.Tag = @"rankletter";
                    rankLetter.Scale = 0.5f;
                    rankLetter.Additive = true;

                    rankLetter.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0,
                                                                      AudioEngine.Time + 400,
                                                                      AudioEngine.Time + 1600, EasingTypes.Out));
                    rankLetter.Transformations.Add(new Transformation(TransformationType.Scale, 0.8f, 1.3f,
                                                                      AudioEngine.Time + 400,
                                                                      AudioEngine.Time + 1600, EasingTypes.Out));
                    spriteManagerAdd.Add(rankLetter);

                    rankLetter =
                        new pSprite(SkinManager.Load(@"ranking-" + r + "-small"), Fields.TopRight,
                                    Origins.Centre,
                                    Clocks.Audio, pos, 0.9F, false, Color.TransparentWhite);

                    rankLetter.Tag = @"rankletter";
                    rankLetter.Scale = 0.8f;
                }

                rankLetter.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1,
                                                                  AudioEngine.Time + 400,
                                                                  AudioEngine.Time + 700, EasingTypes.Out));
                rankLetter.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0,
                                                                  EventManager.BreakCurrent.EndTime - 1000,
                                                                  EventManager.BreakCurrent.EndTime - 700, EasingTypes.In));

                spriteManagerInterfaceWidescreen.Add(rankLetter);
            }
        }

        private void HidePauseCursor()
        {
            if (pauseCursor != null)
            {
                foreach (pSprite p in spriteManager.GetTagged(@"pausecursor"))
                {
                    p.AlwaysDraw = false;
                    p.FadeOut(40);
                }

                pauseCursor = null;
                pauseCursorText = null;
            }

            Unpausing = false;
            UnpauseConfirmed = false;
        }

        private void UpdatePhysics()
        {
            if (GameBase.SixtyFramesPerSecondFrame)
            {
                mouseVelocity = InputManager.s_Cursor.Position / GameBase.WindowRatio - mouseVelocityLastPosition;
                mouseVelocityLastPosition = InputManager.s_Cursor.Position / GameBase.WindowRatio;
            }
        }

        internal virtual void OnFailableHp()
        {
            Ruleset.Fail(false);
        }

        protected virtual void OnFailed()
        {
            TogglePause();
        }

        protected virtual void OnScoreChanged()
        {
            //keep a history of score frames.
            if (currentScore.Frames.Count == 0 || AudioEngine.Time > currentScore.Frames[currentScore.Frames.Count - 1].time)
                currentScore.Frames.Add(Player.GetScoreFrame());

            checkFlashlightHax();
        }

        protected virtual void UpdateScoreboard()
        {
            if (ScoreBoard != null && scoreEntry != null)
            {
                if (GameBase.SixtyFramesPerSecondFrame)
                {
                    if (Passed || Failed || (!EventManager.BreakMode && !ScoreBoard.AlwaysVisible))
                    {
                        if (ScoreBoard.spriteManager.Alpha > 0)
                        {
                            ScoreBoard.spriteManager.Alpha = Math.Max(0, ScoreBoard.spriteManager.Alpha - 0.08f);
                        }
                    }
                    else if (ScoreBoard.spriteManager.Alpha < 1)
                    {
                        ScoreBoard.spriteManager.Alpha = Math.Min(1, ScoreBoard.spriteManager.Alpha + 0.08f);
                    }
                }

                if (scoreEntry != null && scoreEntry.score != currentScore.totalScore)
                {
                    scoreEntry.SetScore(currentScore);
                    ScoreBoard.Reorder(false);
                }
            }
        }

        private void CursorBurst(Color? burstColour = null)
        {
            if (AudioEngine.Paused || HideMouse)
                return;

            if (Playing && !Ruleset.UsesMouse)
                return;


            if (burstColour == null) burstColour = starBreakAdditive;

            int time2 = GameBase.Time + GameBase.random.Next(300, 1000);

            pSprite p2 = new pSprite(t_star2, Fields.TopLeft,
                                     Origins.Centre,
                                     Clocks.Game,
                                     new Vector2(InputManager.s_Cursor.Position.X, InputManager.s_Cursor.Position.Y) / GameBase.WindowRatio, 0.05F, false, burstColour.Value);
            Transformation ts =
                new Transformation(TransformationType.Scale, 1, (float)(GameBase.random.NextDouble() * (ConfigManager.sMyPcSucks ? 1.1 : 2)), GameBase.Time,
                                   time2);

            Transformation tf = new Transformation(TransformationType.Fade,
                                                   InputManager.s_Cursor.Alpha, 0, GameBase.Time,
                                                   time2);
            Transformation tr =
                new Transformation(TransformationType.Rotation, (float)(GameBase.random.NextDouble() * 4 - 2),
                                   (float)(GameBase.random.NextDouble() * 4 - 2), GameBase.Time, time2);

            Vector2 initialGravity = mouseVelocity * 4;

            if ((Relaxing || Relaxing2 || KiaiActive) && !EventManager.BreakMode)
            {
                if (!(IsSliding || IsSpinning))
                    initialGravity = new Vector2(GameBase.random.Next(-500, 500), GameBase.random.Next(-500, 500));
            }
            else if (InputManager.leftButton == ButtonState.Pressed &&
                     InputManager.rightButton == ButtonState.Pressed)
                initialGravity += new Vector2(GameBase.random.Next(-460, 460), GameBase.random.Next(-160, 160));
            else if (InputManager.leftButton == ButtonState.Pressed)
                initialGravity += new Vector2(GameBase.random.Next(-460, 0), GameBase.random.Next(-40, 40));
            else if (InputManager.rightButton == ButtonState.Pressed)
                initialGravity += new Vector2(GameBase.random.Next(0, 460), GameBase.random.Next(-40, 40));

            ts.Easing = EasingTypes.Out;
            tr.Easing = EasingTypes.Out;

            p2.Transformations.Add(ts);
            p2.Transformations.Add(tf);
            p2.Transformations.Add(tr);

            p2.Additive = true;

            spriteManagerAdd.Add(p2);
            GameBase.PhysicsManager.Add(p2, initialGravity);
        }

        internal void UpdatePassing(IncreaseScoreType comboAddition)
        {
            UpdatePassing((comboAddition & IncreaseScoreType.GekiAddition) > 0);
        }

        internal void UpdatePassing()
        {
            UpdatePassing(Ruleset.IsPassing);
        }

        internal void UpdatePassing(bool value)
        {
            if (IsTutorial)
                return;

            if (value == Passing)
                return;

            Ruleset.UpdatePassing(value);

            Passing = value;

            if (mode != PlayModes.Taiko && eventManager.HasPassFailLayers)
                eventManager.Flash(0.2f);
        }

        internal void DoPass()
        {
            if (StreamingManager.HasSpectators && !InputManager.ReplayMode)
                StreamingManager.PurgeFrames(ReplayAction.Completion);

            if (GameBase.TestMode)
            {
                InputManager.ReplayMode = false;
                GameBase.ChangeMode(OsuModes.Edit);
            }
            else
            {
                if (currentScore.ranking == Rankings.F && !(this is PlayerVs))
                {
                    NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Player_ErrorDuringScoreCalculation));
                    GameBase.ChangeMode(OsuModes.SelectPlay);
                }
                else
                    GotoRanking();
            }
        }

        internal virtual void GotoRanking(bool force = false)
        {
            GameBase.ChangeMode(OsuModes.Rank);
        }

        public override void Draw()
        {
            if (Status != PlayerStatus.Busy)
            {
                if (Ruleset.ComboCounter.HitCombo == 0)
                    flameFlashLast = 0;

                if (Ruleset.IsMilestoneCombo && flameFlashLast < Ruleset.ComboCounter.HitCombo)
                    edgeStartBurstPending = true;

                HitObject spinnerCheck = hitObjectManager.hitObjectsMinimal.Find(s => s is SpinnerOsu && !s.IsHit && s.SpriteCollection[0].IsVisible);
                bool spinnerOptimise = !(Ruleset is RulesetTaiko) && SkinManager.Current.SpinnerFadePlayfield && spinnerCheck != null && OptimiseSpinnerDraw;

                //If the spinner totally visible, we can hide background draw events to decrease overdraw.
                if (!spinnerOptimise || spinnerCheck.SpriteCollection[0].Alpha != 1)
                {
                    if (spinnerOptimise && GameBase.Widescreen)
                        eventManager.spriteManagerBGWide.Blackness = Math.Max(eventManager.spriteManagerBGWide.Blackness, (EventManager.UserDimLevel + spinnerCheck.SpriteCollection[0].Alpha * (100 - EventManager.UserDimLevel)) / 100f);

                    eventManager.DrawBG();

                    if (BeatmapManager.Current.StoryFireInFront)
                        eventManager.DrawFG();


                    if (!IsSpinning)
                        DrawFlame(
                            OsuMathHelper.Clamp(
                                (float)(Ruleset.ComboCounter.DisplayCombo - 29) / 300 *
                                (Ruleset.HpBar != null ? Ruleset.HpBar.s_barBg.Alpha : 0), 0,
                                (float)BeatmapManager.MapRange(ConfigManager.sComboFireHeight, 0.05, 0.3, 0.8)));

                    if (!BeatmapManager.Current.StoryFireInFront)
                        eventManager.DrawFG();
                }

                spriteManagerMetadata.Blackness = EventManager.Instance.spriteManagerBGWide.Blackness;
                spriteManagerMetadata.Draw();

                spriteManagerBelowScoreboardWidescreen.Draw();

                DrawScoreboardLevelItems();

                spriteManagerBelowHitObjectsWidescreen.Draw();

                drawRuleset();

                spriteManager.Draw();

                spriteManagerAdd.Alpha = !Paused || pauseSprites == null ? 1 : 1 - (pauseSprites[0].Alpha);
                spriteManagerAdd.Draw();

                DrawOverlay();

                spriteManagerInterface.Draw();

                if (progressBar != null)
                    progressBar.Draw();

                spriteManagerInterfaceWidescreen.Draw();

                if (inputOverlay != null && InputManager.ReplayMode)
                    inputOverlay.Draw();

                if (Paused || Unpausing)
                    spriteManagerPauseScreen.Draw();

                spriteManagerHighest.Draw(); //Skip button, volume control etc.
            }

            if (!Loader.Loaded)
            {
                if (SpriteManagerLoading != null) SpriteManagerLoading.Draw();
                spriteManagerMetadata.Draw();
                Loader.Draw();
            }

            if (visualSettings != null) visualSettings.Draw();
            if (scrubber != null) scrubber.Draw();

            base.Draw();
        }

        protected virtual void drawRuleset()
        {
            Ruleset.DrawBefore();

            //display for playing
            if (inputOverlay != null && ConfigManager.sKeyOverlay && !InputManager.ReplayMode) inputOverlay.Draw();

            hitObjectManager.Draw();

            Ruleset.Draw();
        }

        /// <summary>
        /// Draw any extension UI overlays for subclasses in here.
        /// </summary>
        protected virtual void DrawOverlay()
        {
        }

        protected virtual void DrawScoreboardLevelItems()
        {
            if (ScoreBoard != null)
                ScoreBoard.Draw();
        }

        protected virtual bool AllowFailShaderEffects
        {
            get { return true; }
        }

        private void UpdateBloomEffects()
        {
            if (ConfigManager.sBloom)
            {
                if (Paused)
                {
                    BloomRenderer.ExtraPass = false;
                }
                else if (Ruleset.HpBar != null && Ruleset.HpBar.CurrentHp < 40 && !GameBase.TestMode && !Relaxing && !Relaxing2 && Ruleset.AllowFailTint && AllowFailShaderEffects && GameBase.FadeState == FadeStates.Idle)
                {
                    BloomRenderer.ExtraPass = true;

                    if (!Failed)
                    {
                        BloomRenderer.Magnitude = 0.0001f;
                        BloomRenderer.Alpha = 1;
                        BloomRenderer.RedTint = (float)Math.Max(0, (40 - Ruleset.HpBar.DisplayHp) / 160);
                        BloomRenderer.HiRange = false;
                        BloomRenderer.Additive = false;
                    }
                    else if (GameBase.SixtyFramesPerSecondFrame)
                    {
                        BloomRenderer.Magnitude = 0.006f *
                                                  ((AudioEngine.InitialFrequency - AudioEngine.AudioFrequency) /
                                                   AudioEngine.InitialFrequency);
                        BloomRenderer.RedTint = (float)Math.Max(0, (40 - Ruleset.HpBar.DisplayHp) / 160);
                        BloomRenderer.HiRange = false;
                        BloomRenderer.Additive = false;
                        BloomRenderer.Alpha = Math.Min(1, BloomRenderer.Alpha + 0.01f);
                    }
                }
                else if (IsSpinning && !BloomRenderer.ExtraPass)
                {
                    SpinnerOsu s = (SpinnerOsu)ActiveHitObject;
                    BloomRenderer.Magnitude = 0.008f;
                    BloomRenderer.RedTint = 0;
                    BloomRenderer.ExtraPass = true;
                    BloomRenderer.HiRange = false;
                    BloomRenderer.Additive = true;
                    BloomRenderer.Alpha = Math.Min(0.5f,
                                                   Math.Max(0,
                                                            (Math.Abs(s.rotationCount) - s.rotationRequirement + 1) /
                                                            (s.rotationRequirement * 0.6f)));
                }
                else if (bloomBurst > 0)
                {
                    BloomRenderer.Magnitude = 0.0001f;
                    BloomRenderer.RedTint = 0;
                    BloomRenderer.ExtraPass = true;
                    BloomRenderer.HiRange = false;
                    BloomRenderer.Additive = true;
                    BloomRenderer.Alpha = bloomBurst * 0.2f;
                    bloomBurst -= (float)(0.05 * GameBase.FrameRatio);
                }
                else
                {
                    BloomRenderer.ExtraPass = false;
                }
            }
        }

        internal void DrawFlame(float height)
        {
            if (height == 0 || ModManager.CheckActive(Mods.Cinema))
                return;

            if (edgeStartBurstPending)
            {
                if (SkinManager.Current.comboSoundBursts.Count == 0)
                {
                    int sample = AudioEngine.LoadSample(@"comboburst-" + burstSampleNumber);
                    if (sample == -1)
                    {
                        if (burstSampleNumber > 0)
                        {
                            //we hit the end of available burst samples; return to zero and prep the next one.
                            sample = AudioEngine.LoadSample(@"comboburst-0");
                            burstSampleNumber = 1;
                        }
                        else
                            sample = AudioEngine.LoadSample(@"comboburst");
                    }
                    else
                        burstSampleNumber++;

                    AudioEngine.PlaySample(sample);
                }

                flameFlashLast = Ruleset.ComboCounter.HitCombo;
                edgeStartBurstPending = false;

                if (EventManager.UserDimLevel != 100)
                    eventManager.Flash(ConfigManager.sComboBurst ? 0.8f : 0.4f);

                bloomBurst = 1;

                Ruleset.ComboBurst();
            }

            if (ConfigManager.sComboFire)
                fireRenderer.Draw(height, Ruleset.ComboCounter.HitCombo);
        }

        public event HitObjectDelegate OnClick;

        public static void UpdateIsPlaying()
        {
            bool oldPlaying = Playing;

            if (GameBase.Mode != OsuModes.Play || Instance == null)
            {
                Playing = false;
            }
            else
            {
                try
                {
                    Playing = !OutroActive
                        && Instance.Status == PlayerStatus.Playing
                        && Instance.hitObjectManager != null
                        && Instance.hitObjectManager.hitObjectsCount > 0
                        && AudioEngine.Time >= Instance.hitObjectManager.EarliestHitObject.HittableStartTime - Instance.hitObjectManager.PreEmpt
                        && AudioEngine.Time <= Instance.hitObjectManager.LatestHitObject.HittableEndTime + Instance.hitObjectManager.HitWindow50
                        && !Failed && !Paused && !EventManager.BreakMode && !currentScore.pass && (!InputManager.ReplayMode || InputManager.ReplayCursorFollow || Relaxing2);
                }
                catch
                {
                    //rare case where this method is called before beatmap is loaded, or on the wrong thread.
                    Playing = false;
                }
            }
        }

        static bool canGcLatency = true;
        private static void SetGCLowLatency(bool status)
        {
            if (!canGcLatency) return;

            try
            {
                _setGCLowLatency(status);
            }
            catch
            {
                canGcLatency = false;
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        static void _setGCLowLatency(bool enabled)
        {
            GCSettings.LatencyMode = enabled ? GCLatencyMode.LowLatency : GCLatencyMode.Interactive;
        }

        #region User Input Handling
        internal void LogHitError(HitObject h, int? customDelta = null)
        {
            if (currentScore.errorList != null)
            {
                if (customDelta.HasValue)
                    currentScore.errorList.Add(customDelta.Value);
                else
                    currentScore.errorList.Add(AudioEngine.Time - h.StartTime);
            }

            if (Ruleset.ScoreMeter != null)
                Ruleset.ScoreMeter.AddMeter(h, customDelta);
        }

        internal void LogSpinSpeed(int speed)
        {
            if (currentScore.spinSpeedList != null)
                currentScore.spinSpeedList.Add(speed);
        }

        private bool onGamePress(object sender, EventArgs e)
        {
            if (GameBase.FadeState == FadeStates.FadeOut)
                return false;

            if (Failed || Paused || Unpausing || Recovering)
                return false;

            if (!MouseManager.GameClickRegistered && !Relaxing && Mode != PlayModes.Taiko)
                return false;

            //Don't accept mouse clicks in relax mode.
            if (Relaxing && sender == null && Mode != PlayModes.Taiko)
                return false;

            if (OutroActive)
                return false;

            Vector2 mouseGamefield = InputManager.ReplayMode
                                         ? InputManager.ReplayGamefieldCursor
                                         : GameBase.DisplayToGamefield(InputManager.CursorPosition);

            // if we are in relaxmode and in taiko mode we want to change the input
            // to the right buttons temporarily
            HitObject h;
            if (Relaxing2)
                h = hitObjectManager.FindCircleAt(mouseGamefield.X, mouseGamefield.Y, true, 100f); // give autopilot a wide berth
            else
                h = hitObjectManager.FindCircleAt(mouseGamefield.X, mouseGamefield.Y, true);

            if (h != null)
            {
                inputReceivedAtLeastOnce = true;

                switch (Ruleset.CheckClickAction(h))
                {
                    case ClickAction.Ignore:
                        break; //; extra semicolon??
                    case ClickAction.Shake:
                        h.Shake();
                        break;
                    case ClickAction.Hit:
                        if (h.IsType(HitObjectType.Normal))
                        {
                            IncreaseScoreType hitValue = hitObjectManager.Hit(h);

                            forceReplayFrame = true;

                            if (hitValue > 0)
                            {
                                if (Relaxing || KiaiActive || Relaxing2)
                                    for (int i = 0; i < (ConfigManager.sMyPcSucks ? 5 : 6); i++) CursorBurst(h.Colour);
                                LogHitError(h);
                                OnHitSuccess(h);
                            }
                        }
                        else if (h.IsType(HitObjectType.Slider))
                        {
                            SliderOsu s = h as SliderOsu;
                            //slider special case
                            if (!s.StartIsHit)
                            {
                                if (s.HitStart() > 0)
                                {
                                    ActiveHitObject = h;
                                    Ruleset.IncreaseScoreHit(IncreaseScoreType.SliderRepeat, h);
                                    forceReplayFrame = true;
                                    LogHitError(h);
                                    if (Relaxing || Relaxing2)
                                        for (int i = 0; i < 6; i++)
                                            CursorBurst(h.Colour);
                                }
                                else
                                    Ruleset.IncreaseScoreHit(IncreaseScoreType.MissHpOnly, h);
                            }
                        }
                        break;
                }
            }

            if (OnClick != null)
                OnClick(h);

            return true;
        }


        private bool onClick(object sender, EventArgs e)
        {
            if (GameBase.FadeState == FadeStates.FadeOut)
                return false;

            if (MouseManager.MouseMiddle == ButtonState.Pressed)
            {
                if (GameBase.Mode == OsuModes.Play && !(this is PlayerVs) && !ConfigManager.sMouseDisableWheel && !InputManager.ReplayMode)
                {
                    TogglePause();
                    return true;
                }
            }

            return false;
        }

        internal virtual void OnHitSuccess(HitObject h)
        {
        }

        /// <summary>
        /// Handles recording of input for replay/spectating.
        /// </summary>
        private void UpdateReplay()
        {
            if ((InputManager.ReplayMode && (!InputManager.ReplayStreaming || InputManager.ReplayStartTime != 0)) || AudioEngine.AudioState != AudioStates.Playing || Failed || Paused || this is PlayerTest)
                return;

            pButtonState newState = MouseManager.pMouseCreate(InputManager.leftButton1, InputManager.leftButton2, InputManager.rightButton1, InputManager.rightButton2, KeyboardHandler.IsKeyDown(BindingManager.For(Bindings.OsuSmoke)));
            Vector2 mp = Ruleset.MousePosition;

            if (InputManager.MouseButtonInstantRelease)
            {
                /* Special case to handle this scenario:
                 *  -MouseLeft pressed and held...
                 *  -Keyboard mouseLeft1 pressed
                 * The resulting replay will not contain the mouseLeft1 event as the state has not changed.
                 * We Will solve this by sending a frame to add an upwards release here...
                 */
                if (InputManager.lastButtonState == newState)
                {
                    pButtonState comp = newState;

                    if (InputManager.leftButton1i)
                        newState &= ~pButtonState.Left1;
                    if (InputManager.leftButton2i)
                        newState &= ~pButtonState.Left2;
                    if (InputManager.leftButton1i)
                        newState &= ~pButtonState.Right1;
                    if (InputManager.rightButton2i)
                        newState &= ~pButtonState.Right2;

                    if (newState != comp)
                    {
                        //If this new state doesn't match, we know there is a necessity to do this hack.
                        bReplayFrame extraFrame = new bReplayFrame(AudioEngine.Time - 1, mp.X, mp.Y, newState);
                        currentScore.replay.Add(extraFrame);
                        StreamingManager.PushNewFrame(extraFrame);
                    }
                }
            }

            bReplayFrame f;
            if (Mode == PlayModes.OsuMania)
                f = new bReplayFrame(AudioEngine.Time, InputManager.ManiaKeyValue(), SpeedMania.Speed, pButtonState.None);
            else
                f = new bReplayFrame(AudioEngine.Time, mp.X, mp.Y, newState);

            bool frameContainsImportantData = (IsSpinning && GameBase.SixtyFramesPerSecondFrame) || //Currently Spinning
                                              forceReplayFrame || InputManager.lastButtonState != newState;

            if (Ruleset.ComboCounter.HitCombo != Ruleset.ComboCounter.HitComboLast)
            {
                Ruleset.ComboCounter.HitComboLast = Ruleset.ComboCounter.HitCombo;
                frameContainsImportantData = true;
            }

            frameContainsImportantData |= Ruleset.IsImportantFrame();

            InputManager.lastButtonState = newState;

            if (GameBase.SixtyFramesPerSecondFrame || frameContainsImportantData)
            {
                StreamingManager.PushNewFrame(f);
                currentScore.replay.Add(f);
                if (forceReplayFrame) forceReplayFrame = false;
            }
        }

        //todo: move this
        private void hitObjectManager_ForcedHit(IncreaseScoreType value, string comboBonus, HitObject h)
        {
            PendingScoreChangeObject = h;

            Ruleset.IncreaseScoreHit(value, h);

            if ((value & IncreaseScoreType.HitValuesOnly) > 0 && hitObjectManager.lastHitObject.MaxHp > 0)
                currentScore.hpGraph.Add(
                    new Vector2(AudioEngine.Time,
                                (float)(Math.Min(1, Ruleset.HpBar.CurrentHp / hitObjectManager.lastHitObject.MaxHp))));

            updateCustomComboBurstSounds();
        }

        int lastFrameTime;
        internal HitObject PendingScoreChangeObject;
        internal static bool OutroSkippable;
        private Color starBreakAdditive;
        private int pauseSelected;
        private List<pSprite> pauseArrows = new List<pSprite>();
        private bool FinishedInitialAudioSetup;
        private bool AsyncLoadComplete;
        private int audioStartTime;
        internal bool IsAutoplayReplay;
        private bool reloadingAfterChange;
        private RankingType rankType;
        public static bool PendingRestart;
        internal int leadInTime;
        private pSprite loadingSpinner;

        public static bScoreFrame GetScoreFrame()
        {
            try
            {
                Player currentPlay = Instance;
                if (currentPlay == null)
                    return new bScoreFrame();

                bScoreFrame frame = new bScoreFrame
                                        {
                                            count300 = currentScore.count300,
                                            count100 = currentScore.count100,
                                            count50 = currentScore.count50,
                                            countGeki = currentScore.countGeki,
                                            countKatu = currentScore.countKatu,
                                            countMiss = currentScore.countMiss,
                                            totalScore = currentScore.totalScore,
                                            maxCombo = (ushort)currentScore.maxCombo,
                                            currentCombo = (ushort)currentPlay.Ruleset.ComboCounter.HitCombo,
                                            perfect = currentScore.perfect,
                                            currentHp = currentPlay.Ruleset.HpBar != null ? (int)currentPlay.Ruleset.HpBar.CurrentHp : 0,
                                            time = AudioEngine.Time,
                                            pass = !Failed
                                        };
                return frame;
            }
            catch
            {
                return new bScoreFrame(); //could happen in a race condition when ruleset is nulled etc.
            }
        }

        public static void StreamingScoreSync()
        {
            bScoreFrame frame = StreamingManager.ScoreSyncFrame;

            currentScore.count300 = frame.count300;
            currentScore.count100 = frame.count100;
            currentScore.count50 = frame.count50;
            currentScore.countGeki = frame.countGeki;
            currentScore.countKatu = frame.countKatu;
            currentScore.countMiss = frame.countMiss;
            currentScore.totalScore = frame.totalScore;
            currentScore.totalScoreD = frame.totalScore;
            currentScore.maxCombo = frame.maxCombo;
            Instance.Ruleset.ComboCounter.SetCombo(frame.currentCombo);
            currentScore.perfect = frame.perfect;
        }

        #endregion

        protected virtual bool OptimiseSpinnerDraw { get { return true; } }
    }

    internal enum PlayerStatus
    {
        Busy,
        Intro,
        Playing,
        Resting,
        Outro,
        Failed,
        Exiting,
        ExitingSpectator
    }

    internal enum ClickAction
    {
        Ignore,
        Shake,
        Hit
    }
}
