﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using osu.GameModes.Play.Components;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu_common;

namespace osu.GameModes.Play
{
    class ReplayWatcher : Player
    {
        private ReplayOverlay replayOverlay;

        public ReplayWatcher()
            : base()
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (replayOverlay != null) replayOverlay.Dispose();
            base.Dispose(disposing);
        }

        protected override void InitializeReplay()
        {
            replayOverlay = new ReplayOverlay(this);
            replayOverlay.Initialize();

            //Reset some replay vars.
            GameBase.TestMode = false;
            InputManager.ReplayModeFF = ReplaySpeed.Normal;
            InputManager.ReplayFrame = 0;

            InputManager.ResetButtons();

            if (!InputManager.ReplayStreaming)
                StreamingManager.Reset();

            if (InputManager.ReplayStartTime > 0)
            {
                //Streaming score uses replayscore for all scoring.
                currentScore = InputManager.ReplayScore;

                //Initialize a new hpgraph since we don't receive this in replay data.
                currentScore.hpGraph = new List<Vector2>();
                currentScore.errorList = new List<int>(BeatmapManager.Current.ObjectCount);
                ResetScore(hitObjectManager.Beatmap);
            }

            if (InputManager.ReplayScore.replay.Count >= 2 && InputManager.ReplayScore.replay[1].time < InputManager.ReplayScore.replay[0].time)
            {
                //Fixes a bug that is hard-recorded into many replays where the first two invisible-mouse frames are the wrong way around.
                InputManager.ReplayScore.replay[1].time = InputManager.ReplayScore.replay[0].time;
                InputManager.ReplayScore.replay[0].time = 0;
            }

            Debug.Print("Replay has " + InputManager.ReplayScore.replay.Count + " frames");
            Debug.Print("Replay has " +
                              InputManager.ReplayScore.replay.FindAll(f => f.mouseLeft || f.mouseRight).Count +
                              " mousedown frames");
        }

        protected override bool onKeyPressed(object sender, Keys k)
        {
            switch (k)
            {
                case Keys.F:
                    if (replayOverlay.buttonFF == null) return false;
                    replayOverlay.buttonFF.Click();
                    return true;
            }

            return base.onKeyPressed(sender, k);
        }

        protected override bool OnLoadComplete(bool success)
        {
            if ((Mods)InputManager.ReplayScore.enabledMods > 0)
                replayOverlay.PushButtonsDownwards(44);

            if (ModManager.CheckActive(Mods.Cinema))
            {
                if (Ruleset.ScoreMeter != null)
                {
                    Ruleset.ScoreMeter.Dispose();
                    Ruleset.ScoreMeter = null;
                }

                if (inputOverlay != null)
                {
                    inputOverlay.Dispose();
                    inputOverlay = null;
                }
            }

            return base.OnLoadComplete(success);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void DrawOverlay()
        {
            if (ModManager.CheckActive(Mods.Cinema))
                return;

            if (InputManager.ReplayMode)
                replayOverlay.Draw();
            base.DrawOverlay();
        }

        public override void Update()
        {
            if (!Loader.Loaded)
            {
                base.Update();
                return;
            }

            if (InputManager.ReplayStreaming)
            {
                if (InputManager.Buffering && !Paused)
                    TogglePause();
                else if (!InputManager.Buffering && Paused)
                    TogglePause();
            }

            replayOverlay.Update();

            base.Update();

        }

        public override void Draw()
        {
            if (hitObjectManager != null && ModManager.CheckActive(Mods.Cinema))
            {
                hitObjectManager.spriteManager.Alpha = 0.01f;
                spriteManager.Alpha = 0;
                spriteManagerHighest.Alpha = 0;
                spriteManagerInterfaceWidescreen.Alpha = 0;
                spriteManagerInterface.Alpha = 0;
                spriteManagerAdd.Alpha = 0;
                spriteManagerBelowHitObjectsWidescreen.Alpha = 0;
            }

            base.Draw();
        }

        protected override void drawRuleset()
        {
            if (ModManager.CheckActive(Mods.Cinema))
                hitObjectManager.Draw(); //only draw hitobjects.
            else
                base.drawRuleset();
        }

        public override bool HideMouse
        {
            get
            {
                if (ModManager.CheckActive(Mods.Cinema))
                    return true;
                return base.HideMouse;
            }
        }

        protected override void DrawScoreboardLevelItems()
        {
            if (ModManager.CheckActive(Mods.Cinema))
                return;
            base.DrawScoreboardLevelItems();
        }

        protected override bool OptimiseSpinnerDraw
        {
            get
            {
                if (ModManager.CheckActive(Mods.Cinema))
                    return false;
                return base.OptimiseSpinnerDraw;
            }
        }

        internal override void GotoRanking(bool force = false)
        {
            if (ModManager.CheckActive(Mods.Cinema))
            {
                GameBase.ChangeMode(OsuModes.SelectPlay);
                return;
            }

            base.GotoRanking(force);
        }
    }
}
