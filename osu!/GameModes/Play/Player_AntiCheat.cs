using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Helpers;
using osu.Input;
using osu_common;

namespace osu.GameModes.Play
{
    internal partial class Player : DrawableGameComponent
    {
        internal static BadFlags flag;

        private int audioCheckCount;
        private int audioCheckTime = -1;
        private int audioCheckTimeComp;


        private bool AllowSubmissionConstantConditions
        {
            get
            {
                return !InputManager.ReplayMode && !GameBase.TestMode
                       && Ruleset.AllowSubmission
                       && BeatmapManager.Current.SubmissionStatus != SubmissionStatus.NotSubmitted
                       && BeatmapManager.Current.SubmissionStatus != SubmissionStatus.Pending
                       && !BeatmapManager.Current.UpdateAvailable
                       && ModManager.AllowRanking(currentScore.enabledMods) && currentScore.AllowSubmission
                       && allowSubmissionHaxCheck && (!BeatmapManager.Current.ServerHasOsz2 || BeatmapManager.Current.InOszContainer);
            }
        }

        private bool AllowSubmissionVariableConditions
        {
            get
            {
                return (AudioEngine.Time - SkipBoundary > 8000 || currentScore.pass) &&
                       currentScore.totalScore > 10000 && currentScore.AllowSubmission &&
                       haxCheckCount > 0 && Ruleset.haxCheckCount > 0 && haxCheckCount == Ruleset.haxCheckCount && inputReceivedAtLeastOnce;
            }
        }

        private void InitializeAntiCheat()
        {
            flag = BadFlags.Clean;
        }

        bool flCheckedThisPlay;
        int flSkippedThisNote = GameBase.random.Next(10, 50);

        internal void AddFlashlightFlag()
        {
            flag |= BadFlags.FlashLightImageHack;
        }

        internal void AddSpinnerFlag()
        {
            flag |= BadFlags.SpinnerHack;
        }


        private void HaxCheckPass()
        {
            try
            {
                if (!GameBase.CheckUniqueId())
                    Player.flag |= BadFlags.ChecksumFailure;
            }
            catch { }
        }

        private void checkFlashlightHax()
        {
            if (InputManager.ReplayMode || Status != PlayerStatus.Playing || Paused || EventManager.BreakMode || KiaiActive)
                return;

            if (flCheckedThisPlay || IsSpinning || !(ModManager.CheckActive(currentScore.enabledMods, Mods.Flashlight) || ModManager.CheckActive(Mods.Flashlight)))
                return;

            if (Ruleset.ComboCounter.HitCombo < 200)
                return;

            //do real check in the 3rd~16th slider.
            flSkippedThisNote--;

            if (Ruleset.s_Flashlight != null && Ruleset.s_Flashlight.Alpha < 1)
                AddFlashlightFlag();

            if (flSkippedThisNote > 0)
                return;

            //calculate a point around the cursor position, make sure pixel at the point is Black in normal play
            Vector2 p = InputManager.CursorPosition;
            switch (Mode)
            {
                case PlayModes.Taiko:
                    //right side of visible area
                    p = new Vector2(550 + GameBase.WindowOffsetXScaled, 190) * GameBase.GamefieldRatio;
                    break;
                case PlayModes.CatchTheBeat:
                    //upper side of visible area
                    p = new Vector2(320 + GameBase.WindowOffsetXScaled, 60) * GameBase.GamefieldRatio;
                    break;
                case PlayModes.OsuMania:
                    //center bottom
                    p = new Vector2(320 + GameBase.WindowOffsetXScaled, 400) * GameBase.GamefieldRatio;
                    break;
                default:
                    {
                        p = InputManager.CursorPosition;
                        float left = p.X - 240 * GameBase.WindowRatio;
                        float right = p.X + 240 * GameBase.WindowRatio;
                        if (left < GameBase.WindowOffsetX) //that area always black
                            p.X = right;
                        else if (right > GameBase.WindowWidth - GameBase.WindowOffsetX)//black too.
                            p.X = left;
                        else
                            p.X = left;
                        //avoid score numbers and mod images area,range(110~height-10)
                        p.Y = Math.Min(Math.Max(110 * GameBase.WindowRatio, p.Y), GameBase.WindowHeight - 10);
                    }

                    break;
            }
            Color[] c = GameBase.PixelColorAt(p, 5);

            if (c != null)
            {
                int notBlackCount = 0;
                for (int i = 0; i < 5; i++)
                {
                    if (c[i].R > 10 || c[i].G > 10 || c[i].B > 10)
                        notBlackCount++;
                }

                if (notBlackCount >= 3)
                {
                    GameBase.RunGraphicsThread(delegate { currentScore.screenshot = GameBase.GetScreenShotStream(); });
                    AddFlashlightFlag();
                }
            }

            flCheckedThisPlay = true;
        }

        internal void UpdateChecksum()
        {
            scoreChecksum = CalcScoreChecksum();
            if (!string.IsNullOrEmpty(scoreChecksum))
            {
                Int64.TryParse(scoreChecksum, out scoreChecksumNumerical);
                scoreChecksumNumerical *= 2;
            }
        }

        bool allowSubmissionHaxCheck = true;

        int haxCheckCount; //number of times we did a check. compare with hitObjectManager's count of asking for a check to makes sure they are equal.

        bool haxFound; //set true on the first discovery so we don't spam notifications

        int lastIpcCheck = GameBase.Time;
        internal void HaxCheck(bool forceFail = false)
        {
            haxCheckCount++;

            string calc = CalcScoreChecksum();

            //check for other running osu! processes every 30s or so.
            if (GameBase.Time - lastIpcCheck > 30000)
            {
                //todo: check and reenable
                //GameBase.Scheduler.Add(() =>
                //{
                //    if (IPC.GetRunningOsu() != null)
                //        flag |= BadFlags.MultipleOsuClients;
                //}, 500);
                lastIpcCheck = GameBase.Time;
            }

            if (currentScore.enabledMods != ModManager.ModStatus)
                flag |= BadFlags.IncorrectModValue;

            if (haxCheckCount % 20 == 0 && GameBase.IsFormOpacityHacked())
                flag |= BadFlags.TransparentWindow;

            //Fight the memory editing hacks.
            if (forceFail || calc != scoreChecksum || Int64.Parse(calc) * 2 != scoreChecksumNumerical ||
                Ruleset.ComboCounter.HitCombo > currentScore.maxCombo)
            {
                if (Status == PlayerStatus.Busy)
                    return;

                flag |= BadFlags.ChecksumFailure;

                if (Status == PlayerStatus.ExitingSpectator || InputManager.ReplayMode || GameBase.TestMode)
                    return;

                currentScore.AllowSubmission = false;
                allowSubmissionHaxCheck = false;

                if (!haxFound)
                {
                    GameBase.Scheduler.AddDelayed(crash, GameBase.random.Next(1000, 6000));
                    GameBase.Scheduler.AddDelayed(delegate { throw new MethodAccessException(); }, GameBase.random.Next(1000, 6000));
                    GameBase.Scheduler.AddDelayed(delegate { Environment.Exit(-1); }, GameBase.random.Next(10000, 15000));
                    haxFound = true;
                }
            }
        }

        private void HaxCheckAudio()
        {
            if (!InputManager.ReplayMode && !GameBase.TestMode)
            {
                if (audioCheckTime == -1)
                {
                    if (AudioEngine.Time > 0)
                    {
                        audioCheckTime = AudioEngine.Time;
                        audioCheckTimeComp = GameBase.Time;
                    }
                }
                else if (AudioEngine.Time - audioCheckTime >= 1000)
                {
                    float rate = 1;
                    if (ModManager.CheckActive(Mods.DoubleTime))
                        rate = 1.5f;
                    else if (ModManager.CheckActive(Mods.HalfTime))
                        rate = 0.75f;

                    float diff = (AudioEngine.Time - audioCheckTime) * (1f / rate);

                    if (Math.Abs(diff - (GameBase.Time - audioCheckTimeComp)) > 60)
                    {
                        if (++audioCheckCount > 5)
                        {
                            NotificationManager.ShowMessage(
                                "There was an error during timing calculations.  If you continue to get this error, please update your AUDIO/SOUND CARD drivers!  Your score will not be submitted for this play.");
                            //BanchoClient.HandleException(new Exception("timing error"), "user is hacking?");
                            currentScore.AllowSubmission = false;
                            GameBase.Scheduler.AddDelayed(delegate { flag |= BadFlags.SpeedHackDetected; }, 200);
                            audioCheckCount = -500;
                        }
                    }
                    else
                        audioCheckCount = 0;
                    audioCheckTime = -1;
                }

                if ((flag & BadFlags.SpeedHackDetected) == 0)
                {
                    long dateTimeTime = DateTime.Now.Ticks / 10000;

                    if (datetimeCheckTimeInitial == -1)
                    {
                        if (AudioEngine.Time > 0)
                        {
                            datetimeCheckTimeInitial = dateTimeTime;
                            datetimeCheckTimeComp = GameBase.Time;
                        }
                    }
                    else
                    {
                        if (Math.Abs((dateTimeTime - datetimeCheckTimeInitial) - (GameBase.Time - datetimeCheckTimeComp)) > 2000)
                        {
                            dateTimeCheckCount++;
                            if (dateTimeCheckCount > 4)
                                flag |= BadFlags.SpeedHackDetected;

                            //reset comparison.
                            datetimeCheckTimeInitial = dateTimeTime;
                            datetimeCheckTimeComp = GameBase.Time;
                        }
                    }
                }
            }
        }

        internal static bool mouseMovementDiscrepancyInMenu = false;
        internal static float mouseMovementAngle = 0;
        internal static void HaxCheckMouse(Vector2 normalMovement, Vector2 rawMovement)
        {
            if (InputManager.ReplayMode || GameBase.TestMode || !GameBase.IsActive)
            {
                return;
            }

            float angle = 0;
            if ((normalMovement == Vector2.Zero && rawMovement != Vector2.Zero) ||
                (normalMovement != Vector2.Zero && rawMovement == Vector2.Zero))
            {
                angle = (float)Math.PI;
            }
            else if (normalMovement == Vector2.Zero && rawMovement == Vector2.Zero)
            {
                angle = 0;
            }
            else
            {
                angle = (float)(Math.Abs(Math.Atan2(rawMovement.Y, rawMovement.X)) - Math.Abs(Math.Atan2(normalMovement.Y, normalMovement.X)));
                angle = Math.Abs(angle);
            }

            float decay = (float)Math.Pow(0.99, GameBase.FrameRatio);
            mouseMovementAngle = decay * mouseMovementAngle + (1 - decay) * angle;

            if (mouseMovementAngle > Math.PI * 0.75)
            {
                if (Player.Playing && !mouseMovementDiscrepancyInMenu)
                {
                    flag |= BadFlags.RawMouseDiscrepancy;
                }
                else
                {
                    mouseMovementDiscrepancyInMenu = true;
                }
            }
        }

        internal void crash()
        {
            throw new ObjectDisposedException(@"WildCatTracer");
        }

        public void HaxCheck2()
        {
            if (!currentScore.AllowSubmission)
                return;

            if (Passed && GameBase.Processes == null)
            {
                GameBase.Processes = new System.Diagnostics.Process[0];
                GameBase.RunBackgroundThread(delegate { GameBase.Processes = System.Diagnostics.Process.GetProcesses(); });
            }

#if !DEBUGz
            if (lastFrameTime == 0 && (AudioEngine.Time < hitObjectManager.hitObjects[0].StartTime || AudioEngine.AudioState != AudioStates.Playing))
                return;

            if (lastFrameTime > 0 && (GameBase.Time - lastFrameTime) > (EventManager.BreakMode ? 8000 : 6000))
            {
                currentScore.AllowSubmission = false;
                NotificationManager.ShowMessageMassive("Score submission disabled due to system lag.", 3000);
            }

            lastFrameTime = GameBase.Time;
#endif
        }

    }

    [Flags]
    internal enum BadFlags
    {
        Clean = 0,
        SpeedHackDetected = 1 << 1,
        IncorrectModValue = 1 << 2,
        MultipleOsuClients = 1 << 3,
        ChecksumFailure = 1 << 4,
        FlashlightChecksumIncorrect = 1 << 5,
        OsuExecutableChecksum = 1 << 6, //server-side
        MissingProcessesInList = 1 << 7, //server-side
        FlashLightImageHack = 1 << 8,
        SpinnerHack = 1 << 9,
        TransparentWindow = 1 << 10,
        FastPress = 1 << 11,
        RawMouseDiscrepancy = 1 << 12,
        RawKeyboardDiscrepancy = 1 << 13,
    };
}
