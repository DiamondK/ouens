﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.GameModes.Select.Drawable;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu_common.Helpers;
using osu_common.Libraries.NetLib;
using osu.Configuration;
using osu.Online.Social;

namespace osu.GameModes.Select
{
    internal class OnlineSelection : DrawableGameComponent
    {
        private readonly List<OnlineBeatmap> Beatmaps = new List<OnlineBeatmap>();
        private readonly pCheckbox displayExisting;
        private readonly pDropdown displayModeDropdown;
        private readonly pDropdown displayOsuModeDropdown;
        private readonly pSprite loadingText;
        private readonly pText results;

        private readonly pScrollableArea resultsPane;

        private readonly pText searchInfo;
        private readonly pTextBoxOmniscient searchTextBox;
        private readonly SpriteManager spriteManager;
        private readonly SpriteManager spriteManagerFg;
        private string lastSearch = null;
        private bool newResults;
        private readonly bool wasPlayingAudio = false;

        private int nextSearchUpdate;
        private bool searching;
        private string tempIncoming;

        public OnlineSelection(Game game)
            : base(game)
        {
            if (AudioEngine.AudioState == AudioStates.Playing)
            {
                wasPlayingAudio = true;
                AudioEngine.TogglePause();
            }

            spriteManager = new SpriteManager(true);
            resultsPane = new pScrollableArea(new Rectangle(0, 82, GameBase.WindowWidthScaled - 4 - OsuDirect.WIDTH, 350), Vector2.Zero, true);
            spriteManagerFg = new SpriteManager(true);

            searchInfo = new pText("Type a search to begin.", 11, new Vector2(20, 40), 1, true, Color.YellowGreen);
            searchInfo.TextBold = true;
            spriteManager.Add(searchInfo);

            results = new pText("", 16, new Vector2(20, 80), 1, true, Color.White);
            spriteManager.Add(results);

            searchTextBox = new pTextBoxOmniscient(18, new Vector2(20, 20), 300, false, 1.1f);
            searchTextBox.OnChange += delegate { nextSearchUpdate = GameBase.Time + 600; };
            spriteManagerFg.Add(searchTextBox.SpriteCollection);

            loadingText = new pSprite(SkinManager.Load(@"searching", SkinSource.Osu), new Vector2(320, 240), 1f, true,
                                      Color.TransparentWhite);
            loadingText.Origin = Origins.Centre;
            loadingText.UpdateTextureAlignment();

            GameBase.spriteManager.Add(loadingText);

            BackButton back = new BackButton(back_OnClick);
            spriteManagerFg.Add(back.SpriteCollection);

            int xPos = 330;

            pButton pb = new pButton("Newest Maps", new Vector2(xPos, 20), new Vector2(100, 20), 1, Color.Orange,
                delegate
                {
                    lastSearch = "Newest";
                    searchTextBox.SetToDefault();
                    displayModeDropdown.SetSelected(0, false);
                });
            spriteManager.Add(pb.SpriteCollection);

            xPos += 105;

            pb = new pButton("Top Rated", new Vector2(xPos, 20), new Vector2(100, 20), 1, Color.OrangeRed,
                delegate
                {
                    lastSearch = "Top Rated";
                    searchTextBox.SetToDefault();
                    displayModeDropdown.SetSelected(0, false);
                });
            spriteManager.Add(pb.SpriteCollection);

            xPos += 105;

            pb = new pButton("Most Played", new Vector2(xPos, 20), new Vector2(100, 20), 1, Color.SkyBlue,
                delegate
                {
                    lastSearch = "Most Played";
                    searchTextBox.SetToDefault();
                    displayModeDropdown.SetSelected(0, false);
                });
            spriteManager.Add(pb.SpriteCollection);


            pText t = new pText("Display:", 11, new Vector2(5, 55), 1, true, Color.White);
            t.TextBold = true;
            spriteManager.Add(t);

            displayExisting = new pCheckbox("Show already downloaded maps", 0.88f, new Vector2(320, 54), 1, false);
            spriteManager.Add(displayExisting.SpriteCollection);

            displayExisting.OnCheckChanged += displayExisting_OnCheckChanged;

            displayModeDropdown = new pDropdown(spriteManagerFg, "", new Vector2(50, 54));
            displayModeDropdown.AddOption("All", 4);
            displayModeDropdown.AddOption("Ranked", 0);
            displayModeDropdown.AddOption("Ranked (Played)", 7);
            displayModeDropdown.AddOption("Qualified", 3);
            displayModeDropdown.AddOption("Pending/Help", 2);
            displayModeDropdown.AddOption("Graveyard", 5);

            displayModeDropdown.SetSelected(4, true);
            displayModeDropdown.OnSelect += pd_OnSelect;

            t = new pText("Mode:", 11, new Vector2(180, 55), 1, true, Color.White);
            t.TextBold = true;
            spriteManager.Add(t);

            displayOsuModeDropdown = new pDropdown(spriteManagerFg, "", new Vector2(220, 54), 85, 1);
            displayOsuModeDropdown.AddOption(@"All", -1);
            displayOsuModeDropdown.AddOption(@"osu!", 0);
            displayOsuModeDropdown.AddOption(@"Taiko", 1);
            displayOsuModeDropdown.AddOption(@"Catch the Beat", 2);
            displayOsuModeDropdown.AddOption(@"osu!mania", 3);

            displayOsuModeDropdown.SetSelected(-1, true);
            displayOsuModeDropdown.OnSelect += mode_OnSelect;


            KeyboardHandler.OnKeyRepeat += KeyboardHandler_OnKeyRepeat;

            GameBase.LoadComplete();
        }

        public override void Initialize()
        {
            displayModeDropdown.SetSelected(0, false);
            base.Initialize();
        }

        public string TempIncoming
        {
            get { return tempIncoming; }
        }

        private void displayExisting_OnCheckChanged(object sender, bool status)
        {
            ExecuteSearch(true);
        }

        private void pd_OnSelect(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(lastSearch))
                lastSearch = @"Newest";
            ExecuteSearch(true);
        }

        private void mode_OnSelect(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(lastSearch))
                lastSearch = @"Newest";
            ExecuteSearch(true);
        }

        protected override void Dispose(bool disposing)
        {
            Beatmaps.ForEach(b => b.Dispose());

            KeyboardHandler.OnKeyRepeat -= KeyboardHandler_OnKeyRepeat;
            spriteManager.Dispose();
            spriteManagerFg.Dispose();
            resultsPane.Dispose();
            searchTextBox.Dispose();

            GameBase.spriteManager.Remove(loadingText);

            //if (didDownloads)
            //    BeatmapManager.CheckAndProcess(false);

            base.Dispose(disposing);

            if (wasPlayingAudio)
            {
                AudioEngine.TogglePause();
            }
        }

        private bool KeyboardHandler_OnKeyRepeat(object sender, Keys k, bool first)
        {
            if (!first) return false;

            switch (k)
            {
                case Keys.Escape:
                    back_OnClick(null, null);
                    return true;
                case Keys.Enter:
                    nextSearchUpdate = GameBase.Time;
                    return true;
            }

            return false;
        }

        private void back_OnClick(object sender, EventArgs e)
        {
            AudioEngine.PlaySamplePositional(@"menuback");
            GameBase.ChangeMode(OsuModes.Menu);
        }

        private void ExecuteSearch(bool newSearch)
        {
            if (searching) return;

            if (newSearch)
            {
                pageOffset = 0;
                canLoadMore = false;
            }

            searching = true;
            StringNetRequest req =
                new StringNetRequest(string.Format(Urls.DIRECT_SEARCH, ConfigManager.sUsername, ConfigManager.sPassword, (int)displayModeDropdown.SelectedObject, GeneralHelper.UrlEncodeParam(lastSearch), (int)displayOsuModeDropdown.SelectedObject, pageOffset));
            req.onFinish += req_onFinish;
            NetManager.AddRequest(req);

            searchInfo.Text = newSearch ? "Searching..." : "Loading...";

            if (newSearch)
            {
                loadingText.FadeIn(200);
                Beatmaps.ForEach(b => b.Dim());
            }
        }

        private void req_onFinish(string data, Exception e)
        {
            lock (Beatmaps)
            {
                if (pageOffset == 0)
                {
                    Beatmaps.ForEach(b => b.Dispose());
                    Beatmaps.Clear();
                    resultsPane.SetScrollPosition(Vector2.Zero);
                }

                string[] lines = data.Split('\n');
                try
                {
                    int status = Int32.Parse(lines[0]);

                    tempIncoming = string.Empty;

                    if (status < 0)
                    {
                        searchInfo.Text = "Error: " + lines[1];
                    }
                    else
                    {
                        for (int i = 1; i < lines.Length; i++)
                        {
                            if (lines[i].Length == 0) continue;
                            OnlineBeatmap ob = new OnlineBeatmap(lines[i]);
                            ob.OnDownloadFinished += delegate
                                                         {
                                                             lock (Beatmaps)
                                                                 Beatmaps.Remove(ob);
                                                             newResults = true;
                                                             ob.Dispose();
                                                         };
                            if (displayExisting.Checked || !ob.exists)
                                Beatmaps.Add(ob);
                        }

                        canLoadMore = status > 100 && pageOffset != 39;

                        status = Math.Min(100, status);

                        int totalCount = (pageOffset * 100 + status);

                        if (canLoadMore)
                            searchInfo.Text = "Over " + totalCount + " results found.";
                        else
                            searchInfo.Text = totalCount + " result" + (totalCount != 1 ? "s" : "") + " found.";
                    }
                }
                catch
                {
                }
            }

            newResults = true;
        }

        public override void Draw()
        {
            spriteManager.Draw();
            resultsPane.Draw();
            spriteManagerFg.Draw();
            base.Draw();
        }

        string searchTextLocal;
        private int pageOffset;
        private bool canLoadMore;

        public override void Update()
        {
            searchTextBox.MaintainFocus = !ChatEngine.IsVisible;

            bool searchReady;
            if(lastSearch == null)
            {
                lastSearch = @"Newest";
                searching = false;

                ExecuteSearch(true);
                searchReady = false;
            }
            else
                searchReady = nextSearchUpdate >= 0 && nextSearchUpdate <= GameBase.Time;

            if (searchReady)
            {
                bool newData = searchTextBox.Text != lastSearch;
                if (searchTextBox.Text.Length == 0)
                {
                    if (newData && lastSearch != @"Newest" && lastSearch != @"Top Rated" && lastSearch != @"Most Played")
                    {
                        lastSearch = @"Newest";
                        ExecuteSearch(true);
                    }
                }
                else if (!searching && newData)
                {
                    lastSearch = searchTextBox.Text;
                    ExecuteSearch(true);

                    nextSearchUpdate = -1;
                }
            }
            resultsPane.Update();

            if (resultsPane.ScrollPosition.Y > resultsPane.ContentDimensions.Y - resultsPane.DisplayRectangle.Height && canLoadMore && !searching)
            {
                pageOffset++;
                ExecuteSearch(false);
            }

            if (newResults)
            {
                loadingText.FadeOut(200);
                int i = 0;
                lock (Beatmaps)
                {
                    foreach (OnlineBeatmap b in Beatmaps)
                    {
                        if (b.DrawAt(new Vector2(10, i)))
                            resultsPane.SpriteManager.Add(b.SpriteCollection);
                        i += 30;
                    }
                }
                resultsPane.SetContentDimensions(new Vector2(640, i + 30));

                searching = false;
                newResults = false;
            }
            base.Update();
        }
    }
}