﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Play;
using osu.GameplayElements.Scoring;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu_common;
using Microsoft.Xna.Framework.Input;
using osu.Input.Handlers;
using osu.Input;
using osu_common.Helpers;
using osu.GameModes.Online;
using osu.Audio;
using osu.GameplayElements.Beatmaps;

namespace osu.GameModes.Select
{
    internal class ModButton
    {
        internal List<pSprite> Sprites = new List<pSprite>();
        private pSprite glow;

        public Mods State;

        internal List<Mods> AvailableStates;

        internal EventHandler StateChanged;

        public ModButton(Vector2 position, params Mods[] states)
        {
            AvailableStates = new List<Mods>(states);

            foreach (Mods m in states)
            {
                string spriteName = @"selection-mod-" + m.ToString().ToLower();

                pSprite s = new pSprite(SkinManager.Load(spriteName), Fields.TopLeft, Origins.Centre, Clocks.Game, position, 0.94F, true, Color.TransparentWhite);
                s.TagNumeric = (int)m;

                s.OnHover += delegate { AudioEngine.Click(null, @"click-short"); };
                s.OnClick += delegate
                {
                    Cycle(MouseManager.MouseRight == ButtonState.Pressed);
                };
                s.HandleInput = true;

                //check for mode-specific tooltip.
                if (Player.Mode != PlayModes.Osu)
                {
                    try
                    {
                        s.ToolTip = LocalisationManager.GetString((OsuString)Enum.Parse(typeof(OsuString), "ModSelection_Mod_" + m.ToString() + "_" + Player.Mode.ToString()));
                    }
                    catch { }
                }

                //fallback to default tooltip.
                if (s.ToolTip == null)
                {
                    try
                    {
                        s.ToolTip = LocalisationManager.GetString((OsuString)Enum.Parse(typeof(OsuString), "ModSelection_Mod_" + m.ToString()));
                    }
                    catch { }
                }

                Sprites.Add(s);
            }

            glow = new pSprite(SkinManager.Load(@"lighting", SkinSource.Osu), Origins.Centre, position, 0.99f, true, Color.SkyBlue);
            glow.Alpha = 0;
            glow.Additive = true;

            Sprites.Add(glow);
        }

        internal void SetStatus(bool status, Mods specificStatus = Mods.None)
        {
            Sprites.ForEach(s =>
            {
                s.FadeOut(100);
                s.ScaleTo(1, 400, EasingTypes.OutElastic);
                s.RotateTo(0, 400, EasingTypes.OutElastic);
            });

            if (!status)
            {
                State = Mods.None;
                Sprites[0].FadeIn(100);
            }
            else
            {
                pSprite match = Sprites.Find(s => s.TagNumeric == (int)specificStatus);
                if (match == null || specificStatus == Mods.None) match = Sprites[0];

                State = (Mods)match.TagNumeric;

                match.FadeIn(100);
                match.ScaleTo(1.2f, 400, EasingTypes.OutElastic);
                match.RotateTo(0.1f, 400, EasingTypes.OutElastic);
            }

            glow.FadeTo(State == Mods.None ? 0 : 0.2f, 100);

            if (StateChanged != null)
                StateChanged(this, null);
        }

        internal void Cycle(bool reverse = false)
        {
            if (State == Mods.None)
            {
                AudioEngine.Click(null, @"check-on");
                SetStatus(true, AvailableStates[reverse ? AvailableStates.Count - 1 : 0]);
            }
            else if (AvailableStates[reverse ? 0 : AvailableStates.Count - 1] == State)
            {
                AudioEngine.Click(null, @"check-off");
                SetStatus(false);
            }
            else
            {
                AudioEngine.Click(null, @"check-on");
                SetStatus(true, AvailableStates[AvailableStates.FindIndex(s => s == State) + (reverse ? -1 : 1)]);
            }
        }
    }


    internal class ModSelection : pDialog
    {
        private const int spacing = 60;
        float verticalInitial;

        private pText modMultiplier;

        private readonly ModSelectionType selectionType;

        Dictionary<Mods, ModButton> Buttons = new Dictionary<Mods, ModButton>();

        public ModSelection(ModSelectionType ms)
            : base(LocalisationManager.GetString(OsuString.ModSelection_Title), true)
        {
            selectionType = ms;

            spriteManager.Add(modMultiplier = new pText("", 30, new Vector2(0, currentVerticalSpace - 20), Vector2.Zero, 0.94F, true, Color.White, true) { Field = Fields.TopCentre });
            modMultiplier.Origin = Origins.Centre;

            currentVerticalSpace += modMultiplier.MeasureText().Y + 35;
            verticalInitial = currentVerticalSpace;

            spriteManager.Add(new pText(LocalisationManager.GetString(OsuString.ModSelection_Reduction), 24, new Vector2(20, currentVerticalSpace - 13), Vector2.Zero, 0.94F, true, Color.LimeGreen, true));

            if ((selectionType & (ModSelectionType.ServerSide | ModSelectionType.ClientSideFreeMod)) > 0) add(new ModButton(pos(0, 0), Mods.Easy));
            if ((selectionType & (ModSelectionType.ServerSide | ModSelectionType.ClientSideFreeMod)) > 0) add(new ModButton(pos(0, 1), Mods.NoFail));
            if ((selectionType & (ModSelectionType.ServerSide)) > 0) add(new ModButton(pos(0, 2), Mods.HalfTime));

            currentVerticalSpace += spacing;

            spriteManager.Add(new pText(LocalisationManager.GetString(OsuString.ModSelection_Increase), 24, new Vector2(20, currentVerticalSpace - 13), Vector2.Zero, 0.94F, true, Color.OrangeRed, true));


            if ((selectionType & (ModSelectionType.ServerSide | ModSelectionType.ClientSideFreeMod)) > 0) add(new ModButton(pos(1, 0), Mods.HardRock));

            if ((selectionType & ModSelectionType.Special) > 0)
                add(new ModButton(pos(1, 1), Mods.SuddenDeath, Mods.Perfect));
            else if ((selectionType & (ModSelectionType.ServerSide | ModSelectionType.ClientSideFreeMod)) > 0)
                add(new ModButton(pos(1, 1), Mods.SuddenDeath));


            if ((selectionType & (ModSelectionType.ServerSide)) > 0) add(new ModButton(pos(1, 2), Mods.DoubleTime, Mods.Nightcore));

            if ((selectionType & (ModSelectionType.ServerSide | ModSelectionType.ClientSideFreeMod)) > 0)
            {
                if (Player.Mode == PlayModes.OsuMania)
                    add(new ModButton(pos(1, 3), Mods.FadeIn, Mods.Hidden));
                else
                    add(new ModButton(pos(1, 3), Mods.Hidden));
                add(new ModButton(pos(1, 4), Mods.Flashlight));
            }

            currentVerticalSpace += spacing;

            spriteManager.Add(new pText(LocalisationManager.GetString(OsuString.ModSelection_Special), 24, new Vector2(20, currentVerticalSpace - 13), Vector2.Zero, 0.94F, true, Color.White, true));

            int index = 0;

            if ((selectionType & (ModSelectionType.ServerSide | ModSelectionType.ClientSideFreeMod)) > 0)
            {
                if (Player.Mode == PlayModes.OsuMania)
                {
                    add(new ModButton(pos(2, index++), Mods.Key4, Mods.Key5, Mods.Key6, Mods.Key7, Mods.Key8, Mods.Key9, Mods.Key1, Mods.Key2, Mods.Key3));
                    add(new ModButton(pos(2, index++), Mods.KeyCoop));
                    add(new ModButton(pos(2, index++), Mods.Random));
                }
                else
                    add(new ModButton(pos(2, index++), Mods.Relax));

                if (Player.Mode == PlayModes.Osu)
                {
                    add(new ModButton(pos(2, index++), Mods.Relax2));
#if !Public
                    if ((selectionType & ModSelectionType.Special) > 0)
                        add(new ModButton(pos(2, index++), Mods.Target));
#endif
                    add(new ModButton(pos(2, index++), Mods.SpunOut));
                }
            }

            if ((selectionType & ModSelectionType.Special) > 0)
                add(new ModButton(pos(2, index++), Mods.Autoplay, Mods.Cinema));

            currentVerticalSpace += spacing;

            AddOption(LocalisationManager.GetString(OsuString.ModSelection_Reset), Color.OrangeRed, clearAll, false);
            AddOption(LocalisationManager.GetString(OsuString.General_Close), Color.Gray, closeWindow);
        }

        private void add(ModButton modButton)
        {
            modButton.StateChanged += stateChanged;
            foreach (Mods m in modButton.AvailableStates)
                Buttons.Add(m, modButton);
            spriteManager.Add(modButton.Sprites);
        }

        internal Vector2 pos(int row, int col)
        {
            return new Vector2(240 + col * 85, verticalInitial + row * 60);
        }

        internal bool toggle(Mods mod, bool? status = null)
        {
            ModButton button = null;

            if (!Buttons.TryGetValue(mod, out button))
                return false;

            if (!status.HasValue)
            {
                if (button.State == mod || button.AvailableStates[0] == mod)
                    button.Cycle();
                else
                    button.SetStatus(true, mod);
            }
            else
                button.SetStatus(status.Value, mod);

            return true;
        }

        bool ignoreStateChanges;
        private void stateChanged(object sender, EventArgs e)
        {
            if (!IsDisplayed || ignoreStateChanges) return;

            ModButton b = sender as ModButton;

            if (b == null) return;

            Mods oldMods = ModManager.ModStatus;

            foreach (Mods s in b.AvailableStates)
                ModManager.ModStatus &= ~s;

            ModManager.ModStatus |= b.State;

            ModManager.CheckForCurrentPlayMode(b.State);

            ignoreStateChanges = true;
            updateMods();
            ignoreStateChanges = false;

            if (BeatmapDifficultyCalculator.MaskRelevantMods(oldMods) != BeatmapDifficultyCalculator.MaskRelevantMods(ModManager.ModStatus))
            {
                ModManager.TriggerModsChanged();
            }
        }

        internal override void Display()
        {
            base.Display();
        }

        internal override void DisplayAfter()
        {
            ignoreStateChanges = true;
            updateMods();
            ignoreStateChanges = false;

            base.DisplayAfter();
        }

        private void clearAll(object sender, EventArgs e)
        {
            bool needsUpdate = BeatmapDifficultyCalculator.MaskRelevantMods(ModManager.ModStatus) > 0;

            ModManager.Reset();
            updateMods();

            if(needsUpdate)
            {
                ModManager.TriggerModsChanged();
            }
        }

        private void closeWindow(object sender, EventArgs e)
        {
        }

        private void updateMods()
        {
            ModManager.CheckForCurrentPlayMode();

            for (Mods m = (Mods)1; m < Mods.LastMod; m = (Mods)((int)m * 2))
            {
                bool active = ModManager.CheckActive(m);
                if (!active)
                {
                    //check we aren't obliterating an already-set mod on this button.
                    ModButton b = null;
                    if (Buttons.TryGetValue(m, out b) && b.State != Mods.None && !active && ModManager.CheckActive(b.State))
                        continue;
                }

                toggle(m, active);
            }

            UpdateMultiplier();
        }

        private void UpdateMultiplier()
        {
            double mult = Player.Mode == PlayModes.OsuMania ? ModManager.ScoreMultiplier(ModManager.ModStatus & ~(Mods.ScoreIncreaseMods)) : ModManager.ScoreMultiplier();
            modMultiplier.Text = string.Format("Score Multiplier: {0:0.00}x", mult);
            if (mult > 1)
                modMultiplier.FadeColour(Color.GreenYellow, 400);
            else if (mult < 1)
                modMultiplier.FadeColour(Color.OrangeRed, 400);
            else
                modMultiplier.FadeColour(Color.White, 400);
        }

        internal static Mods ModFromBinding(Keys k)
        {
            switch (BindingManager.For(k, BindingTarget.ModSelect))
            {
                case Bindings.Easy:
                    return Mods.Easy;
                case Bindings.NoFail:
                    return Mods.NoFail;
                case Bindings.HalfTime:
                    return Mods.HalfTime;
                case Bindings.HardRock:
                    return Mods.HardRock;
                case Bindings.SuddenDeath:
                    if (KeyboardHandler.ShiftPressed)
                        return Mods.Perfect;
                    else
                        return Mods.SuddenDeath;
                case Bindings.DoubleTime:
                    if (KeyboardHandler.ShiftPressed)
                        return Mods.Nightcore;
                    else
                        return Mods.DoubleTime;
                case Bindings.Hidden:
                    if (!KeyboardHandler.ShiftPressed && Player.Mode == PlayModes.OsuMania)
                        return Mods.FadeIn;
                    else
                        return Mods.Hidden;
                case Bindings.Flashlight:
                    return Mods.Flashlight;
                case Bindings.Relax:
                    if (Player.Mode == PlayModes.OsuMania)
                        return Mods.Key4;
                    else
                        return Mods.Relax;
                case Bindings.Autopilot:
                    if (Player.Mode == PlayModes.OsuMania)
                        return Mods.Random;
                    else
                        return Mods.Relax2;
                case Bindings.SpunOut:
                    return Mods.SpunOut;
                case Bindings.Auto:
                    if (KeyboardHandler.ShiftPressed)
                        return Mods.Cinema;
                    else
                        return Mods.Autoplay;
            }

            return Mods.None;
        }

        internal override bool HandleKey(Keys k)
        {
            Mods modFromBinding = ModFromBinding(k);
            if(modFromBinding != Mods.None)
            {
                toggle(modFromBinding);
                return true;
            }

            switch (k)
            {
                case Keys.F1:
                    Close();
                    return true;
            }

            if (Player.Mode == PlayModes.OsuMania)
            {
                switch (k)
                {
                    case Keys.D0:
                        toggle(Mods.Key4, false);
                        return true;
                    case Keys.D4:
                    case Keys.D5:
                    case Keys.D6:
                    case Keys.D7:
                    case Keys.D8:
                        toggle((Mods)((int)Mods.Key4 << (int)(k - Keys.D4)), true);
                        return true;
                }
            }

            return base.HandleKey(k);
        }

        internal override void Close(bool isCancel = false)
        {
            base.Close(isCancel);
        }
    }

    [Flags]
    internal enum ModSelectionType
    {
        ClientSide = 1,
        ServerSide = 2,
        Special = 4,
        ClientSideFreeMod = 8,
        AllMultiplayer = ClientSide | ClientSideFreeMod | ServerSide,
        All = ClientSide | ClientSideFreeMod | ServerSide | Special,
    }
}