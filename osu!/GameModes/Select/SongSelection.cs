﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Notifications;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Helpers;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu.Properties;
using osu_common;
using osu_common.Bancho.Requests;
using osu_common.Libraries.NetLib;
using osu.Properties;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using System.Threading;
using osu.Online.P2P;
using osu_common.Helpers;
using osu.GameModes.Play.Rulesets.Mania;
using osu_common.Bancho.Objects;
using System.Text.RegularExpressions;
using Amib.Threading;
using osu.Graphics.Primitives;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Fx;
using osu.GameModes.Menus;
using System.Diagnostics;
using osu.Online.Social;


namespace osu.GameModes.Select
{
    internal partial class SongSelection : DrawableGameComponent
    {
        private bool handleInput;
        private bool HandleInput
        {
            get
            {
                return handleInput;
            }

            set
            {
                handleInput = value;
                beatmapTreeManager.HandleInput = value;
            }
        }

        private bool Starting;
        private readonly pSprite buttonMods;
        private readonly pSprite buttonOptions;
        private readonly pSprite buttonRandom;
        private readonly pText details;
        private readonly pText details2;
        private readonly pText details3;
        private readonly pText details4;
        private readonly pText details5;
        private readonly pDropdown dropdownGroup;
        private readonly pDropdown dropdownSort;
        private readonly Beatmap initialBeatmap;
        private readonly pText modsList;
        private readonly pText speedLabel;
        private readonly bool MultiMode;
        private readonly bool PlayMode;
        private readonly pCheckbox rankedOnlyCheckbox;
        private readonly pSprite rankForum;
        private readonly List<pSprite> ratingStars = new List<pSprite>();
        private pSprite s_ModeLogo;
        private pSprite s_modeSprite;
        private readonly pSprite s_Osu;
        private readonly pSprite s_Osu1;
        private readonly pSprite s_Osu2;
        private readonly pSprite scrollbarBackground;
        private readonly pSprite scrollbarForeground;
        private readonly pSprite searchBackground;
        private readonly pText searchInfo;
        private pTextBoxOmniscient searchTextBox;

        private static string lastSearchText;
        private static string lastOpenGroupName;

        private readonly pText searchText;
        private readonly SpriteManager spriteManagerHigh;
        private readonly SpriteManager spriteManagerLow;
        private readonly SpriteManager spriteManagerBackground;
        private readonly SpriteManager spriteManagerDraggedItems;

        private readonly pScrollableArea scoreList;
        private pTexture star2;
        private readonly pSprite statusIcon;
        private readonly pTexture statusIconApproved;
        private readonly pTexture statusIconRanked;
        private readonly pTexture statusIconUnknown;
        private readonly pTabCollection tabs;
        private readonly pSprite whiteOverlay;
        private int activeGroupNumber = -1;

        private BeatmapTreeItem draggedItem;

        /// <summary>
        /// True when audio can't be loaded for current song.  Used primarily to stop file system calls in UpdateMusic()
        /// </summary>
        private bool audioError;
        internal pSprite beatmapBackground;
        private pSprite checkButton;
        private int dialogCooldown;
        private bool dialogVisible;
        private bool Exiting;


        private string newBeatmapName;
        private int nextSearchUpdate = -1;
        private bool OnlineScoreDisplay { get { return (BanchoClient.Connected || (!string.IsNullOrEmpty(ConfigManager.sUsername) && BanchoClient.FailCount == 0)) && ConfigManager.sRankType != RankingType.Local; } }
        private BeatmapTreeGroup openGroup;
        private bool popupMenuActive;


        private bool searchMode;
        private string searchString;
        private bool searchTextChanged;
        private TreeGroupMode tabDisplayedMode = TreeGroupMode.None;

        private SmartThreadPool difficultyCalculationThreadPool;
        private int nextBeatmapToScheduleIndex = 0;

        float displayedRating;

        List<pSprite> spritesPersonalBest = new List<pSprite>();
        private pSprite topBackground;
        private pSprite buttonMode;
        private pSpriteMenu modeMenu;
        public static bool AllowModeChange = true;
        internal static Bindable<PlayModes> LastSelectMode;
        private pList<Score> Scores;
        private pDropdown rankingType;
        private pSprite loadingSpinner;

        private bool forceNextSelection = true;

        private BeatmapTreeManager beatmapTreeManager;


        internal SongSelection(Game game)
            : base(game)
        {
            BeatmapTreeItem.LoadSkinElements();

            STPStartInfo stpStartInfo = new STPStartInfo();
            stpStartInfo.MaxWorkerThreads = 1;
            stpStartInfo.AreThreadsBackground = true;
            stpStartInfo.IdleTimeout = 1 * 1000; // 1 second is enough until idle timeout. If there are still beatmaps to process new work will be there near instantly.
            stpStartInfo.ThreadPriority = ThreadPriority.BelowNormal;
            difficultyCalculationThreadPool = new SmartThreadPool(stpStartInfo);

            initialBeatmap = BeatmapManager.Current;

            PlayMode = (GameBase.Mode == OsuModes.SelectPlay);
            MultiMode = (GameBase.Mode == OsuModes.SelectMulti);

            if (!MultiMode && PlayMode) Player.Mode = LastSelectMode;

            ScoreManager.InitializeScoreManager();
            CollectionManager.InitializeManager();

            //foreach (Beatmap b in BeatmapManager.BeatmapAvailable)
            if (BeatmapManager.Current != null)
                BeatmapManager.Current.Scores.Clear();

            Player.currentScore = null;

            lock (StreamingManager.LockReplayScore)
            {
                InputManager.ReplayMode = false;
                InputManager.ReplayStreaming = false;
            }

            CheckHasPlayable();

            if ((BeatmapManager.Current == null || string.IsNullOrEmpty(BeatmapManager.Current.RelativeContainingFolder) || !BeatmapManager.Current.BeatmapPresent) && (GameBase.Mode == OsuModes.SelectPlay || GameBase.Mode == OsuModes.SelectMulti || GameBase.Mode == OsuModes.SelectEdit))
                MusicControl.ChooseRandomSong(true, true);

            KeyboardHandler.OnKeyRepeat += KeyboardHandler_OnKeyRepeat;
            KeyboardHandler.OnKeyReleased += KeyboardHandler_OnKeyReleased;
            JoystickHandler.OnButtonPressed += JoystickHandler_OnButtonPressed;

            BanchoClient.OnConnect += BanchoClient_OnConnect;

            ModManager.OnModsChanged += ModManager_OnModsChanged;

            beatmapTreeManager = new BeatmapTreeManager(game);

            beatmapTreeManager.OnSelectedBeatmap += beatmapTreeManager_OnSelectedBeatmap;
            beatmapTreeManager.OnSelectedGroup += beatmapTreeManager_OnSelectedGroup;
            beatmapTreeManager.OnRightClicked += beatmapTreeManager_OnRightClicked;
            beatmapTreeManager.OnActivated += beatmapTreeManager_OnActivated;
            beatmapTreeManager.OnExpanded += beatmapTreeManager_OnItemExpanded;
            beatmapTreeManager.OnDragged += beatmapTreeManager_OnDragged;
            beatmapTreeManager.OnStartRandom += beatmapTreeManager_OnStartRandom;
            beatmapTreeManager.OnEndRandom += beatmapTreeManager_OnEndRandom;


            if (Player.SpriteManagerLoading != null)
            {
                spriteManagerBackground = Player.SpriteManagerLoading;
            }
            else
            {
                spriteManagerBackground = new SpriteManager(true);
            }

            spriteManagerLow = new SpriteManager(true);
            spriteManagerDraggedItems = new SpriteManager(true);

            scoreList = new pScrollableArea(new Rectangle(-5, 92, 290, (int)(33.4f * 8)), Vector2.Zero, true)
            {
                ScrollDraggerOnLeft = true
            };

            spriteManagerHigh = new SpriteManager(true);

            details = new pText(@" ", 18, new Vector2(21, -3), Vector2.Zero, 0.79F, true, Color.White, false);
            details.MeasureText();
            spriteManagerHigh.Add(details);

            details2 = new pText(string.Empty, 12, new Vector2(23, 12), Vector2.Zero, 0.79F, true, Color.White, false);
            spriteManagerHigh.Add(details2);
            details3 = new pText(string.Empty, 12, new Vector2(1, 24), Vector2.Zero, 0.79F, true, Color.White, false);
            spriteManagerHigh.Add(details3);
            details4 = new pText(string.Empty, 12, new Vector2(1, 36), Vector2.Zero, 0.79F, true, Color.White, false);
            spriteManagerHigh.Add(details4);
            details5 = new pText(string.Empty, 8, new Vector2(1, 48), Vector2.Zero, 1, true, Color.White, false);
            spriteManagerHigh.Add(details5);

            speedLabel = new pText(string.Empty, 20, new Vector2(5, -1), 1, true, new Color(255, 255, 255, 128))
            {
                Field = Fields.TopRight,
                Origin = Origins.TopRight,
                Tag = 0d
            };
            spriteManagerHigh.Add(speedLabel);

            statusIconRanked = SkinManager.Load(@"selection-ranked", SkinSource.Osu);
            statusIconUnknown = SkinManager.Load(@"selection-question", SkinSource.Osu);
            statusIconApproved = SkinManager.Load(@"selection-approved", SkinSource.Osu);

            statusIcon = new pSprite(statusIconRanked, Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(12, 12), 0.78f, true, Color.TransparentWhite);
            spriteManagerHigh.Add(statusIcon);

            whiteOverlay = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, Vector2.Zero, 1, true, Color.TransparentWhite);
            whiteOverlay.ScaleToWindowRatio = false;
            whiteOverlay.Additive = true;
            whiteOverlay.VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight);
            spriteManagerHigh.Add(whiteOverlay);


            scrollbarBackground = new pSprite(GameBase.WhitePixel, Fields.TopRight, Origins.TopLeft, Clocks.Game, new Vector2(3, 73), 0.88f, true, new Color(0, 0, 0, 128));
            scrollbarBackground.Scale = 1.6f;
            scrollbarBackground.VectorScale = new Vector2(3, 330);

            scrollbarForeground = new pSprite(GameBase.WhitePixel, Fields.TopRight, Origins.TopLeft, Clocks.Game, new Vector2(3, 73), 0.89f, true, new Color(255, 255, 255, 255));
            scrollbarForeground.Scale = 1.6f;
            scrollbarForeground.VectorScale = new Vector2(3, 20);

            spriteManagerHigh.Add(scrollbarBackground);
            spriteManagerHigh.Add(scrollbarForeground);

            if (PlayMode || MultiMode)
            {
                bool newPositioning = SkinManager.UseNewLayout || SkinManager.Load(@"selection-mods").Source == SkinSource.Osu;

                Fields bottomField = newPositioning ? Fields.BottomLeft : Fields.TopLeft;
                Origins bottomOrigin = newPositioning ? Origins.BottomLeft : Origins.TopLeft;
                float bottomVerticalPos = newPositioning ? 0 : 426;
                float bottomHorizontalPos = GameBase.Widescreen ? 140 : 120;

                if (AllowModeChange)
                {
                    buttonMode = new pSprite(SkinManager.Load(@"selection-mode"), bottomField, bottomOrigin, Clocks.Game, new Vector2(bottomHorizontalPos, bottomVerticalPos), 0.95F, true, Color.White);
                    spriteManagerHigh.Add(buttonMode);

                    buttonMode = new pSprite(SkinManager.Load(@"selection-mode-over"), bottomField, bottomOrigin, Clocks.Game, new Vector2(bottomHorizontalPos, bottomVerticalPos), 0.96F, true, Color.White);
                    buttonMode.Alpha = 0.01f;
                    buttonMode.HoverEffect = new Transformation(TransformationType.Fade, 0.01F, 1, 0, 100);
                    buttonMode.HandleInput = true;
                    buttonMode.OnHover += delegate { AudioEngine.Click(null, @"click-short"); };
                    buttonMode.OnClick += delegate { AudioEngine.Click(null, @"select-expand"); modeMenu.Toggle(); };
                    spriteManagerHigh.Add(buttonMode);

                    modeMenu = new pSpriteMenu(spriteManagerHigh);

                    float box_size = 80;

                    Vector2 pos = new Vector2(bottomHorizontalPos, 424 - box_size);

                    for (int m = 0; m < 4; m++)
                    {
                        PlayModes playMode = (PlayModes)m;
                        pTexture modeTexture;

                        switch (playMode)
                        {
                            case PlayModes.CatchTheBeat:
                                modeTexture = SkinManager.Load(@"mode-fruits-med");
                                break;
                            case PlayModes.Taiko:
                                modeTexture = SkinManager.Load(@"mode-taiko-med");
                                break;
                            case PlayModes.OsuMania:
                                modeTexture = SkinManager.Load(@"mode-mania-med");
                                break;
                            default:
                                modeTexture = SkinManager.Load(@"mode-osu-med");
                                break;
                        }

                        pSprite modeButton = new pSprite(modeTexture, Fields.TopLeft, Origins.Centre, Clocks.Game, pos + new Vector2(box_size / 2f, box_size / 2f), 0.991f, true, Color.TransparentWhite);
                        spriteManagerHigh.Add(modeButton);

                        pText modeText = new pText(OsuCommon.PlayModeString(playMode), 24, pos + new Vector2(box_size + 10, box_size / 2f), 0.992f, true, Color.TransparentWhite)
                        {
                            Origin = Origins.CentreLeft,
                            TextBold = true,
                            TextShadow = true
                        };

                        spriteManagerHigh.Add(modeText);

                        pSpriteMenuItem item = new pSpriteMenuItem(modeMenu, spriteManagerHigh, new Vector2((box_size * 3.5f) * 1.6f, box_size * 1.6f), pos, false);
                        item.Sprites.Add(modeButton);
                        item.Sprites.Add(modeText);

                        item.Background.OnHover += delegate
                        {
                            AudioEngine.Click(null, @"click-short");
                            if (modeButton.Texture.Source == SkinSource.Osu)
                                modeButton.ScaleTo(1.1f, 1000, EasingTypes.OutElastic);
                            modeText.MoveTo(modeText.InitialPosition + new Vector2(10, 0), 300, EasingTypes.Out);
                        };

                        item.Background.OnHoverLost += delegate
                        {
                            if (modeButton.Texture.Source == SkinSource.Osu)
                                modeButton.ScaleTo(1f, 1000, EasingTypes.OutElastic);
                            modeText.MoveTo(modeText.InitialPosition, 300, EasingTypes.Out);
                        };

                        item.OnClick += delegate
                        {
                            AudioEngine.Click(null, @"click-short-confirm");
                            ChangeMode(playMode);
                        };

                        modeMenu.Items.Add(item);

                        pos.Y -= box_size;
                    }

                    bottomHorizontalPos += 57.6f;
                }

                if (!MultiMode)
                {
                    buttonMods = new pSprite(SkinManager.Load(@"selection-mods"), bottomField, bottomOrigin, Clocks.Game, new Vector2(bottomHorizontalPos, bottomVerticalPos), 0.95F, true, Color.White);
                    spriteManagerHigh.Add(buttonMods);

                    buttonMods = new pSprite(SkinManager.Load(@"selection-mods-over"), bottomField, bottomOrigin, Clocks.Game, new Vector2(bottomHorizontalPos, bottomVerticalPos), 0.96F, true, Color.White);
                    buttonMods.Alpha = 0.01f;
                    buttonMods.HoverEffect = new Transformation(TransformationType.Fade, 0.01F, 1, 0, 100);
                    buttonMods.OnHover += delegate { AudioEngine.Click(null, @"click-short"); };
                    buttonMods.HandleInput = true;
                    buttonMods.OnClick += buttonMods_OnClick;
                    spriteManagerHigh.Add(buttonMods);

                    bottomHorizontalPos += 48;
                }

                if (MultiMode || PlayMode)
                {
                    buttonRandom = new pSprite(SkinManager.Load(@"selection-random"), bottomField, bottomOrigin, Clocks.Game, new Vector2(bottomHorizontalPos, bottomVerticalPos), 0.95F, true, Color.White);
                    spriteManagerHigh.Add(buttonRandom);

                    buttonRandom = new pSprite(SkinManager.Load(@"selection-random-over"), bottomField, bottomOrigin, Clocks.Game, new Vector2(bottomHorizontalPos, bottomVerticalPos), 0.96F, true, Color.White);
                    buttonRandom.Alpha = 0.01f;
                    buttonRandom.HoverEffect = new Transformation(TransformationType.Fade, 0.01F, 1, 0, 100);
                    buttonRandom.OnHover += delegate { AudioEngine.Click(null, @"click-short"); };
                    buttonRandom.HandleInput = true;
                    buttonRandom.OnClick += buttonRandom_OnClick;
                    spriteManagerHigh.Add(buttonRandom);

                    bottomHorizontalPos += 48;
                }

                buttonOptions = new pSprite(SkinManager.Load(@"selection-options"), bottomField, bottomOrigin, Clocks.Game, new Vector2(bottomHorizontalPos, bottomVerticalPos), 0.95F, true, Color.White);
#if !ARCADE
                spriteManagerHigh.Add(buttonOptions);
#endif
                buttonOptions = new pSprite(SkinManager.Load(@"selection-options-over"), bottomField, bottomOrigin, Clocks.Game, new Vector2(bottomHorizontalPos, bottomVerticalPos), 0.96F, true, Color.White);
                buttonOptions.Alpha = 0.01f;
                buttonOptions.OnHover += delegate { AudioEngine.Click(null, @"click-short"); };
                buttonOptions.HoverEffect = new Transformation(TransformationType.Fade, 0.01F, 1, 0, 100);
                buttonOptions.HandleInput = true;
                buttonOptions.OnClick += buttonOptions_OnClick;

#if !ARCADE
                spriteManagerHigh.Add(buttonOptions);
                bottomHorizontalPos += 48;
#endif

                bottomHorizontalPos += 48;

                GameBase.User.DrawAt(new Vector2(bottomHorizontalPos, 429), false, 0);
                spriteManagerHigh.Add(GameBase.User.Sprites);
            }
            else
            {
                //edit mode
                ModManager.ModStatus = Mods.None;

                pSprite p = new pSprite(SkinManager.Load(@"selection-drop", SkinSource.Osu), new Vector2(5, 110), 0.5f, true, Color.White);
                spriteManagerHigh.Add(p);
            }

            if (PlayMode)
            {
                Color col = (BanchoClient.Permission & Permissions.Supporter) > 0 ? Color.Black : new Color(30, 30, 30);

                rankingType = new pDropdown(spriteManagerHigh, string.Empty, new Vector2(5, 73.5f), 190, 1);
                rankingType.SpriteMainBox.BorderColour = new Color(31, 184, 255, 180);
                rankingType.SpriteMainBox.TextSize = 14;

                rankingType.AddOption(LocalisationManager.GetString(OsuString.SongSelection_Rank_Local), RankingType.Local);
                rankingType.AddOption(LocalisationManager.GetString(OsuString.SongSelection_Rank_Country), RankingType.Country, col);
                rankingType.AddOption(LocalisationManager.GetString(OsuString.SongSelection_Rank_Top), RankingType.Top);
                rankingType.AddOption(LocalisationManager.GetString(OsuString.SongSelection_Rank_Mod), RankingType.SelectedMod, 0, col);
                rankingType.AddOption(LocalisationManager.GetString(OsuString.SongSelection_Rank_Friends), RankingType.Friends, col);

                rankingType.SetSelected((object)ConfigManager.sRankType.Value, true);

                rankingType.OnSelect += delegate
                {
                    ConfigManager.sRankType.Value = (RankingType)rankingType.SelectedObject;
                    switch (ConfigManager.sRankType.Value)
                    {
                        case RankingType.SelectedMod:
                        case RankingType.Friends:
                        case RankingType.Country:
                            if ((BanchoClient.Permission & Permissions.Supporter) == 0)
                            {
                                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.SongSelection_NeedSupporter), 5000);
                                rankingType.SetSelected(ConfigManager.sRankType.Value = RankingType.Top, false);
                                return;
                            }
                            break;
                        case RankingType.Top:
                            if (!BanchoClient.Connected)
                            {
                                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.SongSelection_NoNetworkConnection), 5000);
                                rankingType.SetSelected(RankingType.Local, true);
                                return;
                            }
                            break;
                    }

                    RefreshScores();
                };
            }

            rankForum = new pSprite(SkinManager.Load(@"rank-forum", SkinSource.Osu), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(214, 81), 0.95F, true, Color.TransparentWhite);
            rankForum.OnClick += rankForum_OnClick;
            rankForum.HandleInput = true;
            rankForum.ToolTip = LocalisationManager.GetString(OsuString.SongSelection_WebAccess);
            rankForum.ClickEffect = new Transformation(TransformationType.Scale, 1.1F, 1, 0, 50);
            spriteManagerHigh.Add(rankForum);

            s_Osu = new pSprite(SkinManager.Load(@"menu-osu", SkinSource.Osu), Fields.TopRight, Origins.Centre, Clocks.Game, new Vector2(40, 450), 0.9F, true, Color.White);
            s_Osu.Scale = 0.4F;
            spriteManagerHigh.Add(s_Osu);

            s_Osu1 = new pSprite(SkinManager.Load(@"menu-osu", SkinSource.Osu), Fields.TopRight, Origins.Centre, Clocks.Game, new Vector2(40, 450), 0.91F, true, Color.White);
            s_Osu1.Scale = 0.4F;
            spriteManagerHigh.Add(s_Osu1);

#if ARCADE
            s_Osu2 = new pSprite(SkinManager.Load(@"dragtobrowse"), Fields.TopRight, Origins.Centre, Clocks.Game, new Vector2(460, 250), 0.5f, true, Color.White);
            spriteManagerHigh.Add(s_Osu2);
#endif

            s_Osu2 = new pSpriteCircular(SkinManager.Load(@"menu-osu", SkinSource.Osu), new Vector2(40, 450), 1, true, Color.White, 250)
            {
                Field = Fields.TopRight,
                Origin = Origins.Centre
            };
            s_Osu2.Alpha = 0.01f;
            s_Osu2.OnClick += start_OnClick;
            s_Osu2.HandleInput = true;
            s_Osu2.Scale = 0.55F;
            s_Osu2.HoverPriority = -100;
            s_Osu2.HoverEffect = new Transformation(TransformationType.Fade, 0.01F, 1, 0, 100);

#if ARCADE
            pSprite l = new pSprite(SkinManager.Load(@"clicktostart"), Fields.TopRight, Origins.Centre, Clocks.Game, new Vector2(140, 455), 1, true, Color.White);

            l.CurrentScale = 0.8f;
            l.FadeInFromZero(300);
            l.IsLoopable = true;

            Transformation loop = new Transformation(TransformationType.MovementX, l.CurrentPosition.X, l.CurrentPosition.X + 25, GameBase.Time + 300, GameBase.Time + 600, EasingTypes.In);
            loop.Loop = true;
            loop.LoopDelay = 300;
            loop.NumericalTag = 2001;
            l.Transformations.Add(loop);


            loop = new Transformation(TransformationType.MovementX, l.CurrentPosition.X + 25, l.CurrentPosition.X, GameBase.Time + 600, GameBase.Time + 900, EasingTypes.Out);
            loop.Loop = true;
            loop.LoopDelay = 300;
            loop.NumericalTag = 2002;
            l.Transformations.Add(loop);

            spriteManagerHigh.Add(l);
#endif

            spriteManagerHigh.Add(s_Osu2);

            if (!MultiMode)
            {
                BackButton back = new BackButton(back_OnClick);
                spriteManagerHigh.Add(back.SpriteCollection);
            }

            tabs = new pTabCollection(spriteManagerHigh, 6, new Vector2(GameBase.WindowWidthScaled - 55, 49), 0.87f, true, Color.Crimson, 0, true);
            tabs.TextSize = 0.8f;

            pText t = new pText(LocalisationManager.GetString(OsuString.SongSelection_Sort), 22, new Vector2(GameBase.WindowWidthScaled - 130, 40), 0.86f, true, new Color(174, 210, 139));
            t.Origin = Origins.BottomRight;
            spriteManagerHigh.Add(t);

            float sortLabelWidth = t.MeasureText().X;

            dropdownSort = new pDropdown(spriteManagerHigh, @"Choice2", new Vector2(GameBase.WindowWidthScaled - 130, 18));
            dropdownSort.HighlightColour = new Color(174, 210, 139);
            dropdownSort.SpriteMainBox.BorderColour = ColourHelper.Darken(dropdownSort.HighlightColour, 0.2f);
            dropdownSort.SpriteMainBox.BorderWidth = 1;

            t = new pText(LocalisationManager.GetString(OsuString.SongSelection_Group), 22, new Vector2(GameBase.WindowWidthScaled - 140 - 118 - sortLabelWidth, 40), 0.86f, true, new Color(146, 195, 230));
            t.Origin = Origins.BottomRight;
            spriteManagerHigh.Add(t);

            dropdownGroup = new pDropdown(spriteManagerHigh, @"Choice1", new Vector2(GameBase.WindowWidthScaled - 140 - 118 - sortLabelWidth, 18));
            dropdownGroup.HighlightColour = new Color(146, 195, 230);
            dropdownGroup.SpriteMainBox.BorderColour = ColourHelper.Darken(dropdownGroup.HighlightColour, 0.2f);
            dropdownGroup.SpriteMainBox.BorderWidth = 1;

            dropdownGroup.OnSelect += delegate(object sender, EventArgs args)
            {
                Tabs_OnTabChanged(sender, null);
            };

            dropdownSort.OnSelect += delegate(object sender, EventArgs args)
            {
                TreeSortMode newSort = (TreeSortMode)sender;
                if (newSort != beatmapTreeManager.CurrentSortMode.Value)
                {
                    beatmapTreeManager.CurrentSortMode.Value = newSort;
                    beatmapTreeManager.SortTreeAndReSelect();
                }
            };

            dropdownSort.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByArtist), TreeSortMode.Artist);
            dropdownSort.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByBPM), TreeSortMode.BPM);
            dropdownSort.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByCreator), TreeSortMode.Creator);
            dropdownSort.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByDateAdded), TreeSortMode.Date);
            dropdownSort.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByDifficulty), TreeSortMode.Difficulty);
            dropdownSort.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByLength), TreeSortMode.Length);
            dropdownSort.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByRankAchieved), TreeSortMode.Rank);
            dropdownSort.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByTitle), TreeSortMode.Title);

            //Need to remain in front of the tab collection.
            //I dunno if this is too hacky, hey.
            dropdownSort.SpriteCollection.ForEach(s => s.HoverPriority = -999);

            dropdownSort.SetSelected(beatmapTreeManager.CurrentSortMode.Value, true);

            tabs.Add(LocalisationManager.GetString(OsuString.SongSelection_NoGrouping), TreeGroupMode.Show_All);

            if (PlayMode || MultiMode)
            {
                tabs.Add(LocalisationManager.GetString(OsuString.SongSelection_ByDifficulty), TreeGroupMode.Difficulty);
                tabs.Add(LocalisationManager.GetString(OsuString.SongSelection_ByArtist), TreeGroupMode.Artist);
            }
            else
            {
                tabs.Add(LocalisationManager.GetString(OsuString.SongSelection_MyMaps), TreeGroupMode.My_Maps);
                tabs.Add(LocalisationManager.GetString(OsuString.SongSelection_ByCreator), TreeGroupMode.Creator);
            }

            tabs.Add(LocalisationManager.GetString(OsuString.SongSelection_RecentlyPlayed), TreeGroupMode.Last_Played);
            if (GameBase.WindowWidthScaled > 720)
                tabs.Add(LocalisationManager.GetString(OsuString.SongSelection_Collections), TreeGroupMode.Collection);

            tabs.OnTabChanged += Tabs_OnTabChanged;

            searchText = new pText(LocalisationManager.GetString(OsuString.SongSelection_Search), 13, new Vector2(220, 54), Vector2.Zero, 0.81F, true, Color.White, false);
            searchText.Field = Fields.TopRight;
            searchText.TextBold = true;
            searchText.TextColour = Color.GreenYellow;
            spriteManagerHigh.Add(searchText);

            searchTextBox = new pTextBoxOmniscient(13, new Vector2(searchText.CurrentPositionActual.X + searchText.MeasureText().X, 54), 174);
            searchTextBox.HandleLeftRightArrows = false;
            searchTextBox.MaintainFocus = !ChatEngine.IsVisible;
            searchTextBox.DefaultText = LocalisationManager.GetString(OsuString.SongSelection_TypeToBegin);
            searchTextBox.Box.TextBold = true;
            searchTextBox.OnChange += new pTextBox.OnCommitHandler(searchTextBox_OnChange);
            spriteManagerHigh.Add(searchTextBox.SpriteCollection);

            searchInfo = new pText(string.Empty, 11, new Vector2(220, 66), Vector2.Zero, 0.81F, true, Color.White, false);
            searchInfo.TextBold = true;
            searchInfo.Field = Fields.TopRight;
            spriteManagerHigh.Add(searchInfo);

            searchBackground = new pSprite(SkinManager.Load(@"menu-button-background", SkinSource.Osu), Fields.TopRight, Origins.Centre, Clocks.Game, new Vector2(14, 46), 0.69F, true, new Color(0, 0, 0, 102));
            spriteManagerHigh.Add(searchBackground);

            topBackground = new pSprite(SkinManager.Load(@"songselect-top", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(0, 0), 0.7F, true, Color.White);
            spriteManagerHigh.Add(topBackground);

            if (GameBase.WindowWidth > 1366)
            {
                topBackground = new pSprite(SkinManager.Load(@"songselect-top", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(1365f / GameBase.WindowRatio, 0), 0.7F, true, Color.White);
                topBackground.DrawLeft = 1365;
                topBackground.DrawWidth = 1;
                topBackground.VectorScale = new Vector2(GameBase.WindowWidth - 1365, 1);

                spriteManagerHigh.Add(topBackground);
            }

            //todo: optimise this out
            pSprite detailsBack = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(0, -10), 0F, true, Color.Black);
            detailsBack.VectorScale = new Vector2((GameBase.WindowWidthScaled + 1) * 1.6f, 97);
            detailsBack.HandleInput = true;
            spriteManagerHigh.Add(detailsBack);

            detailsBack = new pSprite(SkinManager.Load(@"songselect-bottom", SkinSource.Osu), Fields.TopLeft, Origins.BottomLeft, Clocks.Game, new Vector2(0, 480), 0.7F, true, Color.White);
            detailsBack.VectorScale = new Vector2((GameBase.WindowWidth / GameBase.WindowRatio) / (detailsBack.DrawWidth * 0.625f), 1);
            detailsBack.HandleInput = true;
            spriteManagerHigh.Add(detailsBack);

            loadingSpinner = pStatusDialog.CreateSpinner(Color.White);
            loadingSpinner.Field = Fields.TopLeft;
            loadingSpinner.Position = new Vector2(118, 200);
            loadingSpinner.FadeOut(0);
            spriteManagerHigh.Add(loadingSpinner);

            bool newFiles = BeatmapManager.lastAddedMap != null;

            if (newFiles)
            {
                BeatmapManager.Current = BeatmapManager.lastAddedMap;
                BeatmapManager.lastAddedMap = null;
            }

            // Select the recommended difficulty if we have new files only. Otherwise we want to keep what we previously had selected.
            LoadTree(beatmapTreeManager.CurrentGroupMode.Value, lastOpenGroupName != null, lastOpenGroupName, newFiles);
            lastOpenGroupName = null;

            if (!string.IsNullOrEmpty(lastSearchText))
            {
                searchTextBox.Text = lastSearchText;
                UpdateSearch(true);
                lastSearchText = null;
            }

            modsList = new pText(string.Empty, 30, new Vector2(60, 397), 0.3F, true, new Color(255, 255, 255, 128));
            modsList.TextBorder = true;
            spriteManagerHigh.Add(modsList);

            LoadModeSpecific();
        }

        void LoadModeSpecific()
        {
            if (s_modeSprite != null)
            {
                spriteManagerHigh.Remove(s_modeSprite);
                s_modeSprite = null;
            }

            if (s_ModeLogo != null)
            {
                s_ModeLogo.AlwaysDraw = false;
                s_ModeLogo.FadeOut(300);
                s_ModeLogo = null;
            }

            if (PlayMode || MultiMode)
            {
                switch (Player.Mode)
                {
                    case PlayModes.CatchTheBeat:
                        s_ModeLogo = new pSprite(SkinManager.Load(@"mode-fruits"), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(GameBase.WindowWidthScaled / 2, 240), 0.12F, true, new Color(255, 255, 255, 70));
                        break;
                    case PlayModes.Taiko:
                        s_ModeLogo = new pSprite(SkinManager.Load(@"mode-taiko"), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(GameBase.WindowWidthScaled / 2, 240), 0.12F, true, new Color(255, 255, 255, 70));
                        break;
                    case PlayModes.OsuMania:
                        s_ModeLogo = new pSprite(SkinManager.Load(@"mode-mania"), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(GameBase.WindowWidthScaled / 2, 240), 0.12F, true, new Color(255, 255, 255, 70));
                        break;
                    default:
                        s_ModeLogo = new pSprite(SkinManager.Load(@"mode-osu"), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(GameBase.WindowWidthScaled / 2, 240), 0.12F, true, new Color(255, 255, 255, 70));
                        break;
                }

                s_ModeLogo.Additive = true;
                spriteManagerBackground.Add(s_ModeLogo);

                if (AllowModeChange)
                {
                    pTexture modeTexture;

                    switch (Player.Mode)
                    {
                        case PlayModes.CatchTheBeat:
                            modeTexture = SkinManager.Load(@"mode-fruits-small");
                            break;
                        case PlayModes.Taiko:
                            modeTexture = SkinManager.Load(@"mode-taiko-small");
                            break;
                        case PlayModes.OsuMania:
                            modeTexture = SkinManager.Load(@"mode-mania-small");
                            break;
                        default:
                            modeTexture = SkinManager.Load(@"mode-osu-small");
                            break;
                    }

                    float bottomHorizontalPos = GameBase.Widescreen ? 140 : 120;
                    s_modeSprite = new pSprite(modeTexture, Fields.BottomLeft, Origins.Centre, Clocks.Game, new Vector2(bottomHorizontalPos + 57.6f / 2, 35), 0.97F, true, Color.White);
                    s_modeSprite.Additive = true;
                    spriteManagerHigh.Add(s_modeSprite);

                    s_modeSprite.Scale = 0;
                    s_modeSprite.ScaleTo(1, 1000, EasingTypes.OutElastic);
                }
            }

            RefreshBeatmapDifficulties();

            // TODO: Enable the removal of a single options to prevent completely re-populating this just for mania.
            dropdownGroup.RemoveAllOptions();

            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_NoGrouping), TreeGroupMode.Show_All);

            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByArtist), TreeGroupMode.Artist);
            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByBPM), TreeGroupMode.BPM);
            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByCreator), TreeGroupMode.Creator);
            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByDateAdded), TreeGroupMode.Date);
            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByDifficulty), TreeGroupMode.Difficulty);
            if (Player.Mode == PlayModes.OsuMania)
                dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByKeyCount), TreeGroupMode.Mania_Keys);

            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByLength), TreeGroupMode.Length);
            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByMode), TreeGroupMode.Mode);
            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByRankAchieved), TreeGroupMode.Rank);
            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ByTitle), TreeGroupMode.Title);

            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_Collections), TreeGroupMode.Collection);
            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_Favourites), TreeGroupMode.Online_Favourites);

            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_MyMaps), TreeGroupMode.My_Maps);
            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_RankedStatus), TreeGroupMode.RankedStatus);
            dropdownGroup.AddOption(LocalisationManager.GetString(OsuString.SongSelection_RecentlyPlayed), TreeGroupMode.Last_Played);

            //Need to remain in front of the tab collection.
            //I dunno if this is too hacky, hey.
            dropdownGroup.SpriteCollection.ForEach(s => s.HoverPriority = -999);

            if (Player.Mode != PlayModes.OsuMania && tabDisplayedMode == TreeGroupMode.Mania_Keys)
            {
                LoadTree(TreeGroupMode.Show_All);
            }
            else
            {
                SelectProperTabAndDropdownGroup();
            }

            ModManager.CheckForCurrentPlayMode();
        }

        void RefreshBeatmapDifficulties()
        {
            // Restart difficulty calculations so that the current mode with current mods is computed
            difficultyCalculationThreadPool.Cancel();
            nextBeatmapToScheduleIndex = 0;

            // Star difficulties and details need to change, too.
            UpdateDetails();
            beatmapTreeManager.UpdateStarDifficulty();

            // This triggers prioritized beatmap difficulty calculations.
            beatmapTreeManager.UpdateStates();
        }

        void ChangeMode(PlayModes newMode)
        {
            if (Player.Mode == newMode)
            {
                return;
            }

            Player.Mode = newMode;

            BanchoClient.UpdateStatus(true);

            // Refresh scores if the map is convertible
            if (BeatmapManager.Current.PlayMode == PlayModes.Osu)
            {
                RefreshScores(true);
            }

            LoadModeSpecific();
        }

        void ModManager_OnModsChanged(object sender, Mods item)
        {
            RefreshBeatmapDifficulties();
        }


        internal void Select(Beatmap beatmap)
        {
            if (beatmap != null)
                beatmapTreeManager.Select(beatmap);
        }

        void beatmapTreeManager_OnSelectedBeatmap(object sender, BeatmapTreeItem item)
        {
            audioError = false; //reset state.

            Beatmap oldMap = BeatmapManager.Current;

            if (BeatmapManager.Current != null)
                BeatmapManager.Current.CancelRequests();

            if (item != null)
                BeatmapManager.Current = item.Beatmap;

            if (BeatmapManager.Current == null)
                return;

            bool isNewMap = oldMap != BeatmapManager.Current;

            if (isNewMap || forceNextSelection || beatmapTreeManager.IsRandomActive)
            {
                UpdateDetails();
            }

            bool continuePlaying = !isNewMap;

            if (!beatmapTreeManager.IsRandomActive && (isNewMap || forceNextSelection))
            {
                bool sameSet = oldMap != null && BeatmapManager.Current.ContainingFolder == oldMap.ContainingFolder;
                if (sameSet && !forceNextSelection)
                {
                    continuePlaying = true;
                    AudioEngine.PlaySample(@"select-difficulty");
                }
                else
                    AudioEngine.PlaySample(@"select-expand");

                UpdateStatusIcon();

                if (!BeatmapManager.Current.HeadersLoaded && BeatmapManager.Current.BeatmapPresent)
                    if (!BeatmapManager.Current.ProcessHeaders())
                    {
                        //File was not found.
                        BeatmapImport.ForceRun(false);
                        return;
                    }

                try
                {
                    //Ensure we are playing from preview point when first entering the screen from play mode.
                    BeginAudioTransition(BeatmapManager.Current, continuePlaying);
                }
                catch
                {
                    NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.SongSelection_AudioError));
                    audioError = true;
                }

                forceNextSelection = false;

                details.Transformations.Clear();
                details.Alpha = 0;
                details2.Transformations.Clear();
                details2.Alpha = 0;
                details3.Transformations.Clear();
                details3.Alpha = 0;
                details4.Transformations.Clear();
                details4.Alpha = 0;
                details5.Transformations.Clear();
                details5.Alpha = 0;

                details.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, GameBase.Time, GameBase.Time + 400));
                details2.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, GameBase.Time + 100, GameBase.Time + 500));
                details3.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, GameBase.Time + 200, GameBase.Time + 600));
                details4.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, GameBase.Time + 200, GameBase.Time + 700));
                details5.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, GameBase.Time + 300, GameBase.Time + 800));

                ShowScores();

                whiteOverlay.Transformations.Clear();
                whiteOverlay.Transformations.Add(new Transformation(TransformationType.Fade, 0.1f, 0, GameBase.Time, GameBase.Time + 500));

                updateManiaSpeed();
            }

            GameBase.TransitionManager.UpdateBackground();
        }

        void beatmapTreeManager_OnSelectedGroup(object sender, BeatmapTreeItem item)
        {
            AudioEngine.PlaySample(@"select-expand");
        }

        void beatmapTreeManager_OnRightClicked(object sender, BeatmapTreeItem item)
        {
            if (item is BeatmapTreeGroup)
            {
                if (beatmapTreeManager.CurrentGroupMode.Value == TreeGroupMode.Collection)
                {
                    TogglePopupCollection(item);
                }
            }
            else
            {
                TogglePopup();
            }
        }

        void beatmapTreeManager_OnActivated(object sender, BeatmapTreeItem item)
        {
            if (KeyboardHandler.ControlPressed && KeyboardHandler.ShiftPressed && !MultiMode && PlayMode)
                ModManager.ModStatus |= Mods.Cinema;

            if (KeyboardHandler.ControlPressed && !MultiMode && PlayMode)
                ModManager.ModStatus |= Mods.Autoplay;

            Start();
        }

        void beatmapTreeManager_OnItemExpanded(object sender, BeatmapTreeItem item)
        {
            if (item.Beatmap != null && item.Beatmap.StarDisplay < 0)
            {
                Beatmap selectedBeatmap = beatmapTreeManager.SelectedBeatmap;

                bool highPriority = selectedBeatmap != null && selectedBeatmap.RelativeContainingFolder == item.Beatmap.RelativeContainingFolder;
                ScheduleBeatmapDifficultyCalculation(item.Beatmap, highPriority ? WorkItemPriority.Normal : WorkItemPriority.BelowNormal);
            }
        }

        void beatmapTreeManager_OnDragged(object sender, BeatmapTreeItem item)
        {
            return; // Don't do anything for now

            beatmapTreeManager.HandleInput = false;
            beatmapTreeManager.XOffset = 500;

            draggedItem = item;
        }


        private void chooseBestSortMode(TreeGroupMode mode)
        {
            TreeSortMode sortMode = beatmapTreeManager.CurrentSortMode.Value;

            switch (mode)
            {
                case TreeGroupMode.Artist:
                    sortMode = TreeSortMode.Artist;
                    break;
                case TreeGroupMode.Creator:
                    sortMode = TreeSortMode.Creator;
                    break;
                case TreeGroupMode.Date:
                    sortMode = TreeSortMode.Date;
                    break;
                case TreeGroupMode.Difficulty:
                    sortMode = TreeSortMode.Difficulty;
                    break;
                case TreeGroupMode.Length:
                    sortMode = TreeSortMode.Length;
                    break;
                case TreeGroupMode.Title:
                    sortMode = TreeSortMode.Title;
                    break;
            }

            if (sortMode != beatmapTreeManager.CurrentSortMode.Value)
            {
                beatmapTreeManager.CurrentSortMode.Value = sortMode;
                dropdownSort.SetSelected(sortMode, true);
            }
        }

        private void ScheduleNextBeatmapDifficultyCalculation()
        {
            while (nextBeatmapToScheduleIndex < BeatmapManager.Beatmaps.Count)
            {
                Beatmap b = BeatmapManager.Beatmaps[nextBeatmapToScheduleIndex];

                ++nextBeatmapToScheduleIndex;

                if (b.StarDisplay < 0)
                {
                    ScheduleBeatmapDifficultyCalculation(b, WorkItemPriority.Lowest);
                    return;
                }
            }
        }

        internal void ScheduleAllRemainingBeatmapDifficultyCalculations()
        {
            while (nextBeatmapToScheduleIndex < BeatmapManager.Beatmaps.Count)
            {
                ScheduleNextBeatmapDifficultyCalculation();
            }
        }

        internal void ScheduleBeatmapDifficultyCalculation(Beatmap beatmap, WorkItemPriority priority)
        {
            beatmap.ComputeDifficultiesTomStarsBackground(difficultyCalculationThreadPool, priority, delegate
            {
                if (beatmap == BeatmapManager.Current)
                {
                    UpdateDetails();
                }

                beatmapTreeManager.UpdateStarDifficulty(beatmap);
            });
        }

        void JoystickHandler_OnButtonPressed(object sender, List<Keys> keys)
        {
            Keys key = keys[0];
            switch (key)
            {
                case JoyKey.Left:
                    KeyboardHandler_OnKeyRepeat(null, Keys.Escape, true);
                    break;
                case JoyKey.Right:
                    KeyboardHandler_OnKeyRepeat(null, Keys.Enter, true);
                    break;
            }
        }

        void searchTextBox_OnChange(pTextBox sender, bool newText)
        {
            searchTextChanged |= newText;
        }

        private void BanchoClient_OnConnect()
        {
            GameBase.Scheduler.Add(delegate { RefreshScores(false); });
        }

        private void Tabs_OnTabChanged(object sender, EventArgs e)
        {
            if (sender == null)
            {
                return;
            }

            TreeGroupMode group = (TreeGroupMode)sender;

            if (group == tabDisplayedMode)
            {
                return;
            }

            chooseBestSortMode(group);
            LoadTree(group);
        }

        private static void rankForum_OnClick(object sender, EventArgs e)
        {
            pDialog dialog = new pDialog(LocalisationManager.GetString(OsuString.SongSelection_OpenOptions), true);
            dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ListingScores), Color.OrangeRed, delegate { BeatmapManager.Current.LoadUrlListing(); });
            dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_Topic), Color.YellowGreen, delegate { BeatmapManager.Current.LoadUrlTopic(); });
            dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_Reply), Color.YellowGreen, delegate { BeatmapManager.Current.LoadUrlReply(); });
            dialog.AddOption(LocalisationManager.GetString(OsuString.General_Cancel), Color.Gray, null);
            GameBase.ShowDialog(dialog);
        }

        private void CheckHasPlayable()
        {
            if (BeatmapManager.Beatmaps.Count == 0 || (PlayMode && BeatmapManager.Beatmaps.Count - BeatmapManager.TotalAudioOnly == 0))
            {
                GameBase.LoadComplete();
                Exiting = true;

                if ((BanchoClient.Permission & Permissions.Supporter) > 0)
                    GameBase.ChangeMode(OsuModes.OnlineSelection);
                else
                {
                    NotificationManager.MessageBox(LocalisationManager.GetString(OsuString.SongSelection_NoBeatmaps), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    if ((BanchoClient.Permission & Permissions.Supporter) > 0)
                    {
                        GameBase.ChangeMode(OsuModes.OnlineSelection);
                    }
                    else
                    {
                        GameBase.ProcessStart(@General.WEB_ROOT + @"/?p=beatmaplist");
                        GameBase.ChangeMode(OsuModes.Menu);
                    }
                }
                return;
            }
        }

        private void TogglePopup()
        {
            if (BeatmapManager.Current == null) return;

            Color c1 = Color.YellowGreen;
            Color c2 = new Color(208, 125, 216);
            pDialog dialog = new pDialog(string.Format(LocalisationManager.GetString(OsuString.SongSelection_ThisBeatmap), BeatmapManager.Current.SortTitle), true);
            dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_Collection), c1, (obj, ev) =>
            {
                CollectionDialog d = new CollectionDialog(delegate { if (beatmapTreeManager.CurrentGroupMode.Value == TreeGroupMode.Collection) LoadTree(TreeGroupMode.Collection, true); });
                GameBase.ShowDialog(d);
            });

            dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_Delete), Color.OrangeRed, optionDeleteBeatmap);
            if (PlayMode)
            {
                dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_RemoveFromUnplayed), c2, optionRemoveUnplayed);
                dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_ClearLocalScores), c2, optionResetScores);
                dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_Edit), Color.OrangeRed, optionEdit);
            }
#if NDS
            dialog.AddOption(@"Export for NDS", new Color(0xe4, 0x56, 0x78), optionExportDS);
#endif
            dialog.AddOption(LocalisationManager.GetString(OsuString.General_Cancel), Color.Gray, null);
            GameBase.ShowDialog(dialog);
        }

        private void TogglePopupCollection(BeatmapTreeItem item)
        {
            string key = item.Name;
            if (!CollectionManager.Collections.ContainsKey(key))
                return;
            pDialog dialog = new pDialog(string.Format(LocalisationManager.GetString(OsuString.SongSelection_ThisCollection), key), true);
            dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_Delete), Color.OrangeRed, (obj, ev) =>
            {
                CollectionManager.Remove(key);
                LoadTree(tabDisplayedMode);
            });
            dialog.AddOption(LocalisationManager.GetString(OsuString.General_Cancel), Color.Gray, null);
            GameBase.ShowDialog(dialog);
        }

        private void optionEdit(object sender, EventArgs e)
        {
            GameBase.ChangeMode(OsuModes.Edit);
        }

        private void optionResetScores(object sender, EventArgs e)
        {
            ScoreManager.Remove(BeatmapManager.Current.BeatmapChecksum);

            NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.SongSelection_ClearedScores), BeatmapManager.Current.SortTitle), Color.LimeGreen, 3000);

            ShowScores();
        }

        private void optionRemoveUnplayed(object sender, EventArgs e)
        {
            BeatmapManager.Current.NewFile = false;
            NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.SongSelection_RemovedFromUnplayedSongs), BeatmapManager.Current.SortTitle), Color.SkyBlue, 3000);
            LoadTree(beatmapTreeManager.CurrentGroupMode.Value);
        }

        private void optionDeleteBeatmap(object sender, EventArgs e)
        {
            pDialog dialog = new pDialog(string.Format(LocalisationManager.GetString(OsuString.SongSelection_DeleteFromDisk), BeatmapManager.Current.DisplayTitle), true);

            bool hasManyDifficulties = BeatmapManager.Beatmaps.FindAll(delegate(Beatmap b) { return b.RelativeContainingFolder == BeatmapManager.Current.RelativeContainingFolder; }).Count > 1;

            if (hasManyDifficulties && !BeatmapManager.Current.InOszContainer)
            {
                dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_DeleteAllDifficulties), Color.OrangeRed, DeleteBeatmap);
                dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_DeleteOnlyThisDifficulty), Color.OrangeRed, DeleteDifficulty);
            }
            else
                dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_DeleteThisBeatmap), Color.OrangeRed, DeleteBeatmap);
            dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_NoCancel), Color.Gray, null);
            GameBase.ShowDialog(dialog);
        }

        private void DeleteDifficulty(object sender, EventArgs e)
        {
            BeatmapManager.DeleteBeatmap(BeatmapManager.Current, false);

            List<BeatmapTreeItem> disposed = beatmapTreeManager.MenuItems.FindAll(bti => bti.Beatmap == BeatmapManager.Current);

            if (disposed.Count == 0)
                return;

            DeletePostProcess(disposed);
        }

        private void DeleteBeatmap(object sender, EventArgs e)
        {
            //string folder = BeatmapManager.Current.ContainingFolder;
            BeatmapManager.DeleteBeatmap(BeatmapManager.Current, true);
            NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.SongSelection_DeletedFromDisk), BeatmapManager.Current.DisplayTitle), Color.Red, 3000);

            List<BeatmapTreeItem> disposed = beatmapTreeManager.MenuItems.FindAll(bti => bti.Beatmap != null && bti.Beatmap.ContainingFolder == BeatmapManager.Current.ContainingFolder);

            DeletePostProcess(disposed);
        }

        private void DeletePostProcess(List<BeatmapTreeItem> disposed)
        {
            CheckHasPlayable();

            if (Exiting)
                return;

            if (BeatmapManager.Current.RelativeContainingFolder.IndexOf(Path.DirectorySeparatorChar) >= 0)
            {
                //Beatmap is stored in a subfolder.
                //We can't handle removing it from the beatmap tree without further processing...
                //So for this rare case let's just rerun the whole.
                BeatmapManager.ChangedFolders.Add(BeatmapManager.Current.ContainingFolder);
                GameBase.ChangeModeInstant(OsuModes.BeatmapImport);
                return;
            }

            disposed.ForEach(ti =>
            {
                BeatmapManager.Root.Beatmaps.Remove(ti.Beatmap);
            });

            // Switch away grouping mode in case we don't have any maps in the current mode. Note, that we can only be here
            // if there still are beatmaps due to the CheckHasPlayable() call above.
            if (beatmapTreeManager.MenuItems.Count == disposed.Count)
            {
                forceNextSelection = true;
                MusicControl.ChooseRandomSong(true, true);
                LoadTree(TreeGroupMode.None, false, null, true);
            }
            else
            {
                beatmapTreeManager.Remove(disposed);
                UpdateSearch(true);
            }

            // We might be skipping some maps now, let's just restart the queue. (No harm done.)
            RefreshBeatmapDifficulties();
        }

        private void buttonOptions_OnClick(object sender, EventArgs e)
        {
            AudioEngine.Click(null, @"click-short-confirm");

            TogglePopup();
        }


        private void buttonMods_OnClick(object sender, EventArgs e)
        {
            if (!PlayMode) return;

            AudioEngine.Click(null, @"click-short-confirm");

            beatmapTreeManager.EndRandom();

            Mods modsBeforeSelection = ModManager.ModStatus;

            ModSelection ms = new ModSelection(ModSelectionType.All);
            ms.Closed += delegate
            {
                if (ModManager.ModStatus != modsBeforeSelection && ConfigManager.sRankType == RankingType.SelectedMod)
                    RefreshScores();
            };
            GameBase.ShowDialog(ms);
        }

        private void buttonRandom_OnClick(object sender, EventArgs e)
        {
            if (KeyboardHandler.ShiftPressed)
            {
                beatmapTreeManager.EndRandom();
                beatmapTreeManager.UndoRandom();
            }
            else
            {
                if (beatmapTreeManager.IsRandomActive)
                {
                    beatmapTreeManager.EndRandom();
                }
                else
                {
                    beatmapTreeManager.StartRandom();
                }
            }

            AudioEngine.Click(null, @"click-short-confirm", 1, true);
        }

        private void start_OnClick(object sender, EventArgs e)
        {
            if (GameBase.FadeState != FadeStates.Idle) return;

            Transformation t = new Transformation(TransformationType.Scale, 0.6f, 2, GameBase.Time, GameBase.Time + 800);
            t.Easing = EasingTypes.Out;
            s_Osu2.Alpha = 1;
            s_Osu2.Transformations.Clear();
            s_Osu2.Transformations.Add(t);
            s_Osu2.HandleInput = false;
            s_Osu.Position = new Vector2(900, 900);
            s_Osu1.Position = new Vector2(900, 900);
            Start();
        }

        private void back_OnClick(object sender, EventArgs e)
        {
            AudioEngine.PlaySamplePositional(@"menuback");

            HandleInput = false;

            if (MultiMode)
            {
                if (initialBeatmap != null)
                {
                    BeatmapManager.Current = initialBeatmap;
                    try
                    {
                        AudioEngine.LoadAudio(BeatmapManager.Current, false, true);
                    }
                    catch { }
                }

                GameBase.ChangeMode(OsuModes.MatchSetup);
            }
            else
                GameBase.ChangeMode(OsuModes.Menu);
        }

        private bool KeyboardHandler_OnKeyReleased(object sender, Keys k)
        {
            switch (k)
            {
                case Keys.F2:
                    beatmapTreeManager.EndRandom();
                    return true;
            }

            return false;
        }

        private bool KeyboardHandler_OnKeyRepeat(object sender, Keys k, bool first)
        {
            if (!HandleInput) return false;

            if (first)
            {
                if (BeatmapManager.Current != null && BeatmapManager.Current.AppropriateMode == PlayModes.OsuMania)
                {
                    if (KeyboardHandler.ControlPressed && (k == Keys.OemPlus || k == BindingManager.For(Bindings.IncreaseSpeed)))
                    {
                        SpeedMania.AdjustSpeed(1);
                        return true;
                    }
                    else if (KeyboardHandler.ControlPressed && (k == Keys.OemMinus || k == BindingManager.For(Bindings.DecreaseSpeed)))
                    {
                        SpeedMania.AdjustSpeed(-1);
                        return true;
                    }
                }

                switch (k)
                {
                    case Keys.D1:
                    case Keys.D2:
                    case Keys.D3:
                    case Keys.D4:
                        if (!KeyboardHandler.ControlPressed || !AllowModeChange) break;

                        PlayModes playMode = (PlayModes)(k - Keys.D1);
                        ChangeMode(playMode);

                        return true;
                    case Keys.Escape:
                        if (beatmapTreeManager.IsRandomActive)
                            beatmapTreeManager.EndRandom();
                        else if (searchMode)
                            ResetSearch();
                        else
                            back_OnClick(null, null);
                        return true;
                    case Keys.A:
                        if (!PlayMode)
                            return false;

                        if (KeyboardHandler.ControlPressed && !MultiMode)
                        {
                            ModManager.ModStatus ^= Mods.Autoplay;
                            ModManager.CheckForCurrentPlayMode();
                            return true;
                        }
                        break;
                    case Keys.Delete:
                        if (KeyboardHandler.ShiftPressed)
                        {
                            optionDeleteBeatmap(null, null);
                            return true;
                        }
                        break;

                    case Keys.F1:
                        buttonMods_OnClick(null, null);
                        return true;
                    case Keys.F2:
                        if (first)
                        {
                            buttonRandom_OnClick(null, null);
                        }
                        return true;
                    case Keys.F3:
                        if (!beatmapTreeManager.IsRandomActive)
                            TogglePopup();
                        return true;
                    case Keys.F5:
                        BeatmapImport.ForceRun(true);
                        return true;
                }
            }

            UpdateSearch();
            return false;
        }

        private void UpdateDraggedItem()
        {
            spriteManagerDraggedItems.Clear(false);

            if (draggedItem == null)
            {
                return;
            }

            draggedItem.Position = MouseManager.MousePosition / GameBase.WindowRatio - new Vector2(150, 0);
            draggedItem.Move(Vector2.Zero);

            draggedItem.AddToSpriteManager(spriteManagerDraggedItems);
        }

        protected override void Dispose(bool disposing)
        {
            // Shutdown without forcing abort. We don't want out difficulties to keep computing outside of song select!
            try
            {
                difficultyCalculationThreadPool.Shutdown(false, 0);
                difficultyCalculationThreadPool.Dispose();
            }
            catch
            {
                //todo: triggering https://gist.github.com/8d0592f5fa64713307d0
                //may need further investigation.
            }

            AllowModeChange = true;

            LastSelectMode.Value = Player.Mode;

            KeyboardHandler.OnKeyRepeat -= KeyboardHandler_OnKeyRepeat;
            KeyboardHandler.OnKeyReleased -= KeyboardHandler_OnKeyReleased;
            JoystickHandler.OnButtonPressed -= JoystickHandler_OnButtonPressed;

            beatmapTreeManager.OnSelectedBeatmap -= beatmapTreeManager_OnSelectedBeatmap;
            beatmapTreeManager.OnSelectedGroup -= beatmapTreeManager_OnSelectedGroup;
            beatmapTreeManager.OnRightClicked -= beatmapTreeManager_OnRightClicked;
            beatmapTreeManager.OnActivated -= beatmapTreeManager_OnActivated;
            beatmapTreeManager.OnExpanded -= beatmapTreeManager_OnItemExpanded;
            beatmapTreeManager.OnDragged -= beatmapTreeManager_OnDragged;
            beatmapTreeManager.OnStartRandom -= beatmapTreeManager_OnStartRandom;
            beatmapTreeManager.OnEndRandom -= beatmapTreeManager_OnEndRandom;


            BeatmapTreeGroup g = beatmapTreeManager.OpenGroup;
            lastOpenGroupName = g == null || !g.Expanded ? null : g.Name;

            beatmapTreeManager.Dispose();

            if (modeMenu != null) modeMenu.Dispose();

            if (audioThread != null) audioThread.Abort();

            BanchoClient.OnConnect -= BanchoClient_OnConnect;
            ModManager.OnModsChanged -= ModManager_OnModsChanged;

            if (spriteManagerHigh != null)
                spriteManagerHigh.Dispose();

            if (spriteManagerLow != null)
                spriteManagerLow.Dispose();

            if (spriteManagerDraggedItems != null)
                spriteManagerDraggedItems.Dispose();

            if (GameBase.Mode != OsuModes.Play)
                spriteManagerBackground.Dispose();
            else
            {
                spriteManagerBackground.SpriteList.ForEach(s =>
                {
                    pSprite sp = s as pSprite;
                    if (sp != null && sp.Texture != null)
                        sp.Texture.Disposable = false;
                });
                Player.SpriteManagerLoading = spriteManagerBackground;
            }

            if (scoreList != null)
                scoreList.Dispose();

            storeSearch();

            Exiting = true;

            base.Dispose(disposing);
        }

        private void storeSearch()
        {
            if (string.IsNullOrEmpty(lastSearchText) && beatmapTreeManager.beatmapSetVisibleCount > 1 && nextSearchUpdate < 0)
                lastSearchText = string.IsNullOrEmpty(searchTextBox.Text) ? null : searchTextBox.Text;
            searchTextBox.Dispose();
        }

        private void Start()
        {
            if (BeatmapManager.Current == null || beatmapTreeManager.IsRandomActive || GameBase.FadeState != FadeStates.Idle)
                return;

            if (!Starting)
            {
                loadingSpinner.FadeOut(80);
                storeSearch();
                HandleInput = false;
                AudioEngine.PlaySamplePositional(@"menuhit");
            }

            switch (GameBase.Mode)
            {
                case OsuModes.SelectEdit:
                    GameBase.ChangeMode(OsuModes.Edit);
                    return;
                case OsuModes.SelectMulti:
                    GameBase.ChangeMode(OsuModes.MatchSetup);
                    return;
            }

            if (Starting)
            {
                GameBase.ChangeModeInstant(OsuModes.Play);
                return;
            }

            Starting = true;
            beatmapTreeManager.XOffset = 500;

            if (beatmapBackground != null)
            {
                pSprite bb2 = beatmapBackground.Clone();
                bb2.Depth += 0.01f;
                bb2.Additive = true;
                bb2.ScaleTo(bb2.Scale + 0.04f, 1000, EasingTypes.Out);
                bb2.FadeOutFromOne(1000);

                spriteManagerBackground.Add(bb2);
            }


            spriteManagerHigh.SpriteList.ForEach(s =>
            {
                if (s.Position.Y > GameBase.WindowDefaultHeight / 2)
                    s.MoveToRelative(new Vector2(0, 110), 800, EasingTypes.Out);
                else
                    s.MoveToRelative(new Vector2(0, -110), 800, EasingTypes.Out);
            });

            spriteManagerLow.SpriteList.ForEach(s => { s.FadeOut(500); });
            scoreList.SpriteManager.SpriteList.ForEach(s => { s.FadeOut(500); s.HandleInput = false; });

            scrollbarBackground.FadeOut(100);
            scrollbarForeground.FadeOut(100);

            s_ModeLogo.FadeColour(Color.SkyBlue, 1000);
            s_ModeLogo.FadeOut(1000);
            s_ModeLogo.Transformations.Add(new Transformation(TransformationType.Scale, 1, 2, GameBase.Time, GameBase.Time + 4000, EasingTypes.Out));
            s_ModeLogo.Transformations.Add(new Transformation(TransformationType.Rotation, 0, 1, GameBase.Time, GameBase.Time + 4000, EasingTypes.In));
        }

        public override void Update()
        {
            if (Starting) //a map is being loaded (transition playing)
            {
                if (topBackground.Transformations.Count == 0)
                    Start();
            }
            else
            {
                IdleHandler.Check(14000);

                scoreList.Update();

                searchTextBox.MaintainFocus = !ChatEngine.IsVisible && GameBase.ActiveDialog == null;

                //Beat the bottom-right osu! logo in time with the beat.
                if (s_ModeLogo != null && GameBase.SixtyFramesPerSecondFrame)
                    s_ModeLogo.Alpha = (float)((80 - AudioEngine.SyncBeatProgress * 30) / 255) * (beatmapBackground == null ? 1 : (1 - beatmapBackground.Alpha / 2));

                //Add a slight delay after dialogs disappear to stop input from being redirected to the fulltext field.
                if (dialogCooldown > 0 && !dialogVisible)
                {
                    dialogCooldown -= 16;
                    if (dialogCooldown < 0)
                        HandleInput = true;
                }
            }

            bool oldDialogVisible = dialogVisible;
            dialogVisible = GameBase.ActiveDialog != null;

            if (dialogVisible != oldDialogVisible)
            {
                if (dialogVisible)
                    HandleInput = false;
                else
                    dialogCooldown = 100;
            }

            if (whiteOverlay.Alpha > 0 && GameBase.FadeState == FadeStates.Idle)
            {
                BloomRenderer.Magnitude = 0.004f;
                BloomRenderer.RedTint = 0;
                BloomRenderer.ExtraPass = true;
                BloomRenderer.HiRange = false;
                BloomRenderer.Additive = true;
                BloomRenderer.Alpha = whiteOverlay.Alpha / 1.18f;
            }

            modsList.Text = ModManager.Format(ModManager.ModStatus, false);

            //Lets stop doing stuff if we're already exiting.  Can't remember why :).
            if (Exiting)
                return;

            UpdateSearch();

            UpdateMusic();

            beatmapTreeManager.AllowDragAnywhere = SpriteManager.ClickHandledSprite == null;
            beatmapTreeManager.Update();

            if (!Starting && beatmapTreeManager.HandleInput)
            {
                beatmapTreeManager.XOffset = 0;
            }

            updateManiaSpeed();

            ScheduleNextBeatmapDifficultyCalculation();

            UpdateDraggedItem();

            scrollbarForeground.MoveTo(new Vector2(scrollbarForeground.Position.X, 73 + (310 * beatmapTreeManager.ScrollPosition)), 30, EasingTypes.Out);

            base.Update();
        }

        private void updateManiaSpeed()
        {
            speedLabel.Tag = SpeedMania.RelativeSpeed;

            if (BeatmapManager.Current != null && BeatmapManager.Current.AppropriateMode == PlayModes.OsuMania)
                speedLabel.Text = SpeedMania.GetSpeedString();
            else
                speedLabel.Text = String.Empty;
        }

        //private void UnloadBackground(bool instant = false)
        //{
        //    pSprite background = beatmapBackground;
        //    beatmapBackground = null;

        //    if (background != null)
        //    {
        //        if (instant)
        //        {
        //            defaultBackground.Show();
        //            background.FadeOut(200);

        //            background.AlwaysDraw = false;
        //            background.IsDisposable = true;
        //        }
        //        else
        //        {
        //            GameBase.Scheduler.Add(delegate
        //            {
        //                background.FadeOut(200);
        //                background.AlwaysDraw = false;
        //                background.IsDisposable = true;
        //            }, 400);
        //        }
        //    }
        //}

        //float backgroundDepth = 0.1f;

        ///// <summary>
        ///// Attempts to load the background of the active beatmapset in place, else falling back with a default.
        ///// </summary>
        //private void UpdateBackground()
        //{
        //    if (!beatmapBackgroundLoaded && BeatmapManager.Current != null && AudioEngine.LoadedBeatmap == BeatmapManager.Current)
        //    {
        //        beatmapBackgroundLoaded = true;

        //        if (BeatmapManager.Current.BackgroundImage != null)
        //        {
        //            Beatmap beatmap = BeatmapManager.Current;
        //            //Make a local reference to the current beatmap.

        //            GameBase.RunGraphicsThread(delegate
        //            {
        //                defaultBackground.FadeOut(800);

        //                if (beatmapBackground != null)
        //                    UnloadBackground(false);

        //                beatmapBackground = new pSprite(SkinManager.Load(beatmap.BackgroundImage, SkinSource.Beatmap), Fields.Centre, Origins.Centre, Clocks.Game, Vector2.Zero, (backgroundDepth += 0.00001f), true, new Color(128, 128, 128, 255));
        //                beatmapBackground.ScaleToScreen();
        //                beatmapBackground.FadeInFromZero(400);
        //                spriteManagerBackground.Add(beatmapBackground);
        //            });
        //        }
        //        else
        //            UnloadBackground(true);
        //    }
        //}

        private void beatmapTreeManager_OnStartRandom()
        {
            s_Osu2.HandleInput = false;

            BeatmapManager.Current = null;
            ShowScores();

            GameBase.TransitionManager.UpdateBackground();
        }

        private void beatmapTreeManager_OnEndRandom()
        {
            s_Osu2.HandleInput = true;
            forceNextSelection = true;
        }

        /// <summary>
        /// Primary function is to mark BeatmapTreeItems as SearchHidden or not. The actual state changes are performed afterwards in UpdateList()
        /// </summary>
        private void UpdateSearch(bool force = false)
        {
            if (!force && (ChatEngine.IsVisible || GameBase.FadeState == FadeStates.FadeOut || !HandleInput))
            {
                searchTextChanged = false;
                return;
            }

            bool newText = force;
            if (searchTextChanged)
            {
                searchTextChanged = false;

                if (searchTextBox.Text.Length == 0)
                    return;

                newText = true;
            }

            bool searchReady = !beatmapTreeManager.IsRandomActive && (nextSearchUpdate >= 0 && nextSearchUpdate <= GameBase.Time || force);

            if (searchTextBox.Text.Length > 0 && (newText || (searchString != searchTextBox.Text && searchReady)))
            {
                nextSearchUpdate = GameBase.Time + 300;
                ShowSearch();

                if (!searchReady)
                    searchInfo.Text = LocalisationManager.GetString(OsuString.SongSelection_Searching);
                else
                {
                    beatmapTreeManager.ResetVisibleItemWindow();

                    int matches = 0;
                    int toSelectIndex = -1;

                    nextSearchUpdate = -1;
                    searchString = searchTextBox.Text;

                    string query = searchTextBox.Text.ToLower();

                    // Filter out all menu items which are not eligible for search anyway
                    foreach (BeatmapTreeItem m in beatmapTreeManager.MenuItems)
                    {
                        m.SearchHidden = !(m is BeatmapTreeGroup || m.Beatmap.BeatmapPresent || GameBase.Mode != OsuModes.SelectPlay);
                    }

                    // Check for each word whether a beatmap doesn't match the filter
                    string[] words = query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string w in words)
                    {
                        searchFilter filter = getSearchFilter(w);

                        foreach (BeatmapTreeItem m in beatmapTreeManager.MenuItems)
                        {
                            if (!m.SearchHidden && !(m is BeatmapTreeGroup) && !filter(m.Beatmap))
                            {
                                m.SearchHidden = true;
                            }
                        }
                    }

                    beatmapTreeManager.UpdateList();
                    beatmapTreeManager.ReturnToHoveredItem(0);

                    bool shallSelect = true;

                    // Count how many matches we got and select the first one
                    for (int i = 0; i < beatmapTreeManager.MenuItems.Count; ++i)
                    {
                        BeatmapTreeItem m = beatmapTreeManager.MenuItems[i];

                        if (!m.SearchHidden && !(m is BeatmapTreeGroup))
                        {
                            matches++;

                            if (shallSelect)
                            {
                                if (toSelectIndex == -1)
                                {
                                    toSelectIndex = i;
                                }
                                // If we find another group leader, then don't select
                                else if (m.IsGroupLeader)
                                {
                                    shallSelect = false;
                                }
                            }
                        }
                    }

                    BeatmapTreeItem selected = beatmapTreeManager.SelectedBti;

                    if (shallSelect &&
                        toSelectIndex > 0 &&
                            (selected == null ||
                             selected.SearchHidden ||
                             selected.GroupNumber != beatmapTreeManager.MenuItems[toSelectIndex].GroupNumber))
                    {
                        beatmapTreeManager.SelectRecommendedDifficulty(toSelectIndex, true);
                    }

                    UpdateMatchesText(matches);
                }
            }
            else if (searchTextBox.Text.Length == 0)
            {
                ResetSearch();
            }
        }

        private void ResetSearch()
        {
            if (!searchMode)
                return;

            beatmapTreeManager.ResetVisibleItemWindow();

            searchString = string.Empty;
            InputManager.TextBox.Clear();

            foreach (BeatmapTreeItem m in beatmapTreeManager.MenuItems)
                m.SearchHidden = false;

            searchMode = false;
            nextSearchUpdate = -1;

            beatmapTreeManager.UpdateList();
            beatmapTreeManager.ReturnToHoveredItem(0);

            searchTextBox.SetToDefault();

            searchBackground.Transformations.Add(new Transformation(TransformationType.Fade, searchBackground.Alpha, 0.4F, GameBase.Time, GameBase.Time + 200));
            searchBackground.Transformations.Add(new Transformation(new Vector2(searchBackground.Position.X, 54), new Vector2(searchBackground.Position.X, 46), GameBase.Time, GameBase.Time + 200, EasingTypes.Out));
            searchInfo.FadeOut(200);
        }

        private void ShowSearch()
        {
            if (searchMode)
                return;

            searchBackground.Transformations.Add(new Transformation(TransformationType.Fade, searchBackground.Alpha, 0.95F, GameBase.Time, GameBase.Time + 300));
            searchBackground.Transformations.Add(new Transformation(new Vector2(searchBackground.Position.X, 46), new Vector2(searchBackground.Position.X, 54), GameBase.Time, GameBase.Time + 200, EasingTypes.Out));
            searchInfo.FadeIn(300);

            searchMode = true;
        }

        private delegate bool searchFilter(Beatmap m);
        static private Regex regComparison = new Regex(@"^(\w*)([<>=]=?|!=)(.*)$");
        static private Regex regNumber = new Regex(@"
            ^
            [+-]?
            (?:
                # start with digit
                (?>\d+) \.?
                |
                # start with point
                \. \d
            )
            \d*
            (?: e [+-]? \d+ )?  # possible exponent
            $
        ", RegexOptions.IgnorePatternWhitespace);

        static private KeyValuePair<double, string>[] modePairs = new KeyValuePair<double, string>[]
        {
            new KeyValuePair<double, string> ((double)PlayModes.Osu, "osu!"),
            new KeyValuePair<double, string> ((double)PlayModes.Taiko, "taiko"),
            new KeyValuePair<double, string> ((double)PlayModes.CatchTheBeat, "catchthebeat"),
            new KeyValuePair<double, string> ((double)PlayModes.CatchTheBeat, "ctb"),
            new KeyValuePair<double, string> ((double)PlayModes.OsuMania, "osu!mania"),
            new KeyValuePair<double, string> ((double)PlayModes.OsuMania, "osumania"),
            new KeyValuePair<double, string> ((double)PlayModes.OsuMania, "mania"),
            new KeyValuePair<double, string> ((double)PlayModes.OsuMania, "o!m"),
        };

        static private KeyValuePair<double, string>[] statusPairs = new KeyValuePair<double, string>[]
        {
            new KeyValuePair<double, string> ((double)SubmissionStatus.Unknown, "unknown"),
            new KeyValuePair<double, string> ((double)SubmissionStatus.NotSubmitted, "notsubmitted"),
            new KeyValuePair<double, string> ((double)SubmissionStatus.Pending, "pending"),
            new KeyValuePair<double, string> ((double)SubmissionStatus.Ranked, "ranked"),
            new KeyValuePair<double, string> ((double)SubmissionStatus.Approved, "approved"),
        };

        static private searchFilter getSearchFilter(string w)
        {
            Match match = regComparison.Match(w);
            if (match.Success)
            {
                string key = match.Groups[1].Value.ToLower();
                string op = match.Groups[2].Value;
                string val = match.Groups[3].Value.ToLower();
                if (op == @"=") op = @"==";

                double num;

                Match matchNum = regNumber.Match(val);
                if (matchNum.Success)
                {
                    num = Double.Parse(matchNum.Groups[0].Value, GameBase.nfi);
                    switch (key)
                    {
                        case "star":
                        case "stars":
                            return delegate(Beatmap b) { return isPatternMatch(Math.Round(b.StarDisplay, 2), op, num); };

                        case "cs":
                            //make sure to limit to game modes that support CS.
                            return delegate(Beatmap b) { return isPatternMatch(Math.Round((double)b.DifficultyCircleSize, 1), op, num) && b.PlayMode != PlayModes.OsuMania && b.PlayMode != PlayModes.Taiko; };
                        case "hp":
                            return delegate(Beatmap b) { return isPatternMatch(Math.Round((double)b.DifficultyHpDrainRate, 1), op, num); };
                        case "od":
                            return delegate(Beatmap b) { return isPatternMatch(Math.Round((double)b.DifficultyOverall, 1), op, num); };
                        case "ar":
                            //make sure to limit to game modes that support AR.
                            return delegate(Beatmap b) { return isPatternMatch(Math.Round((double)b.DifficultyApproachRate, 1), op, num) && b.PlayMode != PlayModes.OsuMania && b.PlayMode != PlayModes.Taiko; };

                        case "key":
                        case "keys":
                            return delegate(Beatmap b) { return isPatternMatch(StageMania.ColumnsWithMods(b), op, num) && b.PlayMode == PlayModes.OsuMania; };

                        case "speed":
                            return delegate(Beatmap b) { return isPatternMatch(b.ManiaSpeed, op, num) && (b.PlayMode == PlayModes.OsuMania || b.PlayMode == PlayModes.Osu); };

                        case "bpm":
                            return delegate(Beatmap b) { return isPatternMatch(Math.Round(b.BeatsPerMinuteRange.Z), op, num); };
                        case "length":
                            return delegate(Beatmap b) { return isPatternMatch(b.TotalLength / 1000, op, num); };
                        case "drain":
                            return delegate(Beatmap b) { return isPatternMatch(Math.Max(b.RealDrainLength, b.DrainLength), op, num); };

                        case "played":
                            DateTime daysAgo;
                            try
                            {
                                daysAgo = DateTime.Now.AddDays(-num);
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                // clamp to the min or max value when out of range
                                daysAgo = num > 0 ? DateTime.MinValue : DateTime.MaxValue;
                            }
                            return delegate(Beatmap b) { return isPatternMatch(b.DateLastPlayed, op, daysAgo); };
                    }
                }

                switch (key)
                {
                    case "unplayed":
                        if (String.IsNullOrEmpty(val))
                            return delegate(Beatmap b) { return isPatternMatch(b.DateLastPlayed, op, DateTime.MinValue); };
                        break;
                    case "mode":
                        num = descriptorToNum(val, modePairs);
                        return delegate(Beatmap b) { return isPatternMatch((double)b.PlayMode, op, num); };
                    case "status":
                        num = descriptorToNum(val, statusPairs);

                        // treat status=ranked specially, as if ranked=approved, just as Groups do
                        // NOTE: this assumes approved == ranked + 1
                        if (num == (double)SubmissionStatus.Ranked)
                        {
                            switch (op)
                            {
                                case @">":
                                case "<=":
                                    num = (double)SubmissionStatus.Approved;
                                    break;
                                case "==":
                                    return delegate(Beatmap b) { return isPatternMatch((double)b.SubmissionStatus, op, num) || isPatternMatch((double)b.SubmissionStatus, op, num + 1); };
                                case "!=":
                                    return delegate(Beatmap b) { return isPatternMatch((double)b.SubmissionStatus, op, num) && isPatternMatch((double)b.SubmissionStatus, op, num + 1); };
                            }
                        }
                        return delegate(Beatmap b) { return isPatternMatch((double)b.SubmissionStatus, op, num); };
                }
            }

            int id;
            if (Int32.TryParse(w, out id))
            {
                return delegate(Beatmap b)
                {
                    //match mapid and mapset id while input is numbers.
                    if (b.BeatmapId == id) return true;
                    if (b.BeatmapSetId == id) return true;
                    if (b.BeatmapTopicId == id) return true;
                    return isWordMatch(b, w);
                };
            }

            return delegate(Beatmap b) { return isWordMatch(b, w); };
        }

        static private bool isPatternMatch<T>(T left, string op, T right) where T : IComparable<T>
        {
            int cmp = left.CompareTo(right);
            switch (op)
            {
                case @"<":
                    return cmp < 0;
                case @">":
                    return cmp > 0;
                case "==":
                    return cmp == 0;
                case ">=":
                    return cmp >= 0;
                case "<=":
                    return cmp <= 0;
                case "!=":
                    return cmp != 0;
                default:
                    return false;
            }
        }
        static private bool isWordMatch(Beatmap b, string word)
        {
            if (b.DisplayTitle.IndexOf(word, StringComparison.CurrentCultureIgnoreCase) >= 0) return true;
            if (b.Creator.IndexOf(word, StringComparison.CurrentCultureIgnoreCase) >= 0) return true;
            if (b.Tags.IndexOf(word, StringComparison.CurrentCultureIgnoreCase) >= 0) return true;
            if (b.Source.IndexOf(word, StringComparison.CurrentCultureIgnoreCase) >= 0) return true;
            if (b.ArtistUnicode != null && b.ArtistUnicode.IndexOf(word, StringComparison.CurrentCultureIgnoreCase) >= 0) return true;
            if (b.TitleUnicode != null && b.TitleUnicode.IndexOf(word, StringComparison.CurrentCultureIgnoreCase) >= 0) return true;
            return false;
        }

        static private double descriptorToNum(string got, KeyValuePair<double, string>[] pairs)
        {
            if (got.Length == 0)
                return Double.NaN;

            foreach (KeyValuePair<double, string> want in pairs)
            {
                // got can be partially typed and still match
                if (want.Value.StartsWith(got))
                    return want.Key;
            }
            return Double.NaN;
        }

        private void SelectProperTabAndDropdownGroup()
        {
            tabs.SetSelected(tabDisplayedMode, true, true);
            dropdownGroup.SetSelected(tabDisplayedMode, true);
        }

        private void LoadTree(TreeGroupMode group, bool expandParent = false, string preferredGroup = null, bool chooseRecommendedDifficulty = false)
        {
            if (!BeatmapManager.OnlineFavouritesLoaded && group == TreeGroupMode.Online_Favourites)
            {
                BeatmapManager.OnOnlineFavouritesLoaded += delegate { GameBase.Scheduler.Add(delegate { if (GameBase.RunningGameMode == this) LoadTree(group, expandParent, preferredGroup, chooseRecommendedDifficulty); }); };
                BeatmapManager.LoadOnlineFavourites();
            }

            if (group == TreeGroupMode.None)
                group = TreeGroupMode.Show_All;

            tabDisplayedMode = group;
            SelectProperTabAndDropdownGroup();


            beatmapTreeManager.HandleInput = false;

            beatmapTreeManager.FadeOut(250);
            beatmapTreeManager.XOffset = 500;
            beatmapTreeManager.GenerateTree(group, !PlayMode && !MultiMode, delegate
            {
                // If we are no longer this gamemode or if we start playing we don't want to process this any further
                if (GameBase.RunningGameMode != this || Starting)
                {
                    return;
                }

                // Return beatmap panels to the visible area and let them handle input again
                beatmapTreeManager.HandleInput = true;
                beatmapTreeManager.NextFrameInstantMove = true;

                beatmapTreeManager.Select(BeatmapManager.Current, expandParent, preferredGroup, chooseRecommendedDifficulty);

                UpdateSearch(true);

                beatmapTreeManager.ReturnToHoveredItem(0);
            });
        }

        private void UpdateMatchesText(int matches)
        {
            if (searchMode)
            {
                if (matches > 1)
                    searchInfo.Text = string.Format(LocalisationManager.GetString(OsuString.SongSelection_MatchesFound), matches);
                else if (matches > 0)
                    searchInfo.Text = string.Format(LocalisationManager.GetString(OsuString.SongSelection_MatchFound), matches);
                else
                    searchInfo.Text = LocalisationManager.GetString(OsuString.SongSelection_Reset);
            }
        }



        internal void UpdateDetails()
        {
            if (beatmapTreeManager.IsRandomActive)
                return;

            Beatmap current = BeatmapManager.Current;

            Vector3 bpmRange = current.BeatsPerMinuteRange;
            double totalLength = HitObjectManager.ApplyModsToTime(current.TotalLength);

            details.Text = current.DisplayTitleFullAuto;
            if (ModManager.CheckActive(Mods.DoubleTime))
            {
                details3.TextColour = new Color(246, 154, 161);
                bpmRange *= 1.5f;
            }
            else if (ModManager.CheckActive(Mods.HalfTime))
            {
                details3.TextColour = Color.LightBlue;
                bpmRange *= 0.75f;
            }
            else
            {
                details3.TextColour = Color.White;
            }

            int mins = (int)totalLength / 60000;
            int secs = (int)(totalLength % 60000) / 1000;

            details3.TextBold = true;
            details3.Text = string.Format(LocalisationManager.GetString(OsuString.SongSelection_BeatmapInfo), mins, secs, Beatmap.RangeToString(bpmRange), current.ObjectCount);
            details4.Text = string.Format(LocalisationManager.GetString(OsuString.SongSelection_BeatmapInfo2), current.countNormal, current.countSlider, current.countSpinner);

            if (BeatmapManager.Current.Creator.Length > 0)
                details2.Text = string.Format(LocalisationManager.GetString(OsuString.SongSelection_BeatmapInfoCreator), current.Creator);
            else
                details2.Text = string.Empty;


            if (ModManager.CheckActive(Mods.HardRock))
            {
                details5.TextColour = new Color(246, 154, 161);
            }
            else if (ModManager.CheckActive(Mods.Easy))
            {
                details5.TextColour = Color.LightBlue;
            }
            else
            {
                details5.TextColour = Color.White;
            }


            details5.Text = current.InfoText;

            // update scroll speed for mania map or autoconvert
            if (current.AppropriateMode == PlayModes.OsuMania)
            {
                SpeedMania.SetSpeed(0, ManiaSpeedSet.Unowned | ManiaSpeedSet.Reset);
                updateManiaSpeed();
            }
        }

        private void UpdateStatusIcon()
        {
            pTexture newTexture = statusIcon.Texture;

            // Ingame rating in song select is deprecated. Considered too much clutter for the benefit at the moment.
            //ShowRating();

            bool fadeIn = true;

            switch (BeatmapManager.Current.SubmissionStatus)
            {
                case SubmissionStatus.Ranked:
                    {
                        Transformation t = new Transformation(TransformationType.Scale, 0.92f, 1.4f, GameBase.Time, GameBase.Time + 200);
                        t.Easing = EasingTypes.In;
                        Transformation t2 = new Transformation(TransformationType.Scale, 1.4f, 1, GameBase.Time + 200, GameBase.Time + 400);
                        t2.Easing = EasingTypes.In;
                        statusIcon.Transformations.Add(t);
                        statusIcon.Transformations.Add(t2);
                        newTexture = statusIconRanked;
                    }
                    break;
                case SubmissionStatus.Approved:
                    {
                        newTexture = statusIconApproved;
                        Transformation t = new Transformation(TransformationType.Rotation, -0.4f, 0, GameBase.Time, GameBase.Time + 400);
                        t.Easing = EasingTypes.Out;
                        Transformation t2 = new Transformation(TransformationType.Scale, 1.4f, 1, GameBase.Time, GameBase.Time + 400);
                        t2.Easing = EasingTypes.Out;
                        statusIcon.Transformations.Add(t);
                        statusIcon.Transformations.Add(t2);
                    }
                    break;

                case SubmissionStatus.NotSubmitted:
                    {
                        statusIcon.FadeOut(500);
                        newTexture = statusIconUnknown;
                        Transformation t = new Transformation(TransformationType.Scale, 1, 0.8f, GameBase.Time, GameBase.Time + 500);
                        t.Easing = EasingTypes.Out;
                        statusIcon.Transformations.Add(t);
                        fadeIn = false;
                    }
                    break;
                default:
                    newTexture = statusIconUnknown;
                    break;
            }

            if (fadeIn)
            {
                if (newTexture != statusIcon.Texture)
                {
                    statusIcon.Texture = newTexture;
                    statusIcon.FadeInFromZero(400);
                }
                else
                    statusIcon.FadeIn(400);
            }
        }



        private void RefreshScores(bool force = false)
        {
            bool showScores = PlayMode || MultiMode;

            if (!OnlineScoreDisplay && showScores)
            {
                if (showScores)
                    ShowScores();
                return;
            }

            Beatmap b = BeatmapManager.Current;

            if (b != null)
            {
                if (loadingSpinner != null) loadingSpinner.FadeIn(200);

                if (showScores)
                    ClearScores();

                b.GetOnlineScores(showScores, force, showScores ? ConfigManager.sRankType.Value : RankingType.Top);
                b.OnGotOnlineScores += Current_OnGotOnlineScores;
            }
        }

        private void Current_OnGotOnlineScores(object sender)
        {
            GameBase.Scheduler.Add(delegate
            {
                if (BeatmapManager.Current != null)
                //is null during random.  we can safely throw away the onlineScresPending call in this case.
                {
                    if (BeatmapManager.Current.OnlineOffset != 0)
                        UpdateDetails();
                    ShowScores(true);
                }
            });

            BeatmapManager.Current.OnGotOnlineScores -= Current_OnGotOnlineScores;
        }

        internal void ShowScores(bool isCallback = false)
        {
            if (Starting) return;

            if (checkButton != null)
            {
                checkButton.AlwaysDraw = false;
                checkButton.FadeOut(200);
                if (checkButton.Tag != null)
                {
                    pSprite a = (pSprite)checkButton.Tag;
                    a.AlwaysDraw = false;
                    a.FadeOut(200);
                }
            }

            if (BeatmapManager.Current != null && BeatmapManager.Current.BeatmapId != 0)
            {
                rankForum.HandleInput = true;
                rankForum.Transformations.Clear();
                rankForum.Transformations.Add(new Transformation(TransformationType.Fade, rankForum.Alpha, 1, GameBase.Time, GameBase.Time + 300));
            }
            else
            {
                rankForum.HandleInput = false;
                rankForum.FadeOut(300);
            }

            loadingSpinner.FadeOut(50);

            if (BeatmapManager.Current == null)
            {
                ClearScores();
                return;
            }

            if (!PlayMode && !MultiMode)
            {
                //edit mode
                if (BeatmapManager.Current.SubmissionStatus >= SubmissionStatus.Ranked || BeatmapManager.Current.UpdateAvailable || isCallback)
                    ShowStatusIcon();
                else
                    RefreshScores();
                return;
            }

            ClearScores();

            Scores = new pList<Score>();

            string strInfo = string.Empty;

            if (!OnlineScoreDisplay)
                ConfigManager.sRankType.Value = RankingType.Local;
            else if (ConfigManager.sRankType.Value > RankingType.Top && BanchoClient.Connected && (BanchoClient.Permission & Permissions.Supporter) == 0)
                ConfigManager.sRankType.Value = RankingType.Top;

            switch (ConfigManager.sRankType.Value)
            {
                default:
                    if (!BeatmapManager.Current.IsRetrievingScores)// && BeatmapManager.Current.Scores.Count == 0)
                    {
                        RefreshScores();
                        return;
                    }
                    else
                    {
                        Scores.AddRange(BeatmapManager.Current.Scores);
                        BeatmapManager.Current.OnGotOnlineScores -= Current_OnGotOnlineScores;
                    }

                    if (ConfigManager.sRankType == RankingType.Friends && BeatmapManager.Current.onlinePersonalScore != null)
                    {
                        if (Scores.Find(s => s.playerName == BeatmapManager.Current.onlinePersonalScore.playerName) == null)
                            Scores.AddInPlace((Score)BeatmapManager.Current.onlinePersonalScore.Clone());

                        for (int i = 0; i < Scores.Count; i++) //rewrite ranks since personal best will offset them.
                            Scores[i].onlineRank = i + 1;
                    }
                    break;
                case RankingType.Local:
                    List<Score> localScores = ScoreManager.FindScores(BeatmapManager.Current.BeatmapChecksum, Player.Mode);
                    if (localScores != null && localScores.Count > 0 && !Scores.Contains(localScores[0]))
                    {
                        int i = 1;
                        foreach (Score s in localScores)
                        {
                            s.onlineRank = i++;
                            Scores.Add(s);
                        }
                    }
                    break;
            }

            BeatmapManager.Current.Scores = Scores;

            int rank = 1;
            int offset = 4;

            if (Scores != null && Scores.Count > 0)
            {
                for (int i = 0; i < Scores.Count; i++)
                {
                    Score s = Scores[i];
                    DisplayScorePanel(s, offset, rank++, s.onlineRank, i < Scores.Count - 1 ? s.totalScore - Scores[i + 1].totalScore : 0);

                    offset += 33;
                }

                if (OnlineScoreDisplay)
                {
                    pText p = new pText(LocalisationManager.GetString(OsuString.SongSelection_OnlineRecord), 14, new Vector2(120, 362f), Vector2.Zero, 0.05f, true, Color.TransparentWhite, false);
                    p.TextShadow = true;
                    p.Origin = Origins.TopCentre;
                    p.TextColour = Color.White;
                    p.TextBold = true;
                    spritesPersonalBest.Add(p);
                    p.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, GameBase.Time, GameBase.Time + 600));

                    DisplayScorePanel(BeatmapManager.Current.onlinePersonalScore, 382, 9, BeatmapManager.Current.onlinePersonalScore != null ? BeatmapManager.Current.onlinePersonalScore.onlineRank : 0, 0, spritesPersonalBest);

                    if (BeatmapManager.Current.onlinePersonalScore != null)
                    {
                        if (BeatmapManager.Current.PlayerRank != BeatmapManager.Current.onlinePersonalScore.ranking)
                        {
                            BeatmapManager.Current.PlayerRank = BeatmapManager.Current.onlinePersonalScore.ranking;
                            beatmapTreeManager.BeatmapManager_NewBeatmapInfo(null, null);
                        }
                    }
                }

                scoreList.SetContentDimensions(new Vector2(50, Scores.Count * 33));
            }
            else
            {
                ShowStatusIcon();
                scoreList.SetContentDimensions(Vector2.Zero);
            }

            scoreList.ScrollToPosition(0, -0.99f);
            if (GameBase.User != null)
            {
                int i = Scores.FindIndex(s => s.playerName == GameBase.User.Name);
                if (i >= 8 && Scores.Count > 8)
                    scoreList.ScrollToPosition(Math.Max(0, scoreList.ContentDimensions.Y - scoreList.ActualDisplayHeight) * ((float)(i - 7) / (float)(Scores.Count - 8)), -0.99f);
            }

            spriteManagerLow.Add(spritesPersonalBest);

            UpdateStatusIcon();
        }



        private void DisplayScorePanel(Score s, float offset, int rank, int displayRank, int diff, List<pSprite> spriteList = null)
        {
            Vector2 left = new Vector2(200, 0);
            float hOffset = spriteList != null ? 3 : 0;

            int thisTime = ((rank - 1) * 50) + GameBase.Time;

            Transformation fadeIn = new Transformation(TransformationType.Fade, 0, 1, thisTime, thisTime + 200);

            bool isPersonal = spriteList != null;

            string str1;

            if (isPersonal)
            {
                if (s == null)
                    str1 = LocalisationManager.GetString(OsuString.SongSelection_NoPersonalRecordSet);
                else if (BeatmapManager.Current.TotalRecords > 0 && BeatmapManager.Current.TotalRecords > displayRank)
                    str1 = string.Format(@"#{0:#,0} of {1:#,0}", displayRank, BeatmapManager.Current.TotalRecords);
                else
                    str1 = string.Format(@"#{0:#,0} {1}", displayRank, s.playerName);
            }
            else
                str1 = s.playerName;

            Color color = Color.TransparentWhite;

            pText rankText = new pText(displayRank > 0 ? displayRank.ToString() : string.Empty, 18, new Vector2(18 + hOffset, offset + 15), Vector2.Zero, 0.6F, true, color, true);
            rankText.Origin = Origins.Centre;
            rankText.TextBorder = true;
            rankText.TextBold = true;
            if (spriteList != null)
                spriteList.Add(rankText);
            else
                scoreList.SpriteManager.Add(rankText);

            pSpriteDynamic avatar = null;

            if (s != null && s.user != null && s.user.Id > 0)
            {
                avatar = new pSpriteDynamic(null, null, s.user.Id == GameBase.User.Id ? 0 : 500, new Vector2(18 + hOffset, offset + 15.4f), 0.45f);
                s.user.LoadAvatarInto(avatar, 46);

                if (spriteList != null)
                    spriteList.Add(avatar);
                else
                    scoreList.SpriteManager.Add(avatar);
            }

            bool isFriend = s != null && s.user != null && ChatEngine.Friends.Contains(s.user.Id);

            if (isFriend && ConfigManager.sRankType.Value != RankingType.Friends)
                color = new Color(255, 235, 115, 0);

            int transformTime = 400;

            pSprite p = new pText(str1, 16, new Vector2(58 + hOffset, s != null ? offset : offset + 6), Vector2.Zero, 0.4F, true, color, true);
            Transformation movement = new Transformation(p.InitialPosition - left, p.InitialPosition, thisTime, thisTime + transformTime);
            movement.Easing = EasingTypes.OutBack;
            p.Transformations.Add(movement);
            p.Transformations.Add(fadeIn);
            if (spriteList != null)
                spriteList.Add(p);
            else
                scoreList.SpriteManager.Add(p);

            if (s != null)
            {
                string str2 = string.Format(LocalisationManager.GetString(OsuString.SongSelection_ScoreList), s.totalScore, s.maxCombo);
                p = new pText(str2, 12, new Vector2(58 + hOffset, 15 + offset), Vector2.Zero, 0.4F, true, Color.TransparentWhite, true);
                movement = new Transformation(p.InitialPosition - left, p.InitialPosition, thisTime, thisTime + transformTime);
                movement.Easing = EasingTypes.OutBack;
                p.Transformations.Add(movement);
                p.Transformations.Add(fadeIn);
                if (spriteList != null)
                    spriteList.Add(p);
                else
                    scoreList.SpriteManager.Add(p);

                pText pt = new pText(
                    ModManager.FormatShort(s.enabledMods, false, false) + '\n' +
                    string.Format(@"{0:0.00}%", s.accuracy * 100) + '\n' +
                    (diff > 0 ? @"+" + diff.ToString(@"#,0") : @"-"),
                    10, new Vector2(236 + hOffset, 0 + offset), new Vector2(100, 0), 0.4F, true, Color.TransparentWhite, true);
                pt.TextBorder = true;
                pt.TextAa = false;
                pt.TextAlignment = TextAlignment.Right;
                pt.Origin = Origins.TopRight;
                movement = new Transformation(pt.InitialPosition - left, pt.InitialPosition, thisTime, thisTime + transformTime);
                movement.Easing = EasingTypes.OutBack;
                pt.Transformations.Add(movement);
                pt.Transformations.Add(fadeIn);
                if (spriteList != null)
                    spriteList.Add(pt);
                else
                    scoreList.SpriteManager.Add(pt);
            }

            DateTime date = DateTime.Now;

            if (s != null)
            {
                try
                {
                    p = new pSprite(SkinManager.Load(string.Format(@"ranking-{0}-small", s.ranking)), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(46 + hOffset, 14.6f + offset), 0.4F, true, Color.TransparentWhite);
                    movement = new Transformation(p.InitialPosition - left, p.InitialPosition, thisTime, thisTime + transformTime);
                    movement.Easing = EasingTypes.OutBack;
                    p.Transformations.Add(movement);
                    p.Transformations.Add(fadeIn);
                    if (spriteList != null)
                        spriteList.Add(p);
                    else
                        scoreList.SpriteManager.Add(p);
                }
                catch (Exception)
                {
                }

                date = s.date.ToLocalTime();

                double hours = (DateTime.Now - date).TotalHours;
                if (hours < 96)
                {
                    pTextAwesome pr = new pTextAwesome(FontAwesome.arrow_circle_o_up, 18, new Vector2(248 + hOffset, 15 + offset))
                    {
                        Depth = 0.4f,
                        AlwaysDraw = true,
                        InitialColour = Color.TransparentWhite,
                        Alpha = 0
                    };
                    movement = new Transformation(pr.InitialPosition - left, pr.InitialPosition, thisTime, thisTime + transformTime);
                    movement.Easing = EasingTypes.OutBack;
                    pr.Transformations.Add(movement);
                    pr.Transformations.Add(fadeIn);
                    if (spriteList != null)
                        spriteList.Add(pr);
                    else
                        scoreList.SpriteManager.Add(pr);

                    string timeDiff = (int)hours + "h";

                    if (hours > 47)
                        timeDiff = (int)(hours / 24) + "d";
                    else if (hours >= 1)
                        timeDiff = (int)hours + "h";
                    else if (hours > 1 / 60.0)
                        timeDiff = (int)(hours * 60) + "m";
                    else
                        timeDiff = (int)(hours * 3600) + "s";

                    pText pt = new pText(timeDiff, 10, new Vector2(258 + hOffset, 15 + offset), new Vector2(100, 0), 0.4F, true, Color.TransparentWhite, true);
                    pt.TextBorder = true;
                    pt.TextAa = false;
                    pt.Origin = Origins.CentreLeft;
                    movement = new Transformation(pt.InitialPosition - left, pt.InitialPosition, thisTime, thisTime + transformTime);
                    movement.Easing = EasingTypes.OutBack;
                    pt.Transformations.Add(movement);
                    pt.Transformations.Add(fadeIn);
                    if (spriteList != null)
                        spriteList.Add(pt);
                    else
                        scoreList.SpriteManager.Add(pt);
                }
            }

            fadeIn = new Transformation(TransformationType.Fade, 0, 0.3F, thisTime, thisTime + 200);

            p = new pSprite(SkinManager.Load(@"menu-button-background"), Fields.TopLeft, Origins.CentreLeft, Clocks.Game, new Vector2(hOffset, 15 + offset), 0.12F + ((float)rank / 1000), true, new Color(0, 0, 0, 120), s);

            if (s != null)
            {
                if (s.playMode == PlayModes.OsuMania)
                    p.ToolTip = string.Format(LocalisationManager.GetString(OsuString.SongSelection_TooltipScoreMania), date, s.countGeki, s.count300, s.countKatu, s.count100 + s.count50 + s.countMiss, s.accuracy * 100, ModManager.Format(s.enabledMods, true));
                else
                    p.ToolTip = string.Format(LocalisationManager.GetString(OsuString.SongSelection_TooltipScore), date, s.count300, s.count100, s.count50, s.countMiss, s.accuracy * 100, ModManager.Format(s.enabledMods, true));
                p.OnClick += score_OnClick;
                p.ClickRequiresConfirmation = true;
                p.HandleInput = true;
                p.HoverEffect = new Transformation(TransformationType.Fade, 0.3f, 0.6f, 0, 200);

                p.OnHover += delegate
                {
                    rankText.FadeIn(100);
                    if (avatar != null) avatar.FadeColour(Color.DarkGray, 100);
                    AudioEngine.Click(null, @"click-short");
                };
                p.OnHoverLost += delegate
                {
                    rankText.FadeOut(100);
                    if (avatar != null) avatar.FadeColour(Color.White, 100);
                };
            }
            movement = new Transformation(p.InitialPosition - left, p.InitialPosition, thisTime, thisTime + transformTime);
            movement.Easing = EasingTypes.OutBack;
            p.Alpha = 0;
            p.Transformations.Add(movement);
            p.Transformations.Add(fadeIn);

            p.Scale = 0.55F;

            int delayIn = 150;
            int delayOut = 900;

            int loopDelay = 0;
            while (loopDelay < delayIn + delayOut)
                loopDelay += (int)Math.Max(100, (AudioEngine.beatLength * 2));
            loopDelay -= delayIn + delayOut;

            int initialOffset = thisTime + (int)(AudioEngine.beatLength * (1 - AudioEngine.SyncBeatProgress)) - delayIn;

            Transformation flashLoop1 = new Transformation(new Color(0, 0, 0, 255), new Color(100, 100, 100, 255), initialOffset, initialOffset + delayIn);
            flashLoop1.Easing = EasingTypes.In;
            flashLoop1.LoopDelay = delayOut + loopDelay;
            flashLoop1.Loop = true;
            p.Transformations.Add(flashLoop1);

            Transformation flashLoop2 = new Transformation(new Color(100, 100, 100, 255), new Color(0, 0, 0, 255), initialOffset + delayIn, initialOffset + delayIn + delayOut);
            flashLoop2.Easing = EasingTypes.Out;
            flashLoop2.LoopDelay = delayIn + loopDelay;
            flashLoop2.Loop = true;
            p.Transformations.Add(flashLoop2);

            if (spriteList != null)
                spriteList.Add(p);
            else
                scoreList.SpriteManager.Add(p);

            scoreList.SpriteManager.SpriteList.ForEach(sp => sp.ClipRectangle = new RectangleF(0, 92, 640, (int)(33.4f * 8)));
        }


        private void ShowRating()
        {
            if (displayedRating == BeatmapManager.Current.OnlineRating)
                return;

            ratingStars.ForEach(ps =>
            {
                ps.AlwaysDraw = false;
                ps.FadeOut(50);
            });
            ratingStars.Clear();

            displayedRating = BeatmapManager.Current.OnlineRating;

            if (displayedRating == 0 || BeatmapManager.Current.SubmissionStatus < SubmissionStatus.Ranked) return;

            for (int i = 0; i < 10; i++)
            {
                pSprite star = new pSprite(SkinManager.Load(@"star", SkinSource.Osu), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(10 + i * 20, 60), 0.97F, true, Color.Yellow);
                star.Scale = 0.6f;
                star.Alpha = 0;

                if (displayedRating > i)
                {
                    if (displayedRating < i + 1)
                        star.DrawWidth = (int)(star.Width * (displayedRating - (int)displayedRating));
                }
                else
                    break;

                int time1 = GameBase.Time + i * 50;
                int time2 = GameBase.Time + i * 50 + 200;

                star.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, time1, time2));
                star.Transformations.Add(new Transformation(star.Position - new Vector2(20, 0), star.Position, time1, time2, EasingTypes.Out));

                ratingStars.Add(star);
                spriteManagerHigh.Add(star);
            }

            pText text = new pText(displayedRating.ToString(@"0.0", GameBase.nfi), 10, new Vector2(10 + displayedRating * 20, 60), 1, true, Color.Yellow);
            text.Alpha = 0;
            text.TextBold = true;
            text.Origin = Origins.Centre;
            text.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, (int)(GameBase.Time + displayedRating * 50), (int)(GameBase.Time + displayedRating * 50 + 300), EasingTypes.Out));
            text.Transformations.Add(new Transformation(TransformationType.Scale, 1.5f, 1, (int)(GameBase.Time + displayedRating * 50), (int)(GameBase.Time + displayedRating * 50 + 300), EasingTypes.Out));
            ratingStars.Add(text);
            spriteManagerHigh.Add(text);
        }

        private void ShowStatusIcon()
        {
            if (BeatmapManager.Current == null) return;

            checkButton = new pSprite(SkinManager.Load(@"selection-norecords", SkinSource.Osu), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(118, 170), 0.12F, true, Color.TransparentWhite);
            checkButton.FadeIn(400);
            checkButton.MoveTo(checkButton.Position + new Vector2(0, 30), 400, EasingTypes.Out);
            spriteManagerLow.Add(checkButton);

            if (OnlineScoreDisplay || !PlayMode)
            {
                if (BeatmapManager.Current.UpdateAvailable)
                {
                    checkButton.Texture = SkinManager.Load(@"selection-update", SkinSource.Osu);
                    checkButton.OnClick += checkBeatmapUpdates_OnClick;
                    checkButton.HoverEffect = new Transformation(Color.White, Color.LimeGreen, 0, 200);

                    checkButton.HandleInput = true;

                    pSprite p2 = new pSprite(SkinManager.Load(@"selection-update", SkinSource.Osu), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(118, 200), 0.13F, true, Color.TransparentWhite);
                    p2.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0, GameBase.Time, GameBase.Time + 400));
                    Transformation t = new Transformation(TransformationType.Scale, 1, 2, GameBase.Time + 400, GameBase.Time + 1400);
                    checkButton.Tag = p2;
                    t.Loop = true;
                    t.Easing = EasingTypes.Out;
                    p2.Transformations.Add(t);

                    t = new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + 400, GameBase.Time + 1400);
                    checkButton.Tag = p2;
                    t.Loop = true;
                    t.Easing = EasingTypes.Out;
                    p2.Transformations.Add(t);

                    spriteManagerHigh.Add(p2);
                }
                else
                {
                    switch (BeatmapManager.Current.SubmissionStatus)
                    {
                        case SubmissionStatus.Unknown:
                            checkButton.Texture = SkinManager.Load(@"selection-unknown", SkinSource.Osu);
                            break;
                        case SubmissionStatus.NotSubmitted:
                            checkButton.Texture = SkinManager.Load(@"selection-notsubmitted", SkinSource.Osu);
                            break;
                        case SubmissionStatus.Pending:
                            checkButton.Texture = SkinManager.Load(@"selection-notranked", SkinSource.Osu);
                            break;
                        case SubmissionStatus.Ranked:
                        case SubmissionStatus.Approved:
                            if (!PlayMode)
                                checkButton.Texture = SkinManager.Load(@"selection-latestranked", SkinSource.Osu);
                            break;
                    }
                }
            }
        }

        private void ClearScores()
        {
            Scores = null;

            spritesPersonalBest.ForEach(s =>
            {
                s.HandleInput = false;
                s.HoverEffect = null;
                s.Depth -= 0.01F;
                s.FadeOut(500);
                s.AlwaysDraw = false;
            });
            spritesPersonalBest.Clear();

            scoreList.ClearSprites();
        }

        private void checkBeatmapUpdates_OnClick(object sender, EventArgs e)
        {
            GameBase.MenuActive = true;

            bool allowReset;

            if (BeatmapManager.Current.Creator == ConfigManager.sUsername)
            {
                pDialog dialog = new pDialog(LocalisationManager.GetString(OsuString.SongSelection_WarningDestroyChanges), false);
                dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_DiscardLocalChanges), Color.OrangeRed, delegate { UpdateDifficulty(sender, e); });
                dialog.AddOption(LocalisationManager.GetString(OsuString.SongSelection_KeepLocalChanges), Color.Gray, null);
                GameBase.ShowDialog(dialog);
            }
            else UpdateDifficulty(sender, e);

            GameBase.MenuActive = false;
        }

        void UpdateDifficulty(object sender, EventArgs e)
        {
            pSprite s = (pSprite)sender;
            s.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0.2F, GameBase.Time, GameBase.Time + 400));
            if (s.Tag != null)
            {
                ((pSprite)s.Tag).Transformations[0] = ((pSprite)s.Tag).Transformations[0].CloneReverse();
                ((pSprite)s.Tag).Transformations[1] = ((pSprite)s.Tag).Transformations[1].CloneReverse();
            }
            s.HandleInput = false;

            Beatmap b = BeatmapManager.Current;

            try
            {
                if (!b.InOszContainer)
                {
                    //check it matches the downloadable copy
                    newBeatmapName = b.Filename + @"_";
                    FileNetRequest request = new FileNetRequest(string.Format(@"{0}/{1}_", b.ContainingFolder, b.Filename), Urls.PATH_MAPS + Path.GetFileName(b.Filename.Replace(@"#", @"%23")));
                    request.onFinish += delegate(string location, Exception e1)
                    {
                        if (e1 == null)
                        {
                            GameBase.Scheduler.Add(delegate
                            {
                                b.ReplaceWithUpdate();
                            });
                        }
                        else
                        {
                            try
                            {
                                File.Delete(newBeatmapName);
                            }
                            catch
                            {
                            }
                        }
                        DownloadFinished(e == null);
                    };
                    BeatmapManager.SongWatcher.EnableRaisingEvents = false;
                    NetManager.AddRequest(request);
                }
                else
                {
                    DataNetRequest request = new DataNetRequest(Urls.PATH_MAPS + Path.GetFileName(b.Filename.Replace(@"#", @"%23")));
                    BeatmapManager.SongWatcher.EnableRaisingEvents = false;
                    b.UpdateOsz2Container();

                    b.Osz2ExtractSafe(Beatmap.CopyColissionMode.Ask, true);
                    BeatmapManager.SongWatcher.EnableRaisingEvents = true;
                }
            }
            catch (Exception)
            {
            }
        }

        private void DownloadFinished(bool success)
        {
            GameBase.Scheduler.Add(delegate
            {
                if (success)
                    RefreshScores();
                else
                {
                    if (checkButton != null)
                    {
                        checkButton.HandleInput = true;
                        checkButton.FadeIn(100);
                        if (checkButton.Tag != null)
                            ((pSprite)checkButton.Tag).Transformations[0] = ((pSprite)checkButton.Tag).Transformations[0].CloneReverse();
                    }
                    NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.SongSelection_UpdateNotAvailable));
                }

                BeatmapManager.SongWatcher.EnableRaisingEvents = true;
            });
        }

        private void score_OnClick(object sender, EventArgs e)
        {
            if (MultiMode) return;
            bool rightButton = MouseManager.LastDownButton == MouseButtons.Right;
            AudioEngine.PlaySamplePositional(@"menuhit");
            Score s = (Score)((pSprite)sender).Tag;
            if (s != null && !s.isOnline && rightButton)
            {
                GameBase.ShowDialog(new ScoreDialog(s));
            }
            else
            {
                InputManager.ReplayScore = s;
                InputManager.ReplayMode = true;
                GameBase.ChangeMode(OsuModes.Rank);
            }
        }

        public override void Draw()
        {
            if (Exiting)
                return;

            //Flying stars on current beatmap.
            if (beatmapTreeManager.HandleInput && !Starting && beatmapTreeManager.HasSelectedBeatmap && beatmapTreeManager.SelectedBti.IsVisible && !beatmapTreeManager.IsRandomActive &&
                AudioEngine.Time != AudioEngine.TimeLastFrame && AudioEngine.BeatSyncing && AudioEngine.beatLength > 150 &&
                Math.Abs(AudioEngine.Time + 300 - AudioEngine.CurrentOffset) % AudioEngine.beatLength < Math.Min(100, AudioEngine.beatLength / 10))
            {
                if (star2 == null) star2 = SkinManager.Load(@"star2");

                for (int j = 0; j < 5; j++)
                {
                    pSprite p2 = new pSprite(star2, Fields.TopRight, Origins.Centre, Clocks.Game, Vector2.Zero, 0.111F, false, Color.White);
                    p2.Additive = true;
                    p2.Scale = 0.8F;
                    Transformation t = new Transformation(TransformationType.Scale, 1, 2, GameBase.Time, GameBase.Time + 1000);
                    t.Easing = EasingTypes.Out;
                    p2.Transformations.Add(t);
                    t = new Transformation(TransformationType.Fade, 1, 0, GameBase.Time, GameBase.Time + 1000);
                    t.Easing = EasingTypes.Out;
                    p2.Transformations.Add(t);
                    t = new Transformation(TransformationType.Rotation, (float)(GameBase.random.NextDouble() * 4 - 2), (float)(GameBase.random.NextDouble() * 4 - 2), GameBase.Time, GameBase.Time + 1000);
                    t.Easing = EasingTypes.Out;
                    p2.Transformations.Add(t);

                    Vector2 StartVector = new Vector2(-100, beatmapTreeManager.SelectedBti.Position.Y + beatmapTreeManager.ScrollPositionYOffset + GameBase.random.Next(-20, 20));
                    Vector2 EndVector = StartVector + new Vector2(GameBase.random.Next(140, 900), 0);
                    t = new Transformation(StartVector, EndVector, GameBase.Time, GameBase.Time + 1000);
                    t.Easing = EasingTypes.Out;
                    p2.Transformations.Add(t);

                    spriteManagerLow.Add(p2);
                }
            }

            int current = GameBase.Time % 1000;
            int length = 1000;
            if (BeatmapManager.Current != null && !beatmapTreeManager.IsRandomActive && AudioEngine.BeatSyncing && AudioEngine.Time - AudioEngine.CurrentOffset > 0)
            {
                current = (int)((AudioEngine.Time - AudioEngine.CurrentOffset) % AudioEngine.beatLength);
                length = (int)AudioEngine.beatLength;
            }

            s_Osu.Scale = OsuMathHelper.easeOutVal(current, 0.45F, 0.4F, length);
            s_Osu1.Alpha = OsuMathHelper.easeOutVal(current, 0.7F, 0, length);
            s_Osu1.Scale = OsuMathHelper.easeOutVal(current, 0.45F, 0.49F, length);

            base.Draw();

            spriteManagerBackground.Draw();
            spriteManagerLow.Draw();

            beatmapTreeManager.Draw();
            spriteManagerDraggedItems.Draw();

            scoreList.Draw();

            spriteManagerHigh.Draw();
        }

        public override void Initialize()
        {
            base.Initialize();

            StreamingManager.PurgeFrames(ReplayAction.SongSelect);

            if (!Exiting)
                GameBase.LoadComplete();

            ModManager.CheckForCurrentPlayMode();

            // We don't want to affect beatmapTreeManager.HandleInput here.
            bool didBeatmapTreeManagerHandleInput = beatmapTreeManager.HandleInput;
            HandleInput = true;
            beatmapTreeManager.HandleInput = didBeatmapTreeManagerHandleInput;

            BanchoClient.UpdateStatus(true);
        }
    }



    enum RankingType
    {
        Local,
        Top,
        SelectedMod,
        Friends,
        Country
    }
}
