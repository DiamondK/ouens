using System;
using System.Collections.Generic;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Xna.Framework;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Notifications;
using osu.Graphics.Sprites;
using osu.Online;
using osu_common.Helpers;
using Color = Microsoft.Xna.Framework.Graphics.Color;
using System.Threading;
using osu.Graphics.Skinning;
using osu.Graphics.UserInterface;
using osu.Properties;
using osu.Audio;
using osu.Helpers;
using System.Diagnostics;
using System.Text;

namespace osu.GameModes.Select
{
    internal class BeatmapImport : DrawableGameComponent
    {
        private static OsuModes CallbackMode = OsuModes.Unknown;
        internal static bool forceFullRefresh;
        private static pStatusDialog status;

        /// <summary>
        /// Check for new maps on entering song select.
        /// </summary>
        private static bool doNewBeatmapCheck = true;

        internal static bool DoNewBeatmapCheck
        {
            get { return doNewBeatmapCheck; }
        }

        internal static void SignalBeatmapCheck(bool newFilesOnly = false)
        {
            forceFullRefresh = !newFilesOnly;
            doNewBeatmapCheck = true;
        }

        public BeatmapImport(Game game, OsuModes callbackMode)
            : base(game)
        {
            CallbackMode = callbackMode;

            GameBase.FadeInComplete += GameBase_FadeInComplete;

            GameBase.fadeLevel = 0;
            GameBase.FadeState = FadeStates.Idle;
            GameBase.LoadComplete();

            status = new pStatusDialog(LocalisationManager.GetString(OsuString.BeatmapImport_CheckingForNewFiles));
            BeatmapManager.AssignStatusHandler(status.SetStatus);
        }

        protected override void Dispose(bool disposing)
        {
            if (status != null)
            {
                status.Dispose();
                status = null;
            }
            base.Dispose(disposing);
        }

        private static Thread processingThread;

        internal static bool IsProcessing { get { return processingThread != null && processingThread.IsAlive; } }

        void GameBase_FadeInComplete(object sender, EventArgs e)
        {
            GameBase.FadeInComplete -= GameBase_FadeInComplete;

            Start();
        }

        internal static void Start()
        {
            if (processingThread == null || !processingThread.IsAlive)
            {
                processingThread = new Thread(work);
                processingThread.IsBackground = true;
                processingThread.Start();
            }
        }

        public override void Draw()
        {
            if (status != null) status.Draw();
            base.Draw();
        }

        public override void Update()
        {
            base.Update();
        }

        private static void work()
        {
            int newFolderCount = -1;

            //Process any new files waiting to be imported.
            if (HandleNewFiles() || BeatmapManager.ChangedPackages.Count > 0 || BeatmapManager.ChangedFolders.Count > 0)
            {
                //We might need a full refresh even with new files added, if this is signalled.
                forceFullRefresh = false;
            }
            else
            {
                //Check if the number of directories in the songs dir has changed.
                //If it has, then we definitely need to do a full pass over available maps.
                newFolderCount = Directory.GetDirectories(BeatmapManager.SONGS_DIRECTORY).Length + Directory.GetFiles(BeatmapManager.SONGS_DIRECTORY).Length;

                if (BeatmapManager.FolderFileCount != newFolderCount)
                {
                    Debug.Print(@"Folder/Filecount changed from {0} to {1}", BeatmapManager.FolderFileCount, newFolderCount);
                    if (BeatmapManager.FolderFileCount == 0)
                    {
                        if (status != null) status.SetStatus(LocalisationManager.GetString(OsuString.BeatmapImport_TimeToPopulateAnEmptyDatabase));
                        promptOnFullProcess = false;
                    }
                    else if (BeatmapManager.FolderFileCount < newFolderCount)
                    {
                        if (status != null) status.SetStatus(LocalisationManager.GetString(OsuString.BeatmapImport_DetectedNewMapsAdded));
                    }
                    else
                    {
                        if (status != null) status.SetStatus(LocalisationManager.GetString(OsuString.BeatmapImport_DetectedMapsRemoved));
                    }

                    forceFullRefresh = true;
                }
            }

            if (status != null) BeatmapManager.AssignStatusHandler(status.SetStatus);

            //confirm this operation if the user has a lot of maps.
            if (forceFullRefresh && promptOnFullProcess
#if !DEBUG
                && BeatmapManager.Beatmaps.Count > 1000
#endif
)
            {
                bool response = false;

                GameBase.RunGraphicsThread(delegate
                {
                    pDialog pd = new pDialog(LocalisationManager.GetString(OsuString.BeatmapImport_FullProcess), false);
                    pd.Closed += delegate { promptOnFullProcess = false; };

                    pd.AddOption(LocalisationManager.GetString(OsuString.General_Confirm), Color.YellowGreen, delegate
                    {
                        response = true;
                        promptOnFullProcess = false;
                    });

                    pd.AddOption(LocalisationManager.GetString(OsuString.General_Cancel), Color.OrangeRed, delegate
                    {
                        response = false;
                        promptOnFullProcess = false;
                    });

                    GameBase.Scheduler.Add(delegate
                    {
                        GameBase.ShowDialog(pd);
                    });
                });

                while (promptOnFullProcess && GameBase.Mode == OsuModes.BeatmapImport)
                    Thread.Sleep(50);

                if (!response)
                {
                    GameBase.Scheduler.Add(Exit);
                    BeatmapManager.ClearStatusHandlers();
                    return;
                }
            }

            BeatmapManager.CheckAndProcess(forceFullRefresh, promptOnFullProcess);
            BeatmapManager.ClearStatusHandlers();

            if (newFolderCount >= 0)
                BeatmapManager.FolderFileCount = newFolderCount;

            GameBase.Scheduler.Add(Exit);
        }


        private static bool HandleNewFiles()
        {
            //todo: this method is going to become horribly inefficient with a lot of osz2 files being stored in this directory.

            //BeatmapManager.SongWatcher.EnableRaisingEvents = false;

            bool foundNew = false;

            string songsDir = BeatmapManager.SONGS_DIRECTORY;

            //todo: could cache the number of files present now
            foreach (string f in Directory.GetFiles(BeatmapManager.SONGS_DIRECTORY))
            {
                string ext = Path.GetExtension(f).ToLower();

                switch (ext)
                {
                    case @".db":
                        //Handle thumbs.db files
                        GeneralHelper.FileDelete(f);
                        break;
                    case @".mp3":
                    case @".osu":
                        {
                            string filename = Path.GetFileName(f);

                            if (status != null) status.SetStatus(string.Format(LocalisationManager.GetString(OsuString.BeatmapImport_Importing), (filename.Length > 50 ? filename.Remove(50) + @"..." : filename)));

                            BeatmapManager.SongWatcher.EnableRaisingEvents = false;

                            //name of .osu or .mp3 is used to identify containing folder
                            string newFolder = Path.Combine(songsDir, Path.GetFileNameWithoutExtension(f));
                            if (!Directory.Exists(newFolder))
                            {
                                Directory.CreateDirectory(newFolder);
                                BeatmapManager.ChangedFolders.Add(newFolder);
                                BeatmapManager.FolderFileCount++;
                            }

                            newFolder = Path.Combine(newFolder, filename);
                            if (!GeneralHelper.FileMove(f, newFolder, true, false))
                            {
                                try
                                {
                                    File.Copy(f, newFolder);
                                }
                                catch
                                {
                                    NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.BeatmapImport_FileInUse), filename));
                                }
                            }

                            BeatmapManager.SongWatcher.EnableRaisingEvents = true;
                            foundNew = true;
                        }
                        break;
                    case @".osz":
                    case @".zip":
                        {
                            //todo: instant osz2 conversion maybe?

                            string filename = Path.GetFileName(f);
                            string filenameNoExt = Path.GetFileNameWithoutExtension(f).Replace(@"-novid", string.Empty);

                            //Remove invalid characters
                            filenameNoExt = GeneralHelper.WindowsPathStrip(filenameNoExt);

                            if (status != null) status.SetStatus(string.Format(LocalisationManager.GetString(OsuString.BeatmapImport_Importing), (filename.Length > 50 ? filename.Remove(50) + @"..." : filename)));

                            short difficultyCharSpace = 36; //Most difficulty names with brackets + extension will fall within this value
                            int allowedPathLength = GeneralHelper.MAX_PATH_LENGTH - difficultyCharSpace;

                            string tempPath = Path.Combine(Path.GetTempPath(), @"osu!");
                            string tempFolder = Path.Combine(tempPath, @"_beatmaps");

                            GeneralHelper.FileDelete(tempFolder);

                            FastZip fz = new FastZip();
                            bool validBeatmap = false;
                            try
                            {
                                fz.ExtractZip(f, tempFolder, @".*");

                                if (fz.FailedCount > 0)
                                {
                                    StringBuilder errorMsg = new StringBuilder("Error extracting files: \n");
                                    for (int i = 0; i < fz.FailedFiles.Count; i++)
                                    {
                                        errorMsg.Append(fz.FailedFiles[i] + "\n");
                                    }
                                    NotificationManager.ShowMessage(errorMsg.ToString());

                                    validBeatmap = checkRequiredFiles(tempFolder);

                                    fz.ClearFailedData();
                                    if (!validBeatmap)
                                        throw (new Exception("Beatmap missing required files"));
                                }

                                int songsDirLength = Path.GetFullPath(songsDir).Length;
                                int highestDirLength = filenameNoExt.Length;
                                if (songsDirLength + (filenameNoExt.Length * 2) >= allowedPathLength)
                                {
                                    highestDirLength = (GeneralHelper.GetMaxPathLength(tempFolder) - tempFolder.Length);
                                }

                                int charsOver = songsDirLength + filenameNoExt.Length + highestDirLength - GeneralHelper.MAX_PATH_LENGTH;

                                if (charsOver > 0)
                                {
                                    if (charsOver < filenameNoExt.Length - 1)
                                    {
                                        filenameNoExt = filenameNoExt.Remove(filenameNoExt.Length - charsOver);
                                    }
                                    else
                                        throw new PathTooLongException();
                                }

                                //Force removal of readonly flag.
                                FileInfo myFile = new FileInfo(f);
                                if ((myFile.Attributes & FileAttributes.ReadOnly) > 0)
                                    myFile.Attributes &= ~FileAttributes.ReadOnly;

                                string dir = songsDir + filenameNoExt;

                                GeneralHelper.RecursiveMove(tempFolder, dir);
                                BeatmapManager.FolderFileCount++;
                                if (!BeatmapManager.ChangedFolders.Contains(dir))
                                    BeatmapManager.ChangedFolders.Add(dir);

                                GeneralHelper.FileDelete(f);
                            }
                            catch (Exception e)
                            {
                                if (e is PathTooLongException)
                                    NotificationManager.ShowMessage(string.Format("File path is too long. Consider renaming the file or containing directory. \nFile: {0}", Path.GetFileName(f)));
                                else
                                {
                                    if (fz.FailedCount > 0)
                                    {
                                        StringBuilder errorMsg = new StringBuilder("Error extracting files: \n");
                                        for (int i = 0; i < fz.FailedFiles.Count; i++)
                                        {
                                            errorMsg.Append(fz.FailedFiles[i] + "\n");
                                        }
                                        NotificationManager.ShowMessage(errorMsg.ToString());

                                        fz.ClearFailedData();
                                    }

                                    NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.BeatmapImport_FileCorrupt), filename));
                                    ErrorSubmission.Submit(new OsuError(e) { Feedback = @"file corrupt" });
                                }

                                try
                                {
                                    Directory.CreateDirectory(BeatmapManager.FAILED_DIRECTORY);
                                    GeneralHelper.FileMove(f, BeatmapManager.FAILED_DIRECTORY + filename);
                                }
                                catch { }

                                try
                                {
                                    Directory.Delete(tempFolder, true);
                                }
                                catch { }

                            }

                            foundNew = true;
                            break;
                        }
                }
            }

            return foundNew;
        }

        /// <summary>
        /// Checks that folder contains a song file and at least one map.
        /// </summary>
        /// <param name="beatmapPath"></param>
        /// <returns></returns>
        private static bool checkRequiredFiles(string beatmapPath)
        {
            if (!Directory.Exists(beatmapPath)) return false;

            bool hasMp3 = false;
            int mapCount = 0;
            foreach (string f in Directory.GetFiles(beatmapPath))
            {
                string ext = Path.GetExtension(f).Replace(@".", string.Empty).ToLower();

                switch (ext)
                {
                    case "osu":
                        mapCount++;
                        break;
                    case "ogg":
                    case "mp3":
                        hasMp3 = true;
                        break;
                    default:
                        break;
                }
            }
            return (hasMp3 && mapCount > 0);
        }

        private static void Exit()
        {
            BeatmapManager.OnlineFavouritesLoaded = false;
            forceFullRefresh = false;
            doNewBeatmapCheck = false;
            if (CallbackMode != OsuModes.Unknown)
                GameBase.ChangeModeInstant(CallbackMode);
            CallbackMode = OsuModes.Unknown;

            if (GameBase.Tournament && GameBase.TournamentManager)
                BeatmapManager.DatabaseSerialize();
        }

        static bool promptOnFullProcess;

        public static void ForceRun(bool promptOnFull)
        {
            if (GameBase.HasPendingDialog) return;
            CallbackMode = GameBase.Mode;
            promptOnFullProcess = promptOnFull;
            forceFullRefresh = true;
            GameBase.Scheduler.Add(delegate { GameBase.ChangeModeInstant(OsuModes.BeatmapImport, true); });
        }
    }
}