using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Graphics.Notifications;
using osu.Input;
using osu.Input.Handlers;
using osu.GameplayElements.Beatmaps;
using osu_common;
using osu_common.Helpers;
using osu.GameModes.Select.Drawable;

namespace osu.GameModes.Select
{
    class CollectionDialog : pDialog
    {
        static CollectionDialog instance;

        private static int elementOffsetx = 65;

        private pTextBox searchInput;

        private List<CollectionSprites> Collections;
        private readonly pScrollableArea channelButtonList;

        EventHandler closeAction;
        internal static CollectionSprites CurrentSelected;

        internal CollectionDialog(EventHandler onClose)
            : base(LocalisationManager.GetString(OsuString.CollectionDialog_Collection), true)
        {
            channelButtonList = new pScrollableArea(new Rectangle(0, 60, 550, 250), Vector2.Zero, false);

            instance = this;

            Closed += onClose;
            closeAction = onClose;

            currentVerticalSpace = 320;
            AddOption(LocalisationManager.GetString(OsuString.CollectionDialog_DeleteCollection), Color.Red, del_OnClick);
            AddOption(LocalisationManager.GetString(OsuString.General_Close), Color.Gray, null, true);

            Collections = new List<CollectionSprites>();
            foreach (KeyValuePair<string, List<string>> pair in CollectionManager.Collections)
                Collections.Add(new CollectionSprites(pair.Key));

            refresh();
        }

        private void refresh()
        {
            int i = 0;
            foreach (CollectionSprites c in Collections)
            {
                if (c.DrawAt(new Vector2(elementOffsetx + 30, i)))
                    channelButtonList.SpriteManager.Add(c.SpriteCollection);
                i += 17;
            }
            channelButtonList.SetContentDimensions(new Vector2(160, i));
        }

        internal override void Close(bool isCancel = false)
        {
            if (instance == this) instance = null;
            base.Close(isCancel);
        }

        public override void Update()
        {
            channelButtonList.Update();
            base.Update();
        }

        internal override void Display()
        {
            string hint = LocalisationManager.GetString(OsuString.Collection_EnterName);
            if (hint.Length > 68) hint = hint.Substring(0, 68) + @"...";
            searchInput = new pTextBox(hint, 15, new Vector2(elementOffsetx + 30 + GameBase.WindowOffsetXScaled, 35), 448, 0.94f);
            searchInput.OnCommit += searchInput_OnCommit;
            searchInput.OnChange += searchInput_OnChange;
            searchInput.OnGotFocus += (obj, ev) => { searchInput.Text = string.Empty; };
            spriteManager.Add(searchInput.SpriteCollection);

            base.Display();
        }

        void searchInput_OnCommit(pTextBox sender, bool newText)
        {
            if (searchInput.Text.Length <= 0 || !newText)
                return;

            string name = searchInput.Text;

            searchInput.Text = LocalisationManager.GetString(OsuString.Collection_EnterName);

            if (!CollectionManager.AddCollection(name))
                return;

            Collections.Add(new CollectionSprites(name));
            GameBase.Scheduler.Add(refresh);
        }

        void searchInput_OnChange(pTextBox sender, bool newText)
        {
            if (searchInput.Text.Length > CollectionSprites.TEXT_LENGTH_LIMIT && searchInput.Text != LocalisationManager.GetString(OsuString.Collection_EnterName))
                searchInput.Text = searchInput.Text.Substring(0, CollectionSprites.TEXT_LENGTH_LIMIT);
        }

        private void del_OnClick(object sender, EventArgs e)
        {
            if (CurrentSelected == null)
            {
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.CollectionDialog_SelectACollectionFirst), 1500);
                GameBase.ShowDialog(new CollectionDialog(closeAction));
                return;
            }

            Close();

            pDialog pd = new pDialogConfirmation(string.Format(LocalisationManager.GetString(OsuString.CollectionDialog_ConfirmDeleteCollection), CurrentSelected.Name), delegate
                {
                    CollectionManager.Remove(CurrentSelected.Name);
                    CurrentSelected = null;
                    GameBase.ShowDialog(new CollectionDialog(closeAction));
                }, delegate
                {
                    GameBase.ShowDialog(new CollectionDialog(closeAction));
                });
            GameBase.ShowDialog(pd);
        }

        internal override void Draw()
        {
            base.Draw();
            channelButtonList.Draw();
        }

        protected override void Dispose(bool disposing)
        {
            channelButtonList.Dispose();
            base.Dispose(disposing);
        }

        internal static void ChangeSelection(CollectionSprites col)
        {
            if (instance != null) instance.Collections.ForEach(c => { if (c != col) c.Deselect(); });
            CurrentSelected = col;
        }
    }
}