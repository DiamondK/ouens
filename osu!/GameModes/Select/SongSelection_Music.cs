﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.GameplayElements.Beatmaps;
using osu.Audio;
using System.Threading;
using osu.Graphics.Notifications;
using osu_common.Helpers;

namespace osu.GameModes.Select
{
    internal partial class SongSelection
    {
        private FadeStates audioState = FadeStates.Idle;
        private Thread audioThread;
        private Beatmap audioThreadLoadingMap;

        private void BeginAudioTransition(Beatmap beatmap, bool continuePlaying)
        {
            if (continuePlaying)
            {
                if (audioThread != null && audioThreadLoadingMap == BeatmapManager.Current && audioState != FadeStates.Idle)
                    return;

                AudioEngine.LoadAudioForPreview(BeatmapManager.Current, continuePlaying, true);
            }
            else
            {
                audioState = FadeStates.FadeOut;

                //if (audioThread != null)
                //    audioThread.Abort();

                //audioThreadLoadingMap = beatmap;

                //audioThread = GameBase.RunBackgroundThread(delegate
                //{
                //    try
                //    {
                //        while (AudioEngine.volumeMusicFade > 10)
                //        {
                //            audioState = FadeStates.FadeOut;
                //            Thread.Sleep(10);
                //        }
                //        audioState = FadeStates.Idle;
                //        AudioEngine.LoadAudioForPreview(BeatmapManager.Current, continuePlaying, true);
                //    }
                //    catch (Exception) {
                //        NotificationManager.ShowMessage("Error loading audio for this beatmap...");
                //        audioError = true;
                //    }

                //    audioThread = null;
                //});
            }
        }

        /// <summary>
        /// Ensure we always have an audio preview playing while on song selection.  Silence sucks, after all.
        /// </summary>
        private void UpdateMusic()
        {
            if (audioError) return;

            if (GameBase.SixtyFramesPerSecondFrame)
            {
                switch (audioState)
                {
                    case FadeStates.FadeOut:
                        AudioEngine.SetVolumeMusicFade(AudioEngine.volumeMusicFade - 8);

                        if (AudioEngine.volumeMusicFade <= 10)
                        {
                            try
                            {
                                AudioEngine.LoadAudioForPreview(BeatmapManager.Current, false, true);
                                audioState = FadeStates.Idle;
                            }
                            catch (Exception)
                            {
                                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.SongSelection_AudioError));
                                audioError = true;
                            }
                        }
                        break;
                }
            }


            try
            {
                if (AudioEngine.AudioState == AudioStates.Stopped && AudioEngine.AllowRandomSong && BeatmapManager.Beatmaps.Count > 0 && GameBase.FadeState == FadeStates.Idle)
                {
                    if (BeatmapManager.Current == null)
                        BeatmapManager.Current = BeatmapManager.Beatmaps[0];
                    AudioEngine.LoadAudioForPreview(BeatmapManager.Current, true, true);
                }
            }
            catch
            {
                audioError = true;
            }
        }

    }
}
