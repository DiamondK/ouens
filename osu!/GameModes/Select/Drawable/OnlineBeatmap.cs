﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Notifications;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Online;
using osu_common;
using osu_common.Helpers;
using osu_common.Libraries.NetLib;
using osu.Graphics;
using System.IO;
using osu.GameModes.Play;
using osu.Helpers;
using osu.Audio;

namespace osu.GameModes.Select.Drawable
{
    internal class OnlineBeatmap
    {
        private static int Width { get { return GameBase.WindowWidthScaled - OsuDirect.WIDTH - 30; } }
        internal readonly string artist;
        internal readonly string creator;

        private readonly DateTime lastupdate;
        private readonly float rating;
        internal readonly int setId;
        private readonly SubmissionStatus submissionStatus;
        internal readonly int threadid;
        internal readonly int postid;
        internal readonly string title;
        private pSprite bg;
        private bool downloading;
        internal bool exists;
        internal string serverFilename;
        internal string filename
        {
            get
            {
                if (serverFilename.Contains(".osz2"))
                    return GeneralHelper.WindowsFilenameStrip(setId + " " + artist + " - " + title + ".osz2");
                else
                    return GeneralHelper.WindowsFilenameStrip(setId + " " + artist + " - " + title + ".osz");
            }
        }
        internal bool hasVideo;
        private pText info;
        private pText info2;
        private pText info3;
        private pSprite levelBarBackground;
        private pSprite levelBarForeground;
        private OsuDirectDownload odd;
        private pSprite progress;
        private pSpriteDynamic thumbnail;
        internal List<pSprite> SpriteCollection;
        private pSprite statusIcon;
        internal int filesize_novideo;
        internal int filesize;
        private bool hasStoryboard;
        internal int beatmapId;
        internal string[] difficulties;

        internal bool HasAttachedDownload { get { return odd != null; } }

        internal OnlineBeatmap(string infoLine, LinkId type, string id)
        {
            string[] l = infoLine.Split('|');
            int i = 0;

            serverFilename = l[i++];
            artist = l[i++];
            title = l[i++];
            creator = l[i++];

            string status = l[i++];
            switch (status)
            {
                default:
                    submissionStatus = SubmissionStatus.Pending;
                    break;
                case "1":
                    submissionStatus = SubmissionStatus.Ranked;
                    break;
                case "2":
                    submissionStatus = SubmissionStatus.Approved;
                    break;
            }

            float.TryParse(l[i++], NumberStyles.Any, GameBase.nfi, out rating);
            try
            {
                lastupdate = Convert.ToDateTime(l[i++]);
            }
            catch
            {
                lastupdate = DateTime.Now;
            }

            setId = Int32.Parse(l[i++]);
            threadid = Int32.Parse(l[i++]);
            hasVideo = l[i++] == "1";
            hasStoryboard = l[i++] == "1";
            filesize = Int32.Parse(l[i++]);
            if (hasVideo && l[i].Length > 0)
                filesize_novideo = Int32.Parse(l[i++]);
            i = 13;
            if (i < l.Length)
            {
                difficulties = l[i++].Split(',');
            }

            if (type == LinkId.Post)
                postid = Int32.Parse(id);

            exists = BeatmapManager.GetBeatmapBySetId(setId) != null;

            if (!downloading)
            {
                OsuDirectDownload existingDownload = OsuDirect.ActiveDownloads.Find(d => d.filename == serverFilename);

                if (existingDownload != null)
                {
                    downloading = true;

                    odd = existingDownload;
                    odd.onStart += req_onStart;
                    odd.onFinish += req_onFinish;
                    odd.onUpdate += req_onUpdate;
                }
            }
        }

        internal OnlineBeatmap(string infoLine)
            : this(infoLine, LinkId.Set, string.Empty)
        {
        }

        internal static void FromId(LinkId idType, string id, EventHandler callback)
        {
            if ((BanchoClient.Permission & Permissions.Supporter) == 0)
                return;

            if (string.IsNullOrEmpty(id)) return;

            if (!GameBase.Tournament)
                NotificationManager.ShowMessageMassive("Looking up beatmap...", 1000);

            char idChar = idType.ToString().ToLower()[0];

            StringNetRequest req =
                new StringNetRequest(string.Format(Urls.DIRECT_SEARCH_SET, ConfigManager.sUsername, ConfigManager.sPassword, idChar, id));
            req.onFinish += delegate(string _result, Exception e)
                                {
                                    if (_result.Length > 0)
                                    {
                                        OnlineBeatmap rb = new OnlineBeatmap(_result, idType, id);
                                        if (idType == LinkId.Beatmap)
                                        {
                                            try
                                            {
                                                rb.beatmapId = Int32.Parse(id);
                                            }
                                            catch
                                            {
                                            }
                                        }
                                        callback(rb, null);
                                    }
                                    else
                                        callback(null, null);
                                };
            NetManager.AddRequest(req);
        }

        internal bool DrawAt(Vector2 pos)
        {
            bool firstPopulation = SpriteCollection == null;

            if (firstPopulation)
            {
                SpriteCollection = new List<pSprite>();

                bg = new pSprite(GameBase.WhitePixel, Fields.TopLeft,
                                 Origins.TopLeft, Clocks.Game, pos, 0.8f, true,
                                 exists ? new Color(134, 180, 255, 100) : new Color(255, 255, 255, 25));
                bg.VectorScale = new Vector2(Width, 28);

                bg.Scale = 1.6f;
                if (!exists)
                {
                    bg.OnHover += delegate
                    {
                        bg.FadeColour(ColourHelper.ChangeAlpha(bg.InitialColour, 50), 100);
                        AudioEngine.Click(null, @"click-short");
                    };
                    bg.OnHoverLost += delegate 
                    { 
                       if(!downloading)
                            bg.FadeColour(ColourHelper.ChangeAlpha(bg.InitialColour, 25), 100); 
                    };
                }
                bg.HandleInput = true;
                bg.ClickRequiresConfirmation = true;
                bg.OnClick += delegate
                {
                    AudioEngine.Click(null, @"click-short-confirm");
                    if (OsuDirect.RespondingBeatmap == this)
                    {
                        Download(false);
                    }
                    else
                    {
                        OsuDirect.QueueBeatmapForResponse(this);
                    }
                };

                progress = new pSprite(GameBase.WhitePixel, Fields.TopLeft,
                                       Origins.TopLeft, Clocks.Game, pos, 0.801f, true,
                                       new Color(163, 236, 85, 200));
                progress.VectorScale = new Vector2(0, 28);
                progress.Scale = 1.6f;
                SpriteCollection.Add(progress);

                thumbnail = new pSpriteDynamic(General.STATIC_WEB_ROOT_BEATMAP + @"/thumb/" + setId + @".jpg", @"Data\bt\" + setId + @".jpg", 500, pos, 0.8f);
                thumbnail.Scale = 39 / 60f;
                SpriteCollection.Add(thumbnail);
                thumbnail.OnTextureLoaded += delegate { thumbnail.FadeInFromZero(400); };

                SpriteCollection.Add(bg);

                string addition;
                switch (submissionStatus)
                {
                    case SubmissionStatus.Ranked:
                        addition = "ranked";
                        break;
                    case SubmissionStatus.Approved:
                        addition = "approved";
                        break;
                    default:
                        addition = "question";
                        break;
                }
                statusIcon =
                    new pSprite(SkinManager.Load(@"selection-" + addition, SkinSource.Osu), Fields.TopLeft,
                                Origins.Centre,
                                Clocks.Game, pos + new Vector2(Width - 12, 19), 0.9f, true, Color.TransparentWhite);
                statusIcon.Scale = 0.8f;
                SpriteCollection.Add(statusIcon);

                info = new pText(artist.Length > 0 ? artist + " - " + title : title, 12, pos + new Vector2(32, 0), 0.9f,
                                 true, Color.White);
                info.TextBold = true;
                SpriteCollection.Add(info);

                info2 = new pText(creator, 12, pos + new Vector2(Width, 0), 0.9f, true, Color.White);
                info2.Origin = Origins.TopRight;
                info2.TextBold = true;
                SpriteCollection.Add(info2);
                info3 = new pText("Last Updated: " + lastupdate, 10, pos + new Vector2(32, 11), 0.9f, true, Color.White);
                SpriteCollection.Add(info3);

                if (difficulties != null && difficulties.Length > 0)
                {
                    int posX = Width - 60;
                    string icon = "mode-taiko-small";
                    foreach (string diff in difficulties)
                    {
                        string[] arr = diff.Split('@');

                        if (arr.Length > 1)
                        {
                            switch (arr[1])
                            {
                                case "1":
                                    icon = "mode-taiko-small";
                                    break;
                                case "2":
                                    icon = "mode-fruits-small";
                                    break;
                                case "3":
                                    icon = "mode-mania-small";
                                    break;
                                default:
                                    icon = "mode-osu-small";
                                    break;
                            }
                        }
                        pSprite p = new pSprite(SkinManager.Load(icon, SkinSource.Osu), Fields.TopLeft, Origins.CentreLeft, Clocks.Game, pos + new Vector2(posX, 19), 0.81f, true, Color.White);
                        p.HandleInput = true;
                        p.ToolTip = arr[0];
                        p.Scale = 0.78f;
                        p.HoverEffect = new Transformation(Color.White, Color.Orange, 0, 200);
                        SpriteCollection.Add(p);
                        posX -= 20;
                        //too much
                        if (posX < Width - 70 - 20 * 12)
                            break;
                    }
                }

                if (submissionStatus == SubmissionStatus.Ranked || submissionStatus == SubmissionStatus.Approved)
                {
                    levelBarBackground = new pSprite(SkinManager.Load(@"levelbar", SkinSource.Osu),
                                                     pos + new Vector2(0, 23), 0.84f, true, Color.Black);
                    levelBarBackground.FlipHorizontal = true;
                    levelBarBackground.DrawWidth = 180;
                    SpriteCollection.Add(levelBarBackground);

                    Color col;
                    if (rating > 9)
                        col = new Color(250, 83, 38, 255);
                    else if (rating > 8)
                        col = new Color(250, 218, 38, 255);
                    else if (rating > 7)
                        col = new Color(38, 123, 250, 255);
                    else
                        col = new Color(178, 207, 222, 255);
                    levelBarForeground = new pSprite(SkinManager.Load(@"levelbar", SkinSource.Osu),
                                                     pos + new Vector2(0, 23), 0.85f, true, col);
                    levelBarForeground.DrawWidth = (int)(180 * ((rating - 4) / 6));
                    levelBarForeground.FlipHorizontal = true;
                    SpriteCollection.Add(levelBarForeground);

                    levelBarForeground.Transformations.Add(new Transformation(TransformationType.VectorScale,
                                                                              new Vector2(0, 1), new Vector2(1, 1),
                                                                              GameBase.Time,
                                                                              GameBase.Time + (int)(rating * 100),
                                                                              EasingTypes.Out));
                }

                foreach (pSprite p in SpriteCollection)
                    p.FadeInFromZero(300);
            }
            else
            {
                Vector2 orig = SpriteCollection[1].InitialPosition;

                foreach (pSprite p in SpriteCollection)
                {
                    Vector2 destination = pos + (p.InitialPosition - orig);
                    p.MoveTo(destination, Width, EasingTypes.Out);
                }
            }

            return firstPopulation;
        }

        internal void Download(bool noVideo = false)
        {
            if (GameBase.Tournament && hasVideo) noVideo = true;

            Logger.Log("Downloading " + title);

            if (odd != null)
            {
                odd.Abort(true);
                return;
            }

            if (String.IsNullOrEmpty(serverFilename))
            {
                NotificationManager.ShowMessageMassive("Can't download this file (please report this)", 2000);
                return;
            }

            if (exists)
            {
                pDialog dialog =
                    new pDialog(
                        "You already have a copy of this beatmap!  Are you sure you want to download it again?", true);
                dialog.AddOption("Yes, redownload this map!", Color.YellowGreen, delegate
                                                                                     {
                                                                                         exists = false;
                                                                                         Download();
                                                                                         exists = true;
                                                                                     });
                dialog.AddOption("No, forget it.", Color.OrangeRed, delegate { });
                GameBase.ShowDialog(dialog);
                return;
            }

            if (!OsuDirect.StartDownload(this, noVideo, (Odd) =>
            {
                this.odd = Odd;
                odd.onStart += req_onStart;
                odd.onFinish += req_onFinish;
                odd.onUpdate += req_onUpdate;
            }))
                return;

            downloading = true;

            if (SpriteCollection != null)
            {
                bg.FadeColour(Color.YellowGreen, Width);
                GameBase.Scheduler.AddDelayed(delegate
                {
                    bg.FadeColour(ColourHelper.ChangeAlpha(bg.InitialColour, 25), 800); //Fade back to original color. 
                }, Width);
                info.Text += " [Downloading]";
            }
        }

        /// <summary>
        /// Aborts active download and resets sprites.
        /// </summary>
        private void ResetDownload()
        {
            if (SpriteCollection != null)
            {
                bg.FadeColour(exists ? new Color(134, 180, 255, 100) : new Color(255, 255, 255, 25), Width);
                progress.VectorScale = new Vector2(0 * Width, 28);
                info.Text = info.Text.Replace(" [Downloading]", "");
            }
            downloading = false;
            odd = null;
        }

        private void req_onUpdate(double progr)
        {
            float prog = (float)progr;
            if (SpriteCollection != null)
                progress.VectorScale = new Vector2(prog * Width, 28);
        }

        private void req_onFinish(bool succeeded)
        {
            if (succeeded)
            {
                NotificationManager.ShowMessage("Finished downloading " + title, Color.White, 3000);
                if (OnDownloadFinished != null)
                    OnDownloadFinished(this, null);
            }
            else
            {
                //Aborted
                ResetDownload();
            }
        }

        internal event EventHandler OnDownloadFinished;

        private void req_onStart()
        {
            if (GameBase.Mode != OsuModes.Play)
                NotificationManager.ShowMessageMassive("Downloading " + title, 1000);
        }

        internal void Dispose()
        {
            if (SpriteCollection == null) return;
            SpriteCollection.ForEach(s =>
                                         {
                                             s.FadeOut(Width);
                                             s.MoveToRelative(new Vector2(50, 0), Width);
                                             s.AlwaysDraw = false;
                                         });
            thumbnail.Dispose();
        }

        internal void Dim()
        {
            if (SpriteCollection == null) return;

            thumbnail.Dispose();

            SpriteCollection.ForEach(s => s.FadeOut(1000));
        }
    }
}