using System.Drawing;
using Microsoft.Xna.Framework;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Notifications;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu_common;
using System;
using System.Collections.Generic;
using System.Text;
using osu_common.Helpers;
using Color = Microsoft.Xna.Framework.Graphics.Color;

namespace osu.GameModes.Select.Drawable
{
    internal class CollectionSprites
    {
        private const int BUTTON_HEIGHT = 11;
        internal const int TEXT_LENGTH_LIMIT = 25;
        private Vector2 OptionsOffset = new Vector2(300, 2);

        internal static Color colourNotPresent = new Color(150, 150, 150);
        internal static Color colourPresent = new Color(255, 255, 255);

        private pTextBox title;
        private pText renameButton;
        private pText addButton;
        private pText addSetButton;
        private pText removeButton;

        private bool _renaming;
        private bool renaming
        {
            get
            {
                return _renaming;
            }
            set
            {
                _renaming = value;
                UpdateState();
            }
        }

        private string _name;
        /// <summary>
        /// We will use this field if the length of the new name
        /// is zero.
        /// </summary>
        private string originalName;

        internal List<pSprite> SpriteCollection;
        internal int Count;
        internal bool Selected;

        internal string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                if (originalName == null)
                    originalName = Name;
            }
        }

        bool containsCurrentMap;

        private Vector2 pos;
        
        internal CollectionSprites(string name)
        {
            Name = name;
        }

        internal bool DrawAt(Vector2 position)
        {
            pos = position;

            bool firstPopulation = SpriteCollection == null;
            if (firstPopulation)
                EnsureInitialized();
            else
            {
                Vector2 orig = SpriteCollection[0].InitialPosition;

                foreach (pSprite p in SpriteCollection)
                {
                    Vector2 destination = pos + (p.InitialPosition - orig);
                    p.MoveTo(destination, 500, EasingTypes.Out);
                }
            }

            UpdateState();

            return firstPopulation;
        }

        private void EnsureInitialized()
        {
            InitializeSpriteCollection();
            InitializeTitle();
            InitializeButtons();
        }

        #region Init:SpriteCollection

        private void InitializeSpriteCollection()
        {
            if (SpriteCollection != null)
                return;

            SpriteCollection = new List<pSprite>();

            List<string> list = CollectionManager.FindCollection(Name);
            if (list == null)
                return;

            int total = list.Count;
            int delete = 0;
            for (int i = total - 1; i >= 0; i--)
            {
                //todo: this shouldn't be done in a sprite/view class.
                Beatmap map = BeatmapManager.GetBeatmapByChecksum(list[i]);
                if (map == null)
                {
                    CollectionManager.RemoveFromCollection(Name, list[i]); //map doesn't exist
                    delete++;
                }
            }
            Count = total - delete;
        }

        #endregion

        #region Init:Title

        private void InitializeTitle()
        {
            if (title != null)
                return;

            title = new pTextBox(string.Empty, 12, pos, 448, 0.95f);
            title.Box.InitialColour = new Color(200, 200, 200);
            title.ReadOnly = true;
            title.Height = BUTTON_HEIGHT + 2 * OptionsOffset.Y;
            title.BorderFocused = Color.Yellow;
            title.Box.BackgroundColour = Color.Black;
            title.Box.BorderWidth = 1;

            title.Box.OnClick += Title_OnClick;
            title.OnChange += Title_OnChange;
            title.OnCommit += Title_OnCommit;
            title.Box.OnHover += delegate
            {
                if (!title.Box.IsVisible || title.Box.Alpha != 1)
                    return;

                title.Box.InitialColour = Color.White;
            };

            title.Box.OnHoverLost += delegate
            {
                if (!title.Box.IsVisible || title.Box.Alpha != 1)
                    return;

                title.Box.InitialColour = new Color(200, 200, 200);
            };
            SpriteCollection.AddRange(title.SpriteCollection);
        }

        private void Title_OnChange(pTextBox sender, bool newText)
        {
            if (renaming)
            {
                //Initial trim
                if (Name.Length > TEXT_LENGTH_LIMIT)
                {
                    Name = Name.Substring(0, TEXT_LENGTH_LIMIT);
                    title.Text = Name.Substring(0, TEXT_LENGTH_LIMIT);
                }
                //Trim any additional characters
                if (title.Text.Length > TEXT_LENGTH_LIMIT)
                    title.Text = title.Text.Substring(0, TEXT_LENGTH_LIMIT);
            }
        }

        private void Title_OnCommit(pTextBox sender, bool newText)
        {
            if (renaming)
            {
                //Don't allow zero length
                if (title.Text.Length == 0)
                {
                    Name = originalName.Length > TEXT_LENGTH_LIMIT 
                        ? originalName.Substring(0, TEXT_LENGTH_LIMIT) 
                        : originalName;
                    ChangeCollectionName(originalName, Name);
                    originalName = Name;
                    title.Text = Name;
                }
                else
                {
                    ChangeCollectionName(originalName, title.Text);
                    originalName = title.Text;
                    Name = title.Text;
                }
                renaming = false;
                UpdateState();
                CollectionDialog.ChangeSelection(this);
            }
        }

        private void Title_OnClick(object sender, EventArgs e)
        {
            Selected = true;
            UpdateState();
            CollectionDialog.ChangeSelection(this);
        }

        #endregion

        #region Init:Buttons

        private void InitializeButtons()
        {

            if (renameButton == null)
            {
                renameButton = new pText(LocalisationManager.GetString(OsuString.General_Rename), 10, pos + OptionsOffset, 0.96f, true, Color.White);

                renameButton.HandleInput = true;
                renameButton.OnClick += rename_OnClick;
                renameButton.TextAlignment = TextAlignment.Centre;
                renameButton.TextBounds = new Vector2(35, 11);
                renameButton.InitialColour = colourPresent;
                renameButton.BackgroundColour = Color.Orange;
                renameButton.BorderWidth = 2;
                renameButton.BorderColour = Color.Orange;
                SpriteCollection.Add(renameButton);

                //We want the rename button to appear as a separate group
                //So we use +10 offset instead of +5
                OptionsOffset += new Vector2(10 + renameButton.TextBounds.X, 0);
            }

            if (addSetButton == null)
            {
                addSetButton = new pText(@"+Set", 10, pos + OptionsOffset, 0.96f, true, Color.White);

                addSetButton.HandleInput = true;
                addSetButton.OnClick += addTo_OnClick;
                addSetButton.ToolTip = LocalisationManager.GetString(OsuString.CollectionSprites_AddSetToCollection);
                addSetButton.TextAlignment = TextAlignment.Centre;
                addSetButton.TextBounds = new Vector2(30, 11);
                addSetButton.BackgroundColour = Color.Green;
                addSetButton.BorderWidth = 2;
                addSetButton.BorderColour = Color.YellowGreen;

                SpriteCollection.Add(addSetButton);

                OptionsOffset += new Vector2(5 + addSetButton.TextBounds.X, 0);
            }
            if (addButton == null)
            {
                addButton = new pText(@"+", 10, pos + OptionsOffset, 0.96f, true, Color.White);

                addButton.HandleInput = true;
                addButton.OnClick += addTo_OnClick;
                addButton.ToolTip = LocalisationManager.GetString(OsuString.CollectionSprites_AddToCollection);
                addButton.TextAlignment = TextAlignment.Centre;
                addButton.TextBounds = new Vector2(20, 11);
                addButton.BackgroundColour = Color.Green;
                addButton.BorderWidth = 2;
                addButton.BorderColour = Color.YellowGreen;

                SpriteCollection.Add(addButton);

                OptionsOffset += new Vector2(5 + addButton.TextBounds.X, 0);
            }
            if (removeButton == null)
            {
                removeButton = new pText(@"-", 10, pos + OptionsOffset, 0.96f, true, Color.White);

                removeButton.HandleInput = true;
                removeButton.OnClick += delFrom_OnClick;
                removeButton.ToolTip = LocalisationManager.GetString(OsuString.CollectionSprites_RemoveFromCollection);
                removeButton.TextAlignment = TextAlignment.Centre;
                removeButton.TextBounds = new Vector2(20, 11);
                removeButton.BackgroundColour = Color.OrangeRed;
                removeButton.BorderWidth = 2;
                removeButton.BorderColour = Color.DarkOrange;

                SpriteCollection.Add(removeButton);
            }
        }

        private void rename_OnClick(object sender, EventArgs e)
        {
            renaming = !renaming;
        }
        #endregion

        private void ChangeCollectionName(string collection, string newName)
        { 

            List<string> oldCollectionMaps = CollectionManager.FindCollection(collection);
            CollectionManager.Remove(collection);
            CollectionManager.AddCollection(newName);
            if (oldCollectionMaps != null)
            {
                for (int i = 0; i < oldCollectionMaps.Count; i++)
                    CollectionManager.AddToCollection(newName, oldCollectionMaps[i]);
            }
        }

        internal void UpdateState()
        {
            if (!renaming)
            {
                title.UnFocus(false);
                title.ReadOnly = true;

                try
                {
                    containsCurrentMap = CollectionManager.FindCollection(originalName).Contains(BeatmapManager.Current.BeatmapChecksum);                    
                }
                catch { return; }

                title.Box.BorderColour = containsCurrentMap
                    ? Color.YellowGreen
                    : Color.White;

                string shortName = Name;
                if (Name.Length > 40)
                    shortName = Name.Substring(0, 40) + @"...";

                title.Text = string.Format(@"{0} ({1})", shortName, Count);

                if (Selected != title.Box.TextBold)
                {
                    title.Box.TextBold = Selected;
                    title.Box.TextChanged = true;
                }

                addButton.InitialColour = addSetButton.InitialColour = containsCurrentMap 
                    ? colourNotPresent 
                    : colourPresent;
                removeButton.InitialColour = containsCurrentMap 
                    ? colourPresent 
                    : colourNotPresent;
            }
            else
            {
                title.Box.BorderColour = Color.Yellow;
                title.Text = Name;

                title.ReadOnly = false;
                title.Focus(false);
            }
        }

        internal void Deselect()
        {
            Selected = false;
            renaming = false;
        }

        private void delFrom_OnClick(object sender, EventArgs e)
        {
            if (renaming)
                renaming = false;
            performAddDel(true, true);
        }

        private void addTo_OnClick(object sender, EventArgs e)
        {
            if (renaming)
                renaming = false;
            performAddDel(false, sender == addSetButton);
        }

        private void performAddDel(bool isDel, bool addCompleteSet = false)
        {
            bool hasChange = false;
            if (addCompleteSet)
            {
                List<Beatmap> maps = BeatmapManager.Beatmaps.FindAll(b => b.ContainingFolder == BeatmapManager.Current.ContainingFolder);
                foreach (Beatmap map in maps)
                {
                    if (isDel)
                    {
                        if (CollectionManager.RemoveFromCollection(Name, map.BeatmapChecksum))
                        {
                            Count--;
                            hasChange = true;
                        }
                    }
                    else
                    {
                        if (CollectionManager.AddToCollection(Name, map.BeatmapChecksum))
                        {
                            Count++;
                            hasChange = true;
                        }

                    }
                }
            }
            else
            {
                if (isDel)
                {
                    if (CollectionManager.RemoveFromCollection(Name, BeatmapManager.Current.BeatmapChecksum))
                    {
                        Count--;
                        hasChange = true;
                    }
                }
                else
                {
                    if (CollectionManager.AddToCollection(Name, BeatmapManager.Current.BeatmapChecksum))
                    {
                        Count++;
                        hasChange = true;
                    }

                }
            }

            if (!hasChange)
                return;

            NotificationManager.ShowMessageMassive(string.Format(isDel ? LocalisationManager.GetString(OsuString.CollectionSprites_Removed) : LocalisationManager.GetString(OsuString.CollectionSprites_Added),
                addCompleteSet ? BeatmapManager.Current.SortTitle : BeatmapManager.Current.DisplayTitleFull, Name), 1500);

            UpdateState();
        }
    }
}
