using Microsoft.Xna.Framework.Graphics;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics.UserInterface;
using System;
using System.Collections.Generic;
using System.Text;
using osu_common.Helpers;

namespace osu.GameModes.Select
{
    internal class ScoreDialog : pDialog
    {
        private Score score;
        internal ScoreDialog(Score s)
            : base(LocalisationManager.GetString(OsuString.ScoreDialog_ScoreManagement), false)
        {
            score = s;
            AddOption(LocalisationManager.GetString(OsuString.ScoreDialog_DeleteScore), Color.YellowGreen, (obj, ev) =>
            {
                ScoreManager.RemoveScore(BeatmapManager.Current.BeatmapChecksum, score.scoreChecksum);
                if (GameBase.RunningGameMode is SongSelection)
                {
                    SongSelection ss = (SongSelection)GameBase.RunningGameMode;
                    ss.ShowScores();
                }
            });
            AddOption(LocalisationManager.GetString(OsuString.ScoreDialog_ExportReplay), Color.LightBlue, (obj, ev) =>
            {
                score.LoadLocalData();
                ScoreManager.ExportReplay(score);
            });
            AddOption(LocalisationManager.GetString(OsuString.General_Close), Color.Gray, null);
        }
    }
}
