﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using osu.Audio;
using osu.Configuration;
using osu.Graphics;
using osu.Graphics.Primitives;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu_common.Libraries.NetLib;

namespace osu.GameModes.Select
{
    internal class Charts : pGameMode
    {
        pText t_currentCategory;
        pText t_categorySelection;

        Color c_background = new Color(34, 34, 32);

        pSearchBox searchBox;

        pScrollableArea sa_chartList;
        SpriteManager spriteManagerTop = new SpriteManager(true);

        internal const float PADDING = 27.5f;
        private List<Chart> charts;

        public override void Initialize()
        {
            //background
            baseSpriteManager.Add(new pSprite(GameBase.WhitePixel, Fields.Native, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0, true, c_background)
            {
                VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight)
            });

            //header
            pText textCharts = new pText("Charts" + @" /", 28, new Vector2(PADDING, 60), 1, true, Color.White) { Origin = Origins.BottomLeft};
            baseSpriteManager.Add(textCharts);

            t_currentCategory = new pText("All Charts", 20, new Vector2(120f, 58), 1, true, new Color(254, 220, 97)) { Origin = Origins.BottomLeft};
            baseSpriteManager.Add(t_currentCategory);

            searchBox = new pSearchBox(18, new Vector2(PADDING, 33), 20, Graphics.Renderers.TextAlignment.Right);
            searchBox.OnChange += searchBox_OnChange;
            baseSpriteManager.Add(searchBox.SpriteCollection);

            baseSpriteManager.Add(new pBox(new Vector2(PADDING, 62), new Vector2(GameBase.WindowWidthScaled - (PADDING * 2), 1), 1, Color.White));

            t_categorySelection = new pText("All  /  Monthly  /  Themed  /  Special", 14, new Vector2(PADDING, 70), 1, true, Color.White) { Origin = Origins.TopLeft };
            baseSpriteManager.Add(t_categorySelection);

            //list

            sa_chartList = new pScrollableArea(new RectangleF(PADDING / 2, 90, GameBase.WindowWidthScaled - PADDING, GameBase.WindowHeightScaled - PADDING - 90), Vector2.Zero, true);

            StringNetRequest req = new StringNetRequest("http://osu.ppy.sh/web/osu-getcharts.php?u={0}&h={1}", ConfigManager.sUsername, ConfigManager.sPassword);
            req.onFinish += delegate(string json, Exception e)
            {
                charts = JsonConvert.DeserializeObject<List<Chart>>(json);

                DoLayout();
            };

            NetManager.AddRequest(req);

            //footer
            BackButton back = new BackButton(delegate
            {
                AudioEngine.PlaySamplePositional(@"menuback");
                GameBase.ChangeMode(OsuModes.Menu);
            });
            spriteManagerTop.Add(back.SpriteCollection);

            base.Initialize();
        }

        private void DoLayout()
        {
            int i = 0;
            foreach (Chart c in charts)
            {
                if (c.Visible)
                {
                    c.spriteManager.ViewOffset.Y = -i * (Chart.HEIGHT + 10);
                    i++;
                }

                if (!sa_chartList.Children.Contains(c.spriteManager))
                    sa_chartList.AddChild(c.spriteManager);
            }

            sa_chartList.SetContentDimensions(new Vector2(0, (Chart.HEIGHT + 10) * i));
        }

        void searchBox_OnChange(pTextBox sender, bool newText)
        {
            if (charts == null) return;

            charts.ForEach(c => {
                c.Visible = searchBox.IsDefault || c.Search(sender.Text);
            });
            DoLayout();
        }

        public override void Draw()
        {
            base.Draw();
            sa_chartList.Draw();

            spriteManagerTop.Draw();
        }

        public override void Update()
        {
            base.Update();
            sa_chartList.Update();
        }

        protected override void Dispose(bool disposing)
        {
            sa_chartList.Dispose();
            searchBox.Dispose();
            spriteManagerTop.Dispose();
            base.Dispose(disposing);
        }
    }

    public interface pSearchable
    {
        bool Search(string term);
    }

    internal class ChartBeatmap
    {
        public int beatmapset_id;
        public bool passed;
    }

    internal class Chart : IDisposable, pSearchable
    {
        internal SpriteManager spriteManager = new SpriteManager(true) { Masking = true };
        public List<ChartBeatmap> beatmaps = new List<ChartBeatmap>();

        internal const float HEIGHT = 80;
        internal const float PADDING = Charts.PADDING / 2;

        public int chart_id;
        public string acronym;
        public string name;
        public DateTime start_date;
        public DateTime end_date;
        public string type;

        internal Chart()
        {
        }

        internal bool Visible
        {
            get { return spriteManager.Alpha > 0; }
            set
            {
                spriteManager.Alpha = value ? 1 : 0;
            }
        }

        [OnDeserialized]
        internal void Initialize(StreamingContext context)
        {
            string description = string.Format("{0} Chart, {1} Beatmaps", new CultureInfo("en-US", false).TextInfo.ToTitleCase(type), beatmaps.Count);

            string leader = "No leader yet!\nYou are not ranked on this chart!";
            bool active = true;

            spriteManager.Add(new pText(name, 20, new Vector2(PADDING * 2 + HEIGHT, 0), 1, true, Color.White));
            spriteManager.Add(new pText(description, 12, new Vector2(PADDING * 2 + HEIGHT, 20), 1, true, new Color(144, 238, 254)));
            spriteManager.Add(new pText(leader, 14, new Vector2(PADDING * 2 + HEIGHT, HEIGHT), 1, true, Color.White) { Origin = Origins.BottomLeft });

            //chart image
            pSpriteDynamic background = new pSpriteDynamic(@"http://s.ppy.sh/a/3103765_1378920280.png", null, 0, new Vector2(PADDING, 0), 0.2f);
            background.MaxDimension = HEIGHT * 1.6f;
            spriteManager.Add(background);

            for (int i = 0; i < beatmaps.Count; i++)
            {
                const int thumb_size = 40;
                float xPos = PADDING * 2 + i * (thumb_size + 2);

                pSpriteDynamic b = new pSpriteDynamic(General.STATIC_WEB_ROOT_BEATMAP + @"/thumb/" + beatmaps[i].beatmapset_id + @".jpg", null, 0, new Vector2(xPos, PADDING), 0.2f);
                b.MaxDimension = thumb_size * 1.6f;
                b.Field = Fields.TopRight;
                b.Origin = Origins.TopRight;
                spriteManager.Add(b);

                if (!beatmaps[i].passed)
                    spriteManager.Add(new pTextAwesome(FontAwesome.circle_o, 14, new Vector2(xPos + thumb_size / 2, thumb_size + 5 + PADDING)) { InitialColour = new Color(253, 202, 55), Field = Fields.TopRight });
                else
                    spriteManager.Add(new pTextAwesome(FontAwesome.check_circle_o, 14, new Vector2(xPos + thumb_size / 2, thumb_size + 5)) { InitialColour = new Color(172, 219, 39), Field = Fields.TopRight });
            }

            //active light
            if (end_date >= DateTime.Now)
                spriteManager.Add(new pBox(new Vector2(0, 0), new Vector2(10, HEIGHT), 0.1f, new Color(172, 219, 39)));

            pBox colourBackground = new pBox(new Vector2(PADDING, 0), new Vector2(GameBase.WindowWidthScaled - Charts.PADDING * 2, HEIGHT), 1, new Color(50, 50, 50))
            {
                Alpha = 0.01f,
                Additive = true,
                HandleInput = true,
                ClickRequiresConfirmation = true
            };
            colourBackground.OnClick += delegate { };
            colourBackground.OnHover += delegate
            {
                colourBackground.Alpha = 1;
                colourBackground.FadeTo(0.6f, 500, EasingTypes.In);
            };
            colourBackground.OnHoverLost += delegate
            {
                colourBackground.FadeTo(0.01f, 500, EasingTypes.In);
            };

            spriteManager.Add(colourBackground);


            //separator (move out?)
            spriteManager.Add(new pBox(new Vector2(PADDING, HEIGHT + 5), new Vector2(GameBase.WindowWidthScaled - Charts.PADDING * 2, 1), 0, new Color(110, 96, 103)));
        }

        internal void Show()
        {
            spriteManager.Alpha = 1;
        }

        internal void Hide()
        {
            spriteManager.Alpha = 0;
        }

        public void Dispose()
        {
            spriteManager.Dispose();
        }

        public bool Search(string term)
        {
            return name.IndexOf(term, StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                acronym.IndexOf(term, StringComparison.CurrentCultureIgnoreCase) >= 0;
        }
    }
}