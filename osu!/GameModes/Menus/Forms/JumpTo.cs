﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.UserInterface;
using osu.Graphics.Sprites;
using osu.Input.Handlers;
using osu.Input;
using osu_common.Helpers;
using osu.Audio;


namespace osu.GameModes.Menus.Forms
{
    internal partial class JumpTo : pDialog
    {
        internal const int PADDING = 10;
        const int MAX_ITEMS = 250;

        internal List<Beatmap> maps = new List<Beatmap>();
        private List<JumpToItem> items = new List<JumpToItem>();

        pDropdown collections;

        private pText collectionLabel;
        private readonly pScrollableArea scrollableView;
        private pSearchBox searchBox;
        private pButton buttonClose;
        private SpriteManager mapManager;
        private SpriteManager topLayer;
        private int width;
        private int searchTime;

        public bool SongConfirmed;
        public Beatmap SelectedMap;

        public JumpTo()
            : base(LocalisationManager.GetString(OsuString.JumpToDialog_Title), true)
        {
            spriteManager.Add(new pBox(new Vector2(PADDING, 79), new Vector2(GameBase.WindowWidthScaled - (PADDING * 2), 1), 1, Color.White));
            spriteManager.Add(new pBox(new Vector2(PADDING, 440), new Vector2(GameBase.WindowWidthScaled - (PADDING * 2), 1), 1, Color.White));
            Rectangle scrollableArea = new Rectangle(PADDING, 80, (int)(640 * GameBase.WidthWidescreenRatio) - (PADDING * 2), 360);
            scrollableView = new pScrollableArea(scrollableArea, Vector2.Zero, true);
            mapManager = new SpriteManager(true);
            topLayer = new SpriteManager(true);
            mapManager.SetWidescreen(GameBase.Widescreen);
            width = GameBase.WindowWidthScaled;
            collectionLabel = new pText(LocalisationManager.GetString(OsuString.SongSelection_Collections) + ":", 20, new Vector2(PADDING, 30), 2, true, new Color(254, 220, 97));
            collections = new pDropdown(topLayer, "All", new Vector2(PADDING, 55), 140, 3);
            searchBox = new pSearchBox(20, new Vector2(PADDING, 50), 20, Graphics.Renderers.TextAlignment.Right);
            searchTime = -1;
            spriteManager.Add(searchBox.SpriteCollection);
            spriteManager.Add(collectionLabel);
            scrollableView.AddChild(mapManager);

            searchBox.OnChange += delegate(pTextBox sender, bool newtext)
            {
                if (newtext)
                {
                    if (searchBox.IsDefault && searchBox.textBeforeCommit.Length == 1)
                        searchTime = GameBase.Time;
                    else if (!searchBox.IsDefault)
                        searchTime = GameBase.Time + 300;
                }
            };
            this.Closed += delegate 
            { 
                MusicControl.ClearPool();
            };

            collections.AddOption("All", "All");
            collections.SetSelected("All", true);
            foreach (string c in CollectionManager.Collections.Keys)
                collections.AddOption(c, c);
            collections.OnSelect += collections_OnSelect;

            SelectedMap = BeatmapManager.Current;

            Layout();
        }

        internal override void Draw()
        {
            base.Draw();

            scrollableView.Draw();
            topLayer.Draw();
            MusicControl.Draw();
        }

        public override void Update()
        {
            updateSelected();
            base.Update();
            scrollableView.Update();

            MusicControl.SpriteManager.Alpha = 1;

            if (!searchBox.HasFocus || !searchBox.InputControl.AcceptText)
                searchBox.Focus(true);

            if (searchTime > 0 && searchTime < GameBase.Time)
            {
                searchTime = -1;
                Layout();
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            searchBox.Dispose();
            scrollableView.Dispose();
        }

        private void Layout()
        {
            int i = 0;
            MusicControl.ClearPool();
            runFiltering();
            mapManager.Clear();
            items.Clear();
            foreach (Beatmap m in maps)
            {
                i++;
                if (i >= MAX_ITEMS)
                    break;

                JumpToItem mapItem = new JumpToItem(m, this, m.Title, m.Artist, i - 1);

                mapItem.colourBackground.OnClick += colourBackground_OnClick;

                items.Add(mapItem);
                mapManager.Add(mapItem.mapText);
                mapManager.Add(mapItem.colourBackground);
                MusicControl.AddPoolItem(m);
            }

            int area = i >= MAX_ITEMS ? i - 1 : i;
            scrollableView.SetContentDimensions(new Vector2(640 * GameBase.WidthWidescreenRatio, area * JumpToItem.ITEM_HEIGHT));
            updateSelected(false, true);
        }

        private void runFiltering()
        {
            maps.Clear();

            string query = searchBox.Text;
            
            List<Beatmap> filteredMaps = null;
            if (collections.SelectedObject != @"All")
                //filter by selected collection
                filteredMaps = BeatmapManager.Beatmaps.FindAll(t => CollectionManager.Collections[collections.SelectedString].Contains(t.BeatmapChecksum));
            else
                filteredMaps = new List<Beatmap>(BeatmapManager.Beatmaps);

            if (!string.IsNullOrEmpty(query))
                //further filter by entered search query
                filteredMaps = filteredMaps.FindAll(t => t.SortTitle.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) >= 0);

            if (filteredMaps.Count == 0) return;

            filteredMaps.Sort();

            maps.Add(filteredMaps[0]);
            for (int i = 1; i < filteredMaps.Count; i++)
                if (filteredMaps[i].ContainingFolder != filteredMaps[i - 1].ContainingFolder)
                    maps.Add(filteredMaps[i]);

            maps.Sort((a, b) => { return b.DateLastPlayed.CompareTo(a.DateLastPlayed); });
        }

        private void collections_OnSelect(object sender, EventArgs e)
        {
            Layout();
        }

        private void colourBackground_OnClick(object sender, EventArgs e)
        {
            if (SongConfirmed)
                Close();
        }

        internal void updateSelected(bool userForced = false, bool firstUpdate = false)
        {
            if (BeatmapManager.Current != SelectedMap || userForced || firstUpdate)
            {
                if (!userForced)
                    SelectedMap = BeatmapManager.Current;
                else
                    BeatmapManager.Current = SelectedMap;

                foreach (JumpToItem m in items)
                {
                    if (SelectedMap.RelativeContainingFolder == m.map.RelativeContainingFolder)
                    {
                        m.colourBackground.FadeTo(0.6f, 100);
                        m.colourBackground.FadeColour(Color.CornflowerBlue, 100);
                        if (!userForced)
                            if (items.Count <= 12)
                                scrollableView.SetContentDimensions(new Vector2(0, 0));
                            else if ((items.IndexOf(m) * JumpToItem.ITEM_HEIGHT) >= (scrollableView.ContentDimensions.Y - (12 * JumpToItem.ITEM_HEIGHT)))
                                scrollableView.SetScrollPosition(new Vector2(0, scrollableView.ContentDimensions.Y - (12 * JumpToItem.ITEM_HEIGHT)));
                            else
                                scrollableView.SetScrollPosition(new Vector2(0, JumpToItem.ITEM_HEIGHT * items.IndexOf(m)));
                    }
                    else
                    {
                        if (m.colourBackground.Alpha != 0.001f)
                        {
                            m.colourBackground.FadeTo(0.001f, 20);
                            m.colourBackground.FadeColour(Color.White, 20);
                        }
                    }
                }
            }

            if (userForced && !SongConfirmed)
            {
                MusicControl.ChooseRandomSong(false);
                MusicControl.PushHistory(BeatmapManager.Current);
            }
        }

        private void chooseSongRelative(bool forward)
        {
            foreach (JumpToItem m in items)
            {
                if (SelectedMap.RelativeContainingFolder == m.map.RelativeContainingFolder)
                {
                    if ((forward && items.IndexOf(m) >= items.Count - 1) || (!forward && items.IndexOf(m) <= 0))
                            break;
                    int index = forward ? items.IndexOf(m) + 1 : items.IndexOf(m) - 1;
                    int position = forward ? 10 : 1;
                    SelectedMap = maps[index];
                    updateSelected(true);
                    if ((forward && scrollableView.ScrollPosition.Y + JumpToItem.ITEM_HEIGHT * 10 <= items.IndexOf(m) * JumpToItem.ITEM_HEIGHT) || (!forward && scrollableView.ScrollPosition.Y >= items.IndexOf(m) * JumpToItem.ITEM_HEIGHT))
                        scrollableView.SetScrollPosition(new Vector2(0, 30 * (items.IndexOf(m) - position)));
                    break;
                }
            }
        }

        internal override bool HandleKey(Keys k)
        {
            bool handled = GameBase.MusicControl.KeyboardHandler_OnKeyPressed(null, k);

            if (handled) return true;

            switch(k)
            {
                case Keys.Down:
                    chooseSongRelative(true);
                    return true;

                case Keys.Up:
                    chooseSongRelative(false);
                    return true;
            }
            return base.HandleKey(k);
        }
    }

    internal partial class JumpToItem
    {
        internal const int ITEM_HEIGHT = 30;

        internal pText mapText;
        internal pText confirmText;
        internal pBox colourBackground;
        internal Beatmap map;
        private JumpTo set;

        internal JumpToItem(Beatmap thisMap, JumpTo global, string title, string artist, int index)
        {
            set = global;
            map = thisMap;
            mapText = new pText(thisMap.SortTitleAuto, 20, new Vector2(0, index * ITEM_HEIGHT + 5), 2, true, Color.White);

            colourBackground = new pBox(new Vector2(0, index * ITEM_HEIGHT), new Vector2((640 * GameBase.WidthWidescreenRatio) - JumpTo.PADDING * 3, ITEM_HEIGHT), 1, new Color(50, 50, 50))
            {
                Alpha = 0.01f,
                Additive = true,
                HandleInput = true,
                ClickRequiresConfirmation = true,
            };

            colourBackground.OnHover += colourBackground_OnHover;
            colourBackground.OnHoverLost += colourBackground_OnHoverLost;
            colourBackground.OnClick += colourBackground_OnClick;
        }

        private void colourBackground_OnHover(object sender, EventArgs e)
        {
            colourBackground.FadeTo(0.6f, 100);
        }

        private void colourBackground_OnHoverLost(object sender, EventArgs e)
        {
            colourBackground.FadeTo(0.001f, 20);
        }

        private void colourBackground_OnClick(object sender, EventArgs e)
        {
            if (set.SelectedMap.ContainingFolder == map.ContainingFolder)
                set.SongConfirmed = true;

            set.SelectedMap = map;
            set.updateSelected(true);
        }
    }
}
