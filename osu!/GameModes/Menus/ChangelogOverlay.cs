﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Helpers;
using osu_common.Helpers;
using osu_common.Libraries.NetLib;

namespace osu.GameModes.Menus
{
    class ChangelogOverlay : DrawableGameComponent
    {
        int Width = 300;
        int Height = 150;
        int startHeight = 16;

        pScrollableArea area;

        public ChangelogOverlay(Game game, bool fullscreen = false)
            : base(game)
        {
            if (fullscreen)
            {
                Width = GameBase.WindowWidthScaled;
                Height = 226;
                startHeight = 0;
            }

            area = new pScrollableArea(new Rectangle(0, GameBase.WindowHeightScaled - Height, Width, Height), Vector2.Zero, true);
            area.HeaderHeight = startHeight;

            int year = General.VERSION / 10000;
            int month = General.VERSION / 100 - year * 100;
            int day = General.VERSION - year * 10000 - month * 100;

            int version_dated = Int32.Parse(new DateTime(year, month, day).AddDays(-14).ToString("yyyyMMdd"));
            int version_current = Int32.Parse(new DateTime(year, month, day).ToString("yyyyMMdd"));

            StringNetRequest snr = new StringNetRequest("http://osu.ppy.sh/p/changelog?updater=3" + (!string.IsNullOrEmpty(General.SUBVERSION) ? "&test=1" : "&current=" + version_current + "&from=" + version_dated));
            snr.onFinish += snr_onFinish;
            NetManager.AddRequest(snr);

            pSprite bg = new pSprite(GameBase.WhitePixel, Vector2.Zero, 0.5f, true, Color.Black);
            bg.Scale = 1.6f;
            bg.ViewOffsetImmune = true;
            bg.VectorScale = new Vector2(Width, Height);
            bg.Alpha = 0.5f;

            area.SpriteManager.Add(bg);

            if (!fullscreen)
            {
                pText t = new pText(LocalisationManager.GetString(OsuString.Update_RecentChanges), 10, new Vector2(3, 3), Vector2.Zero, 0.9992f, true, Color.White, false);
                t.ViewOffsetImmune = true;
                t.TextBold = true;

                area.SpriteManager.Add(t);

                bg = new pSprite(GameBase.WhitePixel, Vector2.Zero, 0.9991f, true, SkinManager.NEW_SKIN_COLOUR_SECONDARY);
                bg.Scale = 1.6f;
                bg.ViewOffsetImmune = true;
                bg.VectorScale = new Vector2(Width, startHeight);
                bg.Alpha = 0.8f;
                bg.HandleInput = true;

                bg.HoverEffect = new Transformation(Color.DarkBlue, Color.LightBlue, 0, 50);

                bg.OnClick += delegate
                {
                    area.SpriteManager.SpriteList.ForEach(s => s.FadeOut(100));
                    GameBase.Scheduler.AddDelayed(delegate { area.Hide(); }, 100);
                };

                area.SpriteManager.Add(bg);
            }
        }

        protected override void Dispose(bool disposing)
        {
            area.Dispose();
            base.Dispose(disposing);
        }

        public override void Draw()
        {
            area.Draw();
            base.Draw();
        }

        public override void Update()
        {
            area.Update();
            base.Update();
        }


        void snr_onFinish(string _result, Exception e)
        {
            GameBase.Scheduler.Add(delegate
            {
                float currentHeight = startHeight;
                foreach (string line in _result.Split('\n'))
                {
                    string[] spl = line.Split('\t');

                    Color col = Color.White;
                    string type = string.Empty;
                    switch (spl[0])
                    {
                        default:
                            col = Color.OrangeRed;
                            type = LocalisationManager.GetString(OsuString.Update_Misc);
                            break;
                        case "+":
                            col = Color.YellowGreen;
                            type = LocalisationManager.GetString(OsuString.Update_Added);
                            break;
                        case "*":
                            col = Color.Orange;
                            type = LocalisationManager.GetString(OsuString.Update_Fixed);
                            break;
                    }

                    pText t = null;

                    if (spl.Length < 2)
                    {
                        currentHeight += 4;

                        t = new pText(line, 10, new Vector2(Width / 2, currentHeight), new Vector2(0, 0), 0.9f, true, Color.White, false);
                        t.TextAlignment = TextAlignment.Centre;
                        t.Origin = Origins.TopCentre;
                        t.TextBold = true;
                    }
                    else
                    {
                        t = new pText(type, 8, new Vector2(4, currentHeight), new Vector2(30, 0), 0.9f, true, Color.White, false);
                        t.BorderColour = ColourHelper.Lighten2(col, 0.2f);
                        t.BackgroundColour = col;
                        t.TextBorder = true;
                        t.TextAlignment = TextAlignment.Centre;
                        area.SpriteManager.Add(t);

                        t = new pText(spl[2], 10, new Vector2(40, currentHeight), new Vector2(Width - 30, 0), 0.9f, true, Color.White, false);
                    }

                    t.TextBorder = true;
                    t.HandleInput = true;

                    area.SpriteManager.Add(t);

                    currentHeight += t.MeasureText().Y + 2;
                }


                pText footer = new pText(LocalisationManager.GetString(OsuString.Update_ViewChangelog), 10, new Vector2(Width / 2, currentHeight), new Vector2(Width, 0), 0.9f, true, Color.LightGoldenrodYellow, false);

                footer.TextBorder = true;
                footer.HandleInput = true;
                footer.TextAlignment = TextAlignment.Centre;
                footer.Origin = Origins.TopCentre;
                footer.TextBold = true;

                footer.OnHover += delegate { footer.InitialColour = Color.White; };
                footer.OnHoverLost += delegate { footer.InitialColour = Color.LightGoldenrodYellow; };
                footer.OnClick += delegate { GameBase.ProcessStart("http://osu.ppy.sh/p/changelog"); };

                area.SpriteManager.Add(footer);
                currentHeight += 1.5f * footer.MeasureText().Y;


                area.SetContentDimensions(new Vector2(Width, currentHeight));
            });
        }
    }
}
