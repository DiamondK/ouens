﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Audio;
using Un4seen.Bass;

namespace osu.GameModes.Menus
{
    class Visualisation : DrawableGameComponent
    {
        protected const int sample_size = 1024;
        protected const float decay = 0.95f;
        internal float epicness = 1;

        internal AudioTrack CustomDataSource;

        internal float[] data = new float[sample_size];
        internal float intensity = 0;

        public Visualisation()
            : base(GameBase.Game)
        {
            Initialize();
        }

        public override void Draw()
        {
            base.Draw();
        }

        public override void Update()
        {
            if (CustomDataSource != null || (AudioEngine.AudioTrack != null && !AudioEngine.Paused && !AudioEngine.ExtendedTime))
            {
                try
                {
                    Bass.BASS_ChannelGetData(CustomDataSource != null ? CustomDataSource.audioStream : AudioEngine.AudioTrack.audioStream, data, (int)BASSData.BASS_DATA_FFT2048);
                }
                catch { }

                if (epicness != 1)
                {
                    for (int i = 0; i < sample_size; i++)
                    {
                        data[i] *= epicness;
                    }
                }
            }
            else
            {
                float decayFactor = (float)Math.Pow(decay, GameBase.FrameRatio);

                for (int i = 0; i < sample_size; i++)
                {
                    data[i] *= decayFactor;
                }
            }

            intensity = 0;
            for (int i = 0; i < sample_size; ++i)
            {
                intensity += data[i];
            }

            base.Update();
        }
    }
}
