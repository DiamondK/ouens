﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;
using osu_common;
using Un4seen.Bass;

namespace osu.GameModes.Menus
{
    class SnowVisualisation : Visualisation
    {
        SpriteManager spriteManager = new SpriteManager(true) { HandleInput = false };

        private pTexture flakeTexture;
        private pSprite flake;

        const int res = 1;
        int piles_count = GameBase.WindowWidthScaled / res + 1;

        float[] piles;

        public override void Initialize()
        {
            piles = new float[piles_count];

            base.Initialize();

            PhysicsManager p = new PhysicsManager();

            flakeTexture = SkinManager.Load(@"menu-snow");

            if (flakeTexture == null)
            {
                switch (Player.Mode)
                {
                    case PlayModes.CatchTheBeat:
                        flakeTexture = SkinManager.Load(@"mode-fruits-small");
                        break;
                    case PlayModes.Taiko:
                        flakeTexture = SkinManager.Load(@"mode-taiko-small");
                        break;
                    case PlayModes.OsuMania:
                        flakeTexture = SkinManager.Load(@"mode-mania-small");
                        break;
                    default:
                        flakeTexture = SkinManager.Load(@"mode-osu-small");
                        break;
                }
            }

            flake = new pSprite(flakeTexture, Origins.Centre, Vector2.Zero, 1, true, Color.White);
            flake.Origin = Origins.Centre;
            flake.Additive = true;
            flake.AlwaysDraw = true;
        }

        protected override void Dispose(bool disposing)
        {
            spriteManager.Dispose();
            base.Dispose(disposing);
        }

        public override void Draw()
        {
            spriteManager.Draw();
            base.Draw();
        }

        Vector2 mouseLast;

        float currentAlpha = 0;
        public override void Update()
        {
            if (GameBase.SixtyFramesPerSecondFrame)
            {
                int word = 0;
                try
                {
                    word = Bass.BASS_ChannelGetLevel(AudioEngine.AudioTrack.audioStream);
                }
                catch
                {
                    //can fault if audio device is not attached.
                }

                int left = Utils.LowWord32(word);
                int right = Utils.HighWord32(word);

                currentAlpha = AudioEngine.KiaiEnabled ? 0.5f : Math.Min(0.5f, Math.Max(0.1f, (left + right - 30000) / 35536f)) * 0.4f;

                if (spriteManager.SpriteList.Count > 0)
                {
                    Vector2 mouseNow = InputManager.CursorPosition / GameBase.WindowRatio;
                    if (mouseLast != Vector2.Zero && mouseLast != mouseNow)
                    {
                        spriteManager.SpriteList.ForEach(s =>
                        {
                            if (s.AlwaysDraw)
                                s.TagNumeric += (int)((mouseNow.X - mouseLast.X) * 10);
                        });
                    }
                    mouseLast = mouseNow;

                    for (int i = 0; i < piles_count; i++)
                    {
                        if (piles[i] > 0 && piles[i] < GameBase.WindowHeightScaled)
                            piles[i] += 0.05f;
                    }
                }
            }

            if (ConfigManager.sMenuSnow.Value && GameBase.SixtyFramesPerSecondFrame && GameBase.random.NextDouble() > 1 - currentAlpha)
            {
                pSprite newFlake = flake.Clone();
                newFlake.Position = new Vector2(GameBase.WindowWidthScaled * (float)GameBase.random.NextDouble(), -50);
                newFlake.Alpha = (float)GameBase.random.NextDouble() * 0.7f;
                newFlake.Scale = 0.6f - (float)GameBase.random.NextDouble() * 0.5f;
                newFlake.TagNumeric = (int)(((float)GameBase.random.NextDouble() - 0.5f) * 1000);
                newFlake.Tag = (int)(((float)GameBase.random.NextDouble() - 0.5f) * 4);
                spriteManager.Add(newFlake);
            }

            spriteManager.SpriteList.ForEach(s =>
            {
                pSprite sp = s as pSprite;
                if (!s.AlwaysDraw) return;
                
                bool inRange = s.Position.X >= 0 && s.Position.X < GameBase.WindowWidthScaled;

                int pileIndex = (int)Math.Min(piles_count - 1, Math.Max(0, s.Position.X / res));

                float pileHeight = inRange ? piles[pileIndex] : 0;

                float bottom = s.Position.Y + s.Scale * (sp.DrawHeight / 1.6f / 2);

                if (bottom >= GameBase.WindowHeightScaled || (pileHeight > 0 && pileHeight <= bottom))
                {
                    s.Tag = 0;
                    s.TagNumeric = 0;
                    s.AlwaysDraw = false;
                    s.FadeOut(60000);

                    if (!inRange)
                        return;

                    piles[pileIndex] = s.Position.Y + s.Scale * (sp.DrawHeight / 1.6f / 4);
                }

                if (s.TagNumeric != 0 || (int)s.Tag != 0)
                {
                    s.TagNumeric += (int)s.Tag;
                    s.Position.X += s.TagNumeric / 5000f * (float)GameBase.FrameRatio;
                    s.Position.Y += 1 * (float)GameBase.FrameRatio;
                    s.Rotation += s.TagNumeric / 10000f * (float)GameBase.FrameRatio * 0.4f;
                }
            });

            base.Update();
        }

    }
}
