﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;

namespace osu.GameModes.Menus
{
    class MenuVisualisation : Visualisation
    {
        SpriteManager spriteManager = new SpriteManager(true) { HandleInput = false };
        List<pSprite> sprites = new List<pSprite>();

        float radius;
        internal float Radius
        {
            get
            {
                return radius;
            }

            set
            {
                if (value == radius) return;
                radius = value;

                for (int i = 0; i < sample_size; i++)
                {
                    float angle = (OsuMathHelper.Pi * 2) * (0.4f + ((float)i / sample_size * (Overshoot * width)));
                    pSprite p = sprites[i];
                    p.Position = Position + new Vector2(Radius * (float)Math.Cos(angle), Radius * (float)Math.Sin(angle));
                    p.Rotation = angle;
                }

            }
        }
        internal Vector2 Position;
        internal float Alpha = 1;
        internal float MaxAlpha = 0.4f;
        internal float Overshoot = 8;

        const int max_velocity = 4;
        const float width = 1f;

        static Color default_colour = new Color(0, 78, 155);

        public override void Initialize()
        {
            base.Initialize();

            for (int i = 0; i < sample_size; i++)
            {
                Vector2 pos = new Vector2(0, width * i);
                pSprite p = new pSprite(SkinManager.Load(@"menu-vis", SkinSource.Osu), Fields.TopLeft, Origins.CentreLeft, Clocks.Game, pos)
                {
                    Alpha = 0.2f,
                    Additive = true,
                    VectorScale = new Vector2(0, width),
                    AlwaysDraw = true,
                    InitialColour = default_colour
                };

                spriteManager.Add(p);
                sprites.Add(p);
            }
        }

        protected override void Dispose(bool disposing)
        {
            spriteManager.Dispose();
            base.Dispose(disposing);
        }

        public override void Draw()
        {
            spriteManager.Draw();
            base.Draw();
        }

        const float cutoff = 0.1f;
        float velocityAdjustment = 1;

        public override void Update()
        {
            base.Update();

            if (GameBase.Tournament)
                velocityAdjustment = 0.5f;
            else
                velocityAdjustment = velocityAdjustment * 0.97f + 0.03f * (float)Math.Max(0, 1 - Math.Max(0, (Math.Pow((GameBase.ElapsedMilliseconds / GameBase.SIXTY_FRAME_TIME), 4) - 1)));

            for (int i = 0; i < sample_size; i++)
            {
                pSprite s = sprites[i];
                s.Alpha = Math.Max(0, Alpha * MaxAlpha * Math.Min(1, (s.VectorScale.X - cutoff) / (cutoff * 2)));
                setBar(s, data[i] * velocityAdjustment);
            }
        }

        internal void SetColour(Color? c = null)
        {
            if (!c.HasValue) c = default_colour;
            sprites.ForEach(s => s.FadeColour(c.Value, 1000));
        }

        void setBar(pSprite s, float velocity)
        {
            float newValue = velocity * max_velocity;
            if (newValue > s.VectorScale.X)
                s.VectorScale.X = velocity * max_velocity;
            else
                s.VectorScale.X *= (float)Math.Pow(decay, GameBase.FrameRatio);
        }
    }
}
