﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Menus.Forms;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Notifications;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;

namespace osu.GameModes.Menus
{
    internal class MusicControl : GameComponent
    {
        private static readonly Stack<string> playFuture = new Stack<string>();
        private static readonly Stack<string> playHistory = new Stack<string>();
        private static List<Beatmap> playPool = new List<Beatmap>();
        internal static readonly SpriteManager SpriteManager = new SpriteManager(true);
        private readonly pSprite scrollbarBackground;
        private readonly pSprite scrollbarForeground;
        private bool manualPause;

        internal MusicControl(Game game)
            : base(game)
        {
            SpriteManager.HandleFirstDraw();
            SpriteManager.HandleOverlayInput = true;

            Transformation t = new Transformation(TransformationType.Scale, 1, 1.2F, 0, 100);
            t.Easing = EasingTypes.Out;

            Transformation t2 = new Transformation(TransformationType.Scale, 1.1F, 1.2F, 0, 100);
            t2.Easing = EasingTypes.Out;

#if !ARCADE

            const int button_height = 11;
            const int button_spacing = 20;

            int x = 136;

            pTextAwesome p = new pTextAwesome(FontAwesome.step_backward, 14, new Vector2(x, button_height)) { Field = Fields.TopRight, Tag = "b" };
            p.HandleInput = true;
            p.ToolTip = "Previous track";
            p.ClickEffect = t2;
            p.OnHover += p_OnHover;
            p.HoverEffect = t;
            p.OnClick += p_prev;
            SpriteManager.Add(p);

            x -= button_spacing;

            p = new pTextAwesome(FontAwesome.play, 14, new Vector2(x, button_height)) { Field = Fields.TopRight, Tag = "b" };
            p.HandleInput = true;
            p.ToolTip = "Play";
            p.ClickEffect = t2;
            p.OnHover += p_OnHover;
            p.HoverEffect = t;
            p.OnClick += p_play;
            SpriteManager.Add(p);

            x -= button_spacing;

            p = new pTextAwesome(FontAwesome.pause, 14, new Vector2(x, button_height)) { Field = Fields.TopRight, Tag = "b" };
            p.HandleInput = true;
            p.ToolTip = "Pause";
            p.ClickEffect = t2;
            p.OnHover += p_OnHover;
            p.HoverEffect = t;
            p.OnClick += p_pause;
            SpriteManager.Add(p);

            x -= button_spacing;

            p = new pTextAwesome(FontAwesome.stop, 14, new Vector2(x, button_height)) { Field = Fields.TopRight, Tag = "b" };
            p.HandleInput = true;
            p.ToolTip = "Stop the music!";
            p.ClickEffect = t2;
            p.OnHover += p_OnHover;
            p.HoverEffect = t;
            p.OnClick += p_stop;
            SpriteManager.Add(p);

            x -= button_spacing;

            p = new pTextAwesome(FontAwesome.step_forward, 14, new Vector2(x, button_height)) { Field = Fields.TopRight, Tag = "b" };
            p.HandleInput = true;
            p.ToolTip = "Next track";
            p.ClickEffect = t2;
            p.HoverEffect = t;
            p.OnHover += p_OnHover;
            p.OnClick += p_next;
            SpriteManager.Add(p);

            x -= button_spacing;

            p = new pTextAwesome(FontAwesome.info, 14, new Vector2(x, button_height)) { Field = Fields.TopRight, Tag = "b" };
            p.HandleInput = true;
            p.ToolTip = "View song info";
            p.ClickEffect = t2;
            p.OnHover += p_OnHover;
            p.HoverEffect = t;
            p.OnClick += p_info;
            SpriteManager.Add(p);

            x -= button_spacing;

            p = new pTextAwesome(FontAwesome.bars, 14, new Vector2(x, button_height)) { Field = Fields.TopRight, Tag = "b" };
            p.HandleInput = true;
            p.ToolTip = "Jump To window";
            p.ClickEffect = t2;
            p.OnHover += p_OnHover;
            p.HoverEffect = t;
            p.OnClick += p_jump;
            SpriteManager.Add(p);

            scrollbarBackground = new pSprite(GameBase.WhitePixel, Fields.TopRight, Origins.TopLeft,
                                              Clocks.Game,
                                              new Vector2(143, 20), 0.96f, true,
                                              new Color(20, 20, 20, 128), "b");
            scrollbarBackground.Scale = 1.6f;
            scrollbarBackground.VectorScale = new Vector2(134, 3);
            scrollbarBackground.HoverEffect = new Transformation(new Color(20, 20, 20, 128), new Color(60, 60, 60, 128),
                                                                 0, 100);
            scrollbarBackground.HandleInput = true;
            scrollbarBackground.ToolTip = "Click to seek to a specific point in the song.";
            scrollbarBackground.OnClick += scrollbarBackground_OnClick;
            scrollbarBackground.Additive = true;

            SpriteManager.Add(scrollbarBackground);

            scrollbarForeground = new pSprite(GameBase.WhitePixel, Fields.TopRight, Origins.TopLeft,
                                              Clocks.Game,
                                              new Vector2(143, 20), 0.97f, true,
                                              new Color(255, 255, 255, 128), "b");
            scrollbarForeground.Scale = 1.6f;
            scrollbarForeground.VectorScale = new Vector2(0, 3);
            scrollbarForeground.Additive = true;

            SpriteManager.Add(scrollbarForeground);

#endif

            KeyboardHandler.OnKeyPressed += KeyboardHandler_OnKeyPressed;
        }

        void p_OnHover(object sender, EventArgs e)
        {
            AudioEngine.Click(null, @"click-short");
        }

        internal static bool ShowControls
        {
            get { return SpriteManager.Alpha > 0; }
            set
            {
                int newAlpha = value ? 1 : 0;
                if (newAlpha == SpriteManager.Alpha) return;
                SpriteManager.Alpha = newAlpha;
                SongInfo(false);
            }
        }

        internal static void Draw()
        {
            if (GameBase.Tournament) return;

            SpriteManager.Draw();
        }

        public override void Update()
        {
            if (!ShowControls) return;

            if (!manualPause && ShowControls)
                StartRandomSongIfSilent();

#if !ARCADE
            if (AudioEngine.AudioLength > 0)
                scrollbarForeground.VectorScale =
                    new Vector2(Math.Max(0, (float)AudioEngine.Time / AudioEngine.AudioLength) * 134f, 3);
#endif

            base.Update();
        }

        internal bool KeyboardHandler_OnKeyPressed(object sender, Keys k)
        {
            if (!ShowControls || KeyboardHandler.ShiftPressed || KeyboardHandler.ControlPressed || KeyboardHandler.AltPressed) return false;

            if (GameBase.Mode == OsuModes.Menu)
            {
                switch (k)
                {
                    case Keys.R:
                        ChooseRandomSong(true);
                        return true;
                    case Keys.J:
                        p_jump(null, null);
                        return true;
                    case Keys.Left:
                    case Keys.Z:
                        p_prev(null, null);
                        return true;
                    case Keys.X:
                        p_play(null, null);
                        return true;
                    case Keys.C:
                        p_pause(null, null);
                        return true;
                    case Keys.Right:
                    case Keys.V:
                        p_next(null, null);
                        return true;
                }
            }

            switch (k)
            {
                case Keys.F1:
                    p_prev(null, null);
                    return true;
                case Keys.F2:
                    p_play(null, null);
                    return true;
                case Keys.F3:
                    p_pause(null, null);
                    return true;
                case Keys.F4:
                    if (!KeyboardHandler.AltPressed)
                    {
                        p_stop(null, null);
                        return true;
                    }
                    break;
                case Keys.F5:
                    p_next(null, null);
                    return true;
                case Keys.F6:
                    p_jump(null, null);
                    return true;
            }

            return false;
        }

        private void scrollbarBackground_OnClick(object sender, EventArgs e)
        {
            if (!ShowControls) return;

            SpriteManager.ResetClick();

            AudioEngine.SeekTo(
                (int)
                (Math.Min(1,
                          ((InputManager.CursorPosition.X - scrollbarBackground.drawPosition.X) /
                           GameBase.WindowRatio) / 134) * AudioEngine.AudioLength));
        }

        private void p_info(object sender, EventArgs e)
        {
            if (!ShowControls) return;

            SongInfo(true);
            NotificationManager.ShowMessageMassive(
                "Song Info will be " + (ConfigManager.sPermanentSongInfo ? "permanently displayed." : "temporarily displayed."), 1000);
        }

        private void p_next(object sender, EventArgs e)
        {
            if (!ShowControls) return;

            AudioEngine.Click(null, @"click-short-confirm");

            NotificationManager.ShowMessageMassive(">> Next", 1000);
            if (playFuture.Count > 0)
            {
                playHistory.Push(BeatmapManager.Current.BeatmapChecksum);
                BeatmapManager.Current = BeatmapManager.GetBeatmapByChecksum(playFuture.Pop());
                ChooseRandomSong(false);
            }
            else
                ChooseRandomSong(true);
            manualPause = false;
        }

        private void p_stop(object sender, EventArgs e)
        {
            if (!ShowControls) return;

            NotificationManager.ShowMessageMassive("Stop Playing", 1000);
            AudioEngine.Click(null, @"click-short-confirm");

            AudioEngine.Stop();
            manualPause = true;
        }

        private void p_jump(object sender, EventArgs e)
        {
            if (!ShowControls) return;

            if (GameBase.ActiveDialog is JumpTo)
            {
                GameBase.ActiveDialog.Close();
                return;
            }

            AudioEngine.Click(null, @"click-short-confirm");

            if (GameBase.Options.PoppedOut)
                GameBase.Options.PoppedOut = false;

            JumpTo dialog = new JumpTo();

            dialog.Closed += delegate
            {
                if (dialog.SelectedMap != null)
                    manualPause = false;
            };

            GameBase.ShowDialog(dialog);
        }

        private void p_pause(object sender, EventArgs e)
        {
            if (!ShowControls) return;

            AudioEngine.Click(null, @"click-short-confirm");

            NotificationManager.ShowMessageMassive(AudioEngine.Paused ? "Unpause" : "Pause", 1000);
            manualPause = AudioEngine.TogglePause();
        }

        private void p_play(object sender, EventArgs e)
        {
            if (!ShowControls) return;

            AudioEngine.Click(null, @"click-short-confirm");

            NotificationManager.ShowMessageMassive("Play", 1000);
            if (AudioEngine.Paused)
                AudioEngine.TogglePause();
            else
            {
                AudioEngine.Play();
                //ChooseRandomSong(false);
            }
            manualPause = false;
        }

        private void p_prev(object sender, EventArgs e)
        {
            if (!ShowControls) return;

            AudioEngine.Click(null, @"click-short-confirm");

            NotificationManager.ShowMessageMassive("<< Prev", 1000);
            if (playHistory.Count > 0 && BeatmapManager.Current != null)
            {
                playFuture.Push(BeatmapManager.Current.BeatmapChecksum);
                BeatmapManager.Current = BeatmapManager.GetBeatmapByChecksum(playHistory.Pop());
            }
            ChooseRandomSong(false);
            manualPause = false;
        }

        internal static void StartRandomSongIfSilent()
        {
            if (AudioEngine.AudioState == AudioStates.Stopped && (AudioEngine.AudioTrack == null || AudioEngine.AudioTrack.Position > AudioEngine.AudioTrack.Length * 0.95) && AudioEngine.AllowRandomSong && BeatmapManager.Beatmaps.Count > 0 && GameBase.FadeState == FadeStates.Idle)
                ChooseRandomSong(true);
        }

        internal static void ChooseRandomSong(bool force, bool forcePreview = false)
        {
            if (BeatmapManager.Beatmaps.Count <= 0) return;

            if (BeatmapManager.Current == null || force)
            {
                List<Beatmap> maps = playPool.Count == 0 ? BeatmapManager.Beatmaps : playPool;
                int count = playPool.Count == 0 ? maps.Count - 1 : maps.Count;
                Beatmap next = maps[GameBase.random.Next(0, count)];

                int chance = 0;
                while (chance++ < 100 && (
                    (BeatmapManager.Current != null && next.SortTitle == BeatmapManager.Current.SortTitle) || !next.AudioPresent || next.GetAudioStream() == null))
                {
                    next = maps[GameBase.random.Next(0, count)];
                }

                if (BeatmapManager.Current != null)
                    playHistory.Push(BeatmapManager.Current.BeatmapChecksum);

                BeatmapManager.Current = next;
            }

            try
            {
                AudioEngine.LoadAudioForPreview(BeatmapManager.Current, true, 
                    forcePreview ||
                    (!ConfigManager.sMenuMusic && (AudioEngine.AudioTrack == null || AudioEngine.LoadedBeatmap == null || string.IsNullOrEmpty(AudioEngine.LoadedBeatmap.RelativeContainingFolder))));
            }
            catch (AudioNotLoadedException)
            {
            }

            SongInfo(false);
        }

        private static bool metadataEnabled = true;
        internal static void EnableMetadata()
        {
            metadataEnabled = true;
            SongInfo();
        }

        internal static void DisableMetadata()
        {
            metadataEnabled = false;
            SongInfo();
        }

        internal static void PushHistory(Beatmap b)
        {
            playHistory.Push(b.BeatmapChecksum);
        }

        internal static void AddPoolItem(Beatmap add)
        {
            playPool.Add(add);
        }

        internal static void ClearPool()
        {
            playPool.Clear();
        }

        private static void SongInfo(bool togglePermanent = false)
        {
            if (BeatmapManager.Current == null) return;

            if (!metadataEnabled)
            {
                SpriteManager.GetTagged("b").ForEach(s => s.MoveTo(s.InitialPosition, 100, EasingTypes.Out));
                SpriteManager.GetTagged("np").ForEach(s => s.FadeOut(60));
                return;
            }

            if (togglePermanent)
                ConfigManager.sPermanentSongInfo.Value = !ConfigManager.sPermanentSongInfo;

            SpriteManager.RemoveTagged("np");

            string npStr = BeatmapManager.Current.SortTitleAuto;

            if (npStr == null) return;

            if (npStr.Length > 54)
                npStr = npStr.Substring(0, 54) + "...";

            pText np = new pText(npStr, 14, Vector2.Zero, Vector2.Zero, 0.999F, ConfigManager.sPermanentSongInfo, Color.White, false);
            np.Field = Fields.TopRight;
            np.Tag = "np";
            float textSize = np.MeasureText().X;

            Transformation t1 = new Transformation(new Vector2(-80 + textSize, 0), new Vector2(10 + textSize, 0),
                                                   GameBase.Time, GameBase.Time + 1000);
            t1.Easing = EasingTypes.Out;
            Transformation t2 = new Transformation(TransformationType.Fade, 0, 1, GameBase.Time,
                                                   GameBase.Time + 1000);
            t1.Easing = EasingTypes.Out;

            Transformation t3 = t1.CloneReverse();
            t3.Time1 = GameBase.Time + 6000;
            t3.Time2 = GameBase.Time + 8000;

            Transformation t4 = t2.CloneReverse();
            t4.Time1 = GameBase.Time + 6000;
            t4.Time2 = GameBase.Time + 8000;

            np.Transformations.Add(t1);
            np.Transformations.Add(t2);
            if (!ConfigManager.sPermanentSongInfo)
            {
                np.Transformations.Add(t3);
                np.Transformations.Add(t4);
            }

            np.OriginPosition = new Vector2(0, -2);

            SpriteManager.Add(np);

            pSprite np2 = new pSprite(SkinManager.Load(@"menu-np", SkinSource.Osu), Fields.TopRight,
                                      Origins.TopLeft,
                                      Clocks.Game,
                                      new Vector2(620, 0), 0.998F, ConfigManager.sPermanentSongInfo, Color.White, "np");
            np2.OriginPosition = new Vector2(100, 0);

            np2.Transformations.Add(t1);
            np2.Transformations.Add(t2);
            if (!ConfigManager.sPermanentSongInfo)
            {
                np2.Transformations.Add(t3);
                np2.Transformations.Add(t4);
            }

            SpriteManager.Add(np2);

            Vector2 offset = new Vector2(0, 20);

            foreach (pSprite p in SpriteManager.GetTagged("b"))
            {
                p.Transformations.Clear();

                t1 = new Transformation(p.Position, p.InitialPosition + offset, GameBase.Time, GameBase.Time + 400);
                t2 = t1.CloneReverse();
                t2.EndVector = p.InitialPosition;
                t2.Time1 = GameBase.Time + 7600;
                t2.Time2 = GameBase.Time + 8000;

                p.Transformations.Add(t1);
                if (!ConfigManager.sPermanentSongInfo)
                    p.Transformations.Add(t2);
            }
        }
    }
}
