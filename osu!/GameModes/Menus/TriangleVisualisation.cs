﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu_common;
using osu_common.Helpers;

namespace osu.GameModes.Menus
{
    class TriangleVisualisation : Visualisation
    {
        /// <summary>
        /// How far off-screen we should create and destroy triangles.
        /// </summary>
        const float padding = 50;

        /// <summary>
        /// The current triangle velocity. Adjusted per-triangle based on their relative scale.
        /// </summary>
        internal float VelocityCurrent = 100;

        /// <summary>
        /// The base velocity, which will be the minimum velocity regardless of other settings.
        /// </summary>
        internal float VelocityBase = 1;

        /// <summary>
        /// New triangles are only spawned when this is true.
        /// </summary>
        internal bool ProduceTriangles = true;

        /// <summary>
        /// The levels of the currently playing song will affect the velocity of triangles (giving them a temporary boost).
        /// </summary>
        internal bool UseAudioVelocity = true;

        /// <summary>
        /// Triangles will fade as they approach base velocity.
        /// </summary>
        internal bool UseVelocityBasedFade = true;

        /// <summary>
        /// Fade triangles when Menu is idle. This should be removed and replaced with code in Menu.cs
        /// </summary>
        internal bool UseMenuIdleFade = true;

        /// <summary>
        /// Triangles can limit their spawn rate when osu! drops below a nominal frame rate (60fps).
        /// </summary>
        internal bool LimitWhenRunningSlow = true;

        /// <summary>
        /// Setting this higher will cause a higher ratio of bonus velocity to be used based on triangle scale.
        /// </summary>
        internal float VelocitySeparation = 1.4f;

        /// <summary>
        /// Velocity will dimish over time, returning to a stable base velocity.
        /// </summary>
        private bool UseVelocityDiminishing = true;

        SpriteManager spriteManager = new SpriteManager(true);

        long createCycle;

        Vector2? lastMouse = null;

        internal List<Color> Colours { get; private set; }

        public TriangleVisualisation()
        {
            Colours = new List<Color>();
        }

        internal void SetColours(List<Color> newColours, bool performFade = true)
        {
            Colours = newColours;

            if (performFade)
                foreach (pSprite p in spriteManager.SpriteList)
                    p.FadeColour(newColours[GameBase.random.Next(0, newColours.Count)], 0);

        }

        internal void SetColours(Stream stream = null, VoidDelegate callback = null)
        {
            GameBase.RunBackgroundThread(delegate
            {
                List<Color> newColours = new List<Color>();

                if (stream != null)
                {
                    using (System.Drawing.Bitmap b = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(stream, false, false))
                        for (int i = 0; i < 50; i++)
                            newColours.Add(OsuMathHelper.CConvert(b.GetPixel((int)(b.Width * GameBase.random.NextDouble()), (int)(b.Height * GameBase.random.NextDouble()))));

                    stream.Dispose();
                }
                else
                {
                    newColours.Add(new Color(255, 195, 83));
                    newColours.Add(new Color(151, 1, 90));
                    newColours.Add(new Color(47, 102, 159));
                    newColours.Add(new Color(47, 102, 159));
                    newColours.Add(new Color(255, 5, 76));

                    for (int i = 0; i < 2; i++)
                    {
                        byte c = (byte)GameBase.random.Next(224, 256);
                        newColours.Add(new Color(c, c, c));
                    }
                }

                GameBase.Scheduler.Add(delegate
                {
                    SetColours(newColours);
                    if (callback != null) callback();
                });
            });
        }

        protected override void Dispose(bool disposing)
        {
            spriteManager.Dispose();
            base.Dispose(disposing);
        }

        public override void Draw()
        {
            spriteManager.Draw();
            base.Draw();
        }

        public override void Update()
        {
            base.Update();

            if (GameBase.ElapsedMilliseconds > 50) return;

            if (UseMenuIdleFade)
            {
                if (Menu.IsIdle)
                    spriteManager.Blackness = Math.Max(0, spriteManager.Blackness - 0.01f * (float)GameBase.FrameRatio);
                else
                    spriteManager.Blackness = Math.Min(0.6f, spriteManager.Blackness + 0.01f * (float)GameBase.FrameRatio);
            }

            bool isStruggling = LimitWhenRunningSlow && GameBase.ElapsedMilliseconds > GameBase.SIXTY_FRAME_TIME;
            if (GameBase.SixtyFramesPerSecondFrame && createCycle++ % (isStruggling ? 10 : 6) == 0)
                for (int i = 0; i < VelocityCurrent; i++)
                    addAnArrow();

            if (UseAudioVelocity)
            {
                float bass = 0;
                int end = sample_size / 25;
                for (int i = 0; i < end; ++i)
                    bass += 2 * data[i] * (float)(end - i) / end;
                VelocityCurrent = Math.Max(VelocityCurrent, (float)Math.Min(bass * 1.5, 6));
            }

            if (UseVelocityDiminishing)
                VelocityCurrent *= 1 - 0.05f * (float)GameBase.FrameRatio;

            float velocity = (VelocityBase + VelocityCurrent) * (float)GameBase.FrameRatio * 0.5f;

            foreach (pSprite a in spriteManager.SpriteList)
            {
                a.Position.Y -= velocity * (0.2f + VelocitySeparation * (1 - a.Scale));
                if (!GameBase.Tournament)
                {
                    a.Position.Y += InputManager.CursorPositionDelta.Y / GameBase.WindowRatio * (0.2f + (1 - a.Scale) / 1) * 0.02f;
                    a.Position.X += InputManager.CursorPositionDelta.X / GameBase.WindowRatio * (0.2f + (1 - a.Scale) / 1) * 0.02f;
                }

                if (a.Position.Y < -padding)
                    a.AlwaysDraw = false;
            }

            if (UseVelocityBasedFade)
                spriteManager.Alpha = (float)Math.Pow(Math.Max(UseAudioVelocity ? 1 : 0, Math.Min(1, velocity / 10f)), 3);
        }

        private void addAnArrow(bool startOnScreen = false)
        {
            if (!ProduceTriangles || Colours.Count == 0) return;

            Color col = Colours[GameBase.random.Next(0, Colours.Count)];

            float scale = 0.2f + 0.8f * (float)GameBase.random.NextDouble();

            pTexture t_triangle = SkinManager.Load(@"triangle", (BanchoClient.Permission & Permissions.Supporter) > 0 ? SkinSource.All : SkinSource.Osu);
            t_triangle.UnloadOnModeChange = false;


            pSprite arrow = new pSprite(t_triangle, Fields.TopLeft, Origins.Centre, Clocks.Game,
                new Vector2((float)GameBase.random.NextDouble() * GameBase.WindowWidthScaled, startOnScreen ? (float)GameBase.random.NextDouble() * GameBase.WindowHeightScaled : GameBase.WindowHeightScaled + padding), 0.8f - scale * 0.01f, true, Color.White)
                {
                    //Rotation = (float)(Math.PI * GameBase.random.Next(0, 2)),
                    InitialColour = col,
                    Scale = scale
                };
            spriteManager.Add(arrow);
        }
    }
}
