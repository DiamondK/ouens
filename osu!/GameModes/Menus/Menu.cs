#region Using Statements

using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Menus.Forms;
using osu.GameModes.Online;
using osu.GameModes.Play;
using osu.GameModes.Play.Components;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Notifications;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu_common;
using osu_common.Helpers;
using osu_common.Libraries.NetLib;
using osudata;
using Un4seen.Bass;
using Utils = Un4seen.Bass.Utils;
using osu.Helpers;
using osu_common.Updater;
using osu.GameplayElements.Tips;
using osu.Graphics.Tips;
using osu.GameplayElements;
using osu.Online.Social;

#endregion

namespace osu.GameModes.Menus
{
    internal class Menu : DrawableGameComponent
    {
        #region General & Drawing

        internal static bool FirstLoad = true;
        private readonly Dictionary<Tiers, List<pSprite>> tierSprites = new Dictionary<Tiers, List<pSprite>>();
        private Tiers currentTier = Tiers.Main;
        private pText generalText;

        private pSpriteDynamic onlineImage;
        private pSprite osuDirect;
        private pSprite osuDirectBg;
        private pSprite permissionBat;
        private pSprite permissionSubscriber;
        private int perSecond;

        private pSprite s_Osu;
        private pSprite s_Osu2;
        SpriteManager spriteManagerIntro;
        private SpriteManager spriteManagerBackground;
        private SpriteManager spriteManager;
        private SpriteManager spriteManagerOverlay;
        private static bool QueuedCheckOnlineImage;
        ChangelogOverlay changelog;

        MenuVisualisation vis = new MenuVisualisation();
        SnowVisualisation snow = new SnowVisualisation();

        internal Menu(Game game)
            : base(game)
        {
        }

        private void startTransition()
        {
            if (GameBase.TotalFramesRendered < 5)
            {
                GameBase.Scheduler.AddDelayed(startTransition, 50);
                return;
            }

            GameBase.Scheduler.AddDelayed(delegate
            {
                if (ConfigManager.sMenuVoice)
                    AudioEngine.PlaySample(@"welcome", 100, (BanchoClient.Permission & Permissions.Supporter) > 0 ? SkinSource.ExceptBeatmap : SkinSource.Osu);
                trackPiano = AudioEngine.PlaySampleAsTrack(@"welcome_piano");
#if CHRISTMAS
                if (ConfigManager.sMenuMusic)
                    AudioEngine.Play();
#else
                trackPiano.Play();
                vis.CustomDataSource = trackPiano;
                vis.epicness = 20;
#endif
            }, 200);

            FirstLoad = false;

            initialFade.FadeTo(0, IntroLength);

            MusicControl.ShowControls = false;

            initialFadeWelcome.FadeInFromZero(IntroLength);
            initialFadeWelcome.Scale = 0.9f;
            initialFadeWelcome.VectorScale = new Vector2(1, 0);
            initialFadeWelcome.Transformations.Add(new Transformation(TransformationType.VectorScale, new Vector2(1, 0), new Vector2(1, 1), GameBase.Time + 200, GameBase.Time + 600, EasingTypes.Out));

            GameBase.Scheduler.AddDelayed(delegate
            {
                initialFade.FadeOut(120);
                initialFadeWelcome.FadeOut(120);
                initialFadeLevel = 0;

#if !CHRISTMAS
                if (ConfigManager.sMenuMusic)
                    AudioEngine.Play();
#endif

                AudioEngine.AllowRandomSong = true;

                MusicControl.ShowControls = true;

                onUpdateStateChange();
                GameBase.UpdateStateChanged += onUpdateStateChange;

                vis.CustomDataSource = null;
                vis.epicness = 1;

                showUserPanel();
                GameBase.User.Refresh();

                topBehind.FadeTo(0.4f, 500);
                bottomBehind.FadeTo(0.4f, 500);

                CheckPermissions();

                GameBase.TransitionManager.UpdateBackground();

                updateVisColour();

                if (GameBase.IsActive && MouseManager.MouseInWindow)
                    InputManager.ResetPosition(new Vector2(GameBase.WindowWidth / 2, GameBase.WindowHeight / 2 + (int)(60 * GameBase.WindowRatio)));

                InputManager.HandleInput = true;

                GameBase.Scheduler.AddDelayed(delegate
                {
#if !ARCADE
                    if (!GameBase.HasLogin && !GameBase.Options.Expanded)
                        GameBase.ShowLogin();
#endif
                    UpdateGeneral();

                }, 800);

            }, IntroLength);
        }

        public override void Initialize()
        {
            base.Initialize();

            if (GameBase.Tournament)
                Player.currentScore = null;

#if ARCADE
            //Set modifiable settings back to sane defaults when returning to the main menu.
            ModManager.Reset();
            BeatmapTreeManager.CurrentSortMode = TreeSortMode.Artist;
            BeatmapTreeManager.CurrentGroupMode = TreeGroupMode.Show_All;
#endif

            InputManager.ReplayMode = false;

            KeyboardHandler.OnKeyPressed += Game1_OnKeyPressed;
            JoystickHandler.OnButtonPressed += JoystickHandler_OnButtonPressed;

            GameBase.OnExitFade += GameBase_OnExitFade;

            spriteManager = new SpriteManager(true) { Masking = true };
            spriteManagerBackground = new SpriteManager(true);
            spriteManagerOverlay = new SpriteManager(true);

            if (FirstLoad)
            {
                try
                {
                    //download bundled maps.
                    if (BeatmapManager.Beatmaps.Count == 0 && (Directory.GetFiles(BeatmapManager.SONGS_DIRECTORY).Length + Directory.GetDirectories(BeatmapManager.SONGS_DIRECTORY).Length) < 6)
                    {
                        OsuDirect.StartDownload(new OsuDirectDownload(3756, @"3756 Peter Lambert - osu! tutorial.osz", @"osu! tutorial"));
                        OsuDirect.StartDownload(new OsuDirectDownload(163112, @"163112 Kuba Oms - My Love.osz", @"Bundled map #1"));
                        OsuDirect.StartDownload(new OsuDirectDownload(140662, @"140662 cYsmix feat. Emmy - Tear Rain.osz", @"Bundled map #2"));
                        OsuDirect.StartDownload(new OsuDirectDownload(151878, @"151878 Chasers - Lost.osz", @"Bundled map #3"));
                        OsuDirect.StartDownload(new OsuDirectDownload(190390, @"190390 Rameses B - Flaklypa.osz", @"Bundled map #4"));
                        OsuDirect.StartDownload(new OsuDirectDownload(123593, @"123593 Rostik - Liquid (Paul Rosenthal Remix).osz", @"Bundled map #5"));
                    }
                }
                catch { }

                InputManager.HandleInput = false;

                AudioEngine.AllowRandomSong = false;

                if (ConfigManager.sMenuMusic)
                {
                    try
                    {
                        welcomeMap = new Beatmap()
                        {
                            AudioFilename = @"welcome.mp3",
#if CHRISTMAS
                            Package = new osu_common.Libraries.Osz2.MapPackage(osu_ui.ResourcesStore.ResourceManager.GetStream(@"bgm_welcome_christmas")),
                            Title = @"Welcome to osu! (Christmas Edition)",
                            Filename = @"nekodex - welcome to christmas! (peppy).osu",
#else
                            Package = new osu_common.Libraries.Osz2.MapPackage(osu_ui.ResourcesStore.ResourceManager.GetStream(@"bgm_welcome")),
                            Title = @"Welcome to osu!",
                            Filename = @"nekodex - welcome to osu! (peppy).osu",
#endif
                            Artist = @"nekodex",
                            SortTitle = @"nekodex - Welcome to osu!"
                        };
                    }
                    catch
                    {
                        welcomeMap = null;
                    }

                    if (welcomeMap != null)
                    {
                        AudioEngine.LoadAudio(welcomeMap, false, false, true, true);
                        BeatmapManager.Current = welcomeMap;
                    }
                }

                spriteManagerIntro = new SpriteManager(true);

                initialFade = new pSprite(GameBase.WhitePixel, Fields.Native, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0.99999f, true, Color.Black);
                initialFade.VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight);
                initialFade.IsVisible = true; //set to true so the first update doesn't catch.

                initialFadeWelcome = new pSprite(SkinManager.Load(@"welcome_text", (BanchoClient.Permission & Permissions.Supporter) > 0 ? SkinSource.All : SkinSource.Osu), Fields.Centre, Origins.Centre, Clocks.Game, new Vector2(0, 0), 1, true, Color.White);
                initialFadeWelcome.Alpha = 0;
                initialFadeWelcome.IsVisible = true; //set to true so the first update doesn't catch.

                GameBase.spriteManagerOverlayHighest.Add(initialFade);
                GameBase.spriteManagerOverlayHighest.Add(initialFadeWelcome);

                GameBase.Scheduler.Add(startTransition);
            }
            else
            {
                onUpdateStateChange();
                GameBase.UpdateStateChanged += onUpdateStateChange;

                showUserPanel();
            }

            s_Osu = new pSpriteCircular(SkinManager.Load(@"menu-osu", SkinSource.Osu), new Vector2(-120, 0), 0.98F, true, Color.White, 300);
            s_Osu.Field = Fields.Centre;
            s_Osu.OnClick += s_Osu_OnClick;
            s_Osu.HandleInput = true;
            spriteManager.Add(s_Osu);

            vis.Position = new Vector2(GameBase.WindowWidthScaled, GameBase.WindowHeightScaled) / 2 + new Vector2(0, 0);

            foreach (Tiers ti in Enum.GetValues(typeof(Tiers)))
                tierSprites.Add(ti, new List<pSprite>());

            //Tier 1

            const int hPosition = -100;

#if !ARCADE
            AddMenuButton(@"play", new Vector2(hPosition, -125), s_Play_OnClick, Tiers.Main);
            AddMenuButton(@"edit", new Vector2(hPosition, -60), s_Edit_OnClick, Tiers.Main);
            AddMenuButton(@"options", new Vector2(hPosition, 5), s_Options_OnClick, Tiers.Main);
            AddMenuButton(@"exit", new Vector2(hPosition, 70), s_Exit_OnClick, Tiers.Main);


#if Public
            AddMenuButton(@"freeplay", new Vector2(hPosition, -92.5f), freeplay_OnClick, Tiers.Play);
            AddMenuButton(@"multiplayer", new Vector2(hPosition, -27.5f), multiplayer_OnClick, Tiers.Play);
            AddMenuButton(@"back", new Vector2(hPosition, 37.5f), back_OnClick, Tiers.Play);
#else
            AddMenuButton(@"freeplay", new Vector2(hPosition, -125), freeplay_OnClick, Tiers.Play);
            AddMenuButton(@"multiplayer", new Vector2(hPosition, -60), multiplayer_OnClick, Tiers.Play);
            AddMenuButton(@"charts", new Vector2(hPosition, 5), charts_OnClick, Tiers.Play);
            AddMenuButton(@"back", new Vector2(hPosition, 70), back_OnClick, Tiers.Play);
#endif

#endif

            s_Osu2 =
                new pSprite(SkinManager.Load(@"menu-osu", SkinSource.Osu), Fields.Centre, Origins.Centre,
                            Clocks.Game,
                            new Vector2(-120, 0), 0.981F, true, Color.White);
            if (ConfigManager.sMyPcSucks)
                s_Osu2.Bypass = true;

            pSprite copyright =
                new pSprite(SkinManager.Load(@"menu-copyright", SkinSource.Osu), Fields.BottomLeft, Origins.BottomLeft,
                            Clocks.Game, new Vector2(2, 2), 0.1F, true, Color.White);
            copyright.HandleInput = true;
            copyright.HoverEffects = new List<Transformation>();
            copyright.HoverEffects.Add(new Transformation(Color.White, new Color(255, 179, 59), 0, 100));
            copyright.HoverEffects.Add(new Transformation(TransformationType.Scale, 1, 1.15f, 0, 180, EasingTypes.Out));
            copyright.HoverEffects.Add(new Transformation(TransformationType.Scale, 1.15f, 1.05f, 180, 360, EasingTypes.In));
            copyright.HoverEffects.Add(new Transformation(TransformationType.Scale, 1.05f, 1.1f, 360, 540, EasingTypes.Out));
            copyright.HoverEffects.Add(new Transformation(TransformationType.Scale, 1.1f, 1.07f, 540, 720, EasingTypes.In));
            copyright.HoverEffects.Add(new Transformation(TransformationType.Scale, 1.07f, 1.085f, 720, 900, EasingTypes.Out));
            copyright.HoverLostEffects = new List<Transformation>();
            copyright.HoverLostEffects.Add(new Transformation(new Color(255, 179, 59), Color.White, 0, 400));
            copyright.HoverLostEffects.Add(new Transformation(TransformationType.Scale, 1.085f, 1, 0, 600, EasingTypes.Out));
            copyright.OnClick += copyright_OnClick;
            if (GameBase.Tournament) copyright.Bypass = true;
            spriteManagerOverlay.Add(copyright);
            spriteManager.Add(s_Osu2);


            generalText = new pText(string.Empty, 14, new Vector2(210, 0), Vector2.Zero, 0.95F, true, Color.White, false) { TextShadow = true };
            spriteManagerOverlay.Add(generalText);

            idleHideSprites.Add(generalText);

            UpdateGeneral();

            string tip = GetTip();
            if (!string.IsNullOrEmpty(tip))
            {
                menuTip = new pText(tip, 16, new Vector2(0, 52), new Vector2(GameBase.WindowWidth / GameBase.WindowRatio, 0), 1, false, Color.White, false);

                menuTip.Field = Fields.BottomCentre;
                menuTip.TextAlignment = TextAlignment.Centre;
                menuTip.Origin = Origins.TopCentre;
                menuTip.TextShadow = true;
                menuTip.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, GameBase.Time + IntroLength, GameBase.Time + IntroLength + 1000));
                menuTip.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + IntroLength + 12000, GameBase.Time + IntroLength + 12500));
                spriteManagerOverlay.Add(menuTip);
            }

            permissionBat =
                new pSprite(SkinManager.Load(@"menu-bat", SkinSource.Osu), Fields.TopLeft, Origins.Centre,
                            Clocks.Game, new Vector2(GameBase.WindowWidth / GameBase.WindowRatio - 50, GameBase.WindowHeight / GameBase.WindowRatio - 30), 0.96F, true, Color.TransparentWhite);
            permissionBat.ToolTip = LocalisationManager.GetString(OsuString.Menu_BAT);
            permissionBat.HandleInput = true;
            spriteManagerOverlay.Add(permissionBat);


            permissionSubscriber =
                new pSprite(SkinManager.Load(@"menu-subscriber", SkinSource.Osu), Fields.TopLeft, Origins.Centre,
                            Clocks.Game, new Vector2(GameBase.WindowWidth / GameBase.WindowRatio - 20, GameBase.WindowHeight / GameBase.WindowRatio - 30), 0.96F, true, Color.TransparentWhite);
            permissionSubscriber.ToolTip = LocalisationManager.GetString(OsuString.Menu_Supporter);
            permissionSubscriber.HandleInput = true;
            spriteManagerOverlay.Add(permissionSubscriber);

            osuDirectBg =
                new pSprite(SkinManager.Load(@"menu-osudirect", SkinSource.Osu), Fields.CentreRight, Origins.CentreRight,
                            Clocks.Game, new Vector2(0, 0), 0.95F, true, Color.TransparentWhite);
            spriteManagerOverlay.Add(osuDirectBg);

            osuDirect =
                new pSprite(SkinManager.Load(@"menu-osudirect-over", SkinSource.Osu), Fields.CentreRight,
                            Origins.CentreRight,
                            Clocks.Game, new Vector2(0, 0), 0.96F, true, Color.White);
            osuDirect.Alpha = 0.01f;
            spriteManagerOverlay.Add(osuDirect);
            osuDirect.HandleInput = true;
            osuDirect.OnClick += startOsuDirect;

            lightLeft = new pSprite(SkinManager.Load(@"menu-flash", SkinSource.Osu), Origins.CentreLeft, new Vector2(0, 240), 0.05f, true, Color.TransparentWhite);
            lightLeft.ViewOffsetImmune = true;
            lightLeft.Additive = true;
            lightLeft.VectorScale = new Vector2(-1, 4);
            spriteManager.Add(lightLeft);

            lightRight = new pSprite(SkinManager.Load(@"menu-flash", SkinSource.Osu), Origins.CentreRight, new Vector2(GameBase.WindowWidth / GameBase.WindowRatio, 240), 0.05f, true, Color.TransparentWhite);

            lightRight.Additive = true;
            lightRight.ViewOffsetImmune = true;
            lightRight.VectorScale = new Vector2(1, 4);
            spriteManager.Add(lightRight);

            topBehind = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0, true, Color.Black);
            topBehind.Alpha = FirstLoad ? 0 : 0.4f;
            topBehind.Scale = 1.6f;
            topBehind.VectorScale = new Vector2(GameBase.WindowWidthScaled, 54);
            spriteManagerOverlay.Add(topBehind);

            bottomBehind = new pSprite(GameBase.WhitePixel, Fields.BottomLeft, Origins.BottomLeft, Clocks.Game, Vector2.Zero, 0, true, Color.Black);
            bottomBehind.Alpha = FirstLoad ? 0 : 0.4f;
            bottomBehind.Scale = 1.6f;
            bottomBehind.VectorScale = new Vector2(GameBase.WindowWidthScaled, 54);
            spriteManagerOverlay.Add(bottomBehind);


            checkPermissions(true);
            BanchoClient.OnPermissionChange += CheckPermissions;

            ChangeOnlineImage();

            star = SkinManager.Load(@"star2", SkinSource.Osu);

            GameBase.FadeInComplete += GameBase_FadeInComplete;
            GameBase.LoadComplete();
            if (FirstLoad)
                GameBase.fadeLevel = 0;

#if ARCADE
            ChangeOnlineImage();

            //start in idle mode
            tierSprites[currentTier].ForEach(p => { p.FadeOut(500); p.IsClickable = false; });
            idleHideSprites.ForEach(p => p.FadeOut(500));
            s_Osu.MoveTo(new Vector2(0, s_Osu2.CurrentPosition.Y), 0, EasingTypes.In);
            s_Osu2.MoveTo
            (new Vector2(0, s_Osu2.CurrentPosition.Y), 0, EasingTypes.In);
#endif

            SetIdle(true, true);

            updateText = new pText(null, 10, new Vector2(0, GameBase.WindowHeightScaled - 34), 1, true, Color.White);
            updateText.TextColour = Color.White;
            updateText.Origin = Origins.TopLeft;
            updateText.TextAlignment = TextAlignment.Centre;
            spriteManagerOverlay.Add(updateText);

            GameBase.CheckForUpdates();

#if Release
            if (ConfigManager.sLastVersion != General.BUILD_NAME && !GameBase.Tournament)
                changelog = new ChangelogOverlay(GameBase.Game, false);
#endif

            if (!osuMain.IsWine && !(FrameworkDetection.GetVersions().Contains(FrameworkVersion.dotnet45) || FrameworkDetection.GetVersions().Contains(FrameworkVersion.dotnet4)))
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Menu_NewFrameworkVersion), Color.Yellow, 300000, delegate
                {
                    GameBase.ProcessStart(@"http://www.microsoft.com/en-us/download/details.aspx?id=42643");
                });
            }

            GameBase.TransitionManager.BackgroundAlpha = 1;
        }

        void GameBase_OnExitFade()
        {
            fadeAllButtons(null);
        }

        private void showUserPanel()
        {
            GameBase.User.DrawAt(new Vector2(0, 0), false, 0);
            GameBase.User.Sprites[0].Depth = 0.8f;
            if (!GameBase.Tournament)
                spriteManagerOverlay.Add(GameBase.User.Sprites);
        }

        void JoystickHandler_OnButtonPressed(object sender, List<Keys> keys)
        {
            Keys key = keys[0];
            switch (key)
            {
                case JoyKey.Right:
                    Game1_OnKeyPressed(null, Keys.P);
                    break;
                case JoyKey.Left:
                    Game1_OnKeyPressed(null, Keys.Escape);
                    break;
            }
        }

        void onUpdateStateChange()
        {
            switch (GameBase.UpdateState)
            {
                case UpdateStates.NeedsRestart:
                    ShowUpdateButton();
                    break;
                default:
                    HideUpdateButton();
                    break;
            }
        }

        pSprite lightLeft;
        pSprite lightRight;

        internal static string titleImageUrl;
        internal static string titleLinkUrl;
        internal static string titleImageCache;
        internal static void ChangeOnlineImage(string imageUrl = null, string linkUrl = null)
        {
            if (imageUrl != null)
            {
                titleImageUrl = imageUrl.Length < 4 ? null : imageUrl;
                titleLinkUrl = linkUrl;
            }

            GameBase.Scheduler.Add(delegate
            {
                if (GameBase.Mode != OsuModes.Menu) return;

                Menu m = GameBase.RunningGameMode as Menu;
                if (m == null || m.updateIcon != null) return;

                if (m.onlineImage != null)
                {
                    m.onlineImage.FadeOut(500);
                    m.onlineImage.AlwaysDraw = false;
                    m.onlineImage.IsDisposable = true;
                }

                if (titleImageUrl == null) return;

                titleImageCache = General.TEMPORARY_FILE_PATH + @"t" + CryptoHelper.GetMd5String(titleImageUrl);

                try
                {
                    m.onlineImage = new pSpriteDynamic(titleImageUrl, titleImageCache, 0, Vector2.Zero, 0.99f)
                    {
                        Field = Fields.BottomCentre,
                        Origin = Origins.BottomCentre,
                        AlwaysDraw = true
                    };

                    m.onlineImage.IsDisposable = true;
                    m.onlineImage.OnHover += delegate
                    {
                        m.onlineImage.ScaleTo(1.1f, 500, EasingTypes.OutBounce);
                    };

                    m.onlineImage.OnHoverLost += delegate
                    {
                        m.onlineImage.ScaleTo(1f, 500, EasingTypes.OutCubic);
                    };
                }
                catch
                {
                    m.onlineImage = null;
                }

                if (m.onlineImage != null)
                {

                    if (!string.IsNullOrEmpty(titleLinkUrl))
                    {
                        m.onlineImage.HandleInput = true;
                        m.onlineImage.OnClick += delegate
                                                    {
                                                        if (!ChatEngine.IsVisible)
                                                        {
                                                            GameBase.ProcessStart(titleLinkUrl);
                                                            pSprite clone = m.onlineImage.Clone();
                                                            clone.Additive = true;
                                                            clone.Depth = 1;
                                                            clone.FadeOutFromOne(400);
                                                            m.spriteManager.Add(clone);
                                                        }
                                                    };
                    }

                    m.spriteManagerOverlay.Add(m.onlineImage);

                    m.onlineImage.OnTextureLoaded += delegate
                    {
                        m.onlineImage.FadeInFromZero(1000);
                    };
                }
            });
        }

        private void CheckPermissions()
        {
            checkPermissions();
        }

        private void checkPermissions(bool firstRun = false)
        {
            if (GameBase.Tournament) return;

            if (initialFadeLevel >= 0)
            {
                int startTime = GameBase.Time + 250;

                if ((BanchoClient.Permission & Permissions.BAT) > 0)
                {
                    permissionBat.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, startTime, startTime + 200));
                    permissionBat.Transformations.Add(new Transformation(TransformationType.Scale, 0.25f, 1, startTime, startTime + 1200, EasingTypes.OutElastic));

                    startTime += 400;
                }
                else
                    permissionBat.FadeOut(100);
                if ((BanchoClient.Permission & Permissions.Supporter) > 0)
                {
                    permissionSubscriber.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, startTime, startTime + 200));
                    permissionSubscriber.Transformations.Add(new Transformation(TransformationType.Scale, 0.25f, 1f, startTime, startTime + 1200, EasingTypes.OutElastic));

                    osuDirect.FadeTo(0.01f, 200);
                    osuDirect.HoverEffect = new Transformation(TransformationType.Fade, 0.01f, 1, 0, 140);
                    osuDirectBg.FadeIn(200);
                }
                else
                {
                    permissionSubscriber.FadeOut(100);
                    osuDirect.FadeOut(100);
                    osuDirect.HoverEffect = null;
                    osuDirectBg.FadeOut(100);
                }
            }
        }

        private void updateVisColour()
        {
            vis.SetColour(SkinManager.Colours[@"MenuGlow"]);
        }

        List<pSprite> idleHideSprites = new List<pSprite>();

        private void AddMenuButton(string name, Vector2 position, EventHandler clickFunction, Tiers tier)
        {
            pSprite sprite =
                new pSprite(SkinManager.Load(@"menu-button-" + name, SkinSource.Osu), Fields.Centre, Origins.TopLeft,
                            Clocks.Game, position, 0.8F - (float)tier / 10, true,
                            (tier == currentTier ? Color.White : Color.TransparentWhite));
            pSprite hoverSprite =
                new pSprite(SkinManager.Load(@"menu-button-" + name + @"-over", SkinSource.Osu), Fields.Centre,
                            Origins.TopLeft, Clocks.Game, position, 0.81F - (float)tier / 10, true,
                            Color.TransparentWhite);

            List<Transformation> effects = new List<Transformation>();
            Transformation move = new Transformation(sprite.Position, sprite.Position + new Vector2(20, 0), 0, 580);
            move.Easing = EasingTypes.OutElastic;
            effects.Add(move);
            sprite.HoverEffects = effects;

            effects = new List<Transformation>();
            move = new Transformation(Vector2.Zero, sprite.Position, 0, 400, EasingTypes.Out);
            move.Easing = EasingTypes.OutCubic;
            effects.Add(move);
            sprite.HoverLostEffects = effects;
            sprite.OnHover += s_Hover;
            sprite.OnHoverLost += s_HoverLost;
            sprite.Tag = hoverSprite;
            sprite.TagNumeric = (int)tier;

            sprite.HandleInput = true;
            sprite.OnClick += clickFunction;
            sprite.OnClick += menuItem_OnClick;

            hoverSprite.TagNumeric = -1; //Ensure it doesnt match a tier

            spriteManager.Add(sprite);
            spriteManager.Add(hoverSprite);

            tierSprites[tier].Add(sprite);
            tierSprites[tier].Add(hoverSprite);
        }

        void menuItem_OnClick(object sender, EventArgs e)
        {
            pSprite s = (pSprite)sender;

            if (!s.Texture.assetName.Contains(@"-play") && !s.Texture.assetName.Contains(@"-back"))
            {
                pSprite cl = s.Clone();
                cl.AlwaysDraw = false;
                cl.Additive = true;
                cl.Depth = s_Osu.Depth - 0.001f;
                cl.InitialColour = new Color(0, 78, 165, 255);
                cl.Alpha = 0.6f;
                if (cl.Transformations.Find(t => t.Type == TransformationType.Fade) == null)
                    cl.FadeOut(500);
                spriteManager.Add(cl);
            }
        }

        private void s_HoverLost(object sender, EventArgs e)
        {
            pSprite p = sender as pSprite;
            if (p.Tag == null) return;

            if ((Tiers)p.TagNumeric != currentTier)
                return;

            pSprite p2 = ((pSprite)p.Tag);
            p2.Transformations.Clear();
            p2.Transformations.AddRange(p.Transformations);
            p2.FadeOut(500);
        }

        private void s_Hover(object sender, EventArgs e)
        {
            pSprite p = sender as pSprite;

            if (currentTier != (Tiers)p.TagNumeric)
                return;

            if (p.Tag != null)
            {
                pSprite p2 = ((pSprite)p.Tag);
                p2.Transformations.Clear();
                p2.Transformations.AddRange(p.Transformations);
                p2.FadeIn(30);
            }
            if (Game.IsActive)
                AudioEngine.PlaySamplePositional(@"menuclick");
        }

        private void s_Osu_OnClick(object sender, EventArgs e)
        {
            hoverBonus -= 0.08f;

#if ARCADE
            freeplay_OnClick(null, null);
#else
            if (CheckIdle()) return;

            if (currentTier == Tiers.Main)
                s_Play_OnClick(null, null);
            else
                freeplay_OnClick(null, null);
#endif
        }

        private bool CheckIdle(bool silent = false)
        {
#if ARCADE
            lastActivity = GameBase.Time;
            return false;
#endif

            lastActivity = GameBase.Time;
            if (IsIdle)
            {
                if (!silent) AudioEngine.PlaySamplePositional(@"menuhit");
                return true;
            }

            return false;
        }

        private void multiplayer_OnClick(object sender, EventArgs e)
        {
            if (!BanchoClient.CheckAuth(true)) return;

            if (!AudioEngine.CheckAudioDevice()) return;

            if (!BanchoClient.Connected)
            {
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.Lobby_Bancho_Fail), 3000);
                return;
            }

            AudioEngine.PlaySamplePositional(@"menuhit");
            GameBase.ChangeMode(OsuModes.Lobby);
            Lobby.chatOpenOnStart = ChatEngine.IsVisible;
            fadeAllButtons(sender);
        }

        private void freeplay_OnClick(object sender, EventArgs e)
        {
            AudioEngine.PlaySamplePositional(@"menuhit");

            GameBase.ChangeMode(OsuModes.SelectPlay);
            fadeAllButtons(sender);
        }

        private void charts_OnClick(object sender, EventArgs e)
        {
            AudioEngine.PlaySamplePositional(@"menuhit");

            GameBase.ChangeMode(OsuModes.Charts);
            fadeAllButtons(sender);
        }

        private void back_OnClick(object sender, EventArgs e)
        {
            AudioEngine.PlaySamplePositional(@"menuback");
            ChangeTier(Tiers.Main);
        }

        private static string GetTip()
        {
            if (GameBase.Tournament) return string.Empty;

            if (!string.IsNullOrEmpty(General.SUBVERSION))
                return @"Welcome to osu!" + General.SUBVERSION + @" (" + General.FULL_VERSION + @").";

            if (!ConfigManager.sShowMenuTips) return string.Empty;

            int tipCount = (int)OsuString.MenuTip_LAST_DONT_TRANSLATE - (int)OsuString.MenuTip_1;
            int thisTip = ConfigManager.sMenuTip % tipCount;

            ConfigManager.sMenuTip.Value = (thisTip + 1) % tipCount;

            return LocalisationManager.GetString(OsuString.MenuTip_1 + thisTip);
        }

        private static void copyright_OnClick(object sender, EventArgs e)
        {
            if (!ChatEngine.IsVisible)
                GameBase.ProcessStart(General.WEB_ROOT + @"/");
        }

        private void GameBase_FadeInComplete(object sender, EventArgs e)
        {
            GameBase.FadeInComplete -= GameBase_FadeInComplete;

#if peppyz
            InputManager.ReplayScore = ScoreManager.ReadReplayFromFile(@"in.osr");
            InputManager.ReplayMode = true;
            InputManager.ReplayScore.ReadReplayData();
            //InputManager.ReplayScore.SubmitScore();
            BeatmapManager.Current = BeatmapManager.GetBeatmapByChecksum(InputManager.ReplayScore.fileChecksum);
            GameBase.ChangeMode(OsuModes.Play);
#endif


        }


        private void UpdateGeneral()
        {
            string general;
            if (ConfigManager.sUsername.Value.Length > 0)
            {
                string tstamp;

                int time = GameBase.Time;

                int hours = time / 3600000;
                time -= hours * 3600000;
                int minutes = time / 60000;
                time -= minutes * 60000;
                int seconds = time / 1000;

                if (hours > 0)
                    tstamp = string.Format(@"{0:00}:{1:00}:{2:00}", hours, minutes, seconds);
                else if (minutes > 0)
                    tstamp = string.Format(@"{0:00}:{1:00}", minutes, seconds);
                else
                    tstamp = string.Format(LocalisationManager.GetString(OsuString.Menu_RunningSeconds), seconds);

                general = String.Format(LocalisationManager.GetString(OsuString.Menu_GeneralInformation),
                                        BeatmapManager.Beatmaps.Count, tstamp, DateTime.Now.ToShortTimeString());
            }
            else
                general =
                    String.Format(LocalisationManager.GetString(OsuString.Menu_GeneralInformation_Offline),
                                  BeatmapManager.Beatmaps.Count);
            generalText.Text = general;
        }


        protected override void Dispose(bool disposing)
        {
            GameBase.UpdateStateChanged -= onUpdateStateChange;
            KeyboardHandler.OnKeyPressed -= Game1_OnKeyPressed;
            JoystickHandler.OnButtonPressed -= JoystickHandler_OnButtonPressed;
            GameBase.FadeInComplete -= GameBase_FadeInComplete;
            GameBase.OnExitFade -= GameBase_OnExitFade;
            BanchoClient.OnPermissionChange -= CheckPermissions;
#if ARCADE
            //Reset volume on exiting screen.
            AudioEngine..VolumeMusic.Value = 100);
#endif

            if (spriteManager != null) spriteManager.Dispose();
            if (spriteManagerOverlay != null) spriteManagerOverlay.Dispose();
            if (spriteManagerIntro != null) spriteManagerIntro.Dispose();
            if (changelog != null) changelog.Dispose();
            if (updateBar != null) updateBar.AlwaysDraw = false;
            if (vis != null) vis.Dispose();
            if (snow != null) snow.Dispose();
            if (trackPiano != null) trackPiano.Dispose();

            base.Dispose(disposing);
        }

        float hoverBonus;

        int current = 0;

        bool wasKiai;
        int lastKiaiStartTime;

        float progressLastFrame;
        public override void Draw()
        {
            int last = current;

            float progress = 0;
            int length = 0;
            current = 0;

            if (initialFadeLevel < 0 && initialFade != null)
            {
                Transformation tr = initialFade.Transformations.Find(f => f.Type == TransformationType.Fade);
                if (tr != null) tr.EndFloat = Math.Min(initialFade.Alpha, 0.05f + 0.15f * (float)GameBase.random.NextDouble());

                initialFadeWelcome.Scale += (float)(0.0004 * GameBase.FrameRatio);
            }

            if (AudioEngine.Paused)
            {
                progress = GameBase.Time % 1000;
                length = 1000;
                current = GameBase.Time / 1000;
            }
            else if (BeatmapManager.Current != null && !AudioEngine.Paused && AudioEngine.BeatSyncing && AudioEngine.Time - AudioEngine.CurrentOffset > 0)
            {
                progress = (int)((AudioEngine.Time + GameBase.FrameRatio * GameBase.SIXTY_FRAME_TIME - AudioEngine.CurrentOffset) % AudioEngine.beatLength);
                current = (int)((AudioEngine.Time + GameBase.FrameRatio * GameBase.SIXTY_FRAME_TIME - AudioEngine.CurrentOffset) / AudioEngine.beatLength);
                length = (int)AudioEngine.beatLength;
            }

            if (last != current)
            {
                if (s_Osu.Hovering && GameBase.IsActive && current >= 16)
#if CHRISTMAS
                    AudioEngine.PlaySample(@"heartbeat-xmas", (int)AudioEngine.volumeMusicFade);
#else
                    AudioEngine.PlaySample(@"heartbeat", (int)AudioEngine.volumeMusicFade);
#endif
                DrawRipple();
            }

            bool kiaiAwesome = AudioEngine.KiaiEnabled && !AudioEngine.Paused;

            if (AudioEngine.SyncNewBeat)
                savedProgressMultiplier = menuAlpha2;

            float velocityProgress = 1 - ((audioVelocity - audioVelocityAverage) / 0.5f);
            float beatProgress = length > 0 ? progress / length : 0.5f;

            length = 1;

            float ratio = (float)Math.Pow(0.5, GameBase.FrameRatio);

            progress = progressLastFrame * ratio + (float)OsuMathHelper.Clamp(1f - (beatProgress * 0.5f + velocityProgress * 0.5f), 0, 1) * (1 - ratio);
            progressLastFrame = progress;

            s_Osu.Scale = OsuMathHelper.easeOutVal(progress, (1.05F + hoverBonus), (1 + hoverBonus), length);

            s_Osu2.Alpha = OsuMathHelper.easeOutVal(progress, kiaiAwesome ? 0.1F : 0.4F, 0, length) * savedProgressMultiplier;
            s_Osu2.Scale = OsuMathHelper.easeOutVal(progress, (1.05F + hoverBonus), (1.08F + hoverBonus), length);

            s_Osu2.Additive = kiaiAwesome;

            spriteManagerBackground.Draw();

            spriteManager.Draw();

            vis.Position = -spriteManager.ViewOffset + s_Osu.Position + new Vector2(GameBase.WindowWidthScaled / 2, GameBase.WindowHeightScaled / 2);
            vis.Radius = OsuMathHelper.easeInVal(length - progress, (1.05F + hoverBonus), (1.08F + hoverBonus), length) * 150 + (initialFadeLevel < 0 ? 8 : 0);
            vis.Alpha = (1 - (s_Osu.Position.X / -120f) * 0.7f) * (kiaiAwesome ? 1 : 0.7f);
            vis.Update();
            vis.Draw();

            spriteManagerOverlay.Draw();

            if (initialFadeLevel >= 0)
            {
                snow.Update();
                snow.Draw();
                if (changelog != null) changelog.Draw();
            }

            if (spriteManagerIntro != null) spriteManagerIntro.Draw();

            base.Draw();
        }

        float savedProgressMultiplier;

        private void DrawRipple()
        {
            pSprite s_Osu3 = new pSprite(SkinManager.Load(@"menu-osu-shockwave", SkinSource.Osu), Fields.Centre, Origins.Centre, Clocks.Game, s_Osu.Position, 0.2F, false, Color.White);
            s_Osu3.Additive = true;
            s_Osu3.Alpha = 0.1f * menuAlpha2;
            s_Osu3.FadeOut(1000);
            s_Osu3.Transformations.Add(new Transformation(TransformationType.Scale, s_Osu.Scale, s_Osu.Scale * 1.4f, GameBase.Time, GameBase.Time + 1000, EasingTypes.Out));
            spriteManager.Add(s_Osu3);
        }

        internal static bool IsIdle;
        int lastActivity;

        float menuAlpha = 0.5f;
        float menuAlpha2 = 0.5f;
        private pTexture star;
        private static long titleByteSize;
        private pText updateBar;
        private pText updateText;

        float initialFadeLevel = FirstLoad ? -1 : 101;

        public override void Update()
        {
            if (initialFadeLevel > 100 && !GameBase.Tournament)
            {
                if (ConfigManager.sMenuParallax)
                {
                    float distanceX = InputManager.CursorPosition.X - (GameBase.WindowWidth / 2);
                    float distanceY = InputManager.CursorPosition.Y - (GameBase.WindowHeight / 2);
                    spriteManager.ViewOffset = new Vector2(distanceX, distanceY) / GameBase.WindowRatio / 60;
                }
                else
                    spriteManager.ViewOffset = Vector2.Zero;
            }

            if (initialFadeLevel <= 100 && GameBase.FadeState != FadeStates.FadeOut)
            {
                if (initialFadeLevel >= 0 && GameBase.SixtyFramesPerSecondFrame && GameBase.Time > 1000)
                    initialFadeLevel += 5;

                spriteManagerBackground.Blackness = spriteManager.Blackness = 1 - initialFadeLevel / 100f;
            }
            else
            {
                if (Menu.IsIdle)
                    spriteManagerBackground.Blackness = Math.Max(0, spriteManagerBackground.Blackness - 0.01f * (float)GameBase.FrameRatio);
                else
                    spriteManagerBackground.Blackness = Math.Min(0.6f, spriteManagerBackground.Blackness + 0.01f * (float)GameBase.FrameRatio);
            }

            if (AudioEngine.AudioTrack != null && welcomeMap == AudioEngine.AudioTrack.Beatmap)
            {
                if (!GameBase.IsActive && AudioEngine.volumeMusicFade > 40)
                    AudioEngine.SetVolumeMusicFade(AudioEngine.volumeMusicFade - 5, true);
            }

            if (wasKiai != AudioEngine.KiaiEnabled)
            {
                wasKiai = AudioEngine.KiaiEnabled;

                if (wasKiai && (lastKiaiStartTime == 0 || GameBase.Time - lastKiaiStartTime > 500) && Math.Abs(AudioEngine.Time - AudioEngine.ActiveControlPoint.offset) < 500)
                {
                    int dir = GameBase.random.Next(-1, 2);

                    for (int i = 0; i < 100; i++)
                    {
                        int time1 = GameBase.Time + i * 10;
                        int time2 = time1 + GameBase.random.Next(300, 1300);

                        Transformation ts =
                            new Transformation(TransformationType.Scale, 1,
                                               (float)(GameBase.random.NextDouble() * (ConfigManager.sMyPcSucks ? 1.1 : 1.8) + 1),
                                               time1, time2);
                        Transformation tf = new Transformation(TransformationType.Fade, 1, 0, time1, time2);
                        Transformation tr =
                            new Transformation(TransformationType.Rotation, (float)(GameBase.random.NextDouble() * 4 - 2),
                                               (float)(GameBase.random.NextDouble() * 4 - 2), time1, time2);

                        ts.Easing = EasingTypes.Out;
                        tf.Easing = EasingTypes.Out;
                        tr.Easing = EasingTypes.Out;

                        //Left Fountain
                        Vector2 startVector = new Vector2(100, 490);
                        pSprite p1 = new pSprite(star, Fields.TopLeft,
                                                 Origins.Centre,
                                                 Clocks.Game,
                                                 startVector, 1, false, Color.White);
                        p1.Additive = true;
                        p1.Transformations.Add(ts);
                        p1.Transformations.Add(tf);
                        p1.Transformations.Add(tr);


                        spriteManager.Add(p1);
                        GameBase.PhysicsManager.Add(p1, new Vector2(dir * (i - 50f) / 50 * 450 + GameBase.random.Next(-50, 50), GameBase.random.Next(-800, -700)), true)
                        .weight = 80;

                        //Right Fountain
                        startVector = new Vector2(GameBase.WindowWidthScaled - 100, 490);
                        pSprite p2 = new pSprite(star, Fields.TopLeft,
                                                 Origins.Centre,
                                                 Clocks.Game,
                                                 startVector, 1, false, Color.White);

                        p2.Additive = true;
                        p2.Transformations.Add(ts);
                        p2.Transformations.Add(tf);
                        p2.Transformations.Add(tr);

                        spriteManager.Add(p2);
                        GameBase.PhysicsManager.Add(p2, new Vector2(dir * (i - 50f) / 50 * -450 + GameBase.random.Next(-50, 50), GameBase.random.Next(-800, -700)), true)
                            .weight = 80;
                    }

                    lastKiaiStartTime = GameBase.Time;
                }
            }

            if (GameBase.GameQueuedState == OsuModes.Exit)
            {
                hoverBonus -= (float)(GameBase.FrameRatio * 0.003);

                s_Osu.Rotation += (float)(GameBase.FrameRatio * 0.001);
                s_Osu2.Rotation += (float)(GameBase.FrameRatio * 0.001);

                s_Osu.Alpha -= (float)(GameBase.FrameRatio * 0.002);
                s_Osu2.Alpha -= (float)(GameBase.FrameRatio * 0.002);
            }
            else if (s_Osu.Hovering || hoverBonus < 0)
                hoverBonus = Math.Min(hoverBonus + (float)(0.012 * GameBase.FrameRatio), 0.1f);
            else
                hoverBonus = Math.Max(hoverBonus - (float)(0.012 * GameBase.FrameRatio), 0);

            float leftAim;
            float rightAim;

            int totalVelocity = 65536 / 2;

            if (AudioEngine.AudioTrack != null && AudioEngine.AudioTrack.IsDummyDevice)
            {
                leftAim = 0.5f;
                rightAim = 0.5f;
            }
            else
            {

                int word = 0;
                try
                {
                    word = Bass.BASS_ChannelGetLevel(AudioEngine.AudioTrack.audioStream);
                }
                catch
                {
                    //can fault if audio device is not attached.
                }

                int left = Utils.LowWord32(word);
                int right = Utils.HighWord32(word);

                totalVelocity = left + right;

                leftAim = Math.Min(1, Math.Max(0, (2 * left - 20000) / 45536f) / 2);
                rightAim = Math.Min(1, Math.Max(0, (2 * right - 20000) / 45536f) / 2);
            }

            UpdateIdleState();

            if (AudioEngine.Paused)
            {
                lightLeft.FadeOut(50);
                lightRight.FadeOut(50);
            }
            else
            {
                if (AudioEngine.SyncNewBeat && (AudioEngine.KiaiEnabled || (AudioEngine.SyncBeat % (int)AudioEngine.timeSignature == 0 && AudioEngine.SyncBeat > 0)) && GameBase.FadeState == FadeStates.Idle)
                {
                    int length = Math.Max(300, (int)AudioEngine.beatLength);

                    if (!AudioEngine.KiaiEnabled || AudioEngine.SyncBeat % 2 == 0)
                    {

                        lightLeft.Transformations.RemoveAll(t => t.Type == TransformationType.Fade);
                        lightLeft.Transformations.Add(new Transformation(TransformationType.Fade, lightLeft.Alpha, leftAim, GameBase.Time, GameBase.Time + 64));
                        lightLeft.Transformations.Add(new Transformation(TransformationType.Fade, leftAim, 0, GameBase.Time + 64, GameBase.Time + length, EasingTypes.In));
                    }

                    if (!AudioEngine.KiaiEnabled || AudioEngine.SyncBeat % 2 == 1)
                    {
                        lightRight.Transformations.RemoveAll(t => t.Type == TransformationType.Fade);
                        lightRight.Transformations.Add(new Transformation(TransformationType.Fade, lightRight.Alpha, rightAim, GameBase.Time, GameBase.Time + 64));
                        lightRight.Transformations.Add(new Transformation(TransformationType.Fade, rightAim, 0, GameBase.Time + 64, GameBase.Time + length, EasingTypes.In));
                    }
                }
            }

            if (GameBase.Time - perSecond >= 1000)
            {
                perSecond += 1000;
                UpdateGeneral();
            }

            if (GameBase.SixtyFramesPerSecondFrame)
            {
                float localAudioVelocity = (AudioEngine.KiaiEnabled ? 1 : 0.6f + Math.Min(1, Math.Max(0, (totalVelocity - 30000) / 35536f)) * 0.4f);

                audioVelocity = (totalVelocity) / 65536f * AudioEngine.volumeMusicFade / 100f;
                audioVelocityAverage = audioVelocityAverage * 0.9f + 0.1f * audioVelocity;

                menuAlpha2 = menuAlpha2 * 0.8f + 0.2f * localAudioVelocity;

                if (localAudioVelocity > menuAlpha)
                    menuAlpha = Math.Min(1, menuAlpha + 0.05f);
                else
                    menuAlpha = Math.Max(0, menuAlpha - 0.0007f);

                if (GameBase.UpdateState == UpdateStates.Updating)
                    updateText.Text = CommonUpdater.GetStatusString(true);
                else
                    updateText.Text = null;
            }


            if (changelog != null) changelog.Update();

            base.Update();
        }

#if ARCADE
        int checkRound = 0;

        private void UpdateAudioFading()
        {
            if (InputManager.CursorPosition != cursorPositionLast && GameBase.IsActive)
                    lastActivity = GameBase.Time;

            if (checkRound++ % 16 == 0)
            {
                if (GameBase.Time - lastActivity > 60000 && AudioEngine.VolumeMusic > 25)
                    AudioEngine..VolumeMusic.Value = AudioEngine.VolumeMusic - 1);
                else if (GameBase.Time - lastActivity > 40000 && AudioEngine.VolumeMusic > 50)
                    AudioEngine..VolumeMusic.Value = AudioEngine.VolumeMusic - 1);
                else if (GameBase.Time - lastActivity > 15000 && AudioEngine.VolumeMusic > 75)
                    AudioEngine..VolumeMusic.Value = AudioEngine.VolumeMusic - 1);
            }

            if (AudioEngine.VolumeMusic < 100 && GameBase.Time - lastActivity < 15000)
                AudioEngine..VolumeMusic.Value = AudioEngine.VolumeMusic + 1);
        }
#endif

        private void UpdateIdleState()
        {
            if (!InputManager.HandleInput) return;

            bool cursorMoved = Vector2.Distance(InputManager.CursorPosition, InputManager.CursorPositionLast) > 5 * GameBase.FrameRatio;

#if ARCADE
            UpdateAudioFading();
#else
            if (!IsIdle)
            {
                if (cursorMoved && GameBase.IsActive)
                    lastActivity = GameBase.Time;
            }

            if (lastActivity > 0 && GameBase.Time - lastActivity > 6000 != IsIdle)
                SetIdle(!IsIdle);

            if (cursorMoved || GameBase.Time - lastActivity < 6000)
                MusicControl.SpriteManager.Alpha = spriteManagerOverlay.Alpha = (float)Math.Min(1, MusicControl.SpriteManager.Alpha + 0.08f * GameBase.FrameRatio);
            else
                MusicControl.SpriteManager.Alpha = spriteManagerOverlay.Alpha = (float)Math.Max(0.01f, MusicControl.SpriteManager.Alpha - 0.001f * GameBase.FrameRatio);

#endif
        }

        private void SetIdle(bool status, bool instant = false)
        {
            IsIdle = status;

            GameBase.TransitionManager.BackgroundAlpha = IsIdle ? 1 : 0.4f;

            if (IsIdle)
            {
                tierSprites[currentTier].ForEach(p =>
                {
                    p.FadeOut(instant ? 0 : 500);
                    p.HandleInput = false;
                    p.MoveTo(p.InitialPosition - new Vector2(100, 0), 2000, EasingTypes.Out);
                });

                idleHideSprites.ForEach(p => p.FadeOut(instant ? 0 : 500));

                s_Osu.MoveTo(new Vector2(0, s_Osu2.Position.Y), instant ? 0 : 2000, EasingTypes.Out);
                s_Osu2.MoveTo(new Vector2(0, s_Osu2.Position.Y), instant ? 0 : 2000, EasingTypes.Out);
            }
            else
            {
                idleHideSprites.ForEach(p => p.FadeIn(instant ? 0 : 500));

                int duration = 100;
                tierSprites[currentTier].ForEach(p =>
                {
                    if (p.TagNumeric == (int)currentTier)
                    {
                        p.FadeIn(350);
                        p.Position = p.InitialPosition - new Vector2(100, 0);
                        p.MoveTo(p.InitialPosition, 400, EasingTypes.Out);
                    }

                    if (p.TagNumeric != -1)
                        p.HandleInput = true;
                });

                s_Osu.MoveTo(s_Osu.InitialPosition, 400, EasingTypes.Out);
                s_Osu2.MoveTo(s_Osu.InitialPosition, 400, EasingTypes.Out);
            }
        }

        private enum Tiers
        {
            Main,
            Play
        }

        #endregion

        #region User Input Handling

        private void s_Play_OnClick(object sender, EventArgs e)
        {
            if (!BanchoClient.CheckAuth(false)) return;

            if (!AudioEngine.CheckAudioDevice()) return;

            if (currentTier == Tiers.Main)
                AudioEngine.PlaySamplePositional(@"menuhit");
            ChangeTier(Tiers.Play);
        }

        private void ChangeTier(Tiers load)
        {
            if (currentTier == load)
                return;

            bool backwards = load < currentTier;

            currentTier = load;

            foreach (KeyValuePair<Tiers, List<pSprite>> s in tierSprites)
            {
                if (currentTier == s.Key)
                {
                    foreach (pSprite p in s.Value)
                    {
                        p.Transformations.Clear();
                        p.Position = p.InitialPosition;
                        if (!p.Texture.assetName.Contains(@"-over"))
                            p.HandleInput = true;
                        if (p.TagNumeric == (int)currentTier)
                        {
                            p.FadeIn(80);
                            p.Position = p.InitialPosition + new Vector2(backwards ? 15 : -30, 0);
                            p.MoveTo(p.InitialPosition, 200, EasingTypes.Out);
                        }
                    }
                }
                else
                {
                    foreach (pSprite p in s.Value)
                    {
                        //p.MoveToRelative(new Vector2(backwards ? -30 : 5, 0), 200, EasingTypes.In);
                        p.Transformations.RemoveAll(t => t.Type == TransformationType.Movement);
                        p.FadeOut(160);
                        p.HandleInput = false;
                    }
                }
            }
        }


        private void fadeAllButtons(object sender)
        {
            foreach (KeyValuePair<Tiers, List<pSprite>> s in tierSprites)
            {
                foreach (pSprite p in s.Value)
                    if (sender == null || (p != sender && p != ((pSprite)sender).Tag))
                    {
                        p.Transformations.Clear();
                        p.MoveToRelative(new Vector2(-50, 0), 100);
                        p.FadeOut(100);
                    }
            }
        }

        private void s_Edit_OnClick(object sender, EventArgs e)
        {
            if (!BanchoClient.CheckAuth(false)) return;

            if (!AudioEngine.CheckAudioDevice()) return;

            if (CheckIdle()) return;

            Player.Mode = PlayModes.Osu;

            AudioEngine.PlaySamplePositional(@"menuhit");
            GameBase.ChangeMode(OsuModes.SelectEdit);

            fadeAllButtons(sender);
        }

        private void s_Options_OnClick(object sender, EventArgs e)
        {
            if (CheckIdle()) return;

            AudioEngine.PlaySamplePositional(@"menuhit");
            GameBase.Options.LoginOnly = false;
            GameBase.Options.Expanded = true;
        }

        private void s_Exit_OnClick(object sender, EventArgs e)
        {
            if (CheckIdle()) return;

            AudioEngine.PlaySamplePositional(@"menuhit");
            GameBase.BeginExit(ChatEngine.IsVisible);
        }

        private bool Game1_OnKeyPressed(object sender, Keys k)
        {
            if (currentTier == Tiers.Main)
            {
                switch (k)
                {
#if DEBUG
                    case Keys.C:
                        BanchoClient.Connect();
                        return true;
#endif
                    case Keys.P:
                        CheckIdle(true);
                        s_Play_OnClick(null, null);
                        return true;
                    case Keys.E:
                        s_Edit_OnClick(null, null);
                        return true;
                    case Keys.O:
                        s_Options_OnClick(null, null);
                        return true;
                    case Keys.Escape:
                    case Keys.Q:
                        GameBase.BeginExit(true);
                        return true;
                    case Keys.D:
                        startOsuDirect(null, null);
                        return true;
                    case Keys.F:
                        ConfigManager.sFpsCounter.Toggle();
                        return true;
                }
            }
            else
            {
                if (CheckIdle()) return false;

                switch (k)
                {
                    case Keys.P:
                        s_Osu_OnClick(null, null);
                        return true;
                    case Keys.M:
                        multiplayer_OnClick(null, null);
                        return true;
                    case Keys.Escape:
                    case Keys.B:
                        back_OnClick(null, null);
                        return true;
                }
            }

            return false;
        }

        private void startOsuDirect(object sender, EventArgs e)
        {
            if ((BanchoClient.Permission & Permissions.Supporter) == 0)
                return;

            AudioEngine.PlaySamplePositional(@"menuhit");
            GameBase.ChangeMode(OsuModes.OnlineSelection);
        }

        #endregion

        List<pSprite> updateSprites = new List<pSprite>();
        pSprite updateIcon;
        private pSprite initialFade;
        private pSprite initialFadeWelcome;
        private float audioVelocity;
        private float audioVelocityAverage;
        private AudioTrack trackPiano;
        private Beatmap welcomeMap;

        public static int IntroLength = 2400;
        private pSprite topBehind;
        private pSprite bottomBehind;
        private pText menuTip;

        void updateIconAnimation1()
        {
            updateIcon.Transformations.RemoveAll(t => t.Type == TransformationType.Rotation);
            updateIcon.Transformations.Add(new Transformation(TransformationType.Rotation, updateIcon.Rotation, updateIcon.Rotation + (float)Math.PI * 1, GameBase.Time, GameBase.Time + 1500, EasingTypes.Out) { Loop = true, LoopDelay = 2500 });
            updateIcon.Transformations.Add(new Transformation(TransformationType.Rotation, updateIcon.Rotation + (float)Math.PI * 1, updateIcon.Rotation + (float)Math.PI * 2, GameBase.Time + 2500, GameBase.Time + 4000, EasingTypes.In) { Loop = true, LoopDelay = 2500 });
        }

        void updateIconAnimation2()
        {
            updateIcon.Transformations.RemoveAll(t => t.Type == TransformationType.Rotation);
            updateIcon.Transformations.Add(new Transformation(TransformationType.Rotation, updateIcon.Rotation, updateIcon.Rotation + (float)Math.PI * 2, GameBase.Time, GameBase.Time + 2000) { Loop = true });
        }

        private void ShowUpdateButton()
        {
            if (updateIcon != null) return;

            if (onlineImage != null)
                onlineImage.FadeOut(500);

            if (menuTip != null && menuTip.Text.Length > 60)
                menuTip.FadeOut(500);

            const float y_pos = 22;

            updateIcon =
                new pSprite(SkinManager.Load(@"menu-update", SkinSource.Osu), Fields.BottomCentre, Origins.Centre, Clocks.Game, new Vector2(0, y_pos), 0.96F, true, Color.TransparentWhite);
            updateIcon.FadeIn(500);

            updateSprites.Add(updateIcon);
            spriteManagerOverlay.Add(updateIcon);
            updateIconAnimation1();

            Transformation fade1 = new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + 1500, GameBase.Time + 2000) { Loop = true, LoopDelay = 4000 };
            Transformation fade2 = new Transformation(TransformationType.Fade, 0, 1, GameBase.Time + 3500, GameBase.Time + 4000) { Loop = true, LoopDelay = 4000 };

            pText updateText1 = new pText(LocalisationManager.GetString(OsuString.Update_ClickToUpdate1), 14, new Vector2(GameBase.WindowWidthScaled / 2, GameBase.WindowHeightScaled - y_pos), 0.97F, true, Color.White);
            updateTextApply(updateText1);
            updateText1.FadeInFromZero(500);
            updateText1.Transformations.Add(fade1.Clone());
            updateText1.Transformations.Add(fade2.Clone());
            spriteManagerOverlay.Add(updateText1);
            updateSprites.Add(updateText1);

            pText updateText2 = new pText(LocalisationManager.GetString(OsuString.Update_ClickToUpdate2), 14, new Vector2(GameBase.WindowWidthScaled / 2, GameBase.WindowHeightScaled - y_pos), 0.97F, true, Color.TransparentWhite);
            updateTextApply(updateText2);
            updateText2.Transformations.Add(fade1.CloneReverse());
            updateText2.Transformations.Add(fade2.CloneReverse());
            spriteManagerOverlay.Add(updateText2);
            updateSprites.Add(updateText2);
        }

        private void HideUpdateButton()
        {
            updateSprites.ForEach(s =>
                {
                    s.FadeOut(100);
                    s.AlwaysDraw = false;
                });
            updateSprites.Clear();

            if (onlineImage != null)
                onlineImage.FadeIn(500);

            updateIcon = null;
        }

        private void updateTextApply(pText t)
        {
            t.OnHover += delegate
            {
                updateSprites.ForEach(s =>
                {
                    s.FadeColour(Color.YellowGreen, 100);
                    s.ScaleTo(1.1f, 500, EasingTypes.Out);
                });

                updateIconAnimation2();
            };
            t.OnHoverLost += delegate
            {
                if (updateSprites.Find(s => s.Hovering) != null) return;

                updateSprites.ForEach(s =>
                {
                    s.FadeColour(Color.White, 100);
                    s.ScaleTo(1, 500, EasingTypes.Out);
                });

                updateIconAnimation1();
            };

            t.HandleInput = true;
            t.TextBorder = true;
            t.TextBold = true;
            t.BorderColour = new Color(0, 0, 0, 100);
            t.Origin = Origins.Centre;
            t.OnClick += delegate { GameBase.CompleteUpdate(true); };
        }
    }
}