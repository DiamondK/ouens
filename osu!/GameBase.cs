﻿#region Using Statements

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Win32;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Edit;
using osu.GameModes.Edit.Forms;
using osu.GameModes.Menus;
using osu.GameModes.Online;
using osu.GameModes.Options;
using osu.GameModes.Play;
using osu.GameModes.Ranking;
using osu.GameModes.Select;
using osu.GameplayElements;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Notifications;
using osu.Graphics.OpenGl;
using osu.Graphics.Primitives;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Helpers;
using osu.Helpers.Forms;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu.Online.Drawable;
using osu.Online.P2P;
using osu_common;
using osu_common.Helpers;
using osu_common.Libraries.NetLib;
using osudata;
using Un4seen.Bass;
using Color = Microsoft.Xna.Framework.Graphics.Color;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Menu = osu.GameModes.Menus.Menu;
using Point = Microsoft.Xna.Framework.Point;
using Rectangle = Microsoft.Xna.Framework.Rectangle;
using System.Security;
using ICSharpCode.SharpZipLib.Zip;
using System.Net.NetworkInformation;
using System.Security.Principal;
using System.Security.AccessControl;
using osu.GameModes.Play.Rulesets.Mania;
using System.Drawing.Imaging;
using osu.GameModes.Play.Components;
using osu_common.Updater;
using osu.Graphics.Tips;
using osu.Online.Social;

#endregion

namespace osu
{
    public partial class GameBase : Game
    {
        #region General

        internal static Bindable<string> BeatmapDirectory;

        internal static char[] arrived = { 'b', 'a', 'n', 'c', 'h', 'o' };

        private const float TooltipPadding = 10;
        private static readonly Vector2 TooltipPaddingVector = new Vector2(TooltipPadding, TooltipPadding);
        private static ChatEngine chatEngine;
        private static List<pDialog> DialogQueue = new List<pDialog>();
        internal static EditorControl EditorControl;
        internal static Form Form;
        private static NotifyIcon notifyIcon;
        private const double FrameAimTime = (double)1000 / 60;

        /// <summary>
        /// The time a frame took to draw compared to 60fps standard.
        /// </summary>
        internal static double FrameRatio;

        internal static Game Game;

        private static Control GameWindow;
        internal static GraphicsDeviceManager graphics;
        internal static GameBase Instance;
        internal static bool IsMinimized;
        internal static LineManager LineManager;
        internal static ILineRenderer LineRenderer;
        internal static bool MenuVisible;
        internal static readonly NumberFormatInfo nfi = new CultureInfo(@"en-US", false).NumberFormat;
        internal static int PixelShaderVersion;
        internal static PrimitiveBatch primitiveBatch;
        internal static Random random = new Random();
        internal static pSprite s_fadeScreen;
        internal const double SIXTY_FRAME_TIME = (double)1000 / 60;
        internal double FrameLimitMillisecondsPerFrame
        {
            get
            {
                switch (ConfigManager.sFrameSync.Value)
                {
                    case FrameSync.Limit240:
                        return 1000d / ConfigManager.sCustomFrameLimit.Value;
                    case FrameSync.CompletelyUnlimited:
                        return 0.01;
                    case FrameSync.Unlimited:
                        return Player.Playing ? 0.01 : 1000 / 120d;
                    case FrameSync.Limit120:
                        return 1000 / 120d;
                }

                return 1000 / 60d;
            }
        }

        internal static bool SixtyFramesPerSecondFrame;
        internal static double SixtyFramesPerSecondLength;
        internal static SpriteBatch spriteBatch;
        internal static SpriteManager spriteManager;
        internal static SpriteManager spriteManagerCursor;
        internal static SpriteManager spriteManagerOverlay;
        internal static SpriteManager spriteManagerOverlayHighest;
        internal static SpriteManager spriteManagerTransition;
        internal static SpriteManager spriteManagerHighest;
        internal static Stopwatch stopWatch = new Stopwatch();
        internal static int Time;
        internal static pTexture WhitePixel;
        internal static bool PendingModeChange = true;
        internal static double TimeAccurateLastUpdate;
        private static double SixtyFrameLast;
        private static double TimeAccurate;
        private static double AccumulatedSleepError;

        internal static OnScreenDisplay OnScreenDisplay;

        internal static PhysicsManager PhysicsManager = new PhysicsManager();
        private bool toolTipDisplaying;
        internal static BanchoStatusOverlay BanchoStatus;

        public static bool IsFirstInstance;

        internal static VolumeControlSet Volume;

        internal static bool Tournament;
        internal static int TournamentClientId;
        internal static bool TournamentManager;
        internal static pText TourneySpectatorName;
        internal static bool TournamentBuffering;
        internal static bool TournamentSkipCalculations;

        internal static TransitionManager TransitionManager;

        internal static double ModeTime;

        internal GameBase(string startupFile)
        {
            StartupNewFiles = startupFile;

            Application.EnableVisualStyles();

            Form = (Form)Control.FromHandle(Window.Handle);

            string exeName = Environment.CurrentDirectory + @"\" + Process.GetCurrentProcess().ProcessName.Replace(@".vshost", string.Empty) + @".exe";

            try
            {
                Form.Icon = Icon.ExtractAssociatedIcon(exeName);
            }
            catch
            {
            }

            Form.AllowDrop = true;

            Form.FormClosing += Form_FormClosing;
            Form.DragEnter += Form_DragEnter;
            Form.DragDrop += Form_DragDrop;
            Form.Deactivate += Form_Deactivate;
            Form.Activated += Form_Activated;
            Form.SizeChanged += Form_SizeChanged;
            FadeOutComplete += ChangeModeCallback;

            MainThread = Thread.CurrentThread;

            Game = this;
            Instance = this;

            ConfigManager.Initialize();

            //If we have failed to update more than five times in a row, let's force an update.
            if (ConfigManager.sUpdateFailCount == 5)
            {
                ConfigManager.sUpdateFailCount.Value = 0;
                ConfigManager.SaveConfig();

                osuMain.Repair();
            }

            LocalisationManager.SetLanguage(ConfigManager.sLanguage, ConfigManager.sLastVersion != General.BUILD_NAME, delegate
            {
                if (GameBase.Time > 0)
                {
                    Scheduler.AddDelayed(delegate
                    {
                        ConfigManager.sLanguage.Value = LocalisationManager.CurrentLanguage;
                    }, Math.Max(0, (Menu.IntroLength + 500) - GameBase.Time));

                    return;
                }

                ConfigManager.sLanguage.Value = LocalisationManager.CurrentLanguage;

            }, osu.Properties.Resources.en);

            KeyboardState keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.LeftShift) || keyState.IsKeyDown(Keys.RightShift))
            {
                Helpers.Forms.Configuration configForm = new Helpers.Forms.Configuration();
                configForm.ShowDialog();
            }

            if (osuMain.IsWine)
            {
                //forced wine settings.
                ConfigManager.sRenderer.Value = @"d3d";
                ConfigManager.sFrameSync.Value = FrameSync.Limit120;
            }

            if (ConfigManager.sRenderer == @"opengl")
                OGL = true;
            else
                D3D = true;

            XnaRendererEnabled = D3D;

            graphics = new GraphicsDeviceManager(this);
            graphics.SynchronizeWithVerticalRetrace = ConfigManager.VSync;
            graphics.PreparingDeviceSettings += graphics_PreparingDeviceSettings;

            if (ConfigManager.Configuration.ContainsKey(@"GraphicsFix"))
            {
                graphics.PreferredDepthStencilFormat = DepthFormat.Depth16;
                graphics.PreferredBackBufferFormat = SurfaceFormat.Bgr565;
            }

            content = new ResourceContentManager(Services, ResourcesStore.ResourceManager);

            InitialDesktopResolution = ResolutionHelper.GetDesktopResolution();
            InitialDesktopLocation = ResolutionHelper.GetDesktopPosition();

            InitializeSound();
        }

        void Form_SizeChanged(object sender, EventArgs e)
        {
            if (Form.WindowState == FormWindowState.Maximized)
            {
                //switch to borderless fullscreen.

                Form.WindowState = FormWindowState.Normal;
                SetScreenSize(null, null, ScreenMode.BorderlessWindow, true);
            }
        }

        internal delegate string DllRegisterServerInvoker();

        internal static void InitializeSound()
        {
            AudioEngine.SetAudioDevice(ConfigManager.sAudioDevice.Value);

            if (Tournament)
                AudioEngine.VolumeMaster.Value = 0;
        }

        internal static bool MenuActive
        {
            get { return localMenuActive; }
            set
            {
                if (localMenuActive == value) return;

                localMenuActive = value;
                Form.TopMost = value;
                MouseManager.UpdateMouseCursorVisibility();
                MouseManager.ResetStatus();
            }
        }

        private static void graphics_PreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            if (e.GraphicsDeviceInformation.PresentationParameters.IsFullScreen)
                e.GraphicsDeviceInformation.PresentationParameters.FullScreenRefreshRateInHz = ConfigManager.sRefreshRate;
        }

        private static int bossKeyRestoreVolume = 0;
        internal static bool OnKeyPressed(object sender, Keys k)
        {
            Bindings bind = BindingManager.CheckKey(k);

#if DEBUG
            if (SpriteManager.Placement && SpriteManager.ClickHandledSprite != null)
            {
                float increment = 1;
                if (KeyboardHandler.ShiftPressed)
                    increment /= 5f;
                else if (KeyboardHandler.ControlPressed)
                    increment *= 5f;

                pSprite p = SpriteManager.ClickHandledSprite;
                pText pt = p as pText;

                switch (k)
                {
                    case Keys.Left:
                        p.Position.X -= increment;
                        break;
                    case Keys.Right:
                        p.Position.X += increment;
                        break;
                    case Keys.Up:
                        p.Position.Y -= increment;
                        break;
                    case Keys.Down:
                        p.Position.Y += increment;
                        break;
                    case Keys.Z:
                        if (pt != null)
                            pt.TextSize -= increment;
                        else
                            p.Scale -= increment / 100f;
                        break;
                    case Keys.X:
                        if (pt != null)
                            pt.TextSize += increment;
                        else
                            p.Scale += increment / 100f;
                        break;
                    case Keys.H:
                        p.Bypass = !p.Bypass;
                        break;
                }
            }
#endif

            switch (bind)
            {
                case Bindings.Screenshot:
                    Screenshot.TakeScreenshot(KeyboardHandler.ShiftPressed);
                    return true;
                case Bindings.DisableMouseButtons:
                    ConfigManager.sMouseDisableButtons.Toggle();
                    NotificationManager.ShowMessageMassive("Mouse buttons are " + (ConfigManager.sMouseDisableButtons ? "disabled" : "enabled") + @".", 1000);
                    return true;
                case Bindings.ToggleFrameLimiter:
                    switch (ConfigManager.sFrameSync.Value)
                    {
                        case FrameSync.VSync:
                            ConfigManager.sFrameSync.Value = FrameSync.Limit120;
                            break;
                        case FrameSync.LowLatencyVSync:
                            ConfigManager.sFrameSync.Value = FrameSync.Limit120;
                            break;
                        case FrameSync.Limit120:
                            ConfigManager.sFrameSync.Value = FrameSync.Limit240;
                            break;
                        case FrameSync.Limit240:
                            ConfigManager.sFrameSync.Value = FrameSync.Unlimited;
                            break;
                        case FrameSync.Unlimited:
                            ConfigManager.sFrameSync.Value = FrameSync.CompletelyUnlimited;
                            break;
                        default:
                            ConfigManager.sFrameSync.Value = FrameSync.Limit120;
                            break;
                    }
                    return true;
                case Bindings.BossKey:
                    MinimizedToTray = true;
                    return true;
            }

            switch (k)
            {
                case Keys.Enter:
                    if (KeyboardHandler.AltPressed)
                    {
                        if (OGL) return false;

                        //toggle between fullscreen
                        switch (Mode)
                        {
                            case OsuModes.Menu:
                            case OsuModes.SelectPlay:
                            case OsuModes.Lobby:
                                ConfigManager.sFullscreen.Toggle();
                                return true;
                        }

                        return false;
                    }

                    break;
                case Keys.S:
                    if (Mode != OsuModes.Play && Mode != OsuModes.Edit && KeyboardHandler.ShiftPressed && KeyboardHandler.AltPressed && KeyboardHandler.ControlPressed)
                    {
                        SkinManager.Reload(true);
                        GameBase.OnNextModeChange += delegate { SkinManager.LoadSkin(null, true); };
                        if (GameBase.ModeCanReload)
                        {
                            GameBase.ChangeMode(GameBase.Mode, true);
                        }
                        NotificationManager.ShowMessageMassive("Skin reloaded.", 2000);
                        return true;
                    }
                    break;

                case Keys.R:
                    if (KeyboardHandler.ControlPressed && Tournament)
                    {
                        User u = StreamingManager.CurrentlySpectating;

                        if (u == null)
                            u = BanchoClient.GetUserByName(TourneySpectatorName.Text);

                        if (u != null)
                        {
                            StreamingManager.StopSpectating(true);
                            StreamingManager.StartSpectating(u);
                        }
                    }

                    break;
#if DEBUG
                case Keys.OemTilde:
                    Native.AllocConsole();
                    Thread t = new Thread(() =>
                    {
                        InterpreterConsole.Run();
                    });
                    t.IsBackground = true;
                    t.Start();
                    return true;
                case Keys.F11:
                    if (GameBase.OnScreenDisplay != null)
                    {
                        GameBase.OnScreenDisplay.ToggleShow();
                        return true;
                    }
                    break;
                case Keys.D0:
                    if (KeyboardHandler.ControlPressed)
                    {
                        BanchoClient.SwitchServer();
                        return true;
                    }
                    break;
                case Keys.G:
                    if (KeyboardHandler.ControlPressed)
                        GC.Collect();
                    break;
#endif
            }

            return false;
        }

        static int owcCount = 0;

        private static void Form_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                Array a = (Array)e.Data.GetData(DataFormats.FileDrop);

                OsuModes gs = OsuModes.Unknown;
                if (a != null)
                {
                    string[] files = new string[a.Length];
                    for (int i = 0; i < a.Length; i++)
                        files[i] = a.GetValue(i).ToString();

                    gs = ReceiveFile(files);
                }
                else
                {
                    string text = e.Data.GetData(DataFormats.Text) as string;
                    if (text.StartsWith(@"http"))
                        ChatEngine.HandleLink(text);
                    else
                        gs = ReceiveFile(text);
                }

                if (gs != OsuModes.Unknown)
                    ChangeMode(gs, true);
            }
            catch (Exception)
            {
            }
        }

        private static void Form_DragEnter(object sender, DragEventArgs e)
        {
            bool isFile = e.Data.GetDataPresent(DataFormats.FileDrop);
            bool isUrl = e.Data.GetDataPresent(DataFormats.Text);
            e.Effect = isFile || isUrl ? DragDropEffects.Copy : DragDropEffects.None;
        }

        private void Form_Deactivate(object sender, EventArgs e)
        {
            if (Tournament) return;

            if (Player.Playing && !InputManager.ReplayMode)
            {
                Form.BringToFront();
                Form.Focus();
                Form.TopMost = true;
            }
            else
                Form.TopMost = false;


            if (OnFocusLost != null)
                OnFocusLost();
        }

        void Form_Activated(object sender, EventArgs e)
        {
            if (OnFocus != null)
                OnFocus();
        }

        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ChatEngine.IsFullVisible && ChatEngine.IsVisible)
            {
                ChatEngine.Toggle();
                e.Cancel = true;
                return;
            }

            switch (Mode)
            {
                case OsuModes.Rank:
                    ChangeMode(OsuModes.SelectPlay);
                    e.Cancel = true;
                    return;
                case OsuModes.RankingVs:
                case OsuModes.RankingTeam:
                case OsuModes.RankingTagCoop:
                    ChangeMode(OsuModes.MatchSetup);
                    e.Cancel = true;
                    return;
                case OsuModes.Edit:
                    Editor editor = RunningGameMode as Editor;
                    if (editor != null)
                        editor.Exit();
                    e.Cancel = true;
                    return;
                case OsuModes.Play:
                    StreamingManager.StopSpectating();

                    if (RunningGameMode is PlayerVs)
                        MatchSetup.LeaveGame();
                    else if (TestMode)
                        ChangeMode(OsuModes.Edit);
                    else
                        ChangeMode(OsuModes.SelectPlay);
                    e.Cancel = true;
                    return;
                case OsuModes.Lobby:
                case OsuModes.OnlineSelection:
                case OsuModes.SkinSelect:
                case OsuModes.OptionsOffsetWizard:
                    ChangeMode(OsuModes.Menu);
                    e.Cancel = true;
                    return;
                case OsuModes.MatchSetup:
                    ChangeMode(OsuModes.Lobby);
                    e.Cancel = true;
                    return;
            }

            if (Mode != OsuModes.Exit && Mode != OsuModes.Update)
            {
                BeginExit(false);
                //Sleepable = false;
                e.Cancel = true;
            }
        }

        internal static event VoidDelegate OnNextModeChange;
        internal static event VoidDelegate OnModeChange;

        internal static event VoidDelegate OnExitFade;

        internal static event VoidDelegate OnFocus;
        internal static event VoidDelegate OnFocusLost;

        private static event VoidDelegate exitAction;
        protected override void OnExiting(object sender, EventArgs args)
        {
            if (exitAction != null) exitAction();

            BanchoClient.Exit();

            ImHelper.SetAllMusic(false, string.Empty, string.Empty, string.Empty, string.Empty);

            if (Mode == OsuModes.Edit)
            {
                try
                {
                    ((Editor)RunningGameMode).Exit();
                }
                catch (Exception)
                {
                }
            }

            try
            {
                ConfigManager.SaveConfig();
            }
            catch { }

            BeatmapManager.DatabaseSerialize();
            PresenceCache.DatabaseSerialize();

            try
            {
                ScoreManager.SaveToDisk();
            }
            catch { }

            try
            {
                CollectionManager.SaveToDisk();
            }
            catch { }

            try
            {
                if (Mode == OsuModes.Play && !InputManager.ReplayMode)
                    Player.Instance.HandleScoreSubmission();
            }
            catch { }



            Screensaver.Restore();

            spriteManager.Dispose();
            spriteManagerOverlay.Dispose();
            spriteManagerOverlayHighest.Dispose();

            if (ConfigManager.sWiimote)
                WiimoteManager.Disconnect();

            Cursor.Show();
            Cursor.Clip = InputManager.CursorOriginalBounds;

            try
            {
                Directory.Delete(@"Data\a", true);
            }
            catch
            {
            }
#if P2P
            Osup2pManager.Shutdown();
#endif

            try
            {
                Bass.BASS_Free();
            }
            catch { }

            InputManager.Dispose();

            base.OnExiting(sender, args);
        }

        internal static event VoidDelegate OnResolutionChange;

        static VoidDelegate pendingScreenModeChange;
        internal static ScreenMode CurrentScreenMode;
        internal static void SetScreenSize(int? width = null, int? height = null, ScreenMode? mode = null, bool saveToConfig = false, bool force = false)
        {
            if (width == WindowWidth && height == WindowHeight && mode == CurrentScreenMode && !force) return;

            if (pendingScreenModeChange == null && !PendingModeChange && !force)
            {
                //Find the current screen parameters
                InitialDesktopResolution = ResolutionHelper.GetDesktopResolution();
                InitialDesktopLocation = ResolutionHelper.GetDesktopPosition();

                pendingScreenModeChange = delegate { SetScreenSize(width, height, mode, saveToConfig); };
                ChangeMode(Mode, true);
                return;
            }

            bool wasHighRes = UseHighResolutionSprites;
            Size desktop = InitialDesktopResolution;
            System.Drawing.Point desktopLocation = InitialDesktopLocation;

            CurrentScreenMode = mode ?? ConfigManager.sScreenMode;

            if (width != null && height != null)
            {
                //if width and height are specified, we should use them.
                WindowWidth = width.Value;
                WindowHeight = height.Value;

                if (saveToConfig)
                {
                    switch (CurrentScreenMode)
                    {
                        case ScreenMode.Fullscreen:
                            ConfigManager.sWidthFullscreen.Value = WindowWidth;
                            ConfigManager.sHeightFullscreen.Value = WindowHeight;
                            break;
                        case ScreenMode.BorderlessWindow:
                        case ScreenMode.Windowed:
                            ConfigManager.sWidth.Value = WindowWidth;
                            ConfigManager.sHeight.Value = WindowHeight;
                            break;
                    }
                }
            }
            else
            {
                switch (CurrentScreenMode)
                {
                    case ScreenMode.Windowed:
                        WindowWidth = ConfigManager.sWidth;
                        WindowHeight = ConfigManager.sHeight;
                        break;
                    case ScreenMode.Fullscreen:
                        WindowWidth = ConfigManager.sWidthFullscreen;
                        WindowHeight = ConfigManager.sHeightFullscreen;
                        break;
                    case ScreenMode.BorderlessWindow:
                        WindowWidth = desktop.Width;
                        WindowHeight = desktop.Height;
                        break;
                }

                if (WindowWidth == 9999 || WindowHeight == 9999)
                {
                    saveToConfig = true;

                    //A fresh config file. Set some sane defaults based on the environment.
                    switch (CurrentScreenMode)
                    {
                        case ScreenMode.Fullscreen:
                            Size s = ResolutionHelper.FindNativeResolution();
                            WindowWidth = ConfigManager.sWidthFullscreen.Value = s.Width;
                            WindowHeight = ConfigManager.sHeightFullscreen.Value = s.Height;
                            break;
                        case ScreenMode.Windowed:
                        case ScreenMode.BorderlessWindow:
                            WindowWidth = ConfigManager.sWidth.Value = desktop.Width;
                            WindowHeight = ConfigManager.sHeight.Value = desktop.Height;
                            break;
                    }
                }
            }

            if (CurrentScreenMode != ScreenMode.Fullscreen)
            {
                //sanity check to make sure we don't try and draw borders on a fullscreen window.
                CurrentScreenMode = WindowWidth == desktop.Width && WindowHeight == desktop.Height ? ScreenMode.BorderlessWindow : ScreenMode.Windowed;

                //if (CurrentScreenMode != ConfigManager.sScreenMode && WindowWidth == ConfigManager.sWidth && WindowHeight == ConfigManager.sHeight)
                //    saveToConfig = true; //save this override to config.
            }

            if (saveToConfig) ConfigManager.sScreenMode.Value = CurrentScreenMode;

            RefreshRate = OGL ? ResolutionHelper.GetCurrentRefreshRate() : graphics.GraphicsDevice.DisplayMode.RefreshRate;

            //limit the aspect ratio from being too square (just causes breaking).
            if (WindowHeight > WindowWidth * 0.8f) WindowHeight = (int)(WindowWidth * 0.8f);

            WindowHeight -= WindowOffsetY;
            if (!D3D) WindowOffsetY = 0;

            //setup window borders
            switch (CurrentScreenMode)
            {
                case ScreenMode.Fullscreen:
                    Form.FormBorderStyle = FormBorderStyle.None;
                    Form.Invalidate();
                    break;
                case ScreenMode.BorderlessWindow:
                    Form.FormBorderStyle = FormBorderStyle.None;
                    Form.Invalidate();
                    GameBase.Scheduler.AddDelayed(delegate { Form.Location = desktopLocation; }, 250);
                    break;
                case ScreenMode.Windowed:
                    if (!Tournament)
                    {
                        Form.FormBorderStyle = FormBorderStyle.FixedSingle;
                        Form.Invalidate();
                    }
                    break;
            }

            graphics.PreferredBackBufferWidth = WindowWidth;
            graphics.PreferredBackBufferHeight = WindowHeight + (GameBase.MenuVisible ? GameBase.EditorControl.Height : 0);
            graphics.IsFullScreen = CurrentScreenMode == ScreenMode.Fullscreen;
            graphics.ApplyChanges();
            if (D3D)
            {
                WindowWidth = graphics.GraphicsDevice.PresentationParameters.BackBufferWidth;
                WindowHeight = graphics.GraphicsDevice.PresentationParameters.BackBufferHeight - (GameBase.MenuVisible ? GameBase.EditorControl.Height : 0);
            }

            //setup window borders
            switch (CurrentScreenMode)
            {
                case ScreenMode.Fullscreen:
                    Form.FormBorderStyle = FormBorderStyle.None;
                    Form.Invalidate();
                    break;
                case ScreenMode.BorderlessWindow:
                    Form.FormBorderStyle = FormBorderStyle.None;
                    Form.Invalidate();
                    GameBase.Scheduler.AddDelayed(delegate { Form.Location = desktopLocation; }, 250);
                    break;
                case ScreenMode.Windowed:
                    if (!Tournament)
                    {
                        Form.FormBorderStyle = FormBorderStyle.FixedSingle;
                        Form.Invalidate();
                    }
                    break;
            }

            if (CurrentScreenMode != ScreenMode.Fullscreen && AeroGlassHelper.GlassEnabled)
            {
                GameBase.Scheduler.AddDelayed(delegate
                {
                    AeroGlassHelper.DisableGlass(GameWindow);
                    AeroGlassHelper.EnableGlass(GameWindow);
                }, 250);
            }

            WindowRatio = (float)WindowHeight / WindowDefaultHeight;
            WindowRatioScaleDown = 1 / WindowRatio;
            WindowRatioInverse = (float)WindowHeight / WindowSpriteRes;

            NativeText.ClearFontCache();

            InputManager.TextInput.Location = new System.Drawing.Point((int)(WindowWidth * 0.015), WindowHeight - (int)(WindowHeight * 0.052) + 2000);
            foreach (TextInputControl c in InputManager.TextInputControls)
                c.SetLocation();

            if (SkinManager.SliderRenderer != null)
            {
                SkinManager.SliderRenderer.Dispose();
                SkinManager.SliderRenderer = null;
            }

            SkinManager.ComputeColours();

            if (LastMode != OsuModes.Unknown && UseHighResolutionSprites != wasHighRes)
                SkinManager.Reload(true);

            if (EditorControl != null)
                EditorControl.Width = WindowWidth;

            if (s_fadeScreen != null)
                s_fadeScreen.VectorScale = new Vector2(WindowWidth, WindowHeight + WindowOffsetY);

            ResizeGamefield(GamefieldSize, true);

            WindowRectangle = new Rectangle(-1 + WindowOffsetX, -1, WindowWidth - WindowOffsetX * 2 + 1, WindowHeight + WindowOffsetY + 1);
            WindowRectangleWidescreen = new Rectangle(-1, -1, WindowWidth + 1, WindowHeight + WindowOffsetY + 1);

            SpriteManager.ViewRectangleCurrent.Width = (int)Math.Ceiling(WindowWidth / WindowRatio);
            SpriteManager.ViewRectangleFull = new Rectangle(0, 0, WindowWidth, WindowHeight + WindowOffsetY);

#if !Public
            if (testBuildOverlay != null)
                testBuildOverlay.VectorScale.X = Math.Max(1, (GameBase.WindowWidth / GameBase.WindowRatioInverse) / testBuildOverlay.DrawWidth);
#endif

            if (D3D) HitObjectManager.UpdateProjectionMatrix();

            if (OGL)
            {
                switch (CurrentScreenMode)
                {
                    case ScreenMode.Fullscreen:
                        Resolution.ChangeResolution(WindowWidth, WindowHeight);
                        break;
                    default:
                        Resolution.ChangeResolution(desktop.Width, desktop.Height);
                        break;
                }

                GlControl.ResizeGL(null, null);
            }

            if (chatEngine != null) ChatEngine.OnResolutionChange();

            UpdateClientBounds();

            if (OnResolutionChange != null)
                OnResolutionChange();
        }

        internal static int GetTime(Clocks clock)
        {
            switch (clock)
            {
                case Clocks.Audio:
                case Clocks.AudioOnce:
                    return AudioEngine.Time;
                case Clocks.Game:
                    return Time;
            }

            return 0;
        }

        internal static string clientHash;

        static Thread StartupOperationThread;

        protected override void Initialize()
        {
#if DEBUG
            if (!Tournament || TournamentManager) OnScreenDisplay = new OnScreenDisplay();
            Debug.Print(@"Running on " + Environment.OSVersion);
#endif

            //Tourney Initialisation
            if (Tournament)
            {
                General.SUBVERSION = @"tourney";

                AudioEngine.VolumeMaster.Value = 35;
                AudioEngine.VolumeMusic.Value = TournamentManager ? 100 : 0;
                AudioEngine.VolumeEffect.Value = TournamentClientId == 0 ? 100 : 0;

                Form.FormBorderStyle = FormBorderStyle.None;
                string tourneyServer = GameModes.Other.Tournament.Config.GetValue(@"privateserver", "");
                if (!string.IsNullOrEmpty(tourneyServer))
                    BanchoClient.SetServer(new[] { tourneyServer });

                Scheduler.Add(delegate
                {
                    Form.StartPosition = FormStartPosition.Manual;
                    if (TournamentManager)
                    {
                        Form.Location = new System.Drawing.Point(0, 0);
                    }
                    else
                    {
                        Vector2 pos = osu.GameModes.Other.Tournament.PositionForClient(TournamentClientId, true);
                        Form.Location = new System.Drawing.Point((int)pos.X, (int)pos.Y);
                        Form.TopMost = true;
                    }
                }, true);
            }

            StartupOperationThread = new Thread((ThreadStart)delegate
            {
                if (!Tournament)
                {
                    CheckFilePermissions();
                    HandleRegistryAssociations(false);
                    Screensaver.Disable();
                }

                IPC.AcceptConnections(IpcChannelName);
            });

            NewGraphicsAvailable = File.Exists(@"osu!gameplay.dll") && File.Exists(@"osu!ui.dll");

            NativeText.Initialize();

            Player.Mode = SongSelection.LastSelectMode;

            StartupOperationThread.IsBackground = true;
            StartupOperationThread.Start();

            GameWindow = Control.FromHandle(Window.Handle);

            string displayName = @"\\.\DISPLAY" + ConfigManager.sDisplay;

            Game.Window.ScreenDeviceNameChanged += Window_ScreenDeviceNameChanged;
            Form.LocationChanged += OnLocationChanged;
            Form.SizeChanged += OnLocationChanged;
            Form.ResizeEnd += OnLocationChanged;
            Form.Activated += OnLocationChanged;
            Form.ClientSizeChanged += OnLocationChanged;

            Activated += delegate
            {
                if (!GameBase.OGL || ConfigManager.sScreenMode != ScreenMode.Fullscreen)
                    return;
                GameBase.Scheduler.Add(delegate
                {
                    if (InitialDesktopResolution.Width != ConfigManager.sWidthFullscreen || InitialDesktopResolution.Height != ConfigManager.sHeightFullscreen)
                        Resolution.ChangeResolution(ConfigManager.sWidthFullscreen, ConfigManager.sHeightFullscreen);
                });
            };

            Deactivated += delegate
            {
                if (!GameBase.OGL || ConfigManager.sScreenMode != ScreenMode.Fullscreen)
                    return;
                GameBase.Scheduler.Add(delegate
                {
                    Form.WindowState = FormWindowState.Minimized;
                    if (InitialDesktopResolution.Width != ConfigManager.sWidthFullscreen || InitialDesktopResolution.Height != ConfigManager.sHeightFullscreen)
                        Resolution.ChangeResolution(InitialDesktopResolution.Width, InitialDesktopResolution.Height);
                });
            };

            if (displayName != Screen.PrimaryScreen.DeviceName)
            {
                try
                {
                    Game.Window.BeginScreenDeviceChange(ConfigManager.sScreenMode == ScreenMode.Fullscreen);
                    Game.Window.EndScreenDeviceChange(displayName, ConfigManager.sWidth, ConfigManager.sHeight);
                }
                catch
                {
                    try
                    {
                        ConfigManager.sDisplay.Value = Int32.Parse(Screen.PrimaryScreen.DeviceName[Screen.PrimaryScreen.DeviceName.Length - 1].ToString());
                    }
                    catch
                    {
                    }
                }

                SystemResolutionWidth = Screen.PrimaryScreen.Bounds.Width;
                SystemResolutionHeight = Screen.PrimaryScreen.Bounds.Height;
            }

            try
            {
                if (!Directory.Exists(GameBase.BeatmapDirectory))
                    Directory.CreateDirectory(GameBase.BeatmapDirectory);

                if (!Directory.Exists(@"Skins"))
                    Directory.CreateDirectory(@"Skins");

                if (!Directory.Exists(@"Data"))
                    Directory.CreateDirectory(@"Data");

                if (!Directory.Exists(@"Data/r"))
                    Directory.CreateDirectory(@"Data/r");

                if (!Directory.Exists(@"Data/a"))
                    Directory.CreateDirectory(@"Data/a");

                File.Delete(@"error_update.txt");
            }
            catch { }

            try
            {
                Font f = new Font(General.FONT_FACE_DEFAULT, 10);
            }
            catch (Exception e)
            {
                TopMostMessageBox.Show("You don't seem to have the Windows default font 'Tahoma' installed.  Please obtain and install this font!");
            }

            BeatmapManager.Initialize();

            if (OGL) InitializeGl(); //needs to be initialized before input to access necessary controls.

            InputManager.Initialize();

            spriteManagerOverlay = new SpriteManager(true);
            spriteManagerOverlay.HandleOverlayInput = true;

            spriteManagerOverlayHighest = new SpriteManager(true);
            spriteManagerOverlayHighest.HandleOverlayInput = true;

            spriteManagerTransition = new SpriteManager(true);

            spriteManager = new SpriteManager();
            spriteManagerCursor = new SpriteManager(true) { HandleInput = false };
            spriteManagerHighest = new SpriteManager(true);

            if (Tournament)
            {
                TourneySpectatorName = new pText(string.Empty, 90, new Vector2(3, 3), 1, true, Color.Yellow);
                TourneySpectatorName.Field = Fields.TopLeft;
                TourneySpectatorName.Origin = Origins.TopLeft;
                TourneySpectatorName.TextShadow = true;
                TourneySpectatorName.TextBold = true;
                TourneySpectatorName.OnRefreshTexture += delegate { TourneySpectatorName.Alpha = 1; };
                spriteManagerOverlayHighest.Add(TourneySpectatorName);

                Logger.Enabled = true;
            }

            //todo: fix this.  used temporarily for textbox alignment
            WindowRatio = (float)WindowHeight / WindowDefaultHeight;
            WhitePixel = pTexture.FromRawBytes(new byte[] { 255, 255, 255, 255 }, 1, 1);

            s_fadeScreen = new pSprite(WhitePixel, Fields.Native, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0, true, Color.Black);

#if !Public
            if (!Tournament)
            {
                testBuildOverlay = new pSprite(SkinManager.Load(@"test-build-overlay", SkinSource.Osu), Fields.BottomRight, Origins.BottomRight, Clocks.Game, Vector2.Zero, 1, true, Color.White);

                spriteManagerOverlayHighest.Add(testBuildOverlay);

                testBuildVersion = new pText(General.BUILD_NAME, 14, new Vector2(0, 0), 0.99f, true, Color.White);
                testBuildVersion.TextShadow = true;
                testBuildVersion.Alpha = 0.4f;
                testBuildVersion.Field = Fields.BottomCentre;
                testBuildVersion.Origin = Origins.BottomCentre;
                testBuildVersion.TextRenderSpecific = false;
                spriteManagerOverlayHighest.Add(testBuildVersion);
            }
#endif

            SetScreenSize();

            SkinManager.LoadSkin();
            AudioEngine.Initialize();

            TransitionManager = new TransitionManager();

            BanchoStatus = new BanchoStatusOverlay();

            spriteManagerTransition.Add(s_fadeScreen);

            float scale = DpiHelper.DPI(GameBase.Form) / 96.0f;
            toolTip = new pText(string.Empty, 11, Vector2.Zero, Vector2.Zero, 1, true, Color.White, false);
            toolTip.Origin = Origins.TopLeft;
            toolTip.Alpha = 0;
            toolTip.CornerBounds = new Vector4(4, 4, 4, 4);
            toolTip.Field = Fields.TopLeft;
            toolTip.BackgroundColour = new Color(10, 10, 10, 230);
            toolTip.BorderColour = new Color(80, 80, 80, 255);
            spriteManagerHighest.Add(toolTip);

            fpsDisplay = new pSpriteText(string.Empty, @"fps", 4, Fields.TopLeft, Origins.BottomRight, Clocks.Game, new Vector2(0, GameBase.WindowHeightScaled), 1, true, Color.White) { TextConstantSpacing = true };
            fpsDisplay.ExactCoordinates = true;

            if (ConfigManager.sFpsCounter)
                spriteManagerOverlayHighest.Add(fpsDisplay);

            if (D3D)
                PixelShaderVersion = graphics.GraphicsDevice.GraphicsDeviceCapabilities.PixelShaderVersion.Major;

            Options = new Options(this);

            osuDirect = new OsuDirect(this);
            chatEngine = new ChatEngine(this);

            Volume = new VolumeControlSet();

            MusicControl = new MusicControl(this);

#if Local
            if (!IsFirstInstance)
                ConfigManager.sUsername.Value = "General Pepper";
            else
                ConfigManager.sUsername.Value = "peppy";
#endif

            User = new User(ConfigManager.sUsername);
            User.DrawAt(new Vector2(0, 0), false, 0);

#if !ARCADE
            //For the time being let's make this not clickable for arcade.  Not sure whether this is required, though.
            User.Clickable = true;
            User.Sprites[0].KeepEventsBound = true;
            User.Sprites[0].OnClick += ShowLocalUserOptions;
#endif

            BanchoClient.Start();

            OsuModes gs = ReceiveFile(StartupNewFiles);

            GameQueuedState = gs != OsuModes.Unknown ? gs : GameQueuedState;

            if (BenchmarkMode)
            {
                BeatmapManager.Current = BeatmapManager.Beatmaps.Find(b => b.PlayMode == PlayModes.Osu);
                ModManager.ModStatus |= Mods.Autoplay;
                //ModManager.ModDoubleTime = true;
                GameQueuedState = OsuModes.Play;
            }

#if DEBUG
            InitializeTester();
#endif

            Components.Add(chatEngine);
            Components.Add(osuDirect);
            Components.Add(MusicControl);

            FadeState = FadeStates.WaitingLoad;
            ChangeModeCallback(this, null);

            IdleHandler.Initialize();

#if P2P
            //activate P2P system
            Osup2pManager.Initialize(new P2PSettings());
            Osup2pManager.Activate();
            //Osup2pManager.LoadMagnet("3oh!3 - I Can't Do It Alone.osz2.osuMagnet");
#endif

            BringToFront();

            base.Initialize();

            if (!string.IsNullOrEmpty(ConfigManager.sLastVersion))
            {
                ConfigManager.sUpdateFailCount.Value = 0;

                if (ConfigManager.sLastVersion != General.BUILD_NAME)
                {
                    SetUpdateState(UpdateStates.Completed);
                }
                else if (ConfigManager.sUpdatePending)
                {
                    SetUpdateState(UpdateStates.Completed);
                }
            }

            ConfigManager.sLastVersion.Value = General.BUILD_NAME;

            stopWatch.Start();

            BeatmapImport.Start();
        }

        private static string createUniqueCheck()
        {
            arrived[0]++;
            return CryptoHelper.GetMd5String(UniqueId + 0x08 + 0x200 + UniqueId2);
        }

        internal static string CreateUniqueId()
        {
            try
            {
                string guid = Uninstaller.GetUninstallId().ToString();
                arrived[1]++;
                UniqueId.Value = CryptoHelper.GetMd5String(guid);
            }
            catch { }

            try
            {
                arrived[2]++;
                UniqueId2.Value = CryptoHelper.GetMd5String(identifier("Win32_DiskDrive", "Signature", "SerialNumber"));
            }
            catch { }

            UniqueId2.allowDestruction = false;
            UniqueId.allowDestruction = false;

            UniqueCheck.Value = createUniqueCheck();

            return UniqueId + "|" + UniqueId2;
        }

        internal static bool CheckUniqueId()
        {
            if (GameBase.Time == 0) return true;
            return createUniqueCheck() == UniqueCheck.Value && UniqueId.c <= 3 + ChangeAllowance && UniqueId2.c <= 3 + ChangeAllowance;
        }

        internal static string identifier(string wmiClass, params string[] wmiProperty)
        //Return a hardware identifier
        {
            arrived[3]++;

            try
            {
                System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
                System.Management.ManagementObjectCollection moc = mc.GetInstances();
                foreach (System.Management.ManagementObject mo in moc)
                {
                    foreach (string w in wmiProperty)
                    {
                        try
                        {
                            object o = mo[w];
                            if (o == null)
                                continue;

                            return mo[w].ToString();
                        }
                        catch
                        {
                        }
                    }
                }
            }
            catch { }

            return string.Empty;
        }

        internal static void BringToFront()
        {
            Form.BringToFront();
            SetForegroundWindow(Form.Handle);
        }

        /// <summary> The RegisterHotKey function defines a system-wide hot key </summary>
        /// <param name="hwnd">Handle to the window that will receive WM_HOTKEY messages
        /// generated by the hot key.</param>
        /// <param name="id">Specifies the identifier of the hot key.</param>
        /// <param name="fsModifiers">Specifies keys that must be pressed in combination with the key
        /// specified by the 'vk' parameter in order to generate the WM_HOTKEY message.</param>
        /// <param name="vk">Specifies the virtual-key code of the hot key</param>
        /// <returns><c>true</c> if the function succeeds, otherwise <c>false</c></returns>
        /// <seealso cref="http://msdn.microsoft.com/en-us/library/ms646309(VS.85).aspx"/>
        [DllImport(@"user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

        #region fields
        private static int MOD_ALT = 0x1;
        private static int MOD_CONTROL = 0x2;
        private static int MOD_SHIFT = 0x4;
        private static int MOD_WIN = 0x8;
        private static int WM_HOTKEY = 0x312;
        #endregion

        private static void OnLocationChanged(object sender, EventArgs e)
        {
            UpdateClientBounds();
        }

        private static void UpdateClientBounds()
        {
            ClientBounds = Form.RectangleToScreen(Form.ClientRectangle);
        }

        internal static bool MouseVisible = true;

        internal static void HandleRegistryAssociations(bool runningElevated)
        {
            //ensure we only run this operation once before release (in the case it is actually run).
            if (!runningElevated && ConfigManager.sLastVersionPermissionsFailed.Value == General.BUILD_NAME)
                return;

            bool success = true;

            //make sure we've initialised our uniqueId here.
            if (Uninstaller.GetUninstallId() == Guid.Empty)
                success = false;

            if (!GeneralHelper.RegistryCheck("osu!", osuMain.FullPath))
            {
                success &= GeneralHelper.RegistryAdd("osz", "osu!", "osu! beatmap", osuMain.FullPath);
                if (success) success &= GeneralHelper.RegistryAdd("osz2", "osu!", "osu! beatmap v2", osuMain.FullPath);
                if (success) success &= GeneralHelper.RegistryAdd("osr", "osu!", "osu! replay", osuMain.FullPath);
                if (success) success &= GeneralHelper.RegistryAdd("osk", "osu!", "osu! skin", osuMain.FullPath);

                if (Registry.ClassesRoot.GetValue(string.Empty, string.Empty).ToString() == "osu") Registry.ClassesRoot.DeleteValue(string.Empty, false);
            }

            if (!GeneralHelper.RegistryCheck("osu", osuMain.FullPath) && !GeneralHelper.RegistryAdd(string.Empty, "osu", "osu! url handler", osuMain.FullPath, true))
                success = false;

            if (!Uninstaller.CreateUninstaller())
                success = false;

            if (!success && !runningElevated)
            {
                GameBase.SetPermissions(true);
                ConfigManager.sLastVersionPermissionsFailed.Value = General.BUILD_NAME;
            }

        }

        /// <summary>
        /// Permission test for vista crap shit fuck.
        /// </summary>
        private static void CheckFilePermissions()
        {
            OperatingSystem osInfo = Environment.OSVersion;
            if (osInfo.Platform != PlatformID.Win32NT || osInfo.Version.Major != 6)
                return;
            try
            {
                Directory.CreateDirectory(".test");
                Directory.Delete(".test");
            }
            catch (Exception)
            {
                SetPermissions(false);
                try
                {
                    if (Directory.Exists(".test"))
                        Directory.Delete(".test");
                }
                catch { }
            }
        }

        internal static void SetPermissions(bool registryOnly)
        {
            if (!osuMain.IsElevated)
            {
                Scheduler.AddDelayed(delegate
                {
                    osuMain.Elevate(registryOnly ? "-setassociations" : "-setpermissions", true);
                }, Math.Max(500, (Menu.IntroLength + 500) - GameBase.Time));

                return;
            }

            GameBase.HandleRegistryAssociations(true);

            if (registryOnly) return;

            osuMain.SetDirectoryPermissions();
        }

        private static void InitializeGl()
        {
            if (GlControl != null) return;

            GlControl = new GlControl();
            Form.Controls.Add(GlControl);

            Instance.LoadGraphicsContent(true);
        }

        private void Window_ScreenDeviceNameChanged(object sender, EventArgs e)
        {
            if (PendingModeChange)
                return;

            string deviceName = Screen.FromHandle(Game.Window.Handle).DeviceName;
            try
            {
                ConfigManager.sDisplay.Value = deviceName.IndexOf("2") >= 0 ? 2 : 1;
            }
            catch
            {
            }

            //if (D3D) Restart();
        }

        //todo: this can be optimised
        internal static bool HasLogin { get { return !string.IsNullOrEmpty(ConfigManager.sPassword) && !string.IsNullOrEmpty(ConfigManager.sUsername); } }

        internal static void ShowLocalUserOptions(object sender, EventArgs e)
        {
            if (!GameBase.HasLogin)
                ShowLogin();
            else
                UserProfile.DisplayProfileFor(User, false);
        }

        int tenSecondUpdate = 0;
        int[] lagTimes = new int[60];
        int lagCounterPos = -1;
        int lagMax = 20;

        private void UpdateLagTime(int lastFrameLag)
        {
            if (lagCounterPos == -1) ResetLagCounter();
            if (lagTimes[lagCounterPos] == lagMax)
            {
                lagTimes[lagCounterPos] = lastFrameLag;
                int max = 0;
                foreach (int x in lagTimes)
                {
                    max = Math.Max(max, x);
                    if (max == lastFrameLag) break;
                }
                lagMax = max;
            }
            else lagTimes[lagCounterPos] = lastFrameLag;
            lagCounterPos = (lagCounterPos + 1) % 60;
        }

        private void ResetLagCounter()
        {
            for (int x = 0; x < 60; x++)
                lagTimes[x] = 20;
            lagCounterPos = 0;
            lagMax = 20;
        }


        int lastThreadedSave;
        protected override void Update()
        {
            // Call the scheduler to give lower-priority background processes a chance to do stuff.
            Thread.Sleep(0);

#if !DEBUG && !PublicNoUpdate
            try
            {
#endif
            IsMinimized = Window.IsMinimized || (Window.ClientBounds.X < -10000 && Window.ClientBounds.Y < -10000);
            bool lowlag = ConfigManager.sFrameSync.Value == FrameSync.LowLatencyVSync && RefreshRate > 0;

            double startTime = 0.0d;

            Game.SkipDrawcall = IsMinimized && !Player.Playing && !Player.IsSpinning && !InputManager.ReplayStreaming;

            if ((IsMinimized || MenuActive || !IsActive || BackgroundLoading) && !Player.Playing && !Player.IsSpinning && !InputManager.ReplayStreaming && !InputManager.ReplayMode)
            {
                if (IdleTime > 30000 && GameBase.Time - lastThreadedSave > 120000 && GameBase.Time - lastThreadedSave > IdleTime)
                {
                    RunBackgroundThread(BackgroundSave, true);
                    lastThreadedSave = GameBase.Time;
                }

                Thread.Sleep(16);
            }

            else if (lowlag)
            {
                // wait as long as possible before processing input
                // so that the time between Present and vblank is as small as possible
                int wait = (1800 - lagMax * RefreshRate * 3) / (2 * RefreshRate);
                if ((wait > 0) && (wait <= 1000 / RefreshRate)) Thread.Sleep(wait);
            }
#if !DEBUG
                else
                    lagCounterPos = -1;

                if (lowlag)
#endif
            startTime = (double)stopWatch.ElapsedTicks / Stopwatch.Frequency * 1000;

            if (OnScreenDisplay != null) OnScreenDisplay.Update();

            UpdateTiming();

            Scheduler.Update();

            UpdateFpsCounter();

            UpdateDialogQueue();

            bool oldBloomPass = BloomRenderer.ExtraPass;
            ChildrenUpdated = false;
            BloomRenderer.ExtraPass = false;

            UpdateFadeState();

            PhysicsManager.Update();

            BanchoStatus.Update();

            TransitionManager.Update();

            Options.Update();

            Volume.Update();

            InputManager.UpdateFrame();

            if (!ChildrenUpdated)
                //In the case children are not updated, we need to remember the bloom pass state.
                //This happens when replays/auto are skipping frames (but we don't want the bloom pass to flicker during this).
                BloomRenderer.ExtraPass = oldBloomPass;

            if (SixtyFramesPerSecondFrame)
            {
                NotificationManager.Update();
                OsuTipManager.Update();

                if (Time / 10000 != tenSecondUpdate)
                {
                    tenSecondUpdate = Time / 10000;
                    SkinManager.Cleanup();
                }

                if (Time / 1000 != oneSecondUpdate)
                {
                    oneSecondUpdate = Time / 1000;
                    InputManager.CheckTextInputFocus(false);
                    UpdateSystemWideDisables();
                }
            }

#if !Release
            if (GameBase.BenchmarkMode && GameBase.Time > 120000)
                BeginExit(false);
#endif

#if !DEBUG && !PublicNoUpdate
                if (lowlag)
                    UpdateLagTime((int)(((double)stopWatch.ElapsedTicks / Stopwatch.Frequency * 1000) - startTime + 1.0d));
            }
            catch (Exception e)
            {
                if (ExceptionCount++ > 1 || Mode == OsuModes.Exit || GameQueuedState == OsuModes.Exit)
                    throw;
                softHandle(e);
            }
#endif

#if !Public
            if (testBuildOverlay != null && testBuildOverlay.Transformations.Count == 0)
            {
                bool hide = Player.Playing || ChatEngine.IsVisible || InputManager.ReplayMode;
                bool hideText = hide || (Mode == OsuModes.Menu && Menu.titleImageUrl != null);
                testBuildOverlay.MoveTo(new Vector2(0, hide ? -5 : 0), 500);
                testBuildVersion.FadeTo(hideText ? 0 : 0.4f, 500);
            }
#endif

            RunGraphicsThread(); //ensure the graphics thread is alive in the case it was killed unexpectedly and still has work.

            if (Tournament && !TournamentManager)
            {
                User u = StreamingManager.CurrentlySpectating;

                TourneySpectatorName.Text = u == null ? string.Empty : u.Name;

                if (GameBase.Time > 40000 && GameBase.Time - IPC.LastContact > 10000)
                    BeginExit();
            }
        }

        /// <summary>
        /// Saves databases to disk in the background when osu! has been idle for a while.
        /// </summary>
        private void BackgroundSave()
        {
            if (GameBase.Tournament) return;

            switch (Mode)
            {
                case OsuModes.Play:
                case OsuModes.BeatmapImport:
                case OsuModes.MatchSetup:
                    return;
            }

            lastThreadedSave = GameBase.Time;

            try
            {
                ConfigManager.SaveConfig();
                CollectionManager.SaveToDisk();
                ScoreManager.SaveToDisk();
                BeatmapManager.DatabaseSerialize();
                PresenceCache.DatabaseSerialize();
            }
            catch { }
        }

        private static void UpdateDialogQueue()
        {
            if (DialogQueue.Count > 0 && ActiveDialog == null && (!Player.Playing || (ChatEngine.IsVisible && ChatEngine.IsFullVisible)))
            {
                TextInputControl tic = InputManager.GetActiveTextInputControl();
                if (tic != null && tic.AcceptText)
                {
                    ActiveDialogMutedTextControl = tic;
                    ActiveDialogMutedTextControl.AcceptText = false;
                }

                ActiveDialog = DialogQueue[0];
                ActiveDialog.Display();
                ActiveDialog.DisplayAfter();
                ActiveDialog.Closed += dialog_Closed;
            }

            if (ActiveDialog != null)
                ActiveDialog.Update();
        }

        /// <summary>
        /// Main time-keeping function.  Handles different kinds of frame limiters and ensures constant velocity where required.
        /// </summary>
        private void UpdateTiming()
        {
            double stopwatchTime;

            //If we are limiting to a specific rate, and not enough time has passed for the next frame to be accepted we should pause here.
            if ((ConfigManager.sFrameSync.Value != FrameSync.CompletelyUnlimited || Mode == OsuModes.BeatmapImport) &&
                (!Player.Playing || ConfigManager.sFrameSync.Value != FrameSync.Unlimited))
            {
                stopwatchTime = (double)stopWatch.ElapsedTicks / Stopwatch.Frequency * 1000;

                if (stopwatchTime - TimeAccurate < FrameLimitMillisecondsPerFrame)
                {
                    // Using ticks for sleeping is pointless due to them being rounded to milliseconds internally anyways (in windows at least).
                    double timeToSleep = FrameLimitMillisecondsPerFrame - (stopwatchTime - TimeAccurate);
                    int timeToSleepFloored = (int)Math.Floor(timeToSleep);

                    Debug.Assert(timeToSleepFloored >= 0);

                    AccumulatedSleepError += timeToSleep - (double)timeToSleepFloored;
                    int accumulatedSleepErrorCompensation = (int)Math.Round(AccumulatedSleepError);

                    // Can't sleep a negative amount of time
                    accumulatedSleepErrorCompensation = Math.Max(accumulatedSleepErrorCompensation, -timeToSleepFloored);

                    AccumulatedSleepError -= accumulatedSleepErrorCompensation;
                    timeToSleepFloored += accumulatedSleepErrorCompensation;

                    // We don't want re-schedules with Thread.Sleep(0). We already have one of these with every update cycles.
                    if (timeToSleepFloored > 0)
                        Thread.Sleep(timeToSleepFloored);

                    // Sleep is not guaranteed to be an exact time. It only guaranteed to sleep AT LEAST the specified time. We also used some time to compute the above things, so this is also factored in here.
                    double afterSleepTime = (double)stopWatch.ElapsedTicks / Stopwatch.Frequency * 1000;
                    AccumulatedSleepError += (double)timeToSleepFloored - (afterSleepTime - stopwatchTime);
                }
                else
                {
                    // We use the negative spareTime to compensate for framerate jitter slightly.
                    double spareTime = (stopwatchTime - TimeAccurate) - FrameLimitMillisecondsPerFrame;
                    AccumulatedSleepError = -spareTime;
                }
            }

            stopwatchTime = (double)stopWatch.ElapsedTicks / Stopwatch.Frequency * 1000;

            TimeAccurateLastUpdate = TimeAccurate;
            TimeAccurate = stopwatchTime;

            Time = (int)Math.Round(TimeAccurate, 0);

            if (TimeAccurate - SixtyFrameLast >= SIXTY_FRAME_TIME || PendingModeChange)
            {
                if (PendingModeChange || (!Player.IsSpinning && !Player.IsSliding && !InputManager.ReplayMode && (IsMinimized || !IsActive || Time - SixtyFrameLast > 500)))
                {
                    //This is a kind of catch-up mode, where we ignore the fact 60fps frames haven't been handled
                    //and force the game back into sync. 500ms is arbitrary, but seems to work well.

                    SixtyFramesPerSecondLength = TimeAccurate - SixtyFrameLast;
                    //TimeAccurateLastUpdate = TimeAccurate - SIXTY_FRAME_TIME;
                    SixtyFrameLast = TimeAccurate - SIXTY_FRAME_TIME;
                }
                else
                {
                    SixtyFramesPerSecondLength = TimeAccurate - SixtyFrameLast;
                    SixtyFrameLast = TimeAccurate - (SixtyFramesPerSecondLength - SIXTY_FRAME_TIME);
                }

                SixtyFramesPerSecondFrame = true;
#if DEBUG
                sixtyFrameCounter++;
#endif
                PendingModeChange = false;
            }
            else
                SixtyFramesPerSecondFrame = false;

            ElapsedMilliseconds = TimeAccurate - TimeAccurateLastUpdate;
            FrameRatio = ElapsedMilliseconds / FrameAimTime;

            ModeTime += ElapsedMilliseconds;
        }

        private static void UpdateSystemWideDisables()
        {
            //Handle whether the screensaver should be disabled or not.
            //todo: run less frequently
            if (IsActive)
                Screensaver.Disable();
            else
                Screensaver.Restore();

            if (IsActive && Player.Playing && !InputManager.ReplayMode)
                WindowsKey.Disable();
            else
                WindowsKey.Enable();
        }

#if DEBUG
        int lastProcessorTime;
        int sixtyFrameCounter;
#endif
        const int fpsDisplayInterval = 250;

        double fpsLastDisplayed = 0;
        int fpsDisplayValue = 0;
        private void UpdateFpsCounter()
        {
            if (ConfigManager.sFpsCounter && (InputManager.HandleInput || fpsDisplayValue > 0))
            {
                fpsElapsedTime += ElapsedMilliseconds;
                fpsElapsedTimeSinceLast += ElapsedMilliseconds;

                int maxFps = IsActive ? (int)Math.Ceiling(1000 / FrameLimitMillisecondsPerFrame) : 60;

                if (fpsElapsedTime > fpsDisplayInterval)
                {
                    fpsElapsedTime -= fpsDisplayInterval;

                    fpsDisplayValue = frameCounter == 0 ? 0 : (int)Math.Ceiling(Math.Min(frameCounter * 1000f / fpsElapsedTimeSinceLast, maxFps));

#if DEBUG
                    sixtyFrameCounter = (int)(sixtyFrameCounter * 1000f / fpsElapsedTimeSinceLast);

                    int newProcessorTime = (int)Process.GetCurrentProcess().TotalProcessorTime.TotalMilliseconds;
                    string bound = newProcessorTime - lastProcessorTime > 900 ? "CPU" : "GPU";
                    lastProcessorTime = newProcessorTime;

                    float drawTimes = SpriteManager.PixelCount / (WindowWidth * WindowHeight);

                    if (fpsDisplay != null)
                    {
                        fpsDisplay.HandleInput = true;
                        if (SpriteManager.Placement && SpriteManager.ClickHandledSprite != null)
                            fpsDisplay.ToolTip = SpriteManager.ClickHandledSprite.ToString();
                        else
                        {
                            fpsDisplay.ToolTip = string.Format(
                                "FPS {0:#,#}fps {15}sfps | NET ↑{5:#,0}kb ↓{6:#,0}kb {28}pkt {27}ms  | MOUSE ({7:0.0},{8:0.0}) POLL{29}\n" +
                                "SPRITES man:{1} sk:{9} bm:{10} osu:{12} dyn:{13}/{14}\n" +
                                "TIME a{16} g{17} v{18}{19} | GC {24}/{23}/{22} | MON {21}hz\n" +
                                "PERF cpu:{20}ms fill:{2:n}m ({3:n}x) {4} sprites:{30} vis:{31}\n" +
                                "{25}",
                                /*  0 */ fpsDisplayValue, SpriteManager.InstanceCount, SpriteManager.PixelCount / 1000000f, drawTimes, bound, /*  4 */
                                /*  5 */ BanchoClient.SentBytes / 1024, BanchoClient.ReceivedBytes / 1024, /*  6 */
                                /*  7 */ MouseManager.MousePositionWindow.X, MouseManager.MousePositionWindow.Y - WindowOffsetY / WindowRatio, /*  8 */
                                /*  9 */ SkinManager.GetCacheCount(SkinSource.Skin), SkinManager.GetCacheCount(SkinSource.Beatmap), 0, /* 11 */
                                /* 12 */ SkinManager.GetCacheCount(SkinSource.Osu), DynamicSpriteCache.DynamicSprites.Count, /* 13 */
                                /* 14 */ DynamicSpriteCache.DynamicSpritesNoUnload.Count, sixtyFrameCounter, AudioEngine.Time, GameBase.Time, /* 17 */
                                /* 18 */ AudioEngine.VirtualTimeAccumulated, AudioEngine.ExtendedTime ? "!" : string.Empty, lagMax, RefreshRate, /* 21 */
                                /* 22 */ GC.CollectionCount(0), GC.CollectionCount(1), GC.CollectionCount(2),
                                /* 25 */ PresenceCache.Status(), Player.Seed, /*26*/
                                BanchoClient.RequestInterval, BanchoClient.SendCount, /*28*/
                                MouseManager.MousePollRate,
                                SpriteManager.TotalSprites, SpriteManager.TotalSpritesVisible);
                        }

                        if (drawTimes >= 8)
                            fpsDisplay.InitialColour = Color.Red;
                        else
                            fpsDisplay.InitialColour = Color.White;

                        sixtyFrameCounter = 0;
                    }
#endif

                    frameCounter = 0;
                    fpsElapsedTimeSinceLast = 0;
                }


                double decay = Math.Pow(0.05, ElapsedMilliseconds / 1000.0);
                fpsLastDisplayed = fpsLastDisplayed * decay + fpsDisplayValue * (1.0 - decay);

                if (GameBase.SixtyFramesPerSecondFrame)
                {
                    if (fpsDisplay != null)
                        fpsDisplay.Text = String.Format(@"{0:#,#}\", (int)Math.Round(fpsLastDisplayed));

                    if (maxFps < 1000 && fpsLastDisplayed < maxFps / 2)
                        fpsDisplay.InitialColour = Color.Pink;
                    else
                        fpsDisplay.InitialColour = Color.White;

                    fpsDisplay.Scale = Math.Max(1, 768f / GameBase.WindowHeight);
                    fpsDisplay.Position.X = WindowWidthScaled - (ChatEngine.IsScrollVisible ? 12 : 2) - fpsDisplay.lastMeasure.X / 1024f;
                }
            }
        }

        private void UpdateFadeState()
        {
            if (FadeState != FadeStates.Idle || fadeLevel > 0)
            {
                if (ConfigManager.sBloom && !s_fadeScreen.Bypass)
                {
                    BloomRenderer.ExtraPass = true;
                    BloomRenderer.Magnitude = 0.004f;
                    BloomRenderer.Additive = false;
                    BloomRenderer.HiRange = false;
                    BloomRenderer.Alpha = OsuMathHelper.Clamp((float)fadeLevel, 0, 100) / 100;
                }

                if (FadeState == FadeStates.FadeIn)
                {
                    if (fadeLevel <= 0)
                    {
                        FadeState = FadeStates.Idle;
                        if (FadeInComplete != null)
                            FadeInComplete(this, null);
                    }
                    else
                        fadeLevel = Math.Max(-1, fadeLevel - fadeInRate * FrameRatio);
                }
                else if (FadeState == FadeStates.FadeOut)
                {
                    if (fadeLevel >= 100)
                    {
                        FadeState = FadeStates.WaitingLoad;
                        if (FadeOutComplete != null)
                            FadeOutComplete(this, null);
                    }
                    else
                    {
                        fadeLevel = Math.Min(100, fadeLevel + fadeOutRate * FrameRatio);

                        if (GameQueuedState == OsuModes.Exit || GameQueuedState == OsuModes.Update)
                        {
                            AudioEngine.SetVolumeMusicFade((int)(100 * Math.Pow(1 - fadeLevel / 100, 3)), true);
                            Form.Opacity = Math.Min(1, 3 - 3 * fadeLevel / 100);
                        }
                        else if (Mode == OsuModes.Play)
                        {
                            if (AudioEngine.volumeMusicFade > 50)
                                AudioEngine.SetVolumeMusicFade(AudioEngine.volumeMusicFade - 1);
                        }
                        else
                            AudioEngine.SetVolumeMusicFade(AudioEngine.volumeMusicFade - 5);
                    }
                }
            }

#if TRIANGLES
            spriteManagerTransition.Alpha = OsuMathHelper.Clamp((float)fadeLevel, 0, 100) / 200;
#else
            spriteManagerTransition.Alpha = OsuMathHelper.Clamp((float)fadeLevel, 0, 100) / 100;
#endif
        }

        internal static void ShowDialog(pDialog dialog)
        {
            DialogQueue.Add(dialog);
        }

        private static void dialog_Closed(object sender, EventArgs e)
        {
            if (ActiveDialog == null)
                return;

            ActiveDialog.spriteManager.SpriteList.ForEach(s => { s.FadeOut(180); s.AlwaysDraw = false; });

            pDialog disposedDialog = ActiveDialog;

            Scheduler.AddDelayed(delegate { disposedDialog.spriteManager.Dispose(); }, 180);

            spriteManagerOverlay.Add(ActiveDialog.spriteManager.SpriteList);

            ActiveDialog = null;

            if (ActiveDialogMutedTextControl != null)
            {
                ActiveDialogMutedTextControl.AcceptText = true;
                ActiveDialogMutedTextControl = null;
            }

            DialogQueue.Remove((pDialog)sender);
            Instance.Components.Remove((IGameComponent)sender);
        }

        static int ExceptionCount;

        protected override void Draw()
        {
            lock (GlControl.ThreadingLock)
            {
                while (!graphicsLoaded)
                {
                    Monitor.Wait(GlControl.ThreadingLock);
                }

#if !DEBUG && !PublicNoUpdate
                try
                {
#endif
                SpriteManager.NewFrame();

                frameCounter++;

                toolTipping = false;

                if (InputManager.HandleInput)
                    SpriteManager.CheckInputGlobal();

                HitObjectManager.DrawTargets();

                InputManager.UpdateCursorTrail();

                if (OGL) Gl.glClear(Gl.GL_COLOR_BUFFER_BIT);
                if (D3D) graphics.GraphicsDevice.Clear(Color.Black);

                bool replayCursor = InputManager.ReplayMode && Mode == OsuModes.Play;

                //Begin bloom virtual renderer.
                if (D3D && ConfigManager.sBloom) BloomRenderer.BloomStart();

                TransitionManager.Draw();

                base.Draw();

                spriteManager.Draw();

                //if ((BanchoClient.Permission & Permissions.Supporter) > 0)
                if (replayCursor) spriteManagerCursor.Draw();

                Options.Draw();

                osuDirect.Draw();

                spriteManagerOverlay.Draw();
                chatEngine.Draw();

                MusicControl.Draw();

                Volume.Draw();

                if (ActiveDialog != null)
                    ActiveDialog.Draw();

                spriteManagerOverlayHighest.Draw();

                BanchoStatus.Draw();

                if (!replayCursor) spriteManagerCursor.Draw();

                spriteManagerHighest.Draw();

                DrawToolTip();

                if (FadeState != FadeStates.Idle || spriteManagerTransition.Alpha > 0)
                    spriteManagerTransition.Draw();

                if (OnScreenDisplay != null) OnScreenDisplay.Draw();

                TransitionManager.DrawHigh();

                //Draw bloom overlay.
                if (D3D && ConfigManager.sBloom) BloomRenderer.BloomEnd();

                if (OGL) GlControl.Draw();
#if !DEBUG && !PublicNoUpdate
                }
                catch (Exception e)
                {
                    if (ExceptionCount++ > 1 || Mode == OsuModes.Exit || GameQueuedState == OsuModes.Exit)
                        throw;
                    softHandle(e);
                }
#endif
            }

            TotalFramesRendered++;
        }

        private static void softHandle(Exception e)
        {
            if (ExceptionCount == 1) //report error the first time
            {
                ErrorSubmission.Submit(new OsuError(e) { Feedback = "soft handle" });
                NotificationManager.ShowMessage("An error occurred somewhere in osu! and has been reported. osu! will attempt to keep running.", Color.Red, 5000);
            }

            if (e is MissingFieldException || e is MissingMethodException || e is MissingMemberException)
                GameBase.ChangeMode(OsuModes.Update);

            GameBase.Scheduler.AddDelayed(delegate { ExceptionCount--; }, 1000);
        }

        private double toolTipTime;

        private void DrawToolTip()
        {
            if (toolTip.Alpha > 0 || toolTipping)
            {
                toolTip.TextSize = 9 - ((GameBase.WindowHeight - 1024) / 256f);
                Vector2 measure = toolTip.MeasureText() * GameBase.WindowRatio;
                Vector2 excess = MouseManager.MousePosition + measure - new Vector2(WindowWidth, WindowHeight + WindowOffsetY) + TooltipPaddingVector * WindowRatio * 2;

                Vector2 position = (MouseManager.MousePosition + TooltipPaddingVector) / WindowRatio;

                position.Y -= WindowOffsetY / WindowRatio;

                if (position.X > WindowWidth - 120 && measure.X < WindowWidth - 120)
                    position.X -= (measure.X + TooltipPadding * 2) / WindowRatio;
                else if (excess.X > 0)
                    position.X -= excess.X / WindowRatio;

                if (position.Y > WindowHeight - 80 && measure.Y < GameBase.WindowHeight - 80)
                    position.Y -= (measure.Y + TooltipPadding * 2) / WindowRatio;
                else if (excess.Y > 0)
                    position.Y -= excess.Y / WindowRatio;


                toolTip.Position = position;
            }
            else
            {
                toolTipTime = 0;
            }

            if (toolTipping && !toolTipDisplaying)
            {
                toolTipTime += GameBase.ElapsedMilliseconds;

                if (toolTipTime > 120)
                {
                    toolTip.FadeIn(200);
                    toolTipDisplaying = true;
                }
            }
            else if (!toolTipping && toolTipDisplaying)
            {
                toolTip.FadeOut(200);
                toolTipDisplaying = false;
            }
        }

        protected override void LoadGraphicsContent(bool loadAllContent)
        {
            lock (GlControl.ThreadingLock)
            {
                base.LoadGraphicsContent(loadAllContent);

                primitiveBatch = new PrimitiveBatch();

                //recreate primitive batch (relies on screen res to create effect)
                HitObjectManager.UpdateProjectionMatrix();

                if (!D3D)
                {
                    if (SkinManager.SliderRenderer != null) SkinManager.SliderRenderer.Dispose();
                    SkinManager.SliderRenderer = null;
                    SkinManager.ComputeColours();
                }

                pTexture white = pTexture.FromRawBytes(new byte[] { 255, 255, 255, 255 }, 1, 1);
                GC.SuppressFinalize(white);

                if (WhitePixel != null)
                {
                    WhitePixel.TextureXna = white.TextureXna;
                    WhitePixel.isDisposed = false;
                }
                else
                    WhitePixel = white;

                if (LineManager == null)
                    LineManager = new LineManager();
                LineManager.Init(graphics.GraphicsDevice, content);
                LineRenderer = LineManager;

                if (D3D)
                {
                    if (spriteBatch != null) spriteBatch.Dispose();
                    spriteBatch = new SpriteBatch(graphics.GraphicsDevice);

                    SkinManager.Reload();

                    InvokeOnReload(loadAllContent);

                    BloomRenderer.Initialize();

                    if (SkinManager.SliderRenderer != null) SkinManager.SliderRenderer.Dispose();
                    SkinManager.SliderRenderer = null;
                    SkinManager.ComputeColours();
                }

                graphicsLoaded = true;
                Monitor.PulseAll(GlControl.ThreadingLock);
            }
        }

        protected override void UnloadGraphicsContent(bool unloadAllContent)
        {
            lock (GlControl.ThreadingLock)
            {
                graphicsLoaded = false;

                if (unloadAllContent)
                {
                    if (D3D)
                    {
                        if (WhitePixel != null && WhitePixel.TextureXna != null)
                            WhitePixel.TextureXna.Dispose();
                    }

                    if (content != null) content.Unload();

                    SkinManager.Unload();
                }

                BloomRenderer.Unload();

                InvokeOnUnload(unloadAllContent);
            }
        }

        internal static bool FrameBufferSupported
        {
            get
            {
                // todo: Opengl FBO
                return D3D;
            }
        }

        //used for flashlight cheat detecting
        internal static Color[] PixelColorAt(Vector2 pos, int count)
        {
            try
            {
                if (D3D)
                {
                    Texture2D dstTexture = new Texture2D(
                        graphics.GraphicsDevice,
                        graphics.GraphicsDevice.Viewport.Width,
                        graphics.GraphicsDevice.Viewport.Height,
                        1,
                        ResourceUsage.ResolveTarget,
                        SurfaceFormat.Bgr32, ResourceManagementMode.Manual);

                    graphics.GraphicsDevice.ResolveBackBuffer(dstTexture);
                    Color[] c = new Color[count];
                    Rectangle r = new Rectangle((int)pos.X, (int)pos.Y, 1, count);
                    dstTexture.GetData(0, r, c, 0, count);
                    dstTexture.Dispose();
                    return c;
                }
                else
                {
                    Color[] c = new Color[count];
                    using (Bitmap b = new Bitmap(WindowWidth, WindowHeight, PixelFormat.Format32bppArgb))
                    {
                        BitmapData bd = b.LockBits(new System.Drawing.Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadOnly, b.PixelFormat);
                        Gl.glReadPixels(0, 0, WindowWidth, WindowHeight, Gl.GL_BGRA, Gl.GL_UNSIGNED_BYTE, bd.Scan0);
                        b.UnlockBits(bd);
                        for (int i = 0; i < count; i++)
                        {
                            System.Drawing.Color sc = b.GetPixel((int)pos.X, (int)pos.Y + i);
                            c[i] = new Color(sc.R, sc.G, sc.B);
                        }
                    }
                    return c;
                }
            }
            catch
            {
                return null;
            }
        }

        internal static MemoryStream GetScreenShotStream()
        {
            try
            {
                MemoryStream image = new MemoryStream();
                if (D3D)
                {
                    using (Texture2D dstTexture = new Texture2D(
                        graphics.GraphicsDevice,
                        graphics.GraphicsDevice.Viewport.Width,
                        graphics.GraphicsDevice.Viewport.Height,
                        1,
                        ResourceUsage.ResolveTarget,
                        SurfaceFormat.Bgr32, ResourceManagementMode.Manual))
                    {
                        graphics.GraphicsDevice.ResolveBackBuffer(dstTexture);

                        string filename = Path.GetTempFileName();
                        //system lagged here, avoid save to disk may help.
                        dstTexture.Save(filename, ImageFileFormat.Jpg);
                        image = new MemoryStream(File.ReadAllBytes(filename));
                        File.Delete(filename);
                        return image;
                    }
                }
                else
                {
                    using (Bitmap b = new Bitmap(WindowWidth, WindowHeight, PixelFormat.Format32bppArgb))
                    {
                        BitmapData bd = b.LockBits(new System.Drawing.Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadOnly, b.PixelFormat);
                        Gl.glReadPixels(0, 0, WindowWidth, WindowHeight, Gl.GL_BGRA, Gl.GL_UNSIGNED_BYTE, bd.Scan0);
                        b.UnlockBits(bd);
                        b.RotateFlip(RotateFlipType.RotateNoneFlipY);
                        b.Save(image, ImageFormat.Jpeg);
                    }
                    return image;
                }
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region Content Management

        internal static ContentManager content;

        #endregion

        #region Gamefield/Window Manipulation

        internal static int GamefieldDefaultHeight = 384;
        internal static int GamefieldDefaultWidth = 512;
        internal static float GamefieldHeight = WindowHeight * 0.8f;
        internal static Vector2 GamefieldOffsetVector1;
        internal static Vector2 GamefieldOffsetVector1Widescreen;
        internal static float GamefieldRatio;
        internal static float GamefieldSize = 1;
        internal static float GamefieldWidth = WindowWidth * 0.8f;

        internal static int WindowDefaultHeight = 480;
        internal static int WindowDefaultWidth = 640;
        internal static int WindowDefaultWidthWidescreen = 820;
        internal static int WindowWidth = 640;
        internal static int WindowWidthScaled { get { return (int)Math.Ceiling(WindowWidth / WindowRatio); } }
        internal static int WindowHeight = 480;
        internal static int WindowHeightScaled { get { return (int)Math.Ceiling(WindowHeight / WindowRatio); } }
        internal static float WindowRatio;
        internal static int WindowSpriteRes = 768;
        internal static int WindowOffsetY;
        internal static int WindowOffsetX;
        internal static float WindowOffsetXScaled { get { return WindowOffsetX / WindowRatio; } }
        internal static float WidthWidescreenRatio { get { return (float)WindowWidthScaled / (float)WindowDefaultWidth; } }
        internal static float PixelsToLoadRatio { get { return 1 / (GamefieldSize * GamefieldSize * WindowDefaultWidth * WindowDefaultHeight); } }
        private static int RefreshRate = 0;

        internal static float GamefieldRatioWindowIndependent
        {
            get { return (float)GamefieldHeight / GamefieldDefaultHeight / WindowRatio; }
        }

        internal static Rectangle GamefieldActual
        {
            get { return new Rectangle(0, 0, GamefieldDefaultWidth + 1, GamefieldDefaultHeight + 1); }
        }

        internal static Rectangle GamefieldDisplay
        {
            get
            {
                return
                    new Rectangle((int)GamefieldOffsetVector1.X, (int)GamefieldOffsetVector1.Y, (int)GamefieldWidth, (int)GamefieldHeight);
            }
        }

        internal static event BoolDelegate OnUnload;

        private static void InvokeOnUnload(bool b)
        {
            BoolDelegate unload = OnUnload;
            if (unload != null) unload(b);
        }

        internal static event BoolDelegate OnReload;

        private static void InvokeOnReload(bool b)
        {
            BoolDelegate reload = OnReload;
            if (reload != null) reload(b);
        }


        internal static Vector2 WindowRatioApply(Vector2 vec)
        {
            return vec * WindowRatio + new Vector2(WindowOffsetX, WindowOffsetY);
        }

        internal static float DisplayToGamefieldXWide(float pos)
        {
            return (pos - GamefieldOffsetVector1Widescreen.X) / GamefieldRatio;
        }

        internal static float DisplayToGamefieldX(float pos)
        {
            return (pos - GamefieldOffsetVector1.X) / GamefieldRatio;
        }

        internal static float DisplayToGamefieldY(float pos)
        {
            return (pos - GamefieldOffsetVector1.Y) / GamefieldRatio;
        }

        internal static Vector2 DisplayToGamefield(Vector2 vec)
        {
            return (vec - GamefieldOffsetVector1) / GamefieldRatio;
        }

        internal static float GamefieldToDisplayX(float pos)
        {
            return pos * GamefieldRatio + GamefieldOffsetVector1.X;
        }

        internal static float GamefieldToDisplayY(float pos)
        {
            return pos * GamefieldRatio + GamefieldOffsetVector1.Y;
        }

        internal static Vector2 GamefieldToDisplay(Vector2 vec)
        {
            Vector2 outVector = vec;
            GamefieldToDisplay(ref outVector);
            return outVector;
        }

        internal static void GamefieldToDisplay(ref Vector2 vec)
        {
            Vector2.Multiply(ref vec, GamefieldRatio, out vec);
            Vector2.Add(ref vec, ref GamefieldOffsetVector1, out vec);
        }

        internal static void GamefieldToDisplayWidescreen(ref Vector2 vec)
        {
            Vector2.Multiply(ref vec, GamefieldRatio, out vec);
            Vector2.Add(ref vec, ref GamefieldOffsetVector1Widescreen, out vec);
        }

        internal static void GamefieldToDisplay(ref Line line)
        {
            GamefieldToDisplay(ref line.p1);
            GamefieldToDisplay(ref line.p2);
        }

        internal static Line GamefieldToDisplay(Line line)
        {
            Line dline = new Line(GamefieldToDisplay(line.p1), GamefieldToDisplay(line.p2));
            return dline;
        }

        internal static bool IsDisplayInGamefield(Vector2 vector)
        {
            return GamefieldDisplay.Contains(new Point((int)vector.X, (int)vector.Y));
        }

        internal static bool IsDisplayInGamefieldVertically(Vector2 vector)
        {
            return GamefieldDisplay.Y < vector.Y && GamefieldDisplay.Bottom > vector.Y;
        }

        internal static bool IsInGamefield(Vector2 vector)
        {
            return GamefieldActual.Contains(new Point((int)vector.X, (int)vector.Y));
        }

        internal static bool IsInGamefieldAlt(Vector2 vector)
        {
            return vector.X >= 0 && vector.X <= 512 && vector.Y >= 0 && vector.Y <= 384;
        }
        internal static bool Widescreen;

        /// <summary>
        /// This was added to attempt to move the gamefield to a location where no circle will overlap a screen edge.
        /// </summary>
        internal static bool GamefieldCorrectionOffsetActive;

        internal static void ResizeGamefield(float size, bool force)
        {
            if (GamefieldSize == size && !force)
                return;

            GamefieldSize = size;

            GamefieldWidth = GamefieldDefaultWidth * WindowRatio * size;
            GamefieldHeight = GamefieldDefaultWidth * WindowRatio * size * 0.75f;


            GamefieldCorrectionOffsetActive = Mode != OsuModes.Edit && Player.Mode == PlayModes.Osu;

            float modeOffset = GamefieldCorrectionOffsetActive ? -16 * WindowRatio : 0;

            GamefieldOffsetVector1 = new Vector2((float)(WindowWidth - GamefieldWidth) / 2,
                                                 (float)(WindowHeight - GamefieldHeight) / 4 * 3 + WindowOffsetY + modeOffset);
            GamefieldOffsetVector1Widescreen = new Vector2(0,
                                                 (float)(WindowHeight - GamefieldHeight) / 4 * 3 + WindowOffsetY + modeOffset);

            GamefieldRatio = (float)GamefieldHeight / GamefieldDefaultHeight;

            WindowOffsetX = (int)((WindowWidth - (WindowHeight * 4 / 3f)) / 2);

            Widescreen = WindowOffsetX > 0;

            HitObjectManager.UpdateStaticVarsAllInstances(true);
        }

        #endregion

        #region Sprites

        internal static Rectangle WindowRectangle;
        internal static Rectangle WindowRectangleWidescreen;
        #endregion

        #region State Management

        internal static double fadeLevel;

        internal static FadeStates FadeState = FadeStates.Idle;
        internal static OsuModes GameQueuedState = OsuModes.Menu;
        internal static OsuModes Mode = OsuModes.Unknown;
        internal static IGameComponent RunningGameMode;

        /// <summary>
        /// Track whether the exit dialog box is displaying (so we don't display more than one).
        /// </summary>
        static bool exitDialogDisplaying;

        /// <summary>
        /// Handle exit confirmation when required.
        /// </summary>
        internal static void BeginExit(bool forceConfirm = false)
        {
            bool downloadsActive = OsuDirect.ActiveDownloads.Count > 0;
            bool waitingPrivateMessages = ChatEngine.channelTabs.Tabs.FindAll(t => t.Alerting && t.tabText.Text != null && !t.tabText.Text.StartsWith("#")).Count > 0;

            if (Tournament)
            {
                ChangeMode(OsuModes.Exit);
                return;
            }

            if (!downloadsActive && !waitingPrivateMessages && !ConfigManager.sConfirmExit && !forceConfirm)
            {
                ChangeMode(OsuModes.Exit);
                return;
            }

            if (exitDialogDisplaying) return;

            exitDialogDisplaying = true;

            string confirmMessage = "Are you sure you want to exit osu!?";

            if (downloadsActive)
                confirmMessage = "Exiting will cancel all active downloads!\n" + confirmMessage;
            else if (waitingPrivateMessages)
                confirmMessage = "You have unread chat messages.\n" + confirmMessage;

            pDialogConfirmation pd = new pDialogConfirmation(confirmMessage,
                delegate { ChangeMode(OsuModes.Exit); },
                delegate { exitDialogDisplaying = false; }
                );
            pd.Closed += delegate { exitDialogDisplaying = false; };

            ShowDialog(pd);
        }

        internal static void ChangeMode(OsuModes newMode, bool force = false)
        {
            //If we are in the editor and not yet prompted for confirmation, do so now.
            if (Mode == OsuModes.Edit && !((Editor)RunningGameMode).ExitedProperly && !((Editor)RunningGameMode).Exit())
                return;

            if ((newMode != OsuModes.Exit && newMode != OsuModes.Update) && (!BanchoClient.CheckAuth(false) || !AudioEngine.CheckAudioDevice()))
            {
                newMode = OsuModes.Menu;
                force = true;
            }

            if (Tournament)
            {
                switch (newMode)
                {
                    case OsuModes.Rank:
                    case OsuModes.Exit:
                    case OsuModes.Busy:
                        break;
                    case OsuModes.SelectPlay:
                        newMode = OsuModes.Menu;
                        break;
                    default:
                        if (Mode == OsuModes.Play && !AudioEngine.Paused &&
                            (InputManager.ReplayToEnd || (Player.Instance != null && Player.Instance.Status == PlayerStatus.Outro)))
                            return;
                        break;
                }

                //todo: figure a proper fix for this.
                if (Mode == OsuModes.Rank && newMode == OsuModes.Play && ModeTime < 10000)
                    return;
            }

            NotificationManager.ClearMessageMassive(null, null);

            if (newMode == Mode && !force)
                return;

            if (newMode == GameQueuedState && !force)
                return;

            Logger.Log("Changing to {0} mode.", newMode.ToString());

            //Close dialogs which are specific to the old game mode.
            List<pDialog> closeUs = new List<pDialog>();
            foreach (pDialog p in DialogQueue)
                if (p.GameModeSpecific)
                    closeUs.Add(p);

            foreach (pDialog p in closeUs)
                p.Close();

            GameQueuedState = newMode;

            s_fadeScreen.Bypass = false;
            fadeOutRate = 10;

            spriteManagerTransition.Clear();
            spriteManagerTransition.Add(s_fadeScreen);

            switch (newMode)
            {
                case OsuModes.Exit:
                    if (OnExitFade != null) OnExitFade();

                    if (!ConfigManager.sMenuVoice || (UpdateState == UpdateStates.NeedsRestart && osuMain.Restart) || Tournament)
                        fadeOutRate = 5;
                    else
                    {
                        fadeOutRate = 0.6;
                        AudioEngine.PlaySample(@"seeya", 100, (BanchoClient.Permission & Permissions.Supporter) > 0 ? SkinSource.ExceptBeatmap : SkinSource.Osu);
                    }

                    InputManager.HandleInput = false;
                    break;
            }

            FadeState = FadeStates.FadeOut;
        }

        internal static void ChangeModeInstant(OsuModes newMode, bool force = false)
        {
            Scheduler.Add(delegate
            {
                if (newMode == Mode && !force)
                    return;

                FadeState = FadeStates.Idle;

                //Close dialogs which are specific to the old game mode.
                List<pDialog> closeUs = new List<pDialog>();
                foreach (pDialog p in DialogQueue)
                    if (p.GameModeSpecific)
                        closeUs.Add(p);

                foreach (pDialog p in closeUs)
                    p.Close();

                GameQueuedState = newMode;

                ChangeModeCallback(null, null);

                if (FadeInComplete != null)
                    FadeInComplete(null, null);
            });
        }


        internal static OsuModes LastMode = OsuModes.Unknown;
        private static void ChangeModeCallback(object sender, EventArgs e)
        {
            if (GameQueuedState != OsuModes.Play || LastMode != OsuModes.Play)
                SetTitle();

            IdleHandler.ForceActivity();

            switch (GameQueuedState)
            {
                case OsuModes.Rank:
                case OsuModes.RankingTagCoop:
                case OsuModes.RankingTeam:
                case OsuModes.RankingVs:
                    break;
                default:
                    //Only clear this cache if we aren't entering the ranking screen.  We want these samples to keep playing otherwise.
                    AudioEngine.ClearSampleEvents();
                    break;
            }

            PendingModeChange = true; //Allows skipping of sync frames after load.

            Mode = GameQueuedState;

            if (OnNextModeChange != null)
            {
                OnNextModeChange();
                OnNextModeChange = null;
            }

            if (OnModeChange != null)
                OnModeChange();

            if (Mode != OsuModes.Menu)
                Options.PoppedOut = false;

            if (RunningGameMode != null)
            {
                Instance.Components.Remove(RunningGameMode);
                ((GameComponent)RunningGameMode).Dispose();
                RunningGameMode = null;
            }

            if (Mode == OsuModes.Exit)
            {
                Instance.Exit();
                goto Finish;
            }

            if (LastMode != OsuModes.Unknown)
            {
                if (LastMode != Mode)
                    SkinManager.LoadSkin();
                else
                    SkinManager.LoadCursor(false);

            }

            BloomRenderer.RedTint = 0;

            ScreenMode modeOverride = ConfigManager.sScreenMode;

            ModeTime = 0;

            switch (Mode)
            {
                case OsuModes.SelectEdit:
                case OsuModes.Edit:
                    if (modeOverride == ScreenMode.Fullscreen)
                        modeOverride = ScreenMode.BorderlessWindow;
                    break;
                case OsuModes.Play:
                    if (TestMode && modeOverride == ScreenMode.Fullscreen)
                        modeOverride = ScreenMode.BorderlessWindow;
                    break;
            }

            bool forceScreenSize = false;
            if (Mode == OsuModes.Edit)
            {
                if (EditorControl == null) EditorControl = new EditorControl();

                if (!MenuVisible)
                {
                    MenuVisible = true;
                    EditorControl.Parent = GameWindow;
                    WindowOffsetY = EditorControl.Height;
                    forceScreenSize = true;
                }
            }
            else
            {
                if (EditorControl != null)
                {
                    EditorControl.Dispose();
                    EditorControl = null;
                }

                if (MenuVisible)
                {
                    MenuVisible = false;
                    WindowOffsetY = 0;
                    forceScreenSize = true;
                }
            }

            if (!Tournament)
            {
                if (modeOverride != CurrentScreenMode || forceScreenSize)
                    SetScreenSize(null, null, modeOverride, false, forceScreenSize);
                else if (pendingScreenModeChange != null)
                    pendingScreenModeChange();
                pendingScreenModeChange = null;
            }

            ResizeGamefield(GamefieldSize, true);

            InputManager.FullTextEnabled = ChatEngine.IsVisible;
            InputManager.fullTextGameBuffer = string.Empty;

            AudioEngine.cachedSeekPending = false;

            bool showMusicControls = false;

            ForceGC(true);

            bool allowMaximize = false;

            switch (Mode)
            {
                case OsuModes.Edit:
                    AudioEngine.ResetAudioTrack();
                    AudioEngine.Stop();
                    RunningGameMode = new Editor(Instance);
                    break;
                case OsuModes.Play:
                    if (LastMode == OsuModes.MatchSetup || PlayerVs.Match != null)
                        RunningGameMode = new PlayerVs(MatchSetup.Match);
                    else if (TestMode)
                        RunningGameMode = new PlayerTest();
                    else if (InputManager.ReplayMode || InputManager.ReplayStreaming || ModManager.CheckActive(Mods.Autoplay))
                        RunningGameMode = new ReplayWatcher();
                    else
                        RunningGameMode = new Player();
                    break;
                case OsuModes.SelectPlay:
                case OsuModes.SelectEdit:
                case OsuModes.SelectMulti:
                    allowMaximize = true;

                    //check for new beatmaps once after starting osu!
                    if (BeatmapImport.DoNewBeatmapCheck ||
                        (LastMode != OsuModes.BeatmapImport && BeatmapManager.Beatmaps.Count == 0))
                    {
                        RunningGameMode = new BeatmapImport(Instance, Mode);
                        Mode = OsuModes.BeatmapImport;
                    }
                    else
                        RunningGameMode = new SongSelection(Instance);
                    break;
                case OsuModes.SkinSelect:
                    allowMaximize = true;
                    RunningGameMode = new OptionsSkin(Instance);
                    break;
                case OsuModes.OptionsOffsetWizard:
                    allowMaximize = true;
                    showMusicControls = true;
                    RunningGameMode = new OptionsOffsetWizard(Instance);
                    break;
                case OsuModes.Menu:
                    allowMaximize = true;
                    if (LastMode != OsuModes.Unknown) Menu.FirstLoad = false;
                    showMusicControls = true;
                    RunningGameMode = new Menu(Instance);
                    break;
                case OsuModes.Lobby:
                    allowMaximize = true;
                    showMusicControls = true;
                    RunningGameMode = new Lobby(Instance);
                    break;
                case OsuModes.MatchSetup:
                    //check for new beatmaps once after starting osu!
                    if (BeatmapImport.DoNewBeatmapCheck)
                    {
                        RunningGameMode = new BeatmapImport(Instance, Mode);
                        Mode = OsuModes.BeatmapImport;
                    }
                    else
                        RunningGameMode = new MatchSetup(Instance);
                    break;
                case OsuModes.OnlineSelection:
                    allowMaximize = true;
                    RunningGameMode = new OnlineSelection(Instance);
                    break;
                case OsuModes.RankingVs:
                    RunningGameMode = new RankingVs(Instance);
                    break;
                case OsuModes.RankingTagCoop:
                    RunningGameMode = new RankingTagCoop(Instance);
                    break;
                case OsuModes.RankingTeam:
                    RunningGameMode = new RankingTeamVs(Instance);
                    break;
                case OsuModes.Rank:
                    RunningGameMode = new Ranking(Instance);
                    break;
                case OsuModes.BeatmapImport:
                    RunningGameMode = new BeatmapImport(Instance, LastMode == OsuModes.Unknown ? OsuModes.Menu : LastMode);
                    break;
                case OsuModes.PackageUpdater:
                    RunningGameMode = new PackageUpdater(Instance, LastMode);
                    break;
                case OsuModes.Benchmark:
                    RunningGameMode = new Benchmark(Instance);
                    break;
                case OsuModes.Tourney:
                    RunningGameMode = new osu.GameModes.Other.Tournament(Instance);
                    break;
                case OsuModes.Charts:
                    RunningGameMode = new Charts();
                    break;
                case OsuModes.Update:
                    osuMain.ForceUpdate();
                    Instance.Exit();
                    goto Finish;
                case OsuModes.Busy:
                    goto Finish;
            }

            if (Form.FormBorderStyle != FormBorderStyle.None)
                Form.MaximizeBox = allowMaximize;

            MusicControl.ShowControls = showMusicControls;

            UpdateClientBounds();

            Instance.Components.Add(RunningGameMode);

        Finish:
            LastMode = Mode;
            return;

        }

        internal static void LoadComplete()
        {
            BanchoClient.UpdateStatus();

            if (FadeState != FadeStates.Idle)
                fadeLevel = 140;

            FadeState = FadeStates.FadeIn;

            ChatEngine.UpdateNowPlaying(false, false);
        }

        #endregion

        #region User Input Handling

        internal static double ElapsedMilliseconds;
        private static bool localMenuActive;

        internal static bool TestMode;
        internal static int TestTime;
        private readonly string StartupNewFiles;

        #endregion

        #region Events

        internal static event EventHandler FadeInComplete;
        internal static event EventHandler FadeOutComplete;

        #endregion

        //private static readonly Queue<VoidDelegate> schedulerQueue = new Queue<VoidDelegate>();
        internal static readonly Scheduler Scheduler = new Scheduler();
        internal static pDialog ActiveDialog;
        private static TextInputControl ActiveDialogMutedTextControl;
        internal static System.Drawing.Rectangle ClientBounds;
        internal static bool D3D;
        private static double fadeOutRate = 8;
        private static double fadeInRate = 4;
        internal static pText fpsDisplay;
        internal static double fpsElapsedTime;
        internal static double fpsElapsedTimeSinceLast;
        internal static double frameCounter;
        internal static GlControl GlControl;
        internal static bool OGL;
        internal static int SystemResolutionHeight;
        internal static int SystemResolutionWidth;
        internal static pText toolTip;

        /// <summary>
        /// Tooltip is available for displaying.
        /// </summary>
        internal static bool toolTipping;

        /// <summary>
        /// The local user's user/panel object.
        /// </summary>
        internal static User User;

        internal static float WindowRatioInverse;
        internal static float WindowRatioScaleDown;

        private OsuDirect osuDirect;

        internal static void SetTitle()
        {
            string titleAddition = string.Empty;
            User current = StreamingManager.CurrentlySpectating;
            if (current != null)
                titleAddition += " (watching " + current.Name + ")";
            SetTitle(titleAddition);
        }

        internal static void SetTitle(string title)
        {
            if (ConfigManager.sScreenMode == ScreenMode.Fullscreen) return;

            Scheduler.Add(delegate
            {
                Instance.Window.Title = ("osu!" + General.SUBVERSION + " " + General.PUBLIC_NAME + (title.Length > 0 ? " - " + title : string.Empty)).Trim();
            });
        }

        static bool isShowingLogin;
        internal static void ShowLogin(bool callback = false)
        {
            if (Tournament) return;

            if (isShowingLogin && !callback) return;
            isShowingLogin = true;

            if (!GameBase.IsActive || StartupOperationThread.IsAlive)
            {
                GameBase.Scheduler.AddDelayed(delegate { ShowLogin(true); }, 100);
                return;
            }

            MenuActive = true;
            Options.Expanded = true;
            Options.LoginOnly = true;

            MenuActive = false;

            isShowingLogin = false;
        }

        internal static OsuModes ReceiveFile(params string[] files)
        {
            if (files == null || files.Length == 0 || (files.Length == 1 && String.IsNullOrEmpty(files[0])))
                return OsuModes.Unknown;

            string firstFile = files[0];
            string ext = string.Empty;
            try
            {
                //Base the extension on the first file dragged.
                //This could fail if there are multiple filetypes, but the user shouldn't be doing this.
                ext = Path.GetExtension(firstFile).Trim('.').ToLower();
            }
            catch
            {
                return OsuModes.Unknown;
            }

            try
            {
                switch (ext)
                {
                    case "osz2":
                    //TODO: reimplement
                    //string newPath = Application.StartupPath + "/Songs/" + Path.GetFileName(files);
                    //BeatmapManager.ChangedPackages.Add(newPath);
                    //goto case "osz";
                    case "zip":
                    case "osz":
                        string songs_fullpath = Path.GetFullPath(BeatmapManager.SONGS_DIRECTORY);

                        foreach (string f in files)
                        {
                            string filename = Path.GetFileName(f);

                            int charsOver = (songs_fullpath.Length + filename.Length) - GeneralHelper.MAX_PATH_LENGTH;

                            if (charsOver > 0)
                            {
                                int newLength = filename.Length - charsOver;
                                //Leave enough room for file chars + extension
                                if (newLength > (ext.Length + 1))
                                {
                                    filename = filename.Remove(newLength) + @"." + ext;
                                }
                                else
                                {
                                    NotificationManager.ShowMessage("Error moving " + filename + ". Path is too long.", Color.Red, 1000);
                                    continue;
                                }
                            }

                            string newPath = BeatmapManager.SONGS_DIRECTORY + filename;
                            Beatmap beatmap = BeatmapManager.Beatmaps.Find(bm => bm.InOszContainer && bm.ContainingFolder == newPath);

                            if (beatmap != null)
                                //in the case a map is already loaded, select it as the current map rather than attempt import.
                                BeatmapManager.Current = beatmap;
                            else if (GeneralHelper.FileMove(f, newPath))
                                BeatmapImport.SignalBeatmapCheck(true);
                            else
                            {
                                if (BeatmapManager.ChangedPackages.Contains(newPath))
                                    BeatmapManager.ChangedPackages.Remove(newPath);
                                TopMostMessageBox.Show("Error moving file " + filename);
                                return OsuModes.Unknown;
                            }
                        }

                        switch (Mode)
                        {
                            case OsuModes.MatchSetup:
                            case OsuModes.SelectEdit:
                            case OsuModes.SelectMulti:
                                return Mode;
                            case OsuModes.Edit:
                            case OsuModes.Play:
                                NotificationManager.ShowMessageMassive(
                                    "New beatmap available - visit song select to finish importing!", 1000);
                                return OsuModes.Unknown;
                            default:
                                return OsuModes.SelectPlay;
                        }
#if P2P
                    case "osumagnet":
                        OsuDirect.StartDownload(new P2PDirectDownload(filename, true));
                        break;
#endif
                    case "ogg":
                    case "mp3":

                        BeatmapImport.SignalBeatmapCheck(true);

                        foreach (string f in files)
                        {
                            string dest = BeatmapManager.SONGS_DIRECTORY + Path.GetFileName(f);
                            GeneralHelper.FileDelete(dest);
                            File.Copy(f, dest);
                        }

                        return OsuModes.SelectEdit;
                    case "osr":
                        {
                            Score score = ScoreManager.ReadReplayFromFile(firstFile);

                            switch (Mode)
                            {
                                case OsuModes.MatchSetup:
                                case OsuModes.Edit:
                                case OsuModes.Play:
                                    NotificationManager.ShowMessageMassive("Replay has been loaded.", 1000);
                                    return OsuModes.Unknown;
                            }

                            if (MatchSetup.Match != null)
                            {
                                NotificationManager.ShowMessageMassive("Replay has been loaded.", 1000);
                                return OsuModes.Unknown;
                            }

                            if (score != null)
                            {
                                Beatmap b = BeatmapManager.GetBeatmapByChecksum(score.fileChecksum);
                                if (BeatmapManager.Current != b)
                                {
                                    BeatmapManager.Current = b;
                                    AudioEngine.LoadAudioForPreview(b, true, false);
                                }
                                InputManager.ReplayScore = score;
                                InputManager.ReplayMode = true;
                                return OsuModes.Rank;
                            }

                            return OsuModes.Unknown;
                        }
                    case "osk":
                        foreach (string f in files)
                        {
                            string filename = Path.GetFileName(f);
                            string filenameNoExt = Path.GetFileNameWithoutExtension(filename);
                            try
                            {
                                string basePath = Path.Combine(@"Skins", filenameNoExt);
                                string path = Path.Combine(Path.Combine(Application.StartupPath, @"Skins"), filename);

                                GeneralHelper.FileMove(f, path);
                                string tempFolder = Path.Combine(Path.Combine(Path.GetTempPath(), @"osu!"), filenameNoExt);

                                if (!Directory.Exists(tempFolder))
                                    Directory.CreateDirectory(tempFolder);

                                FastZip fz = new FastZip();
                                fz.ExtractZip(path, tempFolder, ".*");

                                //Force removal of readonly flag.
                                FileInfo myFile = new FileInfo(path);
                                if ((myFile.Attributes & FileAttributes.ReadOnly) > 0)
                                    myFile.Attributes &= ~FileAttributes.ReadOnly;

                                GeneralHelper.RecursiveMove(tempFolder, basePath);
                                GeneralHelper.FileDelete(path);

                                // So now we have the skin extracted in Skins/SkinName/
                                // but they may still be in a subdirectory depending on how the skin was packaged.
                                int maximumTries = 5;

                                while (Directory.GetFiles(basePath).Length < 1 && maximumTries-- > 0)
                                {
                                    string[] dirs = Directory.GetDirectories(basePath);
                                    GeneralHelper.RecursiveMove(dirs[0], basePath);
                                }

                                ConfigManager.sSkin.Value = filenameNoExt;
                            }
                            catch
                            {
                                NotificationManager.ShowMessage("Unable to import skin \"" + filename + "\".");
                            }
                        }
                        InputManager.ReplayMode = false;
                        return OsuModes.SkinSelect;
                    case "avi":
                    case "mpg":
                    case "flv":
                    case "jpg":
                    case "png":
                        if (Mode == OsuModes.Menu && (BanchoClient.Permission & Permissions.Supporter) > 0 && (ext == "png" || ext == "jpg"))
                        {
                            SkinManager.CreateUserSkin();

                            string dest = GeneralHelper.PathSanitise(Path.Combine(Path.Combine(@"Skins", SkinManager.Current.RawName), @"menu-background.jpg"));

                            GeneralHelper.FileDelete(dest);
                            File.Copy(firstFile, dest);
                            SkinManager.LoadSkin(ConfigManager.sSkin, true);
                            return OsuModes.Menu;
                        }

                        if (Mode != OsuModes.Edit)
                        {
                            NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.General_File_EnterEditor));
                            return OsuModes.Unknown;
                        }

                        pDialogConfirmation d = new pDialogConfirmation("Would you like to use \"" + Path.GetFileName(firstFile) + "\" as your map's background?",
                            delegate { Editor.Instance.Design.InsertBackground(firstFile); }, null);
                        GameBase.ShowDialog(d);

                        return OsuModes.Unknown;
                    default:
                        if (firstFile.StartsWith(@"osu://"))
                        {
                            if (BanchoClient.Connected)
                                ChatEngine.HandleLink(firstFile);
                            else
                            {
                                VoidDelegate del = null;
                                del = delegate
                                {
                                    ChatEngine.HandleLink(firstFile);
                                    BanchoClient.OnPermissionChange -= del;
                                };

                                BanchoClient.OnPermissionChange += del;
                            }

                            return OsuModes.Unknown;
                        }

                        if (!firstFile.StartsWith(@"-") && firstFile.Contains(@"."))
                            NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.General_File_UnknownType) + @" (" + firstFile + @")");
                        return OsuModes.Unknown;
                }
            }
            catch (Exception ex)
            {
                TopMostMessageBox.Show("Error moving file " + files + "\n" + ex);
            }

            return OsuModes.Unknown;
        }

        internal void UpdateChildren()
        {
            ChildrenUpdated = true;
            base.Update();
        }

        internal static void PackageFile(string fileName, string saveDirectory)
        {
            if (!Directory.Exists(@"Exports")) Directory.CreateDirectory(@"Exports");

            string winFileName = @"Exports\" + GeneralHelper.WindowsFilenameStrip(fileName);
            ZipConstants.DefaultCodePage = 932;
            FastZip fz = new FastZip();
            fz.CreateZip(winFileName, saveDirectory, true, "[^z]+$");

            NotificationManager.ShowMessage(fileName + " has been exported.  Opening the Exports folder...", Color.YellowGreen, 6000);
            GameBase.OpenFolder(@"Exports");
        }

        internal static void OpenFolder(string folderPath)
        {
            if (String.IsNullOrEmpty(folderPath)) return;

            if (folderPath[folderPath.Length - 1] != Path.DirectorySeparatorChar)
                folderPath += Path.DirectorySeparatorChar;
            GameBase.ProcessStart(folderPath);
        }

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern bool SetForegroundWindow(IntPtr hwnd);

        [SuppressUnmanagedCodeSecurityAttribute()]
        [DllImport(@"user32.dll")]
        internal static extern bool FlashWindow(IntPtr hwnd, bool bInvert);

        [DllImport(@"user32.dll")]
        public static extern bool GetLayeredWindowAttributes(IntPtr hwnd, out IntPtr pcrKey, out IntPtr pbAlpha, out IntPtr pdwFlags);

        internal static void FlashWindow()
        {
            if (GameBase.Tournament) return;

            try
            {
                if (ConfigManager.sChatAudibleHighlight)
                    AudioEngine.PlaySample(@"match-start");
                Form.Invoke(new VoidDelegate(delegate { FlashWindow(Form.Handle, true); }));
            }
            catch { }
        }

        internal static void Restart(VoidDelegate restartAction = null)
        {
            if (restartAction != null) exitAction += restartAction;
            osuMain.Restart = true;
            ChangeMode(OsuModes.Exit);
        }

        internal static bool IsFormOpacityHacked()
        {
            try
            {
                IntPtr pcr = IntPtr.Zero;
                IntPtr alpha = IntPtr.Zero;
                IntPtr flag = new IntPtr(0x00000002);//LWA_ALPHA
                if (GetLayeredWindowAttributes(Form.Handle, out pcr, out alpha, out flag))
                    return alpha.ToInt32() < 255;
            }
            catch { }
            return false;
        }


        internal static MusicControl MusicControl;
        internal static Options Options;
        private static pSprite s_WidescreenLeft;
        private static pSprite s_WidescreenRight;



        static Thread graphicsThread;
        static Queue<VoidDelegate> graphicsThreadWorkQueue = new Queue<VoidDelegate>();
        internal static Thread RunGraphicsThread(VoidDelegate run = null)
        {
            if (IsMinimized || (OGL && !GlControl.AllowBackgroundRender))
            {
                if (run != null) GameBase.Scheduler.Add(run, true);
                return MainThread;
            }

            lock (graphicsThreadWorkQueue)
            {
                if (run != null) graphicsThreadWorkQueue.Enqueue(run);

                if (graphicsThreadWorkQueue.Count > 0 && (graphicsThread == null || !graphicsThread.IsAlive))
                {
                    graphicsThread = RunBackgroundThread(delegate
                    {
                        if (OGL) GlControl.MakeCurrent(true);

                        while (true)
                        {
                            VoidDelegate work = null;

                            lock (graphicsThreadWorkQueue)
                            {
                                if (graphicsThreadWorkQueue.Count != 0)
                                    work = graphicsThreadWorkQueue.Dequeue();
                            }

#if !DEBUG
                            try
                            {
#endif
                            if (work != null)
                            {
                                work();
                            }
#if !DEBUG
                            }
                            catch { }
#endif

                            try
                            {
                                Thread.Sleep(20);
                            }
                            catch { }
                        }
                    });
                }
            }

            return graphicsThread;
        }

        internal static Thread RunBackgroundThread(VoidDelegate d, bool waitForCompletion = false, ThreadPriority priority = ThreadPriority.Normal)
        {
            Thread t;

            try
            {
                t = new Thread(new ThreadStart(d));
                t.IsBackground = !waitForCompletion;
                t.Priority = priority;
                t.Start();
            }
            catch { return null; }

            return t;
        }

        internal static Thread MainThread;
        private bool ChildrenUpdated;
        private int oneSecondUpdate;

        public static int LastForcedGC;
        internal static Obfuscated<string> UniqueCheck = "unknown";
        internal static Obfuscated<string> UniqueId = "unknown";
        internal static Obfuscated<string> UniqueId2 = "unknown";

        internal static bool BenchmarkMode;
        internal static string IpcChannelName = "osu!";
        public static bool BackgroundLoading;
        internal static void ForceGC(bool ignoreTime = false)
        {
            if (GameBase.Time - LastForcedGC < 5000 && !ignoreTime) return;

            LastForcedGC = GameBase.Time;

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        internal static void ProcessStart(string process, string flags = null)
        {
            if (Tournament) return;

#if ARCADE
            //We don't want the user to be able to start any external processes.
            //This includes the ability to open URLs (which is handled through this method).
            return;
#endif

            Scheduler.Add(delegate
            {
                if (Form != null)
                {
                    if (graphics != null && graphics.IsFullScreen)
                        Form.WindowState = FormWindowState.Minimized;

                    Form.TopMost = false;
                    IsActive = false;
                }

                try
                {
                    ProcessStartInfo psi = new ProcessStartInfo(process, flags);
                    Process.Start(psi);
                }
                catch (Exception)
                { }
            });
        }

        public static int IdleTime
        {
            get
            {
                return GameBase.Time - InputManager.LastActionTime;
            }
        }

        public static bool HasPendingDialog { get { return ActiveDialog != null || DialogQueue.Count > 0; } }

        public static bool UseHighResolutionSprites { get { return (WindowHeight >= 800 || ConfigManager.sHighResolution) && !ConfigManager.sLowResolution; } }

        public static bool NewGraphicsAvailable;
        internal static Size InitialDesktopResolution;
        internal static System.Drawing.Point InitialDesktopLocation;
        public static long TotalFramesRendered;

#if !Public
        private static pSprite testBuildOverlay;
        private static pText testBuildVersion;
#endif

        internal static int ChangeAllowance;
        private bool graphicsLoaded;
        internal static Process[] Processes;

        /// <summary>
        /// Force osu! to run in a window. Required to display forms and menus.
        /// </summary>
        internal static bool ForceWindowed()
        {
            if (GameBase.CurrentScreenMode != ScreenMode.Fullscreen)
                return false;
            GameBase.SetScreenSize(null, null, ScreenMode.BorderlessWindow); //force to windowed mode
            return true;
        }

        public static bool IsWindows81
        {
            get
            {
                OperatingSystem os = Environment.OSVersion;
                return os.Platform == PlatformID.Win32NT && os.Version.Major >= 6 && os.Version.Minor >= 3;
            }
        }

        internal static void RefreshSpriteBatch()
        {
            if (spriteBatch == null) return;

            spriteBatch.Dispose();
            spriteBatch = new SpriteBatch(graphics.GraphicsDevice);
        }

        static bool minimizedToTray;
        public static bool MinimizedToTray
        {
            get
            {
                return minimizedToTray;
            }

            set
            {
                if (value == minimizedToTray) return;
                minimizedToTray = value;

                if (minimizedToTray)
                {
                    if (notifyIcon == null)
                    {
                        notifyIcon = new NotifyIcon();
                        notifyIcon.Icon = Form.Icon;
                        notifyIcon.Click += (obj, e) => { MinimizedToTray = false; };
                    }

                    notifyIcon.Visible = true;

                    if (ConfigManager.sBossKeyFirstActivation)
                    {
                        notifyIcon.ShowBalloonTip(10000, LocalisationManager.GetString(OsuString.BossKeyActivated), LocalisationManager.GetString(OsuString.BossKeyActivated_Tooltip), ToolTipIcon.Info);
                        ConfigManager.sBossKeyFirstActivation.Value = false;
                    }

                    Form.WindowState = FormWindowState.Minimized;
                    Form.Visible = false;

                    Player p = Player.Instance;
                    if (p != null && !(p is PlayerVs && Player.Loaded) && !AudioEngine.Paused && !Player.Paused && !InputManager.ReplayMode)
                        p.TogglePause();

                    bossKeyRestoreVolume = AudioEngine.VolumeMaster.Value;
                    AudioEngine.VolumeMaster.Value = 0;
                }
                else
                {
                    Form.Visible = true;
                    Form.WindowState = FormWindowState.Normal;

                    Form.ShowInTaskbar = true;
                    notifyIcon.Visible = false;

                    BringToFront();
                    AudioEngine.VolumeMaster.Value = bossKeyRestoreVolume;
                }
            }
        }

        /// <summary>
        /// Are we able to (forcefully) reload the current game mode without having an adverse effect on the overall state of osu!?
        /// </summary>
        public static bool ModeCanReload
        {
            get
            {
                switch (Mode)
                {
                    case OsuModes.Play:
                    case OsuModes.Rank:
                    case OsuModes.RankingVs:
                    case OsuModes.RankingTeam:
                    case OsuModes.RankingTagCoop:
                        return false;
                }

                return true;
            }
        }
    }

    public enum OsuModes
    {
        Menu,
        Edit,
        Play,
        Exit,
        SelectEdit,
        SelectPlay,
        SkinSelect,
        Rank,
        Update,
        Busy,
        Unknown,
        Lobby,
        MatchSetup,
        SelectMulti,
        RankingVs,
        OnlineSelection,
        OptionsOffsetWizard,
        RankingTagCoop,
        RankingTeam,
        BeatmapImport,
        PackageUpdater,
        Benchmark,
        Tourney,
        Charts
    };

    internal enum FadeStates
    {
        FadeOut,
        FadeIn,
        Idle,
        WaitingLoad,
    } ;

    internal enum FrameSync
    {
        VSync = 1,
        Limit120 = 0,
        Unlimited = 2,
        LowLatencyVSync = 3,
        CompletelyUnlimited = 4,
        Limit240 = 5
    } ;
}
