﻿using osu.GameplayElements.Tips;
using osu.Graphics.Notifications;
using osu.Graphics.Primitives;
using System;
using System.Collections.Generic;
using System.Text;

namespace osu.Graphics.Tips
{
    internal static class OsuTipManager
    {
        private static List<bool> displayed = new List<bool>();
        private static readonly Queue<OsuTip> TipQueue = new Queue<OsuTip>();
        private static int nextQueueDisplay;

        //must be called before first tip show.
        internal static void ReadFromString(string display)
        {
            displayed.Clear();
            foreach (char c in display)
            {
                if (c == '1')
                    displayed.Add(true);
                else
                    displayed.Add(false);
            }
            //ensure the same length
            if (displayed.Count < OsuTipConstant.Tips.Length)
            {
                for (int i = displayed.Count; i < OsuTipConstant.Tips.Length; i++)
                    displayed.Add(false);
            }
        }

        internal static string WriteToString()
        {
            StringBuilder sb = new StringBuilder(100);
            foreach (bool bl in displayed)
                sb.Append(bl ? "1" : "0");
            return sb.ToString();
        }

        internal static void ShowTip(int index, bool forceClear = false)
        {
            if (forceClear)
                ClearTips();
            if (index < 0 || index >= displayed.Count || displayed[index] || index >= OsuTipConstant.Tips.Length)
                return;
            //parse tip string
            int next = -1;
            try
            {
                string tip = OsuTipConstant.Tips[index];
                string[] arr = tip.Split(']');
                if (arr.Length != 2)
                    return;
                arr[0] = arr[0].Substring(1); // remove [
                string[] param = arr[0].Split(',');
                if (param.Length < 4)
                    return;
                int duration = param.Length > 4 ? Int32.Parse(param[4]) : 4000;
                int x;
                char snap = param[0][0];
                if (snap == 'R')
                    x = GameBase.WindowWidthScaled - Int32.Parse(param[0].Substring(1));
                else if (snap == 'W')
                    x = (int)GameBase.WindowOffsetXScaled + Int32.Parse(param[0].Substring(1));
                else
                    x = Int32.Parse(param[0]);
                RectangleF rect = new RectangleF(x, Int32.Parse(param[1]), Int32.Parse(param[2]), Int32.Parse(param[3]));
                OsuTip t = new OsuTip(arr[1], duration, rect);
                t.Index = index;
                TipQueue.Enqueue(t);
                if (param.Length > 5)
                {
                    //support both relative and absolute move.
                    char c = param[5][0];
                    if (c == '+')
                        next = index + Int32.Parse(param[5].Substring(1));
                    else if (c == '-')
                        next = index - Int32.Parse(param[5].Substring(1));
                    else
                        next = Int32.Parse(param[5]);
                }
            }
            catch (Exception e){}
            if (next != -1)
                ShowTip(next);
        }

        internal static void ClearTips()
        {
            TipQueue.Clear();
        }

        internal static void Update()
        {
            if (GameBase.Time < nextQueueDisplay || TipQueue.Count == 0)
                return;
            /*
             * TODO: there should have some pause logic for playing tips display.
             */
            OsuTip tip = TipQueue.Dequeue();
            tip.Show();
            displayed[tip.Index] = true;
            nextQueueDisplay = GameBase.Time + tip.Duration + 300;
            GameBase.Scheduler.Add(() =>
            {
                GameBase.spriteManagerOverlayHighest.Add(tip.SpriteCollection);
                NotificationManager.ShowMessageMassive(tip.Tip, tip.Duration);
            });
        }
    }
}
