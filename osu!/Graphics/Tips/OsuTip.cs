﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Notifications;
using osu.Graphics.Primitives;
using osu.Graphics.Renderers;
using osu.Graphics.Sprites;
using System;
using System.Collections.Generic;
using System.Text;

namespace osu.GameplayElements.Tips
{
    /// <summary>
    /// Guide tip for newbies.
    /// It has a highlight window focus on new feature area.
    /// </summary>
    internal class OsuTip
    {
        internal List<pSprite> SpriteCollection;
        internal int Duration;
        internal string Tip;
        internal int Index;

        internal OsuTip(string tip, int duration, RectangleF focusRect)
        {
            Duration = duration;
            Tip = tip;
            SpriteCollection = new List<pSprite>(4);
            if (focusRect.Width == 0 || focusRect.Height == 0)
                return;
            //left part
            pSprite p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(0, 0), 0.9f, false, Color.Black);
            p.Alpha = 0.94f;
            p.VectorScale = new Vector2(focusRect.X, 480) * 1.6F;
            SpriteCollection.Add(p);
            //right part
            p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(focusRect.X+focusRect.Width, 0), 0.9f, false, Color.Black);
            p.Alpha = 0.94f;
            p.VectorScale = new Vector2(GameBase.WindowWidthScaled - focusRect.X - focusRect.Width, 480) * 1.6F;
            SpriteCollection.Add(p);
            //top part
            p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(focusRect.X, 0), 0.9f, false, Color.Black);
            p.Alpha = 0.94f;
            p.VectorScale = new Vector2(focusRect.Width, focusRect.Y) * 1.6F;
            SpriteCollection.Add(p);
            //bottom part
            p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(focusRect.X, focusRect.Y+focusRect.Height), 0.9f, false, Color.Black);
            p.Alpha = 0.94f;
            p.VectorScale = new Vector2(focusRect.Width, 480 - focusRect.Y - focusRect.Height) * 1.6F;
            SpriteCollection.Add(p);
        }

        internal void Show()
        {
            foreach (pSprite ps in SpriteCollection)
            {
                ps.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.8f, GameBase.Time, GameBase.Time + 100));
                ps.Transformations.Add(new Transformation(TransformationType.Fade, 0.8f, 0, GameBase.Time + Duration, GameBase.Time + Duration + 150));
            }
        }
    }
}
