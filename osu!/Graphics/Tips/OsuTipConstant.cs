﻿using System;
using System.Collections.Generic;
using System.Text;

namespace osu.Graphics.Tips
{
    internal static class OsuTipConstant
    {
        //format: [x,y,width,height,duration,next]tip
        //x: (R/W)number, right for right snap, W for wide screen
        //duration: default 4000
        //next:format (+/-)number
        internal static string[] Tips = new[]
        {
            "[W350,310, 264, 31,4000]Here is a new toggle, try it out!"  
        };
    }
}
