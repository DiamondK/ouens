﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Audio;
using osu.Graphics.Sprites;

namespace osu.Graphics
{
    internal class PhysicsManager
    {
        private List<GravitySprite> GravitySprites = new List<GravitySprite>();

        internal PhysicsManager()
        {
        }

        internal GravitySprite Add(GravitySprite g)
        {
            GravitySprites.Add(g);

            return g;
        }
        internal GravitySprite Add(pSprite p, Vector2? initial = null, bool bounce = false)
        {
            GravitySprite g = new GravitySprite(p, initial.HasValue ? initial.Value : Vector2.Zero);
            Add(g);
            return g;
        }

        internal void Update()
        {
            //we loop back to front for more efficiency
            for (int i = GravitySprites.Count - 1; i >= 0; i--)
            {
                GravitySprite gs = GravitySprites[i];
                pSprite s = gs.sprite;

                bool removableCondition = (s.Transformations.Count == 0 || s.Transformations[s.Transformations.Count - 1].Time2 < SpriteManager.GetSpriteTime(s)) && (!s.IsVisible || s.Position.Y > 550);
                bool skippableConditionTooEarly = s.Transformations.Count > 0 && s.Transformations[0].Time1 > SpriteManager.GetSpriteTime(s);
                bool skippableConditionPaused = s.Clock == Clocks.Audio && AudioEngine.Paused;

                if (removableCondition || skippableConditionTooEarly || skippableConditionPaused)
                {
                    if (removableCondition)
                        GravitySprites.RemoveAt(i);
                    continue;
                }

                gs.gravity.Y += 8f * (float)GameBase.FrameRatio * (1 + gs.weight / 100f);

                s.Position += gs.gravity * (float)(GameBase.ElapsedMilliseconds / 1000);

                if (gs.bounce && s.Field == Fields.TopLeft && ((s.Position.X < 0 && gs.gravity.X < 0) ||
                                                     (s.Position.X > GameBase.WindowWidthScaled && gs.gravity.X > 0)))
                    gs.gravity.X = -gs.gravity.X;
            }
        }
    }
    internal class GravitySprite
    {
        internal pSprite sprite;
        internal Vector2 gravity;
        internal float weight;
        internal bool bounce;

        public GravitySprite(pSprite sprite, Vector2 initial)
        {
            this.sprite = sprite;
            this.gravity = initial;
        }
    }

}
