﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Graphics.Primitives;
using Microsoft.Xna.Framework.Graphics;

namespace osu.Graphics.Renderers
{
    /// <summary>
    /// peppysliders, d3d only
    /// </summary>
    internal class PeppySliderRenderer : LineManager, ISliderRenderer
    {
        private List<Color> m_colours;
        private Color m_tag;
        private Color m_grey;
        private Color m_border;
        private float m_default_radius;
        private float border_ratio;

        internal PeppySliderRenderer()
        {
            this.border_ratio = (GameBase.D3D && GameBase.PixelShaderVersion < 2 ? (float)19 / 20 : 1) * (59.0f / 64.0f);
        }

        public List<Color> Colours
        {
            get
            {
                return m_colours;
            }
            set
            {
                m_colours = value;
            }
        }

        public Color Tag
        {
            get
            {
                return m_tag;
            }
            set
            {
                m_tag = value;
            }
        }

        public Color Grey
        {
            get
            {
                return m_grey;
            }
            set
            {
                m_grey = value;
            }
        }

        public Color Border
        {
            get
            {
                return m_border;
            }
            set
            {
                m_border = value;
            }
        }

        public float DefaultRadius
        {
            get
            {
                return m_default_radius;
            }
            set
            {
                m_default_radius = value;
            }
        }

        public void AssignColours(List<Color> colours, Color tag, Color grey, Color border, float default_radius)
        {
            Colours = colours;
            Tag = tag;
            Grey = grey;
            Border = border;
            DefaultRadius = default_radius;
        }

        public void Draw(List<Line> lines, Line prev, float radius, Color colour, Color border_colour)
        {
            GameBase.spriteBatch.GraphicsDevice.RenderState.AlphaBlendEnable = true;
            GameBase.graphics.GraphicsDevice.RenderState.SeparateAlphaBlendEnabled = true;
            GameBase.spriteBatch.GraphicsDevice.RenderState.AlphaSourceBlend = Blend.One;
            GameBase.spriteBatch.GraphicsDevice.RenderState.AlphaDestinationBlend = Blend.InverseSourceAlpha;

            // solid border
            Draw(lines, radius * border_ratio, border_colour, 0, "StandardBorder", true);

            GameBase.graphics.GraphicsDevice.RenderState.SeparateAlphaBlendEnabled = false;
            GameBase.spriteBatch.GraphicsDevice.RenderState.AlphaSourceBlend = Blend.SourceAlpha;
            GameBase.spriteBatch.GraphicsDevice.RenderState.AlphaDestinationBlend = Blend.InverseSourceAlpha;

            // slinky inside
            GameBase.LineManager.Draw(lines, radius * 52.0f / 64.0f, new Color(colour.R, colour.G, colour.B, 255), 0, "Standard", true);
        }

        public override void Draw(List<Line> lines, Line prev, float radius, Color colour)
        {
            Draw(lines, prev, radius, colour, m_border);
        }

        public void Draw(List<Line> lines, Line prev, float radius, int colour_index)
        {
            Color colour;
            if (colour_index == -1) colour = m_grey;
            else if (colour_index == -2) colour = m_tag;
            else colour = m_colours[colour_index];
            Draw(lines, prev, radius, colour, m_border);
        }

        public void Draw(List<Line> lines, Line prev, float radius, int colour_index, Color border_colour)
        {
            Draw(lines, prev, radius, m_colours[colour_index], border_colour);
        }
    }
}
