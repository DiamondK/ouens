﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.Graphics.Notifications;
using osu.Graphics.Skinning;

namespace osu.Graphics.Renderers
{
    internal class FireRenderer : IDisposable
    {
        private Effect fire;
        private EffectParameter fireHeight;
        private EffectParameter fireTime;
        private GraphicsDevice GraphicsDevice;
        private VertexBuffer vb;
        private pTexture fireYellow;
        private pTexture fireBlue;
        private bool blueFire;
        const int BLUE_FIRE_COMBO = 500;

        internal FireRenderer()
        {
            GameBase.OnReload += GameBase_OnReload;
            GameBase.OnUnload += GameBase_OnUnload;
            Initialize();
        }

        void GameBase_OnUnload(bool b)
        {
            Dispose();
            GameBase.OnReload += GameBase_OnReload;
            GameBase.OnUnload += GameBase_OnUnload;
        }

        void GameBase_OnReload(bool b)
        {
            Initialize();
        }

        public void Dispose()
        {
            if (fire != null) fire.Dispose();
            if (vb != null) vb.Dispose();
            if (fireYellow != null) fireYellow.Dispose();
            if (fireBlue != null) fireBlue.Dispose();

            GameBase.OnReload -= GameBase_OnReload;
            GameBase.OnUnload -= GameBase_OnUnload;
        }


        internal bool Initialize()
        {
            if (GameBase.OGL)
            {
                if (ConfigManager.sComboFire)
                {
                    ConfigManager.sComboFire.Value = false;
                    NotificationManager.ShowMessage("Combo fire is not supported when running in OpenGl mode!");
                    return false;
                }

                return false;
            }

            try
            {
                GraphicsDevice = GameBase.graphics.GraphicsDevice;

                fireYellow = SkinManager.Load(@"FireBase", SkinSource.Osu);
                fireBlue = SkinManager.Load(@"FireBase2", SkinSource.Osu);

                //Effect could be a disposed reference inside ContentManager
                fire = GameBase.content.Load<Effect>("Fire").Clone(GraphicsDevice);

                fire.CurrentTechnique = fire.Techniques["Fire"];
                fireYellow = SkinManager.Load(@"FireBase", SkinSource.Osu);
                fireBlue = SkinManager.Load(@"FireBase2", SkinSource.Osu);
                fire.Parameters["fire_base_Tex"].SetValue(SkinManager.Load(@"FireBase", SkinSource.Osu).TextureXna);
                fire.Parameters["fire_distortion_Tex"].SetValue(SkinManager.Load(@"FireDistortion", SkinSource.Osu).TextureXna);
                fire.Parameters["fire_opacity_Tex"].SetValue(SkinManager.Load(@"FireOpacity", SkinSource.Osu).TextureXna);
                fireTime = fire.Parameters["Fire_Effects_Fire_Single_Pass_Vertex_Shader_time_0_X"];
                fireHeight = fire.Parameters["height"];
                fire.CommitChanges();

                float h = Math.Max(0, ((float)(double)ConfigManager.sComboFireHeight - 5) / 10f);

                customvertex[] vertices = new customvertex[6];
                vertices[0] = new customvertex(new Vector4(-1, -1, 0, 1), Vector3.Zero);
                vertices[1] = new customvertex(new Vector4(1, -1, 0, 1), Vector3.Zero);
                vertices[2] = new customvertex(new Vector4(1, h, 1, 1), Vector3.Zero);
                vertices[3] = new customvertex(new Vector4(-1, -1, 0, 1), Vector3.Zero);
                vertices[4] = new customvertex(new Vector4(-1, h, 0, 1), Vector3.Zero);
                vertices[5] = new customvertex(new Vector4(1, h, 0, 1), Vector3.Zero);

                vb = new VertexBuffer(GameBase.graphics.GraphicsDevice, customvertex.SizeInBytes * 6, ResourceUsage.WriteOnly);
                vb.SetData(vertices);
            }
            catch (Exception)
            {
                if (ConfigManager.sComboFire)
                {
#if !DEBUG
                    ConfigManager.sComboFire.Value = false;
#endif
                    Dispose();
                    NotificationManager.ShowMessage("Rendering of fire effect failed - automatically disabling.");
                    return false;
                }
            }

            return true;
        }

        internal void Draw(float height, int hitCombo)
        {
            if (hitCombo >= BLUE_FIRE_COMBO && !blueFire)
            {
                fire = GameBase.content.Load<Effect>("Fire");
                fire.CurrentTechnique = fire.Techniques["Fire"];

                fire.Parameters["fire_base_Tex"].SetValue(SkinManager.Load(@"FireBase2", SkinSource.Osu).TextureXna);
                fire.Parameters["fire_distortion_Tex"].SetValue(SkinManager.Load(@"FireDistortion", SkinSource.Osu).TextureXna);
                fire.Parameters["fire_opacity_Tex"].SetValue(SkinManager.Load(@"FireOpacity", SkinSource.Osu).TextureXna);
                fireTime = fire.Parameters["Fire_Effects_Fire_Single_Pass_Vertex_Shader_time_0_X"];
                fireHeight = fire.Parameters["height"];
                fire.CommitChanges();

                blueFire = true;
            }
            else if (hitCombo < BLUE_FIRE_COMBO && blueFire)
            {

                fire = GameBase.content.Load<Effect>("Fire");
                fire.CurrentTechnique = fire.Techniques["Fire"];

                fire.Parameters["fire_base_Tex"].SetValue(SkinManager.Load(@"FireBase", SkinSource.Osu).TextureXna);
                fire.Parameters["fire_distortion_Tex"].SetValue(SkinManager.Load(@"FireDistortion", SkinSource.Osu).TextureXna);
                fire.Parameters["fire_opacity_Tex"].SetValue(SkinManager.Load(@"FireOpacity", SkinSource.Osu).TextureXna);
                fireTime = fire.Parameters["Fire_Effects_Fire_Single_Pass_Vertex_Shader_time_0_X"];
                fireHeight = fire.Parameters["height"];
                fire.CommitChanges();

                fire.Parameters["fire_base_Tex"].SetValue(fireYellow.TextureXna);
                blueFire = false;
            }

            try
            {
                fireTime.SetValue((float)AudioEngine.Time / 1000);
                fireHeight.SetValue(height);
                fire.CommitChanges();

                fire.Begin();
                foreach (EffectPass pass in fire.CurrentTechnique.Passes)
                {
                    pass.Begin();
                    GraphicsDevice.VertexDeclaration = new VertexDeclaration(GraphicsDevice, customvertex.Elements);
                    GraphicsDevice.Vertices[0].SetSource(vb, 0, customvertex.SizeInBytes);
                    GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
                    pass.End();
                }
                fire.End();
            }
            catch (Exception)
            {
#if !DEBUG
                ConfigManager.sComboFire.Value = false;
#endif
                Dispose();
                NotificationManager.ShowMessage("Rendering of fire effect failed - automatically disabling.");
            }
        }

        #region Nested type: customvertex

        private struct customvertex
        {
            internal const int SizeInBytes = sizeof(float) * 7;

            internal static readonly VertexElement[] Elements =
                {
                    new VertexElement(0, 0, VertexElementFormat.Vector4, VertexElementMethod.Default,
                                      VertexElementUsage.Position, 0),
                    new VertexElement(0, 0, VertexElementFormat.Vector3, VertexElementMethod.Default,
                                      VertexElementUsage.TextureCoordinate, 0)
                };

            private Vector4 position;
            private Vector3 texturepos;

            internal customvertex(Vector4 position, Vector3 texturepos)
            {
                this.position = position;
                this.texturepos = texturepos;
            }
        }

        #endregion

    }
}
