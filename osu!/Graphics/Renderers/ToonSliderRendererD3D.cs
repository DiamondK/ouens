﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using osu.Helpers;
using Microsoft.Xna.Framework;
using osu.GameplayElements;

namespace osu.Graphics.Renderers
{
    class ToonSliderRendererD3D : TexturedSliderRendererD3D
    {
        private const int TEX_WIDTH = 128; // Please keep power of two

        protected override Texture2D CreateTexture(Color colour, Color border)
        {
            Color inner, outer;
            ComputeSliderColour(colour, out inner, out outer);
            return RenderSliderTexture(border, inner, outer);
        }

        /// <summary>
        /// Helper function to turn a single colour into a lighter and darker shade for use with the slider's gradient.
        /// </summary>
        /// <param name="colour">HitObject colour</param>
        /// <param name="InnerColour">Track center</param>
        /// <param name="OuterColour">Track edges</param>
        internal static void ComputeSliderColour(Color colour, out Color InnerColour, out Color OuterColour)
        {
            Color col = new Color(colour.R, colour.G, colour.B, (byte)180);
            InnerColour = ColourHelper.Lighten2(col, 0.5f);
            OuterColour = ColourHelper.Darken(col, 0.1f);
        }

        /// <summary>
        /// Render a gradient into a 256x1 texture.
        /// </summary>
        private Texture2D RenderSliderTexture(Color shadow, Color border, Color InnerColour, Color OuterColour, float aa_width)
        {
            RenderTarget2D GradientTarget = new RenderTarget2D(device, TEX_WIDTH, 1, 1, SurfaceFormat.Color);
            VertexPositionColor[] GradientVertices;

            Color col1 = ColourHelper.ColourLerp(OuterColour, InnerColour, 1.0f / 3.0f);
            Color col2 = ColourHelper.ColourLerp(OuterColour, InnerColour, 2.0f / 3.0f);

            float stop0 = 0.1875f;
            float stop4 = 1.0f;
            float stop1 = OsuMathHelper.Lerp(stop0, stop4, 0.25f);
            float stop2 = OsuMathHelper.Lerp(stop0, stop4, 0.5f);
            float stop3 = OsuMathHelper.Lerp(stop0, stop4, 0.75f);

            GradientVertices = new VertexPositionColor[13];
            GradientVertices[0] = new VertexPositionColor(new Vector3(-0.5f, 0.0f, 0.0f), Color.TransparentBlack);
            GradientVertices[1] = new VertexPositionColor(new Vector3(0.0f, 0.0f, 0.0f), Color.TransparentBlack);
            GradientVertices[2] = new VertexPositionColor(new Vector3(0.078125f - aa_width, 0.0f, 0.0f), shadow);
            GradientVertices[3] = new VertexPositionColor(new Vector3(0.078125f + aa_width, 0.0f, 0.0f), border);
            GradientVertices[4] = new VertexPositionColor(new Vector3(stop0 - aa_width, 0.0f, 0.0f), border);
            GradientVertices[5] = new VertexPositionColor(new Vector3(stop0 + aa_width, 0.0f, 0.0f), OuterColour);
            GradientVertices[6] = new VertexPositionColor(new Vector3(stop1 - aa_width, 0.0f, 0.0f), OuterColour);
            GradientVertices[7] = new VertexPositionColor(new Vector3(stop1 + aa_width, 0.0f, 0.0f), col1);
            GradientVertices[8] = new VertexPositionColor(new Vector3(stop2 - aa_width, 0.0f, 0.0f), col1);
            GradientVertices[9] = new VertexPositionColor(new Vector3(stop2 + aa_width, 0.0f, 0.0f), col2);
            GradientVertices[10] = new VertexPositionColor(new Vector3(stop3 - aa_width, 0.0f, 0.0f), col2);
            GradientVertices[11] = new VertexPositionColor(new Vector3(stop3 + aa_width, 0.0f, 0.0f), InnerColour);
            GradientVertices[12] = new VertexPositionColor(new Vector3(1.5f, 0.0f, 0.0f), InnerColour);

            RenderTarget2D oldTarget = (RenderTarget2D)(device.GetRenderTarget(0));
            device.SetRenderTarget(0, GradientTarget);

            device.RenderState.AlphaBlendEnable = false;
            device.RenderState.DepthBufferEnable = false;
            device.RenderState.AlphaTestEnable = false;
            device.RenderState.BlendFunction = BlendFunction.Add;
            device.RenderState.AlphaBlendOperation = BlendFunction.Add;
            device.VertexDeclaration = new VertexDeclaration(device, VertexPositionColor.VertexElements);

            device.Clear(Color.PaleVioletRed);

            Effect.World = Matrix.Identity;
            Effect.Projection = Matrix.CreateOrthographicOffCenter(0.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f);
            Effect.View = Matrix.Identity;

            Effect.TextureEnabled = false;
            Effect.Texture = null;
            Effect.VertexColorEnabled = true;

            Effect.Begin();
            Effect.CurrentTechnique.Passes[0].Begin();

            device.DrawUserPrimitives(PrimitiveType.LineStrip, GradientVertices, 0, 12);

            Effect.CurrentTechnique.Passes[0].End();
            Effect.End();

            device.ResolveRenderTarget(0);

            Texture2D tex = GradientTarget.GetTexture();
            device.SetRenderTarget(0, oldTarget);

            return tex;
        }

        /// <summary>
        /// This overload computes the outer/inner colours and AA widths, and has a hardcoded shadow colour.
        /// </summary>
        private Texture2D RenderSliderTexture(Color border, Color InnerColour, Color OuterColour)
        {
            // This formula is used to keep sliders smooth for a wide variety of screen resolutions and circle sizes.
            // In most cases, it's set to equal a nice ratio of screen pixels, but it's constrained to within two values.
            // When circles are very large, the hitcircle(overlay).png textures become a bit fuzzy, so sharp sliders would look unnatural.
            // When they are tiny, we get the opposite problem. I confine it at 1/16th of a circle-radius, which is almost as wide as its border.
            float aa_width = Math.Min(Math.Max(0.5f / DefaultRadius, 3.0f / 256.0f), 1.0f / 16.0f);

            Color shadow = new Color(0, 0, 0, 64);

            return RenderSliderTexture(shadow, border, InnerColour, OuterColour, aa_width);
        }
    }
}
