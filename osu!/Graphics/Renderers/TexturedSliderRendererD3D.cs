﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Primitives;
using osu.Helpers;

namespace osu.Graphics.Renderers
{
    internal abstract class TexturedSliderRendererD3D : ISliderRenderer
    {
        private const int MAXRES = 24; // A higher MAXRES produces rounder endcaps at the cost of more vertices

        // Make the quad overhang just slightly to avoid 1px holes between a quad and a wedge from rounding errors.
        private const float QUAD_OVERLAP_FUDGE = 3.0e-4f;

        // If the peak vertex of a quad is at exactly 0, we get a crack running down the center of horizontal linear sliders.
        // We shift the vertex slightly off to the side to avoid this.
        private const float QUAD_MIDDLECRACK_FUDGE = 1.0e-4f;

        // Bias to the number of polygons to render in a given wedge. Also ... fixes ... holes.
        private const float WEDGE_COUNT_FUDGE = 0.0f; // Seems this fudge is unneeded YIPEE

        private int bytesPerVertex;
        private BasicEffect effect;
        private IndexBuffer ib_quad;
        private IndexBuffer ib_cap;
        private int numIndices_quad;
        private int numIndices_cap;
        private int numPrimitives_quad;
        private int numPrimitives_cap;
        private int numVertices_quad;
        private int numVertices_cap;
        private VertexBuffer vb;
        private VertexDeclaration vdecl;

        protected Texture2D[] textures;
        protected Texture2D texture_grey;
        protected Texture2D texture_tag;

        private VertexPositionNormalTexture[] vertices;

        private bool am_initted_geom = false;
        private bool am_initted_tex = false;
        private bool m_disposing = false;

        private List<Color> m_colours = new List<Color>();
        private Color m_tag = Color.Gray;
        private Color m_grey = Color.Gray;
        private Color m_border = Color.White;
        private float m_default_radius = 64.0f;

        internal void Initialize(GraphicsDevice device, ContentManager content)
        {
            if ((device == null) || (content == null))
#if DEBUG
                throw new InvalidOperationException("Textured sliders were initted under invalid circumstances.");
#else
                return;
#endif

            effect = new BasicEffect(device, null);

            if (!am_initted_geom)
            {
                bytesPerVertex = VertexPositionNormalTexture.SizeInBytes;
                vdecl = new VertexDeclaration(device, VertexPositionNormalTexture.VertexElements);

                numVertices_quad = 6;
                numPrimitives_quad = 4;
                numIndices_quad = 6;

                numVertices_cap = MAXRES + 2;
                numPrimitives_cap = MAXRES;
                numIndices_cap = 3 * MAXRES;

                vertices = new VertexPositionNormalTexture[numVertices_quad + numVertices_cap];

                CalculateQuadMesh();
                CalculateCapMesh();

                // Both quad and cap get dumped into a single vertex buffer.
                vb = new VertexBuffer(device, (numVertices_quad + numVertices_cap) * bytesPerVertex, ResourceUsage.None, ResourceManagementMode.Automatic);
                vb.SetData(vertices);

                am_initted_geom = true;
            }

            if (!am_initted_tex)
            {
                ComputeAllColours();
                am_initted_tex = true;
            }
        }

        public List<Color> Colours
        {
            get
            {
                return m_colours;
            }
            set
            {
                if (value.Count != m_colours.Count)
                {
                    m_colours = new List<Color>(value);
                    if (am_initted_tex) ComputeSliderColours();
                    return;
                }
                for (int x = 0; x < value.Count; x++)
                {
                    if (value[x] != m_colours[x])
                    {
                        m_colours = new List<Color>(value);
                        if (am_initted_tex) ComputeSliderColours();
                        return;
                    }
                }
            }
        }

        public Color Tag
        {
            get
            {
                return m_tag;
            }
            set
            {
                if (m_tag == value) return;
                m_tag = value;
                if (am_initted_tex) ComputeTagColour();
            }
        }

        public Color Grey
        {
            get
            {
                return m_grey;
            }
            set
            {
                if (m_grey == value) return;
                m_grey = value;
                if (am_initted_tex) ComputeGreyColour();
            }
        }

        public Color Border
        {
            get
            {
                return m_border;
            }
            set
            {
                if (m_border == value) return;
                m_border = value;
                if (am_initted_tex) ComputeAllColours();
            }
        }

        public float DefaultRadius
        {
            get
            {
                return m_default_radius;
            }
            set
            {
                if (m_default_radius == value) return;
                m_default_radius = value;
                if (am_initted_tex) ComputeAllColours();
            }
        }

        public void AssignColours(List<Color> colours, Color tag, Color grey, Color border, float default_radius)
        {
            if (m_border != border || m_default_radius != default_radius)
            {
                m_colours = new List<Color>(colours);
                m_tag = tag;
                m_grey = grey;
                m_border = border;
                m_default_radius = default_radius;
                if (am_initted_tex) ComputeAllColours();
                return;
            }
            Colours = colours;
            Tag = tag;
            Grey = grey;
        }

        public bool ProgressiveRendering
        {
            get
            {
                return true;
            }
        }

        private void ComputeSliderColours()
        {
            int colour_count = m_colours.Count;

            if (textures != null)
            {
                m_disposing = true;
                foreach (Texture2D tex in textures)
                {
                    TryDispose(tex);
                }
                m_disposing = false;
            }

            if (colour_count == 0) // Temporary catch for i332-triggered hard crash
            {
                textures = new Texture2D[1];
                textures[0] = CreateTexture(Color.TransparentBlack, m_border);
            }
            else
            {
                textures = new Texture2D[colour_count];

                for (int x = 0; x < colour_count; x++)
                {
                    textures[x] = CreateTexture(m_colours[x], m_border);
                }
            }
        }

        private void ComputeTagColour()
        {
            if (texture_tag != null)
            {
                m_disposing = true;
                TryDispose(texture_tag);
                m_disposing = false;
            }
            texture_tag = CreateTexture(m_tag, m_border);
        }

        private void ComputeGreyColour()
        {
            if (texture_grey != null)
            {
                m_disposing = true;
                TryDispose(texture_grey);
                m_disposing = false;
            }
            texture_grey = CreateTexture(m_grey, m_border);
        }

        private void ComputeAllColours()
        {
            ComputeSliderColours();
            ComputeTagColour();
            ComputeGreyColour();
        }

        protected abstract Texture2D CreateTexture(Color colour, Color border);

        /// <summary>
        /// The quad mesh is liek a tent shape :3 or toblerone. It's raised in the middle.
        /// </summary>
        private void CalculateQuadMesh()
        {
            short[] indices = new short[numIndices_quad];

            vertices[0] = new VertexPositionNormalTexture(new Vector3(-QUAD_OVERLAP_FUDGE, -1, 0), new Vector3(0, 0, 1), new Vector2(0.0f, 0.0f));
            vertices[1] = new VertexPositionNormalTexture(new Vector3(1 + QUAD_OVERLAP_FUDGE, -1, 0), new Vector3(0, 0, -1), new Vector2(0.0f, 0.0f));
            vertices[2] = new VertexPositionNormalTexture(new Vector3(-QUAD_OVERLAP_FUDGE, QUAD_MIDDLECRACK_FUDGE, 1), new Vector3(0, 0, 1), new Vector2(1.0f, 0.0f));
            vertices[3] = new VertexPositionNormalTexture(new Vector3(1 + QUAD_OVERLAP_FUDGE, QUAD_MIDDLECRACK_FUDGE, 1), new Vector3(0, 0, -1), new Vector2(1.0f, 0.0f));
            vertices[4] = new VertexPositionNormalTexture(new Vector3(-QUAD_OVERLAP_FUDGE, 1, 0), new Vector3(0, 0, 1), new Vector2(0.0f, 0.0f));
            vertices[5] = new VertexPositionNormalTexture(new Vector3(1 + QUAD_OVERLAP_FUDGE, 1, 0), new Vector3(0, 0, -1), new Vector2(0.0f, 0.0f));

            indices[0] = 0;
            indices[1] = 1;
            indices[2] = 2;
            indices[3] = 3;
            indices[4] = 4;
            indices[5] = 5;

            ib_quad = new IndexBuffer(device, numIndices_quad * 2, ResourceUsage.None, ResourceManagementMode.Automatic, IndexElementSize.SixteenBits);
            ib_quad.SetData(indices);
        }

        /// <summary>
        /// The cap mesh is a half cone.
        /// </summary>
        private void CalculateCapMesh()
        {
            short[] indices = new short[numIndices_cap];

            vertices[numVertices_quad] = new VertexPositionNormalTexture(new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0, 0, 1), new Vector2(1.0f, 0.0f)); // center

            float maxRes = (float)MAXRES;
            float step = OsuMathHelper.Pi / maxRes;

            // This avoids a rounding error with the rotation matrix causing one-pixel holes in the sliders.
            vertices[numVertices_quad + 1] = new VertexPositionNormalTexture(new Vector3(0.0f, -1.0f, 0.0f), new Vector3(0, 0, 1), new Vector2(0.0f, 0.0f));

            for (int z = 1; z < MAXRES; z++)
            {
                float angle = (float)z * step;
                vertices[numVertices_quad + z + 1] = new VertexPositionNormalTexture(new Vector3((float)(Math.Sin(angle)), -(float)(Math.Cos(angle)), 0), new Vector3(0, 0, 1), new Vector2(0.0f, 0.0f));
            }

            vertices[numVertices_quad + MAXRES] = new VertexPositionNormalTexture(new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0, 0, 1), new Vector2(0.0f, 0.0f));

            for (int z = 0; z < MAXRES; z++) // one fewer triangles
            {
                indices[3 * z] = (short)(numVertices_quad);
                indices[3 * z + 1] = (short)(numVertices_quad + z + 1);
                indices[3 * z + 2] = (short)(numVertices_quad + z + 2);
            }

            ib_cap = new IndexBuffer(device, numIndices_cap * 2, ResourceUsage.None, ResourceManagementMode.Automatic, IndexElementSize.SixteenBits);
            ib_cap.SetData(indices);
        }

        public void Draw(Line line, float radius, Color colour)
        {
            List<Line> lines = new List<Line>(1);
            lines.Add(line);

            Draw(lines, null, radius, colour);
        }

        public void Draw(List<Line> lines, Line prev, float radius, Color colour)
        {
            Draw(lines, prev, radius, colour, m_border);
        }

        public void Draw(List<Line> lines, Line prev, float radius, Color colour, Color border_colour)
        {
            Texture2D tex = CreateTexture(colour, border_colour);
            DrawD3D(lines, radius, tex, prev);
            tex.Dispose();
        }

        public void Draw(List<Line> lines, Line prev, float radius, int colour_index)
        {
            switch (colour_index)
            {
                case -1: // Grey
                    DrawD3D(lines, radius, texture_grey, prev);
                    break;
                case -2: // Multi custom
                    DrawD3D(lines, radius, texture_tag, prev);
                    break;
                default:
                    if ((colour_index >= textures.Length) || (colour_index < 0))
                    {
#if DEBUG
                        DrawD3D(lines, radius, texture_grey, prev);
                    //    throw new ArgumentOutOfRangeException("Colour index outside the range of the collection.");
#else
                        DrawD3D(lines, radius, texture_grey, prev);
#endif
                    }
                    else DrawD3D(lines, radius, textures[colour_index], prev);
                    break;
            }
        }

        public void Draw(List<Line> lines, Line prev, float radius, int colour_index, Color border_colour)
        {
            if (colour_index>=m_colours.Count)
                Draw(lines, prev, radius, Color.Gray, border_colour);
            else
                Draw(lines, prev, radius, m_colours[colour_index], border_colour);
        }

        /// <summary>
        /// Core drawing method in D3D
        /// </summary>
        /// <param name="lineList">List of lines to use</param>
        /// <param name="globalRadius">Width of the slider</param>
        /// <param name="texture">Texture used for the track</param>
        /// <param name="prev">The last line which was rendered in the previous iteration, or null if this is the first iteration.</param>
        private void DrawD3D(List<Line> lineList, float globalRadius, Texture2D texture, Line prev)
        {
            if (lineList.Count == 0) return;

            device.VertexDeclaration = vdecl;
            device.Vertices[0].SetSource(vb, 0, bytesPerVertex);

            device.RenderState.CullMode = CullMode.None;
            device.RenderState.AlphaBlendEnable = false;
            device.RenderState.DepthBufferEnable = true;
            device.RenderState.DepthBufferWriteEnable = true;
            device.RenderState.DepthBufferFunction = CompareFunction.LessEqual;

            effect.Texture = texture;
            effect.TextureEnabled = true;
            effect.VertexColorEnabled = false;

            device.SamplerStates[0].AddressU = TextureAddressMode.Clamp;
            device.SamplerStates[0].AddressV = TextureAddressMode.Clamp;

            effect.Projection = Matrix.CreateOrthographicOffCenter(0.0f, (GameBase.WindowWidth), (GameBase.WindowHeight + GameBase.WindowOffsetY), 0.0f, -1.0f, 1.5f);
            effect.View = Matrix.Identity;

            int count = lineList.Count;

            for (int x = 1; x < count; x++)
            {
                DrawLineD3D(prev, lineList[x - 1], lineList[x], globalRadius);

                prev = lineList[x - 1];
            }

            DrawLineD3D(prev, lineList[count - 1], null, globalRadius);
        }

        private void DrawLineD3D(Line prev, Line curr, Line next, float globalRadius)
        {
            EffectPass pass = effect.CurrentTechnique.Passes[0];

            // Quad
            device.Indices = ib_quad;
            effect.World = new Matrix(curr.rho, 0, 0, 0, // Scale-X
                                      0, globalRadius, 0, 0, // Scale-Y
                                      0, 0, 1, 0,
                                      0, 0, 0, 1) * curr.WorldMatrix();

            effect.Begin();
            pass.Begin();
            device.DrawIndexedPrimitives(PrimitiveType.TriangleStrip, 0, 0, numVertices_quad, 0, numPrimitives_quad);
            pass.End();
            effect.End();

            int end_triangles;
            bool flip;
            if (next == null)
            {
                flip = false; // totally irrelevant
                end_triangles = numPrimitives_cap;
            }
            else
            {
                float theta = next.theta - curr.theta;

                // keep on the +- pi/2 range.
                if (theta > Math.PI) theta -= (float)(Math.PI * 2);
                if (theta < -Math.PI) theta += (float)(Math.PI * 2);

                if (theta < 0)
                {
                    flip = true;
                    end_triangles = (int)Math.Ceiling((-theta) * MAXRES / Math.PI + WEDGE_COUNT_FUDGE);
                }
                else if (theta > 0)
                {
                    flip = false;
                    end_triangles = (int)Math.Ceiling(theta * MAXRES / Math.PI + WEDGE_COUNT_FUDGE);
                }
                else
                {
                    flip = false; // totally irrelevant
                    end_triangles = 0;
                }
            }
            end_triangles = Math.Min(end_triangles, numPrimitives_cap);

            // Cap on end
            device.Indices = ib_cap;
            if (flip) effect.World = new Matrix(globalRadius, 0, 0, 0,
                                                0, -globalRadius, 0, 0,
                                                0, 0, 1, 0,
                                                0, 0, 0, 1) * curr.EndWorldMatrix();
            else effect.World = new Matrix(globalRadius, 0, 0, 0,
                                           0, globalRadius, 0, 0,
                                           0, 0, 1, 0,
                                           0, 0, 0, 1) * curr.EndWorldMatrix();

            effect.Begin();
            pass.Begin();
            if (end_triangles > 0) device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, numVertices_quad + numVertices_cap, 0, end_triangles);
            pass.End();
            effect.End();

            // Cap on start
            bool hasStartCap = false;

            if (prev == null) hasStartCap = true;
            else if (curr.p1 != prev.p2) hasStartCap = true;

            if (hasStartCap)
            {
                // Catch for Darrinub and other slider inconsistencies. (Redpoints seem to be causing some.)
                // Render a complete beginning cap if this Line isn't connected to the end of the previous line.

                //device.Indices = ib_cap; // already set.
                effect.World = new Matrix(-globalRadius, 0, 0, 0,
                                          0, -globalRadius, 0, 0,
                                          0, 0, 1, 0,
                                          0, 0, 0, 1) * curr.WorldMatrix();

                effect.Begin();
                pass.Begin();
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, numVertices_quad + numVertices_cap, 0, numPrimitives_cap);
                pass.End();
                effect.End();
            }
        }

        protected GraphicsDevice device
        {
            get
            {
                return GameBase.graphics.GraphicsDevice;
            }
        }

        protected BasicEffect Effect
        {
            get
            {
                return effect;
            }
        }

        ~TexturedSliderRendererD3D()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool am_disposing)
        {
            if (am_disposing)
            {
                TryDispose(effect);
                TryDispose(ib_quad);
                TryDispose(ib_cap);
                TryDispose(vb);
                TryDispose(vdecl);

                if (textures == null) return;
                m_disposing = true;
                foreach (Texture2D tex in textures)
                {
                    TryDispose(tex);
                }
                TryDispose(texture_grey);
                TryDispose(texture_tag);
                m_disposing = false;
            }
        }

        private void TryDispose(IDisposable d)
        {
            if (d != null) d.Dispose();
        }
    }
}
