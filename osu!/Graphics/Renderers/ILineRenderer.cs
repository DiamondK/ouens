﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Graphics.Primitives;
using Microsoft.Xna.Framework.Graphics;

namespace osu.Graphics.Renderers
{
    internal interface ILineRenderer : IDisposable
    {
        /// <summary>
        /// Draws a Line with a custom colour. Cached textures are not used.
        /// </summary>
        /// <param name="line">Single line to draw</param>
        /// <param name="radius">Radius, within the terms of the current matrix. 0 uses the stored values.</param>
        /// <param name="colour">Colour. TransparentBlack uses the stored values.</param>
        void Draw(Line line, float radius = 0, Color colour = new Color());

        /// <summary>
        /// Draws a Line with a custom colour. Cached textures are not used.
        /// </summary>
        /// <param name="lines">Collection of lines to draw</param>
        /// <param name="prev">An already drawn segment which should be joined to this line</param>
        /// <param name="radius">Radius, within the terms of the current matrix. 0 uses the stored values.</param>
        /// <param name="colour">Colour. TransparentBlack uses the stored values.</param>
        void Draw(List<Line> lines, Line prev, float radius = 0, Color colour = new Color());

        /// <summary>
        /// If true, the renderer can draw using multiple smaller curves merged together instead of requiring a single curve all at once.
        /// </summary>
        bool ProgressiveRendering
        {
            get;
        }
    }
}
