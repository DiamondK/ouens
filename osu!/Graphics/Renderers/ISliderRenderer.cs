﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Graphics.Primitives;
using Microsoft.Xna.Framework.Graphics;

namespace osu.Graphics.Renderers
{
    internal interface ISliderRenderer : ILineRenderer
    {
        List<Color> Colours
        {
            get;
            set;
        }

        Color Tag
        {
            get;
            set;
        }

        Color Grey
        {
            get;
            set;
        }

        Color Border
        {
            get;
            set;
        }

        float DefaultRadius
        {
            get;
            set;
        }

        void AssignColours(List<Color> colours, Color tag, Color grey, Color border, float default_radius);

        /// <summary>
        /// Draws a Slider with a custom colour and border colour. Cached textures are not used.
        /// Stored values may or may not be supported, depending on the renderer.
        /// </summary>
        /// <param name="lines">Collection of lines to draw</param>
        /// <param name="prev">An already drawn segment which should be joined to this line</param>
        /// <param name="radius">Radius, within the terms of the current matrix</param>
        /// <param name="colour">Colour</param>
        /// <param name="border_colour">Non-default border colour</param>
        void Draw(List<Line> lines, Line prev, float radius, Color colour, Color border_colour);

        /// <summary>
        /// Draws a Slider with default settings. Cached textures will be used if applicable to this method.
        /// Stored values may or may not be supported, depending on the renderer.
        /// </summary>
        /// <param name="lines">Collection of lines to draw</param>
        /// <param name="prev">An already drawn segment which should be joined to this line</param>
        /// <param name="radius">Radius, within the terms of the current matrix</param>
        /// <param name="colour_index">The zero-based combo colour index. -1 is grey for Multiplayer. -2 is custom for Tag Multiplayer.</param>
        void Draw(List<Line> lines, Line prev, float radius, int colour_index);

        /// <summary>
        /// Draws a Slider with a custom colour and border colour. Cached textures are not used.
        /// Stored values may or may not be supported, depending on the renderer.
        /// </summary>
        /// <param name="lines">Collection of lines to draw</param>
        /// <param name="prev">An already drawn segment which should be joined to this line</param>
        /// <param name="radius">Radius, within the terms of the current matrix</param>
        /// <param name="colour_index">The zero-based combo colour index. -1 is grey for Multiplayer. -2 is custom for Tag Multiplayer.</param>
        /// <param name="border_colour">Non-default border colour</param>
        void Draw(List<Line> lines, Line prev, float radius, int colour_index, Color border_colour);
    }
}
