﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using Microsoft.Xna.Framework;
using osu.Helpers;
using osu.Online;
using osu_common.Helpers;
using Color = Microsoft.Xna.Framework.Graphics.Color;
using Rectangle = System.Drawing.Rectangle;
using System.Collections.Generic;
using osu.Graphics.Skinning;
using System.IO;
using System.Runtime.InteropServices;
using osu.Configuration;
using System.Diagnostics;

namespace osu.Graphics.Renderers
{
    internal static class NativeText
    {
        static readonly Brush shadowBrush = new SolidBrush(System.Drawing.Color.FromArgb(100, 0, 0, 0));
        static readonly Brush greyBrush = new SolidBrush(System.Drawing.Color.FromArgb(160, 20, 20, 20));

        enum Language
        {
            English,
            Japanese,
            Korean,
            Cyrillic,
            CJK
        }

        static bool checkJapaneseCharacter(char c)
        {
            //U+3040–U+309F Hiragana
            //U+30A0–U+30FF Katakana
            return (c >= 0x4e00 && c <= 0x9fbf) || checkHiraganaKatakana(c);
        }

        static bool checkHiraganaKatakana(char c)
        {
            return (c >= 0x3040 && c <= 0x309f) || (c >= 0x30a0 && c <= 0x30ff);
        }

        static bool checkHangul(char c)
        {
            return (c >= 0xAC00 && c <= 0xD7AF) || (c >= 0x1100 && c <= 0x11FF) || (c >= 0x3130 && c <= 0x318F) || (c >= 0x3200 && c <= 0x32FF) || (c >= 0xA960 && c <= 0xA97F) || (c >= 0xD7B0 && c <= 0xD7FF);
        }

        static bool checkCjkCharacter(char c)
        {
            //CJK Unified Ideographs                  4E00-9FFF   Common
            //CJK Unified Ideographs Extension A      3400-4DFF   Rare
            //CJK Unified Ideographs Extension B      20000-2A6DF Rare, historic
            //CJK Compatibility Ideographs            F900-FAFF   Duplicates, unifiable variants, corporate characters
            //CJK Compatibility Ideographs Supplement 2F800-2FA1F Unifiable variants
            return (c >= 0x4E00 && c <= 0x9FFF) || (c >= 0x3400 && c <= 0x4DFF) || (c >= 0x20000 && c <= 0x2A6DF) || (c >= 0xF900 && c <= 0xFAFF) || (c >= 0x2F800 && c <= 0x2FA1F);
        }

        static bool checkCyrillic(char c)
        {
            //Cyrillic: U+0400–U+04FF, 256 characters
            //Cyrillic Supplement: U+0500–U+052F, 48 characters
            //Cyrillic Extended-A: U+2DE0–U+2DFF, 32 characters
            //Cyrillic Extended-B: U+A640–U+A69F, 96 characters
            //Phonetic Extensions: U+1D2B, U+1D78, 2 Cyrillic characters
            return (c >= 0x0400 && c <= 0x04FF) || (c >= 0x0500 && c <= 0x052F) || (c >= 0x2DE0 && c <= 0x2DFF) || (c >= 0xA640 && c <= 0xA69F);
        }

        private static Language detectLanguage(string text)
        {
            bool hasCjk = false;

            foreach (char c in text)
            {
                if (checkHiraganaKatakana(c)) return Language.Japanese;
                if (checkHangul(c)) return Language.Korean;
                if (checkCyrillic(c)) return Language.Cyrillic;
                hasCjk |= checkCjkCharacter(c);
            }

            return hasCjk ? Language.CJK : Language.English;
        }

        static object createTextLock = new object();

#if DEBUG
        static int currentSecond;
        static int drawCount;
#endif

        internal static pTexture CreateText(string text, float size, Vector2 restrictBounds, Color color, ShadowType shadow, bool bold, bool underline, TextAlignment alignment, bool forceAa, out Vector2 measured, out RectangleF[] characterRegions, Color background, Color border, int borderWidth, bool measureOnly, bool getCharacterRegions, string fontFace, Vector4 cornerBounds, Vector2 padding, pTexture lastTexture = null, int startIndex = 0, int length = -1)
        {
#if DEBUG
            if (text != null && !text.Contains(@"NativeText"))
            {
                int limit_per_second = osu.GameModes.Play.Player.Playing ? 5 : 58;
                bool newSecond = GameBase.Time / 1000 != currentSecond;

                drawCount++;

                if (drawCount > limit_per_second)
                {
                    Debug.Print(@"NativeText: High number of text refreshes per second.");
                    drawCount = 0;
                }

                if (newSecond)
                {
                    currentSecond = GameBase.Time / 1000;
                    drawCount = 0;
                }
            }
#endif

            //This lock ensures we are only using the shared GDI+ object (FromHwnd) in one place at a time.
            lock (createTextLock)
            {
                characterRegions = null;

                try
                {
                    using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromHwnd(IntPtr.Zero))
                    using (StringFormat sf = new StringFormat())
                    {
                        if (dpiRatio == 0) dpiRatio = 96 / graphics.DpiX;

                        size *= dpiRatio;

                        if (text == null)
                        {
                            measured = Vector2.Zero;
                            return null;
                        }

                        graphics.TextRenderingHint = TextRenderingHint.AntiAlias;

                        SizeF measuredSize;

                        if (fontFace.StartsWith(@"Aller"))
                        {
                            //if we are using the default osu! font, allow specific language overrides based on simple detection.
                            string fontFaceOverride = getLanguageSpeicificFont(text);
                            if (fontFaceOverride != null)
                                fontFace = fontFaceOverride;
                        }

                        if (DateTime.Now.Month == 4 && DateTime.Now.Day == 1)
                            fontFace = @"Comic Sans MS";

                        if (startIndex != 0 || length > 0)
                            text = text.Substring(startIndex, length);
                        else if (length == -1)
                            length = text.Length;


                        if (size < 20 && fontFace.EndsWith(@" Light"))
                            fontFace = fontFace.Replace(@" Light", string.Empty);

                        FontStyle fs = FontStyle.Regular;
                        if (bold)
                        {
                            if (fontFace.EndsWith(@" Light"))
                                fontFace = fontFace.Replace(@" Light", string.Empty);
                            fs |= FontStyle.Bold;
                        }
                        if (underline) fs |= FontStyle.Underline;

                        switch (alignment)
                        {
                            case TextAlignment.Left:
                            case TextAlignment.LeftFixed:
                                sf.Alignment = StringAlignment.Near;
                                break;
                            case TextAlignment.Centre:
                                sf.Alignment = StringAlignment.Center;
                                break;
                            case TextAlignment.Right:
                                sf.Alignment = StringAlignment.Far;
                                break;
                        }

                        if (!osuMain.IsWine && fontFace.StartsWith(@"Aller"))
                        {
                            for (char c = '0'; c <= '9'; c++)
                                text = text.Replace(c, (char)(c + (0xf83c - '0')));
                        }

                        Font f = GetFont(fontFace, size * ScaleModifier, fs);
                        if (ScaleModifier != 1) restrictBounds *= ScaleModifier;

                        try
                        {
                            if (text.Length == 0) text = " ";
                            measuredSize = restrictBounds != Vector2.Zero
                                                ? graphics.MeasureString(text, f, new SizeF(restrictBounds.X, restrictBounds.Y), sf)
                                                : graphics.MeasureString(text, f);
                        }
                        catch (InvalidOperationException)
                        {
                            measured = Vector2.Zero;
                            return null;
                        }

                        int width = (int)(measuredSize.Width + 1);
                        int height = (int)(measuredSize.Height + 1);

                        if (restrictBounds.Y != 0)
                            height = (int)restrictBounds.Y;

                        if (restrictBounds.X != 0 && (alignment != TextAlignment.Left || background.A > 0))
                            width = (int)restrictBounds.X;

                        if (padding != Vector2.Zero && restrictBounds == Vector2.Zero)
                        {
                            width += (int)(padding.X * 2);
                            height += (int)(padding.Y * 2);
                        }

                        measured = new Vector2(width, height);
                        float offset = Math.Max(0.5f, Math.Min(1f, (size * ScaleModifier) / 14));

                        if (getCharacterRegions)
                        {
                            characterRegions = new RectangleF[text.Length];

                            // SetMeasurableCharacterRanges only accepts a maximum of 32 intervals to be queried, so we as the library user are
                            // forced to split the string into 32 character long chunks and perform MeasureCharacterRanges on each.
                            int numIntervals = (text.Length / 32) + 1;
                            for (int i = 0; i < numIntervals; ++i)
                            {
                                int offsetIndex = i * 32;
                                int end = Math.Min(text.Length - offsetIndex, 32);

                                CharacterRange[] characterRanges = new CharacterRange[end];
                                for (int j = 0; j < end; ++j)
                                {
                                    characterRanges[j] = new CharacterRange(j + offsetIndex, 1);
                                }

                                sf.SetMeasurableCharacterRanges(characterRanges);
                                Region[] regions = graphics.MeasureCharacterRanges(
                                    text,
                                    f,
                                    new RectangleF(
                                        padding.X,
                                        padding.Y,
                                        restrictBounds.X == 0 ? Single.PositiveInfinity : restrictBounds.X,
                                        restrictBounds.Y == 0 ? Single.PositiveInfinity : restrictBounds.Y),
                                    sf);

                                for (int j = 0; j < end; ++j)
                                {
                                    Region region = regions[j] as Region;
                                    characterRegions[j + offsetIndex] = region.GetBounds(graphics);
                                }
                            }
                        }

                        if (measureOnly)
                        {
                            int startSpace = 0;
                            int endSpace = 0;

                            int i = 0;
                            while (i < text.Length && text[i++] == ' ')
                                startSpace++;
                            int j = text.Length - 1;
                            while (j >= i && text[j--] == ' ')
                                endSpace++;
                            if (startSpace == text.Length)
                                endSpace += startSpace;

                            measured = new Vector2(width + (endSpace * 5.145f * size / 12), height);
                            return null;
                        }

                        using (Bitmap b = new Bitmap(width, height, PixelFormat.Format32bppArgb))
                        using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(b))
                        {
                            //Quality settings
                            g.TextRenderingHint = graphics.TextRenderingHint;
                            g.SmoothingMode = SmoothingMode.HighQuality;
                            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                            if (background.A > 0)
                            {
                                if (cornerBounds != Vector4.Zero)
                                {
                                    fillRoundedRectangle(g, new Rectangle(0, 0, width, height), new SolidBrush(OsuMathHelper.CConvert(background)), cornerBounds);

                                    if (borderWidth > 0)
                                        drawRoundedRectangle(g,
                                            new Rectangle(0, 0, width - (int)Math.Ceiling(borderWidth / 2f), height - (int)Math.Ceiling(borderWidth / 2f)),
                                            new Pen(OsuMathHelper.CConvert(border), borderWidth),
                                            cornerBounds);
                                }
                                else
                                {
                                    g.Clear(OsuMathHelper.CConvert(background));
                                    if (borderWidth > 0)
                                        g.DrawRectangle(new Pen(OsuMathHelper.CConvert(border), borderWidth),
                                                        new Rectangle(borderWidth / 2, borderWidth / 2, width - borderWidth, height - borderWidth));
                                }
                            }
                            else
                            {
                                g.Clear(System.Drawing.Color.FromArgb(1, color.R, color.G, color.B));
                            }


                            using (Brush brush = new SolidBrush(OsuMathHelper.CConvert(color)))
                            {
                                if (restrictBounds != Vector2.Zero)
                                {
                                    restrictBounds.X -= padding.X * 2;
                                    restrictBounds.Y -= padding.Y * 2;

                                    switch (shadow)
                                    {
                                        case ShadowType.Normal:
                                            g.DrawString(text, f, shadowBrush, new RectangleF(padding.X - offset, offset + padding.Y, restrictBounds.X, restrictBounds.Y), sf);
                                            g.DrawString(text, f, shadowBrush, new RectangleF(padding.X + offset, offset + padding.Y, restrictBounds.X, restrictBounds.Y), sf);
                                            break;
                                        case ShadowType.Border:
                                            Brush borderBrush = greyBrush;
                                            if (background.A == 0 && borderWidth == 1 && border.A > 0)
                                                borderBrush = new SolidBrush(OsuMathHelper.CConvert(border));

                                            g.DrawString(text, f, borderBrush, new RectangleF(padding.X + offset, padding.Y + offset, restrictBounds.X, restrictBounds.Y), sf);
                                            g.DrawString(text, f, borderBrush, new RectangleF(padding.X + offset, padding.Y - offset, restrictBounds.X, restrictBounds.Y), sf);
                                            g.DrawString(text, f, borderBrush, new RectangleF(padding.X - offset, padding.Y + offset, restrictBounds.X, restrictBounds.Y), sf);
                                            g.DrawString(text, f, borderBrush, new RectangleF(padding.X - offset, padding.Y - offset, restrictBounds.X, restrictBounds.Y), sf);
                                            break;
                                    }

                                    g.DrawString(text, f, brush, new RectangleF(padding.X, padding.Y, restrictBounds.X, restrictBounds.Y), sf);
                                }
                                else
                                {
                                    switch (shadow)
                                    {
                                        case ShadowType.Normal:
                                            g.DrawString(text, f, shadowBrush, padding.X - offset, padding.Y + offset);
                                            g.DrawString(text, f, shadowBrush, padding.X + offset, padding.Y + offset);
                                            break;
                                        case ShadowType.Border:
                                            Brush borderBrush = greyBrush;
                                            if (background.A == 0 && borderWidth == 1 && border.A > 0)
                                                borderBrush = new SolidBrush(OsuMathHelper.CConvert(border));

                                            g.DrawString(text, f, borderBrush, padding.X + offset, padding.Y + offset);
                                            g.DrawString(text, f, borderBrush, padding.X - offset, padding.Y + offset);
                                            g.DrawString(text, f, borderBrush, padding.X + offset, padding.Y - offset);
                                            g.DrawString(text, f, borderBrush, padding.X - offset, padding.Y - offset);
                                            break;
                                    }

                                    g.DrawString(text, f, brush, padding.X, padding.Y);
                                }
                            }

                            BitmapData data = b.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, b.PixelFormat);

                            if (lastTexture == null || lastTexture.isDisposed || lastTexture.Width != width || lastTexture.Height != height)
                                lastTexture = pTexture.FromRawBytes(data.Scan0, width, height, true, text);
                            else
                                lastTexture.SetData(data.Scan0, width, height, text);

                            b.UnlockBits(data);

                            return lastTexture;
                        }
                    }
                }
                catch (Exception e)
                {
                    measured = Vector2.Zero;
                    return null;
                }
            }
        }

        static void fillRoundedRectangle(System.Drawing.Graphics graphics, Rectangle rectangle, Brush brush, Vector4 corners)
        {
            using (GraphicsPath path = roundedPath(rectangle, corners))
                graphics.FillPath(brush, path);
        }

        static void drawRoundedRectangle(System.Drawing.Graphics graphics, Rectangle rectangle, Pen pen, Vector4 corners)
        {
            using (GraphicsPath path = roundedPath(rectangle, corners))
                graphics.DrawPath(pen, path);
        }

        static GraphicsPath roundedPath(Rectangle r, Vector4 corners)
        {
            GraphicsPath path = new GraphicsPath();

            int d = (int)(corners.W * 2);
            path.AddLine(r.Left + d, r.Top, r.Right - d, r.Top);
            if (d > 0) path.AddArc(Rectangle.FromLTRB(r.Right - d, r.Top, r.Right, r.Top + d), -90, 90);

            d = (int)(corners.X * 2);
            path.AddLine(r.Right, r.Top + d, r.Right, r.Bottom - d);
            if (d > 0) path.AddArc(Rectangle.FromLTRB(r.Right - d, r.Bottom - d, r.Right, r.Bottom), 0, 90);

            d = (int)(corners.Y * 2);
            path.AddLine(r.Right - d, r.Bottom, r.Left + d, r.Bottom);
            if (d > 0) path.AddArc(Rectangle.FromLTRB(r.Left, r.Bottom - d, r.Left + d, r.Bottom), 90, 90);

            d = (int)(corners.Z * 2);
            path.AddLine(r.Left, r.Bottom - d, r.Left, r.Top + d);
            if (d > 0) path.AddArc(Rectangle.FromLTRB(r.Left, r.Top, r.Left + d, r.Top + d), 180, 90);

            path.CloseFigure();
            return path;
        }

        private static string getLanguageSpeicificFont(string text)
        {
            // we want to fallback to some special fonts when asian languages are used.
            switch (detectLanguage(text))
            {
                case Language.CJK:
                    switch (ConfigManager.sLanguage)
                    {
                        case @"zh-CHS":
                            return @"Microsoft Yahei";
                            break;
                        case @"zh-CHT":
                            return @"Microsoft JhengHei";
                            break;
                        default:
                            return @"Segoe UI";
                            break;
                        case @"ja":
                            return @"Meiryo";
                            break;
                        case @"ko":
                            return @"Malgun Gothic";
                            break;
                    }
                    break;
                case Language.Cyrillic:
                    return @"Tahoma";
                    break;
                case Language.Japanese:
                    return @"Meiryo";
                    break;
                case Language.Korean:
                    return @"Malgun Gothic";
                    break;
            }

            return null;
        }

        internal static void ClearFontCache()
        {
            foreach (Font f in fontCache.Values)
                f.Dispose();
            fontCache.Clear();
        }

        static PrivateFontCollection fontCollection = new PrivateFontCollection();

        static Dictionary<string, Font> fontCache = new Dictionary<string, Font>();
        private static Font GetFont(string fontFace, float size, FontStyle fs)
        {
            if (fontFace.StartsWith(@"skin-"))
            {
                fontFace = fontFace.Replace(@"skin-", string.Empty);

                //todo: expand this should we need it. requires a font cache in SkinManager
                Font skinnedFont = SkinManager.GetTaikoMetadataFont(size, FontStyle.Regular);
                if (skinnedFont != null) return skinnedFont;
            }

            Font f;
            string identifier = fontFace + size + (int)fs;

            if (fontCache.TryGetValue(identifier, out f))
                return f;

            FontFamily family = LoadFont(fontFace, true);
            if (family != null)
                f = new Font(family, size, fs);
            else
                f = new Font(fontFace, size, fs);

            return (fontCache[identifier] = f);
        }

        internal static void Initialize()
        {
            //load some known fonts
            LoadFont(@"FontAwesome");
            LoadFont(@"Aller");
            LoadFont(@"Aller_Lt");
            LoadFont(@"Aller_Bd");
        }

        private static FontFamily LoadFont(string fontFace, bool checkOnly = false)
        {
            if (fontFace.StartsWith(@"res:"))
            {
                fontFace = fontFace.Replace(@"res:", string.Empty);
                checkOnly = false;
            }

            foreach (FontFamily search in fontCollection.Families)
            {
                if (search.Name == fontFace)
                {
                    return search;
                    break;
                }
            }

            if (checkOnly) return null;

            FontFamily family = null;
            if (family == null)
            {
                try
                {
                    byte[] bytes = (byte[])osu_ui.ResourcesStore.ResourceManager.GetObject(fontFace);

                    if (bytes != null && bytes.Length > 0)
                    {
                        GCHandle pinnedArray = GCHandle.Alloc(bytes, GCHandleType.Pinned);
                        IntPtr pointer = pinnedArray.AddrOfPinnedObject();
                        fontCollection.AddMemoryFont(pointer, (int)bytes.Length);
                        pinnedArray.Free();
                    }
                }
                catch { }
            }

            return family;
        }

        private static float dpiRatio = 0;
        internal static float ScaleModifier = 1;
    }

    internal enum TextAlignment
    {
        Left,
        LeftFixed,
        Centre,
        Right
    }

    internal enum ShadowType
    {
        None,
        Normal,
        Border
    }
}