﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Graphics.Primitives;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.OpenGl;

namespace osu.Graphics.Renderers
{
    internal class LegacySliderRendererGL : LineManager, ISliderRenderer
    {
        private List<Color> m_colours;
        private Color m_tag;
        private Color m_grey;
        private Color m_border;
        private float m_default_radius;
        private float border_ratio;

        public List<Color> Colours
        {
            get
            {
                return m_colours;
            }
            set
            {
                m_colours = value;
            }
        }

        public Color Tag
        {
            get
            {
                return m_tag;
            }
            set
            {
                m_tag = value;
            }
        }

        public Color Grey
        {
            get
            {
                return m_grey;
            }
            set
            {
                m_grey = value;
            }
        }

        public Color Border
        {
            get
            {
                return m_border;
            }
            set
            {
                m_border = value;
            }
        }

        public float DefaultRadius
        {
            get
            {
                return m_default_radius;
            }
            set
            {
                m_default_radius = value;
            }
        }

        public void AssignColours(List<Color> colours, Color tag, Color grey, Color border, float default_radius)
        {
            Colours = colours;
            Tag = tag;
            Grey = grey;
            Border = border;
            DefaultRadius = default_radius;
        }

        public void Draw(List<Line> lines, Line prev, float radius, Color colour, Color border_colour)
        {
            GlControl.SetBlend(Gl.GL_ONE, Gl.GL_ONE_MINUS_SRC_ALPHA);

            Draw(lines, radius * 58 / 64, border_colour, 0, "StandardBorder", true);

            GlControl.SetBlend(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA);

            Draw(lines, radius * 51 / 64, new Color(colour.R, colour.G, colour.B, 255), 0, "StandardNoRect", true);
        }

        public override void Draw(List<Line> lines, Line prev, float radius, Color colour)
        {
            Draw(lines, prev, radius, colour, m_border);
        }

        public void Draw(List<Line> lines, Line prev, float radius, int colour_index)
        {
            Color colour;
            if (colour_index == -1) colour = m_grey;
            else if (colour_index == -2) colour = m_tag;
            else colour = m_colours[colour_index];
            Draw(lines, prev, radius, colour, m_border);
        }

        public void Draw(List<Line> lines, Line prev, float radius, int colour_index, Color border_colour)
        {
            Draw(lines, prev, radius, m_colours[colour_index], border_colour);
        }
    }
}
