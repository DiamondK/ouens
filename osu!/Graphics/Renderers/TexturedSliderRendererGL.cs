﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Graphics.OpenGl;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Primitives;
using osu.Helpers;

namespace osu.Graphics.Renderers
{
    internal abstract class TexturedSliderRendererGL : ISliderRenderer
    {
        private const int MAXRES = 24; // A higher MAXRES produces rounder endcaps at the cost of more vertices

        // Make the quad overhang just slightly to avoid 1px holes between a quad and a wedge from rounding errors.
        private const float QUAD_OVERLAP_FUDGE = 3.0e-4f;

        // If the peak vertex of a quad is at exactly 0, we get a crack running down the center of horizontal linear sliders.
        // We shift the vertex slightly off to the side to avoid this.
        private const float QUAD_MIDDLECRACK_FUDGE = 1.0e-4f;

        // Bias to the number of polygons to render in a given wedge. Also ... fixes ... holes.
        private const float WEDGE_COUNT_FUDGE = 0.0f; // Seems this fudge is unneeded YIPEE

        private int numIndices_quad;
        private int numIndices_cap;
        private int numPrimitives_quad;
        private int numPrimitives_cap;
        private int numVertices_quad;
        private int numVertices_cap;

        protected TextureGl[] textures;
        protected TextureGl texture_grey;
        protected TextureGl texture_tag;

        private Vector3[] vertices;

        private bool am_initted_geom = false;
        private bool am_initted_tex = false;

        private List<Color> m_colours = new List<Color>();
        private Color m_tag = Color.Gray;
        private Color m_grey = Color.Gray;
        private Color m_border = Color.White;
        private float m_default_radius = 64.0f;

        internal void Initialize()
        {
            if (!am_initted_geom) // TODO: Vertex buffers
            {
                numVertices_quad = 6;
                numPrimitives_quad = 4;
                numIndices_quad = 6;

                numVertices_cap = MAXRES + 2;
                numPrimitives_cap = MAXRES;
                numIndices_cap = 3 * MAXRES;

                CalculateCapMesh();

                am_initted_geom = true;
            }

            if (!am_initted_tex)
            {
                ComputeAllColours();
                am_initted_tex = true;
            }
        }

        public List<Color> Colours
        {
            get
            {
                return m_colours;
            }
            set
            {
                if (value.Count != m_colours.Count)
                {
                    m_colours = new List<Color>(value);
                    if (am_initted_tex) ComputeSliderColours();
                    return;
                }
                for (int x = 0; x < value.Count; x++)
                {
                    if (value[x] != m_colours[x])
                    {
                        m_colours = new List<Color>(value);
                        if (am_initted_tex) ComputeSliderColours();
                        return;
                    }
                }
            }
        }

        public Color Tag
        {
            get
            {
                return m_tag;
            }
            set
            {
                if (m_tag == value) return;
                m_tag = value;
                if (am_initted_tex) ComputeTagColour();
            }
        }

        public Color Grey
        {
            get
            {
                return m_grey;
            }
            set
            {
                if (m_grey == value) return;
                m_grey = value;
                if (am_initted_tex) ComputeGreyColour();
            }
        }

        public Color Border
        {
            get
            {
                return m_border;
            }
            set
            {
                if (m_border == value) return;
                m_border = value;
                if (am_initted_tex) ComputeAllColours();
            }
        }

        public float DefaultRadius
        {
            get
            {
                return m_default_radius;
            }
            set
            {
                if (m_default_radius == value) return;
                m_default_radius = value;
                if (am_initted_tex) ComputeAllColours();
            }
        }

        public void AssignColours(List<Color> colours, Color tag, Color grey, Color border, float default_radius)
        {
            if (m_border != border || m_default_radius != default_radius)
            {
                m_colours = new List<Color>(colours);
                m_tag = tag;
                m_grey = grey;
                m_border = border;
                m_default_radius = default_radius;
                if (am_initted_tex) ComputeAllColours();
                return;
            }
            Colours = colours;
            Tag = tag;
            Grey = grey;
        }

        public bool ProgressiveRendering
        {
            get
            {
                return true;
            }
        }

        private void ComputeSliderColours()
        {
            int colour_count = m_colours.Count;

            if (colour_count == 0) // Temporary catch for i332-triggered hard crash
            {
                textures = new TextureGl[1];
                textures[0] = CreateTexture(Color.TransparentBlack, m_border);
            }
            else
            {
                if (textures != null)
                {
                    foreach (TextureGl tex in textures)
                    {
                        if (tex != null) tex.Dispose();
                    }
                }

                textures = new TextureGl[colour_count];

                for (int x = 0; x < colour_count; x++)
                {
                    textures[x] = CreateTexture(m_colours[x], m_border);
                }
            }
        }

        private void ComputeTagColour()
        {
            if (texture_tag != null) texture_tag.Dispose();
            texture_tag = CreateTexture(m_tag, m_border);
        }

        private void ComputeGreyColour()
        {
            if (texture_grey != null) texture_grey.Dispose();
            texture_grey = CreateTexture(m_grey, m_border);
        }

        private void ComputeAllColours()
        {
            ComputeSliderColours();
            ComputeTagColour();
            ComputeGreyColour();
        }

        protected abstract TextureGl CreateTexture(Color colour, Color border);
        protected abstract System.Drawing.Size TextureSize
        {
            get;
        }

        /// <summary>
        /// The cap mesh is a half cone.
        /// </summary>
        private void CalculateCapMesh()
        {
            vertices = new Vector3[numVertices_cap - 1];

            float maxRes = (float)MAXRES;
            float step = OsuMathHelper.Pi / maxRes;

            vertices[0] = new Vector3(0.0f, -1.0f, 0.0f);

            for (int z = 1; z < MAXRES; z++)
            {
                float angle = (float)z * step;
                vertices[z] = new Vector3((float)(Math.Sin(angle)), -(float)(Math.Cos(angle)), 0.0f);
            }

            vertices[MAXRES] = new Vector3(0.0f, 1.0f, 0.0f);
        }

        public void Draw(Line line, float radius, Color colour)
        {
            List<Line> lines = new List<Line>(1);
            lines.Add(line);

            Draw(lines, null, radius, colour);
        }

        public void Draw(List<Line> lines, Line prev, float radius, Color colour)
        {
            Draw(lines, prev, radius, colour, m_border);
        }

        public void Draw(List<Line> lines, Line prev, float radius, Color colour, Color border_colour)
        {
            TextureGl tex = CreateTexture(colour, border_colour);
            DrawOGL(lines, radius, tex, prev);
            tex.Dispose();
        }

        public void Draw(List<Line> lines, Line prev, float radius, int colour_index)
        {
            switch (colour_index)
            {
                case -1: // Grey
                    DrawOGL(lines, radius, texture_grey, prev);
                    break;
                case -2: // Multi custom
                    DrawOGL(lines, radius, texture_tag, prev);
                    break;
                default:
                    if ((colour_index >= textures.Length) || (colour_index < 0))
                    {
#if DEBUG
                        throw new ArgumentOutOfRangeException("Colour index outside the range of the collection.");
#else
                        DrawOGL(lines, radius, texture_grey, prev);
#endif
                    }
                    else DrawOGL(lines, radius, textures[colour_index], prev);
                    break;
            }
        }

        public void Draw(List<Line> lines, Line prev, float radius, int colour_index, Color border_colour)
        {
            Draw(lines, prev, radius, m_colours[colour_index], border_colour);
        }

        private void glDrawQuad()
        {
            // Todo: vertex buffers

            Gl.glBegin(Gl.GL_TRIANGLE_STRIP);

            Gl.glTexCoord2f(0, 0);
            Gl.glVertex3f(-QUAD_OVERLAP_FUDGE, -1.0f, 0.0f);
            Gl.glVertex3f(1.0f + QUAD_OVERLAP_FUDGE, -1.0f, 0.0f);

            Gl.glTexCoord2f(GlControl.SurfaceType == Gl.GL_TEXTURE_2D ? 1.0f - 1.0f / (float)TextureSize.Width : TextureSize.Width, 0);
            Gl.glVertex3f(-QUAD_OVERLAP_FUDGE, QUAD_MIDDLECRACK_FUDGE, 1.0f);
            Gl.glVertex3f(1.0f + QUAD_OVERLAP_FUDGE, QUAD_MIDDLECRACK_FUDGE, 1.0f);

            Gl.glTexCoord2f(0, 0);
            Gl.glVertex3f(-QUAD_OVERLAP_FUDGE, 1.0f, 0.0f);
            Gl.glVertex3f(1.0f + QUAD_OVERLAP_FUDGE, 1.0f, 0.0f);

            Gl.glEnd();
        }

        private void glDrawHalfCircle(int count)
        {
            if (count > 0)
            {
                // Todo: vertex buffers

                Gl.glBegin(Gl.GL_TRIANGLE_FAN);

                Gl.glTexCoord2f(GlControl.SurfaceType == Gl.GL_TEXTURE_2D ? 1.0f - 1.0f / (float)TextureSize.Width : TextureSize.Width, 0);
                Gl.glVertex3f(0.0f, 0.0f, 1.0f);

                Gl.glTexCoord2f(0, 0);
                for (int x = 0; x <= count; x++)
                {
                    Vector3 v = vertices[x];
                    Gl.glVertex3f(v.X, v.Y, v.Z);
                }

                Gl.glEnd();
            }
        }

        /// <summary>
        /// Core drawing method in OpenGL
        /// </summary>
        /// <param name="lineList">List of lines to use</param>
        /// <param name="globalRadius">Width of the slider</param>
        /// <param name="texture">Texture used for the track</param>
        /// <param name="prev">The last line which was rendered in the previous iteration, or null if this is the first iteration.</param>
        private void DrawOGL(List<Line> lineList, float globalRadius, TextureGl texture, Line prev)
        {
            Gl.glPushAttrib(Gl.GL_ENABLE_BIT);
            GlControl.Texturing = false;

            Gl.glDisable(Gl.GL_CULL_FACE);
            Gl.glDisable(Gl.GL_BLEND);
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glDepthMask((byte)(Gl.GL_TRUE));
            Gl.glDepthFunc(Gl.GL_LEQUAL);

            GlControl.Texturing = true;
            Gl.glColor3ub(255, 255, 255);

            // Select The Modelview Matrix
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            // Reset The Modelview Matrix
            Gl.glLoadIdentity();

            GlControl.Bind(texture.TextureId);
            Gl.glTexParameteri(GlControl.SurfaceType, Gl.GL_TEXTURE_MIN_FILTER, (int)Gl.GL_LINEAR);
            Gl.glTexParameteri(GlControl.SurfaceType, Gl.GL_TEXTURE_MAG_FILTER, (int)Gl.GL_LINEAR);

            int count = lineList.Count;

            for (int x = 1; x < count; x++)
            {
                DrawLineOGL(prev, lineList[x - 1], lineList[x], globalRadius);

                prev = lineList[x - 1];
            }

            DrawLineOGL(prev, lineList[count - 1], null, globalRadius);

            Gl.glLoadIdentity();

            Gl.glPopAttrib();
        }

        private void DrawLineOGL(Line prev, Line curr, Line next, float globalRadius)
        {
            // Quad
            Gl.glLoadMatrixf(OsuMathHelper.MatrixToFloats(new Matrix(curr.rho, 0, 0, 0, // Scale-X
                                                                     0, globalRadius, 0, 0, // Scale-Y
                                                                     0, 0, 1, 0,
                                                                     0, 0, 0, 1) * curr.WorldMatrix()));

            glDrawQuad();

            int end_triangles;
            bool flip;
            if (next == null)
            {
                flip = false; // totally irrelevant
                end_triangles = numPrimitives_cap;
            }
            else
            {
                float theta = next.theta - curr.theta;

                // keep on the +- pi/2 range.
                if (theta > Math.PI) theta -= (float)(Math.PI * 2);
                if (theta < -Math.PI) theta += (float)(Math.PI * 2);

                if (theta < 0)
                {
                    flip = true;
                    end_triangles = (int)Math.Ceiling((-theta) * MAXRES / Math.PI + WEDGE_COUNT_FUDGE);
                }
                else if (theta > 0)
                {
                    flip = false;
                    end_triangles = (int)Math.Ceiling(theta * MAXRES / Math.PI + WEDGE_COUNT_FUDGE);
                }
                else
                {
                    flip = false; // totally irrelevant
                    end_triangles = 0;
                }
            }
            end_triangles = Math.Min(end_triangles, numPrimitives_cap);

            // Cap on end
            if (flip) Gl.glLoadMatrixf(OsuMathHelper.MatrixToFloats(new Matrix(globalRadius, 0, 0, 0,
                                                                               0, -globalRadius, 0, 0,
                                                                               0, 0, 1, 0,
                                                                               0, 0, 0, 1) * curr.EndWorldMatrix()));
            else Gl.glLoadMatrixf(OsuMathHelper.MatrixToFloats(new Matrix(globalRadius, 0, 0, 0,
                                                                          0, globalRadius, 0, 0,
                                                                          0, 0, 1, 0,
                                                                          0, 0, 0, 1) * curr.EndWorldMatrix()));

            glDrawHalfCircle(end_triangles);

            // Cap on start
            bool hasStartCap = false;

            if (prev == null) hasStartCap = true;
            else if (curr.p1 != prev.p2) hasStartCap = true;

            if (hasStartCap)
            {
                // Catch for Darrinub and other slider inconsistencies. (Redpoints seem to be causing some.)
                // Render a complete beginning cap if this Line isn't connected to the end of the previous line.

                Gl.glLoadMatrixf(OsuMathHelper.MatrixToFloats(new Matrix(-globalRadius, 0, 0, 0,
                                                                         0, -globalRadius, 0, 0,
                                                                         0, 0, 1, 0,
                                                                         0, 0, 0, 1) * curr.WorldMatrix()));
                glDrawHalfCircle(numPrimitives_cap);
            }
        }

        ~TexturedSliderRendererGL()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool am_disposing)
        {
            if (am_disposing)
            {
                if (textures == null) return;
                foreach (TextureGl tex in textures)
                {
                    TryDispose(tex);
                }
                TryDispose(texture_grey);
                TryDispose(texture_tag);
            }
        }

        private void TryDispose(IDisposable d)
        {
            if (d != null) d.Dispose();
        }

    }
}
