using System;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.OpenGl;
using osudata.Libraries.FFmpeg;

namespace osu.Graphics.Renderers
{
    public class VideoTexture : IDisposable
    {
        private const int FRAME_BUFFER = 1;
        private readonly int height;
        internal readonly int length;
        private readonly VideoDecoder vd;
        private readonly pTexture[] videoTextures;
        private readonly int width;
        private int currentTexture = -1;
        private int displayTexture;
        private int framesArrived;
        private int lastSeek;
        internal volatile PlaybackState playbackState = PlaybackState.Stopped;
        internal int startTime;

        #region Constructors

        internal VideoTexture(string videoFile)
        {
            byte[] video = BeatmapManager.Current.GetFileBytes(videoFile);

            vd = new VideoDecoder((int) (AudioEngine.CurrentPlaybackRate/100f*4));
            vd.Open(video);

            width = vd.width;
            height = vd.height;

            videoTextures = new pTexture[FRAME_BUFFER];

            Initialize();

            length = (int) vd.Length*1000;

            GameBase.OnReload += GameBase_OnReload;
            GameBase.OnUnload += GameBase_OnUnload;
        }

        private void GameBase_OnUnload(bool b)
        {
            if (!b) return;
            Unload();
        }

        private void GameBase_OnReload(bool b)
        {
            if (!b) return;
            Initialize();
        }


        private void Initialize()
        {
            byte[] black = new byte[width*height*4];
            for (int j = 0; j < black.Length - 1; j++)
                black[j] = (byte) (j%4 == 3 ? 255 : 0);

            for (int i = 0; i < FRAME_BUFFER; i++)
            {
                Texture2D textureXna = GameBase.D3D
                                           ? new Texture2D(GameBase.graphics.GraphicsDevice, width, height, 1,
                                                           ResourceUsage.None, SurfaceFormat.Bgr32,
                                                           ResourceManagementMode.Automatic)
                                           : null;
                TextureGl textureGl = GameBase.OGL ? new TextureGl(width, height) : null;
                videoTextures[i] = new pTexture(textureXna, textureGl, width, height);
                videoTextures[i].SetData(black);
            }
        }

        private void Unload()
        {
            for (int i = 0; i < FRAME_BUFFER; i++)
                if (videoTextures[i] != null)
                    videoTextures[i].Dispose();
        }

        #endregion

        internal double LastSample
        {
            get { return vd.CurrentTime; }
        }

        /// <summary>
        /// The video texture. This texture is continously updated with current data from the video source.
        /// </summary>
        internal pTexture Texture
        {
            get { return videoTextures[displayTexture]; }
        }

        /// <summary>
        /// The width of the source video stream (pixels).
        /// </summary>
        internal int Width
        {
            get { return width; }
        }

        /// <summary>
        /// The height of the source video stream (pixels).
        /// </summary>
        internal int Height
        {
            get { return height; }
        }

        ~VideoTexture()
        {
            Dispose(false);
        }

        #region IDisposable Members

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        /// <summary>
        /// Shut down the VideoTexture.
        /// </summary>
        public void Dispose(bool isDisposing)
        {
            if (vd != null) vd.Dispose();
            if (videoTextures != null)
                foreach (pTexture t in videoTextures)
                    if (t != null) t.Dispose();
            GameBase.OnReload -= GameBase_OnReload;
            GameBase.OnUnload -= GameBase_OnUnload;
        }

        #endregion

        internal void NextFrame()
        {
            byte[] data = vd.GetFrame(AudioEngine.TimeUnedited - startTime);
            if (data == null) return;

            framesArrived++;

            currentTexture = 0;
            displayTexture = 0;

            videoTextures[currentTexture].SetData(data);
        }


        public void Seek(int time, bool force)
        {
            //only allow seeking to the same time once.
            if (time - lastSeek < 2000 && !force) return;

            lastSeek = time;

            vd.Seek(time);
        }

        #region Nested type: PlaybackState

        internal enum PlaybackState
        {
            Stopped,
            Paused,
            Running,
            Exiting
        }

        #endregion
    }
}