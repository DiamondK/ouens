﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.Graphics.Notifications;

namespace osu.Graphics.Renderers
{
    internal static class BloomRenderer
    {
        internal static bool Additive;
        internal static float Alpha = 0.2f;
        private static Effect bloomatic;

        private static RenderTarget2D bloomTarg;
        internal static Color Colour = Color.White;

        internal static bool ExtraPass;
        internal static bool HiRange;
        internal static float Magnitude = 0.004f;
        internal static float RedTint;
        private static RenderTarget2D rTarg;

        internal static void Unload()
        {
            if (!ConfigManager.sBloom) return;

            if (rTarg != null) rTarg.Dispose();
            rTarg = null;
            if (bloomTarg != null) bloomTarg.Dispose();
            bloomTarg = null;
        }

        internal static bool Initialize()
        {
            if (!ConfigManager.sBloom) return true;

            if (GameBase.OGL)
            {
                ConfigManager.sBloom.Value = false;
                NotificationManager.ShowMessage("Shaders are not yet implemented for OpenGL, sorry!");
                return false;
            }

            try
            {
                if (rTarg != null) rTarg.Dispose();

                rTarg = new RenderTarget2D(GameBase.graphics.GraphicsDevice, GameBase.WindowWidth,
                                           GameBase.WindowHeight + GameBase.WindowOffsetY,
                                           1, SurfaceFormat.Color);

                if (bloomTarg != null) bloomTarg.Dispose();
                bloomTarg = new RenderTarget2D(GameBase.graphics.GraphicsDevice, 256, 256,
                                               1, SurfaceFormat.Color);
                bloomatic = GameBase.content.Load<Effect>("Bloom");
            }
            catch
            {
                ConfigManager.sBloom.Value = false;
                NotificationManager.ShowMessage("No shader support - disabling bloom effects.");
                return false;
            }
            return true;
        }

        internal static void BloomStart()
        {
            if (!(ConfigManager.sBloomSoftening || ExtraPass)) return;
            
            GameBase.graphics.GraphicsDevice.SetRenderTarget(0, rTarg);
            GameBase.graphics.GraphicsDevice.Clear(Color.Black);
        }

        internal static void BloomEnd()
        {
            if (!(ConfigManager.sBloomSoftening || ExtraPass)) return;
            
#if !DEBUG
            try
            {
#endif
                GameBase.graphics.GraphicsDevice.ResolveRenderTarget(0);
                GameBase.graphics.GraphicsDevice.SetRenderTarget(0, null);
                GameBase.graphics.GraphicsDevice.Clear(Color.Black);

                Texture2D tex = rTarg.GetTexture();

                GameBase.spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate,
                                           SaveStateMode.None);
                
                GameBase.spriteBatch.Draw(tex,
                                          new Rectangle(0, 0, GameBase.WindowWidth,
                                                        GameBase.WindowHeight + GameBase.WindowOffsetY),
                                          new Rectangle(0, 0, GameBase.WindowWidth,
                                                        GameBase.WindowHeight + GameBase.WindowOffsetY), Color.White);
                GameBase.spriteBatch.End();

                EffectPass pass = bloomatic.CurrentTechnique.Passes[0];

                bloomatic.Begin();

                if (ConfigManager.sBloomSoftening)
                {
                    bloomatic.Parameters["mag"].SetValue(0.004f);
                    bloomatic.Parameters["alpha"].SetValue(0.15f);
                    bloomatic.Parameters["hirange"].SetValue(false);
                    bloomatic.Parameters["redtint"].SetValue(0);

                    GameBase.spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate,
                                               SaveStateMode.None);
                    
                    pass.Begin();
                    GameBase.spriteBatch.Draw(tex,
                                              new Rectangle(0, 0, GameBase.WindowWidth,
                                                            GameBase.WindowHeight + GameBase.WindowOffsetY),
                                              new Rectangle(0, 0, GameBase.WindowWidth,
                                                            GameBase.WindowHeight + GameBase.WindowOffsetY), Color.White);
                    pass.End();
                    GameBase.spriteBatch.End();
                }

                if (ExtraPass)
                {
                    bloomatic.Parameters["mag"].SetValue(Magnitude);
                    bloomatic.Parameters["alpha"].SetValue(Alpha);
                    bloomatic.Parameters["redtint"].SetValue(RedTint);
                    bloomatic.Parameters["hirange"].SetValue(HiRange);

                    GameBase.spriteBatch.Begin(Additive ? SpriteBlendMode.Additive : SpriteBlendMode.AlphaBlend,
                                               SpriteSortMode.Immediate,
                                               SaveStateMode.None);

                    pass.Begin();
                    GameBase.spriteBatch.Draw(tex,
                                              new Rectangle(0, 0, GameBase.WindowWidth,
                                                            GameBase.WindowHeight + GameBase.WindowOffsetY),
                                              new Rectangle(0, 0, GameBase.WindowWidth,
                                                            GameBase.WindowHeight + GameBase.WindowOffsetY), Colour);
                    pass.End();
                    GameBase.spriteBatch.End();
                }

                bloomatic.End();
#if !DEBUG
            }
            catch
            {
                ConfigManager.sBloom.Value = false;
                NotificationManager.ShowMessage("No shader support - disabling bloom effects.");
            }
#endif
        }
    }
}