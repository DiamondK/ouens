﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Graphics.OpenGl;
using Microsoft.Xna.Framework.Graphics;
using osu.Helpers;
using Microsoft.Xna.Framework;
using osu.GameplayElements;

namespace osu.Graphics.Renderers
{
    class ToonSliderRendererGL : TexturedSliderRendererGL
    {
        private const int TEX_WIDTH = 128; // Please keep power of two

        protected override TextureGl CreateTexture(Color colour, Color border)
        {
            Color inner, outer;
            ComputeSliderColour(colour, out inner, out outer);
            return RenderSliderTexture(border, inner, outer);
        }

        /// <summary>
        /// Helper function to turn a single colour into a lighter and darker shade for use with the slider's gradient.
        /// </summary>
        /// <param name="colour">HitObject colour</param>
        /// <param name="InnerColour">Track center</param>
        /// <param name="OuterColour">Track edges</param>
        internal static void ComputeSliderColour(Color colour, out Color InnerColour, out Color OuterColour)
        {
            Color col = new Color(colour.R, colour.G, colour.B, (byte)180);
            InnerColour = ColourHelper.Lighten2(col, 0.5f);
            OuterColour = ColourHelper.Darken(col, 0.1f);
        }

        /// <summary>
        /// Render a gradient into a 256x1 texture.
        /// </summary>
        private TextureGl RenderSliderTexture(Color shadow, Color border, Color InnerColour, Color OuterColour, float aa_width)
        {
            Gl.glPushAttrib(Gl.GL_ENABLE_BIT);

            GlControl.Texturing = false;

            Gl.glViewport(0, 0, TEX_WIDTH, 1);
            Gl.glDisable(Gl.GL_DEPTH_TEST);
            Gl.glDisable(Gl.GL_BLEND);

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Gl.glOrtho(0.0d, 1.0d, 1.0d, -1.0d, -1.0d, 1.0d);

            Gl.glClearColor(0.859f, 0.439f, 0.576f, 1.0f);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT);
            Gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

            Color col1 = ColourHelper.ColourLerp(OuterColour, InnerColour, 1.0f / 3.0f);
            Color col2 = ColourHelper.ColourLerp(OuterColour, InnerColour, 2.0f / 3.0f);

            float stop0 = 0.1875f;
            float stop4 = 1.0f;
            float stop1 = OsuMathHelper.Lerp(stop0, stop4, 0.25f);
            float stop2 = OsuMathHelper.Lerp(stop0, stop4, 0.5f);
            float stop3 = OsuMathHelper.Lerp(stop0, stop4, 0.75f);

            Gl.glBegin(Gl.GL_LINE_STRIP);

            GlControl.SetColour(Color.TransparentBlack);
            Gl.glVertex2f(-0.5f, 0.0f);
            Gl.glVertex2f(0.0f, 0.0f);

            GlControl.SetColour(shadow);
            Gl.glVertex2f(0.078125f - aa_width, 0.0f);

            GlControl.SetColour(border);
            Gl.glVertex2f(0.078125f + aa_width, 0.0f);
            Gl.glVertex2f(stop0 - aa_width, 0.0f);

            GlControl.SetColour(OuterColour);
            Gl.glVertex2f(stop0 + aa_width, 0.0f);
            Gl.glVertex2f(stop1 - aa_width, 0.0f);

            GlControl.SetColour(col1);
            Gl.glVertex2f(stop1 + aa_width, 0.0f);
            Gl.glVertex2f(stop2 - aa_width, 0.0f);

            GlControl.SetColour(col2);
            Gl.glVertex2f(stop2 + aa_width, 0.0f);
            Gl.glVertex2f(stop3 - aa_width, 0.0f);

            GlControl.SetColour(InnerColour);
            Gl.glVertex2f(stop3 + aa_width, 0.0f);
            Gl.glVertex2f(1.5f, 0.0f);

            Gl.glEnd();

            TextureGl result = new TextureGl(TEX_WIDTH, 1);
            int[] textures = new int[1];

            Gl.glGenTextures(1, textures);
            GlControl.Texturing = true;

            GlControl.Bind(textures[0]);
            Gl.glTexParameteri(GlControl.SurfaceType, Gl.GL_TEXTURE_MIN_FILTER, (int)Gl.GL_LINEAR);
            Gl.glTexParameteri(GlControl.SurfaceType, Gl.GL_TEXTURE_MAG_FILTER, (int)Gl.GL_LINEAR);

            Gl.glCopyTexImage2D(GlControl.SurfaceType, 0, Gl.GL_RGBA, 0, 0, TEX_WIDTH, 1, 0);

            result.SetData(textures[0]);

            Gl.glPopAttrib();

            GlControl.ResetViewport(false);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT);

            return result;
        }

        /// <summary>
        /// This overload computes the outer/inner colours and AA widths, and has a hardcoded shadow colour.
        /// </summary>
        private TextureGl RenderSliderTexture(Color border, Color InnerColour, Color OuterColour)
        {
            // This formula is used to keep sliders smooth for a wide variety of screen resolutions and circle sizes.
            // In most cases, it's set to equal a nice ratio of screen pixels, but it's constrained to within two values.
            // When circles are very large, the hitcircle(overlay).png textures become a bit fuzzy, so sharp sliders would look unnatural.
            // When they are tiny, we get the opposite problem. I confine it at 1/16th of a circle-radius, which is almost as wide as its border.
            float aa_width = Math.Min(Math.Max(0.5f / DefaultRadius, 3.0f / 256.0f), 1.0f / 16.0f);

            Color shadow = new Color(0, 0, 0, 64);

            return RenderSliderTexture(shadow, border, InnerColour, OuterColour, aa_width);
        }

        protected override System.Drawing.Size TextureSize
        {
            get
            {
                return new System.Drawing.Size(TEX_WIDTH, 1);
            }
        }
    }
}
