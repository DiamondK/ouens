﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Sprites;
using osu.Graphics.Notifications;

namespace osu.Graphics
{
    internal class OnScreenDisplay : pDrawableComponent
    {
        static Vector2 padding = new Vector2(0, 0);
        const float width = 700;

        static Color bg_black = new Color(0, 0, 0, 180);
        static Color bg_new = new Color(50, 176, 255, 180);

        private pTraceListener listener;

        internal OnScreenDisplay()
            : base(true)
        {
            Visible = false;
            listener = new pTraceListener();
            Debug.Listeners.Add(listener);
        }

        internal bool Visible { get; set; }

        internal void ToggleShow()
        {
            Visible = !Visible;
            NotificationManager.ShowMessage("Debug messages are now " + (Visible ? "visible" : "hidden") + ". Press F11 to toggle.");
        }

        internal override void Update()
        {
            base.Update();

            if (spriteManager != null)
                spriteManager.Alpha = Visible ? 1 : 0;

            foreach (string s in listener.GetPendingMessages())
            {
                if (s == null || s.Length == 0) continue;

                pText text = new pText(s, 9, padding, 1, false, Color.White)
                {
                    Field = Fields.BottomLeft,
                    TextBounds = new Vector2(width, 0),
                    Origin = Origins.BottomLeft,
                    TextRenderSpecific = false,
                    BackgroundColour = bg_new,
                    TextAa = false,
                    TextShadow = true
                };

                spriteManager.SpriteList.ForEach(spr =>
                {
                    spr.Position.Y += padding.Y + text.MeasureText().Y / 0.625f / GameBase.WindowRatio;
                });

                //Remove any sprites above the screen
                if (spriteManager.SpriteList.Count > 0 && spriteManager.SpriteList[0].Position.Y > 480)
                    spriteManager.Remove(spriteManager.SpriteList[0]);

                spriteManager.Add(text);

                text.FadeOut(15000, EasingTypes.InExpo);

                GameBase.Scheduler.AddDelayed(delegate { text.BackgroundColour = bg_black; }, 320);
            }
        }
    }

    internal class pTraceListener : TraceListener
    {
        List<string> pendingLines = new List<string>();

        public override void Write(string message)
        {
            lock (pendingLines)
                pendingLines.Add(message);
        }

        public override void WriteLine(string message)
        {
            lock (pendingLines)
                pendingLines.Add(message);
        }

        internal IEnumerable<string> GetPendingMessages()
        {
            lock (pendingLines)
            {
                List<string> pending = new List<string>(pendingLines);
                pendingLines.Clear();
                return pending;
            }
        }
    }
}
