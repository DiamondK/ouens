﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using osu_common.Helpers;

namespace osu.Graphics
{
    class ASyncLoader : pDrawableComponent
    {
        public bool Loaded;

        VoidDelegate loadFunction;
        BoolReturnDelegate completeFunction;

        Thread runningThread;
        public Exception LastException;

        public ASyncLoader(VoidDelegate load, BoolReturnDelegate complete = null)
        {
            GameBase.BackgroundLoading = true;

            this.loadFunction = load;
            this.completeFunction = complete;

            runningThread = GameBase.RunGraphicsThread(run);
        }

        private void run()
        {
            if (wasDisposed) return;

            bool success = true;

#if !DEBUG
            try
            {
#endif
                loadFunction();
#if !DEBUG
            }
            catch (Exception e)
            {
                success = false;
                LastException = e;
            }
#endif

            if (wasDisposed) return;

            complete(success, true);
        }

        internal void Complete()
        {
            //if (runningThread != null && runningThread.IsAlive)
            //    throw new Exception("still loading");

            complete();
        }

        private void complete(bool success = true, bool runSuccessFunction = false)
        {
            if (Loaded && !success) return;

            GameBase.Scheduler.Add(delegate
            {
                if (wasDisposed) return;

                Loaded = completeFunction != null && runSuccessFunction ? completeFunction(success) : true;
                if (Loaded) GameBase.BackgroundLoading = false;
            });
        }

        bool wasDisposed;
        internal override void Dispose(bool isDisposing)
        {
            wasDisposed = true;

            if (runningThread != null && runningThread.IsAlive && runningThread != GameBase.MainThread)
                runningThread.Abort();

            GameBase.BackgroundLoading = false;
            base.Dispose(isDisposing);
        }
    }
}
