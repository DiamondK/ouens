﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Graphics.Sprites;

namespace osu.Graphics
{
    partial class pDrawable : IDisposable, IComparable<pDrawable>
    {
        public pDrawable(bool alwaysDraw = true)
        {
            AlwaysDraw = alwaysDraw;
            if (alwaysDraw) Alpha = 1;
        }

        /// <summary>
        /// Hide sprite instantly.
        /// </summary>
        /// <returns></returns>
        internal void Hide()
        {
            FadeOut(0);
        }

        /// <summary>
        /// Show sprite instantly.
        /// </summary>
        internal void Show()
        {
            FadeIn(0);
        }

        internal void FadeIn(int duration, EasingTypes easing = EasingTypes.None)
        {
            int count = Transformations.Count;

            if (count == 0 && Alpha == 1)
                return;

            if (count == 1)
            {
                Transformation t = Transformations[0];
                if (t.Type == TransformationType.Fade && t.EndFloat == 1 && t.Time2 - t.Time1 == duration)
                    return;
            }

            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => t.Type == TransformationType.Fade);

            if (1 - Alpha < float.Epsilon)
                return;

            if (duration == 0)
            {
                Alpha = 1;
                return;
            }

            Transformation tr =
                new Transformation(TransformationType.Fade, (float)Alpha,
                                   1,
                                   GameBase.GetTime(Clock) - (int)GameBase.ElapsedMilliseconds,
                                   GameBase.GetTime(Clock) + duration, easing);
            Transformations.Add(tr);
        }

        internal Transformation FadeInFromZero(int duration)
        {
            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => t.Type == TransformationType.Fade);

            Transformation tr =
                new Transformation(TransformationType.Fade, 0, 1,
                                   GameBase.GetTime(Clock) - (int)GameBase.ElapsedMilliseconds,
                                   GameBase.GetTime(Clock) + duration);
            Transformations.Add(tr);
            return tr;
        }

        internal void FadeOut(int duration, EasingTypes easing = EasingTypes.None)
        {
            int count = Transformations.Count;

            if (count == 0 && Alpha == 0)
                return;

            if (count == 1)
            {
                Transformation t = Transformations[0];
                if (t.Type == TransformationType.Fade && t.EndFloat == 0 && t.Time2 - t.Time1 == duration)
                    return;
            }

            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => t.Type == TransformationType.Fade);

            if (Alpha < float.Epsilon)
                return;

            if (duration == 0)
            {
                Alpha = 0;
                return;
            }

            Transformation tr =
                new Transformation(TransformationType.Fade, (float)Alpha, 0,
                                   GameBase.GetTime(Clock) - (int)GameBase.ElapsedMilliseconds,
                                   GameBase.GetTime(Clock) + duration, easing);
            Transformations.Add(tr);
        }

        internal Transformation FadeOutFromOne(int duration)
        {
            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => t.Type == TransformationType.Fade);

            Transformation tr =
                new Transformation(TransformationType.Fade, 1, 0,
                                   GameBase.GetTime(Clock) - (int)GameBase.ElapsedMilliseconds,
                                   GameBase.GetTime(Clock) + duration);
            Transformations.Add(tr);
            return tr;
        }

        internal void MoveTo(Vector2 destination, int duration)
        {
            MoveTo(destination, duration, EasingTypes.None);
        }

        internal Transformation MoveTo(Vector2 destination, int duration, EasingTypes easing)
        {
            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => (t.Type & TransformationType.Movement) > 0);

            if (destination == Position)
                return null;

            if (duration == 0)
            {
                Position = destination;
                return null;
            }

            Transformation tr =
                new Transformation(Position, destination,
                                   GameBase.GetTime(Clock) - (int)Math.Max(1, GameBase.ElapsedMilliseconds),
                                   GameBase.GetTime(Clock) + duration);
            tr.Easing = easing;
            Transformations.Add(tr);

            return tr;
        }

        internal void MoveToX(float destination, int duration, EasingTypes easing = EasingTypes.None)
        {
            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => (t.Type & TransformationType.MovementX) > 0);

            if (destination == Position.X)
                return;

            if (duration == 0)
            {
                Position.X = destination;
                return;
            }

            Transformation tr =
                new Transformation(TransformationType.MovementX, Position.X, destination,
                                   GameBase.GetTime(Clock) - (int)Math.Max(1, GameBase.ElapsedMilliseconds),
                                   GameBase.GetTime(Clock) + duration);
            tr.Easing = easing;
            Transformations.Add(tr);
        }

        internal void MoveToY(float destination, int duration, EasingTypes easing = EasingTypes.None)
        {
            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => (t.Type & TransformationType.MovementY) > 0);

            if (destination == Position.Y)
                return;

            if (duration == 0)
            {
                Position.Y = destination;
                return;
            }

            Transformation tr =
                new Transformation(TransformationType.MovementY, Position.Y, destination,
                                   GameBase.GetTime(Clock) - (int)Math.Max(1, GameBase.ElapsedMilliseconds),
                                   GameBase.GetTime(Clock) + duration);
            tr.Easing = easing;
            Transformations.Add(tr);
        }

        internal Transformation MoveToRelative(Vector2 destination, int duration)
        {
            Transformation lastRelative = Transformations.Find(t => t.TagNumeric == 125);
            if (lastRelative != null)
                Position = lastRelative.EndVector;

            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => (t.Type & TransformationType.Movement) > 0);

            Transformation tr =
                new Transformation(Position, Position + destination, GameBase.GetTime(Clock),
                                   GameBase.GetTime(Clock) + duration);

            tr.TagNumeric = 125;

            Transformations.Add(tr);

            return tr;
        }

        internal void MoveToRelative(Vector2 destination, int duration, EasingTypes easing)
        {
            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => (t.Type & TransformationType.Movement) > 0);

            Transformation tr =
                new Transformation(Position, Position + destination, GameBase.GetTime(Clock),
                                   GameBase.GetTime(Clock) + duration);
            tr.Easing = easing;
            Transformations.Add(tr);
        }

        internal void TimeWarp(int change)
        {
            if (change == 0)
                return;

            foreach (Transformation t in Transformations)
            {
                t.Time1 += change;
                t.Time2 += change;
            }
        }

        public void FadeTo(float final, int duration, EasingTypes easing = EasingTypes.None)
        {
            if (Transformations.Count == 1)
            {
                Transformation t = Transformations[0];
                if (t.Type == TransformationType.Fade && t.EndFloat == final && t.Time2 - t.Time1 == duration)
                    return;
            }

            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => t.Type == TransformationType.Fade);

            if (Alpha == final)
                return;

            Transformation tr =
                new Transformation(TransformationType.Fade, (float)Alpha,
                                   (InitialColour.A != 0 ? (float)InitialColour.A / 255 * final : final),
                                   GameBase.GetTime(Clock) - (int)GameBase.ElapsedMilliseconds,
                                   GameBase.GetTime(Clock) + duration);
            tr.Easing = easing;
            Transformations.Add(tr);
        }

        public Transformation ScaleTo(float final, int duration, EasingTypes easing = EasingTypes.None)
        {
            if (Transformations.Count == 1)
            {
                Transformation t = Transformations[0];
                if (t.Type == TransformationType.Scale && t.EndFloat == final && t.Time2 - t.Time1 == duration)
                    return null;
            }

            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => t.Type == TransformationType.Scale);

            if (Scale == final)
                return null;

            Transformation tr =
                new Transformation(TransformationType.Scale, (float)Scale, final,
                                   GameBase.GetTime(Clock) - (int)GameBase.ElapsedMilliseconds,
                                   GameBase.GetTime(Clock) + duration);
            tr.Easing = easing;
            Transformations.Add(tr);
            
            return tr;
        }

        public void RotateTo(float final, int duration, EasingTypes easing = EasingTypes.None)
        {
            if (Transformations.Count == 1)
            {
                Transformation t = Transformations[0];
                if (t.Type == TransformationType.Rotation && t.EndFloat == final && t.Time2 - t.Time1 == duration)
                    return;
            }

            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => t.Type == TransformationType.Rotation);

            if (Rotation == final)
                return;

            Transformation tr =
                new Transformation(TransformationType.Rotation, (float)Rotation, final,
                                   GameBase.GetTime(Clock) - (int)GameBase.ElapsedMilliseconds,
                                   GameBase.GetTime(Clock) + duration);
            tr.Easing = easing;
            Transformations.Add(tr);
        }
    }
}
