using System;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Helpers;

namespace osu.Graphics.OpenGl
{
    public class TextureGl : IDisposable
    {
        private readonly int potHeight;
        private readonly int potWidth;
        private readonly int textureHeight;
        private readonly int textureWidth;
        private int textureId;
        public bool Loaded { get { return textureId > 0; } }

        public TextureGl(int width, int height)
        {
            textureId = -1;
            textureWidth = width;
            textureHeight = height;

            if (GlControl.SurfaceType == Gl.GL_TEXTURE_2D)
            {
                potWidth = GetPotDimension(width);
                potHeight = GetPotDimension(height);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion

        /// <summary>
        /// Removes texture from GL memory.
        /// </summary>
        public void Delete()
        {
            if (textureId == -1)
                return;


            if (GameBase.GlControl.IsDisposed) return;

            int disposableId = textureId;

            GameBase.Scheduler.Add(delegate
            {
                try
                {
                    if (Gl.glIsTexture(disposableId) > 0)
                    {
                        int[] textures = new[] { disposableId };
                        Gl.glDeleteTextures(1, textures);
                    }
                }
                catch
                {
                }
            });

            textureId = -1;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Delete();
            }
        }

        /// <summary>
        /// Blits sprite to OpenGL display with specified parameters.
        /// </summary>
        public void Draw(Vector2 currentPos, Vector2 origin, Color drawColour, Vector2 scaleVector, float rotation,
                         Rectangle? srcRect)
        {
            if (textureId < 0)
                return;

            Rectangle drawRect = srcRect == null ? new Rectangle(0, 0, textureWidth, textureHeight) : srcRect.Value;

            float drawHeight = drawRect.Height * Math.Abs(scaleVector.Y);
            float drawWidth = drawRect.Width * Math.Abs(scaleVector.X);

            Vector2 originVector = new Vector2(origin.X * drawWidth / drawRect.Width, origin.Y * drawHeight / drawRect.Height);

            bool verticalFlip = scaleVector.Y < 0;
            bool horizontalFlip = scaleVector.X < 0;

            GlControl.SetColour(drawColour);

            GlControl.Bind(textureId);

            Gl.glPushMatrix();

            Gl.glTranslatef(currentPos.X, currentPos.Y, 0);
            if (rotation != 0) Gl.glRotatef(MathHelper.ToDegrees(rotation), 0, 0, 1.0f);
            if (originVector != Vector2.Zero) Gl.glTranslatef(-originVector.X, -originVector.Y, 0);

            Gl.glBegin(Gl.GL_QUADS);

            if (GlControl.SurfaceType == Gl.GL_TEXTURE_2D)
            {
                float left = (float)drawRect.Left / potWidth;
                float right = (float)drawRect.Right / potWidth;
                float top = (float)drawRect.Top / potHeight;
                float bottom = (float)drawRect.Bottom / potHeight;

                Gl.glTexCoord2f(horizontalFlip ? right : left, verticalFlip ? top : bottom);
                Gl.glVertex2f(0, drawHeight);

                Gl.glTexCoord2f(horizontalFlip ? left : right, verticalFlip ? top : bottom);
                Gl.glVertex2f(drawWidth, drawHeight);

                Gl.glTexCoord2f(horizontalFlip ? left : right, verticalFlip ? bottom : top);
                Gl.glVertex2f(drawWidth, 0);

                Gl.glTexCoord2f(horizontalFlip ? right : left, verticalFlip ? bottom : top);
                Gl.glVertex2f(0, 0);
            }
            else
            {
                Gl.glTexCoord2f(horizontalFlip ? drawRect.Right : drawRect.Left, verticalFlip ? drawRect.Top : drawRect.Bottom);
                Gl.glVertex2f(0, drawHeight);

                Gl.glTexCoord2f(horizontalFlip ? drawRect.Left : drawRect.Right, verticalFlip ? drawRect.Top : drawRect.Bottom);
                Gl.glVertex2f(drawWidth, drawHeight);

                Gl.glTexCoord2f(horizontalFlip ? drawRect.Left : drawRect.Right, verticalFlip ? drawRect.Bottom : drawRect.Top);
                Gl.glVertex2f(drawWidth, 0);

                Gl.glTexCoord2f(horizontalFlip ? drawRect.Right : drawRect.Left, verticalFlip ? drawRect.Bottom : drawRect.Top);
                Gl.glVertex2f(0, 0);
            }

            Gl.glEnd();
            Gl.glPopMatrix();
        }


        public void SetData(int textureId)
        {
            this.textureId = textureId;
        }

        /// <summary>
        /// Load texture data from a raw byte array (BGRA 32bit format)
        /// </summary>
        public void SetData(byte[] data, int level = 0, int format = Gl.GL_BGRA)
        {
            GCHandle h0 = GCHandle.Alloc(data, GCHandleType.Pinned);
            SetData(h0.AddrOfPinnedObject(), level, format);
            h0.Free();
        }

        internal static int GetPotDimension(int size)
        {
            int pot = 1;
            while (pot < size)
                pot *= 2;
            return pot;
        }

        /// <summary>
        /// Load texture data from a raw IntPtr location (BGRA 32bit format)
        /// </summary>
        public void SetData(IntPtr dataPointer, int level, int format)
        {
            if (format == 0)
                format = Gl.GL_BGRA;

            bool newTexture = false;

            int thisTextureId = textureId;

            if (level == 0 && textureId < 0)
            {
                Delete();
                newTexture = true;
                int[] textures = new int[1];
                Gl.glGenTextures(1, textures);
                thisTextureId = textures[0];
            }

            if (level > 0)
                return;

            GlControl.Texturing = true;

            GlControl.Bind(thisTextureId);
            Gl.glTexParameteri(GlControl.SurfaceType, Gl.GL_TEXTURE_MIN_FILTER, (int)Gl.GL_LINEAR);
            Gl.glTexParameteri(GlControl.SurfaceType, Gl.GL_TEXTURE_MAG_FILTER, (int)Gl.GL_LINEAR);

            if (newTexture)
            {
                if (GlControl.SurfaceType == Gl.GL_TEXTURE_2D)
                {
                    if (potWidth == textureWidth && potHeight == textureHeight)
                    {
                        Gl.glTexImage2D(GlControl.SurfaceType, level, Gl.GL_RGBA, potWidth, potHeight, 0, format,
                                        Gl.GL_UNSIGNED_BYTE, dataPointer);
                    }
                    else
                    {
                        byte[] temp = new byte[potWidth * potHeight * 4];
                        GCHandle h0 = GCHandle.Alloc(temp, GCHandleType.Pinned);
                        Gl.glTexImage2D(GlControl.SurfaceType, level, Gl.GL_RGBA, potWidth, potHeight, 0, format,
                                        Gl.GL_UNSIGNED_BYTE, h0.AddrOfPinnedObject());
                        h0.Free();

                        Gl.glTexSubImage2D(GlControl.SurfaceType, level, 0, 0, textureWidth, textureHeight, format,
                                           Gl.GL_UNSIGNED_BYTE, dataPointer);
                    }
                }
                else
                {
                    Gl.glTexImage2D(GlControl.SurfaceType, level, Gl.GL_RGBA, textureWidth, textureHeight, 0, format,
                                    Gl.GL_UNSIGNED_BYTE, dataPointer);
                }
            }
            else
            {
                lock (GlControl.ThreadingLock)
                {
                    Gl.glTexSubImage2D(GlControl.SurfaceType, level, 0, 0, textureWidth, textureHeight, format,
                                       Gl.GL_UNSIGNED_BYTE, dataPointer);

                    if (GameBase.MainThread != Thread.CurrentThread)
                        Gl.glFinish();
                }
            }

            if (GameBase.MainThread != Thread.CurrentThread && textureId != thisTextureId)
            {
                //We are required to call glFinish after threaded texture load operations (quite likely to happen in the work delegate).
                //http://higherorderfun.com/blog/tag/tutorial/
                Gl.glFinish();
            }

            textureId = thisTextureId;
        }

        /// <summary>
        /// Required for texturemapping. (read-only)
        /// </summary>
        public int TextureId
        {
            get
            {
                return textureId;
            }
        }
    }
}