﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Color = Microsoft.Xna.Framework.Graphics.Color;
using osu.Configuration;
using osu.Graphics.Notifications;

namespace osu.Graphics.OpenGl
{
    public sealed class GlControl : UserControl
    {
        private static string SupportedExtensions;
        internal static uint SurfaceType = Gl.GL_TEXTURE_2D;
        internal uint dc;
        private uint m_uint_HWND;
        internal uint renderContext;
        internal uint renderContextBackground;
        internal static bool AllowBackgroundRender = false;

        public GlControl()
        {
            CausesValidation = false;
            Capture = false;

            InitializeComponent();
            //Attach load and size change event handlers
            SizeChanged += ResizeGL;

            SetStyle(ControlStyles.AllPaintingInWmPaint, true); // No Need To Erase Form Background
            SetStyle(ControlStyles.DoubleBuffer, false); // Buffer Control
            SetStyle(ControlStyles.Opaque, true); // No Need To Draw Form Background
            SetStyle(ControlStyles.ResizeRedraw, true); // Redraw On Resize
            SetStyle(ControlStyles.UserPaint, true); // We'll Handle Painting Ourselves

            DoubleBuffered = false;
        }

        /// <summary>
        /// Gets the window handle for the control.
        /// </summary>
        public uint HWND
        {
            get { return m_uint_HWND; }
        }

        /// <summary>
        /// Gets the device context for the control.
        /// </summary>
        public uint DC
        {
            get { return dc; }
        }

        /// <summary>
        /// Gets the rendering context for the control.
        /// </summary>
        public uint RC
        {
            get { return renderContext; }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (renderContext != 0)
                {
                    WGL.wglDeleteContext(renderContext);
                    renderContext = 0;
                }

                if (renderContextBackground != 0)
                {
                    WGL.wglDeleteContext(renderContextBackground);
                    renderContextBackground = 0;
                }
            }
            base.Dispose(disposing);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            // This overrides the System.Windows.Forms.Control protected method
            // "OnPaintBackground()" so that we don't clear the client area of
            // this form window -- so the OpenGL doesn't flicker on each frame.
        }

        /// <summary>
        /// OnPaint is called when the control needs to be refreshed.  OpenGL
        /// specific drawing should be done by overriding this method.
        /// </summary>
        protected override void OnPaint(PaintEventArgs e)
        {
        }

        int badFrame;
        long lastFrame;
        internal void Draw()
        {
            if (dc == 0 || renderContext == 0)
                return;

            if (!ConfigManager.VSync)
            {
                Gl.glFinish();
            }
            else
            {
                WGL.wglSwapBuffers(dc);
                Invalidate(false);
            }
        }

        internal bool MakeCurrent(bool backgroundThread = false)
        {
            return WGL.wglMakeCurrent(dc, backgroundThread ? renderContextBackground : renderContext) == 0;
        }

        internal void RemoveCurrent()
        {
            WGL.wglMakeCurrent(0, 0);
        }

        /// <summary>
        ///	Event handler called when the form is loaded.  It retrieves the controls
        ///	window handle and device context and creates the rendering context.
        /// </summary>
        internal void InitializeGL()
        {
            m_uint_HWND = (uint)Handle.ToInt32();
            dc = WGL.GetDC(m_uint_HWND);

            WGL.wglSwapBuffers(dc);

            if (ConfigManager.VSync)
            {
                Gl.glReadBuffer(Gl.GL_BACK);
                Gl.glDrawBuffer(Gl.GL_BACK);
            }

            //Get the pixel format
            WGL.PIXELFORMATDESCRIPTOR pfd = new WGL.PIXELFORMATDESCRIPTOR();
            WGL.ZeroPixelDescriptor(ref pfd);
            pfd.nVersion = 1;
            pfd.dwFlags = (WGL.PFD_DRAW_TO_WINDOW | WGL.PFD_SUPPORT_OPENGL | (ConfigManager.VSync ? WGL.PFD_DOUBLEBUFFER : WGL.PFD_DOUBLEBUFFER_DONTCARE));
            // | WGL.PFD_DOUBLEBUFFER
            pfd.iPixelType = (byte)(WGL.PFD_TYPE_RGBA);
            pfd.cColorBits = 32;
            pfd.cAlphaBits = 8;
            pfd.cDepthBits = 32;
            pfd.iLayerType = (byte)(WGL.PFD_MAIN_PLANE);

            int pixelFormatIndex = 0;
            pixelFormatIndex = WGL.ChoosePixelFormat(dc, ref pfd);
            if (pixelFormatIndex == 0)
            {
                MessageBox.Show("Unable to retrieve pixel format");
                return;
            }

            if (WGL.SetPixelFormat(dc, pixelFormatIndex, ref pfd) == 0)
            {
                MessageBox.Show("Unable to set pixel format");
                return;
            }

            //Create rendering context
            renderContext = WGL.wglCreateContext(dc);

            try
            {
                renderContextBackground = WGL.wglCreateContext(dc);
                if (!WGL.wglShareLists(renderContext, renderContextBackground))
                    throw new Exception();
            }
            catch
            {
                AllowBackgroundRender = false;
            }

            if (WGL.wglMakeCurrent(dc, renderContext) == 0)
            {
                MessageBox.Show("Unable to make rendering context current");
                return;
            }

            if (CheckExtension(@"GL_NV_texture_rectangle"))
                SurfaceType = Gl.GL_TEXTURE_RECTANGLE_NV;
            else if (CheckExtension(@"GL_ARB_texture_rectangle"))
                SurfaceType = Gl.GL_TEXTURE_RECTANGLE_ARB;
            else
                SurfaceType = Gl.GL_TEXTURE_2D;

            //Set up OpenGL related characteristics
            ResizeGL(null, null);

            Gl.glHint(Gl.GL_PERSPECTIVE_CORRECTION_HINT, Gl.GL_NICEST);
            Gl.glDisable(Gl.GL_DEPTH_TEST);
            Gl.glDisable(Gl.GL_LIGHTING);
            Gl.glEnable(Gl.GL_BLEND);
        }

        private static unsafe bool CheckExtension(string extensionName)
        {
            try
            {
                if (SupportedExtensions == null)
                {
                    byte* c = (byte*)Gl.glGetString(Gl.GL_EXTENSIONS);
                    int i = 0;
                    StringBuilder sb = new StringBuilder();
                    while (c[i] != 0)
                    {
                        sb.Append((char)c[i]);
                        i++;
                    }
                    SupportedExtensions = sb.ToString();
                }

                return SupportedExtensions.Contains(extensionName);
            }
            catch { }

            return false;
        }

        /// <summary>
        /// Event handler called when the control is resized.
        /// </summary>
        internal void ResizeGL(object sender, EventArgs e)
        {
            if (dc == 0 || renderContext == 0)
                return;
            if (Width == 0 || Height == 0)
                return;

            Location = new Point(0, GameBase.MenuVisible ? GameBase.EditorControl.Height : 0);
            GameBase.Form.ClientSize = new Size(GameBase.WindowWidth, GameBase.WindowHeight + (GameBase.MenuVisible ? GameBase.EditorControl.Height : 0));

            Size = new Size(GameBase.WindowWidth, GameBase.WindowHeight);

            ResetViewport();
        }

        internal static void ResetViewport(bool revert = true)
        {
            if (revert)
            {
                Viewport = new Rectangle(0, 0, GameBase.WindowWidth, GameBase.WindowHeight);
                Gl.glDrawBuffer(Gl.GL_FRONT_AND_BACK);
            }

            // Reset The Current Viewport
            Gl.glViewport(0, 0, Math.Abs(Viewport.Width), Math.Abs(Viewport.Height));

            if (revert) Gl.glDrawBuffer(Gl.GL_BACK);

            // Select The Projection Matrix
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            // Reset The Projection Matrix
            Gl.glLoadIdentity();
            Gl.glOrtho(Viewport.Left, Viewport.Right, Viewport.Bottom, Viewport.Top, -1, 1);
            // Select The Modelview Matrix
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            // Reset The Modelview Matrix
            Gl.glLoadIdentity();
        }

        internal static Rectangle Viewport;
        public static object ThreadingLock = new object();

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Name = "BaseGLControl";
            InitializeGL();
        }

        #endregion

        static bool texturing;
        public static bool Texturing
        {
            get
            {
                return texturing;
            }

            set
            {
                if (texturing == value) return;
                texturing = value;

                if (texturing)
                    Gl.glEnable(GlControl.SurfaceType);
                else
                    Gl.glDisable(GlControl.SurfaceType);
            }
        }

        static int lastBoundTexture = -1;
        internal static void Bind(int textureId)
        {
            Texturing = true;

            if (lastBoundTexture != textureId)
            {
                Gl.glBindTexture(GlControl.SurfaceType, textureId);
                lastBoundTexture = textureId;
            }
        }

        private static Color? lastColour;

        internal static void SetColour(Color c)
        {
            if (lastColour == c) return;

            Gl.glColor4ub(c.R, c.G, c.B, c.A);
            lastColour = c;
        }

        static uint lastSrcBlend, lastDestBlend;
        internal static void SetBlend(uint src, uint dest)
        {
            if (lastSrcBlend == src && lastDestBlend == dest)
                return;

            Gl.glBlendFunc(src, dest);

            lastSrcBlend = src;
            lastDestBlend = dest;
        }
    }
}
