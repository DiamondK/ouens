using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Primitives;
using System.Runtime.InteropServices;

namespace osu.Graphics.Sprites
{

    /// <summary>
    /// See http://easings.net/ for more samples.
    /// </summary>
    internal enum EasingTypes
    {
        None,
        Out,
        In,
        InQuad,
        OutQuad,
        InOutQuad,
        InCubic,
        OutCubic,
        InOutCubic,
        InQuart,
        OutQuart,
        InOutQuart,
        InQuint,
        OutQuint,
        InOutQuint,
        InSine,
        OutSine,
        InOutSine,
        InExpo,
        OutExpo,
        InOutExpo,
        InCirc,
        OutCirc,
        InOutCirc,
        InElastic,
        OutElastic,
        OutElasticHalf,
        OutElasticQuarter,
        InOutElastic,
        InBack,
        OutBack,
        InOutBack,
        InBounce,
        OutBounce,
        InOutBounce
    }

    [Flags]
    internal enum TransformationType
    {
        None                    = 0,
        Movement                = 1 << 0,
        Fade                    = 1 << 1,
        Scale                   = 1 << 2,
        Rotation                = 1 << 3,
        Colour                  = 1 << 4,
        ParameterFlipHorizontal = 1 << 5,
        ParameterFlipVertical   = 1 << 6,
        MovementX               = 1 << 7,
        MovementY               = 1 << 8,
        VectorScale             = 1 << 9,
        ParameterAdditive       = 1 << 10,
        ClippingWidth           = 1 << 11,
        ClippingHeight          = 1 << 12,
        ClipRectangle           = 1 << 13,
    }

    [StructLayout(LayoutKind.Explicit)]
    internal class Transformation : IComparable<Transformation>
    {
        [FieldOffset(0)]
        internal float StartFloat;
        [FieldOffset(4)]
        internal Vector2 StartVector;
        [FieldOffset(12)]
        internal Color StartColour;
        [FieldOffset(0)]
        internal RectangleF StartRectangle;

        [FieldOffset(16)]
        internal float EndFloat;
        [FieldOffset(20)]
        internal Vector2 EndVector;
        [FieldOffset(28)]
        internal Color EndColour;
        [FieldOffset(16)]
        internal RectangleF EndRectangle;

        [FieldOffset(32)]
        internal int Time1;
        [FieldOffset(36)]
        internal int Time2;
        [FieldOffset(40)]
        internal int LoopDelay;
        [FieldOffset(44)]
        internal int MaxLoopCount;
        [FieldOffset(48)]
        internal int CurrentLoopCount;

        [FieldOffset(52)]
        internal TransformationType Type;
        [FieldOffset(56)]
        internal EasingTypes Easing;

        [FieldOffset(60)]
        internal byte TagNumeric;
        [FieldOffset(61)]
        internal bool Loop;
        [FieldOffset(62)]
        internal bool IsLoopStatic;

        internal Transformation() { }

        internal Transformation(TransformationType type, float startFloat, float endFloat, int time1, int time2, EasingTypes easing = EasingTypes.None)
        {
            Type = type;
            StartFloat = startFloat;
            EndFloat = endFloat;
            Time1 = time1;
            Time2 = time2;
            Easing = easing;
        }

        internal Transformation(Vector2 startPosition, Vector2 endPosition, int time1, int time2, EasingTypes easing = EasingTypes.None)
        {
            Type = TransformationType.Movement;
            StartVector = startPosition;
            EndVector = endPosition;
            Easing = easing;
            Time1 = time1;
            Time2 = time2;
        }

        internal Transformation(TransformationType type, Vector2 startVector, Vector2 endVector, int time1, int time2, EasingTypes easing = EasingTypes.None)
        {
            Type = type;
            StartVector = startVector;
            EndVector = endVector;
            Time1 = time1;
            Time2 = time2;
            Easing = easing;
        }

        internal Transformation(Color startColour, Color endColour, int time1, int time2)
        {
            Type = TransformationType.Colour;
            StartColour = startColour;
            EndColour = endColour;
            Time1 = time1;
            Time2 = time2;
        }

        internal Transformation(RectangleF startClipRectangle, RectangleF endClipRectangle, int time1, int time2, EasingTypes easing = EasingTypes.None)
        {
            Type = TransformationType.ClipRectangle;
            StartRectangle = startClipRectangle;
            EndRectangle = endClipRectangle;
            Time1 = time1;
            Time2 = time2;
            Easing = easing;
        }

        internal int Duration
        {
            get { return Time2 - Time1; }
        }

        #region IComparable<Transformation> Members

        public int CompareTo(Transformation other)
        {
            int compare = Time1.CompareTo(other.Time1);
            if (compare != 0) return compare;
            compare = Time2.CompareTo(other.Time2);
            if (compare != 0) return compare;
            return Type.CompareTo(other.Type);
        }

        #endregion

        internal Transformation Clone()
        {
            return (Transformation)MemberwiseClone();
        }

        internal Transformation CloneReverse()
        {
            Transformation t = Clone();

            t.StartFloat = EndFloat;
            t.StartColour = EndColour;
            t.StartVector = EndVector;
            t.StartRectangle = t.EndRectangle;

            t.EndFloat = StartFloat;
            t.EndColour = StartColour;
            t.EndVector = StartVector;
            t.EndRectangle = StartRectangle;

            switch (Easing)
            {
                case EasingTypes.Out:
                    t.Easing = EasingTypes.In;
                    break;
                case EasingTypes.In:
                    t.Easing = EasingTypes.Out;
                    break;
                default:
                    t.Easing = EasingTypes.None;
                    break;
            }

            return t;
        }

        internal static bool testFlag(TransformationType options, TransformationType search)
        {
            return (options & search) > 0;
        }

        public override string ToString()
        {
            switch (Type)
            {
                default:
                    return string.Format("{2} {0}-{1} {3}->{4}", Time1, Time2, Type, StartFloat, EndFloat);
                case TransformationType.Movement:
                    return string.Format("{2} {0}-{1} {3}->{4}", Time1, Time2, Type, StartVector, EndVector);
                case TransformationType.Colour:
                    return string.Format("{2} {0}-{1} {3}->{4}", Time1, Time2, Type, StartColour, EndColour);
                case TransformationType.ClipRectangle:
                    return string.Format("{2} {0}-{1} {3}->{4}", Time1, Time2, Type, StartRectangle, EndRectangle);
            }
        }
    }
}