using System;
using System.Collections.Generic;
using osu_common.Helpers;

namespace osu.Graphics.Sprites
{
    internal class TransformationLoop
    {
        internal int LoopCount = 1;
        internal int StartTime;
        internal pList<Transformation> Transformations = new pList<Transformation>();

        internal TransformationLoop(int startTime, int loopCount)
        {
            LoopCount = loopCount;
            StartTime = startTime;
        }

        internal TransformationLoop Clone()
        {
            TransformationLoop clone = new TransformationLoop(StartTime, LoopCount);
            foreach (Transformation t in Transformations)
                clone.Transformations.Add(t.Clone());
            return clone;
        }

        internal virtual List<Transformation> MakeStaticContent()
        {
            if (Transformations.Count == 0)
                return null;

            List<Transformation> addUs = new List<Transformation>();

            int last = -1;
            int first = -1;

            for (int i = 0; i < Transformations.Count; i++)
            {
                if (Transformations[i].Time1 < first || first == -1)
                    first = Transformations[i].Time1;
                if (Transformations[i].Time2 > last || last == -1)
                    last = Transformations[i].Time2;
            }

            int lengthOneLoop = last - first;

            for (int il = 0; il < Transformations.Count; il++)
            {
                Transformation staticTransform = Transformations[il].Clone();
                staticTransform.IsLoopStatic = true;

                if (LoopCount > 1)
                {
                    staticTransform.Loop = true;
                    staticTransform.LoopDelay = lengthOneLoop - (staticTransform.Time2 - staticTransform.Time1);
                    staticTransform.MaxLoopCount = LoopCount;
                }

                staticTransform.Time1 += StartTime;
                staticTransform.Time2 += StartTime;

                addUs.Add(staticTransform);
            }

            return addUs;
        }
    }
}