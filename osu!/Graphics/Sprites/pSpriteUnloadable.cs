﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace osu.Graphics.Sprites
{
    internal class pSpriteUnloadable : pSprite
    {
        internal pSpriteUnloadable(pTexture texture, Fields fieldType, Origins origin, Clocks clock, Vector2 position)
            : base(texture, fieldType, origin, clock, position)
        {
        }

        internal pSpriteUnloadable(pTexture texture, Origins origin, Vector2 startPosition, float drawDepth, bool alwaysDraw, Color colour)
            : base(texture, origin, startPosition, drawDepth, alwaysDraw, colour)
        {
        }

        internal pSpriteUnloadable(pTexture texture, Vector2 startPosition, float drawDepth, bool alwaysDraw, Color colour)
            : base(texture, startPosition, drawDepth, alwaysDraw, colour)
        {
        }

        internal pSpriteUnloadable(pTexture texture, Fields fieldType, Origins origin, Clocks clock, Vector2 startPosition, float drawDepth, bool alwaysDraw, Color colour)
            : base(texture, fieldType, origin, clock, startPosition, drawDepth, alwaysDraw, colour)
        {
        }

        internal pSpriteUnloadable(pTexture texture, Fields fieldType, Origins origin, Clocks clock, Vector2 startPosition, float drawDepth, bool alwaysDraw, Color colour, object tag)
            : base(texture, fieldType, origin, clock, startPosition, drawDepth, alwaysDraw, colour, tag)
        {
        }

        internal bool CheckUnloadable()
        {
            if (localTexture != null && GameBase.Time - localTexture.LastAccess > 5000)
                return Unload();

            return false;
        }

        internal bool Unload()
        {
            if (localTexture == null) return false;

            localTexture.Dispose();
            localTexture = null;
            State = LoadState.Unknown;


            return true;
        }

        internal LoadState State = LoadState.Unknown;
    }

    internal enum LoadState
    {
        Unknown,
        Retrieving,
        Loaded,
        Unloaded,
        Error
    }
}
