﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.GameplayElements.Events.Trigger;

namespace osu.Graphics.Sprites
{
    class TriggerLoop : TransformationLoop
    {
        internal readonly EventTrigger Trigger;
        internal int TriggerEndTime;
        internal int TriggerStartTime;
        private List<Transformation> lastStatic;

        public TriggerLoop(EventTrigger trigger, int startTime, int endTime) : base(startTime,1)
        {
            Trigger = trigger;
            TriggerEndTime = endTime;
            TriggerStartTime = startTime;
        }

        internal override List<Transformation> MakeStaticContent()
        {
            List<Transformation> staticContent = base.MakeStaticContent();

            lastStatic = staticContent;

            return staticContent;
        }

        public List<Transformation> GetLastTransformations()
        {
            return lastStatic;
        }
    }
}
