using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Renderers;
using osu_common.Helpers;
using System.Collections.Generic;

namespace osu.Graphics.Sprites
{
    internal class pText : pSpriteUnloadable
    {
        public int BorderWidth = 1;
        private int renderingResolution = GameBase.WindowHeight;
        internal bool TextAa;
        internal TextAlignment TextAlignment;

        internal event EventHandler OnRefreshTexture;

        public bool TextBold;
        internal Vector2 TextBounds;

        public Vector4 CornerBounds;
        public Vector2 Padding;

        private Color backgroundColour;
        internal Color BackgroundColour
        {
            get
            {
                return backgroundColour;
            }
            set
            {
                backgroundColour = value;
                TextChanged = true;
            }
        }

        private Color borderColour;
        internal Color BorderColour
        {
            get
            {
                return borderColour;
            }
            set
            {
                borderColour = value;
                TextChanged = true;
            }
        }

        private Color textColour;
        internal Color TextColour
        {
            get
            {
                return textColour;
            }
            set
            {
                textColour = value;
                TextChanged = true;
            }
        }

        internal bool TextRenderSpecific; //todo: deprecate this somehow.

        internal bool TextShadow
        {
            get { return Shadow == ShadowType.Normal; }
            set
            {
                Shadow = value ? ShadowType.Normal : ShadowType.None;
            }
        }
        internal bool TextBorder
        {
            get { return Shadow == ShadowType.Border; }
            set
            {
                Shadow = value ? ShadowType.Border : ShadowType.None;
            }
        }

        internal ShadowType Shadow;

        private string text;

        internal bool TextChanged = true;

        internal virtual string Text
        {
            get { return text; }
            set
            {
                if (text == value)
                    return;
                TextChanged = true;
                text = value;
            }
        }

        private float textSize;
        internal float TextSize
        {
            get { return textSize; }
            set
            {
                if (textSize == value) return;

                TextChanged = true;
                textSize = value;
            }
        }
        public bool TextUnderline;
        private bool aggressiveCleanup;


        private string fontFace = General.FONT_FACE_DEFAULT;
        internal string FontFace
        {
            get { return fontFace; }
            set
            {
                if (value == fontFace) return;
                fontFace = value;
                TextChanged = true;
            }
        }

        internal bool AggressiveCleanup
        {
            get { return aggressiveCleanup; }
            set
            {
                aggressiveCleanup = value;
                if (base.Texture != null)
                {
                    base.Texture.TrackAccessTime = true;
                    DynamicSpriteCache.Load(this, true);
                }
            }
        }

        public override pSprite Clone()
        {
            pText clone = new pText(Text, TextSize, Position, Depth, AlwaysDraw, InitialColour);
            clone.TextAlignment = TextAlignment;
            if (localTexture != null) clone.Texture = localTexture;
            clone.OriginPosition = OriginPosition;
            clone.Origin = Origin;
            clone.Position = Position;
            clone.DrawLeft = DrawLeft;
            clone.DrawTop = DrawTop;
            clone.DrawWidth = DrawWidth;
            clone.DrawHeight = DrawHeight;


            return clone;
        }

        internal pText(string text, float textSize, Vector2 startPosition, Vector2 bounds, float drawDepth,
                       bool alwaysDraw, Color colour, bool shadow = true)
            : base(
                null, Fields.TopLeft, Origins.TopLeft, Clocks.Game, startPosition, drawDepth, alwaysDraw,
                colour)
        {
            Text = text;
            IsDisposable = true;
            TextColour = Color.White;
            TextShadow = !(colour.R == 0 && colour.G == 0 && colour.B == 0) && shadow;
            TextSize = textSize;
            TextAlignment = TextAlignment.Left;
            TextBounds = bounds;
            TextAa = true;

            if (textSize * GameBase.WindowRatio <= 15)
                ExactCoordinates = true;

            ScaleToWindowRatio = false;
            TextRenderSpecific = true;
        }

        protected pText(string text, Fields field, Origins origin, Clocks clock, Vector2 position, float depth, bool alwaysDraw, Color colour)
            : base(null, field, origin, clock, position)
        {
            Depth = depth;
            AlwaysDraw = alwaysDraw;
            InitialColour = colour;
            Alpha = InitialColour.A / 255f;
        }

        public pText(string text, float textSize, Vector2 startPosition, float drawDepth, bool alwaysDraw, Color colour) :
            this(text, textSize, startPosition, Vector2.Zero, drawDepth, alwaysDraw, colour, true)
        {
        }

        internal override pTexture Texture
        {
            get
            {
                if (string.IsNullOrEmpty(text) && BackgroundColour.A == 0)
                    return null;

                if (localTexture != null && !localTexture.IsDisposed && !TextChanged && GameBase.WindowHeight == renderingResolution)
                    return localTexture;

                return refreshTexture();
            }
            set {
                if (localTexture != null) return;
                refreshTexture(value);
            }
        }

        internal List<int> ComputeLineBreaks(System.Drawing.RectangleF[] characterRegions)
        {
            List<int> result = new List<int>();
            result.Add(0);

            if (Text.Length == 0)
                return result;

            float lastY = characterRegions[0].Y;

            for (int i = 1; i < Text.Length; ++i)
            {
                if (characterRegions[i].Y > lastY)
                {
                    result.Add(i);
                    lastY = characterRegions[i].Y;
                }
            }

            return result;
        }

        internal System.Drawing.RectangleF[] MeasureCharacters()
        {
            float size = (TextRenderSpecific ? (float)GameBase.WindowHeight / 768 : 1) * TextSize;

            if (Text.Length == 0)
                return null;

            Vector2 bounds = (TextRenderSpecific ? GameBase.WindowRatio : 1) * TextBounds;

            System.Drawing.RectangleF[] regions;
            Vector2 measure;
            NativeText.CreateText(Text, size, bounds, TextColour, Shadow, TextBold, TextUnderline, TextAlignment, TextAa, out measure, out regions, BackgroundColour, BorderColour, BorderWidth, true, true, fontFace, Vector4.Zero, Vector2.Zero, localTexture);

            if (TextRenderSpecific)
            {
                for (int i = 0; i < Text.Length; ++i)
                {
                    regions[i].X /= GameBase.WindowRatio;
                    regions[i].Y /= GameBase.WindowRatio;
                    regions[i].Width /= GameBase.WindowRatio;
                    regions[i].Height /= GameBase.WindowRatio;
                }

                return regions;
            }

            for (int i = 0; i < Text.Length; ++i)
            {
                regions[i].X *= 0.625f;
                regions[i].Y *= 0.625f;
                regions[i].Width *= 0.625f;
                regions[i].Height *= 0.625f;
            }

            return regions;
        }

        internal Vector2 MeasureText(int startIndex, int length)
        {
            return MeasureText(startIndex, length, Vector2.Zero);
        }

        internal Vector2 MeasureText(int startIndex, int length, Vector2 customBounds)
        {
            float size = (TextRenderSpecific ? (float)GameBase.WindowHeight / 768 : 1) * TextSize;

            if (Text.Length == 0)
                return Vector2.Zero;

            if (length == 0)
                return Vector2.Zero;

            Vector2 bounds = (TextRenderSpecific ? GameBase.WindowRatio : 1) * customBounds;

            System.Drawing.RectangleF[] regions;
            Vector2 measure;
            NativeText.CreateText(Text, size, bounds, TextColour, Shadow, TextBold, TextUnderline, TextAlignment, TextAa, out measure, out regions, BackgroundColour, BorderColour, BorderWidth, true, false, fontFace, Vector4.Zero, Vector2.Zero, localTexture, startIndex, length);
            if (TextRenderSpecific)
                return measure / GameBase.WindowRatio;

            return measure * 0.625f;
        }

        internal virtual Vector2 MeasureText()
        {
            pTexture tex = Texture; // calling this will refresh the texture if required.

            if (TextRenderSpecific)
                return lastMeasure / GameBase.WindowRatio;
            else
                return lastMeasure * 0.625f;
        }

        public override UpdateResult Update()
        {
            UpdateResult res = base.Update();
            if (res != UpdateResult.Visible) return res;

            if (string.IsNullOrEmpty(Text) && TextBounds == Vector2.Zero)
                return UpdateResult.NotVisible;

            if (TextChanged)
            {
                refreshTexture();
                base.Update(); //call this again to make sure we update the texture alignment after re-rendering.
            }

            return res;
        }

        protected virtual pTexture refreshTexture(pTexture tex = null)
        {
            if (OnRefreshTexture != null)
                OnRefreshTexture(this, null);

            bool existed = false;

            pTexture old = localTexture;

            TextChanged = false;

            if (string.IsNullOrEmpty(Text) && TextBounds.X == 0)
            {
                lastMeasure = TextBounds;
                return null;
            }

            renderingResolution = GameBase.WindowHeight;

            float size = (TextRenderSpecific ? (float)GameBase.WindowHeight / 768 : 1) * TextSize;
            Vector2 bounds = (TextRenderSpecific ? GameBase.WindowRatio : 1) * TextBounds;

            Vector4 corners = CornerBounds;
            Vector2 padding = Padding;

            if (corners != Vector4.Zero && TextRenderSpecific)
                corners *= (float)GameBase.WindowHeight / 768;

            if (Padding != Vector2.Zero)
                padding *= (float)GameBase.WindowHeight / 768;

            float borderWidth = BorderWidth;
            if (borderWidth > 0)
                borderWidth *= (float)GameBase.WindowHeight / 768;

            System.Drawing.RectangleF[] regions;
            localTexture =
                tex != null ? tex :
                NativeText.CreateText(Text, size, bounds, TextColour, Shadow, TextBold, TextUnderline, TextAlignment,
                                      TextAa, out lastMeasure, out regions, BackgroundColour, BorderColour, (int)Math.Round(borderWidth), false, false, fontFace, corners, padding, localTexture);

            if (old != null && localTexture != old) old.Dispose();

            if (aggressiveCleanup)
            {
                if (localTexture != null)
                    localTexture.TrackAccessTime = true;
                if (old != localTexture)
                    DynamicSpriteCache.Load(this, true);
            }

            DrawWidth = (int)Math.Round(lastMeasure.X);
            DrawHeight = (int)Math.Round(lastMeasure.Y);

            UpdateTextureSize();
            UpdateTextureAlignment();

            return localTexture;
        }

        public override string ToString()
        {
            return "Text: " + Text + " (" + Position.X + "," + Position.Y + ") @ " + textSize + "pt " + Math.Round(Alpha * 100, 1) + "%";
        }

        internal override void Dispose(bool isDisposing)
        {
            if (aggressiveCleanup)
                DynamicSpriteCache.Remove(this);
            base.Dispose(isDisposing);
        }
    }
}