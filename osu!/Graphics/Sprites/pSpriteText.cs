﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.OpenGl;
using osu.Graphics.Primitives;

namespace osu.Graphics.Sprites
{
    internal class pSpriteText : pText
    {
        internal List<Vector2> renderCoordinates = new List<Vector2>();
        internal List<pTexture> renderTextures = new List<pTexture>();

        internal bool TextConstantSpacing;
        internal string TextFont = @"default";
        internal string TextFontPrefix;

        internal int SpacingOverlap;

        internal pSpriteText(string text, string fontname, int spacingOverlap, Fields fieldType, Origins origin, Clocks clock,
                             Vector2 startPosition, float drawDepth, bool alwaysDraw, Color colour, bool precache = true)
            : base(null, fieldType, origin, clock, startPosition, drawDepth, alwaysDraw, colour)
        {
            Text = text;
            localTexture = null;
            TextFont = fontname;
            TextFontPrefix = fontname + '-';
            SpacingOverlap = spacingOverlap;

            if (precache)
            {
                //cache all sprites that may be used in the future.
                for (int i = 0; i < 10; i++)
                    SkinManager.Load(TextFontPrefix + i);
                SkinManager.Load(TextFontPrefix + @"dot");
                SkinManager.Load(TextFontPrefix + @"comma");
                SkinManager.Load(TextFontPrefix + @"percent");
            }
        }

        internal override Vector2 MeasureText()
        {
            if (TextChanged)
                refreshTexture();

            return lastMeasure;
        }

        private int constantWidth = -1;
        private int spaceWidth = -1;

        public override UpdateResult Update()
        {
            UpdateResult res = base.Update();
            if (res != UpdateResult.Visible) return res;

            if (TextChanged)
                refreshTexture();

            return res;
        }

        protected override pTexture refreshTexture(pTexture tex = null)
        {
            TextChanged = false;

            renderTextures.Clear();
            renderCoordinates.Clear();

            int currentX = 0;
            int height = 0;

            int width = 0;

            string text = Text;
            int length = text == null ? 0 : text.Length;

            for (int i = 0; i < length; i++)
            {
                tex = null;
                currentX -= (TextConstantSpacing || i == 0 ? 0 : SpacingOverlap);

                int x = currentX;

                switch (text[i])
                {
                    case ' ':
                        tex = null;
                        continue;
                    case ',':
                        tex = SkinManager.Load(TextFontPrefix + @"comma");
                        break;
                    case '.':
                        tex = SkinManager.Load(TextFontPrefix + @"dot");
                        break;
                    case '%':
                        tex = SkinManager.Load(TextFontPrefix + @"percent");
                        break;
                    case '\\':
                        tex = SkinManager.Load(TextFontPrefix + @"fps");
                        break;
                    default:
                        tex = SkinManager.Load(TextFontPrefix + text[i]);
                        break;
                }

                if (tex == null) continue;

                //add spacing
                switch (text[i])
                {
                    case ' ':
                        if (spaceWidth < 0)
                        {
                            pTexture pt = SkinManager.Load(TextFontPrefix + @"5");
                            constantWidth = pt != null ? pt.Width : 40;
                            pt = SkinManager.Load(TextFontPrefix + @"dot");
                            spaceWidth = pt != null ? pt.Width : 40;
                        }

                        currentX += spaceWidth;
                        continue;
                    case ',':
                    case '.':
                    case '%':
                        currentX += tex.Width;
                        break;
                    case '\\':
                        currentX += tex.Width;
                        break;
                    default:
                        if (!TextConstantSpacing)
                            currentX += tex.Width;
                        break;
                }

                if (TextConstantSpacing)
                    renderCoordinates.Add(new Vector2(currentX - x, 0));
                else
                    renderCoordinates.Add(new Vector2(x, 0));

                renderTextures.Add(tex);

                if (height == 0)
                    height = tex.Height;
            }

            if (TextConstantSpacing && length > 1)
            {
                if (spaceWidth < 0)
                {
                    pTexture pt = SkinManager.Load(TextFontPrefix + @"5");
                    constantWidth = pt != null ? pt.Width : 40;
                    pt = SkinManager.Load(TextFontPrefix + @"dot");
                    spaceWidth = pt != null ? pt.Width : 40;
                }

                float last = 0;

                currentX = 0;

                for (int i = 0; i < renderCoordinates.Count; i++)
                {
                    float special = renderCoordinates[i].X;

                    if (special == 0)
                    {
                        renderCoordinates[i] = new Vector2(currentX + Math.Max(0, (constantWidth - renderTextures[i].Width) / 2), 0);
                        currentX += constantWidth - SpacingOverlap;
                    }
                    else
                    {
                        renderCoordinates[i] = new Vector2(currentX, 0);
                        currentX += (int)special - SpacingOverlap;
                    }
                }
            }

            width = currentX;

            lastMeasure = new Vector2(width, height);

            UpdateTextureSize();
            UpdateTextureAlignment();

            return null;
        }

        public override bool Draw()
        {
            if (!base.Draw()) return false;

            for (int i = 0; i < renderCoordinates.Count; i++)
            {
                if (renderTextures[i].TextureXna == null)
                    refreshTexture();

                if (i == 0 && renderTextures[i].DpiScale != 1)
                    drawScaleVector *= 1f / renderTextures[i].DpiScale;

                drawPosition = new Vector2(renderCoordinates[i].X * drawScale + drawRectangle.X, drawRectangle.Y);

                Rectangle clipRectangle = IsClipped ? ClipRectangleScaled : SpriteManager.ViewRectangleFull;

                if (GameBase.D3D)
                {
                    GameBase.spriteBatch.GraphicsDevice.ScissorRectangle = clipRectangle;
                    GameBase.spriteBatch.Draw(renderTextures[i].TextureXna,
                                              drawPosition, null,
                                              drawColour, Rotation,
                                              Vector2.Zero,
                                              drawScaleVector, SpriteEffects.None,
                                              Math.Min(1, Depth + i * 0.00001f));
                }
                if (GameBase.OGL)
                {
                    Gl.glScissor(clipRectangle.Left, GameBase.WindowHeight - clipRectangle.Bottom, clipRectangle.Width, clipRectangle.Height);
                    renderTextures[i].TextureGl.Draw(drawPosition,
                                                        Vector2.Zero, drawColour,
                                                        drawScaleVector,
                                                        Rotation, null);
                }
            }

            return true;
        }

        internal override void UpdateTextureSize()
        {
            DrawWidth = Width = (int)Math.Round(lastMeasure.X);
            DrawHeight = Height = (int)Math.Round(lastMeasure.Y);
            DrawTop = 0;
            DrawLeft = 0;
        }


    }
}