﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Microsoft.Xna.Framework;
using System.Windows.Forms;
using Color = Microsoft.Xna.Framework.Graphics.Color;
using Rectangle = System.Drawing.Rectangle;
using osu_common.Helpers;

namespace osu.Graphics.Sprites
{
    internal class pBrowser : pSprite
    {
        WebBrowser wb;
        private pTexture tex;
        Bitmap bitmapBacking;

        internal event VoidDelegate OnLoaded;
        private bool Loaded;

        internal pBrowser(string url, Vector2 startPosition, Vector2 dimensions, float drawDepth, Color colour)
            : base(null, Fields.TopLeft, Origins.TopLeft, Clocks.Game, startPosition, drawDepth, true, colour)
        {
            ScaleToWindowRatio = false;

            wb = new WebBrowser();
            wb.ScriptErrorsSuppressed = true;
            wb.ScrollBarsEnabled = false;
            wb.Width = (int)(dimensions.X);
            wb.Height = (int)(dimensions.Y);
            Width = (int)(dimensions.X);
            Height = (int)(dimensions.Y);
            DrawWidth = Width;
            DrawHeight = Height;

            ExactCoordinates = true;

            wb.ProgressChanged += wb_ProgressChanged;

            Load(url);
        }

        internal void Load(string url)
        {
            Loaded = false;
            if (url != null)
                wb.Navigate(url);
        }

        void wb_ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {
            if (e.CurrentProgress != e.MaximumProgress)
                return;

            if (Loaded) return;
            Loaded = true;

            GameBase.Scheduler.AddDelayed(delegate
            {
                try
                {
                    if (bitmapBacking == null)
                        bitmapBacking = new Bitmap(Width, Height);

                    wb.DrawToBitmap(bitmapBacking, new Rectangle(0, 0, Width, Height));
                    if (tex == null)
                        tex = pTexture.FromBitmap(bitmapBacking);
                    else
                        tex.SetData(bitmapBacking);

                    if (OnLoaded != null)
                        OnLoaded();
                }
                catch { }
            }, 300);
        }

        public override void Dispose()
        {
            if (IsDisposable)
            {
                if (wb != null) wb.Dispose();
                if (tex != null) tex.Dispose();
                if (bitmapBacking != null) bitmapBacking.Dispose();
            }

            base.Dispose();
        }

        internal override pTexture Texture
        {
            get
            {
                return tex;
            }
        }

    }
}