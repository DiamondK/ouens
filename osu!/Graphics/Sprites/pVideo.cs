using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Graphics.Renderers;

namespace osu.Graphics.Sprites
{
    internal class pVideo : pSprite
    {
        internal int EndTime;
        private int startTime;

        internal VideoTexture video;

        internal pVideo(string filename, int startTime)
            : base(
                null, Fields.StoryboardCentre, Origins.Centre, Clocks.Audio, Vector2.Zero, 0.1F, true, Color.White)
        {
            video = new VideoTexture(filename);

            video.startTime = startTime;

            EndTime = startTime + video.length;
            Width = video.Width;
            Height = video.Height;
            OriginPosition = new Vector2(video.Width / 2, video.Height / 2);
            DrawWidth = Width;
            DrawHeight = Height;
            StartTime = startTime;
        }

        internal int StartTime
        {
            get { return startTime; }
            set
            {
                startTime = value;
                video.startTime = value;
            }
        }

        internal bool Running
        {
            get { return video.playbackState == VideoTexture.PlaybackState.Running; }
        }

        internal override pTexture Texture
        {
            get { return video.Texture; }
        }

        internal void NextFrame()
        {
            video.NextFrame();
        }

        internal override void Dispose(bool isDisposing)
        {
            if (video != null)
                video.Dispose();
            video = null;

            base.Dispose(isDisposing);
        }

        internal void SeekToCurrent(bool force)
        {
            int seekTime = AudioEngine.TimeUnedited - StartTime;
            if (seekTime < 0) seekTime = 0;
            video.Seek(seekTime, force);
        }
    }
}