using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.Graphics.Skinning;
using osu_common;
using osu_common.Helpers;
using RectangleF = osu.Graphics.Primitives.RectangleF;

namespace osu.Graphics.Sprites
{
    internal class pBox : pSprite
    {
        internal pBox(Vector2 position, Vector2 size, float depth, Color colour)
            : base(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, position, depth, true, colour)
        {
            Scale = 1.6f;
            VectorScale = size;
        }
    }
}