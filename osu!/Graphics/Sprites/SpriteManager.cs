#region Using Statements

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.GameplayElements;
using osu.Graphics.OpenGl;
using osu.Graphics.Primitives;
using osu.Graphics.Renderers;
using osu.Input;
using osu.Input.Handlers;
using osu.Helpers;

#endregion

namespace osu.Graphics.Sprites
{
    internal class SpriteManager : IDisposable
    {
        internal static pSprite ClickHandledSprite;
        internal static object SpriteLock = new object();

        internal static List<SpriteManager> SpriteManagersCreationOrder = new List<SpriteManager>();
        private readonly pDrawableDepthComparer pDrawableDepthComparer = new pDrawableDepthComparer();
        internal float Alpha = 1;
        internal bool Bypass;
        internal float Blackness;
        internal float BlacknessShade = 0;
        internal int CreationTime = GameBase.Time;
        internal bool FirstDraw = true;
        internal bool EnableProfiling;
        internal long LastDrawTime;
        public float Scale = 1;
        internal float GamefieldSpriteRatio = 0;

#if DEBUG
        public static bool Placement;
#endif

        /// <summary>
        ///   Should we override the standard view rectangle?
        /// </summary>
        private bool viewRectangleCustom;

        /// <summary>
        /// Energises this SpriteManager to better handle workflows where we will always be playing forwards, and never seeking back in time.
        /// </summary>
        internal bool ForwardPlayOptimisations;

        private bool forwardPlayOptimisedAdd;

        /// <summary>
        /// Counts how many consecutive exceptions we have attempted to ignore.
        /// </summary>
        int exceptionCount;

        /// <summary>
        /// Set this to true when items added via Add() should be considered for the forward optimisation queue.
        /// On setting to false, the queue is populated.
        /// </summary>
        internal bool ForwardPlayOptimisedAdd
        {
            get { return forwardPlayOptimisedAdd; }
            set
            {
                if (forwardPlayOptimisedAdd && !value)
                {
                    int c = forwardPlayList.Count;
                    if (c > 0)
                    {
                        for (int i = 0; i < c; i++)
                            forwardPlayQueue.Enqueue(forwardPlayList[i] as pSprite);
                        forwardPlayList.Clear();
                    }
                }
                forwardPlayOptimisedAdd = value;
            }
        }

        /// <summary>
        /// List of sprites in order of their display.
        /// This is an intermediate list used to sort sprites during the Add() process before making them into a queue.
        /// </summary>
        private readonly List<pDrawable> forwardPlayList = new List<pDrawable>();

        /// <summary>
        /// Stores past sprites which are no longer needed for display (but may be used when seeking backwards).
        /// </summary>
        private readonly Stack<pDrawable> forwardPlayPast = new Stack<pDrawable>();

        /// <summary>
        /// This is the final result of ForwardPlayOptimisations.
        /// </summary>
        private readonly Queue<pDrawable> forwardPlayQueue = new Queue<pDrawable>();

        /// <summary>
        ///   Should we check for mouse clicks?
        /// </summary>
        internal bool HandleInput = true;

        /// <summary>
        ///   The currently hovered sprite.
        /// </summary>
        internal pSprite CurrentHoverSprite;

        internal bool Masking;

        internal bool WidescreenAutoOffset = true;
        internal List<pDrawable> SpriteList;
        internal Vector2 ViewOffset = Vector2.Zero;
        internal Vector2 ScrollOffset = Vector2.Zero;

        internal static RectangleF ViewRectangleCurrent = new RectangleF(0, 0, GameBase.WindowDefaultWidth, GameBase.WindowDefaultHeight);
        internal static Rectangle ViewRectangleFull = new Rectangle(0, 0, GameBase.WindowWidth, GameBase.WindowWidth);
        internal static RectangleF ViewRectangle43 = new RectangleF(0, 0, GameBase.WindowDefaultWidth, GameBase.WindowDefaultHeight);

        /// <summary>
        /// BaseOffset is applied on top of ViewOffset and offers a second degree of control in cases where scrolling is involved.
        /// </summary>
        internal Vector2 BaseOffset = Vector2.Zero;


        internal RectangleF ViewRectangle = new RectangleF(0, 0, GameBase.WindowDefaultWidth, GameBase.WindowDefaultHeight);
        internal RectangleF ViewRectangleScaled;
        internal Rectangle ClipRectangle;

        internal bool CalculateInvisibleUpdates;
        internal float BlacknessTarget = -1;

        internal int TotalSpriteCount
        {
            get
            {
                return SpriteList.Count + forwardPlayList.Count + forwardPlayQueue.Count;
            }
        }


        internal SpriteManager()
            : this(false)
        {
        }

        internal static int InstanceCount;

        internal static SpriteManager Current;

        bool isWidescreen;
        internal bool IsWidescreen { get { return isWidescreen; } }

        internal void SetWidescreen(bool enabled, bool? autoOffset = null)
        {
            isWidescreen = enabled;
            WidescreenAutoOffset = autoOffset ?? !enabled;
            ViewRectangle = enabled ? ViewRectangleCurrent : ViewRectangle43;
        }

        internal SpriteManager(bool widescreenAware)
        {
            InstanceCount++;
            SpriteList = new List<pDrawable>();
            SpriteManagersCreationOrder.Add(this);

            SetWidescreen(widescreenAware);

            GameBase.OnUnload += GameBase_OnUnload;
            GameBase.OnResolutionChange += GameBase_OnResolutionChange;
        }

        void GameBase_OnResolutionChange()
        {
            if (viewRectangleCustom) return;

            SetWidescreen(isWidescreen, WidescreenAutoOffset);
        }

        internal void RefreshCreationOrder()
        {
            SpriteManagersCreationOrder.Remove(this);
            SpriteManagersCreationOrder.Add(this);
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool isDisposed;

        private void Dispose(bool isDisposing)
        {
            if (isDisposed) return;
            isDisposed = true;

            InstanceCount--;
            Clear();
            SpriteManagersCreationOrder.Remove(this);

            GameBase.OnUnload -= GameBase_OnUnload;
            GameBase.OnResolutionChange -= GameBase_OnResolutionChange;
        }

        private void GameBase_OnUnload(bool b)
        {
            // When unloading we create a new sprite batch. We always want to call Begin again.
            hasBegun = false;

            if (!b) return;

            int count = SpriteList.Count;
            for (int index = 0; index < count; index++)
            {
                pDrawable p = SpriteList[index];
                pText pt = p as pText;
                if (pt != null && pt.localTexture != null)
                {
                    pt.localTexture.Dispose();
                    pt.localTexture = null;
                }
            }
        }

        private void resetHoverSprite()
        {
            CurrentHoverSprite = null;
        }

        /// <summary>
        ///   Special case scenario where we don't want to add using ForwardPlayOptimisedAdd.
        ///   Easier than toggling.
        /// </summary>
        /// <param name = "p"></param>
        internal void AddNonOptimised(pSprite p)
        {
            if (p == null) return;

            lock (SpriteLock)
            {
                int index = SpriteList.BinarySearch(p, pDrawableDepthComparer);
                //if (p.HoverPriority >= 0) p.HoverPriority = hoverPriorityCurrent++;
                SpriteList.Insert(index < 0 ? ~index : index, p);
            }
        }

        internal void Add(pDrawable p)
        {
            if (p == null) return;

            if (ForwardPlayOptimisedAdd && p.Transformations.Count > 0)
            {
                int index = forwardPlayList.BinarySearch(p);

                if (index < 0)
                    forwardPlayList.Insert(~index, p);
                else
                    forwardPlayList.Insert(index, p);
                return;
            }

            lock (SpriteLock)
            {
                int index = SpriteList.BinarySearch(p, pDrawableDepthComparer);
                //if (p.HoverPriority >= 0) p.HoverPriority = hoverPriorityCurrent++;
                SpriteList.Insert(index < 0 ? ~index : index, p);
            }
        }

        internal void ResortDepth(pDrawable p)
        {
            Remove(p);
            Add(p);
        }

        internal void ResortDepth()
        {
            lock (SpriteList)
                SpriteList.Sort(pDrawableDepthComparer);
        }

        internal void Add<T>(IEnumerable<T> sprites) where T : pDrawable
        {
            foreach (T d in sprites)
                Add(d);
        }

        internal void Clear(bool dispose = true)
        {
            resetHoverSprite();

            lock (SpriteLock)
            {
                if (dispose)
                    for (int index = 0; index < SpriteList.Count; index++)
                        SpriteList[index].Dispose();
                SpriteList.Clear();

                if (dispose)
                    foreach (pSprite p in forwardPlayQueue)
                        p.Dispose();
                forwardPlayQueue.Clear();

                if (dispose)
                    for (int index = 0; index < forwardPlayList.Count; index++)
                        forwardPlayList[index].Dispose();
                forwardPlayList.Clear();

                if (dispose)
                    while (forwardPlayPast.Count > 0)
                        forwardPlayPast.Pop().Dispose();
                forwardPlayPast.Clear();
            }
        }

        internal List<pDrawable> GetTagged(Object tag)
        {
            lock (SpriteLock)
                return
                    SpriteList.FindAll(p => (p.Tag == null ? false : p.Tag.Equals(tag)));
        }

        internal void RemoveTagged(Object tag)
        {
            lock (SpriteLock)
            {
                List<pDrawable> matches = SpriteList.FindAll(p => (p.Tag == null ? false : p.Tag.Equals(tag)));
                matches.ForEach(s => s.Dispose());
                RemoveRange(matches);
            }
        }

        internal void Remove(pDrawable p)
        {
            if (p != null)
            {
                lock (SpriteLock)
                    SpriteList.Remove(p);

                if (p.IsDisposable) p.Dispose();

                p.IsVisible = false;
            }
        }

        internal void RemoveRange<T>(List<T> l) where T : pDrawable
        {
            if (l == null) return;

            if (l.Count > 50)
            {
                int first = SpriteList.BinarySearch(l[0], pDrawableDepthComparer);

                if (first >= 0)
                {
                    int last = first + l.Count - 1;

                    if (last < SpriteList.Count && SpriteList[last] == l[l.Count - 1])
                    {
                        for (int i = first; i < last; i++)
                        {
                            if (SpriteList[i].IsDisposable)
                                SpriteList[i].Dispose();
                        }
                        SpriteList.RemoveRange(first, l.Count);

                        return;
                    }
                }
            }

            int count = l.Count;
            lock (SpriteLock)
                for (int i = 0; i < count; i++)
                    Remove(l[i]);
        }

        /// <summary>
        /// Handle input for all sprites contained in this handler.
        /// </summary>
        /// <returns>true if input was handled</returns>
        internal bool CheckInput(bool highPass, bool alreadyHandled)
        {
            if (!HandleInput) return false;

            if (highPass != HandleOverlayInput) return false;

            if (Alpha < 0.01f || Bypass) return false;

            pSprite lastHoverSprite = CurrentHoverSprite;

            resetHoverSprite();

            lock (SpriteList)
            {
                for (int i = 0; i < SpriteList.Count; i++)
                {
                    pDrawable s = SpriteList[i];

                    if (s.bypass) continue;

                    if (!alreadyHandled)
                        CheckHover(s);
                    else
                        s.Hovering = false;
                }
            }

            if (CurrentHoverSprite != null)
            {
                GameBase.toolTipping = !string.IsNullOrEmpty(CurrentHoverSprite.ToolTip) && (MouseManager.showGame || MouseManager.showMouse);
                if (GameBase.toolTipping && GameBase.toolTip.Text != CurrentHoverSprite.ToolTip)
                {
                    GameBase.toolTip.TextBounds = Vector2.Zero;
                    GameBase.toolTip.Text = CurrentHoverSprite.ToolTip;
                    if (GameBase.toolTip.MeasureText().X > 400)
                    {
                        GameBase.toolTip.TextBounds = new Vector2(400, 0);
                        GameBase.toolTip.TextChanged = true;
                    }
                }

                if (!CurrentHoverSprite.Hovering)
                    CurrentHoverSprite.Hovering = true;
            }

            if (lastHoverSprite != null && lastHoverSprite != CurrentHoverSprite)
                lastHoverSprite.Hovering = false;

            return CurrentHoverSprite != null;
        }


        /// <summary>
        ///   Draws this instance.
        /// </summary>
        internal bool Draw()
        {
            lock (SpriteLock)
            {
                if (FirstDraw)
                    HandleFirstDraw();

                int spriteListCount = SpriteList.Count;
                PixelsDrawn = 0;

                //Setup view rectangle
                if (viewRectangleCustom)
                {
                    ViewRectangleScaled =
                        new RectangleF(
                            (float)Math.Floor((WidescreenAutoOffset ? GameBase.WindowOffsetX : 0) + ViewRectangle.X * GameBase.WindowRatio),
                            (float)Math.Floor(ViewRectangle.Y * GameBase.WindowRatio + GameBase.WindowOffsetY),
                            (float)Math.Ceiling(ViewRectangle.Width * GameBase.WindowRatio),
                            (float)Math.Ceiling(ViewRectangle.Height * GameBase.WindowRatio));
                }
                else if (WidescreenAutoOffset)
                {
                    ViewRectangleScaled = new RectangleF(
                        GameBase.WindowOffsetX + ViewRectangle43.X * GameBase.WindowRatio,
                        ViewRectangle43.Y * GameBase.WindowRatio + GameBase.WindowOffsetY,
                        ViewRectangle43.Width * GameBase.WindowRatio, ViewRectangle43.Height * GameBase.WindowRatio);
                }
                else
                {
                    ViewRectangleScaled = new RectangleF(ViewRectangleCurrent.X * GameBase.WindowRatio,
                                                         ViewRectangleCurrent.Y * GameBase.WindowRatio +
                                                         GameBase.WindowOffsetY,
                                                         ViewRectangleCurrent.Width * GameBase.WindowRatio,
                                                         ViewRectangleCurrent.Height * GameBase.WindowRatio);
                }

                ClipRectangle = new Rectangle((int)ViewRectangleScaled.X, (int)ViewRectangleScaled.Y, (int)Math.Ceiling(ViewRectangleScaled.Width), (int)Math.Ceiling(ViewRectangleScaled.Height));

                if (spriteListCount == 0 && !ForwardPlayOptimisations) return false;

                if (Alpha < 0.01f || Bypass)
                {
                    for (int m = 0; m < spriteListCount; m++)
                        SpriteList[m].IsVisible = false;
                    return false;
                }

                LastDrawTime = GameBase.Time;

                if (BlacknessTarget >= 0)
                {
                    if (BlacknessTarget > Blackness)
                        Blackness = Math.Min(BlacknessTarget, Blackness + 0.018f * (float)GameBase.FrameRatio);
                    else if (BlacknessTarget < Blackness)
                        Blackness = Math.Max(BlacknessTarget, Blackness - 0.018f * (float)GameBase.FrameRatio);
                }

                int gameTime = GameBase.Time;
                int audioTime = AudioEngine.Time;

                if (ForwardPlayOptimisations)
                {
                    while (forwardPlayQueue.Count > 0)
                    {
                        pDrawable i = forwardPlayQueue.Peek();
                        if (i.Transformations.Count > 0 && i.Transformations[0].Time1 > (i.Clock == Clocks.Game ? gameTime : audioTime))
                            break;

                        if (i.Transformations.Count == 0)
                        {
                            forwardPlayQueue.Dequeue().Dispose();
                            continue;
                        }

                        Add(forwardPlayQueue.Dequeue());
                        spriteListCount++;
                    }

                    if (AudioEngine.SeekedBackwards)
                    {
                        //if we are seeking backwards, re-add any previousyl removed items.
                        while (forwardPlayPast.Count > 0)
                        {
                            pDrawable i = forwardPlayPast.Peek();
                            if (i.Transformations.Count > 0)
                            {
                                int lastTransform = i.Transformations[i.Transformations.Count - 1].Time2;
                                if (lastTransform < audioTime)
                                {
                                    if (audioTime - lastTransform > 5000)
                                        break; //allowance for out-of-order additions to this queue.
                                }
                            }

                            i = forwardPlayPast.Pop();

                            //if (i.Clock != Clocks.AudioOnce)
                            {
                                Add(i);
                                spriteListCount++;
                            }
                        }
                    }
                }

                int step = Math.Max(1, spriteListCount / 4096);

                startBatch();

#if !DEBUG
                try
                {
#endif
                    bool firstDraw = true;
                    
                    //Keep track of the OGL rectangle
                    Rectangle currentClipRectangle = Masking ? ClipRectangle : ViewRectangleFull;
                    //In most cases we won't be clipping more than at a spritemanager level
                    //so we can save on rectangle copies/ogl calls by resetting the initial scissor rectangle here
                    if (GameBase.D3D) GameBase.spriteBatch.GraphicsDevice.ScissorRectangle = currentClipRectangle;
                    if (GameBase.OGL) Gl.glScissor(currentClipRectangle.Left, GameBase.WindowHeight - currentClipRectangle.Bottom, currentClipRectangle.Width, currentClipRectangle.Height);

                    for (int m = 0; m < spriteListCount; m++)
                    {
                        pDrawable s = SpriteList[m];
                        if (s.bypass || (!s.IsVisible && (m + GameBase.Time) % step != 0))
                            continue;

#if DEBUG
                        TotalSprites++;
#endif

                        switch (s.Update())
                        {
                            case UpdateResult.Discard:
                                SpriteList.RemoveAt(m--);
                                spriteListCount--;

                                if (ForwardPlayOptimisations && s.Clock != Clocks.Game && AllowRewind)
                                    forwardPlayPast.Push(s);
                                else
                                    s.Dispose();
                                break;
                            case UpdateResult.NotVisible:
                                break;
                            case UpdateResult.Visible:
#if DEBUG
                                TotalSpritesVisible++;
#endif

                                if (!s.UsesSpriteBatch)
                                    endBatch();

                                if (GameBase.D3D)
                                {
                                    if (GameBase.spriteBatch.GraphicsDevice.ScissorRectangle != s.ClipRectangleScaled)
                                    {
                                        //Can't have more than one scissor rectangle per spritebatch so we restart
                                        //This is only used for per-sprite clipping
                                        if (s.UsesSpriteBatch && !firstDraw)
                                        {
                                            endBatch();
                                            startBatch();
                                        }
                                        GameBase.spriteBatch.GraphicsDevice.ScissorRectangle = s.ClipRectangleScaled;
                                    }
                                }
                                else if (currentClipRectangle != s.ClipRectangleScaled)
                                {
                                    currentClipRectangle = s.ClipRectangleScaled;
                                    Gl.glScissor(currentClipRectangle.Left, GameBase.WindowHeight - currentClipRectangle.Bottom, currentClipRectangle.Width, currentClipRectangle.Height);
                                }

                                s.Draw();
                                firstDraw = false;
                                break;
                        }
                    }

                    if (hasBegun)
                        //there's an occasional case where blending mode is transferred outside of SpriteBatches (DirectX only).
                        //this is a safety call to reset to alpha blending.
                        SetBlending(false);

                    endBatch(true);
#if !DEBUG

                    exceptionCount = 0;
                }
                catch (Exception e)
                {
                    /* TODO: Figure out why dialog popups are causing XNA exceptions
                     * See https://gist.github.com/324f5de475b438f48416. */

                    try
                    {
                        endBatch(true);
                    }
                    catch { }

                    //assume the spriteBatch is in a bad state here, so reinitialise it.
                    GameBase.RefreshSpriteBatch();
                    
                    if (!(e is InvalidOperationException) || ++exceptionCount > 1)
                        throw;
                }
#endif
            }


            return true;
        }

        /// <summary>
        ///   On the first draw, we want to move forward events that were initialized but not drawn on time due to loading time.
        ///   This only applies to sprites with transformations on the Game clock.
        /// </summary>
        internal void HandleFirstDraw()
        {
            if (!FirstDraw) return;

            if (GameBase.Time != CreationTime)
            {
                int diff = GameBase.Time - CreationTime;

                SpriteList.FindAll(s => s.Clock == Clocks.Game).ForEach(s => s.Transformations.ForEach(
                    t =>
                    {
                        if (t.Time1 - CreationTime > 5000)
                            return;

                        t.Time1 += diff;
                        t.Time2 += diff;
                    }));
            }

            FirstDraw = false;
        }

#if DEBUG
        internal static float PixelCount;
        internal static int TotalSprites, TotalSpritesVisible;
#endif

        internal int PixelsDrawn;

        internal void CheckHover<T>(T d) where T : pDrawable
        {
            if (d == null || !d.IsVisible || d.Bypass) return;

            pSprite s = d as pSprite;

            if (s == null) return;

            if (s.HandleInput)
            {
                if (MouseManager.MouseInWindow &&
                    (CurrentHoverSprite == null || s.HoverPriority < CurrentHoverSprite.HoverPriority) &&
                    s.CheckHover(MouseManager.MousePosition)
                    //TODO: this will fail for circular sprites inside a masked spriteManager. see what Masking adjustments do to CurrentPositionScaled in Draw()
                    && (!Masking || ViewRectangleScaled.Contains(MouseManager.MousePoint))
                    )
                {
                    if (CurrentHoverSprite != null)
                        CurrentHoverSprite.Hovering = false;

                    CurrentHoverSprite = s;
                }
                else if (s.Hovering)
                {
                    s.Hovering = false;
                }
            }
            else if (s.Hovering)
            {
                s.Hovering = false;
            }
        }

        internal static int GetSpriteTime(pSprite s)
        {
            return (s.Clock == Clocks.Game ? GameBase.Time : (int)AudioEngine.Time);
        }


        internal void AddRangeOrdered(List<pDrawable> collection)
        {
            if (collection == null || collection.Count == 0) return;

            if (SpriteList.Count == 0)
            {
                SpriteList.AddRange(collection);
                return;
            }

            int depthAfter = SpriteList.BinarySearch(collection[collection.Count - 1], pDrawableDepthComparer);
            if (depthAfter < 0)
            {
                depthAfter = ~depthAfter;
            }


            List<pDrawable> l = SpriteList.GetRange(depthAfter, SpriteList.Count - depthAfter);

            SpriteList.RemoveRange(depthAfter, SpriteList.Count - depthAfter);
            SpriteList.AddRange(collection);
            SpriteList.AddRange(l);
        }

        internal void SetVisibleArea(RectangleF rectangle)
        {
            viewRectangleCustom = true;
            Masking = true;
            ViewRectangle = rectangle;
        }

        internal bool AllowRewind;
        private SpriteBlendMode currentBlend;
        internal static void NewFrame()
        {
#if DEBUG
            PixelCount = 0;
            TotalSprites = 0;
            TotalSpritesVisible = 0;
#endif
        }

        /// <summary>
        /// Reset click handling, useful to allow rapid clicking of a sprite when holding mouse button down.
        /// </summary>
        internal void ResetClick()
        {
            GameBase.Scheduler.Add(delegate
            {
                ClickHandledSprite = null;
                clickHandledAtLeastOnce = false;
                globalClickHandledAtLeastOnce = false;
            }, true);
        }

        /// <summary>
        ///   Used by spinners.  Has a range of 0-0.2
        /// </summary>
        /// <param name = "number"></param>
        /// <returns></returns>
        internal static
            float drawOrderFwdLowPrio(float number)
        {
            return (number % 1999999) / 10000000;
        }

        /// <summary>
        ///   Used by hit values.  Has a range of 0.8-1 and loops every 10000 seconds (over 1 hour).
        /// </summary>
        /// <param name = "number"></param>
        /// <returns></returns>
        internal static
            float drawOrderFwdPrio(float number)
        {
            return 0.8F + (number % 6000000) / 30000000;
        }

        /// <summary>
        ///   Used by hitcircles.  Has a range of 0.8-0.2 and loops every 6000 seconds (1 hour).
        /// </summary>
        /// <param name = "number"></param>
        /// <returns></returns>
        internal static
            float drawOrderBwd(float number)
        {
            return 0.8F - (number % 6000000) / 10000000;
        }

        bool hasBegun;

        /// <summary>
        /// Set to true if this is an "overlay" level element. It will be considered for priority input no matter when it was created.
        /// It is placed on the same playing field as all other overlay level managers and handled in reverse creation order.
        /// </summary>
        internal bool HandleOverlayInput;

        internal void SetBlending(bool additive)
        {
            SpriteBlendMode thisBlend = additive ? SpriteBlendMode.Additive : SpriteBlendMode.AlphaBlend;

            if (currentBlend != thisBlend || !hasBegun)
            {
                currentBlend = thisBlend;
                startBatch(true);
            }
        }

        private void startBatch(bool allowRecycle = false)
        {
            if (hasBegun && !allowRecycle) return;

            endBatch();

            if (GameBase.D3D)
            {
                GameBase.spriteBatch.Begin(currentBlend, SpriteSortMode.Immediate, SaveStateMode.None);
                GameBase.spriteBatch.GraphicsDevice.RenderState.ScissorTestEnable = true;
                GameBase.spriteBatch.GraphicsDevice.RenderState.SeparateAlphaBlendEnabled = true;
                GameBase.spriteBatch.GraphicsDevice.RenderState.AlphaSourceBlend = Blend.One;
                GameBase.spriteBatch.GraphicsDevice.RenderState.AlphaDestinationBlend = Blend.InverseSourceAlpha;
            }

            if (GameBase.OGL)
            {
                Gl.glEnable(Gl.GL_SCISSOR_TEST);
                if (currentBlend == SpriteBlendMode.Additive)
                    GlControl.SetBlend(Gl.GL_SRC_ALPHA, Gl.GL_ONE);
                else
                    GlControl.SetBlend(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA);
            }

            hasBegun = true;
            Current = this;
            NativeText.ScaleModifier = Scale;

        }

        private void endBatch(bool final = false)
        {
            if (final) Current = null;

            if (!hasBegun) return;

            hasBegun = false;
            NativeText.ScaleModifier = 1;

            if (GameBase.D3D)
            {
                GameBase.spriteBatch.End();
                GameBase.spriteBatch.GraphicsDevice.RenderState.ScissorTestEnable = false;
            }

            if (GameBase.OGL)
                Gl.glDisable(Gl.GL_SCISSOR_TEST);
        }

        /// <summary>
        /// Process SpriteManagers in correct order to allow for input handling to happen.
        /// </summary>
        internal static SpriteManager CheckInputGlobal()
        {
            SpriteManager handled = null;

            //high "overlay" pass
            for (int i = SpriteManagersCreationOrder.Count - 1; i >= 0; i--)
            {
                SpriteManager s = SpriteManagersCreationOrder[i];
                if (s.CheckInput(true, handled != null))
                    handled = s;
            }

            //low pass
            for (int i = SpriteManagersCreationOrder.Count - 1; i >= 0; i--)
            {
                SpriteManager s = SpriteManagersCreationOrder[i];
                if (s.CheckInput(false, handled != null))
                    handled = s;
            }

            return handled;
        }

        internal static void HandleClickUp()
        {
            SpriteManagersCreationOrder.ForEach(s => s.ResetClick());
            globalClickHandledAtLeastOnce = false;
        }

        static bool globalClickHandledAtLeastOnce;
        internal static void HandleClick(bool repeat)
        {
            if (repeat && globalClickHandledAtLeastOnce)
                return;

            globalClickHandledAtLeastOnce = true;

            SpriteManager s = CheckInputGlobal();
            if (s != null) s.handleClick(repeat);
        }

        bool clickHandledAtLeastOnce;
        private void handleClick(bool repeat)
        {
            if (Alpha == 0 || LastDrawTime < (GameBase.Time - GameBase.ElapsedMilliseconds * 2))
            {
                clickHandledAtLeastOnce = true;
                return;
            }

            //Force update of currently hovered sprite.
            pSprite lastHoverSprite = CurrentHoverSprite;
            resetHoverSprite();
            int count = SpriteList.Count;

            if (repeat && clickHandledAtLeastOnce) return;

            for (int i = 0; i < count; i++)
                CheckHover(SpriteList[i]);

            clickHandledAtLeastOnce = true;

            if (lastHoverSprite != null && lastHoverSprite != CurrentHoverSprite)
                lastHoverSprite.Hovering = false;

            if (CurrentHoverSprite == null) return;

            if (!HandleInput || ClickHandledSprite != null || !CurrentHoverSprite.HandleInput || !CurrentHoverSprite.IsVisible || MouseManager.MouseMiddle == ButtonState.Pressed)
                return;

            if (CurrentHoverSprite.Click(false))
                InputManager.LastClickHandled = true;

            ClickHandledSprite = CurrentHoverSprite;
        }

    }

    internal enum Clocks
    {
        /// <summary>
        /// A simple clock type which is based off the game's internal clock.
        /// Sprites of this clock type will be thrown out and recycled after their transformations are exhausted unless set to AlwaysDraw.
        /// </summary>
        Game,
        /// <summary>
        /// A simple clock type which is bound to the active audio track.
        /// Sprites of this clocktype will be kept in history (to allow for seeking) unless ForwardPlayOptimisations is in use.
        /// </summary>
        Audio,
        /// <summary>
        /// When rewinding, sprites using AudioOnce will be removed completely after they have been "unplayed".
        /// This allows for some extra effects (like hit burst particles, kiai flashes etc.) which are generated on-the-fly
        /// to still play in rewind, but not duplicate when they are next generated after playing in a forward direction.
        /// </summary>
        AudioOnce
    } ;

    internal enum Fields
    {
        /// <summary>
        ///   The gamefield resolution (512x384).  Used for hitobjects and anything which needs to align with gameplay elements.
        ///   This is scaled in proportion to the native resolution and aligned accordingly.
        /// </summary>
        Gamefield = 1,
        GamefieldWide = 2,

        /// <summary>
        ///   Gamefield "container" resolution.  This is where storyboard and background events sits, and is the same
        ///   scaling/positioning as Standard when in play mode.  It differs in editor design mode where this field
        ///   will be shrunk to allow for editing.
        /// </summary>
        Storyboard = 3,
        StoryboardCentre = 4,

        /// <summary>
        ///   Native screen resolution.
        /// </summary>
        Native = 5,

        TopLeft,
        TopCentre,
        TopRight,

        CentreLeft,
        Centre,
        CentreRight,

        BottomLeft,
        BottomCentre,
        BottomRight,

        StandardGamefieldScale,

        /// <summary>
        ///   Native screen resolution with 1024x768-native sprite scaling.
        /// </summary>
        NativeStandardScale,

        /// <summary>
        ///   Native screen resolution aligned from the right-hand side of the screen, where an X position of 0 is translated to Standard(WindowWidth).
        /// </summary>
        NativeRight,

        NativeBottomCentre,
    } ;

    internal enum Origins
    {
        TopLeft,
        Centre,
        CentreLeft,
        TopRight,
        BottomCentre,
        TopCentre,
        Custom,
        CentreRight,
        BottomLeft,
        BottomRight
    } ;
}