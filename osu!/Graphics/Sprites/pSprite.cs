using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.GameplayElements;
using osu.Graphics.OpenGl;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Helpers;
using osu_common;
using osu_common.Helpers;
using RectangleF = osu.Graphics.Primitives.RectangleF;
using System.Diagnostics;

namespace osu.Graphics.Sprites
{
    internal class pSprite : pDrawable
    {
        internal bool Additive;

        internal Color drawColour;
        internal Vector2 CurrentPositionActual
        {
            get
            {
                if (Field == Fields.TopRight)
                    return new Vector2(GameBase.WindowWidthScaled - Position.X, Position.Y);
                return Position;
            }
        }

        internal override bool UsesSpriteBatch
        {
            get
            {
                return true;
            }
        }

        internal int PixelsDrawn
        {
            get
            {
                // Not using GameBase.WindowDefaultWidthWidescreen because it isn't 16:9.
                float windowDefaultWidth16_9 = (GameBase.WindowDefaultHeight * 16) / 9f;

                // A sprite won't draw on more pixels than there is on the screen.
                int maxPixelsDrawn = (int)((windowDefaultWidth16_9 * GameBase.GamefieldSize)
                                     * (GameBase.WindowDefaultHeight * GameBase.GamefieldSize));

                return Math.Min((int)((drawRectangle.Width * drawRectangle.Height) / (GameBase.WindowRatio * GameBase.WindowRatio)), maxPixelsDrawn);
            }
        }

        /// <summary>
        /// NOTE: Scaled position of top-left of sprite for now. see SpriteManager.cs:1166 for more details.
        /// </summary>
        internal Vector2 drawPosition;

        private bool drawExact;

        internal bool DimImmune;

        internal int DrawHeight;
        internal int DrawLeft;
        internal int DrawTop;
        internal int DrawWidth;
        internal bool? ExactCoordinates;
        internal Fields Field;

        internal int Height;
        internal Transformation HoverEffect;
        internal List<Transformation> HoverEffects;

        internal List<Transformation> HoverLostEffects;
        internal float HoverPriority;

        internal Vector2 lastMeasure;
        internal pTexture localTexture;
        internal Vector2 OriginPosition;
        internal Origins Origin;
        internal RectangleF drawRectangle;
        internal virtual bool Reverse { get; set; }

        internal string ToolTip;
        internal bool TrackRotation;

        internal bool ViewOffsetImmune;
        internal float ViewOffsetImmunityPoint;
        internal int Width;
        internal event VoidDelegate OnUpdate;

        internal pSprite(string spriteName, Vector2 position, SkinSource source = SkinSource.All, Origins origin = Origins.Centre)
            : this(SkinManager.Load(spriteName, source), Fields.TopLeft, origin, Clocks.Game, position, 1, true, Color.White)
        {
        }

        internal pSprite(pTexture texture, Fields fieldType, Origins origin, Clocks clock, Vector2 position)
            : this(texture, fieldType, origin, clock, position, 1, false, Color.White)
        {
        }

        internal pSprite(pTexture texture, Origins origin, Vector2 startPosition, float drawDepth, bool alwaysDraw, Color colour)
            : this(texture, Fields.TopLeft, origin, Clocks.Game, startPosition, drawDepth, alwaysDraw, colour, null)
        {
        }

        internal pSprite(pTexture texture, Vector2 startPosition, float drawDepth, bool alwaysDraw, Color colour)
            : this(texture, Fields.TopLeft, Origins.TopLeft, Clocks.Game, startPosition, drawDepth, alwaysDraw, colour, null)
        {
        }

        internal pSprite(pTexture texture, Fields fieldType, Origins origin, Clocks clock, Vector2 startPosition, float drawDepth, bool alwaysDraw, Color colour, Object tag = null)
            : base(alwaysDraw)
        {
            localTexture = texture;

            Origin = origin;

            UpdateTextureSize();
            UpdateTextureAlignment();

            Clock = clock;
            Position = startPosition;
            InitialPosition = Position;

            Scale = 1;
            Depth = drawDepth;
            HoverPriority = -Depth;
            Field = fieldType;
            Alpha = colour.A / 255f;
            InitialColour = colour;

            Tag = tag;

#if DEBUG
            if (SpriteManager.Placement)
                HandleInput = true;
#endif
        }

        public override UpdateResult Update()
        {
            UpdateResult result = base.Update();
            if (result != UpdateResult.Visible)
                return result;

            Vector2 lastPositionScaled = drawPosition;

            result = UpdateTransformations();

            if (result != UpdateResult.Visible)
                return result;

            SpriteManager host = SpriteManager.Current;

            bool shouldDraw = Alpha > 0.001 && host.Alpha > 0;

            if (shouldDraw && Field == Fields.TopLeft && !ViewOffsetImmune)
            {
                float distanceAbove = (host.ViewOffset.Y - host.BaseOffset.Y + host.ScrollOffset.Y) - Position.Y;
                float distanceLeft = (host.ViewOffset.X - host.BaseOffset.X + host.ScrollOffset.X) - Position.X;
                float distanceBelow = Position.Y - (host.ViewOffset.Y - host.BaseOffset.Y + host.ScrollOffset.Y + host.ViewRectangle.Height / host.Scale);
                float distanceRight = Position.X - (host.ViewOffset.X - host.BaseOffset.X + host.ScrollOffset.X + host.ViewRectangle.Width / host.Scale);

                bool spriteWithinViewRectangleAtOrigin = distanceAbove <= 0 && distanceBelow <= 0 && distanceLeft <= 0 && distanceRight <= 0;

                if (!spriteWithinViewRectangleAtOrigin && Rotation == 0)
                {
                    /* We know:
                     *  - The sprite is outside the view rectangle.
                     *  - The sprite is not rotated.
                     * We can shortcut here by checking where the origin is and handling things accordingly...
                     */

                    float height = Math.Abs(DrawHeight * VectorScale.Y * Scale / 1.6f);
                    float width = Math.Abs(DrawWidth * VectorScale.X * Scale / 1.6f);

                    switch (Origin)
                    {
                        case Origins.TopLeft:
                            if (distanceBelow > 0 || distanceAbove > height || distanceRight > 0 || distanceLeft > width)
                                shouldDraw = false;
                            break;
                        case Origins.TopCentre:
                            if (distanceBelow > 0 || distanceAbove > height || distanceRight > width / 2 || distanceLeft > width / 2)
                                shouldDraw = false;
                            break;
                        case Origins.TopRight:
                            if (distanceBelow > 0 || distanceAbove > height || distanceRight > width || distanceLeft > 0)
                                shouldDraw = false;
                            break;
                        case Origins.CentreLeft:
                            if (distanceBelow > height / 2 || distanceAbove > height / 2 || distanceRight > 0 || distanceLeft > width)
                                shouldDraw = false;
                            break;
                        case Origins.Centre:
                            if (distanceBelow > height / 2 || distanceAbove > height / 2 || distanceRight > width / 2 || distanceLeft > width / 2)
                                shouldDraw = false;
                            break;
                        case Origins.CentreRight:
                            if (distanceBelow > height / 2 || distanceAbove > height / 2 || distanceRight > width || distanceLeft > 0)
                                shouldDraw = false;
                            break;
                        case Origins.BottomLeft:
                            if (distanceBelow > height || distanceAbove > 0 || distanceRight > 0 || distanceLeft > width)
                                shouldDraw = false;
                            break;
                        case Origins.BottomCentre:
                            if (distanceBelow > height || distanceAbove > 0 || distanceRight > width / 2 || distanceLeft > width / 2)
                                shouldDraw = false;
                            break;
                        case Origins.BottomRight:
                            if (distanceBelow > height || distanceAbove > 0 || distanceRight > width || distanceLeft > 0)
                                shouldDraw = false;
                            break;
                    }
                }
            }

            if (!shouldDraw && !host.CalculateInvisibleUpdates)
            {
                IsVisible = false;
                return UpdateResult.NotVisible;
            }

            drawPosition = Position;
            drawOrigin = OriginPosition;
            drawScale = Math.Max(Scale, 0);

            if (host.Masking && (!ViewOffsetImmune || ViewOffsetImmunityPoint > 0))
            {
                Vector2 adjustVector = host.ScrollOffset - host.BaseOffset;

                // Need to flip sign of ViewOffset depending on Field since position might be inverted later on
                switch (Field)
                {
                    default:
                        adjustVector += host.ViewOffset;
                        break;
                    case Fields.TopRight:
                    case Fields.CentreRight:
                    case Fields.NativeRight:
                        adjustVector += new Vector2(-host.ViewOffset.X, host.ViewOffset.Y);
                        break;

                    case Fields.BottomLeft:
                    case Fields.BottomCentre:
                    case Fields.NativeBottomCentre:
                        adjustVector += new Vector2(host.ViewOffset.X, -host.ViewOffset.Y);
                        break;

                    case Fields.BottomRight:
                        adjustVector -= host.ViewOffset;
                        break;
                }

                if (ViewOffsetImmunityPoint > 0)
                    adjustVector.Y = Math.Max(0, Math.Min(ViewOffsetImmunityPoint, adjustVector.Y));

                drawPosition -= adjustVector;
            }

            bool addViewRectangle = true;

            float areaWidth = host.Masking ? host.ViewRectangle.Width * GameBase.WindowRatio : GameBase.WindowWidth;
            float areaHeight = host.Masking ? host.ViewRectangle.Height * GameBase.WindowRatio : GameBase.WindowHeight;

            //apply scale
            switch (Field)
            {
                default:
                    if (ScaleToWindowRatio)
                        drawScale *= GameBase.WindowRatioInverse;
                    break;
                case Fields.Native:
                case Fields.NativeBottomCentre:
                case Fields.NativeRight:
                    break;
                case Fields.StandardGamefieldScale:
                    drawScale *= GameBase.WindowRatio * GameBase.GamefieldRatioWindowIndependent;
                    break;
                case Fields.Gamefield:
                case Fields.GamefieldWide:
                    drawScale *= host.GamefieldSpriteRatio;
                    break;
                case Fields.Storyboard:
                case Fields.StoryboardCentre:
                    drawScale *= GameBase.GamefieldRatio;
                    break;
            }

            //apply position
            switch (Field)
            {
                case Fields.TopLeft:
                    drawPosition = drawPosition * GameBase.WindowRatio;
                    break;
                case Fields.TopCentre:
                    drawPosition = drawPosition * GameBase.WindowRatio;
                    drawPosition.X += areaWidth / 2;
                    break;
                case Fields.TopRight:
                    drawPosition = drawPosition * GameBase.WindowRatio;
                    drawPosition.X = areaWidth - drawPosition.X;
                    break;
                case Fields.CentreLeft:
                    drawPosition.X = drawPosition.X * GameBase.WindowRatio;
                    drawPosition.Y = areaHeight / 2 + drawPosition.Y * GameBase.WindowRatio;
                    break;
                case Fields.Centre:
                    drawPosition.X = areaWidth / 2 + drawPosition.X * GameBase.WindowRatio;
                    drawPosition.Y = areaHeight / 2 + drawPosition.Y * GameBase.WindowRatio;
                    break;
                case Fields.CentreRight:
                    drawPosition.X = areaWidth - drawPosition.X * GameBase.WindowRatio;
                    drawPosition.Y = areaHeight / 2 + drawPosition.Y * GameBase.WindowRatio;
                    break;
                case Fields.BottomLeft:
                    drawPosition = drawPosition * GameBase.WindowRatio;
                    drawPosition.Y = areaHeight - drawPosition.Y;
                    break;
                case Fields.BottomCentre:
                    drawPosition = drawPosition * GameBase.WindowRatio;
                    drawPosition.X = areaWidth / 2 + drawPosition.X;
                    drawPosition.Y = areaHeight + GameBase.WindowOffsetY - drawPosition.Y;
                    break;
                case Fields.BottomRight:
                    drawPosition = drawPosition * GameBase.WindowRatio;
                    drawPosition.X = areaWidth - drawPosition.X;
                    drawPosition.Y = areaHeight - drawPosition.Y;
                    break;
                case Fields.NativeStandardScale:
                    addViewRectangle = false;
                    break;
                case Fields.NativeRight:
                    drawPosition.X = areaWidth - drawPosition.X;
                    addViewRectangle = false;
                    break;
                case Fields.NativeBottomCentre:
                    drawPosition.X = areaWidth / 2 + drawPosition.X;
                    drawPosition.Y = areaHeight + GameBase.WindowOffsetY - drawPosition.Y;
                    addViewRectangle = false;
                    break;
                case Fields.StandardGamefieldScale:
                    drawPosition = GameBase.WindowRatio * drawPosition;
                    break;
                case Fields.GamefieldWide:
                    GameBase.GamefieldToDisplayWidescreen(ref drawPosition);
                    addViewRectangle = false;
                    break;
                case Fields.Gamefield:
                    GameBase.GamefieldToDisplay(ref drawPosition);
                    addViewRectangle = false;
                    break;
                case Fields.Storyboard:
                    drawPosition = drawPosition * GameBase.WindowRatio * GameBase.GamefieldSize;
                    drawPosition.X += (areaWidth + (!host.WidescreenAutoOffset ? GameBase.WindowOffsetX * 2 : 0)) * ((1 - GameBase.GamefieldSize) * 0.5f);
                    drawPosition.Y += (areaHeight + GameBase.WindowOffsetY) * ((1 - GameBase.GamefieldSize) * 0.75f);
                    break;
                case Fields.StoryboardCentre:
                    drawPosition += new Vector2((host.WidescreenAutoOffset ? GameBase.WindowDefaultWidth : GameBase.WindowWidthScaled) / 2, GameBase.WindowDefaultHeight / 2);
                    drawPosition = drawPosition * GameBase.WindowRatio * GameBase.GamefieldSize;
                    drawPosition.X += (areaWidth + (!host.WidescreenAutoOffset ? GameBase.WindowOffsetX * 2 : 0)) * ((1 - GameBase.GamefieldSize) * 0.5f);
                    drawPosition.Y += (areaHeight + GameBase.WindowOffsetY) * ((1 - GameBase.GamefieldSize) * 0.75f);
                    break;
                default:
                    addViewRectangle = false;
                    break;
            }

            //Late updating of colour to save on constructors
            byte alpha = (byte)(Alpha * host.Alpha * 255);
            byte colourAlpha = InitialColour.A;
            if (colourAlpha > 0 && colourAlpha < 255)
                alpha = (byte)(alpha * (colourAlpha / 255f));

            if (host.Blackness == 0)
                drawColour = new Color(InitialColour.R, InitialColour.G, InitialColour.B, alpha);
            else
                drawColour =
                    new Color((byte)Math.Max(0, Math.Min(255, ((255 * host.BlacknessShade) * host.Blackness + InitialColour.R * (1 - host.Blackness)))),
                              (byte)Math.Max(0, Math.Min(255, ((255 * host.BlacknessShade) * host.Blackness + InitialColour.G * (1 - host.Blackness)))),
                              (byte)Math.Max(0, Math.Min(255, ((255 * host.BlacknessShade) * host.Blackness + InitialColour.B * (1 - host.Blackness)))), alpha);

            drawScaleVector = VectorScale * drawScale;

            if (host.Scale != 1 && !ViewOffsetImmune)
            {
                if (!(this is pText)) drawScaleVector *= host.Scale;
                drawPosition *= host.Scale;
            }

            if (addViewRectangle)
            {
                drawPosition.X += host.ViewRectangleScaled.X;
                drawPosition.Y += host.ViewRectangleScaled.Y;
            }

            drawExact = false;
            if (ExactCoordinates.HasValue)
                drawExact = ExactCoordinates.Value;
            else
            {
                //swith to using exact when sprites are displayed at their original scale and are not moving.
                if (drawScaleVector.X == 1 && drawScaleVector.Y == 1 && Vector2.DistanceSquared(lastPositionScaled, drawPosition) < float.Epsilon)
                    drawExact = true;
            }

            drawOriginScaled = new Vector2(drawOrigin.X * Math.Abs(drawScaleVector.X), drawOrigin.Y * Math.Abs(drawScaleVector.Y));

            Vector2 finalPositionScaled = drawPosition - drawOriginScaled;

            if (drawExact)
            {
                finalPositionScaled.X = (float)Math.Round(finalPositionScaled.X);
                finalPositionScaled.Y = (float)Math.Round(finalPositionScaled.Y);
            }

            //Scaled position of top-left of sprite for now.
            //This gets changed to the origin position before draw below (in draw call)...
            //todo: this doesn't reflect the displayRectangle
            drawRectangle = new RectangleF(
                finalPositionScaled.X,
                finalPositionScaled.Y,
                DrawWidth * Math.Abs(drawScaleVector.X),
                DrawHeight * Math.Abs(drawScaleVector.Y));

            //todo: check whether we need to reimplement this
            if (host.Masking &&
                Rotation == 0 && //hack for rotated things
                Field != Fields.NativeStandardScale && //hack for mouse cursor
                !(Field == Fields.TopRight && host.WidescreenAutoOffset) && //hack for "show chat button"
                !RectangleF.RectCollide(Rotation, drawRectangle, host.ViewRectangleScaled, drawOriginScaled)
                //Both of the above hacks could be avoided if spriteManagerOverlay is reworked.
                //These sprites are in there due to depth ordering, but should be in a widescreen-supporting spriteManager.
                )
            {
                IsVisible = false;
                if (Hovering) Hovering = false;
                return UpdateResult.NotVisible;
            }

            drawRectangleSource = new Rectangle(
                DrawLeft,
                DrawTop,
                Math.Min(Width, Math.Max(0, DrawWidth)),
                Math.Min(Height, Math.Max(0, DrawHeight)));

            if (OnUpdate != null) OnUpdate();

            // At this point the sprite will only be invisible if its host calculates invisible updates
            IsVisible = shouldDraw;
            if (!IsVisible)
                return UpdateResult.NotVisible;

            return UpdateResult.Visible;
        }

        private UpdateResult UpdateTransformations(bool updateLoops = true)
        {
            int transformationCount = Transformations.Count;

            if (transformationCount == 0)
                return AlwaysDraw ? UpdateResult.Visible : UpdateResult.Discard;

            bool shouldDraw = AlwaysDraw;

            int time = getClockTime();

            bool hasFuture = false;
            bool hasPast = false;

            bool? horizontalFlip = null;
            bool? verticalFlip = null;
            bool? additive = null;

            bool hasRotation = false;
            bool hasScale = false;
            bool hasFade = false;
            bool hasMovement = false, hasMovementX = false, hasMovementY = false;
            bool hasColour = false;
            bool hasUpdateableLoops = false;

            bool hasClippingWidth = false;
            bool hasClippingHeight = false;

            bool hasClipRectangle = false;

            // First check for active transformations
            foreach (Transformation t in Transformations)
            {
                if (t.Time1 >= time || t.Time2 > time)
                {
                    hasFuture = true;

                    if (t.Loop && t.CurrentLoopCount > 0 && !SpriteManager.Current.ForwardPlayOptimisations)
                        hasUpdateableLoops = true;
                    if (t.Time1 > time)
                        continue;
                }

                if (t.Time2 <= time)
                {
                    hasPast = true;

                    if (t.Loop && (t.MaxLoopCount == 0 || t.CurrentLoopCount < t.MaxLoopCount - 1))
                        hasUpdateableLoops = true;
                    if (t.Time2 < time)
                        continue;
                }

                shouldDraw = true;

                switch (t.Type)
                {
                    case TransformationType.Fade:
                        if (hasFade) break;
                        Alpha = OsuMathHelper.TweenValues(t.StartFloat, t.EndFloat, time, t.Time1, t.Time2, t.Easing);
                        hasFade = true;
                        break;
                    case TransformationType.Movement:
                        if (hasMovement) break;
                        Position = OsuMathHelper.TweenValues(t.StartVector, t.EndVector, time, t.Time1, t.Time2, t.Easing);
                        if (TrackRotation)
                        {
                            Vector2 sub = t.EndVector - t.StartVector;
                            Rotation = (float)Math.Atan2(sub.Y, sub.X);
                            if (Reverse)
                                Rotation -= (float)Math.PI;
                        }
                        hasMovement = true;
                        break;
                    case TransformationType.MovementX:
                        if (hasMovementX) break;
                        Position.X = OsuMathHelper.TweenValues(t.StartFloat, t.EndFloat, time, t.Time1, t.Time2, t.Easing);
                        hasMovementX = true;
                        break;
                    case TransformationType.MovementY:
                        if (hasMovementY) break;
                        Position.Y = OsuMathHelper.TweenValues(t.StartFloat, t.EndFloat, time, t.Time1, t.Time2, t.Easing);
                        hasMovementY = true;
                        break;
                    case TransformationType.Scale:
                        if (hasScale) break;
                        Scale = OsuMathHelper.TweenValues(t.StartFloat, t.EndFloat, time, t.Time1, t.Time2, t.Easing);
                        hasScale = true;
                        break;
                    case TransformationType.VectorScale:
                        if (hasScale) break;
                        VectorScale = OsuMathHelper.TweenValues(t.StartVector, t.EndVector, time, t.Time1, t.Time2, t.Easing);
                        hasScale = true;
                        break;
                    case TransformationType.Rotation:
                        if (hasRotation) break;
                        Rotation = OsuMathHelper.TweenValues(t.StartFloat, t.EndFloat, time, t.Time1, t.Time2, t.Easing);
                        hasRotation = true;
                        break;
                    case TransformationType.Colour:
                        if (hasColour) break;
                        InitialColour = OsuMathHelper.TweenValues(t.StartColour, t.EndColour, time, t.Time1, t.Time2, t.Easing);
                        hasColour = true;
                        break;
                    case TransformationType.ParameterFlipHorizontal:
                        horizontalFlip = FlipHorizontal = true;
                        break;
                    case TransformationType.ParameterFlipVertical:
                        verticalFlip = FlipVertical = true;
                        break;
                    case TransformationType.ParameterAdditive:
                        additive = Additive = true;
                        break;
                    case TransformationType.ClippingWidth:
                        if (hasClippingWidth) break;
                        DrawWidth = (int)OsuMathHelper.TweenValues(t.StartFloat, t.EndFloat, time, t.Time1, t.Time2, t.Easing);
                        hasClippingWidth = true;
                        break;
                    case TransformationType.ClippingHeight:
                        if (hasClippingHeight) break;
                        DrawHeight = (int)OsuMathHelper.TweenValues(t.StartFloat, t.EndFloat, time, t.Time1, t.Time2, t.Easing);
                        hasClippingHeight = true;
                        break;
                    case TransformationType.ClipRectangle:
                        if (hasClipRectangle) break;
                        ClipRectangle = OsuMathHelper.TweenValues(t.StartRectangle, t.EndRectangle, time, t.Time1, t.Time2, t.Easing);
                        hasClipRectangle = true;
                        break;
                }
            }

            // Update loopable transformations before going farther, as transformations may need to be updated again if any is moved.
            if (hasUpdateableLoops && updateLoops)
            {
                // Don't update endless loops if they would be the only thing keeping the sprite visible.
                bool updateEndlessLoops = AlwaysDraw || hasFuture;

                bool transformationsChanged = false;
                bool keepSorted = true;

                // Iterate backwards so that moved loops won't get processed twice, as long as time is moving forwards.
                for (int index = transformationCount - 1; index >= 0; index--)
                {
                    Transformation tr = Transformations[index];

                    if (!tr.Loop)
                        continue;

                    if (tr.MaxLoopCount == 0 && !updateEndlessLoops)
                        continue;

                    int loopsToDo = 0;
                    int transformationDuration = tr.Duration;
                    int loopDuration = transformationDuration + tr.LoopDelay;

                    if (tr.Time2 <= time)
                    {
                        int remainingLoops = tr.MaxLoopCount - tr.CurrentLoopCount - 1;
                        if (tr.MaxLoopCount > 0 && remainingLoops < 1)
                            continue;

                        // Move the loop forwards
                        loopsToDo = (time - tr.Time1) / loopDuration;
                        if (tr.MaxLoopCount > 0)
                            loopsToDo = Math.Min(loopsToDo, remainingLoops);
                    }
                    else if (!SpriteManager.Current.ForwardPlayOptimisations && tr.Time1 > time)
                    {
                        int rewindableLoops = tr.CurrentLoopCount;
                        if (rewindableLoops < 1)
                            continue;

                        // Rewind the loop
                        loopsToDo = (time - tr.Time2 - tr.LoopDelay) / loopDuration;
                        loopsToDo = Math.Max(-rewindableLoops, loopsToDo);
                    }

                    if (loopsToDo != 0)
                    {
                        if (!transformationsChanged)
                            keepSorted = ListHelper.IsSorted(Transformations);

                        tr.Time1 = tr.Time1 + loopDuration * loopsToDo;
                        tr.Time2 = tr.Time1 + transformationDuration;
                        tr.CurrentLoopCount += loopsToDo;

                        if (keepSorted)
                        {
                            // Keep transformations sorted if they are, this is faster than sorting them all at the end.
                            Transformations.RemoveAt(index);
                            int insertedIndex = Transformations.AddInPlace(tr);

                            if (insertedIndex < index)
                                index++;
                        }

                        transformationsChanged = true;
                    }
                }

                if (transformationsChanged)
                {
                    // Transformations need to be updated again, loops won't need to be updated as the current time stays the same.
                    if (!keepSorted)
                        Transformations.Sort();
                    return UpdateTransformations(false);
                }
            }

            //dispose of past sprites
            if (!hasFuture && !shouldDraw && (Clock == Clocks.Game || SpriteManager.Current.ForwardPlayOptimisations))
            {
                IsVisible = false;
                return UpdateResult.Discard;
            }

            if (!(hasFuture && hasPast) && !shouldDraw)
            {
                //not a current sprite. will not be drawn.
                IsVisible = false;
                if (Clock == Clocks.AudioOnce)
                    return UpdateResult.Discard;

                return UpdateResult.NotVisible;
            }

            //apply past transformations
            for (int i = transformationCount - 1; i >= 0; i--)
            {
                Transformation t = Transformations[i];
                if (t.Time2 >= time) continue;

                shouldDraw = true;

                switch (t.Type)
                {
                    case TransformationType.Fade:
                        if (hasFade) break;
                        Alpha = t.EndFloat;
                        hasFade = true;
                        break;
                    case TransformationType.Movement:
                        if (hasMovement || hasMovementX || hasMovementY) break;
                        Position = t.EndVector;
                        hasMovement = true;
                        break;
                    case TransformationType.MovementX:
                        if (hasMovementX) break;
                        Position.X = t.EndFloat;
                        hasMovementX = true;
                        break;
                    case TransformationType.MovementY:
                        if (hasMovementY) break;
                        Position.Y = t.EndFloat;
                        hasMovementY = true;
                        break;
                    case TransformationType.Scale:
                        if (hasScale) break;
                        Scale = t.EndFloat;
                        hasScale = true;
                        break;
                    case TransformationType.VectorScale:
                        if (hasScale) break;
                        VectorScale = t.EndVector;
                        hasScale = true;
                        break;
                    case TransformationType.Rotation:
                        if (hasRotation) break;
                        Rotation = t.EndFloat;
                        hasRotation = true;
                        break;
                    case TransformationType.Colour:
                        if (hasColour) break;
                        InitialColour = t.EndColour;
                        hasColour = true;
                        break;
                    case TransformationType.ParameterAdditive:
                        if (!additive.HasValue)
                        {
                            bool isPermanent = t.Duration == 0;
                            additive = Additive = isPermanent;
                        }
                        break;
                    case TransformationType.ParameterFlipHorizontal:
                        if (!horizontalFlip.HasValue)
                        {
                            bool isPermanent = t.Duration == 0;
                            horizontalFlip = FlipHorizontal = isPermanent;
                        }
                        break;
                    case TransformationType.ParameterFlipVertical:
                        if (!verticalFlip.HasValue)
                        {
                            bool isPermanent = t.Duration == 0;
                            verticalFlip = FlipVertical = isPermanent;
                        }
                        break;
                    case TransformationType.ClippingWidth:
                        if (hasClippingWidth) break;
                        DrawWidth = (int)t.EndFloat;
                        hasClippingWidth = true;
                        break;
                    case TransformationType.ClippingHeight:
                        if (hasClippingHeight) break;
                        DrawHeight = (int)t.EndFloat;
                        hasClippingHeight = true;
                        break;
                    case TransformationType.ClipRectangle:
                        if (hasClipRectangle) break;
                        ClipRectangle = t.EndRectangle;
                        hasClipRectangle = true;
                        break;
                }

                //remove any old past transformations for long-life sprites
                if (Clock == Clocks.Game && !t.Loop && AlwaysDraw)
                {
                    Transformations.RemoveAt(i);
                    transformationCount--;
                }
            }

            //apply future transformations as a last resort.
            if (hasFuture && Clock != Clocks.Game)
            {
                for (int i = 0; i < transformationCount; i++)
                {
                    Transformation t = Transformations[i];
                    if (t.Time1 < time) continue;

                    switch (t.Type)
                    {
                        case TransformationType.Fade:
                            if (hasFade) break;
                            Alpha = t.StartFloat;
                            hasFade = true;
                            break;
                        case TransformationType.Movement:
                            if (hasMovement || hasMovementX || hasMovementY) break;
                            Position = t.StartVector;
                            hasMovement = true;
                            break;
                        case TransformationType.MovementX:
                            if (hasMovementX) break;
                            hasMovementX = true;
                            Position.X = t.StartFloat;
                            break;
                        case TransformationType.MovementY:
                            if (hasMovementY) break;
                            hasMovementY = true;
                            Position.Y = t.StartFloat;
                            break;
                        case TransformationType.Scale:
                            if (hasScale) break;
                            Scale = t.StartFloat;
                            hasScale = true;
                            break;
                        case TransformationType.VectorScale:
                            if (hasScale) break;
                            VectorScale = t.StartVector;
                            hasScale = true;
                            break;
                        case TransformationType.Rotation:
                            if (hasRotation) break;
                            Rotation = t.StartFloat;
                            hasRotation = true;
                            break;
                        case TransformationType.Colour:
                            if (hasColour) break;
                            InitialColour = t.StartColour;
                            hasColour = true;
                            break;
                    }
                }
            }

            return shouldDraw ? UpdateResult.Visible : UpdateResult.NotVisible;
        }

        public override bool Draw()
        {
            if (!base.Draw()) return false;

            SpriteManager.Current.SetBlending(Additive);

            pTexture texture = Texture;
            if (texture == null || texture.IsDisposed)
                return true;

            if (texture.DpiScale != 1)
            {
                drawScaleVector *= 1f / texture.DpiScale;
                drawRectangleSource.Width *= texture.DpiScale;
                drawRectangleSource.Height *= texture.DpiScale;
                drawRectangleSource.X *= texture.DpiScale;
                drawRectangleSource.Y *= texture.DpiScale;
                drawOrigin *= texture.DpiScale;
            }

            Vector2 d = new Vector2(drawRectangle.X + drawOriginScaled.X, drawRectangle.Y + drawOriginScaled.Y);

            if (GameBase.D3D && texture.TextureXna != null && !texture.TextureXna.IsDisposed)
            {
                SpriteEffects drawEffect = SpriteEffects.None;
                if (drawScaleVector.X < 0) drawEffect |= SpriteEffects.FlipHorizontally;
                if (drawScaleVector.Y < 0) drawEffect |= SpriteEffects.FlipVertically;

                drawScaleVector.X = Math.Abs(drawScaleVector.X);
                drawScaleVector.Y = Math.Abs(drawScaleVector.Y);

                GameBase.spriteBatch.Draw(texture.TextureXna, d, drawRectangleSource, drawColour, Rotation, drawOrigin, drawScaleVector, drawEffect, 0);
            }

            if (GameBase.OGL && texture.TextureGl != null)
                texture.TextureGl.Draw(d, drawOrigin, drawColour, drawScaleVector, Rotation, drawRectangleSource);

            if (SpriteManager.Current.EnableProfiling)
                SpriteManager.Current.PixelsDrawn += PixelsDrawn;

#if DEBUG
            SpriteManager.PixelCount += Math.Abs((drawRectangleSource.Width * drawScaleVector.X) * (drawRectangleSource.Height * drawScaleVector.Y) / GameBase.WindowRatioInverse);
#endif

            return true;
        }

        internal void DrawBorder(Color colour, int minimalSize = 0)
        {
            Vector2 topLeft, topRight, bottomLeft, bottomRight;
            getHoverArea(minimalSize).GetRotatedCorners(drawPosition, Rotation, out topLeft, out topRight, out bottomLeft, out bottomRight);

            GameBase.primitiveBatch.Begin();
            GameBase.primitiveBatch.AddVertex(topLeft, colour);
            GameBase.primitiveBatch.AddVertex(bottomLeft, colour);
            GameBase.primitiveBatch.AddVertex(bottomLeft, colour);
            GameBase.primitiveBatch.AddVertex(bottomRight, colour);
            GameBase.primitiveBatch.AddVertex(bottomRight, colour);
            GameBase.primitiveBatch.AddVertex(topRight, colour);
            GameBase.primitiveBatch.AddVertex(topRight, colour);
            GameBase.primitiveBatch.AddVertex(topLeft, colour);
            GameBase.primitiveBatch.End();
        }

        public override string ToString()
        {
            if (localTexture != null)
                return localTexture.assetName + " @" + Position.X + "," + Position.Y + ") a:" + Math.Round(Alpha, 2) + " depth:" + Depth;
            return base.ToString();
        }

        internal virtual pTexture Texture
        {
            get { return localTexture; }
            set
            {
                if (value == localTexture)
                    return;

                if (localTexture != null && IsDisposable)
                    localTexture.Dispose();
                localTexture = value;
                UpdateTextureSize();
                UpdateTextureAlignment();
            }
        }

        internal bool IsFullyVisible
        {
            get { return drawColour.A == InitialColour.A; }
        }

        internal override bool Hovering
        {
            get
            {
                return base.Hovering;
            }
            set
            {
                if (value == hovering) return;

                base.Hovering = value;

                if (hovering)
                {
                    HoverStart();
                    if (OnHover != null)
                        OnHover(this, null);
                }
                else if (!hovering)
                {
                    HoverEnd();
                    if (OnHoverLost != null)
                        OnHoverLost(this, null);
                }
            }
        }



        private void HoverStart()
        {
            int time = SpriteManager.GetSpriteTime(this);
            if (HoverEffect != null)
            {
                lock (SpriteManager.SpriteLock)
                    Transformations.RemoveAll(t => t.TagNumeric == 1);

                Transformation h = HoverEffect.Clone();
                h.StartVector = Position;
                switch (HoverEffect.Type)
                {
                    case TransformationType.Fade:
                        h.StartFloat = (float)Alpha;
                        break;
                    case TransformationType.Scale:
                        h.StartFloat = Scale;
                        break;
                    case TransformationType.Rotation:
                        h.StartFloat = Rotation;
                        break;
                }
                h.StartColour = InitialColour;


                int duration = HoverEffect.Duration;
                h.Time1 = time;
                h.Time2 = time + duration;

                h.TagNumeric = 1;

                Transformations.Add(h);
            }
            if (HoverEffects != null)
            {
                lock (SpriteManager.SpriteLock)
                    Transformations.RemoveAll(t => t.TagNumeric == 1);
                bool first = true;
                foreach (Transformation t in HoverEffects)
                {
                    Transformation h = t.Clone();
                    if (first)
                    {
                        Transformations.RemoveAll(tr => tr.Type == h.Type);
                        h.StartVector = Position;
                        switch (h.Type)
                        {
                            case TransformationType.Fade:
                                h.StartFloat = (float)Alpha;
                                break;
                            case TransformationType.Scale:
                                h.StartFloat = Scale;
                                break;
                            case TransformationType.Rotation:
                                h.StartFloat = Rotation;
                                break;
                        }
                        h.StartColour = InitialColour;
                        first = false;
                    }
                    h.Time1 += time;
                    h.Time2 += time;

                    h.TagNumeric = 1;

                    Transformations.Add(h);
                }
            }
        }

        private void HoverEnd()
        {
            int time = SpriteManager.GetSpriteTime(this);
            if (HoverEffect != null)
            {
                lock (SpriteManager.SpriteLock)
                    Transformations.RemoveAll(t => t.TagNumeric == 1);

                Transformation h = HoverEffect.CloneReverse();

                h.StartVector = Position;

                switch (HoverEffect.Type)
                {
                    case TransformationType.Fade:
                        h.StartFloat = (float)Alpha;
                        break;
                    case TransformationType.Scale:
                        h.StartFloat = Scale;
                        break;
                    case TransformationType.Rotation:
                        h.StartFloat = Rotation;
                        break;
                }

                h.StartColour = InitialColour;

                int duration = HoverEffect.Duration;
                h.Time1 = time;
                h.Time2 = time + duration;
                h.TagNumeric = 1;
                Transformations.Add(h);
            }
            if (HoverLostEffects != null)
            {
                lock (SpriteManager.SpriteLock)
                    Transformations.RemoveAll(t => t.TagNumeric == 1);
                bool first = true;
                foreach (Transformation t in HoverLostEffects)
                {
                    Transformation h = t.Clone();
                    if (first)
                    {
                        h.StartVector = Position;
                        switch (h.Type)
                        {
                            case TransformationType.Fade:
                                h.StartFloat = (float)Alpha;
                                break;
                            case TransformationType.Scale:
                                h.StartFloat = Scale;
                                break;
                            case TransformationType.Rotation:
                                h.StartFloat = Rotation;
                                break;
                        }
                        h.StartColour = InitialColour;
                        first = false;
                    }


                    h.Time1 += time;
                    h.Time2 += time;

                    h.TagNumeric = 1;

                    Transformations.Add(h);
                }
            }
        }

        internal virtual void UpdateTextureSize()
        {
            if (localTexture != null)
            {
                Width = localTexture.Width;
                Height = localTexture.Height;
            }

            DrawWidth = Width;
            DrawHeight = Height;
            DrawTop = 0;
            DrawLeft = 0;
        }

        internal virtual void UpdateTextureAlignment()
        {
            switch (Origin)
            {
                case Origins.TopLeft:
                    OriginPosition = Vector2.Zero;
                    break;
                case Origins.Centre:
                    OriginPosition = new Vector2(Width / 2, Height / 2);
                    break;
                case Origins.TopCentre:
                    OriginPosition = new Vector2(Width / 2, 0);
                    break;
                case Origins.CentreLeft:
                    OriginPosition = new Vector2(0, Height / 2);
                    break;
                case Origins.CentreRight:
                    OriginPosition = new Vector2(Width, Height / 2);
                    break;
                case Origins.BottomCentre:
                    OriginPosition = new Vector2(Width / 2, Height);
                    break;
                case Origins.BottomLeft:
                    OriginPosition = new Vector2(0, Height);
                    break;
                case Origins.BottomRight:
                    OriginPosition = new Vector2(Width, Height);
                    break;
                case Origins.TopRight:
                    OriginPosition = new Vector2(Width, 0);
                    break;
            }
        }

        internal event EventHandler OnHover;
        internal event EventHandler OnHoverLost;

        internal Transformation FadeColour(Color colour, int duration, bool force = false, EasingTypes easing = EasingTypes.None)
        {
            if (!force && InitialColour == colour && Transformations.Count == 0)
                return null;
            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => t.Type == TransformationType.Colour);

            if (duration == 0)
            {
                //Shortcut a duration of 0.
                InitialColour = colour;
                return null;
            }

            Transformation tr = new Transformation(InitialColour, colour,
                                   GameBase.GetTime(Clock) - (int)GameBase.ElapsedMilliseconds,
                                   GameBase.GetTime(Clock) + duration)
                                   {
                                       Easing = easing
                                   };
            Transformations.Add(tr);
            return tr;
        }

        internal const int TRANSFORMATION_TAG_FLASH = 51;

        internal void FlashColour(Color colour, int duration)
        {
            if (InitialColour == colour)
                return;

            Color startcol = InitialColour;

            lock (SpriteManager.SpriteLock)
            {
                Transformation otherColourTransform = Transformations.Find(t => t.Type == TransformationType.Colour);
                if (otherColourTransform != null && otherColourTransform.TagNumeric != TRANSFORMATION_TAG_FLASH)
                    return;

                if (otherColourTransform != null)
                    startcol = otherColourTransform.EndColour;

                Transformations.RemoveAll(t => t.Type == TransformationType.Colour);
            }

            Transformation flash = new Transformation(colour, startcol,
                                   GameBase.GetTime(Clock) - (int)GameBase.ElapsedMilliseconds,
                                   GameBase.GetTime(Clock) + duration);
            flash.TagNumeric = TRANSFORMATION_TAG_FLASH;
            Transformations.Add(flash);
        }

        internal bool KeepEventsBound;

        internal override void Dispose(bool isDisposing)
        {
            if (IsDisposable && !KeepEventsBound)
            {
                ClearClickHandlers();
                OnHover = null;
                OnHoverLost = null;
            }

            if (OnDisposable != null)
            {
                OnDisposable(IsDisposable, null);
            }

            if (IsDisposable && localTexture != null)
            {
                localTexture.Dispose();
                localTexture = null;
            }
        }

        public virtual pSprite Clone()
        {
            pSprite clone = new pSprite(Texture, Field, Origin, Clock, InitialPosition, Depth, AlwaysDraw, InitialColour, Tag);
            clone.OriginPosition = OriginPosition;
            clone.Position = Position;
            clone.DrawLeft = DrawLeft;
            clone.DrawTop = DrawTop;
            clone.DrawWidth = DrawWidth;
            clone.DrawHeight = DrawHeight;

            clone.VectorScale = VectorScale;

            clone.Scale = Scale;

            foreach (Transformation t in Transformations)
                if (!t.IsLoopStatic) clone.Transformations.Add(t.Clone());
            if (Loops != null)
            {
                clone.Loops = new List<TransformationLoop>(Loops.Count);
                foreach (TransformationLoop tl in Loops)
                    clone.Loops.Add(tl.Clone());
            }

            return clone;
        }

        public event EventHandler OnDisposable;
        private Vector2 drawOrigin;
        protected Vector2 drawScaleVector;
        protected float drawScale;
        private Vector2 drawOriginScaled;
        private Rectangle drawRectangleSource;

        /// <summary>
        /// Determine whether this sprite should be scaled upwards/downwards to window ratio.
        /// Useful for disabling on sprites which are rendered at native resolution and don't require scaling proportionally to other game sprites.
        /// </summary>
        internal bool ScaleToWindowRatio = true;

        public bool FlipVertical
        {
            get { return VectorScale.Y < 0; }
            set
            {
                if (FlipVertical == value) return;
                VectorScale.Y = -VectorScale.Y;
            }
        }

        public bool FlipHorizontal
        {
            get { return VectorScale.X < 0; }
            set
            {
                if (FlipHorizontal == value) return;
                VectorScale.X = -VectorScale.X;
            }
        }

        internal void ScaleToScreen(bool preferWidescreen = true, bool favourWidth = false)
        {
            float heightFit = (float)GameBase.WindowHeightScaled / Height;
            float bestFit = (float)GameBase.WindowWidthScaled / Width;

            if (heightFit > bestFit && !favourWidth) bestFit = heightFit;

            if (Field != Fields.Storyboard && Field != Fields.StoryboardCentre)
            {
                bestFit *= 1.6f;
                heightFit *= 1.6f;
            }

            if ((float)Width / Height > 1.34f)
                Scale = bestFit;
            else
            {
                switch (ConfigManager.sScaleMode.Value)
                {
                    case ScaleMode.Letterbox:
                        Scale = heightFit;
                        break;
                    case ScaleMode.WidescreenConservative:
                        Scale = preferWidescreen ? bestFit : heightFit;
                        break;
                    case ScaleMode.WidescreenAlways:
                        Scale = bestFit;
                        break;
                }
            }
        }

        public void ClipWidthTo(int final, int duration, EasingTypes easing = EasingTypes.None)
        {
            if (Transformations.Count == 1)
            {
                Transformation t = Transformations[0];
                if (t.Type == TransformationType.ClippingWidth && t.EndFloat == final && t.Time2 - t.Time1 == duration)
                    return;
            }

            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => t.Type == TransformationType.ClippingWidth);

            if (DrawWidth == final)
                return;

            Transformation tr =
                new Transformation(TransformationType.ClippingWidth, (float)DrawWidth, final,
                                   GameBase.GetTime(Clock) - (int)GameBase.ElapsedMilliseconds,
                                   GameBase.GetTime(Clock) + duration);
            tr.Easing = easing;
            Transformations.Add(tr);
        }

        public void ClipHeightTo(int final, int duration, EasingTypes easing = EasingTypes.None)
        {
            if (Transformations.Count == 1)
            {
                Transformation t = Transformations[0];
                if (t.Type == TransformationType.ClippingHeight && t.EndFloat == final && t.Time2 - t.Time1 == duration)
                    return;
            }

            lock (SpriteManager.SpriteLock)
                Transformations.RemoveAll(t => t.Type == TransformationType.ClippingHeight);

            if (DrawHeight == final)
                return;

            Transformation tr =
                new Transformation(TransformationType.ClippingHeight, (float)DrawHeight, final,
                                   GameBase.GetTime(Clock) - (int)GameBase.ElapsedMilliseconds,
                                   GameBase.GetTime(Clock) + duration);
            tr.Easing = easing;
            Transformations.Add(tr);
        }

        internal virtual bool CheckHover(Vector2 pos, int minimalSize = 0)
        {
            return getHoverArea(minimalSize).ContainsRotated(pos, drawPosition, Rotation);
        }

        private RectangleF getHoverArea(int minimalSize = 0)
        {
            if (minimalSize == 0)
                return drawRectangle;

            return RectangleF.Inflate(drawRectangle,
                Math.Max(0, (minimalSize - drawRectangle.Width) * 0.5f),
                Math.Max(0, (minimalSize - drawRectangle.Height) * 0.5f));
        }
    }

    internal class pSpriteCircular : pSprite
    {
        internal int HoverRadius;

        public pSpriteCircular(pTexture texture, Vector2 position, float depth, bool alwaysDraw, Color color, int radius)
            : base(texture, Fields.TopLeft, Origins.Centre, Clocks.Game, position, depth, alwaysDraw, color)
        {
            HoverRadius = radius;
        }

        internal override bool CheckHover(Vector2 pos, int minimalSize = 0)
        {
            float radius = (Scale * HoverRadius * GameBase.WindowRatio) / 2;
            return Vector2.DistanceSquared(pos, drawPosition) < radius * radius;
        }
    }

    internal class pDrawableDepthComparer : IComparer<pDrawable>
    {
        #region IComparer<pDrawable> Members

        public int Compare(pDrawable x, pDrawable y)
        {
            return x.Depth.CompareTo(y.Depth);
        }

        #endregion
    }
}