using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Graphics.Skinning;
using osu.Helpers;
using osu_common.Helpers;

namespace osu.Graphics.Sprites
{
    internal class pAnimation : pSprite
    {
        public bool DrawDimensionsManualOverride;
        public LoopTypes LoopType;
        internal bool RunAnimation = true;
        internal int TextureCount;

        private int currentFrame;
        private int textureFrame;
        private double frameDelay = GameBase.SIXTY_FRAME_TIME;
        private double animationStartTime;
        private bool firstFrame = true;
        private bool finished = false;
        private pTexture[] textureArray;
        private int[] customSequence;

        internal event VoidDelegate AnimationFinished;

        internal pAnimation(pTexture[] textures, Fields fieldType, Origins origin, Clocks clock,
                            Vector2 startPosition, float drawDepth, bool alwaysDraw, Color colour)
            : this(textures, fieldType, origin, clock, startPosition, drawDepth, alwaysDraw, colour, null)
        {
        }

        internal pAnimation(pTexture[] textures, Fields fieldType, Origins origin, Clocks clock,
                            Vector2 startPosition, float drawDepth, bool alwaysDraw, Color colour, object tag)
            : base(
                textures == null || textures.Length == 0 ? null : textures[0], fieldType, origin, clock,
                startPosition, drawDepth, alwaysDraw, colour, tag)
        {
            if (textures != null)
                TextureArray = textures;
        }

        internal override bool Reverse
        {
            set
            {
                if (base.Reverse == value)
                    return;

                if (!firstFrame && frameDelay > 0)
                {
                    double time = SpriteManager.GetSpriteTime(this);
                    if (animationStartTime < time)
                    {
                        // Move the start time so that the current frame stays the same
                        int frameCount = MaxFrame + 1;
                        double frame = (time - animationStartTime) / frameDelay;
                        double frameOffset = Math.Floor(frame / frameCount) * frameCount;
                        double reversedFrame = MaxFrame - (frame - frameOffset);
                        animationStartTime = time - (frameOffset + reversedFrame) * frameDelay;
                    }
                }

                base.Reverse = value;
            }
        }

        internal int CurrentFrame
        {
            get { return currentFrame; }
            set
            {
                if (value < 0) value = 0;

                currentFrame = LoopType == LoopTypes.LoopOnce ? 
                    Math.Min(value, MaxFrame) : value % (MaxFrame + 1);

                // Move the start time so that the new current frame starts now
                double time = SpriteManager.GetSpriteTime(this);
                int frame = Reverse ? (value - currentFrame) + (MaxFrame - currentFrame) : value;
                animationStartTime = time - frame * frameDelay;

                firstFrame = false;
            }
        }

        public int MaxFrame { get { return !HasCustomSequence ? TextureCount - 1 : CustomSequence.Length - 1; } }

        internal double FrameDelay
        {
            get { return frameDelay; }
            set
            {
                if (frameDelay == value)
                    return;

                if (!firstFrame && frameDelay > 0)
                {
                    double time = SpriteManager.GetSpriteTime(this);
                    if (animationStartTime < time)
                    {
                        // Move the start time so that the current frame stays the same
                        double frame = (time - animationStartTime) / frameDelay;
                        animationStartTime = LoopType == LoopTypes.LoopOnce && frame > MaxFrame ?
                            animationStartTime + MaxFrame * frameDelay - MaxFrame * value : 
                            time - frame * value;
                    }
                }

                frameDelay = value;
            }
        }

        internal pTexture[] TextureArray
        {
            get { return textureArray; }
            set
            {
                if (value == textureArray) return;

                textureArray = value;

                if (textureArray != null)
                {
                    TextureCount = textureArray.Length;
                    if (TextureCount > 0)
                        base.Texture = textureArray[0];
                    else
                        base.Texture = null;
                }
                else
                    TextureCount = 0;

                textureFrame = 0;
                ResetAnimation();
            }
        }

        internal int[] CustomSequence
        {
            get { return customSequence; }
            set
            {
                if (customSequence == value)
                    return;

                customSequence = value;
                ResetAnimation();
            }
        }

        public bool HasCustomSequence { get { return CustomSequence != null; } }

        internal override pTexture Texture
        {
            get
            {
                if (TextureArray == null || TextureCount == 0)
                    return null;
                return localTexture;
            }
        }

        internal void ResetAnimation()
        {
            firstFrame = true;
            finished = false;
            currentFrame = Reverse ? MaxFrame : 0;
        }

        public override UpdateResult Update()
        {
            UpdateResult result = base.Update();
            if (result != UpdateResult.Visible) return result;

            UpdateFrame();
            return UpdateResult.Visible;
        }

        internal void UpdateFrame()
        {
            if (TextureCount < 2)
                return;

            if (RunAnimation && frameDelay > 0)
            {
                double time = SpriteManager.GetSpriteTime(this);

                if (!firstFrame && time < animationStartTime)
                    ResetAnimation();

                if (firstFrame)
                {
                    animationStartTime = Transformations.Count > 0 ? Transformations[0].Time1 : time;
                    firstFrame = false;
                }

                double animationTime = time - animationStartTime;
                int frame = (int)Math.Floor(animationTime / frameDelay);

                if (frame < 0)
                    frame = 0;

                bool justFinished = false;
                if (LoopType == LoopTypes.LoopOnce)
                {
                    if (frame > MaxFrame)
                    {
                        frame = MaxFrame;
                        if (!finished)
                            justFinished = finished = true;
                    }
                    else
                    {
                        finished = false;
                    }
                }
                else
                {
                    frame %= MaxFrame + 1;
                    finished = false;
                }

                currentFrame = Reverse ? MaxFrame - frame : frame;

                if (justFinished && AnimationFinished != null)
                    AnimationFinished();
            }
            else
            {
                // Since the animation isn't playing, set the current frame in a way that will make it resume from the correct frame when started.
                CurrentFrame = OsuMathHelper.Clamp(currentFrame, 0, MaxFrame);
            }

            if (textureFrame != currentFrame)
            {
                localTexture = HasCustomSequence ? TextureArray[Math.Min(TextureArray.Length - 1, CustomSequence[currentFrame])] : TextureArray[currentFrame];

                if (Texture != null)
                {
                    if (!DrawDimensionsManualOverride)
                        UpdateTextureSize();
                    if (Origin != Origins.TopLeft)
                        UpdateTextureAlignment();
                }

                textureFrame = currentFrame;
            }
        }

        internal void SetFramerateFromSkin()
        {
            if (SkinManager.Current == null || textureArray == null) return;

            if (SkinManager.Current.AnimationFramerate > 0)
                FrameDelay = 1000f / SkinManager.Current.AnimationFramerate;
            else
                FrameDelay = 1000f / TextureArray.Length;
        }

        public override pSprite Clone()
        {
            pAnimation clone = new pAnimation(TextureArray, Field, Origin, Clock, InitialPosition, Depth, AlwaysDraw,
                                              InitialColour, Tag);
            clone.FrameDelay = FrameDelay;
            clone.LoopType = LoopType;
            foreach (Transformation t in Transformations)
                clone.Transformations.Add(t.Clone());
            return clone;
        }
    }

    internal enum LoopTypes
    {
        LoopForever,
        LoopOnce
    }
}