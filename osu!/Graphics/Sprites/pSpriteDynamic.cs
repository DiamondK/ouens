using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Helpers;
using osu_common.Libraries.NetLib;
using osu_common.Helpers;

namespace osu.Graphics.Sprites
{
    internal static class DynamicSpriteCache
    {
        internal static List<pSpriteUnloadable> DynamicSprites = new List<pSpriteUnloadable>();
        internal static List<pSpriteUnloadable> DynamicSpritesNoUnload = new List<pSpriteUnloadable>();

        internal static bool Remove(pSpriteUnloadable s)
        {
            return DynamicSprites.Remove(s) || DynamicSpritesNoUnload.Remove(s);
        }

        internal static void Load(pSpriteUnloadable s, bool allowUnloadDynamically)
        {
            if (s == null) return;

            if (allowUnloadDynamically)
                DynamicSprites.Add(s);
            else
                DynamicSpritesNoUnload.Add(s);
        }

        internal static void CheckForUnused()
        {
            for (int i = 0; i < DynamicSprites.Count; i++)
            {
                if (DynamicSprites[i].CheckUnloadable())
                    DynamicSprites.RemoveAt(i--);
            }
        }

        public static void Reload()
        {
            foreach (pSpriteUnloadable s in DynamicSprites)
                s.Unload();
            foreach (pSpriteUnloadable s in DynamicSpritesNoUnload)
                s.Unload();
            DynamicSprites.Clear();
            DynamicSpritesNoUnload.Clear();
        }
    }

    internal class pSpriteDynamic : pSpriteUnloadable
    {
        internal int TimeBeforeLoad;
        private string url;
        internal string Url
        {
            get
            {
                return url;
            }
            set
            {
                if (url != value)
                {
                    if (localTexture != null)
                    {
                        localTexture.Dispose();
                        localTexture = null;
                    }

                    url = value;
                    State = LoadState.Unknown;
                }

                if (string.IsNullOrEmpty(url))
                    State = LoadState.Error;
            }
        }

        private string localCache;
        internal string LocalCache
        {
            get
            {
                return localCache;
            }
            set
            {
                if (localCache != value)
                {
                    localCache = value;
                    State = LoadState.Unknown;
                }
            }
        }

        internal pSpriteDynamic(string url, string localCache, int timeBeforeLoad, Vector2 position, float depth)
            : base(null, position, depth, true, Color.White)
        {
            if (string.IsNullOrEmpty(localCache) && !string.IsNullOrEmpty(url))
                localCache = General.TEMPORARY_FILE_PATH + "dynamic_sprite_" + CryptoHelper.GetMd5String(url);

            this.localCache = localCache;
            this.url = url;
            TimeBeforeLoad = timeBeforeLoad;
            IsDisposable = true;

            if (url == null)
                State = LoadState.Error;
        }

        internal override void Dispose(bool isDisposing)
        {
            DynamicSpriteCache.Remove(this);
            base.Dispose(isDisposing);
        }

        private int lastTextureRequest;
        private int consecutiveRequestTime;
        internal float MaxDimension;

        /// <summary>
        /// Skips the unloading procedure.
        /// </summary>
        public bool NoUnload;

        FileNetRequest nr;

        /// <summary>
        /// A queue to make sure we only request the same resource once.
        /// </summary>
        static List<FileNetRequest> activeRequests = new List<FileNetRequest>();

        internal override pTexture Texture
        {
            get
            {
                if (State == LoadState.Unknown)
                {
                    if (url == null) return null;

                    if (lastTextureRequest > 0 && GameBase.Time - lastTextureRequest < 200)
                        consecutiveRequestTime += (GameBase.Time - lastTextureRequest);
                    else
                        consecutiveRequestTime = 0;

                    lastTextureRequest = GameBase.Time;

                    if (consecutiveRequestTime < TimeBeforeLoad)
                        return null;

                    if (localTexture != null)
                        localTexture.Dispose();

                    nr = activeRequests.Find(r => r != null && r.m_filename == localCache && r.m_url == url);
                    if (nr != null) //a request has already been made for this. block here until it is completed.
                        return null;

                    if (!File.Exists(LocalCache))
                    {
                        string dir = Path.GetDirectoryName(LocalCache);
                        if (!Directory.Exists(Path.GetDirectoryName(LocalCache)))
                            Directory.CreateDirectory(dir);

                        State = LoadState.Retrieving;

                        nr = new FileNetRequest(LocalCache, Url);

                        nr.onFinish += delegate
                                           {
                                               State = LoadState.Unloaded;
                                               lock (activeRequests)
                                                   activeRequests.Remove(nr);
                                               nr = null;
                                           };
                        NetManager.AddRequest(nr);
                        lock (activeRequests)
                            activeRequests.Add(nr);
                    }
                    else
                    {
                        State = LoadState.Unloaded;
                    }
                }

                if (State == LoadState.Unloaded)
                {
                    State = LoadState.Loaded;
                    try
                    {
                        Texture = pTexture.FromFile(LocalCache);
                        if (Texture == null)
                        {
                            try { File.Delete(LocalCache); }
                            catch { }
                            State = LoadState.Error;
                            return null;
                        }

                        if (!NoUnload)
                            base.Texture.TrackAccessTime = true;

                        DynamicSpriteCache.Load(this, !NoUnload);

                        if (MaxDimension != 0)
                            Scale = MaxDimension / Math.Max(Width, Height);
                        if (Alpha == 1 && Transformations.Count == 0)
                        {
                            //there is a case where this is being called from the SpriteManager.Draw() method, where alpha for the current draw frame has already been calculated.
                            //we need to reset it here to avoid a one-frame flash.
                            drawColour = new Color(drawColour.R, drawColour.G, drawColour.B, 0);

                            FadeInFromZero(200);
                        }

                        InvokeOnTextureLoaded();
                    }
                    catch
                    {
                        try { File.Delete(LocalCache); }
                        catch { }
                        State = LoadState.Error;
                    }
                }

                return base.Texture;
            }
            set { base.Texture = value; }
        }

        /// <summary>
        /// Occurs when [texture is loaded dynamically].  Only fires each bound delegate once.
        /// </summary>
        internal event VoidDelegate OnTextureLoaded;

        private void InvokeOnTextureLoaded()
        {
            VoidDelegate d = OnTextureLoaded;
            if (d != null) d();

            OnTextureLoaded = null;
        }

        public void Reload()
        {
            State = LoadState.Unknown;
        }

        public override void Dispose()
        {
            //Unload();
            if (nr != null)
            {
                nr.Abort();
                lock (activeRequests)
                    activeRequests.Remove(nr);
                State = LoadState.Unknown;
            }

        }
    }
}