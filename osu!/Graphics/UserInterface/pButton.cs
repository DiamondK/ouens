﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;

namespace osu.Graphics.UserInterface
{
    internal class pButton
    {
        internal List<pSprite> SpriteCollection;
        private Color litUp;
        private Color orig;
        internal pText Text;

        public bool SoundsEnabled = true;

        internal event EventHandler OnClick;

        internal pButton(string text, Vector2 position, Vector2 dimensions, float drawdepth,
                         Color colour, EventHandler onclick, bool skinnable = true, bool snapToRight = false, float? textSize = null)
        {
            SetColour(colour);

            if (onclick != null)
                OnClick += onclick;

            SpriteCollection = new List<pSprite>();

            int premult = snapToRight ? -1 : 1;

            Text = new pText(text, textSize.HasValue ? textSize.Value : 14 * dimensions.Y / 18f / (text.IndexOf('\n') > 0 ? 2 : 1), position, drawdepth + 0.002F, true, Color.White)
            {
                FontFace = General.FONT_FACE_REGULAR
            };
            if (snapToRight) Text.Field = Fields.TopRight;
            Text.Origin = Origins.Centre;

            float textWidth = Text.MeasureText().X;
            if (textWidth > dimensions.X)
                dimensions.X = textWidth;

            Text.InitialPosition += new Vector2(dimensions.X * premult, dimensions.Y) / 2;
            Text.Position = Text.InitialPosition;
            Text.UpdateTextureAlignment();

            pSprite p =
                new pSprite(skinnable ? SkinManager.Load(@"button-left", SkinSource.Osu | SkinSource.Skin) : SkinManager.Load(@"button-left", SkinSource.Osu), snapToRight ? Fields.TopRight : Fields.TopLeft, Origins.TopLeft, Clocks.Game, position,
                            drawdepth, true,
                            orig, this);
            p.Scale = dimensions.Y / (p.DrawHeight * 0.625f);
            p.HandleInput = true;
            p.OnClick += p_OnClick;
            p.ClickRequiresConfirmation = true;
            p.OnHover += pButton_OnHover;
            p.OnHoverLost += pButton_OnHoverLost;
            SpriteCollection.Add(p);

            float wd = SpriteCollection[0].DrawWidth * 0.625f * SpriteCollection[0].Scale;

            p =
    new pSprite(skinnable ? SkinManager.Load(@"button-right", SkinSource.Osu | SkinSource.Skin) : SkinManager.Load(@"button-right", SkinSource.Osu), snapToRight ? Fields.TopRight : Fields.TopLeft, Origins.TopLeft, Clocks.Game, position + premult * new Vector2(dimensions.X - wd, 0),
                drawdepth, true,
                orig, this);
            p.Scale = dimensions.Y / (p.DrawHeight * 0.625f);
            p.HandleInput = true;
            p.OnClick += p_OnClick;
            p.ClickRequiresConfirmation = true;
            p.OnHover += pButton_OnHover;
            p.OnHoverLost += pButton_OnHoverLost;
            SpriteCollection.Add(p);

            p =
                new pSprite(skinnable ? SkinManager.Load(@"button-middle", SkinSource.Osu | SkinSource.Skin) : SkinManager.Load(@"button-middle", SkinSource.Osu), snapToRight ? Fields.TopRight : Fields.TopLeft, Origins.TopLeft, Clocks.Game,
                            position + premult * new Vector2(wd, 0), drawdepth + 0.001F, true,
                            orig, this);
            p.HandleInput = true;
            p.OnClick += p_OnClick;
            p.ClickRequiresConfirmation = true;
            p.OnHover += pButton_OnHover;
            p.OnHoverLost += pButton_OnHoverLost;
            if (p.Texture != null) p.VectorScale = new Vector2((dimensions.X - (wd * 2)) / p.Texture.Width * 1.6f, SpriteCollection[1].Scale);
            SpriteCollection.Add(p);

            SpriteCollection.Add(Text);
        }

        void p_OnClick(object sender, EventArgs e)
        {
            if (OnClick != null)
                OnClick(this, null);
            if (SoundsEnabled)
                AudioEngine.PlaySample(@"click-short-confirm");
            SpriteCollection.ForEach(p => p.FlashColour(Color.White, 400));
        }

        bool hovering = false;
        void pButton_OnHover(object sender, EventArgs e)
        {
            if (hovering) return;
            hovering = true;

            SpriteCollection[0].FadeColour(litUp, 50, true);
            SpriteCollection[1].FadeColour(litUp, 50, true);
            SpriteCollection[2].FadeColour(litUp, 50, true);

            if (SoundsEnabled)
                AudioEngine.Click(null, @"click-short");
        }

        void pButton_OnHoverLost(object sender, EventArgs e)
        {
            GameBase.Scheduler.Add(delegate
            {
                if (SpriteCollection[0].Hovering || SpriteCollection[1].Hovering || SpriteCollection[2].Hovering) return;

                SpriteCollection[0].FadeColour(orig, 50);
                SpriteCollection[1].FadeColour(orig, 50);
                SpriteCollection[2].FadeColour(orig, 50);

                hovering = false;
            }, true);
        }

        public void Hide()
        {
            SpriteCollection.ForEach(p =>
                {
                    p.HandleInput = false;
                    p.FadeOut(50);
                });
        }

        public void Show()
        {
            SpriteCollection.ForEach(p =>
            {
                if (!(p is pText))
                    p.HandleInput = true;
                p.FadeIn(50);
            });
        }

        public void SetColour(Color colour)
        {
            litUp = colour;
            orig = new Color((byte)Math.Max(0, litUp.R - 20), (byte)Math.Max(0, litUp.G - 20), (byte)Math.Max(0, litUp.B - 20));

            if (SpriteCollection != null && SpriteCollection.Count >= 3)
            {
                SpriteCollection[0].InitialColour = orig;
                SpriteCollection[1].InitialColour = orig;
                SpriteCollection[2].InitialColour = orig;
            }
        }

        public string ToolTip
        {
            get { return SpriteCollection[0].ToolTip; }
            set
            {
                SpriteCollection.ForEach(s => s.ToolTip = value);

            }
        }
    }
}