﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Sprites;
using osu.Online;
using osu.Input.Handlers;
using Microsoft.Xna.Framework.Input;
using osu_common.Helpers;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;

namespace osu.Graphics.UserInterface
{
    class pSearchBox : pTextBoxOmniscient
    {
        private pTextAwesome icon;
        float size;
        Vector2 position;
        TextAlignment alignment;
        public bool IsDefault { get { return isDefaultValue; } }

        public pSearchBox(int size, Vector2 position, int lengthLimit = 20, TextAlignment alignment = TextAlignment.Left)
            : base(size, position, 0, true)
        {
            MaintainFocus = true;
            Box.TextBold = false;
            Box.HandleInput = false;
            LengthLimit = lengthLimit;
            this.size = size;
            this.position = position;
            this.alignment = alignment;

            HandleLeftRightArrows = false;

            DefaultText = LocalisationManager.GetString(OsuString.SongSelection_TypeToBegin);

            //search bar
            icon = new pTextAwesome(FontAwesome.search, size + 2, new Vector2(position.X, position.Y + size * 0.1f));
            icon.Depth = Box.Depth;
            SpriteCollection.Add(icon);

            if (alignment != TextAlignment.Centre)
                Box.Position.X += size;

            SpriteCollection.ForEach(s =>
            {
                switch (alignment)
                {
                    case TextAlignment.Left:
                        s.Field = Fields.TopLeft;
                        s.Origin = Origins.TopLeft;
                        break;
                    case TextAlignment.Centre:
                        s.Field = Fields.TopCentre;
                        s.Origin = Origins.TopCentre;
                        break;
                    case TextAlignment.Right:
                        s.Field = Fields.TopRight;
                        s.Origin = Origins.TopRight;
                        break;
                }
            });

            OnChange += delegate { layout(); };
        }

        void layout()
        {
            if (alignment == TextAlignment.Centre)
            {
                float textLength = Box.MeasureText().X;

                Box.Position.X = position.X + size / 2;
                icon.Position.X = position.X - textLength / 2;
            }
        }

        internal void Shake()
        {
            SpriteCollection.ForEach(s =>
                {
                    s.FlashColour(SkinManager.NEW_SKIN_COLOUR_MAIN, 500);
                    s.Rotation = (float)GameBase.random.NextDouble() - 0.5f;
                    s.RotateTo(0, 500);
                });
        }
    }
}
