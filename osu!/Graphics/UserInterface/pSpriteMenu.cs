﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Graphics.Sprites;
using Microsoft.Xna.Framework;
using osu.Input.Handlers;
using osu.Input;

namespace osu.Graphics.UserInterface
{
    /// <summary>
    /// The menu component of a dropdown menu
    /// </summary>
    internal class pSpriteMenu : IDisposable
    {
        internal List<pSpriteMenuItem> Items;
        private SpriteManager m_sprite_manager;
        private bool m_visible;
        private bool m_opening_click;

        internal pSpriteMenu(SpriteManager sprite_manager)
            : this(sprite_manager, new List<pSpriteMenuItem>())
        {
        }

        internal pSpriteMenu(SpriteManager sprite_manager, List<pSpriteMenuItem> items)
        {
            Items = items;
            m_sprite_manager = sprite_manager;
            m_visible = false;

            InputManager.Bind(InputEventType.OnClick, onClick);
        }

        internal void Toggle()
        {
            if (Visible)
                Hide();
            else
                Show();
        }


        /// <summary>
        /// Expands the menu
        /// </summary>
        internal void Show()
        {
            m_visible = true;
            m_opening_click = true;

            int offset = 0;
            foreach (pSpriteMenuItem item in Items)
            {
                item.Show(offset);
                offset += 20;
            }
        }

        /// <summary>
        /// Hide
        /// </summary>
        internal void Hide()
        {
            m_visible = false;
            foreach (pSpriteMenuItem item in Items)
            {
                item.Hide();
            }
        }

        internal bool Visible
        {
            get
            {
                return m_visible;
            }
            set
            {
                m_visible = value;
                if (value) Show();
                else Hide();
            }
        }

        /// <summary>
        /// CALL THIS instead of using Items.Clear() to perform all cleanup!
        /// </summary>
        internal void ClearItems()
        {
            foreach (pSpriteMenuItem item in Items)
            {
                // Unattaches the item's pSprites from the SpriteManager and potentially disposes their textures if uncached.
                item.Dispose();
            }

            Items.Clear();
        }

        /// <summary>
        /// Triggers when the menu loses focus and closes itself
        /// </summary>
        internal event EventHandler OnBlur;

        private bool onClick(object sender, EventArgs e)
        {
            if (Visible && !m_opening_click)
            {
                Hide();
                if (OnBlur != null) OnBlur(this, EventArgs.Empty);
                return true;
            }

            m_opening_click = false;
            return false;
        }

        #region IDisposable Members

        public void Dispose()
        {
            ClearItems();
        }

        #endregion
    }
}
