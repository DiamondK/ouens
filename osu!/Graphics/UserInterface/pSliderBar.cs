﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Graphics.Primitives;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;

namespace osu.Graphics.UserInterface
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    internal class pSliderBar : IDisposable
    {
        private readonly double initial;
        internal Vector2 position { get { return lineLeft.Position; } }
        internal double amount = 0;

        private bool captured;
        private Vector2 capturedPosition;

        internal bool IsDragging
        {
            get
            {
                return captured;
            }
        }

        internal double current;
        internal int length;
        internal Color lineColour;
        internal double max;
        internal double min;
        internal pSprite seekbar1;
        private bool visible = true;
        internal bool KeyboardControl;
        internal double KeyboardControlIncrement = 1;
        internal bool Visible
        {
            get { return visible && seekbar1.IsVisible; }
            set
            {
                if (visible == value) return;

                visible = value;

                if (visible)
                {
                    seekbar1.FadeIn(100);
                    lineLeft.FadeIn(100);
                    lineRight.FadeIn(100);
                }
                else
                {
                    seekbar1.FadeOut(100);
                    lineLeft.FadeOut(100);
                    lineRight.FadeOut(100);

                    KillCapture();
                }
            }
        }

        internal string Tooltip
        {
            get { return seekbar1.ToolTip; }
            set
            {
                lineLeft.ToolTip = lineRight.ToolTip = seekbar1.ToolTip = value;
            }
        }
        internal List<pSprite> SpriteCollection = new List<pSprite>();
        internal pSprite lineLeft;
        internal pSprite lineRight;

        internal event EventHandler ValueChanged;

        internal pSliderBar(SpriteManager spriteManager, double min, double max, double initial, Vector2 position, int length)
        {
            this.min = min;
            this.max = max;
            current = this.initial = Math.Max(min, Math.Min(max, initial));
            this.length = length;
            lineColour = SkinManager.NEW_SKIN_COLOUR_MAIN;

            lineLeft = new pSpriteSliderBarLine(GameBase.WhitePixel, position, 0.992f, lineColour, length);
            lineLeft.HandleInput = true;
            lineLeft.ExactCoordinates = true;
            lineLeft.VectorScale = new Vector2(length * 1.6f, 1.5f);

            lineRight = new pSprite(GameBase.WhitePixel, position, 0.991f, true, lineColour);
            lineRight.ExactCoordinates = true;
            lineRight.HandleInput = true;
            lineRight.Alpha = 0.5f;
            lineRight.VectorScale = new Vector2(length * 1.6f, 1.5f);

            seekbar1 =
                new pSprite(SkinManager.Load(@"circle-empty", SkinSource.Osu), Fields.TopLeft, Origins.Centre,
                            Clocks.Game, seekPosition, 0.99f, true, lineColour, null);
            seekbar1.HandleInput = true;

            seekbar1.OnUpdate += delegate
            {
                float v1 = seekbar1.drawRectangle.X - lineLeft.drawRectangle.X;
                float halfCircle = seekbar1.DrawWidth / 1.6f / 2;

                lineLeft.VectorScale.X = Math.Max(0, v1 / GameBase.WindowRatioInverse);
                lineRight.VectorScale.X = Math.Max(0, length * 1.6f - (v1 + seekbar1.DrawWidth) / GameBase.WindowRatioInverse);
                lineRight.Position.X = seekbar1.Position.X + halfCircle;
            };

            SpriteCollection.Add(seekbar1);
            SpriteCollection.Add(lineLeft);
            SpriteCollection.Add(lineRight);

            lineLeft.OnHover += sprite_OnHover;
            lineLeft.OnHoverLost += sprite_OnHoverLost;

            if (spriteManager != null)
            {
                spriteManager.Add(seekbar1);
                spriteManager.Add(lineLeft);
                spriteManager.Add(lineRight);
            }

            updatePosition();
        }

        bool _eventsBound = false;
        bool eventsBound
        {
            get { return _eventsBound; }
            set
            {
                if (value == _eventsBound) return;
                _eventsBound = value;

                if (_eventsBound)
                {
                    InputManager.Bind(InputEventType.OnClick, onClick, InputTargetType.Highest, BindLifetime.Manual);
                    InputManager.Bind(InputEventType.OnClickUp, onClickUp, InputTargetType.Highest, BindLifetime.Manual);
                    InputManager.Bind(InputEventType.OnDrag, onDrag, InputTargetType.Highest, BindLifetime.Manual);
                    KeyboardHandler.OnKeyRepeat += GameBase_OnKeyRepeat;
                }
                else
                {
                    InputManager.Unbind(InputEventType.OnClick, onClick, InputTargetType.Highest);
                    InputManager.Unbind(InputEventType.OnClickUp, onClickUp, InputTargetType.Highest);
                    InputManager.Unbind(InputEventType.OnDrag, onDrag, InputTargetType.Highest);
                    KeyboardHandler.OnKeyRepeat -= GameBase_OnKeyRepeat;
                }
            }
        }

        internal bool Hovering { get; private set; }

        void sprite_OnHoverLost(object sender, EventArgs e)
        {
            if (!captured) eventsBound = false;
        }

        void sprite_OnHover(object sender, EventArgs e)
        {
            if (ReadOnly || !Visible) return;

            Hovering = true;
            eventsBound = true;
        }

        bool onClick(object sender, EventArgs e)
        {
            capturedPosition = MouseManager.MousePosition;
            return false;
        }

        bool onDrag(object sender, EventArgs e)
        {
            if (!captured)
            {
                Vector2 diff = (MouseManager.MousePosition - capturedPosition) * GameBase.WindowRatioScaleDown;

                // If we dragged too far don't re-allow dragging. We do this by setting the captured position to nirvana.
                if (Math.Abs(diff.Y) >= 5)
                {
                    capturedPosition.Y = Single.NegativeInfinity;
                    return false;
                }

                if (Math.Abs(diff.X) > 2.5 &&
                    Math.Abs(diff.Y) < 5)
                {
                    captured = true;
                }
                else
                {
                    return false;
                }
            }

            DoPositionUpdate();
            return true;
        }

        bool onClickUp(object sender, EventArgs e)
        {
            Vector2 diff = (MouseManager.MousePosition - capturedPosition) * GameBase.WindowRatioScaleDown;

            if (captured || Math.Abs(diff.Y) < 5)
            {
                captured = false;
                DoPositionUpdate();
            }

            if (!lineLeft.Hovering) eventsBound = false;
            return false;
        }

        private double DragPosition
        {
            get
            {
                RectangleF active = activeRectangle;
                return (double)(MouseManager.MousePosition.X - active.X) / active.Width * (max - min);
            }
        }

        private void DoPositionUpdate()
        {
            if (Visible)
            {
                SetValue(Math.Min(max, Math.Max(min, min + DragPosition)));
            }
        }

        private RectangleF activeRectangle
        {
            get
            {
                return new RectangleF(lineLeft.drawRectangle.X, lineLeft.drawRectangle.Y - seekbar1.drawRectangle.Height / 2, length * GameBase.WindowRatio, seekbar1.drawRectangle.Height);
            }
        }

        private float progress
        {
            get { return (float)((current - min) / (max - min)); }
        }

        private Vector2 seekPosition
        {
            get { return position + new Vector2(length, 0) * progress; }
        }

        private bool GameBase_OnKeyRepeat(object sender, Keys k, bool first)
        {
            if (!KeyboardControl) return false;

            switch (k)
            {
                case Keys.Left:
                    SetValue(Math.Min(max, Math.Max(min, current - KeyboardControlIncrement)));
                    return true;
                case Keys.Right:
                    SetValue(Math.Min(max, Math.Max(min, current + KeyboardControlIncrement)));
                    return true;
            }

            return false;
        }

        public void Dispose()
        {
            KillCapture();
        }

        /// <summary>
        /// Note this won't work if startPosition is being used as an offset value (or is the sliderBar is in the middle of a movement adnimation).
        /// </summary>
        internal void ChangeBounds(double min, double max, double current)
        {
            this.min = min;
            this.max = max;

            SetValue(current);
        }

        internal void SetValue(double value, bool silent = false)
        {
            value = Math.Min(max, Math.Max(min, value));

            if (current != value || !IsDragging)
            {
                current = value;

                if (!silent)
                {
                    if (ValueChanged != null) ValueChanged(this, null);

                    if (IsDragging)
                    {
                        AudioEngine.Click(null, @"sliderbar", 1 + progress * 0.2f);
                    }
                }

                updatePosition();
            }
        }

        private void updatePosition()
        {
            int duration = seekbar1.IsVisible ? 100 : 0; //temp fix

            seekbar1.InitialPosition = seekPosition + (lineLeft.InitialPosition - lineLeft.Position);
            seekbar1.MoveTo(seekPosition, duration, EasingTypes.Out);
        }

        internal void KillCapture()
        {
            captured = false;
            eventsBound = false;
        }

        public bool ReadOnly { get; set; }

        internal class pSpriteSliderBarLine : pSprite
        {
            float length;
            internal pSpriteSliderBarLine(pTexture texture, Vector2 position, float depth, Color lineColour, float length) :
                base(texture, position, depth, true, lineColour)
            {
                this.length = length;
            }

            internal override bool CheckHover(Vector2 pos, int minimalSize = 0)
            {
                float height = drawRectangle.Height;

                RectangleF drawExtended = drawRectangle;
                drawExtended.Y -= height * 7;
                drawExtended.Height += height * 14;
                drawExtended.Width = length * GameBase.WindowRatio;

                return drawExtended.Contains(pos);
            }

        }
    }
}