﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace osu.Graphics.UserInterface
{
    /// <summary>
    /// super-quick helper class to ask a yes or no question
    /// </summary>
    internal class pDialogConfirmation : pDialog
    {
        internal pDialogConfirmation(string question, EventHandler yes, EventHandler no)
            : base(question,true)
        {
            AddOption("Yes", Color.YellowGreen, yes);
            AddOption("No", Color.OrangeRed, no);
        }
    }
}
