﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;
using osu_common.Helpers;
using osu.Graphics.Primitives;
using osu.Helpers;

namespace osu.Graphics.UserInterface
{
    internal class pScrollableArea : GameComponent
    {
        private readonly pSprite scrollDraggerV;
        internal Vector2 ContentDimensions;
        internal RectangleF DisplayRectangle;
        internal bool ScrollDraggerOnLeft;
        internal bool ScrollDragger = true;
        internal float ScrollbarOffset;
        internal bool ScrollDraggerPadding = true;

        internal float ActualDisplayHeight { get { return DisplayRectangle.Height - headerHeight; } }
        internal int LastScrollTime;

        private InputTargetType inputTarget;

        private float scrollVelocity;
        internal float ScrollVelocity
        {
            get
            {
                return scrollVelocity;
            }

            set
            {
                if (Single.IsNaN(value))
                {
                    throw new Exception("NaN in ScrollVelocity. " + ScrollPosition.Y + " " + scrollVelocity + " " + scrollDeceleration);
                }

                scrollVelocity = value;
            }
        }

        internal bool IsDragging;
        private bool scrollingVerticallyReversed;
        private Vector2 ScrollMouseOffset;
        internal Vector2 ScrollPosition;
        private Vector2 scrollStartPosition;
        private float headerHeight;
        internal float HeaderHeight { get { return headerHeight; } set { headerHeight = value; UpdateDraggerV(); } }
        internal SpriteManager SpriteManager;
        internal SpriteManager SpriteManagerBelow;
        internal List<SpriteManager> Children = new List<SpriteManager>();
        internal bool AllowDragScrolling = true;

        internal float ClampingStart = 0;

        internal bool Visible
        {
            get
            {
                return Alpha >= 0.01f;
            }
        }

        internal bool HandleOverlayInput
        {
            get { return SpriteManager.HandleOverlayInput; }

            set
            {
                SpriteManager.HandleOverlayInput = value;
                SpriteManagerBelow.HandleOverlayInput = value;
            }
        }

        internal virtual bool IsScrolling
        {
            get { return Math.Abs(ScrollVelocity) > VELOCITY_CUTOFF || IsDragging; }
        }

        internal event VoidDelegate FinishedScrolling;

        internal bool DraggerHovered
        {
            get
            {
                return scrollDraggerV.Hovering;
            }
        }

        internal pScrollableArea(Rectangle displayRectangle, Vector2 contentDimensions, bool widescreenAware, float headerHeight = 0,
            InputTargetType inputTarget = InputTargetType.Default)
            : this(new RectangleF(displayRectangle.X, displayRectangle.Y, displayRectangle.Width, displayRectangle.Height), contentDimensions, widescreenAware, headerHeight, inputTarget)
        { }

        internal pScrollableArea(RectangleF displayRectangle, Vector2 contentDimensions, bool widescreenAware, float headerHeight = 0,
            InputTargetType inputTarget = InputTargetType.Default)
            : base(GameBase.Instance)
        {
            this.inputTarget = inputTarget;

            InputManager.Bind(InputEventType.OnDrag, onDrag, inputTarget, BindLifetime.Manual);
            InputManager.Bind(InputEventType.OnEndDrag, onEndDrag, inputTarget, BindLifetime.Manual);

            InputManager.Bind(InputEventType.OnMouseWheelUp, onMouseWheelUp, inputTarget, BindLifetime.Manual);
            InputManager.Bind(InputEventType.OnMouseWheelDown, onMouseWheelDown, inputTarget, BindLifetime.Manual); 

            SpriteManager = new SpriteManager(widescreenAware);

            SpriteManagerBelow = new SpriteManager(widescreenAware);

            scrollDraggerV = new pSprite(GameBase.WhitePixel, Fields.TopLeft,
                                         Origins.TopLeft, Clocks.Game, Vector2.Zero, 1.1f, true, Color.White);
            scrollDraggerV.HandleInput = true;
            scrollDraggerV.HoverEffect = new Transformation(Color.White, Color.YellowGreen, 0, 0);
            scrollDraggerV.ViewOffsetImmune = true;
            scrollDraggerV.Scale = 1.6f;

            SpriteManager.Add(scrollDraggerV);

            HeaderHeight = headerHeight;

            SetContentDimensions(contentDimensions);

            SetDisplayRectangle(displayRectangle);
        }

        internal bool MouseContained;
        private bool enableDragger;


        const float mouse_sensitivity = 0.6f;
        public float ScrollMultiplier = 1;

        private bool onMouseWheelDown(object sender, EventArgs e)
        {
            if (!Visible || !MouseContained || SpriteManager.ClickHandledSprite != null)
                return false;

            if (ScrollVelocity < 0)
                ScrollVelocity = 0;

            ScrollDistance(100 * ScrollMultiplier);
            return true;
        }

        private bool onMouseWheelUp(object sender, EventArgs e)
        {
            if (!Visible || !MouseContained || SpriteManager.ClickHandledSprite != null)
                return false;

            if (ScrollVelocity > 0)
                ScrollVelocity = 0;

            ScrollDistance(-100 * ScrollMultiplier);
            return true;
        }

        internal void SetContentDimensions(Vector2 contentDimensions)
        {
            ContentDimensions = contentDimensions;
            SetScrollPosition(ScrollPosition);
        }

        private void UpdateDraggerV()
        {
            enableDragger = ScrollDragger && ContentDimensions.Y > ActualDisplayHeight;

            scrollDraggerV.HandleInput = enableDragger;

            if (enableDragger)
            {
                scrollDraggerV.VectorScale =
                    new Vector2(6, Math.Max(14, (ActualDisplayHeight / ContentDimensions.Y) * ActualDisplayHeight - 4));

                scrollDraggerV.Position = new Vector2(ScrollDraggerOnLeft ? 2 + ScrollbarOffset : DisplayRectangle.Width - 8 - ScrollbarOffset,
                    HeaderHeight + 2 + (ScrollPosition.Y / (ContentDimensions.Y - ActualDisplayHeight)) * (ActualDisplayHeight - scrollDraggerV.VectorScale.Y - 4));
            }
        }

        internal Vector2 DisplayRectangleExtension = Vector2.Zero;

        internal void SetDisplayRectangle(RectangleF displayRectangle)
        {
            DisplayRectangle = displayRectangle;

            displayRectangle.Width += DisplayRectangleExtension.X;
            displayRectangle.Height += DisplayRectangleExtension.Y;

            SpriteManager.SetVisibleArea(displayRectangle);
            SpriteManagerBelow.SetVisibleArea(displayRectangle);
            Children.ForEach(s => s.SetVisibleArea(displayRectangle));

            SetContentDimensions(ContentDimensions);
        }

        internal void SetScrollPosition(Vector2 scrollPosition, float clamp = 0)
        {
            ScrollPosition = new Vector2(ScrollDraggerOnLeft && ScrollDragger && ScrollDraggerPadding ? -8 : 0, OsuMathHelper.Clamp(scrollPosition.Y, ClampingStart, Math.Max(ClampingStart, ContentDimensions.Y - ActualDisplayHeight)) * clamp + scrollPosition.Y * (1 - clamp));

            if (float.IsNaN(ScrollPosition.X))
                ScrollPosition.X = 0;
            if (float.IsNaN(ScrollPosition.Y))
                ScrollPosition.Y = ClampingStart;

            SpriteManager.ViewOffset = ScrollPosition;
            SpriteManagerBelow.ViewOffset = ScrollPosition;

            Children.ForEach(s => s.ScrollOffset = ScrollPosition);

            UpdateDraggerV();
        }

        public void Show()
        {
            Alpha = 1;
        }

        internal void Hide()
        {
            Alpha = 0;
        }

        bool wdragChecked;

        private bool onEndDrag(object sender, EventArgs e)
        {
            dragChecked = false;

            if (IsDragging)
            {
                if (scrollingVerticallyReversed)
                {
                    lastMouse = Vector2.Zero;
                }

                // 2 30FPS frames are tolerated. Afterwards penalty is applied. This is due to touch screens / windows
                // often not firing a mouse up event immediately after stopping movement.
                ScrollVelocity *= (float)Math.Pow(0.95, Math.Max(0, accumulatedTimeSinceLastMovement - 66));
                accumulatedTimeSinceLastMovement = 0;

                IsDragging = false;

                if (FinishedScrolling != null)
                    FinishedScrolling();
            }

            return false;
        }

        Vector2 lastMouse;
        private bool dragChecked;

        private double accumulatedTimeSinceLastMovement = 0;

        private bool onDrag(object sender, EventArgs e)
        {
            if (!Visible || dragChecked) return false;

            if (InputManager.LastClickHandled) return false;

            if (!IsDragging)
            {
                if (scrollDraggerV.Hovering || (AllowDragScrolling && MouseContained))
                {
                    IsDragging = true;
                    accumulatedTimeSinceLastMovement = 0;

                    scrollingVerticallyReversed = !scrollDraggerV.Hovering;
                    ScrollMouseOffset = MouseManager.MousePosition * GameBase.WindowRatioScaleDown;

                    // Needs to account for the clamping factor of 0.5 we are going to use below!
                    scrollStartPosition = ScrollPosition + new Vector2(0, ScrollPosition.Y - clampedPosition.Y);
                }
                else
                {
                    dragChecked = true;
                    return false;
                }
            }

            if (IsDragging)
            {
                const float THROWING_SCROLL_DECAY = 0.996f; // Per millisecond!

                // Exponential scroll decay. It's more decay when dragging at lower speeds.
                scrollDeceleration = ScrollVelocity == 0 ? 0.5f :  -(float)Math.Max(0.5, THROWING_SCROLL_DECAY - 0.002 / Math.Abs(ScrollVelocity));

                Vector2 newMouse = MouseManager.MousePosition * GameBase.WindowRatioScaleDown;

                if (lastMouse != Vector2.Zero && GameBase.ElapsedMilliseconds > 0)
                {
                    // Speed needs to be accumulated over multiple frames due to framerate not being synced
                    // with the rate at which input devices report movement.
                    accumulatedTimeSinceLastMovement += GameBase.ElapsedMilliseconds;

                    float velocity = (float)((lastMouse.Y - newMouse.Y) / accumulatedTimeSinceLastMovement);

                    // We want to quickly adapt to new high speeds or direction changes but not too rapidly
                    // slow down. This is because before lifting the fingers / mouse button / pen we naturally
                    // slow down slightly.
                    bool highDecay =
                        Math.Sign(velocity) == -Math.Sign(ScrollVelocity) ||
                        Math.Abs(velocity) > Math.Abs(ScrollVelocity);

                    float decay = (float)Math.Pow(highDecay ? 0.90 : 0.95, accumulatedTimeSinceLastMovement);
                    if (velocity != 0)
                    {
                        accumulatedTimeSinceLastMovement = 0;
                        ScrollVelocity = ScrollVelocity * decay + (1f - decay) * velocity;
                    }
                }

                lastMouse = newMouse;

                if (scrollingVerticallyReversed)
                    SetScrollPosition(scrollStartPosition + (ScrollMouseOffset - (MouseManager.MousePosition * GameBase.WindowRatioScaleDown)), 0.5f);
                else
                    SetScrollPosition(scrollStartPosition + ((MouseManager.MousePosition * GameBase.WindowRatioScaleDown) - ScrollMouseOffset) * (ContentDimensions.Y - ActualDisplayHeight) / (ActualDisplayHeight - scrollDraggerV.VectorScale.Y - 4), 1);
                return true;
            }

            return false;
        }

        public void Draw()
        {
            if (!Visible) return;

            SpriteManagerBelow.Draw();

            for (int i = Children.Count - 1; i >= 0; i--)
                Children[i].Draw();

            SpriteManager.Draw();
        }

        protected override void Dispose(bool disposing)
        {
            InputManager.Unbind(InputEventType.OnDrag, onDrag, inputTarget);
            InputManager.Unbind(InputEventType.OnEndDrag, onEndDrag, inputTarget);
            InputManager.Unbind(InputEventType.OnMouseWheelUp, onMouseWheelUp, inputTarget);
            InputManager.Unbind(InputEventType.OnMouseWheelDown, onMouseWheelDown, inputTarget);

            SpriteManager.Dispose();
            SpriteManagerBelow.Dispose();
            Children.ForEach(s => s.Dispose());

            base.Dispose(disposing);
        }

        public override void Initialize()
        {
        }

        Vector2 clampedPosition { get { return new Vector2(0, OsuMathHelper.Clamp(ScrollPosition.Y, ClampingStart, Math.Max(ClampingStart, ContentDimensions.Y - ActualDisplayHeight))); } }


        public override void Update()
        {
            if (Visible)
            {
                MouseContained = Visible && SpriteManager.ViewRectangleScaled.Contains(MouseManager.MousePoint.X, MouseManager.MousePoint.Y);
                if (enableDragger && (MouseContained || IsScrolling))
                    scrollDraggerV.FadeIn(50);
                else
                    scrollDraggerV.FadeOut(50);
                SpriteManager.HandleInput = true;
                SpriteManagerBelow.HandleInput = true;
            }
            else
            {
                MouseContained = false;
                SpriteManager.HandleInput = false;
                SpriteManagerBelow.HandleInput = false;
            }

            if (Math.Abs(ScrollVelocity) <= VELOCITY_CUTOFF && clampedPosition == ScrollPosition)
                return;

            LastScrollTime = GameBase.Time;

            float scrollVelocityThisFrame = UpdateScrollVelocity();

            SetScrollPosition(new Vector2(ScrollPosition.X, ScrollPosition.Y + scrollVelocityThisFrame * (float)GameBase.ElapsedMilliseconds));

            if (Math.Abs(scrollVelocityThisFrame) <= VELOCITY_CUTOFF && !IsScrolling && clampedPosition == ScrollPosition)
            {
                if (FinishedScrolling != null)
                    FinishedScrolling();
            }
        }

        private const float SCROLL_DECELERATION_DEFAULT = -0.98f;
        private float scrollDeceleration = SCROLL_DECELERATION_DEFAULT;

        /// <summary>
        /// Scroll to a specified position.
        /// </summary>
        /// <param name="positionY">The target position to scroll to.</param>
        internal void ScrollToPosition(float positionY, float deceleration = 0)
        {
            scrollDeceleration = deceleration;
            if (scrollDeceleration == 0)
            {
                scrollDeceleration = SCROLL_DECELERATION_DEFAULT;
            }

            float distanceY = positionY - ScrollPosition.Y;

            if (scrollDeceleration < 0)
            {
                // Solve equation dist = -v / ln(-scrollDeceleration) for dist.
                ScrollVelocity = Math.Sign(distanceY) * VELOCITY_CUTOFF - distanceY * (float)Math.Log(-scrollDeceleration);
            }
            else
            {
                // Solve algebraic equation by ScrollVelocityTarget:
                //      Math.Abs(distanceToTravel) = (ScrollVelocityTarget / VELOCITY_LOSS_PER_MILLISECOND) * (ScrollVelocityTarget / 2)
                ScrollVelocity = Math.Sign(distanceY) * (float)Math.Sqrt(Math.Abs(distanceY * scrollDeceleration * 2));
            }
        }

        internal float TargetHeight
        {
            get
            {
                if (scrollDeceleration == 0)
                {
                    return 0;
                }

                float result;

                // Exponential decay mode
                if (scrollDeceleration < 0)
                {
                    result = ScrollPosition.Y + (Math.Sign(ScrollVelocity) * VELOCITY_CUTOFF - ScrollVelocity) / (float)Math.Log(-scrollDeceleration);
                }
                else
                {
                    result = ScrollPosition.Y + (ScrollVelocity / scrollDeceleration) * (ScrollVelocity / 2.0f) * Math.Sign(ScrollVelocity);
                }

                if (Single.IsNaN(result))
                {
                    throw new Exception("NaN in TargetHeight. " + ScrollPosition.Y + " " + ScrollVelocity + " " + scrollDeceleration);
                }

                return result;
            }
        }

        /// <summary>
        /// Scroll a specified distance.
        /// </summary>
        /// <param name="positionY">The distance to scroll to.</param>
        internal void ScrollDistance(float distanceY, float deceleration = 0)
        {
            // Reverse target position out of current velocity (physically correct modelling for the win!)
            float previousTargetPosition = TargetHeight;
            ScrollToPosition(previousTargetPosition + distanceY, deceleration);
        }


        internal void ScrollToBottom(float deceleration = 0)
        {
            float pos = Math.Max(ClampingStart, ContentDimensions.Y - ActualDisplayHeight);
            ScrollToPosition(pos, deceleration);
        }

        internal void ResetScrollVelocity()
        {
            ScrollVelocity = 0;
            scrollDeceleration = SCROLL_DECELERATION_DEFAULT;
        }


        /// <summary>
        /// Controls how "strong" the clamping is.
        /// </summary>
        internal double ClampingForceFactor = 2;

        private const float VELOCITY_CUTOFF = 0.01f;

        private float UpdateScrollVelocity()
        {
            if (IsDragging)
            {
                return 0;
            }

            float scrollVelocityThisFrame = ScrollVelocity;

            // Handle clamping "force"
            float clampedDifference = clampedPosition.Y - ScrollPosition.Y;
            if (clampedDifference != 0 && !IsDragging)
            {
                double clampingDecay = 1.0 - 0.2 * ClampingForceFactor;

                // We only want to dampen the direction we are going if we are going AWAY from the clamping region
                if (Math.Sign(ScrollVelocity) != Math.Sign(clampedDifference))
                {
                    ScrollVelocity *= (float)Math.Pow(clampingDecay, GameBase.FrameRatio);
                }

                // See calculus. (integrate 0.8^t between a and b and divide by b-a to obtain average)
                scrollVelocityThisFrame = (ScrollVelocity != scrollVelocityThisFrame && GameBase.FrameRatio > 0) ? ((ScrollVelocity - scrollVelocityThisFrame) / (float)(GameBase.FrameRatio * Math.Log(clampingDecay))) : ScrollVelocity;
            }
            else if (scrollDeceleration < 0)
            {
                float timeToCutoff = Math.Abs(ScrollVelocity) <= VELOCITY_CUTOFF ? 0 : (float)Math.Log(VELOCITY_CUTOFF / Math.Abs(ScrollVelocity), -scrollDeceleration);
                float elapsedTime = (float)Math.Min(timeToCutoff, GameBase.ElapsedMilliseconds);
                ScrollVelocity = ScrollVelocity * (float)Math.Pow(-scrollDeceleration, elapsedTime);

                // See calculus. (integrate 0.8^t between a and b and divide by b-a to obtain average)
                scrollVelocityThisFrame = (elapsedTime > 0 && ScrollVelocity != scrollVelocityThisFrame) ? ((ScrollVelocity - scrollVelocityThisFrame) / (float)(elapsedTime * Math.Log(-scrollDeceleration))) : ScrollVelocity;
            }
            else if (scrollDeceleration > 0)
            {
                // Since the velocity is decreasing linearly we can simply take the average between the start and end velocity to obtain the correct distance travelled in this frame.
                // We need however to consider the case where the velocity hits zero during this very frame to avoid overshooting.
                float TimeUntilZero = (float)Math.Abs(ScrollVelocity) / scrollDeceleration;

                if (ScrollVelocity > 0)
                    ScrollVelocity = Math.Max(0, ScrollVelocity - scrollDeceleration * (float)GameBase.ElapsedMilliseconds);
                else if (ScrollVelocity < 0)
                    ScrollVelocity = Math.Min(0, ScrollVelocity + scrollDeceleration * (float)GameBase.ElapsedMilliseconds);

                scrollVelocityThisFrame = (scrollVelocityThisFrame + ScrollVelocity) * Math.Min(TimeUntilZero / (float)GameBase.ElapsedMilliseconds, 1f) / 2;
            }

            if (Math.Abs(scrollVelocityThisFrame) <= VELOCITY_CUTOFF && Math.Abs(ScrollVelocity) <= VELOCITY_CUTOFF && Math.Abs(clampedDifference) < 0.5)
            {
                SetScrollPosition(clampedPosition);
                scrollVelocityThisFrame = 0;
                ScrollVelocity = 0;

                // Revert deceleration back to default after coming to a halt. Otherwise "throwing" the scrollable area will be veeeery off.
                scrollDeceleration = SCROLL_DECELERATION_DEFAULT;
            }
            else if (GameBase.ElapsedMilliseconds > 0)
            {
                float distanceToShrink = clampedDifference - clampedDifference * (float)Math.Pow(1.0 - 0.005 * ClampingForceFactor, GameBase.ElapsedMilliseconds);
                scrollVelocityThisFrame += distanceToShrink / (float)GameBase.ElapsedMilliseconds;
            }

            return scrollVelocityThisFrame;
        }


        public void ClearSprites(bool resetDimensions = true)
        {
            SpriteManager.Clear();
            SpriteManagerBelow.Clear();
            SpriteManager.Add(scrollDraggerV);
            if (resetDimensions) SetContentDimensions(Vector2.Zero);

            Children.ForEach(s => s.Clear());
        }

        internal void AddChild(SpriteManager sm)
        {
            sm.SetVisibleArea(DisplayRectangle);
            sm.HandleOverlayInput = HandleOverlayInput;

            Children.Add(sm);
        }

        public float Alpha
        {
            get
            {
                return SpriteManager.Alpha;
            }

            set
            {
                SpriteManager.Alpha = value;
                SpriteManagerBelow.Alpha = value;
                Children.ForEach(s => s.Alpha = value);
            }
        }

        /// <summary>
        /// Re-binds mouse event handlers to be later in the list than content.
        /// Really need a better way to do this.
        /// </summary>
        internal void Rebind()
        {
            InputManager.Unbind(InputEventType.OnDrag, onDrag, inputTarget);
            InputManager.Unbind(InputEventType.OnEndDrag, onEndDrag, inputTarget);
            InputManager.Unbind(InputEventType.OnMouseWheelUp, onMouseWheelUp, inputTarget);
            InputManager.Unbind(InputEventType.OnMouseWheelDown, onMouseWheelDown, inputTarget);

            InputManager.Bind(InputEventType.OnDrag, onDrag, inputTarget, BindLifetime.Manual);
            InputManager.Bind(InputEventType.OnEndDrag, onEndDrag, inputTarget, BindLifetime.Manual);
            InputManager.Bind(InputEventType.OnMouseWheelUp, onMouseWheelUp, inputTarget, BindLifetime.Manual);
            InputManager.Bind(InputEventType.OnMouseWheelDown, onMouseWheelDown, inputTarget, BindLifetime.Manual); 
        }
    }
}