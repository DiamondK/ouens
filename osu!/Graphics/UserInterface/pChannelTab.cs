﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;

namespace osu.Graphics.UserInterface
{
    class pChannelTab : pTab
    {
        internal pSprite closeButton;
        internal pSprite closeButtonOver;
        
        internal bool canClose;

        internal pChannelTab(string name, object tag, Vector2 position, float depth, bool flip, bool skinnable, Transformation hoverEffect, bool canClose)
            : base(name, tag, position, depth, flip, skinnable, hoverEffect)
        {
            this.canClose = canClose;

            if (canClose)
            {
                closeButton = new pSprite(SkinManager.Load(@"selection-tab-close", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft, Clocks.Game, position, depth + 0.0002f, true, new Color(255, 255, 255, 1));
                closeButton.OriginPosition = new Vector2(-49, 8);
                closeButton.Tag = this;
                SpriteCollection.Add(closeButton);

                closeButtonOver = new pSprite(SkinManager.Load(@"selection-tab-close", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft, Clocks.Game, position, depth + 0.0002f, true, new Color(255, 255, 255, 1));
                closeButtonOver.OriginPosition = new Vector2(-49, 8);
                closeButtonOver.HandleInput = true;
                closeButtonOver.OnClick += closeButton_OnClick;
                closeButtonOver.Tag = this;
                closeButtonOver.HoverPriority = -10;
                closeButtonOver.OnHover += delegate { closeButtonOver.Alpha = tabBackground.Alpha * 1; closeButtonOver.InitialColour = Color.White; };
                closeButtonOver.OnHoverLost += delegate { closeButtonOver.Alpha = tabBackground.Alpha * 0.01f; closeButtonOver.InitialColour = new Color(255, 255, 255, 1); };
                SpriteCollection.Add(closeButtonOver);

                tabBackground.OnHover += tabBackground_OnHover;
                tabBackground.OnHoverLost += tabBackground_OnHoverLost;
            }
        }

        void tabBackground_OnHoverLost(object sender, EventArgs e)
        {
            closeButton.InitialColour = new Color(255, 255, 255, 1);
            closeButton.Alpha = 0.01f;
        }

        void tabBackground_OnHover(object sender, EventArgs e)
        {
            closeButton.InitialColour = Color.White;
            closeButton.Alpha = 0.4f;
        }

        internal event EventHandler OnCloseTriggered;

        void closeButton_OnClick(object sender, EventArgs e)
        {
            AudioEngine.Click(null, @"click-close");
            if (OnCloseTriggered != null)
                OnCloseTriggered(this, null);
        }
    }
}
