﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;

namespace osu.Graphics.UserInterface
{
    internal class pTabCollectionRearrangeable : pTabCollection
    {
        //note: this is specifically used for chat display.
        //need to fix input handling if using elsewhere or you will run into binding issues.

        internal pTabCollectionRearrangeable(SpriteManager spriteManager, int tabsWide, Vector2 bottomLeft, float baseDepth, bool skinnable, Color colour)
            : base(spriteManager, tabsWide, bottomLeft, baseDepth, skinnable, colour)
        {
            InputManager.Bind(InputEventType.OnDrag, onDrag, InputTargetType.Default, BindLifetime.Permanent);
            InputManager.Bind(InputEventType.OnClickUp, onClickUp, InputTargetType.Default, BindLifetime.Permanent);
        }

        protected override pTab createTab(string name, object tag, Vector2 position, float depth)
        {
            pTab tab = base.createTab(name, tag, position, depth);
            return tab;
        }

        protected override void tab_OnClick(object sender, EventArgs e)
        {
            base.tab_OnClick(sender, e);

            draggingTab = sender as pTab;

            lastSwitchX = MouseManager.MousePosition.X;
        }

        protected pTab draggingTab;
        pTab lastSwitchTab;
        float lastSwitchX;
        int lastSwitchTime;
        int direction;

        bool onDrag(object sender, EventArgs e)
        {
            pTab hoveringTab = null;

            foreach (pTab tab in Tabs)
                if (tab != draggingTab && tab.tabBackground.Hovering)
                {
                    hoveringTab = tab;
                    break;
                }

            if (hoveringTab != null
                && (lastSwitchX == 0 || Math.Abs(MouseManager.MousePosition.X - lastSwitchX) > 20 || GameBase.Time - lastSwitchTime > 500) //make sure the cursor has moved sufficiently
                && GameBase.Time - lastSwitchTime > 100) //make sure we don't get any race conditions during vertical switches
            {
                if (lastSwitchTab == hoveringTab && direction == (MouseManager.MousePosition.X > lastSwitchX ? 1 : -1) && GameBase.Time - lastSwitchTime < 500)
                    return true;

                lastSwitchTab = hoveringTab;

                lastSwitchTime = GameBase.Time;

                int oldIndex = Tabs.IndexOf(draggingTab);
                int newIndex = Tabs.IndexOf(hoveringTab);

                if (draggingTab != null)
                {
                    Tabs.Remove(draggingTab);

                    direction = MouseManager.MousePosition.X > lastSwitchX ? 1 : -1;
                    lastSwitchX = MouseManager.MousePosition.X;

                    Tabs.Insert(newIndex, draggingTab);
                }

                Resort();

                return true;
            }

            return draggingTab != null;
        }

        public override void Remove(pTab t)
        {
            base.Remove(t);
            onClickUp(this, null);
        }

        bool onClickUp(object sender, EventArgs e)
        {
            draggingTab = null;
            lastSwitchTab = null;
            lastSwitchX = 0;
            return false;
        }
    }
}