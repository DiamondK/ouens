﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu_common.Helpers;

namespace osu.Graphics.UserInterface
{
    internal class BackButton
    {
        internal List<pSprite> SpriteCollection = new List<pSprite>();

        const float layer_left_idle = 22;
        const float layer_left_active = 40;

        const float layer_right_idle = 60;
        const float layer_right_active = 86;

        const float centre_offset = 2.4f;

        const int animation_delay = 600;

        const float vertical_offset = 21;

        /// <summary>
        /// depending on localisation we may need to give the text a bit more room.
        /// </summary>
        float layerRightTextAllowance;

        Color colour_idle = new Color(238, 51, 153);
        Color colour_active = new Color(187, 17, 119);

        private pText text;
        private pTextAwesome icon;
        private pSprite layerLeft;
        private pSprite layerRight;

        EventHandler Action;

        public BackButton(EventHandler action, bool skinnable = true, bool dialog = false)
        {
            Action = action;

            pTexture[] textures = null;

            if (skinnable)
                textures = SkinManager.LoadAll(@"menu-back", SkinSource.Osu | SkinSource.Skin, true);

            if (textures != null && textures.Length > 0)
            {
                //old back button behaviour
                pAnimation back = new pAnimation(textures, Fields.TopLeft, Origins.BottomLeft, Clocks.Game, new Vector2(0, 480), 0.9F, true, Color.White);
                back.SetFramerateFromSkin();
                back.ViewOffsetImmune = true;
                SpriteCollection.Add(back);

                pAnimation back2 = new pAnimation(textures, Fields.TopLeft, Origins.BottomLeft, Clocks.Game, new Vector2(0, 480), 0.91F, true, Color.White);
                if (dialog)
                    //Force an initial alpha
                    back2.FadeTo(0, 0);
                back2.Alpha = 0.01f;
                back2.SetFramerateFromSkin();

                back2.OnClick += back_OnClick;
                back2.HandleInput = true;
                back2.ViewOffsetImmune = true;
                back2.Additive = true;
                back2.HoverEffect = new Transformation(TransformationType.Fade, 1 / 255f, 0.4f, 0, 250);
                SpriteCollection.Add(back2);
            }
            else
            {
                SpriteCollection.Add(text = new pText(LocalisationManager.GetString(OsuString.General_Back).ToLower(), 15, new Vector2(0, vertical_offset), 0.91F, true, Color.White)
                {
                    Field = Fields.BottomLeft,
                    Origin = Origins.Centre,
                    ViewOffsetImmune = true
                });

                float backTextWidth = text.MeasureText().X;
                if (backTextWidth > layer_right_idle - layer_left_idle)
                    layerRightTextAllowance = backTextWidth - (layer_right_idle - layer_left_idle);

                SpriteCollection.Add(layerLeft = new pSprite(SkinManager.Load(@"back-button-layer", SkinSource.Osu), Fields.BottomLeft, Origins.BottomRight, Clocks.Game, new Vector2(0, 0), 0.91F, true, colour_idle)
                {
                    ViewOffsetImmune = true
                });

                SpriteCollection.Add(icon = new pTextAwesome(FontAwesome.chevron_circle_left, 18, new Vector2(0, vertical_offset))
                {
                    Depth = 0.92f,
                    Field = Fields.BottomLeft,
                });

                icon.OnUpdate += delegate
                {
                    if (AudioEngine.SyncNewBeat)
                    {
                        icon.Scale = expanded ? 1.5f : 0.9f;
                        Transformation tr = icon.ScaleTo(1, (int)AudioEngine.beatLength * 3, EasingTypes.OutElastic);
                        if (tr != null)
                        {
                            tr.Time1 -= (int)AudioEngine.beatLength / 4;
                            tr.Time2 -= (int)AudioEngine.beatLength / 4;
                        }
                    }
                };

                SpriteCollection.Add(layerRight = new pSprite(SkinManager.Load(@"back-button-layer", SkinSource.Osu), Fields.BottomLeft, Origins.BottomRight, Clocks.Game, new Vector2(0, 0), 0.9F, true, colour_idle)
                {
                    HandleInput = true,
                    ViewOffsetImmune = true
                });

                layerRight.OnClick += delegate { back_OnClick(this, null); contract(EasingTypes.InOutBack, 400); };
                layerRight.OnHover += delegate { expand(EasingTypes.OutElastic); };
                layerRight.OnHoverLost += delegate { contract(EasingTypes.OutElastic); };

                contract(EasingTypes.OutElastic, 0);
            }
        }

        bool expanded;

        void expand(EasingTypes easing, int delay = animation_delay)
        {
            expanded = true;

            layerLeft.MoveTo(new Vector2(layer_left_active, layerLeft.Position.Y), delay, easing);
            icon.MoveTo(new Vector2(layer_left_active / centre_offset, icon.Position.Y), delay, easing);

            layerRight.MoveTo(new Vector2(layer_right_active + layerRightTextAllowance, layerRight.Position.Y), delay, easing);
            text.MoveTo(new Vector2((layer_right_active + layerRightTextAllowance - layer_left_active) / centre_offset + layer_left_active, text.Position.Y), delay, easing);

            layerLeft.FadeColour(colour_active, delay, false, easing);
        }

        void contract(EasingTypes easing, int delay = animation_delay)
        {
            expanded = false;

            layerLeft.MoveTo(new Vector2(layer_left_idle, layerLeft.Position.Y), delay, easing);
            icon.MoveTo(new Vector2(layer_left_idle / centre_offset, icon.Position.Y), delay, easing);

            layerRight.MoveTo(new Vector2(layer_right_idle + layerRightTextAllowance, layerRight.Position.Y), delay, easing);
            text.MoveTo(new Vector2((layer_right_idle + layerRightTextAllowance - layer_left_idle) / centre_offset + layer_left_idle, text.Position.Y), delay, easing);

            layerLeft.FadeColour(colour_idle, delay, false, easing);
        }

        private void back_OnClick(object sender, EventArgs e)
        {
            if (Action != null) Action(this, null);
        }

    }
}
