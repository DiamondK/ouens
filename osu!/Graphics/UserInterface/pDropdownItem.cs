
namespace osu.Graphics.UserInterface
{
    internal class pDropdownItem
    {
        public pDropdownItem(string text, object value)
        {
            this.Text = text;
            this.Value = value;
        }

        public override string ToString()
        {
            return Text;
        }

        internal string Text;
        internal object Value;
    }
}