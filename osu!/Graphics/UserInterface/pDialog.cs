﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Graphics.Renderers;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Configuration;
using osu.Online;
using osu.Online.Social;

namespace osu.Graphics.UserInterface
{
    internal class pDialog : GameComponent
    {
        internal float currentVerticalSpace;
        internal bool IsDisplayed = false;
        internal bool GameModeSpecific;
        internal SpriteManager spriteManager = new SpriteManager(true) { FirstDraw = false };

        internal bool DontDrawBehind;

        byte FINAL_ALPHA = 235;

        internal bool AllowTransparentDrawing
        {
            get
            {
                return (!DontDrawBehind && !ConfigManager.sMyPcSucks) || Background.Alpha < (FINAL_ALPHA - 1) / 255f;
            }
        }

        bool chatToggle;

        internal pDialog(string title, bool modeSpecific)
            : base(GameBase.Instance)
        {
            Background =
                new pSprite(GameBase.WhitePixel, Fields.Native, Origins.TopLeft, Clocks.Game,
                            Vector2.Zero, 0.1F,
                            true,
                            new Color(0, 0, 0, FINAL_ALPHA));
            Background.VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight + GameBase.WindowOffsetY);
            Background.HandleInput = true;

            spriteManager.Add(Background);

            GameModeSpecific = modeSpecific;
            if (!string.IsNullOrEmpty(title))
            {
                pText p = new pText(title, 24, new Vector2(2, 2), new Vector2(GameBase.WindowWidthScaled - 2, 0), 0.92F, true, Color.White, true);
                spriteManager.Add(p);
                currentVerticalSpace = p.MeasureText().Y + p.Position.Y + 50;
            }
        }

        public void Dispose()
        {
            spriteManager.Dispose();
            base.Dispose();
        }

        internal virtual void AddSprite(pSprite p)
        {
            spriteManager.Add(p);
        }

        internal virtual void Draw()
        {
            if (!IsDisplayed) return;
            spriteManager.Draw();
        }

        internal int GetTimeOffset()
        {
            return IsDisplayed ? GameBase.Time : 0;
        }

        protected virtual bool hideChatOnDisplay { get { return true; } }

        internal virtual void Display()
        {
            if (IsDisplayed)
                return;

            if (ChatEngine.IsVisible && hideChatOnDisplay)
            {
                ChatEngine.Toggle(true);
                chatToggle = true;
            }

            for (int i = spriteManager.SpriteList.Count - 1; i > -1; i--)
            {
                pDrawable s = spriteManager.SpriteList[i];
                
                if (s.Transformations.Count > 0 && s.Transformations[0].Time2 < GameBase.Time)
                    s.Transformations.ForEach(t => { t.Time1 += GameBase.Time; t.Time2 += GameBase.Time; });

                if (s.Transformations.Count == 0 && s.InitialColour != Color.TransparentWhite)
                    s.FadeInFromZero(300);
            }

            IsDisplayed = true;
        }

        internal event EventHandler Closed;
        private event EventHandler cancelAction;

        internal void AddCancel(string text, Color textColour, EventHandler onClick)
        {
            cancelAction += onClick;
            AddOption(text, textColour, delegate { Close(true); }, false);
        }


        List<pSprite> options = new List<pSprite>();
        private bool AddOdd;
        internal pSprite Background;
        internal Vector2 ButtonOffset;

        internal void AddOption(string text, Color textColour, EventHandler onClick, bool closePopup = true, bool halfWidth = false, bool leftHalf = true)
        {
            pButton pbut = new pButton((options.Count + 1) + ". " + text, new Vector2(GameBase.WindowWidthScaled / 2 - 230 + (leftHalf ? 0 : 240), currentVerticalSpace) + ButtonOffset, new Vector2(halfWidth ? 220 : 460, 40), 0.92f, textColour, onClick, GameModeSpecific);
            ((pText)pbut.SpriteCollection[3]).TextShadow = true;
            for (int i = 0; i < 4; i++)
            {
                pSprite p = pbut.SpriteCollection[i];

                if (i < 3 && closePopup)
                {
                    p.OnClick += option_DoClose;
                    p.Tag = pbut;
                }

                if (IsDisplayed)
                    p.FadeInFromZero(140);
                else
                {
                    int start = options.Count * 60;
                    p.Alpha = 0;
                    p.Transformations.Add(new Transformation(p.InitialPosition + new Vector2(AddOdd ? -40 : 40, 0), p.InitialPosition, start, start + 800, EasingTypes.OutBounce));
                    p.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, start, start + 800, EasingTypes.Out));
                    p.Position = p.Position + new Vector2(AddOdd ? -40 : 40, 0);
                }

                spriteManager.Add(p);
            }

            options.Add(pbut.SpriteCollection[0]);

            AddOdd = !AddOdd;

            currentVerticalSpace += 50;
        }

        private void option_DoClose(object sender, EventArgs e)
        {
            Close();
            pSprite s = sender as pSprite;
            pButton b = s != null ? s.Tag as pButton : sender as pButton;
            if (b != null)
                b.SpriteCollection.ForEach(sp => sp.FadeOut(600));
        }

        internal virtual void Close(bool isCancel = false)
        {
            if (!IsDisplayed) return;

            foreach (pSprite p in spriteManager.SpriteList)
            {
                p.FadeOut(120);
                p.HandleInput = false;
                p.AlwaysDraw = false;
            }

            if (isCancel && cancelAction != null) cancelAction(this, null);
            if (Closed != null) Closed(this, null);

            if (chatToggle && hideChatOnDisplay)
                ChatEngine.Toggle(true);
            IsDisplayed = false;

            Dispose();
        }

        public override void Initialize()
        {

        }

        public override void Update()
        {
            if (ConfigManager.sBloom && IsDisplayed)
            {
                BloomRenderer.ExtraPass = true;
                BloomRenderer.Magnitude = 0.004f;
                BloomRenderer.Additive = false;
                BloomRenderer.HiRange = true;
                BloomRenderer.HiRange = false;
                BloomRenderer.Alpha = (float)spriteManager.SpriteList[0].Alpha;
            }

            base.Update();
        }

        internal virtual void DisplayAfter() { }

        protected override void Dispose(bool disposing)
        {
            if (IsDisplayed) Close();
            Closed = null;
            base.Dispose(disposing);
        }

        internal virtual bool HandleKey(Keys k)
        {
            int option;

            switch (k)
            {
                case Keys.Enter:
                    Close();
                    return true;
                case Keys.Escape:
                    Close(true);
                    return true;
            }

            if (Int32.TryParse(k.ToString().Replace(@"D", @""), out option))
            {
                if (option < options.Count + 1 && option > 0)
                {
                    options[option - 1].Click(true);
                    return true;
                }
            }

            return false;
        }

        internal virtual void HandleMouseWheelUp()
        {

        }

        internal virtual void HandleMouseWheelDown()
        {

        }
    }
}