﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Graphics.Sprites;

namespace osu.Graphics.UserInterface
{
    internal class pTabCollection
    {
        private readonly float baseDepth;
        protected readonly Transformation hoverEffect;
        private readonly Transformation hoverEffectAlerted;
        internal Vector2 BottomLeft;
        protected pTab defaultTab;

        /// <summary>
        /// Tabs will hang down from a surface, rather than protrude upwards.
        /// </summary>
        internal bool Downwards;

        internal pTab SelectedTab;
        internal int MaxTabsWide;
        protected int secondRowOffset = 0;
        internal bool Skinnable;
        internal SpriteManager SpriteManager;
        internal List<pSprite> Sprites = new List<pSprite>();
        internal Color TabColour;
        internal Color TabColourAlert;
        internal bool RightToLeft = false;

        internal List<pTab> Tabs = new List<pTab>();

        internal pTabCollection(SpriteManager spriteManager, int tabsWide, Vector2 bottomLeft, float baseDepth, bool skinnable, Color colour, int secondRowOffset = 0, bool rightToLeft = false)
        {
            this.secondRowOffset = secondRowOffset;
            this.SpriteManager = spriteManager;
            this.baseDepth = baseDepth;
            Skinnable = skinnable;
            MaxTabsWide = tabsWide;
            this.BottomLeft = bottomLeft;
            RightToLeft = rightToLeft;
            TabColour = colour;
            TabColourAlert = new Color((byte)Math.Min(255, TabColour.R * 1.8f),
                                       (byte)Math.Min(255, TabColour.G * 1.8f),
                                       (byte)Math.Min(255, TabColour.B * 1.8f));

            hoverEffect = new Transformation(TabColour,
                                             new Color((byte)(Math.Min(255, TabColour.R + 20)),
                                                       (byte)(Math.Min(255, TabColour.G + 50)),
                                                       (byte)(Math.Min(255, TabColour.B + 50))), 0, 80);
            hoverEffectAlerted = new Transformation(TabColourAlert,
                                                    new Color((byte)(Math.Min(255, TabColourAlert.R + 20)),
                                                              (byte)(Math.Min(255, TabColourAlert.G + 50)),
                                                              (byte)(Math.Min(255, TabColourAlert.B + 50))), 0, 80);
        }

        internal event EventHandler OnTabChanged;
        protected const int DEFAULT_SEPARATION_HEIGHT = 13;
        protected int separationHeight = DEFAULT_SEPARATION_HEIGHT;
        internal float TextSize = 1;

        internal virtual pTab Add(string name, object tag, bool insert = false)
        {
            return Add(name, tag, insert ? 0 : Tabs.Count);
        }

        internal virtual pTab Add(string name, object tag, int insertPosition)
        {
            if (Tabs.Find(t => t.Tag == tag) != null) return null;

            Vector2 position = new Vector2(BottomLeft.X + (Tabs.Count % MaxTabsWide) * secondRowOffset + (Tabs.Count / MaxTabsWide) * 10 * (RightToLeft?-1:1),
                                           BottomLeft.Y + 6 - (14 * (1 + Tabs.Count / MaxTabsWide)));

            float depth = baseDepth - Tabs.Count * 0.0002f - (Tabs.Count / MaxTabsWide) * 0.0001F;

            pTab tab = createTab(name, tag, position, depth);
            tab.OnClick += tab_OnClick;

            if (defaultTab == null)
                defaultTab = tab;

            Sprites.AddRange(tab.SpriteCollection);
            SpriteManager.Add(tab.SpriteCollection);

            if (insertPosition < 0) insertPosition = 0;

            Tabs.Insert(insertPosition > Tabs.Count ? Tabs.Count : insertPosition, tab);

            Resort();

            return tab;
        }

        protected virtual pTab createTab(string name, object tag, Vector2 position, float depth)
        {
            return new pTab(name, tag, position, depth, Downwards, Skinnable, hoverEffect, TextSize);
        }

        internal void SetMaxTabsWide(int i)
        {
            MaxTabsWide = i;
            Resort();
        }

        internal virtual void Resort()
        {
            for (int i = 0; i < Tabs.Count; i++)
            {
                pTab t = Tabs[i];
                Vector2 position = new Vector2(BottomLeft.X + (i % MaxTabsWide) * pTab.TAB_WIDTH * (RightToLeft ? -1 : 1) + ((i / MaxTabsWide) % 2) * secondRowOffset,
                                               BottomLeft.Y + 6 - 14 - (separationHeight * (i / MaxTabsWide)));

                float depth = baseDepth - i * 0.0002f - (i / MaxTabsWide) * 0.0001F;

                t.SpriteCollection[0].HoverPriority = (int)(-100 + depth * 100);
                for (int j = 0; j < t.SpriteCollection.Count; j++)
                {
                    pSprite sprite = t.SpriteCollection[j];

                    sprite.InitialPosition = position;
                    sprite.MoveTo(position, 300, EasingTypes.OutElasticQuarter);
                    sprite.Depth = depth + 0.0001f * j + (SelectedTab == t ? 0.001f : 0);
                }
            }

            SpriteManager.ResortDepth();
        }

        internal pTab SetSelected(object tag)
        {
            if (tag == null || tag is pTabAdd) return null;

            return SetSelected(tag, false);
        }

        internal pTab SetSelected(object tag, bool silent, bool unselectIfNotExisting = false)
        {
            pTab tab = GetFromTag(tag);
            if (tab == null && !unselectIfNotExisting) return null;

            SetSelected(tab, silent);
            return tab;
        }

        internal pTab GetFromTag(object tag)
        {
            return Tabs.Find(t => tag.Equals(t.Tag));
        }

        protected virtual void tab_OnClick(object sender, EventArgs e)
        {
            AudioEngine.Click(null, @"click-short-confirm");
            SetSelected((pTab)sender, false);
        }

        internal void SetSelected(pTab tab, bool silent)
        {
            if (SelectedTab == tab)
                return;

            if (tab != null && !tab.AllowSelection) return;

            pTab lastSelected = SelectedTab;
            SelectedTab = tab;

            foreach (pTab ta in Tabs)
            {
                if (ta == SelectedTab)
                {
                    ta.tabBackground.FadeColour(Color.White, 100);
                    ta.tabBackground.HoverEffect = null;
                    ta.Alerting = false;

                    ta.tabText.FadeColour(Color.Black, 20);
                    ta.tabText.TextShadow = false;
                    ta.tabText.TextChanged = true;

                    ta.SpriteCollection.ForEach(p => p.Depth += 0.001F);
                    SpriteManager.RemoveRange(ta.SpriteCollection);
                    SpriteManager.Add(ta.SpriteCollection);
                }
                else
                {
                    if (ta == lastSelected)
                    {
                        ta.tabBackground.HoverEffect = hoverEffect;

                        ta.tabBackground.FadeColour(TabColour, 20);
                        ta.tabText.FadeColour(Color.White, 20);
                        ta.tabText.TextShadow = true;
                        ta.tabText.TextChanged = true;

                        ta.SpriteCollection.ForEach(p => p.Depth -= 0.001F);
                        SpriteManager.RemoveRange(ta.SpriteCollection);
                        SpriteManager.Add(ta.SpriteCollection);
                    }
                }
            }

            if (!silent && OnTabChanged != null)
                OnTabChanged(SelectedTab != null ? SelectedTab.Tag : null, null);
        }

        public void Remove(object tag)
        {
            Remove(GetFromTag(tag));
        }

        public virtual void Remove(pTab t)
        {
            if (t == null) return;

            int index = Tabs.IndexOf(t);

            if (index < 0 || index >= Tabs.Count)
                return;

            Tabs.Remove(t);

            t.SpriteCollection.ForEach(p =>
            {
                Sprites.Remove(p);
                p.AlwaysDraw = false;
                p.FadeOut(200);
            });

            if (SelectedTab == t && Tabs.Count > 0)
            {
                if (index < Tabs.Count && Tabs[index].AllowSelection)
                    SetSelected(Tabs[index], false);
                else if (index > 0)
                    SetSelected(Tabs[index - 1], false);
            }

            Resort();
        }

        public void SetAlert(object tag)
        {
            pTab t = GetFromTag(tag);

            if (t == null || t == SelectedTab) return;

            t.Alerting = true;
            t.tabBackground.FadeColour(TabColourAlert, 100);
            t.tabBackground.HoverEffect = hoverEffectAlerted;
        }
    }
}