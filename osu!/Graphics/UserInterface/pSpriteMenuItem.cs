﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Graphics.Sprites;
using osu.Graphics.Skinning;
using Microsoft.Xna.Framework.Graphics;

namespace osu.Graphics.UserInterface
{
    internal class pSpriteMenuItem : IDisposable
    {
        private Vector2 m_size;
        private Vector2 m_location;
        private bool m_selected;
        private bool m_visible;

        internal List<pSprite> Sprites = new List<pSprite>();

        internal Color BackgroundColour = new Color(0, 0, 0, 160);
        internal Color HighlightColour = new Color(165, 90, 234, 230);
        internal Color SelectedColour = new Color(0, 85, 85, 85);

        internal object Tag;
        internal String TagString;
        internal int TagNumeric;

        internal pSprite Background;
        private pSpriteMenu m_menu; // should never change?
        private SpriteManager m_sprite_manager; // ^

        internal pSpriteMenuItem(pSpriteMenu menu, SpriteManager sprite_manager, Vector2 size, Vector2 location, bool selected)
        {
            m_size = size;
            m_location = location;
            m_menu = menu;
            m_sprite_manager = sprite_manager;
            m_selected = selected;
            m_visible = false;

            Color backcol = selected ? SelectedColour : BackgroundColour;

            Background = new pSprite(SkinManager.Load(@"opaque-white", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft,
                                       Clocks.Game, new Vector2(location.X, location.Y), 0.99f, true, Color.TransparentBlack, null);
            Background.VectorScale = new Vector2((float)size.X / 16.0f, (float)size.Y / 16.0f);

            Background.HoverEffects = new List<Transformation>();
            Background.HoverEffects.Add(new Transformation(backcol, HighlightColour, 0, 10));

            Background.HoverLostEffects = new List<Transformation>();
            Background.HoverLostEffects.Add(new Transformation(HighlightColour, backcol, 0, 100));

            Background.OnClick += Click;

            m_sprite_manager.Add(Background);
        }

        internal Vector2 Size
        {
            get
            {
                return m_size;
            }
            set
            {
                m_size = value;
                Background.VectorScale = new Vector2((float)value.X / 16.0f, (float)value.Y / 16.0f);
            }
        }

        internal Vector2 Location
        {
            get
            {
                return m_location;
            }
            set
            {
                m_location = value;
                Background.MoveTo(new Vector2(value.X, value.Y), 0);
            }
        }

        internal bool Visible
        {
            get
            {
                return m_visible;
            }
            set
            {
                m_visible = value;
                if (value) Show(0);
                else Hide();
            }
        }

        internal bool Selected
        {
            get
            {
                return m_selected;
            }
            set
            {
                if (value)
                {
                    Background.Transformations.Clear();
                    Background.FadeColour(SelectedColour, 0);
                }
                else
                {
                    Background.Transformations.Clear();
                    Background.FadeColour(BackgroundColour, 0);
                }
            }
        }

        internal void Show(int time_offset)
        {
            if (m_visible) return;

            Color backcol = m_selected ? SelectedColour : BackgroundColour;
            foreach (pSprite p in Sprites)
            {
                p.Transformations.Clear();
                p.FadeIn(200);
            }

            Background.Transformations.Clear();
            Background.FadeColour(backcol, 0);
            Background.Transformations.Add(new Transformation(TransformationType.Fade, 0.0f, (float)backcol.A / 255.0f, GameBase.Time + time_offset, GameBase.Time + time_offset + 200));
            Background.IsVisible = true;
            Background.HandleInput = true;

            m_visible = true;
        }

        internal void Hide()
        {
            if (!m_visible) return;

            foreach (pSprite p in Sprites)
            {
                p.Transformations.Clear();
                p.FadeOut(100);
            }

            Background.HandleInput = false;
            Background.Transformations.Clear();
            Background.FadeOut(100);

            m_visible = false;
        }

        internal event EventHandler OnClick;

        private void Click(object sender, EventArgs e)
        {
            if (OnClick != null) OnClick(this, EventArgs.Empty);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                foreach (pSprite p in Sprites)
                {
                    m_sprite_manager.Remove(p);
                    p.Dispose();
                }

                m_sprite_manager.Remove(Background);
                Background.Dispose();
            }
        }

        ~pSpriteMenuItem()
        {
            Dispose(false);
        }
    }
}
