﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Sprites;
using osu.Online.Social;

namespace osu.Graphics.UserInterface
{
    class pChannelTabCollection : pTabCollectionRearrangeable
    {
        internal pChannelTabCollection(SpriteManager spriteManager, int tabsWide, Vector2 bottomLeft, float baseDepth, bool skinnable, Color colour)
            : base(spriteManager, tabsWide, bottomLeft, baseDepth, skinnable, colour)
        {
            Vector2 position = new Vector2(bottomLeft.X + (Tabs.Count % MaxTabsWide) * 74 + (Tabs.Count / MaxTabsWide) * 10,
                                           bottomLeft.Y + 6 - (14 * (1 + Tabs.Count / MaxTabsWide)));
            pTabAdd = new pTabAdd(position, 1, false, false);
            pTabAdd.OnClick += pTabAdd_OnClick;
            Sprites.AddRange(pTabAdd.SpriteCollection);
            spriteManager.Add(pTabAdd.SpriteCollection);
            Tabs.Insert(0, pTabAdd);
        }

        internal override pTab Add(string name, object tag, bool insert = false)
        {
            return base.Add(name, tag, insert ? 0 : Tabs.Count - 1);
        }

        protected override pTab createTab(string name, object tag, Vector2 position, float depth)
        {
            Channel c = tag as Channel;
            bool allowClose = c != null && c.Closeable;

            pChannelTab pt = new pChannelTab(name, tag, position, depth, Downwards, Skinnable, hoverEffect, allowClose);

            pt.OnCloseTriggered += pt_OnCloseTriggered;

            return pt;
        }

        internal event EventHandler OnTabCloseRequested;
        internal event EventHandler OnTabAddRequested;
        private pTabAdd pTabAdd;

        void pt_OnCloseTriggered(object sender, EventArgs e)
        {
            pChannelTab pt = sender as pChannelTab;

            CloseTab(pt);
        }

        internal virtual void CloseTab(pChannelTab pt)
        {
            if (pt == null || !pt.canClose) return;

            Remove(pt);

            if (OnTabCloseRequested != null)
                OnTabCloseRequested(pt.Tag, null);
        }

        void pTabAdd_OnClick(object sender, EventArgs e)
        {
            if (OnTabAddRequested != null)
                OnTabAddRequested(this, null);
        }

        internal void ChangeTabCyclic(int change)
        {
            if (Tabs.Count - 1 == 0)
            {
                SetSelected(Tabs[0], false);
                return;
            }

            int index = (Tabs.IndexOf(SelectedTab) + change + Tabs.Count - 1) % (Tabs.Count - 1);
            SetSelected(Tabs[index], false);
        }


        internal override void Resort()
        {
            if (Tabs.Count == 0) return;

            if (pTabAdd != null && Tabs[Tabs.Count - 1] != pTabAdd)
            {
                Tabs.Remove(pTabAdd);
                Tabs.Add(pTabAdd);
            }

            base.Resort();
        }

        public int ExcessHeight { get { return ((Tabs.Count - 1) / MaxTabsWide) * separationHeight; } }

        internal void UpdateVisibility()
        {
            int newHeight = draggingTab != null || (SpriteManager.CurrentHoverSprite != null && SpriteManager.CurrentHoverSprite.Tag is pTab) ? DEFAULT_SEPARATION_HEIGHT : DEFAULT_SEPARATION_HEIGHT / 2;

            if (newHeight != separationHeight)
            {
                separationHeight = newHeight;
                Resort();
            }
        }
    }
}
