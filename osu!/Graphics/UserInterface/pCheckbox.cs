﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;

namespace osu.Graphics.UserInterface
{
    internal class pCheckbox
    {
        private bool localChecked;
        internal List<pSprite> SpriteCollection = new List<pSprite>();
        private pTexture t_checked;
        private pTexture t_unchecked;
        internal pText sText;
        internal pSprite sBox;
        private bool enabled = true;

        internal bool Enabled
        {
            get { return enabled; }
            set
            {
                if (enabled == value)
                    return;

                enabled = value;

                if (enabled)
                {
                    foreach (pSprite s in SpriteCollection)
                    {
                        s.InitialColour = Color.White;
                        s.HandleInput = true;
                    }
                    sBox.InitialColour = SkinManager.NEW_SKIN_COLOUR_MAIN;
                }
                else
                {
                    foreach (pSprite s in SpriteCollection)
                    {
                        s.InitialColour = Color.Gray;
                        s.HandleInput = false;
                    }
                }
            }

        }

        internal pCheckbox(string text, Vector2 position, float depth, bool defaultStatus) :
            this(text, 1, position, depth, defaultStatus)
        {
        }

        internal string Text
        {
            get { return sText.Text; }

            set { sText.Text = value; }
        }

        private Vector2 pos;
        internal Vector2 Position
        {
            get { return pos; }
            set
            {
                pos = value;
                sBox.Position = pos + new Vector2(8, 8);
                sText.Position = pos + new Vector2(20 * size, 8);
            }
        }

        float size;
        internal pCheckbox(string text, float size, Vector2 position, float depth, bool defaultStatus, float maxWidth = 0)
        {
            pos = position;

            localChecked = defaultStatus;
            this.size = size;

            t_checked = SkinManager.Load(@"circle-full", SkinSource.Osu);
            t_unchecked = SkinManager.Load(@"circle-empty", SkinSource.Osu);

            sBox = new pSprite(localChecked ? t_checked : t_unchecked, Fields.TopLeft, Origins.Centre, Clocks.Game, position + new Vector2(8, 8), depth, true, SkinManager.NEW_SKIN_COLOUR_MAIN);
            sBox.HandleInput = true;
            sBox.Scale = size;
            sBox.OnClick += p_OnClick;
            sBox.ClickRequiresConfirmation = true;

            sText = new pText(text, 15 * size, position + (new Vector2(20 * size, 8)), new Vector2(Math.Max(0, maxWidth - 20 * size), 0), depth, true, Color.White, true);
            sText.Origin = Origins.CentreLeft;
            sText.ClickRequiresConfirmation = true;
            sText.HandleInput = true;
            sText.OnClick += p_OnClick;


            SpriteCollection.Add(sBox);
            SpriteCollection.Add(sText);
        }

        internal bool Checked
        {
            get { return localChecked; }
            set
            {
                if (localChecked != value)
                {
                    localChecked = value;
                    UpdateStatus();
                    if (OnCheckChanged != null)
                        OnCheckChanged(this, Checked);
                }
            }
        }

        public string Tooltip
        {
            get { return sText.ToolTip; }
            set
            {
                sText.ToolTip = value;
                sBox.ToolTip = value;
            }
        }

        public bool Hovering
        {
            get { return SpriteCollection.Exists(p => p.Hovering); }
        }

        private void p_OnClick(object sender, EventArgs e)
        {
            Checked = !Checked;
            AudioEngine.PlaySample(@"check-" + (Checked ? @"on" : @"off"));
        }

        internal void SetStatusQuietly(bool status)
        {
            if (localChecked == status) return;

            localChecked = status;
            UpdateStatus();
        }

        private void UpdateStatus()
        {
            sBox.Texture = Checked ? t_checked : t_unchecked;

            sBox.Transformations.RemoveAll(t => t.Type == TransformationType.Scale);
            if (Checked)
                sBox.Transformations.Add(new Transformation(TransformationType.Scale, size * 1.2f, size, GameBase.Time, GameBase.Time + 350, EasingTypes.Out));
            else
                sBox.Transformations.Add(new Transformation(TransformationType.Scale, size * 0.8f, size, GameBase.Time, GameBase.Time + 350, EasingTypes.Out));
        }

        internal event CheckboxCallbackDelegate OnCheckChanged;

        public void Hide()
        {
            Hide(false);
        }

        public void Hide(bool instant)
        {
            SpriteCollection.ForEach(s => s.FadeOut(instant ? 0 : 20));
        }

        public void Show()
        {
            SpriteCollection.ForEach(s => s.FadeIn(20));
        }

    }

    internal delegate void CheckboxCallbackDelegate(object sender, bool status);
}