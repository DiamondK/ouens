using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;

namespace osu.Graphics.UserInterface
{
    internal class pTabAdd : pTab
    {
        internal override bool AllowSelection
        {
            get
            {
                return false;
            }
        }

        internal pTabAdd(Vector2 position, float depth, bool flip, bool skinnable)
            : base(null, null, position, depth, flip, skinnable, new Transformation(Color.LightGray, Color.White, 0, 200))
        {
            tabBackground.Texture = SkinManager.Load(@"selection-tab-add", SkinSource.Osu);
            tabBackground.InitialColour = Color.LightGray;
            tabBackground.OriginPosition = new Vector2(70, 12);
            tabBackground.HandleInput = true;
            tabBackground.OnClick += tab_OnClick;

        }
    }
}