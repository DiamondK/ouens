﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Renderers;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Input.Handlers;
using osu.Online;
using osu_common.Helpers;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using osu.Online.Social;

namespace osu.Graphics.UserInterface
{
    internal class pTextBox : IDisposable
    {
        internal readonly pText Box;
        internal List<pSprite> SpriteCollection = new List<pSprite>();
        internal Color BorderFocused = Color.White;
        internal int LengthLimit;
        internal bool ResetTextOnEdit;
        internal string textBeforeCommit;

        protected pSprite cursor;
        protected bool isDefaultValue;
        protected bool hasDefaultValue;
        internal bool HasFocus { get; private set; }
        public bool ReadOnly;
        private int selectionStartLast;
        private int selectionStart;

        internal float Height;

        internal TextInputControl InputControl;

        internal delegate void OnCommitHandler(pTextBox sender, bool newText);
        internal event OnCommitHandler OnCommit;
        public event OnCommitHandler OnChange;
        public event OnCommitHandler OnGotFocus;

        protected void InvokeOnCommit(bool newText)
        {
            OnCommitHandler commit = OnCommit;
            if (commit != null) commit(this, newText);
        }

        private void InvokeOnChange(bool newText)
        {
            OnCommitHandler change = OnChange;
            if (change != null) change(this, newText);
        }

        private void InvokeOnGotFocus()
        {
            OnCommitHandler focus = OnGotFocus;
            if (focus != null) focus(this, false);
        }

        internal int TextSize { get; private set; }

        float Length;

        public pTextBox(string text, int size, Vector2 pos, float length, float depth = 1, bool passwordBox = false)
        {
            TextSize = size;

            Length = length;

            Box = new pText(string.Empty, size, pos, new Vector2(length, 0), depth, true, Color.White, false);
            Box.TextBold = true;
            Box.BackgroundColour = backgroundUnfocused;
            Box.TextColour = Color.White;
            Box.TextAlignment = TextAlignment.LeftFixed;
            Box.HandleInput = true;
            Box.ClickRequiresConfirmation = true;
            Box.BorderColour = new Color(180, 180, 180);
            Box.BorderWidth = 2;

            cursor = new pSprite(GameBase.WhitePixel, Fields.TopLeft,
                                 Origins.TopLeft, Clocks.Game, pos, depth + 0.001f, true, Color.TransparentWhite);
            cursor.VectorScale = new Vector2(2, size * 1.5f);
            cursor.Bypass = true;
            cursor.Position.Y += CURSOR_Y_OFFSET;
            SpriteCollection.Add(cursor);

            if (passwordBox)
                InputControl = new PasswordInputControl(this);
            else
                InputControl = new TextInputControl(this);
            InputControl.OnNewImeComposition += InputControl_OnNewImeComposition;
            InputControl.OnImeStart += new VoidDelegate(InputControl_OnImeStart);
            InputControl.OnImeEnd += new VoidDelegate(InputControl_OnImeEnd);

            ImeLine = new pText(string.Empty, size, cursor.Position, depth + 0.002f, true, Color.PaleVioletRed);
            SpriteCollection.Add(ImeLine);

            enabled = true;

            InputControl.pTextBox = this;

            Box.OnClick += box_OnClick;
            InputManager.Bind(InputEventType.OnClick, onClick, InputTargetType.Highest, BindLifetime.Manual); //todo: this is too global
            InputControl.OnNewText += TextInputControl_OnNewText;
            InputControl.OnKey += UpdateCursorPositionScheduled;

            SpriteCollection.Add(Box);

            Text = text;
        }

        void InputControl_OnImeStart()
        {
            killImeLine();
        }

        void InputControl_OnImeEnd()
        {
            killImeLine();
        }

        internal pText ImeLine;

        void InputControl_OnNewImeComposition(string s)
        {
            killImeLine();

            ImeLine.Position = cursor.Position;
            ImeLine.Position.Y -= CURSOR_Y_OFFSET;
            ImeLine.FlashColour(Color.White, 200);
            ImeLine.Text = s;
        }

        ~pTextBox()
        {
            Dispose(true);
        }

        public void Dispose()
        {
            OnChange = null;
            OnGotFocus = null;
            OnCommit = null;

            GameBase.Form.Controls.Remove(InputControl);
            InputControl.Dispose();

            GC.SuppressFinalize(this);
            Dispose(false);
        }

        private void Dispose(bool isDisposing)
        {
            InputManager.Unbind(InputEventType.OnClick, onClick, InputTargetType.Highest);

            InputControl.OnNewText -= TextInputControl_OnNewText;
            InputControl.OnKey -= UpdateCursorPositionScheduled;
            if (GameBase.Mode != OsuModes.Exit)
                FocusLost(false);
        }

        protected virtual void TextInputControl_OnNewText(object sender, EventArgs e)
        {
            if (ReadOnly) return;

            killImeLine();

            if (LengthLimit != 0 && InputControl.Text.Length > LengthLimit && !isDefaultValue)
            {
                InputControl.Text = Box.Text;
                Box.BorderColour = Color.Red;
            }
            else
            {
                Box.TextColour = enabled ? Color.White : new Color(180, 180, 180);
                Box.Text = InputControl.Text;
                Box.BorderColour = enabled ? BorderFocused : new Color(180, 180, 180);
                InvokeOnChange(true);
            }

            updateDisplay();
        }

        private void updateDisplay()
        {
            float height = Height > selectionMeasurement.Y ? Height : (HasFocus ? Height : selectionMeasurement.Y);
            Box.TextBounds.Y = height;
            Box.TextChanged = true;

            InvokeOnChange(false);
        }

        private void killImeLine()
        {
            ImeLine.Text = string.Empty;
        }

        internal virtual bool HandleKey(object sender, Keys k, bool first)
        {
            if (InputControl.TextBox.ImeActive ||
    (InputControl.TextBox.ImeDeactivateTime > 0 &&
     GameBase.Time - InputControl.TextBox.ImeDeactivateTime < ChatEngine.ImeCheckDelay))
            {
                return true;
            }

            if (HasFocus)
            {
                if (first)
                {
                    switch (k)
                    {
                        case Keys.F8:
                        case Keys.F9:
                        case Keys.Escape:
                            FocusLost(false);
                            return true;
                        case Keys.Enter:
                            FocusLost(true);
                            return true;
                    }

                    if (KeyboardHandler.ControlPressed)
                    {
                        switch (k)
                        {
                            case Keys.C:
                            case Keys.X:
                                return InputControl.TextBox.SelectionLength > 0;
                            case Keys.A:
                                return Text.Length > 0;
                            case Keys.V:
                            case Keys.Left:
                            case Keys.Right:
                                return true;

                        }
                    }
                }
            }

            return false;
        }

        protected virtual void UpdateCursorPositionScheduled(object sender, KeyEventArgs e)
        {
            if (!HasFocus) return;

            GameBase.Scheduler.Add(UpdateCursorPosition, true);
        }

        internal int CURSOR_Y_OFFSET = 2;

        internal virtual void UpdateCursorPosition()
        {
            cursor.Bypass = !HasFocus || (hasDefaultValue && isDefaultValue) || Box.Text.Length == LengthLimit;
            int currentSelection = Math.Max(0, InputControl.TextBox.SelectionStart);
            //if the text is the default value we don't want the cursor position to be changed.
            if (hasDefaultValue && isDefaultValue)
                InputControl.TextBox.SelectionStart = InputControl.TextBox.Text.Length;
            selectionStartLast = selectionStart;
            selectionStart = currentSelection;

            if (selectionStartLast != selectionStart)
            {
                //figure out what line we are on first...
                Vector2 measure = Box.MeasureText(0, (int)Math.Min(Box.Text.Length, selectionStart));
                measure.X -= 3;
                measure.Y -= 3;

                int lineNumber = (int)Math.Max(0, Math.Floor(measure.X / Length));

                cursor.Bypass |= lineNumber > 0;

                float y = (lineNumber) * (measure.Y);
                float x = measure.X - (lineNumber * (3 * 2));

                cursor.Position = Box.CurrentPositionActual;
                cursor.Position.X += Math.Max(0, x);
                cursor.Position.Y += CURSOR_Y_OFFSET + y;
            }
        }

        private void box_OnClick(object sender, EventArgs e)
        {
            if (ReadOnly) return;

            //Force delayed execution to next frame to avoid interference when
            //swapping from one text input field to another.  Kinda hacky but very
            //effective.
            GameBase.Scheduler.Add(FocusGot, true);
        }

        private void FocusGot()
        {
            if (ReadOnly) return;

            if (!HasFocus)
            {
                textBeforeCommit = Box.Text;
                if (ResetTextOnEdit)
                    Box.Text = string.Empty;
                HasFocus = true;
                Box.BorderColour = BorderFocused;
                Box.BackgroundColour = backgroundFocused;

                //set text to current content.
                InputControl.Text = Box.Text;
                InputControl.AcceptText = true;


                //chatWasVisible = ChatEngine.IsVisible;

                cursor.Transformations.Clear();
                cursor.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0.4F, GameBase.Time, GameBase.Time + 1000)
                {
                    Easing = EasingTypes.Out,
                    Loop = true
                });

                selectionStartLast = -1;

                //Moved before UpdateCursorPosition to fix cursor mispositioning.  Hopefully won't cause any side-effects.
                InvokeOnGotFocus();

                UpdateCursorPosition();

                updateDisplay();
            }
        }

        private bool onClick(object sender, EventArgs e)
        {
            if (!HasFocus || ReadOnly || !MouseManager.PhysicalClickRegistered) return false;

            if (!Box.drawRectangle.Contains(MouseManager.MousePoint))
                FocusLost(CommitOnLeave);

            return false;
        }

        private Color backgroundFocused = new Color(50, 50, 50, 200);

        internal Color BackgroundFocused
        {
            get { return backgroundFocused; }
            set
            {
                backgroundFocused = value;
                if (HasFocus) Box.BackgroundColour = value;
            }
        }

        private Color backgroundUnfocused = new Color(50, 50, 50, 80);

        internal Color BackgroundUnfocused
        {
            get { return backgroundUnfocused; }
            set
            {
                backgroundUnfocused = value;
                if (!HasFocus) Box.BackgroundColour = value;
            }
        }

        protected virtual void FocusLost(bool success)
        {
            if (ReadOnly) return;

            if (HasFocus)
            {
                HasFocus = false;
                Box.BorderColour = new Color(180, 180, 180);
                Box.BackgroundColour = backgroundUnfocused;

                InputControl.AcceptText = false;

                cursor.Transformations.Clear();
                cursor.FadeOut(50);

                InvokeOnCommit(success);

                updateDisplay();
            }
        }

        public void Select()
        {
            if (ReadOnly) return;
            FocusGot();
        }



        internal virtual string Text
        {
            get
            {
                PasswordInputControl inputControl = InputControl as PasswordInputControl;
                return inputControl != null ? inputControl.UnderlyingText : Box.Text;
            }
            set
            {
                if (value == Box.Text)
                    return;

                InputControl.Text = value;
                Box.Text = InputControl.Text;
                updateDisplay();
            }
        }


        internal bool CommitOnLeave = true;

        private bool enabled = false;
        public virtual bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                if (enabled == value)
                    return;

                enabled = value;

                Box.HandleInput = enabled;
                Box.TextColour = enabled ? Color.White : new Color(180, 180, 180);

                if (HasFocus)
                    FocusLost(CommitOnLeave);
            }
        }

        internal void Focus(bool force)
        {
            if (force) HasFocus = false;
            FocusGot();
        }
        internal void UnFocus(bool force)
        {
            if (force) HasFocus = true;
            FocusLost(false);
        }

        public Vector2 selectionMeasurement
        {
            get
            {
                return Box.MeasureText();
            }
        }
    }
}