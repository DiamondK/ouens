﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Renderers;
using osu.Graphics.Sprites;
using osu.Input.Handlers;
using osu.Helpers;
using osu.Graphics.Skinning;
using osu.Audio;
using osu.Input;


namespace osu.Graphics.UserInterface
{
    internal class pDropdown
    {
        private static Color default_background_colour = new Color(0, 0, 0, 240);

        internal event EventHandler OnSelect;
        internal event EventHandler OnDisplayChanged;

        internal bool CentreText;
        internal int ColumnLimit;

        internal SpriteManager SpriteManager;
        internal List<pSprite> SpriteCollection = new List<pSprite>();
        internal List<pText> OptionsSprites = new List<pText>();
        private pTextAwesome spriteMainBoxArrow;
        internal pText SpriteMainBox;

        internal Color HighlightColour = SkinManager.NEW_SKIN_COLOUR_MAIN_DARKER;
        private Vector2 position;
        private Vector2 boxOffset;
        private bool reverse;

        private int visibleTime;
        private bool enabled = true;
        private float width;
        private float depth;

        internal object SelectedObject;
        internal string SelectedString;
        internal int SelectedNumeric;

        internal bool Visible { get; private set; }

        internal bool Bypass
        {
            get { return spriteMainBoxArrow.Bypass; }
            set { spriteMainBoxArrow.Bypass = SpriteMainBox.Bypass = value; }
        }

        internal pDropdown(SpriteManager spriteManager, string initialText, Vector2 position)
            : this(spriteManager, initialText, position, 118, 0.98f, false)
        { }

        internal pDropdown(SpriteManager spriteManager, string initialText, Vector2 position, bool reverse)
            : this(spriteManager, initialText, position, 118, 0.98f, reverse)
        { }

        internal pDropdown(SpriteManager spriteManager, string initialText, Vector2 position, float width, float depth)
            : this(spriteManager, initialText, position, width, depth, false)
        { }

        internal pDropdown(SpriteManager spriteManager, string initialText, Vector2 position, float width, float depth, bool reverse, bool centre = false)
        {
            this.SpriteManager = spriteManager;
            this.width = width;
            this.reverse = reverse;

            CentreText = centre;

            this.position = position;
            this.depth = depth;

            ResetBoxOffset();

            SpriteMainBox = new pText(initialText, 13, position, new Vector2(width + 2, 15), depth + 0.001f, true, Color.White, false);
            SpriteMainBox.BackgroundColour = new Color(0, 0, 0, 128);

            SpriteMainBox.CornerBounds = Vector4.One * 5;

            SpriteMainBox.TextAlignment = CentreText ? TextAlignment.Centre : TextAlignment.LeftFixed;
            SpriteMainBox.TextShadow = true;
            SpriteMainBox.OnHover += HoverGainedEffect;
            SpriteMainBox.OnHoverLost += HoverLostEffect;
            SpriteMainBox.HandleInput = true;
            SpriteMainBox.OnClick += optionClicked;

            SpriteCollection.Add(SpriteMainBox);

            spriteMainBoxArrow = new pTextAwesome(FontAwesome.chevron_down, 14, new Vector2(position.X + width - 8, position.Y + 8))
            {
                Depth = depth + 0.002f
            };
            SpriteCollection.Add(spriteMainBoxArrow);

            if (spriteManager != null)
                spriteManager.Add(SpriteCollection);
        }

        public bool Enabled
        {
            get { return enabled; }
            set
            {
                if (enabled == value)
                    return;

                if (enabled)
                    Close();

                enabled = value;

                if (!enabled)
                    SpriteMainBox.Hovering = false;

                SpriteMainBox.HandleInput = enabled;
                SpriteMainBox.BackgroundColour = enabled ? Color.Black : Color.DarkGray;
                SpriteMainBox.TextChanged = true;
            }
        }

        bool onClick(object sender, EventArgs e)
        {
            if (!MouseManager.PhysicalClickRegistered) return false;
            
            foreach (pSprite s in OptionsSprites)
                if (s.Hovering) return false;

            if (SpriteMainBox.Hovering)
                return false;

            Close();
            return true;
        }

        bool onClickUp(object sender, EventArgs e)
        {
            InputManager.Unbind(InputEventType.OnClickUp, onClickUp, InputTargetType.Highest);

            if (!Visible) return false;

            if (SpriteManager != null && SpriteManager.CurrentHoverSprite != null && SpriteManager.CurrentHoverSprite != SpriteMainBox)
            {
                SpriteManager.CurrentHoverSprite.Click();
                return true;
            }

            return false;
        }

        private void Display()
        {
            if (Visible || !enabled) return;

            Visible = true;
            visibleTime = GameBase.Time;
            InputManager.Bind(InputEventType.OnClick, onClick, InputTargetType.Highest, BindLifetime.Manual);
            InputManager.Bind(InputEventType.OnClickUp, onClickUp, InputTargetType.Highest, BindLifetime.Manual);

            AudioEngine.Click(null, @"select-expand");

            if (OptionsSprites.Count > 0)
            {
                if (reverse)
                {
                    OptionsSprites[0].CornerBounds.X = 5;
                    OptionsSprites[0].CornerBounds.Y = 5;

                    OptionsSprites[OptionsSprites.Count - 2].CornerBounds.Z = 5;
                    OptionsSprites[OptionsSprites.Count - 2].CornerBounds.W = 5;
                }
                else
                {
                    OptionsSprites[0].CornerBounds.Z = 5;
                    OptionsSprites[0].CornerBounds.W = 5;

                    OptionsSprites[OptionsSprites.Count - 2].CornerBounds.X = 5;
                    OptionsSprites[OptionsSprites.Count - 2].CornerBounds.Y = 5;
                }
            }


            int i = 0;

            Vector2 offset = new Vector2(0, 0);

            foreach (pText p in OptionsSprites)
            {
                bool isTextAwesome = p is pTextAwesome;

                if (!isTextAwesome)
                    offset.Y += (reverse ? -1 : 1) * p.MeasureText().Y + (reverse ? 0.5f : -0.5f);

                int startTime = GameBase.Time + i * 10;
                p.Bypass = false;

                p.Transformations.Clear();

                p.Position = SpriteMainBox.Position + (isTextAwesome ? new Vector2(6, 9) : Vector2.Zero);
                p.MoveTo(p.Position + offset, (i > 0 ? 200 : 0) + i * 10, EasingTypes.OutCirc);

                p.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, startTime, startTime + 200, EasingTypes.Out));

                i++;
            }

            if (OnDisplayChanged != null)
                OnDisplayChanged(this, null);
        }

        private void Close()
        {
            if (!Visible || GameBase.Time - visibleTime < 100)
                return;

            Visible = false;
            InputManager.Unbind(InputEventType.OnClick, onClick, InputTargetType.Highest);
            InputManager.Unbind(InputEventType.OnClickUp, onClickUp, InputTargetType.Highest);
            
            int i = 0;
            foreach (pSprite p in OptionsSprites)
            {
                bool isTextAwesome = p is pTextAwesome;
                if (isTextAwesome)
                    p.FadeOut(0);
                else
                {
                    p.MoveTo(SpriteMainBox.Position + (isTextAwesome ? new Vector2(6, 9) : Vector2.Zero), 200 + i * 10, EasingTypes.In);
                    p.FadeOut(100 + i++ * 10);
                }
            }

            if (OnDisplayChanged != null)
                OnDisplayChanged(this, null);

            //pretty dirty hack to ensure option sprites aren't displayed by an external mechanism (ie. online users overlay).
            //shouldn't break.
            GameBase.Scheduler.AddDelayed(delegate { if (!Visible) OptionsSprites.ForEach(s => s.Bypass = true); }, 100 + i * 10);
        }

        internal pSprite AddOption(pDropdownItem item)
        {
            return AddOption(item.Text, item.Value, 0, default_background_colour);
        }

        internal pSprite AddOption(string text, object value)
        {
            return AddOption(text, value, 0, default_background_colour);
        }

        internal pSprite AddOption(string text, object value, Color bgColour)
        {
            return AddOption(text, value, 0, bgColour);
        }

        internal pSprite AddOption(string text, object value, int tagNumeric, Color bgColour, bool enabled = true)
        {
            pText option = new pText(text, SpriteMainBox.TextSize - 1, boxOffset, new Vector2(width + 2, 14 + 3), depth + 0.003f, true, Color.TransparentWhite, true);
            option.TextAlignment = CentreText ? TextAlignment.Centre : TextAlignment.LeftFixed;

            pText option2 = new pTextAwesome(FontAwesome.chevron_right, 10, boxOffset + new Vector2(6, 9))
            {
                Depth = depth + 0.004f,
                InitialColour = Color.TransparentBlack,
                Alpha = 0,
                TextShadow = false
            };

            option.BackgroundColour = bgColour;
            option.Padding = new Vector2(12, 3);

            option.Bypass = true;
            option2.Bypass = true;

            if (enabled)
            {
                option.HandleInput = true;
                option.ClickRequiresConfirmation = true;

                if (value != null)
                {
                    option.OnClick += optionClicked;
                    option.OnHover += HoverGainedEffect;
                    option.OnHoverLost += HoverLostEffect;
                }

                option.Tag = value;
                option.TagNumeric = tagNumeric;
                option.HoverPriority = -1000;
            }

            OptionsSprites.Add(option);
            OptionsSprites.Add(option2);

            SpriteCollection.Add(option);
            SpriteCollection.Add(option2);

            if (SpriteManager != null)
            {
                SpriteManager.Add(option);
                SpriteManager.Add(option2);
            }

            return option;
        }

        internal void RemoveAllOptions()
        {
            while (OptionsSprites.Count > 0)
            {
                pText sprite = OptionsSprites[0];
                OptionsSprites.Remove(sprite);
                SpriteCollection.Remove(sprite);
                if (SpriteManager != null) SpriteManager.Remove(sprite);

                sprite.Dispose();
            }

            ResetBoxOffset();

            SelectedNumeric = 0;
            SelectedObject = null;
            SelectedString = null;
        }

        private void ResetBoxOffset(int column = 0)
        {
            if (reverse)
                boxOffset = position + new Vector2(width * column, 2);
            else
                boxOffset = position + new Vector2(width * column, 17);
        }

        void optionClicked(object sender, EventArgs e)
        {
            select(sender, false);
        }

        pText lastSelected;
        private void select(object sender, bool silent)
        {
            if (sender == null) sender = SpriteMainBox;

            pText senderText = (pText)sender;

            if (senderText == SpriteMainBox)
            {
                if (Visible)
                    Close();
                else
                    Display();
                
                return;
            }

            Close();

            if (senderText.Text != SpriteMainBox.Text)
                SpriteMainBox.TextChanged = true;

            if (lastSelected != null && senderText != SpriteMainBox)
            {
                lastSelected.TextBold = false;
                lastSelected.TextChanged = true;
            }

            if (senderText != null)
            {
                senderText.TextBold = true;
                SpriteMainBox.Tag = senderText.Tag;
            }

            if (senderText.Tag == null)
                return;

            SelectedObject = senderText.Tag;
            SelectedString = senderText.Text;
            SelectedNumeric = senderText.TagNumeric;

            if (senderText != SpriteMainBox)
                lastSelected = senderText;

            SpriteMainBox.Text = string.IsNullOrEmpty(senderText.Text) ? " " : senderText.Text;

            if (!silent)
                AudioEngine.Click(null, @"click-short-confirm");

            if (senderText.Tag is Color)
            {
                Color spriteCol = (Color)senderText.Tag;
                if (spriteCol.A < 1)
                    SpriteMainBox.BackgroundColour = Color.Black; //disallow transparent bg colours for sake of style
                else
                    SpriteMainBox.BackgroundColour = spriteCol;
            }

            // TODO: in case of non-pText clone the sprite.
            if (senderText.Tag != null && senderText != SpriteMainBox)
            {
                if (!silent && OnSelect != null)
                    OnSelect(senderText.Tag, null);
            }
        }

        void HoverLostEffect(object sender, EventArgs e)
        {
            pText senderText = sender as pText;
            if (senderText != null)
            {
                if (senderText.Tag is Color) // OH GOD this is a HAAACKKKK!
                    senderText.BackgroundColour = ColourHelper.Darken(senderText.BackgroundColour, 0.1f);
                else if (senderText == SpriteMainBox)
                    senderText.BackgroundColour = new Color(0, 0, 0, 128);
                else
                    senderText.BackgroundColour = default_background_colour;

                senderText.TextChanged = true;
            }
        }

        void HoverGainedEffect(object sender, EventArgs e)
        {
            pText senderText = sender as pText;
            if (senderText != null)
            {
                if (senderText.Tag is Color)
                    senderText.BackgroundColour = ColourHelper.Lighten(senderText.BackgroundColour, 0.1f);
                else
                    senderText.BackgroundColour = HighlightColour;

                AudioEngine.Click(null, @"click-short");

                senderText.TextChanged = true;
            }
        }


        public bool SetSelected(object tag, bool silent)
        {
            pSprite selected = spriteByTag(tag);

            if (selected != null)
            {
                select(selected, silent);
                return true;
            }

            return false;
        }

        public pSprite spriteByTag(object tag)
        {
            foreach (pSprite p in OptionsSprites)
                if (p.Tag != null && p.Tag.Equals(tag))
                    return p;

            return null;
        }

        public void SelectNone()
        {
            // todo: kinda hackish but should be okay
            if (lastSelected != null && lastSelected is pText)
            {
                //(lastSelected as pText).TextBold = false;
                lastSelected.TextChanged = true;
            }
            SpriteMainBox.Text = " ";
            SelectedNumeric = -1;
            SelectedObject = null;
        }
    }
}