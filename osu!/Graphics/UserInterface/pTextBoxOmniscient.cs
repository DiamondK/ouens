﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Online;
using osu.Input.Handlers;
using Microsoft.Xna.Framework.Input;
using osu.Online.Social;

namespace osu.Graphics.UserInterface
{
    class pTextBoxOmniscient : pTextBox
    {
        public pTextBoxOmniscient(int size, Vector2 position, float length, bool overrideStyle = true, float depth = 0.98f)
            : base(string.Empty, size, position, length, depth)
        {
            if (overrideStyle)
            {
                Box.BorderWidth = 0;
                BackgroundUnfocused = Color.TransparentWhite;
                BackgroundFocused = Color.TransparentWhite;
                Box.TextBold = true;
            }
        }

        internal bool HandleLeftRightArrows
        {
            get { return !InputControl.SuppressLeftRightKeys; }
            set { InputControl.SuppressLeftRightKeys = !value; }
        }

        private bool maintainFocus;

        internal bool MaintainFocus
        {
            get { return maintainFocus && Enabled; }
            set
            {
                if (maintainFocus == value) return;

                maintainFocus = value;

                if (value)
                    Focus(true);
                else
                    FocusLost(false);
            }
        }

        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                if (Enabled == value) return;

                //return focus after re-enabling an omniscent textbox.
                if (base.Enabled = value)
                    Focus(true);
            }
        }

        internal override bool HandleKey(object sender, Microsoft.Xna.Framework.Input.Keys k, bool first)
        {
            if (InputControl.TextBox.ImeActive || (InputControl.TextBox.ImeDeactivateTime > 0 && GameBase.Time - InputControl.TextBox.ImeDeactivateTime < ChatEngine.ImeCheckDelay))
            {
                return true;
            }

            if (!HasFocus) return false;

            if (KeyboardHandler.ControlPressed)
            {
                switch (k)
                {
                    case Keys.C:
                    case Keys.X:
                        return InputControl.TextBox.SelectionLength > 0;
                    case Keys.A:
                        return Text.Length > 0;
                    case Keys.V:
                        return true;
                    case Keys.Left:
                    case Keys.Right:
                        return HandleLeftRightArrows;
                }
            }

            return false;
        }

        protected override void FocusLost(bool success)
        {
            if (!MaintainFocus)
                base.FocusLost(success);
            else
                InvokeOnCommit(success);
        }

        internal override void UpdateCursorPosition()
        {
            int length = InputControl.TextBox.Text.Length;
            if (!HandleLeftRightArrows && InputControl.TextBox.SelectionStart != length)
                InputControl.TextBox.SelectionStart = InputControl.TextBox.Text.Length;

            base.UpdateCursorPosition();
        }

        protected override void TextInputControl_OnNewText(object sender, EventArgs e)
        {
            if (hasDefaultValue)
            {
                if (!isDefaultValue)
                {
                    if (InputControl.Text.Length == 0)
                    {
                        isDefaultValue = true;
                        InputControl.Text = defaultText;
                    }
                }
                else
                {
                    if (InputControl.Text.Length < defaultText.Length)
                    {
                        //they just backspaced or deleted the default text.
                        //let's replace it back.
                        isDefaultValue = true;
                        InputControl.Text = defaultText;
                    }
                    else
                    {
                        string replacement = InputControl.Text.Replace(defaultText, "");
                        if (replacement.Length > 0 && replacement != InputControl.Text)
                        {
                            isDefaultValue = false;
                            InputControl.Text = replacement;
                        }
                    }
                }
            }

            base.TextInputControl_OnNewText(sender, e);
        }

        private string defaultText;
        public string DefaultText
        {
            get { return defaultText; }
            set
            {
                hasDefaultValue = value.Length > 0;
                isDefaultValue = true;
                defaultText = value;
                Text = defaultText;
                UpdateCursorPosition();
            }
        }

        internal void SetToDefault()
        {
            Text = DefaultText ?? string.Empty;
            if (hasDefaultValue) isDefaultValue = true;
            UpdateCursorPosition();
        }

        internal override string Text
        {
            get
            {
                if (isDefaultValue) return string.Empty;
                return base.Text;
            }
            set
            {
                isDefaultValue = value == defaultText;
                base.Text = value;

                UpdateCursorPosition();
            }
        }
    }

}
