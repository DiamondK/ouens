﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Graphics.Sprites;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using Microsoft.Xna.Framework;

namespace osu.Graphics.UserInterface
{
    class pStatusDialog : IDisposable
    {
        pText status;
        SpriteManager spriteManagerProgressText = new SpriteManager(true);
        SpriteManager spriteManager = new SpriteManager(true);

        bool didDrawAFrame = true;
        private pSprite spinner;

        public static pSprite CreateSpinner(Color col)
        {
            pSprite spinner = new pSprite(SkinManager.Load(@"beatmapimport-spinner", SkinSource.Osu), Fields.Centre, Origins.Centre, Clocks.Game, Vector2.Zero, 1, true, col);

            spinner.Transformations.Add(new Transformation(TransformationType.Rotation, 0, (float)Math.PI * 2, GameBase.Time, GameBase.Time + 1500) { Loop = true });
            spinner.Transformations.Add(new Transformation(TransformationType.Scale, 0.5f, 1.3f, GameBase.Time, GameBase.Time + 200, EasingTypes.Out));
            spinner.Transformations.Add(new Transformation(TransformationType.Scale, 1.3f, 0.9f, GameBase.Time + 200, GameBase.Time + 400, EasingTypes.In));
            spinner.Transformations.Add(new Transformation(TransformationType.Scale, 0.9f, 1, GameBase.Time + 400, GameBase.Time + 500, EasingTypes.Out));

            return spinner;
        }

        public pStatusDialog(string initialStatus)
        {
            status = new pText("", 14, new Vector2(GameBase.WindowWidthScaled / 2, 240), 1, true, Color.White);
            status.Origin = Origins.Centre;

            spriteManager.Add(new pSprite(SkinManager.Load(@"beatmapimport-top", SkinSource.Osu), Vector2.Zero, 1, true, Color.White));
            spinner = CreateSpinner(new Color(255, 96, 171));
            spinner.FadeInFromZero(2000);
            spinner.Position.Y += 70;
            spriteManager.Add(spinner);

            SetStatus(initialStatus);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            spriteManagerProgressText.Dispose();
            spriteManager.Dispose();

        }

        public void SetStatus(string statusText)
        {
            if (!didDrawAFrame)
                return;
            didDrawAFrame = false;

            GameBase.Scheduler.Add(delegate
            {
                pText newStatus = (pText)status.Clone();
                newStatus.Text = statusText;

                spriteManagerProgressText.Add(newStatus);

                foreach (pSprite p in spriteManagerProgressText.SpriteList)
                {
                    if (p.Position.Y < 0)
                    {
                        p.Transformations.Clear();
                        p.AlwaysDraw = false;
                    }
                    else if (p.Alpha > 0.01)
                    {
                        p.MoveToRelative(new Vector2(0, -20), 500);
                        p.FadeOut(10000);
                        p.AlwaysDraw = false;
                    }
                }
            });
        }

        public void Draw()
        {
            spriteManagerProgressText.Draw();
            spriteManager.Draw();
            didDrawAFrame = true;
        }
    }
}
