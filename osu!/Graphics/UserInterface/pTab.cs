using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;

namespace osu.Graphics.UserInterface
{
    internal class pTab
    {
        internal pSprite tabBackground;
        internal pText tabText;
        internal object Tag;
        internal List<pSprite> SpriteCollection = new List<pSprite>();
        internal bool Alerting;

        internal const int TAB_WIDTH = 74;

        internal virtual bool AllowSelection { get { return true; } }

        internal pTab(string name, object tag, Vector2 position, float depth, bool flip, bool skinnable, Transformation hoverEffect, float textSize = 1)
        {
            tabBackground = new pSprite(skinnable ? SkinManager.Load(@"selection-tab") : SkinManager.Load(@"selection-tab", SkinSource.Osu), Fields.TopLeft,
                                      Origins.TopLeft,
                                      Clocks.Game,
                                      position, depth, true, hoverEffect.StartColour);
            if (flip) tabBackground.FlipVertical = true;
            tabBackground.HoverEffect = hoverEffect;
            tabBackground.OnHover += delegate { AudioEngine.Click(null, @"click-short"); };
            tabBackground.OriginPosition = new Vector2(70, 12);
            tabBackground.Tag = this;
            Tag = tag ?? this;

            int lengthLimit = (int)(13 / textSize);

            if (!string.IsNullOrEmpty(name))
            {
                name = name.Length > lengthLimit ? name.Remove(lengthLimit) + ".." : name;
                tabBackground.HandleInput = true;
                tabBackground.OnClick += tab_OnClick;
            }
            else //Dud
                tabBackground.InitialColour = new Color(160, 8, 42);

            tabText =
                new pText(name, 12 * textSize, position, new Vector2(82, 12) * textSize, depth + 0.0001F, true, Color.White, false);
            tabText.TextBold = true;
            tabText.TextShadow = true;
            tabText.Origin = Origins.Centre;
            tabText.TextAlignment = TextAlignment.Centre;

            SpriteCollection.Add(tabBackground);
            SpriteCollection.Add(tabText);
        }

        internal event EventHandler OnClick;

        protected void tab_OnClick(object sender, EventArgs e)
        {
            if (OnClick != null)
                OnClick(this, null);
        }
    }
}