﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Graphics.Sprites;
using osu_common.Helpers;
using osu.Graphics.Primitives;

namespace osu.Graphics
{
    partial class pDrawable : IDisposable, IComparable<pDrawable>
    {
        internal Clocks Clock;
        internal float Depth;

        #region Transformations
        internal pList<Transformation> Transformations = new pList<Transformation>();
        internal List<TransformationLoop> Loops;
        #endregion

        #region Settings
        /// <summary>
        /// Draw even when no transformations are active.
        /// </summary>
        internal bool AlwaysDraw;

        internal virtual bool UsesSpriteBatch { get { return false; } }
        #endregion

        #region Initial State
        internal Color InitialColour;
        internal Vector2 InitialPosition;

        internal Object Tag;
        internal int TagNumeric;
        #endregion

        #region Input Handling
        internal bool HandleInput;

        internal bool ClickRequiresConfirmation;

        internal Transformation ClickEffect;

        internal bool Click(bool confirmed = true)
        {
            if (ClickRequiresConfirmation && !confirmed) return false;

            bool hasAction = OnClick != null;
            if (hasAction)
                OnClick(this, null);
            if (ClickEffect != null)
            {
                int duration = ClickEffect.Time2 - ClickEffect.Time1;

                ClickEffect.Time1 = GameBase.Time;
                ClickEffect.Time2 = GameBase.Time + duration;

                if (!Transformations.Contains(ClickEffect))
                    Transformations.Add(ClickEffect);
            }

            return hasAction;
        }

        internal void ClearClickHandlers()
        {
            OnClick = null;
        }

        internal event EventHandler OnClick;

        #endregion

        #region Current State
        internal float Rotation;
        internal float Scale;
        internal float Alpha;
        internal Vector2 Position;
        internal bool IsVisible;

        /// <summary>
        /// Allows relative scaling of the sprite (where 1 is 100%) in two directions.
        /// </summary>
        internal Vector2 VectorScale = Vector2.One;

        private Rectangle? clipRectangleScaled = null;
        internal Rectangle ClipRectangleScaled { get { return IsClipped ? (clipRectangleScaled ?? SpriteManager.Current.ClipRectangle) : SpriteManager.ViewRectangleFull; } }
        private RectangleF? clipRectangle = null;
        internal RectangleF? ClipRectangle
        {
            get { return clipRectangle ?? SpriteManager.Current.ViewRectangle; }
            set
            {
                clipRectangle = value;
                if (value == null)
                    clipRectangleScaled = null;
                else
                {
                    clipRectangleScaled = new Rectangle((int)(value.Value.X * GameBase.WindowRatio), (int)(value.Value.Y * GameBase.WindowRatio),
                                                        (int)Math.Ceiling(value.Value.Width * GameBase.WindowRatio), (int)Math.Ceiling(value.Value.Height * GameBase.WindowRatio));
                }
            }
        }

        protected bool IsClipped { get { return SpriteManager.Current.Masking || clipRectangle != null; } }
        #endregion

        #region Input Handling
        protected bool hovering;
        internal virtual bool Hovering
        {
            get { return hovering; }
            set
            {
                if (value == hovering)
                    return;
                hovering = value;
            }
        }
        #endregion

        internal bool bypass;
        /// <summary>
        /// Totally ignore processing this sprite.  Performance optimisation.
        /// </summary>
        internal bool Bypass
        {
            get { return bypass; }
            set
            {
                bypass = value;
                if (bypass) Hovering = false;
            }
        }

        protected int getClockTime()
        {
            return Clock == Clocks.Game ? GameBase.Time : AudioEngine.Time;
        }

        /// <summary>
        /// Draw this object
        /// </summary>
        /// <returns>true if the object is deemed to be visible</returns>
        public virtual bool Draw()
        {
            if (bypass) return false;

            return true;
        }

        public virtual UpdateResult Update()
        {
            if (bypass) return UpdateResult.NotVisible;

            return UpdateResult.Visible;
        }

        internal void MakeAllLoopsStatic()
        {
            if (Loops == null || Loops.Count == 0)
                return;

            Transformations.RemoveAll(t => t.IsLoopStatic);

            foreach (TransformationLoop l in Loops)
                MakeLoopStatic(l);
        }

        internal void MakeLoopStatic(TransformationLoop l)
        {
            List<Transformation> list = l.MakeStaticContent();

            if (list == null) return;

            foreach (Transformation t in list)
                Transformations.AddInPlace(t);
        }

        #region Low Level Logic
        public int CompareTo(pDrawable other)
        {
            return Transformations[0].Time1.CompareTo(other.Transformations[0].Time1);
        }

        ~pDrawable()
        {
            Dispose(true);
        }

        internal bool IsDisposable;

        public virtual void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(false);
        }

        internal virtual void Dispose(bool isDisposing)
        {
        }
        #endregion
    }

    internal enum UpdateResult
    {
        Visible,
        NotVisible,
        Discard
    }
}