using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.Graphics.Notifications;
using osu.Graphics.OpenGl;
using Color = Microsoft.Xna.Framework.Graphics.Color;
using System.Windows.Forms;
using osu_common.Libraries.NetLib;
using osu_common.Helpers;

namespace osu.Graphics
{
    public class Screenshot
    {
        internal static byte[] TakeDesktopScreenshot()
        {
            try
            {
                using (Bitmap b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, PixelFormat.Format32bppArgb))
                {
                    using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(b))
                    {
                        g.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y, 0, 0, Screen.PrimaryScreen.Bounds.Size, CopyPixelOperation.SourceCopy);
                        MemoryStream str = new MemoryStream();
                        b.Save(str, ImageFormat.Jpeg);

                        return str.ToArray();
                    }
                }
            }
            catch
            {
            }

            return null;
        }

        internal static void TakeScreenshot(bool upload)
        {
            try
            {
                bool pause = AudioEngine.AudioState == AudioStates.Playing;
                if (pause)
                    AudioEngine.TogglePause();

                AudioEngine.PlaySample(@"shutter");

                string filename = string.Empty;

                if (!Directory.Exists("Screenshots"))
                    Directory.CreateDirectory("Screenshots");

                int num = ConfigManager.sScreenshot;
                do
                {
                    num++;
                    filename = String.Format("Screenshots/screenshot{0:000}", num);
                } while (File.Exists(filename + ".jpg") || File.Exists(filename + ".png"));

                filename += "." + ConfigManager.sScreenshotFormat.ToString().ToLower();

                if (GameBase.D3D)
                {
                    GraphicsDevice device = GameBase.graphics.GraphicsDevice;

                    Texture2D dstTexture = new Texture2D(
                        device,
                        device.Viewport.Width,
                        device.Viewport.Height,
                        1,
                        ResourceUsage.ResolveTarget,
                        SurfaceFormat.Bgr32, ResourceManagementMode.Manual);

                    device.ResolveBackBuffer(dstTexture);

                    GameBase.RunGraphicsThread(delegate
                    {
                        dstTexture.Save(filename, ConfigManager.sScreenshotFormat);

                        if (upload)
                        {
                            if (ConfigManager.sScreenshotFormat == ImageFileFormat.Jpg)
                                UploadScreenshot(filename, false);
                            else
                            {
                                string tmpFilename = filename.Replace(".png", ".jpg");
                                dstTexture.Save(tmpFilename, ImageFileFormat.Jpg);
                                UploadScreenshot(tmpFilename, true);
                            }
                        }
                        dstTexture.Dispose();
                    });
                }
                else
                {
                    using (Bitmap b = new Bitmap(GameBase.WindowWidth, GameBase.WindowHeight, PixelFormat.Format32bppArgb))
                    {
                        BitmapData bd = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, b.PixelFormat);
                        Gl.glReadPixels(0, 0, GameBase.WindowWidth, GameBase.WindowHeight, Gl.GL_BGRA, Gl.GL_UNSIGNED_BYTE, bd.Scan0);
                        b.UnlockBits(bd);

                        b.RotateFlip(RotateFlipType.RotateNoneFlipY);

                        using (Bitmap b2 = b.Clone(new Rectangle(0, 0, b.Width, b.Height), PixelFormat.Format24bppRgb))
                        {
                            b2.Save(filename, ConfigManager.sScreenshotFormat == ImageFileFormat.Jpg ? ImageFormat.Jpeg : ImageFormat.Png);

                            if (upload)
                            {
                                if (ConfigManager.sScreenshotFormat == ImageFileFormat.Jpg)
                                    UploadScreenshot(filename, false);
                                else
                                {
                                    string tmpFilename = filename.Replace(".png", ".jpg");
                                    b2.Save(tmpFilename, ImageFormat.Jpeg);
                                    UploadScreenshot(tmpFilename, true);
                                }
                            }
                        }
                    }
                }

                if (pause)
                    AudioEngine.TogglePause();

                ConfigManager.sScreenshot.Value = num;
                NotificationManager.ShowMessage("Saved screenshot to " + filename, Color.BlueViolet, 6000);
            }
            catch
            {
                NotificationManager.ShowMessage("Screnshot capture failed!", Color.Red, 3000);
            }
        }

        private static int lastUpload;

        private static void UploadScreenshot(string filename, bool deleteAfterUpload)
        {
            if (filename == null) return;

            GameBase.RunBackgroundThread(delegate
            {
                try
                {
                    if (lastUpload > 0 && GameBase.Time - lastUpload < 30000)
                    {
                        NotificationManager.ShowMessage("You are trying to upload screenshots too fast!");
                        return;
                    }

                    lastUpload = GameBase.Time;

                    NotificationManager.ShowMessage("Uploading screenshot...", Color.Yellow, 3000);

                    FileUploadNetRequest fileUpload =
                        new FileUploadNetRequest(
                            string.Format(General.WEB_ROOT + "/web/osu-screenshot.php?u={0}&p={1}&v=1", ConfigManager.sUsername, ConfigManager.sPassword), File.ReadAllBytes(filename), Path.GetExtension(filename).Trim('.'), "ss");
                    string returned = fileUpload.BlockingPerform();

                    if (returned.Contains("http"))
                        GameBase.ProcessStart(returned);
                    else
                        GameBase.ProcessStart(General.WEB_ROOT + "/ss/" + returned);

                    NotificationManager.ShowMessage("Screenshot has been uploaded!  Opening in browser window..." + filename, Color.YellowGreen, 6000);
                }
                catch
                {
                }
                finally
                {
                    try
                    {
                        if (deleteAfterUpload) File.Delete(filename);
                    }
                    catch
                    {
                    }
                }
            });
        }
    }
}