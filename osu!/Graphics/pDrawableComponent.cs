﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Graphics.Sprites;

namespace osu.Graphics
{
    internal abstract class pDrawableComponent : IDisposable
    {
        internal SpriteManager spriteManager;

        public pDrawableComponent(bool widescreenAware = false)
        {
            spriteManager = new SpriteManager(widescreenAware);
        }

        internal virtual void Dispose(bool isDisposing)
        {
            spriteManager.Dispose();
        }

        internal virtual void Initialize()
        {
        }

        internal virtual void Draw()
        {
            spriteManager.Draw();
        }

        internal virtual void Update()
        {
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(false);
        }
    }
}
