#region Using Statements

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace osu.Graphics.Primitives
{
    internal class ColouredLine : Line
    {
        internal Color colour = Color.White;

        internal ColouredLine(Vector2 p1, Vector2 p2, Color color) : base(p1, p2)
        {
            this.colour = color;
        }
    }

    /// <summary>
    /// Represents a single line segment.  Drawing is handled by the LineManager class.
    /// </summary>
    internal class Line : ICloneable
    {
        internal Vector2 p1; // Begin point of the line
        internal Vector2 p2; // End point of the line
        internal bool forceEnd; // this line's endpoint is important so always include it in optimizations
        internal bool straight; // This line is part of a longer straight line so it's subject to more optimizations

        internal Line(Vector2 p1, Vector2 p2)
        {
            this.p1 = p1;
            this.p2 = p2;
        }

        internal float rho
        {
            get { return (p2 - p1).Length(); }
        }

        internal float theta
        {
            get
            {
                return (float)Math.Atan2(p2.Y - p1.Y, p2.X - p1.X);
            }
        }

        /// <summary>
        /// Distance squared from an arbitrary point p to this line.
        /// </summary>
        /// <remarks>
        /// See http://geometryalgorithms.com/Archive/algorithm_0102/algorithm_0102.htm, near the bottom.
        /// </remarks>
        internal float DistanceSquaredToPoint(Vector2 p)
        {
            Vector2 v = p2 - p1; // Vector from line's p1 to p2
            Vector2 w = p - p1; // Vector from line's p1 to p

            // See if p is closer to p1 than to the segment
            float c1 = Vector2.Dot(w, v);
            if (c1 <= 0)
                return Vector2.DistanceSquared(p, p1);

            // See if p is closer to p2 than to the segment
            float c2 = Vector2.Dot(v, v);
            if (c2 <= c1)
                return Vector2.DistanceSquared(p, p2);

            // p is closest to point pB, between p1 and p2
            float b = c1 / c2;
            Vector2 pB = p1 + b * v;
            return Vector2.DistanceSquared(p, pB);
        }


        internal Matrix WorldMatrix()
        {
            Matrix rotate = Matrix.CreateRotationZ(theta);
            Matrix translate = Matrix.CreateTranslation(p1.X, p1.Y, 0);
            return rotate * translate;
        }

        /// <summary>
        /// It's the end of the world as we know it
        /// </summary>
        internal Matrix EndWorldMatrix()
        {
            return Matrix.CreateRotationZ(theta) * Matrix.CreateTranslation(p2.X, p2.Y, 0);
        }


        public object Clone()
        {
            return this.MemberwiseClone();
        }
    } ;
}