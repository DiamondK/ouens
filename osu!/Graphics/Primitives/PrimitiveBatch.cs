#region File Description

//-----------------------------------------------------------------------------
// PrimitiveBatch.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------

#endregion

#region Using Statements

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.OpenGl;

#endregion

namespace osu.Graphics.Primitives
{
    internal class PrimitiveBatch : IDisposable
    {
        #region Constants and Fields

        // this constant controls how large the vertices buffer is. Larger buffers will
        // require flushing less often, which can increase performance. However, having
        // buffer that is unnecessarily large will waste memory.
        private const int DefaultBufferSize = 500;

        // a block of vertices that calling AddVertex will fill. Flush will draw using
        // this array, and will determine how many primitives to draw from
        // positionInBuffer.

        // a basic effect, which contains the shaders that we will use to draw our
        // primitives.
        private BasicEffect basicEffect;

        // the device that we will issue draw calls to.
        private GraphicsDevice device;

        // this value is set by Begin, and is the type of primitives that we are
        // drawing.

        // hasBegun is flipped to true once Begin is called, and is used to make
        // sure users don't call End before Begin is called.
        private bool hasBegun = false;

        private bool isDisposed = false;
        private int numVertsPerPrimitive;
        private int positionInBuffer = 0;
        private PrimitiveType primitiveType;
        private VertexDeclaration vertexDeclaration;
        private VertexPositionColor[] vertices = new VertexPositionColor[DefaultBufferSize];

        #endregion

        // the constructor creates a new PrimitiveBatch and sets up all of the internals
        // that PrimitiveBatch will need.
        internal PrimitiveBatch()
        {

            if (GameBase.D3D)
            {
                device = GameBase.graphics.GraphicsDevice;


                // create a vertex declaration, which tells the graphics card what kind of
                // data to expect during a draw call. We're drawing using
                // VertexPositionColors, so we'll use those vertex elements.
                vertexDeclaration = new VertexDeclaration(device,
                                                          VertexPositionColor.VertexElements);

                // set up a new basic effect, and enable vertex colors.
                basicEffect = new BasicEffect(device, null);
                basicEffect.VertexColorEnabled = true;

                // projection uses CreateOrthographicOffCenter to create 2d projection
                // matrix with 0,0 in the upper left.
                basicEffect.Projection = Matrix.CreateOrthographicOffCenter
                    (0, device.Viewport.Width,
                     device.Viewport.Height, 0,
                     0, 1);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !isDisposed)
            {
                if (vertexDeclaration != null)
                    vertexDeclaration.Dispose();

                if (basicEffect != null)
                    basicEffect.Dispose();

                isDisposed = true;
            }
        }

        // Begin is called to tell the PrimitiveBatch what kind of primitives will be
        // drawn, and to prepare the graphics card to render those primitives.
        internal void Begin()
        {
            if (hasBegun)
            {
                throw new InvalidOperationException
                    ("End must be called before Begin can be called again.");
            }
            
            if (GameBase.D3D)
            {
                this.primitiveType = PrimitiveType.LineList;

                // how many verts will each of these primitives require?
                numVertsPerPrimitive = NumVertsPerPrimitive(primitiveType);

                // prepare the graphics device for drawing by setting the vertex declaration
                // and telling our basic effect to begin.
                device.VertexDeclaration = vertexDeclaration;
                basicEffect.Begin();
                basicEffect.CurrentTechnique.Passes[0].Begin();
            }
            else
            {
                GlControl.Texturing = false;
                GlControl.SetBlend(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA);
                Gl.glLineWidth(1);
                Gl.glBegin(Gl.GL_LINES);
            }

            // flip the error checking boolean. It's now ok to call AddVertex, Flush,
            // and End.
            hasBegun = true;

        }

        // AddVertex is called to add another vertex to be rendered. To draw a point,
        // AddVertex must be called once. for lines, twice, and for triangles 3 times.
        // this function can only be called once begin has been called.
        // if there is not enough room in the vertices buffer, Flush is called
        // automatically.
        internal void AddVertex(Vector2 vertex, Color color)
        {
            
            
            if (!hasBegun)
            {
                throw new InvalidOperationException
                    ("Begin must be called before AddVertex can be called.");
            }

            if (GameBase.D3D)
            {

                // are we starting a new primitive? if so, and there will not be enough room
                // for a whole primitive, flush.
                bool newPrimitive = ((positionInBuffer % numVertsPerPrimitive) == 0);

                if (newPrimitive &&
                    (positionInBuffer + numVertsPerPrimitive) >= vertices.Length)
                {
                    Flush();
                }

                // once we know there's enough room, set the vertex in the buffer,
                // and increase position.
                vertices[positionInBuffer].Position = new Vector3(vertex, 0);
                vertices[positionInBuffer].Color = color;

                positionInBuffer++;
            }
            else
            {
                GlControl.SetColour(color);
                Gl.glVertex2f(vertex.X, vertex.Y);
            }
        }

        // End is called once all the primitives have been drawn using AddVertex.
        // it will call Flush to actually submit the draw call to the graphics card, and
        // then tell the basic effect to end.
        internal void End()
        {
            if (!hasBegun)
            {
                throw new InvalidOperationException
                    ("Begin must be called before End can be called.");
            }

            if (GameBase.D3D)
            {
                // Draw whatever the user wanted us to draw
                Flush();
                // and then tell basic effect that we're done.
                basicEffect.CurrentTechnique.Passes[0].End();
                basicEffect.End();
            }
            else
            {
                Gl.glEnd();
            }

            hasBegun = false;
        }

        // Flush is called to issue the draw call to the graphics card. Once the draw
        // call is made, positionInBuffer is reset, so that AddVertex can start over
        // at the beginning. End will call this to draw the primitives that the user
        // requested, and AddVertex will call this if there is not enough room in the
        // buffer.
        private void Flush()
        {
            if (!hasBegun)
            {
                throw new InvalidOperationException
                    ("Begin must be called before Flush can be called.");
            }

            // no work to do
            if (positionInBuffer == 0)
            {
                return;
            }

            // how many primitives will we draw?
            int primitiveCount = positionInBuffer/numVertsPerPrimitive;

            // submit the draw call to the graphics card
            device.RenderState.AlphaSourceBlend = Blend.SourceAlpha;
            device.RenderState.DestinationBlend = Blend.InverseSourceAlpha;
            device.DrawUserPrimitives(primitiveType, vertices, 0, primitiveCount);

            // now that we've drawn, it's ok to reset positionInBuffer back to zero,
            // and write over any vertices that may have been set previously.
            positionInBuffer = 0;
        }

        #region Helper functions

        // NumVertsPerPrimitive is a boring helper function that tells how many vertices
        // it will take to draw each kind of primitive.
        private static int NumVertsPerPrimitive(PrimitiveType primitive)
        {
            int numVertsPerPrimitive;
            switch (primitive)
            {
                case PrimitiveType.PointList:
                    numVertsPerPrimitive = 1;
                    break;
                case PrimitiveType.LineList:
                    numVertsPerPrimitive = 2;
                    break;
                case PrimitiveType.TriangleList:
                    numVertsPerPrimitive = 3;
                    break;
                default:
                    throw new InvalidOperationException("primitive is not valid");
            }
            return numVertsPerPrimitive;
        }

        #endregion
    }
}