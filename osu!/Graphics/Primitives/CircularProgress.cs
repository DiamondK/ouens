using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.OpenGl;

namespace osu.GameModes.Play.Components
{
    internal class CircularProgress : DrawableGameComponent
    {
        internal Vector2 Position;
        internal float Alpha = 1;
        internal Color Colour;
        internal float Radius = 10;

        /// <summary>
        /// Where zero is top, 1 is bottom.
        /// </summary>
        internal float StartOffset = 0;

        internal float Progress;

        public CircularProgress()
            : base(GameBase.Game)
        {
            Initialize();
        }

        private const float da = (float)(Math.PI / 20);
        private float startAngle
        {
            get
            {
                return (float)((StartOffset - 0.5f) * Math.PI);
            }
        }

        private BasicEffect basicEffect;
        private List<VertexPositionColor> pointList;

        public override void Draw()
        {
            if (Alpha <= 0 || Progress == 0) return;

            try
            {
                float endAngle = (float)(Progress * (2 * Math.PI) + startAngle);

                float scaledRadius = Radius * GameBase.WindowRatio;
                Vector2 scaledPosition = Position * GameBase.WindowRatio + new Vector2(0, GameBase.WindowOffsetY);

                float angle1, angle2;
                if (endAngle < startAngle)
                {
                    angle1 = endAngle;
                    angle2 = startAngle;
                }
                else
                {
                    angle1 = startAngle;
                    angle2 = endAngle;
                }

                pointList = new List<VertexPositionColor>();
                pointList.Add(new VertexPositionColor(new Vector3(scaledPosition.X, scaledPosition.Y, 0), Colour));

                for (float a = angle1; a < angle2; a += da)
                    pointList.Add(new VertexPositionColor(new Vector3((float)(scaledPosition.X + Math.Cos(a) * scaledRadius), (float)(scaledPosition.Y + Math.Sin(a) * scaledRadius), 0), Colour));
                pointList.Add(new VertexPositionColor(new Vector3((float)(scaledPosition.X + Math.Cos(angle2) * scaledRadius), (float)(scaledPosition.Y + Math.Sin(angle2) * scaledRadius), 0), Colour));

                if (pointList.Count >= 3)
                {

                    if (GameBase.D3D)
                    {
                        short[] triangleListIndices = new short[pointList.Count];
                        for (short i = 0; i < pointList.Count; i++)
                            triangleListIndices[i] = i;

                        basicEffect.Begin();
                        foreach (EffectPass pass in basicEffect.CurrentTechnique.Passes)
                        {
                            pass.Begin();
                            GameBase.graphics.GraphicsDevice.DrawUserIndexedPrimitives(PrimitiveType.TriangleFan, pointList.ToArray(), 0, pointList.Count, triangleListIndices, 0, pointList.Count - 2);
                            pass.End();
                        }
                        basicEffect.End();
                    }
                    else
                    {
                        GlControl.Texturing = false;

                        Gl.glBegin(Gl.GL_TRIANGLE_FAN);
                        GlControl.SetColour(Colour);
                        foreach (VertexPositionColor p in pointList)
                            Gl.glVertex2d(p.Position.X, p.Position.Y);
                        Gl.glEnd();
                    }
                }
            }
            catch (InvalidOperationException) { }
        }

        public override void Initialize()
        {
            base.Initialize();

            if (GameBase.D3D)
            {
                if (basicEffect == null)
                {
                    GameBase.OnReload += GameBase_OnReload;
                    GameBase.OnUnload += GameBase_OnUnload;
                    GameBase.OnResolutionChange += delegate { Initialize(); };
                }
                else
                {
                    if (!basicEffect.IsDisposed)
                        basicEffect.Dispose();
                }

                basicEffect = new BasicEffect(GameBase.graphics.GraphicsDevice, null);
                basicEffect.VertexColorEnabled = true;
                basicEffect.World = Matrix.CreateTranslation(0, 0, 0);
                basicEffect.View = Matrix.CreateLookAt(new Vector3(0.0f, 0.0f, 1.0f), Vector3.Zero, Vector3.Up);
                basicEffect.Projection = Matrix.CreateOrthographicOffCenter(0, GameBase.graphics.GraphicsDevice.Viewport.Width, GameBase.graphics.GraphicsDevice.Viewport.Height, 0, 1.0f, 1000.0f);
            }
        }

        protected override void Dispose(bool isDisposing)
        {
            if (basicEffect != null) basicEffect.Dispose();

            GameBase.OnReload -= GameBase_OnReload;
            GameBase.OnUnload -= GameBase_OnUnload;
        }

        void GameBase_OnUnload(bool b)
        {
            if (!b) return;
            if (basicEffect != null) basicEffect.Dispose();
        }

        void GameBase_OnReload(bool b)
        {
            if (!b) return;
            Initialize();
        }
    }
}