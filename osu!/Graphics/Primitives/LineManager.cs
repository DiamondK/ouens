using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using osu.GameplayElements;
using osu.Graphics.OpenGl;
using osu.Graphics.Renderers;
using osu.Helpers;

namespace osu.Graphics.Primitives
{
    /// <summary>
    /// Class to handle drawing a list of lines.
    /// </summary>
    internal class LineManager : ILineRenderer
    {
        private const int MAXRES = 15; // A higher MAXRES produces rounder endcaps at the cost of more vertices
        private int bytesPerVertex;
        private GraphicsDevice device;
        private Effect effect;
        private IndexBuffer ib;
        private IndexBuffer ib2;
        private EffectParameter lengthParameter;
        private EffectParameter lineColorParameter;
        private int numIndices;
        private int numPrimitives;
        private int numPrimitives2;
        private int numVertices;
        private int numVertices2;
        private EffectParameter radiusParameter;
        private EffectParameter timeParameter;
        private VertexBuffer vb;
        private VertexBuffer vb2;
        private VertexDeclaration vdecl;
        private VertexDeclaration vdecl2;
        private EffectParameter wvpMatrixParameter;
        private const string DEFAULT_TECHNIQUE_NG = "StandardNoGradient";
        private const string DEFAULT_TECHNIQUE = "Standard";

        internal void Init(GraphicsDevice device, ContentManager content)
        {
            if (GameBase.D3D)
            {
                this.device = device;
                effect = content.Load<Effect>("Line");
                wvpMatrixParameter = effect.Parameters["worldViewProj"];
                timeParameter = effect.Parameters["time"];
                lengthParameter = effect.Parameters["length"];
                radiusParameter = effect.Parameters["radius"];
                lineColorParameter = effect.Parameters["lineColor"];

                CreateLineMesh();
            }
        }


        /// <summary>
        /// Create a mesh for a line.
        /// </summary>
        /// <remarks>
        /// The lineMesh has 3 sections:
        /// 1.  Two quads, from 0 to 1 (left to right)
        /// 2.  A half-disc, off the left side of the quad
        /// 3.  A half-disc, off the right side of the quad
        ///
        /// The X and Y coordinates of the "normal" encode the rho and theta of each vertex
        /// The "texture" encodes how much to scale and translate the vertex horizontally by length and radius
        /// </remarks>
        private void CreateLineMesh()
        {
            numVertices = 6 + (MAXRES + 2) + (MAXRES + 2);
            numPrimitives = 4 + MAXRES + MAXRES;
            numIndices = 3 * numPrimitives;
            short[] indices = new short[numIndices];
            bytesPerVertex = VertexPositionNormalTexture.SizeInBytes;
            VertexPositionNormalTexture[] tri = new VertexPositionNormalTexture[numVertices];

            // quad vertices
            int iVertexBase;
            tri[0] =
                new VertexPositionNormalTexture(new Vector3(0.0f, -1.0f, 0), new Vector3(1, 0, 0), Vector2.Zero);
            tri[1] =
                new VertexPositionNormalTexture(new Vector3(0.0f, -1.0f, 0), new Vector3(1, 0, 0), new Vector2(0, 1));
            tri[2] =
                new VertexPositionNormalTexture(new Vector3(0.0f, 0.0f, 0), new Vector3(0, 0, 0), new Vector2(0, 1));
            tri[3] =
                new VertexPositionNormalTexture(new Vector3(0.0f, 0.0f, 0), new Vector3(0, 0, 0), Vector2.Zero);
            tri[4] =
                new VertexPositionNormalTexture(new Vector3(0.0f, 1.0f, 0), new Vector3(1, 0, 0), new Vector2(0, 1));
            tri[5] =
                new VertexPositionNormalTexture(new Vector3(0.0f, 1.0f, 0), new Vector3(1, 0, 0), Vector2.Zero);

            // quad indices
            int iIndexBase;
            indices[0] = 0;
            indices[1] = 1;
            indices[2] = 2;
            indices[3] = 2;
            indices[4] = 3;
            indices[5] = 0;
            indices[6] = 2;
            indices[7] = 4;
            indices[8] = 3;
            indices[9] = 4;
            indices[10] = 5;
            indices[11] = 3;

            iVertexBase = 6;
            iIndexBase = 12;

            // left halfdisc vertices
            for (int i = 0; i < (MAXRES + 2); i++)
            {
                float x;
                float y;
                float distFromCenter;
                if (i == 0)
                {
                    x = 0;
                    y = 0;
                    distFromCenter = 0;
                }
                else
                {
                    float theta = (float)(i - 1) / (2 * MAXRES) * OsuMathHelper.TwoPi + OsuMathHelper.PiOver2;
                    x = (float)Math.Cos(theta);
                    y = (float)Math.Sin(theta);
                    distFromCenter = 1;
                }
                tri[iVertexBase + i] =
                    new VertexPositionNormalTexture(new Vector3(x, y, 0), new Vector3(distFromCenter, 0, 0),
                                                    new Vector2(1, 0));
            }

            // left halfdisc indices
            int iIndex = 0;
            for (int iPrim = 0; iPrim < MAXRES; iPrim++)
            {
                indices[iIndexBase + iIndex++] = (short)(iVertexBase + 0);
                indices[iIndexBase + iIndex++] = (short)(iVertexBase + iPrim + 1);
                indices[iIndexBase + iIndex++] = (short)(iVertexBase + iPrim + 2);
            }

            iVertexBase += (MAXRES + 2);
            iIndexBase += MAXRES * 3;

            // right halfdisc vertices
            for (int i = 0; i < (MAXRES + 2); i++)
            {
                float x;
                float y;
                float distFromCenter;
                if (i == 0)
                {
                    x = 0.0f;
                    y = 0;
                    distFromCenter = 0;
                }
                else
                {
                    float theta = (float)(i - 1) / (2 * MAXRES) * OsuMathHelper.TwoPi - OsuMathHelper.PiOver2;
                    x = (float)Math.Cos(theta);
                    y = (float)Math.Sin(theta);
                    distFromCenter = 1;
                }
                tri[iVertexBase + i] =
                    new VertexPositionNormalTexture(new Vector3(x, y, 0), new Vector3(distFromCenter, 0, 0),
                                                    new Vector2(1, 1));
            }

            // right halfdisc indices
            iIndex = 0;
            for (int iPrim = 0; iPrim < MAXRES; iPrim++)
            {
                indices[iIndexBase + iIndex++] = (short)(iVertexBase + 0);
                indices[iIndexBase + iIndex++] = (short)(iVertexBase + iPrim + 1);
                indices[iIndexBase + iIndex++] = (short)(iVertexBase + iPrim + 2);
            }

            vb =
                new VertexBuffer(device, numVertices * bytesPerVertex, ResourceUsage.None,
                                 ResourceManagementMode.Automatic);
            vb.SetData(tri);
            vdecl = new VertexDeclaration(device, VertexPositionNormalTexture.VertexElements);

            ib =
                new IndexBuffer(device, numIndices * 2, ResourceUsage.None, ResourceManagementMode.Automatic,
                                IndexElementSize.SixteenBits);
            ib.SetData(indices);


            numVertices2 = 6;
            numPrimitives2 = 4;
            indices = new short[numIndices];
            bytesPerVertex = VertexPositionNormalTexture.SizeInBytes;
            tri = new VertexPositionNormalTexture[numVertices];

            // quad vertices
            tri[0] =
                new VertexPositionNormalTexture(new Vector3(0.0f, -1.0f, 0), new Vector3(1, 0, 0), Vector2.Zero);
            tri[1] =
                new VertexPositionNormalTexture(new Vector3(0.0f, -1.0f, 0), new Vector3(1, 0, 0), new Vector2(0, 1));
            tri[2] =
                new VertexPositionNormalTexture(new Vector3(0.0f, 0.0f, 0), new Vector3(0, 0, 0), new Vector2(0, 1));
            tri[3] =
                new VertexPositionNormalTexture(new Vector3(0.0f, 0.0f, 0), new Vector3(0, 0, 0), Vector2.Zero);
            tri[4] =
                new VertexPositionNormalTexture(new Vector3(0.0f, 1.0f, 0), new Vector3(1, 0, 0), new Vector2(0, 1));
            tri[5] =
                new VertexPositionNormalTexture(new Vector3(0.0f, 1.0f, 0), new Vector3(1, 0, 0), Vector2.Zero);

            // quad indices
            indices[0] = 0;
            indices[1] = 1;
            indices[2] = 2;
            indices[3] = 2;
            indices[4] = 3;
            indices[5] = 0;
            indices[6] = 2;
            indices[7] = 4;
            indices[8] = 3;
            indices[9] = 4;
            indices[10] = 5;
            indices[11] = 3;

            vb2 =
                new VertexBuffer(device, numVertices * bytesPerVertex, ResourceUsage.None,
                                 ResourceManagementMode.Automatic);
            vb2.SetData(tri);
            vdecl2 = new VertexDeclaration(device, VertexPositionNormalTexture.VertexElements);

            ib2 =
                new IndexBuffer(device, numIndices * 2, ResourceUsage.None, ResourceManagementMode.Automatic,
                                IndexElementSize.SixteenBits);
            ib2.SetData(indices);
        }

        internal void DrawQuad(Vector2 topLeft, Vector2 bottomRight, Color colour)
        {
            if (GameBase.D3D)
            {
                if (topLeft.Y > bottomRight.Y)
                {
                    Vector2 temp = topLeft;
                    topLeft = bottomRight;
                    bottomRight = temp;
                }

                effect.CurrentTechnique = effect.Techniques["NoBlur"];

                effect.Begin();
                EffectPass pass = effect.CurrentTechnique.Passes[0];

                device.VertexDeclaration = vdecl2;
                device.Vertices[0].SetSource(vb2, 0, bytesPerVertex);
                device.Indices = ib2;
                device.RenderState.CullMode = CullMode.CullClockwiseFace;

                pass.Begin();

                timeParameter.SetValue(0);

                Vector4 lineColor = new Vector4((float)colour.R / 255, (float)colour.G / 255, (float)colour.B / 255,
                                                (float)colour.A / 255);
                lineColorParameter.SetValue(lineColor);

                float globalRadius = Math.Abs((bottomRight.Y - topLeft.Y) / 2);

                float centre = topLeft.Y + Math.Abs((bottomRight.Y - topLeft.Y) / 2);

                if (globalRadius != 0)
                    radiusParameter.SetValue(globalRadius);

                Line line = new Line(new Vector2(topLeft.X, centre), new Vector2(bottomRight.X, centre));

                Matrix worldViewProjMatrix = line.WorldMatrix() * Matrix.Identity * HitObjectManager.projMatrix;
                wvpMatrixParameter.SetValue(worldViewProjMatrix);
                lengthParameter.SetValue(line.rho);
                if (globalRadius == 0)
                    radiusParameter.SetValue(1);
                effect.CommitChanges();
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, numVertices2, 0, numPrimitives2);
                pass.End();

                effect.End();
            }
            else
            {
                GlControl.Texturing = false;
                GlControl.SetColour(colour);

                Gl.glBegin(Gl.GL_QUADS);
                Gl.glVertex2f(topLeft.X, bottomRight.Y);
                Gl.glVertex2f(bottomRight.X, bottomRight.Y);
                Gl.glVertex2f(bottomRight.X, topLeft.Y);
                Gl.glVertex2f(topLeft.X, topLeft.Y);

                Gl.glEnd();
            }
        }

        internal void DrawLine(Line line, float radius, Color colour)
        {
            Draw(line, radius, colour);
        }

        public void Draw(Line line, float radius, Color colour)
        {
            List<Line> lines = new List<Line>(1);
            lines.Add(line);

            Draw(lines, null, radius, colour);
        }

        public virtual void Draw(List<Line> lines, Line prev, float radius, Color colour)
        {
            Draw(lines, radius, colour, 0, DEFAULT_TECHNIQUE_NG, true);
        }

        /// <summary>
        /// Draw a list of Lines.
        /// </summary>
        /// <remarks>
        /// Set globalRadius = 0 to use the radius stored in each Line.
        /// Set globalColor to Color.TransparentBlack to use the color stored in each Line.
        /// </remarks>
        internal void Draw(List<Line> lineList, float globalRadius, Color globalColour, float time, string techniqueName,
                           bool roundEdges)
        {
            if (GameBase.D3D)
            {
                if (GameBase.PixelShaderVersion < 2 && techniqueName.StartsWith(DEFAULT_TECHNIQUE))
                    techniqueName += "11";

                if (techniqueName == null)
                    effect.CurrentTechnique = effect.Techniques[0];
                else
                {
                    EffectTechnique t = effect.Techniques[techniqueName];
                    effect.CurrentTechnique = t;
                }
                effect.Begin();
                EffectPass pass = effect.CurrentTechnique.Passes[0];

                int v, p;
                if (roundEdges)
                {
                    device.VertexDeclaration = vdecl;
                    device.Vertices[0].SetSource(vb, 0, bytesPerVertex);
                    device.Indices = ib;
                    device.RenderState.CullMode = CullMode.None;
                    v = numVertices;
                    p = numPrimitives;
                }
                else
                {
                    device.VertexDeclaration = vdecl2;
                    device.Vertices[0].SetSource(vb2, 0, bytesPerVertex);
                    device.Indices = ib2;
                    device.RenderState.CullMode = CullMode.CullClockwiseFace;
                    v = numVertices2;
                    p = numPrimitives2;
                }

                pass.Begin();

                timeParameter.SetValue(time);

                Vector4 lineColor =
                    new Vector4((float)globalColour.R / 255, (float)globalColour.G / 255, (float)globalColour.B / 255,
                                (float)globalColour.A / 255);
                lineColorParameter.SetValue(lineColor);

                radiusParameter.SetValue(globalRadius);

                Color lastColour = globalColour;
                foreach (Line line in lineList)
                {
                    Matrix worldViewProjMatrix = line.WorldMatrix() * Matrix.Identity * HitObjectManager.projMatrix;
                    wvpMatrixParameter.SetValue(worldViewProjMatrix);
                    ColouredLine cl = line as ColouredLine;
                    if (cl != null && cl.colour != Color.White && cl.colour != lastColour)
                    {
                        lineColorParameter.SetValue(new Vector4((float)cl.colour.R / 255, (float)cl.colour.G / 255, (float)cl.colour.B / 255, (float)cl.colour.A / 255));
                        lastColour = cl.colour;
                    }
                    lengthParameter.SetValue(line.rho);
                    effect.CommitChanges();
                    device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, v, 0, p);
                }
                pass.End();

                effect.End();
            }
            else
            {
                GlControl.Texturing = false;
                GlControl.SetColour(globalColour);

                bool noBlur = techniqueName == @"NoBlur";
                foreach (Line l in lineList)
                {
                    ColouredLine cl = l as ColouredLine;
                    Vector2 direction = Vector2.Normalize(l.p2 - l.p1);

                    Vector2 cross = new Vector2(globalRadius * direction.Y, -globalRadius * direction.X);

                    if (cl != null && cl.colour != Color.White)
                        globalColour = cl.colour;

                    Gl.glBegin(Gl.GL_QUADS);

                    Vector2 p1n = l.p1 + cross;
                    Vector2 p1m = l.p1 - cross;

                    Vector2 p2n = l.p2 - cross;
                    Vector2 p2m = l.p2 + cross;

                    Gl.glVertex2f(p1n.X, p1n.Y);
                    Gl.glVertex2f(p2m.X, p2m.Y);

                    if (!noBlur)
                    {
                        GlControl.SetColour(new Color(globalColour.R, globalColour.G, globalColour.B, 128));

                        Gl.glVertex2f(l.p2.X, l.p2.Y);
                        Gl.glVertex2f(l.p1.X, l.p1.Y);

                        Gl.glVertex2f(l.p1.X, l.p1.Y);
                        Gl.glVertex2f(l.p2.X, l.p2.Y);
                    }

                    GlControl.SetColour(globalColour);

                    Gl.glVertex2f(p2n.X, p2n.Y);
                    Gl.glVertex2f(p1m.X, p1m.Y);

                    Gl.glEnd();

                    if (roundEdges)
                    {
                        HalfCircle(l.p1.X, l.p1.Y, globalRadius, (float)(l.theta + Math.PI * 5 / 2), globalColour, true);
                        HalfCircle(l.p2.X, l.p2.Y, globalRadius, (float)(l.theta + Math.PI * 3 / 2), globalColour, true);
                    }
                }
            }
        }

        internal void HalfCircle(float x, float y, float radius, float startAngle, Color colourCentre, bool shade)
        {
            const float da = (float)(Math.PI / MAXRES);

            Gl.glBegin(Gl.GL_TRIANGLE_FAN);
            GlControl.SetColour(new Color(colourCentre.R, colourCentre.G, colourCentre.B, 128));

            Gl.glVertex2d(x, y);

            if (!shade)
                GlControl.SetColour(colourCentre);

            float endAngle = (float)(Math.PI + startAngle);

            for (float a = startAngle; a <= endAngle; a += da)
            {
                if (shade)
                {
                    float alpha = (float)Math.Max(0.55f, Math.Sqrt(Math.Abs((a - startAngle - Math.PI / 2) / (Math.PI / 2))));
                    GlControl.SetColour(new Color(colourCentre.R, colourCentre.G, colourCentre.B, (byte)(alpha * colourCentre.A)));
                }
                Gl.glVertex2d(x + Math.Cos(a) * radius, y + Math.Sin(a) * radius);
            }

            Gl.glEnd();
        }

        public void DrawPoint(double xPctr, double yPctr, Color c)
        {
            DrawQuad(new Vector2((float)(xPctr - 2), (float)(yPctr - 2)), new Vector2((float)(xPctr + 2), (float)(yPctr + 2)), c);
        }

        public bool ProgressiveRendering
        {
            get
            {
                return false;
            }
        }

        ~LineManager()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool am_disposing)
        {
            if (am_disposing)
            {
                TryDispose(ib);
                TryDispose(ib2);
                TryDispose(vb);
                TryDispose(vb2);
                TryDispose(vdecl);
                TryDispose(vdecl2);
            }
        }

        private void TryDispose(IDisposable d)
        {
            if (d != null) d.Dispose();
        }

    }
}