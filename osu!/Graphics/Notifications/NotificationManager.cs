﻿
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.GameModes.Play;
using osu.Graphics.Renderers;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu.Input;
using osu.Online;
using osu_common.Helpers;

namespace osu.Graphics.Notifications
{
    internal static class NotificationManager
    {
        private static readonly Queue<Notification> NotificationQueue = new Queue<Notification>();
        private static int lastQueueDisplay;
        private static pText massiveMessage;
        private static pSprite massiveMessageBg;

        private static bool canDisplay { get { return (!Player.Playing || ConfigManager.sPopupDuringGameplay) && InputManager.HandleInput; } }


        internal static void ShowMessageMassive(string s, int time = 5000)
        {
            if (s == null) return;

            if (GameBase.Tournament)
            {
                Logger.Log(s);
                return;
            }

            GameBase.Scheduler.Add(delegate
                                  {
                                      int startTime = GameBase.Time;

                                      if (massiveMessage != null && massiveMessage.Alpha > 0)
                                      {
                                          ClearMessageMassive();
                                      }

                                      massiveMessage = new pText(s, 18, new Vector2(GameBase.WindowWidthScaled / 2, 240), new Vector2(GameBase.WindowWidth / GameBase.WindowRatio, 0), 1, false, Color.White, false);
                                      massiveMessage.Origin = Origins.TopCentre;
                                      massiveMessage.TextAlignment = TextAlignment.Centre;
                                      massiveMessage.TextBorder = true;
                                      massiveMessage.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, startTime,
                                                                                            startTime + 100));
                                      massiveMessage.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, startTime + time,
                                                                                            startTime + time + 150));
                                      if (GameBase.spriteManagerOverlayHighest != null)
                                          GameBase.spriteManagerOverlayHighest.Add(massiveMessage);

                                      //Call the long version of MeasureText so we don't make a texture on the wrong thread.
                                      float yMeasure = massiveMessage.MeasureText().Y;

                                      massiveMessage.Position.Y -= yMeasure / 2;

                                      yMeasure *= 1.5f;

                                      massiveMessageBg = new pSprite(GameBase.WhitePixel, new Vector2(0, 240 - yMeasure / 2), 0.999f,
                                                                   false, new Color(0, 0, 0, 180));
                                      massiveMessageBg.Scale = 1.6f;
                                      massiveMessageBg.Origin = Origins.TopLeft;
                                      massiveMessageBg.VectorScale = new Vector2(GameBase.WindowWidthScaled, yMeasure);

                                      massiveMessageBg.Transformations.Add(new Transformation(TransformationType.VectorScale, new Vector2(GameBase.WindowWidthScaled, 0),
                                                                                    massiveMessageBg.VectorScale, startTime, startTime + 300, EasingTypes.OutBack));
                                      massiveMessageBg.Transformations.Add(new Transformation(new Vector2(0, 240), massiveMessageBg.InitialPosition, startTime,
                                                                                    startTime + 300, EasingTypes.OutBack));
                                      massiveMessageBg.Transformations.Add(new Transformation(TransformationType.Fade, 180 / 255f, 0, startTime + time,
                                                                                            startTime + time + 150));
                                      if (GameBase.spriteManagerOverlayHighest != null)
                                          GameBase.spriteManagerOverlayHighest.Add(massiveMessageBg);
                                  }, true);
        }

        internal static void ClearMessageMassive(object sender, EventArgs e)
        {
            ClearMessageMassive();
        }

        internal static void ClearMessageMassive()
        {
            GameBase.Scheduler.Add(delegate
            {
                if (massiveMessage == null) return;

                massiveMessage.FadeOut(300);
                massiveMessage.MoveToRelative(new Vector2(0, 15), 600, EasingTypes.In);
                massiveMessage.HandleInput = false;

                massiveMessageBg.FadeOut(300);

                massiveMessage = null;
            });
        }

        internal static void ShowMessage(string s)
        {
            GameBase.Scheduler.Add(delegate { ShowMessage(new Notification(s, s.Length * 100, Color.DarkRed)); }, true);
        }

        internal static void ShowMessage(string s, Color background, int duration, VoidDelegate action = null)
        {
            GameBase.Scheduler.Add(delegate { ShowMessage(new Notification(s, duration, background, action)); }, true);
        }

        private static Notification lastNotification;

        internal static void ShowMessage(Notification notification)
        {
            if (GameBase.Tournament)
            {
                Logger.Log(notification.Text);
                return;
            }

            if (lastNotification != null && notification.Text == lastNotification.Text && lastNotification.DisplayTime + lastNotification.Duration > GameBase.Time)
                return;

            if (!canDisplay)
            {
                NotificationQueue.Enqueue(notification);
                return;
            }

            lastNotification = notification;

            GameBase.Scheduler.Add(delegate
                                  {
                                      List<pDrawable> tagged = GameBase.spriteManagerOverlayHighest.GetTagged(@"notice");

                                      foreach (pDrawable pspr in tagged)
                                      {
                                          Transformation move = pspr.Transformations.Find(tr => tr.Type == TransformationType.Movement);
                                          if (move != null)
                                          {
                                              pspr.Position = move.EndVector;
                                              pspr.Transformations.Remove(move);
                                          }
                                      }

                                      float push = notification.Display();

                                      tagged.ForEach(sp => sp.MoveToRelative(new Vector2(0, -push), 800, EasingTypes.OutElasticQuarter));


                                      GameBase.spriteManagerOverlayHighest.Add(notification.SpriteCollection);
                                  }, true);
        }

        internal static void NotificationClicked(object sender, EventArgs e)
        {
            pSprite p = sender as pSprite;
            p.FadeOut(50);
            p.AlwaysDraw = false;
        }

        internal static void Update()
        {
            if (canDisplay)
            {
                if (GameBase.Time > lastQueueDisplay + 500 && NotificationQueue.Count > 0)
                {
                    lastQueueDisplay = GameBase.Time;
                    Notification n = NotificationQueue.Dequeue();
                    ShowMessage(n);
                }
            }
            else if (!ConfigManager.sPopupDuringGameplay && Player.Playing)
            {
                if (lastQueueDisplay > 0)
                {
                    lastQueueDisplay = 0;
                    foreach (pSprite p in GameBase.spriteManagerOverlayHighest.GetTagged(@"notice"))
                    {
                        p.AlwaysDraw = false;
                        p.FadeOut(100);
                    }
                }
            }
        }

        public static DialogResult MessageBox(string text, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            GameBase.MenuActive = true;
            DialogResult r = System.Windows.Forms.MessageBox.Show(text, "osu!", buttons, icon);
            GameBase.MenuActive = false;

            return r;
        }

        static NotificationAnchor Anchoring = NotificationAnchor.Bottom;

        public static void SetAnchoring(NotificationAnchor anchor)
        {
            Anchoring = anchor;

            switch (anchor)
            {
                case NotificationAnchor.Top:
                    foreach (pSprite p in GameBase.spriteManagerOverlayHighest.GetTagged(@"notice"))
                        if (p.Position.Y > 320)
                            p.MoveTo(new Vector2(p.Position.X, 480 - p.Position.Y), 300, EasingTypes.Out);
                    break;
                case NotificationAnchor.Bottom:
                    foreach (pSprite p in GameBase.spriteManagerOverlayHighest.GetTagged(@"notice"))
                        if (p.Position.Y < 320)
                            p.MoveTo(new Vector2(p.Position.X, 480 - p.Position.Y), 300, EasingTypes.Out);
                    break;
            }
        }
    }

    enum NotificationAnchor { Top, Bottom };
}