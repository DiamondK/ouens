﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.GameModes.Play;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu.Input;
using osu.Input.Handlers;
using osu_common.Helpers;

namespace osu.Graphics.Notifications
{
    internal class Notification
    {
        internal readonly string Text;
        internal readonly int Duration;
        internal int DisplayTime { get; private set; }

        internal readonly Color Colour;
        internal List<pSprite> SpriteCollection = new List<pSprite>();

        const int TOP_HEIGHT = 13;
        const int MIDDLE_HEIGHT = 13;
        const int BOTTOM_HEIGHT = 30;

        const float TOP_HEIGHT_WINDOW = TOP_HEIGHT * 0.625f;
        const float MIDDLE_HEIGHT_WINDOW = MIDDLE_HEIGHT * 0.625f;
        const float BOTTOM_HEIGHT_WINDOW = BOTTOM_HEIGHT * 0.625f;

        const int BUFFER = 28;

        const float total_width = 160f;
        const float drawable_width = 150f;

        float totalHeight;
        private pSprite middle;
        private pSprite top;
        private pSprite bottom;
        private pText content;

        internal VoidDelegate Action;
        private bool Closed;

        internal Notification(string text, int duration, Color colour, VoidDelegate action = null)
        {
            Text = text;
            Duration = duration;
            Colour = colour;
            Action = action;
        }

        void onHoverLost(object sender, EventArgs e)
        {
            if (Closed) return;

            DisplayTime = GameBase.Time;

            SpriteCollection.ForEach(s =>
                {
                    if (s != content) s.FadeColour(Colour, 60);

                    s.Transformations.RemoveAll(t => t.Type == TransformationType.Fade && t.EndFloat == 0);
                    s.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + 200 + Duration - 500, GameBase.Time + 200 + Duration, EasingTypes.In));
                });
        }

        void onHover(object sender, EventArgs e)
        {
            if (Closed) return;

            DisplayTime = GameBase.Time;

            SpriteCollection.ForEach(s =>
                {
                    if (s != content) s.FadeColour(Color.White, 60);

                    s.Transformations.RemoveAll(t => t.Type == TransformationType.Fade && t.EndFloat == 0);
                    s.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + 200 + Duration - 500 + 60000, GameBase.Time + 200 + Duration + 60000, EasingTypes.In));
                });
        }

        void onClick(object sender, EventArgs e)
        {
            Closed = true;
            SpriteCollection.ForEach(s => s.FadeOut(100));
            if (InputManager.leftButton == ButtonState.Pressed && Action != null && !Player.Playing)
                Action();
        }

        internal float Display()
        {
            pTexture notificationTexture = SkinManager.Load(@"notification", SkinSource.Osu);

            float total_width = notificationTexture.Width * 0.625f;
            float drawable_width = total_width * 0.90f;

            float resulting_padding = (total_width - drawable_width) / 2;

            float currentHeight = GameBase.WindowHeightScaled;

            content = new pText(Text, 9f - ((GameBase.WindowHeight - 1024) / 256f), new Vector2(3 + resulting_padding + drawable_width, currentHeight + resulting_padding), new Vector2(drawable_width, 0), 1, false, Color.White, false);
            //content.BorderColour = new Color(20, 20, 20);
            content.Field = Fields.TopRight;
            content.Origin = Origins.TopLeft;
            content.TextBold = true;

            Vector2 size = content.MeasureText();

            top = new pSprite(notificationTexture, Fields.TopRight, Origins.TopRight, Clocks.Game, new Vector2(0, currentHeight), 0.98f, false, Colour);
            top.ExactCoordinates = false;
            top.DrawHeight = TOP_HEIGHT;
            top.OnClick += onClick;
            top.HandleInput = true;
            top.OnHover += onHover;
            top.OnHoverLost += onHoverLost;

            currentHeight += TOP_HEIGHT_WINDOW;

            float excessHeight = size.Y - (TOP_HEIGHT_WINDOW + BOTTOM_HEIGHT_WINDOW - resulting_padding * 2);

            if (excessHeight > 0)
            {
                middle = new pSprite(notificationTexture, Fields.TopRight, Origins.TopRight, Clocks.Game, new Vector2(0, currentHeight), 0.98f, false, Colour);
                middle.ExactCoordinates = false;
                middle.DrawTop = TOP_HEIGHT;
                middle.DrawHeight = MIDDLE_HEIGHT;
                middle.OnClick += onClick;
                middle.HandleInput = true;
                middle.OnHover += onHover;
                middle.OnHoverLost += onHoverLost;
                middle.VectorScale.Y = excessHeight / MIDDLE_HEIGHT_WINDOW;

                SpriteCollection.Add(middle);

                currentHeight += MIDDLE_HEIGHT_WINDOW * middle.VectorScale.Y;
            }

            bottom = new pSprite(notificationTexture, Fields.TopRight, Origins.TopRight, Clocks.Game, new Vector2(0, currentHeight), 0.98f, false, Colour);
            bottom.ExactCoordinates = false;
            bottom.DrawTop = TOP_HEIGHT + MIDDLE_HEIGHT;
            bottom.DrawHeight = BOTTOM_HEIGHT;
            bottom.HandleInput = true;
            bottom.OnHover += onHover;
            bottom.OnHoverLost += onHoverLost;
            bottom.OnClick += onClick;

            currentHeight += BOTTOM_HEIGHT_WINDOW;

            SpriteCollection.Add(top);
            SpriteCollection.Add(content);
            SpriteCollection.Add(bottom);

            totalHeight = currentHeight - GameBase.WindowHeightScaled;

            DisplayTime = GameBase.Time;

            foreach (pSprite p in SpriteCollection)
            {
                p.Position.Y -= totalHeight + BUFFER;

                p.Position.X -= 100;

                p.MoveToRelative(new Vector2(100, 0), 800, EasingTypes.OutElasticHalf);

                p.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, GameBase.Time, GameBase.Time + 200));
                p.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + 200 + Duration - 500, GameBase.Time + 200 + Duration, EasingTypes.In));
                p.Tag = @"notice";

                GameBase.Scheduler.AddDelayed(delegate { content.ExactCoordinates = true; }, 200);
            }

            return totalHeight;
        }
    }
}

