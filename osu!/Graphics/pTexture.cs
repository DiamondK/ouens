﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.OpenGl;
using osu.Graphics.Skinning;
using osu_common;
using osudata;

namespace osu.Graphics
{
    public class pTexture : IDisposable
    {
        public string assetName;
        public bool fromResourceStore;
        internal int InternalWidth;
        internal int InternalHeight;

        public int Width { get { return InternalWidth / DpiScale; } }
        public int Height { get { return InternalHeight / DpiScale; } }

        public bool UnloadOnModeChange = true;

        internal int LastAccess;
        internal bool isDisposed;
        public SkinSource Source;

        /// <summary>
        /// Temporary helper to transfer xna to gl texture
        /// </summary>
        /// <param name="xnaTexture">The xna texture.</param>
        public pTexture(Texture2D xnaTexture)
        {
            InternalWidth = xnaTexture.Width;
            InternalHeight = xnaTexture.Height;

            TextureXna = xnaTexture;
        }

        ~pTexture()
        {
            //no need to dispose of stuff when we know the game is going to be shut down.
            if (GameBase.Mode == OsuModes.Exit || GameBase.Mode == OsuModes.Update) return;

            Dispose(false);
        }

        public pTexture(Texture2D textureXna, TextureGl textureGl, int width, int height)
        {
            TextureXna = textureXna;
            TextureGl = textureGl;
            InternalWidth = width;
            InternalHeight = height;
        }

        private pTexture()
        {
        }

        internal TextureGl TextureGl;
        internal bool TrackAccessTime;

        internal Texture2D textureXna;
        internal Texture2D TextureXna
        {
            get { return textureXna; }
            set
            {
                if (textureXna != null) textureXna.Disposing -= TextureXna_Disposing;
                textureXna = value;
                if (textureXna != null) textureXna.Disposing += TextureXna_Disposing;
            }

        }

        void TextureXna_Disposing(object sender, EventArgs e)
        {
            isDisposed = true;
        }

        public bool Disposable = true;
        public int DpiScale = 1;

        public bool IsDisposed
        {
            get
            {
                if (TrackAccessTime)
                    LastAccess = GameBase.Time;
                return isDisposed;
            }
            set { isDisposed = value; }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        /// <summary>
        /// Disposes of textures but leaves the GC finalizer in place.
        /// This is used to temporarily unload sprites which are not accessed in a while.
        /// </summary>
        internal void TemporalDispose()
        {
            isDisposed = true;

            if (TextureXna != null)
            {
                TextureXna.Dispose();
                TextureXna = null;
            }
            if (TextureGl != null)
            {
                TextureGl.Dispose();
                TextureGl = null;
            }
        }

        private void Dispose(bool isDisposing)
        {
            if (isDisposed) return;
            isDisposed = true;

            if (TextureXna != null)
            {
                TextureXna.Dispose();
                TextureXna = null;
            }
            if (TextureGl != null)
            {
                TextureGl.Dispose();
                TextureGl = null;
            }
        }

        #endregion

        public void SetData(byte[] data)
        {
            SetData(data, 0, 0);
        }

        public void SetData(byte[] data, int level, int format)
        {
            if (GameBase.D3D && TextureXna != null)
                TextureXna.SetData(level, null, data, 0, data.Length, SetDataOptions.Discard | SetDataOptions.NoOverwrite);
            if (GameBase.OGL && TextureGl != null)
            {
                if (format != 0)
                    TextureGl.SetData(data, level, format);
                else
                    TextureGl.SetData(data, level);
            }
        }

        public static pTexture FromNewResourceStore(string filename)
        {
            bool highResolution = GameBase.UseHighResolutionSprites;
            if (highResolution) filename += @"@2x";

            using (Bitmap b = osu_gameplay.ResourcesStore.ResourceManager.GetObject(filename) as Bitmap)
                if (b != null)
                {
                    pTexture tex = FromBitmap(b);
                    if (highResolution) tex.DpiScale = 2;
                    return tex;
                }

            //dll modification checks
            using (Bitmap b = osu_ui.ResourcesStore.ResourceManager.GetObject(filename) as Bitmap)
                if (b != null)
                {
                    bool valid = true;

                    switch (filename)
                    {
                        //case @"menu-osu":
                        //    valid &= b.GetPixel(320 / 2, 320 / 2) == System.Drawing.Color.FromArgb(255, 255, 190, 219);
                        //    valid &= b.GetPixel(220 / 2, 220 / 2) == System.Drawing.Color.FromArgb(255, 255, 199, 227);
                        //    valid &= b.GetPixel(420 / 2, 420 / 2) == System.Drawing.Color.FromArgb(255, 255, 245, 249);
                        //    break;
                        //case @"menu-osu@2x":
                        //    valid &= b.GetPixel(320, 320) == System.Drawing.Color.FromArgb(255, 255, 190, 220);
                        //    valid &= b.GetPixel(220, 220) == System.Drawing.Color.FromArgb(255, 255, 199, 227);
                        //    valid &= b.GetPixel(420, 420) == System.Drawing.Color.FromArgb(255, 255, 245, 249);
                        //    break;
                        //case @"menu-background":
                        //    valid &= b.GetPixel(0, 0) == System.Drawing.Color.FromArgb(243, 0, 206, 255);
                        //    valid &= b.GetPixel(400 / 2, 400 / 2) == System.Drawing.Color.FromArgb(255, 0, 165, 255);
                        //    valid &= b.GetPixel(1552 / 2, 1024 / 2) == System.Drawing.Color.FromArgb(255, 0, 144, 255);
                        //    break;
                        //case @"menu-background@2x":
                        //    valid &= b.GetPixel(0, 0) == System.Drawing.Color.FromArgb(228, 1, 206, 255);
                        //    valid &= b.GetPixel(400, 400) == System.Drawing.Color.FromArgb(255, 0, 165, 255);
                        //    valid &= b.GetPixel(1552, 1024) == System.Drawing.Color.FromArgb(255, 0, 144, 255);
                        //    break;
                    }

                    if (!valid)
                    {
#if DEBUG
                        throw new Exception(@"DLL modification checks need updating");
#else
                        osuMain.ExitImmediately();
#endif
                    }

                    pTexture tex = FromBitmap(b);
                    if (highResolution) tex.DpiScale = 2;
                    return tex;
                }
            return null;
        }

        /// <summary>
        /// Read a pTexture from the ResourceStore (ouenresources project).
        /// </summary>
        public static pTexture FromResourceStore(string filename)
        {
            //load using newer resources where available.
            if (GameBase.NewGraphicsAvailable)
            {
                pTexture tex = FromNewResourceStore(filename);
                if (tex != null)
                {
                    tex.assetName = filename;
                    tex.fromResourceStore = true;

                    return tex;
                }
            }

            byte[] bytes = ResourcesStore.ResourceManager.GetObject(filename) as byte[];

            if (bytes == null) return null;

            pTexture pt = new pTexture();

            pt.assetName = filename;
            pt.fromResourceStore = true;

            using (Stream stream = new MemoryStream(bytes))
            {
                using (HaxBinaryReader br = new HaxBinaryReader(stream))
                {
                    //XNA pipeline header crap.  Fuck it all.
                    br.ReadBytes(10);
                    int typeCount = br.Read7BitEncodedInt();
                    for (int i = 0; i < typeCount; i++)
                    {
                        br.ReadString();
                        br.ReadInt32();
                    }
                    br.Read7BitEncodedInt();
                    br.Read7BitEncodedInt();
                    //And that's the header dealt with.

                    SurfaceFormat format = (SurfaceFormat)br.ReadInt32();
                    pt.InternalWidth = br.ReadInt32();
                    pt.InternalHeight = br.ReadInt32();
                    int numberLevels = br.ReadInt32();

                    if (GameBase.D3D)
                        pt.TextureXna = new Texture2D(GameBase.graphics.GraphicsDevice, pt.Width, pt.Height,
                                                      numberLevels,
                                                      ResourceUsage.None, format, ResourceManagementMode.Automatic);
                    if (GameBase.OGL)
                        pt.TextureGl = new TextureGl(pt.Width, pt.Height);

                    for (int i = 0; i < numberLevels; i++)
                    {
                        int count = br.ReadInt32();
                        byte[] data = br.ReadBytes(count);
                        pt.SetData(data, i, 0);
                    }
                }
            }

            return pt;
        }

        /// <summary>
        /// Read a pTexture from an arbritrary file.
        /// </summary>
        public static pTexture FromFile(string filename)
        {
            if (!File.Exists(filename)) return null;

            try
            {
                using (Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
                    return FromStream(stream, filename);
            }
            catch
            {
                return null;
            }
        }

        public static pTexture FromStream(Stream stream, string assetname)
        {
            return FromStream(stream, assetname, false);
        }

        /// <summary>
        /// Read a pTexture from an arbritrary file.
        /// </summary>
        public static pTexture FromStream(Stream stream, string assetname, bool saveToFile)
        {
            if (stream == null || stream.Length == 0)
                return null;

            try
            {
                using (Bitmap b = (Bitmap)Image.FromStream(stream, false, false))
                {
                    BitmapData data = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadOnly,
                                                 PixelFormat.Format32bppArgb);
                    if (saveToFile)
                    {
                        byte[] bitmap = new byte[b.Width * b.Height * 4];
                        Marshal.Copy(data.Scan0, bitmap, 0, bitmap.Length);
                        File.WriteAllBytes(assetname, bitmap);
                    }

                    pTexture tex = FromRawBytes(data.Scan0, b.Width, b.Height);
                    tex.assetName = assetname;
                    b.UnlockBits(data);
                    return tex;
                }
            }
            catch
            {
                return null;
            }
        }

        public static pTexture FromBitmap(Bitmap b)
        {
            if (b == null) return null;

            try
            {
                BitmapData data = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadOnly,
                                                PixelFormat.Format32bppArgb);
                pTexture tex = FromRawBytes(data.Scan0, b.Width, b.Height);
                b.UnlockBits(data);
                return tex;
            }
            catch
            {
                return null;
            }
        }

        public static pTexture FromBytes(byte[] data)
        {
            using (MemoryStream ms = new MemoryStream(data))
                return FromStream(ms, "");
        }

        public static pTexture FromBytesSaveRawToFile(byte[] data, string filename)
        {
            using (MemoryStream ms = new MemoryStream(data))
                return FromStream(ms, filename, true);
        }

        public static pTexture FromRawBytes(IntPtr location, int width, int height, bool canRecreate = false, string assetName = null)
        {
            pTexture pt = new pTexture();
            pt.InternalWidth = width;
            pt.InternalHeight = height;

            try
            {
                if (GameBase.D3D)
                {
                    pt.TextureXna = new Texture2D(GameBase.graphics.GraphicsDevice, pt.Width, pt.Height, 1, ResourceUsage.None, SurfaceFormat.Color, canRecreate ? ResourceManagementMode.Manual : ResourceManagementMode.Automatic);
                }
                else if (GameBase.OGL)
                    pt.TextureGl = new TextureGl(pt.Width, pt.Height);

                pt.SetData(location, width, height, assetName);
            }
            catch
            {
            }

            return pt;
        }

        public void SetData(Bitmap bitmap)
        {
            BitmapData data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly,
                                                PixelFormat.Format32bppArgb);
            SetData(data.Scan0, bitmap.Width, bitmap.Height);
            bitmap.UnlockBits(data);
        }

        static Stack<byte[]> setDataBuffers = new Stack<byte[]>();

        public void SetData(IntPtr location, int width, int height, string assetName = null)
        {
#if DEBUG
            if (assetName != null) this.assetName = assetName;
#endif

            InternalWidth = width;
            InternalHeight = height;

            try
            {
                if (GameBase.D3D)
                {
                    int requiredBytes = width * height * 4;

                    byte[] buffer = null;
                    lock (setDataBuffers)
                        if (setDataBuffers.Count > 0) buffer = setDataBuffers.Pop();


                    if (buffer == null || buffer.Length < requiredBytes)
                        buffer = new byte[requiredBytes];

                    Marshal.Copy(location, buffer, 0, requiredBytes);
                    TextureXna.SetData(buffer, 0, requiredBytes, SetDataOptions.Discard | SetDataOptions.NoOverwrite);

                    lock (setDataBuffers)
                        setDataBuffers.Push(buffer);
                }

                if (GameBase.OGL)
                {
                    //OpenGL outperforms XNA in this case as we can remain in native unsafe territory.
                    TextureGl.SetData(location, 0, 0);
                }
            }
            catch
            {
            }
        }

        public static pTexture FromRawBytes(byte[] bitmap, int width, int height, bool canRecreate = false)
        {
            pTexture pt = new pTexture();
            pt.InternalWidth = width;
            pt.InternalHeight = height;

            try
            {
                if (GameBase.D3D)
                    pt.TextureXna = new Texture2D(GameBase.graphics.GraphicsDevice, pt.Width, pt.Height, 1,
                                                  ResourceUsage.None, SurfaceFormat.Color,
                                                  canRecreate ? ResourceManagementMode.Manual : ResourceManagementMode.Automatic);
                if (GameBase.OGL)
                    pt.TextureGl = new TextureGl(pt.Width, pt.Height);
                pt.SetData(bitmap);
            }
            catch
            {
            }

            return pt;
        }
    }

    /// <summary>
    /// BinaryWriter exposing protected MS function.
    /// </summary>
    internal class HaxBinaryReader : BinaryReader
    {
        public HaxBinaryReader(Stream input)
            : base(input)
        {
        }

        public HaxBinaryReader(Stream input, Encoding encoding)
            : base(input, encoding)
        {
        }

        public new int Read7BitEncodedInt()
        {
            byte num3;
            int num = 0;
            int num2 = 0;
            do
            {
                if (num2 == 0x23)
                {
                    return -1;
                }
                num3 = ReadByte();
                num |= (num3 & 0x7f) << num2;
                num2 += 7;
            } while ((num3 & 0x80) != 0);
            return num;
        }
    }
}