﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using osu.Helpers;

namespace osu.Graphics.Skinning
{
    internal class SkinMania : Skin
    {
        internal const string ManiaKeyPrefix = @"mania-key";
        internal const string ManiaNotePrefix = @"mania-note";
        internal const string ManiaSpecial = @"S";
        internal const string ManiaNormal1 = @"1";
        internal const string ManiaNormal2 = @"2";

        internal List<float> ColumnLineWidth = new List<float>();
        internal List<float> ColumnSpacing = new List<float>();
        internal List<float> ColumnWidth = new List<float>();

        internal ManiaSpecialStyle SpecialStyle = ManiaSpecialStyle.None;

        internal float BarlineHeight = 1.2f;

        internal bool JudgementLine = true;
        internal bool UpsideDown = false;
        internal bool SeparateScore = true;
        internal bool KeysUnderNotes = false;

        private bool splitStages = false;
        internal bool? SplitStagesFromSkin = null;
        internal bool SplitStages
        {
            get { return SplitStagesFromSkin ?? splitStages; }
            set { splitStages = value; }
        }

        internal float ColumnStart = 136;
        internal float ColumnRight = 19;
        internal int HitPosition = 402;
        internal int LightPosition = 413;
        internal int ComboPosition = 111;
        internal int ScorePosition = 325;

        internal float StageSeparation = 40;

        private int columns = 10;
        internal int Columns
        {
            get { return columns; }
            set
            {
                columns = value;

                ColumnLineWidth.Clear();
                ColumnSpacing.Clear();
                ColumnWidth.Clear();

                for (int i = 0; i <= columns; i++)
                {
                    ColumnLineWidth.Add(2);
                    if (i < columns)
                        ColumnWidth.Add(30);
                    if (i < columns - 1)
                        ColumnSpacing.Add(0);
                }
            }
        }
        internal int LightFramePerSecond = 60;

        internal Dictionary<string, Color> Colours = new Dictionary<string, Color>(10);
        internal Dictionary<string, string> ImageMap = new Dictionary<string, string>(10);

        internal SkinMania()
        {
            AddSection(@"Mania");
        }

        private string repeat(string contents, int length, string delimiter = @"")
        {
            string ret = "";
            for (int i = 0; i < length; i++)
                ret += contents + (i < length - 1 ? delimiter : @"");
            return ret;
        }

        #region Skin Reading/Writing

        internal void LoadSkin(bool migrate)
        {
            string temp = null;
            string[] split;
            Dictionary<string, string> matches;

            SkinSection section = SkinSections[@"Mania"];

            if (migrate)
            {
                //Old mania skins had a constant ColumnLineWidth value
                //and a bitfield determining visibility of column lines
                float w = 0;
                if (section.ReadValue(@"ColumnLineWidth", ref w))
                {
                    if (w > 0 && w < 2)
                        w = 2;
                    for (int i = 0; i <= Columns; i++)
                    {
                        if (i >= ColumnLineWidth.Count) break;
                        ColumnLineWidth[i] = (float)Convert.ToDouble(w, GameBase.nfi);
                    }
                }

                if (section.ReadValue(@"ColumnLine", ref temp))
                {
                    split = temp.Split(',');
                    for (int i = 0; i < split.Length; i++)
                    {
                        if (i >= ColumnLineWidth.Count) break;
                        ColumnLineWidth[i] = split[i] == @"0" ? 0 : ColumnLineWidth[i];
                    }
                }

                section.ReadValue(@"BarlineWidth", ref BarlineHeight);

                int specStyle = 0;
                if (section.ReadValue(@"SpecialStyle", ref specStyle))
                {
                    int pos = 0;
                    section.ReadValue(@"SpecialPositionLeft", ref pos);

                    if (specStyle == 1 && pos == 1)
                        SpecialStyle = ManiaSpecialStyle.Left;
                    else if (specStyle == 1)
                        SpecialStyle = ManiaSpecialStyle.Right;
                    else
                        SpecialStyle = ManiaSpecialStyle.None;
                }

                section.ReadValue(@"FontCombo", ref SkinManager.Current.FontCombo);

                section.RemoveValue(@"FontCombo");
                section.RemoveValue(@"ColumnLineWidth");
                section.RemoveValue(@"ColumnLine");
                section.RemoveValue(@"BarlineWidth");
                section.RemoveValue(@"SpecialPositionLeft");
            }
            else
            {
                if (section.ReadValue(@"Keys", ref temp))
                    Columns = Convert.ToInt32(temp);

                if (section.ReadValue(@"ColumnLineWidth", ref temp))
                {
                    split = temp.Split(',');
                    for (int i = 0; i < split.Length; i++)
                    {
                        if (i >= ColumnLineWidth.Count) break;
                        float w;
                        if (float.TryParse(split[i], out w))
                        {
                            if (w > 0 && w < 2)
                                w = 2;
                            ColumnLineWidth[i] = w;
                        }
                    }
                }

                section.ReadValue(@"BarlineHeight", ref BarlineHeight);
                section.ReadValue(@"SpecialStyle", ref SpecialStyle);
            }

            if (section.ReadValue("ColumnWidth", ref temp))
            {
                split = temp.Split(',');
                for (int i = 0; i < split.Length; i++)
                {
                    if (i >= ColumnWidth.Count) break;
                    int w;
                    if (int.TryParse(split[i], out w))
                        ColumnWidth[i] = OsuMathHelper.Clamp(w, 5, 100);
                }
            }

            matches = section.FindAll(key => (key.StartsWith(@"KeyImage") || key.StartsWith(@"NoteImage") || key.StartsWith(@"Stage") || key.StartsWith(@"Hit") || key.StartsWith(@"Lighting") || key.StartsWith(@"WarningArrow")) && !ImageMap.ContainsKey(key));
            foreach (KeyValuePair<string, string> kvp in matches)
                ImageMap.Add(kvp.Key, kvp.Value);

            matches = section.FindAll(key => key.StartsWith(@"Colour") && !Colours.ContainsKey(key));
            foreach (KeyValuePair<string, string> kvp in matches)
            {
                Color tempColor = new Color();
                section.ReadValue(kvp.Key, ref tempColor);
                Colours.Add(kvp.Key, tempColor);
            }

            section.ReadValue(@"StageSeparation", ref StageSeparation);
            StageSeparation = Math.Max(StageSeparation, 5);
            section.ReadValue(@"SeparateScore", ref SeparateScore);
            section.ReadValue(@"SplitStages", ref SplitStagesFromSkin);
            section.ReadValue(@"KeysUnderNotes", ref KeysUnderNotes);
            section.ReadValue(@"ColumnStart", ref ColumnStart);
            section.ReadValue(@"ColumnRight", ref ColumnRight);
            section.ReadValue(@"JudgementLine", ref JudgementLine);
            section.ReadValue(@"HitPosition", ref HitPosition);
            HitPosition = Math.Max(240, Math.Min(HitPosition, 480));
            section.ReadValue(@"LightPosition", ref LightPosition);
            section.ReadValue(@"ComboPosition", ref ComboPosition);
            section.ReadValue(@"ScorePosition", ref ScorePosition);
            section.ReadValue(@"UpsideDown", ref UpsideDown);
            section.ReadValue(@"LightFramePerSecond", ref LightFramePerSecond);
            if (LightFramePerSecond <= 0)
                LightFramePerSecond = 24;
            if (section.ReadValue(@"ColumnSpacing", ref temp))
            {
                split = temp.Split(',');
                for (int i = 0; i < split.Length; i++)
                {
                    int w;
                    if (int.TryParse(split[i], out w))
                        ColumnSpacing[i] = w;
                }
            }
        }

        internal override void LoadSkin()
        {
            LoadSkin(false);
        }

        internal virtual void WriteSkin(StreamWriter stream)
        {
            SkinSection section = SkinSections[@"Mania"];
            section.WriteValue(@"Keys",                 Columns.ToString(), null, true);
            section.WriteValue(@"ColumnWidth",          string.Join(@",", ColumnWidth.ConvertAll(c => c.ToString(GameBase.nfi)).ToArray()), repeat(@"30", ColumnWidth.Count, @","));
            section.WriteValue(@"ColumnLineWidth",      string.Join(@",", ColumnLineWidth.ConvertAll(c => c.ToString(GameBase.nfi)).ToArray()), repeat(@"2", ColumnLineWidth.Count, @","));
            section.WriteValue(@"SpecialStyle",         @"SpecialStyle");
            section.WriteValue(@"ColumnStart",          @"ColumnStart");
            section.WriteValue(@"ColumnRight",          @"ColumnRight");
            section.WriteValue(@"JudgementLine",        @"JudgementLine");
            section.WriteValue(@"BarlineHeight",        @"BarlineHeight");
            section.WriteValue(@"HitPosition",          @"HitPosition");
            section.WriteValue(@"LightPosition",        @"LightPosition");
            section.WriteValue(@"ComboPosition",        @"ComboPosition");
            section.WriteValue(@"ScorePosition",        @"ScorePosition");
            section.WriteValue(@"UpsideDown",           @"UpsideDown");
            section.WriteValue(@"LightFramePerSecond",  @"LightFramePerSecond");
            section.WriteValue(@"SeparateScore",        @"SeparateScore");
            section.WriteValue(@"KeysUnderNotes",       @"KeysUnderNotes");
            section.WriteValue(@"SplitStages",          @"splitStagesFromSkin");
            section.WriteValue(@"StageSeparation",      @"StageSeparation");

            foreach (KeyValuePair<string, string> kvp in ImageMap)
                section.WriteValue(kvp.Key, kvp.Value, null);

            foreach (KeyValuePair<string, Color> kvp in Colours)
                section.WriteValue(kvp.Key, (Color?)kvp.Value, null);

            base.WriteSkin(stream);
        }

        #endregion

        #region Skin Reading Helpers

        internal Color GetColour(string colourName, Color defaultColour)
        {
            Color tempColour;
            if (Colours.TryGetValue(@"Colour" + colourName, out tempColour))
                return tempColour;
            return defaultColour;
        }

        internal pTexture Load(string imageMapName, string defaultName, SkinSource skinSource)
        {
            string tempName;
            pTexture p;
            if (ImageMap.TryGetValue(imageMapName, out tempName) && (p = SkinManager.Load(tempName, skinSource)) != null)
                return p;
            return SkinManager.Load(defaultName, skinSource);
        }

        internal pTexture[] LoadAll(string imageMapName, string defaultName, SkinSource skinSource)
        {
            string tempName;
            pTexture[] p;
            if (ImageMap.TryGetValue(imageMapName, out tempName) && (p = SkinManager.LoadAll(tempName, skinSource)) != null)
                return p;
            return SkinManager.LoadAll(defaultName, skinSource);
        }

        #endregion

        internal SkinMania Clone()
        {
            SkinMania ret = (SkinMania)MemberwiseClone();
            ret.ImageMap = new Dictionary<string, string>(ImageMap);
            ret.Colours = new Dictionary<string, Color>(Colours);
            ret.ColumnLineWidth = new List<float>(ColumnLineWidth);
            ret.ColumnWidth = new List<float>(ColumnWidth);
            ret.ColumnSpacing = new List<float>(ColumnSpacing);
            return ret;
        }
    }

    internal enum ManiaSpecialStyle
    {
        None = 0,
        Left = 1,
        Right = 2
    }
}
