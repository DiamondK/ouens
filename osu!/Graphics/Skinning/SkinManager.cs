using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Notifications;
using osu.Graphics.Sprites;
using osu.Input;
using osu_common;
using osu.Graphics.Primitives;
using osu.Helpers;
using osu.GameplayElements;
using osu.Graphics.Renderers;
using osu_common.Libraries.Osz2;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements.HitObjects.Osu;
using System.Drawing.Text;
using System.Drawing;
using Color = Microsoft.Xna.Framework.Graphics.Color;
using osu_common.Helpers;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace osu.Graphics.Skinning
{
    internal static class SkinManager
    {
        private static readonly Dictionary<string, int> BeatmapCacheReferences = new Dictionary<string, int>();

        private static Dictionary<string, pTexture[]> AnimationCache = new Dictionary<string, pTexture[]>();

        private static Dictionary<SkinSource, Dictionary<string, pTexture>> LookupCache = new Dictionary<SkinSource, Dictionary<string, pTexture>>();

        private static Dictionary<string, pTexture> BeatmapSpriteCache = new Dictionary<string, pTexture>();
        private static Dictionary<string, pTexture> DefaultSpriteCache = new Dictionary<string, pTexture>();
        private static Dictionary<string, pTexture> SkinSpriteCache = new Dictionary<string, pTexture>();

        internal const string SKINS_FOLDER = @"Skins";
        internal const string USER_SKIN = @"User";

        internal static int GetCacheCount(SkinSource source)
        {
            switch (source)
            {
                case SkinSource.Beatmap:
                    return BeatmapSpriteCache.Count;
                case SkinSource.Skin:
                    return SkinSpriteCache.Count;
                case SkinSource.Osu:
                    return DefaultSpriteCache.Count;
            }

            return 0;
        }

        internal static event VoidDelegate OnSkinChanged;

        internal static Dictionary<string, Color> BeatmapColours = new Dictionary<string, Color>();
        internal static Dictionary<string, Color> Colours = new Dictionary<string, Color>();

        internal static SkinOsu CurrentCursor { get { return ConfigManager.sUseSkinCursor ? CurrentUserSkin : Current; } }

        internal static SkinOsu Current;
        internal static SkinOsu CurrentUserSkin;
        internal static SkinFruits CurrentFruitsSkin { get { return Current.FruitsSkin; } }

        internal static Color[] DefaultColours = new[]
                                                     {
                                                         new Color(255, 192, 0),
                                                         new Color(0, 202, 0),
                                                         new Color(18, 124, 255),
                                                         new Color(242, 24, 57),
                                                         new Color(0, 0, 0, 0),
                                                         new Color(0, 0, 0, 0),
                                                         new Color(0, 0, 0, 0),
                                                         new Color(0, 0, 0, 0)
                                                     };
        internal const int MAX_COLOUR_COUNT = 8;

        internal static List<string> Skins;

        private static bool unloaded;
        private static List<pTexture> Unloadables = new List<pTexture>();

        internal static ISliderRenderer SliderRenderer;

        internal static SliderStyle SliderStyle
        {
            get
            {
                return ConfigManager.sForceSliderRendering ? CurrentUserSkin.SliderStyle : Current.SliderStyle;
            }
        }

        private static void CreateSliderRenderer()
        {
            if (GameBase.OGL)
            {
                if (SliderRenderer is MmSliderRendererGL) return;
                if (SliderRenderer != null) SliderRenderer.Dispose();
                MmSliderRendererGL glRenderer = new MmSliderRendererGL();
                glRenderer.Initialize();
                SliderRenderer = glRenderer;
                return;
            }

            switch (SliderStyle)
            {
                case SliderStyle.PeppySliders:
                    if (SliderRenderer is PeppySliderRenderer) return;
                    if (SliderRenderer != null) SliderRenderer.Dispose();
                    PeppySliderRenderer pprRenderer = new PeppySliderRenderer();
                    pprRenderer.Init(GameBase.graphics.GraphicsDevice, GameBase.content);
                    SliderRenderer = pprRenderer;
                    break;
                case SliderStyle.MmSliders:
                default:
                    if (SliderRenderer is MmSliderRendererD3D) return;
                    if (SliderRenderer != null) SliderRenderer.Dispose();
                    MmSliderRendererD3D mmRenderer = new MmSliderRendererD3D();
                    mmRenderer.Initialize(GameBase.graphics.GraphicsDevice, GameBase.content);
                    SliderRenderer = mmRenderer;
                    break;
            }
        }

        internal static void RefreshAvailableSkins()
        {
            Skins = new List<string>();

            string[] skins = Directory.GetDirectories(SKINS_FOLDER);

            foreach (string st in skins)
                Skins.Add(st.Replace(SKINS_FOLDER + Path.DirectorySeparatorChar, string.Empty));

            Skins.Sort();

            Skins.Insert(0, @"Default");
        }

        internal static void Reference(string filename)
        {
            if (BeatmapCacheReferences.ContainsKey(filename))
                BeatmapCacheReferences[filename]++;
            else
                BeatmapCacheReferences[filename] = 1;
        }

        /// <summary>
        /// When set to true, references will not be dereferenced.
        /// </summary>
        internal static bool BeatmapCacheHoldReferences = false;

        internal static void Dereference(string filename)
        {
            if (BeatmapCacheHoldReferences) return;

            int referenceCount = 0;

            if (BeatmapCacheReferences.TryGetValue(filename, out referenceCount))
            {
                if (referenceCount > 1)
                { //Still other references being held.
                    BeatmapCacheReferences[filename]--;
                    return;
                }

            }
            else
                return;
            //Doesn't exist in beatmapCache (is this right?)

            BeatmapCacheReferences.Remove(filename);

            if (!BeatmapSpriteCache.ContainsKey(filename))
                return;

            pTexture tex = BeatmapSpriteCache[filename];

            if (tex != null)
            {
                tex.Dispose();
                BeatmapSpriteCache.Remove(filename);
                LookupCache.Clear();
            }
        }

        internal static bool LoadSkin(string skin = null, bool clearCache = false)
        {
            InitializeSkinList();

            bool found = true;

            if (skin == null || Skins.Find(s => s.Equals(skin, StringComparison.CurrentCultureIgnoreCase)) == null)
            {
                found = skin == null;
                skin = ConfigManager.sSkin;
            }

            bool forceDefault = false;
            if ((GameBase.Mode == OsuModes.Edit || GameBase.TestMode) && ConfigManager.sEditorDefaultSkin)
            {
                skin = @"Default";
                forceDefault = true;
            }

            bool isNewSkin = false;

            if (clearCache || Current == null || !skin.Equals(Current.RawName, StringComparison.CurrentCultureIgnoreCase))
            {
                if (Current != null) Debug.Print(@"Skin reloaded");

                ClearSkinCache(true);

                SkinOsu newSkin = LoadSkinRaw(skin);

                if (newSkin != null)
                {
                    isNewSkin = true;
                    Current = newSkin;

                    if (skin == ConfigManager.sSkin)
                        CurrentUserSkin = Current;

                    loadOldManiaSkins();

                    AudioEngine.ClearSkinCache();
                    //AudioEngine.LoadSampleSet(AudioEngine.CurrentSampleSet);

                    if (GameBase.Mode == OsuModes.Play || GameBase.Mode == OsuModes.SkinSelect) //todo: check if we actually need to load samples here.
                        AudioEngine.LoadAllSamples();
                    else
                        AudioEngine.LoadPrioritySamples();
                }
            }

            BeatmapColours.Clear();

            ResetColoursToSkin();

            if (ConfigManager.sUseSkinCursor && Current != CurrentUserSkin && !forceDefault)
            {
                //Special user override.
                SkinOsu tempHolder = Current;

                Current = CurrentUserSkin;
                LoadCursor();
                Current = tempHolder;
            }
            else
            {
                LoadCursor();
            }

            if (isNewSkin)
                if (OnSkinChanged != null) OnSkinChanged();

            return found;
        }

        private static HitObjectManager _hitObjectManager;
        internal static void ComputeColours(HitObjectManager hitObjectManager)
        {
            _hitObjectManager = hitObjectManager;
            ComputeColours();
        }

        internal static void ComputeColours()
        {
            if (Current == null || _hitObjectManager == null) return;

            List<Color> colours = new List<Color>();

            if (LoadColour(@"SliderTrackOverride").A > 0.0f)
                colours.Add(LoadColour(@"SliderTrackOverride"));
            else if (BeatmapManager.Current != null && BeatmapManager.Current.PlayMode != PlayModes.Taiko)
                colours.AddRange(_hitObjectManager.ComboColours);
            else
                colours.Add(new Color(252, 184, 6)); //taiko Yellow

            GameBase.Scheduler.Add(delegate
            {
                if (Current == null || _hitObjectManager == null) return;

                // should be readonly so reusing this copy is okay
                CreateSliderRenderer();
                if (SliderRenderer != null)
                    SliderRenderer.AssignColours(colours, SliderRenderer.Tag, SliderRenderer.Grey, LoadColour(@"SliderBorder"), _hitObjectManager.HitObjectRadius * GameBase.GamefieldRatio);
            });
        }

        internal static void LoadCursor(bool force = true)
        {
            if (!force && InputManager.s_Cursor != null && InputManager.s_Cursor.Texture != null && !InputManager.s_Cursor.Texture.IsDisposed)
                return;

            if (Current == null) return;

            pTexture t_cursor = Load(@"cursor", ConfigManager.sUseSkinCursor ? SkinSource.Skin | SkinSource.Osu : SkinSource.All);
            t_cursortrail = Load(@"cursortrail", ConfigManager.sUseSkinCursor ? SkinSource.Skin | SkinSource.Osu : SkinSource.All);

            if (t_cursor == null || t_cursortrail == null) return;

            InputManager.CursorExpand = Current.CursorExpand;

            pSprite cursor = InputManager.s_Cursor;
            pSprite cursor2 = InputManager.s_Cursor2;

            if (cursor == null)
            {
                cursor = InputManager.s_Cursor = new pSprite(null, Fields.NativeStandardScale, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0.999F, true, Color.White) { Alpha = 0 };
                GameBase.spriteManagerCursor.Add(cursor);

                cursor2 = InputManager.s_Cursor2 = new pSprite(null, Fields.NativeStandardScale, Origins.TopLeft, Clocks.Game, Vector2.Zero, 1, true, Color.White) { Alpha = 0 };
                GameBase.spriteManagerCursor.Add(cursor2);

            }

            bool alreadyRotating = false;
            for (int i = 0; i < cursor.Transformations.Count; i++)
            {
                if (cursor.Transformations[i].Type == TransformationType.Rotation)
                {
                    alreadyRotating = true;
                    //Optimize a bit if later we will put add
                    //more transformations to the cursor
                    break;
                }
            }

            if (!Current.CursorRotate)
            {
                //Stop the rotation and reset to original position
                cursor.Transformations.Clear();
                cursor.Rotation = 0;
            }
            else if (!alreadyRotating)
            {
                //Begin the rotation
                cursor.Transformations.Add(new Transformation(TransformationType.Rotation, 0, (float)Math.PI * 2, 0, 10000) { Loop = true });
            }

            cursor.Origin = cursor2.Origin = Current.CursorCentre ? Origins.Centre : Origins.TopLeft;
            cursor.Scale = 1;

            cursor.Texture = t_cursor;
            cursor2.Texture = SkinManager.Load(@"cursormiddle", t_cursor.Source);

            InputManager.UpdateCursorSize();
        }

        internal static void ClearBeatmapCache(bool force = false)
        {
            SkinManager.BeatmapCacheHoldReferences = force;

            if (BeatmapSpriteCache.Count == 0) return;

            //Avoid inter-thread access problems.
            pTexture[] cache = new pTexture[BeatmapSpriteCache.Count];
            BeatmapSpriteCache.Values.CopyTo(cache, 0);

            foreach (pTexture t in cache)
            {
                if (t == null) continue;

                if (t.Disposable)
                    //textures marked as disposable will be cleaned up themselves, so we don't need to forcefully dispose.
                    //this case is used in TransitionManager to allow extended lifetime on background sprites.
                    continue;

                t.Dispose();
            }

            BeatmapSpriteCache.Clear();

            AnimationCache.Clear();

            BeatmapCacheReferences.Clear();

            SkinManager.BeatmapCacheHoldReferences = false;

            LookupCache.Clear();
        }

        internal static void ClearDefaultCache(bool force = false)
        {
            List<pTexture> keepTextures = new List<pTexture>();

            foreach (pTexture t in DefaultSpriteCache.Values)
            {
                if (t != null)
                {
                    if (t.Disposable || force)
                        t.Dispose();
                    else
                        keepTextures.Add(t);
                }
            }

            DefaultSpriteCache.Clear();
            if (keepTextures.Count > 0)
            {
                foreach (pTexture t in keepTextures)
                    DefaultSpriteCache.Add(t.assetName, t);
            }

            AnimationCache.Clear();

            LookupCache.Clear();
        }

        internal static void ClearSkinCache(bool force = false)
        {
            List<pTexture> keepTextures = new List<pTexture>();

            foreach (pTexture t in SkinSpriteCache.Values)
            {
                if (t != null)
                {
                    if (t.Disposable || force)
                        t.Dispose();
                    else
                        keepTextures.Add(t);
                }
            }

            SkinSpriteCache.Clear();
            if (keepTextures.Count > 0)
            {
                foreach (pTexture t in keepTextures)
                    SkinSpriteCache.Add(t.assetName, t);
            }

            AnimationCache.Clear();

            LookupCache.Clear();
        }

        internal static Font GetTaikoMetadataFont(float size, FontStyle style)
        {
            string filename = Path.Combine(Path.Combine(SKINS_FOLDER, Current.RawName), @"taiko.ttf");

            if (IsDefault || !File.Exists(filename)) return null;

            using (PrivateFontCollection fontCollection = new PrivateFontCollection())
            {
                fontCollection.AddFontFile(filename);
                FontFamily tFont = fontCollection.Families[0];
                return new Font(tFont, 19, style);
            }
        }

        internal static SkinOsu LoadSkinRaw(string skinName)
        {
            SkinOsu skin = new SkinOsu();
            skin.RawName = skinName;

            string iniFilename = Path.Combine(Path.Combine(SKINS_FOLDER, skinName), @"skin.ini");
            bool hasIni = File.Exists(iniFilename);
            bool isDefault = skinName == @"Default";

            if (isDefault || skinName == USER_SKIN || !hasIni)
                skin.Version = SkinOsu.SKIN_VERSION;

            try
            {
                if (isDefault)
                {
                    skin.SkinName = @" osu!";
                    skin.SkinAuthor = @"peppy";
                }
                else if (hasIni)
                {
                    using (StreamReader sr = File.OpenText(iniFilename))
                    {
                        string section = @"General";
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine().Trim();

                            if (line.StartsWith(@"["))
                            {
                                section = line.Trim('[', ']');
                                if (section == @"Mania")
                                    skin.ManiaSkins.Add(new SkinMania());
                                continue;
                            }
                            skin.AddLine(section, line);
                        }
                    }
                }
                skin.LoadSkin();
            }
            catch (Exception)
            {
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.SkinManager_ErrorLoadingConfig), 5000);
                if (!isDefault)
                    return LoadSkinRaw(@"Default");
            }

            return skin;
        }

        /// <summary>
        /// Migrates all nK.ini configs into the user's skin.ini.
        /// </summary>
        [Obsolete(@"Support for loading mania skins from nK.ini only exists for migration to new system.")]
        private static void loadOldManiaSkins()
        {
            if (IsDefault)
                return;

            try
            {
                string path = Path.Combine(SKINS_FOLDER, Current.RawName);
                string[] files = Directory.GetFiles(path, @"*K.ini");
                bool newSkinAdded = false;

                for (int i = 0; i < files.Length; i++)
                {
                    SkinMania maniaSkin = new SkinMania();
                    string fileName = files[i].Substring(files[i].LastIndexOf(Path.DirectorySeparatorChar) + 1);
                    maniaSkin.Columns = Convert.ToInt32(fileName.Substring(0, fileName.IndexOf(@"K", StringComparison.CurrentCultureIgnoreCase)));

                    if (Current.ManiaSkins.Find(s => s.Columns == maniaSkin.Columns) != null)
                        continue;

                    using (FileStream fs = new FileStream(files[i], FileMode.Open))
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        while (sr.Peek() != -1)
                        {
                            string line = sr.ReadLine();
                            maniaSkin.AddLine(@"Mania", line);
                        }
                    }

                    maniaSkin.LoadSkin(true);
                    Current.ManiaSkins.Add(maniaSkin);
                    newSkinAdded = true;
                }

                if (newSkinAdded)
                {
                    //nK.ini skins may be broken by skin versions later than 2.4
                    Current.Version = 2.4;
                    SaveSkin();
                }
            }
            catch
            {
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.SkinManager_ErrorLoadingConfig), 5000);
            }
        }

        internal static void SaveSkin()
        {
            if (IsDefault)
                CreateUserSkin();

            using (FileStream fs = new FileStream(Path.Combine(Path.Combine(SKINS_FOLDER, Current.RawName), @"skin.ini"), FileMode.Create))
            using (StreamWriter sw = new StreamWriter(fs))
                Current.WriteSkin(sw);
        }

        internal static SkinMania LoadManiaSkin(int column, Mods mods = Mods.None)
        {
            SkinMania ret = Current.ManiaSkins.Find(s => s.Columns == column);

            if (ret == null)
            {
                ret = new SkinMania()
                {
                    Columns = column
                };
                Current.ManiaSkins.Add(ret);
            }

            if (column <= 1)
            {
                // disallow split stages for 1K
                ret.SplitStages = false;
                ret.SplitStagesFromSkin = null;
            }
            else
                ret.SplitStages = column >= 10 || (mods & Mods.KeyCoop) > 0;

            return ret;
        }

        internal static void ResetColoursToSkin()
        {
            Colours.Clear();

            foreach (KeyValuePair<string, Color> kvp in Current.Colours)
                Colours[kvp.Key] = kvp.Value;
        }

        internal static Color LoadColour(string name, bool allowBeatmapOverride = true)
        {
            Color ret;
            if (allowBeatmapOverride && BeatmapColours.TryGetValue(name, out ret))
            {
                if (ret.A > 0)
                    return ret;
                return Color.TransparentWhite;
            }

            if (Colours.TryGetValue(name, out ret) && ret.A > 0)
                return ret;

            return Color.TransparentWhite;
        }

        internal static bool IsDefault { get { return Current != null && Current.RawName == @"Default"; } }
        internal static bool UseNewLayout
        {
            get
            {
                return GameBase.NewGraphicsAvailable &&
                (
                    IsDefault ||
                    (Current != null && Current.RawName == USER_SKIN) ||
                    (Current == null) ||
                    (Current != null && (Current.Version > 1))
                );
            }
        }

        internal static pTexture t_cursortrail;
        public static bool IgnoreBeatmapSkin;

        internal static pTexture Load(string name, SkinSource source = SkinSource.All)
        {
            if (name == null) return null;

            if (IgnoreBeatmapSkin && source != SkinSource.Beatmap)
                source &= ~SkinSource.Beatmap;

            if (source == SkinSource.All)
            {
                switch (GameBase.Mode)
                {
                    case OsuModes.Play:
                    case OsuModes.Edit:
                        break;
                    default:
                        //As a default, don't load from the beatmap unless we are in play or edit mode.
                        source &= ~SkinSource.Beatmap;
                        break;
                }
            }

            pTexture tex = null;

            Dictionary<string, pTexture> dic = null;
            if (!LookupCache.TryGetValue(source, out dic))
                dic = LookupCache[source] = new Dictionary<string, pTexture>();

            if (dic.TryGetValue(name, out tex))
                return tex;

            try
            {
                //Check for beatmap-specific sprite availability
                Beatmap current = null;
                if ((source & SkinSource.Beatmap) > 0 && (current = BeatmapManager.Current) != null)
                {
                    if (BeatmapSpriteCache.TryGetValue(name, out tex))
                    {
                        //Cached sprite is available.
                        //Note that we must check null *inside* this if block because a cached null means this source has no available file.
                        //If we find a null, we don't want to enter the else block and check for a file every time, so we quietly ignore this source here.
                        if (tex != null)
                            return tex;
                    }
                    else
                    {
                        using (Stream stream = current.GetFileStream(name.IndexOf('.') < 0 ? name + @".png" : name))
                        {
                            if (stream != null)
                            {
                                tex = pTexture.FromStream(stream, name);
                                if (tex != null)
                                    tex.Source = SkinSource.Beatmap;
                                displayOnce(tex);

                                BeatmapSpriteCache[name] = tex;
                                return tex;
                            }
                        }

                        BeatmapSpriteCache[name] = null;
                    }
                }

                if ((source & SkinSource.Skin) > 0 && !IsDefault)
                {
                    if (SkinSpriteCache.TryGetValue(name, out tex))
                    {
                        //Cached sprite is available.
                        //Note that we must check null *inside* this if block because a cached null means this source has no available file.
                        //If we find a null, we don't want to enter the else block and check for a file every time, so we quietly ignore this source here.
                        if (tex != null)
                            return tex;
                    }
                    else
                    {
                        //todo: replace with Path.Combine after switching to .net4+
                        string filename = Path.Combine(Path.Combine(SKINS_FOLDER, Current.RawName), name.IndexOf('.') < 0 ? name + @".png" : name);

                        string filename2x = filename.Insert(filename.LastIndexOf('.'), @"@2x");
                        if (GameBase.UseHighResolutionSprites && File.Exists(filename2x))
                        {
                            tex = pTexture.FromFile(filename2x);
                            tex.DpiScale = 2;
                            SkinSpriteCache[name] = tex;
                            if (tex != null) tex.Source = SkinSource.Skin;
                            displayOnce(tex);
                            return tex;
                        }

                        if (File.Exists(filename))
                        {
                            tex = pTexture.FromFile(filename);
                            SkinSpriteCache[name] = tex;
                            if (tex != null) tex.Source = SkinSource.Skin;
                            displayOnce(tex);
                            return tex;
                        }

                        SkinSpriteCache[name] = null;
                    }
                }

                if ((source & SkinSource.Osu) == 0)
                    return null;

                try
                {
                    if (DefaultSpriteCache.TryGetValue(name, out tex))
                        return tex;

#if LOAD_LOCAL
                    if ((tex = Load(name, SkinSource.Skin)) != null)
                        return tex;
#endif

                    name = GeneralHelper.PathSanitise(name);
                    tex = name.IndexOf(Path.DirectorySeparatorChar) > 0 ? pTexture.FromFile(name) : pTexture.FromResourceStore(name);
                    displayOnce(tex);

                    DefaultSpriteCache[name] = tex;
                    if (tex != null) tex.Source = SkinSource.Osu;

                    return tex;
                }
                catch
                {
                    return null;
                }
            }
            finally
            {
                dic[name] = tex;
            }
        }

        static SpriteManager loadingManager = new SpriteManager() { Masking = false };
        static bool loadingManagerPending;

        public static Color NEW_SKIN_COLOUR_MAIN = new Color(236, 117, 140);
        public static Color NEW_SKIN_COLOUR_MAIN_DARKER = new Color(187, 17, 119);
        public static Color NEW_SKIN_COLOUR_SECONDARY = new Color(34, 153, 187);
        private static void displayOnce(pTexture tex)
        {
            pSprite temp = new pSprite(tex, Fields.NativeStandardScale, Origins.TopCentre, Clocks.Game, new Vector2(-9999, -9999)) { AlwaysDraw = true };
            loadingManager.Add(temp);

            if (!loadingManagerPending)
            {
                GameBase.Scheduler.Add(delegate
                {
                    loadingManager.Draw();
                    loadingManager.Clear();
                    loadingManagerPending = false;
                }, true);
                loadingManagerPending = true;
            }
        }

        internal static pTexture[] LoadAll(string s, SkinSource source = SkinSource.All, bool dashSeparator = true)
        {
            pTexture[] texArray;
            if (AnimationCache.TryGetValue(s, out texArray) && texArray[0].Source == source)
                return texArray;

            string dash = dashSeparator ? @"-" : string.Empty;

            pTexture animated = Load(s + dash + 0, source);
            pTexture sprite = Load(s, source);

            if (animated != null && animated == textureFromMostSpecificSkin(animated, sprite))
            {
                List<pTexture> textures = new List<pTexture>();

                for (int i = 1; animated != null; i++)
                {
                    textures.Add(animated);
                    animated = Load(s + dash + i, animated.Source);
                }

                texArray = textures.ToArray();
                AnimationCache[s] = texArray;
                return texArray;
            }

            if (sprite != null)
            {
                texArray = new[] { sprite };
                AnimationCache[s] = texArray;
                return texArray;
            }

            return null;
        }

        private static pTexture textureFromMostSpecificSkin(pTexture first, pTexture second)
        {
            if (first != null && (
                second == null ||
                first.Source == SkinSource.Beatmap ||
                first.Source == SkinSource.Skin && second.Source != SkinSource.Beatmap ||
                first.Source == SkinSource.Osu && second.Source != SkinSource.Beatmap && second.Source != SkinSource.Skin)
            ) return first;
            return second;
        }

        internal static pTexture LoadFirst(string s, SkinSource source = SkinSource.All, bool dashSeparator = true)
        {
            return textureFromMostSpecificSkin(Load(s, source), LoadAll(s, source, dashSeparator)[0]);
        }

        internal static void InitializeSkinList()
        {
            if (Skins == null)
                RefreshAvailableSkins();
        }

        internal static void Unload()
        {
            if (!GameBase.D3D) return; //not required

            Unload(DefaultSpriteCache);
            Unload(BeatmapSpriteCache);
            Unload(SkinSpriteCache);

            //These textures we really can't do much about at the moment - they are being dynamically loaded from somewhere.
            Unloadables.ForEach(t =>
            {
                if (!t.IsDisposed && t.TextureXna == null)
                    return;
                t.Dispose();
            });

            Unloadables.Clear();


            //content.Unload();
            unloaded = true;
        }

        private static void Unload(Dictionary<string, pTexture> cache)
        {
            foreach (KeyValuePair<string, pTexture> s in cache)
                if (s.Value != null && s.Value.TextureXna != null)
                {
                    s.Value.TextureXna.Dispose();
                    s.Value.TextureXna = null;
                }
        }

        internal static void Reload(bool force = false)
        {
            //Reload is not required for OGL
            if ((!unloaded || !GameBase.D3D) && !force) return;
            unloaded = false;

            Debug.Print(@"Reloaded all textures (SkinManager)");

            AnimationCache.Clear();
            LookupCache.Clear();

            ReloadCache(DefaultSpriteCache, force);
            ReloadCache(BeatmapSpriteCache, force);
            ReloadCache(SkinSpriteCache, force);
            DynamicSpriteCache.Reload();

        }

        internal static void ReloadCache(Dictionary<string, pTexture> cache, bool force = false)
        {
            //Reload is not required for OGL
            if (!GameBase.D3D && !force) return;

            Dictionary<string, pTexture> oldCache = new Dictionary<string, pTexture>(cache);
            cache.Clear();

            foreach (KeyValuePair<string, pTexture> s in oldCache)
            {
                if (s.Value == null)
                    cache[s.Key] = null;
                else
                {
                    pTexture newlyLoaded = Load(s.Key, s.Value.Source);

                    if (newlyLoaded != null)
                    {
                        GC.SuppressFinalize(newlyLoaded);
                        s.Value.TextureXna = newlyLoaded.TextureXna;
                        s.Value.TextureGl = newlyLoaded.TextureGl;
                        s.Value.DpiScale = newlyLoaded.DpiScale;
                        s.Value.InternalHeight = newlyLoaded.InternalHeight;
                        s.Value.InternalWidth = newlyLoaded.InternalWidth;

                        s.Value.isDisposed = false;
                    }
                    else
                    {
                        s.Value.TextureXna = null;
                        s.Value.TextureGl = null;
                    }

                    cache[s.Key] = s.Value;
                    LookupCache[s.Value.Source][s.Key] = s.Value;
                }
            }
        }

        internal static void Cleanup()
        {
            DynamicSpriteCache.CheckForUnused();
        }

        public static void RegisterUnrecoverableTexture(pTexture texture)
        {
            Unloadables.Add(texture);
        }

        internal static void CreateUserSkin()
        {
            if (!IsDefault) return;

            if (!Directory.Exists(Path.Combine(SKINS_FOLDER, USER_SKIN)))
                Directory.CreateDirectory(Path.Combine(SKINS_FOLDER, USER_SKIN));

            ConfigManager.sSkin.Value = USER_SKIN;
            LoadSkin(ConfigManager.sSkin, true);
        }

        internal static Color RandomDefaultColour()
        {
            return DefaultColours[GameBase.random.Next(0, 4)];
        }

        internal static void ClearAllCache()
        {
            ClearSkinCache(true);
            ClearDefaultCache(true);
            LoadCursor(true);
        }
    }

    [Flags]
    public enum SkinSource
    {
        None = 0,
        Osu = 1,
        Skin = 2,
        Beatmap = 4,
        Temporal = 8,
        ExceptBeatmap = Osu | Skin,
        All = Osu | Skin | Beatmap
    }
}
