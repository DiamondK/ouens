﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace osu.Graphics.Skinning
{
    internal class SkinOsu : Skin
    {
        internal Dictionary<string, Color> Colours = new Dictionary<string, Color>();
        private Dictionary<string, Color> originalColours = new Dictionary<string, Color>()
        {
            { @"Combo1",                 SkinManager.DefaultColours[0] },
            { @"Combo2",                 SkinManager.DefaultColours[1] },
            { @"Combo3",                 SkinManager.DefaultColours[2] },
            { @"Combo4",                 SkinManager.DefaultColours[3] },
            { @"Combo5",                 SkinManager.DefaultColours[4] },
            { @"MenuGlow",               new Color(0, 78, 155) },
            { @"SliderBall",             new Color(2, 170, 255) },
            { @"SliderBorder",           new Color(255, 255, 255) },
            { @"SpinnerApproachCircle",  new Color(77, 139, 217) },
            { @"SongSelectActiveText",   Color.Black },
            { @"SongSelectInactiveText", Color.White },
            { @"StarBreakAdditive",      Color.LightPink },
        };

        internal bool CursorCentre = true;
        internal bool CursorExpand = true;
        internal bool CursorRotate = true;

        internal string SkinAuthor = string.Empty;
        internal string SkinName = @"Unknown";

        internal bool SliderBallFlip = false;
        internal int SliderBallFrames = 10;
        internal string RawName;
        internal string FontHitCircle = @"default";
        internal string FontScore = @"score";
        internal int FontScoreOverlap = 0;
        internal string FontCombo = @"score";
        internal int FontComboOverlap = 0;

        internal int FontHitCircleOverlap = -2;
        internal bool OverlayAboveNumber = true;
        internal bool SpinnerFrequencyModulate = true;
        internal bool LayeredHitSounds = true;
        internal bool SpinnerFadePlayfield = !SkinManager.UseNewLayout;
        internal bool SpinnerNoBlink = false;
        internal int AnimationFramerate = -1;
        internal bool CursorTrailRotate = false;
        internal List<int> comboSoundBursts = new List<int>();
        internal SliderStyle SliderStyle = SliderStyle.MmSliders;
        internal bool AllowSliderBallTint = false;
        internal bool ComboBurstRandom = false;

        private bool isLatestVersion = false;
        internal double Version = 1;

        internal List<SkinMania> ManiaSkins = new List<SkinMania>();
        internal SkinFruits FruitsSkin = new SkinFruits();

        internal SkinOsu()
        {
            AddSection(@"General");
            AddSection(@"Colours");
            AddSection(@"Fonts");
            SkinSections[@"Colours"].AllowTransparentColours = false;
        }

        #region Skin Reading/Writing

        internal override void AddLine(string section, string s)
        {
            switch (section)
            {
                case @"Mania":
                    ManiaSkins[ManiaSkins.Count - 1].AddLine(section, s);
                    break;
                case @"CatchTheBeat":
                    FruitsSkin.AddLine(section, s);
                    break;
                default:
                    base.AddLine(section, s);
                    break;
            }
        }

        internal override void LoadSkin()
        {
            //Begin general header
            SkinSection section = SkinSections[@"General"];
            section.ReadValue(@"Name", ref SkinName);
            section.ReadValue(@"Author", ref SkinAuthor);
            section.ReadValue(@"SliderBallFlip", ref SliderBallFlip);
            section.ReadValue(@"SliderBallDontRotate", ref SliderBallFlip);
            section.ReadValue(@"CursorRotate", ref CursorRotate);
            section.ReadValue(@"CursorExpand", ref CursorExpand);
            section.ReadValue(@"CursorCentre", ref CursorCentre);
            section.ReadValue(@"SliderBallFrames", ref SliderBallFrames);
            section.ReadValue(@"HitCircleOverlayAboveNumber", ref OverlayAboveNumber);
            section.ReadValue(@"HitCircleOverlayAboveNumer", ref OverlayAboveNumber);
            section.ReadValue(@"SpinnerFrequencyModulate", ref SpinnerFrequencyModulate);
            section.ReadValue(@"LayeredHitSounds", ref LayeredHitSounds);
            section.ReadValue(@"SpinnerFadePlayfield", ref SpinnerFadePlayfield);
            section.ReadValue(@"SpinnerNoBlink", ref SpinnerNoBlink);
            section.ReadValue(@"AllowSliderBallTint", ref AllowSliderBallTint);
            section.ReadValue(@"AnimationFramerate", ref AnimationFramerate);
            section.ReadValue(@"CursorTrailRotate", ref CursorTrailRotate);

            string temp = null;
            if (section.ReadValue(@"CustomComboBurstSounds", ref temp))
                foreach (string st in temp.Split(','))
                    comboSoundBursts.Add(Int32.Parse(st));
            section.ReadValue(@"ComboBurstRandom", ref ComboBurstRandom);
            section.ReadValue(@"SliderStyle", ref SliderStyle);
            if (section.ReadValue(@"Version", ref temp))
            {
                isLatestVersion = temp == @"latest";
                Version = isLatestVersion ? SKIN_VERSION : Convert.ToDouble(temp, GameBase.nfi);
            }

            //Begin colours header
            section = SkinSections[@"Colours"];
            Dictionary<string, Color> match = section.FindAll<Color>();
            foreach (KeyValuePair<string, Color> kvp in match)
                Colours[kvp.Key] = kvp.Value;

            //Add default colours for any missing values.
            bool skinHasComboColours = Colours.ContainsKey(@"Combo1");
            foreach (KeyValuePair<string, Color> kvp in originalColours)
            {
                if (Colours.ContainsKey(kvp.Key)) continue;
                if (skinHasComboColours && kvp.Key.StartsWith(@"Combo")) continue;

                Colours[kvp.Key] = kvp.Value;
            }

            //Begin fonts header
            section = SkinSections[@"Fonts"];
            section.ReadValue(@"HitCirclePrefix", ref FontHitCircle);
            section.ReadValue(@"HitCircleOverlap", ref FontHitCircleOverlap);
            section.ReadValue(@"ScorePrefix", ref FontScore);
            section.ReadValue(@"ComboPrefix", ref FontCombo);
            section.ReadValue(@"ScoreOverlap", ref FontScoreOverlap);
            section.ReadValue(@"ComboOverlap", ref FontComboOverlap);

            FruitsSkin.LoadSkin();

            for (int i = 0; i < ManiaSkins.Count; i++)
                ManiaSkins[i].LoadSkin();
        }

        internal virtual void WriteSkin(StreamWriter stream)
        {
            //Begin general header
            SkinSection section = SkinSections[@"General"];
            section.WriteValue(@"Name",                         @"SkinName");
            section.WriteValue(@"Author",                       @"SkinAuthor");
            section.WriteValue(@"SliderBallFlip",               @"SliderBallFlip");
            section.WriteValue(@"CursorRotate",                 @"CursorRotate");
            section.WriteValue(@"CursorExpand",                 @"CursorExpand");
            section.WriteValue(@"CursorCentre",                 @"CursorCentre");
            section.WriteValue(@"SliderBallFrames",             @"SliderBallFrames");
            section.WriteValue(@"HitCircleOverlayAboveNumber",  @"OverlayAboveNumber");
            section.WriteValue(@"SpinnerFrequencyModulate",     @"SpinnerFrequencyModulate");
            section.WriteValue(@"LayeredHitSounds",             @"LayeredHitSounds");
            section.WriteValue(@"SpinnerFadePlayfield",         @"SpinnerFadePlayfield");
            section.WriteValue(@"SpinnerNoBlink",               @"SpinnerNoBlink");
            section.WriteValue(@"AllowSliderBallTint",          @"AllowSliderBallTint");
            section.WriteValue(@"AnimationFramerate",           @"AnimationFramerate");
            section.WriteValue(@"CursorTrailRotate",            @"CursorTrailRotate");
            section.WriteValue(@"CustomComboBurstSounds",       string.Join(@",", comboSoundBursts.ConvertAll(c => c.ToString(GameBase.nfi)).ToArray()), @"");
            section.WriteValue(@"ComboBurstRandom",             @"ComboBurstRandom");
            section.WriteValue(@"SliderStyle",                  @"SliderStyle");
            if (isLatestVersion)
                section.WriteValue(@"Version", @"latest", @"latest");
            else
                section.WriteValue(@"Version", @"Version");

            //Begin colours header
            section = SkinSections[@"Colours"];
            foreach (KeyValuePair<string, Color> kvp in Colours)
            {
                Color defaultColour;
                if (originalColours.TryGetValue(kvp.Key, out defaultColour))
                    SkinSections[@"Colours"].WriteValue(kvp.Key, Colours[kvp.Key], defaultColour);
                else
                    SkinSections[@"Colours"].WriteValue(kvp.Key, (Color?)Colours[kvp.Key], null);
            }

            //Begin fonts header
            section = SkinSections[@"Fonts"];
            section.WriteValue(@"HitCirclePrefix",  @"FontHitCircle");
            section.WriteValue(@"HitCircleOverlap", @"FontHitCircleOverlap");
            section.WriteValue(@"ScorePrefix",      @"FontScore");
            section.WriteValue(@"ComboPrefix",      @"FontCombo");
            section.WriteValue(@"ScoreOverlap",     @"FontScoreOverlap");
            section.WriteValue(@"ComboOverlap",     @"FontComboOverlap");

            base.WriteSkin(stream);

            FruitsSkin.WriteSkin(stream);

            for (int i = 0; i < ManiaSkins.Count; i++)
                ManiaSkins[i].WriteSkin(stream);
        }

        #endregion
    }

    internal enum SliderStyle
    {
        PeppySliders = 1, // peppy's opaque, segmented style
        MmSliders = 2, // MM's smooth transparent style
        ToonSliders = 3, // MM's style with steps instead of a gradient (cel shading like?)
        AwfulGlSliders = 4 // Old GL compatability rendering (should never be needed?)
    }
}
