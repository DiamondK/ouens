﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.InteropServices;
using System.Reflection;

namespace osu.Graphics.Skinning
{
    internal abstract class Skin
    {
        //2.1 introduced new taiko element positions.
        //2.2 reorganised information on beatmap panels.
        //2.3 add new ctb catcher, ctb specific comboburst
        //2.4 mania stage and scaling rework
        //2.5 mania UpsideDown note flipping
        internal const double SKIN_VERSION = 2.5;

        protected Dictionary<string, SkinSection> SkinSections = new Dictionary<string, SkinSection>();

        internal void AddSection(string name)
        {
            SkinSections.Add(name, new SkinSection(this));
        }

        /// <summary>
        /// Extracts the skin properties into
        /// the respective fields.
        /// </summary>
        internal abstract void LoadSkin();

        /// <summary>
        /// Adds a line to the specified skin section.
        /// </summary>
        internal virtual void AddLine(string section, string s)
        {
            if (s.StartsWith(@"["))
                return;
            SkinSections[section].AddLine(s);
        }

        /// <summary>
        /// Writes all skin sections to the stream.
        /// </summary>
        internal virtual void WriteSkin(StreamWriter stream)
        {
            foreach (KeyValuePair<string, SkinSection> kvp in SkinSections)
            {
                if (kvp.Value.IsEmpty)
                    continue;
                stream.WriteLine('[' + kvp.Key + ']');
                foreach (string line in kvp.Value)
                    stream.WriteLine(line);
            }
        }
    }

    class SkinSection : IEnumerable<string>
    {
        private Skin host;

        //Default caches for later use
        private Skin defaultSkin;
        private List<PropertyInfo> defaultProperties;
        private List<FieldInfo> defaultFields;

        private Dictionary<string, string> properties = new Dictionary<string, string>();
        private List<SkinLine> lines = new List<SkinLine>();

        internal bool IsEmpty { get { return lines.Count == 0; } }
        internal bool AllowTransparentColours = true;

        internal SkinSection(Skin host)
        {
            this.host = host;
        }

        /// <summary>
        /// If the line is a skinning property, it is added
        /// as a key + value pair in properties. Lines are also added
        /// into lines for enumeration at write-time.
        /// </summary>
        internal void AddLine(string line)
        {
            SkinLine newLine = new SkinLine(line);
            lines.Add(newLine);
            if (!properties.ContainsKey(newLine.Key) && !line.Contains(@"//") && !string.IsNullOrEmpty(line))
                properties.Add(newLine.Key, newLine.Value);
        }

        /// <summary>
        /// Reads a property from the skin into the destination object
        /// converted to the destination type.
        /// </summary>
        /// <param name="key">The property key.</param>
        /// <param name="dest">The destination object.</param>
        /// <param name="destType">The destination type.</param>
        /// <returns>True if succeeded, otherwise false. Casted property value
        /// is written to dest if succeeded, default value otherwise.</returns>
        private bool tryGetValue(string key, out object dest, Type destType)
        {
            Type underlyingType = Nullable.GetUnderlyingType(destType) ?? destType;

            string value;
            if (!properties.TryGetValue(key, out value) || (string.IsNullOrEmpty(value) && underlyingType != typeof(string) && underlyingType == destType))
            {
                dest = getDefaultValue(destType);
                return false;
            }

            if (underlyingType == typeof(Color))
            {
                string[] split = value.Split(',');
                switch (split.Length)
                {
                    case 3:
                        dest = Convert.ChangeType(new Color((byte)Convert.ToInt32(split[0]), (byte)Convert.ToInt32(split[1]), (byte)Convert.ToInt32(split[2])), underlyingType, GameBase.nfi);
                        return true;
                    case 4:
                        dest = Convert.ChangeType(new Color((byte)Convert.ToInt32(split[0]), (byte)Convert.ToInt32(split[1]),
                                                               (byte)Convert.ToInt32(split[2]), (byte)(AllowTransparentColours ? Convert.ToInt32(split[3]) : 255)), underlyingType, GameBase.nfi);
                        return true;
                    default:
                        dest = getDefaultValue(destType);
                        return false;
                }
            }
            else if (underlyingType == typeof(bool))
            {
                bool boolTemp;
                if (bool.TryParse(value, out boolTemp))
                    dest = Convert.ChangeType(boolTemp, underlyingType, GameBase.nfi);
                else
                    dest = Convert.ChangeType(Convert.ToInt32(value), underlyingType, GameBase.nfi);
            }
            else if (underlyingType.IsEnum)
                dest = Enum.Parse(underlyingType, value, true);
            else
                dest = Convert.ChangeType(value, underlyingType, GameBase.nfi);

            return true;
        }

        /// <summary>
        /// Reads a property from the skin into the destination object.
        /// </summary>
        /// <typeparam name="T">The destination type.</typeparam>
        /// <param name="key">The property key.</param>
        /// <param name="dest">The destination object.</param>
        /// <returns>True if succeeded, otherwise false. Property value is written to dest if succeeded.</returns>
        internal bool ReadValue<T>(string key, ref T dest)
        {
            object ret;
            if (tryGetValue(key, out ret, typeof(T)))
            {
                dest = (T)ret;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Removes a property from the skin.
        /// </summary>
        /// <param name="key">The property key to remove.</param>
        internal void RemoveValue(string key)
        {
            if (properties.ContainsKey(key))
            {
                properties.Remove(key);
                lines.Remove(lines.Find(s => s.Key == key));
            }
        }

        /// <summary>
        /// Returns all the properties for which the key matches the predicate.
        /// </summary>
        /// <param name="match">The predicate to match the keys against.</param>
        /// <returns>A list of key-value pairs.</returns>
        internal Dictionary<string, string> FindAll(Predicate<string> match)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> prop in properties)
            {
                if (match(prop.Key))
                    ret[prop.Key] = prop.Value;
            }
            return ret;
        }

        /// <summary>
        /// Returns all the properties which can be casted to the specified type.
        /// </summary>
        /// <typeparam name="T">The cast type.</typeparam>
        /// <returns></returns>
        internal Dictionary<string, T> FindAll<T>()
        {
            Dictionary<string, T> ret = new Dictionary<string, T>();
            foreach (KeyValuePair<string, string> prop in properties)
            {
                T temp = default(T);
                if (ReadValue(prop.Key, ref temp))
                    ret.Add(prop.Key, temp);
            }

            return ret;
        }

        private string getString(object value, Type valueType)
        {
            if (value == null)
                return null;

            Type underlyingType = Nullable.GetUnderlyingType(valueType) ?? valueType;
            if (underlyingType == typeof(Color))
            {
                Color? c = value as Color?;
                string colourFormat = "{0},{1},{2}" + (AllowTransparentColours ? @",{3}" : @"");
                return string.Format(colourFormat, c.Value.R, c.Value.G, c.Value.B, c.Value.A);
            }
            if (underlyingType == typeof(bool) || underlyingType.IsEnum)
                return Convert.ToInt32(value).ToString(GameBase.nfi);
            if (underlyingType == typeof(double) || underlyingType == typeof(float))
                return Convert.ToDouble(value).ToString(GameBase.nfi);
            return value.ToString();
        }

        private void modifyValue(string key, object value, Type valueType)
        {
            foreach (SkinLine line in lines)
            {
                if (line.Key == key)
                    line.Value = getString(value, valueType);
            }
            properties[key] = getString(value, valueType);
        }

        /// <summary>
        /// Modifies the existing property if it exists, otherwise
        /// adds the property to the section lines.
        /// </summary>
        /// <param name="key">The property key.</param>
        /// <param name="value">The object to write.</param>
        /// <param name="defaultValue">The default value for the object.</param>
        /// <param name="valueType">The type of the property value.</param>
        /// <param name="topMost">If the property should appear as the first item under the section header.</param>
        private void writeValue(string key, object value, object defaultValue, Type valueType, bool topMost = false)
        {
            object oldValue;
            if (tryGetValue(key, out oldValue, valueType))
            {
                if (getString(oldValue, valueType) != getString(value, valueType))
                    modifyValue(key, value, valueType);
            }
            else if ((defaultValue == null && value != null) || getString(value, valueType) != getString(defaultValue, valueType))
            {
                if (topMost)
                    lines.Insert(0, new SkinLine(key, getString(value, valueType)));
                else
                    lines.Add(new SkinLine(key, getString(value, valueType)));
                properties.Add(key, getString(value, valueType));
            }
        }

        /// <summary>
        /// Modifies the existing property if it exists, otherwise
        /// adds the property to the section lines.
        /// <para>
        /// If you need to use a value type with a null default value,
        /// either make the value into a nullable type or convert to string.
        /// </para>
        /// </summary>
        /// <param name="key">The property key.</param>
        /// <param name="value">The object to write.</param>
        /// <param name="defaultValue">The default value for the object.</param>
        /// <param name="topMost">If the property should appear as the first item under the section header.</param>
        internal void WriteValue<T>(string key, T value, T defaultValue, bool topMost = false)
        {
            writeValue(key, value, defaultValue, typeof(T), topMost);
        }

        /// <summary>
        /// Automatically resolves the current value and the default value
        /// and writes using WriteValue. Useful when writing simple properties.
        /// </summary>
        /// <param name="key">The property key.</param>
        /// <param name="property">The literal property name in the class.</param>
        internal void WriteValue(string key, string property, bool topMost = false)
        {
            //To save on reinitializing all this, we cache it
            if (defaultSkin == null || host.GetType() != defaultSkin.GetType())
            {
                defaultSkin = (Skin)Activator.CreateInstance(host.GetType(), true);
                defaultProperties = new List<PropertyInfo>(host.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance));
                defaultFields = new List<FieldInfo>(host.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance));
            }

            PropertyInfo pi = defaultProperties.Find(p => p.Name == property);
            FieldInfo fi = defaultFields.Find(f => f.Name == property);
            if (pi != null) writeValue(key, pi.GetValue(host, null), pi.GetValue(defaultSkin, null), pi.PropertyType, topMost);
            if (fi != null) writeValue(key, fi.GetValue(host), fi.GetValue(defaultSkin), fi.FieldType, topMost);
        }

        private object getDefaultValue(Type type)
        {
            if (type.IsValueType)
                return Activator.CreateInstance(type, true);
            return null;
        }

        public IEnumerator<string> GetEnumerator()
        {
            foreach (SkinLine line in lines)
                yield return line.ToString();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    internal class SkinLine
    {
        internal string Key;
        internal string Value;

        internal SkinLine(string line)
        {
            Key = line;
            if (line.Contains(@":"))
            {
                string[] var = line.Trim().Split(':');
                if (var.Length > 1)
                {
                    Key = var[0].Trim();
                    Value = var[1].Trim();
                }
            }
        }

        internal SkinLine(string key, string value)
        {
            this.Key = key;
            this.Value = value;
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(Value))
                return Key;
            return string.Format(@"{0}: {1}", Key, Value);
        }
    }
}
