﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace osu.Graphics.Skinning
{
    internal class SkinFruits : Skin
    {
        internal Color ColourHyperDash = Color.Red;

        private Color? colourHyperDashAfterImage = null;
        internal Color ColourHyperDashAfterImage
        {
            get { return colourHyperDashAfterImage ?? ColourHyperDash; }
            set { colourHyperDashAfterImage = value; }
        }

        private Color? colourHyperDashFruit = null;
        internal Color ColourHyperDashFruit
        {
            get { return colourHyperDashFruit ?? ColourHyperDash; }
            set { colourHyperDashFruit = value; }
        }

        internal SkinFruits()
        {
            AddSection(@"CatchTheBeat");
        }

        internal override void LoadSkin()
        {
            SkinSections[@"CatchTheBeat"].ReadValue(@"HyperDash", ref ColourHyperDash);
            SkinSections[@"CatchTheBeat"].ReadValue(@"HyperDashAfterImage", ref colourHyperDashAfterImage);
            SkinSections[@"CatchTheBeat"].ReadValue(@"HyperDashFruit", ref colourHyperDashFruit);
        }

        internal override void WriteSkin(System.IO.StreamWriter stream)
        {
            SkinSections[@"CatchTheBeat"].WriteValue(@"HyperDash", @"ColourHyperDash");
            SkinSections[@"CatchTheBeat"].WriteValue(@"HyperDashAfterImage", ColourHyperDashAfterImage, ColourHyperDash);
            SkinSections[@"CatchTheBeat"].WriteValue(@"HyperDashFruit", ColourHyperDashFruit, ColourHyperDash);

            base.WriteSkin(stream);
        }
    }
}
