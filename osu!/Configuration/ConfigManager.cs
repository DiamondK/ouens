using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Components;
using osu.GameplayElements.HitObjects.Osu;
using osu.Input;
using osu.Online;
using osu_common.Helpers;
using osu.Graphics.UserInterface;
using osu_common;
using osu.GameModes.Select;
using osu.Helpers;
using osu.GameModes.Options;
using osu.GameModes.Other;
using osu.Graphics.Renderers;
using osu.Input.Handlers;
using osu.Graphics.Notifications;
using osu.GameplayElements.Beatmaps;
using osu_common.Updater;
using osu.GameModes.Play.Rulesets.Mania;
using osu.Online.Social;

namespace osu.Configuration
{
    internal static class ConfigManager
    {
        internal readonly static Dictionary<string, object> Configuration = new Dictionary<string, object>();

        internal static BindableBool sBlockNonFriendPM;
        private static Bindable<FrameSync> sFrameSyncD3d;
        private static Bindable<FrameSync> sFrameSyncGl;
        internal static Bindable<ProgressBarTypes> sProgressBarType;
        internal static Bindable<string> sLanguage;
        internal static BindableBool sAllowPublicInvites;
        internal static BindableBool sAutoChatHide;
        internal static BindableBool sAutomaticDownload;
        internal static BindableBool sBloom;
        internal static BindableBool sBloomSoftening;
        internal static BindableBool sBossKeyFirstActivation;
        internal static BindableBool sChatAudibleHighlight;
        internal static BindableBool sComboColourSliderBall;
        internal static BindableBool sChatFilter;
        internal static BindableBool sChatHighlightName;
        internal static BindableBool sChatRemoveForeign;
        internal static BindableBool sComboBurst;
        internal static BindableBool sComboFire;
        internal static BindableBool sConfirmExit;
        internal static BindableBool sDisplayCityLocation;
        internal static BindableBool sDistanceSpacingEnabled;
        internal static BindableBool sEditorVideo;
        internal static BindableBool sEditorDefaultSkin;
        internal static BindableBool sEditorSnakingSliders;
        internal static BindableBool sEditorHitAnimations;
        internal static BindableBool sEditorFollowPoints;
        internal static BindableBool sCursorRipple;
        internal static BindableBool sEloTouchscreen;
        internal static BindableBool sFastEditor;
        internal static BindableBool sForceSliderRendering;
        internal static BindableBool sFpsCounter;
        internal static BindableBool sHighResolution;
        internal static BindableBool sHitLighting;
        internal static BindableBool sIgnoreBarline;
        internal static BindableBool sIgnoreBeatmapSamples;
        internal static BindableBool sIgnoreBeatmapSkins;
        internal static BindableBool sJoystick;
        internal static BindableBool sKeyOverlay;
        internal static BindableBool sLoadSubmittedThread;
        internal static BindableBool sLobbyShowExistingOnly;
        internal static BindableBool sLobbyShowFriendsOnly;
        internal static BindableBool sLobbyShowFull;
        internal static BindableBool sLobbyShowInProgress;
        internal static BindableBool sLobbyShowPassworded;
        internal static BindableBool sLogPrivateMessages;
        internal static BindableBool sLowResolution;
        internal static BindableBool sMouseDisableButtons;
        internal static BindableBool sMouseDisableWheel;
        internal static BindableBool sMsnIntegration;
        internal static BindableBool sMyPcSucks;
        internal static BindableBool sNotifyFriends;
        internal static BindableBool sNotifySubmittedThread;
        internal static BindableBool sPermanentSongInfo;
        internal static BindableBool sPopupDuringGameplay;
        internal static BindableBool sSavePassword;
        internal static BindableBool sSaveUsername;
        internal static BindableBool sScoreboardVisible;
        internal static BindableBool sShowReplayComments;
        internal static BindableBool sShowSpectators;
        internal static BindableBool sShowStoryboard;
        internal static BindableBool sMenuParallax;
        internal static BindableBool sShowUnicode;
        internal static BindableBool sSkinSamples;
        internal static BindableBool sSkipTablet;
        internal static BindableBool sSnakingSliders;
        internal static BindableBool sTablet;
        internal static BindableBool sTicker;
        internal static BindableBool sUpdatePending;
        internal static BindableBool sUseSkinCursor;
        internal static BindableBool sUseTaikoSkin;
        internal static BindableBool sAutoSendNowPlaying;
        internal static BindableDouble sCursorSize;
        internal static BindableBool sAutomaticCursorSizing;
        internal static BindableBool sVideo;
        internal static BindableBool sWiimote;
        internal static BindableBool sYahooIntegration;
        internal static BindableDouble sComboFireHeight;
        internal static BindableDouble sDistanceSpacing;
        internal static BindableDouble sMouseSpeed;
        internal static BindableInt sCustomFrameLimit;
        internal static BindableInt sDimLevel;
        internal static BindableInt sEditorBeatDivisor;
        internal static BindableInt sEditorGridSize;
        internal static BindableInt sEditorTip;
        internal static BindableInt sHeight;
        internal static BindableInt sHeightFullscreen;
        internal static BindableInt sManiaSpeed;
        internal static BindableBool sUsePerBeatmapManiaSpeed;
        internal static BindableBool sManiaSpeedBPMScale;
        internal static BindableInt sMenuTip;
        internal static BindableInt sOffset;
        internal static BindableInt sRefreshRate;
        internal static BindableInt sWidth;
        internal static BindableInt sWidthFullscreen;
        internal static Bindable<ImageFileFormat> sScreenshotFormat;
        internal static BindableInt sDisplay;
        internal static BindableInt sLobbyPlayMode;
        internal static BindableInt sScreenshot;
        internal static BindableInt sTouchDragOffset;
        internal static Bindable<RankingType> sRankType;
        internal static Bindable<ScaleMode> sScaleMode;
        internal static Bindable<ScoreMeterType> sScoreMeter;
        internal static Bindable<ScreenMode> sScreenMode;
        internal static BindableBool sFullscreen;
        internal static BindableBool sShowInterface;
        internal static BindableBool sMenuSnow;
        internal static BindableBool sMenuTriangles;
        internal static BindableBool sSongSelectThumbnails;
        internal static Bindable<string> sChatChannels;
        internal static Bindable<string> sChatLastChannel;
        internal static Bindable<string> sGuideTips;
        internal static Bindable<string> sHelpTips;
        internal static Bindable<string> sAudioDevice;
        internal static Bindable<string> sHighlightWords;
        internal static Bindable<string> sIgnoreList;
        internal static Bindable<string> sLastVersion;
        internal static Bindable<string> sLastVersionPermissionsFailed;
        internal static Bindable<string> sPassword;
        internal static Bindable<string> sRenderer;
        internal static Bindable<string> sSkin;
        internal static string sTouchCalibration;
        internal static Bindable<string> sUsername;
        internal static Bindable<UserFilterType> sUserFilter;
        internal static Bindable<UserSortMode> sChatSortMode;
        internal static BindableBool sRawInput;
        internal static Bindable<TreeGroupMode> sCurrentGroupMode;
        internal static Bindable<TreeSortMode> sCurrentSortMode;

        internal static BindableBool OpenGL;
        internal static BindableBool DirectX;
        internal static BindableDouble sScoreMeterScale;
        internal static BindableBool sShowMenuTips;
        internal static BindableBool sAlternativeChatFont;
        public static Bindable<ReleaseStream> sReleaseStream;
        public static BindableBool sMenuMusic;
        public static BindableBool sMenuVoice;
        internal static BindableInt sUpdateFailCount;

        public static BindableBool sHiddenShowFirstApproach;

        internal static Bindable<FrameSync> sFrameSync;
        internal static BindableBool sAbsoluteToOsuWindow;

        internal static bool VSync
        {
            get
            {
                switch (sFrameSync.Value)
                {
                    case FrameSync.VSync:
                    case FrameSync.LowLatencyVSync:
                        return true;
                }

                return false;
            }
        }

        internal static string ConfigFilename
        {
            get { return @"osu!." + GeneralHelper.WindowsPathStrip(Environment.UserName) + @".cfg"; }
        }

        static ConfigManager()
        {
            Initialize();
        }

        public static void Initialize()
        {
            if (Configuration.Count > 0) return;

            ReadConfigFile(@"osu!.cfg");
            ReadConfigFile(ConfigFilename);

            GameBase.BeatmapDirectory = ReadString(@"BeatmapDirectory", @"Songs");
            GameBase.BeatmapDirectory.Value = GeneralHelper.PathSanitise(GameBase.BeatmapDirectory.Value);
            if (!Directory.Exists(GameBase.BeatmapDirectory.Value))
            {
                GameBase.BeatmapDirectory.Value = @"Songs";
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.ConfigManager_SongFolderNotFound));
            }

            AudioEngine.VolumeMaster = ReadInt(@"VolumeUniversal", 100, 0, 100);
            AudioEngine.VolumeEffect = ReadInt(@"VolumeEffect", 80, 0, 100);
            AudioEngine.VolumeMusic = ReadInt(@"VolumeMusic", 80, 0, 100);

            sAllowPublicInvites = ReadBool(@"AllowPublicInvites", true);
            sAutoChatHide = ReadBool(@"AutoChatHide", true);
            sAutomaticDownload = ReadBool(@"AutomaticDownload", true);
            sBlockNonFriendPM = ReadBool(@"BlockNonFriendPM", false);
            sBloom = ReadBool(@"Bloom", false);
            sBloomSoftening = ReadBool(@"BloomSoftening", false);
            sBossKeyFirstActivation = ReadBool(@"BossKeyFirstActivation", true);
            sChatAudibleHighlight = ReadBool(@"ChatAudibleHighlight", true);
            sChatChannels = ReadString(@"ChatChannels", string.Empty);
            sChatFilter = ReadBool(@"ChatFilter", false);
            sChatHighlightName = ReadBool(@"ChatHighlightName", true);
            sChatLastChannel = ReadString(@"ChatLastChannel", string.Empty);
            sChatRemoveForeign = ReadBool(@"ChatRemoveForeign", false);
            sChatSortMode = ReadValue<UserSortMode>(@"ChatSortMode", UserSortMode.Rank);
            sComboBurst = ReadBool(@"ComboBurst", true);
            sComboFire = ReadBool(@"ComboFire", false);
            sComboFireHeight = ReadInt(@"ComboFireHeight", 3);
            sConfirmExit = ReadBool(@"ConfirmExit", false);

            sAutoSendNowPlaying = ReadBool(@"AutoSendNowPlaying", true);

            sCursorSize = ReadDouble(@"CursorSize", 1.0, 0.5f, 2);

            sAutomaticCursorSizing = ReadBool(@"AutomaticCursorSizing", false);

            sDimLevel = ReadInt(@"DimLevel", 30, 0, 100);

            sDisplay = ReadInt(@"Display", 1);
            sDisplayCityLocation = ReadBool(@"DisplayCityLocation", false);

            sDistanceSpacingEnabled = ReadBool(@"DistanceSpacingEnabled", true);

            sEditorTip = ReadInt(@"EditorTip", 0);
            sEditorVideo = ReadBool(@"VideoEditor", sVideo);
            sEditorDefaultSkin = ReadBool(@"EditorDefaultSkin", false);
            sFastEditor = ReadBool(@"FastEditor", false);
            sEditorSnakingSliders = ReadBool(@"EditorSnakingSliders", true);
            sEditorHitAnimations = ReadBool(@"EditorHitAnimations", false);
            sEditorFollowPoints = ReadBool(@"EditorFollowPoints", true);
            sForceSliderRendering = ReadBool(@"ForceSliderRendering", false);
            sFpsCounter = ReadBool(@"FpsCounter", false);
            sFrameSyncD3d = ReadValue<FrameSync>(@"FrameSync", FrameSync.Limit120);
            sFrameSyncGl = ReadValue<FrameSync>(@"FrameSyncGl", FrameSync.VSync);
            sGuideTips = ReadString(@"GuideTips", @"");
            sCursorRipple = ReadBool(@"CursorRipple", false);
            sHighlightWords = ReadString(@"HighlightWords", string.Empty);
            sHighResolution = ReadBool(@"HighResolution", false);
            sHitLighting = ReadBool(@"HitLighting", true);
            sIgnoreBarline = ReadBool(@"IgnoreBarline", false);
            sIgnoreBeatmapSamples = ReadBool(@"IgnoreBeatmapSamples", false);
            sIgnoreBeatmapSkins = ReadBool(@"IgnoreBeatmapSkins", false);
            sIgnoreList = ReadString(@"IgnoreList", string.Empty);
            sJoystick = ReadBool(@"Joystick", false);
            sKeyOverlay = ReadBool(@"KeyOverlay", false);
            sLanguage = ReadString(@"Language", @"unknown");
            SongSelection.LastSelectMode = ReadValue<PlayModes>(@"LastPlayMode", 0);

            sLastVersion = ReadString(@"LastVersion", string.Empty);
            sLastVersionPermissionsFailed = ReadString(@"LastVersionPermissionsFailed", string.Empty);

            sLoadSubmittedThread = ReadBool(@"LoadSubmittedThread", true);
            sLobbyPlayMode = ReadInt(@"LobbyPlayMode", -1);
            sShowInterface = ReadBool(@"ShowInterface", true);
            sLobbyShowExistingOnly = ReadBool(@"LobbyShowExistingOnly", false);
            sLobbyShowFriendsOnly = ReadBool(@"LobbyShowFriendsOnly", false);
            sLobbyShowFull = ReadBool(@"LobbyShowFull", false);
            sLobbyShowInProgress = ReadBool(@"LobbyShowInProgress", true);
            sLobbyShowPassworded = ReadBool(@"LobbyShowPassworded", true);
            sLogPrivateMessages = ReadBool(@"LogPrivateMessages", false);
            sLowResolution = ReadBool(@"LowResolution", false);
            sManiaSpeed = ReadInt(@"ManiaSpeed", SpeedMania.SPEED_DEFAULT, SpeedMania.SPEED_MIN, SpeedMania.SPEED_MAX);
            sUsePerBeatmapManiaSpeed = ReadBool(@"UsePerBeatmapManiaSpeed", true);
            sManiaSpeedBPMScale = ReadBool(@"ManiaSpeedBPMScale", true);
            sMenuTip = ReadInt(@"MenuTip", 0);
            sMouseDisableButtons = ReadBool(@"MouseDisableButtons", false);
            sMouseDisableWheel = ReadBool(@"MouseDisableWheel", false);
            sMouseSpeed = ReadDouble(@"MouseSpeed", 1, 0.4, 6);
            sOffset = ReadInt(@"Offset", 0, -300, 300);
            sScoreMeterScale = ReadDouble(@"ScoreMeterScale", 1, 0.5, 2);
            sDistanceSpacing = ReadDouble(@"DistanceSpacing", 0.8, 0.5, 32);
            sEditorBeatDivisor = ReadInt(@"EditorBeatDivisor", 1, 1, 16);
            sEditorGridSize = ReadInt(@"EditorGridSize", 32, 4, 32);
            sHeight = ReadInt(@"Height", 9999, GameBase.Tournament ? 0 : 240, 9999);
            sWidth = ReadInt(@"Width", 9999, GameBase.Tournament ? 0 : 320, 9999);
            sHeightFullscreen = ReadInt(@"HeightFullscreen", 9999, 240, 9999);
            sCustomFrameLimit = ReadInt(@"CustomFrameLimit", 240, GameBase.Tournament ? 30 : 240, 1000);
            if (sCustomFrameLimit.Value == 60 || sCustomFrameLimit.Value == 120)
                //These options would be pointless as defaults exist for both, causing duplication on options menus.
                sCustomFrameLimit.Value = 240;
            sWidthFullscreen = ReadInt(@"WidthFullscreen", 9999, 320, 9999);
            sMsnIntegration = ReadBool(@"MsnIntegration", false);
            sMyPcSucks = ReadBool(@"MyPcSucks", false);
            sNotifyFriends = ReadBool(@"NotifyFriends", true);
            sNotifySubmittedThread = ReadBool(@"NotifySubmittedThread", true);
            sPopupDuringGameplay = ReadBool(@"PopupDuringGameplay", true);
            sProgressBarType = ReadValue<ProgressBarTypes>(@"ProgressBarType", ProgressBarTypes.Pie);
            sRankType = ReadValue<RankingType>(@"RankType", RankingType.Top);
            sRefreshRate = ReadInt(@"RefreshRate", 60);
            sRenderer = ReadString(@"Renderer", @"d3d");
            sScaleMode = ReadValue<ScaleMode>(@"ScaleMode", ScaleMode.WidescreenConservative);
            sScoreboardVisible = ReadBool(@"ScoreboardVisible", true);
            sScoreMeter = ReadValue<ScoreMeterType>(@"ScoreMeter", ScoreMeterType.Error);
            sScreenMode = ReadValue<ScreenMode>(@"Fullscreen", ScreenMode.BorderlessWindow);
            sScreenshot = ReadInt(@"ScreenshotId", 0);
            sMenuSnow = ReadBool(@"MenuSnow", false);
            sMenuTriangles = ReadBool(@"MenuTriangles", true);
            sSongSelectThumbnails = ReadBool(@"SongSelectThumbnails", true);
            sScreenshotFormat = ReadValue<ImageFileFormat>(@"ScreenshotFormat", ImageFileFormat.Jpg);
            sShowReplayComments = ReadBool(@"ShowReplayComments", true);
            sShowSpectators = ReadBool(@"ShowSpectators", true);
            sShowStoryboard = ReadBool(@"ShowStoryboard", true);
            sSkin = ReadString(@"Skin", @"Default");
            sSkinSamples = ReadBool(@"SkinSamples", true);
            sSkipTablet = ReadBool(@"SkipTablet", false);
            sSnakingSliders = ReadBool(@"SnakingSliders", true);
            sTablet = ReadBool(@"Tablet", false);
            sUpdatePending = ReadBool(@"UpdatePending", false);
            sUserFilter = ReadValue<UserFilterType>(@"UserFilter", 0);
            sUseSkinCursor = ReadBool(@"UseSkinCursor", false);
            sUseTaikoSkin = ReadBool(@"UseTaikoSkin", false);
            sVideo = ReadBool(@"Video", true);
            sWiimote = ReadBool(@"Wiimote", false);
            sYahooIntegration = ReadBool(@"YahooIntegration", false);

            sMenuMusic = ReadBool(@"MenuMusic", true);
            sMenuVoice = ReadBool(@"MenuVoice", true);

            sMenuParallax = ReadBool(@"MenuParallax", true);

            sRawInput = ReadBool(@"RawInput", false);
            sAbsoluteToOsuWindow = ReadBool(@"AbsoluteToOsuWindow", sRawInput.Value);
            sShowMenuTips = ReadBool(@"ShowMenuTips", true);

            sHiddenShowFirstApproach = ReadBool(@"HiddenShowFirstApproach", true);

            sComboColourSliderBall = ReadBool(@"ComboColourSliderBall", true);

            sAlternativeChatFont = ReadBool(@"AlternativeChatFont", false);
            sAlternativeChatFont.ValueChanged += delegate { ChatEngine.UpdateFontFace(); };

            sPassword = ReadString(@"Password", string.Empty);
            sUsername = ReadString(@"Username", string.Empty);

            sAudioDevice = ReadString(@"AudioDevice", string.Empty);
            sAudioDevice.ValueChanged += delegate
            {
                if (!AudioEngine.SetAudioDevice(sAudioDevice.Value))
                    sAudioDevice.Value = AudioEngine.CurrentAudioDevice;
            };

            sReleaseStream = ReadValue<ReleaseStream>(@"_ReleaseStream", ReleaseStream.Stable, true);

            sUpdateFailCount = ReadInt(@"_UpdateFailCount", 0);

            sReleaseStream.ValueChanged += delegate
            {
#if Release
                if (sReleaseStream.Value == ReleaseStream.CuttingEdge45 && !(FrameworkDetection.GetVersions().Contains(FrameworkVersion.dotnet45) || FrameworkDetection.GetVersions().Contains(FrameworkVersion.dotnet4)))
                {
                    NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Menu_NewFrameworkVersion), Color.Yellow, 300000, delegate { GameBase.ProcessStart(@"http://www.microsoft.com/en-us/download/details.aspx?id=42643"); });
                    sReleaseStream.Value = ReleaseStream.CuttingEdge;
                    return;
                }
#endif

                CommonUpdater.Reset();
                GameBase.UpdateState = UpdateStates.NoUpdate;
                if (GameBase.Time > 0)
                    GameBase.CheckForUpdates(false, true);
            };

            sAutoSendNowPlaying.ValueChanged += delegate
            {
                if (sAutoSendNowPlaying.Value)
                    sShowSpectators.Value = true;
            };
            sShowSpectators.ValueChanged += delegate
            {
                if (!sShowSpectators.Value)
                    sAutoSendNowPlaying.Value = false;
            };

            sCursorSize.ValueChanged += delegate { InputManager.UpdateCursorSize(); };

            sSavePassword = ReadBool(@"SavePassword", !string.IsNullOrEmpty(sPassword));
            sSavePassword.ValueChanged += delegate
            {
                if (sSavePassword)
                    Configuration[@"Password"] = sPassword;
                else
                    Configuration[@"Password"] = string.Empty;
            };

            sSaveUsername = ReadBool(@"SaveUsername", true);
            sSaveUsername.ValueChanged += delegate
            {
                if (sSaveUsername)
                    Configuration[@"Username"] = sUsername;
                else
                    Configuration[@"Username"] = string.Empty;
            };

            sManiaSpeedBPMScale.ValueChanged += delegate
            {
                if (sUsePerBeatmapManiaSpeed.Value)
                    BeatmapManager.Beatmaps.ForEach(b => { b.ManiaSpeed = 0; });
            };

            sUsePerBeatmapManiaSpeed.ValueChanged += delegate
            {
                if (!sUsePerBeatmapManiaSpeed.Value)
                    BeatmapManager.Beatmaps.ForEach(b => { b.ManiaSpeed = 0; });
            };

            if (!sSavePassword) Configuration[@"Password"] = string.Empty;
            if (!sSaveUsername) Configuration[@"Username"] = string.Empty;

            sCurrentGroupMode = ReadValue<TreeGroupMode>(@"TreeSortMode", TreeGroupMode.Show_All);
            sCurrentSortMode = ReadValue<TreeSortMode>(@"TreeSortMode2", TreeSortMode.Title);

            sHighlightWords.ValueChanged += delegate { ChatEngine.LoadHighlights(); };
            sIgnoreList.ValueChanged += delegate { ChatEngine.LoadIgnoreLists(); };

            InputManager.CheckWindows81Mouse();

            sLanguage.ValueChanged += delegate
            {
                bool wasExpanded = GameBase.Options == null ? false : GameBase.Options.Expanded;
                if (GameBase.Options != null) GameBase.Options.Expanded = false;

                GameBase.OnNextModeChange += delegate
                {
                    if (GameBase.Options != null)
                    {
                        GameBase.Options.ReloadElements();
                        GameBase.Options.Expanded = wasExpanded;
                    }
                };

                if (GameBase.Time > 0)
                {
                    LocalisationManager.SetLanguage(sLanguage.Value, ConfigManager.sLastVersion != General.BUILD_NAME, delegate
                    {
                        GameBase.Scheduler.Add(delegate
                        {
                            GameBase.ChangeMode(OsuModes.Menu, true);
                            ConfigManager.sLanguage.Value = LocalisationManager.CurrentLanguage;
                        });
                    }, osu.Properties.Resources.en);
                }
            };

            DirectX = new BindableBool();
            OpenGL = new BindableBool();
            sRenderer.ValueChanged += delegate { DirectX.Value = !(OpenGL.Value = sRenderer == @"opengl"); };
            DirectX.ValueChanged += delegate
            {
                if (!DirectX && !OpenGL)
                {
                    //cancel if trying to de-select.
                    DirectX.Value = true;
                    return;
                }

                sRenderer.Value = DirectX ? @"d3d" : @"opengl";
            };

            OpenGL.ValueChanged += delegate
            {
                if (!DirectX && !OpenGL)
                {
                    //cancel if trying to de-select.
                    OpenGL.Value = true;
                    return;
                }

                sRenderer.Value = !OpenGL ? @"d3d" : @"opengl";
            };
            sRenderer.TriggerChange();

            sRenderer.ValueChanged += delegate
            {
                if (GameBase.Time > 0) GameBase.Restart();
            };

            sRawInput.ValueChanged += delegate
            {
                if (!sRawInput)
                {
                    if (GameBase.IsWindows81)
                    {
                        sMouseSpeed.Value = 1;
                    }

                    return;
                }

                RawMouseHandler handler = (RawMouseHandler)InputManager.GetHandler(typeof(RawMouseHandler));

                if (handler == null)
                {
                    handler = new RawMouseHandler();
                    handler.SetPosition(MouseManager.MousePosition);

                    if (!InputManager.AddHandler(handler))
                    {
                        NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.Options_Input_RawInputFailed), 5000);
                        sRawInput.Value = false;
                    }
                }
            };

            sTablet.ValueChanged += delegate
            {
                TabletHandler handler = InputManager.GetHandler(typeof(TabletHandler)) as TabletHandler;

                if (sTablet.Value)
                {
                    if (handler == null)
                    {
                        handler = new TabletHandler();
                        if (!InputManager.AddHandler(handler))
                        {
                            sTablet.Value = false;
                            handler = null;
                        }
                    }
                }

                if (handler != null)
                {
                    handler.Enabled = sTablet.Value;
                }
            };

            sMouseSpeed.ValueChanged += delegate
            {
                InputManager.CheckWindows81Mouse();
            };

            sFpsCounter.ValueChanged += delegate
            {
                if (GameBase.spriteManagerOverlayHighest != null && GameBase.fpsDisplay != null)
                {
                    if (ConfigManager.sFpsCounter)
                    {

                        GameBase.spriteManagerOverlayHighest.Add(GameBase.fpsDisplay);
                        GameBase.fpsDisplay.Text = @"...";
                        GameBase.fpsElapsedTime = 0;
                        GameBase.frameCounter = 0;
                    }
                    else
                        GameBase.spriteManagerOverlayHighest.Remove(GameBase.fpsDisplay);
                }
            };

            sFullscreen = new BindableBool();
            sScreenMode.ValueChanged += delegate { sFullscreen.Value = sScreenMode.Value == ScreenMode.Fullscreen; };
            sScreenMode.TriggerChange();
            sFullscreen.ValueChanged += delegate
            {
                if (GameBase.Time > 0)
                    GameBase.SetScreenSize(null, null, GameBase.CurrentScreenMode == ScreenMode.Fullscreen ? ScreenMode.Windowed : ScreenMode.Fullscreen, true);
            };

            //bind sFrameSync to GL/D3d components.

            sFrameSyncD3d.ValueChanged += delegate
            {
                if (GameBase.graphics != null && GameBase.graphics.SynchronizeWithVerticalRetrace != ConfigManager.VSync)
                {
                    GameBase.graphics.SynchronizeWithVerticalRetrace = ConfigManager.VSync;
                    GameBase.graphics.ApplyChanges();
                }
            };

            sFrameSync = new Bindable<FrameSync>(OpenGL ? sFrameSyncGl : sFrameSyncD3d);
            sFrameSync.ValueChanged += delegate
            {
                if (OpenGL)
                {
                    if ((sFrameSync.Value == FrameSync.LowLatencyVSync || sFrameSync.Value == FrameSync.VSync) != (sFrameSyncGl.Value == FrameSync.LowLatencyVSync || sFrameSyncGl.Value == FrameSync.VSync))
                    {
                        FrameSync requestedValue = sFrameSync.Value;
                        sFrameSync.Value = sFrameSyncGl.Value;

                        if (GameBase.Mode == OsuModes.Menu && GameBase.Time > 0)
                            GameBase.Restart(delegate { sFrameSyncGl.Value = requestedValue; });
                        else
                            NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Options_Graphics_RestartRequired));
                        return;
                    }

                    if (sFrameSyncGl.Value == sFrameSync.Value) return;

                    sFrameSyncGl.Value = sFrameSync.Value;
                }
                else
                {
                    if (sFrameSyncD3d.Value == sFrameSync.Value) return;

                    sFrameSyncD3d.Value = sFrameSync.Value;
                }

                if (GameBase.Time > 0)
                {
                    GameBase.PendingModeChange = true;

                    string statusString = string.Empty;
                    switch (ConfigManager.sFrameSync.Value)
                    {
                        case FrameSync.VSync:
                            statusString = LocalisationManager.GetString(OsuString.Options_Graphics_FrameLimiter_60);
                            break;
                        case FrameSync.Limit120:
                            statusString = LocalisationManager.GetString(OsuString.Options_Graphics_FrameLimiter_120);
                            break;
                        case FrameSync.Limit240:
                            statusString = ConfigManager.sCustomFrameLimit + @"fps";
                            break;
                        case FrameSync.Unlimited:
                            statusString = LocalisationManager.GetString(OsuString.Options_Graphics_FrameLimiter_UnlimGreen);
                            break;
                        case FrameSync.CompletelyUnlimited:
                            statusString = LocalisationManager.GetString(OsuString.Options_Graphics_FrameLimiter_Unlim);
                            break;
                        case FrameSync.LowLatencyVSync:
                            statusString = LocalisationManager.GetString(OsuString.Options_Graphics_FrameLimiter_LowLatency);
                            break;
                    }

                    NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.Options_Graphics_FrameLimiter) + ' ' + statusString, 1000);
                }
            };

            sBloom.ValueChanged += delegate
            {
                if (sBloom && !BloomRenderer.Initialize())
                    sBloom.Value = false;

                if (!sBloom) sBloomSoftening.Value = false;
            };

            sBloomSoftening.ValueChanged += delegate
            {
                if (sBloomSoftening)
                {
                    if (!sBloom) sBloom.Value = true;

                    //initialisation may have failed.
                    if (!sBloom) sBloomSoftening.Value = false;
                }
            };

            bool unicodeDefault = false;
            switch (sLanguage)
            {
                case @"zh":
                case @"ja":
                case @"ko":
                    unicodeDefault = true;
                    break;
            }

            sShowUnicode = ReadBool(@"ShowUnicode", unicodeDefault);

#if ARCADE
            sPermanentSongInfo = true; //Song info will always display in arcade mode.
            sTicker = false; //Disable this for the time being.
#else
            sPermanentSongInfo = ReadBool(@"PermanentSongInfo", false);
            sTicker = ReadBool(@"Ticker", false);
#endif

            sTicker.ValueChanged += delegate { ChatEngine.UpdateButtonStates(); };
            sAutoChatHide.ValueChanged += delegate { ChatEngine.UpdateButtonStates(); };

#if TOUCHSCREEN
            sEloTouchscreen = CheckBool(@"EloTouchscreen", false);
            sTouchCalibration = CheckString(@"TouchCalibration", string.Empty);
            sTouchDragOffset = CheckInt(@"TouchDragOffset", 100);
#endif

#if CHRISTMAS
            sMenuMusic = true;
            sMenuSnow = true;
#endif

            if (sMyPcSucks)
            {
                sComboFire = false;
                sBloom = false;
                sBloomSoftening = false;
                sHitLighting = false;
                sComboBurst = false;
                sSnakingSliders = false;
            }

            if (sRenderer == @"opengl" && sScreenMode == ScreenMode.Fullscreen)
                sScreenMode.Value = ScreenMode.BorderlessWindow;

            if (GameBase.Tournament)
            {
                if (GameBase.TournamentManager)
                {
                    ConfigManager.sTicker.Value = true;

                    ConfigManager.sWidth.Value = (int)Tournament.CompleteWorkingSpace.X;
                    ConfigManager.sHeight.Value = (int)Tournament.CompleteWorkingSpace.Y;
                }
                else
                {
                    ConfigManager.sTicker.Value = false;

                    ConfigManager.sWidth.Value = (int)Tournament.ClientSizePadded.X;
                    ConfigManager.sHeight.Value = (int)Tournament.ClientSizePadded.Y;
                }

                ConfigManager.sFpsCounter.Value = false;
                ConfigManager.sSkin.Value = @"User";
                ConfigManager.sComboBurst.Value = false;
                ConfigManager.sComboFire.Value = false;
                ConfigManager.sShowStoryboard.Value = false;
                ConfigManager.sScreenMode.Value = ScreenMode.Windowed;
                ConfigManager.sRenderer.Value = @"d3d";
                ConfigManager.sFrameSync.Value = FrameSync.Limit240;
                ConfigManager.sNotifyFriends.Value = false;
                ConfigManager.sChatAudibleHighlight.Value = false;
                ConfigManager.sPopupDuringGameplay.Value = false;
                ConfigManager.sAutomaticDownload.Value = false;
                ConfigManager.sScoreMeter.Value = ScoreMeterType.Colour;
                ConfigManager.sCustomFrameLimit.Value = Tournament.Config.GetValue<int>(@"fps", 60);
                ConfigManager.sIgnoreBeatmapSkins.Value = true;
                ConfigManager.sMenuVoice.Value = false;

                AudioEngine.VolumeMaster.Value = 50;
            }

            BindingManager.Initialize();

            ChatEngine.LoadSettings();
        }

        private static void ReadConfigFile(string filename)
        {
            if (!File.Exists(filename)) return;

            using (StreamReader r = File.OpenText(filename))
                while (!r.EndOfStream)
                {
                    string l = r.ReadLine();
                    if (l.Length == 0 || l[0] == '#') continue;

                    string[] line = l.Split('=');
                    if (line.Length < 2)
                        continue;
                    Configuration[line[0].Trim()] = line[1].Trim();
                }
        }

        internal static void SaveConfig()
        {
            BindingManager.WriteConfiguration();

            if (ChatEngine.channelTabs != null)
            {
                string channels = string.Empty;
                //add tabs in correct order
                foreach (pTab t in ChatEngine.channelTabs.Tabs)
                {
                    Channel c = t.Tag as Channel;
                    if (c != null) channels += c.Name + @" ";
                }
                //add channels which could not be joined
                foreach (string s in sChatChannels.Value.Split(' '))
                    if (!channels.Contains(s))
                        channels += s + @" ";
                Configuration[@"ChatChannels"] = channels.Trim();
            }

            //don't write config for tourney spectators.
            if (GameBase.Tournament) return;

            writeToFile(@"osu!." + GeneralHelper.WindowsPathStrip(Environment.UserName) + @".cfg", false);
            writeToFile(@"osu!.cfg", true);
        }

        internal static void ResetHashes()
        {
            //cached checksum is farked; let's clear and start again.
            List<string> resetKeys = new List<string>();
            foreach (string s in Configuration.Keys)
                if (s.StartsWith(@"h_")) resetKeys.Add(s);
            foreach (string s in resetKeys)
                Configuration.Remove(s);
            SaveConfig();
        }

        private static bool writeToFile(string filename, bool global)
        {
            try
            {
                using (Stream stream = new SafeWriteStream(filename))
                using (StreamWriter w = new StreamWriter(stream))
                {
                    if (!global)
                    {
                        string username = string.IsNullOrEmpty(ConfigManager.sUsername.Value) ? Environment.UserName : ConfigManager.sUsername.Value;
                        w.WriteLine(@"# " + LocalisationManager.GetString(OsuString.ConfigManager_OsuConfigurationFor0), username);
                        w.WriteLine(@"# " + LocalisationManager.GetString(OsuString.ConfigManager_LastUpdatedOn0), DateTime.Now.ToLongDateString());
                        w.WriteLine();
                        w.WriteLine(@"# " + LocalisationManager.GetString(OsuString.ConfigManager_DoNotShare));
                        w.WriteLine(@"# " + LocalisationManager.GetString(OsuString.ConfigManager_DoNotShare2));
                        w.WriteLine();
                    }
                    foreach (KeyValuePair<string, object> p in Configuration)
                        if (global == (p.Key.Contains(@"_")))
                            w.WriteLine(@"{0} = {1}", p.Key, p.Value);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        internal static bool GetTipStatus(Tips index)
        {
            int _index = (int)index;
            if (sGuideTips.Value.Length > _index)
                return sGuideTips.Value[_index] == '1';
            else
                return false;
        }

        internal static void SetTipStatus(Tips index, bool value)
        {
            int _index = (int)index;
            while (sGuideTips.Value.Length <= _index)
                sGuideTips.Value += @"0";
            sGuideTips.Value = sGuideTips.Value.Remove(_index, 1).Insert(_index, value ? @"1" : @"0");
        }

        /// <param name="strictEnum">Set to true to always require correct enum entries, and not convert from int indices</param>
        private static Bindable<T> ReadValue<T>(string key, T defaultValue, bool strictEnum = false) where T : IComparable
        {
            object test;
            T val = defaultValue;
            int intVal = 0;

            if (Configuration.TryGetValue(key, out test))
            {
                if (typeof(T).IsEnum)
                {
                    if (Int32.TryParse(test.ToString(), System.Globalization.NumberStyles.Any, GameBase.nfi, out intVal))
                    {
                        if (!strictEnum) val = (T)(object)intVal;
                    }
                    else
                    {
                        try
                        {
                            val = (T)Enum.Parse(typeof(T), test.ToString(), true);
                        }
                        catch
                        {
                        }
                    }
                }
                else
                    val = (T)test;

            }

            Bindable<T> ret = new Bindable<T>(val) { Description = key, Default = defaultValue };
            Configuration[key] = ret;
            return ret;
        }

        private static Bindable<string> ReadString(string key, string defaultValue)
        {
            return ReadValue<string>(key, defaultValue);
        }

        private static BindableBool ReadBool(string key, bool defaultValue)
        {
            int intVal = 0;
            bool boolVal = defaultValue;
            object test;
            if (Configuration.TryGetValue(key, out test))
            {
                System.Diagnostics.Debug.Assert(!(test is HasObjectValue));

                if (Int32.TryParse(test.ToString(), System.Globalization.NumberStyles.Any, GameBase.nfi, out intVal))
                    boolVal = intVal == 1;
            }

            BindableBool ret = new BindableBool(boolVal) { Default = defaultValue };
            Configuration[key] = ret;
            return ret;
        }

        private static BindableInt ReadInt(string key, int defaultValue, int minValue = Int32.MinValue, int maxValue = Int32.MaxValue)
        {
            BindableInt ret = new BindableInt(ReadIntRaw(key, defaultValue)) { MinValue = minValue, MaxValue = maxValue, Default = defaultValue };
            Configuration[key] = ret;
            return ret;
        }

        private static int ReadIntRaw(string key, int defaultValue)
        {
            int val = defaultValue;
            object test;
            if (Configuration.TryGetValue(key, out test))
            {
                System.Diagnostics.Debug.Assert(!(test is HasObjectValue));

                if (!Int32.TryParse(test.ToString(), System.Globalization.NumberStyles.Any, GameBase.nfi, out val))
                    val = defaultValue;
            }

            return val;
        }

        private static BindableDouble ReadDouble(string key, double defaultValue, double minValue = double.MinValue, double maxValue = double.MaxValue)
        {
            BindableDouble ret = new BindableDouble(ReadDoubleRaw(key, defaultValue)) { MinValue = minValue, MaxValue = maxValue, Default = defaultValue };
            Configuration[key] = ret;
            return ret;
        }

        private static double ReadDoubleRaw(string key, double defaultValue)
        {
            double val = defaultValue;
            object test;
            if (Configuration.TryGetValue(key, out test))
            {
                System.Diagnostics.Debug.Assert(!(test is HasObjectValue));

                if (!Double.TryParse(test.ToString(), System.Globalization.NumberStyles.Any, GameBase.nfi, out val))
                    val = defaultValue;
            }

            return val;
        }

        internal static void ReloadHashCache()
        {
            ReadConfigFile(@"osu!.cfg");
        }

        internal static void PurgeHashCache()
        {
            foreach (string v in new List<string>(Configuration.Keys))
                if (v[1] == '_')
                    Configuration.Remove(v);
            SaveConfig();
        }
    }

}
