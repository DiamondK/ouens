using System;
using System.Collections.Generic;
using System.Text;

namespace osu
{
    /// <summary>
    /// Contains only the version number. This is used by osu!Builder to build specific versions.
    /// It should be updated when necessary to support client-side database fallback support.
    /// </summary>
    internal static partial class General
    {
        internal const int VERSION = 20141028;
        internal const int internal_version = 0;
    }
}
