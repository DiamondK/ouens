namespace osu
{
    internal static partial class General
    {
        internal const string FONT_FACE_DEFAULT = @"Aller Light";
        internal const string FONT_FACE_REGULAR = @"Aller";
        internal const string FONT_FACE_AWESOME = @"FontAwesome";
        internal const string FONT_FACE_ALTERNATIVE = @"Tahoma";
        internal const string IRC_CHANNEL_ERROR = @"#error";
        internal const string IRC_CHANNEL_GLOBAL = @"#global";
        internal const string IRC_CHANNEL_OSU = @"#osu";
        internal const int SLIDER_DETAIL_LEVEL = 50;
        internal const int SYSTEM_METRIC_TABLETPC = 86;
        internal const string TEMPORARY_FILE_PATH = @"Data/a/";

        internal static byte[] OSU_KEY = new byte[] { 1, 3, 3, 7, 7, 3, 3, 1 };

        internal static int VERSION_FIRST_OSZ2 = 20121008;

#if ARCADE
        internal static string SUBVERSION = @"arcade";
#elif Beta
        internal static string SUBVERSION = @"beta";
#elif Release
        internal static string SUBVERSION = @"cuttingedge";
#elif DEBUG
        internal static string SUBVERSION = @"dev";
#elif PublicNoUpdate
        internal static string SUBVERSION = @"public_test";
#else
        internal static string SUBVERSION = string.Empty;
#endif

        internal static string BUILD_NAME { get { return @"b" + FULL_VERSION + SUBVERSION; } }

        internal static string FULL_VERSION { get { return VERSION + (internal_version > 0 ? @"." + internal_version : string.Empty); } }

        /// <summary>
        /// Version string visible in window title.
        /// </summary>
#if !Public
        internal static string PUBLIC_NAME = @"b" + VERSION;
#else
        internal static string PUBLIC_NAME = string.Empty;
#endif

        internal const string WEB_ROOT = @"http://osu.ppy.sh";
        internal const string STATIC_WEB_ROOT = @"http://s.ppy.sh";
        internal const string STATIC_WEB_ROOT_BEATMAP = @"http://b.ppy.sh";
        internal const string STATIC_WEB_ROOT_AVATAR = @"http://a.ppy.sh";
    }
}
