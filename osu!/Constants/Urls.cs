namespace osu
{
    static class Urls
    {
        /// <summary>
        /// User specific ranking page.
        /// </summary>
        public static string USER_RANKING = General.WEB_ROOT + @"/p/playerranking/?f={0}#jumpto";

        public static string USER_PROFILE = General.WEB_ROOT + @"/u/{0}";

        public static string USER_PROFILE_COMPACT = USER_PROFILE + @"?compact=1";

        /// <summary>
        /// Quick reply page for a thread.
        ///
        /// {0} - Thread ID
        /// </summary>
        public const string BEATMAP_REPLY = General.WEB_ROOT + @"/forum/posting.php?mode=reply&t={0}";

        /// <summary>
        /// Thread page.
        ///
        /// {0} - Thread ID
        /// </summary>
        public const string FORUM_TOPIC = General.WEB_ROOT + @"/forum/t/{0}";

        /// <summary>
        /// Gets the topic contents of the topic of a specific beatmapset.
        ///
        /// Arguments:
        /// {u} - Username
        /// {h} - Password
        /// {s} - BeatmapSetId
        ///
        /// Returns(a list of responses separated by ETX chars):
        /// [0] - Response (0 = success, >0 = error code)
        /// [1] - thread id
        /// [2] - thread subject
        /// [3] - thread contents(html removed)
        /// </summary>
        public const string FORUM_GET_BEATMAP_TOPIC_INFO = General.WEB_ROOT + @"/web/osu-get-beatmap-topic.php?u={0}&h={1}&s={2}";

        /// <summary>
        /// Post page.
        ///
        /// {0} - Post ID
        /// </summary>
        public const string FORUM_POST = General.WEB_ROOT + @"/forum/p/{0}";

        /// <summary>
        /// Score listing page for a specific beatmap.
        ///
        /// {0} - Beatmap ID
        /// </summary>
        public const string BEATMAP_LISTING = General.WEB_ROOT + @"/b/{0}";

        public const string BEATMAP_SET_LISTING = General.WEB_ROOT + @"/s/{0}";

        internal const string HELP_FAQ = General.WEB_ROOT + @"/p/faq/?n=5";

        internal const string UPDATE_DOWNLOAD_URL = General.WEB_ROOT + @"/release/";

        /// <summary>
        /// Does server-side checks to ensure a beatmap can be submitted online.
        ///
        /// {0} - Username
        /// {1} - Password
        /// {2} - BeatmapSetId (if available)
        /// {3} - BeatmapIds (comma separated list)
        /// {4} - Osz2Hash (if available)
        ///
        /// Returns:
        /// [0] - Response (0 = success, >0 = error code)
        /// [1] - BeatmapSetId (if new)
        /// [2] - New beatmapIds (comma separated list, up to count required)
        /// [3] - Type of submission (1 = full submit, 2 = patch-submit)
        ///
        /// </summary>
        internal const string OSZ2_SUBMIT_GET_ID = General.WEB_ROOT + @"/web/osu-osz2-bmsubmit-getid.php?u={0}&h={1}&s={2}&b={3}&z={4}";


        /// <summary>
        /// Does server-side checks to ensure a beatmap can be submitted online.
        ///
        /// {0} - Username
        /// {1} - Password
        /// {2} - Type of submission (1 = full submit, 2 = patch-submit)
        /// {3} - Hashes of the new osz2 file
        /// {4} - BeatmapSetID
        /// {post} - File contents: either osz2 or bz2
        ///
        /// Returns:
        /// [0] - Response (0 = success, >0 = error code)
        ///
        /// </summary>
        internal const string OSZ2_SUBMIT_UPLOAD = General.WEB_ROOT + @"/web/osu-osz2-bmsubmit-upload.php?u={0}&h={1}&t={2}&z={3}&s={4}";


        /// <summary>
        /// Gets the BodyHash and the HeaderHash of the latest osz2 file stored on the server.
        ///
        /// {0} - Beatmap ID
        /// </summary>
        internal const string OSZ2_GET_HASHES = General.WEB_ROOT + @"/web/osu-gethashes.php?s={0}";

        /// <summary>
        /// Gets the latest OsuMagnet
        ///
        /// {0} - BeatmapSetID
        /// {1} - Novideo? (0 = false) (1 = true)
        /// {2} - User
        /// {3} - Pass
        /// </summary>
        internal const string OSZ2_GET_MAGNET = General.WEB_ROOT + @"/web/osu-magnet.php?s={0}&v={1}&u={2}&p={3}";


        /// <summary>
        /// Gets the raw encrypted osz2 header.
        ///
        /// {0} - Username
        /// {1} - Password
        /// {2} - BeatmapSetID
        /// </summary>
        internal const string OSZ2_GET_RAW_HEADER = General.WEB_ROOT + @"/web/osu-osz2-getrawheader.php?u={0}&h={1}&s={2}";

        /// <summary>
        /// Gets the unencrypted filecontents of the specified file in the osz2 package.
        ///
        /// {0} - Username
        /// {1} - Password
        /// {2} - BeatmapSetID
        /// {3} - Filename
        internal const string OSZ2_GET_FILE_CONTENTS= General.WEB_ROOT + @"/web/osu-osz2-getfilecontents.php?u={0}&h={1}&s={2}&f={3}";


        /// <summary>
        /// Return the list of all fileinfo headers contained in the mappackage by encoding it into a string:
        /// Fileinfo will be written in a comma seperated list as: filename, offset, length, md5(hex)
        /// Multiple fileinfo headers will be seperated by the '|' char.
        ///
        /// {0} - Username
        /// {1} - Password
        /// {2} - BeatmapSetID
        internal const string OSZ2_GET_FILE_INFO = General.WEB_ROOT + @"/web/osu-osz2-getfileinfo.php?u={0}&h={1}&s={2}";




        /// <summary>
        /// Streams an avatar.
        ///
        /// {0} - Filename of avatar to stream back.
        /// </summary>
        internal const string USER_GET_AVATAR = General.STATIC_WEB_ROOT_AVATAR + @"/{0}";

        /// <summary>
        /// Streams an avatar avoiding any CDN caching.
        ///
        /// {0} - Filename of avatar to stream back.
        /// </summary>
        internal const string USER_GET_AVATAR_NOCACHE = General.STATIC_WEB_ROOT_AVATAR + @"/{0}";

        /// <summary>
        /// Sets an avatar via phpbb cpanel.
        /// </summary>
        internal const string USER_SET_AVATAR = General.WEB_ROOT + @"/forum/ucp.php?i=profile&mode=avatar";

        /// <summary>
        /// Path containing raw .osu files for all submitted maps.
        /// </summary>
        internal const string PATH_MAPS = General.WEB_ROOT + @"/web/maps/";

        /// <summary>
        /// Link to thread containing details on how to use custom samples.
        /// </summary>
        internal const string CUSTOM_SAMPLES = General.WEB_ROOT + @"/forum/t/729";

        /// <summary>
        /// Link to thread containing details on customising the countdown.
        /// </summary>
        internal const string CUSTOM_COUNTDOWN = General.WEB_ROOT + @"/forum/t/2003";

        /// <summary>
        /// {0} - Username
        /// {1} - Password
        /// </summary>
        internal const string GET_FAVOURITES = General.WEB_ROOT + @"/web/osu-getfavourites.php?u={0}&h={1}";

        /// <summary>
        /// {0} - Username
        /// {1} - Password
        /// {2} - BeatmapSetId
        /// </summary>
        internal const string ADD_FAVOURITE = General.WEB_ROOT + @"/web/osu-addfavourite.php?u={0}&h={1}&a={2}";

        internal const string RANKING_CRITERIA = General.WEB_ROOT + @"/wiki/Ranking_Criteria";
        internal const string FORUM_MAPPING_HELP = General.WEB_ROOT + @"/forum/56";
        internal const string HELP_SUBMISSION = General.WEB_ROOT + @"/wiki/BSS";
        internal const string FORUM_MODDING_QUEUES = General.WEB_ROOT + @"/forum/60";
        /// <summary>
        /// {0} - Username
        /// {1} - Password
        /// {2} - SearchType
        /// {3} - Search
        /// </summary>
        internal const string DIRECT_SEARCH_SET = General.WEB_ROOT + @"/web/osu-search-set.php?u={0}&h={1}&{2}={3}";

        /// <summary>
        /// {0} - Username
        /// {1} - Password
        /// {2} - DisplayMode
        /// {3} - Search
        /// </summary>
        internal const string DIRECT_SEARCH = General.WEB_ROOT + @"/web/osu-search.php?u={0}&h={1}&r={2}&q={3}&m={4}&p={5}";
    }
}
