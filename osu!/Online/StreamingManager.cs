﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Online;
using osu.GameModes.Play;
using osu.GameModes.Select;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Input;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements;
using osu.Online.Social;

namespace osu.Online
{
    internal static class StreamingManager
    {
        internal static User CurrentlySpectating;
        private static bool iDontHaveThatBeatmap;
        internal static object LockReplayScore = new object();
        private static ReplayAction queuedAction = ReplayAction.Standard;
        internal static Queue<Score> ReplayQueue = new Queue<Score>();
        public static List<User> Spectators = new List<User>();
        public static List<User> FellowSpectators = new List<User>();
        internal static List<bReplayFrame> waitingOutgoingFrames = new List<bReplayFrame>();
        internal static int ScoreSyncNext;
        internal static bScoreFrame ScoreSyncFrame;
        internal static bool HasSpectators;
        private static bool NewUserAndSong;

        internal static ReplayAction LastAction;


        public static void HandleFrames(bReplayFrameBundle bundle)
        {
#if DEBUG
            /*GameBase.ShowMessage(
                "received frames (" + bundle.Action +
                (bundle.ReplayFrames.Count > 0
                     ? "," + bundle.ReplayFrames[0].time + "-" + bundle.ReplayFrames[bundle.ReplayFrames.Count - 1].time +
                       ")"
                     : "0)"), Color.Green, 1000);*/
#endif

            waitingOnHost = false;

            LastAction = bundle.Action;

            switch (bundle.Action)
            {
                case ReplayAction.Completion:
                    InputManager.ReplayToEnd = true;
                    break;
                case ReplayAction.Fail:
                    InputManager.ReplayToEnd = true;
                    break;
                case ReplayAction.WatchingOther:
                    User u = BanchoClient.GetUserById(bundle.Extra);
                    if (u != null && u.Id != GameBase.User.Id)
                    {
                        NotificationManager.ShowMessage("Following host to new user...", Color.Green, 3000);
                        StartSpectating(u);
                    }
                    else
                        StopSpectating();
                    break;
                case ReplayAction.NewSong:
                    iDontHaveThatBeatmap = false;
                    //ensure spectators use the same seed as players.
                    Player.Seed = bundle.Extra;
                    HandleSongChange(true);
                    break;
                case ReplayAction.Skip:
                    Player.QueueSkip();
                    break;
                case ReplayAction.Standard:
                    break;
                case ReplayAction.Pause:
                    if (!Player.Paused && Player.Instance != null) Player.Instance.TogglePause();
                    break;
                case ReplayAction.Unpause:
                    if (Player.Paused && Player.Instance != null) Player.Instance.TogglePause();
                    break;
            }


            if (!iDontHaveThatBeatmap && InputManager.ReplayScore == null)
            {
                if (bundle.ReplayFrames.Count > 0)
                    InputManager.ReplayStartTime = bundle.ReplayFrames[0].time - 50;
                else // set to 1 instead of 0 to make it not doubleskip if the first frame is a skip frame.
                    InputManager.ReplayStartTime = 1;
                Player.Seed = bundle.Extra;
                HandleSongChange(false);
            }

            float lastY = 0;
            int lastOffset = 0;
            lock (LockReplayScore)
            {
                if (InputManager.ReplayScore != null && InputManager.ReplayScore.replay != null)
                {
                    foreach (bReplayFrame f in bundle.ReplayFrames)
                    {
                        InputManager.ReplayScore.replay.Add(f);
                    }
                    if (bundle.ReplayFrames.Count > 0)
                    {
                        int last = bundle.ReplayFrames.Count - 1;
                        lastY = bundle.ReplayFrames[last].mouseY;
                        lastOffset = bundle.ReplayFrames[last].time;
                    }
                    if ((ScoreSyncNext <= 0 || ScoreSyncNext < InputManager.ReplayFrame || InputManager.ReplayScore.replay.Count - 1 < ScoreSyncNext) && bundle.ScoreFrame.totalScore > 0)
                    {
                        ScoreSyncNext = InputManager.ReplayScore.replay.Count - 1;
                        ScoreSyncFrame = bundle.ScoreFrame;
                    }
                }
            }

            if (HasSpectators && CurrentlySpectating != null && CurrentlySpectating.Id != GameBase.User.Id && GameBase.Time - lastFollowNotification > 10000)
            {
                //tell spectators we are watching someone else.
                BanchoClient.SendRequest(RequestType.Osu_SpectateFrames, new bReplayFrameBundle(new List<bReplayFrame>(), ReplayAction.WatchingOther, new bScoreFrame(), CurrentlySpectating.Id));
                lastFollowNotification = GameBase.Time;
            }
        }

        static int lastFollowNotification;

        public static void PushNewFrame(bReplayFrame f)
        {
            if (!HasSpectators && waitingOutgoingFrames.Count > 0)
            {
                waitingOutgoingFrames.Clear();
                return;
            }

            waitingOutgoingFrames.Add(f);

            if (waitingOutgoingFrames.Count >= 30)
            {
                PurgeFrames(queuedAction);
                queuedAction = ReplayAction.Standard;
            }
        }

        static bool waitingOnHost;
        internal static void WaitingOnHost()
        {
            waitingOnHost = true;
            GameBase.Scheduler.AddDelayed(delegate {
                if (waitingOnHost && GameBase.Mode == OsuModes.Play && Player.Paused)
                    GameBase.ChangeMode(OsuModes.Menu);
                waitingOnHost = false;
            }, 10000);
        }

        internal static void PurgeFrames(ReplayAction action, int? extra = null)
        {
            if (!HasSpectators || InputManager.ReplayMode)
                return;

            if (action == ReplayAction.NewSong)
                lock (Spectators)
                    foreach (User u in Spectators)
                        u.CantSpectate = false;

            bScoreFrame scoreFrame = Player.GetScoreFrame();

            BanchoClient.SendRequest(RequestType.Osu_SpectateFrames, new bReplayFrameBundle(waitingOutgoingFrames, action, scoreFrame, extra ?? Player.Seed));

            waitingOutgoingFrames = new List<bReplayFrame>();
        }

        public static void StopSpectating(bool ExitPlayMode = true)
        {
            if (GameBase.TourneySpectatorName != null) GameBase.TourneySpectatorName.Text = string.Empty;

            if (ExitPlayMode && (GameBase.Mode == OsuModes.Play || (GameBase.Mode != OsuModes.Menu && GameBase.Tournament)))
            {
                InputManager.ReplayMode = false;
                if (CurrentlySpectating == null)
                    GameBase.ChangeMode(OsuModes.SelectPlay, true);
                else
                    GameBase.ChangeMode(OsuModes.Menu, true);
            }

            if (CurrentlySpectating != null)
            {
                lock (LockReplayScore)
                {
                    BanchoClient.SendRequest(RequestType.Osu_StopSpectating, null);
                    NotificationManager.ShowMessage("Stopped spectating " + CurrentlySpectating.Name, Color.Tomato, 3000);
                    CurrentlySpectating = null;
                    InputManager.ReplayStreaming = false;
                }
                if (ChatEngine.channels.Exists(s => s.Name == "#spectator"))
                    ChatEngine.RemoveChannel(ChatEngine.channels.Find(s => s.Name == "#spectator"), false, true);
            }

            FellowSpectators.Clear();
            ReplayQueue.Clear();
            GameBase.SetTitle();
            InputManager.ReplayStartTime = 0;
        }

        public static bool StartSpectating(User u)
        {
            if (u == null) return false;

            if (Lobby.Status != LobbyStatus.NotJoined)
            {
                NotificationManager.ShowMessage("Can't spectate while in multiplayer mode!", Color.Tomato, 3000);
                return false;
            }

            bool newUser = u != CurrentlySpectating;

            if (u.Id == GameBase.User.Id)
            {
                StopSpectating(CurrentlySpectating != null);
                return false;
            }

            lock (LockReplayScore)
            {
                NewUserAndSong = true;

                if (GameBase.Mode == OsuModes.Edit || GameBase.Mode == OsuModes.Play)
                    GameBase.ChangeMode(OsuModes.Menu, true);

                CurrentlySpectating = u;
                InputManager.ReplayMode = false;
                iDontHaveThatBeatmap = false;

                if (newUser)
                {
                    FellowSpectators.Clear();
                    u.RequestPresence();

                    NotificationManager.ShowMessage("Started spectating " + u.Name, Color.Green, 3000);
                    GameBase.SetTitle();
                }

                BanchoClient.SendRequest(RequestType.Osu_StartSpectating, new bInt(u.Id));
            }

            return true;
        }

        public static void HandleSongChange(bool resetStart)
        {
            lock (LockReplayScore)
            {
                if (CurrentlySpectating == null)
                    return;

                Beatmap b = BeatmapManager.GetBeatmapByChecksum(CurrentlySpectating.CurrentBeatmapChecksum);

                if (GameBase.Tournament && LastAction == ReplayAction.SongSelect)
                    return;

                if (b != null)
                {
                    NotificationManager.ShowMessage("Host is playing:" + b.DisplayTitle, Color.Green, 1000);

                    BeatmapManager.Current = b;
                    InputManager.ReplayMode = true;
                    InputManager.ReplayToEnd = false;

                    InputManager.ReplayStreaming = true;

                    InputManager.ReplayScore = ScoreFactory.Create(CurrentlySpectating.PlayMode, BeatmapManager.Current);
                    InputManager.ReplayScore.AllowSubmission = false;
                    InputManager.ReplayScore.enabledMods = CurrentlySpectating.CurrentMods;

                    if (resetStart)
                        InputManager.ReplayStartTime = 0;

                    if (NewUserAndSong)
                    {
                        GameBase.Scheduler.Add(delegate { if (ChatEngine.IsFullVisible) ChatEngine.ToggleFull(); });
                        NewUserAndSong = false;
                    }

                    GameBase.ChangeMode(OsuModes.Play, true);
                }
                else if (!string.IsNullOrEmpty(CurrentlySpectating.BeatmapName))
                {
                    if (CurrentlySpectating.BeatmapId > 0 && (BanchoClient.Permission & Permissions.Supporter) > 0)
                    {
                        if (OsuDirect.ActiveDownloads.Find(d => d.beatmap != null && d.beatmap.beatmapId == CurrentlySpectating.BeatmapId) == null
                            && (OsuDirect.RespondingBeatmap == null || OsuDirect.RespondingBeatmap.beatmapId != CurrentlySpectating.BeatmapId))
                            OsuDirect.HandlePickup(LinkId.Beatmap, CurrentlySpectating.BeatmapId, RestartSpectating);
                    }
                    else
                    {
                        string message = string.Format("You don't have the beatmap the host is playing ({0}).", CurrentlySpectating.BeatmapName);
                        NotificationManager.ShowMessage(message);
                    }

                    BanchoClient.SendRequest(RequestType.Osu_CantSpectate, null);
                    iDontHaveThatBeatmap = true;

                    InputManager.ReplayMode = false;
                    if (GameBase.Mode == OsuModes.Play)
                        GameBase.ChangeMode(OsuModes.Menu, true);
                }
            }
        }

        private static void RestartSpectating(object sender, EventArgs e)
        {
            User u = CurrentlySpectating;
            if (u == null) return;

            BeatmapImport.ForceRun(false);

            GameBase.Scheduler.Add(delegate { restartSpectatingPoll(u); }, true);
        }

        private static void restartSpectatingPoll(User u)
        {
            if (GameBase.Mode != OsuModes.BeatmapImport)
            {
                StartSpectating(u);
                return;
            }

            GameBase.Scheduler.AddDelayed(delegate { restartSpectatingPoll(u); }, 100);
        }

        public static void Reset()
        {
            waitingOutgoingFrames = new List<bReplayFrame>();
            PurgeFrames(ReplayAction.NewSong);
            StopSpectating(false);
        }

        internal static void Cleanup()
        {
            lock (LockReplayScore)
            {
                Spectators.Clear();
                StopSpectating(false);
                HasSpectators = false;
            }
        }
    }
}