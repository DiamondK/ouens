using System;
using System.Collections.Generic;
using System.Text;
using osu.Online.Drawable;
using osu.Graphics.UserInterface;
using osu.Graphics.Notifications;
using osu.GameModes.Online;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.Input;
using Microsoft.Xna.Framework;
using osu.Graphics;
using osu_common.Bancho.Requests;
using osu_common.Bancho.Objects;
using osu.Graphics.Sprites;
using osu_common.Helpers;
using osu.Online.Social;

namespace osu.Online
{
    class UserProfile : pDrawableComponent
    {
        internal static List<UserProfile> ActiveProfiles = new List<UserProfile>();

        internal User User;
        internal User DisplayedUser;
        private bool ShowExtraOptions;
        private static pBrowser browser;

        public UserProfile(User user)
        {
            User = user;
            DisplayedUser = user.Clone() as User;
        }

        internal static void DisplayProfileFor(User user, bool userPanelClick = false)
        {
            UserProfile profile = ActiveProfiles.Find(p => p.User == user);

            if (profile != null)
                return;

            profile = new UserProfile(user);
            profile.ShowExtraOptions = !userPanelClick;

            ActiveProfiles.Add(profile);

            profile.Display();
        }

        internal override void Dispose(bool isDisposing)
        {
            if (DisplayedUser != null) DisplayedUser.Dispose();

            base.Dispose(isDisposing);
        }

        private void Display()
        {
            pDialog dialog = new pDialog(null, false);
            dialog.spriteManager.HandleOverlayInput = true;
            dialog.ButtonOffset = new Vector2(50, 60);

            ChatEngine.RequestUserStats(new List<User>() { User }, true);

            bool isSelf = User.Name == GameBase.User.Name;

            dialog.Closed += delegate { ActiveProfiles.Remove(this); };

            if (User.IsOsu && !isSelf)
            {
                if (GameBase.Mode != OsuModes.Edit && (GameBase.Mode != OsuModes.Play || !GameBase.TestMode))
                {
                    dialog.AddOption(User == StreamingManager.CurrentlySpectating ? LocalisationManager.GetString(OsuString.UserProfile_StopSpectating) : LocalisationManager.GetString(OsuString.UserProfile_StartSpectating),
                                     Color.YellowGreen,
                                     delegate
                                     {
                                         if (Lobby.Status != LobbyStatus.NotJoined)
                                         {
                                             NotificationManager.ShowMessage(
                                                 LocalisationManager.GetString(OsuString.UserProfile_CantSpectate));
                                             return;
                                         }

                                         if (GameBase.Mode == OsuModes.Play && !InputManager.ReplayMode)
                                         {
                                             NotificationManager.ShowMessage(
                                                 LocalisationManager.GetString(OsuString.UserProfile_CantSpectate));
                                             return;
                                         }

                                         if (User.Name == ConfigManager.sUsername ||
                                             User == StreamingManager.CurrentlySpectating)
                                             StreamingManager.StopSpectating(true);
                                         else
                                             StreamingManager.StartSpectating(User);
                                     });
                }
            }

            dialog.AddOption(LocalisationManager.GetString(OsuString.UserProfile_ViewProfile), new Color(58, 110, 165),
                delegate
                {
                    GameBase.ProcessStart(User.Id == 0 ? @"http://osu.ppy.sh/wiki/BanchoBot" : String.Format(Urls.USER_PROFILE, User.Id));
                });

            if (isSelf)
            {
                if (GameBase.Mode == OsuModes.Menu && BanchoClient.AllowUserSwitching)
                    dialog.AddOption(LocalisationManager.GetString(OsuString.UserProfile_SignOut), Color.Orange, delegate 
                    {
                        GameBase.Options.PerformLogout();
                        if (!GameBase.Options.Expanded)
                            GameBase.ShowLogin();
                    });
                dialog.AddOption(LocalisationManager.GetString(OsuString.UserProfile_ChangeAvatar), Color.Orange, delegate { GameBase.ProcessStart(Urls.USER_SET_AVATAR); });
            }
            else
            {
                dialog.AddOption(LocalisationManager.GetString(OsuString.UserProfile_StartChat), Color.MediumPurple,
                             delegate { ChatEngine.StartChat(User); });

                if (User.IsOsu)
                {
                    if (MatchSetup.Match != null)
                        dialog.AddOption(LocalisationManager.GetString(OsuString.UserProfile_Invitetogame), Color.Yellow, delegate
                        {
                            BanchoClient.SendRequest(new Request(RequestType.Osu_Invite, new bInt(User.Id)));
                            NotificationManager.ShowMessageMassive(string.Format(LocalisationManager.GetString(OsuString.ChatEngine_UserInvited), User.Name));
                        });
                }

                dialog.AddOption(User.IsFriend ? LocalisationManager.GetString(OsuString.UserProfile_CancelFriendship) : LocalisationManager.GetString(OsuString.UserProfile_AddFriend), Color.Pink, delegate { ChatEngine.ToggleFriend(User, !User.IsFriend); });

                if (ShowExtraOptions && User.Id > 0)
                {
                    dialog.AddOption(LocalisationManager.GetString(OsuString.UserProfile_ReportUser), Color.Red,
                                     delegate
                                     {
                                         BanchoClient.SendRequest(RequestType.Osu_SendIrcMessage, new bMessage(null, ChatEngine.activeChannel.Name, "!report " + User.Name));
                                     });

                    if (ChatEngine.CheckIgnore(User))
                        dialog.AddOption(LocalisationManager.GetString(OsuString.UserProfile_UnignoreUser), Color.Gray, delegate { ChatEngine.UnignoreUser(User); });
                    else
                        dialog.AddOption(LocalisationManager.GetString(OsuString.UserProfile_IgnoreUser), Color.Gray, delegate { ChatEngine.IgnoreUser(User); });
                }
            }

            dialog.AddOption(LocalisationManager.GetString(OsuString.General_Close), Color.DarkGray, null);


            if (DisplayedUser.DrawAt(new Vector2(5, 5), true, 0))
                dialog.spriteManager.Add(DisplayedUser.Sprites);

            GameBase.ShowDialog(dialog);

            /*if (User.Id != 0)
            {
                GameBase.Scheduler.Add(delegate
                {
                    if (!dialog.IsDisplayed) return;

                    if (browser == null)
                    {
                        GameBase.OnResolutionChange += GameBase_OnResolutionChange;
                        browser = new pBrowser(null, Vector2.Zero, new Vector2(220, GameBase.WindowHeight - 48 * GameBase.WindowRatio), 0.911f, Color.White);
                        browser.Disposable = false;
                        browser.OnLoaded += delegate
                        {
                            browser.FadeIn(500);
                            browser.MoveToRelative(new Vector2(88, 0), 500, EasingTypes.In);
                        };
                    }

                    browser.Transformations.Clear();
                    browser.CurrentAlpha = 0;
                    browser.AlwaysDraw = true;
                    browser.CurrentPosition = new Vector2(-88, 48);
                    browser.Load(string.Format(Urls.USER_PROFILE_COMPACT, User.Id));

                    dialog.spriteManager.Add(browser);
                }, 1000);
            }*/
        }

        void GameBase_OnResolutionChange()
        {
            GameBase.OnResolutionChange -= GameBase_OnResolutionChange;
            if (browser != null)
            {
                browser.IsDisposable = true;
                browser.Dispose();
                browser = null;
            }
        }

        internal void HandleUpdate(bUserStats stats)
        {
            throw new NotImplementedException();
        }
    }
}
