﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace osu.Online
{
    /// <summary>
    /// Location and intensity data used for displaying points on a map.
    /// </summary>
    class HotSpot
    {
        public Vector2 Position;
        public int Intensity;

        public HotSpot(Vector2 position, int intensity = 0)
        {
            this.Position = position;
            this.Intensity = intensity;
        }
    }
}
