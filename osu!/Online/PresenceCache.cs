﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using osu.Helpers;
using osu_common.Bancho;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu_common.Helpers;

namespace osu.Online
{
    internal class PresenceCacheItem : bSerializable
    {
        internal bUserPresence Presence;
        internal DateTime LastUpdate;

        public void ReadFromStream(SerializationReader sr)
        {
            Presence = new bUserPresence(sr);
            LastUpdate = sr.ReadDateTime();
        }

        public void WriteToStream(SerializationWriter sw)
        {
            Presence.WriteToStream(sw);
            sw.Write(LastUpdate);
        }
    }

    static class PresenceCache
    {
        internal static string DATABASE_FILENAME = "presence.db";

        private static int DatabaseVersion;
        static bool initialised;

        static Dictionary<int, PresenceCacheItem> idToPresenceCache = new Dictionary<int, PresenceCacheItem>();
        static List<PresenceCacheItem> cache = new List<PresenceCacheItem>();

        internal static void DatabaseSerialize()
        {
            //never write a database if we aren't the lead spectator.
            if (GameBase.Tournament && !GameBase.TournamentManager)
                return;

            try
            {
                using (Stream stream = new SafeWriteStream(DATABASE_FILENAME))
                using (SerializationWriter sw = new SerializationWriter(stream))
                {
                    sw.Write(General.VERSION);
                    sw.Write(cache);
                }
            }
            catch (Exception e)
            {
            }
        }

        static bool isInitialized;
        internal static void Initialize()
        {
            if (isInitialized) return;
            DatabaseDeserialize();
        }

        internal static void ResetActiveRequests()
        {
            lock (idToPresenceCache)
            {
                pendingQueries = new List<int>();
                activeQueries = new List<int>();
            }
        }

        private static void DatabaseDeserialize()
        {
            if (File.Exists(DATABASE_FILENAME))
            {
                try
                {
                    using (Stream stream = File.Open(DATABASE_FILENAME, FileMode.Open, FileAccess.Read))
                    {
                        using (SerializationReader sr = new SerializationReader(stream))
                        {
                            DatabaseVersion = sr.ReadInt32();
                            cache = (List<PresenceCacheItem>)sr.ReadBList<PresenceCacheItem>();
                        }
                    }
                }
                catch (Exception e)
                {
                }

                DateTime expiryDate = DateTime.Now.AddHours(-24 * 4);

                cache.RemoveAll(i => i.LastUpdate < expiryDate);

                ReloadDictionaries();
            }
        }

        private static void ReloadDictionaries()
        {
            foreach (PresenceCacheItem i in cache)
                idToPresenceCache[i.Presence.userId] = i;
        }

        internal static void Cache(bUserPresence presence)
        {
            lock (idToPresenceCache)
            {
                PresenceCacheItem item = Query(presence.userId, false);

                if (item == null)
                {
                    item = new PresenceCacheItem();

                    cache.Add(item);
                    idToPresenceCache[presence.userId] = item;
                }

                item.Presence = presence;
                item.LastUpdate = DateTime.Now;

                totalFills++;

                activeQueries.Remove(presence.userId);
            }
        }

        static List<int> activeQueries = new List<int>();
        static List<int> pendingQueries = new List<int>();

        static int totalHits, totalMisses, totalRequests, totalFills;

        internal static PresenceCacheItem Query(int id, bool fill = true)
        {
            lock (idToPresenceCache)
            {
                PresenceCacheItem match = null;
                if (idToPresenceCache.TryGetValue(id, out match))
                    totalHits++;
                else
                    totalMisses++;

                if (fill && (match == null || (DateTime.Now - match.LastUpdate).TotalHours > 24))
                    Fill(id);

                return match;
            }
        }

        private static void Fill(int id)
        {
            lock (idToPresenceCache)
            {
                if (id <= 0 || activeQueries.Contains(id)) return;

                totalRequests++;

                bool scheduleRequired = pendingQueries.Count == 0;

                activeQueries.Add(id);
                pendingQueries.Add(id);

                if (scheduleRequired) GameBase.Scheduler.AddDelayed(scheduledFill, 600);
            }
        }

        private static void scheduledFill()
        {
            lock (idToPresenceCache)
            {
                if (pendingQueries.Count > 256)
                    QueryAll();
                else
                    BanchoClient.SendRequest(RequestType.Osu_UserPresenceRequest, new bListInt(pendingQueries));

                pendingQueries = new List<int>();
            }
        }

        static int lastFullPresenceRequest;
        internal static bool QueryAll()
        {
            if (lastFullPresenceRequest > 0 && GameBase.Time - lastFullPresenceRequest < 300000)
                return false;

            lastFullPresenceRequest = GameBase.Time;
            BanchoClient.SendRequest(RequestType.Osu_UserPresenceRequestAll, GameBase.Time);
            return true;
        }

        internal static string Status()
        {
            return "PRESENCE C: " + cache.Count + " | H: " + totalHits + " M: " + totalMisses + " R: " + totalRequests + " F: " + totalFills + " | act: " + activeQueries.Count;
        }
    }

    internal delegate void PresenceFilledCallback(PresenceCacheItem b);
}
