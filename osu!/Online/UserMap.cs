﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Online.Drawable;
using Microsoft.Xna.Framework.Graphics;

namespace osu.Online
{
    class UserMap : DrawableGameComponent
    {
        internal SpriteManager spriteManager;

        private pSprite map;
        private bool Loaded;

        public UserMap(Game game)
            : base(game)
        {
            spriteManager = new SpriteManager(true);
        }

        protected override void Dispose(bool disposing)
        {
            spriteManager.Dispose();
            base.Dispose(disposing);
        }

        public override void Draw()
        {
            if (!Loaded)
                InitialLoad();

            if (spriteManager.Alpha == 0) return;

            spriteManager.Draw();

            foreach (HotSpot v in points)
                GameBase.LineManager.DrawPoint(v.Position.X, v.Position.Y, new Color(255, 85, 204, (byte)(spriteManager.Alpha * 100f * Math.Min(2, (1 + 8 * v.Intensity / 100f)))));

            base.Draw();
        }


        int lastUpdateTime;

        public override void Update()
        {
            if (spriteManager.Alpha == 0) return;

            if (GameBase.Time - lastUpdateTime > 5000)
                doUpdate();

            base.Update();
        }

        List<HotSpot> points = new List<HotSpot>();
        private void doUpdate()
        {
            points.Clear();

            int xOffset = GameBase.WindowWidthScaled;
            float xScale = 1.541666666666668f;
            float yOffset = 61f;
            float yScale = 1.55f;

            float threshold = 2;

            lock (BanchoClient.Users)
                foreach (User u in BanchoClient.Users)
                {
                    if (!u.Connected) continue;

                    HotSpot thisPoint = new HotSpot(new Vector2((u.Longitude * xScale + GameBase.WindowWidthScaled / 2) * GameBase.WindowRatio, ((-u.Latitude + 90) * yScale + yOffset) * GameBase.WindowRatio + GameBase.WindowOffsetY));

                    HotSpot found = points.Find(h => Vector2.DistanceSquared(h.Position, thisPoint.Position) < threshold);

                    if (found != null)
                        found.Intensity++;
                    else
                        points.Add(thisPoint);
                }

            lastUpdateTime = GameBase.Time;
        }

        private void InitialLoad()
        {
            if (Loaded) return;

            //todo: this should be a temporal load
            map = new pSprite(SkinManager.Load(@"worldmap", SkinSource.Osu), Fields.TopCentre, Origins.TopCentre, Clocks.Game, new Vector2(0, 65));
            map.Scale = 1;
            map.AlwaysDraw = true;

            spriteManager.Add(map);

            Loaded = true;
        }
    }
}
