﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Select.Drawable;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Online.P2P;
using osu_common.Libraries.NetLib;
using MonoTorrent.Common;
using osu.Graphics.Notifications;
using osu.GameplayElements.Beatmaps;
using osu_common.Helpers;
using osu.GameModes.Play;

namespace osu.Online
{
    internal enum DownloadStatus
    {
        Waiting,
        Starting,
        Downloading,
        Completed
    }
    internal delegate void DownloadStarted();
    internal delegate void DownloadUpdated(double progress);
    internal delegate void DownloadFinished(bool succeeded);

    internal class OsuDirectDownload : IComparer<OsuDirectDownload>, IComparable<OsuDirectDownload>
    {
        internal const string DOWNLOADS_FOLDER = @"Downloads";

        protected const float HEIGHT = 18;
        internal const float PADDING = 5;
        protected const float PROGRESS_BAR_WIDTH = 120 - PADDING * 2;
        protected const float WIDTH = 120;
        public string filename { get; protected set; }
        protected FileNetRequest request;
        public string title { get; protected set; }
        protected pSprite s_background;
        protected pSprite s_progressBar;
        protected pSprite s_progressBarBg;
        protected pText s_progressText;
        protected pText s_title;
        internal List<pSprite> SpriteCollection;
        internal DownloadStatus Status;
        protected string localFilename;
        protected int filesize;
        internal OnlineBeatmap beatmap;
        internal DownloadStarted onStart = delegate { };
        internal DownloadUpdated onUpdate = delegate { };
        internal DownloadFinished onFinish = delegate { };
        public OsuDirectDownload(OnlineBeatmap ob, bool noVideo)
            : this(ob.setId, ob.filename, ob.title, noVideo, noVideo ? ob.filesize_novideo : ob.filesize)
        {
            beatmap = ob;
        }

        public OsuDirectDownload(int setId, string filename, string title = null, bool noVideo = false, int filesize = 0)
        {
            this.filename = filename;
            this.filesize = filesize;
            this.title = title;

            if (!Directory.Exists(DOWNLOADS_FOLDER))
                Directory.CreateDirectory(DOWNLOADS_FOLDER);

            localFilename = Path.GetFileName(filename);
            string destination = Path.Combine(DOWNLOADS_FOLDER, localFilename);

            try
            {
                File.Delete(destination);
            }
            catch
            {
                return;
            }

            request = new FileNetRequest(destination, string.Format(General.WEB_ROOT + "/d/{0}{1}?u={2}&h={3}", setId, (noVideo ? "n" : ""), ConfigManager.sUsername, ConfigManager.sPassword));
            request.onStart += req_onStart;
            request.onFinish += req_onFinish;
            request.onUpdate += req_onUpdate;
        }

        protected OsuDirectDownload()
        {
        }

        #region IComparer<OsuDirectDownload> Members

        public int Compare(OsuDirectDownload x, OsuDirectDownload y)
        {
            return x.filename.CompareTo(y.filename);
        }

        #endregion

        internal bool DrawAt(Vector2 pos)
        {
            if (SpriteCollection == null)
            {
                SpriteCollection = new List<pSprite>();

                s_background = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft,
                                           Clocks.Game,
                                           new Vector2(pos.X, pos.Y), 0.79f, true,
                                           new Color(40, 40, 40, 150));
                s_background.Scale = 1.6f;
                s_background.HandleInput = true;
                s_background.HoverEffect = new Transformation(s_background.InitialColour, new Color(60, 60, 60, 180), 0,
                                                              10);
                s_background.OnClick += delegate { Abort(true); };
                s_background.VectorScale = new Vector2(WIDTH, HEIGHT);

                s_progressBarBg = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft,
                                              Clocks.Game,
                                              new Vector2(pos.X + PADDING, pos.Y + 10), 0.8f, true,
                                              new Color(0, 0, 0, 150));
                s_progressBarBg.Scale = 1.6f;
                s_progressBarBg.HoverEffect = new Transformation(TransformationType.Fade, 0.4f, 0.7f, 0, 600);
                s_progressBarBg.VectorScale = new Vector2(PROGRESS_BAR_WIDTH, 5);

                s_progressBar = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft,
                                            Clocks.Game,
                                            new Vector2(pos.X + PADDING, pos.Y + 10), 0.81f, true,
                                            new Color(0, 78, 192, 255));
                s_progressBar.Scale = 1.6f;
                s_progressBar.HoverEffect = new Transformation(TransformationType.Fade, 0.4f, 0.7f, 0, 600);
                s_progressBar.VectorScale = new Vector2(0, 5);

                s_progressText = new pText(Status == DownloadStatus.Waiting ? "waiting..." : "initialising", 9, new Vector2(pos.X + PADDING, pos.Y + 8), 0.82f, true,
                                           Color.White);

                s_title = new pText(title, 9, new Vector2(pos.X + PADDING, pos.Y), 0.81f, true, Color.White);

                SpriteCollection.Add(s_background);
                SpriteCollection.Add(s_progressBarBg);
                SpriteCollection.Add(s_progressBar);
                SpriteCollection.Add(s_progressText);
                SpriteCollection.Add(s_title);

                OsuDirect.UpdatePending = true;

                return true;
            }

            Vector2 orig = SpriteCollection[0].InitialPosition;

            foreach (pSprite p in SpriteCollection)
            {
                Vector2 destination = pos + (p.InitialPosition - orig);

                if (destination == p.Position && p.Transformations.Count == 0) continue;

                p.MoveTo(destination, 500, EasingTypes.Out);
                p.InitialPosition = destination;
            }

            return false;
        }

        private void req_onUpdate(object sender, long current, long total)
        {
            if (Status == DownloadStatus.Completed)
                return;

            if (Status == DownloadStatus.Starting)
                Status = DownloadStatus.Downloading;

            long size = Math.Max(filesize, total);

            double prog = size == 0 ? 0 : (double)current / size;

            s_progressText.Text = string.Format("{0:0.0%}", prog);

            if (SpriteCollection != null)
                s_progressBar.VectorScale.X = ((float)prog * PROGRESS_BAR_WIDTH);

            s_progressText.Position =
                new Vector2(
                    s_progressBar.InitialPosition.X + Math.Min(s_progressBar.VectorScale.X, PROGRESS_BAR_WIDTH - 22),
                    s_progressBar.Position.Y - 2);
            s_progressText.InitialPosition = s_progressText.Position;
            onUpdate(prog);
        }

        private void req_onFinish(string _fileLocation, Exception e)
        {
            try
            {
                File.Delete(BeatmapManager.SONGS_DIRECTORY + localFilename);
            }
            catch
            {
                NotificationManager.ShowMessage("osu!Direct download failed because the file already exists in your songs folder and is not writeable.");
            }

            bool succeeded = e == null;

            try
            {

                if (succeeded && File.Exists(_fileLocation))
                {
                    succeeded &= new FileInfo(_fileLocation).Length > 100;

                    if (!succeeded)
                    {
                        string text = File.ReadAllText(_fileLocation);
                        File.Delete(_fileLocation);

                        if (text.StartsWith(@"ERROR:"))
                        {
                            switch (text.Replace(@"ERROR:", @"").Trim())
                            {
                                case @"DOWNLOAD_NOT_AVAILABLE":
                                    NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.OsuDirect_DownloadNotAvailable), Color.DarkRed, 4000, delegate
                                    {
                                        GameBase.ProcessStart(General.WEB_ROOT + @"/d/" + beatmap.setId);
                                    });
                                    break;
                            }

                            return;
                        }
                    }

                    string ext = Path.GetExtension(filename).ToLower();

                    string songs_fullpath = Path.GetFullPath(BeatmapManager.SONGS_DIRECTORY);
                    int charsOver = songs_fullpath.Length + filename.Length - GeneralHelper.MAX_PATH_LENGTH;

                    if (charsOver > 0)
                    {
                        if (charsOver < filename.Length - (ext.Length + 1) - 1)
                        {
                            filename = filename.Substring(0, filename.Length - charsOver - (ext.Length + 1)) + ext;
                        }
                        else
                        {
                            succeeded = false;
                        }

                    }

                    if (succeeded)
                    {
                        if (!Player.Playing || ConfigManager.sPopupDuringGameplay.Value)
                            AudioEngine.PlaySample(@"match-confirm");
                        File.Move(_fileLocation, BeatmapManager.SONGS_DIRECTORY + localFilename);
                    }
                    else
                    {
                        NotificationManager.ShowMessage("osu!direct download failed. Please check your connection and try again!");
                        File.Delete(_fileLocation);
                    }
                }

            }
            finally
            {
                Status = DownloadStatus.Completed;
                OsuDirect.UpdatePending = true;
                onFinish(succeeded);
            }
        }

        private void req_onStart()
        {
            onStart();
        }

        internal virtual void Abort(bool promptForConfirmation)
        {
            if (promptForConfirmation)
            {
                pDialog dialog =
                    new pDialog("Would you like to cancel the download?", true);
                dialog.AddOption("Yes, cancel this download.", Color.OrangeRed, delegate { Abort(false); });
                dialog.AddOption("No, keep downloading.", Color.YellowGreen, delegate { });
                GameBase.ShowDialog(dialog);
                return;
            }
            if (request != null)
                request.Abort();

            Status = DownloadStatus.Completed;

        }


        public int CompareTo(OsuDirectDownload other)
        {
            return filename.CompareTo(other.filename);
        }

        bool started;

        internal void Start()
        {
            if (started) return;

            started = true;
            Status = DownloadStatus.Starting;

            if (s_progressText != null) s_progressText.Text = "initialising";

            if (request != null) NetManager.AddRequest(request);
        }
    }
}
