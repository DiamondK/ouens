﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.Graphics.Notifications;
using osu_common.Libraries.NetLib;

namespace osu.Online.Social
{
    internal static class ChatFilter
    {
        private static bool Initialized;
        private static bool Initializing;

        static string[] match;
        static int count;

        internal static string Filter(string text)
        {
            if (!Initialized)
                Initialize();

            if (Initializing)
                return "Chat filter is updating...";

            for (int i = 0; i < count; i++)
            {
                int idx = text.IndexOf(match[i], StringComparison.OrdinalIgnoreCase);

                while (idx >= 0 && match[i].Length > 0)
                {
                    text = text.Replace(text.Substring(idx, match[i].Length), "*beep*");
                    idx = text.IndexOf(match[i], StringComparison.OrdinalIgnoreCase);
                }
            }

            return text;
        }

        internal static void Initialize()
        {
            if (Initializing) return;
            Initializing = true;

            if (!File.Exists("Data/filter.txt") || (DateTime.Now - File.GetLastWriteTime("Data/filter.txt")).TotalDays > 7)
            {
                FileNetRequest fnr = new FileNetRequest("Data/filter.txt",General.WEB_ROOT + "/release/filter.txt");
                fnr.onFinish += delegate { LoadData(); };
                NetManager.AddRequest(fnr);
                return;
            }

            LoadData();
        }

        private static void LoadData()
        {
            if (!File.Exists("Data/filter.txt"))
            {
                ConfigManager.sChatFilter.Value = false;
                NotificationManager.ShowMessage("Failed to download chat filter data.  Disabling filter.",Color.Red, 5000);
            }

            match = File.ReadAllLines("Data/filter.txt");
            count = match.Length;

            Initialized = true;
            Initializing = false;
        }
    }
}
