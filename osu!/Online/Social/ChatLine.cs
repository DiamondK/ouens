﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Online;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu_common.Helpers;
using osu.Graphics.Sprites;

namespace osu.Online.Social
{
    class ChatLine
    {
        private pText messageSprite;
        private pText nameSprite;

        internal readonly List<pSprite> Sprites = new List<pSprite>();

        private Color userFlashColour = new Color(104, 137, 255);
        private Color initialColour;

        private float fontSize;

        internal float ExtentY
        {
            get
            {
                float extent = (float)Math.Max(messageSprite.MeasureText().Y, nameSprite.MeasureText().Y);
                extent -= extent % fontSize;
                return extent;
            }
        }

        internal Vector2 Position
        {
            get
            {
                return nameSprite.InitialPosition;
            }

            set
            {
                foreach (pSprite p in Sprites)
                {
                    if (p == nameSprite)
                    {
                        continue;
                    }

                    p.InitialPosition += value - nameSprite.InitialPosition;
                    p.Position = p.InitialPosition;
                }

                nameSprite.InitialPosition = value;
                nameSprite.Position = value;
            }
        }

        public ChatLine(Vector2 position, float drawDepth, float fontSize, float lineWidth, Message msg, string fontFace)
        {
            this.fontSize = fontSize;

            nameSprite = new pText(string.Empty, fontSize, position, new Vector2(lineWidth, 0), drawDepth, true, Color.White);
            messageSprite = new pText(string.Empty, fontSize, position, new Vector2(lineWidth, 0), drawDepth, true, Color.White);

            nameSprite.FontFace = fontFace;
            messageSprite.FontFace = fontFace;

            bool isMe = msg.Name.Length > 8 && msg.Name[7] == '*' && msg.Content.Length == 0;

            initialColour = msg.Colour;

            messageSprite.Text = msg.Content;
            nameSprite.Text = msg.Name;
            nameSprite.InitialColour = initialColour;

            nameSprite.HandleInput = !isMe && msg.User != null;

            nameSprite.Tag = msg.User;

            nameSprite.OnHover += delegate(object sender, EventArgs args) { ((pSprite)sender).FadeColour(userFlashColour, 100); };
            nameSprite.OnHoverLost += delegate(object sender, EventArgs args) { ((pSprite)sender).FadeColour(initialColour, 700); };
            nameSprite.OnClick += ChatEngine.user_OnClick;
            nameSprite.ClickRequiresConfirmation = true;

            float offsetX = Math.Max(50, nameSprite.MeasureText().X);

            messageSprite.Position.X += offsetX;
            messageSprite.InitialPosition.X += offsetX;
            messageSprite.TextBounds.X -= offsetX;

            pText checkSprite = isMe ? nameSprite : messageSprite;

            if (msg.Colour.A != 0)
                Sprites.Add(nameSprite);

            Sprites.Add(messageSprite);

            try
            {
                if (checkSprite.Text.Length > 0 && msg.FormatterResults != null && msg.FormatterResults.Links.Count > 0)
                {
                    System.Drawing.RectangleF[] characterRegions = checkSprite.MeasureCharacters();
                    List<int> lineBreaks = checkSprite.ComputeLineBreaks(characterRegions);

                    foreach (Link l in msg.FormatterResults.Links)
                    {
                        if (l.Url.IndexOf('\uD83D') == 0 && l.Url.Length == 2)
                        {
                            //emoji
                            float thisOffsetX = (int)(offsetX + checkSprite.MeasureText(0, l.Index).X - (l.Index == 0 ? 4 : 1));
                            string temp = ((int)l.Url[1]).ToString();
                            pSprite p = new pSprite(SkinManager.Load("emoji-" + temp), Fields.TopLeft, Origins.TopLeft, Clocks.Game, nameSprite.Position + new Vector2(thisOffsetX, 0), 0.975f, true, Color.White);

                            Sprites.Add(p);
                        }
                        else
                        {
                            // First smaller or equal element. I.E. linebreak immediately before the link starts.
                            int start = lineBreaks.BinarySearch(l.Index);
                            if (start < 0)
                            {
                                start = ~start - 1;
                            }

                            List<pSprite> linkSprites = new List<pSprite>();

                            int pos = l.Index;
                            while (pos < l.Index + l.Length)
                            {
                                int nextLineBreak = start >= lineBreaks.Count - 1 ? checkSprite.Text.Length : (lineBreaks[start + 1]);
                                pos = Math.Min(nextLineBreak, l.Index + l.Length);

                                int beginIndex = Math.Max(lineBreaks[start], l.Index);

                                // Go backwards until we find the first character that actually has bounds. Reasoning is, that whitespaces at the end of the line can have 0,0,0,0 as rect.
                                int end = pos-1;
                                while (end > beginIndex && characterRegions[end].Width == 0 && characterRegions[end].Height == 0 && characterRegions[end].X == 0 && characterRegions[end].Y == 0)
                                {
                                    --end;
                                }

                                offsetX = characterRegions[beginIndex].X - 1;
                                System.Drawing.RectangleF endRect = characterRegions[end];
                                float sizeX = endRect.X + endRect.Width - offsetX + 1;

                                linkSprites.Add(AddLinkSprite(l, linkSprites, new Vector2(offsetX, fontSize * start + 1f), new Vector2(sizeX, fontSize)));

                                ++start;
                            }
                        }
                    }
                }
            }
            catch { }
        }

        private pSprite AddLinkSprite(Link l, List<pSprite> linkSprites, Vector2 offset, Vector2 size)
        {
            pSprite linkSprite = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, messageSprite.Position + offset);
            linkSprite.InitialPosition = messageSprite.InitialPosition + offset;
            linkSprite.Depth = messageSprite.Depth - 0.001f;
            linkSprite.AlwaysDraw = true;
            linkSprite.Tag = @"link";
            linkSprite.InitialColour = new Color(39, 70, 120, 255);

            linkSprite.Scale = 1.6f;
            linkSprite.VectorScale = size;

            linkSprite.HandleInput = true;

            linkSprite.OnHover += delegate
            {
                foreach (pSprite s in linkSprites)
                    s.FadeColour(Color.LightBlue, 100);
            };

            linkSprite.OnHoverLost += delegate
            {
                foreach (pSprite s in linkSprites)
                    s.FadeColour(new Color(39, 70, 120, 255), 100);
            };

            linkSprite.OnClick += delegate { ChatEngine.HandleLink(l.Url); };

            linkSprite.ToolTip = @"link: " + l.Url;

            linkSprite.ClickRequiresConfirmation = true;
            Sprites.Add(linkSprite);

            return linkSprite;
        }


        internal void Dispose()
        {
            foreach (pSprite s in Sprites)
            {
                s.Dispose();
            }
        }

    }
}
