﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Graphics.Notifications;
using osu.Input;
using osu.Input.Handlers;
using osu.Graphics;
using osu_common.Helpers;
using osu.Helpers;

namespace osu.Online.Social
{
    class ChannelListDialog : pDialog
    {
        private static int elementOffsetx = 65;
        private pTextBox searchInput;

        const int spacing = 33;

        private Color cChanJoined = new Color(154, 205, 50);
        private Color cChanPart = new Color(255, 165, 0);

        private readonly pScrollableArea channelButtonList;

        internal ChannelListDialog()
            : base(LocalisationManager.GetString(OsuString.ChannelListDialog_SelectAnyChannelYouWishToJoin), true)
        {
            channelButtonList = new pScrollableArea(new Rectangle(0, 60, 550, 321), Vector2.Zero, false);
            currentVerticalSpace = 400;
            AddOption(LocalisationManager.GetString(OsuString.General_Close), SkinManager.NEW_SKIN_COLOUR_MAIN, null, true);
        }

        public override void Update()
        {
            channelButtonList.Update();
            base.Update();
        }

        internal override void Display()
        {
            pText searchLabel = new pText(LocalisationManager.GetString(OsuString.Lobby_Search), 15, new Vector2(elementOffsetx + 30, 35), 0.95f, true, Color.White);
            spriteManager.Add(searchLabel);

            searchInput = new pTextBox(string.Empty, 15, new Vector2(elementOffsetx + 80, 35), 100, 0.94f);
            searchInput.CommitOnLeave = false;
            searchInput.OnChange += new pTextBox.OnCommitHandler(searchInput_OnChange);
            searchInput.OnCommit += new pTextBox.OnCommitHandler(searchInput_OnCommit);
            spriteManager.Add(searchInput.SpriteCollection);

            spawnChannelListing();
            searchInput.Focus(true);
            base.Display();
        }

        void searchInput_OnCommit(pTextBox sender, bool newText)
        {
            if (searchInput.Text.Length <= 0 || !newText) return;

            searchInput.Focus(true);
        }

        Dictionary<Channel, List<pSprite>> channelSprites = new Dictionary<Channel, List<pSprite>>();
        private List<Channel> channels;

        void spawnChannelListing()
        {
            Vector2 pos = new Vector2(elementOffsetx + 30, 0);
            channels = new List<Channel>(ChatEngine.channels);

            channels.Sort(
                delegate(Channel c1, Channel c2)
                {
                    return c1.Name.CompareTo(c2.Name);
                });

            foreach (Channel c in channels)
            {
                if (!c.Name.StartsWith(@"#")) continue;

                string description = string.Format(LocalisationManager.GetString(OsuString.ChannelListDialog_TopicAndUserCount), string.IsNullOrEmpty(c.Topic) ? LocalisationManager.GetString(OsuString.ChannelListDialog_NoDescriptionAvailable) : c.Topic, c.UserCount);
                pText pb = new pText(string.Empty, 12, pos, 0.95f, true, c.Joined ? SkinManager.NEW_SKIN_COLOUR_MAIN : SkinManager.NEW_SKIN_COLOUR_SECONDARY);

                pb.HandleInput = true;
                pb.Tag = c.Name;
                pb.CornerBounds = Vector4.One * 4;
                pb.ClickRequiresConfirmation = true;
                pb.TextBounds = new Vector2(445, 30);
                pb.BackgroundColour = Color.Black;
                pb.BorderWidth = 2;
                pb.BorderColour = Color.White;
                pb.TextAlignment = TextAlignment.LeftFixed;

                Vector2 posName = pos;
                posName.Y += 8;
                posName.X += 10;

                pText pname = new pText(c.Name, 12, posName, 0.99f, true, c.Joined ? SkinManager.NEW_SKIN_COLOUR_MAIN : SkinManager.NEW_SKIN_COLOUR_SECONDARY);
                pname.HandleInput = false;

                pText pdesc = new pText(description, 12, new Vector2(pos.X + 100, pos.Y + 8), 0.99f, true, Color.White);
                pdesc.TextBounds = new Vector2(345, 30);
                pdesc.TextAlignment = TextAlignment.LeftFixed;
                pdesc.HandleInput = false;

                pb.OnClick += delegate { pb_OnClick(pb, pname); };

                pb.OnHover += delegate
                {
                    if (!pb.IsVisible || pb.Alpha != 1) return;

                    pb.BackgroundColour = ColourHelper.Darken(pb.BorderColour, 4f);

                    pname.TextBold = true;
                    pname.TextChanged = true;
                    pdesc.TextChanged = true;
                };

                pb.OnHoverLost += delegate
                {
                    if (!pb.IsVisible || pb.Alpha != 1) return;

                    pb.BackgroundColour = Color.Black;

                    pname.TextBold = false;
                    pname.TextChanged = true;
                    pdesc.TextChanged = true;
                };

                pos.Y += spacing;
                channelButtonList.SpriteManager.Add(pb);
                channelButtonList.SpriteManager.Add(pname);
                channelButtonList.SpriteManager.Add(pdesc);

                channelSprites[c] = new List<pSprite>() { pb, pname, pdesc };
            }

            channelButtonList.SetContentDimensions(new Vector2(160, pos.Y));
        }

        void searchInput_OnChange(pTextBox sender, bool newText)
        {
            bool showAll = searchInput.Box.Text.Length == 0;

            if (newText)
            {
                float pos = 0;
                foreach (Channel c in channels)
                {
                    bool show = showAll || c.Name.Contains(searchInput.Text);

                    if (!c.Name.Contains("#")) continue;

                    channelSprites[c].ForEach(s =>
                        {
                            clearSpriteEffects(s);
                            if (show)
                            {
                                s.FadeIn(300);
                                s.MoveToY(s.HandleInput ? pos : pos + 8, 300, EasingTypes.Out);
                            }
                            else
                                s.FadeOut(300);
                        });

                    if (show) pos += spacing;
                }

                channelButtonList.SetContentDimensions(new Vector2(160, pos));
            }
        }

        internal override void Draw()
        {
            base.Draw();
            channelButtonList.Draw();
        }

        void clearSpriteEffects(pSprite s)
        {
            pText sprite = s as pText;
            sprite.TextBold = false;
        }

        void pb_OnClick(pText p, pText pname)
        {
            string attemptJoin = p.Tag.ToString();

            Channel c = ChatEngine.channels.Find(s => s.Name == attemptJoin);

            if (c == null)
            {
                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.ChannelListDialog_CantRejoinThisChannel), 1500);
                return;
            }

            if (InputManager.rightButtonLast == ButtonState.Pressed)
            {
                if (!c.Joined || !c.Closeable)
                    return;
                ChatEngine.RemoveChannel(c);
                NotificationManager.ShowMessageMassive(string.Format(LocalisationManager.GetString(OsuString.ChannelListDialog_Left), c.Name), 1500);
                p.FadeColour(SkinManager.NEW_SKIN_COLOUR_SECONDARY, 300);
                pname.FadeColour(SkinManager.NEW_SKIN_COLOUR_SECONDARY, 300);
            }
            else
            {
                if (!c.Joined)
                {
                    Channel chan = null;

                    try
                    {
                        chan = ChatEngine.AddChannel(attemptJoin, false, true);
                        if (chan == null)
                        {
                            NotificationManager.ShowMessageMassive(string.Format(LocalisationManager.GetString(OsuString.ChannelListDialog_JoiningTooFast), attemptJoin), 1500);
                            return;
                        }

                        ChatEngine.channelTabs.SetSelected(chan);
                        NotificationManager.ShowMessageMassive(string.Format(LocalisationManager.GetString(OsuString.ChannelListDialog_Joined), chan.Name), 1500);
                        p.FadeColour(SkinManager.NEW_SKIN_COLOUR_MAIN, 300);
                        pname.FadeColour(SkinManager.NEW_SKIN_COLOUR_MAIN, 300);
                    }
                    catch
                    {

                    }
                }
                else
                {
                    NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.ChannelListDialog_AlreadyJoined), 1500);
                }
            }

            Update();
        }

        protected override void Dispose(bool disposing)
        {
            channelButtonList.Dispose();
            base.Dispose(disposing);
        }
    }
}