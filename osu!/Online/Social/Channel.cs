using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu_common.Helpers;
using System.IO;
using System;

namespace osu.Online.Social
{
    internal class Channel : bChannel
    {
        internal List<Message> messageBuffer = new List<Message>();
        internal string messageToSend = "";

        internal bool Joined;
        public bool Closeable;
        public bool Removeable;
        int JoinTime;

        internal Channel(string name, bool closeable)
        {
            Name = name;
            Closeable = closeable;
        }

        public Channel(SerializationReader sr)
            : base(sr)
        {
        }

        internal bool Joining = false;

        bool virtualChannel;
        public bool VirtualChannel
        {
            get { return virtualChannel; }
            set
            {
                virtualChannel = value;
                if (value) Joined = true;
            }
        }

        public bool Join(bool allowQuickRejoin = false)
        {
            allowQuickRejoin |= Name == "#lobby";

            if (Joined) return true;

            if (Joining) return false;

            if (!allowQuickRejoin && JoinTime > 0 && GameBase.Time - JoinTime < 10000)
                return false;

            JoinTime = GameBase.Time;

            BanchoClient.SendRequest(RequestType.Osu_ChannelJoin, new bString(Name));
            messageBuffer.Add(new Message("Attempting to join channel...", "", Color.LightCoral));
            Joining = true;
            return true;
        }

        public void Leave()
        {
            if (Joined)
            {
                BanchoClient.SendRequest(RequestType.Osu_ChannelLeave, new bString(Name));
                Joined = false;
                Joining = false;
                messageToSend = "";
            }
        }

        internal void HandleJoinSuccess()
        {
            if (Joined) return;
            Joined = true;
            Joining = false;
            messageToSend = "";
            messageBuffer.Add(new Message("Joined " + Name + "!", "", Color.YellowGreen));
        }

        internal void Reset()
        {
            if (virtualChannel) return;

            Joined = false;
            Joining = false;
            messageToSend = "";
        }

    }
}