#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Edit;
using osu.GameModes.Online;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Graphics.Renderers;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Helpers;
using osu.Input;
using osu.Input.Handlers;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu_common.Helpers;
using osu.GameModes.Select;
using osu.Input.Drawable;
using osu.GameModes.Play.Rulesets.Mania;
using osu_common.Bancho;
using osu.Graphics.Primitives;
using System.Diagnostics;

#endregion

namespace osu.Online.Social
{
    internal class ChatEngine : GameComponent
    {
        internal static readonly int ImeCheckDelay = 80;
        internal static Channel activeChannel;
        internal static List<Message> activeMessageBuffer;
        private static pSprite bHideBottom;
        private static pSprite bHideMode;
        private static pSprite bTicker;
        internal static List<Channel> channels = new List<Channel>();
        internal static pChannelTabCollection channelTabs;
        internal static pCheckbox cTestBancho;
        private static UserSortMode CurrentSortMode;
        private static int displayWindow;
        private static List<string> friendAlertOnlineQueue = new List<string>();
        private static List<string> friendAlertOfflineQueue = new List<string>();
        private static int friendAlertLastOnline;
        private static int friendAlertLastOffline;
        internal static List<int> Friends = new List<int>();
        private static int historyCurrent;
        internal static bool IsFullVisible;
        internal static bool IsVisible;
        private static int lastToggleTime;

        internal static string FontFace;

        private static pScrollableArea chatArea;
        private static float ChatAreaPositionY
        {
            get
            {
                return chatBg.Position.Y + 14;
            }
        }

        private static RectangleF ChatAreaDisplayRect
        {
            get
            {
                return new RectangleF(2, ChatAreaPositionY, GameBase.WindowWidthScaled - 2, CHATAREA_HEIGHT - 29);
            }
        }


        // Always leave this >= 100. Otherwise breakage occurs.
        private const int FADE_DURATION = 400;


        private static bool isScrollVisible;
        internal static bool IsScrollVisible
        {
            get { return false; }
        }

        private static int lineCount;
        private static string lastPrivateUser;
        private static Message lastTickerMessage;
        private static bool noMessageCleared;
        private static string nowPlayingLast;
        internal static List<string> ReplayNames = new List<string>();
        internal static List<string> sendBuffer = new List<string>();
        internal static pText sendLine;
        internal static SpriteManager spriteManager;
        internal static pText tickerText;
        internal static pScrollableArea usersArea;
        private static pSprite bToggleOnline;
        private static pSprite chatBg;
        internal static pSprite cursor;
        private readonly pText headerText;
        private readonly pSprite selection;
        private readonly pTabCollection sortingTabs;
        private readonly Vector2 TickerPositionEdit = new Vector2(0, 447f);
        private readonly Vector2 TickerPositionNormal = new Vector2(0, 467f);
        private static pSprite userList;
        private bool ForceHide;
        private string lastTextboxText;
        private int displayedCount;
        private int textboxTextOffset;
        private Comparison<User> UserSortDelegate;
        private int selectionStart;
        private int selectionStartLast;
        private static readonly List<string> HighlightList = new List<string>();
        private static int nowPlayingLastTime;
        private readonly pCheckbox cLockPanels;
        private static bool scrollLockingPermanent;
        private static UserMap userMap;
        private static SpriteManager spriteManagerAboveUsersArea;
        private static List<pSprite> bottomSprites = new List<pSprite>();
        private static readonly List<string> ignoreListChat = new List<string>();
        private static readonly List<string> ignoreListHighlight = new List<string>();
        private static readonly List<string> ignoreListPrivate = new List<string>();
        internal static readonly int CHATAREA_HEIGHT = GameBase.Tournament ? 96 : 160;
        internal const int FONT_SIZE = 12;
        private const int CHANNEL_BUFFER_SIZE = 500;
        private static InputCandidate Candidates;
        private pDropdown userFilterDropdown;
        private static pSprite emojiButton;
        private static List<pSprite> emojis = new List<pSprite>();
        private static List<pSprite> sendlineEmojis = new List<pSprite>();
        private static int[] emojiList = { 56397, 56398, 56444, 56832, 56835, 56838, 56840,
                                             56842, 56845, 56846, 56847, 56849, 56854, 56855,
                                             56859, 56864, 56865, 56869, 56876, 56877, 56878, 56879 };
        private static int emojiWidth = 2;

        /// <summary>
        /// Tried to switch active channel to the last active one before closing osu! (ChatLastChannel in config file).
        /// </summary>
        internal static bool hasActivatedLastActiveChannel;

        internal static bool Initialized = false;


        internal ChatEngine(Game game)
            : base(game)
        {
            userMap = new UserMap(game);

            spriteManager = GameBase.spriteManagerOverlay;
            spriteManager.Add(InputManager.TextBoxOmniscient.ImeLine);

            usersArea = new pScrollableArea(new Rectangle(10, 60, GameBase.WindowWidthScaled - 15, 259), new Vector2(630, 600), true, 0, InputTargetType.Chat);
            usersArea.FinishedScrolling += new VoidDelegate(usersArea_FinishedScrolling);
            usersArea.SpriteManager.FirstDraw = false;
            usersArea.SpriteManager.HandleOverlayInput = true;
            usersArea.Hide();

            spriteManagerAboveUsersArea = new SpriteManager(true) { HandleOverlayInput = true };

            sortingTabs = new pTabCollection(GameBase.spriteManagerOverlay, 7, new Vector2(50, 60), 0.98f, false,
                                             Color.Crimson);
            sortingTabs.Add(LocalisationManager.GetString(OsuString.ChatEngine_SortName), (int)UserSortMode.Name);
            sortingTabs.Add(LocalisationManager.GetString(OsuString.ChatEngine_SortRank), (int)UserSortMode.Rank);
            //sortingTabs.Add("Status", (int)UserSortMode.Status);
            //sortingTabs.Add("Accuracy", (int)UserSortMode.Accuracy);
            //sortingTabs.Add("Playcount", (int)UserSortMode.PlayCount);
            //sortingTabs.Add("Level", (int)UserSortMode.Level);
            sortingTabs.Add(LocalisationManager.GetString(OsuString.ChatEngine_SortLocation), (int)UserSortMode.Country);
            sortingTabs.Add(LocalisationManager.GetString(OsuString.ChatEngine_SortTimeZone), (int)UserSortMode.Timezone);
            sortingTabs.Add(LocalisationManager.GetString(OsuString.ChatEngine_SortWorldMap), (int)UserSortMode.WorldMap);
            sortingTabs.OnTabChanged += sortingTabs_OnTabChanged;

            pText pt = new pText(LocalisationManager.GetString(OsuString.ChatEngine_Filter), 12, new Vector2(220, 5), 0.97F, true, Color.White);
            pt.Origin = Origins.TopRight;
            pt.Tag = @"user";
            pt.Alpha = 0;
            spriteManager.Add(pt);

            userFilterDropdown = new pDropdown(spriteManagerAboveUsersArea, string.Empty, new Vector2(220, 5), 98, 0.995F);
            userFilterDropdown.AddOption(LocalisationManager.GetString(OsuString.User_Filter_All), UserFilterType.All, (int)UserFilterType.All, Color.Black);
            userFilterDropdown.AddOption(LocalisationManager.GetString(OsuString.User_Filter_Friends), UserFilterType.Friends, (int)UserFilterType.Friends, Color.Black);
            userFilterDropdown.AddOption(LocalisationManager.GetString(OsuString.User_Filter_Near), UserFilterType.Near, (int)UserFilterType.Near, Color.Black);
            userFilterDropdown.AddOption(LocalisationManager.GetString(OsuString.User_Filter_Country), UserFilterType.Country, (int)UserFilterType.Country, Color.Black);
            userFilterDropdown.SetSelected(ConfigManager.sUserFilter.Value, true);
            userFilterDropdown.OnSelect += pdd_OnSelect;

            cLockPanels = new pCheckbox(LocalisationManager.GetString(OsuString.ChatEngine_LockPanels), 0.8f, new Vector2(330, 5), 0.98f, false);
            cLockPanels.OnCheckChanged += cLockPanels_OnCheckChanged;
            spriteManager.Add(cLockPanels.SpriteCollection);

            foreach (pSprite p in cLockPanels.SpriteCollection)
            {
                p.Alpha = 0;
                p.ToolTip = LocalisationManager.GetString(OsuString.ChatEngine_LockPanels_Tooltip);
                p.Tag = @"user";
            }


            pt = new pText(LocalisationManager.GetString(OsuString.SongSelection_Search), 12, new Vector2(220, 25), 0.97F, true, Color.White);
            pt.Origin = Origins.TopRight;
            pt.Tag = @"user";
            pt.Alpha = 0;
            spriteManager.Add(pt);

            searchFilter = new pTextBox(string.Empty, 12, new Vector2(220, 25), 100, 0.98f);
            searchFilter.LengthLimit = 14;
            searchFilter.OnChange += delegate { ResetSortTime(); };
            searchFilter.SpriteCollection.ForEach(p =>
            {
                p.Tag = @"user";
                p.Alpha = 0;
            });
            spriteManager.Add(searchFilter.SpriteCollection);

            modeFilterOsu = new pSprite(SkinManager.Load(@"mode-osu-small", SkinSource.Osu), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(340, 32)) { AlwaysDraw = true };
            modeFilterOsu.Alpha = 0;
            modeFilterOsu.HandleInput = true;
            modeFilterOsu.OnHover += modeSprite_OnHover;
            modeFilterOsu.OnClick += modeSprite_OnClick;
            modeFilterOsu.Tag = @"user";
            modeFilterOsu.TagNumeric = (int)PlayModes.Osu;

            modeFilterTaiko = new pSprite(SkinManager.Load(@"mode-taiko-small", SkinSource.Osu), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(370, 32)) { AlwaysDraw = true };
            modeFilterTaiko.Alpha = 0;
            modeFilterTaiko.HandleInput = true;
            modeFilterTaiko.OnHover += modeSprite_OnHover;
            modeFilterTaiko.OnClick += modeSprite_OnClick;
            modeFilterTaiko.Tag = @"user";
            modeFilterTaiko.TagNumeric = (int)PlayModes.Taiko;

            modeFilterFruits = new pSprite(SkinManager.Load(@"mode-fruits-small", SkinSource.Osu), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(400, 32)) { AlwaysDraw = true };
            modeFilterFruits.Alpha = 0;
            modeFilterFruits.OnHover += modeSprite_OnHover;
            modeFilterFruits.HandleInput = true;
            modeFilterFruits.OnClick += modeSprite_OnClick;
            modeFilterFruits.Tag = @"user";
            modeFilterFruits.TagNumeric = (int)PlayModes.CatchTheBeat;

            modeFilterMania = new pSprite(SkinManager.Load(@"mode-mania-small", SkinSource.Osu), Fields.TopLeft, Origins.Centre, Clocks.Game, new Vector2(430, 32)) { AlwaysDraw = true };
            modeFilterMania.Alpha = 0;
            modeFilterMania.OnHover += modeSprite_OnHover;
            modeFilterMania.HandleInput = true;
            modeFilterMania.OnClick += modeSprite_OnClick;
            modeFilterMania.Tag = @"user";
            modeFilterMania.TagNumeric = (int)PlayModes.OsuMania;

            spriteManager.Add(modeFilterOsu);
            spriteManager.Add(modeFilterTaiko);
            spriteManager.Add(modeFilterFruits);
            spriteManager.Add(modeFilterMania);



#if DEBUGz
            cTestBancho = new pCheckbox(@"Use Test Bancho", 1, new Vector2(400, 5), 0.98f,
                                        false);
            cTestBancho.OnCheckChanged += cTestBancho_OnCheckChanged;

            spriteManager.Add(cTestBancho.SpriteCollection);

            foreach (pSprite p in cTestBancho.SpriteCollection)
            {
                p.CurrentAlpha = 0;
                p.Tag = @"user";
                p.CatchUniversal = true;
            }
#endif

            if (null == sortingTabs.SetSelected((int)ConfigManager.sChatSortMode.Value))
                sortingTabs.SetSelected(1);

            foreach (pSprite p in sortingTabs.Sprites)
            {
                p.Alpha = 0;
                p.Tag = @"user";
            }

            if (ConfigManager.sChatFilter)
                ChatFilter.Initialize();

            channelTabs = new pChannelTabCollection(spriteManager, 8, new Vector2(46, 482 - CHATAREA_HEIGHT + 12), 0.97f, false,
                                                    new Color(51, 71, 157));
            channelTabs.OnTabChanged += channelTabs_OnTabChanged;
            channelTabs.OnTabAddRequested += channelTabs_OnTabAddRequested;
            channelTabs.OnTabCloseRequested += channelTabs_OnTabCloseRequested;

            activeChannel = AddChannel(@"#osu", true, false);

            channelTabs.SetSelected(activeChannel);

            lineCount = GameBase.Tournament ? 6 : 11;



            int offset = 0;

#if EMOJI
            offset += 10;
            emojiButton = new pSprite(SkinManager.Load(@"emoji-56832", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(1, 466), 0.98f, true, Color.White);
            //emojiButton.VectorScale = new Vector2(12, 12) * 1.6f;
            emojiButton.CurrentScale = 1.2F;
            emojiButton.CurrentAlpha = 0;
            emojiButton.IsClickable = true;
            emojiButton.OnClick += emojiButton_OnClick;
            bottomSprites.Add(emojiButton);
            spriteManager.Add(emojiButton);

            pSprite bg = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.BottomLeft, Clocks.Game, new Vector2(2, 466), 0.98f, true, Color.DarkGray);
            bg.VectorScale = new Vector2(102, 70) * 1.6f;
            bg.Bypass = true;
            bottomSprites.Add(bg);
            emojis.Add(bg);
            spriteManager.Add(bg);

            int index = 0;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (index >= emojiList.Length)
                        break;
                    pSprite emoji = new pSprite(SkinManager.LoadDefault("emoji-"+emojiList[index]), Fields.TopLeft,
                        Origins.Centre, Clocks.Game, new Vector2(10 + j * 17, 405 + i * 17), 0.982f, true, Color.White);
                    emoji.TagNumeric = emojiList[index];
                    emoji.Bypass = true;
                    emoji.CurrentScale = 1.2f;
                    emoji.IsClickable = true;
                    emoji.OnClick += emoji_OnClick;
                    bottomSprites.Add(emoji);
                    emojis.Add(emoji);
                    spriteManager.Add(emoji);
                    index++;
                }
            }
#endif

            sendLine = new pText(string.Empty, 12, new Vector2(3 + offset, 465), 0.97F, true, Color.White);
            bottomSprites.Add(sendLine);
            spriteManager.Add(sendLine);
            sendLine.Alpha = 0;

            sendLine.Text = @"aa";
            float widthMin = sendLine.MeasureText().X;
            sendLine.Text = @"a\0a";
            float widthMax = sendLine.MeasureText().X;
            emojiWidth = (int)Math.Ceiling(10.0 / Math.Max(1, widthMax - widthMin));
            sendLine.Text = string.Empty;

            userList = new pSprite(GameBase.WhitePixel, new Vector2(0, 320), 0.961F, true, new Color(200, 200, 200, 50));
            bottomSprites.Add(userList);
            userList.VectorScale = new Vector2(GameBase.WindowWidth, 22);
            spriteManager.Add(userList);
            userList.Alpha = 0;

            UpdateFontFace();

            cursor = new pSprite(GameBase.WhitePixel, Fields.TopLeft,
                                 Origins.TopLeft, Clocks.Game, new Vector2(11 + offset, 466), 0.974F, true, Color.White);
            bottomSprites.Add(cursor);
            cursor.VectorScale = new Vector2(2, 18);
            cursor.Alpha = 0;
            spriteManager.Add(cursor);

            selection = new pSprite(GameBase.WhitePixel, Fields.TopLeft,
                                    Origins.TopLeft, Clocks.Game, new Vector2(0, 320), 0.974F, true,
                                    new Color(255, 255, 255, 100));
            bottomSprites.Add(selection);
            selection.VectorScale = new Vector2(0, 32);
            spriteManager.Add(selection);

            chatBg =
                new pSprite(GameBase.WhitePixel, Fields.TopLeft,
                            Origins.TopLeft, Clocks.Game, new Vector2(0, 480 - CHATAREA_HEIGHT), 0.96F, true,
                            new Color(0, 0, 0, 210));
            bottomSprites.Add(chatBg);
            chatBg.Alpha = 0;
            chatBg.HandleInput = true;
            chatBg.VectorScale = new Vector2(GameBase.WindowWidthScaled, CHATAREA_HEIGHT) * 1.6F;
            spriteManager.Add(chatBg);

            chatArea = new pScrollableArea(ChatAreaDisplayRect, Vector2.Zero, true, 0, InputTargetType.Chat);
            chatArea.SpriteManager.HandleOverlayInput = true;
            chatArea.ScrollMultiplier = 3 * FONT_SIZE / 100f;
            chatArea.DisplayRectangleExtension.Y = 1;

            detailsBack =
                new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft,
                            Clocks.Game, new Vector2(0, 0), 0.95F, true, new Color(0, 0, 0, 210), @"user");
            detailsBack.Alpha = 0;
            spriteManager.Add(detailsBack);
            detailsBack.TagNumeric = 12;
            detailsBack.HandleInput = true;
            detailsBack.VectorScale = new Vector2(GameBase.WindowWidthScaled, 320) * 1.6F;

            Transformation tr = new Transformation(Color.White, Color.Pink, 0, 100);

            bHideBottom =
                new pSprite(SkinManager.Load(@"overlay-show", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                            Clocks.Game,
                            new Vector2(30, 474), 0.98F, true, Color.White);
            //bHideBottom.CurrentAlpha = 0;
            bHideBottom.HoverEffect = tr;
            bHideBottom.TagNumeric = 14;
            bHideBottom.ToolTip = LocalisationManager.GetString(OsuString.ChatEngine_ToggleChat_Tooltip);
            bHideBottom.HandleInput = true;
            bHideBottom.OnHover += modeSprite_OnHover;
            bHideBottom.OnClick += hideBottom_OnClick;
            spriteManager.Add(bHideBottom);

            bToggleOnline =
                new pSprite(SkinManager.Load(@"overlay-online", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                            Clocks.Game,
                            new Vector2(90, 474), 0.98F, true, Color.White);
            //bToggleOnline.CurrentAlpha = 0;
            bToggleOnline.HoverEffect = tr;
            bToggleOnline.TagNumeric = 12;
            bToggleOnline.ToolTip = LocalisationManager.GetString(OsuString.ChatEngine_ToggleOnlineUsers_Tooltip);
            bToggleOnline.HandleInput = true;
            bToggleOnline.OnHover += modeSprite_OnHover;
            bToggleOnline.OnClick += toggleOnline_OnClick;
            spriteManager.Add(bToggleOnline);

            tr =
                new Transformation((ConfigManager.sTicker ? Color.White : Color.Gray),
                                   (ConfigManager.sTicker ? Color.White : Color.Pink), 0, 100);

            bTicker =
                new pSprite(
                    SkinManager.Load(ConfigManager.sTicker
                                                  ? @"overlay-ticker2"
                                                  : @"overlay-ticker", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                    Clocks.Game,
                    new Vector2(210, 474), 0.98F, true, (ConfigManager.sTicker ? Color.White : Color.Gray));
            bottomSprites.Add(bTicker);
            bTicker.Alpha = 0;
            bTicker.HoverEffect = tr;
            bTicker.ToolTip =
                LocalisationManager.GetString(OsuString.ChatEngine_Ticker_Tooltip);
            bTicker.TagNumeric = 12;
            bTicker.HandleInput = true;
            bTicker.OnHover += modeSprite_OnHover;
            bTicker.OnClick += ticker_OnClick;
            spriteManager.Add(bTicker);

            tr =
                new Transformation((ConfigManager.sAutoChatHide ? Color.White : Color.Gray),
                                   (ConfigManager.sAutoChatHide ? Color.White : Color.Pink), 0, 100);

            bHideMode =
                new pSprite(
                    SkinManager.Load(ConfigManager.sAutoChatHide ? @"overlay-hidemode2" : @"overlay-hidemode", SkinSource.Osu), Fields.TopRight, Origins.Centre,
                    Clocks.Game,
                    new Vector2(150, 474), 0.98F, true, (ConfigManager.sAutoChatHide ? Color.White : Color.Gray));
            bottomSprites.Add(bHideMode);
            bHideMode.Alpha = 0;
            bHideMode.HoverEffect = tr;
            bHideMode.TagNumeric = 12;
            bHideMode.ToolTip = LocalisationManager.GetString(OsuString.ChatEngine_AutoHide_Tooltip);
            bHideMode.HandleInput = true;
            bHideMode.OnHover += modeSprite_OnHover;
            bHideMode.OnClick += hideMode_OnClick;
            spriteManager.Add(bHideMode);

            foreach (pSprite s in bottomSprites)
                s.Position = s.InitialPosition + new Vector2(0, 80);

            headerText =
                new pText(@"osu!Bancho", 30, new Vector2(0, 0), 0.955F, true, new Color(255, 255, 255, 255));
            headerText.Tag = @"user";
            headerText.Alpha = 0;
            headerText.TagNumeric = 12;
            spriteManager.Add(headerText);

            headerText = new pText(LocalisationManager.GetString(OsuString.BanchoClient_Connecting), 16, new Vector2(4, 26), 0.955F, true, new Color(255, 255, 255, 255));
            headerText.Tag = @"user";
            headerText.Alpha = 0;
            headerText.TagNumeric = 12;
            spriteManager.Add(headerText);

            tickerText = new pText(string.Empty, 12, TickerPositionNormal, 0.975F, true, Color.White);
            tickerText.TextBounds = new Vector2(GameBase.WindowWidthScaled, 13.5f);
            tickerText.Tag = @"ticker";
            tickerText.TextAlignment = TextAlignment.LeftFixed;
            tickerText.BackgroundColour = Color.Black;
            tickerText.Alpha = 0;
            spriteManager.Add(tickerText);

            Candidates = new InputCandidate(spriteManager, new Vector2(2, 466), InputManager.TextBox);
            Candidates.Downward = false;

            OnResolutionChange();
            GameBase.OnResolutionChange += OnResolutionChange;

            Initialized = true;
        }

        void modeSprite_OnHover(object sender, EventArgs e)
        {
            AudioEngine.Click(null, @"click-short");
        }

        internal static void UpdateFontFace()
        {
            FontFace = ConfigManager.sAlternativeChatFont.Value ? General.FONT_FACE_ALTERNATIVE : General.FONT_FACE_DEFAULT;
            sendLine.FontFace = FontFace;

            ResetMessages();
        }

        void emoji_OnClick(object sender, EventArgs e)
        {
            pSprite p = (pSprite)sender;
            if (p.TagNumeric == 0)
                return;
            byte[] arr = new byte[4];
            arr[0] = 0x3d; //low
            arr[1] = 0xd8; //high
            arr[2] = (byte)(p.TagNumeric | 256);
            arr[3] = (byte)(p.TagNumeric >> 8);
            string emoji = Encoding.Unicode.GetString(arr);
            //sendLine.Text += emoji;
            InputManager.TextBox.Text += emoji;
            InputManager.TextBox.SelectionStart = InputManager.TextBox.Text.Length;
            emojis.ForEach(em =>
            {
                em.Bypass = true;
            });
        }

        void emojiButton_OnClick(object sender, EventArgs e)
        {
            emojis.ForEach(p =>
            {
                if (p.Bypass)
                    p.Bypass = false;
                else
                    p.Bypass = true;
            });
        }

        void pdd_OnSelect(object sender, EventArgs e)
        {
            ConfigManager.sUserFilter.Value = (UserFilterType)userFilterDropdown.SelectedNumeric;
            lastUpdateSortTime = 0; //force an instant update.
            ResetSortTime();
        }

        static bool[] playModeStatus = new bool[] { true, true, true, true };

        void modeSprite_OnClick(object sender, EventArgs e)
        {
            pSprite spr = sender as pSprite;
            PlayModes mode = (PlayModes)spr.TagNumeric;

            AudioEngine.Click(null, @"click-short-confirm");

            SetPlayModeFilter(mode, !playModeStatus[spr.TagNumeric]);
        }

        internal static void SetPlayModeFilter(PlayModes mode)
        {
            foreach (PlayModes m in Enum.GetValues(typeof(PlayModes)))
                SetPlayModeFilter(m, mode == m);
        }

        internal static void SetPlayModeFilter(PlayModes mode, bool status)
        {
            pSprite spr;

            switch (mode)
            {
                default:
                    spr = modeFilterOsu;
                    break;
                case PlayModes.Taiko:
                    spr = modeFilterTaiko;
                    break;
                case PlayModes.CatchTheBeat:
                    spr = modeFilterFruits;
                    break;
                case PlayModes.OsuMania:
                    spr = modeFilterMania;
                    break;
            }

            playModeStatus[(int)mode] = status;

            if (status)
                spr.FadeColour(new Color(255, 255, 255, 255), 100);
            else
                spr.FadeColour(new Color(128, 128, 128, 255), 100);

            lastUpdateSortTime = 0;
        }

        void usersArea_FinishedScrolling()
        {
            ResetSortTime();
        }

        private void channelTabs_OnTabAddRequested(object sender, EventArgs e)
        {
            GameBase.ShowDialog(new ChannelListDialog());
        }

        private void channelTabs_OnTabCloseRequested(object sender, EventArgs e)
        {
            Channel c = sender as Channel;
            if (c != null)
                RemoveChannel(c, true);
            if (channelTabs.Tabs.Count == 1)
            {
                ClearBuffer(activeMessageBuffer);
            }
        }

        internal static bool StreamMyReplay
        {
            get
            {
                return
                    BanchoClient.Connected && !InputManager.ReplayMode && ReplayNames.Count > 0 &&
                    GameBase.Mode == OsuModes.Play;
            }
        }

        private void cLockPanels_OnCheckChanged(object sender, bool status)
        {
            scrollLockingPermanent = status;
        }

        private void cTestBancho_OnCheckChanged(object sender, bool status)
        {
            BanchoClient.Reconnect();
        }

        internal static void DisposeBufferSprites(List<Message> buffer)
        {
            if (buffer == null)
                return;

            foreach (Message m in buffer)
            {
                if (m.HasChatLine)
                {
                    m.ChatLine.Dispose();
                    m.ChatLine = null;
                }
            }
        }

        internal static void ClearBuffer(List<Message> buffer)
        {
            DisposeBufferSprites(buffer);
            buffer.Clear();

            if (buffer == activeMessageBuffer)
            {
                ResetMessages();
            }
        }

        internal static Channel AddChannel(string s, bool alreadyJoined = false, bool doJoin = true)
        {
            return AddChannel(new Channel(s, s != @"#osu" && s != @"#multiplayer" && s != @"#spectator"), alreadyJoined, doJoin);
        }

        internal static Channel AddChannel(Channel c, bool alreadyJoined, bool doJoin)
        {
            if (c.Name == @"#spectator" && !ConfigManager.sShowSpectators && StreamingManager.CurrentlySpectating == null)
                return null; //block own spectator channel if hiding spectators.

            doJoin |= localChannelList.Contains(c.Name) && !alreadyJoined;

            Channel chan = channels.Find(cg => cg.Name == c.Name);

            bool found = chan != null;

            if (!found)
            {
                chan = c;
                chan.Joined = alreadyJoined;
                channels.Add(chan);
            }
            else if (!string.IsNullOrEmpty(c.Topic) || c.UserCount > 0)
            {
                //propagate updates
                chan.Topic = c.Topic;
                chan.UserCount = c.UserCount;
            }

            if (chan.Name[0] != '#')
                chan.Removeable = true;

            chan.Closeable = chan.Name != @"#multiplayer";

            if (chan.Name == @"#multiplayer" || chan.Name == @"#spectator")
            {
                chan.Removeable = true;
                channelTabs.Add(chan.Name, chan, true);
                channelTabs.SetSelected(chan);
            }
            else if (doJoin)
            {
                if (!chan.Join()) return null;
                channelTabs.Add(chan.Name, chan);
            }
            else if (alreadyJoined)
            {
                channelTabs.Add(chan.Name, chan);
            }

            channelTabs.Sprites.ForEach(sp =>
                                            {
                                                bottomSprites.Add(sp);

                                                sp.Position.Y = sp.InitialPosition.Y;

                                                if (!IsVisible)
                                                    sp.Alpha = 0;
                                            });

            if (!hasActivatedLastActiveChannel && ConfigManager.sChatLastChannel == chan.Name)
            {
                channelTabs.SetSelected(chan);
                hasActivatedLastActiveChannel = true;
            }

            return chan;
        }

        public static void RemoveChannel(string s)
        {
            Channel chan = channels.Find(c => c.Name == s);
            if (chan == null) return;
            RemoveChannel(chan, false, true);
        }

        public static void RemoveChannel(Channel chan, bool userRequestedRemoval = false, bool forceRemove = false)
        {
            if (chan.Name == @"#spectator" && !forceRemove)
            {
                chan.Leave();
                localChannelList.Remove(chan.Name);
                channelTabs.Remove(chan);
            }
            else
            {
                chan.Leave();
                localChannelList.Remove(chan.Name);
                if (chan.Removeable || (chan.Joining && !chan.Joined))
                    channels.Remove(chan);
                if (userRequestedRemoval)
                    ConfigManager.sChatChannels.Value = ConfigManager.sChatChannels.Value.Replace(chan.Name, string.Empty).Replace("  ", string.Empty);
                channelTabs.Remove(chan);

                if (activeChannel == chan)
                    channelTabs.ChangeTabCyclic(1);
            }
        }

        private void channelTabs_OnTabChanged(object sender, EventArgs e)
        {
            if (sender == null)
                return;

            if (activeChannel != null)
            {
                string text = InputManager.TextBox.Text;
                if (text.StartsWith(@"!") || text.StartsWith(@"/"))
                {
                    //activeChannel.messageToSend = string.Empty;
                }
                else
                {
                    activeChannel.messageToSend = InputManager.TextBox.Text;
                }
            }

            activeChannel = sender as Channel;

            if (activeChannel == null) return;
            InputManager.SetFullTextBuffer(activeChannel.messageToSend);
            //lastTextboxText = null;

            activeChannel.Join();

            DisposeBufferSprites(activeMessageBuffer);

            // We enforce the buffer size limit before making it the active buffer to avoid overhead when
            // fiddling with the sprites and the scrollable area.
            EnforceBufferSizeLimit(activeChannel.messageBuffer);
            activeMessageBuffer = activeChannel.messageBuffer;

            ResetMessages();

            if (hasActivatedLastActiveChannel || GameBase.Time > 20000 || ConfigManager.sChatLastChannel.Value.Length == 0)
            {
                //i397: Private messages shouldn't count as "channels"
                if (activeChannel.Name[0] == '#')
                {
                    // a bit dodgy but should work well enough. after 20s we assume the user has reached a point of connection.
                    ConfigManager.sChatLastChannel.Value = activeChannel.Name;
                }
            }

            displayWindow = 0;
        }

        private void sortingTabs_OnTabChanged(object sender, EventArgs e)
        {
            CurrentSortMode = (UserSortMode)sender;

            ConfigManager.sChatSortMode.Value = CurrentSortMode;

            lastUpdateSortTime = 0;
            usersArea.SetScrollPosition(Vector2.Zero);
            usersArea.LastScrollTime = -1;
            ResetSortTime();

            switch (CurrentSortMode)
            {
                /*case UserSortMode.Status:
                    UserSortDelegate = delegate(User a, User b)
                                           {
                                               if (!a.IsOsu && !b.IsOsu)
                                                   return a.Name.CompareTo(b.Name);
                                               if (!a.IsOsu)
                                                   return -1;
                                               if (!b.IsOsu)
                                                   return 1;

                                               int stComp = a.Status.CompareTo(b.Status);
                                               if (stComp != 0)
                                                   return stComp;
                                               else
                                                   return a.Name.CompareTo(b.Name);
                                           };
                    break;*/
                default:
                case UserSortMode.Name:
                    UserSortDelegate = ((a, b) => a.Name.CompareTo(b.Name));
                    break;
                case UserSortMode.Country:
                    UserSortDelegate = ((a, b) =>
                    {
                        int compare = 0;
                        if (a.Location != null && b.Location != null) compare = a.Location.CompareTo(b.Location);
                        if (compare == 0) return a.Name.CompareTo(b.Name);
                        return compare;
                    });
                    break;
                case UserSortMode.Timezone:
                    UserSortDelegate = ((a, b) =>
                    {
                        int compare = a.Timezone.CompareTo(b.Timezone);
                        if (compare == 0)
                            return a.Name.CompareTo(b.Name);
                        return compare;
                    });
                    break;
                case UserSortMode.Rank:
                    UserSortDelegate = delegate(User a, User b)
                                           {
                                               if (!a.IsOsu && !b.IsOsu)
                                                   return a.Name.CompareTo(b.Name);
                                               if (!a.IsOsu)
                                                   return 1;
                                               if (!b.IsOsu)
                                                   return -1;

                                               int rank = a.Rank.CompareTo(b.Rank);

                                               if (a.Rank == 0 && b.Rank == 0)
                                                   a.Name.CompareTo(b.Name);

                                               if (a.Rank == 0 && b.Rank == 0)
                                                   return a.Name.CompareTo(b.Name);

                                               if (a.Rank == 0) return 1;
                                               if (b.Rank == 0) return -1;

                                               return rank != 0 ? rank : a.Name.CompareTo(b.Name);
                                           };
                    break;
                /*case UserSortMode.Accuracy:
                    UserSortDelegate = delegate(User a, User b)
                                           {
                                               if (!a.IsOsu && !b.IsOsu)
                                                   return a.Name.CompareTo(b.Name);
                                               if (!a.IsOsu)
                                                   return -1;
                                               if (!b.IsOsu)
                                                   return 1;
                                               int accuracy = b.Accuracy.CompareTo(a.Accuracy);
                                               return accuracy != 0 ? accuracy : a.Name.CompareTo(b.Name);
                                           };
                    break;
                case UserSortMode.PlayCount:
                    UserSortDelegate = delegate(User a, User b)
                                           {
                                               if (!a.IsOsu && !b.IsOsu)
                                                   return a.Name.CompareTo(b.Name);
                                               if (!a.IsOsu)
                                                   return -1;
                                               if (!b.IsOsu)
                                                   return 1;
                                               int playcount = b.PlayCount.CompareTo(a.PlayCount);
                                               return playcount != 0 ? playcount : a.Name.CompareTo(b.Name);
                                           };
                    break;
                case UserSortMode.Level:
                    UserSortDelegate = delegate(User a, User b)
                                           {
                                               if (!a.IsOsu && !b.IsOsu)
                                                   return a.Name.CompareTo(b.Name);
                                               if (!a.IsOsu)
                                                   return -1;
                                               if (!b.IsOsu)
                                                   return 1;
                                               int level = b.Level.CompareTo(a.Level);
                                               return level != 0 ? level : a.Name.CompareTo(b.Name);
                                           };
                    break;*/
            }
        }

        internal static void BanchoConnected()
        {
        }

        private void toggleOnline_OnClick(object sender, EventArgs e)
        {
            AudioEngine.Click(null, @"click-short-confirm");

            if (IsVisible || (!IsVisible && !IsFullVisible))
                ToggleFull();

            if (!IsVisible && CheckLogin())
                GameBase.Scheduler.Add(delegate { Toggle(); });
        }

        public void Draw()
        {
            if (GameBase.Tournament && !GameBase.TournamentManager) return;

            chatArea.Draw();

            if (IsFullVisible)
                usersArea.SetDisplayRectangle(new RectangleF(10, 60, GameBase.WindowWidthScaled - 15, 259 - channelTabs.ExcessHeight));

            switch (ConfigManager.sChatSortMode.Value)
            {
                case UserSortMode.WorldMap:
                    userMap.spriteManager.Alpha = headerText.Alpha;
                    usersArea.SpriteManager.Alpha = 0;
                    userMap.Draw();
                    break;
                default:
                    usersArea.SpriteManager.Alpha = headerText.Alpha;
                    usersArea.Draw();
                    break;
            }

            spriteManagerAboveUsersArea.Draw();
            spriteManagerAboveUsersArea.Alpha = headerText.Alpha;
        }

        internal static bool CursorWithinChatBounds
        {
            get { return IsVisible && chatBg.drawRectangle.Contains(MouseManager.MousePoint); }
        }


        internal static void hideMode_OnClick(object sender, EventArgs e)
        {
            AudioEngine.Click(null, @"click-short-confirm");
            ConfigManager.sAutoChatHide.Value = !ConfigManager.sAutoChatHide;
        }

        internal static void ticker_OnClick(object sender, EventArgs e)
        {
            AudioEngine.Click(null, @"click-short-confirm");
            ConfigManager.sTicker.Value = !ConfigManager.sTicker;
        }

        internal static void UpdateButtonStates()
        {
            if (bTicker != null)
            {
                bTicker.HoverEffect = new Transformation((ConfigManager.sTicker ? Color.White : Color.Gray), (ConfigManager.sTicker ? Color.White : Color.Pink), 0, 100); bTicker.FadeColour((ConfigManager.sTicker ? Color.White : Color.Gray), 100);
                bTicker.Texture = SkinManager.Load(ConfigManager.sTicker ? @"overlay-ticker2" : @"overlay-ticker", SkinSource.Osu);
            }

            if (bHideMode != null)
            {
                bHideMode.HoverEffect = new Transformation((ConfigManager.sAutoChatHide ? Color.White : Color.Gray), (ConfigManager.sAutoChatHide ? Color.White : Color.Pink), 0, 100); bHideMode.FadeColour((ConfigManager.sAutoChatHide ? Color.White : Color.Gray), 100);
                bHideMode.Texture = SkinManager.Load(ConfigManager.sAutoChatHide ? @"overlay-hidemode2" : @"overlay-hidemode", SkinSource.Osu);
            }
        }

        private static void hideBottom_OnClick(object sender, EventArgs e)
        {
            AudioEngine.Click(null, @"click-short-confirm");

            if (CheckLogin() || IsVisible)
            {
                if (!IsVisible && IsFullVisible)
                    ToggleFull();
                Toggle();
            }
        }

        internal static bool GameBase_OnKeyRepeat(object sender, Keys k, bool first)
        {
            if (IsVisible)
            {
                if (InputManager.TextBox.ImeActive ||
    (InputManager.TextBox.ImeDeactivateTime > 0 &&
     GameBase.Time - InputManager.TextBox.ImeDeactivateTime < ChatEngine.ImeCheckDelay))
                {
                    return true;
                }
            }

            if (first)
            {
                if (k == BindingManager.For(Bindings.ToggleChat))
                {
                    if (IsVisible || CheckLogin())
                        Toggle();
                    return true;
                }

                if (k == BindingManager.For(Bindings.ToggleExtendedChat))
                {
                    if (CheckLogin())
                    {
                        if (IsVisible)
                            ToggleFull();
                        else
                        {
                            if (!IsFullVisible)
                                ToggleFull();
                            if (CheckLogin())
                                Toggle();
                        }
                    }
                    return true;
                }
            }

            if (IsVisible)
            {
                if (KeyboardHandler.AltPressed && !KeyboardHandler.ShiftPressed && !KeyboardHandler.ControlPressed)
                {
                    if (k >= Keys.D0 && k <= Keys.D9)
                    {
                        int n = k == Keys.D0 ? 9 : k - Keys.D1;
                        if (n < channelTabs.Tabs.Count)
                        {
                            channelTabs.SetSelected(channelTabs.Tabs[n].Tag);
                            return true;
                        }
                    }
                }

                if (first)
                {
                    if (KeyboardHandler.ControlPressed)
                    {
                        switch (k)
                        {
                            case Keys.Tab:
                                if (KeyboardHandler.ShiftPressed)
                                    channelTabs.ChangeTabCyclic(-1);
                                else
                                    channelTabs.ChangeTabCyclic(1);
                                break;
                            case Keys.W:
                                channelTabs.CloseTab(channelTabs.SelectedTab as pChannelTab);
                                break;
                            case Keys.N:
                                channelTabs.ChangeTabCyclic(1);
                                break;
                            case Keys.P:
                                channelTabs.ChangeTabCyclic(-1);
                                break;
                        }
                    }

                    if (KeyboardHandler.AltPressed)
                    {
                        switch (k)
                        {
                            case Keys.Right:
                                channelTabs.ChangeTabCyclic(1);
                                break;
                            case Keys.Left:
                                channelTabs.ChangeTabCyclic(-1);
                                break;
                        }

                        return true;
                    }

                    switch (k)
                    {
                        case Keys.Tab:
                            DoNicknameCompletion();
                            return true;
                        case Keys.Enter:
                            if (InputManager.TextBox.Text.Length == 0 || !BanchoClient.AllowUserSwitching)
                                return true;

                            //Don't send/handle message if no tabs
                            if (channelTabs.Tabs.Count != 1)
                                SendMessage(InputManager.TextBox.Text, true);

                            InputManager.SetFullTextBuffer(string.Empty);
                            return true;
                        case Keys.Escape:
                            if (IsVisible)
                            {
                                if (Lobby.Status != LobbyStatus.NotJoined && Lobby.Status != LobbyStatus.Play)
                                {
                                    if (IsFullVisible)
                                        ToggleFull();
                                    else
                                    {
                                        switch (Lobby.Status)
                                        {
                                            case LobbyStatus.Idle:
                                                GameBase.ChangeMode(OsuModes.Menu);
                                                break;
                                            case LobbyStatus.Setup:
                                                MatchSetup.DoExit();
                                                break;
                                        }
                                    }
                                }
                                else
                                    Toggle();
                            }
                            return true;
                    }
                }

                switch (k)
                {
                    case Keys.PageUp:
                        if (first || chatArea.TargetHeight > chatArea.ClampingStart)
                            chatArea.ScrollDistance(-FONT_SIZE * lineCount);
                        return true;
                    case Keys.PageDown:
                        if (first || chatArea.TargetHeight < chatArea.ContentDimensions.Y - chatArea.ActualDisplayHeight)
                            chatArea.ScrollDistance(FONT_SIZE * lineCount);
                        return true;
                    case Keys.Down:
                        SendHistoryNext();
                        return true;
                    case Keys.Up:
                        SendHistoryPrev();
                        return true;
                }
            }
            return false;
        }

        private static void DoNicknameCompletion()
        {
            string text = InputManager.TextBox.Text;

            if (text.Length == 0)
                return;

            int currentPosition = InputManager.TextBox.SelectionStart;

            while (currentPosition < text.Length && text[currentPosition] != ' ')
                currentPosition++;

            int firstSpace = text.LastIndexOf(' ', Math.Max(0, currentPosition - 1));
            int lastSpace = text.IndexOf(' ', Math.Max(0, currentPosition - 1));

            if (lastSpace == -1)
                lastSpace = text.Length;
            else
                lastSpace -= 1;

            if (firstSpace == -1)
                firstSpace = 0;
            else
                firstSpace += 1;

            if (lastSpace < firstSpace)
                lastSpace = firstSpace;

            string currentWord = text.Substring(firstSpace, lastSpace - firstSpace).Replace(' ', '_').Trim();

            if (currentWord.Length == 0)
                return;

            List<User> matches = null;
            lock (BanchoClient.Users)
                matches = BanchoClient.Users.FindAll(delegate(User u) { return u.IrcFormattedName.StartsWith(currentWord, StringComparison.InvariantCultureIgnoreCase) && u.Connected; });

            if (matches.Count == 1)
            {
                string replacement = matches[0].IrcFormattedName +
                                     (firstSpace == 0 ? @": " : @" ");
                InputManager.TextBox.Text =
                    InputManager.TextBox.Text.Remove(firstSpace, lastSpace - firstSpace).Insert(firstSpace,
                                                                                                replacement);

                InputManager.TextBox.SelectionStart = currentPosition + (replacement.Length - currentWord.Length);
                InputManager.TextBox.SelectionLength = 0;
            }
            else if (matches.Count > 1)
            {
                string replacement = string.Empty;
                int charCount = 0;
                bool stillOk = true;
                while (stillOk)
                {
                    if (charCount > matches[0].IrcFormattedName.Length - 1)
                        break;

                    char storedChar = matches[0].IrcFormattedName[charCount];
                    for (int i = 1; i < matches.Count; i++)
                    {
                        if (matches[i].IrcFormattedName.Length - 1 < charCount ||
                            char.ToLower(matches[i].IrcFormattedName[charCount]) != char.ToLower(storedChar))
                        {
                            stillOk = false;
                            break;
                        }
                    }
                    if (stillOk)
                        replacement += storedChar;
                    charCount++;
                }

                replacement = replacement.Replace(' ', '_');
                InputManager.TextBox.Text =
                    InputManager.TextBox.Text.Remove(firstSpace, lastSpace - firstSpace).Insert(firstSpace,
                                                                                                replacement);

                InputManager.TextBox.SelectionStart = currentPosition + (replacement.Length - currentWord.Length);
                InputManager.TextBox.SelectionLength = 0;

                string list = string.Empty;
                for (int i = 0; i < matches.Count; i++)
                {
                    if (i == 20)
                    {
                        list += @"...";
                        break;
                    }

                    list += matches[i].IrcFormattedName + @" ";
                }

                activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_TabComplete) + list, string.Empty, Color.YellowGreen));
            }
        }

        internal static void ToggleFull()
        {
            IsFullVisible = !IsFullVisible;

            if (IsFullVisible) GameBase.Options.PoppedOut = false;

            DisplayFull();

            bool bo = (!IsVisible || (IsVisible && IsFullVisible));
            bToggleOnline.HoverEffect =
                new Transformation(bo ? Color.White : Color.Gray, bo ? Color.White : Color.Pink, 0, 100);
            bToggleOnline.FadeColour(bo ? Color.White : Color.Gray, 100);
        }

        private static void DisplayFull()
        {
            if (IsFullVisible && IsVisible)
            {
                usersArea.Show();
                ResetSortTime();
            }
            else
                usersArea.Hide();

            foreach (pSprite p in spriteManager.GetTagged(@"user"))
            {
                if (IsFullVisible && IsVisible)
                    p.FadeIn(300);
                else
                    p.FadeOut(300);
            }

            foreach (pSprite p in GameBase.spriteManagerOverlayHighest.GetTagged(@"user"))
            {
                if (IsFullVisible && IsVisible)
                    p.FadeIn(300);
                else
                    p.FadeOut(300);
            }
        }

        internal static void Toggle(bool force = false)
        {
            if (!IsVisible && !BanchoClient.Connected)
                return;

            bool shouldDisplay = (GameBase.RunningGameMode is Lobby || GameBase.RunningGameMode is MatchSetup);

            if (!force && (IsVisible || GameBase.ActiveDialog != null) && shouldDisplay)
            {
                if (IsFullVisible)
                    ToggleFull();
                return;
            }

            IsVisible = !IsVisible;
            lastToggleTime = GameBase.Time;

            bHideBottom.Texture = SkinManager.Load(IsVisible ? @"overlay-hide" : @"overlay-show", SkinSource.Osu);

            bool bo = (!IsVisible || (IsVisible && IsFullVisible));
            bToggleOnline.HoverEffect =
                new Transformation(bo ? Color.White : Color.Gray, bo ? Color.White : Color.Pink, 0, 100);
            bToggleOnline.FadeColour(bo ? Color.White : Color.Gray, 100);

            AudioEngine.PlaySample(@"select-expand");

            if (IsVisible)
            {
                GameBase.Options.Expanded = false;

                // We want to instantly update our text rendering when becoming visible.
                EnforceBufferSizeLimit(activeMessageBuffer, true);
                ResetMessages();

                lock (SpriteManager.SpriteLock)
                {
                    foreach (pSprite p in bottomSprites)
                    {
                        p.FadeIn(FADE_DURATION - 100);
                        p.MoveToY(p.InitialPosition.Y, FADE_DURATION, EasingTypes.OutSine);
                    }
                }

                tickerText.FadeOut(20);

                emojis.ForEach(p =>
                {
                    p.Bypass = true;
                });
            }
            else
            {
                lock (spriteManager.SpriteList)
                    foreach (pSprite p in bottomSprites)
                    {
                        p.FadeOut(FADE_DURATION - 100);
                        p.MoveToY(p.InitialPosition.Y + 80, FADE_DURATION, EasingTypes.InSine);
                    }
            }
            DisplayFull();

            InputManager.FullTextEnabled = IsVisible;
        }

        private static bool CheckLogin()
        {
#if ARCADE
            if (!BanchoClient.Connected)
                return false;
#endif

            if (!GameBase.HasLogin && GameBase.Mode != OsuModes.Play)
            {
#if !ARCADE
                GameBase.ShowLogin();
#endif
                return false;
            }
            if (!GameBase.HasLogin)
                return false;

            return true;
        }

        static Queue<VoidDelegate> handleMessageQueue = new Queue<VoidDelegate>();

        internal static void HandleMessage(string message)
        {
            Channel console = ChatEngine.AddChannel(@"#console", true, false);
            console.VirtualChannel = true;
            channelTabs.SetSelected(console);

            HandleMessage(new bMessage(@"BanchoBot", console.Name, message), false);
        }

        internal static void HandleMessage(bMessage message, bool outgoing, bool allowHighlight = true)
        {
            lock (handleMessageQueue)
                handleMessageQueue.Enqueue(delegate
                    {
                        string messageTarget = message.target;
                        string rawSender = outgoing ? GameBase.User.Name : message.sendingClient.ToString();

                        string messageString = message.message;
                        string senderString = rawSender;

                        if (messageString.Length == 0) return;

                        if (ConfigManager.sChatFilter)
                            messageString = ChatFilter.Filter(messageString);

                        if (ConfigManager.sChatRemoveForeign)
                        {
                            int count = 0;

                            for (int i = 0; i < messageString.Length; i++)
                                if (messageString[i] > 0x7e)
                                {
                                    messageString = messageString.Replace(messageString[i], ' ');
                                    count++;
                                }

                            if (count > messageString.Length / 2)
                                return;
                        }

                        bool isAction = false;

                        if (messageTarget == @"#spectator" && !ConfigManager.sShowSpectators && StreamingManager.CurrentlySpectating == null)
                            return; //block own spectator channel's messages if hiding spectators.

                        User userIncoming = null;

                        if (!outgoing)
                        {
                            userIncoming = BanchoClient.GetUserById(message.senderId);

                            //handle blocking of incoming messages from users in the ignore list.
                            if (!message.isprivate && ignoreListChat.Contains(rawSender.ToLower().Replace(' ', '_')))
                                return;

                            //handle blocking of incoming PMs from uesrs in the ignore list.
                            if (message.isprivate && ignoreListPrivate.Contains(rawSender.ToLower().Replace(' ', '_')))
                                return;
                        }

                        Channel chan = null;

                        //incoming message channel search
                        if (message.isprivate)
                        {
                            chan = channels.Find(c => c.Name == (outgoing ? messageTarget : rawSender));
                            if (chan == null)
                            {
                                //create a new channel for this incoming PM.
                                chan = AddChannel(rawSender, true, false);
                                chan.VirtualChannel = true;
                            }
                        }
                        else
                            chan = channels.Find(c => c.Name == messageTarget);

                        if (chan == null) return;

                        channelTabs.SetAlert(chan);

                        //BanchoBot gets focus priority.
                        if (message.isprivate && rawSender == @"BanchoBot")
                            SwapChannel(chan);

                        if (messageString.StartsWith('\x1' + @"ACTION ") && messageString.Length >= 8)
                        {
                            messageString = messageString.Substring(8).Trim('\x1');
                            senderString = @"*" + senderString;
                            isAction = true;
                        }
                        else if (messageString.IndexOf('\x1') >= 0 || messageString.IndexOf('/') == 0)
                            return;
                        else
                            senderString += @":";

                        LinkFormatterResult linkResult;
                        try
                        {
                            linkResult = LinkFormatter.Format(messageString, 0, emojiWidth);

                            //remove channel links for non-existing channels.
                            linkResult.Links.RemoveAll(l => l.Url.StartsWith(@"osu://chan/") && channels.Find(c => c.Name == l.Url.Substring(11)) == null);
                        }
                        catch
                        {
                            linkResult = new LinkFormatterResult(string.Empty);
                        }

                        messageString = linkResult.Text;

                        List<Message> channelBuffer = chan.messageBuffer;

                        senderString = DateTime.Now.ToString(@"HH:mm") + ' ' + senderString;

                        Color col;

                        if (message.isprivate && !outgoing)
                        {
                            if (!IsVisible)
                                NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.ChatEngine_PrivateMessageReceived), rawSender), Color.DarkOrchid, 6000, delegate
                                {
                                    if (userIncoming != null)
                                    {
                                        StartChat(userIncoming);
                                        if (!IsVisible) Toggle();
                                    }
                                });
                            if (!Game.IsActive || GameBase.IsMinimized)
                                GameBase.FlashWindow();

                            lastPrivateUser = message.sendingClient.ToString();
                        }

                        if (rawSender == GameBase.User.Name || rawSender == GameBase.User.IrcFormattedName)
                        {
                            col = Color.White;
                        }
                        else if (allowHighlight && messageTarget != @"#highlight" && !message.isprivate && !outgoing && !ignoreListHighlight.Contains(rawSender.ToLower().Replace(' ', '_')) && CheckHighlight(messageString))
                        {
                            col = Color.YellowGreen;
                            if (ConfigManager.sChatHighlightName)
                            {
                                if (!IsVisible)
                                    NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.ChatEngine_Mention), message.sendingClient), Color.DarkOrchid, 6000,
                                        delegate
                                        {
                                            if (chan != null)
                                            {
                                                channelTabs.SetSelected(chan);
                                                if (!IsVisible) Toggle();
                                            }
                                        });
                                if (!Game.IsActive || GameBase.IsMinimized)
                                    GameBase.FlashWindow();
                            }

                            //Create and populate a highlight channel buffer
                            Channel highlight = ChatEngine.AddChannel(@"#highlight", true, false);
                            highlight.VirtualChannel = true;

                            channelTabs.SetAlert(highlight);

                            HandleMessage(new bMessage(message.sendingClient, @"#highlight", messageTarget + @" " + messageString) { senderId = message.senderId }, false);
                        }
                        else if (rawSender == @"BanchoBot")
                            col = new Color(250, 129, 198);
                        else if (isAction)
                            col = Color.White;
                        else if (message.isprivate)
                            col = new Color(89, 116, 255);
                        else if (userIncoming == null)
                            col = new Color(255, 240, 154);
                        else if ((userIncoming.Permission & Permissions.Peppy) > 0)
                            col = new Color(116, 208, 255);
                        else if ((userIncoming.Permission & Permissions.BAT) > 0)
                            col = Color.OrangeRed;
                        else if ((userIncoming.Permission & Permissions.Friend) > 0)
                            col = new Color(183, 232, 255);
                        else if ((userIncoming.Permission & Permissions.Supporter) > 0)
                            col = new Color(255, 223, 46);
                        else
                            col = new Color(255, 240, 154);

                        Message m = new Message(userIncoming, senderString, messageString, col) { FormatterResults = linkResult };
                        channelBuffer.Add(m);

                        if (ConfigManager.sLogPrivateMessages && rawSender != @"BanchoBot" && message.isprivate && !outgoing)
                            saveLineToLog(rawSender, message.message);
                    });
        }

        internal static void AddUserLog(string s)
        {
            Channel log = ChatEngine.AddChannel(@"#userlog", true, false);
            log.VirtualChannel = true;
            HandleMessage(new bMessage(@"BanchoBot", @"#userlog", s), false, false);
        }

        private static bool CheckHighlight(string s)
        {
            if (CheckHighlight(ConfigManager.sUsername, s))
                return true;
            if (ConfigManager.sUsername.Value.IndexOf(' ') > 0 && CheckHighlight(ConfigManager.sUsername.Value.Replace(@" ", @"_"), s))
                return true;

            for (int i = 0; i < HighlightList.Count; i++)
            {
                if (CheckHighlight(HighlightList[i], s))
                    return true;
            }

            return false;
        }

        private static bool CheckHighlight(string needle, string haystack)
        {
            if (string.IsNullOrEmpty(needle) || string.IsNullOrEmpty(haystack)) return false;

            int startIndex = 0;

            int haystackLength = haystack.Length;

            do
            {
                int index = haystack.IndexOf(needle, startIndex, haystackLength - startIndex, StringComparison.InvariantCultureIgnoreCase);

                if (index < 0) return false;

                bool validSurrounds = true;


                //if we are checking a unicode highlight, we don't need to specifically check for valid surrounds.
                if (!checkNoSpaceRange(needle[0]) || !checkNoSpaceRange(needle[needle.Length - 1]))
                {
                    if (validSurrounds && index > 0)
                        validSurrounds &= !char.IsLetter(haystack[index - 1]) || checkNoSpaceRange(haystack[index - 1]);
                    if (validSurrounds && index + needle.Length < haystackLength)
                        validSurrounds &= !char.IsLetter(haystack[index + needle.Length]) || checkNoSpaceRange(haystack[index + needle.Length]);
                }

                if (validSurrounds) return true;

                startIndex = index + needle.Length;
            } while (startIndex < haystackLength - 1);

            return false;
        }

        private static bool checkNoSpaceRange(char p)
        {
            //russian
            if (p >= 0x0400 && p <= 0x04ff) return false;
            if (p >= 0x0500 && p <= 0x052f) return false;
            if (p >= 0x2de0 && p <= 0x2dff) return false;
            if (p >= 0xa640 && p <= 0xa69f) return false;
            if (p >= 0x1d2b && p <= 0x1d78) return false;

            return p > 256;
        }

        internal static void HandleChangeUsername(string UserOldNick, string UserNewNick)
        {
            //this never happens anymore
        }

        private int lastFullUpdateTime;

        /// <summary>
        /// When filtering we need to update the user list more often than usual.
        /// This handles the edge case where filter box is cleared.
        /// </summary>
        private bool searchFilterChanged;

        List<User> visibleUsers = new List<User>();

        public override void Update()
        {
            UpdateMessageQueue();

            UpdateUserRemoval();

            switch (ConfigManager.sChatSortMode.Value)
            {
                default:
                    usersArea.Update();
                    break;
                case UserSortMode.WorldMap:
                    userMap.Update();
                    break;
            }

            UpdateFriendAlerts();

            UpdateClampingStart();

            chatArea.Update();
            chatArea.SetDisplayRectangle(ChatAreaDisplayRect);
            chatArea.Alpha = chatBg.Alpha;

            UpdateActiveMessageBuffer();

            if (IsVisible && GameBase.Time - lastToggleTime > FADE_DURATION)
            {
                UpdateUserPanels();
                channelTabs.UpdateVisibility();
            }

            UpdateSendLine();

            UpdateBottomButtonVisibility();

            base.Update();
        }

        private static void UpdateClampingStart()
        {
            if (activeMessageBuffer.Count == 0)
            {
                chatArea.ClampingStart = -chatArea.ActualDisplayHeight;
            }
            else
            {
                Message m = activeMessageBuffer[activeMessageBuffer.Count - 1];

                if (m.HasChatLine)
                {
                    ChatLine line = m.ChatLine;

                    float newClampingStart = (float)Math.Min(0, line.Position.Y + line.ExtentY - chatArea.ActualDisplayHeight);

                    if (chatArea.ScrollVelocity < 0 || chatArea.ScrollPosition.Y >= newClampingStart)
                    {
                        chatArea.ClampingStart = (float)Math.Min(0, line.Position.Y + line.ExtentY - chatArea.ActualDisplayHeight);
                    }
                }
            }
        }

        private void UpdateUserPanels()
        {
            if (IsFullVisible && IsVisible)
            {
                changeUpdateMode(ConfigManager.sUserFilter == UserFilterType.Friends ? bReceiveUpdates.Friends : bReceiveUpdates.All);

                lastFullUpdateTime = GameBase.Time;

                bool scrollLocking = usersArea.LastScrollTime > 0 && GameBase.Time - usersArea.LastScrollTime <= 10000;

                int user_columns = (int)Math.Round(GameBase.WindowWidthScaled / 205f);

                usersArea.SpriteManager.Scale = user_columns != 3 ? GameBase.WindowWidthScaled / (25 + user_columns * 205f) : 1;

                bool inactive = !GameBase.IsActive || GameBase.IdleTime > 60000;

                if (GameBase.Time - lastUpdateSortTime > timeBetweenResorts * (inactive ? 4 : 1) || lastUpdateSortTime == 0)
                {
                    lastUpdateSortTime = GameBase.Time;
                    timeBetweenResorts = Math.Max(500, Math.Min((int)(timeBetweenResorts * 1.2f), 10000));
                    searchFilterChanged = false;

                    if (!scrollLocking && !scrollLockingPermanent)
                        lock (BanchoClient.Users) BanchoClient.Users.Sort(UserSortDelegate);

                    int countOsu = 0;
                    displayedCount = 0;
                    string searchTerm = searchFilter.Box.Text.ToLower();
                    bool hasSearch = searchTerm.Length > 0;

                    List<User> visibleUsers = new List<User>();

                    bool overrideModeFilters = ConfigManager.sUserFilter == UserFilterType.Friends || hasSearch;

                    modeFilterOsu.Bypass = overrideModeFilters;
                    modeFilterTaiko.Bypass = overrideModeFilters;
                    modeFilterFruits.Bypass = overrideModeFilters;
                    modeFilterMania.Bypass = overrideModeFilters;

                    int countPresence = 0;

                    lock (BanchoClient.Users)
                        foreach (User u in BanchoClient.Users)
                        {
                            if (u.Id < 1 || !u.Connected)
                                continue;

                            if (u.InitialLoadComplete)
                                countPresence++;

                            if ((!hasSearch || (u.InitialLoadComplete && u.Name.ToLower().Contains(searchTerm))) &&
                                (hasSearch || ConfigManager.sUserFilter != UserFilterType.Friends || u.IsFriend) &&
                                (hasSearch || ConfigManager.sUserFilter != UserFilterType.Near || (Math.Abs(u.Latitude - GameBase.User.Latitude) < 0.01 && Math.Abs(u.Longitude - GameBase.User.Longitude) < 0.01)) &&     //100Km
                                (hasSearch || ConfigManager.sUserFilter != UserFilterType.Country || (u.CountryCode == GameBase.User.CountryCode)) &&
                                (overrideModeFilters || playModeStatus[(int)u.PlayMode]))
                            {
                                if (!scrollLocking && !scrollLockingPermanent)
                                {
                                    if (u.DrawAt(new Vector2((displayedCount % user_columns) * 205, (displayedCount / user_columns) * 52), true, countOsu))
                                    {
                                        //first time displayed/populated.
                                        u.spriteBackground.HandleInput = true;
                                        u.spriteBackground.ClickRequiresConfirmation = true;
                                        u.spriteBackground.OnClick += user_OnClick;
                                        u.spriteBackground.Tag = u;
                                        u.spriteManager = usersArea.SpriteManager;
                                        usersArea.SpriteManager.Add(u.Sprites);

                                        foreach (pSprite p in u.Sprites)
                                            p.Transformations.Add(new Transformation(TransformationType.Fade, 0, p.Alpha, GameBase.Time, GameBase.Time + 500));
                                    }

                                    u.Sprites.ForEach(s => s.Bypass = false);
                                }

                                displayedCount++;

                                if (u.spriteBackground != null && u.spriteBackground.IsVisible)
                                    visibleUsers.Add(u);
                            }
                            else if (u.spriteBackground != null)
                            {
                                if (!u.spriteBackground.Bypass)
                                    u.Sprites.ForEach(s => s.Bypass = true);
                            }

                            countOsu++;
                        }

                    if (visibleUsers.Count == 0)
                        ResetSortTime();
                    else
                        GameBase.Scheduler.AddDelayed(delegate { RequestUserStats(visibleUsers); }, 500);

                    headerText.Text = string.Format(LocalisationManager.GetString(OsuString.ChatEngine_UsersConnected), countOsu.ToString(@"#,0", GameBase.nfi));
                    usersArea.SetContentDimensions(new Vector2(user_columns, (displayedCount / user_columns) * 52 + 52));

                    if (countOsu - countPresence > 500) PresenceCache.QueryAll();
                }
            }
            else
            {
                if (lastFullUpdateTime > 0 && lastFullUpdateTime + 40000 < GameBase.Time)
                {
                    lastFullUpdateTime = 0;
                    changeUpdateMode(bReceiveUpdates.None);

                    if (visibleUsers.Count > 0)
                        visibleUsers.Clear();
                }
            }
        }

        private void UpdateSendLine()
        {
            if (!BanchoClient.AllowUserSwitching)
            {
                sendLine.InitialColour = Color.Red;
                cursor.FadeOut(0);

                int mins = (int)Math.Ceiling(BanchoClient.AllowUserSwitchingRestoration.Subtract(DateTime.Now).TotalMinutes);

                lastTextboxText = sendLine.Text = string.Format(LocalisationManager.GetString(OsuString.ChatEngine_SilenceNotification), mins);
            }
            else
            {
                sendLine.InitialColour = Color.White;
                string textboxText = InputManager.TextBox.Text;

                if (textboxText != lastTextboxText)
                {
                    lastTextboxText = textboxText;

                    textboxTextOffset = 0;

                    LinkFormatterResult result = LinkFormatter.Format(textboxText, 0, emojiWidth);
                    bool hasEmoji = false;
                    //try to add emoji at sendline
                    sendlineEmojis.ForEach(p =>
                    {
                        bottomSprites.Remove(p);
                        spriteManager.Remove(p);
                        p.Dispose();
                    });
                    sendlineEmojis.Clear();
                    result.Links.ForEach(l =>
                    {
                        if (l.Url.IndexOf('\uD83D') == 0 && l.Url.Length == 2)
                        {
                            if (!hasEmoji)
                            {
                                hasEmoji = true;
                                textboxText = result.Text;
                                sendLine.Text = @">" + textboxText;
                            }
                            Vector2 thisOffset = new Vector2(sendLine.MeasureText(0, l.Index + 1).X - 2, 0);
                            string temp = ((int)l.Url[1]).ToString();
                            pSprite p = new pSprite(SkinManager.Load(@"emoji-" + temp), Fields.TopLeft, Origins.TopLeft, Clocks.Game, sendLine.Position + thisOffset, 0.975f, true, Color.White);
                            bottomSprites.Add(p);
                            sendlineEmojis.Add(p);
                            spriteManager.Add(p);
                        }
                    });
                    if (!hasEmoji)
                        sendLine.Text = @">" + textboxText;

                    float length = sendLine.MeasureText(textboxTextOffset, sendLine.Text.Length - textboxTextOffset).X;
                    while (length > GameBase.WindowWidthScaled - 60)
                    {
                        textboxTextOffset += 4;
                        length = sendLine.MeasureText(textboxTextOffset, sendLine.Text.Length - textboxTextOffset).X;
                    }

                    if (textboxTextOffset > 0)
                    {
                        textboxText = textboxText.Remove(0, textboxTextOffset);
                        sendLine.Text = textboxText;
                    }

                    if (length > GameBase.WindowWidthScaled - 240)
                    {
                        bHideMode.FadeOut(200);
                        bTicker.FadeOut(200);
                        bToggleOnline.FadeOut(200);
                    }
                    else
                    {
                        bHideMode.FadeIn(200);
                        bTicker.FadeIn(200);
                        bToggleOnline.FadeIn(200);
                    }
                }

                selectionStartLast = selectionStart;

                selectionStart =
                    Math.Max(0, InputManager.TextBox.SelectionStart + (textboxTextOffset == 0 ? 1 : -textboxTextOffset));
                int selectionLength =
                    Math.Min(InputManager.TextBox.SelectionLength, sendLine.Text.Length - selectionStart);


                if (selectionLength > 0)
                {
                    selection.Position = sendLine.Position;
                    //todo: fix this
                    try
                    {
                        selection.Position.X += sendLine.MeasureText(0, selectionStart).X - 2;
                        selection.VectorScale =
                            new Vector2(sendLine.MeasureText(selectionStart, selectionLength).X - 4, 32) * 1.6F;
                    }
                    catch (Exception)
                    {
                    }
                    cursor.FadeOut(50);
                    selection.FadeIn(50);

                    selectionStartLast = -1;
                    selectionStart = -1;
                }
                else
                {
                    selection.FadeOut(50);
                    if (cursor.Transformations.Count == 0)
                        cursor.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0.4F, GameBase.Time, GameBase.Time + 1000) { Easing = EasingTypes.Out, Loop = true });

                    if (selectionStartLast != selectionStart)
                    {
                        cursor.Position.X = (selectionStart == sendLine.Text.Length ? 2 : 1) + sendLine.Position.X + sendLine.MeasureText(0, Math.Min(sendLine.Text.Length, selectionStart)).X - 3.5f;
                        cursor.InitialPosition.X = cursor.Position.X;
                    }
                }
                Candidates.StartPosition.X = cursor.Position.X;
                Candidates.Update();
            }
        }

        private static void AddNewMessages(List<Message> buffer, bool instant = false)
        {
            int lastInitializedIndex = buffer.Count - 1;
            while (lastInitializedIndex >= 0 && !buffer[lastInitializedIndex].HasChatLine) --lastInitializedIndex;
            ++lastInitializedIndex;

            if (lastInitializedIndex < buffer.Count)
            {
                float bottomPosBefore = chatArea.ContentDimensions.Y - chatArea.ActualDisplayHeight;

                for (int i = lastInitializedIndex; i < buffer.Count; ++i)
                {
                    AddMessageToView(i, true);
                }

                float bottomPos = chatArea.ContentDimensions.Y - chatArea.ActualDisplayHeight;
                float lastMessageSize = buffer[buffer.Count - 1].ChatLine.ExtentY;

                if (bottomPosBefore - chatArea.TargetHeight < lastMessageSize && chatArea.ScrollPosition.Y < bottomPos)
                {
                    chatArea.ScrollToBottom(instant ? -0.01f : 0);
                }
            }
        }

        private static void AddNewlyVisibleMessages(List<Message> buffer)
        {
            for (int i = buffer.Count - 1; i >= 0 && (!buffer[i].HasChatLine || buffer[i].ChatLine.Position.Y > chatArea.ScrollPosition.Y); --i)
            {
                Message m = buffer[i];
                if (!m.HasChatLine)
                {
                    AddMessageToView(i);

                    if (m.ChatLine.Position.Y <= chatArea.ScrollPosition.Y)
                    {
                        break;
                    }
                }
            }
        }

        private static void EnforceBufferSizeLimit(List<Message> buffer, bool forceQuick = false)
        {
            if (buffer.Count <= CHANNEL_BUFFER_SIZE)
            {
                return;
            }

            if (buffer == activeMessageBuffer && !forceQuick)
            {
                do
                {
                    RemoveMessage(0);
                }
                while (buffer.Count > CHANNEL_BUFFER_SIZE);
            }
            else
            {
                buffer.RemoveRange(0, buffer.Count - CHANNEL_BUFFER_SIZE);
            }
        }

        private static void UpdateTextRendering(bool instant = false)
        {
            if (activeMessageBuffer == null)
            {
                return;
            }

            AddNewMessages(activeMessageBuffer, instant);
            AddNewlyVisibleMessages(activeMessageBuffer);

            EnforceBufferSizeLimit(activeMessageBuffer);
        }

        private void UpdateChatTicker()
        {
            if (activeMessageBuffer == null)
            {
                return;
            }

            if (ConfigManager.sTicker)
            {
                if (activeMessageBuffer.Count > 0 && lastTickerMessage != activeMessageBuffer[activeMessageBuffer.Count - 1])
                {
                    lastTickerMessage = activeMessageBuffer[activeMessageBuffer.Count - 1];

                    tickerText.Position = GameBase.Mode == OsuModes.Edit ? TickerPositionEdit : TickerPositionNormal;

                    if (lastTickerMessage != null && lastTickerMessage.Content.Length > 0)
                    {
                        tickerText.Text = lastTickerMessage.Name + @" " + lastTickerMessage.Content;
                        if (!IsVisible)
                        {
                            tickerText.InitialColour = lastTickerMessage.Colour;
                            tickerText.FadeOutFromOne(8000);
                        }
                    }
                }
            }
        }

        private void UpdateActiveMessageBuffer()
        {
            // No need to update text rendering if invisible
            if (IsVisible)
            {
                UpdateTextRendering();
            }

            UpdateChatTicker();
        }

        private void UpdateMessageQueue()
        {
            if ((IsVisible || !Player.Playing || ConfigManager.sPopupDuringGameplay) && handleMessageQueue.Count > 0)
                lock (handleMessageQueue) handleMessageQueue.Dequeue()();
        }

        private static void UpdateUserRemoval()
        {
            //optimised removal of users from list
            if (BanchoClient.RemovedUsers > 100 && !Player.Playing)
            {
                lock (BanchoClient.Users) BanchoClient.Users.RemoveAll(u => !u.Connected);
                BanchoClient.RemovedUsers = 0;
            }
        }

        static int lastUserStatsRequest;
        internal static void RequestUserStats(List<User> users, bool force = false)
        {
            if (users.Count == 0) return;

            if (!force)
            {
                if (GameBase.Time - lastUserStatsRequest < 1000)
                    return;
                lastUserStatsRequest = GameBase.Time;
            }

            List<int> userIds = new List<int>(users.Count);

            foreach (User u in users)
                if ((u.spriteBackground != null && u.spriteBackground.IsVisible) || force)
                {
                    userIds.Add(u.Id);
                    u.RequestPresence();

                    if (userIds.Count == 32)
                    {
                        BanchoClient.SendRequest(RequestType.Osu_UserStatsRequest, new bListInt(userIds));
                        userIds = new List<int>();
                    }
                }

            if (userIds.Count > 0)
                BanchoClient.SendRequest(RequestType.Osu_UserStatsRequest, new bListInt(userIds));
        }

        bReceiveUpdates UpdateMode;
        private void changeUpdateMode(bReceiveUpdates newMode)
        {
            if (!BanchoClient.Connected)
            {
                UpdateMode = bReceiveUpdates.None;
                return;
            }

            if (UpdateMode == newMode)
                return;

            BanchoClient.SendRequest(RequestType.Osu_ReceiveUpdates, (int)newMode);
            UpdateMode = newMode;
        }

        private void UpdateFriendAlerts()
        {
            if (!ConfigManager.sNotifyFriends)
                return;

            if (friendAlertLastOnline > 0 && GameBase.Time - friendAlertLastOnline > 1000 && friendAlertOnlineQueue.Count > 0)
            {
                friendAlertLastOnline = 0;

                string friendAlertBuiltOnline = friendAlertOnlineQueue[0];
                for (int i = 1; i < friendAlertOnlineQueue.Count; i++)
                    friendAlertBuiltOnline += @", " + friendAlertOnlineQueue[i];

                User notificationUser = friendAlertOnlineQueue.Count == 1 ? BanchoClient.GetUserByNameEx(friendAlertOnlineQueue[0]) : null;

                NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.ChatEngine_FriendsOnline), friendAlertBuiltOnline), Color.YellowGreen, 8000, delegate
                {
                    if (notificationUser != null)
                    {
                        StartChat(notificationUser);
                        if (!IsVisible) Toggle();
                    }
                });

                friendAlertOnlineQueue.Clear();
            }

            if (friendAlertLastOffline > 0 && GameBase.Time - friendAlertLastOffline > 1000 && friendAlertOfflineQueue.Count > 0)
            {
                friendAlertLastOffline = 0;

                string friendAlertBuiltOffline = friendAlertOfflineQueue[0];
                for (int i = 1; i < friendAlertOfflineQueue.Count; i++)
                    friendAlertBuiltOffline += @", " + friendAlertOfflineQueue[i];

                NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.ChatEngine_FriendsOffline), friendAlertBuiltOffline), Color.OrangeRed, 8000);
                friendAlertOfflineQueue.Clear();
            }
        }

        private void UpdateBottomButtonVisibility()
        {
            bool hideButtons = true;
            bool cinema = ModManager.CheckActive(Mods.Cinema);

            if (!BanchoClient.Connected || (GameBase.Tournament && !GameBase.TournamentManager))
            {
                if (IsVisible)
                    Toggle();

                hideButtons = true;
            }
            else if (GameBase.Mode == OsuModes.Play && (!InputManager.ReplayMode || cinema))
            {
                //Check whether chat is set to auto-hide and we are playing
                hideButtons = !IsVisible && (!AudioEngine.Paused || Player.Unpausing) && (Player.Playing || Player.SkipBoundaryActive || Player.OutroSkippable || cinema);

                if (ConfigManager.sAutoChatHide)
                {
                    //Auto-hide when playing.  ForceHide stores the status of whether the chat should actually reopen later on or not.
                    if (Player.Playing && IsVisible)
                    {
                        if (!ForceHide)
                        {
                            ForceHide = true;
                            Toggle();
                        }
                    }
                    else if (!Player.Playing && !IsVisible && ForceHide)
                    {
                        ForceHide = false;
                        Toggle();
                    }
                }
            }
            else
            {
                hideButtons = !(GameBase.ActiveDialog == null &&
                     (IsVisible || (GameBase.Mode == OsuModes.Menu && !osu.GameModes.Menus.Menu.IsIdle) || GameBase.Mode == OsuModes.Rank ||
                      GameBase.Mode == OsuModes.RankingVs || GameBase.Mode == OsuModes.RankingTagCoop ||
                      GameBase.Mode == OsuModes.RankingTeam || GameBase.Mode == OsuModes.Play ||
                      GameBase.Mode == OsuModes.OnlineSelection || GameBase.Mode == OsuModes.BeatmapImport));
            }

            if (hideButtons != bHideBottom.IsVisible) return;

            if (hideButtons)
            {
                bHideBottom.FadeOut(50);
                bToggleOnline.FadeOut(50);
                GameBase.fpsDisplay.MoveToY(GameBase.WindowHeightScaled - 2, 300, EasingTypes.Out);
            }
            else
            {
                bHideBottom.FadeIn(50);
                bToggleOnline.FadeIn(50);
                GameBase.fpsDisplay.MoveToY(GameBase.WindowHeightScaled - 14, 300, EasingTypes.Out);
            }
        }

        private static void AddMessageToView(int index, bool isAtEnd = false)
        {
            Message m = activeMessageBuffer[index];

            ChatLine line = m.ChatLine;

            isAtEnd |= index == activeMessageBuffer.Count - 1;

            line.Position = new Vector2(0, isAtEnd ? chatArea.ContentDimensions.Y : activeMessageBuffer[index + 1].ChatLine.Position.Y - line.ExtentY);

            // Need to move everything upwards if we go out of chat bounds!
            if (line.Position.Y < 0)
            {
                for (int i = index + 1; i < activeMessageBuffer.Count; ++i)
                {
                    Message messageToShift = activeMessageBuffer[i];
                    if (messageToShift.HasChatLine)
                    {
                        messageToShift.ChatLine.Position += new Vector2(0, -line.Position.Y);
                    }
                }

                chatArea.ScrollPosition.Y += -line.Position.Y;
                chatArea.SetContentDimensions(chatArea.ContentDimensions + new Vector2(0, -line.Position.Y));
                line.Position = new Vector2(line.Position.X, 0);
            }

            chatArea.SpriteManager.Add(line.Sprites);

            if (isAtEnd)
            {
                chatArea.SetContentDimensions(chatArea.ContentDimensions + new Vector2(0, line.ExtentY));
            }
        }

        private static void ResetMessages()
        {
            if (chatArea == null)
                return;

            chatArea.ClearSprites();

            if (activeMessageBuffer == null)
                return;

            chatArea.ContentDimensions.Y = (float)Math.Max(0, (activeMessageBuffer.Count - 1) * FONT_SIZE); // Assume previous messages are all one-liners for now.

            if (activeMessageBuffer.Count > 0)
            {
                DisposeBufferSprites(activeMessageBuffer);
                AddMessageToView(activeMessageBuffer.Count - 1);
            }

            UpdateClampingStart();

            chatArea.ScrollPosition.Y = (float)Math.Max(chatArea.ClampingStart, chatArea.ContentDimensions.Y - chatArea.ActualDisplayHeight);
            chatArea.ResetScrollVelocity();
        }

        private static void RemoveMessage(int index)
        {
            Message m = activeMessageBuffer[index];

            Vector2 offset = Vector2.Zero;
            if (m.HasChatLine)
            {
                ChatLine line = m.ChatLine;
                offset.Y = line.ExtentY;

                line.Dispose();
                chatArea.SpriteManager.RemoveRange(line.Sprites);
            }
            else
            {
                offset.Y = FONT_SIZE;
            }

            activeMessageBuffer.RemoveAt(index);
            chatArea.SetContentDimensions(chatArea.ContentDimensions - offset);

            // We only adjust the scroll position if there would be visible movement on the chat window otherwise.
            float positionY = index >= activeMessageBuffer.Count ? chatArea.ContentDimensions.Y : activeMessageBuffer[index].ChatLine.Position.Y;
            if (positionY < chatArea.ScrollPosition.Y + chatArea.ActualDisplayHeight)
            {
                chatArea.ScrollPosition -= offset;
            }

            // Move all subsequent messages up
            for (int i = index; i < activeMessageBuffer.Count; ++i)
            {
                if (activeMessageBuffer[i].HasChatLine)
                {
                    activeMessageBuffer[i].ChatLine.Position -= offset;
                }
            }
        }

        internal static void SwapChannel(Channel c)
        {
            if (c == null) return;

            if (!localChannelList.Contains(c.Name))
                AddChannel(c.Name, false, true);
            channelTabs.SetSelected(c);
        }

        private static void linkSprite_OnClick(object sender, EventArgs e)
        {
            string link = ((pText)sender).Text;

            HandleLink(link);
        }

        internal static void HandleLink(string link)
        {
            try
            {
                string trimmedLink = link.Replace(@"index.php", string.Empty);

                string idtype = null;
                string domain = Regex.Escape(General.WEB_ROOT).Replace(@"http", @"http[s]?");

                Regex regex = new Regex(domain + @"/\?p=beatmap&(.)=([0-9]*)");
                Match m = regex.Match(trimmedLink);

                if (m.Success)
                    idtype = m.Groups[1].Value;

                if (idtype == null)
                {
                    Regex regex2 = new Regex(domain + @"/(.)/([0-9]*)");
                    m = regex2.Match(trimmedLink);

                    if (m.Success)
                        idtype = m.Groups[1].Value;
                }

                if (idtype == null)
                {
                    Regex regex3 = new Regex(domain + @"/forum/viewtopic.php.*[\?|&]t=([^0].[0-9]*).*");
                    m = regex3.Match(trimmedLink);

                    if (m.Success)
                        idtype = @"t";
                }

                if (idtype == null)
                {
                    Regex regex3 = new Regex(domain + @"/forum/viewtopic.php\?.*p=([^0].[0-9]*).*");
                    m = regex3.Match(trimmedLink);

                    if (m.Success)
                        idtype = @"p";
                }

                //url handling.
                if (trimmedLink.StartsWith(@"osu://"))
                {

                    string[] split = trimmedLink.Replace(@"osu://", string.Empty).Split('/');
                    switch (split[0])
                    {
                        case @"dl":
                            OsuDirect.HandlePickup(LinkId.Set, Int32.Parse(split[1]), null);
                            return;
                        case @"edit":
                            if (GameBase.Mode != OsuModes.Edit)
                            {
                                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.ChatEngine_MustBeEditMode), 2000);
                                return;
                            }

                            //Minimized and bosskey is not active
                            if (GameBase.IsMinimized && GameBase.Form.Visible)
                            {
                                GameBase.Scheduler.Add(delegate { GameBase.Form.WindowState = System.Windows.Forms.FormWindowState.Normal; });
                            }
                            Editor.LoadSelection(split[1].Replace(@"%20", @" "));
                            return;
                        case @"chan":
                            ChatEngine.SwapChannel(ChatEngine.AddChannel(split[1]));
                            return;

                    }
                }

                if (idtype == null)
                {
                    Regex regex3 = new Regex(@"osump\://([0-9]*)/(.*)");
                    m = regex3.Match(trimmedLink);

                    if (m.Success)
                    {
                        if (!BanchoClient.CheckAuth(true)) return;

                        if (MatchSetup.Match != null)
                        {
                            NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.ChatEngine_AlreadyInMatch));
                            return;
                        }

                        int matchId;
                        if (!int.TryParse(m.Groups[1].Value, out matchId))
                            return;
                        string password = m.Groups[2].Value;

                        Lobby.JoinMatch(matchId, password);
                        return;
                    }

                }

                if (GameBase.RunningGameMode is SongSelection)
                {
                    SongSelection select = (SongSelection)GameBase.RunningGameMode;
                    Beatmap map = null;

                    if (idtype == @"s") map = BeatmapManager.GetBeatmapBySetId(Int32.Parse(m.Groups[2].Value));
                    else if (idtype == @"b") map = BeatmapManager.GetBeatmapById(Int32.Parse(m.Groups[2].Value));

                    if (map != null)
                    {
                        select.Select(map);
                        return;
                    }
                }

                if ((idtype != null) && ((BanchoClient.Permission & Permissions.Supporter) > 0))
                {
                    switch (idtype)
                    {
                        case @"s":
                            OsuDirect.HandlePickup(LinkId.Set, Int32.Parse(m.Groups[2].Value), null,
                                                   delegate { NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.ChatEngine_CouldntFindThisBeatmap)); });
                            return;
                        case @"b":
                            OsuDirect.HandlePickup(LinkId.Beatmap, Int32.Parse(m.Groups[2].Value), null,
                                                   delegate { NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.ChatEngine_CouldntFindThisBeatmap)); });
                            return;
                        case @"t":
                            OsuDirect.HandlePickup(LinkId.Topic, Int32.Parse(m.Groups[1].Value), null,
                                                   delegate { GameBase.ProcessStart(link); });
                            return;
                        case @"p":
                            OsuDirect.HandlePickup(LinkId.Post, Int32.Parse(m.Groups[1].Value), null,
                                                   delegate { GameBase.ProcessStart(link); });
                            return;
                    }
                }
            }
            catch
            {
            }

            if (!link.StartsWith(@"http://") && !link.StartsWith("https://"))
                link = @"http://" + link;

            GameBase.ProcessStart(link);
        }

        internal static void user_OnClick(object sender, EventArgs e)
        {
            if (!IsVisible) return;

            pSprite sprite = sender as pSprite;

            if (sprite == null || sprite.Tag == null) return;

            User u = sprite.Tag as User;

            if (u == null) return;

            if (u == GameBase.User)
            {
                //this is the local user.
                GameBase.ShowLocalUserOptions(null, null);
                return;
            }

            UserProfile.DisplayProfileFor(u, !(sender is pText));

            return;
        }

        internal static void ToggleFriend(User user, bool status)
        {
            if (user.IsFriend == status)
                return;

            user.IsFriend = status;

            if (user.IsFriend)
            {
                NotificationManager.ShowMessageMassive("You are now friends with " + user.Name + ".", 2000);
                Friends.Add(user.Id);
                BanchoClient.SendRequest(RequestType.Osu_FriendAdd, new bInt(user.Id));
            }
            else
            {
                NotificationManager.ShowMessageMassive("You are no longer friends with " + user.Name + ".",
                                                       2000);
                Friends.Remove(user.Id);
                BanchoClient.SendRequest(RequestType.Osu_FriendRemove, new bInt(user.Id));
            }
        }

        internal static void SendMessage(string m, bool history)
        {
            SendMessage(m, activeChannel.Name, history);
        }

        internal static void SendMessage(string m, string channel, bool history)
        {
            if (!BanchoClient.InitializationComplete)
                return;

            if (CheckKeySpam(m))
                return;

            if (m.Length == 0)
                return;

            if (m.IndexOf('\n') >= 0)
                m = m.Remove(m.IndexOf('\n'));

            if (history)
            {
                if (sendBuffer.Count > 0)
                    sendBuffer.RemoveRange(0, historyCurrent);
                historyCurrent = 0;
                sendBuffer.Insert(historyCurrent, m);
            }

            if (!BanchoClient.Connected)
                activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_NotConnected), string.Empty, Color.Red));

            bool isCommand = m[0] == '/';

            bool privateChannel = channel[0] != '#';

            if (m.StartsWith(@"/bb "))
            {
                SendMessage(@"/chat BanchoBot", false);
                SendMessage(m.Replace(@"/bb ", string.Empty), false);
                return;
            }

            Channel chan = channels.Find(c => c.Name == channel);

            if (isCommand)
            {
                int firstSpace = m.IndexOf(' ');

                string command = firstSpace > 0 ? m.Substring(1, firstSpace - 1) : m.Substring(1);
                string arg = firstSpace > 0 ? m.Substring(firstSpace + 1).Trim() : null;

                switch (command)
                {
                    case @"ignore":
                        if (arg == null) return;
                        {
                            string[] argSplit = arg.Split('@');
                            if (argSplit.Length < 2)
                            {
                                argSplit = new string[] { argSplit[0], @"cp" };
                            }

                            User found = BanchoClient.GetUserByNameEx(argSplit[0]);

                            IgnoreUser(found, argSplit[1]);
                        }
                        return;
                    case @"unignore":
                        {
                            if (arg == null) return;

                            User found = BanchoClient.GetUserByNameEx(arg);

                            UnignoreUser(found);
                        }
                        return;
                    case @"np":
                        UpdateNowPlaying(true, false);
                        return;
                    case @"away":
                        {
                            bMessage msg = new bMessage(null, null, arg);
                            BanchoClient.SendRequest(RequestType.Osu_SetIrcAwayMessage, msg);
                        }
                        return;
                    case @"watch":
                        if (arg == null) return;
                        {
                            if (GameBase.Mode == OsuModes.Edit || (GameBase.Mode == OsuModes.Play && GameBase.TestMode))
                            {
                                activeMessageBuffer.Add(
                                    new Message(LocalisationManager.GetString(OsuString.ChatEngine_ExitEditorForSpectator), string.Empty, Color.Red));
                                return;
                            }

                            User found = BanchoClient.GetUserByNameEx(arg);

                            if (found != null)
                                StreamingManager.StartSpectating(found);
                            else
                                activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_UserNotOnline), string.Empty, Color.Red));
                            return;
                        }
                    case @"savelog":
                        ExportLog(channel);
                        return;
                    case @"addfriend":
                        {
                            if (arg == null) return;

                            arg = arg.Replace('_', ' ');
                            User found = BanchoClient.GetUserByNameEx(arg);

                            if (found == GameBase.User)
                            {
                                activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_FriendAddSelf), string.Empty, Color.Red));
                                return;
                            }

                            if (found != null)
                                ToggleFriend(found, true);
                            else
                                activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_UserNotOnline), string.Empty, Color.Red));
                        }
                        return;
                    case @"delfriend":
                        {
                            User found = BanchoClient.GetUserByNameEx(arg);

                            if (found == GameBase.User)
                            {
                                activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_FriendRemoveSelf), string.Empty, Color.Red));
                                return;
                            }

                            if (found != null)
                                ToggleFriend(found, false);
                            else
                                activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_UserNotOnline), string.Empty, Color.Red));
                        }
                        return;
                    case @"invite":
                        {
                            User found = BanchoClient.GetUserByNameEx(arg);
                            if (found == null || !found.IsOsu)
                                activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_UserNotOnline), string.Empty, Color.Red));
                            else if (MatchSetup.Match != null)
                            {
                                BanchoClient.SendRequest(new Request(RequestType.Osu_Invite, new bInt(found.Id)));
                                activeMessageBuffer.Add(new Message(string.Format(LocalisationManager.GetString(OsuString.ChatEngine_UserInvited), found.Name), string.Empty, Color.YellowGreen));
                            }
                        }
                        return;
                    case @"chat":
                    case @"query":
                    case @"msg":
                        {
                            if (arg == null)
                            {
                                if (lastPrivateUser != null)
                                    arg = lastPrivateUser;
                                else
                                    return;
                            }

                            User found = BanchoClient.GetUserByNameEx(arg);
                            if (found != null)
                            {
                                StartChat(found);
                            }
                            else
                            {
                                activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_UserNotOnline), string.Empty, Color.Red));
                            }
                        }
                        return;
                    case @"close":
                        if (channel[0] == '#')
                            return;
                        RemoveChannel(channel);
                        return;
                    case @"nopm":
                        ConfigManager.sBlockNonFriendPM.Toggle();
                        BanchoClient.SendRequest(RequestType.Osu_UserToggleBlockNonFriendPM, ConfigManager.sBlockNonFriendPM ? 1 : 0);

                        if (ConfigManager.sBlockNonFriendPM)
                            NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.Options_Online_BlockNonFriendPM_Toggle_On), 5000);
                        else
                            NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.Options_Online_BlockNonFriendPM_Toggle_Off), 5000);

                        return;
                    case @"help":
                        activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_Help1), string.Empty, Color.YellowGreen));
                        activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_Help2), string.Empty, Color.LightGray));
                        activeMessageBuffer.Add(new Message(@"/addfriend /delfriend <user>", LocalisationManager.GetString(OsuString.ChatEngine_Help3),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/away <message>", LocalisationManager.GetString(OsuString.ChatEngine_Help4),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/bb", LocalisationManager.GetString(OsuString.ChatEngine_Help5),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/chat <user>", LocalisationManager.GetString(OsuString.ChatEngine_Help6),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/clear", LocalisationManager.GetString(OsuString.ChatEngine_Help7),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/help", LocalisationManager.GetString(OsuString.ChatEngine_Help8), Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/ignore <user>[@chp]", LocalisationManager.GetString(OsuString.ChatEngine_Help9),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(string.Empty, "By adding an @ followed by the letters, c, h, and/or p, you may ignore them in chat, highlights, or PMs respectively.",
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/unignore <user>", LocalisationManager.GetString(OsuString.ChatEngine_Help10),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/keys", LocalisationManager.GetString(OsuString.ChatEngine_Help11), Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/me <action>", LocalisationManager.GetString(OsuString.ChatEngine_Help12),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/np",
                                                            LocalisationManager.GetString(OsuString.ChatEngine_HelpNowPlaying),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/reply or /r",
                                                            LocalisationManager.GetString(OsuString.ChatEngine_HelpReply),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/savelog", LocalisationManager.GetString(OsuString.ChatEngine_HelpSavelog),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/watch <user>", LocalisationManager.GetString(OsuString.ChatEngine_HelpSpectate),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/nopm", LocalisationManager.GetString(OsuString.ChatEngine_HelpDisablePrivateMessage),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"/invite", LocalisationManager.GetString(OsuString.ChatEngine_HelpInvite),
                                                            Color.OrangeRed));



                        return;
                    case @"clear":
                        ClearBuffer(activeMessageBuffer);
                        return;

                    case @"keys":
                        activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_HelpKeys), string.Empty, Color.YellowGreen));
                        activeMessageBuffer.Add(new Message(@"PageUp/PageDown",
                                                            LocalisationManager.GetString(OsuString.ChatEngine_HelpPageUpDown),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"Tab", LocalisationManager.GetString(OsuString.ChatEngine_HelpAutocomplete),
                                                            Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"F8", LocalisationManager.GetString(OsuString.ChatEngine_HelpToggleChat), Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"F9", LocalisationManager.GetString(OsuString.ChatEngine_HelpToggleOnlineUsers), Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"Ctrl-C/V", LocalisationManager.GetString(OsuString.ChatEngine_HelpCopyPaste), Color.OrangeRed));
                        activeMessageBuffer.Add(new Message(@"Alt+0-9", LocalisationManager.GetString(OsuString.ChatEngine_HelpJumpTab), Color.OrangeRed));
                        return;
                    case @"r":
                    case @"reply":
                        {
                            string message = m.Remove(0, m.IndexOf(' ') + 1);

                            if (lastPrivateUser != null)
                            {
                                if (BanchoClient.GetUserByNameEx(lastPrivateUser) != null)
                                {
                                    bMessage msg = new bMessage(null, lastPrivateUser, message);
                                    BanchoClient.SendRequest(RequestType.Osu_SendIrcMessagePrivate, msg);
                                    HandleMessage(msg, true);
                                }
                                else
                                    activeMessageBuffer.Add(new Message(string.Format(LocalisationManager.GetString(OsuString.ChatEngine_LastPrivateMessageWentOffline), lastPrivateUser), string.Empty, Color.Red));
                            }
                            else
                                activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_LastPrivateMessageNotExist), string.Empty, Color.Red));
                        }
                        return;
                    case @"me":
                        if (m.Trim().Length < 5) return;
                        m = '\x1' + @"ACTION " + m.Substring(4) + '\x1';
                        break;
                    case @"j":
                    case @"join":
                        if (arg == null)
                        {
                            GameBase.ShowDialog(new ChannelListDialog());
                            return;
                        }

                        if (!arg.StartsWith(@"#")) arg = @"#" + arg;

                        Channel c = AddChannel(arg);
                        if (c != null)
                            channelTabs.SetSelected(c);
                        return;
                    case @"p":
                    case @"part":
                        if (chan != null && !chan.Closeable)
                            return;
                        if (arg == null) arg = channel;
                        if (!arg.StartsWith(@"#")) arg = @"#" + arg;
                        RemoveChannel(arg);
                        return;
                    default:
                        return;
                }
            }

            if (privateChannel && channel != GameBase.User.Name)
            {
                User found = BanchoClient.GetUserByNameEx(channel);
                if (found != null)
                {
                    BanchoClient.SendRequest(RequestType.Osu_SendIrcMessagePrivate, new bMessage(null, channel, m));
                    if (ConfigManager.sLogPrivateMessages && channel != @"BanchoBot")
                        saveLineToLog(channel, m, true);
                }
                else
                {
                    activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_UserNotOnline), string.Empty, Color.Red));
                }
            }
            else if (channel != "#announce")
            {
                BanchoClient.SendRequest(RequestType.Osu_SendIrcMessage, new bMessage(null, channel, m));
            }

            HandleMessage(new bMessage(ConfigManager.sUsername, channel, m), true);
        }

        internal static bool CheckIgnore(User user)
        {
            if (user == null) return false;

            if (ignoreListChat.Contains(user.Name)) return true;
            if (ignoreListHighlight.Contains(user.Name)) return true;
            if (ignoreListPrivate.Contains(user.Name)) return true;

            return false;
        }

        internal static void UnignoreUser(User user)
        {
            if (user != null)
            {
                bool listChanged = false;

                string underscoredName = user.Name.Replace(' ', '_').ToLower();

                listChanged |= ignoreListChat.Remove(underscoredName);
                listChanged |= ignoreListHighlight.Remove(underscoredName);
                listChanged |= ignoreListPrivate.Remove(underscoredName);

                if (listChanged)
                    activeMessageBuffer.Add(new Message(string.Format(LocalisationManager.GetString(OsuString.ChatEngine_Unignore), user.Name), string.Empty, Color.Yellow));
                else
                    activeMessageBuffer.Add(new Message(string.Format(LocalisationManager.GetString(OsuString.ChatEngine_UnignoreFailed), user.Name), string.Empty, Color.Yellow));
            }
            else
                activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_UserNotOnline), string.Empty, Color.Red));
        }

        internal static void IgnoreUser(User user, string flags = @"cp")
        {
            if (user != null)
            {
                bool listChanged = false;
                string whatChanged = string.Empty;

                if (flags.Contains(@"c") && !ignoreListChat.Contains(user.Name))
                {
                    listChanged = true;
                    ignoreListChat.Add(user.Name.Replace(' ', '_').ToLower());
                    whatChanged += @", chat";
                }

                if (flags.Contains(@"h") && !ignoreListHighlight.Contains(user.Name))
                {
                    listChanged = true;
                    ignoreListHighlight.Add(user.Name.Replace(' ', '_').ToLower());
                    whatChanged += @", highlights";
                }

                if (flags.Contains(@"p") && !ignoreListPrivate.Contains(user.Name))
                {
                    listChanged = true;
                    ignoreListPrivate.Add(user.Name.Replace(' ', '_').ToLower());
                    whatChanged += @", PMs";
                }

                if (listChanged)
                    activeMessageBuffer.Add(new Message(string.Format("You will no longer hear {0}.", user.Name), " {" + whatChanged.Substring(2) + "}", Color.Yellow));
                else
                    activeMessageBuffer.Add(new Message(string.Format(LocalisationManager.GetString(OsuString.ChatEngine_IgnoreFailed), user.Name), string.Empty, Color.Yellow));
            }
            else
                activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_UserNotOnline), string.Empty, Color.Red));
        }

        internal static void StartChat(User user)
        {
            if (user.Id == 2)
            {
                GameBase.ProcessStart(@"http://osu.ppy.sh/p/doyoureallywanttoaskpeppy");
                return;
            }

            Channel c = AddChannel(user.Name, true, false);
            if (c != null)
            {
                c.VirtualChannel = true;

                channelTabs.SetSelected(c);
                activeMessageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.ChatEngine_CloseTab), string.Empty,
                                                    Color.Wheat));
            }
        }

        internal static string[] keySpamCheckArray;
        internal static readonly List<string> localChannelList = new List<string>();
        private pTextBox searchFilter;
        private static pSprite detailsBack;
        private const int clen = 9;
        private static int lastUpdateSortTime;
        private static pSprite modeFilterFruits;
        private static pSprite modeFilterTaiko;
        private static pSprite modeFilterOsu;
        private static pSprite modeFilterMania;
        private static int timeBetweenResorts = 1000;

        internal static void ResetSortTime()
        {
            timeBetweenResorts = 0;
            usersArea.LastScrollTime = -1;
        }

        /// <summary>
        /// Checks input of a message as to whether it is button spam from play mode.
        /// </summary>
        private static bool CheckKeySpam(string m)
        {
            if (GameBase.Mode != OsuModes.Play || InputManager.ReplayMode)
                return false;

            int len = m.Length;
            int count = 0;

            if (keySpamCheckArray == null)
            {
                keySpamCheckArray = new string[9];
                switch (Player.Mode)
                {
                    case PlayModes.Osu:
                        keySpamCheckArray[0] = BindingManager.For(Bindings.OsuLeft).ToString();
                        keySpamCheckArray[1] = BindingManager.For(Bindings.OsuRight).ToString();
                        break;
                    case PlayModes.Taiko:
                        keySpamCheckArray[2] = BindingManager.For(Bindings.TaikoInnerLeft).ToString();
                        keySpamCheckArray[3] = BindingManager.For(Bindings.TaikoInnerRight).ToString();
                        keySpamCheckArray[4] = BindingManager.For(Bindings.TaikoOuterLeft).ToString();
                        keySpamCheckArray[5] = BindingManager.For(Bindings.TaikoOuterRight).ToString();
                        break;
                    case PlayModes.CatchTheBeat:
                        keySpamCheckArray[6] = BindingManager.For(Bindings.FruitsDash).ToString();
                        keySpamCheckArray[7] = BindingManager.For(Bindings.FruitsLeft).ToString();
                        keySpamCheckArray[8] = BindingManager.For(Bindings.FruitsRight).ToString();
                        break;
                }
            }


            for (int i = 0; i < len; i++)
            {
                for (int j = 0; j < clen; j++)
                {
                    if (char.ToUpper(m[i]).ToString() == keySpamCheckArray[j])
                    {
                        count++;
                        break;
                    }
                }
            }

            return count > len / 2;
        }

        private static void ExportLog(string channel)
        {
            if (!Directory.Exists(@"Chat"))
                Directory.CreateDirectory(@"Chat");
            string filename = string.Format(@"Chat\{0}-{1:yyyyMMdd-hhmmss}.txt", Regex.Replace(channel, string.Format(@"[{0}]", Regex.Escape(new string(Path.GetInvalidFileNameChars()))), @"_"), DateTime.Now);
            using (StreamWriter t = File.CreateText(filename))
            {
                string line = string.Empty;
                foreach (Message m in activeMessageBuffer)
                    if (m.FormatterResults != null)
                    {
                        string newLine = string.Format(@"{0} {1}", m.Name, m.FormatterResults.OriginalText);
                        if (newLine == line) continue;

                        line = newLine;
                        t.WriteLine(line);
                    }
            }
            NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.ChatEngine_ExportChat), filename), Color.YellowGreen, 3000, delegate { GameBase.ProcessStart(filename); });
        }

        internal static void saveLineToLog(string username, string message, bool outgoing = false)
        {
            if (!Directory.Exists(@"Chat"))
                Directory.CreateDirectory(@"Chat");
            string filename = string.Format(@"Chat\{0}.txt", Regex.Replace(username, string.Format(@"[{0}]", Regex.Escape(new string(Path.GetInvalidFileNameChars()))), @"_"));

            string formatted;
            if (message.Contains(@" *"))
                formatted = string.Format(@"{0:yyyy-MM-dd HH:mm} {1}" + '\r' + '\n', DateTime.Now, message);
            else
                formatted = string.Format(@"{0:yyyy-MM-dd HH:mm} {1}: {2}" + '\r' + '\n', DateTime.Now, outgoing ? GameBase.User.Name : username, message);

            File.AppendAllText(filename, formatted);
        }

        internal static string[] GenerateNowPlayingComponents()
        {
            string[] ret = new string[3];
            //nowPlaying    == ret[0]
            //version       == ret[1]
            //verb          == ret[2]

            if (BeatmapManager.Current != null)
                ret[0] = BeatmapManager.Current.SortTitle + (GameBase.Mode == OsuModes.Play || GameBase.Mode == OsuModes.Edit ? @" [" + BeatmapManager.Current.Version + @"]" : string.Empty);
            ret[1] = string.Empty;
            ret[2] = @"listening to";

            switch (GameBase.Mode)
            {
                case OsuModes.Play:
                    switch (Player.Mode)
                    {
                        case PlayModes.Osu:
                            break;
                        case PlayModes.Taiko:
                            ret[1] += @" <Taiko>";
                            break;
                        case PlayModes.CatchTheBeat:
                            ret[1] += @" <CatchTheBeat>";
                            break;
                        case PlayModes.OsuMania:
                            ret[1] += @" <osu!mania>";
                            break;
                    }
                    if (Player.currentScore != null)
                    {
                        if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Easy))
                            ret[1] += @" -Easy";
                        if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.NoFail))
                            ret[1] += @" -NoFail";
                        if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Hidden))
                            ret[1] += @" +Hidden";
                        if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Perfect))
                            ret[1] += @" +Perfect";
                        else if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.SuddenDeath))
                            ret[1] += @" +SuddenDeath";
                        if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.HardRock))
                            ret[1] += @" +HardRock";
                        if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Nightcore))
                            ret[1] += @" +Nightcore";
                        else if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.DoubleTime))
                            ret[1] += @" +DoubleTime";
                        if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.HalfTime))
                            ret[1] += @" -HalfTime";
                        if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Flashlight))
                            ret[1] += @" +Flashlight";
                        if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.SpunOut))
                            ret[1] += @" -SpunOut";
                        if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Cinema))
                            ret[1] += @" |Cinema|";
                        else if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Autoplay))
                            ret[1] += @" |Autoplay|";
                        if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Target))
                            ret[1] += @" ~Target~";
                        if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Relax))
                            ret[1] += @" ~Relax~";
                        if (ModManager.CheckActive(Player.currentScore.enabledMods, Mods.Relax2))
                            ret[1] += @" ~Autopilot~";
                        if (Player.currentScore.playMode == PlayModes.OsuMania)
                        {
                            ret[1] += @" |" + StageMania.ColumnsWithMods(BeatmapManager.Current, Player.currentScore.enabledMods) + @"K|";
                        }
                    }
                    ret[2] = InputManager.ReplayMode ? @"watching" : @"playing";
                    break;
                case OsuModes.Edit:
                    ret[2] = @"editing";
                    break;
            }
            return ret;
        }

        internal static string GenerateNowPlayingString(string[] nowPlayingStrings = null)
        {
            if (BeatmapManager.Current == null) return null;

            if (nowPlayingStrings == null)
                nowPlayingStrings = GenerateNowPlayingComponents();

            string nowPlayingNew = nowPlayingStrings[0];
            string versionString = nowPlayingStrings[1];
            string verb = nowPlayingStrings[2];

            string link;
            if (BeatmapManager.Current.BeatmapId > 0)
                link = string.Format(@"[{0}/b/{1} {2}]{3}", General.WEB_ROOT, BeatmapManager.Current.BeatmapId, nowPlayingNew, versionString);
            else if (BeatmapManager.Current.BeatmapSetId > 0)
                link = string.Format(@"[{0}/s/{1} {2}]{3}", General.WEB_ROOT, BeatmapManager.Current.BeatmapSetId, nowPlayingNew, versionString);
            else
                link = nowPlayingNew + versionString;
            return @"/me is " + verb + @" " + link;
        }

        internal static void UpdateNowPlaying(bool sendToChat = false, bool force = false)
        {
            if (BeatmapManager.Current == null) return;

            string[] nowPlayingStrings = GenerateNowPlayingComponents();

            ImHelper.SetAllMusic(true, nowPlayingStrings[2], BeatmapManager.Current.Title, BeatmapManager.Current.Artist,
                                 GameBase.Mode == OsuModes.Play ? BeatmapManager.Current.Version : string.Empty);

            if (sendToChat && (nowPlayingLast != nowPlayingStrings[0] || GameBase.Time - nowPlayingLastTime > 60000))
            {
                nowPlayingLast = nowPlayingStrings[0];

                SendMessage(GenerateNowPlayingString(nowPlayingStrings), false);

                nowPlayingLastTime = GameBase.Time;
            }
        }

        internal static void SendHistoryPrev()
        {
            if (sendBuffer.Count == 0)
                return;

            if (InputManager.TextBox.Text.Length > 0)
            {
                if (historyCurrent == 0 && InputManager.TextBox.Text != sendBuffer[historyCurrent])
                {
                    sendBuffer.Insert(historyCurrent, InputManager.TextBox.Text);
                    historyCurrent++;
                }
                else
                    sendBuffer[historyCurrent] = InputManager.TextBox.Text;
            }
            else
                historyCurrent = Math.Max(-1, historyCurrent - 1);

            historyCurrent = Math.Max(0, Math.Min(sendBuffer.Count - 1, historyCurrent + 1));
            if (historyCurrent >= sendBuffer.Count)
                return;
            if (sendBuffer[historyCurrent] != InputManager.TextBox.Text)
                InputManager.SetFullTextBuffer(sendBuffer[historyCurrent]);
        }

        internal static void SendHistoryNext()
        {
            if (InputManager.TextBox.Text.Length > 0)
            {
                if (historyCurrent == 0 &&
                    (sendBuffer.Count == 0 || InputManager.TextBox.Text != sendBuffer[historyCurrent]))
                    sendBuffer.Insert(historyCurrent, InputManager.TextBox.Text);
                else
                    sendBuffer[historyCurrent] = InputManager.TextBox.Text;
            }
            if (historyCurrent == 0)
            {
                InputManager.SetFullTextBuffer(string.Empty);
                return;
            }
            historyCurrent = Math.Max(0, historyCurrent - 1);
            if (historyCurrent >= sendBuffer.Count)
                return;
            if (sendBuffer[historyCurrent] != InputManager.TextBox.Text)
                InputManager.SetFullTextBuffer(sendBuffer[historyCurrent]);
        }

        public static void HandleUserPresence(int userId)
        {
            lock (BanchoClient.Users)
            {
                User user = BanchoClient.GetUserById(userId, false);

                //prep a new user.
                if (user == null)
                    BanchoClient.AddUser((user = new User(userId)));

                if (Friends.Contains(user.Id))
                    user.RequestPresence();
            }
        }


        public static void HandleUserPresence(bUserPresence presence)
        {
            PresenceCache.Cache(presence);

            lock (BanchoClient.Users)
            {
                User user = BanchoClient.GetUserById(presence.userId);
                if (user != null)
                    user.ReceivePresence(presence);
                else
                    BanchoClient.AddUser(new User(presence));
            }
        }

        public static void CheckFriend(User user)
        {
            //todo: change this lookup to cause less burden with large friends lists.
            if (Friends.Contains(user.Id))
            {
                user.IsFriend = true;

                friendAlertLastOnline = GameBase.Time;
                if (friendAlertOfflineQueue.Contains(user.Name))
                    friendAlertOfflineQueue.Remove(user.Name);
                else if (!friendAlertOnlineQueue.Contains(user.Name))
                    friendAlertOnlineQueue.Add(user.Name);
            }
        }

        public static void HandleUserUpdate(bUserStats stats)
        {
            User user = BanchoClient.GetUserById(stats.userId);
            if (user != null)
                user.ReceiveUserStats(stats);
        }

        public static void HandleUserQuit(int id, QuitNewState state)
        {
            User user = BanchoClient.GetUserById(id, false);

            if (user != null)
            {
                switch (state)
                {
                    case QuitNewState.IrcRemaining:
                        user.IsOsu = false;
                        break;
                    case QuitNewState.OsuRemaining:
                        user.IsOsu = true;
                        break;
                    case QuitNewState.Gone:
                        if (user.IsFriend)
                        {
                            friendAlertLastOffline = GameBase.Time;
                            if (friendAlertOnlineQueue.Contains(user.Name))
                                friendAlertOnlineQueue.Remove(user.Name);
                            if (!friendAlertOfflineQueue.Contains(user.Name))
                                friendAlertOfflineQueue.Add(user.Name);
                        }

                        if (user.Sprites != null)
                        {
                            foreach (pSprite p in user.Sprites)
                            {
                                if (user.Visible)
                                    p.FadeOut(500);
                                else
                                    p.Alpha = 0;
                                p.AlwaysDraw = false;
                            }
                        }

                        BanchoClient.RemoveUser(user);
                        break;
                }

                //osu! client is gone. Handle clean-up for stuff osu!-specific.
                if (state == QuitNewState.Gone || state == QuitNewState.IrcRemaining)
                {
                    if (StreamingManager.CurrentlySpectating == user)
                        lock (StreamingManager.LockReplayScore)
                            StreamingManager.StopSpectating(true);
                }
            }
        }

        public static void ResetUsers()
        {
            usersArea.ClearSprites(true);

            lock (BanchoClient.Users) BanchoClient.Users.Clear();
            BanchoClient.RemovedUsers = 0;
            BanchoClient.UserDictionary.Clear();

            lastUpdateSortTime = 0;
        }

        public static void ClearChannels()
        {
            Channel[] channela = new Channel[channels.Count];
            channels.CopyTo(channela, 0);
            for (int i = channela.Length - 1; i >= 0; i--)
            {
                Channel c = channela[i];

                if (c.Name == @"#multiplayer" || c.Name == @"#spectator")
                {
                    channelTabs.Remove(c);
                    channels.Remove(c);
                }
            }
        }

        public static void HandleFriendsList(List<int> friends)
        {
            Friends.AddRange(friends);
        }

        public static void LoadHighlights()
        {
            HighlightList.Clear();
            foreach (string s in ConfigManager.sHighlightWords.Value.Split(' '))
                if (s.Length > 0 && !HighlightList.Contains(s))
                    HighlightList.Add(s);
        }

        public static void LoadIgnoreLists()
        {
            ignoreListChat.Clear();
            ignoreListHighlight.Clear();
            ignoreListPrivate.Clear();
            foreach (string s in ConfigManager.sIgnoreList.Value.Split(' '))
            {
                if (s.Length == 0) continue;

                string[] split = s.Split('@');

                if (split.Length < 2)
                    split = new string[] { split[0], @"cp" };

                if (split[1].Contains(@"c") && !ignoreListChat.Contains(split[0]))
                    ignoreListChat.Add(split[0].ToLower());

                if (split[1].Contains(@"h") && !ignoreListHighlight.Contains(split[0]))
                    ignoreListHighlight.Add(split[0].ToLower());

                if (split[1].Contains(@"p") && !ignoreListPrivate.Contains(split[0]))
                    ignoreListPrivate.Add(split[0].ToLower());
            }
        }

        public static void LoadSettings()
        {
            LoadHighlights();

            localChannelList.Clear();
            foreach (string s in ConfigManager.sChatChannels.Value.Split(' '))
                if (s.Length > 0 && !localChannelList.Contains(s))
                    localChannelList.Add(s);
            if (!localChannelList.Contains(@"#osu"))
                localChannelList.Insert(0, @"#osu");

            LoadIgnoreLists();
        }

        protected override void Dispose(bool disposing)
        {
            chatArea.Dispose();
            usersArea.Dispose();
            base.Dispose(disposing);
        }

        internal static void OnResolutionChange()
        {
            userList.VectorScale = new Vector2(GameBase.WindowWidthScaled * 1.6f, 22);
            channelTabs.SetMaxTabsWide((int)(GameBase.WindowWidth / (pTab.TAB_WIDTH * GameBase.WindowRatio)));
            detailsBack.VectorScale = new Vector2(GameBase.WindowWidthScaled, 320) * 1.6F;
            tickerText.TextBounds = new Vector2(GameBase.WindowWidthScaled, 13.5f);
            tickerText.TextBounds = new Vector2(GameBase.WindowWidthScaled, 13.5f);
            chatBg.VectorScale = new Vector2(GameBase.WindowWidthScaled, 160) * 1.6F;

            ResetMessages();
        }

        /// <summary>
        /// restore channel ordering from config file
        /// </summary>
        internal static void LoadUserChannelOrder()
        {
            List<pTab> newOrdering = new List<pTab>();

            foreach (string s in localChannelList)
            {
                pTab tab = channelTabs.Tabs.Find(t => t.tabText.Text == s);
                if (tab != null)
                    newOrdering.Add(tab);
            }

            foreach (pTab tab in channelTabs.Tabs)
                if (!newOrdering.Contains(tab))
                    newOrdering.Add(tab);

            channelTabs.Tabs = newOrdering;
            channelTabs.Resort();
        }

        internal static void RemoveUserMessages(int userId)
        {
            foreach (Channel c in channels)
            {
                if (c.messageBuffer == activeMessageBuffer)
                {
                    for (int i = 0; i < activeMessageBuffer.Count; ++i)
                    {
                        Message m = activeMessageBuffer[i];
                        if (m.User != null && m.User.Id == userId)
                        {
                            RemoveMessage(i);
                            --i;
                        }
                    }
                }
                else
                {
                    c.messageBuffer.RemoveAll(m => m.User != null && m.User.Id == userId);
                }
            }
        }
    }

    internal enum UserSortMode
    {
        Name,
        Rank,
        Country,
        WorldMap,
        Timezone

    };

    internal enum UserFilterType
    {
        All,
        Friends,
        Country,
        Near
    };
}
