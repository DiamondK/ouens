using System;
using Microsoft.Xna.Framework.Graphics;
using osu.Online.Drawable;
using osu.Online.Social;
using osu_common.Helpers;
using Microsoft.Xna.Framework;

namespace osu.Online.Social
{
    internal class Message
    {
        internal Color Colour;
        internal string Content;
        internal string Name;
        internal DateTime Time = DateTime.Now;
        internal User User;
        internal LinkFormatterResult FormatterResults;

        private ChatLine chatLine = null;
        internal ChatLine ChatLine
        {
            get
            {
                if (chatLine == null)
                {
                    chatLine = new ChatLine(Vector2.Zero, 0.97f, ChatEngine.FONT_SIZE, GameBase.WindowWidthScaled - 10, this, ChatEngine.FontFace);
                }

                return chatLine;
            }

            set
            {
                chatLine = value;
            }
        }

        internal bool HasChatLine
        {
            get
            {
                return chatLine != null;
            }
        }

        internal Message(User user, string name, string message, Color colour)
        {
            User = user;
            Name = name;
            Content = message;
            Colour = colour;
        }

        internal Message(string name, string message, Color colour)
        {
            Name = name;
            Content = message;
            Colour = colour;
        }
    }
}