using MonoTorrent.Common;

namespace osu.Online.P2P
{
    internal delegate void TorrentEventHandler(TorrentState oldState, TorrentInfo e);

    internal delegate void MagnetLoadSucceeded(bool succeeded, string key);

    internal delegate void MainLoopStarted();

    internal delegate void DisplayInfoUpdated();

    //null if not succeeded
    internal delegate void GotOsz2Hashes(byte[] bodyHash, byte[] headerHash);

    //null if not succeeded
    internal delegate void GotOsuMagnet(OsuMagnet magnet);
    
}