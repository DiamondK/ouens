﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using osu.Configuration;
using osu_common.Libraries.NetLib;
using osu_common.Libraries.Osz2;

namespace osu.Online.P2P
{
    static class OsuMagnetFactory
    {
        internal static OsuMagnet LoadCheckSaneCases (string pathFile)
        {
            string fileName = Path.GetFileName(pathFile);

            /*
            if (Path.GetDirectoryName(pathFile) == magnetsPathFinished)
            {
                showMessage("This beatmap already has been downloaded");
                return null;
            }*/
            if (Path.GetDirectoryName(pathFile) == Osup2pManager.magnetsPathUnfinished)
            {
                Osup2pManager.showMessage("You are already downloading this beatmap");
                return null;
            }
            OsuMagnet potentialDownload = null;
            try
            {
                potentialDownload = new OsuMagnet(pathFile);
            }
#if !DEBUG
            catch (Exception e)
#else
            catch (AbandonedMutexException e) //we don't catch anything
#endif
            {
                Osup2pManager.showMessage(String.Format("the osuMagnet file {0} seems to be corruped, please redownload.", fileName));
                return null;
            }
            return potentialDownload;
        }


        internal static void DownloadOsuMagnet(int beatmapID,bool noVideo, GotOsuMagnet callWhenReceived)
        {
            DataNetRequest getOsuMagnet = new DataNetRequest(String.Format(Urls.OSZ2_GET_MAGNET, beatmapID, noVideo ? 1 : 0, ConfigManager.sUsername, ConfigManager.sPassword));
            getOsuMagnet.onFinish += (D, E) =>
            {
                if (D == null || E != null)
                    callWhenReceived(null);

                try
                {
                    OsuMagnet osuMagnet;
                    using (MemoryStream donwloadStream = new MemoryStream(D, false))
                        osuMagnet = new OsuMagnet(donwloadStream);

                    //now we write the osuMagnet to the magnetCache
                    string pathFile = Path.Combine(Osup2pManager.magnetsPathUnfinished, osuMagnet.InternalMagnetName);
                    using (FileStream file = new FileStream(pathFile, FileMode.Create))
                        file.Write(D, 0, D.Length);

                    osuMagnet.PathFile = pathFile;
                    callWhenReceived(osuMagnet);
                }
                catch
                {
                     callWhenReceived(null);
                }
            };
            NetManager.AddRequest(getOsuMagnet);
        }

        //for testing purposes only
        static internal void GenerateMagnetsAndTorrent (string osz2File)
        {
            string osz2FilePath = Path.GetFullPath(osz2File);
            MapPackage beatmap = new MapPackage(osz2FilePath, null, false, true);
            bool hasVideo = !string.IsNullOrEmpty(beatmap.GetMetadata(MapMetaType.VideoDataLength));
            string outputPath = Path.Combine(Osup2pManager.magnetsPathFinished, Path.GetFileName(osz2File));

            //create video-magnet or normal magnet if there is no video.
            streamToFile(OsuMagnet.Create(osz2FilePath, false),  outputPath+".OsuMagnet");
            streamToFile(OsuMagnet.CreateTorrent(osz2FilePath), outputPath + ".torrent");

            Debug.Print(BitConverter.ToString(beatmap.hash_body));

            if (hasVideo)
                streamToFile(OsuMagnet.Create(osz2FilePath, true), outputPath.Replace(".osz2", "-novid.osz2") + ".OsuMagnet");
        }

        static private void streamToFile (Stream input, string outputPathFile)
        {
            using (Stream output = new FileStream(outputPathFile, FileMode.Create))
            {
                byte[] buffer = new byte[input.Length];
                input.Read(buffer, 0, buffer.Length);
                output.Write(buffer, 0, buffer.Length);
            }
        }

        //should actually moved to a osz2 helper class
        static public void WebGetOz2Hashes(int beatmapID, GotOsz2Hashes callWhenReceived)
        {
            //do some web query stuff in a seperate thread
            StringNetRequest getBodyHash = new StringNetRequest(String.Format(Urls.OSZ2_GET_HASHES, beatmapID));
            getBodyHash.onFinish += (D, E) =>
            {
                string[] hashes = D.Split('|');
                if (String.IsNullOrEmpty(D) || D[0] != '1' || hashes.Length != 3 || hashes[1].Length != 32)
                {
#if P2P
                Osup2pManager.runMainThread.Add(() => callWhenReceived(null, null));
#else
                GameBase.Scheduler.Add(() => callWhenReceived(null,null));
#endif
                return;
                }

                string bodyHashS = hashes[1];
                string headerHashS = hashes[2];
                byte[] bodyHash = new byte[16];
                byte[] headerHash = new byte[16];
                for (int i = 0; i < 32; i += 2)
                {
                    bodyHash[i / 2] = Convert.ToByte(bodyHashS.Substring(i, 2), 16);
                    headerHash[i / 2] = Convert.ToByte(headerHashS.Substring(i, 2), 16);
                }
#if P2P
                Osup2pManager.runMainThread.Add(() => callWhenReceived(bodyHash, headerHash));
#else
                GameBase.Scheduler.Add(() => callWhenReceived(bodyHash, headerHash));
#endif
            };
            NetManager.AddRequest(getBodyHash);
        }


    }
}
