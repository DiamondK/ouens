﻿using System;
using System.Collections.Generic;
using System.Text;
using MonoTorrent.Client;
using MonoTorrent.Client.Encryption;

namespace osu.Online.P2P
{
    enum EncryptionMode
    {
        Forced,
        Passive,
        Disallowed // for performance reasons
    }


    //todo make serializable
    internal class P2PSettings
    {
        //bytes a second
        internal int maxUpload;
        internal int maxDownload;

        internal EncryptionMode encryptionMode;
        internal int maxConnections;

        internal int portRangeMin;
        internal int portRangeMax;

        //bytes a second
        internal int maxIOBandwidth;

        
        internal P2PSettings()
        {
            // 0 means unlimited
            maxUpload = 0;
            maxDownload = 0;
            maxIOBandwidth = Int16.MaxValue;
            maxConnections = 200;
            portRangeMin = 5000;
            portRangeMax = 10000;
            encryptionMode = EncryptionMode.Passive;
        }

        internal EngineSettings EngineSettings
        {
            get
            {
                var foo = new EngineSettings
                    (Osup2pManager.DownloadsPath,
#if DEBUG
                     13333,
#else
                     Osup2pManager.random.Next(portRangeMin, portRangeMax),
#endif
                     maxConnections,
                     Math.Max(maxConnections /20, 10),
                     maxDownload,
                     maxUpload,
                     encryptionMode == EncryptionMode.Disallowed
                         ?
                             EncryptionTypes.None
                         :
                             (encryptionMode == EncryptionMode.Forced
                                  ?
                                      EncryptionTypes.RC4Full
                                  : EncryptionTypes.All))
                              {
                                  PreferEncryption = encryptionMode != EncryptionMode.Disallowed,
                                  MaxReadRate = maxIOBandwidth,
                                  MaxWriteRate = maxIOBandwidth
                              };

                return foo;
            }
        }

        /*internal TorrentSettings genTorrentSettings(int seeds, int downloads, bool isDLTorrent)
    {
        return new 
            TorrentSettings(maxConnections / 4 * 3, maxConnections, maxDownload, maxUpload);
    }*/
    }
}
