﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using MonoTorrent.Common;
using osu_common.Helpers;
using osu_common.Libraries.Osz2;
using FileInfo = osu_common.Libraries.Osz2.FileInfo;

namespace osu.Online.P2P
{
    class OsuMagnet
    {
        private static readonly byte[] KEY = new byte[]
                                                 {
                                                    250, 255, 216, 196, 54, 183, 139, 63, 104, 8, 64, 12, 54, 140, 90, 71,
                                                    132, 136, 94, 213, 92, 138, 125, 58, 119, 205, 140, 10, 250, 43
                                                 };

        internal Torrent InternalTorrent { get; private set; }
        internal bool BeatmapInstalled { get; private set; }
        internal bool BeatmapMagnetSynced { get; private set; }
        //internal int MagnetVersion { get; private set; }
        //internal uint BeatmapVersion { get; private set; }
        internal byte[] MagnetBodyHash { get; private set; }
        internal byte[] MagnetHeaderHash { get; private set; }
        internal byte[] BeatmapBodyHash { get; private set; }
        internal byte[] BeatmapHeaderHash { get; private set; }

        internal string BeatmapFilename { get; private set; }
        internal string BeatmapName { get; private set; }
        internal int BeatmapID { get; private set; }
        private int beatmapDataOffset;
        private int videoDataOffset;
        private int videoDataLength;
        private long fileSize;
        internal List<FileInfo> BeatmapFileIndex { get; private set; }
        private MapPackage InternalBeatmap;
        internal bool NoVideoVersion { get; private set; }
        internal string InternalMagnetName { get { return InternalTorrent.Name + ".OsuMagnet"; } }
        
        internal string PathFile = null;
        internal readonly bool FromFile;

        internal OsuMagnet(string pathFile)
        {
            PathFile = pathFile;
            FromFile = true;
            using (FileStream fileStream = new FileStream(pathFile, FileMode.Open, FileAccess.Read))
            {
                OsuMagnetInitialize(fileStream, Path.GetFileName(pathFile));
            }
        }

        internal OsuMagnet(Stream stream)
        {
            FromFile = false;
            OsuMagnetInitialize(stream,  string.Empty);
        }


        private void OsuMagnetInitialize(Stream fileStream, string fileName)
        {

            //unencrypt here

            Stream unencrypted = new FastEncryptorStream(fileStream,EncryptionMethod.XXTEA,KEY);
            SerializationReader reader = new SerializationReader(unencrypted);
            //torrent file specifications is maintained for convenience
            InternalTorrent = Torrent.Load(reader.ReadByteArray());

            /*if ((InternalTorrent.Name.ToLower() + ".osumagnet") 
                != Path.GetFileName(pathFile).ToLower())
                throw new ArgumentException("osumagnet file should have the same name as the internal beatmap file.");*/

            //osu! client decides what tracker to connect to
            //this makes it easier if we transfer the tracker to an other dns or split it.
            InternalTorrent.AnnounceUrls.Clear();
            MonoTorrentCollection<string> announceUrl = new MonoTorrentCollection<string> { "http://127.0.0.1:6969/announce" };
            InternalTorrent.AnnounceUrls.Add(announceUrl);
            // HashAlgoFactory.
            if (InternalTorrent.Files.Length == 0)
                throw new Exception("osuMagnet should have more than one file stored");

            fileSize = InternalTorrent.Files[0].Length;
            //MagnetVersion = (uint)reader.ReadInt32();
            MagnetBodyHash = reader.ReadByteArray();
            MagnetHeaderHash = reader.ReadByteArray();
            BeatmapFilename = InternalTorrent.Name;

            BeatmapMagnetSynced = true;
            string osz2PathFile = Path.Combine(Osup2pManager.Songsfolder,
                                                   BeatmapFilename);
            //todo: find beatmapinfo and file
            try
            {
                
                InternalBeatmap = Osz2Factory.TryOpen(osz2PathFile, false, true);
                if (InternalBeatmap==null)
                {
                    AutoResetEvent waitOnFileOpen = Osz2Factory.TryGetWaitHandle(osz2PathFile);
                    if (waitOnFileOpen!=null && !waitOnFileOpen.WaitOne(300))
                        throw new Exception();
                    //try one more time after waiting until filelock diappeared
                    InternalBeatmap = Osz2Factory.TryOpen(osz2PathFile, false, true);
                }

                if (InternalBeatmap != null)
                {
                    BeatmapHeaderHash = InternalBeatmap.hash_meta;
                    BeatmapBodyHash = InternalBeatmap.hash_body;
                    BeatmapInstalled = true;
                    for (int i = 0; i < 16; i++)
                        if (BeatmapBodyHash[i] != MagnetBodyHash[i] ||
                            BeatmapHeaderHash[i] != MagnetHeaderHash[i])
                        {
                            BeatmapMagnetSynced = false;
                            break;
                        }
                }
                else
                {
                    BeatmapMagnetSynced = false;
                    BeatmapInstalled = false;
                }
            }
#if DEBUG && bla
            catch (AbandonedMutexException e) //we don't catch anything
#else
            catch (Exception e)
#endif
            {
                BeatmapMagnetSynced = false;
                BeatmapInstalled = false;
            }
            finally
            {
                if (InternalBeatmap != null)
                {
                    try
                    {
                        Osz2Factory.CloseMapPackage(InternalBeatmap);
                        InternalBeatmap = null;
                    }
                    catch
                    {
                    }
                }

            }

            BeatmapName = reader.ReadString();
            BeatmapID = reader.ReadInt32();
            NoVideoVersion = reader.ReadBoolean();
            //todo: I should wrap arround torrent instead..
            InternalTorrent.Comment = BeatmapName;

            BeatmapFileIndex = reader.ReadBList<FileInfo>() as List<FileInfo>;
            beatmapDataOffset = reader.ReadInt32();
            videoDataOffset = reader.ReadInt32();
            videoDataLength = reader.ReadInt32();

            //if novideo mode we'll make sure that the torrent doesn't download or seed video stuff
            if (NoVideoVersion && videoDataOffset !=-1)
            {
                int totalBlocksSize = InternalTorrent.Pieces.Count*InternalTorrent.PieceLength;
                int videoPieceStart = videoDataOffset/InternalTorrent.PieceLength +1;
                int videoPieceEnd = (videoDataOffset + videoDataLength)/InternalTorrent.PieceLength -1;
                InternalTorrent.IgnorePiecesRange(videoPieceStart, videoPieceEnd);
            }
        }
        private void ProcessRelatedBeatmapinfo()
        {
            BeatmapFilename = "";
            //todo: find out what I'm supposed to do here
        }


        internal static Stream Create(string beatmapPathFile, bool NoVideoVersion)
        {
            Stream magnet = CreateTorrent(beatmapPathFile);
            
            byte[] magnetBytes = new byte[magnet.Length];
            magnet.Read(magnetBytes, 0, (int)magnet.Length);

            //string torrentEncoded = reader.ReadToEnd();
 
            magnet.Flush();
            magnet.SetLength(0);
            Stream encryptor = new FastEncryptorStream(magnet, EncryptionMethod.XXTEA, KEY);
            SerializationWriter writer = new SerializationWriter(encryptor);
            writer.Write(magnetBytes);
            MapPackage MP = Osz2Factory.TryOpen(beatmapPathFile, false);
            if (MP == null)
                throw new Exception("Osz2 was locked while trying to create a magnet.");

            writer.Write(MP.hash_body);
            writer.Write(MP.hash_meta);
            //get beatmap metadata and write to magnet
            string title = MP.GetMetadata(MapMetaType.Title);
            writer.Write(title ?? "Title Placeholder");
            writer.Write(Convert.ToInt32(MP.GetMetadata(MapMetaType.BeatmapSetID)?? "83"));
            writer.Write(NoVideoVersion);

            writer.Write<FileInfo>(MP.GetFileInfo());
            writer.Write(MP.DataOffset);
            int videoDataOffset;
            int videoDataLength;
            MP.getVideoOffset(out videoDataOffset, out videoDataLength);
            writer.Write(videoDataOffset);
            writer.Write(videoDataLength);
            Osz2Factory.CloseMapPackage(MP);
            magnet.Seek(0, SeekOrigin.Begin);
            //compress and encrypt here
            return magnet;

        }
        internal static Stream CreateTorrent(string beatmapPathFile)
        {
            
            //generate native torrentfile in memory
            TorrentCreator TC = new TorrentCreator();
            TC.PieceLength = TC.RecommendedPieceSize();
            TC.Path = beatmapPathFile;
            TC.PieceLength = 512000;
            //we need an empty announce to make the internal torrent parse it right
            //TC.Announces.Add(new List<string>(new[] { General.WEB_ROOT + ":13382/announce" }));
            TC.Announces.Add(new List<string>(new[] { "http://127.0.0.1:6969/announce" }));
            Stream magnet = new MemoryStream();
            TC.Create(magnet);
            magnet.Seek(0, SeekOrigin.Begin);
            return magnet;
        }

        internal void MakeUpdateReady()
        {
            string beatmapFile = Path.Combine(Osup2pManager.Songsfolder, InternalTorrent.Name);
            string beatmapFileDest = Path.Combine(Osup2pManager.DownloadsPath, InternalTorrent.Name);
            if (!File.Exists(beatmapFile))
            {
                if (BeatmapInstalled)
                    throw new Exception("beatmap should be in the songs folder to be applicable for update.");
                else
                    throw new Exception("Beatmap can't be updated as the corresponding beatmap doesn't exist.");
            }

            //close current beatmap reference so we can move it and p2p can update it.
            //Osz2Factory.CloseMappackage(InternalBeatmap);
            InternalBeatmap = null;

            //if there is an exception we let the caller catch it.
            if (File.Exists(beatmapFileDest))
                File.Delete(beatmapFileDest);
            File.Copy(beatmapFile, beatmapFileDest);
            MapPackage.Pad(beatmapFileDest, BeatmapFileIndex, beatmapDataOffset, fileSize);
            
            
            
        }
    }
}
