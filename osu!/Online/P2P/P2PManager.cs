using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using MonoTorrent.Common;
using MonoTorrent.Client;
using System.Net;
using System.Threading;
using MonoTorrent.BEncoding;
using MonoTorrent.Client.Tracker;
using MonoTorrent.Dht;
using MonoTorrent.Dht.Listeners;
using osu.Graphics.Notifications;
using osu.Helpers;
using osu_common.Helpers;
using osu_common.Libraries.Osz2;

namespace osu.Online.P2P
{
    static class Osup2pManager
    {
        static string dhtNodeFile;
        static string basePath;
        internal static string DownloadsPath { get; private set; }
        static string fastResumeFile;
        static internal string magnetsPathUnfinished;
        static internal string magnetsPathFinished;
        //thsis one should actually be in osu!constants but zzZzzz
        static internal string Songsfolder { get; private set; }

        static ClientEngine engine;				// The engine used for downloading
        static List<TorrentManager> leechTorrents;
        static List<TorrentManager> seedTorrents;

        static List<string> possibleShares; //filenames
        static List<TorrentManager> moveQueue;
        static List<TorrentManager> removeQueue;
        static readonly object moveQueueLock = new object();
        static private P2PSettings settings;
        static private TorrentSettings leechSettings = new TorrentSettings();
        static private TorrentSettings seedSettings = new TorrentSettings();


        static internal readonly Random random = new Random(); //not thread-safe
        static internal Osup2pDisplay Info { get; private set; } //thread-safe
        static internal bool Activated { get; private set; }
        static internal bool MainLoopActive { get { return runMainThread != null; } }

        static SleepHandle sleepHandle = new SleepHandle();
        internal static SleepScheduler runMainThread;

        static internal TorrentEventHandler torrentStatusChanged = delegate { };
        static internal MainLoopStarted onMainLoopStarted = delegate { };
        //should not be null on start
        //todo: in retospect starting a complete thread to avoid null problems is kinda silly
        static private Thread downloadManager = new Thread(new ThreadStart(delegate { }));



        internal static void Initialize(P2PSettings setup)
        {
            /* Generate the paths to the folder we will save .magnet files to and where we download files to */
            basePath = Path.Combine(Environment.CurrentDirectory, "Downloads");
            string dataPath = Path.Combine(Environment.CurrentDirectory, "Data");
            magnetsPathUnfinished = Path.Combine(basePath, "Unfinished");
            magnetsPathFinished = Path.Combine(basePath, "MagnetCache");
            DownloadsPath = Path.Combine(magnetsPathUnfinished, "Downloads");
            fastResumeFile = Path.Combine(dataPath, "ResumeIndex.dat");
            dhtNodeFile = Path.Combine(dataPath, "DhtNodes.dat");
            Songsfolder = Path.Combine(Environment.CurrentDirectory, GameBase.BeatmapDirectory);

            if (!Directory.Exists(magnetsPathFinished))
                Directory.CreateDirectory(magnetsPathFinished);
            if (!Directory.Exists(DownloadsPath))
                Directory.CreateDirectory(DownloadsPath);
            if (!Directory.Exists(Songsfolder))
                Directory.CreateDirectory(Songsfolder);

            leechTorrents = new List<TorrentManager>();
            seedTorrents = new List<TorrentManager>();
            moveQueue = new List<TorrentManager>();
            settings = setup;

            // if we are leeching we expect other seeders to play or testplay once
            // a while and pause the connnection for a few minutes
            leechSettings.TimeToWaitUntilIdle = new TimeSpan(0, 0, 500);
            leechSettings.EnablePeerExchange = true;

            //while seeding we give priority to leechers who are not playing.
            seedSettings.TimeToWaitUntilIdle = new TimeSpan(0, 0, 100);
            //seedSettings.InitialSeedingEnabled = true;

            Activated = false;

#if DEBUG
            OsuMagnetFactory.GenerateMagnetsAndTorrent("27332.osz2");

#endif

            StartEngine();
        }

        private static void StartEngine()
        {
            /*  int port;
              Torrent torrent = null;
              // Ask the user what port they want to use for incoming connections
              Console.Write(Environment.NewLine + "Choose a listen port: ");
              while (!Int32.TryParse(Console.ReadLine(), out port)) { }*/
            Torrent torrent = null;


            FastEncryptionProvider Encryptor = new FastEncryptionProvider();
            Encryptor.SetKey(KEY, EncryptionMethod.XTEA);
            // Create an instance of the engine.
            engine = new ClientEngine(settings.EngineSettings);
            engine.ChangeListenEndpoint(new IPEndPoint(IPAddress.Any, engine.Settings.ListenPort));
            Info = new Osup2pDisplay(leechTorrents, engine);
            byte[] nodes = null;
            try
            {
                nodes = File.ReadAllBytes(dhtNodeFile);
                Encryptor.Decrypt(nodes);
            }
            catch
            {
                showMessage("No existing dht nodes could be loaded");
            }

            //we use the same port for both connectiontypes (this is allowed)
            DhtListener dhtListner = new DhtListener(new IPEndPoint(IPAddress.Any, engine.Settings.ListenPort));
            DhtEngine dht = new DhtEngine(dhtListner);
            engine.RegisterDht(dht);
            dhtListner.Start();
            engine.DhtEngine.Start(nodes);


            BEncodedDictionary fastResume;
            try
            {
                byte[] fastresumeUnencrypted = File.ReadAllBytes(fastResumeFile);
                Encryptor.Decrypt(fastresumeUnencrypted);
                fastResume = BEncodedValue.Decode<BEncodedDictionary>(fastresumeUnencrypted);
            }
            catch
            {
                showMessage("could not load fastResume file");
                fastResume = new BEncodedDictionary();
            }
            TorrentManager manager = null;
            //possible complete beatmaps to share/seed.
            possibleShares = new List<string>(Directory.GetFiles(magnetsPathFinished, "*.OsuMagnet"));
            // remove absolute filepaths
            possibleShares = possibleShares.ConvertAll(S => Path.GetFileName(S));

            // For each file in the torrents path that is a .OsuMagnet file, load it into the engine.
            // these are incomplete beatmap downloads from the previous osu! session
            foreach (string file in Directory.GetFiles(magnetsPathUnfinished))
            {
                if (file.ToLower().EndsWith(".osumagnet"))
                {
                    try
                    {
                        // Load the .OsuMagnet from the file into a Torrent instance
                        torrent = new OsuMagnet(file).InternalTorrent;
#if DEBUG
                        showMessage(torrent.InfoHash.ToString());
#endif
                    }
#if !DEBUG
                    catch (Exception e)
#else
                    catch (AbandonedMutexException e) //we don't catch anything
#endif
                    {
                        showMessage(String.Format("Couldn't decode {0}.\nPlease redownload. ", file));
                        continue;
                    }

                    manager = new TorrentManager(torrent, DownloadsPath, leechSettings);
                    if (fastResume.ContainsKey(torrent.InfoHash.ToHex()))
                        manager.LoadFastResume(new FastResume((BEncodedDictionary)fastResume[torrent.InfoHash.ToHex()]));
                    engine.Register(manager);

                    // Store the torrent manager in our list so we can access it later
                    leechTorrents.Add(manager);
                    //if we already have the beatmap we should not be able to seed it while updating
                    possibleShares.Remove(torrent.Name + ".osuMagnet");

                    Info.AddTorrent(manager, false);
                    manager.PeersFound += ManagerPeersFound;
                    OsuDirect.StartDownload(new P2PDirectDownload(torrent.Name, false));
                }
            }

#if P2P && CRASHTEST
            ThreadStart TestSleepScheduler =
            () =>
            {
                int counterA = 0;
                int counterB = 0;
                bool run = false;
                SleepScheduler savedRun = null;
                while (true)
                {
                    if (MainLoopActive)
                    {
                        savedRun = runMainThread;
                        run = true;
                        counterA++;
                        savedRun.Add(() =>
                        {
                            Thread.SpinWait(110000/40);
                            counterB++;
                        });
                    }
                    else
                    {
                        if (run)
                            break;
                    }
                    Thread.Sleep(1);
                }

                if (counterA != counterB)
                    Console.Write("breakpointME");
            };

            for (int i = 0; i < 40; i++)
                new Thread(TestSleepScheduler).Start();

#endif

        }

        public static void ChangeSettings(P2PSettings setup)
        {
            engine.Settings = setup.EngineSettings;
        }


        public static void LoadMagnet(string pathFile, MagnetLoadSucceeded CallbackFinished)
        {
            runMainThread.Add(() =>
            {
                OsuMagnet OM = OsuMagnetFactory.LoadCheckSaneCases(pathFile);
                LoadMagnet(OM, CallbackFinished);
            });
        }

        public static void LoadMagnet(OsuMagnet OM, MagnetLoadSucceeded CallbackFinished)
        {
            runMainThread.Add(() =>
            {
                if (OM == null)
                {
                    GameBase.Scheduler.Add(() => CallbackFinished(false, ""));
                    return;
                }

                OsuMagnetFactory.WebGetOz2Hashes(OM.BeatmapID, (BodyHash, HeaderHash) =>
                {
                    // is uptodate?
                    if (BodyHash != null && (!Toolbox.ByteMatch(OM.MagnetBodyHash, BodyHash) ||
                                             !Toolbox.ByteMatch(OM.MagnetHeaderHash, HeaderHash)))
                    {

                        //the magnet we are currently opening is not uptodate
                        //so we redownload
                        OsuMagnetFactory.DownloadOsuMagnet(OM.BeatmapID, OM.NoVideoVersion,
                        (OMre) => runMainThread.Add(() =>
                        {
                            if (OMre != null && loadMagnetInternal(OMre, BodyHash) != null)
                                GameBase.Scheduler.Add(() => CallbackFinished(true, OMre.BeatmapFilename));
                            else
                                GameBase.Scheduler.Add(() => CallbackFinished(false, ""));
                        }));
                        return;
                    }



                    if (OM == null || BodyHash == null)
                    {
                        showMessage("Could not retrieve the beatmap information from the server.");
                        GameBase.Scheduler.Add(() => CallbackFinished(false, ""));
                    }

                    //appears to be uptodate : lets try to start the magnet.
                    if (OM != null && BodyHash != null && loadMagnetInternal(OM, BodyHash) != null)
                        GameBase.Scheduler.Add(() => CallbackFinished(true, OM.BeatmapFilename));
                    else
                        GameBase.Scheduler.Add(() => CallbackFinished(false, ""));
                });

            });
        }



        private static OsuMagnet loadMagnetInternal(OsuMagnet potentialDownload, byte[] bodyHashLastestVersion)
        {

            bool updating = false;

            //are we already downloading this beatmap?
            //todo check if name property adds file extension
            if (leechTorrents.Exists(T => T.Torrent == potentialDownload.InternalTorrent ||
                                          T.Torrent.Name == potentialDownload.BeatmapFilename))
            {
                showMessage("You are already downloading this beatmap");
                return null;
            }
            //if (File.Exists (Path.Combine(Songsfolder, fileName)))



            if (potentialDownload.BeatmapInstalled)
            {
                //TODO: make sure that we close down the seeding of the specific map that gets updated

                //is the osz2 uptodate?
                if (!potentialDownload.BeatmapMagnetSynced)
                    try
                    {
                        //we just copy the beatmapfile and let auto error correction take care
                        //the rest.
                        potentialDownload.MakeUpdateReady();
                        //if we got here it means that we already have the beatmap it issn't uptodate
                        //we dont want to share a map that's not uptodate
                        possibleShares.Remove(possibleShares.Find(S => S == potentialDownload.InternalTorrent.Name));
                        seedTorrents.Remove(seedTorrents.Find(T => T.Torrent == potentialDownload.InternalTorrent));
                        updating = true;
                    }
#if !DEBUG
                    catch (Exception e)
#else
                    catch (AbandonedMutexException e) //we don't catch anything
#endif
                    {
                        showMessage("Beatmap update failed: could not copy original beatmap file");

                        runMainThread.Add(() =>
                        {
                            try
                            {
                                if (File.Exists(potentialDownload.PathFile))
                                    File.Delete(potentialDownload.PathFile);
                            }
                            catch
                            {
                                //todo: queue delete;
                            }
                        }, true);

                        return null;
                    }
                else
                {
                    showMessage("You already have the latest version of this beatmap.");
                    return null;
                }
            }



            try
            {

                string newPathFile = Path.Combine(magnetsPathUnfinished, potentialDownload.InternalMagnetName);
                if (!newPathFile.Equals(potentialDownload.PathFile))
                {
                    if (File.Exists(newPathFile))
                        File.Delete(newPathFile);
                    File.Copy(potentialDownload.PathFile, newPathFile);
                }
            }
            catch
            {
                showMessage("OsuMagnet file could not be copied over, do you have UAC enabled?");
                return null;
            }
            addLeechTorrent(potentialDownload.InternalTorrent, true, updating);



            return potentialDownload;
        }

        //do not make public without checking if magnet is being write-locked
        private static bool updateMagnet(string pathFile)
        {
            return false;

        }

        private static TorrentInfo addLeechTorrent(Torrent torrent, bool start, bool updating)
        {
            TorrentManager manager = new TorrentManager(torrent, DownloadsPath, leechSettings);
            if (torrentExists(torrent))
                throw new Exception("Cannot add same torrent twice");
            //todo: add torrentmanager events
            manager.PieceHashed += (onPieceHashed);
            engine.Register(manager);
            leechTorrents.Add(manager);
            if (start && Activated)
                manager.Start();
            return Info.AddTorrent(manager, updating);

        }

        private static bool torrentExists(Torrent torrent)
        {
            return leechTorrents.Exists(t => t.Torrent == torrent) ||
                   seedTorrents.Exists(t => t.Torrent == torrent);
        }
        internal static void removeLeechTorrent(TorrentManager torrentManager, bool delete)
        {
            runMainThread.Add(delegate
            {
                if (Activated)
                    torrentManager.Stop();
                //todo: maybe we need to wait before unregistering in a nonstalling way

                while (true)
                {
                    if (torrentManager.State == TorrentState.Stopped)
                    {
                        //check if already deleted while scheduling here
                        if (leechTorrents.Exists(t => t.Torrent == torrentManager.Torrent))
                        {
                            leechTorrents.Remove(torrentManager);
                            engine.Unregister(torrentManager);
                            Info.RemoveTorrent(torrentManager);
                        }
                        break;
                    }
                    Thread.Sleep(1);
                }


                try
                {
                    //always delete the magnet
                    File.Delete(Path.Combine(magnetsPathUnfinished, torrentManager.Torrent.Name + ".OsuMagnet"));
                    if (delete)
                        File.Delete(Path.Combine(torrentManager.SavePath, torrentManager.Torrent.Name));
                }
                catch
                {
                }
            });
        }


        internal static void Activate()
        {
            if (Activated)
                return;
            allTorrents.Clear();
            allTorrents.AddRange(leechTorrents);
            allTorrents.AddRange(seedTorrents);
            // For each torrent manager we loaded and stored in our list, hook into the events
            // in the torrent manager and start the engine.
            foreach (TorrentManager manager in allTorrents)
            {
                // Every time a piece is hashed, this is fired.
                manager.PieceHashed += (onPieceHashed);

                // Every time the state changes (Stopped -> Seeding -> Downloading -> Hashing) this is fired
                manager.TorrentStateChanged += onStateChanged;

                // Every time the tracker's state changes, this is fired
#if DEBUG
                foreach (TrackerTier tier in manager.TrackerManager)
                {
                    foreach (MonoTorrent.Client.Tracker.Tracker t in tier.Trackers)
                    {
                        t.AnnounceComplete += delegate(object sender, AnnounceResponseEventArgs e)
                        {
                            showMessage(string.Format("{0}: {1}", e.Successful, e.Tracker.ToString()));
                        };
                    }
                }
#endif
                // Start the torrentmanager. The file will then hash (if required) and begin downloading/seeding
                manager.Start();


            }
            foreach (TorrentManager manager in seedTorrents)
                manager.Start();

            Activated = true;
            downloadManager = new Thread(new ThreadStart(MainLoop));
            downloadManager.Start();
        }

        private static void onPieceHashed(object o, PieceHashedEventArgs e)
        {
#if DEBUG
            showMessage(string.Format("Piece Hashed: {0} - {1}", e.PieceIndex, e.HashPassed ? "Pass" : "Fail"));
#endif
        }

        static private void onStateChanged(object o, TorrentStateChangedEventArgs e)
        {
            if (!MainLoopActive)
                return;

            //todo gamebase.schedule
            bool isLeechTorrent = leechTorrents.Contains(e.TorrentManager);

            if (isLeechTorrent)
                runMainThread.Add(
                    () => Info.StateChanged(e.TorrentManager.Torrent.Name, e.OldState, e.NewState)
                    );

            if (e.NewState != TorrentState.Seeding || !isLeechTorrent)
                return;

            //now we know we just finished downloading/updating.
            TorrentManager T = e.TorrentManager;
            T.Stop();

            //we have to wait until the download gets stopped but we don't want to stall the caller
            lock (moveQueueLock)
            {
                moveQueue.Add(T);
            }




        }


        private static void processDownloadCompleted(TorrentManager T, bool calledFromSeperateThread)
        {
            AutoResetEvent FileMoveHandle = Osz2Factory.TryGetWaitHandle(Path.Combine(DownloadsPath, T.Torrent.Name));
            if (FileMoveHandle != null)
            {
                if (calledFromSeperateThread)
                    FileMoveHandle.WaitOne();
                else
                {
                    ThreadPool.QueueUserWorkItem((D) =>
                    {
                        try
                        {
                            processDownloadCompleted(T, true);
                        }
#if !DEBUG
                        catch (Exception e)
#else
                        catch (AbandonedMutexException e) //we don't catch anything
#endif
                        {

                        }
                    });
                    NotificationManager.ShowMessageMassive(
                    "The beatmap that is currently getting updated is locked and is added to the processing queue.", 5000);
                }
            }
            T.MoveFiles(Songsfolder, true);
            string newPathFile = Path.Combine(magnetsPathFinished, T.Torrent.Name) + ".OsuMagnet";
            string oldPathFile = Path.Combine(magnetsPathUnfinished, T.Torrent.Name) + ".OsuMagnet";
            if (File.Exists(newPathFile))
                File.Delete(newPathFile);
            File.Move(oldPathFile, newPathFile);


#if SEEDENABLED
            //todo: also lock this stuff or schedule
            seedTorrents.Add(T);
            //start seeding if p2p is activated
            if (Activated)
                T.Start();
#else
            engine.Unregister(T);
#endif
        }






        internal static void Deactivate()
        {
            if (downloadManager == null)
                return;

            //reset eventhandlers and pause torrent
            foreach (TorrentManager manager in leechTorrents)
            {
                //manager.PieceHashed = new EventHandler<PieceHashedEventArgs>();
                manager.TorrentStateChanged -= onStateChanged;
                manager.PieceHashed -= (onPieceHashed);
                // Every time the tracker's state changes, this is fired
                foreach (TrackerTier tier in manager.TrackerManager)
                {
                    foreach (MonoTorrent.Client.Tracker.Tracker t in tier.Trackers)
                    {
                        // t.AnnounceComplete = new EventHandler<AnnounceResponseEventArgs>();
                    }
                }
                manager.Pause();
            }

            foreach (TorrentManager manager in seedTorrents)
                manager.Pause();


            Activated = false;
        }
        static LinkedList<String> lastFiveMessages = new LinkedList<string>(new string[5]);
        //todo: change to link query concat
        static List<TorrentManager> allTorrents = new List<TorrentManager>();
        private static void MainLoop()
        {
            //catch all thread exceptions:
            //todo: marshal to osu! main thread
            //todo: cange this ffs
#if DEBUG
            AppDomain.CurrentDomain.ProcessExit += delegate { Shutdown(); };
            /*Thread.GetDomain().UnhandledException += delegate(object sender, UnhandledExceptionEventArgs e)
            {
                e.ExceptionObject.ToString();
                BanchoClient.HandleException(e.ExceptionObject as Exception,"p2p crash")
                NotificationManager.ShowMessageMassive("Osu!P2P seems to have crashed, all downlaods will be resumed after restarting Osu!",2000);
                try
                {
                    Shutdown();
                }
                catch
                {
                }
            };*/
#endif

            //don't start if we already have shutdown
            if (shutdownRun)
                return;

            runMainThread = new SleepScheduler(sleepHandle);
            //fire events
            onMainLoopStarted();
            // While the torrents are still running, print out some stats to the screen.
            // Details for all the loaded torrent managers are shown.
            int i = 0;
            StringBuilder sb = new StringBuilder(1024);
            try
            {
                while (Activated)
                {
                    //todo: move to end, and check whether this oculd cause issues.
                    runMainThread.Update();
#if DEBUG
                    sb.Remove(0, sb.Length);

                    AppendFormat(sb, "Total Download Rate: {0:0.00}kB/sec", engine.TotalDownloadSpeed / 1024.0);
                    AppendFormat(sb, "Total Upload Rate:   {0:0.00}kB/sec", engine.TotalUploadSpeed / 1024.0);
                    AppendFormat(sb, "Disk Read Rate:      {0:0.00} kB/s", engine.DiskManager.ReadRate / 1024.0);
                    AppendFormat(sb, "Disk Write Rate:     {0:0.00} kB/s", engine.DiskManager.WriteRate / 1024.0);
                    AppendFormat(sb, "Total Read:         {0:0.00} kB", engine.DiskManager.TotalRead / 1024.0);
                    AppendFormat(sb, "Total Written:      {0:0.00} kB", engine.DiskManager.TotalWritten / 1024.0);
                    AppendFormat(sb, "Open Connections:    {0}", engine.ConnectionManager.OpenConnections);
                    //stupid copy for debug purposes only
                    allTorrents.Clear();
                    allTorrents.AddRange(leechTorrents);
                    allTorrents.AddRange(seedTorrents);
                    foreach (TorrentManager manager in allTorrents)
                    {
                        AppendSeperator(sb);
                        AppendFormat(sb, "State:           {0}", manager.State);
                        AppendFormat(sb, "Name:            {0}",
                                     manager.Torrent == null ? "MetaDataMode" : manager.Torrent.Name);
                        AppendFormat(sb, "Progress:           {0:0.00}", manager.Progress);
                        AppendFormat(sb, "Download Speed:     {0:0.00} kB/s", manager.Monitor.DownloadSpeed / 1024.0);
                        AppendFormat(sb, "Upload Speed:       {0:0.00} kB/s", manager.Monitor.UploadSpeed / 1024.0);
                        AppendFormat(sb, "Total Downloaded:   {0:0.00} MB",
                                     manager.Monitor.DataBytesDownloaded / (1024.0 * 1024.0));
                        AppendFormat(sb, "Total Uploaded:     {0:0.00} MB",
                                     manager.Monitor.DataBytesUploaded / (1024.0 * 1024.0));
                        Tracker tracker = manager.TrackerManager.CurrentTracker;
                        //AppendFormat(sb, "Tracker Status:     {0}", tracker == null ? "<no tracker>" : tracker.State.ToString());
                        AppendFormat(sb, "Warning Message:    {0}",
                                     tracker == null ? "<no tracker>" : tracker.WarningMessage);
                        AppendFormat(sb, "Failure Message:    {0}",
                                     tracker == null ? "<no tracker>" : tracker.FailureMessage);
                        if (manager.PieceManager != null)
                            AppendFormat(sb, "Current Requests:   {0}", manager.PieceManager.CurrentRequestCount());

                        foreach (PeerId p in manager.GetPeers())
                            AppendFormat(sb, "\t{2} - {1:0.00}/{3:0.00}kB/sec - {0}", p.Peer.ConnectionUri,
                                         p.Monitor.DownloadSpeed / 1024.0,
                                         p.AmRequestingPiecesCount,
                                         p.Monitor.UploadSpeed / 1024.0);

                        AppendFormat(sb, "", null);
                        if (manager.Torrent != null)
                            foreach (TorrentFile file in manager.Torrent.Files)
                                AppendFormat(sb, "{1:0.00}% - {0}", file.Path, file.BitField.PercentComplete);
                        foreach (string Message in lastFiveMessages)
                            sb.AppendLine(Message);
                    }
                    //Console.Clear();
                    if (i % 10 == 0)
                        Console.Write(sb.ToString());
#endif
                    Info.Update();

                    //torrents that are finished are moved to the movequeue
                    //we check if torrentM disconnected all peers and removed IO lock
                    //then we move downloaded files to the song directory.

                    //copy to reduce locktime
                    List<TorrentManager> tempQueue;
                    lock (moveQueueLock)
                    {
                        tempQueue = new List<TorrentManager>(moveQueue);
                        moveQueue.RemoveAll(T => T.State == TorrentState.Stopped);
                    }
                    for (int j = tempQueue.Count - 1; j >= 0; j--)
                    {
                        TorrentManager T = tempQueue[j];
                        if (T.State != TorrentState.Stopped)
                            continue;

                        try
                        {
                            Info.StateChanged(T.Torrent.Name, TorrentState.Stopping, TorrentState.Stopped);
                            leechTorrents.Remove(T);
                            Info.RemoveTorrent(T);
                            processDownloadCompleted(T, false);
                        }
#if !DEBUG
                        catch (Exception e)
#else
                        catch (AbandonedMutexException e) //we don't catch anything
#endif
                        {
                            showMessage("Failed to download beatmap: Could not move it to the songs directory.");
                            continue;
                        }
                    }


#if SEEDENABLED
    //todo: check inside beatmapfolder list instead of magnetCache list.
                    //todo: filelocks etc
                while (true)
                {
                    //share a random file each 3 seconds until we hit
                    //the connection cap or 60 maps
                    if (!(i++%3 == 0 && seedTorrents.Count < 60 && possibleShares.Count > 0 &&
                        engine.ConnectionManager.OpenConnections <= engine.ConnectionManager.MaxOpenConnections))
                        break;

                    int randomIndex = random.Next(0, possibleShares.Count);
                    //(maybe even update if we haven't)

                    //todo: possibleshare should be locked :/
                    string file = possibleShares[randomIndex];
                    possibleShares.RemoveAt(randomIndex);
                    Torrent torrent = null;
                    try
                    {
                        file = Path.Combine(magnetsPathFinished, file);
                        // Load the .OsuMagnet from the file into a Torrent instance
                        OsuMagnet magnet = new OsuMagnet(file);
                        torrent = magnet.InternalTorrent;
                        if (!magnet.BeatmapMagnetSynced && magnet.BeatmapInstalled)
                        {
                            File.Delete(file);
                            continue;
                        }

                        showMessage(torrent.InfoHash.ToString());
                    }
                    catch (Exception e)
                    {
                        Console.Write("Couldn't decode {0}: ", file);
                        showMessage(e.Message);
                        continue;
                    }
                    TorrentManager manager = new TorrentManager(torrent, songsfolder, SeedSettings);
                    engine.Register(manager);
                    seedTorrents.Add(manager);
                }
#endif
                    //sleep but this thread can be disrupted temporarily by the scheduler.
                    sleepHandle.Sleep(1000);

                    //Thread.Sleep(1000);

                }
                //run the task scheduler until no tasks are left.
                int iterCount = 0;
                while (runMainThread.Update())
                {
                    //if (iterCount++ > 10)
                    //    throw new Exception("Cannot close runMainThread: reached iteration limit.");
                    Thread.Sleep(0);
                }


            }
#if !DEBUG
            catch (Exception e)
#else
            catch (PlatformNotSupportedException e) //we don't catch anything
#endif
            {
                ErrorSubmission.Submit(new OsuError(e) { Feedback = "p2p crash" });
                GameBase.Scheduler.Add(() =>
                    NotificationManager.ShowMessageMassive("Osu!P2P seems to have crashed, all downlaods will be resumed after restarting Osu!", 2000));
                try
                {
                    Shutdown();
                }
                catch
                {
                }
            }
            finally
            {
                //activated is set to false. now we remove all references to this thread.
                downloadManager = null;
                //exit thread and delete task scheduler.
                runMainThread = null;
            }
        }
        static void ManagerPeersFound(object sender, PeersAddedEventArgs e)
        {
            showMessage(string.Format("Found {0} new peers and {1} existing peers", e.NewPeers, e.ExistingPeers));//throw new Exception("The method or operation is not implemented.");
        }

        internal static void showMessage(string Message)
        {
            lastFiveMessages.RemoveLast();
            lastFiveMessages.AddFirst(Message);

            //thread safeness, and we want the message to appear after gamemode switch.
            GameBase.Scheduler.Add
                (() => NotificationManager.ShowMessage(Message, Color.Cyan, Message.Length * 80 + 1000));

        }
        private static void AppendSeperator(StringBuilder sb)
        {
            AppendFormat(sb, "", null);
            AppendFormat(sb, "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -", null);
            AppendFormat(sb, "", null);
        }
        private static void AppendFormat(StringBuilder sb, string str, params object[] formatting)
        {
            if (formatting != null)
                sb.AppendFormat(str, formatting);
            else
                sb.Append(str);
            sb.AppendLine();
        }


        private static readonly uint[] KEY = new uint[] { 287054061, 222010230, 391764117, 330051188 };
        private static bool shutdownRun = false;
        internal static void Shutdown()
        {
            if (shutdownRun)
                return;
            shutdownRun = true;
            Deactivate();
            while (downloadManager != null) Thread.Sleep(1);
            BEncodedDictionary fastResume = new BEncodedDictionary();
            // tell all the torrent responsible threads to stop downloading
            foreach (TorrentManager T in leechTorrents)
                T.Stop();
            foreach (TorrentManager T in seedTorrents)
                T.Stop();
            //unregister all events here

            //wait until all torrents have been stopped and save to to the
            //resume file.
            int torrentCount = leechTorrents.Count + seedTorrents.Count;
            int stoppedCount = 0;
            while (torrentCount != stoppedCount)
            {
                for (int i = 0; i < leechTorrents.Count; i++)
                {
                    if (leechTorrents[i].State != TorrentState.Stopped)
                    {
                        showMessage(String.Format("{0} is {1}", leechTorrents[i].Torrent.Name, leechTorrents[i].State));
                        continue;
                    }
                    stoppedCount++;
                    fastResume.Add(leechTorrents[i].Torrent.InfoHash.ToHex(), leechTorrents[i].SaveFastResume().Encode());
                }
                foreach (TorrentManager T in seedTorrents)
                    if (T.State == TorrentState.Stopped)
                        stoppedCount++;
                Thread.Sleep(500);
            }
            FastEncryptionProvider Encryptor = new FastEncryptionProvider();
            Encryptor.SetKey(KEY, EncryptionMethod.XTEA);
#if !DISABLE_DHT
            byte[] dhtEncrypted = engine.DhtEngine.SaveNodes();
            Encryptor.Encrypt(dhtEncrypted);
            File.WriteAllBytes(dhtNodeFile, dhtEncrypted);
#endif
            byte[] fastResumeEncrypted = fastResume.Encode();
            Encryptor.Encrypt(fastResumeEncrypted);
            File.WriteAllBytes(fastResumeFile, fastResumeEncrypted);
            engine.Dispose();

        }
    }
}
