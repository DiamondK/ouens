﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Select.Drawable;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Online.P2P;
using osu_common.Libraries.NetLib;
using MonoTorrent.Common;



namespace osu.Online
{


    internal class P2PDirectDownload : OsuDirectDownload
    {
        private Osup2pDisplay P2PData;
        private TorrentInfo RelatedTorrent;


        public P2PDirectDownload(OnlineBeatmap ob, bool noVideo) : this(ob.setId, ob.serverFilename, ob.title, noVideo, noVideo? ob.filesize_novideo : ob.filesize)
        {
            beatmap = ob;
        }
        
        public P2PDirectDownload(int setId, string filename, string title, bool noVideo, int filesize) : base()
        {

            P2PData = Osup2pManager.Info;
            //it's important we have some kind of unique ID for osudirect to work with
            //even though this is not the real filename.
            bool parentDir = filename.Contains("../");

            if (parentDir)
                filename = filename.Replace("../", "");

            this.filename = filename;
            this.filesize = filesize;
            this.title = title;

            /*if (!Directory.Exists("Temp"))
                Directory.CreateDirectory("Temp");

            localFilename = filename;
            if (localFilename.IndexOf('/') > 0)
                localFilename = localFilename.Substring(filename.IndexOf('/') + 1);
            string destination = "Temp/" + localFilename;

            try
            {
                File.Delete(destination);
            }
            catch
            {
                return;
            }*/
            Status = DownloadStatus.Downloading;
            OsuMagnetFactory.DownloadOsuMagnet(setId,noVideo,(OM) => Osup2pManager.LoadMagnet(OM, req_onStart));
            
        }
        public P2PDirectDownload(string OsuMagnet, bool external) : base()
        {
            P2PData = Osup2pManager.Info;
            Status = DownloadStatus.Downloading;
            if (external) // if osuMagnet is outside of the download directory
                Osup2pManager.LoadMagnet(OsuMagnet, req_onStart);
            else // in this case the download is already loaded before by the p2pmanager
                if (Osup2pManager.MainLoopActive)
                    req_onStart(true, OsuMagnet);
                else
                    Osup2pManager.onMainLoopStarted+=()=>req_onStart(true, OsuMagnet);

               
        }




        private void req_onUpdate()
        {
            if (Status == DownloadStatus.Completed)
                return;

            float prog = (float) RelatedTorrent.Progress;

            
            //in case we gat an update before first draw.
            if (SpriteCollection != null)
            {
                s_progressText.Text = string.Format("{0:0.0}", prog);
                s_progressBar.VectorScale.X = (prog*PROGRESS_BAR_WIDTH /100);

                s_progressText.Position =
                    new Vector2(
                        s_progressBar.InitialPosition.X + Math.Min(s_progressBar.VectorScale.X, PROGRESS_BAR_WIDTH - 22),
                        s_progressText.Position.Y);
                s_progressText.InitialPosition = s_progressText.Position;
            }

            onUpdate(RelatedTorrent.Progress);
            //todo: actualy we should do this with the stateChanged callback
            if (prog >= 100)
                req_onFinish();
        }

        private void req_onFinish()
        {
            /*File.Delete(BeatmapManager.SONGS_DIRECTORY + localFilename);

            if (e == null)
            {
                AudioEngine.PlaySample(@"match-confirm");
                File.Move(_fileLocation, BeatmapManager.SONGS_DIRECTORY + localFilename);
            }*/

            Status = DownloadStatus.Completed;
            P2PData.DisplayInfoUpdated -= req_onUpdate;
            OsuDirect.UpdatePending = true;
            onFinish(true);
        }

        private void req_onStart(bool succeeded, string key)
        {
            if (!succeeded)
            {
                //osuMagnet is corrupted or could not be opened
                Abort(false);
                return;
            }

            //osuMagnet is loaded and started
            RelatedTorrent = P2PData.GetTorrent(key);
            P2PData.DisplayInfoUpdated += req_onUpdate;
            Status = DownloadStatus.Downloading;

            //update title information
            title = RelatedTorrent.Name;
            if (SpriteCollection != null)
                s_title.Text = title;

            //start registered start events
            onStart();
            
        }

        internal override void Abort(bool promptForConfirmation)
        {
            if (promptForConfirmation)
            {
                pDialog dialog =
                    new pDialog("Would you like to cancel the download?", true);
                dialog.AddOption("Yes, cancel this download.", Color.OrangeRed, delegate { Abort(false); });
                dialog.AddOption("No, keep downloading.", Color.YellowGreen, delegate { });
                GameBase.ShowDialog(dialog);
                return;
            }

            //request.Abort();

            Status = DownloadStatus.Completed;
            OsuDirect.UpdatePending = true;
            //remove and stop related Torrents
            if (RelatedTorrent != null)
            {
                Osup2pManager.removeLeechTorrent(RelatedTorrent.UnsafeTorrentReference, true);
                P2PData.DisplayInfoUpdated -= new DisplayInfoUpdated(req_onUpdate);
            }
            onUpdate(0.01);
            onFinish(false);
        }


    }
}