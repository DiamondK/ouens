﻿using System;
using System.Collections.Generic;
using MonoTorrent.Client;
using MonoTorrent.Common;

namespace osu.Online.P2P
{

    class TorrentInfo
    {
        internal TorrentState State;
        internal string Key;
        internal string Name;
        internal double Progress;
        //in bytes
        internal int DownloadSpeed;
        internal int UploadSpeed;
        internal long UploadTotal;
        internal long DownloadTotal;
        internal bool Updating;
        internal long FileSize;
        internal TorrentEventHandler StateChanged = delegate{};
        internal TorrentManager UnsafeTorrentReference;
        //todo add per peer info
    }


    //thread safe way to get running torrent information (ex. from OsuDirect)
    //right now only used to store LeechTorrents.
    class Osup2pDisplay
    {
        //not thread safe references
        private readonly ClientEngine engine;
        private List<TorrentManager> torrents;
        private List<TorrentInfo> TorrentValues;
        private List<string> TorrentKeys;
        private object TorrentsLock = new object();
        //public properties
        internal Boolean DownloadsActive { get { return torrents.Exists(m => m.State != TorrentState.Stopped); } }
        


        internal int TotalDownloadSpeed { get; private set; }
        internal int TotalUploadSpeed { get; private set; }
        internal int OpenConnections { get; private set; }

        internal Osup2pDisplay(List<TorrentManager> TorrentCollection, ClientEngine Engine)
        {
            engine = Engine;
            torrents = TorrentCollection;
            int count = TorrentCollection.Count;
            TorrentKeys = new List<string>(count);
            TorrentValues = new List<TorrentInfo>(count);
            Update();
        }

        //should only be called from the p2pManager class
        internal TorrentInfo AddTorrent(TorrentManager t, bool updating)
        {
            TorrentInfo TI = new TorrentInfo()
                                 {
                                     State = t.State,
                                     Key = t.Torrent.Name,
                                     Name = t.Torrent.Comment,
                                     Progress = t.Progress,
                                     DownloadSpeed = t.Monitor.DownloadSpeed,
                                     UploadSpeed = t.Monitor.UploadSpeed,
                                     DownloadTotal = t.Monitor.DataBytesDownloaded,
                                     UploadTotal = t.Monitor.DataBytesUploaded,
                                     Updating = updating,
                                     //todo..
                                     FileSize = t.Torrent.Files[0].Length,
                                     UnsafeTorrentReference = t
                                 };

            lock (TorrentsLock)
            {
                TorrentKeys.Add(t.Torrent.Name);
                TorrentValues.Add(TI);
            }
            return TI;
        }

        //should only be called from the p2pManager class
        internal void RemoveTorrent(TorrentManager t)
        {
            lock (TorrentsLock)
            {
                int index = TorrentKeys.IndexOf(t.Torrent.Name);
                TorrentKeys.RemoveAt(index);
                TorrentValues.RemoveAt(index);
            }
        }
        internal TorrentInfo GetTorrent(string name)
        {
            TorrentInfo TI = null;
            lock (TorrentsLock)
            {
                int index = TorrentKeys.IndexOf(name);
                if (index != -1)
                    TI = TorrentValues[index];
            }
            if (TI == null)
                throw new KeyNotFoundException();
            return TI;
        }

        internal void StateChanged(string torrent, TorrentState oldS, TorrentState newS)
        {
            if (newS != TorrentState.Stopped && newS != TorrentState.Stopping)
            {
                TorrentInfo TI = GetTorrent(torrent);
                TI.State = newS;
                GameBase.Scheduler.Add(() => TI.StateChanged(oldS, TI));
            }
        }
        //thread-safe way to update displayInfo
        internal DisplayInfoUpdated DisplayInfoUpdated = delegate { };
        internal void Update()
        {
            TotalDownloadSpeed = engine.TotalDownloadSpeed;
            TotalUploadSpeed = engine.TotalUploadSpeed;
            OpenConnections = engine.ConnectionManager.OpenConnections;


            List<TorrentInfo> torrentTemp;
            lock (TorrentsLock)
                torrentTemp = new List<TorrentInfo>(TorrentValues);
            int count = torrentTemp.Count;
            for (int i = 0; i<count; i++)
            {
                TorrentInfo TI = torrentTemp[i];
                TorrentManager Internaltorrent = torrents[i];
                TI.Progress = Internaltorrent.Progress;
                ConnectionMonitor monitor = Internaltorrent.Monitor;
                TI.DownloadSpeed = monitor.DownloadSpeed;
                TI.UploadSpeed = monitor.UploadSpeed;
                TI.DownloadTotal = monitor.DataBytesDownloaded;
                TI.UploadTotal = monitor.DataBytesUploaded;
            }
            lock (TorrentsLock)
            {
                int currentCount = TorrentValues.Count;
                int tempCount = torrentTemp.Count;
                if (currentCount > tempCount)
                    torrentTemp.Add(TorrentValues[currentCount - 1]);
                else if (currentCount < tempCount)
                    torrentTemp.RemoveAt(currentCount - 1);
                TorrentValues = torrentTemp;
            }
            DisplayInfoUpdated();
        }
    }
}
