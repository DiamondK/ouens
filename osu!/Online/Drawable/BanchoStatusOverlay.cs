﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Graphics;
using osu.Graphics.Sprites;
using osu.Graphics.Skinning;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Input;
using osu.Input.Handlers;
using osu_common.Helpers;
using osu.GameModes.Play;

namespace osu.Online.Drawable
{
    class BanchoStatusOverlay : pDrawableComponent
    {
        private pSprite spinner;
        private pText message;
        private pSprite background;
        private bool visible;

        const int hide_offset = 25;

        public BanchoStatusOverlay()
        {
            background = new pSprite(SkinManager.Load(@"notification", SkinSource.Osu), Fields.NativeBottomCentre, Origins.Centre, Clocks.Game, new Vector2(0, 0), 0.8f, true, new Color(0, 0, 0, 128));
            spriteManager.Add(background);

            spinner = new pSprite(SkinManager.Load(@"loading-small", SkinSource.Osu), Fields.NativeBottomCentre, Origins.Centre, Clocks.Game, new Vector2(-100, 11), 1, true, Color.White);
            Transformation tr = new Transformation(TransformationType.Rotation, 0, (float)Math.PI * 2, GameBase.Time, GameBase.Time + 1500);
            tr.Loop = true;

            spinner.Transformations.Add(tr);
            spinner.FadeInFromZero(2000);
            spriteManager.Add(spinner);

            message = new pText("Bancho is connecting...", 8, new Vector2(-88, 18), 1, true, Color.White);
            message.TextAa = false;
            message.TextRenderSpecific = false;
            message.Field = Fields.NativeBottomCentre;

            spriteManager.Add(message);

            spriteManager.SpriteList.ForEach(s => { s.Alpha = 0; s.Position.Y -= hide_offset; });
            spriteManager.Alpha = 0;
        }

        string text;
        internal string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
                GameBase.Scheduler.Add(delegate
                {
                    message.Text = text;

                    float textWidth = message.MeasureText().X;

                    background.VectorScale.X = Math.Max(1, (textWidth + 40) / (background.Width / 1.6f));

                    message.InitialPosition.X = message.Position.X = -88 * background.VectorScale.X - 30 * Math.Max(0, background.VectorScale.X - 1);
                    message.Transformations.ForEach(t => t.EndVector.X = message.Position.X);

                    spinner.InitialPosition.X = spinner.Position.X = -12 - 88 * background.VectorScale.X - 30 * Math.Max(0, background.VectorScale.X - 1);
                    spinner.Transformations.ForEach(t => t.EndVector.X = spinner.Position.X);

                    if (!string.IsNullOrEmpty(text)) Visible = true;
                });
            }
        }

        internal override void Update()
        {
            bool shouldDisplay = !Player.Playing && InputManager.HandleInput;
            if (shouldDisplay)
            {
                if (spriteManager.Alpha < 1)
                    spriteManager.Alpha = Math.Min(1, spriteManager.Alpha + 0.01f);
            }
            else
            {
                if (spriteManager.Alpha > 0)
                    spriteManager.Alpha = Math.Max(0, spriteManager.Alpha - 0.01f);
            }

            base.Update();
        }

        internal bool Visible
        {
            get { return visible; }
            set
            {
                if (GameBase.Tournament) return;

                GameBase.Scheduler.Add(delegate
                {
                    if (value == visible) return;
                    visible = value;

                    foreach (pSprite p in spriteManager.SpriteList)
                    {
                        if (visible)
                        {
                            p.FadeIn(500);
                            p.MoveTo(p.InitialPosition, 500, EasingTypes.Out);
                        }
                        else
                        {
                            p.FadeOut(3000);
                            p.MoveTo(new Vector2(p.InitialPosition.X, p.InitialPosition.Y - hide_offset), 1500, EasingTypes.In);
                        }
                    }
                });
            }
        }
    }
}
