﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu_common.Libraries.NetLib;
using osu.Graphics;
using osu.Helpers;
using osu.Audio;
using osu_common.Helpers;
using osu.Online.Social;

namespace osu.Online.Drawable
{
    internal class User : IDisposable, IComparable<User>, ICloneable
    {
        private static readonly long[] toNextLevel = new[]
                                                         {
                                                             30000, 100000, 210000, 360000, 550000, 780000, 1050000,
                                                             1360000, 1710000, 2100000, 2530000, 3000000, 3510000, 4060000,
                                                             4650000, 5280000, 5950000, 6660000, 7410000, 8200000, 9030000,
                                                             9900000, 10810000, 11760000, 12750000, 13780000, 14850000,
                                                             15960000, 17110000, 18300000, 19530000, 20800000, 22110000,
                                                             23460000, 24850000, 26280000, 27750000, 29260000, 30810000,
                                                             32400000, 34030000, 35700000, 37410000, 39160000, 40950000,
                                                             42780000, 44650000, 46560000, 48510000, 50500000, 52530000,
                                                             54600000, 56710000, 58860000, 61050000, 63280000, 65550000,
                                                             67860000, 70210001, 72600001, 75030002, 77500003, 80010006,
                                                             82560010, 85150019, 87780034, 90450061, 93160110, 95910198,
                                                             98700357, 101530643, 104401157, 107312082, 110263748,
                                                             113256747, 116292144, 119371859, 122499346, 125680824,
                                                             128927482, 132259468, 135713043, 139353477, 143298259,
                                                             147758866, 153115959, 160054726, 169808506, 184597311,
                                                             208417160, 248460887, 317675597, 439366075, 655480935,
                                                             1041527682, 1733419828, 2975801691, 5209033044, 9225761479,
                                                             99999999999, 99999999999, 99999999999, 99999999999,
                                                             99999999999, 99999999999, 99999999999, 99999999999,
                                                             99999999999, 99999999999
                                                         };

        private static String[] countryNames = {
            @"Unknown",@"Oceania",@"Europe",@"Andorra",@"UAE",
            @"Afghanistan",@"Antigua",@"Anguilla",@"Albania",@"Armenia",
            @"Netherlands Antilles",@"Angola",@"Antarctica",@"Argentina",@"American Samoa",
            @"Austria",@"Australia",@"Aruba",@"Azerbaijan",@"Bosnia",
            @"Barbados",@"Bangladesh",@"Belgium",@"Burkina Faso",@"Bulgaria",@"Bahrain",
            @"Burundi",@"Benin",@"Bermuda",@"Brunei Darussalam",@"Bolivia",@"Brazil",@"Bahamas",
            @"Bhutan",@"Bouvet Island",@"Botswana",@"Belarus",@"Belize",@"Canada",
            @"Cocos Islands",@"Congo",
            @"Central African Republic",@"Congo",@"Switzerland",@"Cote D'Ivoire",
            @"Cook Islands",@"Chile",@"Cameroon",@"China",@"Colombia",@"Costa Rica",@"Cuba",
            @"Cape Verde",@"Christmas Island",@"Cyprus",@"Czech Republic",@"Germany",
            @"Djibouti",@"Denmark",@"Dominica",@"Dominican Republic",@"Algeria",@"Ecuador",
            @"Estonia",@"Egypt",@"Western Sahara",@"Eritrea",@"Spain",@"Ethiopia",@"Finland",
            @"Fiji",@"Falkland Islands",@"Micronesia, Federated States of",
            @"Faroe Islands",@"France",@"France, Metropolitan",@"Gabon",@"United Kingdom",
            @"Grenada",@"Georgia",@"French Guiana",@"Ghana",@"Gibraltar",@"Greenland",@"Gambia",
            @"Guinea",@"Guadeloupe",@"Equatorial Guinea",@"Greece",
            @"South Georgia",@"Guatemala",@"Guam",
            @"Guinea-Bissau",@"Guyana",@"Hong Kong",@"Heard Island",
            @"Honduras",@"Croatia",@"Haiti",@"Hungary",@"Indonesia",@"Ireland",@"Israel",@"India",
            @"British Indian Ocean Territory",@"Iraq",@"Iran, Islamic Republic of",
            @"Iceland",@"Italy",@"Jamaica",@"Jordan",@"Japan",@"Kenya",@"Kyrgyzstan",@"Cambodia",
            @"Kiribati",@"Comoros",@"St. Kitts and Nevis",
            @"Korea, Democratic People's Republic of",@"Korea",@"Kuwait",
            @"Cayman Islands",@"Kazakhstan",@"Lao",@"Lebanon",
            @"St. Lucia",@"Liechtenstein",@"Sri Lanka",@"Liberia",@"Lesotho",@"Lithuania",
            @"Luxembourg",@"Latvia",@"Libyan Arab Jamahiriya",@"Morocco",@"Monaco",
            @"Moldova, Republic of",@"Madagascar",@"Marshall Islands",
            @"Macedonia, the Former Yugoslav Republic of",@"Mali",@"Myanmar",@"Mongolia",
            @"Macau",@"Northern Mariana Islands",@"Martinique",@"Mauritania",@"Montserrat",
            @"Malta",@"Mauritius",@"Maldives",@"Malawi",@"Mexico",@"Malaysia",@"Mozambique",
            @"Namibia",@"New Caledonia",@"Niger",@"Norfolk Island",@"Nigeria",@"Nicaragua",
            @"Netherlands",@"Norway",@"Nepal",@"Nauru",@"Niue",@"New Zealand",@"Oman",@"Panama",
            @"Peru",@"French Polynesia",@"Papua New Guinea",@"Philippines",@"Pakistan",
            @"Poland",@"St. Pierre",@"Pitcairn",@"Puerto Rico",@"" +
            @"Palestinian Territory",@"Portugal",@"Palau",@"Paraguay",@"Qatar",
            @"Reunion",@"Romania",@"Russian Federation",@"Rwanda",@"Saudi Arabia",
            @"Solomon Islands",@"Seychelles",@"Sudan",@"Sweden",@"Singapore",@"St. Helena",
            @"Slovenia",@"Svalbard and Jan Mayen",@"Slovakia",@"Sierra Leone",@"San Marino",
            @"Senegal",@"Somalia",@"Suriname",@"Sao Tome and Principe",@"El Salvador",
            @"Syrian Arab Republic",@"Swaziland",@"Turks and Caicos Islands",@"Chad",
            @"French Southern Territories",@"Togo",@"Thailand",@"Tajikistan",@"Tokelau",
            @"Turkmenistan",@"Tunisia",@"Tonga",@"Timor-Leste",@"Turkey",@"Trinidad and Tobago",
            @"Tuvalu",@"Taiwan",@"Tanzania",@"Ukraine",@"Uganda",
            @"US (Island)",@"United States",@"Uruguay",@"Uzbekistan",
            @"Holy See",@"St. Vincent",
            @"Venezuela",@"Virgin Islands, British",@"Virgin Islands, U.S.",@"Vietnam",
            @"Vanuatu",@"Wallis and Futuna",@"Samoa",@"Yemen",@"Mayotte",@"Serbia",
            @"South Africa",@"Zambia",@"Montenegro",@"Zimbabwe",@"Unknown",
            @"Satellite Provider",@"Other",
            @"Aland Islands",@"Guernsey",@"Isle of Man",@"Jersey",@"St. Barthelemy",
            @"Saint Martin"};

        internal double Accuracy;

        private Color backgroundColour = new Color(10, 29, 75);
        internal int BeatmapId;
        internal string BeatmapName = string.Empty;
        internal bool CantSpectate;
        internal string CurrentBeatmapChecksum = string.Empty;
        internal Mods CurrentMods = Mods.None;
        private bool extended;
        private bool firstReceived = true;
        internal int Id;
        //internal bool Ignored;
        private string InfoText = string.Empty;
        internal string IrcFormattedName;
        internal bool IsFriend;
        internal bool IsOsu;
        internal float Level;
        private pSprite levelBarBackground;
        private pSprite levelBarForeground;
        internal string Location;

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                IrcFormattedName = value.Replace(' ', '_');
            }
        }
        internal Permissions Permission;
        internal int PlayCount;
        internal PlayModes PlayMode;
        internal int Rank;
        private long RankedScore;
        private short PerformancePoints;
        private string RankText;

        private pSpriteDynamic spriteAvatar;

        internal double RecommendedDifficulty()
        {
            if (StatsLoaded && PerformancePoints > 0)
            {
                return Math.Pow((double)PerformancePoints, 0.4) * 0.195;
            }

            return 0;
        }

        private void InitializeAvatar()
        {
            Vector2 startPosition = currentPosition;
            Vector2 offset = new Vector2(23, 23);
            float depth = spriteBackground != null ? spriteBackground.Depth + 0.000001f : 0;

            if (spriteAvatar != null)
            {
                spriteAvatar.InitialPosition = startPosition + offset;
                if (spriteAvatar.Texture == null) spriteAvatar.OnTextureLoaded += delegate { spriteAvatar.Scale = 74F / Math.Max(spriteAvatar.Width, spriteAvatar.Height); };
                spriteAvatar.Scale = 74F / Math.Max(spriteAvatar.Width, spriteAvatar.Height);
            }
            else
            {
                spriteAvatar = GetFreshAvatarSprite(startPosition + offset, depth, 74f);
                if (Sprites != null) Sprites.Add(spriteAvatar);
            }

            spriteAvatar.Position = currentPosition + offset;
            spriteAvatar.Origin = Origins.Centre;
        }

        internal pSpriteDynamic GetFreshAvatarSprite(Vector2 position, float depth, float maxDimension)
        {
            bool isLocal = Id == GameBase.User.Id;

            pSpriteDynamic psd = new pSpriteDynamic(Id < 0 ? null : string.Format(this.Id == GameBase.User.Id ? Urls.USER_GET_AVATAR_NOCACHE : Urls.USER_GET_AVATAR, Id), General.TEMPORARY_FILE_PATH + Id, isLocal ? 0 : 500, position, depth);

            psd.OnTextureLoaded += delegate { psd.Scale = maxDimension / Math.Max(psd.Width, psd.Height); };

            return psd;
        }

        public bool LoadAvatarInto(pSpriteDynamic p, float maxDimension)
        {
            p.Origin = Origins.Centre;
            p.Url = string.Format(this.Id == GameBase.User.Id ? Urls.USER_GET_AVATAR_NOCACHE : Urls.USER_GET_AVATAR, Id);
            p.LocalCache = General.TEMPORARY_FILE_PATH + Id;
            p.MaxDimension = maxDimension;

            return p.State == LoadState.Unknown;
        }

        internal pSprite spriteBackground;
        private pSprite spriteForeground;
        internal pText spriteInfo;
        internal SpriteManager spriteManager = GameBase.spriteManagerOverlay;
        private pText spriteName;
        private pText spriteRank;
        internal List<pSprite> Sprites;
        private pText spriteStatus;
        internal bool StatsLoaded;
        internal bool StatsLoading;
        internal bStatus Status;
        private string StatusText = string.Empty;
        internal int Timezone;
        internal bool Visible = true;
        internal bool Watchable = true;
        private pText statUpdateSprite;
        private pSprite modeStatus;

        internal static string LOADING_STRING = LocalisationManager.GetString(OsuString.General_Loading);

        internal bool InitialLoadComplete;

        internal User()
        {
        }

        internal User(int id, string name = null)
        {
            IsOsu = false;
            Id = id;
            Name = name ?? LOADING_STRING;
        }

        internal User(string name)
            : this(-1, name)
        {
        }

        internal User(bUserPresence presence)
            : this(presence, null)
        {

        }

        internal User(bUserPresence presence, bUserStats userStats)
        {
            IsOsu = presence.isOsu;
            extended = true;

            ReceivePresence(presence);

            if (userStats != null)
                ReceiveUserStats(userStats);
        }

        #region IComparable<User> Members

        public int CompareTo(User u)
        {
            int nameCompare = Name.CompareTo(u.Name);
            if (nameCompare != 0) return nameCompare;
            return Id.CompareTo(u.Id);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(false);
            GC.SuppressFinalize(this);
        }

        #endregion

        ~User()
        {
            Dispose(true);
        }

        internal void Dispose(bool isDisposing)
        {
            if (spriteName != null) spriteName.Dispose();
            if (spriteInfo != null) spriteInfo.Dispose();
            if (spriteStatus != null) spriteStatus.Dispose();
            if (spriteRank != null) spriteRank.Dispose();
        }

        internal float Longitude;
        internal float Latitude;
        public int CountryCode;

        internal bool PullStats(bool force)
        {
            if (StatsLoaded && !force)
                return true;

            if (StatsLoading)
                return false;

            if (Name.Length > 0)
            {
                StatsLoading = true;

                if (Sprites != null)
                {
                    spriteBackground.FlashColour(Color.Crimson, 3000);
                    spriteName.Text = Name;

                    if (firstReceived)
                        spriteInfo.Text = string.Empty;
                }

                if (Name == ConfigManager.sUsername)
                    BanchoClient.SendRequest(RequestType.Osu_RequestStatusUpdate, null);
            }
            else
            {
                Name = @"Guest";
                Level = 0;
                Id = -1;
                Permission = Permissions.Normal;
                InfoText = LocalisationManager.GetString(OsuString.Options_Online_ClickToLogin);

                spriteName.Text = Name;
                spriteInfo.Text = InfoText;
                spriteRank.Text = string.Empty;
                modeStatus.Texture = null;

                LoadAvatarInto(spriteAvatar, spriteAvatar.MaxDimension);

                updateLevelBar();
            }

            return true;
        }

        internal void ReceivePresence(bUserPresence presence)
        {
            Id = presence.userId;
            Name = presence.username;
            Timezone = presence.timezone;

            //never downgrade from an osu! to non-osu presence
            if (!IsOsu || presence.isOsu) IsOsu = presence.isOsu;

            CountryCode = presence.countryCode;
            Location = countryNames[presence.countryCode];

            Longitude = presence.longitude;
            Latitude = presence.latitude;
            Permission = presence.permission;
            Rank = presence.rank;

            if (!StatsLoaded)
                PlayMode = presence.playMode;

            if (spriteAvatar != null) LoadAvatarInto(spriteAvatar, spriteAvatar.MaxDimension);

            UpdateTextFields();
            UpdateColour();

            if (!InitialLoadComplete)
            {
                ChatEngine.CheckFriend(this);
                InitialLoadComplete = true;
            }
        }

        internal void ReceiveUserStats(bUserStats bus)
        {
#if ARCADE
            //Don't support online play yet.
            return;
#endif

            StatsLoaded = true;

            long oldRankedScore = RankedScore;
            double oldAccuracy = Accuracy;
            short oldPp = PerformancePoints;
            int oldRank = Rank;

            long addedScore = 0;
            int i = 0;
            while (addedScore + toNextLevel[i] < bus.totalScore)
            {
                addedScore += toNextLevel[i];
                i++;
            }

            Level = i + 1 + (float)(bus.totalScore - addedScore) / toNextLevel[i];

            RankedScore = bus.rankedScore;
            Accuracy = Math.Round(bus.accuracy * 100, 2);
            PlayCount = bus.playcount;
            PerformancePoints = bus.performance;
            Rank = bus.rank;

            bool playModeChanged = PlayMode != bus.status.playMode;
            PlayMode = bus.status.playMode;

            if (Sprites != null && !firstReceived && (!extended || spriteBackground.IsVisible) && !playModeChanged)
            {
                if (RankedScore != oldRankedScore)
                {
                    bool increase = oldRankedScore < RankedScore;
                    pText p =
                        new pText(
                            (increase ? @"+" : @"") +
                            (RankedScore - oldRankedScore).ToString(@"0,0", GameBase.nfi), 10,
                            spriteInfo.Position + new Vector2(spriteInfo.MeasureText().X, 0) / 2,
                            Vector2.Zero, 0.98F, false,
                            Color.YellowGreen, true);
                    p.AggressiveCleanup = true;
                    p.Tag = this;
                    p.MoveTo(spriteInfo.Position + new Vector2(spriteInfo.MeasureText().X + 2, 0), 1000,
                             EasingTypes.Out);
                    p.FadeOut(6000);
                    spriteManager.Add(p);
                    statUpdateSprite = p;
                }
                if (Accuracy != oldAccuracy)
                {
                    bool increase = oldAccuracy < Accuracy;
                    pText p =
                        new pText(
                            "\n" + (increase ? @"+" : string.Empty) +
                            String.Format(@"{0:n}%", Math.Round(Accuracy - oldAccuracy, 2)), 10,
                            spriteInfo.Position + new Vector2(spriteInfo.MeasureText().X, 0) / 2,
                            Vector2.Zero, 0.98F, false,
                            (increase ? Color.YellowGreen : Color.OrangeRed), true);
                    p.AggressiveCleanup = true;
                    p.MoveTo(spriteInfo.Position + new Vector2(spriteInfo.MeasureText().X + 2, 0), 1000,
                             EasingTypes.Out);
                    p.Tag = this;
                    p.FadeOut(6000);
                    spriteManager.Add(p);
                    statUpdateSprite = p;
                }
                if (Rank != oldRank)
                {
                    bool increase = oldRank > Rank;
                    pText p =
                        new pText((increase ? @"+" : string.Empty) + (-Rank + oldRank), 30,
                                  spriteRank.Position, Vector2.Zero, 0.976F, true,
                                  Color.White, false);
                    p.AggressiveCleanup = true;
                    p.Origin = Origins.TopRight;
                    p.MoveToRelative(new Vector2(0, -19), 1000, EasingTypes.Out);
                    p.Tag = this;
                    p.FadeOut(4000);
                    spriteManager.Add(p);
                    statUpdateSprite = p;
                }
            }

            BeatmapName = bus.status.statusText;
            BeatmapId = bus.status.beatmapId;
            CurrentBeatmapChecksum = bus.status.beatmapChecksum;
            CurrentMods = bus.status.currentMods;
            Status = bus.status.status;

            if (modeStatus != null && (modeStatus.Texture == null || playModeChanged))
                modeStatus.Texture = getModeTexture();

            string beatmapName = string.Empty;

            switch (bus.status.status)
            {
                case bStatus.Playing:
                case bStatus.Paused:
                    backgroundColour = new Color(140, 160, 160);
                    beatmapName = @" " + BeatmapName;
                    break;
                case bStatus.Editing:
                    backgroundColour = new Color(160, 60, 60);
                    beatmapName = @" " + BeatmapName;
                    break;
                case bStatus.Modding:
                    backgroundColour = new Color(60, 160, 60);
                    beatmapName = @" " + BeatmapName;
                    break;
                case bStatus.Watching:
                    backgroundColour = new Color(60, 60, 160);
                    beatmapName = @" " + BeatmapName;
                    break;
                case bStatus.Testing:
                    backgroundColour = new Color(160, 60, 160);
                    beatmapName = @" " + BeatmapName;
                    break;
                case bStatus.Multiplayer:
                case bStatus.Lobby:
                    backgroundColour = new Color(164, 108, 28);
                    break;
                case bStatus.Multiplaying:
                    backgroundColour = new Color(221, 190, 0);
                    beatmapName = @" " + BeatmapName;
                    break;
                case bStatus.Submitting:
                    backgroundColour = new Color(139, 238, 180);
                    beatmapName = @" " + BeatmapName;
                    break;
                case bStatus.Afk:
                    backgroundColour = new Color(10, 10, 10);
                    break;
                default:
                    backgroundColour = new Color(10, 29, 75);
                    break;
            }

            if (!extended)
                backgroundColour = new Color(1, 1, 1);

            string newStatus = string.Format("{1:HH:mm} @ {3}\n{0} {4}", Status,
                  DateTime.UtcNow.AddHours(Timezone),
                  (Timezone >= 0 ? @"+" : string.Empty) + Timezone, Location, BeatmapName);
            bool showTemp = newStatus != StatusText;
            StatusText = newStatus;
            if (showTemp)
                ShowStatusTemporarily();

            firstReceived = false;

            UpdateColour();

            UpdateTextFields();

            StatsLoading = false;
        }

        private void UpdateTextFields()
        {
            if (!StatsLoaded)
                InfoText = string.Empty;
            else
            {
                if (PerformancePoints > 0)
                {
                    InfoText = string.Format(GameBase.nfi, "Performance:{0}pp\nAccuracy:{1:0.00}%\n" + (extended ? @"Play Count: {2} (Lv{3:0})" : @"Lv{3}"), PerformancePoints.ToString(@"#,0", GameBase.nfi), Accuracy, PlayCount, (int)Level, PerformancePoints);
                }
                else if (RankedScore > 0)
                {
                    float score = RankedScore;
                    string suffix = string.Empty;
                    if (score > 1000000000)
                    {
                        suffix = @"b";
                        score /= 1000000000f;
                    }
                    else if (score > 1000000)
                    {
                        suffix = @"m";
                        score /= 1000000f;
                    }
                    else if (score > 0)
                    {
                        suffix = @"k";
                        score /= 1000f;
                    }

                    string scoreString = (score % 1 != 0 ? score.ToString(@"#,0.0", GameBase.nfi) : score.ToString(@"#,0", GameBase.nfi)) + suffix;

                    InfoText = string.Format(GameBase.nfi, "Score:{0}\nAccuracy:{1:0.00}%\n" + (extended ? @"Play Count: {2} (Lv{3:0})" : @"Lv{3}"), scoreString, Accuracy, PlayCount, (int)Level);
                }
                else
                    InfoText = LocalisationManager.GetString(OsuString.User_Neverplayed);

                RankText = Rank == 0 ? string.Empty : @"#" + Rank;
            }

            if (Sprites != null)
            {
                spriteInfo.Text = InfoText;
                spriteName.Text = Name;
                if (spriteStatus != null)
                    spriteStatus.Text = StatusText;
                updateLevelBar();
                spriteRank.Text = RankText;
            }
        }

        private void updateLevelBar()
        {
            if (levelBarForeground != null)
                levelBarForeground.DrawWidth = (int)(198 * (Level - (int)Level));
        }

        private void ShowStatusTemporarily()
        {
            if (!Visible || spriteStatus == null)
                return;

            spriteInfo.FadeOut(100);
            spriteStatus.FadeIn(100);



            spriteStatus.Transformations.Add(
                new Transformation(TransformationType.Fade, 1, 0, GameBase.Time + 4000,
                                   GameBase.Time + 5000));
            spriteInfo.Transformations.Add(
                new Transformation(TransformationType.Fade, 0, 1, GameBase.Time + 4000,
                                   GameBase.Time + 5000));
        }

        Vector2 currentPosition;

        /// <summary>
        /// Set to false when the user is disconnected. Allows optimised/delayed removal from user list.
        /// </summary>
        public bool Connected = true;

        internal bool DrawAt(Vector2 position, bool ext, int depth)
        {
            extended = ext;

            currentPosition = position;

            if (Sprites == null)
            {
                Sprites = new List<pSprite>();

                float fdepth = 0.92f + depth * 0.0001f;

                Vector2 panelOffset = new Vector2(-4, -4);

                spriteBackground = new pSprite(SkinManager.Load(@"user-bg", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft, Clocks.Game, position + panelOffset, fdepth, true, new Color(backgroundColour.R, backgroundColour.G, backgroundColour.B, (byte)(Visible ? 200 : 0)));
                spriteBackground.OnClick += delegate { AudioEngine.Click(null, @"click-short-confirm"); };
                Sprites.Add(spriteBackground);

                spriteForeground = new pSprite(SkinManager.Load(@"user-border", SkinSource.Osu), Fields.TopLeft, Origins.TopLeft, Clocks.Game, position + panelOffset, fdepth + 0.000002f, true, new Color(0, 0, 0, (byte)(Visible ? 255 : 0)));
                Sprites.Add(spriteForeground);

                spriteName = new pText(Name, 14, position + new Vector2(48, -2), Vector2.Zero, fdepth + 0.000003f, true, new Color(255, 255, 255, (byte)(Visible ? 255 : 0)), true);
                Sprites.Add(spriteName);
                spriteName.AggressiveCleanup = true;

                spriteInfo = new pText(InfoText, 10, position + new Vector2(48, 12), new Vector2(150, 33), fdepth + 0.000003f, true, new Color(255, 255, 255, (byte)(Visible ? 255 : 0)), true);
                spriteInfo.AggressiveCleanup = true;
                spriteInfo.TagNumeric = 2;
                Sprites.Add(spriteInfo);

                spriteRank = new pText(RankText, 36, position + new Vector2(204, extended ? 14 : 7), Vector2.Zero, fdepth + 0.000001f, true, new Color(255, 255, 255, (byte)(Visible ? 255 : 0)), false);
                spriteRank.Origin = Origins.TopRight;
                spriteRank.AggressiveCleanup = true;
                Sprites.Add(spriteRank);

                modeStatus = new pSprite(getModeTexture(), Fields.TopLeft, Origins.TopLeft, Clocks.Game, position + new Vector2(176, 0), fdepth + 0.000002f, true, new Color(255, 255, 255, (byte)(Visible ? 70 : 0))) { TagNumeric = 1 };
                Sprites.Add(modeStatus);

                InitializeAvatar();

                if (extended)
                {
                    spriteStatus = new pText(StatusText, 10, position + new Vector2(48, 12), new Vector2(150, 32), fdepth + 0.000004f, true, new Color(255, 255, 255, 0), true);
                    spriteStatus.AggressiveCleanup = true;
                    spriteStatus.TagNumeric = 1;
                    Sprites.Add(spriteStatus);

                    spriteBackground.OnHover += spriteBackground_OnHover;
                    spriteBackground.OnHoverLost += spriteBackground_OnHoverLost;
                }
                else
                {
                    levelBarBackground = new pSprite(SkinManager.Load(@"levelbar-bg", SkinSource.Osu), position, fdepth + 0.000003f, true, new Color(255, 255, 255, (byte)(Visible && !ext ? 255 : 0)));
                    levelBarBackground.OriginPosition = new Vector2(-120, -62);

                    Sprites.Add(levelBarBackground);
                    levelBarBackground.TagNumeric = 1;


                    levelBarForeground = new pSprite(SkinManager.Load(@"levelbar", SkinSource.Osu), position, fdepth + 0.0000035f, true, new Color(252, 184, 6, (byte)(Visible && !ext ? 255 : 0)));
                    levelBarForeground.OriginPosition = new Vector2(-120, -62);
                    updateLevelBar();
                    Sprites.Add(levelBarForeground);
                    levelBarForeground.TagNumeric = 1;

                    levelBarBackground.Additive = true;
                    levelBarBackground.Alpha = 0.4f;

                    levelBarForeground.Additive = true;
                    levelBarForeground.Alpha = 0.7f;

                    spriteBackground.Additive = true;
                    backgroundColour = spriteBackground.InitialColour = new Color(1, 1, 1);

                    spriteForeground.Bypass = true;
                }

                UpdateColour();
                return true;
            }

            Vector2 orig = Sprites[0].InitialPosition;

            bool moved = false;

            foreach (pSprite p in Sprites)
            {
                Vector2 destination = position + (p.InitialPosition - orig);

                if (destination == p.Position && p.Transformations.Count == 0)
                    continue;

                moved = true;

                if (extended)
                    if (Visible && spriteManager.Alpha > 0 && spriteBackground.IsVisible)
                        p.MoveTo(destination, 500, EasingTypes.OutElasticHalf);
                    else
                    {
                        p.Transformations.RemoveAll(t => t.Type == TransformationType.Movement);
                        p.Position = destination;
                    }
                else
                    p.Position = destination;

                p.InitialPosition = destination;

                if (p.TagNumeric == 0)
                    p.Alpha = Visible ? 1 : 0;
            }

            UpdateColour();

            if (moved)
            {
                //Stat change text gets tagged with the user - lets move these too.
                if (statUpdateSprite != null)
                {
                    if (!statUpdateSprite.IsVisible)
                        statUpdateSprite = null;
                    else
                        statUpdateSprite.MoveTo(position + (statUpdateSprite.Position - orig), 500, EasingTypes.Out);
                }
            }

            return false;
        }

        private pTexture getModeTexture()
        {
            if (RankedScore > 0 || Status == bStatus.Playing || Status == bStatus.Multiplaying || Status == bStatus.Testing)
            {
                switch (PlayMode)
                {
                    case PlayModes.Osu:
                        return SkinManager.Load(@"mode-osu-small", SkinSource.Osu);
                    case PlayModes.Taiko:
                        return SkinManager.Load(@"mode-taiko-small", SkinSource.Osu);
                    case PlayModes.CatchTheBeat:
                        return SkinManager.Load(@"mode-fruits-small", SkinSource.Osu);
                    case PlayModes.OsuMania:
                        return SkinManager.Load(@"mode-mania-small", SkinSource.Osu);
                }
            }

            return null;
        }

        private void UpdateColour()
        {
            if (spriteBackground != null)
            {
                Color dimmedBackground = ColourHelper.Darken(backgroundColour, 0.2f);
                if (Visible && spriteManager.Alpha > 0 && spriteBackground.IsVisible)
                {
                    spriteBackground.FadeColour(backgroundColour, 300, true);
                    spriteForeground.FadeColour(dimmedBackground, 300, true);
                }
                else
                {
                    spriteBackground.Transformations.RemoveAll(t => t.Type == TransformationType.Colour);
                    spriteBackground.InitialColour = spriteBackground.drawColour = backgroundColour;
                    spriteForeground.Transformations.RemoveAll(t => t.Type == TransformationType.Colour);
                    spriteForeground.InitialColour = spriteForeground.drawColour = dimmedBackground;
                }

                spriteBackground.HoverEffect =
                    new Transformation(backgroundColour,
                                       new Color((byte)Math.Min(255, backgroundColour.R + 40),
                                                 (byte)Math.Min(255, backgroundColour.G + 40),
                                                 (byte)Math.Min(255, backgroundColour.B + 40)), 0, 100);
                if (Rank > 200000)
                    spriteRank.TextColour = new Color(255, 255, 255, 20);
                if (Rank > 100000)
                    spriteRank.TextColour = new Color(255, 255, 255, 40);
                else if (Rank > 50000)
                    spriteRank.TextColour = new Color(255, 255, 255, 60);
                else if (Rank > 1000)
                    spriteRank.TextColour = new Color(255, 255, 255, 80);
                else if (Rank > 10)
                    spriteRank.TextColour = new Color(255, 255, 255, 100);
                else if (Rank > 1)
                    spriteRank.TextColour = new Color(244, 218, 73, 120);
                else
                    spriteRank.TextColour = new Color(88, 171, 248, 120);
            }
        }

        private void spriteBackground_OnHoverLost(object sender, EventArgs e)
        {
            spriteInfo.FadeIn(100);
            spriteInfo.IsVisible = true;
            spriteStatus.FadeOut(100);
            spriteForeground.FadeColour(Color.Black, 200);
        }

        private void spriteBackground_OnHover(object sender, EventArgs e)
        {
            AudioEngine.Click(null, @"click-short");
            spriteInfo.FadeOut(100);
            spriteStatus.FadeIn(100);
            spriteStatus.IsVisible = true;
            spriteForeground.FadeColour(Color.White, 200);
        }


        internal void Refresh()
        {
#if ARCADE
            //Don't support online play yet.
            return;
#endif

            //StatsLoading = false;
            PullStats(true);
        }

        internal void Hide()
        {
            Visible = false;
            if (Sprites != null)
            {
                spriteBackground.Bypass = true;
                spriteForeground.Bypass = true;
                spriteInfo.Bypass = true;
                spriteStatus.Bypass = true;
                spriteName.Bypass = true;
                spriteAvatar.Bypass = true;
                spriteRank.Bypass = true;
                modeStatus.Bypass = true;
            }
        }

        internal void Show()
        {
            Visible = true;
            if (Sprites != null)
            {
                spriteBackground.Bypass = false;
                spriteForeground.Bypass = false;
                spriteInfo.Bypass = false;
                spriteName.Bypass = false;
                spriteAvatar.Bypass = false;
                spriteRank.Bypass = false;
                modeStatus.Bypass = false;
                spriteStatus.Bypass = false;
            }
        }

#if ARCADE
        public bool Clickable { get { return false; } set { } }
#else
        public bool Clickable { get { return Sprites[0].HandleInput; } set { Sprites[0].HandleInput = value; } }
#endif

        #region ICloneable Members

        public object Clone()
        {
            User u = MemberwiseClone() as User;
            u.ClearSprites();
            return u;
        }

        private void ClearSprites()
        {
            Sprites = null;
            spriteAvatar = null;
        }

        #endregion

        internal bool RequestPresence(bool fill = true, bool force = false)
        {
            if (InitialLoadComplete && !force) return false;

            PresenceCacheItem i = PresenceCache.Query(Id, fill);
            if (i != null)
            {
                ReceivePresence(i.Presence);
                return true;
            }

            return false;
        }
    }
}
