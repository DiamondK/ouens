using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Online;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Helpers;
using osu.Input.Handlers;
using osu.GameplayElements.Scoring;
using osu.Helpers;
using osu.Audio;

namespace osu.Online.Drawable
{
    internal class LobbyMatch : IComparable<LobbyMatch>
    {
        internal bMatch matchInfo;
        internal List<pSprite> SpriteCollection;
        private pSprite bg;
        private pText infoLeft1;
        private pText infoLeft2;
        private pText infoRight1;
        private pText infoRight2;
        private pSprite password;
        private pSpriteDynamic hostAvatar;
        pSprite hostBackground;
        internal bool Filtered;

        private List<pSpriteDynamic> avatars = new List<pSpriteDynamic>();
        private List<pSprite> avatarBackgrounds = new List<pSprite>();

        public LobbyMatch(bMatch match)
        {
            matchInfo = match;
        }

        public bool DrawAt(Vector2 pos)
        {
            bool firstPopulation = SpriteCollection == null;

            if (firstPopulation)
            {
                position = pos;

                SpriteCollection = new List<pSprite>();

                bg = new pSprite(GameBase.WhitePixel, Fields.TopLeft,
                                     Origins.TopLeft, Clocks.Game, pos, 0.89F, true,
                                     new Color(255, 255, 255, 25));
                bg.VectorScale = new Vector2(GameBase.WindowWidthScaled - 15, 48);
                bg.Scale = 1.6f;
                bg.OnHover += delegate
                {
                    bg.FadeColour(ColourHelper.ChangeAlpha(bg.InitialColour, 50), 100);
                    AudioEngine.Click(null, @"click-short");
                };
                bg.OnHoverLost += delegate { CheckUnhover(); };
                bg.ClickRequiresConfirmation = true;
                bg.OnClick += joinMatch;

                infoLeft1 = new pText(string.Empty, 12, pos + new Vector2(20, 0), 0.9f, true, Color.White);
                infoLeft1.TextBold = true;
                SpriteCollection.Add(infoLeft1);

                infoLeft2 = new pText(string.Empty, 11, pos + new Vector2(56, 13), 0.9f, true, Color.White);
                SpriteCollection.Add(infoLeft2);

                infoRight1 = new pText(string.Empty, 12, pos + new Vector2(240, 0), 0.9f, true, Color.White);
                infoRight1.TextBold = true;
                SpriteCollection.Add(infoRight1);
                infoRight2 = new pText(string.Empty, 11, pos + new Vector2(240, 11), 0.9f, true, Color.White);
                SpriteCollection.Add(infoRight2);

                password = new pSprite(SkinManager.Load(@"lobby-lock", SkinSource.Osu), pos + new Vector2(3, 5), 0.895f, true, Color.TransparentWhite);
                SpriteCollection.Add(password);

                gametype = new pSprite(null, pos + new Vector2(0, 3), 0.894f, true, Color.White);
                SpriteCollection.Add(gametype);

                Vector2 avPos = pos + new Vector2(210, 24);

                hostAvatar = new pSpriteDynamic(null, null, 200, avPos, 0.920001f);
                hostBackground = new pSprite(SkinManager.Load(@"lobby-avatar", SkinSource.Osu), Origins.Centre, avPos, 0.92f, true, playerColour);
                hostBackground.Alpha = 1;
                hostBackground.Scale = 1f;
                hostBackground.HandleInput = true;
                hostBackground.OnHoverLost += delegate { CheckUnhover(); };
                hostBackground.ClickRequiresConfirmation = true;
                hostBackground.OnClick += joinMatch;


                SpriteCollection.Add(hostAvatar);
                SpriteCollection.Add(hostBackground);

                slot_size = !GameBase.Widescreen && matchInfo.slotUsedAboveEight ? 0.75f : 1;

                for (int i = 0; i < bMatch.MAX_PLAYERS - 1; i++)
                {
                    avPos = pos + new Vector2(i * 33f * slot_size + 251.4f, 24f + 10.4f);

                    avatars.Add(new pSpriteDynamic(null, null, 200, avPos, 0.920001f));
                    pSprite abg = new pSprite(SkinManager.Load(@"lobby-avatar", SkinSource.Osu), Origins.Centre, avPos, 0.92f, true, Color.TransparentWhite);
                    abg.Alpha = 0;
                    abg.Scale = 0.58f * slot_size;
                    abg.HandleInput = true;
                    abg.OnHoverLost += delegate { CheckUnhover(); };
                    //Because these also get hovered, if the mouse moves from the avatar to another point outside the LobbyMatch the bg will not return states unless this triggers.

                    abg.OnClick += joinMatch;
                    abg.ClickRequiresConfirmation = true;
                    avatarBackgrounds.Add(abg);
                }

                SpriteCollection.AddRange(avatarBackgrounds);
                foreach (pSpriteDynamic p in avatars)
                    SpriteCollection.Add(p);

                SpriteCollection.Add(bg);
            }
            else
            {
                foreach (pSprite p in SpriteCollection)
                {
                    Vector2 destination = pos + (p.InitialPosition - position);
                    p.MoveTo(destination, 500, EasingTypes.Out);
                }
            }

            password.Bypass = !matchInfo.passwordRequired;

            float avgLevel = 0;
            float avgRank = 0;
            float minLevel = 1000;
            float maxLevel = 0;
            float minRank = 1000000;
            float maxRank = 0;
            bool hasUnranked = false;
            int totalPlayers = 0;

            int k = -1;
            for (int j = 0; j < bMatch.MAX_PLAYERS; j++)
            {
                int s = matchInfo.slotId[j];

                pSpriteDynamic thisAvatar;
                pSprite thisAvatarBackground;

                bool isHost = s == matchInfo.hostId;

                if (!isHost)
                {
                    //Add non-host players to the player list.

                    k++;
                    if (k >= avatars.Count) break;
                    thisAvatar = avatars[k];
                    thisAvatarBackground = avatarBackgrounds[k];

                    if (matchInfo.slotStatus[j] != SlotStatus.Locked)
                    {
                        thisAvatarBackground.Bypass = false;
                        if (matchInfo.slotStatus[j] != SlotStatus.Open)
                        {
                            thisAvatarBackground.HandleInput = true;
                            thisAvatarBackground.FadeColour(playerColour, 200);
                            thisAvatarBackground.FadeTo(1, 200, EasingTypes.None);
                        }
                        else
                        {
                            thisAvatarBackground.HandleInput = false;
                            thisAvatarBackground.FadeColour(openColour, 200);
                            thisAvatarBackground.FadeTo(100 / 255f, 200, EasingTypes.None);
                        }
                    }
                    else
                    {
                        thisAvatarBackground.HandleInput = false;
                        thisAvatarBackground.FadeColour(closedColour, 200);
                        thisAvatarBackground.FadeTo(100 / 255f, 200, EasingTypes.None);
                    }
                }
                else
                {
                    thisAvatar = hostAvatar;
                    thisAvatarBackground = hostBackground;
                }

                if (s >= 0)
                {
                    User u = BanchoClient.GetUserById(s);

                    if (u == null)
                        continue;

                    avgLevel += u.Level;
                    avgRank += u.Rank;

                    if (u.Level < minLevel) minLevel = u.Level;
                    if (u.Level > maxLevel) maxLevel = u.Level;

                    if (u.Rank < minRank)
                    {
                        if (u.Rank == 0)
                            hasUnranked = true;
                        else
                            minRank = u.Rank;
                    }
                    if (u.Rank > maxRank) maxRank = u.Rank;

                    totalPlayers++;

                    if (string.IsNullOrEmpty(thisAvatarBackground.ToolTip) || thisAvatarBackground.ToolTip.StartsWith(User.LOADING_STRING))
                        thisAvatarBackground.ToolTip = string.Format(u.Name + " (#" + u.Rank + ")\n" + u.Location);

                    if (thisAvatar.Tag != u)
                    {
                        thisAvatar.Tag = u;

                        if (u.LoadAvatarInto(thisAvatar, (thisAvatar == hostAvatar ? 63f : 36f) * slot_size))
                        {
                            thisAvatar.OnTextureLoaded += delegate
                            {
                                thisAvatar.Transformations.Add(new Transformation(TransformationType.Scale, 0, thisAvatar.Scale, GameBase.Time, GameBase.Time + 200, EasingTypes.Out));
                                if (!Filtered) thisAvatar.FadeIn(500);
                            };
                        }
                    }
                }
                else
                {
                    if (thisAvatar.Tag != null)
                    {
                        thisAvatarBackground.ToolTip = null;
                        thisAvatar.Tag = null;
                        thisAvatar.Texture = null;
                        thisAvatar.FadeOut(500);
                    }
                }
            }

            avgLevel /= totalPlayers;
            avgRank /= totalPlayers;

            switch (matchInfo.playMode)
            {
                case PlayModes.Osu:
                    gametype.Texture = SkinManager.Load(@"mode-osu-small", SkinSource.Osu);
                    break;
                case PlayModes.CatchTheBeat:
                    gametype.Texture = SkinManager.Load(@"mode-fruits-small", SkinSource.Osu);
                    break;
                case PlayModes.Taiko:
                    gametype.Texture = SkinManager.Load(@"mode-taiko-small", SkinSource.Osu);
                    break;
                case PlayModes.OsuMania:
                    gametype.Texture = SkinManager.Load(@"mode-mania-small", SkinSource.Osu);
                    break;
            }


            bool foundBeatmapBefore = foundBeatmap;
            if (foundChecksum != matchInfo.beatmapChecksum)
            {
                foundBeatmap = !string.IsNullOrEmpty(matchInfo.beatmapChecksum) && BeatmapManager.GetBeatmapByChecksum(matchInfo.beatmapChecksum) != null;
                foundChecksum = matchInfo.beatmapChecksum;
            }

            bg.HandleInput = true;
            if (foundBeatmapBefore != foundBeatmap)
                bg.InitialColour = foundBeatmap ? new Color(185, 64, 255, 25) : new Color(202, 202, 202, 25);

            infoLeft1.InitialColour = matchInfo.inProgress ? Color.Gray : Color.White;
            infoRight1.InitialColour = matchInfo.inProgress ? Color.Gray : Color.White;

            infoLeft1.Text = string.Format(
@"{0} ({3})
{1} / {2} ",
                GeneralHelper.FormatEnum(matchInfo.playMode.ToString()),
                matchInfo.slotUsedCount,
                matchInfo.slotOpenCount,
                GeneralHelper.FormatEnum(matchInfo.matchTeamType.ToString()));

            /*infoLeft2.Text =
                string.Format(minLevel != maxLevel ? @"lv{0:0}-{1:0}" : @"lv{0:0}", minLevel, maxLevel);*/

            if (hasUnranked)
                //maxRank > 0 ensures that at least one non-null rank has been processed.
                infoLeft2.Text = @"rank: " + string.Format(maxRank > 0 ? @"{0:#,#} - ?" : @" ?", minRank, maxRank);
            else
                infoLeft2.Text = @"rank: " + string.Format(minRank != maxRank ? @"{0:#,#} - {1:#,#}" : @"{0:#,#}", minRank, maxRank);

            infoRight1.Text = matchInfo.gameName + @" " + (matchInfo.inProgress ? LocalisationManager.GetString(OsuString.LobbyMatch_InProgress) : string.Empty);
            infoRight2.Text =
                string.IsNullOrEmpty(matchInfo.beatmapChecksum) ? LocalisationManager.GetString(OsuString.LobbyMatch_ChangingBeatmap) :
                string.Format(@"{0}{1}", ModManager.FormatShort(matchInfo.activeMods, true, true), matchInfo.beatmapName);//bofore mapname is better
            infoRight2.InitialColour = new Color(255, 215, 109);

            return firstPopulation;

        }

        private void CheckUnhover()
        {
            if (!bg.drawRectangle.Contains(MouseManager.MousePoint))
                bg.FadeColour(ColourHelper.ChangeAlpha(bg.InitialColour, 25), 100);
        }

        string foundChecksum;
        bool foundBeatmap;
        private pSprite gametype;
        private Vector2 position;
        private static readonly Color openColour = Color.YellowGreen;
        private static readonly Color playerColour = Color.OrangeRed;
        private static readonly Color closedColour = new Color(0, 0, 0, 100);
        private float slot_size;

        void joinMatch(object sender, EventArgs e)
        {
            AudioEngine.Click(null, @"click-short-confirm");
            Lobby.JoinMatch(matchInfo);
        }

        public int CompareTo(LobbyMatch other)
        {
            return matchInfo.matchId.CompareTo(other.matchInfo.matchId);
        }
    }
}