﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Play;
using osu.GameModes.Select.Drawable;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Graphics.UserInterface;
using osu.Input;
using osu.Input.Handlers;
using osu.Online.P2P;
using osu_common;
using osu_common.Libraries.NetLib;
using osu.Graphics;
using osu.Audio;
using osu.Graphics.Notifications;
using osu.GameplayElements.Beatmaps;
using osu.GameModes.Online;
using osu.Configuration;
using osu.Graphics.Primitives;
using osu.Online.Social;

namespace osu.Online
{

    internal delegate void GotDirectDownload(OsuDirectDownload odd);

    internal class OsuDirect : GameComponent
    {
        private const int HEIGHT = 270;
        private const int LIST_HEIGHT = HEIGHT - 20;
        private const int VISIBLE_TIME = 800;
        internal const int WIDTH = 120;
        private const int XPOS = 120;
        private const int XPOSHIDDEN = 2;
        private const int YPOS = 50;
        private static readonly Queue<OnlineBeatmap> ResponseQueue = new Queue<OnlineBeatmap>();
        internal static List<OsuDirectDownload> ActiveDownloads = new List<OsuDirectDownload>();
        internal static OnlineBeatmap RespondingBeatmap;
        private static List<pSprite> ResponseSprites;
        internal static bool WasPlayingAudio;
        internal static bool UpdatePending;

        private readonly pScrollableArea downloadListPanel =
            new pScrollableArea(new Rectangle(XPOS, YPOS + 20, WIDTH, LIST_HEIGHT), Vector2.Zero, true);

        private readonly SpriteManager spriteManager = new SpriteManager(true);
        private bool alwaysShow;
        private pSprite background;
        private int LastShow;
        private bool mustHide;
        private bool delayClose;
        internal bool Visible;

        public OsuDirect(Game game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            background = new pSprite(GameBase.WhitePixel, Fields.TopRight, Origins.TopLeft,
                                     Clocks.Game,
                                     new Vector2(XPOSHIDDEN, YPOS), 0.68f, true,
                                     new Color(0, 0, 0, 180));
            background.Scale = 1.6f;
            background.VectorScale = new Vector2(WIDTH, HEIGHT);
            spriteManager.Add(background);


            pText header = new pText("osu!direct", 14, new Vector2(XPOSHIDDEN, YPOS), 1, true, Color.YellowGreen)
                               {
                                   Field = Fields.TopRight,
                                   TextBold = true
                               };
            spriteManager.Add(header);
            /*BanchoClient.OnPermissionChange += delegate {
                                                            header.Text = (BanchoClient.Permission &
                                                                           Permissions.Supporter) > 0
                                                                              ? "osu!direct"
                                                                              : "osu!p2p"; };*/
            pText header2 = new pText("panel", 12, new Vector2(XPOSHIDDEN - 40, YPOS + 8), 1, true, Color.White);
            header2.Field = Fields.TopRight;
            header2.TextBold = true;
            spriteManager.Add(header2);

            spriteManager.FirstDraw = false; //Don't do skippy stuff for this spritemanager.

            base.Initialize();
        }

        internal void Show()
        {
            Show(false);
        }

        internal void Show(bool extended)
        {
            LastShow = GameBase.Time;
            if (extended) LastShow += 2000;

            if (Visible) return;

            AudioEngine.PlaySample(@"select-expand");

            spriteManager.SpriteList.ForEach(
                s => s.MoveTo(new Vector2(s.InitialPosition.X + WIDTH - 2, s.InitialPosition.Y), 400, EasingTypes.Out));
            Visible = true;
        }
        internal void Hide()
        {
            if (!Visible) return;

            int duration;

            if (delayClose)
            {
                duration = 400;
            }
            else
            {
                duration = 0;
                spriteManager.SpriteList.ForEach(s => s.Hide());
            }
            if (LastShow + VISIBLE_TIME > GameBase.Time) return;

            spriteManager.SpriteList.ForEach(
                s => s.MoveTo(new Vector2(s.InitialPosition.X, s.InitialPosition.Y), duration, EasingTypes.Out));

            if (!delayClose)
                spriteManager.SpriteList.ForEach(s => s.Show());

            Visible = false;
            delayClose = true;
        }

        internal void Draw()
        {
            if (GameBase.Tournament) return;

            if (Player.Playing /*|| (BanchoClient.Permission & Permissions.Supporter) == 0*/ ||
                (mustHide && background.Position == background.InitialPosition))
                return;

            float height = GameBase.Mode == OsuModes.OnlineSelection ? GameBase.WindowHeightScaled : HEIGHT - 20;

            background.VectorScale.Y = height + 20;

            spriteManager.HandleOverlayInput = RespondingBeatmap != null && !(ChatEngine.IsVisible && ChatEngine.IsFullVisible);

            spriteManager.Draw();

            downloadListPanel.SetDisplayRectangle(new RectangleF((int)Math.Ceiling(background.CurrentPositionActual.X + 1),
                                                    YPOS + 20, WIDTH,
                                                    (int)height));

            //Need to set alpha etc. here so hover sprites are released correctly.
            downloadListPanel.SpriteManager.HandleOverlayInput = Visible && !spriteManager.HandleOverlayInput;
            downloadListPanel.SpriteManager.Alpha = RespondingBeatmap == null ? 1 : 0;

            downloadListPanel.Draw();
        }

        const int MAX_CONCURRENT_DOWNLOADS = 2;

        public override void Update()
        {
            downloadListPanel.Update();

            if (sampleTrack != null)
            {
                if (sampleTrack.IsPlaying)
                    sampleTrack.Volume = AudioEngine.VolumeMusicAdjusted / 100f;
                else
                    ResetAudioPreview();
            }

            //if (GameBase.Mode == OsuModes.Play) //closes dialogs with active samples playing
            //    HideResponseDialog();

            if (ActiveDownloads.Count > 0 || RespondingBeatmap != null)
            {
                delayClose = true;
            }
            else if (GameBase.Mode == OsuModes.OnlineSelection)
            {
                delayClose = false;
            }

            alwaysShow = GameBase.Mode == OsuModes.OnlineSelection || RespondingBeatmap != null;
            mustHide = ActiveDownloads.Count == 0 && !alwaysShow;

            if (!mustHide &&
                (alwaysShow ||
                 (!Player.Playing &&
                  (background.drawRectangle.Contains(MouseManager.MousePoint) ||
                   MouseManager.MousePosition.X > GameBase.WindowWidth))))
                Show();
            else
                Hide();

            if (UpdatePending)
            {
                int y = 0;

                int ActiveCount = ActiveDownloads.FindAll(d => d.Status > DownloadStatus.Waiting).Count;

                UpdatePending = false;

                for (int i = 0; i < ActiveDownloads.Count; i++)
                {
                    OsuDirectDownload d = ActiveDownloads[i];

                    if (d.Status == DownloadStatus.Waiting && ActiveCount < MAX_CONCURRENT_DOWNLOADS)
                    {
                        d.Start();
                        ActiveCount++;
                    }
                    else if (d.Status == DownloadStatus.Completed)
                    {
                        if (d.SpriteCollection != null)
                        {
                            foreach (pSprite p in d.SpriteCollection)
                            {
                                if (Visible || (Player.Playing && !ConfigManager.sPopupDuringGameplay.Value))
                                    p.FadeOut(300);
                                else
                                {
                                    downloadListPanel.SpriteManager.Remove(p);
                                    GameBase.spriteManagerOverlay.Add(p);

                                    p.Field = Fields.TopRight;

                                    p.Position = new Vector2(XPOSHIDDEN, p.Position.Y + YPOS + 20);

                                    p.FadeOut(2500);
                                    p.MoveTo(new Vector2(XPOS, p.Position.Y), 800, EasingTypes.Out);
                                }
                                p.AlwaysDraw = false;
                            }
                        }
                        ActiveDownloads.RemoveAt(i--);

                        //loop from start again
                        UpdatePending = true;

                        continue;
                    }

                    if (d.DrawAt(new Vector2(0, y)))
                    {
                        downloadListPanel.SpriteManager.Add(d.SpriteCollection);
                        Show(true);
                    }

                    y += 20;
                }

                downloadListPanel.SetContentDimensions(new Vector2(WIDTH, y));
            }

            if (ResponseQueue.Count > 0)
            {
                if (RespondingBeatmap != null)
                    HideResponseDialog();
                ShowResponseDialog(ResponseQueue.Dequeue());
            }

            base.Update();
        }

        static AudioTrack sampleTrack;

        private void ShowResponseDialog(OnlineBeatmap beatmap)
        {
            if ((GameBase.TournamentManager || (ConfigManager.sAutomaticDownload && (MatchSetup.Match != null || StreamingManager.CurrentlySpectating != null)))
                && !beatmap.exists && !beatmap.HasAttachedDownload)
            {
                beatmap.Download();
                return;
            }

            if (GameBase.Tournament)
                return;

            background.FlashColour(Color.Gray, 500);

            RespondingBeatmap = beatmap;
            ResponseSprites = new List<pSprite>();

            float y = 24;

            pText pt = new pText(beatmap.artist, 12, new Vector2(background.Position.X, YPOS + y), 1, true,
                                 Color.Orchid);
            pt.Field = Fields.TopRight;
            pt.TextBold = true;
            ResponseSprites.Add(pt);

            y += 12;

            pt = new pText(beatmap.title, 12, new Vector2(background.Position.X, YPOS + y), 1, true, Color.White);
            pt.Field = Fields.TopRight;

            pt.TextBold = true;
            ResponseSprites.Add(pt);

            y += 12;

            pt = new pText("by " + beatmap.creator, 12, new Vector2(background.Position.X, YPOS + y), 1, true,
                           Color.White);
            pt.Field = Fields.TopRight;
            ResponseSprites.Add(pt);

            y += 14;

            pSprite ps = new pSprite(null, Fields.TopRight,
                                Origins.Centre,
                                Clocks.Game, new Vector2(background.Position.X - (WIDTH / 2), YPOS + y + (WIDTH * 0.8f / 2)), 1, true, Color.TransparentWhite);
            ps.IsDisposable = true;
            ps.Scale = 1.2f;
            ResponseSprites.Add(ps);

            y += 100;

            DataNetRequest dnr = new DataNetRequest(General.STATIC_WEB_ROOT_BEATMAP + @"/thumb/" + beatmap.setId + @"l.jpg");
            dnr.onFinish += delegate(byte[] data, Exception e)
            {
                if (e != null || data.Length == 0) return;

                GameBase.Scheduler.Add(delegate
                {
                    ps.Texture = pTexture.FromBytes(data);
                    SkinManager.RegisterUnrecoverableTexture(ps.Texture);

                    ps.Alpha = 0;
                    if (sampleTrack == null)
                    {
                        ps.FadeTo(0.6f, 500, EasingTypes.Out);
                        ps.Transformations.Add(new Transformation(TransformationType.Scale, 0.7f, 1.1f, GameBase.Time, GameBase.Time + 300, EasingTypes.Out));
                        ps.Transformations.Add(new Transformation(TransformationType.Scale, 1.1f, 1.2f, GameBase.Time + 300, GameBase.Time + 5300));
                    }
                });
            };

            NetManager.AddRequest(dnr);

            if (GameBase.Mode != OsuModes.Play)
            {
                DataNetRequest dnr2 = new DataNetRequest(General.STATIC_WEB_ROOT_BEATMAP + "/preview/" + beatmap.setId + ".mp3");
                dnr2.onFinish += delegate(byte[] data, Exception e)
                {
                    if (e != null || data.Length == 0) return;

                    GameBase.Scheduler.Add(delegate
                    {
                        if (RespondingBeatmap != beatmap)
                            return;

                        WasPlayingAudio = AudioEngine.AudioState == AudioStates.Playing;
                        if (WasPlayingAudio && GameBase.Mode != OsuModes.OnlineSelection && GameBase.Mode != OsuModes.Play)
                        {
                            AudioEngine.AllowRandomSong = false;
                            AudioEngine.TogglePause();
                        }

                        if (sampleTrack != null)
                            sampleTrack.Pause();

                        sampleTrack = AudioEngine.PlaySampleAsTrack(data);
                        sampleTrack.Play();
                        ps.FadeIn(500);
                        ps.Transformations.RemoveAll(t => t.Type == TransformationType.Scale);
                        ps.Transformations.Add(new Transformation(TransformationType.Scale, ps.Scale, 1.25f, GameBase.Time, GameBase.Time + 300, EasingTypes.Out));
                        ps.Transformations.Add(new Transformation(TransformationType.Scale, 1.25f, 1.2f, GameBase.Time + 300, GameBase.Time + 700, EasingTypes.In));

                    });
                };
                NetManager.AddRequest(dnr2);
            }

            bool hasMap = BeatmapManager.GetBeatmapBySetId(beatmap.setId) != null;

            pButton pbut = null;

            float buttonHeight;
            if (hasMap && !beatmap.HasAttachedDownload && beatmap.hasVideo)
            {
                buttonHeight = 16.25f;
            }
            else if (hasMap || (!beatmap.HasAttachedDownload && beatmap.hasVideo))
            {
                buttonHeight = 20f;
            }
            else
            {
                buttonHeight = 25f;
            }


            if (hasMap)
            {
                pbut = new pButton("Go to map",
                                       new Vector2(background.Position.X, YPOS + y), new Vector2(WIDTH, buttonHeight),
                                       0.92f, Color.SkyBlue,
                                       delegate
                                       {
                                           if (MatchSetup.Match != null || Player.Instance != null)
                                               return;

                                           Beatmap b = BeatmapManager.GetBeatmapBySetId(beatmap.setId);

                                           BeatmapManager.Current = b;
                                           GameBase.ChangeMode(OsuModes.SelectPlay, true);

                                           HideResponseDialog();
                                       }, false, true);
                ResponseSprites.AddRange(pbut.SpriteCollection);

                y += buttonHeight + 2;
            }

            pbut = new pButton(beatmap.HasAttachedDownload ? "Cancel DL" : "Download",
                                       new Vector2(background.Position.X, YPOS + y), new Vector2(WIDTH, buttonHeight),
                                       0.92f, Color.Bisque,
                                       delegate
                                       {
                                           beatmap.Download();
                                           HideResponseDialog();
                                       }, false, true);
            ResponseSprites.AddRange(pbut.SpriteCollection);

            y += buttonHeight + 2;

            if (!beatmap.HasAttachedDownload && beatmap.hasVideo)
            {
                pbut = new pButton("DL NoVideo", new Vector2(background.Position.X, YPOS + y),
                                   new Vector2(WIDTH, buttonHeight), 0.92f, Color.BlueViolet,
                                   delegate
                                   {
                                       beatmap.Download(true);
                                       HideResponseDialog();
                                   }, false, true);
                ResponseSprites.AddRange(pbut.SpriteCollection);

                y += buttonHeight + 2;
            }

            pbut = new pButton("Cancel", new Vector2(background.Position.X, YPOS + y), new Vector2(WIDTH, buttonHeight),
                               0.92f, Color.Gray,
                               delegate { HideResponseDialog(); }, false, true);
            ResponseSprites.AddRange(pbut.SpriteCollection);

            y += buttonHeight + 2;

            if (beatmap.postid > 0)
            {
                pbut = new pButton("View Post", new Vector2(background.Position.X, YPOS + y),
                               new Vector2(WIDTH, buttonHeight), 0.92f, Color.YellowGreen,
                               delegate { GameBase.ProcessStart(String.Format(Urls.FORUM_POST, beatmap.postid)); }, false, true);
                ResponseSprites.AddRange(pbut.SpriteCollection);
            }
            else
            {
                pbut = new pButton("View Thread", new Vector2(background.Position.X, YPOS + y),
                               new Vector2(WIDTH, buttonHeight), 0.92f, Color.YellowGreen,
                               delegate { GameBase.ProcessStart(String.Format(Urls.FORUM_TOPIC, beatmap.threadid)); }, false, true);
                ResponseSprites.AddRange(pbut.SpriteCollection);
            }


            y += buttonHeight + 2;

            pbut = new pButton("View Listing", new Vector2(background.Position.X, YPOS + y),
                               new Vector2(WIDTH, buttonHeight), 0.92f, Color.OrangeRed,
                               delegate { GameBase.ProcessStart(String.Format(Urls.BEATMAP_SET_LISTING, beatmap.setId)); }, false, true);
            ResponseSprites.AddRange(pbut.SpriteCollection);


            //The panel isn't yet visible so we are fine to animate as per-usual.
            ResponseSprites.ForEach(s =>
                {
                    s.FadeInFromZero(100);
                });
            spriteManager.Add(ResponseSprites);

            if (background.Transformations.Count > 0)
            {
                Transformation movement = background.Transformations.Find(t => t.Type == TransformationType.Movement);
                if (movement != null)
                {
                    ResponseSprites.ForEach(s => s.Transformations.Add(
                        new Transformation(TransformationType.MovementX, s.InitialPosition.X, s.InitialPosition.X - (background.Position.X - movement.EndVector.X), movement.Time1, movement.Time2, movement.Easing)
                        ));
                }
            }

        }

        private static void HideResponseDialog()
        {
            ResetAudioPreview();

            ResponseSprites.ForEach(s =>
                                        {
                                            s.FadeOut(100);
                                            s.AlwaysDraw = false;
                                        });
            RespondingBeatmap = null;
            WasPlayingAudio = false;
        }

        private static void ResetAudioPreview()
        {
            if (sampleTrack != null)
            {
                sampleTrack.Pause();
                sampleTrack = null;

                if (WasPlayingAudio && GameBase.Mode != OsuModes.OnlineSelection && GameBase.Mode != OsuModes.Play)
                {
                    AudioEngine.AllowRandomSong = true;
                    AudioEngine.TogglePause();
                }
            }
        }

        public static bool StartDownload(OnlineBeatmap ob, bool noVideo, GotDirectDownload callback)
        {
            //check for existing downloads for this beatmap first.
            if (ActiveDownloads.Find(d => d.beatmap.setId == ob.setId) != null)
            {
                NotificationManager.ShowMessageMassive("This beatmap is already being downloaded!", 2000);
                return false;
            }

            GameBase.Scheduler.Add(delegate
            {
                if (RespondingBeatmap == ob)
                    HideResponseDialog();
            });

#if P2P
            OsuMagnetFactory.WebGetOz2Hashes(ob.setid, (hash, dummy) => GameBase.Scheduler.Add(() =>
            {
                OsuDirectDownload odd;
                if (hash!=null)
                    odd = new P2PDirectDownload(ob, noVideo);
                else
                    odd = new OsuDirectDownload(ob, noVideo);
                StartDownload(odd);
                callback(odd);
            }));
#else
            OsuDirectDownload odd = new OsuDirectDownload(ob, noVideo);

            StartDownload(odd);
            callback(odd);
#endif

            return true;

        }

        public static bool StartDownload(OsuDirectDownload odd)
        {
            if (ActiveDownloads.Contains(odd))
                return false;
            ActiveDownloads.Add(odd);
            UpdatePending = true;
            return true;
        }

        public static void HandlePickup(LinkId idType, int id, EventHandler onFinish)
        {
            HandlePickup(idType, id, onFinish, null);
        }

        public static void HandlePickup(LinkId idType, int id, EventHandler onFinish, EventHandler onFail)
        {
            if ((BanchoClient.Permission & Permissions.Supporter) > 0)
            {
                OnlineBeatmap.FromId(idType, id.ToString(), delegate(object beatmap, EventArgs f)
                                                     {
                                                         OnlineBeatmap ob = beatmap as OnlineBeatmap;

                                                         if (ob == null)
                                                         {
                                                             if (onFail == null)
                                                                 HandlePickupFallback(idType, id);
                                                             else
                                                                 onFail(null, null);
                                                             return;
                                                         }


                                                         QueueBeatmapForResponse(ob);

                                                         ob.OnDownloadFinished += onFinish;
                                                     });

                return;
            }


            HandlePickupFallback(idType, id);
        }

        public static void HandlePickup(string checksum, EventHandler onFinish, EventHandler onFail)
        {
            if ((BanchoClient.Permission & Permissions.Supporter) > 0)
            {
                OnlineBeatmap.FromId(LinkId.Checksum, checksum, delegate(object beatmap, EventArgs f)
                {
                    OnlineBeatmap ob = beatmap as OnlineBeatmap;

                    if (ob == null)
                    {
                        if (onFail != null)
                            onFail(null, null);
                        return;
                    }


                    QueueBeatmapForResponse(ob);

                    ob.OnDownloadFinished += onFinish;
                });

                return;
            }

            if (onFail != null)
                onFail(null, null);
        }

        private static void HandlePickupFallback(LinkId idType, int id)
        {
            switch (idType)
            {
                case LinkId.Beatmap:
                    GameBase.ProcessStart(string.Format(Urls.BEATMAP_LISTING, id));
                    break;
                case LinkId.Set:
                    GameBase.ProcessStart(string.Format(Urls.BEATMAP_SET_LISTING, id));
                    break;
                case LinkId.Post:
                    GameBase.ProcessStart(string.Format(Urls.FORUM_POST, id));
                    break;
                case LinkId.Topic:
                    GameBase.ProcessStart(string.Format(Urls.FORUM_TOPIC, id));
                    break;
            }
        }

        internal static void QueueBeatmapForResponse(OnlineBeatmap ob)
        {
            if (!ResponseQueue.Contains(ob) && RespondingBeatmap != ob)
                ResponseQueue.Enqueue(ob);
        }
    }
}