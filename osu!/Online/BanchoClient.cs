#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Ionic.Zlib;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.GameModes.Menus;
using osu.GameModes.Online;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Input;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu_common.Helpers;
using osu.Graphics;
using System.Diagnostics;
using System.Net.NetworkInformation;
using osu_common.Libraries;
using osu.Helpers;
using osu_common.Updater;
using osu_common.Libraries.NetLib;
using osu.Graphics.Sprites;
using Microsoft.Xna.Framework;
using osu.Graphics.Skinning;
using System.Management;
using osu.Audio;
using osu.Online.Social;

#endregion

namespace osu.Online
{

    /// <summary>
    /// Handles all connectivity to Bancho (osu!'s server component)
    /// </summary>
    internal static partial class BanchoClient
    {
        /// <summary>
        /// The non-changing, constant endpoints.
        /// </summary>
        private static readonly string[] localEndpoints = 
        {
            "http://c.ppy.sh",   //cloudflare
            "http://c1.ppy.sh", //DNS-resolved backup
            //"http://162.243.131.91", //DNS-free staminus
            //"http://c2.ppy.sh:13381"
        };

        /// <summary>
        /// The endpoint list which changes with SetServer().
        /// </summary>
        private static List<string> endpoints = new List<string>(localEndpoints);

        private static Dictionary<string, List<string>> country_endpoints = new Dictionary<string, List<string>>()
        {
            //{"cn", new List<string>() { "http://223.100.98.69:13381" }},
            //{"tw", new List<string>() { "http://223.100.98.69:13381" }},
        };

        private static bool UsingLocalEndpoints = true;
        private static int ActiveEndpointIndex = 0;
        private static string ActiveEndpoint { get { return endpoints[ActiveEndpointIndex < 0 ? 0 : ActiveEndpointIndex % endpoints.Count]; } }

        private static bool checkedAddress = false;

        private const int PING_TIMEOUT = 80000;
        private static Queue<Request> Requests = new Queue<Request>();
        internal static bool Authenticated;
        public static bool Connected;
        private static int errorRetryDelay = 3000;
        private static bool FirstFail = true;
        public static bool InitializationComplete;
        internal static bool InstantReconnect;
        private static int lastActivityTime;
        private static int lastSendTime;
        private static int lastErrorCode = -1;
        internal static Obfuscated<Permissions> Permission = Permissions.None;
        internal static bool PermissionsReceivedOnce;
        internal static int pingTimeout = PING_TIMEOUT;
        private static bBuffer readBuffer = new bBuffer(new byte[32768], 0, 32768);
        private static int readBytes;
        private static int requiredLength;
        private static bool readingHeader = true;
        private static RequestType readType;
        internal static long ReceivedBytes;
        private static bool IsExiting;
        private static bool Running = true;
        internal static long SentBytes;
        private static bStatus Status;
        private static Thread thread;
        internal static Dictionary<int, User> UserDictionary = new Dictionary<int, User>(16384);
        internal static List<User> Users = new List<User>(16384);

        private static bool allowUserSwitching = true;
        internal static bool AllowUserSwitching
        {
            get
            {
                if (allowUserSwitching)
                    return true;
                if (DateTime.Now > AllowUserSwitchingRestoration)
                {
                    allowUserSwitching = true;
                    return true;
                }
                return false;
            }

            set { allowUserSwitching = value; }
        }
        internal static DateTime AllowUserSwitchingRestoration;

        private static SerializationWriter sw = new SerializationWriter(new MemoryStream());

        private static int BUFFER_SIZE = 8192;
        /// <summary>
        /// number of times connection to bancho has failed (consecutively)
        /// </summary>
        internal static int FailCount;

        internal static bool UseBackupBancho;

        internal static event EventHandler LoginResult;
        internal static event VoidDelegate OnConnect;
        internal static event VoidDelegate OnPermissionChange;

        /// <summary>
        /// Blocks.
        /// </summary>
        internal static void Connect()
        {
            InstantReconnect = false;
            Failing = false;

            if (!GameBase.HasLogin)
            {
                Permission = Permissions.None;
                return; //We can't attempt to connect if no username is provided...
            }

            if (!checkedAddress && !GameBase.Tournament)
            {
                GameBase.BanchoStatus.Text = LocalisationManager.GetString(OsuString.BanchoClient_FindingClosestServer);
                checkBanchoAddress();

                int checkTime = 5000;
                int sleepTime = 100;
                while (!checkedAddress && (checkTime -= sleepTime) > 0)
                    Thread.Sleep(sleepTime);
            }

            if (Authenticated)
                GameBase.BanchoStatus.Text = LocalisationManager.GetString(OsuString.BanchoClient_ConnectionLost);
            else
                GameBase.BanchoStatus.Text = LocalisationManager.GetString(OsuString.BanchoClient_ConnectingToBancho);

            try
            {
                pingTimeout = PING_TIMEOUT;

                disconnect();

                ChatEngine.channels[0].messageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.BanchoClient_Connecting), string.Empty, Color.OrangeRed));

                refreshHttp();
                readStream = new MemoryStream();

                using (MemoryStream req = new MemoryStream())
                using (StreamWriter writer = new StreamWriter(req))
                {
                    writer.NewLine = "\n";
                    writer.WriteLine(ConfigManager.sUsername);
                    writer.WriteLine(ConfigManager.sPassword);
                    writer.WriteLine(@"{0}|{1}|{2}|{3}|{4}", General.BUILD_NAME,
                        TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Hours,
                        ConfigManager.sDisplayCityLocation ? @"1" : @"0",
                        GameBase.clientHash,
                        ConfigManager.sBlockNonFriendPM ? @"1" : @"0");
                    writer.Flush();
                    req.Position = 0;

                    string[] headers = { 
                                            @"Content-Length: " + req.Length,
                                            @"osu-version: " + General.BUILD_NAME,
                                       };
                    string[] resHeaders = http.SendRequest(@"POST", ActiveEndpoint, headers, req, readStream);

                    foreach (string h in resHeaders)
                    {
                        int colon = h.IndexOf(':');
                        if (colon <= 0) continue;

                        string key = h.Remove(colon).Trim().ToLower();
                        string val = h.Substring(colon + 1).Trim();

                        switch (key)
                        {
                            case "cho-token":
                                Token = val;
                                break;
                        }
                    }
                }

                if (Token == null) throw new Exception("no token");

                Connected = true;
                errorRetryDelay = 3000;
                GameBase.BanchoStatus.Text = LocalisationManager.GetString(OsuString.BanchoClient_LoggingIn);

                lastSendTime = GameBase.Time;
                FirstFail = true;
                ServerIsRestarting = false;
                downMessage = null;
            }
            catch (Exception e)
            {
                UseBackupBancho = false;

                NetManager.AddRequest(
                    new osu_common.Libraries.NetLib.StringNetRequest(
                        string.Format(General.WEB_ROOT + @"/web/bancho_connect.php?v={0}&u={1}&h={2}&fail={3}", General.BUILD_NAME, ConfigManager.sUsername, ConfigManager.sPassword, ActiveEndpoint)
                    )
                );

                checkDownMessage();

                if (ServerIsRestarting && FailCount <= 10)
                    FailConnection(LocalisationManager.GetString(OsuString.BanchoClient_BanchoRestart), 30000);
                else
                {
                    if (FailCount > 3) checkedAddress = false;

                    if (FailCount == 5) ErrorSubmission.Submit(new OsuError(new Exception(e.ToString())) { Feedback = "connection (" + ActiveEndpoint + ")" });

                    if (FailCount % 2 == 0)
                        ActiveEndpointIndex++;

                    if (FailCount > 1)
                        FailConnection(downMessage ?? LocalisationManager.GetString(OsuString.BanchoClient_ConnectionFailedWillKeepRetrying), 30000);
                    else
                        FailConnection(downMessage ?? LocalisationManager.GetString(OsuString.BanchoClient_ConnectionFailedRetryingIn30s), 30000);
                }
            }
        }

        private static void refreshHttp()
        {
            try
            {
                if (http != null) http.Dispose();
            }
            catch { }

            http = new Http() { TimeOut = 8000 };
        }

        private static void checkBanchoAddress()
        {
            string fxString = "";
            try
            {

                foreach (FrameworkVersion v in FrameworkDetection.GetVersions())
                    fxString += v + "|";
            }
            catch
            {
                fxString = "fail";
            }

            GameBase.arrived[4]++;

            osu_common.Libraries.NetLib.StringNetRequest snr = new osu_common.Libraries.NetLib.StringNetRequest(
                string.Format(General.WEB_ROOT + @"/web/bancho_connect.php?v={0}&u={1}&h={2}&fx={3}&mx={4}",
                    General.BUILD_NAME, ConfigManager.sUsername, ConfigManager.sPassword, fxString.Trim('|'), new string(GameBase.arrived)));

            snr.onFinish += delegate(string result, Exception ex)
            {
                if (ex != null || result.Length == 0 || result.Contains(@"<")) //@"<" check is an naive check to block router/proxy responses from being displayed.
                    return;

                List<string> eps = null;
                if (country_endpoints.TryGetValue(result, out eps))
                    if (!endpoints.Contains(eps[0]))
                        endpoints.InsertRange(result.Length > 2 ? 0 : 1, eps);
                checkedAddress = true;
            };

            osu_common.Libraries.NetLib.NetManager.AddRequest(snr);
        }

        static string downMessage;

        static void checkDownMessage()
        {
            if (downMessage != null) return;
            osu_common.Libraries.NetLib.StringNetRequest snr = new osu_common.Libraries.NetLib.StringNetRequest(General.WEB_ROOT + @"/web/osu-checktweets.php");
            snr.onFinish += delegate(string result, Exception ex)
            {
                if (ex != null || result.Length == 0 || result.Contains(@"<")) //@"<" check is an naive check to block router/proxy responses from being displayed.
                {
                    downMessage = null;
                    return;
                }

                downMessage = result;
                ActiveEndpointIndex = -10;
            };

            osu_common.Libraries.NetLib.NetManager.AddRequest(snr);
        }

        /// <summary>
        /// Handle a failed connection.
        /// </summary>
        private static void FailConnection(string message, int msRetry)
        {
            if (FirstFail)
            {
                FirstFail = false;
                FailCount = 0;
            }

            GameBase.BanchoStatus.Text = message;

            FailCount++;

            lastSendTime = GameBase.Time;
            pingTimeout = msRetry + GameBase.random.Next(-msRetry / 4, msRetry / 4);

            Disconnect(false);
        }

        /// <summary>
        /// Disconnects from bancho.
        /// This should be called to initiate the disconnect process.
        /// The actual disconnect will happen when things free up.
        /// </summary>
        internal static void Disconnect(bool resetTimeout = true)
        {
            if (!Connected || PendingDisconnect) return;

            Failing = false;

            if (GameBase.BanchoStatus != null && GameBase.BanchoStatus.Text == LocalisationManager.GetString(OsuString.BanchoClient_LoggingIn))
                GameBase.BanchoStatus.Text = LocalisationManager.GetString(OsuString.BanchoClient_Disconnected);

            SendRequest(RequestType.Osu_Exit, new bInt(0));

            if (resetTimeout)
                pingTimeout = PING_TIMEOUT;

            PendingDisconnect = true;
        }

        /// <summary>
        /// Disconnects this instance.
        /// Tidies things up internally.
        /// </summary>
        private static void disconnect()
        {
            bool wasConnected = Connected;

            Failing = false;

            if (wasConnected && Requests.Count > 0)
            {
                //try to do one last send to clear pending requests.
                try { send(); }
                catch { }

                if (Authenticated)
                    Requests.Clear();
            }

            PendingDisconnect = false;
            Connected = false;
            InitializationComplete = false;

            SocketErrorCount = 0;

            if (GameBase.User != null)
                GameBase.User.StatsLoading = false;

            Lobby.Status = LobbyStatus.NotJoined;
            GameBase.Scheduler.Add(delegate { MatchSetup.Match = null; });

            //we may want to keep this in some cases, though.
            Token = null;
            Authenticated = false;

            PresenceCache.ResetActiveRequests();

            if (!IsExiting)
            {
                if (wasConnected)
                {
                    Permission = Permissions.None;

                    GameBase.Scheduler.Add(delegate
                    {
                        if (OnPermissionChange != null)
                            OnPermissionChange();
                    });

                    BeatmapManager.BeatmapInfoSendListAll = null;
                    BeatmapManager.BeatmapInfoSendListPartial = null;
                }

                //Clean up spectators.
                StreamingManager.Cleanup();

                GameBase.Scheduler.Add(ChatEngine.ClearChannels);

                OsuCommon.ProtocolVersion = 0;

                GameBase.Scheduler.Add(ChatEngine.ResetUsers);
            }

            ResetReadArray(true);

            //reset joined status so a rejoin attempt will be completed later on
            ChatEngine.channels.ForEach(c => c.Reset());
            ChatEngine.RemoveChannel(@"#multiplayer");
            ChatEngine.RemoveChannel(@"#spectator");
            ChatEngine.Friends.Clear();
        }

        public static bool Start()
        {
            if (thread != null) return false;

            InitializeGraphics();

            thread = new Thread(Run);
            thread.Priority = ThreadPriority.Highest;
            thread.IsBackground = true;
            thread.Start();

            return true;
        }

        /// <summary>
        /// Completely disable bancho from attempting to reconnect.
        /// </summary>
        internal static bool DisableBancho;

        /// <summary>
        /// We fudge the display of chat when in idle states to make sure it doesn't look like it is coming in huge chunks.
        /// This keeps track of the last display time for further randomisation.
        /// </summary>
        static int lastChatMessageDisplay;

        private static void Run()
        {
            Initialize();

            Connect();

            while (Running || Connected)
            {
                if (DisableBancho || !GameBase.HasLogin)
                {
                    GameBase.BanchoStatus.Visible = false;
                    if (Connected) Disconnect();
                }
                else if (InstantReconnect || GameBase.Time - lastSendTime > pingTimeout)
                {
                    if (Connected) FailConnection(LocalisationManager.GetString(OsuString.BanchoClient_ConnectionTimedOut), errorRetryDelay);
                    Connect();

                }

                if (!Connected)
                {
                    Thread.Sleep(!GameBase.HasLogin || DisableBancho ? 1000 : 50);
                    continue;
                }

                if (PendingDisconnect)
                {
                    disconnect();
                    continue;
                }

                //todo: run less often
                //if (Permission != Permissions.None && Permission != GameBase.User.Permission) GameBase.BeginExit(false);
                CheckAfk();

                try
                {
                    send();
                    receive();
                }
                catch (Exception e)
                {
                    Failing = true;

                    int errorCode = e is HttpError ? ((HttpError)e).ErrorCode : -1;

                    if (errorCode == 403)
                        FailConnection(LocalisationManager.GetString(OsuString.BanchoClient_ConnectionTimedOut), errorRetryDelay);
                    else if (errorCode == 520)
                    {
                        // Random cloudflare error, let's just ignore it and try again if it only happens once.
                        refreshHttp();
                        if (lastErrorCode != 520)
                            Failing = false;
                    }
                    else if (e is SocketException || e is System.Net.Sockets.SocketError || e is osu_common.Libraries.NetLib.SocketError)
                        refreshHttp();
                    else
                    {
                        FailConnection(LocalisationManager.GetString(OsuString.BanchoClient_ConnectionTimedOut), errorRetryDelay);
                        errorRetryDelay = Math.Min(120000, (int)(errorRetryDelay * 1.5));
                    }

                    lastErrorCode = errorCode;

                    Thread.Sleep(1000);
                }

                Thread.Sleep(20);
            }
        }

        static int SocketErrorCount = 0;

        private static void Initialize()
        {
            PresenceCache.Initialize();

            initializePrivate();
        }

        private static void InitializeGraphics()
        {
            disconnectionOverlay = new pSprite(SkinManager.Load(@"menu-connection", SkinSource.Osu), Fields.TopLeft, Origins.Centre, Clocks.Game,
                new Vector2(GameBase.WindowWidth / GameBase.WindowRatio - 80, GameBase.WindowHeight / GameBase.WindowRatio - 30), 0.96F, true, Color.TransparentWhite);

            GameBase.spriteManagerOverlay.Add(disconnectionOverlay);
        }

        internal static bool CheckAuth(bool requireSupporter = false)
        {
            if (osuMain.AllowMultipleInstances && (!Connected || (BanchoClient.Permission & Permissions.BAT) == 0))
                return false;

            if (requireSupporter)
            {
#if !ARCADE && Release
                if (!Authenticated || !Connected || (BanchoClient.Permission & Permissions.Supporter) == 0)
                {
                    NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.BanchoClient_NeedSupporter), 4000);
                    return false;
                }
#endif
            }

            return true;
        }

        static object queueLock = new object();
        private static Stream readStream;
        private static string Token;

        /// <summary>
        /// How often requests are made when running with an HTTP protocol.
        /// </summary>
        internal static int RequestInterval;

        /// <summary>
        /// Total packets sent.
        /// </summary>
        internal static int SendCount;

        /// <summary>
        /// Number of pings sent consecutively, with no user data in between.
        /// </summary>
        static int consecutivePingCount;

        /// <summary>
        /// Sends any requests waiting in the outgoing queue.
        /// </summary>
        private static void send(bool force = false)
        {
            if (!force && (!Authenticated || readStream != null)) return;

            RequestInterval = 1000;

            if (!ChatEngine.IsVisible && !(InputManager.ReplayStreaming && GameBase.Mode == OsuModes.Play))
            {
                RequestInterval *= 1 + GameBase.IdleTime / 10000;
                RequestInterval *= 1 + consecutivePingCount;
            }

            RequestInterval = Math.Min(22000, Math.Max(1000, RequestInterval));
            if (GameBase.Tournament) RequestInterval = 1000;

            //override everything
            if (performFastRead) RequestInterval = 0;

            if (Requests.Count == 0)
            {
                if (GameBase.Time - lastSendTime < RequestInterval)
                    return;
                SendRequest(RequestType.Osu_Pong, null);
                consecutivePingCount++;
            }
            else
            {
                consecutivePingCount = 0;
            }

            Request[] requestsBeforeSend = Requests.ToArray();

            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    performFastRead = false;

                    Request r;
                    while (Requests.Count > 0 && ms.Length < (8192 - 2048))
                    {
                        lock (queueLock) r = Requests.Dequeue();
                        SentBytes += r.Send(ms, sw);

                    }

                    if (ms.Length > 0)
                    {
                        ms.Position = 0;

                        readStream = new MemoryStream();
                        http.SendRequest(@"POST", ActiveEndpoint, new string[] { @"Content-Length: " + ms.Length, @"osu-token: " + Token }, ms, readStream);

                        lastSendTime = GameBase.Time;
                        SendCount++;
                    }
                }
            }
            catch
            {
                lock (queueLock)
                {
                    if (Requests.Count == 0)
                        foreach (Request r in requestsBeforeSend)
                            Requests.Enqueue(r);
                }
                throw;
            }

            SocketErrorCount = 0;
            Failing = false;
        }

        static Http http;

        private static void receive()
        {
            bool doChatDelay = lastRequestType == RequestType.Osu_Pong;
            int chatMessageDelay = Math.Max(0, lastChatMessageDisplay - GameBase.Time);

            if (readStream != null)
            {
                if (readStream.Length == 0)
                {
                    readStream = null;
                    return;
                }

                if (readStream.Length > 4096) //likely more data waiting for us.
                    performFastRead = true;
                readStream.Position = 0;
            }

            while (Connected && readStream != null)
            {
                int newBytes = readStream.Read(readBuffer.bBlock, readBuffer.bOffset + readBytes, Math.Min(requiredLength - readBytes, (int)readStream.Length));
                if (readStream.Position == readStream.Length) readStream = null;

                readBytes += newBytes;
                ReceivedBytes += newBytes;

                //Read header data
                if (readingHeader)
                {
                    if (readBytes == requiredLength)
                    {
                        readType = (RequestType)readBuffer.Reader.ReadUInt16();
                        readBuffer.Reader.ReadByte();
                        int length = (int)readBuffer.Reader.ReadUInt32();

#if PEPPY
                            Debug.Print(@"R" + length + @": " + readType + (compression ? @" compressed" : string.Empty));
#endif

                        ResetReadArray(false, length);
                    }
                }

                //Read payload data
                if (readBytes != requiredLength)
                    continue;

                switch (readType)
                {
                    case RequestType.Bancho_AccountRestricted:
                        Restricted = true;
                        break;
                    /* CONNECTION MANAGEMENT */
                    case RequestType.Bancho_ProtocolNegotiation:
                        OsuCommon.ProtocolVersion = new bInt(readBuffer.Reader).number;
                        break;
                    case RequestType.Bancho_LoginReply:
                        GameBase.User.Id = new bInt(readBuffer.Reader).number;
                        switch (GameBase.User.Id)
                        {
                            case -1:
                                NotificationManager.ShowMessage(
                                    LocalisationManager.GetString(OsuString.BanchoClient_AuthenticationFailed),
                                    Color.Red, 10000);

                                ConfigManager.sPassword.Value = string.Empty;

                                Disconnect(true);
                                if (LoginResult != null)
                                    LoginResult(false, null);
                                break;
                            case -2:
                                NotificationManager.ShowMessage(
                                    LocalisationManager.GetString(OsuString.BanchoClient_OldVersion),
                                    Color.Red, 8400);
                                GameBase.CheckForUpdates(true);

                                DisableBancho = true;
                                Disconnect(true);
                                break;
                            case -4:
                            case -3:
                                NotificationManager.ShowMessage("You are banned.  If you think this is an error please contact osu! accounts at accounts@ppy.sh.", Color.Red, 60000);
                                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.BanchoClient_NoMoreAccounts), Color.Red, 60000);

                                DisableBancho = true;

                                ConfigManager.sPassword.Value = string.Empty;

                                AllowUserSwitching = false;
                                AllowUserSwitchingRestoration = DateTime.Now.AddDays(1);

                                Disconnect(true);
                                break;
                            case -5:
                                NotificationManager.ShowMessage(
                                    LocalisationManager.GetString(OsuString.BanchoClient_ErrorOccurred),
                                    Color.Red, 4400);
                                FailConnection(LocalisationManager.GetString(OsuString.BanchoClient_ConnectionFailedRetryingIn30s), 30000);
                                break;
                            case -6:
                                NotificationManager.ShowMessage(
                                    LocalisationManager.GetString(OsuString.BanchoClient_NeedSupporter),
                                    Color.Red, 60000);

                                //force updater to load non-test build next time it is run.
                                ConfigManagerCompact.LoadConfig();
                                ConfigManagerCompact.Configuration[@"u_UpdaterTestBuild"] = @"0";
                                ConfigManagerCompact.SaveConfig();

                                DisableBancho = true;
                                Disconnect(true);
                                break;
                            default:
                                Status = bStatus.Idle;
                                UpdateStatus();

                                Authenticated = true;
                                GameBase.BanchoStatus.Text = LocalisationManager.GetString(OsuString.BanchoClient_ReceivingData);

                                if (OnConnect != null)
                                    OnConnect();
                                if (LoginResult != null)
                                    LoginResult(true, null);
                                break;
                        }
                        break;
                    case RequestType.Bancho_SwitchServer:
                        if (GameBase.IdleTime < new bInt(readBuffer.Reader).number || MatchSetup.Match != null)
                            break;
                        SwitchServer();
                        break;
                    case RequestType.Bancho_SwitchTourneyServer:
                        if (!UsingLocalEndpoints)
                        {
                            NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.BanchoClient_TournamentMatchFinished), Color.LightBlue, 5000);
                            GameBase.Scheduler.AddDelayed(delegate
                            {
                                SetServer(localEndpoints);
                            }, 5000);
                        }
                        else
                        {
                            string tourneyServer = new bString(readBuffer.Reader).text;
                            NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.BanchoClient_TournamentMatchReady), 5), Color.LightBlue, 5000);
                            GameBase.Scheduler.AddDelayed(delegate
                            {
                                SetServer(new[] { tourneyServer });
                            }, 5000);
                        }
                        break;
                    case RequestType.Bancho_LoginPermissions:
                        PermissionsReceivedOnce = true;
                        Permission = (Permissions)new bInt(readBuffer.Reader).number;
                        GameBase.Scheduler.Add(delegate
                        {
                            if (OnPermissionChange != null)
                                OnPermissionChange();
                        });
                        break;
                    case RequestType.Bancho_Ping:
                        if (Connected && Permission != Permissions.None && Permission != GameBase.User.Permission)
                            GameBase.BeginExit(false);
                        SendRequest(RequestType.Osu_Pong, null);
                        break;
                    case RequestType.Bancho_VersionUpdate:
                        GameBase.CheckForUpdates(false, true);
                        break;
                    case RequestType.Bancho_VersionUpdateForced:
                        GameBase.CheckForUpdates(true, true);
                        break;
                    case RequestType.Bancho_Restart:
                        int restartTime = new bInt(readBuffer.Reader).number;
                        ServerIsRestarting = true;
                        FailConnection(LocalisationManager.GetString(OsuString.BanchoClient_BanchoRestart), restartTime);
                        break;
                    case RequestType.Bancho_TitleUpdate:
                        string text = new bString(readBuffer.Reader).text;
                        if (text == null)
                            Menu.ChangeOnlineImage();
                        else
                        {
                            string[] split = text.Split('|');
                            Menu.ChangeOnlineImage(split[0], split.Length > 1 ? split[1] : null);
                        }
                        break;
                    /* CHAT and MESSAGING */
                    case RequestType.Bancho_Invite:
                        {
                            bMessage msg = new bMessage(readBuffer.Reader);
                            User u = BanchoClient.GetUserByName(msg.sendingClient.ToString());
                            if (u == null || (!ConfigManager.sAllowPublicInvites && !u.IsFriend))
                                break; //don't accept invites from non-friends if specified.

                            ChatEngine.HandleMessage(msg, false);
                        }
                        break;
                    case RequestType.Bancho_UserPMBlocked:
                        {
                            bMessage msg = new bMessage(readBuffer.Reader);
                            User u = BanchoClient.GetUserByName(msg.target);

                            ChatEngine.activeChannel.messageBuffer.Add(new Message(string.Format(LocalisationManager.GetString(OsuString.Bancho_NoFriendPMBlocked), u.Name), string.Empty, Color.OrangeRed));
                        }
                        break;
                    case RequestType.Bancho_TargetIsSilenced:
                        {
                            bMessage msg = new bMessage(readBuffer.Reader);

                            ChatEngine.activeChannel.messageBuffer.Add(new Message(string.Format(LocalisationManager.GetString(OsuString.Bancho_TargetIsSilenced), msg.target), string.Empty, Color.OrangeRed));
                        }
                        break;
                    case RequestType.Bancho_SendMessage:

                        if (doChatDelay && chatMessageDelay < RequestInterval)
                            chatMessageDelay += GameBase.random.Next(0, (RequestInterval - chatMessageDelay) / 2);

                        lastChatMessageDisplay = chatMessageDelay + GameBase.Time;

                        bMessage m = new bMessage(readBuffer.Reader);
                        GameBase.Scheduler.AddDelayed(delegate { ChatEngine.HandleMessage(m, false); }, chatMessageDelay);
                        break;
                    case RequestType.Bancho_HandleIrcChangeUsername:
                        string message = new bString(readBuffer.Reader).text;
                        string oldname = message.Substring(0, message.IndexOf(@">>>>"));
                        string newname = message.Remove(0, message.IndexOf(@">>>>") + 4);

                        GameBase.Scheduler.Add(delegate
                        {
                            ChatEngine.HandleChangeUsername(oldname, newname);
                        });

                        break;
                    case RequestType.Bancho_HandleUserQuit:
                        bUserQuit quit = new bUserQuit(readBuffer.Reader);
                        ChatEngine.HandleUserQuit(quit.UserId, quit.State);
                        break;
                    /* STATISTICS */
                    case RequestType.Bancho_HandleOsuUpdate:
                        bUserStats stats = new bUserStats(readBuffer.Reader);
                        GameBase.Scheduler.Add(delegate
                        {
                            ChatEngine.HandleUserUpdate(stats);
                            UserProfile pr = UserProfile.ActiveProfiles.Find(p => p.User != null && p.User.Id == stats.userId);
                            if (pr != null)
                                pr.DisplayedUser.ReceiveUserStats(stats);

                            if (stats.userId == GameBase.User.Id)
                            {
                                GameBase.User.ReceiveUserStats(stats);
                                ChatEngine.SetPlayModeFilter(stats.status.playMode);
                            }
                        });
                        break;
                    case RequestType.Bancho_UserPresenceSingle:
                        int userId = new bInt(readBuffer.Reader).number;
                        GameBase.Scheduler.Add(delegate
                        {
                            ChatEngine.HandleUserPresence(userId);
                        });
                        break;
                    case RequestType.Bancho_UserPresenceBundle:
                        bListInt list = new bListInt(readBuffer.Reader);
                        GameBase.Scheduler.Add(delegate
                        {
                            foreach (int id in list.list)
                                ChatEngine.HandleUserPresence(id);
                        });
                        break;
                    case RequestType.Bancho_UserPresence:
                        bUserPresence presence = new bUserPresence(readBuffer.Reader);

                        GameBase.Scheduler.Add(delegate
                        {
                            ChatEngine.HandleUserPresence(presence);

                            if (presence.userId == GameBase.User.Id && presence.isOsu)
                            {
                                GameBase.User.ReceivePresence(presence);

                                //To avoid issues with incorrect case and the likes, we should set the local username
                                //to match the server-side one.
                                ConfigManager.sUsername.Value = presence.username;
                            }
                        });
                        break;
                    /* SPECTATING */
                    case RequestType.Bancho_FellowSpectatorJoined:
                        lock (StreamingManager.Spectators)
                        {
                            User u = GetUserById(new bInt(readBuffer.Reader).number);

                            if (u.Id == GameBase.User.Id) //don't add self to other spectators list.
                                break;

                            if (!StreamingManager.FellowSpectators.Contains(u))
                                StreamingManager.FellowSpectators.Add(u);
                            else
                                u.CantSpectate = false;
                            StreamingManager.FellowSpectators.Sort();
                        }
                        break;
                    case RequestType.Bancho_FellowSpectatorLeft:
                        lock (StreamingManager.Spectators)
                        {
                            User u = GetUserById(new bInt(readBuffer.Reader).number);
                            StreamingManager.FellowSpectators.Remove(u);
                        }
                        break;
                    case RequestType.Bancho_SpectatorJoined:
                        //Start sending spectator frames.
                        lock (StreamingManager.Spectators)
                        {
                            User u = GetUserById(new bInt(readBuffer.Reader).number);
                            Debug.Assert(u != null);

                            if (u == null)
                                break;

                            u.CantSpectate = false;
                            if (!StreamingManager.Spectators.Contains(u))
                            {
                                StreamingManager.Spectators.Add(u);
                                //    if (u.IsFriend)
                                //        ChatEngine.AddUserLog(string.Format(LocalisationManager.GetString(OsuString.Userlog_Watch),u.Name));
                            }
                            StreamingManager.HasSpectators = true;
                            StreamingManager.Spectators.Sort();
                        }
                        break;
                    case RequestType.Bancho_SpectatorCantSpectate:
                        GetUserById(new bInt(readBuffer.Reader).number).CantSpectate = true;
                        break;

                    case RequestType.Bancho_SpectatorLeft:
                        //Stop sending spectator frames.
                        lock (StreamingManager.Spectators)
                        {
                            User u = GetUserById(new bInt(readBuffer.Reader).number);
                            u.CantSpectate = false;
                            StreamingManager.Spectators.Remove(u);
                            if (StreamingManager.Spectators.Count == 0)
                                StreamingManager.HasSpectators = false;
                        }
                        break;
                    case RequestType.Bancho_SpectateFrames:
                        if (StreamingManager.CurrentlySpectating == null)
                            break;

                        bReplayFrameBundle frames = new bReplayFrameBundle(readBuffer.Reader);
                        GameBase.Scheduler.Add(delegate { StreamingManager.HandleFrames(frames); });
                        //Received some spectator frames.
                        break;
                    case RequestType.Bancho_GetAttention:
                        GameBase.Scheduler.Add(delegate { if (!ChatEngine.IsVisible) ChatEngine.Toggle(); });
                        break;
                    case RequestType.Bancho_Announce:
                        NotificationManager.ShowMessage(new bString(readBuffer.Reader).text.Replace(@"\n", '\n'.ToString()),
                                             Color.Orange, 30000);
                        break;
                    case RequestType.Bancho_MatchUpdate:
                    case RequestType.Bancho_MatchNew:
                        {
                            try
                            {
                                ClientSideMatch match = new ClientSideMatch(readBuffer.Reader);
                                GameBase.Scheduler.Add(delegate { Lobby.IncomingMatch(match); });
                            }
                            catch
                            {
                                //match packet may have been corrupt.
                            }
                        }
                        break;
                    case RequestType.Bancho_MatchChangePassword:
                        {
                            try
                            {
                                bString s = new bString(readBuffer.Reader);
                                GameBase.Scheduler.Add(delegate
                                {
                                    ClientSideMatch match = MatchSetup.Match;
                                    if (match != null) match.gamePassword = s.text;
                                });
                            }
                            catch
                            {
                                //match packet may have been corrupt.
                            }
                        }
                        break;
                    case RequestType.Bancho_MatchStart:
                        {
                            try
                            {
                                ClientSideMatch match = new ClientSideMatch(readBuffer.Reader);
                                GameBase.Scheduler.Add(delegate { MatchSetup.MatchStart(match); });
                            }
                            catch
                            {
                                //match packet may have been corrupt.
                            }
                        }
                        break;
                    case RequestType.Bancho_MatchDisband:
                        Lobby.DisbandedMatch(new bInt(readBuffer.Reader).number);
                        break;
                    case RequestType.Bancho_MatchJoinFail:
                        GameBase.Scheduler.Add(Lobby.OnJoinFail);
                        break;
                    case RequestType.Bancho_MatchJoinSuccess:
                        {
                            try
                            {
                                ClientSideMatch match = new ClientSideMatch(readBuffer.Reader);
                                GameBase.Scheduler.Add(delegate
                                {
                                    if (MatchSetup.Match != null)
                                        //keep the password stored locally so it will appear on the change password dialog.
                                        match.gamePassword = MatchSetup.Match.gamePassword;
                                    MatchSetup.Match = match;
                                    Lobby.OnJoinSuccess();
                                });
                            }
                            catch
                            {
                                //match packet may have been corrupt.
                            }
                        }
                        break;
                    case RequestType.Bancho_MatchScoreUpdate:
                        if (GameBase.Mode == OsuModes.Play && Lobby.Status == LobbyStatus.Play)
                            PlayerVs.MatchScoreUpdate(new bScoreFrame(readBuffer.Reader));
                        else if (GameBase.Mode == OsuModes.MatchSetup)
                            MatchSetup.IncomingScoreUpdate(new bScoreFrame(readBuffer.Reader));
                        break;
                    case RequestType.Bancho_MatchTransferHost:
                        MatchSetup.MatchTransferHost();
                        break;
                    case RequestType.Bancho_MatchAllPlayersLoaded:
                        if (GameBase.Mode == OsuModes.Play && Lobby.Status == LobbyStatus.Play)
                            PlayerVs.AllPlayersLoaded = true;
                        break;
                    case RequestType.Bancho_MatchPlayerFailed:
                        if (GameBase.Mode == OsuModes.Play && Lobby.Status == LobbyStatus.Play)
                            PlayerVs.MatchPlayerFailed(new bInt(readBuffer.Reader).number);
                        break;
                    case RequestType.Bancho_MatchPlayerSkipped:
                        if (GameBase.Mode == OsuModes.Play && Lobby.Status == LobbyStatus.Play)
                            PlayerVs.MatchPlayerSkipped(new bInt(readBuffer.Reader).number);
                        break;
                    case RequestType.Bancho_MatchComplete:
                        if (GameBase.Mode == OsuModes.Play && Lobby.Status == LobbyStatus.Play)
                            PlayerVs.MatchComplete();
                        break;
                    case RequestType.Bancho_MatchSkip:
                        if (GameBase.Mode == OsuModes.Play && Lobby.Status == LobbyStatus.Play)
                            Player.QueueSkip();
                        break;
                    case RequestType.Bancho_ChannelJoinSuccess:
                        string matching = new bString(readBuffer.Reader).text;
                        Channel channel = ChatEngine.channels.Find(ch => ch.Name == matching);
                        if (channel != null)
                            channel.HandleJoinSuccess();
                        else
                            GameBase.Scheduler.Add(delegate { ChatEngine.AddChannel(matching, true, true); });

                        //Initialisation is considered to be complete once channel information is received.
                        if (!InitializationComplete)
                        {
                            InitializationComplete = true;
                            GameBase.BanchoStatus.Text = LocalisationManager.GetString(OsuString.BanchoClient_WelcomeToBancho);
                            GameBase.BanchoStatus.Visible = false;

                            ChatEngine.channels[0].messageBuffer.Add(new Message(string.Format(LocalisationManager.GetString(OsuString.BanchoClient_WelcomeUser), ConfigManager.sUsername), string.Empty, Color.White));
                            ChatEngine.channels[0].messageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.BanchoClient_CommandHelp), string.Empty, Color.LightGray));
                            ChatEngine.channels[0].messageBuffer.Add(new Message(LocalisationManager.GetString(OsuString.BanchoClient_Behaviour), string.Empty, Color.LightGray));
                        }

                        break;
                    case RequestType.Bancho_ChannelAvailable:
                    case RequestType.Bancho_ChannelAvailableAutojoin:
                        {
                            bool autoJoin = readType == RequestType.Bancho_ChannelAvailableAutojoin;
                            Channel c = new Channel(readBuffer.Reader);
                            if (string.IsNullOrEmpty(c.Name))
                                break;
                            GameBase.Scheduler.Add(delegate { ChatEngine.AddChannel(c, autoJoin, false); });
                        }
                        break;
                    case RequestType.Bancho_ChannelListingComplete:
                        GameBase.Scheduler.Add(delegate { ChatEngine.LoadUserChannelOrder(); });
                        break;
                    case RequestType.Bancho_ChannelRevoked:
                        {
                            string s = new bString(readBuffer.Reader).text;
                            GameBase.Scheduler.Add(delegate { ChatEngine.RemoveChannel(s); });
                        }
                        break;
                    case RequestType.Bancho_BeatmapInfoReply:
                        bBeatmapInfoReply bir = new bBeatmapInfoReply(readBuffer.Reader);
                        GameBase.Scheduler.Add(delegate { BeatmapManager.IncomingBeatmapInfoReply(bir); });
                        break;
                    case RequestType.Bancho_FriendsList:
                        ChatEngine.HandleFriendsList(new bListInt(readBuffer.Reader).list);
                        break;
                    case RequestType.Bancho_BanInfo:
                        int length = new bInt(readBuffer.Reader).number;
                        if (length <= 0)
                            AllowUserSwitching = true;
                        else
                        {
                            AllowUserSwitching = false;
                            AllowUserSwitchingRestoration = DateTime.Now.AddSeconds(length);
                        }
                        break;
                    case RequestType.Bancho_Monitor:
                        TriggerMonitor();
                        break;
                    case RequestType.Bancho_UserSilenced:
                        {
                            int user = new bInt(readBuffer.Reader).number;
                            GameBase.Scheduler.Add(delegate
                            {
                                ChatEngine.RemoveUserMessages(user);
                            });
                        }
                        break;
                    case RequestType.Bancho_RTX:
                        string notifytext = new bString(readBuffer.Reader).text;

                        GameBase.Scheduler.AddDelayed(delegate
                        {
                            GameBase.RunBackgroundThread(delegate
                            {
                                GameBase.Scheduler.Add(GameBase.BringToFront);

                                int volBefore = AudioEngine.VolumeMaster.Value;

                                pSprite blackness = new pSprite(GameBase.WhitePixel, Fields.Native, Origins.TopLeft, Clocks.Game, Vector2.Zero, 0, true, Color.Black);
                                blackness.VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight + GameBase.WindowOffsetY);
                                blackness.Alpha = 1;

                                GameBase.Scheduler.Add(delegate
                                {
                                    InputManager.HandleInput = false;
                                    AudioEngine.VolumeMaster.Value = 0;
                                    GameBase.spriteManagerOverlayHighest.Add(blackness);
                                });

                                NotificationManager.ShowMessageMassive(notifytext, 5000);

                                Console.Beep(2000, 100);
                                Console.Beep(2600, 100);

                                Thread.Sleep(100);

                                blackness.FadeOutFromOne(5000);
                                blackness.AlwaysDraw = false;

                                GameBase.Scheduler.Add(delegate { Thread.Sleep(2000); });
                                Thread.Sleep(2000);

                                InputManager.HandleInput = true;
                                AudioEngine.VolumeMaster.Value = volBefore;
                            });
                        }, GameBase.random.Next(3000, 8000));
                        break;
                    case RequestType.Osu_MatchAbort:
                        if (GameBase.Mode == OsuModes.Play && Lobby.Status == LobbyStatus.Play && PlayerVs.Instance != null)
                            PlayerVs.Instance.GotoRanking(true);
                        break;
                }

                ResetReadArray(true); //reset to read the next packet's header.
            }
            
            lastErrorCode = -1;
        }

        private static void CheckAfk()
        {
            bool isAfk = GameBase.Time - lastActivityTime > 150000;
            if (isAfk != (bStatus.Afk == Status))
            {
                if (isAfk)
                    UpdateStatus(bStatus.Afk);
                else
                    UpdateStatus();
            }
        }

        private static void ResetReadArray(bool isHeader, int length = Request.HEADER_LEN)
        {
            readBuffer.Stream.Seek(0, SeekOrigin.Begin);
            readingHeader = isHeader;
            requiredLength = length;
            readBytes = 0;
        }

        internal static void DisconnectImmediately()
        {
            try
            {
                Disconnect(true);
                disconnect();
            }
            catch { }
        }

        internal static void Exit()
        {
            if (!Running) return;

            IsExiting = true;

            try
            {
                Disconnect(true);
            }
            catch { }
        }

        internal static User GetUserById(int id, bool fill = true)
        {
            User val;
            if (UserDictionary.TryGetValue(id, out val))
            {
                val.RequestPresence(fill);
                return val;
            }

            return null;
        }

        internal static User GetUserByNameEx(string name)
        {
            if (string.IsNullOrEmpty(name)) return null;

            return GetUserByName(name) ?? GetUserByNameWithUnderscores(name);
        }

        internal static User GetUserByName(string name)
        {
            lock (Users) return Users.Find(u => string.Compare(u.Name, name, true) == 0 && Connected);
        }

        internal static User GetUserByNameWithUnderscores(string name)
        {
            return GetUserByName(name.Replace(' ', '_'));
        }

        static RequestType lastRequestType;
        private static bool PendingDisconnect;
        private static bool performFastRead;
        private static pSprite disconnectionOverlay;

        static bool failing;
        private static bool Failing
        {
            get { return failing; }
            set
            {
                if (value == failing) return;
                failing = value;

                lock (SpriteManager.SpriteLock)
                {
                    if (failing)
                    {
                        disconnectionOverlay.Transformations.Clear();
                        disconnectionOverlay.FadeTo(0.6f, 500);
                        disconnectionOverlay.Transformations.Add(new Transformation(TransformationType.Fade, 0.6f, 0.4f, GameBase.Time + 500, GameBase.Time + 2100) { Loop = true, LoopDelay = 500 });
                        disconnectionOverlay.Transformations.Add(new Transformation(TransformationType.Fade, 0.4f, 0.6f, GameBase.Time + 2100, GameBase.Time + 2600) { Loop = true, LoopDelay = 2000 });

                    }
                    else
                    {
                        disconnectionOverlay.Transformations.Clear();
                        disconnectionOverlay.FadeOut(500);
                    }
                }
            }
        }

        internal static void SendRequest(RequestType resType, int value)
        {
            lastRequestType = resType;
            SendRequest(new Request(resType, new bInt(value)));
        }

        internal static void SendRequest(RequestType resType, bSerializable obj)
        {
            lastRequestType = resType;
            SendRequest(new Request(resType, obj));
        }

        internal static void SendRequest(Request request)
        {
            lastRequestType = request.type;
            lock (queueLock)
                Requests.Enqueue(request);
        }

        internal static void UpdateStatus(bool force = false)
        {
            UpdateStatus(bStatus.Unknown, force);
        }

        internal static void UpdateStatus(bStatus status, bool force = false)
        {
            string text = string.Empty;
            string checksum = string.Empty;
            int id = 0;
            Mods mods = Mods.None;

            bool sendSong = false;

            if (status == bStatus.Unknown)
            {
                if (BeatmapManager.Current != null)
                {
                    text = (InputManager.ReplayMode
                                ? (StreamingManager.CurrentlySpectating != null
                                       ? StreamingManager.CurrentlySpectating.Name
                                       : InputManager.ReplayScore.playerName) + @" play "
                                : string.Empty) + BeatmapManager.Current.DisplayTitle;
                    checksum = BeatmapManager.Current.BeatmapChecksum;
                    id = BeatmapManager.Current.BeatmapId;
                    mods = ModManager.ModStatus;
                }

                status = bStatus.Idle;

                switch (GameBase.Mode)
                {
                    case OsuModes.Edit:
                        if (BeatmapManager.Current.Creator == ConfigManager.sUsername ||
                            BeatmapManager.Current.Creator.Length == 0)
                            status = bStatus.Editing;
                        else
                            status = bStatus.Modding;
                        sendSong = true;
                        break;
                    case OsuModes.Play:
                        if (InputManager.ReplayMode)
                            status = bStatus.Watching;
                        else if (GameBase.TestMode)
                            status = bStatus.Testing;
                        else if (Lobby.Status == LobbyStatus.Play)
                            status = bStatus.Multiplaying;
                        else
                            status = bStatus.Playing;
                        sendSong = true;
                        break;
                    case OsuModes.Lobby:
                    case OsuModes.MatchSetup:
                    case OsuModes.SelectMulti:
                        status = bStatus.Multiplayer;
                        break;
                    case OsuModes.OnlineSelection:
                        status = bStatus.OsuDirect;
                        break;
                    default:
                        text = string.Empty;
                        checksum = string.Empty;
                        break;
                }
            }

            if (status == Status && !force) return;
            Status = status;

            SendRequest(RequestType.Osu_SendUserStatus, new bStatusUpdate(Status, sendSong, text, checksum, id, mods, Player.Mode));
        }

        public static void Activity()
        {
            lastActivityTime = GameBase.Time;
        }

        public static void AddUser(User user)
        {
            lock (Users) Users.Add(user);
            lock (UserDictionary) UserDictionary.Add(user.Id, user);
        }

        public static int RemovedUsers;
        private static bool ServerIsRestarting;
        private static bool Restricted;
        public static void RemoveUser(User user)
        {
            user.Connected = false;
            RemovedUsers++;
            lock (UserDictionary) UserDictionary.Remove(user.Id);
        }

        public static void Reconnect()
        {
            InstantReconnect = true;
            GameBase.BanchoStatus.Text = LocalisationManager.GetString(OsuString.BanchoClient_Reconnecting);
            Disconnect(true);
        }

        internal static void SwitchServer()
        {
            UseBackupBancho = !BanchoClient.UseBackupBancho;
            Reconnect();
        }

        internal static void SetServer(string[] newEndpoints)
        {
            UsingLocalEndpoints = newEndpoints == localEndpoints;
            endpoints.Clear();
            endpoints.AddRange(newEndpoints);
            ActiveEndpointIndex = 0;
            if (thread != null)
                Reconnect();
        }
    }
}
