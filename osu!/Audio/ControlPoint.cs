using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using osu.GameplayElements.Beatmaps;
using osu_common.Bancho;
using osu_common.Helpers;
using Microsoft.Xna.Framework;
using osu.Helpers;

namespace osu.Audio
{
    internal enum CustomSampleSet
    {
        Default = 0,
        Custom1 = 1,
        Custom2 = 2
    }

    [Flags]
    internal enum EffectFlags
    {
        None = 0,
        Kiai = 1,
        OmitFirstBarLine = 8
    }

    internal class ControlPoint : IComparable<ControlPoint>, ICloneable, bSerializable
    {
        internal double beatLength;
        internal CustomSampleSet customSamples;
        internal double offset;
        internal SampleSet sampleSet;
        internal TimeSignatures timeSignature;
        internal int volume;
        private bool _timingChange = true;
        internal bool timingChange
        {
            get { return _timingChange; }
            set
            {
                if (!value && beatLength >= 0) beatLength = -100;
                //If we change to an inheriting section, force the beatLength to a sane value.

                _timingChange = value;
            }
        }
        internal bool kiaiMode
        {
            get
            {
                return (effectFlags & EffectFlags.Kiai) > 0;
            }
            set
            {
                if (value) effectFlags |= EffectFlags.Kiai;
                else effectFlags &= ~EffectFlags.Kiai;
            }
        }

        internal EffectFlags effectFlags;

        internal float bpmMultiplier
        {
            get
            {
                if (beatLength >= 0) return 1;

                return OsuMathHelper.Clamp((float)-beatLength, 10, 1000) / 100f;
            }
        }

        internal ControlPoint(double offset, double beatLength)
            :
                this(
                offset, beatLength, TimeSignatures.SimpleQuadruple, AudioEngine.CurrentSampleSet != SampleSet.None ? AudioEngine.CurrentSampleSet : BeatmapManager.Current.DefaultSampleSet,
                AudioEngine.CustomSamples, (BeatmapManager.Current != null ? BeatmapManager.Current.SampleVolume : 100), true, EffectFlags.None)
        {
        }

        public ControlPoint()
        { }


        internal ControlPoint(double offset, double beatLength, TimeSignatures timeSignature, SampleSet sampleSet,
                             CustomSampleSet customSamples, int volume, bool timingChange, EffectFlags effectFlags)
        {
            this.offset = offset;
            this.beatLength = beatLength;
            this.timeSignature = timeSignature;
            this.sampleSet = sampleSet == SampleSet.None ? SampleSet.Soft : sampleSet;
            this.customSamples = customSamples;
            this.volume = volume;
            this.timingChange = timingChange;
            this.effectFlags = effectFlags;
        }

        internal double bpm
        {
            get { return beatLength == 0 ? 0 : 60000 / beatLength; }
        }

        public override bool Equals(object obj)
        {
            ControlPoint cp = (ControlPoint)obj;
            if (Math.Abs(cp.offset-offset) > double.Epsilon)
                return false;
            if (cp.timingChange != timingChange)
                return false;
            if (Math.Abs(cp.beatLength - beatLength) > double.Epsilon)
                return false;
            if (cp.kiaiMode != kiaiMode)
                return false;
            if (cp.sampleSet != sampleSet)
                return false;
            if (cp.timeSignature != timeSignature)
                return false;
            if (cp.volume != volume)
                return false;
            if (cp.customSamples != customSamples)
                return false;
            return true;
        }

        #region ICloneable Members

        public object Clone()
        {
            return new ControlPoint(offset, beatLength, timeSignature, sampleSet, customSamples, volume, timingChange, effectFlags);
        }

        #endregion

        #region IComparable<TimingPoint> Members

        public int CompareTo(ControlPoint other)
        {
            if (offset == other.offset)
                return other.timingChange.CompareTo(timingChange);

            return offset.CompareTo(other.offset);
        }

        #endregion

        public override string ToString()
        {
            return String.Format(@"{5}{0:00}:{1:00}:{2:00} {3}/4 {4} {6}{7}{8}", ((int)offset / 60000), (int)offset % 60000 / 1000,
                (int)offset % 1000 / 10, (int)timeSignature, beatLength < 0 ? Math.Round(-100f / beatLength, 2) + @"x sm" : (60000 / beatLength).ToString(@"N") + @"bpm",
                                 !timingChange ? @"^ " : string.Empty,
                                 sampleSet == SampleSet.Soft ? @"S" : @"N",
                                 customSamples == CustomSampleSet.Custom1 ? @":C1" : (customSamples == CustomSampleSet.Custom2 ? @":C2" : string.Empty),
                                 kiaiMode ? @" Ki" : string.Empty);
        }

        public void ReadFromStream(SerializationReader sr)
        {
            beatLength = sr.ReadDouble();
            offset = sr.ReadDouble();
            timingChange = sr.ReadBoolean();
        }

        public void WriteToStream(SerializationWriter sw)
        {
            sw.Write(beatLength);
            sw.Write(offset);
            sw.Write(timingChange);
        }
    }
}