using System;
using System.IO;
using System.Runtime.InteropServices;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Fx;

namespace osu.Audio
{
    class AudioTrackBass : AudioTrack
    {
        internal int audioStreamForwards;
        internal int audioStreamBackwards;
        internal int audioStreamPrefilter;
        internal Stream DataStream;

        internal AudioStates State;

        float initialAudioFrequency;
        float currentAudioFrequency;

        internal bool Looping;

        private BASS_FILEPROCS procs;

        internal AudioTrackBass(Stream data, bool quick = false, bool loop = false)
        {
            procs = new BASS_FILEPROCS(ac_Close, ac_Length, ac_Read, ac_Seek);

            Preview = quick;
            Looping = loop;

            BASSFlag flags = Preview ? 0 : (BASSFlag.BASS_STREAM_DECODE | BASSFlag.BASS_STREAM_PRESCAN);

            DataStream = data;

            if (DataStream == null)
            {
                throw new AudioNotLoadedException();
            }
            else
            {
                DataStream.Seek(0, SeekOrigin.Begin);
                audioStreamPrefilter = Bass.BASS_StreamCreateFileUser(BASSStreamSystem.STREAMFILE_NOBUFFER, flags, procs, IntPtr.Zero);
            }

            if (Preview)
                audioStream = audioStreamForwards = audioStreamPrefilter;
            else
            {
                audioStream = audioStreamForwards = BassFx.BASS_FX_TempoCreate(audioStreamPrefilter, loop ? BASSFlag.BASS_MUSIC_LOOP : BASSFlag.BASS_DEFAULT);
                audioStreamBackwards = BassFx.BASS_FX_ReverseCreate(audioStreamPrefilter, 5f, BASSFlag.BASS_DEFAULT);

                Bass.BASS_ChannelSetAttribute(audioStream, BASSAttribute.BASS_ATTRIB_TEMPO_OPTION_USE_QUICKALGO, Bass.TRUE);
                Bass.BASS_ChannelSetAttribute(audioStream, BASSAttribute.BASS_ATTRIB_TEMPO_OPTION_OVERLAP_MS, 4);
                Bass.BASS_ChannelSetAttribute(audioStream, BASSAttribute.BASS_ATTRIB_TEMPO_OPTION_SEQUENCE_MS, 30);
            }

            Length = (Bass.BASS_ChannelBytes2Seconds(audioStream, Bass.BASS_ChannelGetLength(audioStream)) * 1000);
            Bass.BASS_ChannelGetAttribute(audioStream, BASSAttribute.BASS_ATTRIB_FREQ, ref initialAudioFrequency);
            currentAudioFrequency = initialAudioFrequency;
        }

        internal override void Reset()
        {
            Bass.BASS_ChannelSetAttribute(audioStream, BASSAttribute.BASS_ATTRIB_VOL, (float)AudioEngine.VolumeMusicAdjusted / 100);
            Bass.BASS_ChannelSetAttribute(audioStream, BASSAttribute.BASS_ATTRIB_FREQ, initialAudioFrequency);
            Bass.BASS_ChannelSetAttribute(audioStream, BASSAttribute.BASS_ATTRIB_TEMPO, AudioEngine.CurrentPlaybackRate - 100);

            currentAudioFrequency = initialAudioFrequency;

            base.Reset();
        }

        internal void FreeBass()
        {
            if (audioStream != 0) Bass.BASS_ChannelStop(audioStream);

            if (audioStreamForwards != 0) Bass.BASS_StreamFree(audioStreamForwards);
            if (audioStreamBackwards != 0) Bass.BASS_StreamFree(audioStreamBackwards);
            if (audioStreamPrefilter != 0) Bass.BASS_StreamFree(audioStreamPrefilter);

            audioStream = 0;
            audioStreamForwards = 0;
            audioStreamBackwards = 0;
            audioStreamPrefilter = 0;
        }

        public override void Dispose()
        {
            base.Dispose();

            State = AudioStates.Stopped;

            FreeBass();

            if (DataStream != null)
            {
                DataStream.Dispose();
                DataStream = null;
            }
        }

        internal void ReinitializeStreams()
        {


        }

        internal override bool IsDummyDevice { get { return false; } }

        internal override void Pause()
        {
            if (IsPlaying)
                TogglePause();
        }

        internal bool TogglePause()
        {
            if (IsDisposed) return false;

            if (BASSActive.BASS_ACTIVE_PLAYING == Bass.BASS_ChannelIsActive(audioStream))
            {
                Bass.BASS_ChannelPause(audioStream);
                State = AudioStates.Stopped;
                return true;
            }
            else
            {
                Bass.BASS_ChannelPlay(audioStream, false);
                State = AudioStates.Playing;
                return false;
            }
        }

        internal void SetDirection(bool reverse)
        {
            if (IsDisposed) return;

            if (reverse == IsReversed) return;

            bool wasPlaying = State != AudioStates.Stopped;

            if (wasPlaying) TogglePause();

            long pos = Bass.BASS_ChannelGetPosition(audioStream);

            if (reverse)
                audioStream = audioStreamBackwards;
            else
                audioStream = audioStreamForwards;

            Bass.BASS_ChannelSetPosition(audioStream, pos);
            //Bass.BASS_ChannelSetAttribute(audioStream, BASSAttribute.BASS_ATTRIB_FREQ, audioFrequency);

            if (wasPlaying) TogglePause();
        }

        internal override void Play(bool restart = true)
        {
            State = AudioStates.Playing;
            Bass.BASS_ChannelPlay(audioStream, restart);
        }

        internal override void Seek(double seek)
        {
            Bass.BASS_ChannelSetPosition(audioStream, seek / 1000d);
        }

        #region Raw Filestream handling
        void ac_Close(IntPtr user)
        {
            if (DataStream == null) return;

            try
            {
                DataStream.Close();
            }
            catch { }
        }

        long ac_Length(IntPtr user)
        {
            if (DataStream == null) return 0;

            try
            {
                return DataStream.Length;
            }
            catch
            {
            }

            return 0;
        }

        int ac_Read(IntPtr buffer, int length, IntPtr user)
        {
            if (DataStream == null) return 0;

            try
            {
                if (length > ac_ReadBuffer.Length)
                    ac_ReadBuffer = new byte[length];

                if (!DataStream.CanRead)
                    return 0;

                int readBytes = DataStream.Read(ac_ReadBuffer, 0, length);
                Marshal.Copy(ac_ReadBuffer, 0, buffer, length);
                return readBytes;
            }
            catch
            {
            }

            return 0;

        }

        bool ac_Seek(long offset, IntPtr user)
        {
            if (DataStream == null) return false;

            try
            {
                return DataStream.Seek(offset, SeekOrigin.Begin) == offset;
            }
            catch
            {
            }
            return false;
        }

        byte[] ac_ReadBuffer = new byte[32768];

        #endregion

        internal override double Position
        {
            get
            {
                return Bass.BASS_ChannelBytes2Seconds(audioStream, Bass.BASS_ChannelGetPosition(audioStream)) * 1000;
            }
        }

        public override bool IsPlaying
        {
            get
            {
                return Bass.BASS_ChannelIsActive(audioStream) == BASSActive.BASS_ACTIVE_PLAYING;
            }
        }

        private float volume = -1;
        internal override float Volume
        {
            get
            {
                return volume;
            }

            set
            {
                if (volume == value) return;

                volume = value;

                Bass.BASS_ChannelSetAttribute(audioStreamForwards, BASSAttribute.BASS_ATTRIB_VOL, volume);
                Bass.BASS_ChannelSetAttribute(audioStreamBackwards, BASSAttribute.BASS_ATTRIB_VOL, volume);
            }
        }

        public override void Update()
        {

        }

        public override bool FrequencyLock
        {
            get
            {
                return base.FrequencyLock;
            }
            set
            {
                base.FrequencyLock = value;
                updatePlaybackRate();
            }
        }

        private double playbackRate = 100;
        public override double PlaybackRate
        {
            get
            {
                return playbackRate;
            }
            set
            {
                if (value == playbackRate) return;
                playbackRate = value;

                updatePlaybackRate();
            }
        }

        private void updatePlaybackRate()
        {
            if (!FrequencyLock)
            {
                Bass.BASS_ChannelSetAttribute(audioStream, BASSAttribute.BASS_ATTRIB_FREQ, (float)(currentAudioFrequency * playbackRate / 100.0f));
                Bass.BASS_ChannelSetAttribute(audioStream, BASSAttribute.BASS_ATTRIB_TEMPO, 0);
            }
            else
            {
                Bass.BASS_ChannelSetAttribute(audioStream, BASSAttribute.BASS_ATTRIB_FREQ, (float)currentAudioFrequency);
                Bass.BASS_ChannelSetAttribute(audioStream, BASSAttribute.BASS_ATTRIB_TEMPO, (float)(playbackRate - 100));
            }
        }

        internal void ChangeDevice(int device)
        {
            Bass.BASS_ChannelSetDevice(audioStreamPrefilter, device);
            if (audioStreamPrefilter != audioStreamForwards)
            {
                Bass.BASS_ChannelSetDevice(audioStreamForwards, device);
                Bass.BASS_ChannelSetDevice(audioStreamBackwards, device);
            }
        }
    }
}