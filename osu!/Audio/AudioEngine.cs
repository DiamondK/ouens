using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.GameModes.Menus;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Skinning;
using osu.Helpers;
using osu.Input;
using osu_common;
using osu_common.Helpers;
using osudata;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Fx;
using osu.GameplayElements.Events;
using osu.GameModes.Play.Rulesets;
using Un4seen.Bass.AddOn.Tags;
using osu.GameplayElements.HitObjects;
using System.Text;
using System.Threading;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using System.Text.RegularExpressions;
using osu.Online;
using osu.Online.Social;

namespace osu.Audio
{
    internal static partial class AudioEngine
    {
        internal static bool AllowRandomSong = true;

        internal static bool SeekedBackwards;

        internal static bool IgnoreBeatmapSamples;

        /// <summary>
        /// Allows for a beatmap to play pre/post-roll storyboard etc.
        /// </summary>
        internal static bool ExtendedTime;

        #region General

        private static readonly Dictionary<string, int> SampleCacheBeatmap = new Dictionary<string, int>();
        private static readonly Dictionary<string, int> SampleCacheDefault = new Dictionary<string, int>();
        private static readonly Dictionary<string, int> SampleCacheSkin = new Dictionary<string, int>();
        internal static AudioStates AudioState;

        static List<AudioSample> ActiveSamples = new List<AudioSample>();

        private static double audioTime = 0;
        internal static int cachedSeek;

        private static int ch_sliderSlide;
        private static int ch_sliderWhistle;
        private static int ch_spinnerSpin;

        internal static bool SlideSamplesPlaying
        {
            get
            {
                return BASSActive.BASS_ACTIVE_PLAYING == Bass.BASS_ChannelIsActive(AudioEngine.ch_sliderWhistle)
                    || BASSActive.BASS_ACTIVE_PLAYING == Bass.BASS_ChannelIsActive(AudioEngine.ch_sliderSlide);
            }
        }

        internal static List<SampleCacheItem> SampleEvents = new List<SampleCacheItem>();
        internal static bool Nightcore = false;

        internal static int Time = 0;
        internal static int TimeUnedited = 0;
        internal static int TimeLastFrame = 0;
        internal static int TimeRaw = 0;

        internal static event VoidDelegate OnNewAudioTrack;

        private static CustomSampleSet CurrentCustomSampleSet;
        internal static SampleSet CurrentSampleSet = SampleSet.None;

        private static CustomSampleSet CurrentSlideCustomSampleSet;
        private static SampleSet CurrentSlideSampleSet = SampleSet.None;
        private static SampleSet CurrentSlideSampleSetAdditions = SampleSet.None;

        internal static string AudioArtist;
        internal static float InitialFrequency;
        internal static float AudioFrequency;

        internal static int AudioLength { get { return AudioTrack == null ? 0 : (int)AudioTrack.Length; } }
        internal static AudioTrack AudioTrack;
        internal static string AudioTitle;
        internal static Beatmap LoadedBeatmap
        {
            get { return AudioTrack == null ? null : AudioTrack.Beatmap; }
            set { AudioTrack.Beatmap = value; } //bug: this should not exist, but is needed for BSS for some reason.
        }
        private static CustomSampleSet localCustomSamples;
        internal static double VirtualTimeAccumulated;
        internal static BindableInt VolumeEffect;
        internal static BindableInt VolumeMusic;
        internal static int volumeMusicFade;
        internal static BindableInt VolumeMaster;

        internal static NightcoreBeat nightcoreBeat;

        /// <summary>
        /// Should be added to AudioTime wherever sounding synchronised events to the audio track is involved.
        /// </summary>
        internal static float FrameAverage;

        internal static Dictionary<string, string> Md5Dictionary = new Dictionary<string, string>();
        internal static int Offset;
        internal static Dictionary<int, long> SilenceCache = new Dictionary<int, long>();

        internal static int SyncBeat;
        internal static double SyncBeatProgress;
        internal static bool SyncNewBeat;

        /// <summary>
        /// Array containing IDs for all samples from all samplesets
        /// X -> hitsound type (Normal, Whistle, Finish, Clap)
        /// Y -> SampleSet (Normal, Soft, Drum)
        /// Z -> Custom override (None, Custom1, Custom2)
        /// </summary>
        internal static int[, ,] s_AllSampleSets = new int[4, 3, 3];

        /// <summary>
        /// Array containing IDs for all slidertick sounds across all samplesets
        /// X -> future use
        /// Y -> SampleSet (Normal, Soft, Drum)
        /// Z -> Custom override (None, Custom1, Custom2)
        /// </summary>
        internal static int[, ,] s_AllTicks = new int[1, 3, 3];

        /// <summary>
        /// Array containing IDs for all sliderslide sounds across all samplesets
        /// X -> hitsound type (Normal, Whistle, Drum)
        /// Y -> SampleSet (Normal, Soft)
        /// Z -> Custom override (None, Custom1, Custom2)
        /// </summary>
        internal static int[, ,] s_AllSlides = new int[2, 3, 3];

        private static void updateMusicVolume(object sender, EventArgs e)
        {
            SetVolumeMusicFade(volumeMusicFade);
        }

        private static void updateEffectVolume(object sender, EventArgs e)
        {
            float vol = VolumeEffectAdjusted / 100f * VolumeSample / 100f;

            Bass.BASS_ChannelSetAttribute(ch_sliderSlide, BASSAttribute.BASS_ATTRIB_VOL, vol);
            Bass.BASS_ChannelSetAttribute(ch_sliderWhistle, BASSAttribute.BASS_ATTRIB_VOL, vol);
            Bass.BASS_ChannelSetAttribute(ch_spinnerSpin, BASSAttribute.BASS_ATTRIB_VOL, vol);

            lock (ActiveSamples)
                ActiveSamples.ForEach(i => i.Update());
        }

        private static int clickSoundTime;
        internal static void Click(int? volume = null, string sample = @"menuclick", float speed = 1, bool force = false)
        {
            if (!GameBase.IsActive)
                return;

            if (!volume.HasValue)
                volume = 100;

            if (GameBase.Time - clickSoundTime > 50 || force)
            {
                clickSoundTime = GameBase.Time;
                PlaySamplePositional(sample, speed);
            }
        }

        const int max_consecutive_samples = 5;

        internal static int LoadSample(string name, bool loop = false, bool universal = true, SkinSource source = SkinSource.All)
        {
            if (GameBase.Tournament && !GameBase.TournamentManager && GameBase.TournamentClientId != 0)
                return -1;

            BASSFlag flags = BASSFlag.BASS_SAMPLE_OVER_POS;

            if (loop) flags |= BASSFlag.BASS_SAMPLE_LOOP;

            string defaultFile = name;
            if (!universal)
            {
                string sampleSetPrefix = CurrentSampleSet.ToString().ToLower() + @"-";

                //Add a taiko prefix if we are playing a taiko mode map.
                if (GameBase.Mode == OsuModes.Play && Player.Mode == PlayModes.Taiko)
                {
                    sampleSetPrefix = @"taiko-" + sampleSetPrefix;

                    //Prevent taiko converts from using hitsounds intended for actual taiko maps in the same mapset
                    if (BeatmapManager.Current != null && BeatmapManager.Current.PlayMode != PlayModes.Taiko)
                        source &= ~SkinSource.Beatmap;
                }

                defaultFile = sampleSetPrefix + defaultFile;
            }

            int sample = -1;

            //Check for sample available in the beatmap's folder
            if ((source & SkinSource.Beatmap) > 0 && !IgnoreBeatmapSamples && (universal || (!universal && CurrentCustomSampleSet != CustomSampleSet.Default)) && BeatmapManager.Current != null)
            {
                int ccss = (int)CurrentCustomSampleSet;
                string beatmapFile = defaultFile + (!universal && ccss > 1 ? ccss.ToString() : string.Empty);

                if (!SampleCacheBeatmap.TryGetValue(beatmapFile, out sample))
                {
                    sample = -1;
                    for (int i = 0; i < 3; i++)
                    {
                        string ext;
                        if (i == 0)
                            ext = @".wav";
                        else if (i == 1)
                            ext = @".mp3";
                        else
                            ext = @".ogg";
                        byte[] data = BeatmapManager.Current.GetFileBytes(beatmapFile + ext) ??
                            (BeatmapManager.Current.BeatmapVersion < 5 ? BeatmapManager.Current.GetFileBytes(name + ext) : null);

                        if (data == null) continue;

                        if (data.Length < 1024)
                        {
                            sample = Bass.BASS_SampleCreate(0, 44100, 2, 8, flags);
                            break;
                        }

                        if ((sample = Bass.BASS_SampleLoad(data, 0, data.Length, 8, flags)) != -1) break;
                    }

                    SampleCacheBeatmap[beatmapFile] = sample;
                }
            }

            if (sample != -1) return sample;

            //Check for sample available in skin's folder.
            if ((source & SkinSource.Skin) > 0 && !SkinManager.IsDefault && ConfigManager.sSkinSamples)
            {
                if (!SampleCacheSkin.TryGetValue(defaultFile, out sample))
                {
                    sample = -1;
                    for (int i = 0; i < 3; i++)
                    {
                        string ext;
                        if (i == 0)
                            ext = @"wav";
                        else if (i == 1)
                            ext = @"mp3";
                        else
                            ext = @"ogg";

                        string file = string.Format(@"Skins/{0}/{1}.{2}", SkinManager.Current.RawName, defaultFile, ext);

                        if (!File.Exists(file)) continue;

                        if ((sample = Bass.BASS_SampleLoad(file, 0, 0, 8, flags)) != -1) break;
                    }

                    SampleCacheSkin[defaultFile] = sample;
                }
            }

            if (sample != -1) return sample;
            else if ((source & SkinSource.Osu) == 0) return -1;

            if (SampleCacheDefault.TryGetValue(defaultFile, out sample))
                return sample;

            byte[] s = null;

            if (s == null) s = osu_ui.ResourcesStore.ResourceManager.GetObject(defaultFile) as byte[];
            if (s == null) s = osu_gameplay.ResourcesStore.ResourceManager.GetObject(defaultFile) as byte[];
            if (s == null) s = ResourcesStore.ResourceManager.GetObject(defaultFile) as byte[];

            if (s == null)
            {
                //no default
                return SampleCacheDefault[defaultFile] = -1;
            }

            return SampleCacheDefault[defaultFile] = sample = Bass.BASS_SampleLoad(s, 0, s.Length, max_consecutive_samples, flags);
        }

        /// <summary>
        /// override beatmapcache
        /// </summary>
        /// <param name="name">normal-hitclap3.wav</param>
        /// <param name="sliderAware">as we add loop to all *slider* samples, carefully avoid them if the function is called from a sample event load</param>
        /// <returns></returns>
        internal static int LoadBeatmapSample(string name, bool loop = false, bool sliderAware = false, bool reload = false)
        {
            BASSFlag flags = BASSFlag.BASS_SAMPLE_OVER_POS;
            if (loop)
                flags |= BASSFlag.BASS_SAMPLE_LOOP;
            int sample = -1;
            string shortName = name;
            string prefix = string.Empty;

            bool missingExtension = false;
            if (name.IndexOf('.') > 0)
                shortName = name.Substring(0, name.LastIndexOf('.'));
            else
                missingExtension = true;

            if (sliderAware && shortName.Contains(@"slider"))
            {
                flags &= ~BASSFlag.BASS_SAMPLE_LOOP;
                prefix = @"$slider$";
            }

            if (SampleCacheBeatmap.TryGetValue(prefix + shortName, out sample) && !reload)
                return sample;

            byte[] data = null;
            if (missingExtension || !BeatmapManager.Current.CheckFileExists(name))
            {
                string ext = @".wav";
                for (int i = 0; i < 3; i++)
                {
                    if (i == 1)
                        ext = @".ogg";
                    else if (i == 2)
                        ext = @".mp3";
                    data = BeatmapManager.Current.GetFileBytes(shortName + ext) ??
                        (BeatmapManager.Current.BeatmapVersion < 5 ? BeatmapManager.Current.GetFileBytes(name + ext) : null);
                    if (data != null)
                        break;
                }
            }
            else
            {
                data = BeatmapManager.Current.GetFileBytes(name) ??
                    (BeatmapManager.Current.BeatmapVersion < 5 ? BeatmapManager.Current.GetFileBytes(name) : null);
            }
            if (data != null)
            {
                if (data.Length < 1024)
                    sample = Bass.BASS_SampleCreate(0, 44100, 2, 8, flags);
                else
                    sample = Bass.BASS_SampleLoad(data, 0, data.Length, 8, flags);
            }
            string key = prefix + shortName;
            int old = -1;
            if (SampleCacheBeatmap.TryGetValue(key, out old))
            {
                if (old != -1)
                {
                    Bass.BASS_SampleFree(old);
                }
            }
            SampleCacheBeatmap[key] = sample;
            return sample;
        }

        /// <summary>
        /// used in editor
        /// </summary>
        /// <param name="name"></param>
        internal static void DeleteBeatmapSample(string name)
        {
            int old = -1;
            if (SampleCacheBeatmap.TryGetValue(name, out old))
            {
                if (old != -1)
                    Bass.BASS_SampleFree(old);
                SampleCacheBeatmap.Remove(name);
            }
        }

        internal static List<string> GetAllBeatmapSamples()
        {
            List<string> list = new List<string>(SampleCacheBeatmap.Count);

            string[] files = Directory.GetFiles(BeatmapManager.Current.ContainingFolder);
            foreach (string file in files)
            {
                string name = Path.GetFileName(file);
                switch (Path.GetExtension(file))
                {
                    case @".wav":
                    case @".ogg":
                    case @".mp3":
                        list.Add(name);
                        break;
                }
            }
            return list;
        }

        private static SampleSet[] samplesetShouldLoad = new[] { SampleSet.Normal, SampleSet.Soft, SampleSet.Drum };
        internal static void LoadExtraSamples(int customSampleSet)
        {
            CurrentCustomSampleSet = (CustomSampleSet)customSampleSet;
            int addr = 0;
            foreach (SampleSet ss in samplesetShouldLoad)
            {
                CurrentSampleSet = ss;

                LoadSample(@"hitnormal", false, false);
                LoadSample(@"hitfinish", false, false);
                LoadSample(@"hitwhistle", false, false);
                LoadSample(@"hitclap", false, false);
                LoadSample(@"sliderslide", true, false);
                LoadSample(@"sliderwhistle", true, false);
                LoadSample(@"slidertick", false, false);
            }
        }

        internal static void LoadAllSamples()
        {
            SampleSet temp = CurrentSampleSet;

            CustomSampleSet[] customs = new[] { CustomSampleSet.Default, CustomSampleSet.Custom1, CustomSampleSet.Custom2 };

            foreach (CustomSampleSet css in customs)
            {
                CurrentCustomSampleSet = css;
                int sampleNumber = 0;

                foreach (SampleSet ss in samplesetShouldLoad)
                {
                    CurrentSampleSet = ss;

                    s_AllSampleSets[0, sampleNumber, (int)css] = LoadSample(@"hitnormal", false, false);
                    s_AllSampleSets[2, sampleNumber, (int)css] = LoadSample(@"hitfinish", false, false);
                    s_AllSampleSets[1, sampleNumber, (int)css] = LoadSample(@"hitwhistle", false, false);
                    s_AllSampleSets[3, sampleNumber, (int)css] = LoadSample(@"hitclap", false, false);
                    s_AllTicks[0, sampleNumber, (int)css] = LoadSample(@"slidertick", false, false);
                    s_AllSlides[0, sampleNumber, (int)css] = LoadSample(@"sliderslide", true, false);
                    s_AllSlides[1, sampleNumber, (int)css] = LoadSample(@"sliderwhistle", true, false);

                    sampleNumber++;
                }
            }

            CurrentSampleSet = SampleSet.Soft;
            CurrentCustomSampleSet = CustomSampleSet.Custom1;

            LoadSample(@"failsound");
            LoadSample(@"spinnerspin", true);
            LoadSample(@"spinnerbonus");
            LoadSample(@"spinner-osu");

            LoadSample(@"sectionpass");
            LoadSample(@"sectionfail");

            LoadSample(@"readys");
            LoadSample(@"count3s");
            LoadSample(@"count2s");
            LoadSample(@"count1s");
            LoadSample(@"gos");

            LoadSample(@"combobreak");

            int i = 0;
            while (true)
            {
                if (LoadSample(@"comboburst-" + i) == -1) break;
                i++;
            }

            if (i == 0)
                LoadSample(@"comboburst");

            LoadSample(@"applause");

            CurrentCustomSampleSet = CustomSampleSet.Default;
            CurrentSlideCustomSampleSet = CurrentCustomSampleSet;

            LoadPrioritySamples();

            CurrentSampleSet = temp;
        }

        internal static void LoadPrioritySamples()
        {
            LoadSample(@"menuback", false, true, SkinSource.ExceptBeatmap);
            LoadSample(@"menuhit", false, true, SkinSource.ExceptBeatmap);
            LoadSample(@"menuclick", false, true, SkinSource.ExceptBeatmap);

            LoadSample(@"metronomehigh", false, true, SkinSource.ExceptBeatmap);
            LoadSample(@"metronomelow", false, true, SkinSource.ExceptBeatmap);
        }

        internal static void ClearAllCaches()
        {
            ClearCache(SampleCacheSkin);
            ClearCache(SampleCacheBeatmap);
            ClearCache(SampleCacheDefault);
        }

        private static void ClearCache(Dictionary<string, int> cache)
        {
            foreach (int s in cache.Values)
                Bass.BASS_SampleFree(s);
            cache.Clear();
        }

        internal static void ClearBeatmapCache()
        {
            ClearCache(SampleCacheBeatmap);
        }

        internal static void ClearSkinCache()
        {
            ClearCache(SampleCacheSkin);
        }

        internal static void LoadSampleSet(SampleSet s, bool force)
        {
            if (!force && ((s == CurrentSampleSet && CustomSamples == CurrentCustomSampleSet) ||
                           !(GameBase.Mode == OsuModes.Play || GameBase.Mode == OsuModes.Edit ||
                             GameBase.Mode == OsuModes.SkinSelect)))
                return;

            if (s == SampleSet.None)
                s = SampleSet.Normal;

            CurrentSampleSet = s;
            CurrentCustomSampleSet = CustomSamples;

            LoadSample(@"hitnormal", false, false);
            LoadSample(@"hitfinish", false, false);
            LoadSample(@"hitwhistle", false, false);
            LoadSample(@"hitclap", false, false);
            LoadSample(@"slidertick", false, false);

            if (ch_spinnerSpin != 0 && BASSActive.BASS_ACTIVE_PLAYING == Bass.BASS_ChannelIsActive(ch_spinnerSpin)) Bass.BASS_ChannelStop(ch_spinnerSpin);
            ch_spinnerSpin = Bass.BASS_SampleGetChannel(LoadSample(@"spinnerspin", true), false);

            float vol = VolumeEffectAdjusted / 100f * VolumeSample / 100f;
            Bass.BASS_ChannelSetAttribute(ch_spinnerSpin, BASSAttribute.BASS_ATTRIB_VOL, vol);

            if (force || CurrentSlideSampleSet == SampleSet.None)
                LoadSlidesSamples(s, s, force);
        }

        internal static void LoadSlidesSamples(SampleSet s, SampleSet additions, bool force)
        {
            if (s == SampleSet.None)
                s = CurrentSampleSet;

            if (additions == SampleSet.None)
                additions = s;

            bool slideNeedsUpdate = CurrentCustomSampleSet != CurrentSlideCustomSampleSet || s != CurrentSlideSampleSet;
            bool additionsNeedsUpdate = CurrentCustomSampleSet != CurrentSlideCustomSampleSet || additions != CurrentSlideSampleSetAdditions;

            if (!force && ((!slideNeedsUpdate && !additionsNeedsUpdate) ||
                           !(GameBase.Mode == OsuModes.Play || GameBase.Mode == OsuModes.Edit ||
                             GameBase.Mode == OsuModes.SkinSelect)))
                return;

            CurrentSlideCustomSampleSet = CurrentCustomSampleSet;

            if (slideNeedsUpdate)
            {
                if (ch_sliderSlide != 0 && BASSActive.BASS_ACTIVE_PLAYING == Bass.BASS_ChannelIsActive(ch_sliderSlide)) Bass.BASS_ChannelStop(ch_sliderSlide);
                
                CurrentSlideSampleSet = s;
                ch_sliderSlide = Bass.BASS_SampleGetChannel(GetHitSampleId(HitObjectSoundType.Normal, CurrentSlideSampleSet, CurrentCustomSampleSet, SampleType.Slide), false);
            }

            if (additionsNeedsUpdate)
            {
                if (ch_sliderWhistle != 0 && BASSActive.BASS_ACTIVE_PLAYING == Bass.BASS_ChannelIsActive(ch_sliderWhistle)) Bass.BASS_ChannelStop(ch_sliderWhistle);
                
                CurrentSlideSampleSetAdditions = additions;
                ch_sliderWhistle = Bass.BASS_SampleGetChannel(GetHitSampleId(HitObjectSoundType.Whistle, CurrentSlideSampleSetAdditions, CurrentCustomSampleSet, SampleType.Slide), false);
            }

            float vol = VolumeEffectAdjusted / 100f * VolumeSample / 100f;
            Bass.BASS_ChannelSetAttribute(ch_sliderSlide, BASSAttribute.BASS_ATTRIB_VOL, vol);
            Bass.BASS_ChannelSetAttribute(ch_sliderWhistle, BASSAttribute.BASS_ATTRIB_VOL, vol);
        }

        static bool registered;
        internal static void Initialize()
        {
            if (!registered)
            {
                GameBase.Scheduler.AddDelayed(checkAudioDeviceChanged, 500, true);

                BassNet.Registration(@"poo@poo.com", @"2X25242411252422");
                registered = true;
            }

            VolumeMaster.UnbindAll();
            VolumeMaster.ValueChanged += updateEffectVolume;
            VolumeMaster.ValueChanged += updateMusicVolume;

            VolumeEffect.UnbindAll();
            VolumeEffect.ValueChanged += updateEffectVolume;

            VolumeMusic.UnbindAll();
            VolumeMusic.ValueChanged += updateMusicVolume;

            //Universal samples.
            LoadPrioritySamples();
        }

        static bool automaticVirtualTime;
        internal static bool AutomaticVirtualTime
        {
            get { return automaticVirtualTime; }
            set
            {
                automaticVirtualTime = value;
                if (!automaticVirtualTime && ExtendedTime)
                {
                    if (Time < 0)
                    {
                        EndVirtualTime();
                        SeekTo(0);
                    }
                    else if (Time > AudioLength)
                    {
                        EndVirtualTime();
                        SeekTo(AudioLength);
                    }
                }
            }
        }

        static int lastAudioTimeAccurateSet;
        internal static void UpdateAudioTime()
        {
            if (AudioTrack == null) return;

            AudioTrack.Update();

            if (AutomaticVirtualTime)
            {
                if (AudioEngine.Time < -Offset || AudioEngine.Time > AudioLength)
                {
                    if (!ExtendedTime && AudioState != AudioStates.Playing)
                    {
                        ExtendedTime = true;
                        AudioTrack.Pause();
                    }
                }
                else
                {
                    if (ExtendedTime)
                    {
                        ExtendedTime = false;
                        cachedSeekPending = false;
                        cachedSeek = -1;

                        if (AudioState == AudioStates.Playing)
                        {
                            if (!AudioTrack.IsPlaying)
                            {
                                AudioTrack.Seek(TimeRaw + Offset);
                                AudioTrack.Play();
                            }
                        }
                    }
                }
            }

            //force a stopped state when reaching end of song
            if (!ExtendedTime && AudioState != AudioStates.Stopped &&
                (AudioTrack == null || AudioTrack.Position == AudioTrack.Length))
                AudioState = AudioStates.Stopped;

            double rate = (CurrentPlaybackRate / 100d) * (AudioFrequency / InitialFrequency);
            double increase = GameBase.ElapsedMilliseconds * rate * (IsReversed ? -1 : 1);

            if (ExtendedTime)
            {
                if (AudioState == AudioStates.Playing)
                {
                    if (increase < 60)
                    {
                        VirtualTimeAccumulated += increase;
                        audioTime += increase;
                    }
                }
            }
            else if (cachedSeekPending)
            {
                audioTime = cachedSeek;
                lastAudioTimeAccurateSet = GameBase.Time;
            }
            else if (AudioTrack != null)
            {
                double sourceTime = AudioTrack.Position;

                bool skipInterpolation = AudioState != AudioStates.Playing || audioTime == 0 || sourceTime == 0 || Player.Failed;

                if (!skipInterpolation)
                //try and interpolate time for added smoothness (vista+ has a precision of 10ms using directsound)
                {
                    double proposedTime = audioTime + increase;
                    double diff = proposedTime - sourceTime;

                    proposedTime -= diff / 8; //approach sourceTime slowly to keep as close as possible.

                    diff = proposedTime - sourceTime;

                    double aimDiff = (GameBase.Time - lastAudioTimeAccurateSet < 1500 || CurrentPlaybackRate < 100 ? 11 : 33);

                    if (Math.Abs(diff) > aimDiff * 2)
                    {
                        //hard upper limit where we just ignore interpolation and get back in sync.
                        skipInterpolation = true;
                        if (AudioEngine.AudioFrequency == AudioEngine.InitialFrequency) Debug.Print("interpolation skipped");
                    }
                    else if (diff < -aimDiff)
                    {
                        //running too far behind. move forward by double the amount until we are back in time.
                        audioTime += increase * 2;
                        lastAudioTimeAccurateSet = GameBase.Time;
                    }
                    else if (diff < aimDiff)
                    {
                        //carry on using interpolated calculations assuming we aren't too far ahead.
                        //this is the normal path of execution when everything is going smoothly.
                        audioTime = proposedTime;
                    }
                    else
                    {
                        //running too far ahead. move forward by half the amount until we are back in time.
                        audioTime += increase / 2;
                        lastAudioTimeAccurateSet = GameBase.Time;
                    }
                }

                if (skipInterpolation)
                {
                    audioTime = sourceTime;
                    lastAudioTimeAccurateSet = GameBase.Time;
                }
            }
            else
                audioTime = 0;

            TimeRaw = (int)Math.Round(audioTime);

            Offset = BeatmapManager.Current != null && BeatmapManager.Current.BeatmapVersion < 5 &&
                     GameBase.Mode != OsuModes.Edit
                         ? 24
                         : 0;

            Offset += GameBase.Mode != OsuModes.Edit && BeatmapManager.Current != null
                          ? BeatmapManager.Current.PlayerOffset + BeatmapManager.Current.OnlineOffset
                          : 0;

            if (CurrentPlaybackRate < 100)
                Offset -= (int)(((100 - CurrentPlaybackRate) / 75f) * 5);

            Offset -= (int)((CurrentPlaybackRate / 100f) * ConfigManager.sOffset);

            if (!cachedSeekPending && (AudioState != AudioStates.Stopped || Time != 0))
                Time = TimeRaw - Offset;
            else
                Time = TimeRaw;

            TimeUnedited = Time;
        }

        internal static void Update()
        {
            if (GameBase.SixtyFramesPerSecondFrame && (GameBase.FadeState == FadeStates.FadeIn || GameBase.FadeState == FadeStates.Idle) &&
                volumeMusicFade < 100 && (GameBase.Mode != OsuModes.Rank || volumeMusicFade < 50))
                SetVolumeMusicFade(volumeMusicFade + 2);

#if AUDIO_DEBUG
            if (GameBase.TotalFramesRendered % 300 == 0)
                Debug.Print("SampleEvents: {0} / Default: {1} Skin: {2} Beatmap: {3} / Channels: {4}", SampleEvents.Count, SampleCacheDefault.Count, SampleCacheSkin.Count, SampleCacheBeatmap.Count, ActiveSamples.Count);
#endif


            TimeLastFrame = Time;

            UpdateAudioTime();

            if (AudioState == AudioStates.Seeking)
            {
                // Make sure we seek to the right timing point.
                UpdateActiveTimingPoint(false);
                AudioState = AudioStates.Playing;
            }

            UpdateActiveSampleChannels();

            if (GameBase.Mode != OsuModes.BeatmapImport && (GameBase.Mode != OsuModes.Play || Player.Loaded))
            {
                FrameAverage = (float)(FrameAverage * 0.9 + (Time - TimeLastFrame) * 0.1);

                UpdateTiming();

                if (AudioTrack == null || (!AudioTrack.IsPlaying && !ExtendedTime))
                    AudioState = AudioStates.Stopped;

                UpdateSampleEvents();

                ElapsedMilliseconds = Time - TimeLastFrame;
                SeekedBackwards = AudioTrack == null ? false : AudioTrack.IsReversed;

                UpdateSyncCues();

                if (nightcoreBeat != null) nightcoreBeat.Update();
            }

            if (!ExtendedTime && IsReversed && Time <= 0)
            {
                SetDirection(false);
                Play();
            }

            if (GameBase.SixtyFramesPerSecondFrame)
                Scrobbler.Update();
        }

        private static void UpdateActiveSampleChannels()
        {
            lock (ActiveSamples)
            {
                foreach (AudioSample s in ActiveSamples)
                {
                    if (s.Played)
                        s.Dispose();
                }

                ActiveSamples.RemoveAll(i => i.Disposed);
            }
        }

        private static void UpdateSampleEvents()
        {
            if (AudioState != AudioStates.Playing) return;

            int time = Time;

            foreach (SampleCacheItem e in SampleEvents)
            {
                if (e.Time <= time && e.Time > time - 400)
                {
                    if (!e.Played)
                    {
                        e.AudioSample = PlaySample(e.Sample, e.Volume, 0, true);
                        e.Played = true;
                    }
                }
                else if (GameBase.Mode == OsuModes.Edit)
                    e.Played = false;
            }
        }

        internal static void UpdateSpinSample(float completion)
        {
            Bass.BASS_ChannelSetAttribute(ch_spinnerSpin, BASSAttribute.BASS_ATTRIB_FREQ,
                Math.Min(100000, 20000 + (int)(40000 * completion)));
        }

        internal static AudioTrack PlaySampleAsTrack(string filename, bool loop = false)
        {
            byte[] s = null;

            if (s == null) s = osu_ui.ResourcesStore.ResourceManager.GetObject(filename) as byte[];
            if (s == null) s = osu_gameplay.ResourcesStore.ResourceManager.GetObject(filename) as byte[];
            if (s == null) s = ResourcesStore.ResourceManager.GetObject(filename) as byte[];

            return PlaySampleAsTrack(s);
        }

        internal static AudioTrack PlaySampleAsTrack(byte[] data, bool loop = false)
        {
            if (data == null) return null;

            MemoryStream ms = new MemoryStream(data);
            AudioTrack track = new AudioTrackBass(ms);
            track.Volume = VolumeMusicAdjusted / 100f;
            return track;
        }

        /// <summary>
        /// Play a sample based on filename. Skinnable by default (at a skin but not beatmap level).
        /// </summary>
        /// <returns>Channel</returns>
        internal static AudioSample PlaySample(string filename, int volume = 100, SkinSource source = SkinSource.ExceptBeatmap)
        {
            AudioSample sample = new AudioSample(LoadSample(filename, false, true, source))
            {
                Volume = volume / 100f
            };

            lock (ActiveSamples) ActiveSamples.Add(sample);
            sample.Play();
            return sample;
        }

        /// <summary>
        /// Play a sample from raw data.
        /// </summary>
        internal static AudioSample PlaySample(byte[] data, int volume = 100)
        {
            AudioSample sample = new AudioSample(Bass.BASS_SampleLoad(data, 0, data.Length, 6, 0))
            {
                Volume = volume / 100f,
                FreeSample = true
            };

            lock (ActiveSamples) ActiveSamples.Add(sample);
            sample.Play();
            return sample;
        }

        /// <summary>
        /// Plays sample with balance set depending on current mouse position.
        /// </summary>
        internal static AudioSample PlaySamplePositional(string filename, float speed = 1, SkinSource source = SkinSource.ExceptBeatmap)
        {
            AudioSample sample = new AudioSample(AudioEngine.LoadSample(filename, false, true, source))
            {
                InputBalanceWeight = 0.4f,
                PlaybackRate = speed
            };

            lock (ActiveSamples) ActiveSamples.Add(sample);
            sample.Play();
            return sample;
        }

        /// <summary>
        /// Play sample with effect-adjusted volume.
        /// </summary>
        internal static AudioSample PlaySample(int sampleId, int volume = 100, float balance = 0, bool matchAudioRate = false, float speed = 1)
        {
            if (sampleId == -1) return null;

            if (matchAudioRate && AudioTrack.PlaybackRate != 100 && AudioTrack.SamplesMatchPlaybackRate)
                speed = 1 + ((float)AudioTrack.PlaybackRate - 100) / 100f;

            AudioSample sample = new AudioSample(sampleId)
            {
                Sample = sampleId,
                Volume = volume / 100f,
                Balance = balance,
                PlaybackRate = speed
            };

            lock (ActiveSamples) ActiveSamples.Add(sample);
            sample.Play();
            return sample;
        }

        internal static void PlayHitSamples(HitSoundInfo info, float balance, bool useVolumeFloor)
        {
            int vol = (int)((useVolumeFloor ? Math.Max(8, info.Volume) : info.Volume));
            SampleSet additions = info.SampleSetAdditions == SampleSet.None ? info.SampleSet : info.SampleSetAdditions;

            // Placing Hitnormal at the TOP of the queue because it matters most for the player's timing in case of laggy performance.
            if (info.SoundType.IsType(HitObjectSoundType.Normal))
                AudioEngine.PlaySample(GetHitSampleId(HitObjectSoundType.Normal, info.SampleSet, info.CustomSampleSet, SampleType.Normal), (int)(vol * 0.8), balance, true);

            if (info.SoundType.IsType(HitObjectSoundType.Finish))
                AudioEngine.PlaySample(GetHitSampleId(HitObjectSoundType.Finish, additions, info.CustomSampleSet, SampleType.Normal), vol, balance, true);

            if (info.SoundType.IsType(HitObjectSoundType.Whistle))
                AudioEngine.PlaySample(GetHitSampleId(HitObjectSoundType.Whistle, additions, info.CustomSampleSet, SampleType.Normal), (int)(vol * 0.85), balance, true);

            if (info.SoundType.IsType(HitObjectSoundType.Clap))
                AudioEngine.PlaySample(GetHitSampleId(HitObjectSoundType.Clap, additions, info.CustomSampleSet, SampleType.Normal), (int)(vol * 0.85), balance, true);
        }

        internal static void PlaySlideSamples(HitObjectSoundType soundType, SampleSet sampleSet, SampleSet sampleSetAdditions, bool layeredHitSounds, float? balance)
        {
            LoadSlidesSamples(sampleSet, sampleSetAdditions, false);

            if ((layeredHitSounds || !soundType.IsType(HitObjectSoundType.Whistle)) && BASSActive.BASS_ACTIVE_PLAYING != Bass.BASS_ChannelIsActive(AudioEngine.ch_sliderSlide))
            {
                Bass.BASS_ChannelSetAttribute(AudioEngine.ch_sliderSlide, BASSAttribute.BASS_ATTRIB_VOL, (float)AudioEngine.VolumeSampleAdjusted / 100);
                Bass.BASS_ChannelPlay(AudioEngine.ch_sliderSlide, true);
            }
            if (soundType.IsType(HitObjectSoundType.Whistle) && BASSActive.BASS_ACTIVE_PLAYING != Bass.BASS_ChannelIsActive(AudioEngine.ch_sliderWhistle))
            {
                Bass.BASS_ChannelSetAttribute(AudioEngine.ch_sliderWhistle, BASSAttribute.BASS_ATTRIB_VOL, (float)AudioEngine.VolumeSampleAdjusted / 100);
                Bass.BASS_ChannelPlay(AudioEngine.ch_sliderWhistle, true);
            }

            if (balance.HasValue)
            {
                Bass.BASS_ChannelSetAttribute(AudioEngine.ch_sliderSlide, BASSAttribute.BASS_ATTRIB_PAN, balance.Value);
                Bass.BASS_ChannelSetAttribute(AudioEngine.ch_sliderWhistle, BASSAttribute.BASS_ATTRIB_PAN, balance.Value);
            }
        }

        internal static void PlaySpinSample(float? completion)
        {
            if (completion.HasValue)
                UpdateSpinSample(completion.Value);

            Bass.BASS_ChannelPlay(ch_spinnerSpin, false);
            Bass.BASS_ChannelSetAttribute(ch_spinnerSpin, BASSAttribute.BASS_ATTRIB_VOL, (float)AudioEngine.VolumeSampleAdjusted / 100);
        }

        internal static void PlayTickSamples(HitSoundInfo info, float balance)
        {
            AudioEngine.PlaySample(GetHitSampleId(info.SoundType, info.SampleSet, info.CustomSampleSet, SampleType.Tick), info.Volume, balance);
        }

        private static int GetHitSampleId(HitObjectSoundType SoundType, SampleSet SampleSet, CustomSampleSet CustomSampleSet, SampleType type)
        {
            int Sound = 0, Sample = 0;
            if (type != SampleType.Tick)
            {
                switch (SoundType)
                {
                    case HitObjectSoundType.Normal:
                        Sound = 0;
                        break;
                    case HitObjectSoundType.Whistle:
                        Sound = 1;
                        break;
                    case HitObjectSoundType.Finish:
                        Sound = 2;
                        break;
                    case HitObjectSoundType.Clap:
                        Sound = 3;
                        break;
                }
            }

            switch (SampleSet)
            {
                case SampleSet.Normal:
                default:
                    Sample = 0;
                    break;
                case SampleSet.None:
                case SampleSet.Soft:
                    Sample = 1;
                    break;
                case SampleSet.Drum:
                    Sample = 2;
                    break;
            }

            int custom_sampleset = (int)CustomSampleSet;
            if (custom_sampleset <= 2)
            {
                if (custom_sampleset < 0)
                    custom_sampleset = 0;

                switch (type)
                {
                    case SampleType.Normal:
                        return s_AllSampleSets[Sound, Sample, custom_sampleset];
                    case SampleType.Tick:
                        return s_AllTicks[Sound, Sample, custom_sampleset];
                    case SampleType.Slide:
                        return s_AllSlides[Sound, Sample, custom_sampleset];
                    default:
                        return -1;
                }
            }
            else
            {
                string key = string.Empty;
                string modePrefix = GameBase.Mode == OsuModes.Play && Player.Mode == PlayModes.Taiko ? @"taiko-" : string.Empty;
                switch (type)
                {
                    case SampleType.Normal:
                        key = string.Format(@"{0}{1}-hit{2}{3}", modePrefix, SampleSet.ToString().ToLower(), SoundType.ToString().ToLower(), custom_sampleset);
                        break;
                    case SampleType.Tick:
                        key = string.Format(@"{0}{1}-slidertick{2}", modePrefix, SampleSet.ToString().ToLower(), custom_sampleset);
                        break;
                    case SampleType.Slide:
                        key = string.Format(@"{0}{1}-slider{2}{3}", modePrefix, SampleSet.ToString().ToLower(), SoundType == HitObjectSoundType.Normal ? @"slide" : SoundType.ToString().ToLower(), custom_sampleset);
                        break;
                }
                int addr = -1;
                if (SampleCacheBeatmap.TryGetValue(key, out addr))
                {
                    if (addr != -1)
                        return addr;
                }
                if (Sound <= 3)
                {
                    switch (type)
                    {
                        case SampleType.Normal:
                            return s_AllSampleSets[Sound, Sample, 0];
                        case SampleType.Tick:
                            return s_AllTicks[Sound, Sample, 0];
                        case SampleType.Slide:
                            return s_AllSlides[Sound, Sample, 0];
                        default:
                            return -1;
                    }
                }
                else
                    return -1;
            }

        }

        #endregion

        internal static int CurrentPlaybackRate
        {
            get { return (int)AudioTrack.PlaybackRate; }

            set
            {
                lastAudioTimeAccurateSet = 0;

                AudioTrack.FrequencyLock = !Nightcore;
                AudioTrack.PlaybackRate = value;
            }
        }

        private static void checkCachedSeek()
        {
            if (!cachedSeekPending) return;

            AudioTrack.Seek(cachedSeek + (cachedSeek != 0 ? Offset : 0));
            cachedSeekPending = false;

            //todo: check this
            ExtendedTime = false;

            UpdateAudioTime();
            TimeLastFrame = Time;
        }

        internal static void SeekTo(int milliseconds, bool allowExceedingRange = false, bool force = false)
        {
            ResetControlPoints();

            if (TimeRaw == milliseconds && Time == milliseconds && !force)
                return;

            EventManager.AllowVideoSeek = true;

            if (milliseconds < 0 && !allowExceedingRange && !ExtendedTime)
                milliseconds = 0;
            else if (milliseconds > AudioLength && !allowExceedingRange)
                milliseconds = AudioLength;

            if (ExtendedTime)
            {
                if (milliseconds > audioTime)
                    VirtualTimeAccumulated += (milliseconds - (audioTime));

                audioTime = milliseconds;

                UpdateAudioTime();
            }


            if (!ExtendedTime || force)
            {
                if (AudioState != AudioStates.Stopped)
                {
                    if (Math.Abs(milliseconds - Time) > 200 || force)
                        AudioTrack.Seek(milliseconds + Offset);
                    AudioState = AudioStates.Seeking;

                    UpdateAudioTime();
                    TimeLastFrame = Time;
                }
                else
                {
                    cachedSeek = milliseconds;
                    cachedSeekPending = true;
                    UpdateAudioTime();
                }
            }

            UpdateActiveTimingPoint(false);
        }

        #region Song Track

        internal static bool IsReversed { get { return AudioTrack != null ? AudioTrack.IsReversed : false; } }
        internal static void SetDirection(bool reverse)
        {
            if (AudioTrack != null) AudioTrack.SetDirection(reverse);
        }

        static Thread effectProcessingThread;
        static int changeFrequencyTarget;
        internal static void ChangeFrequencyRate(float target = -1)
        {
            changeFrequencyTarget = (int)(InitialFrequency * target);
            if (effectProcessingThread != null) return;

            int initialFreq = (int)AudioFrequency;

            int currentFreq = initialFreq * (IsReversed ? -1 : 1);
            effectProcessingThread = GameBase.RunBackgroundThread(delegate
            {
                while (currentFreq != changeFrequencyTarget)
                {
                    if (currentFreq > changeFrequencyTarget)
                        currentFreq -= Math.Max(1, (currentFreq - changeFrequencyTarget) / 10);
                    else
                        currentFreq += Math.Max(1, (changeFrequencyTarget - currentFreq) / 10);

                    if (currentFreq == 0) continue;

                    if (currentFreq < 0 && !IsReversed)
                        GameBase.Scheduler.Add(delegate { SetDirection(!IsReversed); });
                    else if (currentFreq > 0 && IsReversed)
                        GameBase.Scheduler.Add(delegate { SetDirection(!IsReversed); });
                    else
                    {
                        AudioFrequency = Math.Abs(currentFreq);
                        GameBase.Scheduler.Add(delegate { Bass.BASS_ChannelSetAttribute(AudioTrack.audioStream, BASSAttribute.BASS_ATTRIB_FREQ, AudioFrequency); });
                    }
                    Thread.Sleep(20);
                }

                effectProcessingThread = null;
            });

        }

        internal static CustomSampleSet CustomSamples
        {
            get
            {
                List<ControlPoint> controlPoints = ControlPoints;

                if (controlPoints != null && controlPoints.Count > 0 && ActiveInheritedTimingPointIndex >= 0 &&
                    ActiveInheritedTimingPointIndex < controlPoints.Count && LoadedBeatmap.BeatmapVersion > 3)
                    return controlPoints[ActiveInheritedTimingPointIndex].customSamples;
                return localCustomSamples;
            }
            set { localCustomSamples = value; }
        }

        internal static int PreviewTime
        {
            get { return LoadedBeatmap == null ? 0 : LoadedBeatmap.PreviewTime; }
        }

        internal static int VolumeMusicAdjusted
        {
            get { return (int)(VolumeMusic.Value * VolumeMaster.Value / 100f); }
        }

        internal static float VolumeEffectAdjusted
        {
            get { return VolumeEffect.Value * VolumeMaster.Value / 100f; }
        }

        internal static bool Paused
        {
            get { return AudioState == AudioStates.Stopped; }
        }

        internal static int VolumeSampleAdjusted
        {
            get { return (int)(VolumeSample * VolumeEffectAdjusted / 100f); }
        }

        internal static int VolumeSample
        {
            get
            {
                if (LoadedBeatmap == null)
                    return 100;

                List<ControlPoint> controlPoints = ControlPoints;

                if (controlPoints != null && ActiveInheritedTimingPointIndex >= 0 && controlPoints.Count > ActiveInheritedTimingPointIndex)
                    return (int)Math.Max(8, controlPoints[ActiveInheritedTimingPointIndex].volume);

                return LoadedBeatmap.SampleVolume;
            }
        }

        internal static bool KiaiEnabled
        {
            get
            {
                ControlPoint cp = ActiveControlPoint;
                return cp == null ? false : cp.kiaiMode;
            }
        }

        internal static void ResetAudioTrack()
        {
            SetDirection(false);
            Nightcore = false;
            CurrentPlaybackRate = 100;
            AudioFrequency = InitialFrequency;
            ResetControlPoints();

            if (AudioTrack != null) AudioTrack.Reset();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="beatmap">Beatmap to load audio from.</param>
        /// <param name="requireId3">Should the ID3 headers be parsed.</param>
        /// <param name="preview">Load mp3 as fast as possible, resulting in incorrect positioning on VBR mp3 files.</param>
        /// <returns></returns>
        internal static bool LoadAudio(Beatmap beatmap, bool requireId3, bool preview, bool unloadPrevious = true, bool loop = false)
        {
            if (!beatmap.HeadersLoaded)
                beatmap.ProcessHeaders();

            if (Nightcore)
            {
                if (nightcoreBeat == null) nightcoreBeat = new NightcoreBeat();
                nightcoreBeat.Reset();
            }
            else
            {
                if (nightcoreBeat != null) nightcoreBeat.Dispose();
                nightcoreBeat = null;
            }

            //if audio is already fully loaded (full pass) then we don't need to reload it no matter what.
            if (AudioTrack != null && !AudioTrack.IsDisposed && (!AudioTrack.Preview || preview) && AudioTrack.Beatmap != null && beatmap.ContainingFolder == AudioTrack.Beatmap.ContainingFolder && beatmap.AudioFilename == AudioTrack.Beatmap.AudioFilename)
            {
                AudioTrack.Beatmap = beatmap;
                return false;
            }

            if (unloadPrevious)
                FreeMusic();

            AudioTrack = AudioTrack.CreateAudioTrack(beatmap, preview, loop);

            if (OnNewAudioTrack != null)
                OnNewAudioTrack();

            if (requireId3)
            {
                try
                {
                    TAG_INFO tagInfo = new TAG_INFO();
                    if (BassTags.BASS_TAG_GetFromFile(AudioTrack.audioStream, tagInfo))
                    {
                        AudioArtist = tagInfo.artist;
                        AudioTitle = tagInfo.title;
                    }
                    else
                    {
                        AudioArtist = LocalisationManager.GetString(OsuString.AudioEngine_Unknown);
                        AudioTitle = LocalisationManager.GetString(OsuString.AudioEngine_Unknown);
                    }
                }
                catch
                {
                    AudioArtist = LocalisationManager.GetString(OsuString.AudioEngine_Unknown);
                    AudioTitle = LocalisationManager.GetString(OsuString.AudioEngine_Unknown);
                }
            }

            Bass.BASS_ChannelGetAttribute(AudioTrack.audioStream, BASSAttribute.BASS_ATTRIB_FREQ, ref AudioFrequency);
            if (AudioFrequency <= 0) AudioFrequency = 44100;
            InitialFrequency = AudioFrequency;

            ResetAudioTrack();
            ChatEngine.UpdateNowPlaying(false, false);

            return true;
        }

        internal static TAG_INFO GetAudioTagInfo()
        {
            if (AudioTrack.audioStream != 0)
            {
                try
                {
                    TAG_INFO tagInfo = new TAG_INFO();
                    if (BassTags.BASS_TAG_GetFromFile(AudioTrack.audioStream, tagInfo))
                        return tagInfo;
                }
                catch
                {
                    return null;
                }
            }
            return null;
        }

        /// <summary>
        /// Loads the audio for preview.
        /// </summary>
        /// <param name="beatmap">The beatmap.</param>
        /// <param name="continuePlayback">Returns false on failure.</param>
        /// <param name="quickLoad">Load mp3 as fast as possible, resulting in incorrect positioning on VBR mp3 files.</param>
        /// <returns></returns>
        internal static bool LoadAudioForPreview(Beatmap beatmap, bool continuePlayback, bool previewPoint, bool quickLoad = true)
        {
            ExtendedTime = false;

            ResetControlPoints();

            // the nightcore beat sounds like hell with a quickload so require full loads for the day
            bool same;
            try
            {
                same = !LoadAudio(beatmap, false, quickLoad);
            }
            catch (UnauthorizedAccessException)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.AudioEngine_NoPermissions));
                return false;
            }

            if (same && continuePlayback && audioTime / AudioLength < 0.98F && CurrentPlaybackRate == 100)
                SetVolumeMusicFade(volumeMusicFade / 3);
            else
            {
                if (previewPoint)
                    AudioTrack.Seek(PreviewTime == -1 ? (int)(AudioLength * 0.4) : PreviewTime);
                SetVolumeMusicFade(0, true);
            }

            AudioTrack.Play(false);
            AudioState = AudioStates.Playing;

            return same;
        }

        internal static void Play()
        {
            checkCachedSeek();
            AudioTrack.Play();
            AudioState = AudioStates.Seeking;
        }

        /// <summary>
        /// Play if paused, pause if playing.
        /// </summary>
        /// <returns>True if paused.</returns>
        internal static bool TogglePause()
        {
            if (ExtendedTime)
            {
                if (AudioState != AudioStates.Playing)
                {
                    AudioState = AudioStates.Playing;
                    UnpauseAllSamples();
                    return false;
                }
                else
                {
                    AudioState = AudioStates.Stopped;
                    PauseAllSamples();
                    return true;
                }
            }

            if (AudioTrack.IsPlaying)
            {
                AudioState = AudioStates.Stopped;
                PauseAllSamples();
                AudioTrack.Pause();
                return true;
            }
            else
            {
                checkCachedSeek();
                UnpauseAllSamples();
                AudioState = AudioStates.Seeking;
                AudioTrack.Play(false);
                return false;
            }
        }

        private static float volumeMusicFadeCalculated;
        internal static bool cachedSeekPending;
        internal static double ElapsedMilliseconds;

        internal static void SetVolumeMusicFade(int volume, bool overrideMinimum = false)
        {
            volumeMusicFade = Math.Max(Math.Min(100, volume), overrideMinimum ? 0 : 10);

            float neVol = (float)VolumeMusicAdjusted / 100 * volumeMusicFade / 100;

            volumeMusicFadeCalculated = neVol;
            if (AudioTrack != null) AudioTrack.Volume = neVol;
        }

        #endregion

        internal static long FindSilence(int sample)
        {
            if (sample == -1 || sample == 0)
                return 0;

            long count;

            if (SilenceCache.TryGetValue(sample, out count))
                return count;

            int threshold = 500;

            BASS_SAMPLE bs = Bass.BASS_SampleGetInfo(sample);

            if (bs == null)
                return 0;

            short[] buffer = new short[bs.length];
            Bass.BASS_SampleGetData(sample, buffer);

            int b = buffer.Length;
            int a;
            b = b / 2; // bytes -> samples (16-bit!)

            for (a = 0; a < b; a++)
            {
                // count silent samples
                if (Math.Abs(buffer[a]) >= threshold)
                    break;
            }
            // now 'a' points to the sample position
            count += a * 2; // since 'a' was the 16-bit index, this is now the BYTE offset!

            if (a < b)
            {
                // sound has begun!
                // move back to a quieter sample (to avoid "click")
                while (a > 0 && Math.Abs(buffer[a]) >= threshold / 4)
                {
                    a--;
                    count -= 2;
                }
            }

            SilenceCache[sample] = count;

            return count;
        }

        /// <summary>
        /// Determines if the input mp3 stream uses a vbr. Assumes input is mp3.
        /// </summary>
        /// <param name="s">An open stream to an mp3 file. Assume seeked to position 0.</param>
        internal static bool IsVbr(Stream s)
        {
            int offsetToFirstFrame = 0;
            byte[] magicBytes = new byte[4];
            while (true)
            {
                // check what we are currently reading
                s.Read(magicBytes, 0, 4);
                if (magicBytes[0] == 'I' && magicBytes[1] == 'D' && magicBytes[2] == '3')
                {
                    // id3 headers - skip these
                    s.Seek(2, SeekOrigin.Current);
                    int id3Length = ((s.ReadByte() & 0x7f) << 21) + ((s.ReadByte() & 0x7f) << 14) + ((s.ReadByte() & 0x7f) << 7) + (s.ReadByte() & 0x7f);
                    s.Seek(id3Length, SeekOrigin.Current);
                    offsetToFirstFrame += id3Length + 10;
                }
                else if (magicBytes[0] == 0xff && (magicBytes[1] & 0xe0) == 0xe0)
                {
                    // start of first frame - 2nd and 4th bytes have information on xing header location
                    bool hasCrc = (magicBytes[1] & 0x1) == 0; // 0 = has CRC, 1 = no CRC
                    bool mpegVersion1 = ((magicBytes[1] >> 3) & 0x3) == 0x3; // 11 = MPEG version 1, everything else = newer version
                    bool singleChannel = (magicBytes[3] >> 6) == 0x3; // 11 = single channel, everything else = two channels

                    // calculate xing header location
                    int bytes = 0;

                    if (hasCrc)
                        bytes += 2; // CRC is 16 bits long

                    if (mpegVersion1 && !singleChannel)
                        bytes += 32;
                    else if (!mpegVersion1 && singleChannel)
                        bytes += 9;
                    else
                        bytes += 17;

                    // try to find xing header
                    s.Seek(bytes, SeekOrigin.Current);
                    byte[] data = new byte[4];
                    s.Read(data, 0, 4);
                    string xingHeader = ASCIIEncoding.ASCII.GetString(data);
                    if (xingHeader == @"Info") // @"Info" is used for cbr files
                        return false;
                    if (xingHeader == @"Xing")
                        return true;

                    // if we can't find xing header, look for vbri header
                    s.Seek(offsetToFirstFrame + 4 + 32, SeekOrigin.Begin); // offset + frame header + vbri offset within frame
                    s.Read(data, 0, 4);
                    string vbriHeader = ASCIIEncoding.ASCII.GetString(data);
                    if (vbriHeader == @"VBRI")
                        return true;

                    // we didn't find either header
                    return false;
                }
                else
                {
                    // unknown header - throw exception?
                    return true; // fail-safe
                }
            }
        }

        internal static void Stop()
        {
            Time = 0;
            TimeUnedited = 0;
            cachedSeek = 0;
            cachedSeekPending = true;
            if (AudioTrack != null) AudioTrack.Stop();
            AudioState = AudioStates.Stopped;
        }

        internal static void FreeMusic()
        {
            if (AudioTrack != null)
            {
                AudioTrack.Dispose();
                AudioTrack = null;
            }

            cachedSeekPending = false;
        }

        internal static void ClearSampleEvents()
        {
            StopAllSampleEvents();

            foreach (SampleCacheItem s in SampleEvents)
                if (s.FreeSample) Bass.BASS_SampleFree(s.Sample);
            SampleEvents.Clear();
        }

        internal static void Continue()
        {
            if (ExtendedTime)
            {
                if (AudioState != AudioStates.Playing)
                {
                    AudioState = AudioStates.Playing;
                    UnpauseAllSamples();
                }

                return;
            }

            checkCachedSeek();
            AudioTrack.Play(false);
            AudioState = AudioStates.Seeking;
        }

        internal static void StopSlideSamples()
        {
            Bass.BASS_ChannelPause(AudioEngine.ch_sliderSlide);
            Bass.BASS_ChannelPause(AudioEngine.ch_sliderWhistle);
        }

        internal static void StopSpinSample()
        {
            Bass.BASS_ChannelPause(AudioEngine.ch_spinnerSpin);
        }

        internal static void StopAllSampleEvents()
        {
            foreach (SampleCacheItem e in SampleEvents)
                if (e.AudioSample != null)
                    e.AudioSample.Stop();
        }

        internal static void PauseAllSamples()
        {
            foreach (SampleCacheItem e in SampleEvents)
                if (e.AudioSample != null && e.AudioSample.Playing)
                {
                    e.AudioSample.Pause();
                    e.Paused = true;
                }
        }

        internal static void UnpauseAllSamples()
        {
            foreach (SampleCacheItem e in SampleEvents)
                if (e.Paused)
                {
                    e.AudioSample.Play(false);
                    e.Paused = false;
                }
        }

        internal static void StartVirtualTime()
        {
            StartVirtualTime(TimeRaw);
        }

        internal static void StartVirtualTime(int time, bool prepareOnly = false)
        {
            ExtendedTime = true;
            Time = time;
            TimeRaw = time;
            audioTime = time;
            cachedSeekPending = false;
            VirtualTimeAccumulated = Offset;
            if (!prepareOnly)
                AudioState = AudioStates.Playing;
        }

        internal static void EndVirtualTime()
        {
            ExtendedTime = false;
        }

        internal static bool CheckAudioDevice()
        {
            if (CurrentAudioDevice != null)
                return true;
            else
            {
                NotificationManager.ShowMessage("No compatible audio device detected. You must plug in a valid audio device in order to play osu!", Color.Red, 4000);
                return false;
            }
        }

        internal static event VoidDelegate AvailableDevicesChanged;
        internal static List<BASS_DEVICEINFO> AudioDevices = new List<BASS_DEVICEINFO>();

        internal static string CurrentAudioDevice = null;
        internal static bool SetAudioDevice(string selectedDevice = null)
        {
            AudioDevices = new List<BASS_DEVICEINFO>(Bass.BASS_GetDeviceInfos());
            if (AvailableDevicesChanged != null)
                AvailableDevicesChanged();

            string oldDevice = CurrentAudioDevice;
            string newDevice = selectedDevice;

            if (string.IsNullOrEmpty(selectedDevice))
            {
                BASS_DEVICEINFO info = AudioDevices.Find(df => df.IsDefault);
                if (info != null)
                    newDevice = info.name;
            }

            int oldDeviceIndex = Bass.BASS_GetDevice();
            BASS_DEVICEINFO oldDeviceInfo = Bass.BASS_GetDeviceInfo(oldDeviceIndex);
            bool oldDeviceValid = oldDeviceInfo != null && oldDeviceInfo.IsEnabled && oldDeviceInfo.IsInitialized;

            if (newDevice == oldDevice)
            {
                //check the old device is still valid
                if (oldDeviceValid)
                    return true;
            }

            if (newDevice == null)
                return false;

            int newDeviceIndex = AudioDevices.FindIndex(df => df.name == newDevice);

            BASS_DEVICEINFO newDeviceInfo = Bass.BASS_GetDeviceInfo(newDeviceIndex);
            //we may have previously initialised this device.

            if (oldDeviceValid && (newDeviceInfo == null || !newDeviceInfo.IsEnabled))
            {
                //handles the case we are trying to load a user setting which is currently unavailable,
                //and we have already fallen back to a sane default.
                return true;
            }

            AudioTrackBass activeTrack = AudioTrack as AudioTrackBass;
            bool activeTrackWasPlaying = activeTrack != null && activeTrack.State != AudioStates.Stopped;

            if (newDevice != null && oldDevice != null)
            {
                //we are preparing to load a new device, so let's clean up any existing device.
                if (activeTrack != null)
                    activeTrack.FreeBass();

                ClearAllCaches();
                Bass.BASS_Free();
            }

            if (newDeviceInfo == null || !Bass.BASS_Init(newDeviceIndex, 44100, 0, GameBase.Instance.Window.Handle, null))
            {
                if (selectedDevice == null)
                {
                    CurrentAudioDevice = null;
                    return false; //we're fucked. the default device won't initialise.
                }

                return SetAudioDevice(); //let's try again using the default device.
            }

            //we have successfully initialised a new device.
            CurrentAudioDevice = newDevice;

            Bass.BASS_SetConfig(BASSConfig.BASS_CONFIG_BUFFER, 100);
            Bass.BASS_SetConfig(BASSConfig.BASS_CONFIG_NET_BUFFER, 500);
            Bass.BASS_SetConfig(BASSConfig.BASS_CONFIG_UPDATEPERIOD, 10);

            if (registered)
            {
                Initialize();
                LoadAllSamples();

                if (activeTrack != null && activeTrack.Beatmap != null)
                {
                    AudioTrack = AudioTrack.CreateAudioTrack(activeTrack.Beatmap, activeTrack.Preview, activeTrack.Looping);
                    AudioTrack.Volume = activeTrack.Volume;
                    AudioTrack.PlaybackRate = activeTrack.PlaybackRate;
                    AudioTrack.Seek(TimeRaw);
                    if (activeTrackWasPlaying) AudioTrack.Play(false);
                }
            }

            return true;
        }

        static int lastDeviceCount;

        private static void checkAudioDeviceChanged()
        {
            int availableDevices = 0;

            string deviceInfo = string.Empty;
            foreach (BASS_DEVICEINFO info in Bass.BASS_GetDeviceInfos())
            {
                if (info.driver == null) continue;

                bool isCurrentDevice = info.name == CurrentAudioDevice;

                if (info.IsEnabled)
                {
                    if (isCurrentDevice && !info.IsDefault && string.IsNullOrEmpty(ConfigManager.sAudioDevice.Value))
                        SetAudioDevice();
                    availableDevices++;
                }
                else if (isCurrentDevice)
                    SetAudioDevice(ConfigManager.sAudioDevice.Value); //the active device has been disabled.
            }

            if (lastDeviceCount != availableDevices)
            {
                if (lastDeviceCount > 0)
                {
                    SetAudioDevice(ConfigManager.sAudioDevice.Value); //just update the available devices.
                    if (availableDevices > lastDeviceCount)
                        NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.AudioEngine_NewDeviceDetected), Color.YellowGreen, 5000);
                }

                lastDeviceCount = availableDevices;
            }
        }
    }

    internal enum AudioStates
    {
        Stopped,
        Playing,
        Seeking
    }

    internal enum SampleSet
    {
        All = -1,
        None = 0,
        Normal = 1,
        Soft = 2,
        Drum = 3
    }

    internal enum SampleType
    {
        Normal,
        Tick,
        Slide
    }

    internal class AudioNotLoadedException : Exception { }
}