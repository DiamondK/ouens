namespace osu.Audio
{
    internal class SampleCacheItem
    {
        internal bool Played;
        internal bool Paused;
        internal int Sample;
        internal int Time;

        public AudioSample AudioSample;

        internal int Volume;
        public bool FreeSample = true;

        internal SampleCacheItem(int time, int sample, int volume = 100, bool freeSample = true)
        {
            Time = time;
            Sample = sample;
            Volume = volume;
            FreeSample = freeSample;
        }
    }
}