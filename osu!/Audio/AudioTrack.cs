﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using osu.GameplayElements.Beatmaps;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Fx;

namespace osu.Audio
{
    internal abstract class AudioTrack : IDisposable
    {
        internal Beatmap Beatmap;
        internal bool IsDisposed;

        internal static AudioTrack CreateAudioTrack(Beatmap beatmap, bool quick, bool loop = false)
        {
            AudioTrack track = null;

            Stream stream = beatmap.GetAudioStream();
            if (stream == null)
                track = new AudioTrackVirtual() { Length = beatmap.TotalLength < 0 ? 600 * 1000 : beatmap.TotalLength + 10000 };
            else
                track = new AudioTrackBass(stream, quick, loop);

            if (track != null) track.Beatmap = beatmap;

            return track;
        }

        ~AudioTrack()
        {
            if (!IsDisposed) Dispose();
        }

        public virtual void Dispose()
        {
            if (IsDisposed) return;
            IsDisposed = true;
        }

        /// <summary>
        /// Adjust track volume. A value of 1 is full volume.
        /// </summary>
        internal abstract float Volume { get; set; }

        /// <summary>
        /// Should this track only be used for preview purposes? This suggests it has not yet been fully loaded.
        /// </summary>
        internal virtual bool Preview { get; set; }

        /// <summary>
        /// Is this track capable of producing audio?
        /// </summary>
        internal virtual bool IsDummyDevice { get { return true; } }

        /// <summary>
        /// Reset this track to a logical default state.
        /// </summary>
        internal virtual void Reset() { }

        internal bool IsReversed;
        internal virtual void SetDirection(bool reverse)
        {
            IsReversed = reverse;
        }

        /// <summary>
        /// Current position in milliseconds.
        /// </summary>
        internal abstract double Position { get; }

        /// <summary>
        /// Lenth of the track in milliseconds.
        /// </summary>
        internal double Length;

        /// <summary>
        /// The handle for this track, if there is one.
        /// </summary>
        internal int audioStream { get; set; }

        internal abstract void Seek(double seek);

        internal abstract void Play(bool restart = true);

        internal abstract void Pause();

        public abstract bool IsPlaying { get; }

        public abstract void Update();

        internal void Stop()
        {
            Pause();
            Seek(0);
        }

        public abstract double PlaybackRate { get; set; }

        /// <summary>
        /// Decides whether the playback rate adjusts the frequency or not.
        /// </summary>
        public virtual bool FrequencyLock { get; set; }

        internal virtual bool SamplesMatchPlaybackRate { get { return Beatmap != null ? Beatmap.SamplesMatchPlaybackRate : false; } }
    }

}
