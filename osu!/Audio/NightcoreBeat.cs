﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.GameplayElements.Beatmaps;
using Un4seen.Bass;
using osu.Graphics.Skinning;

namespace osu.Audio
{
    class NightcoreBeat : GameComponent
    {
        private int lastBeat = -1;
        private bool m_hats = true;

        AudioSample nightcoreKick = new AudioSample(AudioEngine.LoadSample(@"nightcore-kick", false, true, SkinSource.ExceptBeatmap));
        AudioSample nightcoreFinish = new AudioSample(AudioEngine.LoadSample(@"nightcore-finish", false, true, SkinSource.ExceptBeatmap));
        AudioSample nightcoreClap = new AudioSample(AudioEngine.LoadSample(@"nightcore-clap", false, true, SkinSource.ExceptBeatmap));
        AudioSample nightcoreHat = new AudioSample(AudioEngine.LoadSample(@"nightcore-hat", false, true, SkinSource.ExceptBeatmap));

        public NightcoreBeat()
            : base(GameBase.Instance)
        {
        }

        public void Reset()
        {
            lastBeat = -1;
            // (in a perfect world) tick rates other than 2 imply there isn't a regular offbeat, so offbeat hats would stand out.
            // only enable them if tick rate is a multiple of 2.
            m_hats = BeatmapManager.Current.DifficultySliderTickRate % 2 == 0;
        }

        public override void Update()
        {
            if (!AudioEngine.BeatSyncing || !AudioEngine.Nightcore || AudioEngine.Time < AudioEngine.CurrentOffset) return;

            int beat = (int)((AudioEngine.Time - AudioEngine.CurrentOffset) * 2 / AudioEngine.beatLength);
            
            if (beat == lastBeat) return;

            lastBeat = beat;

            int beatInBar = beat % (int)AudioEngine.timeSignature;

            if (beat % (8 * (int)AudioEngine.timeSignature) == 0) // cymbal every 4 bars
            {
                nightcoreKick.Play(true, true);
                bool finish = false;
                try
                {
                    finish = beat > 0 || (AudioEngine.ControlPoints[AudioEngine.ActiveTimingPointIndex].effectFlags & EffectFlags.OmitFirstBarLine) == 0;
                }
                catch { }
                if (finish) nightcoreFinish.Play(true, true);

                return;
            }

            if (beatInBar % 4 == 0) // kick every 2 beats
            {
                nightcoreKick.Play(true, true);
                return;
            }

            if (beatInBar % 4 == 2) // clap every odd second beat
            {
                nightcoreClap.Play(true, true);
                return;
            }

            if (m_hats) // hats every odd quaver
            {
                nightcoreHat.Play(true, true);
                return;
            }
        }
    }
}
