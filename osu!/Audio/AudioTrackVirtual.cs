using System;

namespace osu.Audio
{
    class AudioTrackVirtual : AudioTrack
    {
        bool isPlaying;
        double position;
        bool reversed;

        internal override void SetDirection(bool reverse)
        {
            reversed = reverse;
        }

        internal override void Seek(double seek)
        {
            position = seek;
        }

        internal override void Play(bool restart = true)
        {
            if (restart) position = 0;
            isPlaying = true;
        }

        internal override void Pause()
        {
            isPlaying = false;
        }

        public override bool IsPlaying
        {
            get { return isPlaying; }
        }

        internal override double Position
        {
            get { return position; }
        }

        internal override float Volume
        {
            get
            {
                return 0;
            }
            set
            {

            }
        }

        private long lastUpdateFrame;
        public override void Update()
        {
            if (GameBase.TotalFramesRendered == lastUpdateFrame)
                return;

            if (isPlaying)
            {
                position += (reversed ? -1 : 1) * GameBase.ElapsedMilliseconds * playbackRate / 100;
                lastUpdateFrame = GameBase.TotalFramesRendered;
            }
        }

        private double playbackRate = 100;
        public override double PlaybackRate
        {
            get
            {
                return playbackRate;
            }
            set
            {
                if (value == playbackRate) return;

                playbackRate = value;
            }
        }

        internal override bool SamplesMatchPlaybackRate { get { return true; } }
    }
}
