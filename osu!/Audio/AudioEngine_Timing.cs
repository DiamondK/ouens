﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.GameModes.Menus;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Skinning;
using osu.Helpers;
using osu.Input;
using osu_common;
using osu_common.Helpers;
using osudata;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Fx;
using osu.GameplayElements.Events;
using osu.GameModes.Play.Rulesets;
using Un4seen.Bass.AddOn.Tags;
using osu.GameplayElements.HitObjects;
using System.Text;
using System.Threading;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using System.Text.RegularExpressions;
using osu.Online;
using osu.Online.Social;

namespace osu.Audio
{
    internal static partial class AudioEngine
    {
        internal static int ActiveTimingPointIndex = -1;

        internal static void UpdateTiming()
        {
            UpdateActiveTimingPoint(false);

            List<ControlPoint> controlPoints = ControlPoints;

            if (BeatmapManager.Current != null && controlPoints != null && controlPoints.Count > ActiveInheritedTimingPointIndex && ActiveInheritedTimingPointIndex >= 0
                && BeatmapManager.Current.BeatmapVersion > 3)
                LoadSampleSet(controlPoints[ActiveInheritedTimingPointIndex].sampleSet, false);
        }

        internal static event VoidDelegate OnNewSyncBeat;


        private static void UpdateSyncCues()
        {
            double bl = beatLength;
            double syncNew = (Time - CurrentOffset) % bl;


            SyncBeatProgress = syncNew / bl;

            int syncNewInt = (int)((Time - CurrentOffset) / bl) + (Time < CurrentOffset ? -1 : 0);
            if (syncNewInt != SyncBeat)
            {
                SyncNewBeat = true;
                if (OnNewSyncBeat != null)
                    OnNewSyncBeat();
                SyncBeat = syncNewInt;
            }
            else
            {
                SyncNewBeat = false;
            }
        }

        internal static void ResetControlPoints()
        {
            ActiveTimingPointIndex = -1;
            ActiveInheritedTimingPointIndex = -1;
        }

        internal static void UpdateActiveTimingPoint(bool runSort)
        {
            try
            {
                //Assign local var to avoid returning the same field many times.
                List<ControlPoint> controlPoints = ControlPoints;

                //50ms allows the timing point to change before an early hit is hit.
                int time = Time + 5;

                if (controlPoints != null && controlPoints.Count > 0)
                {
                    if (runSort)
                        controlPoints.Sort();

                    if (runSort || ActiveTimingPointIndex < 0 || AudioState != AudioStates.Playing)
                    {
                        int oldTiming = ActiveTimingPointIndex;
                        int oldInherit = ActiveInheritedTimingPointIndex;

                        //in the case we're not actively playing audio we should just find the last index.
                        ActiveTimingPointIndex = controlPoints.FindLastIndex(cp => cp.timingChange && cp.offset < time);
                        if (ActiveTimingPointIndex < 0) ActiveTimingPointIndex = 0;
                        ActiveInheritedTimingPointIndex = controlPoints.FindLastIndex(cp => cp.offset < time);
                        if (ActiveInheritedTimingPointIndex < 0) ActiveInheritedTimingPointIndex = 0;

                        if (oldTiming != ActiveTimingPointIndex)
                            InvokeActiveTimingPointChanged(controlPoints[ActiveTimingPointIndex]);
                        if (oldInherit != ActiveInheritedTimingPointIndex)
                        {
                            InvokeActiveInheritedTimingPointChanged(controlPoints[ActiveInheritedTimingPointIndex]);
                            SyncBeat = -999;
                        }
                    }
                    else
                    {
                        int count = controlPoints.Count;
                        for (int i = ActiveInheritedTimingPointIndex + 1; i < count; i++)
                        {
                            ControlPoint t = controlPoints[i];
                            if (t.offset > time) break;

                            ActiveInheritedTimingPointIndex = i;
                            InvokeActiveInheritedTimingPointChanged(t);

                            if (t.timingChange)
                            {
                                ActiveTimingPointIndex = i;
                                InvokeActiveTimingPointChanged(t);
                            }

                        }
                    }
                }
                else
                {
                    ActiveTimingPointIndex = -1;
                    ActiveInheritedTimingPointIndex = -1;
                    SyncBeat = -999;
                }
            }
            catch
            {
                ActiveTimingPointIndex = -1;
                ActiveInheritedTimingPointIndex = -1;
                SyncBeat = -999;
            }
        }

        internal static int ActiveInheritedTimingPointIndex = -1;

        internal static ControlPoint ActiveControlPoint
        {
            get
            {
                List<ControlPoint> controlPoints = ControlPoints;

                if (controlPoints == null || controlPoints.Count == 0) return null;

                return ActiveInheritedTimingPointIndex >= 0 && ActiveInheritedTimingPointIndex < controlPoints.Count ? controlPoints[ActiveInheritedTimingPointIndex] : controlPoints[0];
            }
        }

        internal static double beatLength
        {
            get
            {
                List<ControlPoint> controlPoints = ControlPoints;

                if (controlPoints == null || controlPoints.Count == 0)
                    return 0;

                int count = controlPoints.Count;

                bool hasActiveTimingPoint = count > 0 && ActiveTimingPointIndex >= 0 && ActiveTimingPointIndex < count;
                bool isInheriting = ActiveInheritedTimingPointIndex > ActiveTimingPointIndex && ActiveInheritedTimingPointIndex < count;

                if (hasActiveTimingPoint)
                {
                    if (isInheriting)
                    {
                        double ibl = controlPoints[ActiveInheritedTimingPointIndex].beatLength;
                        if (ibl < 0)// && (ibl == 50 || ibl == 100 || ibl == 200))
                            return controlPoints[ActiveTimingPointIndex].beatLength;
                    }

                    return controlPoints[ActiveTimingPointIndex].beatLength;
                }

                return 0;

            }

            set
            {
                List<ControlPoint> controlPoints = ControlPoints;

                if (controlPoints == null) return;

                if (controlPoints.Count > 0)
                {
                    if (ActiveTimingPointIndex >= 0)
                        controlPoints[ActiveTimingPointIndex].beatLength = value;
                    else
                        controlPoints[0].beatLength = value;
                }
                else
                    controlPoints.Add(new ControlPoint(0, value));
            }
        }

        internal static double BeatLengthNotInheritted
        {
            get
            {
                List<ControlPoint> controlPoints = ControlPoints;

                if (controlPoints == null || controlPoints.Count == 0)
                    return 0;

                int count = controlPoints.Count;
                if (count > 0 && ActiveTimingPointIndex >= 0 && ActiveInheritedTimingPointIndex >= 0
                    && ActiveInheritedTimingPointIndex < count && ActiveTimingPointIndex < count)
                    return controlPoints[ActiveTimingPointIndex].beatLength;

                return 0;
            }
        }

        internal static double CurrentBPM
        {
            get
            {
                if (beatLength > 0)
                    return 1000 / beatLength * 60;
                return 0;
            }

            set { beatLength = 60 / value * 1000; }
        }

        internal static double CurrentOffset
        {
            get
            {
                List<ControlPoint> controlPoints = ControlPoints;

                if (controlPoints == null || controlPoints.Count == 0)
                    return 0;

                int count = controlPoints.Count;

                if (count == 0 || ActiveTimingPointIndex < 0 || ActiveTimingPointIndex >= count)
                    return 0;

                return controlPoints[ActiveTimingPointIndex].offset;
            }

            set
            {
                List<ControlPoint> controlPoints = ControlPoints;

                if (controlPoints == null) return;

                if (controlPoints.Count > 0)
                {
                    if (ActiveTimingPointIndex >= 0)
                        controlPoints[ActiveTimingPointIndex].offset = value;
                    else
                        controlPoints[0].offset = value;
                }
                else
                    controlPoints.Add(new ControlPoint(value, 0));
                controlPoints.Sort();
            }
        }

        internal static List<ControlPoint> ControlPoints
        {
            get { return (LoadedBeatmap != null ? LoadedBeatmap.ControlPoints : null); }
            set
            {
                LoadedBeatmap.ControlPoints = value;

                ActiveTimingPointIndex = 0;
                ActiveInheritedTimingPointIndex = 0;
            }
        }

        internal static List<ControlPoint> TimingPoints
        {
            get
            {
                return (LoadedBeatmap != null ? LoadedBeatmap.TimingPoints : null);
            }
        }

        internal static bool BeatSyncing
        {
            get
            {
                List<ControlPoint> controlPoints = ControlPoints;

                if (controlPoints == null) return false;

                return ActiveTimingPointIndex < controlPoints.Count && ActiveTimingPointIndex >= 0 && beatLength > 0;
            }
        }

        internal static TimeSignatures timeSignature
        {
            get
            {
                List<ControlPoint> controlPoints = ControlPoints;

                if (controlPoints != null && controlPoints.Count > 0 && ActiveTimingPointIndex >= 0)
                    return controlPoints[ActiveTimingPointIndex].timeSignature;

                return TimeSignatures.SimpleQuadruple;
            }

            set
            {
                List<ControlPoint> controlPoints = ControlPoints;

                if (controlPoints == null || controlPoints.Count <= 0) return;

                if (ActiveTimingPointIndex >= 0)
                    controlPoints[ActiveTimingPointIndex].timeSignature = value;
            }
        }


        /// <summary>
        /// Beats the offset at.
        /// </summary>
        /// <param name="time">The time.</param>
        /// <returns></returns>
        internal static double beatOffsetCloseToZeroAt(double time)
        {
            Beatmap beatmap = LoadedBeatmap;
            return beatmap == null ? 0.0 : beatmap.beatOffsetCloseToZeroAt(time);
        }

        internal static double beatOffsetAt(double time)
        {
            Beatmap beatmap = LoadedBeatmap;
            return beatmap == null ? 0.0 : beatmap.beatOffsetAt(time);
        }

        internal static double beatLengthAt(double time, bool allowMultiplier = true)
        {
            Beatmap beatmap = LoadedBeatmap;
            return beatmap == null ? 0.0 : beatmap.beatLengthAt(time, allowMultiplier);
        }

        internal static float bpmMultiplierAt(double time)
        {
            Beatmap beatmap = LoadedBeatmap;
            return beatmap == null ? 1.0f : beatmap.bpmMultiplierAt(time);
        }

        // todo: refactor the above searches to use this
        internal static int controlPointIndexAt(double time, bool allowInherit)
        {
            Beatmap beatmap = LoadedBeatmap;
            return beatmap == null ? -1 : beatmap.controlPointIndexAt(time, allowInherit);
        }

        /// <summary>
        /// Best-effort finding algorithm. Only returns null if there are no controlPoints available.
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        internal static ControlPoint controlPointAt(double time)
        {
            Beatmap beatmap = LoadedBeatmap;
            return beatmap == null ? null : beatmap.controlPointAt(time);
        }

        internal static ControlPoint controlPointAtBin(double time)
        {
            Beatmap beatmap = LoadedBeatmap;
            return customPointAtBin(time, beatmap == null ? null : beatmap.ControlPoints);
        }

        internal static ControlPoint customPointAtBin(double time, List<ControlPoint> list)
        {
            return Beatmap.customPointAtBin(time, list);
        }

        internal static event ControlPointChangeDelegate ActiveTimingPointChanged;
        private static void InvokeActiveTimingPointChanged(ControlPoint tp)
        {
            ControlPointChangeDelegate changed = ActiveTimingPointChanged;
            if (changed != null) changed(tp);
        }

        internal static event ControlPointChangeDelegate ActiveInheritedTimingPointChanged;
        private static void InvokeActiveInheritedTimingPointChanged(ControlPoint tp)
        {
            ControlPointChangeDelegate changed = ActiveInheritedTimingPointChanged;
            if (changed != null) changed(tp);
        }

        internal delegate void ControlPointChangeDelegate(ControlPoint tp);
    }

    internal enum TimeSignatures
    {
        SimpleQuadruple = 4,
        SimpleTriple = 3
    }
}
