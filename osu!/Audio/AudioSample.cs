using System;
using osu.Helpers;
using Un4seen.Bass;
namespace osu.Audio
{
    internal class AudioSample : IDisposable
    {
        #region Attribute Handling

        //private bool looping;
        //public bool Looping
        //{
        //    get { return looping; }
        //    set
        //    {
        //        if (value == looping) return;

        //        looping = value;
        //        updateAttributes();
        //    }
        //}

        private float volume = 1;

        /// <summary>
        /// Adjust sample volume. A value of 1 is full volume.
        /// </summary>
        public float Volume
        {
            get { return volume; }
            set
            {
                if (value == volume) return;

                volume = value;
                Update();
            }
        }

        private float balance;
        public float Balance
        {
            get { return balance; }
            set
            {
                if (value == balance) return;

                balance = OsuMathHelper.Clamp(value, -1, 1);
                Update();
            }
        }

        private float inputBalanceWeight;

        public float InputBalanceWeight
        {
            get { return inputBalanceWeight; }
            set
            {
                if (value == inputBalanceWeight) return;

                inputBalanceWeight = value;
                Update();
            }
        }


        private float playbackRate = 1;
        public float PlaybackRate
        {
            get { return playbackRate; }
            set
            {
                if (value == playbackRate) return;

                playbackRate = value;
                Update();
            }
        }

        internal void Update()
        {
            if (!hasChannel) return;

            if (inputBalanceWeight > 0)
            {
                if (osu.GameModes.Play.Rulesets.Ruleset.Instance != null)
                    //Use the ruleset position, as this accounts for taiko and CtB specialties.
                    balance = osu.GameModes.Play.Rulesets.Ruleset.Instance.MousePosition.X / 512f - 0.5f;
                else
                    balance = osu.Input.InputManager.CursorPosition.X / GameBase.WindowRatio / GameBase.WindowWidthScaled - 0.5f;

                balance *= inputBalanceWeight;
            }

            Bass.BASS_ChannelSetAttribute(channel, BASSAttribute.BASS_ATTRIB_VOL, volume * AudioEngine.VolumeEffectAdjusted / 100f);
            Bass.BASS_ChannelSetAttribute(channel, BASSAttribute.BASS_ATTRIB_PAN, balance);
            Bass.BASS_ChannelSetAttribute(channel, BASSAttribute.BASS_ATTRIB_FREQ, initialFrequency * playbackRate);
        }

        #endregion

        bool hasChannel { get { return channel != 0; } }

        private int channel;

        /// <summary>
        /// The Bass channel ID for the last playback of this sample.
        /// </summary>
        public int Channel
        {
            get
            {
                if (!hasChannel)
                {
                    channel = Bass.BASS_SampleGetChannel(sample, false);
                    Bass.BASS_ChannelGetAttribute(channel, BASSAttribute.BASS_ATTRIB_FREQ, ref initialFrequency);
                    Update();
                }

                return channel;
            }
        }

        bool hasSample { get { return sample != 0; } }

        private int sample;

        /// <summary>
        /// The Bass sample ID for this sample.
        /// </summary>
        public int Sample
        {
            get { return sample; }
            set
            {
                if (value == sample) return;

                sample = value;
                resetChannel();
            }
        }

        /// <summary>
        /// Decides whether this sample should be freed after playing.
        /// </summary>
        public bool FreeSample = false;

        void resetChannel()
        {
            channel = 0;
        }

        float initialFrequency;

        internal AudioSample(int sampleId)
        {
            Sample = sampleId;
        }

        bool wasStarted;

        internal void Play(bool restart = true, bool newChannel = false)
        {
            wasStarted = true;

            if (!hasSample)
                return;

            if (newChannel) resetChannel();

            int c = Channel;

            Bass.BASS_ChannelPlay(c, restart);
        }

        internal void Stop(bool cleanup = true)
        {
            if (!hasChannel) return;

            Bass.BASS_ChannelStop(channel);
        }

        internal bool Disposed;

        ~AudioSample()
        {
            this.Dispose(false);
        }

        public void Dispose()
        {
            if (!this.Disposed)
            {
                this.Dispose(true);
                GC.SuppressFinalize(this);
                this.Disposed = true;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // clean up managed resources
            }
        }

        public bool Playing { get { return hasChannel && Bass.BASS_ChannelIsActive(channel) != 0; } }
        internal bool Played { get { return (!hasSample || Bass.BASS_ChannelIsActive(channel) == BASSActive.BASS_ACTIVE_STOPPED) && wasStarted; } }

        internal void Pause()
        {
            if (!Playing || !hasChannel) return;

            Bass.BASS_ChannelPause(channel);
        }
    }
}