﻿using System;
using Microsoft.Xna.Framework.Input;

namespace osu.Input
{
    internal class LayoutMania
    {
        internal int Columns;
        internal ManiaLayoutsFor LayoutFor;

        internal int AlternateColumns
        {
            get { return LayoutFor == ManiaLayoutsFor.Split ? 2 : 1; }
        }

        private Keys[] keys;
        internal Keys this[int index, ManiaLayoutAccess access = ManiaLayoutAccess.Keys]
        {
            get
            {
                return keys[accessIndex(index, access)];
            }
            set
            {
                //Prevent duplicate entries
                for (int i = 0; i < keys.Length; i++)
                {
                    if (keys[i] == value)
                        keys[i] = Keys.None;
                }
                keys[accessIndex(index, access)] = value;
            }
        }

        private int accessIndex(int index, ManiaLayoutAccess access)
        {
            switch (access)
            {
                case ManiaLayoutAccess.Keys:
                    if (index < Columns) return index;
                    break;
                case ManiaLayoutAccess.AlternateKeys:
                    if (index < AlternateColumns) return index + Columns;
                    break;
                case ManiaLayoutAccess.Full:
                    return index;
            }
            throw new IndexOutOfRangeException();
        }

        internal LayoutMania(int columns, ManiaLayoutsFor layoutFor, string layoutString = @"")
        {
            Columns = columns;
            LayoutFor = layoutFor;
            keys = new Keys[Columns + AlternateColumns];

            //Split layout string on whitespace
            string[] tokens = layoutString.Split(default(char[]), StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < keys.Length; i++)
            {
                try
                {
                    this[i, ManiaLayoutAccess.Full] = (Keys)Enum.Parse(typeof(Keys), tokens[i], true);
                }
                catch
                {
                    this[i, ManiaLayoutAccess.Full] = Keys.None;
                }
            }
        }

        public override string ToString()
        {
            //Index after Columns to stop at when there's nothing but None afterwards
            int last = Columns - 1;
            for (int i = Columns; i < keys.Length; i++)
            {
                if (this[i, ManiaLayoutAccess.Full] != Keys.None)
                    last = i;
            }

            string ret = String.Empty;
            for (int i = 0; i <= last; i++)
            {
                ret += this[i, ManiaLayoutAccess.Full].ToString() + ' ';
            }
            return ret.TrimEnd();
        }
    }

    internal enum ManiaLayoutAccess
    {
        Full = -1,
        Keys,
        AlternateKeys
    }
}
