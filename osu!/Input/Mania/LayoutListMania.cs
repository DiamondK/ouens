﻿using System;
using System.Collections.Generic;

namespace osu.Input
{
    internal class LayoutListMania
    {
        internal int Columns;
        internal ManiaLayoutsFor LayoutsFor;

        private List<LayoutMania> layouts = new List<LayoutMania>();
        private int selected = -1;
        internal int Selected
        {
            get { return selected; }
            set { selected = value < layouts.Count ? Math.Max(value, -1) : -1; }
        }

        internal LayoutMania this[int index]
        {
            get { return index >= 0 && index < layouts.Count ? layouts[index] : null; }
        }

        internal LayoutMania Layout { get { return this[Selected]; } }

        internal int Count { get { return layouts.Count; } }

        internal LayoutListMania(int columns, ManiaLayoutsFor layoutsFor)
        {
            Columns = columns;
            LayoutsFor = layoutsFor;
        }

        internal void Add(string layoutsString)
        {
            foreach (string layout in layoutsString.Split(';'))
            {
                layouts.Add(new LayoutMania(Columns, LayoutsFor, layout));
            }
        }

        internal void Insert(int index)
        {
            layouts.Insert(index, new LayoutMania(Columns, LayoutsFor));
        }

        internal void Remove(int index)
        {
            if (this[index] != null)
                layouts.RemoveAt(index);

            //Revalidate the current selected index
            Selected = Selected;
        }

        public override string ToString()
        {
            string ret = String.Empty;
            foreach (LayoutMania layout in layouts)
            {
                ret += layout.ToString() + ';';
            }
            return ret.TrimEnd(';');
        }
    }
}
