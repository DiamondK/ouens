﻿using System;
using osu.Configuration;
using osu.GameModes.Play.Rulesets.Mania;

namespace osu.Input
{
    internal static class BindingManagerMania
    {
        private static LayoutListMania[,] layoutsFor = new LayoutListMania[StageMania.MAX_COLUMNS + 1, (int)ManiaLayoutsFor.LengthOfEnum];

        internal static void Initialize()
        {
            for (int i = 1; i <= StageMania.MAX_COLUMNS; i++)
            {
                for (int f = 0; f < (int)ManiaLayoutsFor.LengthOfEnum; f++)
                {
                    LayoutListMania ll = layoutsFor[i, f] = new LayoutListMania(i, (ManiaLayoutsFor)f);
                    string forString = layoutForToString((ManiaLayoutsFor)f);

                    try
                    {
                        object l;
                        if (ConfigManager.Configuration.TryGetValue(@"ManiaLayouts" + i.ToString() + 'K' + forString, out l))
                            ll.Add(l.ToString());
                    }
                    catch { }
                    try
                    {
                        object s;
                        if (ConfigManager.Configuration.TryGetValue(@"ManiaLayoutSelected" + i.ToString() + 'K' + forString, out s))
                            ll.Selected = int.Parse(s.ToString());
                    }
                    catch { }
                }
            }
        }

        internal static void WriteConfiguration()
        {
            for (int i = 1; i <= StageMania.MAX_COLUMNS; i++)
            {
                for (int f = 0; f < (int)ManiaLayoutsFor.LengthOfEnum; f++)
                {
                    LayoutListMania ll = layoutsFor[i, f];
                    string layouts;
                    if (!String.IsNullOrEmpty(layouts = ll.ToString()))
                    {
                        string forString = layoutForToString((ManiaLayoutsFor)f);
                        ConfigManager.Configuration[@"ManiaLayouts" + i.ToString() + 'K' + forString] = layouts;
                        ConfigManager.Configuration[@"ManiaLayoutSelected" + i.ToString() + 'K' + forString] = ll.Selected.ToString();
                    }
                }
            }
        }

        internal static LayoutListMania GetLayoutList(int i, ManiaLayoutsFor f)
        {
            return layoutsFor[i, (int)f];
        }

        private static string layoutForToString(ManiaLayoutsFor f)
        {
            return f == ManiaLayoutsFor.Normal ? String.Empty : f.ToString();
        }
    }

    internal enum ManiaLayoutsFor
    {
        Normal,
        Split,
        LengthOfEnum
    }
}
