﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.GameModes.Menus;
using osu.GameModes.Play;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu_common.Bancho.Objects;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
using osu.Online.Social;
using osu.Online;
using Microsoft.Xna.Framework.Input;
using osu.Helpers;

namespace osu.Input.Handlers
{
    internal class MouseManager
    {
        internal static long MousePollTime;
        internal static int MousePollRate;
        internal static int MousePollLatency;
        internal static float MousePollRateInterpolated;

        private static ButtonState _back;
        private static ButtonState _forward;
        private static ButtonState _left;
        private static ButtonState _middle;
        private static ButtonState _right;
        private static int _scroll;
        internal static Vector2 ClickPosition;
        internal static int ClickTime;
        public static bool DoubleClick;

        /// <summary>
        /// Mouse was clicked this update (from a gameplay perspective).
        /// </summary>
        public static bool GameClickRegistered;

        /// <summary>
        /// Mouse is held-down (from a gameplay perspective).
        /// </summary>
        internal static bool GameDownState;

        public static MouseButtons LastButton;
        public static MouseButtons LastButton2;

        internal static ButtonState mouseForwardLast;
        internal static ButtonState mouseBackLast;

        internal static bool MouseInWindow
        {
            get
            {
                return GameBase.WindowRectangleWidescreen.Contains(MousePoint);
            }
        }

        internal static bool MouseDown;
        internal static MouseButtons MouseDownButton;
        internal static MouseButtons LastDownButton;

        private static ButtonState mouseLeft;
        internal static ButtonState MouseLeft
        {
            get
            {
                return mouseLeft;
            }

            set
            {
                // We don't immediately set to being unpressed so that we don't "lose" mouse clicks that go down and up within the same frame
                if (value == ButtonState.Pressed)
                    mouseLeft = value;

                _left = value;
            }
        }

        private static ButtonState mouseRight;
        internal static ButtonState MouseRight
        {
            get
            {
                return mouseRight;
            }

            set
            {
                // We don't immediately set to being unpressed so that we don't "lose" mouse clicks that go down and up within the same frame
                if (value == ButtonState.Pressed)
                    mouseRight = value;

                _right = value;
            }
        }

        private static ButtonState mouseMiddle;
        internal static ButtonState MouseMiddle
        {
            get
            {
                return mouseMiddle;
            }

            set
            {
                // We don't immediately set to being unpressed so that we don't "lose" mouse clicks that go down and up within the same frame
                if (value == ButtonState.Pressed)
                    mouseMiddle = value;

                _middle = value;
            }
        }

        private static ButtonState mouseBack;
        internal static ButtonState MouseBack
        {
            get
            {
                return mouseBack;
            }

            set
            {
                // We don't immediately set to being unpressed so that we don't "lose" mouse clicks that go down and up within the same frame
                if (value == ButtonState.Pressed)
                    mouseBack = value;

                _back = value;
            }
        }

        private static ButtonState mouseForward;
        internal static ButtonState MouseForward
        {
            get
            {
                return mouseForward;
            }

            set
            {
                // We don't immediately set to being unpressed so that we don't "lose" mouse clicks that go down and up within the same frame
                if (value == ButtonState.Pressed)
                    mouseForward = value;

                _forward = value;
            }
        }

        internal static void Initialize()
        {
            GameBase.Form.Deactivate += Form_Deactivate;
            GameBase.Form.MouseWheel += Form_MouseWheel;

            if (GameBase.OGL)
            {
                GameBase.GlControl.MouseDown += Form_MouseDown;
                GameBase.GlControl.MouseUp += Form_MouseUp;
                GameBase.GlControl.MouseWheel += Form_MouseWheel;
            }
            else
            {
                GameBase.Form.MouseDown += Form_MouseDown;
                GameBase.Form.MouseUp += Form_MouseUp;
            }
        }

        internal static void ApplyHandler(CursorInputHandler handler)
        {
            SetPosition(handler.Position);

            if (handler.Left != null) MouseLeft = handler.Left.Value;
            if (handler.Right != null) MouseRight = handler.Right.Value;
            if (handler.Middle != null) MouseMiddle = handler.Middle.Value;
            if (handler.Back != null) MouseBack = handler.Back.Value;
            if (handler.Forward != null) MouseForward = handler.Forward.Value;
        }

        internal static void SetPosition(Vector2 pos)
        {
            MousePosition = pos;

            // We always want out handlers to stay in sync with their relative input.
            InputManager.SetCursorHandlerPositions(pos);
        }

        internal static void SetPosition(Point pos)
        {
            SetPosition(new Vector2(pos.X, pos.Y));
        }

        internal static void SetNativePosition(Point pos, bool force = false)
        {
            if (!force && !GameBase.IsActive)
                return;

            int editorOffset = GameBase.MenuVisible && GameBase.OGL ? GameBase.EditorControl.Height : 0;
            Cursor.Position = GameBase.Form.PointToScreen(new System.Drawing.Point(pos.X, pos.Y + editorOffset));
        }

        internal static Point GetNativePosition()
        {
            int editorOffset = GameBase.MenuVisible && GameBase.OGL ? GameBase.EditorControl.Height : 0;

            System.Drawing.Point pos = GameBase.Form.PointToClient(Cursor.Position);
            return new Point(pos.X, pos.Y - editorOffset);
        }

        internal static Point MousePoint
        {
            get;
            private set;
        }

        internal static Vector2 MousePositionWindow
        {
            get;
            private set;
        }

        private static Vector2 mousePosition;

        internal static Vector2 MousePosition
        {
            get
            {
                return mousePosition;
            }

            private set
            {
                mousePosition = value;

                MousePositionWindow = (mousePosition + new Vector2(0, GameBase.MenuVisible && GameBase.OGL ? GameBase.EditorControl.Height : 0)) / GameBase.WindowRatio;
                MousePoint = new Point((int)Math.Round(value.X), (int)Math.Round(value.Y));
            }
        }

        internal static bool PhysicalClickRegistered;
        internal static bool showMouse;
        internal static bool showWindow;
        internal static bool showGame;

        internal static void ResetPosition()
        {
            SetPosition(InputManager.CursorPosition);
        }

        internal static bool PosInWindow(Vector2 pos, int lenience = 0)
        {
            return PosInWindow(new Point((int)Math.Round(pos.X), (int)Math.Round(pos.Y)), lenience);
        }

        internal static bool PosInWindow(Point pos, int lenience = 0)
        {
            return pos.X >= lenience && pos.X < GameBase.WindowWidth - lenience && pos.Y >= lenience & pos.Y < GameBase.WindowHeight + GameBase.WindowOffsetY - lenience;
        }

        internal static void ChangeCursor()
        {
            ChangeCursor(Cursors.Arrow);
        }

        internal static void ChangeCursor(Cursor c)
        {
            if (GameBase.Form.Cursor != c)
                GameBase.Form.Cursor = c;
        }

        internal static void UpdateMouseCursorVisibility()
        {
            if (GameBase.Tournament)
            {
                showGame = GameBase.Mode == OsuModes.Play && InputManager.ReplayMode;
                showWindow = true;
                showMouse = true;
            }
            else
            {
                showGame = InputManager.HandleInput && GameBase.Mode != OsuModes.Edit && (InputManager.ReplayMode || !(ChatEngine.IsFullVisible && ChatEngine.IsVisible));

                //special conditions for play mode (decided by ruleset itself).
                showGame &= GameBase.Mode != OsuModes.Play || Player.Instance == null || !Player.Instance.HideMouse;

                //special case for menu, where cursor will disappear after idle.
                showGame &= (GameBase.Mode != OsuModes.Menu || GameBase.IdleTime < 5000);

                showGame &= GameBase.Mode != OsuModes.Exit;

                showWindow = (ChatEngine.IsFullVisible && ChatEngine.IsVisible)
                             || GameBase.Mode == OsuModes.Edit
                             || (GameBase.Mode == OsuModes.Play && InputManager.ReplayMode && InputManager.ReplayCursorFollow)
                             || GameBase.GameQueuedState == OsuModes.Exit
                             || GameBase.MenuActive;

                showMouse = showWindow || (!MouseInWindow && !PosInWindow(GetNativePosition()));
            }

            if (showMouse != GameBase.MouseVisible)
            {
                GameBase.MouseVisible = showMouse;

                if (showMouse)
                    Cursor.Show();
                else
                    Cursor.Hide();
            }

            if (InputManager.s_Cursor == null)
                return;

            if ((showGame && InputManager.s_Cursor.Alpha == 1) || (!showGame && InputManager.s_Cursor.Alpha == 0))
                return; //we are already at the correct alpha.

            if (InputManager.s_Cursor.Transformations.Find(t => t.Type == TransformationType.Fade) != null)
                return; //we are already transforming.

            if (showGame)
                InputManager.s_Cursor.FadeIn(100);
            else
                InputManager.s_Cursor.FadeOut(300);
        }

        internal static CursorInputHandler HandleCursorInput(List<InputHandler> inputHandlers)
        {
            CursorInputHandler currentCursorHandler = null;

            CursorInputHandler mouseHandler = null;
            CursorInputHandler rawMouseHandler = null;

            foreach (InputHandler h in inputHandlers)
            {
                // Make first handler which is active the current handler. (Handlers are ordered by priority.)
                if (currentCursorHandler == null && h.IsActive && h is CursorInputHandler)
                    currentCursorHandler = (CursorInputHandler)h;

                if (h is MouseHandler)
                    mouseHandler = (CursorInputHandler)h;

                if (h is RawMouseHandler)
                    rawMouseHandler = (CursorInputHandler)h;

                h.UpdateInput(currentCursorHandler == h);
            }

            if (currentCursorHandler != null)
            {
                if ((currentCursorHandler is MouseHandler || currentCursorHandler is RawMouseHandler) &&
                    mouseHandler != null &&
                    rawMouseHandler != null)
                {
                    Player.HaxCheckMouse(mouseHandler.Position - MousePosition, rawMouseHandler.Position - MousePosition);
                }

                ApplyHandler(currentCursorHandler);
            }

            return currentCursorHandler;
        }

        internal static void PostProcessButtons()
        {
            if (Player.Playing && ConfigManager.sMouseDisableButtons)
            {
                ResetStatus();
            }
            else
            {
                //handle back/forward buttons on mouse.
                if (Game.IsActive && GameBase.Mode != OsuModes.Edit && GameBase.Mode != OsuModes.Menu)
                {
                    if (MouseBack == ButtonState.Pressed && mouseBackLast != MouseBack)
                        KeyboardHandler.DoKeyPressed(Microsoft.Xna.Framework.Input.Keys.Escape);
                    mouseBackLast = MouseBack;

                    if (MouseForward == ButtonState.Pressed && mouseForwardLast != MouseForward)
                        KeyboardHandler.DoKeyPressed(BindingManager.For(Bindings.Skip));
                    mouseForwardLast = MouseForward;
                }
            }
        }

        private static bool reverseInputOrder
        {
            get
            {
                return (GameBase.ActiveDialog == null || !(GameBase.ActiveDialog is GameModes.Options.OptionsBindingDialog)) && //OptionsBindingDialog interferes with this method.
                    (
                    //chat is visible and cursor is contained, or users panel is visible (IsFullVisible)
                        (ChatEngine.IsVisible && (MousePositionWindow.Y > GameBase.WindowHeightScaled - ChatEngine.CHATAREA_HEIGHT || ChatEngine.IsFullVisible)) ||
                        (KeyboardHandler.ControlPressed && KeyboardHandler.ShiftPressed)
                    );
            }
        }

        internal static void DoMouseWheelUp()
        {
            InputManager.LastActionTime = GameBase.Time;
            InputManager.TriggerEvent(InputEventType.OnMouseWheelUp);
        }

        internal static void DoMouseWheelDown()
        {
            InputManager.LastActionTime = GameBase.Time;
            InputManager.TriggerEvent(InputEventType.OnMouseWheelDown);
        }

        internal static void DoOnDrag()
        {
            InputManager.TriggerEvent(InputEventType.OnDrag);
            InputManager.IsDragging = true;
        }

        internal static void DoOnGamePress()
        {
            if (ConfigManager.sCursorRipple && (GameBase.Mode != OsuModes.Play || !ModManager.CheckActive(Mods.Cinema)) && (Player.Instance == null || !Player.Instance.HideMouse))
            {
                pTexture ripple = SkinManager.Load(@"cursor-ripple");
                if (ripple == null) ripple = SkinManager.Load(@"menu-osu-shockwave", SkinSource.Osu);

                pSprite rippleSprite = new pSprite(ripple, Fields.Native, Origins.Centre, Clocks.Game, InputManager.CursorPosition, 0.2F, false, Color.White);
                rippleSprite.Additive = true;
                rippleSprite.Alpha = 0.2f;
                rippleSprite.FadeOut(700);
                rippleSprite.Transformations.Add(new Transformation(TransformationType.Scale, 0.05f, 0.5f, GameBase.Time, GameBase.Time + 700, EasingTypes.Out));
                GameBase.spriteManagerOverlay.Add(rippleSprite);
            }

            InputManager.TriggerEvent(InputEventType.OnGamePress);
        }

        internal static void DoOnClick()
        {
            if (PhysicalClickRegistered)
            {
                SpriteManager.ClickHandledSprite = null;
                BanchoClient.Activity();
            }

            SpriteManager.HandleClick(false);

            InputManager.TriggerEvent(InputEventType.OnClick);
        }

        internal static void DoOnClickRepeat()
        {
            SpriteManager.HandleClick(true);
        }

        internal static void DoOnClickUp()
        {
            InputManager.TriggerEvent(InputEventType.OnClickUp);
            SpriteManager.HandleClickUp();
        }

        internal static void DoOnDoubleClick()
        {
            InputManager.TriggerEvent(InputEventType.OnDoubleClick);
            InputManager.DoubleClickTime = 0;
        }

        internal static void DoOnEndDrag()
        {
            InputManager.TriggerEvent(InputEventType.OnEndDrag);
            InputManager.IsDragging = false;
        }

        public static pButtonState pMouseCreate(bool l, bool l2, bool r, bool r2, bool s = false)
        {
            pButtonState p = pButtonState.None;
            if (l) p |= pButtonState.Left1;
            if (l2) p |= pButtonState.Left2;
            if (r) p |= pButtonState.Right1;
            if (r2) p |= pButtonState.Right2;
            if (s) p |= pButtonState.Smoke;

            return p;
        }

        private static void Form_Deactivate(object sender, EventArgs e)
        {
            MouseLeft = ButtonState.Released;
            MouseRight = ButtonState.Released;
            MouseMiddle = ButtonState.Released;
            MouseBack = ButtonState.Released;
            MouseForward = ButtonState.Released;
        }

        private static void Form_MouseWheel(object sender, MouseEventArgs e)
        {
            _scroll += e.Delta;
        }

        private static void Form_MouseUp(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    MouseLeft = ButtonState.Released;
                    break;
                case MouseButtons.Right:
                    MouseRight = ButtonState.Released;
                    break;
                case MouseButtons.Middle:
                    MouseMiddle = ButtonState.Released;
                    break;
                case MouseButtons.XButton1:
                    MouseBack = ButtonState.Released;
                    break;
                case MouseButtons.XButton2:
                    MouseForward = ButtonState.Released;
                    break;
            }
        }

        private static void Form_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    MouseLeft = ButtonState.Pressed;
                    break;
                case MouseButtons.Right:
                    MouseRight = ButtonState.Pressed;
                    break;
                case MouseButtons.Middle:
                    MouseMiddle = ButtonState.Pressed;
                    break;
                case MouseButtons.XButton1:
                    MouseBack = ButtonState.Pressed;
                    break;
                case MouseButtons.XButton2:
                    MouseForward = ButtonState.Pressed;
                    break;
            }
        }

        /// <summary>
        /// Updates the active button states from internally set values. If any button has changed, then this returns true.
        /// </summary>
        internal static bool UpdateButtons()
        {
            if (mouseLeft != _left || mouseRight != _right || mouseMiddle != _middle || mouseBack != _back || mouseForward != _forward)
            {
                mouseLeft = _left;
                mouseRight = _right;
                mouseMiddle = _middle;
                mouseBack = _back;
                mouseForward = _forward;

                return true;
            }

            return false;
        }

        internal static void UpdateWheel()
        {
            if (_scroll == 0) return;

            if (_scroll > 0)
                DoMouseWheelUp();
            else
                DoMouseWheelDown();
            _scroll = 0;
        }

        internal static void ResetStatus()
        {
            mouseLeft = ButtonState.Released;
            _left = ButtonState.Released;
            mouseRight = ButtonState.Released;
            _right = ButtonState.Released;
            mouseMiddle = ButtonState.Released;
            _middle = ButtonState.Released;
        }
    }
}
