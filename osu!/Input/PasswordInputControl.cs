﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using osu.Graphics.UserInterface;
using osu.Input.Handlers;
using osu_common.Helpers;

namespace osu.Input
{
    class PasswordInputControl : TextInputControl
    {
        public PasswordInputControl(pTextBox textBox)
            : base(textBox)
        {
            UnderlyingText = TextBox.Text;
            OnCandidateChange += UpdateUnderlyingText;

            OnKey += keyDown;
        }

        public override string Text
        {
            get
            {
                return RepeatCharacter('*', TextBox.Text.Length);
            }
            set
            {
                base.Text = value;
                UnderlyingText = value;
            }
        }
        private void UpdateUnderlyingText()
        {
            UnderlyingText = TextBox.Text;
        }

        internal string UnderlyingText { get; private set; }

        private string RepeatCharacter(char character, int amount)
        {
            StringBuilder sb = new StringBuilder(amount);
            for (int i = 0; i < amount; i++)
                sb.Append(character);
            return sb.ToString();
        }

        private void keyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
                e.SuppressKeyPress = true;
        }
    }
}
