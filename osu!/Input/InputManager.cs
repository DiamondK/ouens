using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameplayElements.Scoring;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu.Input.Handlers;
using osu.Online;
using osu_common;
using osu_common.Bancho.Objects;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Point = Microsoft.Xna.Framework.Point;
using Rectangle = System.Drawing.Rectangle;
using osu.Graphics.UserInterface;
using osu_common.Helpers;
using osu.GameModes.Play.Rulesets.Mania;
using osu.Graphics.Notifications;
using osu.GameplayElements;
using osu.GameplayElements.Events;
using Microsoft.Xna.Framework.Graphics;
using osu.GameplayElements.Beatmaps;
using osu.Online.Social;
using System.Diagnostics;
using osu.GameModes.Edit;
using osu.Graphics;

namespace osu.Input
{
    internal static class InputManager
    {
        private const int REPEAT_TICK_RATE = 100;
        internal static readonly Rectangle CursorOriginalBounds = Cursor.Clip;
        private static readonly List<InputHandler> inputHandlers = new List<InputHandler>();
        private static readonly Queue<VoidDelegate> mouseFrameQueue = new Queue<VoidDelegate>();
        internal static bool Buffering;
        internal static Point CursorPoint;
        internal static Vector2 CursorPosition;
        internal static int DoubleClickTime;
        private static string fullTextChatBuffer;
        private static bool fullTextChatLocal;
        internal static string fullTextGameBuffer;
        private static bool fullTextGameLocal;
        private static bool fullTextLocal;
        internal static bool IsDragging;
        internal static KeyboardState keyboard;
        private static KeyboardState keyboardLast;
        internal static pButtonState lastButtonState;
        internal static ButtonState leftButton;
        internal static bool leftButton1;
        internal static bool leftButton1i;
        internal static bool leftButton2;
        internal static bool leftButton2i;
        internal static ButtonState leftButtonLast;
        internal static bool leftButtonLastBool;
        internal static bool leftCond; //click detected this update frame
        private static bool LocalInputPriority = true;
        private static bool middleCond;
        internal static bool MouseButtonInstantRelease;
        private static ButtonState mouseLeftActualPrev;
        private static ButtonState mouseMiddlePrev;
        private static ButtonState mouseRightActualPrev;
        internal static List<Keys> PressedKeys = new List<Keys>();
        private static double mouseRepeatTickCurrent;
        private static double keyboardRepeatTickCurrent;
        internal static bool ReplayCursorFollow = true;
        internal static bool HandleInput = true;
        internal static int ReplayFrame;
        internal static Vector2 ReplayGamefieldCursor;


        private static bool replayMode;
        internal static bool ReplayMode
        {
            get { return replayMode; }
            set
            {
                replayMode = value;
                if (!replayMode)
                {
                    ReplayScore = null;
                    CurrentReplayFrame = null;
                }
            }
        }

        internal static ReplaySpeed ReplayModeFF;
        internal static bool NewReplayFrame;
        internal static Score ReplayScore;
        internal static bool ReplayStreaming;
        internal static int ReplayStartTime;
        internal static bool ReplayToEnd;
        internal static ButtonState rightButton;
        internal static bool rightButton1;
        internal static bool rightButton1i;
        internal static bool rightButton2;
        internal static bool rightButton2i;
        internal static ButtonState rightButtonLast;
        internal static bool rightButtonLastBool;
        internal static bool rightCond;
        internal static pSprite s_Cursor;
        internal static ImeTextBox TextBox;
        internal static pTextBoxOmniscient TextBoxOmniscient;
        internal static TextInputControl TextInput;
        private static bool activeLastFrame;

        public static bool FullTextEnabled
        {
            get { return fullTextLocal; }
            set
            {
                if (fullTextLocal == value) return;
                fullTextLocal = value;

                TextBoxOmniscient.MaintainFocus = value;
            }
        }

        /// <summary>
        /// Evaluates to true if a touch input device is currently being actively used.
        /// </summary>
        public static bool TouchDevice
        {
            get { return CurrentCursorHandler is TouchHandler || CurrentCursorHandler is EloTouchscreenHandler; }
        }

        public static List<Vector2> AllClickPositions
        {
            get
            {
                if (CurrentCursorHandler is TouchHandler)
                    return new List<Vector2>(((TouchHandler)CurrentCursorHandler).TouchDownPositions.Values);
                else if (MouseManager.MouseDown)
                    return new List<Vector2>(new[] { MouseManager.ClickPosition / GameBase.WindowRatio });
                else
                    return new List<Vector2>();
            }
        }

        /// <summary>
        /// True when we should handle this replay frame as having accurate positioning/clicking data.
        /// </summary>
        internal static bool ScorableFrame;

        private static StageMania ManiaStage { get { return GameBase.Mode == OsuModes.Play ? Player.Instance.hitObjectManager.ManiaStage : Editor.Instance.hitObjectManager.ManiaStage; } }

        public static void SetFullTextBuffer(string s)
        {
            if (s == null) s = string.Empty;
            GameBase.Scheduler.Add(delegate
            {
                TextInput.TextBox.Text = s;
                TextInput.TextBox.Select(Math.Max(0, s.Length), 0);
            });
        }

        public static void FullTextFocus()
        {
        }

        internal static void Initialize()
        {
            GameBase.OnResolutionChange += GameBase_OnResolutionChange;

            InitializeTargets();

            MouseManager.Initialize();
            InitializeInputBox();

            WiimoteManager.Initialize();

            AddHandler(new TouchHandler());
            if (ConfigManager.sEloTouchscreen) AddHandler(new EloTouchscreenHandler());
            if (!ConfigManager.sSkipTablet) AddHandler(new TabletHandler());
            if (ConfigManager.sJoystick) AddHandler(new JoystickHandler());
            AddHandler(new RawMouseHandler());
            AddHandler(new MouseHandler());
            //AddHandler(new RawKeyboardHandler());

            GameBase.Instance.Activated += GameBase_ActivationChanged;
            GameBase.Instance.Deactivated += GameBase_ActivationChanged;
        }

        internal static void Dispose()
        {
            foreach (InputHandler h in inputHandlers)
            {
                h.Dispose();
            }

            GameBase.OnResolutionChange -= GameBase_OnResolutionChange;
        }

        internal static void GameBase_OnResolutionChange()
        {
            foreach (InputHandler h in inputHandlers)
            {
                h.OnResolutionChange();
            }
        }


        static void GameBase_ActivationChanged(object sender, EventArgs e)
        {
            UpdateShallClampMouseToWindow();

            if (GameBase.IsActive)
            {
                MouseManager.ClickPosition = CursorPosition;
            }
        }

        internal static bool ShallClampMouseToWindow = false;
        private static bool cursorClippedByOsu = false;
        internal static void UpdateShallClampMouseToWindow()
        {
            if (GameBase.Tournament || osuMain.IsWine)
                return;

            bool playModeUsingMouse = false;
            Player p = Player.Instance;
            if (p != null && p.Ruleset != null)
                playModeUsingMouse = p.Ruleset.UsesMouse;

            bool playingLocally = (Player.Playing && playModeUsingMouse) && !InputManager.ReplayMode;

            RawMouseHandler rawMouse = CurrentCursorHandler as RawMouseHandler;
            bool forceClipping = rawMouse != null && !rawMouse.AbsoluteMovement && MouseManager.PosInWindow(MouseManager.MousePoint, 1);

            if (LocalInputPriority && (playingLocally || ((GameBase.graphics.IsFullScreen || forceClipping) && Game.IsActive)))
            {
                if (Cursor.Clip != GameBase.ClientBounds)
                {
                    Cursor.Clip = GameBase.ClientBounds;
                    if (playingLocally) GameBase.Form.TopMost = true;
                    cursorClippedByOsu = true;
                }

                ShallClampMouseToWindow = true;
            }
            else
            {
                if ((Cursor.Clip != CursorOriginalBounds && cursorClippedByOsu) || GameBase.Form.TopMost)
                {
                    Cursor.Clip = CursorOriginalBounds;
                    GameBase.Form.TopMost = false;
                    cursorClippedByOsu = false;
                }

                ShallClampMouseToWindow = false;
            }
        }

        private static void InitializeInputBox()
        {
            TextBoxOmniscient = new pTextBoxOmniscient(12, new Vector2(10.5f, 465), 640);
            TextBoxOmniscient.Box.TextBold = false;

            TextInput = TextBoxOmniscient.InputControl;
            TextBox = TextInput.TextBox;
            TextBoxOmniscient.MaintainFocus = false;

            AddTextInputControl(TextInput);
        }

        private static bool AllowSensitivityChanges
        {
            get
            {
                return Game.IsActive && GameBase.Mode != OsuModes.Edit;
            }
        }


        internal static CursorInputHandler CurrentCursorHandler = null;

        internal static void UpdateFrame()
        {
            bool wasLocalInputPriority = LocalInputPriority;
            LocalInputPriority = !(ReplayMode || (Player.Relaxing2 && !Player.Paused && (!EventManager.BreakMode || (Player.Instance != null && !Player.Instance.Passed))));

            UpdateShallClampMouseToWindow();

            MouseManager.UpdateMouseCursorVisibility();
            CurrentCursorHandler = MouseManager.HandleCursorInput(inputHandlers);
            MouseManager.PostProcessButtons();

            if (DoubleClickTime > 0)
                DoubleClickTime = (int)Math.Max(0, DoubleClickTime - GameBase.ElapsedMilliseconds);

            //Initialises the current input/processing loop frame.  In the case of 2x replays, up to two frames may be processed in a single game tick.
            int loopFrame = 0;

            CursorPositionDelta = LocalInputPriority && wasLocalInputPriority ? CursorPositionLast - InputManager.CursorPosition : Vector2.Zero;
            CursorPositionLast = InputManager.CursorPosition;

            int time = AudioEngine.Time;
            int last = AudioEngine.TimeLastFrame;

            bool needsMoreFrames = false;

            do
            {
                AudioEngine.Update(); //run an audio update each frame (resets times back to sane values).

                ScorableFrame = GameBase.SixtyFramesPerSecondFrame;

                if (!LocalInputPriority)
                {
                    needsMoreFrames = UpdateReplay(); //REPLAY MAGIC HAPPENS HERE

                    if (!ReplayMode && Player.Relaxing2)
                    {
                        Player.UpdateIsPlaying();
                        UpdateButtonsLocalPlayer();
                    }
                }
                else if (!GameBase.IsMinimized)
                {
                    Player.UpdateIsPlaying();

                    UpdateButtonsLocalPlayer();

                    if (CursorPosition != MouseManager.MousePosition)
                    {
                        if (MouseManager.MouseInWindow) LastActionTime = GameBase.Time;
                        CursorPosition = MouseManager.MousePosition;
                    }

                    CursorPoint = MouseManager.MousePoint;
                    ReplayGamefieldCursor = GameBase.DisplayToGamefield(CursorPosition);
                }

                //Update mouse cursor position, except in editor mode where we use the windows cursor.
                //Sometimes we want to read replay data without controlling the cursor, too.
                if (!GameBase.MenuVisible)
                    s_Cursor.Position = ReplayCursorFollow ? CursorPosition : MouseManager.MousePosition;

                //s_Cursor.Bypass = TouchDevice; //todo: move this to somewhere it actively gets called

                //Only do actions on the input if the window is active.
                if (HandleInput && (Game.IsActive || ReplayMode))
                {
                    MouseManager.UpdateWheel();

                    try
                    {
                        keyboard = Keyboard.GetState();
                    }
                    catch (InvalidOperationException)
                    {
                    }

                    keys.Clear();

                    if (GameBase.IsActive)
                    {
                        Keys[] kArray = keyboard.GetPressedKeys();

                        if (kArray.Length > 0)
                            keys.AddRange(kArray);
                    }

                    if (ConfigManager.sWiimote)
                        WiimoteManager.Update(keys);
                    if (ConfigManager.sJoystick)
                        keys.AddRange(JoystickHandler.KeyDown);


                    if (!ReplayMode)
                        MouseViaKeyboardControls();

                    UpdateKeyboard(keys);

                    mouseRepeatTickCurrent += GameBase.ElapsedMilliseconds;
                    if (mouseRepeatTickCurrent >= REPEAT_TICK_RATE)
                    {
                        mouseRepeatTickCurrent -= REPEAT_TICK_RATE;

                        if (leftButton == ButtonState.Pressed && MouseManager.MouseDown &&
                            GameBase.FadeState != FadeStates.FadeOut)
                            mouseFrameQueue.Enqueue(MouseManager.DoOnClickRepeat);
                    }

                    leftCond = leftButton == ButtonState.Pressed && (MouseButtonInstantRelease ? leftButton1i || leftButton2i : leftButtonLast != leftButton);
                    rightCond = rightButton == ButtonState.Pressed && (MouseButtonInstantRelease ? rightButton1i || rightButton2i : rightButtonLast != rightButton);
                    middleCond = MouseManager.MouseMiddle == ButtonState.Pressed && mouseMiddlePrev != MouseManager.MouseMiddle;

                    bool localDown = Game.IsActive && ((MouseManager.MouseLeft == ButtonState.Pressed && mouseLeftActualPrev != ButtonState.Pressed) || (MouseManager.MouseRight == ButtonState.Pressed && mouseRightActualPrev != ButtonState.Pressed) || middleCond);
                    bool localUp = (MouseManager.MouseLeft == ButtonState.Released && mouseLeftActualPrev != ButtonState.Released) || (MouseManager.MouseRight == ButtonState.Released && mouseRightActualPrev != ButtonState.Released) || (MouseManager.MouseMiddle == ButtonState.Released && mouseMiddlePrev != ButtonState.Released);

                    MouseManager.PhysicalClickRegistered = localDown || localUp;
                    MouseManager.DoubleClick = false;

                    if (leftButtonLast != leftButton || rightButtonLast != rightButton)
                    {
                        MouseManager.LastButton2 = MouseManager.LastButton;
                        MouseManager.LastButton = MouseManager.MouseDownButton;

                        MouseManager.MouseDownButton = MouseButtons.None;
                        if (leftButton == ButtonState.Pressed)
                        {
                            MouseManager.MouseDownButton |= MouseButtons.Left;
                            MouseManager.LastDownButton = MouseButtons.Left;
                        }

                        if (rightButton == ButtonState.Pressed)
                        {
                            MouseManager.MouseDownButton |= MouseButtons.Right;
                            MouseManager.LastDownButton = MouseButtons.Right;
                        }
                    }

                    bool didClick = false;
                    bool cursorUp;

                    //We don't have a replay click, so lets check for physical mouse clicks.
                    //In the case that there is a replay and actual click the same frame, doOnClick will be called only once (above).
                    if (localDown)
                    {
                        MouseManager.ClickPosition = MouseManager.MousePosition;
                        MouseManager.ClickTime = GameBase.Time;
                        LastActionTime = GameBase.Time;

                        if (GameBase.FadeState != FadeStates.FadeOut)
                        {
                            if (DoubleClickTime > 0)
                            {
                                MouseManager.DoubleClick = true;
                                mouseFrameQueue.Enqueue(MouseManager.DoOnDoubleClick);
                                DoubleClickTime = 0;
                            }
                            else
                            {
                                mouseFrameQueue.Enqueue(MouseManager.DoOnClick);
                                didClick = true;
                                mouseRepeatTickCurrent = 0;
                                DoubleClickTime = 250;
                            }
                        }
                    }

                    MouseManager.GameClickRegistered = false;

                    if (leftCond || rightCond)
                    {
                        if (CursorExpand && (!ReplayMode || ReplayCursorFollow))
                        {
                            cursorIsExpanded = true;
                            s_Cursor.Transformations.RemoveAll(tr => tr.Type == TransformationType.Scale);
                            Transformation t = new Transformation(TransformationType.Scale, 1, 1.3F, GameBase.Time, GameBase.Time + 100);
                            t.Easing = EasingTypes.Out;
                            s_Cursor.Transformations.Add(t);
                        }

                        MouseManager.GameDownState = true;
                        MouseManager.ClickTime = GameBase.Time;
                        MouseManager.GameClickRegistered = true;

                        if (GameBase.FadeState != FadeStates.FadeOut)
                        {
                            mouseFrameQueue.Enqueue(MouseManager.DoOnGamePress);
                            if (!InputManager.ReplayMode && !didClick)
                            {
                                mouseFrameQueue.Enqueue(MouseManager.DoOnClick);
                                didClick = true;
                            }
                        }
                    }

                    cursorUp = leftButton == ButtonState.Released && rightButton == ButtonState.Released && MouseManager.GameDownState;

                    if (localUp || cursorUp)
                    {
                        if (SpriteManager.ClickHandledSprite != null)
                        {
                            if (SpriteManager.ClickHandledSprite.ClickRequiresConfirmation)
                            {
                                if (Vector2.Distance(MouseManager.ClickPosition, MouseManager.MousePosition) * GameBase.WindowRatioScaleDown < 10)
                                {
                                    SpriteManager.ClickHandledSprite.Click(true);
                                }
                            }

                            SpriteManager.ClickHandledSprite = null;
                        }

                        LastClickHandled = false;
                        if (!ReplayMode || localUp)
                        {
                            mouseFrameQueue.Enqueue(MouseManager.DoOnClickUp);
                        }
                    }

                    if (cursorUp)
                    {
                        cursorIsExpanded = false;
                        if (CursorExpand)
                        {
                            s_Cursor.Transformations.RemoveAll(tr => tr.Type == TransformationType.Scale);
                            Transformation t = new Transformation(TransformationType.Scale, s_Cursor.Scale, 1, GameBase.Time, GameBase.Time + 100);
                            t.Easing = EasingTypes.Out;
                            s_Cursor.Transformations.Add(t);
                        }

                        MouseManager.GameDownState = false;
                    }

                    if (MouseManager.MouseLeft == ButtonState.Pressed && GameBase.FadeState != FadeStates.FadeOut)
                    {
                        MouseManager.MouseDown = true;
                        if (MouseManager.MousePosition != MouseManager.ClickPosition)
                            mouseFrameQueue.Enqueue(MouseManager.DoOnDrag);
                    }
                    else
                    {
                        MouseManager.MouseDown = false;
                        if (IsDragging)
                            mouseFrameQueue.Enqueue(MouseManager.DoOnEndDrag);
                    }
                }
                else
                {
                    leftButton = ButtonState.Released;
                    rightButton = ButtonState.Released;
                    KeyboardHandler.ControlPressed = false;
                    KeyboardHandler.AltPressed = false;
                    KeyboardHandler.ShiftPressed = false;
                }


                activeLastFrame = Game.IsActive;

                time = AudioEngine.Time;
                last = AudioEngine.TimeLastFrame;

                while (mouseFrameQueue.Count > 0)
                    mouseFrameQueue.Dequeue().Invoke();

                GameBase.Instance.UpdateChildren();

                leftButtonLast = leftButton;
                rightButtonLast = rightButton;

                leftButtonLastBool = leftButton == ButtonState.Pressed;
                rightButtonLastBool = rightButton == ButtonState.Pressed;

                mouseLeftActualPrev = MouseManager.MouseLeft;
                mouseRightActualPrev = MouseManager.MouseRight;
                mouseMiddlePrev = MouseManager.MouseMiddle;

                keyboardLast = keyboard;

            } while ((MouseManager.UpdateButtons() ||
                     ((ReplayMode || !LocalInputPriority) && needsMoreFrames))

                     && loopFrame++ < 30);
        }


        /// <summary>
        /// Handles replay input.
        /// </summary>
        /// <returns>true if the replay is running slow/behind</returns>
        private static bool UpdateReplay()
        {
            bool runningSlow = false;

            lock (StreamingManager.LockReplayScore)
            {
                if (ReplayScore != null && ReplayScore.replay != null)
                {
                    int count = ReplayScore.replay.Count;
                    NewReplayFrame = false;

                    // Autoplay requires all frames to be processed just like play mode since we can't know in advance which frames are relevant for scoring.
                    ScorableFrame = false;

                    int direction = AudioEngine.IsReversed ? -1 : 1;

                    Player p = Player.Instance;

                    if (GameBase.Mode == OsuModes.Play && Player.currentScore != null && p != null && p.Status != PlayerStatus.Busy && Player.Loaded &&
                        count > 0 && ReplayFrame < count - 1 &&
                        (ReplayToEnd || !GameBase.Tournament || !GameBase.TournamentBuffering) &&
                        (ReplayToEnd || !ReplayStreaming || ReplayScore.replay[count - 1].time - AudioEngine.Time > 1000))
                    {
                        Buffering = false;

                        if (!AudioEngine.IsReversed)
                        {
                            if (ReplayFrame < count - 1 && ReplayScore.replay[ReplayFrame + 1].time <= AudioEngine.Time)
                            {
                                ReplayFrame++;
                                NewReplayFrame = true;
                            }

                            runningSlow = AudioEngine.Time - ReplayScore.replay[ReplayFrame].time > 16 && NewReplayFrame;

                            //skip unnecessary frames
                            if (runningSlow && ReplayScore.replayAllowFrameSkip)
                                while (ReplayFrame < count - 1 && !ReplayScore.replay[ReplayFrame].mouseLeft &&
                                       !ReplayScore.replay[ReplayFrame].mouseRight &&
                                       ReplayScore.replay[ReplayFrame + 1].time <= AudioEngine.Time &&
                                       ReplayScore.replay[ReplayFrame].mouseLeft == leftButtonLastBool &&
                                       ReplayScore.replay[ReplayFrame].mouseRight == rightButtonLastBool &&
                                       ReplayScore.replay[ReplayFrame].buttonState == lastButtonState &&
                                       !Player.IsSpinning)
                                {
                                    if (AudioEngine.AudioFrequency == AudioEngine.InitialFrequency) Debug.Print(@"replay frame {0} skipped!!!!", ReplayFrame);
                                    ReplayFrame += direction;
                                }
                        }
                        else
                        {
                            //special case for when audio is playing back in reverse
                            if (ReplayFrame > 0 && ReplayScore.replay[ReplayFrame].time > AudioEngine.Time)
                                ReplayFrame--;

                            runningSlow = Math.Abs(ReplayScore.replay[ReplayFrame].time - AudioEngine.Time) > 16;

                            //skip unnecessary frames
                            if (runningSlow && ReplayScore.replayAllowFrameSkip)
                                while (ReplayFrame > 0 && !ReplayScore.replay[ReplayFrame].mouseLeft &&
                                       !ReplayScore.replay[ReplayFrame].mouseRight &&
                                       ReplayScore.replay[ReplayFrame - 1].time > AudioEngine.Time &&
                                       ReplayScore.replay[ReplayFrame].mouseLeft == leftButtonLastBool &&
                                       ReplayScore.replay[ReplayFrame].mouseRight == rightButtonLastBool &&
                                       ReplayScore.replay[ReplayFrame].buttonState == lastButtonState &&
                                       !Player.IsSpinning)
                                {
                                    Debug.Print(@"replay frame {0} skipped!!!!", ReplayFrame);
                                    ReplayFrame += direction;
                                }
                        }

                        Vector2 pos;

                        if (MouseButtonInstantRelease)
                        {
                            leftButton1i = false;
                            leftButton2i = false;
                            rightButton1i = false;
                            rightButton2i = false;
                            leftButton = ButtonState.Released;
                            rightButton = ButtonState.Released;
                        }

                        CurrentReplayFrame = ReplayScore.replay[ReplayFrame];

                        if (NewReplayFrame)
                        {
                            AudioEngine.Time = CurrentReplayFrame.time;

                            ScorableFrame = true;

                            leftButton1i = (!MouseButtonInstantRelease || !leftButton1) &&
                                           CurrentReplayFrame.mouseLeft1;
                            leftButton1 = CurrentReplayFrame.mouseLeft1;

                            leftButton2i = (!MouseButtonInstantRelease || !leftButton2) &&
                                           CurrentReplayFrame.mouseLeft2;
                            leftButton2 = CurrentReplayFrame.mouseLeft2;

                            rightButton1i = (!MouseButtonInstantRelease || !rightButton1) &&
                                            CurrentReplayFrame.mouseRight1;
                            rightButton1 = CurrentReplayFrame.mouseRight1;

                            rightButton2i = (!MouseButtonInstantRelease || !rightButton2) &&
                                            CurrentReplayFrame.mouseRight2;
                            rightButton2 = CurrentReplayFrame.mouseRight2;

                            leftButton = leftButton1i || leftButton2i ? ButtonState.Pressed : ButtonState.Released;
                            rightButton = rightButton1i || rightButton2i ? ButtonState.Pressed : ButtonState.Released;
                            pos = new Vector2(CurrentReplayFrame.mouseX,
                                              CurrentReplayFrame.mouseY);
                            if (Player.Loaded && ReplayScore.playMode == PlayModes.OsuMania)
                            {
                                int keyvalue = (int)CurrentReplayFrame.mouseX;
                                int speed = (int)CurrentReplayFrame.mouseY;

                                HitObjectManagerMania homMania = (HitObjectManagerMania)Player.Instance.hitObjectManager;

                                for (int i = 0; i < ManiaStage.Columns.Count; i++)
                                {
                                    bool hitted = (keyvalue & ManiaStage.Columns[i].KeyValue) > 0;
                                    if (ManiaStage.SpecialStyleRight)
                                    {
                                        if (i == 0)
                                            ManiaStage.Columns[ManiaStage.Columns.Count - 1].KeyState = hitted;
                                        else
                                            ManiaStage.Columns[i - 1].KeyState = hitted;
                                    }
                                    else
                                        ManiaStage.Columns[i].KeyState = hitted;
                                }
                                if (speed > 0 && !ModManager.CheckActive(Mods.Autoplay))
                                {
                                    ManiaSpeedSet set = ManiaSpeedSet.Unowned;
                                    if (ReplayScore.version < 20141018)
                                        set |= ManiaSpeedSet.Legacy;

                                    if (SpeedMania.SetSpeed(speed, set))
                                        homMania.ReCalculate(CurrentReplayFrame.time);
                                }
                                pos = new Vector2(-500, -500);
                            }

                            lastButtonState = CurrentReplayFrame.buttonState;
                        }
                        else if (ReplayScore.playMode == PlayModes.OsuMania)
                        {
                            //Force KeyStateLast == KeyState for LNs where there's only one click frame
                            for (int i = 0; i < ManiaStage.Columns.Count; i++)
                                ManiaStage.Columns[i].KeyStateLast = ManiaStage.Columns[i].KeyState;
                            pos = new Vector2(-500, -500);
                        }
                        else if (CurrentReplayFrame.time == ReplayScore.replay[ReplayFrame + 1].time)
                        {
                            pos =
                                new Vector2(CurrentReplayFrame.mouseX,
                                            CurrentReplayFrame.mouseY);
                        }
                        else if (CurrentReplayFrame.time >= AudioEngine.Time)
                        {
                            int p1 = Math.Max(0, ReplayFrame - 1);
                            int p2 = ReplayFrame;

                            pos =
                                Vector2.Lerp(
                                    new Vector2(ReplayScore.replay[p1].mouseX, ReplayScore.replay[p1].mouseY),
                                    new Vector2(ReplayScore.replay[p2].mouseX, ReplayScore.replay[p2].mouseY),
                                    ReplayScore.replay[p2].time == ReplayScore.replay[p1].time
                                        ? 0
                                        :
                                            Math.Max(0,
                                                     (1 -
                                                      (float)
                                                      (ReplayScore.replay[p2].time - AudioEngine.Time) /
                                                      (ReplayScore.replay[p2].time -
                                                       ReplayScore.replay[p1].time))));
                        }
                        else
                        {
                            int p1 = ReplayFrame;
                            int p2 = Math.Min(count - 1, ReplayFrame + 1);

                            pos =
                                Vector2.Lerp(
                                    new Vector2(ReplayScore.replay[p1].mouseX, ReplayScore.replay[p1].mouseY),
                                    new Vector2(ReplayScore.replay[p2].mouseX, ReplayScore.replay[p2].mouseY),
                                    ReplayScore.replay[p2].time == ReplayScore.replay[p1].time
                                        ? 0
                                        :
                                            Math.Max(0,
                                                     1 -
                                                     ((float)
                                                      (ReplayScore.replay[p2].time - AudioEngine.Time) /
                                                      (ReplayScore.replay[p2].time -
                                                       ReplayScore.replay[p1].time))));
                        }

                        ReplayGamefieldCursor = pos;
                        CursorPosition = GameBase.GamefieldToDisplay(pos);
                        CursorPoint = new Point((int)CursorPosition.X, (int)CursorPosition.Y);

                        Player.Playing = false;
                    }
                    else if (ReplayStreaming)
                    {
                        //handle buffering or failing in the case of spectator playback.
                        Player.Playing = false;

                        if (ReplayToEnd)
                        {
                            ScorableFrame = true;

                            if (Player.Paused && !Player.Failed)
                            {
                                Buffering = false;
                            }

                            if (Player.ReplayBufferText != null && Player.Failed)
                                Player.ReplayBufferText.Text = LocalisationManager.GetString(OsuString.InputManager_HostFailed);
                        }
                        else
                        {
                            Buffering = true;

                            float percent;

                            if (count == 0)
                                percent = 0;
                            else
                                percent =
                                    OsuMathHelper.Clamp(
                                        (float)(ReplayScore.replay[count - 1].time - AudioEngine.Time) / 10, 0, 100);

                            if (Player.ReplayBufferText != null)
                            {
                                string text = null;
                                switch (StreamingManager.LastAction)
                                {
                                    case ReplayAction.Fail:
                                        text += LocalisationManager.GetString(OsuString.InputManager_HostFailed);
                                        StreamingManager.WaitingOnHost();
                                        break;
                                    case ReplayAction.SongSelect:
                                        text += LocalisationManager.GetString(OsuString.InputManager_HostIsSelectingASong);
                                        StreamingManager.WaitingOnHost();
                                        break;
                                    case ReplayAction.Pause:
                                        text += LocalisationManager.GetString(OsuString.InputManager_HostPaused);
                                        break;
                                }

                                if (text == null)
                                    text = string.Format(LocalisationManager.GetString(OsuString.InputManager_Buffering), percent);

                                Player.ReplayBufferText.Text = text;
                            }
                        }
                    }
                }
            }

            return runningSlow;
        }

        private static void UpdateKeyboard(List<Keys> keys)
        {
            if (keys == null) keys = new List<Keys>();

            List<Keys> newKeys = new List<Keys>(keys);

            KeyboardHandler.ControlPressed = (keyboard.IsKeyDown(Keys.LeftControl) ||
                                                  keyboard.IsKeyDown(Keys.RightControl));
            KeyboardHandler.AltPressed = (keyboard.IsKeyDown(Keys.LeftAlt) ||
                                          keyboard.IsKeyDown(Keys.RightAlt));
            KeyboardHandler.ShiftPressed = (keyboard.IsKeyDown(Keys.LeftShift) ||
                                            keyboard.IsKeyDown(Keys.RightShift));

            if (keys == null || keys.Count == 0 || !activeLastFrame)
                keyboardRepeatTickCurrent = -120;

            bool isFunctionKey = KeyboardHandler.ControlPressed || KeyboardHandler.AltPressed;

            //handle key releases. note that this currently fires always when TextInputControls have focus.
            foreach (Keys k in PressedKeys)
                if (!keys.Contains(k))
                {
                    LastActionTime = GameBase.Time;
                    KeyboardHandler.DoKeyReleased(k);
                }

            TextInputControl tic = GetActiveTextInputControl();
            if (tic != null && tic.AcceptText)
            {
                bool allow = false;
                //If allow is set to true, game code receives the keystroke (even though a textbox is active).

                if (keys.Count >= 2 && keys.Contains(Keys.LeftControl) && keys.Contains(Keys.RightAlt))
                    //This is an AltGr combination - game shouldn't handle.
                    allow = false;
                else if (keys.Count > 1 && (keys.Contains(Keys.LeftAlt) || keys.Contains(Keys.LeftControl) || keys.Contains(Keys.RightAlt) || keys.Contains(Keys.RightControl)))
                    //Handle the case where we are activating a key shortcut combination...
                    allow = true;
                else if (keys.Count == 1 && BindingManager.CheckKey(keys[0], Bindings.BossKey))
                    //always allow boss key
                    allow = true;
                else
                {
                    for (int i = 0; i < keys.Count; i++)
                    {
                        Keys k = keys[i];
                        if (k >= JoyKey.Button0)
                        {
                            allow = true;
                            continue;
                        }
                        switch (k)
                        {
                            case Keys.Left:
                            case Keys.Right:
                                allow = tic.SuppressLeftRightKeys;
                                break;
                            case Keys.LeftAlt:
                            case Keys.RightAlt:
                            case Keys.LeftControl:
                            case Keys.RightControl:
                                allow = true;
                                break;
                            case Keys.Escape:
                            case Keys.Up:
                            case Keys.Down:
                            case Keys.Enter:
                            case Keys.Delete:
                            case Keys.PageDown:
                            case Keys.PageUp:
                            case Keys.Tab:
                                allow = true;
                                continue;
                            default:
                                string ks = k.ToString();
                                if (ks[0] == 'F')
                                {
                                    ks = ks.Substring(1);
                                    int test;
                                    if (Int32.TryParse(ks, out test))
                                    {
                                        allow = true;
                                        isFunctionKey = true;
                                        continue;
                                    }
                                }

                                keys.Remove(k);
                                i--;
                                continue;
                        }
                    }
                }

                if (!allow)
                {
                    keyboardRepeatTickCurrent = -120;
                    PressedKeys = newKeys;
                    return;
                }
            }

            for (int i = 0; i < keys.Count; i++)
            {
                Keys k = keys[i];

                bool bFound = false;

                //is null possibly on first frame/update of game.
                if (PressedKeys != null)
                {
                    for (int j = 0; j < PressedKeys.Count; j++)
                        if (k == PressedKeys[j])
                        {
                            bFound = true;
                            break;
                        }
                }

                if (GameBase.FadeState != FadeStates.FadeOut)
                {
                    if (!bFound)
                    {
                        mouseRepeatTickCurrent = -100;
                        BanchoClient.Activity();

                        /* Order of handling keys should be something like this:
                         * 1. Universal shortcuts (take priority over everything).
                         * 2. Active TextInputControl.
                         * 3. The rest
                         */

                        bool handled = false;

                        handled = GameBase.OnKeyPressed(null, k);

                        //handle non-omniscent text boxes first (could be inside a dialog)
                        if (!handled && tic != null && tic.pTextBox != null) handled = tic.pTextBox.HandleKey(null, k, true);

                        if (!handled && GameBase.ActiveDialog != null)
                        {
                            handled = GameBase.ActiveDialog.HandleKey(k);
                            //dialog ALWAYS handles the key (it has superduper priority)
                            handled = true;
                        }

                        if (!handled) if (GameBase.Volume != null) handled = GameBase.Volume.KeyboardHandler_OnKeyRepeat(null, k, true);
                        if (GameBase.MenuActive) handled = true;

                        if (!handled) handled = ChatEngine.GameBase_OnKeyRepeat(null, k, true);
                        if (!handled && GameBase.Options.PoppedOut) handled = GameBase.Options.KeyboardHandler_OnKeyRepeat(null, k, true);

                        if (!ChatEngine.IsVisible || isFunctionKey)
                            if (!handled)
                            {
                                LastActionTime = GameBase.Time;
                                KeyboardHandler.DoKeyPressed(k);
                            }
                    }
                    else
                    {
                        bool bNotFound = true;
                        for (int j = 0; j < PressedKeys.Count; j++)
                            if (k == PressedKeys[j])
                            {
                                bNotFound = false;
                                break;
                            }
                    }
                }
            }

            int countRealKeys = 0;
            for (int j = 0; j < PressedKeys.Count; j++)
            {
                switch (PressedKeys[j])
                {
                    case Keys.LeftAlt:
                    case Keys.RightAlt:
                    case Keys.LeftShift:
                    case Keys.RightShift:
                    case Keys.LeftControl:
                    case Keys.RightControl:
                        break;
                    default:
                        countRealKeys++;
                        break;
                }
            }

            if (countRealKeys > 0)
            {
                //Handle key repeats.
                if (keyboardRepeatTickCurrent >= REPEAT_TICK_RATE)
                {
                    keyboardRepeatTickCurrent -= REPEAT_TICK_RATE;

                    if (PressedKeys != null)
                    {
                        foreach (Keys k in keys)
                        {
                            bool bFound = false;

                            foreach (Keys k2 in PressedKeys)
                                if (k == k2)
                                {
                                    bFound = true;
                                    break;
                                }

                            if (k == Keys.Attn || k == Keys.Zoom) continue;

                            bool handled = false;

                            if (!handled) if (tic != null && tic.pTextBox != null) handled = tic.pTextBox.HandleKey(null, k, false);
                            if (!handled) if (GameBase.Volume != null) handled = GameBase.Volume.KeyboardHandler_OnKeyRepeat(null, k, false);
                            if (!handled) handled = ChatEngine.GameBase_OnKeyRepeat(null, k, false);
                            if (!handled && GameBase.Options.PoppedOut) handled = GameBase.Options.KeyboardHandler_OnKeyRepeat(null, k, false);
                            if (!handled && GameBase.ActiveDialog != null)
                            {
                                //dialog ALWAYS handles the key (it has superduper priority)
                                handled = true;
                            }
                            if (!ChatEngine.IsVisible)
                            {
                                if (!handled)
                                {
                                    LastActionTime = GameBase.Time;
                                    KeyboardHandler.DoOnKeyRepeat(k);
                                }
                            }
                        }
                    }
                }

                keyboardRepeatTickCurrent += GameBase.ElapsedMilliseconds;
            }
            else
                keyboardRepeatTickCurrent = -120;

            PressedKeys = newKeys;
        }

        /// <summary>
        /// Use this method if you need drag procedure to finish before MouseDown is false.
        /// </summary>
        internal static void DragFinish()
        {
            if (!IsDragging) return;

            MouseManager.MouseDown = false;
            MouseManager.ClickPosition = InputManager.CursorPosition;
            IsDragging = false;

            MouseManager.DoOnEndDrag();
        }

        internal static void ResetButtons()
        {
            leftButton = ButtonState.Released;
            leftButton1i = false;
            leftButton1 = false;
            leftButton2i = false;
            leftButton2 = false;

            rightButton = ButtonState.Released;
            rightButton1i = false;
            rightButton1 = false;
            rightButton2i = false;
            rightButton2 = false;
        }

        internal static int ManiaKeyValue()
        {
            int val = 0;
            //notice: encode n+1 special column as normal layerout(special on left)
            //decode in Update()
            for (int i = 0; i < ManiaStage.Columns.Count; i++)
            {
                if (ManiaStage.Columns[i].KeyState)
                {
                    if (Player.Instance.hitObjectManager is HitObjectManagerMania && ManiaStage.SpecialStyleRight)
                    {
                        if (i == ManiaStage.Columns.Count - 1)
                            val += ManiaStage.Columns[0].KeyValue;
                        else
                            val += ManiaStage.Columns[i + 1].KeyValue;
                    }
                    else
                        val += ManiaStage.Columns[i].KeyValue;
                }
            }
            return val;
        }

        internal static void UpdateButtonsLocalPlayer()
        {
            leftButton = MouseManager.MouseLeft;
            leftButton1i = leftButton == ButtonState.Pressed && (!MouseButtonInstantRelease || !leftButtonLastBool);
            leftButton1 = leftButton == ButtonState.Pressed;
            leftButton2i = false;//leftButton == ButtonState.Pressed && (!MouseButtonInstantRelease || !leftButtonLastBool);
            leftButton2 = false;//leftButton == ButtonState.Pressed;

            rightButton = MouseManager.MouseRight;
            rightButton1i = rightButton == ButtonState.Pressed && (!MouseButtonInstantRelease || !rightButtonLastBool);
            rightButton1 = rightButton == ButtonState.Pressed;
            rightButton2i = false;//rightButton == ButtonState.Pressed && (!MouseButtonInstantRelease || !rightButtonLastBool);
            rightButton2 = false;//rightButton == ButtonState.Pressed;
        }

        /// <summary>
        /// Updates mouse button states from keyboard bindings.
        /// Internall we still handle keypresses as left/right mouse button presses.
        /// </summary>
        private static void MouseViaKeyboardControls()
        {
            if (GameBase.Mode != OsuModes.Play)
                return;

            bool keyLeft1;
            bool keyLeft2;
            bool keyRight1;
            bool keyRight2;

            //Assign the base button states (don't care about whether the button was hit this frame, just whether it is held down).
            Keys key = Keys.None;

            if ((key = BindingManager.GetPlayKey(PlayKey.Left1)) != Keys.None)
            {
                leftButton1 |= keyLeft1 = (keyboard.IsKeyDown(key) || JoystickHandler.IsKeyDown(key));
                leftButton1i |= keyLeft1 && (!MouseButtonInstantRelease || !(keyboardLast.IsKeyDown(key) || JoystickHandler.IsLastKeyDown(key)));
            }


            if ((key = BindingManager.GetPlayKey(PlayKey.Left2)) != Keys.None)
            {
                leftButton2 |= keyLeft2 = (keyboard.IsKeyDown(key) || JoystickHandler.IsKeyDown(key));
                leftButton2i |= keyLeft2 && (!MouseButtonInstantRelease || !(keyboardLast.IsKeyDown(key) || JoystickHandler.IsLastKeyDown(key)));
            }

            if ((key = BindingManager.GetPlayKey(PlayKey.Right1)) != Keys.None)
            {
                rightButton1 |= keyRight1 = (keyboard.IsKeyDown(key) || JoystickHandler.IsKeyDown(key));
                rightButton1i |= keyRight1 && (!MouseButtonInstantRelease || !(keyboardLast.IsKeyDown(key) || JoystickHandler.IsLastKeyDown(key)));
            }

            if ((key = BindingManager.GetPlayKey(PlayKey.Right2)) != Keys.None)
            {
                key = BindingManager.GetPlayKey(PlayKey.Right2);
                rightButton2 |= keyRight2 = (keyboard.IsKeyDown(key) || JoystickHandler.IsKeyDown(key));
                rightButton2i |= keyRight2 && (!MouseButtonInstantRelease || !(keyboardLast.IsKeyDown(key) || JoystickHandler.IsLastKeyDown(key)));
            }

            if ((leftButton2i || leftButton1i) && !ReplayMode && !(Player.Paused && ChatEngine.IsVisible))
                leftButton = ButtonState.Pressed;
            if ((rightButton1i || rightButton2i) && !ReplayMode && !(Player.Paused && ChatEngine.IsVisible))
                rightButton = ButtonState.Pressed;
        }

        static Vector2 lastTrailPosition = -Vector2.One;
        internal static pSprite s_Cursor2;
        static int lastTrailTime = 0;

        static List<pSprite> trailSegments = new List<pSprite>();

        internal static bool IsReplayMovement
        {
            get
            {
                return (ReplayMode && ReplayCursorFollow) || Player.Relaxing2;
            }
        }

        internal static void UpdateCursorTrail()
        {
            if (s_Cursor == null || s_Cursor2 == null)
                return;

            if (lastTrailPosition == -Vector2.One ||
                (IsReplayMovement && (!MouseManager.PosInWindow(lastTrailPosition, -100) ||
                                      !MouseManager.PosInWindow(s_Cursor.Position, -100))))
            {
                lastTrailPosition = s_Cursor.Position;
            }

            s_Cursor2.Position = s_Cursor.Position;
            s_Cursor2.Alpha = s_Cursor.Alpha;

            float adjustAmount = (float)GameBase.ElapsedMilliseconds / 300;
            foreach (pSprite s in trailSegments)
            {
                s.Alpha = Math.Max(0, s.Alpha - adjustAmount);
            }

            // We remove all leading elements with an alpha value <= 0
            int toRemove = 0;
            for (; toRemove < trailSegments.Count && trailSegments[toRemove].Alpha <= 0; ++toRemove)
            {
                trailSegments[toRemove].AlwaysDraw = false;
            }

            trailSegments.RemoveRange(0, toRemove);

            if (!s_Cursor.IsVisible) return;

            pTexture trailTexture = SkinManager.t_cursortrail;

            if (trailTexture == null || trailTexture.Width < 5) return;

            float acceptedWidth = (trailTexture.Width * 0.625f * GameBase.WindowRatio * s_Cursor.Scale) / 2.5f;

            if (s_Cursor2.Texture != null)
            {
                if (Vector2.Distance(lastTrailPosition, s_Cursor.Position) < acceptedWidth)
                    return;

                // Only take intermediate positions of the current input device if we are not in a replay or using a mode that controls the cursor.
                List<Vector2> positions = IsReplayMovement ? new List<Vector2>() : CurrentCursorHandler.IntermediatePositions;

                positions.Add(s_Cursor.Position);

                for (int j = 0; j < positions.Count; j++)
                {
                    float distance = Vector2.Distance(lastTrailPosition, positions[j]);
                    float steps = distance / acceptedWidth;

                    for (int i = 0; i < (int)steps; i++)
                    {
                        pSprite trail = new pSprite(trailTexture, Fields.NativeStandardScale,
                                (SkinManager.CurrentCursor.CursorCentre ? Origins.Centre : Origins.TopLeft),
                                Clocks.Game, Vector2.Lerp(lastTrailPosition, positions[j], i / steps), (s_Cursor2 != null ? s_Cursor2.Depth : s_Cursor.Depth) - 0.001F,
                                true,
                                s_Cursor.drawColour);

                        trail.Additive = true;
                        trail.Scale = s_Cursor.Scale;
                        trail.VectorScale = s_Cursor.VectorScale;

                        if (SkinManager.CurrentCursor.CursorTrailRotate) trail.Rotation = s_Cursor.Rotation;
                        GameBase.spriteManagerCursor.Add(trail);
                        trailSegments.Add(trail);
                    }

                    if (steps > 0)
                        lastTrailPosition = Vector2.Lerp(lastTrailPosition, positions[j], (int)steps / steps);
                }

                positions.RemoveAt(positions.Count - 1);
                lastTrailTime = GameBase.Time;
            }
            else
            {
                if (!GameBase.SixtyFramesPerSecondFrame && GameBase.Time - lastTrailTime < GameBase.SIXTY_FRAME_TIME)
                    return;

                pSprite trail =
                    new pSprite(trailTexture, Fields.NativeStandardScale,
                                (SkinManager.CurrentCursor.CursorCentre ? Origins.Centre : Origins.TopLeft),
                                Clocks.Game, s_Cursor.Position, s_Cursor.Depth - 0.001F,
                                false,
                                s_Cursor.drawColour);
                trail.FadeOut(150);

                lastTrailTime = GameBase.Time;

                if (trail != null)
                {
                    trail.Scale = s_Cursor.Scale;
                    trail.VectorScale = s_Cursor.VectorScale;

                    if (SkinManager.CurrentCursor.CursorTrailRotate)
                        trail.Rotation = s_Cursor.Rotation;
                    GameBase.spriteManagerCursor.Add(trail);
                }
            }
        }

        public static InputHandler GetHandler(Type handlerType)
        {
            return inputHandlers.Find(h => h.GetType() == handlerType);
        }

        private static InputHandlerComparer inputHandlerComparer = new InputHandlerComparer();

        public static bool AddHandler(InputHandler handler)
        {
            try
            {
                if (handler.Initialize())
                {
                    int index = inputHandlers.BinarySearch(handler, inputHandlerComparer);
                    if (index < 0)
                    {
                        index = ~index;
                    }

                    inputHandlers.Insert(index, handler);
                    return true;
                }
            }
            catch
            {
            }

            return false;
        }

        internal static List<TextInputControl> TextInputControls = new List<TextInputControl>();

        /// <summary>
        /// Locally stored value as skin may be overridden.
        /// </summary>
        public static bool CursorExpand;
        public static int LastActionTime;
        private static List<Keys> keys = new List<Keys>();
        public static bool LastClickHandled;

        internal static bReplayFrame CurrentReplayFrame;
        private static bool cursorIsExpanded;
        internal static Vector2 CursorPositionDelta;
        internal static Vector2 CursorPositionLast;

        internal static void UpdateCursorSize()
        {
            //CS range is generally 2-7. default (1.0x) cursor feels best at CS4 in testing.
            float mapScale = GameBase.Mode == OsuModes.Play && ConfigManager.sAutomaticCursorSizing.Value ?
                1f - 0.7f * (float)(HitObjectManager.ApplyModsToDifficulty(BeatmapManager.Current.DifficultyCircleSize, 1.3, ModManager.ModStatus) - 4) / 5f : 1;

            if (s_Cursor != null) s_Cursor.VectorScale = mapScale * Vector2.One * (float)ConfigManager.sCursorSize.Value;
            if (s_Cursor2 != null) s_Cursor2.VectorScale = s_Cursor.VectorScale;
        }

        internal static void AddTextInputControl(TextInputControl t)
        {
            if (!TextInputControls.Remove(t))
                GameBase.Form.Controls.Add(t);
            TextInputControls.Add(t);

#if DEBUG
            t.BringToFront();
#else
            t.SendToBack();
#endif
            CheckTextInputFocus(true);
        }

        internal static void RemoveTextInputControl(TextInputControl t)
        {
            if (t == TextInput) return;

            GameBase.Form.Controls.Remove(t);
            TextInputControls.Remove(t);

            CheckTextInputFocus(true);
        }

        internal static TextInputControl CheckTextInputFocus(bool force)
        {
            TextInputControl tic = GetActiveTextInputControl();
            if (!GameBase.MenuActive && GameBase.IsActive && (!tic.Focused || force))
                tic.Focus();
            return tic;
        }

        internal static TextInputControl GetActiveTextInputControl()
        {
            if (TextInputControls.Count == 0) return null;
            int i = TextInputControls.Count - 1;
            while (i >= 0)
            {
                TextInputControl tic = TextInputControls[i];
                if (tic.AcceptText)
                    return tic;

                i--;
            }

            return TextInputControls[0];
        }

        /// <summary>
        /// Checks whether we are running on a windows 8.1 host and trying to use a non-1.0x mouse multiplier.
        /// </summary>
        /// <returns>true if we had to make changes to settings</returns>
        internal static bool CheckWindows81Mouse()
        {
            if (ConfigManager.sMouseSpeed != 1.0 && !ConfigManager.sRawInput && GameBase.IsWindows81)
            {
                ConfigManager.sRawInput.Value = true;
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Options_Input_RawInputRequired), Color.OrangeRed, 10000);
                return true;
            }

            return false;
        }

        internal static void ResetPosition(Vector2 pos)
        {
            MouseManager.SetNativePosition(new Point((int)Math.Round(pos.X), (int)Math.Round(pos.Y)));
            CursorPosition = pos;

            CursorPositionLast = CursorPosition;

            // We want to make sure _all_ handlers have their position updated, hence we set the current handler to null.
            CurrentCursorHandler = null;
            MouseManager.ResetPosition();
        }

        internal static void SetCursorHandlerPositions(Vector2 pos)
        {
            foreach (InputHandler h in inputHandlers)
            {
                // We don't want to set the position of the currently active cursor handler since it is the one controlling out position
                // in the first place. This is avoiding potential overhead with resetting its position. (Such as forcfully moving the windows cursor.)
                if (h != CurrentCursorHandler && h is CursorInputHandler)
                {
                    ((CursorInputHandler)h).SetPosition(pos);
                }
            }
        }

        private static List<InputTarget> Targets = new List<InputTarget>();

        internal static void InitializeTargets()
        {
            GameBase.OnModeChange += delegate
            {
                while (modeUnbindQueue.Count != 0)
                    modeUnbindQueue.Dequeue()();
            };

            List<InputTargetType> targetTypes = new List<InputTargetType>((InputTargetType[])Enum.GetValues(typeof(InputTargetType)));
            targetTypes.Reverse();

            foreach (InputTargetType type in targetTypes)
                Targets.Add(new InputTarget(type));
        }

        internal static void TriggerEvent(InputEventType eventType)
        {
            foreach (InputTarget t in Targets)
                if (t.Invoke(eventType))
                    break;
        }

        internal static void Bind(InputEventType eventType, InputHandlerDelegate del, InputTargetType target = InputTargetType.Default, BindLifetime lifetime = BindLifetime.Mode)
        {
            Targets.Find(t => t.Type == target).Bind(eventType, del);
            if (lifetime == BindLifetime.Mode)
                modeUnbindQueue.Enqueue(delegate { Targets.Find(t => t.Type == target).Unbind(eventType, del); });
        }

        internal static void Unbind(InputEventType eventType, InputHandlerDelegate del, InputTargetType target = InputTargetType.Default)
        {
            Targets.Find(t => t.Type == target).Unbind(eventType, del);
        }

        internal static void SetTargetState(InputTargetType target, bool active)
        {
            Targets.Find(t => t.Type == target).Enabled = active;
        }

        internal static void ChangeTargetPriority(InputTargetType target, InputTargetType handleBefore)
        {
            if (target == handleBefore) return;

            InputTarget t = Targets.Find(t1 => t1.Type == target);

            Targets.Remove(t);
            Targets.Insert(Targets.FindIndex(t2 => t2.Type == handleBefore) + 1, t); //remember, this is highest to lowest
        }

        static Queue<VoidDelegate> modeUnbindQueue = new Queue<VoidDelegate>();
    }

    public delegate bool InputHandlerDelegate(object sender, EventArgs e);

    internal enum InputEventType
    {
        OnMouseWheelDown,
        OnMouseWheelUp,
        OnEndDrag,
        OnDrag,
        OnClickRepeat,
        OnDoubleClick,
        OnClickUp,
        OnGamePress,
        OnClick,
    }

    internal enum BindLifetime
    {
        Mode,
        Permanent,
        Manual
    }

    internal enum InputTargetType
    {
        Lowest,
        Volume,
        Default,
        Options,
        Chat,
        Dialog,
        Online,
        AltOverrides,
        Highest
    }

    internal enum ReplaySpeed
    {
        Normal,
        Half,
        Double,
        Triple
    }
}
