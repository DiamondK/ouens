using System;
using System.Collections.Generic;
using osu.Input.Handlers;

namespace osu.Input
{
    internal class InputTarget
    {
        internal InputTargetType Type;
        internal bool Enabled;

        internal InputTarget(InputTargetType type, bool enabled = true)
        {
            Type = type;
            Enabled = enabled;
        }

        internal void Bind(InputEventType eventType, InputHandlerDelegate del)
        {
            InputEventList list = getEventList(eventType);
            if (list != null) list.BindList.Add(del);
        }

        internal void Unbind(InputEventType eventType, InputHandlerDelegate del)
        {
            InputEventList list = getEventList(eventType);
            if (list != null) list.BindList.Remove(del);
        }

        internal bool Invoke(InputEventType eventType)
        {
            if (!Enabled) return false;

            InputEventList list = getEventList(eventType);
            if (list == null) return false;

            for (int i = list.BindList.Count - 1; i >= 0; i--)
            {
                Delegate d = list.BindList[i];
                if ((bool)d.DynamicInvoke(null, null))
                    return true;
            }

            return false;
        }

        internal List<InputEventList> eventLists = new List<InputEventList>();
        private InputEventList getEventList(InputEventType eventType)
        {
            InputEventList list = eventLists.Find(h => h.Type == eventType);

            if (list == null)
                eventLists.Add(list = new InputEventList(eventType));

            return list;
        }

        internal class InputEventList
        {
            internal InputEventType Type;
            internal List<InputHandlerDelegate> BindList = new List<InputHandlerDelegate>();

            public InputEventList(InputEventType type)
            {
                Type = type;
            }
        }
    }
}