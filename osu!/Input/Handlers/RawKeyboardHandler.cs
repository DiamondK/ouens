﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Configuration;
using osu.GameModes.Play;
using osu.Graphics.Notifications;
using osu_common.Helpers;
using osu.Helpers;

namespace osu.Input.Handlers
{
    internal unsafe class RawKeyboardHandler : WndProcInputHandler
    {
        private List<RawKeyboard> pendingChanges = new List<RawKeyboard>();

        internal override bool Initialize()
        {
            bool success = false;

            try
            {
                RawInputDevice r = new RawInputDevice();
                WindowsGameWindow w = GameBase.Instance.Window as WindowsGameWindow;

                r.UsagePage = HIDUsagePage.Generic;
                r.Usage = HIDUsage.Keyboard;
                r.Flags = RawInputDeviceFlags.InputSink;
                r.WindowHandle = w.WindowsForm.Handle;

                success = Native.RegisterRawInputDevices(new[] { r }, 1, sizeof(RawInputDevice));

                if (!success)
                {
                    ConfigManager.sRawInput = false;
                    return false;
                }
            }
            catch { }

            success = base.Initialize();

            if (success)
            {
                OnKeyboard += handler;
            }

            return success;
        }

        internal override void Dispose()
        {
            OnKeyboard -= handler;
            base.Dispose();
        }

        private void handler(RawInput data)
        {
            lock (this)
            {
                if (pendingChanges.Count > 1000) pendingChanges.Clear();
                pendingChanges.Add(data.Keyboard);
            }
        }

        internal override void UpdateInput(bool isActive)
        {
        }

        /// <summary>
        /// As of now this InputHandler writes directly to the static state, hence it should not be processed in the actual input pipeline.
        /// </summary>
        internal override bool IsActive
        {
            get
            {
                return false;
            }
        }

        internal override int Priority
        {
            get
            {
                return 2;
            }
        }

        internal RawKeyboard[] GetChangesList()
        {
            lock (this)
            {
                RawKeyboard[] changes = pendingChanges.ToArray();
                pendingChanges.Clear();
                return changes;
            }
        }
    }
}