﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using osu.Configuration;
using osu.Helpers;
using osu_common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;

namespace osu.Input.Handlers
{
    class MouseHandler : InputHandler, CursorInputHandler
    {
        private bool wasActive = false;
        private Vector2 position = Vector2.One;
        private Point previousNativeMousePosition;

        internal override bool Initialize()
        {
            previousNativeMousePosition = MouseManager.GetNativePosition();
            return true;
        }

        internal override void Dispose()
        {
        }

        internal override void UpdateInput(bool isActive)
        {
            Point nativeMousePosition = MouseManager.GetNativePosition();

            if (!isActive)
            {
                wasActive = false;
                position.X += nativeMousePosition.X - previousNativeMousePosition.X;
                position.Y += nativeMousePosition.Y - previousNativeMousePosition.Y;

                previousNativeMousePosition = nativeMousePosition;
                return;
            }

            previousNativeMousePosition = nativeMousePosition;

            if (!wasActive)
            {
                if (GameBase.TotalFramesRendered == 0 || !MouseManager.PosInWindow(nativeMousePosition))
                {
                    position.X = nativeMousePosition.X;
                    position.Y = nativeMousePosition.Y;
                }
                else
                {
                    Point pixelPosition = new Point((int)Math.Round(position.X), (int)Math.Round(position.Y));

                    MouseManager.SetNativePosition(pixelPosition, true);
                    nativeMousePosition = MouseManager.GetNativePosition();
                }
            }

            wasActive = true;

            if (ConfigManager.sMouseSpeed != 1 && !MouseManager.showMouse && !ConfigManager.sRawInput)
            {
                Point pixelPosition = new Point((int)Math.Round(position.X), (int)Math.Round(position.Y));

                if (MouseManager.PosInWindow(new Point(nativeMousePosition.X, nativeMousePosition.Y)))
                {
                    position.X += (nativeMousePosition.X - pixelPosition.X) * (float)ConfigManager.sMouseSpeed;
                    position.Y += (nativeMousePosition.Y - pixelPosition.Y) * (float)ConfigManager.sMouseSpeed;

                    pixelPosition = new Point((int)Math.Round(position.X), (int)Math.Round(position.Y));
                }
                else
                {
                    position.X = nativeMousePosition.X;
                    position.Y = nativeMousePosition.Y;

                    //the input stream is sometimes hooked by crappy mouse drivers. so we want to equalize
                    //both changes and non changes in sensitivity mode.
                    pixelPosition = new Point(nativeMousePosition.X, nativeMousePosition.Y);
                }

                MouseManager.SetNativePosition(pixelPosition);
                nativeMousePosition = MouseManager.GetNativePosition();

                // Windows might restrict our mouse cursor's movement. In this case we adjust to windows' position.
                // (This is relevant for cursor clamping at monitor boundaries in full-screen and clamping to the osu! window in windowed mode)
                if (pixelPosition.X != nativeMousePosition.X)
                {
                    Debug.Print("X position disparity detected.");
                    position.X += nativeMousePosition.X - pixelPosition.X;
                }

                if (pixelPosition.Y != nativeMousePosition.Y)
                {
                    Debug.Print("Y position disparity detected.");
                    position.Y += nativeMousePosition.Y - pixelPosition.Y;
                }
            }
            else
            {
                position.X = nativeMousePosition.X;
                position.Y = nativeMousePosition.Y;
            }
        }

        public void SetPosition(Vector2 pos)
        {
            position = pos;

            // This forces a windows cursor position reset which is important for non-raw input mouse to not snap back.
            wasActive = false;
        }

        public Vector2 Position
        {
            get
            {
                return position;
            }
        }

        public ButtonState? Left
        {
            get
            {
                return null;
            }
        }

        public ButtonState? Right
        {
            get
            {
                return null;
            }
        }

        public ButtonState? Middle
        {
            get
            {
                return null;
            }
        }

        public ButtonState? Back
        {
            get
            {
                return null;
            }
        }

        public ButtonState? Forward
        {
            get
            {
                return null;
            }
        }

        public List<Vector2> IntermediatePositions
        {
            get
            {
                return new List<Vector2>();
            }
        }

        /// <summary>
        /// This input handler is always active, handling the cursor position if no other input handler does.
        /// </summary>
        internal override bool IsActive
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Lowest priority. We want the normal mouse handler to only kick in if all other handlers don't do anything.
        /// </summary>
        internal override int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
