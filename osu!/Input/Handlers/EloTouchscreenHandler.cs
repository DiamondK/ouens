﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using osu.Configuration;
using osu.GameModes.Play;
using osu.Graphics.Notifications;
using System.Collections.Generic;

namespace osu.Input.Handlers
{
    internal unsafe class EloTouchscreenHandler : InputHandler, CursorInputHandler
    {
        #region GetPointsCode enum

        private enum GetPointsCode
        {
            ReturnImmediately = 1,
            ReturnOnTouch,
            ReturnOnUntouch,
            ReturnOnNextValidTouch
        } ;

        #endregion

        #region GetPointsStatus enum

        private enum GetPointsStatus
        {
            InitialTouch = 1,
            StreamTouch = 2,
            UnTouch = 4
        } ;

        #endregion

        private const int MAX_SUPPORTED_SCR = 32;
        private bool Calibrated;
        private bool isActive = false;

        private bool left;
        private bool pendingTouch;
        private int pendingX;
        private int pendingY;

        private Point ratioBase1;
        private Point ratioBase1S;
        private Point ratioBase2;
        private Point ratioBase2S;
        private Vector2 rawOffset;
        private Vector2 rawRatio;
        private ButtonState state;

        internal override bool Initialize()
        {
            if (ConfigManager.sTouchCalibration == "")
                ResetCalibration();
            else
            {
                string[] split = ConfigManager.sTouchCalibration.Split(':');
                rawRatio = new Vector2(float.Parse(split[0]), float.Parse(split[1]));
                rawOffset = new Vector2(float.Parse(split[2]), float.Parse(split[3]));
                Calibrated = true;
            }
            
            Thread t = new Thread(checkThread);
            t.IsBackground = true;
            t.Start();

            return true;
        }

        internal override void Dispose()
        {
        }

        internal override void OnResolutionChange()
        {
            ResetCalibration();

            base.OnResolutionChange();
        }

        public void SetPosition(Vector2 pos)
        {

        }

        public Vector2 Position
        {
            get
            {
                return new Vector2(pendingX, pendingY);
            }
        }

        private ButtonState leftState = ButtonState.Released;
        public ButtonState? Left
        {
            get
            {
                return leftState;
            }
        }

        private ButtonState rightState = ButtonState.Released;
        public ButtonState? Right
        {
            get
            {
                return rightState;
            }
        }

        public ButtonState? Middle
        {
            get
            {
                return null;
            }
        }

        public ButtonState? Back
        {
            get
            {
                return null;
            }
        }

        public ButtonState? Forward
        {
            get
            {
                return null;
            }
        }

        public List<Vector2> IntermediatePositions
        {
            get
            {
                return new List<Vector2>();
            }
        }

        internal override bool IsActive
        {
            get
            {
                bool result = ConfigManager.sEloTouchscreen && Calibrated &&
                    (pendingTouch || state == ButtonState.Pressed);

                // We want to stay active for once additional check after being flagged as inactive
                if (result == false && isActive)
                {
                    result = true;
                    isActive = false;
                }
                else if (result == true)
                {
                    isActive = true;
                }

                return result && Game.IsActive && !MouseManager.showMouse;
            }
        }

        internal override int Priority
        {
            get 
            {
                return 2;
            }
        }

        internal override void UpdateInput(bool isActive)
        {
            leftState = ButtonState.Released;
            rightState = ButtonState.Released;

            if (pendingTouch)
            {
                left = Player.Paused || InputManager.ReplayMode || GameBase.Mode != OsuModes.Play ? true : !left;

                if (left)
                    leftState = ButtonState.Pressed;
                else
                    rightState = ButtonState.Pressed;

                pendingTouch = false;
            }
            else
            {
                if (left && state == ButtonState.Pressed)
                    leftState = state;
                else if (!left && state == ButtonState.Pressed)
                    rightState = state;
            }
        }

        private void checkThread()
        {
            uint[] monno = new uint[MAX_SUPPORTED_SCR];
            int a;
            EloGetScreenInfo(monno, out a);

            TouchPoint tp = new TouchPoint();

            uint monitor = monno[0];

            bool pressureTouched = false;

            int lastPressure = 0;
            int highestPressure = 0;

            while (true)
            {
                EloGetTouch(&tp, false, GetPointsCode.ReturnOnNextValidTouch, ref monitor);

                if (!Calibrated)
                    HandleCalibration(tp);
                else
                {
                    pendingX = (int) ((tp.X - rawOffset.X)/rawRatio.X);
                    pendingY = (int) ((tp.Y - rawOffset.Y)/rawRatio.Y);

                    if (!pressureTouched)
                    {
                        if (tp.Z > lastPressure)
                        {
                            pressureTouched = true;
                            pendingTouch = true;
                        }
                    }
                    else if (tp.Z <= Math.Max(12, highestPressure - (Player.Playing ? 20 : 100)))
                    {
                        pressureTouched = false;
                        highestPressure = 0;
                    }

                    if (pressureTouched && tp.Z > highestPressure) highestPressure = tp.Z;

                    state = tp.Status == GetPointsStatus.UnTouch ? ButtonState.Released : ButtonState.Pressed;

                    lastPressure = tp.Z;
                }
            }
        }

        internal void ResetCalibration()
        {
            NotificationManager.ShowMessageMassive("Touchscreen Input Enabled - Please calibrate by touching top-left and bottom-right corners", 20000);
            Calibrated = false;
            ratioBase1 = Point.Zero;
            ratioBase2 = Point.Zero;
            ConfigManager.sTouchCalibration = string.Empty;
        }

        private void HandleCalibration(TouchPoint tp)
        {
            Thread.Sleep(50);
            Point nativeMousePosition = MouseManager.GetNativePosition();

            if (ratioBase1.X == 0)
            {
                if (nativeMousePosition.X < 200)
                {
                    ratioBase1.X = tp.X;
                    ratioBase1S.X = nativeMousePosition.X;
                }
            }
            else
            {
                if (nativeMousePosition.X > 600)
                {
                    ratioBase2.X = tp.X;
                    ratioBase2S.X = nativeMousePosition.X;
                }
            }

            if (ratioBase1.Y == 0)
            {
                if (nativeMousePosition.Y < 200)
                {
                    ratioBase1.Y = tp.Y;
                    ratioBase1S.Y = nativeMousePosition.Y;
                }
            }
            else
            {
                if (nativeMousePosition.Y > 600)
                {
                    ratioBase2.Y = tp.Y;
                    ratioBase2S.Y = nativeMousePosition.Y;
                }
            }

            if (ratioBase2.X > 0 && ratioBase2.Y > 0)
            {
                Calibrated = true;

                rawRatio.X = (float) (ratioBase2.X - ratioBase1.X)/(ratioBase2S.X - ratioBase1S.X);
                rawRatio.Y = (float) (ratioBase2.Y - ratioBase1.Y)/(ratioBase2S.Y - ratioBase1S.Y);

                rawOffset.X = ratioBase1.X - (ratioBase1S.X*rawRatio.X);
                rawOffset.Y = ratioBase1.Y - (ratioBase1S.Y*rawRatio.Y);

                ConfigManager.sTouchCalibration = rawRatio.X.ToString(GameBase.nfi) + ":" + rawRatio.Y.ToString(GameBase.nfi) + ":" + rawOffset.X.ToString(GameBase.nfi) + ":" + rawOffset.Y.ToString(GameBase.nfi);

                NotificationManager.ShowMessageMassive("Touchscreen Calibration Complete!", 2000);
            }
        }

        [SuppressUnmanagedCodeSecurityAttribute()]
        [DllImport("EloPubIf.dll", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        private static extern int EloGetScreenInfo(uint[] dwMonNo, out int iScrCnt);

        [SuppressUnmanagedCodeSecurityAttribute()]
        [DllImport("EloPubIf.dll", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        private static extern int EloGetTouch(TouchPoint* tpxy, bool xlated, GetPointsCode getCode, ref uint nScrNo);

        #region Nested type: TouchPoint

        [StructLayout(LayoutKind.Sequential)]
        private struct TouchPoint
        {
            public int X;
            public int Y;
            public int Z;
            public GetPointsStatus Status;
        } ;

        #endregion
    }
}