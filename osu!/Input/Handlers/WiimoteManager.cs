﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.Graphics.Notifications;
using osu_common;
using WiimoteLib;

namespace osu.Input.Handlers
{
    internal static class WiimoteManager
    {
        private static int rumbleTimeout;
        internal static WiimoteState State;
        private static TaikoDrumState taikoState;
        private static TaikoDrumState taikoStateLast;
        internal static Wiimote Wiimote;

        internal static bool Initialized
        {
            get { return Wiimote != null; }
        }

        internal static bool Initialize()
        {
            if (!ConfigManager.sWiimote) return false;

            try
            {
                Wiimote = new Wiimote();
                Wiimote.Connect();
            }
            catch
            {
                NotificationManager.ShowMessage(
                    "Could not find Wiimote.  Please check connection then reenable in options.", Color.Red, 8000);
                ConfigManager.sWiimote.Value = false;
                Disconnect();
                return false;
            }

            NotificationManager.ShowMessage("Found Wiimote!", Color.YellowGreen, 6000);
            Wiimote.SetLEDs(true, true, true, true);
            Wiimote.WiimoteChanged += Wiimote_WiimoteChanged;

            return true;
        }

        private static void Wiimote_WiimoteChanged(object sender, WiimoteChangedEventArgs e)
        {
        }

        internal static void Disconnect()
        {
            if (Wiimote != null)
            {
                Wiimote.SetLEDs(false, false, false, false);
                Wiimote.Disconnect();
                Wiimote = null;
            }
        }

        internal static bool GetState()
        {
            if (Wiimote == null)
                return false;

            State = Wiimote.WiimoteState;

            if (rumbleTimeout > 0 && rumbleTimeout <= GameBase.Time)
            {
                rumbleTimeout = -1;
                Wiimote.SetRumble(false);
            }

            return State != null;
        }

        internal static void NextLED()
        {
            throw new NotImplementedException();
        }

        internal static void Rumble(int rumbleTime)
        {
            if (!Initialized) return;

            Wiimote.SetRumble(true);
            rumbleTimeout = GameBase.Time + rumbleTime;
        }

        internal static void Update(List<Keys> keys)
        {
            taikoStateLast = taikoState;

            if (!GetState())
                return;

            if (AudioEngine.SyncNewBeat)
            {
                Wiimote.SetLEDs(AudioEngine.SyncBeat % 4 == 0, AudioEngine.SyncBeat % 4 == 1, AudioEngine.SyncBeat % 4 == 2, AudioEngine.SyncBeat % 4 == 3);
            }

            WiimoteState state = State;

            taikoState = State.TaikoDrumState;

            bool il = (!taikoStateLast.InnerLeft && taikoState.InnerLeft);
            bool ir = (!taikoStateLast.InnerRight && taikoState.InnerRight);
            bool ol = (!taikoStateLast.OuterLeft && taikoState.OuterLeft);
            bool or = (!taikoStateLast.OuterRight && taikoState.OuterRight);

            if (!InputManager.ReplayMode)
            {
                InputManager.leftButton1i |= il;
                InputManager.leftButton2i |= ir;
                InputManager.rightButton1i |= ol;
                InputManager.rightButton2i |= or;

                InputManager.leftButton1 |= taikoState.InnerLeft;
                InputManager.leftButton2 |= taikoState.InnerRight;
                InputManager.rightButton1 |= taikoState.OuterLeft;
                InputManager.rightButton2 |= taikoState.OuterRight;
            }

            switch (GameBase.Mode)
            {
                case OsuModes.Menu:
                    if (il || ir)
                    {
                        if (GameBase.FadeState == FadeStates.Idle)
                        {
                            Player.Mode = PlayModes.Taiko;
                            GameBase.ChangeMode(OsuModes.SelectPlay);
                            AudioEngine.PlaySamplePositional(@"menuhit");
                        }
                    }

                    if (state.ButtonState.A)
                    {
                        if (GameBase.FadeState == FadeStates.Idle)
                        {
                            Player.Mode = PlayModes.Taiko;
                            GameBase.ChangeMode(OsuModes.SelectPlay);
                            AudioEngine.PlaySamplePositional(@"menuhit");
                        }
                    }

                    if (state.ButtonState.B)
                        keys.Add(Keys.Escape);
                    break;
                case OsuModes.SelectPlay:
                    if (il || ir)
                        keys.Add(Keys.Enter);
                    else if (taikoStateLast.OuterLeft || state.ButtonState.Up)
                        keys.Add(Keys.Up);
                    else if (taikoStateLast.OuterRight || state.ButtonState.Down)
                        keys.Add(Keys.Down);
                    if (state.ButtonState.B)
                        keys.Add(Keys.Escape);
                    break;
                case OsuModes.Play:
                    if (state.BalanceBoardState.CenterOfGravity.X < 0 || state.ButtonState.Left)
                        keys.Add(BindingManager.For(Bindings.FruitsLeft));
                    if (state.BalanceBoardState.CenterOfGravity.X > 0 || state.ButtonState.Right)
                        keys.Add(BindingManager.For(Bindings.FruitsRight));
                    if (state.ButtonState.A || state.ButtonState.B)
                    {
                        keys.Add(BindingManager.For(Bindings.FruitsDash));
                    }

                    break;
            }

            if (state.ButtonState.Plus || (Player.Paused && state.ButtonState.Up))
                keys.Add(Keys.Up);
            if (state.ButtonState.Minus || (Player.Paused && state.ButtonState.Down))
                keys.Add(Keys.Down);
            if (state.ButtonState.Home)
                keys.Add(Keys.Escape);
            if (state.ButtonState.A)
            {
                if (Player.Failed && !Player.Paused)
                    keys.Add(Keys.Escape);
                else if (GameBase.Mode == OsuModes.Play && !Player.Paused)
                    keys.Add(BindingManager.For(Bindings.Skip));
                else
                    keys.Add(Keys.Enter);
            }
        }
    }
}