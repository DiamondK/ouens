﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;
using osu.Online;

namespace osu.Input.Handlers
{
    internal class KeyboardHandler : InputHandler
    {
        public static bool ControlPressed;
        public static bool AltPressed;
        public static bool ShiftPressed;

        internal static event KeyEventHandler OnKeyPressed;
        internal static event KeyEventHandler OnKeyReleased;
        internal static event RepeatKeyEventHandler OnKeyRepeat;

        internal static bool DoKeyPressed(Keys k)
        {
            if (DoOnKeyRepeat(k, true)) return true;

            if (OnKeyPressed != null)
            {
                Delegate[] dels = OnKeyPressed.GetInvocationList();

                //ctrl+shift is considered a universal override.
                //it will reverse the order of event handling to allow global-level UI to handle keys/mouse before others.
                if (KeyboardHandler.ControlPressed && KeyboardHandler.ShiftPressed)
                {
                    List<Delegate> temp = new List<Delegate>(dels);
                    temp.Reverse();
                    dels = temp.ToArray();
                }

                if (dels.Length == 0) return false;

                for (int i = dels.Length - 1; i >= 0; i--)
                {
                    KeyEventHandler d = dels[i] as KeyEventHandler;
                    if (d == null) // cast error. should never happen.
                        continue;

                    if ((bool)d.DynamicInvoke(null, k))
                        return true;
                }
            }

            return false;
        }

        internal static void DoKeyReleased(Keys k)
        {
            if (OnKeyReleased != null)
            {
                Delegate[] dels = OnKeyReleased.GetInvocationList();

                //ctrl+shift is considered a universal override.
                //it will reverse the order of event handling to allow global-level UI to handle keys/mouse before others.
                if (KeyboardHandler.ControlPressed && KeyboardHandler.ShiftPressed)
                {
                    List<Delegate> temp = new List<Delegate>(dels);
                    temp.Reverse();
                    dels = temp.ToArray();
                }

                if (dels.Length == 0) return;

                for (int i = dels.Length - 1; i >= 0; i--)
                {
                    KeyEventHandler d = dels[i] as KeyEventHandler;
                    if (d == null) // cast error. should never happen.
                        continue;

                    if ((bool)d.DynamicInvoke(null, k))
                        break;
                }
            }
        }

        internal static bool DoOnKeyRepeat(Keys k, bool first = false)
        {
            if (OnKeyRepeat != null)
            {
                Delegate[] dels = OnKeyRepeat.GetInvocationList();

                //ctrl+shift is considered a universal override.
                //it will reverse the order of event handling to allow global-level UI to handle keys/mouse before others.
                if (KeyboardHandler.ControlPressed && KeyboardHandler.ShiftPressed)
                {
                    List<Delegate> temp = new List<Delegate>(dels);
                    temp.Reverse();
                    dels = temp.ToArray();
                }

                if (dels.Length == 0) return false;

                for (int i = dels.Length - 1; i >= 0; i--)
                {
                    RepeatKeyEventHandler d = dels[i] as RepeatKeyEventHandler;
                    if (d == null) // cast error. should never happen.
                        continue;

                    if ((bool)d.DynamicInvoke(null, k, first))
                        return true;
                }
            }

            return false;
        }

        #region Nested type: KeyEventHandler

        internal delegate bool KeyEventHandler(object sender, Keys k);

        #endregion

        #region Nested type: RepeatKeyEventHandler

        internal delegate bool RepeatKeyEventHandler(object sender, Keys k, bool first);

        #endregion

        internal static bool IsKeyDown(Keys key)
        {
            if (key == Keys.None) return false;

            return InputManager.PressedKeys.Contains(key);
        }

        internal override bool Initialize()
        {
            return true;
        }

        internal override void Dispose()
        {
        }

        internal override void UpdateInput(bool isActive)
        {

        }

        /// <summary>
        /// As of now this InputHandler writes directly to the static state, hence it should not be processed in the actual input pipeline.
        /// </summary>
        internal override bool IsActive
        {
            get
            {
                return false;
            }
        }

        internal override int Priority
        {
            get
            {
                return 2;
            }
        }
    }
}