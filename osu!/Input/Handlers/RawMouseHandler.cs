﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Configuration;
using osu.GameModes.Play;
using osu.Graphics.Notifications;
using osu_common.Helpers;
using System.Diagnostics;
using osu.Helpers;

namespace osu.Input.Handlers
{
    internal unsafe class RawMouseHandler : WndProcInputHandler, CursorInputHandler
    {
        private bool absoluteMovement = false;
        internal bool AbsoluteMovement
        {
            get
            {
                return absoluteMovement;
            }

            private set
            {
                if (value != absoluteMovement && IsActive)
                {
                    // We might have to start clamping now. (Switch from absolute -> relative)
                    InputManager.UpdateShallClampMouseToWindow();
                }

                absoluteMovement = value;
            }
        }
        private long lastProcessTime = 0;
        private double latency = 0;
        private int amountMessages = 0;

        private Vector2 position = Vector2.One;
        private List<Vector2> intermediatePositions = new List<Vector2>();
        private List<Vector2> intermediatePositionsNextFrame = new List<Vector2>();

        internal override bool Initialize()
        {
            if (!base.Initialize())
            {
                return false;
            }

            try
            {
                RawInputDevice r = new RawInputDevice();

                r.UsagePage = HIDUsagePage.Generic;
                r.Usage = HIDUsage.Mouse;
                r.Flags = RawInputDeviceFlags.None;
                r.WindowHandle = windowHandle;

                if (!Native.RegisterRawInputDevices(new[] { r }, 1, sizeof(RawInputDevice)))
                {
                    return false;
                }
            }
            catch { }


            OnMouse += MouseHandler;
            return true;
        }

        internal override void Dispose()
        {
            OnMouse -= MouseHandler;
            base.Dispose();
        }

        private void MouseHandler(RawInput data)
        {
            RawMouse m = data.Mouse;
            float speed = (float)ConfigManager.sMouseSpeed;

            AbsoluteMovement = (m.Flags & RawMouseFlags.MoveAbsolute) > 0;

            if (AbsoluteMovement)
            {
                if (ConfigManager.sAbsoluteToOsuWindow)
                {
                    const int range = 65536;
                    position.X = ((float)((m.LastX - range / 2) * speed) + range / 2) / range * GameBase.WindowWidth;
                    position.Y = ((float)((m.LastY - range / 2) * speed) + range / 2) / range * GameBase.WindowHeight;
                }
                else
                {
                    // Absolute coordinates need to be shifted to the osu window's position and scaled to the desktop resolution.
                    position = ToScreenCoords(m) - new Vector2(GameBase.ClientBounds.X, GameBase.ClientBounds.Y);

                    position.X = (position.X - GameBase.WindowWidth / 2) * speed + GameBase.WindowWidth / 2;
                    position.Y = (position.Y - GameBase.WindowHeight / 2) * speed + GameBase.WindowHeight / 2;
                }
            }
            else
            {
                position.X += m.LastX * speed;
                position.Y += m.LastY * speed;
            }

            if (InputManager.ShallClampMouseToWindow)
            {
                position.X = OsuMathHelper.Clamp(position.X, 0, GameBase.WindowWidth - 1);
                position.Y = OsuMathHelper.Clamp(position.Y, 0, GameBase.WindowHeight - 1);
            }

            intermediatePositionsNextFrame.Add(position);

            ++amountMessages;
            latency += GameBase.stopWatch.ElapsedMilliseconds - lastProcessTime;
        }

        internal static Vector2 ToScreenCoords(RawMouse hardware)
        {
            const int range = 65536;

            Rectangle screenRect = (hardware.Flags & RawMouseFlags.VirtualDesktop) > 0 ?
                VirtualScreenRect :
                new Rectangle(
                    GameBase.InitialDesktopLocation.X, GameBase.InitialDesktopLocation.Y,
                    GameBase.InitialDesktopResolution.Width, GameBase.InitialDesktopResolution.Height);

            return new Vector2(
                (float)hardware.LastX / range * screenRect.Width + screenRect.X,
                (float)hardware.LastY / range * screenRect.Height + screenRect.Y);
        }

        private void UpdateRawInputStatistics()
        {
            if (amountMessages > 0)
            {
                latency /= amountMessages;
            }

            float decay = (float)Math.Pow(0.90, GameBase.FrameRatio);
            MouseManager.MousePollRateInterpolated = MouseManager.MousePollRateInterpolated * decay + (1.0f - decay) * (amountMessages / (float)GameBase.ElapsedMilliseconds * 1000);
            MouseManager.MousePollRate = amountMessages;
            MouseManager.MousePollLatency = (int)Math.Round(latency);
            MouseManager.MousePollTime = lastProcessTime;

            amountMessages = 0;
            latency = 0;
            lastProcessTime = GameBase.stopWatch.ElapsedMilliseconds;
        }

        internal override void UpdateInput(bool isActive)
        {
            UpdateRawInputStatistics();

            intermediatePositions.Clear();
            intermediatePositions.AddRange(intermediatePositionsNextFrame);
            intermediatePositionsNextFrame.Clear();

            // The rest of the code in this method is in theory COMPLETELY unnecessary since SetPosition is going to be called in the next frame
            // in the base mouse handler anyways.
            // SetPosition however happens to not work reliably on windows 8.1 and thus this extra call increases the chances of the cursor
            // not doing weird jumps at window boundaries.
            Point pixelPosition = new Point((int)Math.Round(position.X), (int)Math.Round(position.Y));
            if (isActive && !AbsoluteMovement && !MouseManager.PosInWindow(pixelPosition, 1))
            {
                Point nativeMousePosition = MouseManager.GetNativePosition();

                if (!MouseManager.PosInWindow(nativeMousePosition))
                {
                    position.X = nativeMousePosition.X;
                    position.Y = nativeMousePosition.Y;
                }
                else
                {
                    MouseManager.SetNativePosition(pixelPosition);
                }
            }
        }

        public void SetPosition(Vector2 pos)
        {
            position = pos;
        }

        public Vector2 Position
        {
            get
            {
                return position;
            }
        }

        public ButtonState? Left
        {
            get
            {
                return null;
            }
        }

        public ButtonState? Right
        {
            get
            {
                return null;
            }
        }

        public ButtonState? Middle
        {
            get
            {
                return null;
            }
        }

        public ButtonState? Back
        {
            get
            {
                return null;
            }
        }

        public ButtonState? Forward
        {
            get
            {
                return null;
            }
        }

        public List<Vector2> IntermediatePositions
        {
            get
            {
                return intermediatePositions;
            }
        }

        internal override bool IsActive
        {
            get
            {
                return ConfigManager.sRawInput && Game.IsActive && !MouseManager.showWindow &&
                    (AbsoluteMovement || (!MouseManager.showMouse && MouseManager.PosInWindow(MouseManager.MousePoint, 1)));
            }
        }

        internal override int Priority
        {
            get
            {
                return 1;
            }
        }
    }
}