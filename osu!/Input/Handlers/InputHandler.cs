﻿using System;
using System.Collections.Generic;
using System.Text;

namespace osu.Input.Handlers
{
    internal abstract class InputHandler
    {
        /// <summary>
        /// Used to initialize resources specific to this InputHandler. It gets called once.
        /// </summary>
        /// <returns>Success of the initialization.</returns>
        internal abstract bool Initialize();

        /// <summary>
        /// Used to clean up resources specific to this InputHandler. It gets called once and only after Initialize has been called and returned true.
        /// </summary>
        internal abstract void Dispose();

        /// <summary>
        /// Gets called whenever the resolution of the game or the desktop changes.
        /// </summary>
        internal virtual void OnResolutionChange()
        {

        }

        /// <summary>
        /// Gets called for every frame of the game. This should be used to update the internal state of this InputHandler according to external circumstances.
        /// </summary>
        /// <param name="isActive">Denotes whether this input is currently active and will be used for controlling the main cursor.</param>
        internal abstract void UpdateInput(bool isActive);

        /// <summary>
        /// Indicates whether this InputHandler is currently delivering input by the user. When handling input the game uses the first InputHandler which is active.
        /// </summary>
        internal abstract bool IsActive
        {
            get;
        }

        /// <summary>
        /// Indicated how high of a priority this handler has. The active handler with the highest priority is controlling the cursor at any given time.
        /// </summary>
        internal abstract int Priority
        {
            get;
        }
    }

    internal class InputHandlerComparer : IComparer<InputHandler>
    {
        public int Compare(InputHandler h1, InputHandler h2)
        {
            return h2.Priority.CompareTo(h1.Priority);
        }
    }
}
