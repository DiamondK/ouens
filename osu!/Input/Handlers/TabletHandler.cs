﻿using System;
using System.Runtime.InteropServices;
using Microsoft.StylusInput;
using Microsoft.StylusInput.PluginData;
using Microsoft.Xna.Framework.Input;
using osu.Configuration;
using osu.Graphics.Notifications;
using osu.Input.Handlers;
using osu.GameModes.Play;
using System.Security;

namespace osu.Input
{
    internal class TabletHandler : InputHandler
    {
        internal bool IsInitialized;
        internal bool PendingTabletUpdate;
        internal ButtonState TabletLeft;

        internal bool Enabled
        {
            get
            {
                if (!IsInitialized)
                {
                    return false;
                }

                return realTimeStylus.Enabled;
            }

            set
            {
                if (!IsInitialized || realTimeStylus.Enabled == value)
                {
                    return;
                }

                realTimeStylus.Enabled = value;
            }
        }

        private RealTimeStylus realTimeStylus;

        [SuppressUnmanagedCodeSecurityAttribute()]
        [DllImport("user32.dll")]
        private static extern int GetSystemMetrics(int nIndex);

        internal static bool IsRunningOnTablet()
        {
            return (GetSystemMetrics(General.SYSTEM_METRIC_TABLETPC) != 0);
        }

        public void Update()
        {
        }

        internal override bool Initialize()
        {
            try
            {
                if (!IsInitialized)
                {
                    realTimeStylus = new RealTimeStylus(GameBase.Instance.Window.Handle, false);
                    SimpleStylus ss = new SimpleStylus(this);
                    realTimeStylus.AsyncPluginCollection.Add(ss);
                    realTimeStylus.Enabled = ConfigManager.sTablet.Value;
                    IsInitialized = true;
                    return true;
                }
            }
            catch (Exception)
            {
                NotificationManager.ShowMessage("Tablet initialisation failed.");
            }

            return false;
        }

        internal override void Dispose()
        {
            Enabled = false;
            realTimeStylus.Dispose();
        }

        internal override void UpdateInput(bool isActive)
        {
            if (Player.Playing && ConfigManager.sMouseDisableButtons)
            {
                PendingTabletUpdate = false;
                return;
            }

            if (PendingTabletUpdate)
            {
                //Make sure we accept a tablet click even when it is release in under 16ms.
                MouseManager.MouseLeft = ButtonState.Pressed;
                PendingTabletUpdate = false;
            }
            else if (TabletLeft == ButtonState.Pressed)
                MouseManager.MouseLeft = TabletLeft;
        }


        /// <summary>
        /// As of now this InputHandler writes directly to the static state, hence it should not be processed in the actual input pipeline.
        /// </summary>
        internal override bool IsActive
        {
            get
            {
                return false;
            }
        }

        internal override int Priority
        {
            get
            {
                return 1;
            }
        }
    }

    internal class SimpleStylus : IStylusAsyncPlugin
    {
        private TabletHandler tabletHandler;

        internal SimpleStylus(TabletHandler handler)
        {
            tabletHandler = handler;
        }

        #region IStylusAsyncPlugin Members

        public DataInterestMask DataInterest
        {
            get
            {
                return DataInterestMask.StylusDown |
                       DataInterestMask.StylusUp | DataInterestMask.StylusButtonDown;
            }
        }

        public void StylusDown(RealTimeStylus s,
                               StylusDownData data)
        {
            tabletHandler.PendingTabletUpdate = true;
            tabletHandler.TabletLeft = ButtonState.Pressed;
        }

        public void StylusUp(RealTimeStylus s,
                             StylusUpData data)
        {
            tabletHandler.TabletLeft = ButtonState.Released;
            if (tabletHandler.PendingTabletUpdate)
                return;
        }

        public void TabletRemoved(RealTimeStylus sender,
                                  TabletRemovedData data)
        {
        }

        public void CustomStylusDataAdded(RealTimeStylus sender,
                                          CustomStylusData data)
        {
        }

        public void RealTimeStylusDisabled(RealTimeStylus sender,
                                           RealTimeStylusDisabledData data)
        {
        }

        public void StylusOutOfRange(RealTimeStylus sender,
                                     StylusOutOfRangeData data)
        {
        }

        public void StylusButtonUp(RealTimeStylus sender,
                                   StylusButtonUpData data)
        {
        }

        public void StylusInRange(RealTimeStylus sender,
                                  StylusInRangeData data)
        {
        }

        public void RealTimeStylusEnabled(RealTimeStylus sender,
                                          RealTimeStylusEnabledData data)
        {
        }

        public void TabletAdded(RealTimeStylus sender,
                                TabletAddedData data)
        {
        }

        public void Error(RealTimeStylus sender,
                          ErrorData data)
        {
        }

        public void InAirPackets(RealTimeStylus sender,
                                 InAirPacketsData data)
        {
        }

        public void Packets(RealTimeStylus sender, PacketsData data)
        {
        }

        public void SystemGesture(RealTimeStylus sender, SystemGestureData data)
        {
            throw new NotImplementedException();
        }

        public void StylusButtonDown(RealTimeStylus sender,
                                     StylusButtonDownData data)
        {
        }

        #endregion
    }
}