﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using osu.GameModes.Play;
using Microsoft.Xna.Framework.Input;

namespace osu.Input.Handlers
{
    internal static class JoyKey
    {
        internal const Keys Button0 = (Keys)300;
        internal const Keys Button1 = (Keys)301;
        internal const Keys Button2 = (Keys)302;
        internal const Keys Button3 = (Keys)303;
        internal const Keys Button4 = (Keys)304;
        internal const Keys Button5 = (Keys)305;
        internal const Keys Button6 = (Keys)306;
        internal const Keys Button7 = (Keys)307;
        internal const Keys Button8 = (Keys)308;
        internal const Keys Button9 = (Keys)309;
        internal const Keys Button10 = (Keys)310;
        internal const Keys Button11 = (Keys)311;
        internal const Keys Button12 = (Keys)312;
        internal const Keys Left = (Keys)340;
        internal const Keys Right = (Keys)341;
        internal const Keys Up = (Keys)342;
        internal const Keys Down = (Keys)343;
    }

    internal class JoystickHandler : InputHandler
    {

        /// <summary>
        /// Function to return the number of joystick devices supported by the current driver.
        /// </summary>
        /// <returns>Number of joysticks supported, 0 if no driver is installed.</returns>
        [DllImport("WinMM.dll")]
        public static extern int joyGetNumDevs();

        /// <summary>
        /// Function to return the joystick device capabilities.
        /// </summary>
        /// <param name="uJoyID">ID of the joystick to return.  -1 will return registry key, whether a device exists or not.</param>
        /// <param name="pjc">Joystick capabilities.</param>
        /// <param name="cbjc">Size of the JOYCAPS structure in bytes.</param>
        /// <returns>0 if successful, non zero if not.</returns>
        [DllImport("WinMM.dll", CharSet = CharSet.Ansi)]
        public static extern int joyGetDevCaps(int uJoyID, ref JOYCAPS pjc, int cbjc);

        /// <summary>
        /// Function to retrieve joystick position information.
        /// </summary>
        /// <param name="uJoyID">ID the of joystick to query.</param>
        /// <param name="pji">Position information.</param>
        /// <returns>0 if successful, non zero if not.  JOYERR_UNPLUGGED if not connected.</returns>
        [DllImport("WinMM.dll", CharSet = CharSet.Ansi)]
        public static extern int joyGetPos(int uJoyID, ref JOYINFO pji);

        /// <summary>
        /// Function to retrieve joystick position information.
        /// </summary>
        /// <param name="uJoyID">ID the of joystick to query.</param>
        /// <param name="pji">Position information.</param>
        /// <returns>0 if successful, non zero if not.  JOYERR_UNPLUGGED if not connected.</returns>
        [DllImport("WinMM.dll", CharSet = CharSet.Ansi)]
        public static extern int joyGetPosEx(int uJoyID, ref JOYINFOEX pji);

        internal delegate void JoyStickHandler(object sender, List<Keys> keys);
        internal static event JoyStickHandler OnButtonPressed;
        internal static event JoyStickHandler OnButtonDown;

        internal int ManufacturerID;
        internal int ProductID;
        internal static List<Keys> KeyDown = new List<Keys>(40);
        internal static List<Keys> LastKeyDown = new List<Keys>(40);
        private bool[] buttons;
        private bool[] lastButtons;
        private bool firstUpdate;
        private int buttonCount;
        private int ID;
        private int maxX;
        private int maxY;
        private int minX;
        private int minY;
        private int defaultX;
        private int defaultY;

        internal static bool IsKeyDown(Keys key)
        {
            if (key == Keys.None) return false;
            return KeyDown.Contains(key);
        }

        internal static bool IsLastKeyDown(Keys key)
        {
            return LastKeyDown.Contains(key);
        }

        internal override bool Initialize()
        {
            int deviceCount = joyGetNumDevs();
            if (deviceCount == 0)
                return false;
            ID = -1;
            JOYINFO test = new JOYINFO();
            for (int i = 0; i < deviceCount; i++)
            {
                if ((joyGetPos(i, ref test) == 0))
                {
                    ID = i;
                    break;
                }
            }
            if (ID == -1)
            {
                return false;
            }
            JOYCAPS capabilities = new JOYCAPS();
            //we just need the first joystick.
            int error = joyGetDevCaps(ID, ref capabilities, Marshal.SizeOf(typeof(JOYCAPS)));
            if (error > 0)
                return false;
            if ((capabilities.RegistryKey == null) || (capabilities.RegistryKey == string.Empty) ||
                    (capabilities.Name == null) || (capabilities.Name == string.Empty))
                return false;

            ManufacturerID = capabilities.ManufacturerID;
            ProductID = capabilities.ProductID;

            maxX = (int)capabilities.MaximumX;
            maxY = (int)capabilities.MaximumY;
            minX = (int)capabilities.MinimumX;
            minY = (int)capabilities.MinimumY;
            buttonCount = (int)capabilities.ButtonCount;
            buttons = new bool[buttonCount + 4];
            lastButtons = new bool[buttonCount + 4];
            firstUpdate = true;
            return true;
        }

        internal override void Dispose()
        {
        }

        internal override void UpdateInput(bool isActive)
        {
            if (!GameBase.SixtyFramesPerSecondFrame)
                return;
            JOYINFOEX ji = new JOYINFOEX();
            ji.Size = Marshal.SizeOf(typeof(JOYINFOEX));
            ji.Flags = JoystickInfoFlags.ReturnButtons | JoystickInfoFlags.ReturnX | JoystickInfoFlags.ReturnY;

            int error = joyGetPosEx(ID, ref ji);
            if (error > 0)
                return;

            List<Keys> Current = new List<Keys>(buttonCount + 4);
            for (int i = 0; i < buttonCount; i++)
            {
                if ((ji.Buttons & (JoystickButtons)(1 << i)) != 0)
                {
                    buttons[i] = true;
                    Current.Add((Keys)(i + 301));
                }
                else
                    buttons[i] = false;
            }
            for (int i = buttonCount; i < buttonCount + 4; i++)
                buttons[i] = false;

            if (firstUpdate)
            {
                firstUpdate = false;
                defaultX = ji.X;
                defaultY = ji.Y;
            }

            if (minX == maxX || defaultX == minX || defaultX == maxX)
            {

            }
            else if (ji.X == minX)
            {
                buttons[buttonCount] = true;
                Current.Add(JoyKey.Left);
            }
            else if (ji.X == maxX)
            {
                buttons[buttonCount + 1] = true;
                Current.Add(JoyKey.Right);
            }

            if (minY == maxY || defaultY == minY || defaultY == maxY)
            {

            }
            else if (ji.Y == minY)
            {
                buttons[buttonCount + 2] = true;
                Current.Add(JoyKey.Up);
            }
            else if (ji.Y == maxY)
            {
                buttons[buttonCount + 3] = true;
                Current.Add(JoyKey.Down);
            }
            if (OnButtonDown != null)
            {
                OnButtonDown(null, Current);
            }
            if (OnButtonPressed != null)
            {
                List<Keys> down = new List<Keys>(buttonCount + 4);
                for (int i = 0; i < buttonCount + 4; i++)
                {
                    //release
                    if (lastButtons[i] && !buttons[i])
                    {
                        if (i >= buttonCount)
                            down.Add((Keys)(340 + i - buttonCount));
                        else
                            down.Add((Keys)(301 + i));
                    }
                }
                if (down.Count != 0)
                    OnButtonPressed(null, down);
            }
            LastKeyDown.Clear();
            LastKeyDown.AddRange(KeyDown);
            KeyDown.Clear();
            KeyDown.AddRange(Current);
            buttons.CopyTo(lastButtons, 0);
        }

        /// <summary>
        /// As of now this InputHandler writes directly to the static state, hence it should not be processed in the actual input pipeline.
        /// </summary>
        internal override bool IsActive
        {
            get
            {
                return false;
            }
        }

        internal override int Priority
        {
            get
            {
                return 2;
            }
        }
    }



    /// <summary>
    /// Value type containing joystick position information.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    internal struct JOYINFO
    {
        /// <summary>X axis.</summary>
        public int X;
        /// <summary>Y axis.</summary>
        public int Y;
        /// <summary>Z axis.</summary>
        public int Z;
        /// <summary>State of buttons.</summary>
        public JoystickButtons Buttons;
    }

    /// <summary>
    /// Value type containing joystick position information.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct JOYINFOEX
    {
        /// <summary>Size of structure, in bytes.</summary>
        public int Size;
        /// <summary>Flags to indicate what information is valid for the device.</summary>
        public JoystickInfoFlags Flags;
        /// <summary>X axis.</summary>
        public int X;
        /// <summary>Y axis.</summary>
        public int Y;
        /// <summary>Z axis.</summary>
        public int Z;
        /// <summary>Rudder position.</summary>
        public int Rudder;
        /// <summary>5th axis position.</summary>
        public int Axis5;
        /// <summary>6th axis position.</summary>
        public int Axis6;
        /// <summary>State of buttons.</summary>
        public JoystickButtons Buttons;
        /// <summary>Currently pressed button.</summary>
        public int ButtonNumber;
        /// <summary>Angle of the POV hat, in degrees (0 - 35900, divide by 100 to get 0 - 359 degrees.</summary>
        public int POV;
        /// <summary>Reserved.</summary>
        int Reserved1;
        /// <summary>Reserved.</summary>
        int Reserved2;
    }

    /// <summary>
    /// Enumeration containing the joystick information flags.
    /// </summary>
    [Flags()]
    public enum JoystickInfoFlags
    {
        /// <summary></summary>
        All = (ReturnX | ReturnY | ReturnZ | ReturnRudder | ReturnAxis5 | ReturnAxis6 | ReturnPOV | ReturnButtons),
        /// <summary></summary>
        ReturnX = 0x00000001,
        /// <summary></summary>
        ReturnY = 0x00000002,
        /// <summary></summary>
        ReturnZ = 0x00000004,
        /// <summary></summary>
        ReturnRudder = 0x00000008,
        /// <summary></summary>
        ReturnAxis5 = 0x00000010,
        /// <summary></summary>
        ReturnAxis6 = 0x00000020,
        /// <summary></summary>
        ReturnPOV = 0x00000040,
        /// <summary></summary>
        ReturnButtons = 0x00000080,
        /// <summary></summary>
        ReturnRawData = 0x00000100,
        /// <summary></summary>
        ReturnPOVContinuousDegreeBearings = 0x00000200,
        /// <summary></summary>
        ReturnCentered = 0x00000400,
        /// <summary></summary>
        UseDeadzone = 0x00000800,
        /// <summary></summary>
        CalibrationReadAlways = 0x00010000,
        /// <summary></summary>
        CalibrationReadXYOnly = 0x00020000,
        /// <summary></summary>
        CalibrationRead3 = 0x00040000,
        /// <summary></summary>
        CalibrationRead4 = 0x00080000,
        /// <summary></summary>
        CalibrationReadXOnly = 0x00100000,
        /// <summary></summary>
        CalibrationReadYOnly = 0x00200000,
        /// <summary></summary>
        CalibrationRead5 = 0x00400000,
        /// <summary></summary>
        CalibrationRead6 = 0x00800000,
        /// <summary></summary>
        CalibrationReadZOnly = 0x01000000,
        /// <summary></summary>
        CalibrationReadRudderOnly = 0x02000000,
        /// <summary></summary>
        CalibrationReadAxis5Only = 0x04000000,
        /// <summary></summary>
        CalibrationReadaxis6Only = 0x08000000
    }

    /// <summary>
    /// Enumeration for joystick capabilities.
    /// </summary>
    [Flags()]
    internal enum JoystickCapabilities
    {
        /// <summary>Has a Z axis.</summary>
        HasZ = 0x0001,
        /// <summary>Has a rudder axis.</summary>
        HasRudder = 0x0002,
        /// <summary>Has a 5th axis.</summary>
        HasU = 0x0004,
        /// <summary>Has a 6th axis.</summary>
        HasV = 0x0008,
        /// <summary>Has a POV hat.</summary>
        HasPOV = 0x0010,
        /// <summary>Has a 4 direction POV hat.</summary>
        POV4Directions = 0x0020,
        /// <summary>Has a continuous degree bearing POV hat.</summary>
        POVContinuousDegreeBearings = 0x0040
    }


    /// <summary>
    /// Value type containing joystick capabilities.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    internal struct JOYCAPS
    {
        /// <summary>Manufacturer ID.</summary>
        public ushort ManufacturerID;
        /// <summary>Product ID.</summary>
        public ushort ProductID;
        /// <summary>Joystick name.</summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string Name;
        /// <summary>Minimum X coordinate.</summary>
        public uint MinimumX;
        /// <summary>Maximum X coordinate.</summary>
        public uint MaximumX;
        /// <summary>Minimum Y coordinate.</summary>
        public uint MinimumY;
        /// <summary>Maximum Y coordinate.</summary>
        public uint MaximumY;
        /// <summary>Minimum Z coordinate.</summary>
        public uint MinimumZ;
        /// <summary>Maximum Z coordinate.</summary>
        public uint MaximumZ;
        /// <summary>Number of buttons on the joystick.</summary>
        public uint ButtonCount;
        /// <summary>Smallest polling frequency (used by joySetCapture).</summary>
        public uint MinimumPollingFrequency;
        /// <summary>Largest polling frequency (used by joySetCapture).</summary>
        public uint MaximumPollingFrequency;
        /// <summary>Minimum rudder value.</summary>
        public uint MinimumRudder;
        /// <summary>Maximum rudder value.</summary>
        public uint MaximumRudder;
        /// <summary>Minimum 5th axis value.</summary>
        public uint Axis5Minimum;
        /// <summary>Maximum 5th axis value.</summary>
        public uint Axis5Maximum;
        /// <summary>Minimum 6th axis value.</summary>
        public uint Axis6Minimum;
        /// <summary>Maximum 6th axis value.</summary>
        public uint Axis6Maximum;
        /// <summary>Joystick capabilities.</summary>
        public JoystickCapabilities Capabilities;
        /// <summary>Maxmimum number of axes for the joystick.</summary>
        public uint MaximumAxes;
        /// <summary>Number of axes on the joystick.</summary>
        public uint AxisCount;
        /// <summary>Maximum buttons for the device.</summary>
        public uint MaximumButtons;
        /// <summary>Registry key for the joystick.</summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string RegistryKey;
        /// <summary>Driver name for the joystick.</summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
        public string DriverName;
    }

    /// <summary>
    /// Enumeration for joystick buttons.
    /// </summary>
    public enum JoystickButtons
        : uint
    {
        /// <summary></summary>
        Button1 = 0x0001,
        /// <summary></summary>
        Button2 = 0x0002,
        /// <summary></summary>
        Button3 = 0x0004,
        /// <summary></summary>
        Button4 = 0x0008,
        /// <summary></summary>
        /*    Button1Changed = 0x0100,
            /// <summary></summary>
            Button2Changed = 0x0200,
            /// <summary></summary>
            Button3Changed = 0x0400,
            /// <summary></summary>
            Button4Changed = 0x0800, */
        /// <summary></summary>
        Button5 = 0x00000010,
        /// <summary></summary>
        Button6 = 0x00000020,
        /// <summary></summary>
        Button7 = 0x00000040,
        /// <summary></summary>
        Button8 = 0x00000080,
        /// <summary></summary>
        Button9 = 0x00000100,
        /// <summary></summary>
        Button10 = 0x00000200,
        /// <summary></summary>
        Button11 = 0x00000400,
        /// <summary></summary>
        Button12 = 0x00000800,
        /// <summary></summary>
        Button13 = 0x00001000,
        /// <summary></summary>
        Button14 = 0x00002000,
        /// <summary></summary>
        Button15 = 0x00004000,
        /// <summary></summary>
        Button16 = 0x00008000,
        /// <summary></summary>
        Button17 = 0x00010000,
        /// <summary></summary>
        Button18 = 0x00020000,
        /// <summary></summary>
        Button19 = 0x00040000,
        /// <summary></summary>
        Button20 = 0x00080000,
        /// <summary></summary>
        Button21 = 0x00100000,
        /// <summary></summary>
        Button22 = 0x00200000,
        /// <summary></summary>
        Button23 = 0x00400000,
        /// <summary></summary>
        Button24 = 0x00800000,
        /// <summary></summary>
        Button25 = 0x01000000,
        /// <summary></summary>
        Button26 = 0x02000000,
        /// <summary></summary>
        Button27 = 0x04000000,
        /// <summary></summary>
        Button28 = 0x08000000,
        /// <summary></summary>
        Button29 = 0x10000000,
        /// <summary></summary>
        Button30 = 0x20000000,
        /// <summary></summary>
        Button31 = 0x40000000,
        /// <summary></summary>
        Button32 = 0x80000000
    }
}
