﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using osu.Configuration;
using osu.GameModes.Play;
using osu.Graphics.Notifications;
using osu_common.Helpers;
using System.Drawing;
using Microsoft.Xna.Framework.Input;
using osu.Helpers;
using Microsoft.Xna.Framework;
using Point = System.Drawing.Point;
using System.Diagnostics;

namespace osu.Input.Handlers
{
    internal unsafe class TouchHandler : WndProcInputHandler, CursorInputHandler
    {
        internal Dictionary<int, Vector2> TouchDownPositions = new Dictionary<int, Vector2>();
        private List<int> currentTouchButtons = new List<int>();
        private int currentTouchID = -1;
        private bool isActive = false;

        private Point position;
        private List<Vector2> intermediatePositions = new List<Vector2>();
        private List<Vector2> intermediatePositionsNextFrame = new List<Vector2>();

        private ButtonState left;
        private ButtonState right;

        private bool isLeft = true;

        internal override bool Initialize()
        {
            if (!base.Initialize())
            {
                return false;
            }

            try
            {
                const string pressAndHoldAtomStr = "MicrosoftTabletPenServiceProperty";
                ushort pressAndHoldAtomID = Native.GlobalAddAtom(pressAndHoldAtomStr);

                if (pressAndHoldAtomID != 0)
                {
                    Native.SetProp(windowHandle, pressAndHoldAtomStr,
                        Native.TABLET_DISABLE_PRESSANDHOLD |
                        Native.TABLET_DISABLE_PENTAPFEEDBACK |
                        Native.TABLET_DISABLE_PENBARRELFEEDBACK |
                        Native.TABLET_DISABLE_FLICKS |
                        Native.TABLET_DISABLE_SMOOTHSCROLLING |
                        Native.TABLET_DISABLE_FLICKFALLBACKKEYS |
                        Native.TABLET_ENABLE_MULTITOUCHDATA);
                }

                Native.GlobalDeleteAtom(pressAndHoldAtomID);

                Native.RegisterTouchWindow(windowHandle, Native.TWF_WANTPALM);
            }
            catch { }

            OnPointer += PointerHandler;
            return true;
        }

        internal override void Dispose()
        {
            OnPointer -= PointerHandler;
            base.Dispose();
        }

        internal override void UpdateInput(bool isActive)
        {
            intermediatePositions.Clear();
            intermediatePositions.AddRange(intermediatePositionsNextFrame);
            intermediatePositionsNextFrame.Clear();
        }

        private bool AlwaysLeft
        {
            get
            {
                return Player.Paused || InputManager.ReplayMode || GameBase.Mode != OsuModes.Play;
            }
        }

        private ButtonState CurrentButton
        {
            set
            {
                if (value == ButtonState.Pressed)
                    isLeft |= AlwaysLeft;

                if (isLeft)
                    left = value;
                else
                    right = value;

                if (value == ButtonState.Pressed)
                    isLeft = !isLeft;
            }
        }

        private void PointerHandler(RawPointerInput data)
        {
            Point posPoint = GameBase.Form.PointToClient(data.PixelLocationRaw);
            Vector2 pos = new Vector2(posPoint.X, posPoint.Y);

            if ((data.Flags & RawPointerFlags.Down) > 0 && !currentTouchButtons.Contains(data.ID))
            {
                // No need to even attempt to press a button if 2 are already pressed.
                if (currentTouchButtons.Count < 2)
                {
                    CurrentButton = ButtonState.Pressed;
                }

                currentTouchID = data.ID;
                currentTouchButtons.Add(data.ID);
                // We expect the touch positions to already be in window-space
                TouchDownPositions.Add(data.ID, pos / GameBase.WindowRatio);
            }

            if (currentTouchID == data.ID)
            {
                position = posPoint;
                intermediatePositionsNextFrame.Add(pos);
            }

            if ((data.Flags & RawPointerFlags.Up) > 0 ||
                (data.Flags & RawPointerFlags.CaptureChanged) > 0)
            {
                if (currentTouchButtons.Contains(data.ID))
                {
                    if (currentTouchButtons.Count <= 2)
                    {
                        if (currentTouchButtons.Count == 1)
                        {
                            left = ButtonState.Released;
                            right = ButtonState.Released;

                            isLeft = true;
                        }
                        else
                        {
                            CurrentButton = ButtonState.Released;
                        }
                    }

                    currentTouchButtons.Remove(data.ID);
                    TouchDownPositions.Remove(data.ID);
                }

                currentTouchID = currentTouchButtons.Count == 0 ? -1 : currentTouchButtons[currentTouchButtons.Count - 1];
            }
        }

        public void SetPosition(Vector2 pos)
        {

        }

        public Vector2 Position
        {
            get
            {
                return new Vector2(position.X, position.Y);
            }
        }

        public ButtonState? Left
        {
            get
            {
                return left;
            }
        }

        public ButtonState? Right
        {
            get
            {
                return right;
            }
        }

        public ButtonState? Middle
        {
            get
            {
                return null;
            }
        }

        public ButtonState? Back
        {
            get
            {
                return null;
            }
        }

        public ButtonState? Forward
        {
            get
            {
                return null;
            }
        }

        public List<Vector2> IntermediatePositions
        {
            get
            {
                return intermediatePositions;
            }
        }

        internal override bool IsActive
        {
            get
            {
                bool result = currentTouchID != -1;

                // We want to stay active for one additional check after being flagged as inactive
                if (result == false && isActive)
                {
                    result = true;
                    isActive = false;
                }
                else if (result == true)
                {
                    isActive = true;
                }

                return result && Game.IsActive && !MouseManager.showMouse;
            }
        }

        internal override int Priority
        {
            get
            {
                return 2;
            }
        }
    }
}