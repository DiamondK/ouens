﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets.Fruits;
using osu.GameplayElements.Events;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu.Helpers;

namespace osu.Input.Drawable
{
    class InputOverlay : DrawableGameComponent
    {
        enum ButtonType
        {
            K1,
            K2,
            M1,
            M2,
        }

        Dictionary<ButtonType, pSprite> buttonSprites = new Dictionary<ButtonType, pSprite>();
        private SpriteManager spriteManager;

        public InputOverlay()
            : base(GameBase.Game)
        {
            spriteManager = new SpriteManager(true);
            Initialize();
        }

        protected override void Dispose(bool disposing)
        {
            spriteManager.Dispose();

            base.Dispose(disposing);
        }

        public override void Initialize()
        {
            int y_start = GameBase.WindowHeightScaled / 2 +
                (PlayerVs.Match != null && (PlayerVs.Match.matchTeamType == MatchTeamTypes.TagTeamVs || PlayerVs.Match.matchTeamType == MatchTeamTypes.TeamVs) ? 40 : -40);

            pSprite background = new pSprite(SkinManager.Load("inputoverlay-background"), Fields.TopLeft, Origins.TopLeft, Clocks.Game, new Vector2(GameBase.WindowWidthScaled, y_start))
            {
                Rotation = OsuMathHelper.PiOver2,
                VectorScale = new Vector2(1.05f, 1),
                AlwaysDraw = true,
                Depth = 0.9f
            };
            spriteManager.Add(background);

            float yOffset = 0;
            foreach (ButtonType b in Enum.GetValues(typeof(ButtonType)))
            {
                Vector2 pos = new Vector2(GameBase.WindowWidthScaled - 15, y_start + 19f + yOffset);

                string desc = getDescription(b);

                pSprite p = new pSprite(SkinManager.Load("inputoverlay-key"), Fields.TopLeft, Origins.Centre, Clocks.Game, pos)
                {
                    AlwaysDraw = true,
                    Depth = 0.98f
                };
                if (string.IsNullOrEmpty(desc))
                {
                    p.InitialColour = Color.LightGray;
                    p.Alpha = 0.5f;
                }

                buttonSprites[b] = p;
                spriteManager.Add(p);


                if (!string.IsNullOrEmpty(desc))
                {
                    p.Tag = new pText(desc, 14, pos, 1, true, Color.White)
                    {
                        Origin = Origins.Centre,
                        TextBold = true,
                        TextBorder = true,
                        TextColour = Color.White
                    };

                    if (!GameBase.Tournament)
                        spriteManager.Add((pSprite)p.Tag);
                }

                yOffset += 29.5f;
            }

            base.Initialize();
        }

        private string getDescription(ButtonType b)
        {
            switch (Player.Mode)
            {
                default:
                    return b.ToString();
                case PlayModes.CatchTheBeat:
                    switch (b)
                    {
                        case ButtonType.K1: return "L";
                        case ButtonType.K2: return "R";
                        case ButtonType.M1: return "D";
                        default: return string.Empty;
                    }
            }
        }

        public override void Draw()
        {
            spriteManager.Draw();
            base.Draw();
        }

        public override void Update()
        {
            bool instantRelease = InputManager.MouseButtonInstantRelease;

            bool mouseBaseCondition = InputManager.ReplayMode || !ConfigManager.sMouseDisableButtons;

            switch (Player.Mode)
            {
                case PlayModes.Osu:
                    SetButtonState(ButtonType.M1, mouseBaseCondition && InputManager.leftButton1 && !InputManager.leftButton2);
                    SetButtonState(ButtonType.M2, mouseBaseCondition && InputManager.rightButton1 && !InputManager.rightButton2);
                    SetButtonState(ButtonType.K1, InputManager.leftButton1 && InputManager.leftButton2);
                    SetButtonState(ButtonType.K2, InputManager.rightButton1 && InputManager.rightButton2);
                    break;
                case PlayModes.CatchTheBeat:
                    RulesetFruits f = Player.Instance.Ruleset as RulesetFruits;
                    SetButtonState(ButtonType.K1, f.LeftPressed);
                    SetButtonState(ButtonType.K2, f.RightPressed);
                    SetButtonState(ButtonType.M1, f.Dashing);
                    break;
            }

            foreach (pSprite p in buttonSprites.Values)
            {
                pSprite t = p.Tag as pSprite;
                if (t != null) t.Scale = p.Scale;
            }

            base.Update();
        }

        Color colourIdle = Color.White;
        Color colourKeyboard = new Color(255, 222, 0);
        Color colourMouse = new Color(248, 0, 158);

        Dictionary<ButtonType, int> pressCounts = new Dictionary<ButtonType, int>();


        private void SetButtonState(ButtonType buttonType, bool down)
        {
            pSprite b = buttonSprites[buttonType];

            int state = down ? 1 : 0;
            if (b.TagNumeric == state) return;
            b.TagNumeric = state;

            if (down)
            {
                pText label = b.Tag as pText;
                if (label != null && !EventManager.BreakMode && !Player.Paused && !Player.Unpausing && !(PlayerVs.Instance != null && InputManager.ReplayMode))
                {
                    int count = 0;
                    if (!pressCounts.TryGetValue(buttonType, out count))
                        pressCounts[buttonType] = 0;

                    if (count == 1000 || count == 100)
                        label.TextSize--;

                    label.Text = (++pressCounts[buttonType]).ToString(GameBase.nfi);
                }

                b.FadeColour(buttonType == ButtonType.K1 || buttonType == ButtonType.K2 ? colourKeyboard : colourMouse, 0);
                b.Transformations.RemoveAll(t => t.Type == TransformationType.Scale);
                b.Transformations.Add(new Transformation(TransformationType.Scale, 1, 0.8f, GameBase.Time, GameBase.Time + 160, EasingTypes.Out));
            }
            else
            {
                b.FadeColour(colourIdle, 100);
                b.Transformations.Add(new Transformation(TransformationType.Scale, b.Scale, 1, GameBase.Time, GameBase.Time + 160, EasingTypes.Out));
            }
        }
    }
}
