﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Graphics.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;

namespace osu.Input.Drawable
{
    internal class InputCandidate
    {
        private pSprite background;
        private pText[] text;
        private ImeTextBox imeTextbox;
        internal Vector2 StartPosition;
        internal bool Downward = true;
        internal Vector2 Position
        {
            set
            {
                background.Position = value;
                for (int i = 0; i < 10; i++)
                    text[i].Position = value + new Vector2(0, 15 * i);
            }
        }

        internal InputCandidate(SpriteManager spriteManager, Vector2 position, ImeTextBox imeTextbox)
        {
            background = new pSprite(
                GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft, Clocks.Game, position, 0.999F, true, Color.Gray);
            background.Scale = 1.6F;
            background.Alpha = 0;
            spriteManager.Add(background);
            this.imeTextbox = imeTextbox;
            StartPosition = position;
            text = new pText[10];
            for (int i = 0; i < 10; i++)
            {
                text[i] = new pText("", 15, position + new Vector2(0, 15 * i), 1F, true, Color.White);
                text[i].Alpha = 0;
                spriteManager.Add(text[i]);
            }
        }

        internal void Update()
        {
            if (GameBase.CurrentScreenMode == osu_common.ScreenMode.Fullscreen) return; //only draw in fullscreen for now.

            if (imeTextbox.Candidates.Count == 0 || imeTextbox.Text == string.Empty)
                FadeOut();
            else
            {
                FadeIn();
                float max = 90;

                Position = StartPosition + (Downward ? Vector2.Zero : new Vector2(0, -15 * imeTextbox.Candidates.Count));
                for (int i = 0; i < imeTextbox.Candidates.Count; i++)
                {
                    text[i].Text = string.Format("{0}.{1}", i + 1, imeTextbox.Candidates[i]);
                    float len = text[i].MeasureText().X;
                    if (len > max)
                        max = len;
                }
                background.VectorScale = new Vector2(Math.Min(max, 250), 15 * imeTextbox.Candidates.Count);
            }
        }

        internal void FadeIn()
        {
            background.Alpha = 1;
            for (int i = 0; i < 10; i++)
                text[i].Alpha = 1;
        }

        internal void FadeOut()
        {
            background.Alpha = 0;
            for (int i = 0; i < 10; i++)
            {
                text[i].Alpha = 0;
                text[i].Text = "";
            }
        }
    }
}
