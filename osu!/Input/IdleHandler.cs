﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.Input.Handlers;
using osu.Graphics.Notifications;

namespace osu.Input
{
    /// <summary>
    /// Returns game to menu state when idle (arcade specific).
    /// </summary>
    internal static class IdleHandler
    {
        internal static void Initialize()
        {
            InputManager.Bind(InputEventType.OnClick, onClick, InputTargetType.Highest, BindLifetime.Manual);
        }

        static int lastActivity;
        static bool activityWarning;

        static bool onClick(object sender, EventArgs e)
        {
            if (activityWarning)
            {
                NotificationManager.ClearMessageMassive();
                activityWarning = false;
            }

            lastActivity = GameBase.Time;

            return false;
        }

        internal static void ForceActivity()
        {
            lastActivity = GameBase.Time;
        }

        internal static void Check(int maxIdleTime)
        {
#if !ARCADE
            return;
#endif

            if (GameBase.Time - lastActivity > maxIdleTime + 3000)
            {
                if (activityWarning)
                {
                    activityWarning = false;
                    GameBase.ChangeMode(OsuModes.Menu);
                }
            }
            else if (GameBase.Time - lastActivity > maxIdleTime)
            {
                if (!activityWarning)
                {
                    activityWarning = true;
                    NotificationManager.ShowMessageMassive("Returning to main menu - touch screen to cancel!", 5000);
                }
            }
            else
                activityWarning = false;
        }
    }
}
