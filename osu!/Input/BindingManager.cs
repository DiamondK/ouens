﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Input;
using osu.Configuration;
using osu.GameModes.Play;
using osu.Online;
using osu_common;
using osu.Input.Handlers;
using osu.Online.Social;

namespace osu.Input
{
    internal enum Bindings
    {
        None = 0,
        _Standard,
        OsuLeft,
        OsuRight,
        OsuSmoke,
        _TaikoMod,
        TaikoInnerLeft,
        TaikoInnerRight,
        TaikoOuterLeft,
        TaikoOuterRight,
        _CatchTheBeatMod,
        FruitsLeft,
        FruitsRight,
        FruitsDash,
        _ManiaMod,
        IncreaseSpeed,
        DecreaseSpeed,
        _InGame,
        Pause,
        Skip,
        ToggleScoreboard,
        IncreaseAudioOffset,
        DecreaseAudioOffset,
        _General,
        ToggleFrameLimiter,
        ToggleChat,
        ToggleExtendedChat,
        Screenshot,
        VolumeIncrease,
        VolumeDecrease,
        DisableMouseButtons,
        BossKey,
        _Editor,
        SelectTool,
        NormalTool,
        SliderTool,
        SpinnerTool,
        NewComboToggle,
        WhistleToggle,
        FinishToggle,
        ClapToggle,
        GridSnapToggle,
        DistSnapToggle,
        NoteLockToggle,
        NudgeLeft,
        NudgeRight,
        HelpToggle,
        JumpToBegin,
        PlayFromBegin,
        AudioPause,
        JumpToEnd,
        GridChange,
        TimingSection,
        InheritingSection,
        RemoveSection,
        _ModSelect,
        Easy,
        NoFail,
        HalfTime,
        HardRock,
        SuddenDeath,
        DoubleTime,
        Hidden,
        Flashlight,
        Relax,
        Autopilot,
        SpunOut,
        Auto
    }

    internal enum BindingTarget
    {
        Universal,
        Editor,
        ModSelect,
        _Last
    }

    internal enum PlayKey
    {
        Left1,
        Left2,
        Right1,
        Right2
    }

    internal static class BindingManager
    {
        static Dictionary<Bindings, Keys> BindingDictionary = new Dictionary<Bindings, Keys>();

        static Dictionary<BindingTarget, Dictionary<Keys, Bindings>> KeyDictionaries = new Dictionary<BindingTarget, Dictionary<Keys, Bindings>>();

        public static string[] BindingNames = new[] {
            "None",
            "_osu! Standard Mode",

            "Left Click",
            "Right Click",
            "Smoke",

            "_Taiko Mode",

            "Drum Centre (Left)",
            "Drum Centre (Right)",
            "Drum Rim (Left)",
            "Drum Rim (Right)",

            "_Catch the Beat Mode",

            "Move Left",
            "Move Right",
            "Dash! (2x movement speed)",

            "_osu! Mania Mode",

            "Increase Speed",
            "Decrease Speed",

            "_In-Game",

            "Game Pause",
            "Skip Cutscene",
            "Toggle Scoreboard",
            "Increase Local Song Offset",
            "Decrease Local Song Offset",

            "_Universal",

            "Toggle Framerate Limit",
            "Toggle Chat",
            "Toggle Extended Chat",
            "Save Screenshot",
            "Increase Volume",
            "Decrease Volume",
            "Disable mouse buttons",
            "Boss key",

            "_Editor",

            "Select Tool",
            "Normal Tool",
            "Slider Tool",
            "Spinner Tool",
            "New Combo Toggle",
            "Whistle Toggle",
            "Finish Toggle",
            "Clap Toggle",
            "Grid Snap Toggle",
            "Distance Snap Toggle",
            "Note Lock Toggle",
            "Nudge Left",
            "Nudge Right",
            "Help Toggle",
            "Jump To First Note",
            "Play From Beginning",
            "Audio Pause",
            "Jump To End",
            "Grid Size Change",
            "Add Timing Section",
            "Add Inheriting Section",
            "Remove Section",

            "_Mod Select",
            "Easy",
            "No Fail",
            "Half Time",
            "Hard Rock",
            "Sudden Death",
            "Double Time",
            "Hidden",
            "Flashlight",
            "Relax",
            "Autopilot",
            "Spun Out",
            "Auto",
        };

        public static readonly int[] ManiaMiddleKeys = new[] {
            4,
            13,
        };
        public static readonly Keys[] ManiaDefaultKeys = new[] {
            Keys.A,
            Keys.S,
            Keys.D,
            Keys.F,
            Keys.Space,
            Keys.J,
            Keys.K,
            Keys.L,
            Keys.OemSemicolon,
            Keys.Q,
            Keys.W,
            Keys.E,
            Keys.R,
            Keys.RightAlt,
            Keys.U,
            Keys.I,
            Keys.O,
            Keys.P,
        };
        public static readonly Keys[] ManiaDefaultSpecialKeys = new[] {
            Keys.LeftShift,
            Keys.RightShift,
        };
        public static readonly Keys[] ManiaDefaultAlternateSpecialKeys = new[] {
            Keys.LeftControl,
            Keys.RightControl,
        };

        public static void Set(Bindings b, Keys k)
        {
            SetUnsafe(b, k);
            CheckBindings();
        }

        private static BindingTarget getBindingTarget(Bindings b)
        {
            int j = (int)b;
            while (j-- > 1)
            {
                string name = ((Bindings)j).ToString();
                if (name[0] == '_')
                {
                    for (int i = (int)BindingTarget._Last - 1; i > 0; i--)
                        if (name.Substring(1) == ((BindingTarget)i).ToString())
                            return (BindingTarget)i;
                }
            }

            return BindingTarget.Universal;
        }

        private static Dictionary<Keys, Bindings> getKeyDictionary(BindingTarget target)
        {
            return KeyDictionaries[target];
        }

        private static void SetUnsafe(Bindings b, Keys k)
        {
            BindingTarget target = getBindingTarget(b);

            Dictionary<Keys, Bindings> KeyDictionary = getKeyDictionary(target);

            Keys kOld;
            if (BindingDictionary.TryGetValue(b, out kOld))
                if (k == kOld) return; //no change


            if (BindingDictionary.TryGetValue(b, out kOld) && kOld != Keys.None)
            {
                bool foundRebind = false;
                foreach (var v in BindingDictionary)
                    if (v.Key != b && v.Value == kOld)
                    {
                        KeyDictionary[kOld] = v.Key;
                        foundRebind = true;
                        break;
                    }

                if (!foundRebind) KeyDictionary.Remove(kOld);
            }

            BindingDictionary[b] = k;
            KeyDictionary[k] = b;

            //Reset binding statics.
            ChatEngine.keySpamCheckArray = null;
        }

        public static Keys For(Bindings bind)
        {
            return BindingDictionary[bind];
        }

        public static Bindings For(Keys key, BindingTarget target = BindingTarget.Universal)
        {
            if (key == Keys.None) return Bindings.None;

            Bindings b;
            return KeyDictionaries[target].TryGetValue(key, out b) ? b : Bindings.None;
        }

        public static string NiceName(Bindings bind)
        {
            Keys k = BindingDictionary[bind];

            return NiceName(k);
        }

        public static string NiceName(Keys key)
        {
            string s = key.ToString().Replace("Oem", "");

            if (s.Length == 2 && s[0] == 'D')
                s = s.Substring(1);
            if (key > Keys.OemClear)
            {
                switch (key)
                {
                    case JoyKey.Left:
                        return "Left";
                    case JoyKey.Right:
                        return "Right";
                    case JoyKey.Up:
                        return "Up";
                    case JoyKey.Down:
                        return "Down";
                    default:
                        int btn = (int)key - 300;
                        return "Btn" + btn.ToString();
                }
            }
            switch (key)
            {
                case Keys.Space:
                    s = "Spc";
                    break;
                case Keys.OemSemicolon:
                    s = ";";
                    break;
                case Keys.OemComma:
                    s = ",";
                    break;
                case Keys.OemPeriod:
                    s = ".";
                    break;
                case Keys.OemQuestion:
                    s = "/";
                    break;
                case Keys.OemQuotes:
                    s = "'";
                    break;
                case Keys.OemOpenBrackets:
                    s = "[";
                    break;
                case Keys.OemCloseBrackets:
                    s = "]";
                    break;
                case Keys.OemPipe:
                    s = "\\";
                    break;
                case Keys.LeftControl:
                    s = "LCon";
                    break;
                case Keys.LeftShift:
                    s = "LSft";
                    break;
                case Keys.LeftAlt:
                    s = "LAlt";
                    break;
                case Keys.RightControl:
                    s = "RCon";
                    break;
                case Keys.RightShift:
                    s = "RSft";
                    break;
                case Keys.RightAlt:
                    s = "RAlt";
                    break;
            }

            return s;
        }

        public static void CheckBindings()
        {
            List<Keys> usedKeys = new List<Keys>();
            usedKeys.Add(For(Bindings.TaikoInnerLeft));

            if (usedKeys.Contains(For(Bindings.TaikoInnerRight)))
                SetUnsafe(Bindings.TaikoInnerRight, Keys.None);
            else
                usedKeys.Add(For(Bindings.TaikoInnerRight));

            if (usedKeys.Contains(For(Bindings.TaikoOuterLeft)))
                SetUnsafe(Bindings.TaikoOuterLeft, Keys.None);
            else
                usedKeys.Add(For(Bindings.TaikoOuterLeft));

            if (usedKeys.Contains(For(Bindings.TaikoOuterRight)))
                SetUnsafe(Bindings.TaikoOuterRight, Keys.None);
            else
                usedKeys.Add(For(Bindings.TaikoOuterRight));
        }

        /// <summary
        /// </summary>
        /// <param name="key">1.left1  2.left2  3.right1  4.right2</param>
        /// <returns></returns>
        public static Keys GetPlayKey(PlayKey key)
        {
            switch (Player.Mode)
            {
                case PlayModes.Osu:
                    {
                        if (key == PlayKey.Left1 || key == PlayKey.Left2)
                            return BindingDictionary[Bindings.OsuLeft];
                        else
                            return BindingDictionary[Bindings.OsuRight];
                    }
                    break;
                case PlayModes.Taiko:
                    {
                        if (key == PlayKey.Left1)
                            return BindingDictionary[Bindings.TaikoInnerLeft];
                        else if (key == PlayKey.Left2)
                            return BindingDictionary[Bindings.TaikoInnerRight];
                        else if (key == PlayKey.Right1)
                            return BindingDictionary[Bindings.TaikoOuterLeft];
                        else if (key == PlayKey.Right2)
                            return BindingDictionary[Bindings.TaikoOuterRight];
                    }
                    break;
            }
            return Keys.None;
        }

        public static void Initialize(bool skipConfig = false)
        {
            BindingDictionary = new Dictionary<Bindings, Keys>();
            KeyDictionaries = new Dictionary<BindingTarget, Dictionary<Keys, Bindings>>();

            foreach (BindingTarget bt in Enum.GetValues(typeof(BindingTarget)))
                KeyDictionaries[bt] = new Dictionary<Keys, Bindings>();

            SetUnsafe(Bindings.OsuLeft, Keys.Z);
            SetUnsafe(Bindings.OsuRight, Keys.X);
            SetUnsafe(Bindings.OsuSmoke, Keys.C);
            SetUnsafe(Bindings.FruitsDash, Keys.LeftShift);
            SetUnsafe(Bindings.FruitsLeft, Keys.Left);
            SetUnsafe(Bindings.FruitsRight, Keys.Right);
            SetUnsafe(Bindings.TaikoInnerLeft, Keys.X);
            SetUnsafe(Bindings.TaikoInnerRight, Keys.C);
            SetUnsafe(Bindings.TaikoOuterLeft, Keys.Z);
            SetUnsafe(Bindings.TaikoOuterRight, Keys.V);
            SetUnsafe(Bindings.Pause, Keys.Escape);
            SetUnsafe(Bindings.Skip, Keys.Space);
            SetUnsafe(Bindings.ToggleScoreboard, Keys.Tab);
            SetUnsafe(Bindings.ToggleChat, Keys.F8);
            SetUnsafe(Bindings.ToggleExtendedChat, Keys.F9);
            SetUnsafe(Bindings.Screenshot, Keys.F12);
            SetUnsafe(Bindings.IncreaseAudioOffset, Keys.OemPlus);
            SetUnsafe(Bindings.DecreaseAudioOffset, Keys.OemMinus);
            SetUnsafe(Bindings.IncreaseSpeed, Keys.F4);
            SetUnsafe(Bindings.DecreaseSpeed, Keys.F3);
            SetUnsafe(Bindings.ToggleFrameLimiter, Keys.F7);
            SetUnsafe(Bindings.VolumeIncrease, Keys.Up);
            SetUnsafe(Bindings.VolumeDecrease, Keys.Down);
            SetUnsafe(Bindings.DisableMouseButtons, Keys.F10);
            SetUnsafe(Bindings.BossKey, Keys.Insert);
            SetUnsafe(Bindings.SelectTool, Keys.D1);
            SetUnsafe(Bindings.NormalTool, Keys.D2);
            SetUnsafe(Bindings.SliderTool, Keys.D3);
            SetUnsafe(Bindings.SpinnerTool, Keys.D4);
            SetUnsafe(Bindings.NewComboToggle, Keys.Q);
            SetUnsafe(Bindings.WhistleToggle, Keys.W);
            SetUnsafe(Bindings.FinishToggle, Keys.E);
            SetUnsafe(Bindings.ClapToggle, Keys.R);
            SetUnsafe(Bindings.GridSnapToggle, Keys.T);
            SetUnsafe(Bindings.DistSnapToggle, Keys.Y);
            SetUnsafe(Bindings.NoteLockToggle, Keys.L);
            SetUnsafe(Bindings.NudgeLeft, Keys.J);
            SetUnsafe(Bindings.NudgeRight, Keys.K);
            SetUnsafe(Bindings.HelpToggle, Keys.H);
            SetUnsafe(Bindings.JumpToBegin, Keys.Z);
            SetUnsafe(Bindings.PlayFromBegin, Keys.X);
            SetUnsafe(Bindings.AudioPause, Keys.C);
            SetUnsafe(Bindings.JumpToEnd, Keys.V);
            SetUnsafe(Bindings.GridChange, Keys.G);
            SetUnsafe(Bindings.TimingSection, Keys.None);
            SetUnsafe(Bindings.InheritingSection, Keys.None);
            SetUnsafe(Bindings.RemoveSection, Keys.None);
            SetUnsafe(Bindings.Easy, Keys.Q);
            SetUnsafe(Bindings.NoFail, Keys.W);
            SetUnsafe(Bindings.HalfTime, Keys.E);
            SetUnsafe(Bindings.HardRock, Keys.A);
            SetUnsafe(Bindings.SuddenDeath, Keys.S);
            SetUnsafe(Bindings.DoubleTime, Keys.D);
            SetUnsafe(Bindings.Hidden, Keys.F);
            SetUnsafe(Bindings.Flashlight, Keys.G);
            SetUnsafe(Bindings.Relax, Keys.Z);
            SetUnsafe(Bindings.Autopilot, Keys.X);
            SetUnsafe(Bindings.SpunOut, Keys.C);
            SetUnsafe(Bindings.Auto, Keys.V);

            if (!skipConfig)
            {
                string[] bindInternalNames = Enum.GetNames(typeof(Bindings));

                BindingTarget currentTarget = BindingTarget.Universal;
                for (int i = 1; i < bindInternalNames.Length; i++)
                {
                    string na = bindInternalNames[i];
                    if (na[0] == '_')
                        continue;

                    object m;
                    try
                    {
                        if (ConfigManager.Configuration.TryGetValue("key" + na, out m))
                            Set((Bindings)i, (Keys)Enum.Parse(typeof(Keys), m.ToString(), true));
                    }
                    catch { }
                }

                CheckBindings();

                BindingManagerMania.Initialize();
            }
        }

        public static void WriteConfiguration()
        {
            foreach (KeyValuePair<Bindings, Keys> na in BindingDictionary)
                ConfigManager.Configuration["key" + na.Key] = na.Value.ToString();

            BindingManagerMania.WriteConfiguration();
        }

        public static Bindings CheckKey(Keys k)
        {
            return For(k);
        }

        public static bool CheckKey(Keys k, Bindings b)
        {
            return For(b) == k;
        }

        internal static bool CheckValidPlayKey(Keys k, PlayModes Mode)
        {
            switch (Mode)
            {
                case PlayModes.Osu:
                    if (k == For(Bindings.OsuLeft)) return true;
                    if (k == For(Bindings.OsuRight)) return true;
                    break;
                case PlayModes.Taiko:
                    if (k == For(Bindings.TaikoInnerLeft)) return true;
                    if (k == For(Bindings.TaikoInnerRight)) return true;
                    if (k == For(Bindings.TaikoOuterLeft)) return true;
                    if (k == For(Bindings.TaikoOuterRight)) return true;
                    break;
                case PlayModes.CatchTheBeat:
                    if (k == For(Bindings.FruitsLeft)) return true;
                    if (k == For(Bindings.FruitsRight)) return true;
                    if (k == For(Bindings.FruitsDash)) return true;
                    break;
                case PlayModes.OsuMania:
                    //TODO: Reimplement this?
                    break;
            }

            return false;
        }
    }
}
