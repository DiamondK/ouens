﻿//#define textbox_debug

using System;
using System.Drawing;
using System.Windows.Forms;
using osu.Graphics.UserInterface;
using osu_common.Helpers;



namespace osu.Input
{
    public partial class TextInputControl : UserControl
    {
#if textbox_debug && DEBUG
        internal const int DISPLAY_OFFSET = 0;
#else
        internal const int DISPLAY_OFFSET = 5000;
#endif
        internal TextInputControl(pTextBox textBox)
        {
            InitializeComponent();

            pTextBox = textBox;

            TextBox.Font = new System.Drawing.Font(General.FONT_FACE_DEFAULT, textBox.Box.TextSize * 1.14f);
            SetLocation();

            TextBox.ReadOnly = true;

            TextBox.KeyDown += TextBox_KeyDown;
            TextBox.KeyUp += TextBox_KeyUp;
            TextBox.OnNewImeComposition += TextBox_OnNewImeComposition;
            TextBox.OnImeActivity += new BoolDelegate(TextBox_OnImeActivity);
        }

        void TextBox_OnImeActivity(bool b)
        {
            if (b)
            {
                if (OnImeStart != null) OnImeStart();
                return;
            }

            if (OnImeEnd != null) OnImeEnd();
        }

        void TextBox_OnNewImeComposition(string s)
        {
            if (OnNewImeComposition != null)
                OnNewImeComposition(s);
        }

        internal void SetLocation()
        {
            float length = pTextBox.Box.TextBounds.X * GameBase.WindowRatio;
            //TextBox.Size = new System.Drawing.Size((int)length, Font.Height);
#if textbox_debug && DEBUG
            Size = new System.Drawing.Size((int)length, (int)(TextBox.Font.Height));
#else
            Size = new System.Drawing.Size((int)length, 0);
#endif

            Location = new System.Drawing.Point((int)(pTextBox.Box.CurrentPositionActual.X * GameBase.WindowRatio), (int)(pTextBox.Box.CurrentPositionActual.Y * GameBase.WindowRatio) + TextInputControl.DISPLAY_OFFSET);
        }

        public override string Text
        {
            get
            {
                return TextBox.Text;
            }
            set
            {
                TextBox.Text = value;
                if (TextBox.Text.Length >= 1)
                    TextBox.SelectionStart = TextBox.Text.Length;
            }
        }

        public override bool Focused
        {
            get
            {
                return base.Focused || TextBox.Focused;
            }
        }

        internal bool AcceptText
        {
            get { return !TextBox.ReadOnly; }
            set
            {
                GameBase.Scheduler.Add(delegate
                {
                    TextBox.ReadOnly = !value;

                    if (value)
                    {
                        InputManager.AddTextInputControl(this);
                    }
                    else
                    {
                        InputManager.RemoveTextInputControl(this);
                    }
                });
            }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (OnKey != null)
                OnKey(sender, e);

            if (e.Control && e.Alt && e.KeyValue >= 48 && e.KeyValue <= 57)
                return; //AltGr range

            if (e.Control && e.KeyCode == Keys.Back)
            {
                if (Text.Length > 0 && TextBox.SelectionLength == 0)
                {
                    int removalEnd = Math.Max(0, TextBox.SelectionStart);
                    int removalBegin = removalEnd;

                    // Skip all spaces
                    while (removalBegin > 0 && Char.IsWhiteSpace(Text[removalBegin - 1]))
                    {
                        --removalBegin;
                    }

                    // Skip everything that's not a space
                    while (removalBegin > 0 && !Char.IsWhiteSpace(Text[removalBegin - 1]))
                    {
                        --removalBegin;
                    }

                    Text = Text.Remove(removalBegin, removalEnd - removalBegin);

                    TextBox.SelectionStart = removalBegin;
                    pTextBox.UpdateCursorPosition();
                }
                e.SuppressKeyPress = true;
                return;
            }

            if (e.Alt && !e.Control)
            {
                switch (e.KeyCode)
                {
                    //These are keys that can activate the editor menu in combination with Alt.
                    case Keys.F:
                    case Keys.D:
                    case Keys.E:
                    case Keys.V:
                    case Keys.O:
                    case Keys.T:
                    case Keys.W:
                    case Keys.H:
                    case Keys.F4:
                        return;
                }

                e.SuppressKeyPress = true;
                return;
            }

            if (e.Shift && e.KeyCode == Keys.Delete)
            {
                //Shift-Delete gets sent through to game - don't let it be handled by textbox.
                e.SuppressKeyPress = true;
                return;
            }

            if (SuppressLeftRightKeys && (e.KeyCode == Keys.Left || e.KeyCode == Keys.Right))
            {
                e.SuppressKeyPress = true;
                return;
            }


            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Escape || e.KeyCode == Keys.Up || e.KeyCode == Keys.Down ||
                e.KeyCode == Keys.F10 || e.KeyCode == Keys.F12 || e.KeyCode == Keys.F5)
            {
                //Common game shortcuts.
                e.SuppressKeyPress = true;
                return;
            }
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (OnCandidateChange != null)
                OnCandidateChange();
        }

        internal event EventHandler OnNewText;

        internal event StringDelegate OnNewImeComposition;
        internal event VoidDelegate OnImeEnd;
        internal event VoidDelegate OnImeStart;
        internal event VoidDelegate OnCandidateChange;

        private void TextInputControl_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Tab)
                e.IsInputKey = false;

        }

        private void TextBox_Leave(object sender, EventArgs e)
        {
            InputManager.CheckTextInputFocus(true);
        }

        protected override void OnGotFocus(EventArgs e)
        {
            TextBox.Focus();
            if (TextBox.Text.Length >= 1)
                TextBox.SelectionStart = TextBox.Text.Length;
            base.OnGotFocus(e);
        }

        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            if (!AcceptText) return;

            if (OnNewText != null)
                OnNewText(null, null);
        }

        public event KeyEventHandler OnKey;
        internal Graphics.UserInterface.pTextBox pTextBox;
        internal bool SuppressLeftRightKeys;
    }
}