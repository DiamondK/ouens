﻿using System.Drawing;

namespace osu.Input
{
    partial class TextInputControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            InputManager.RemoveTextInputControl(this);

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBox = new osu.Input.ImeTextBox();
            this.SuspendLayout();
            // 
            // TextBox
            // 
            this.TextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBox.CausesValidation = false;
            this.TextBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextBox.HideSelection = false;
            this.TextBox.Location = new System.Drawing.Point(0, 0);
            this.TextBox.Name = "TextBox";
            this.TextBox.Size = new System.Drawing.Size(227, 20);
            this.TextBox.TabIndex = 0;
            this.TextBox.TabStop = false;
            this.TextBox.WordWrap = false;
            this.TextBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            this.TextBox.Leave += new System.EventHandler(this.TextBox_Leave);
            // 
            // TextInputControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CausesValidation = false;
            this.Controls.Add(this.TextBox);
            this.Location = new System.Drawing.Point(0, 0); //-20);
            this.Name = "TextInputControl";
            this.Size = new System.Drawing.Size(227, 20);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.TextInputControl_PreviewKeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal ImeTextBox TextBox;
    }
}
