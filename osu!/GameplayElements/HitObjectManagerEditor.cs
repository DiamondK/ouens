﻿using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Fruits;
using osu.Graphics.Skinning;
using osu.GameplayElements.HitObjects.Osu;
using osu.Audio;
using System;
using Un4seen.Bass;
using Microsoft.Xna.Framework;

namespace osu.GameplayElements
{
    internal class HitObjectManagerEditor : HitObjectManager
    {
        internal HitObjectManagerEditor(bool withSpriteManager = true)
            : base(withSpriteManager)
        {
        }

        internal override HitFactory CreateHitFactory()
        {
            return new HitFactoryOsu(this);
        }

        protected override void AddSlider(Slider s)
        {
            //convert old style slider to hold
            if (Beatmap.PlayMode == osu_common.PlayModes.OsuMania)
            {
                HitCircleHold h = new HitCircleHold(this, s.Position, s.StartTime, false, s.Whistle, s.Finish, s.Clap, 0);
                h.EndTime = ((SliderOsu)s).VirtualEndTime;
                h.SampleAddr = s.SampleAddr;
                h.SampleFile = s.SampleFile;
                h.SampleSet = s.SampleSet;
                h.SampleSetAdditions = s.SampleSetAdditions;
                h.SampleVolume = s.SampleVolume;
                h.CustomSampleSet = s.CustomSampleSet;
                Add(h, false);
                s.Dispose();
            }
            else
                Add(s, false);
        }

        bool sliderActive = false;
        internal override void UpdateHitObjects()
        {
            sliderActive = false;
            base.UpdateHitObjects();

            if (!sliderActive && AudioEngine.SlideSamplesPlaying)
                AudioEngine.StopSlideSamples();
        }

        protected override void UpdateHitObject(HitObject currHitObject)
        {
            SliderOsu slider = currHitObject as SliderOsu;

            if (slider != null)
            {
                if (AudioEngine.Time >= slider.StartTime && AudioEngine.Time <= slider.EndTime)
                {
                    slider.InitSlide();
                    sliderActive = true;
                }
            }

            if (AudioEngine.AudioState == AudioStates.Playing && AudioEngine.FrameAverage < 300)
            {
                if ((currHitObject.StartTime <= AudioEngine.Time &&
                     currHitObject.StartTime >= AudioEngine.TimeLastFrame &&
                     !currHitObject.IsType(HitObjectType.Spinner))
                    ||
                    (currHitObject.EndTime <= AudioEngine.Time &&
                     currHitObject.EndTime >= AudioEngine.TimeLastFrame))
                {
                    if (!currHitObject.Sounded)
                    {
                        currHitObject.PlaySound();
                        currHitObject.Sounded = true;
                    }
                }
                else if (currHitObject.Sounded &&
                         Math.Abs(currHitObject.StartTime - AudioEngine.Time) > AudioEngine.FrameAverage * 4)
                    currHitObject.Sounded = false;
            }

            base.UpdateHitObject(currHitObject);
        }

        //special for bemani
        internal override void AddSpecial(HitObject h)
        {
            HitObject eh = hitObjects.Find(x => x.StartTime <= h.StartTime && x.EndTime >= h.EndTime && x.Position.X == h.Position.X);
            if (eh != null)
            {
                Remove(eh, false);
            }
            Add(h, false);
        }

        internal override void RemoveSpecial(HitObject h)
        {
            HitObject eh = hitObjects.Find(x => x!=h && x.StartTime <= h.StartTime && x.EndTime >= h.EndTime && x.Position.X == h.Position.X);
            if (eh != null )
            {
                Remove(eh, false);
            }
        }
    }
}