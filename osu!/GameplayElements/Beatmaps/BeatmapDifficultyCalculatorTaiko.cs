﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using osu.GameModes.Edit.AiMod;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Taiko;
using osu_common;

namespace osu.GameplayElements.Beatmaps
{
    public class BeatmapDifficultyCalculatorTaiko : BeatmapDifficultyCalculator
    {
        public BeatmapDifficultyCalculatorTaiko(Beatmap Beatmap)
            : base(Beatmap)
        {
        }

        protected override PlayModes PlayMode
        {
            get
            {
                return PlayModes.Taiko;
            }
        }

        static public Mods RelevantMods
        {
            get { return Mods.DoubleTime | Mods.HalfTime | Mods.HardRock | Mods.Easy; }
        }

        internal override HitObjectManager NewHitObjectManager()
        {
            return new HitObjectManagerTaiko(false);
        }

        private const double STAR_SCALING_FACTOR = 0.04125;


        List<DifficultyHitObjectTaiko> DifficultyHitObjects;

        protected override double ComputeDifficulty(Dictionary<String, String> CategoryDifficulty)
        {
            // Fill our custom DifficultyHitObject class, that carries additional information
            DifficultyHitObjects = new List<DifficultyHitObjectTaiko>(HitObjects.Count);

            foreach (HitObject hitObject in HitObjects)
            {
                DifficultyHitObjects.Add(new DifficultyHitObjectTaiko(hitObject));
            }

            // Sort DifficultyHitObjects by StartTime of the HitObjects - just to make sure. Not using CompareTo, since it results in a crash (HitObjectBase inherits MarshalByRefObject)
            DifficultyHitObjects.Sort((a, b) => a.BaseHitObject.StartTime - b.BaseHitObject.StartTime);

            if (CalculateStrainValues() == false)
            {
                return 0;
            }

            double StarRating = CalculateDifficulty() * STAR_SCALING_FACTOR;

            if (CategoryDifficulty != null)
            {
                CategoryDifficulty.Add("Strain", StarRating.ToString("0.00", GameBase.nfi));
                CategoryDifficulty.Add("Hit window 300", (HitObjectManager.HitWindow300 / TimeRate).ToString("0.00", GameBase.nfi));
            }

            return StarRating;
        }

        protected bool CalculateStrainValues()
        {
            // Traverse hitObjects in pairs to calculate the strain value of NextHitObject from the strain value of CurrentHitObject and environment.
            List<DifficultyHitObjectTaiko>.Enumerator HitObjectsEnumerator = DifficultyHitObjects.GetEnumerator();
            if (HitObjectsEnumerator.MoveNext() == false)
            {
                return false;
            }

            DifficultyHitObjectTaiko CurrentHitObject = HitObjectsEnumerator.Current;
            DifficultyHitObjectTaiko NextHitObject;

            // First hitObject starts at strain 1. 1 is the default for strain values, so we don't need to set it here. See DifficultyHitObject.

            while (HitObjectsEnumerator.MoveNext())
            {
                NextHitObject = HitObjectsEnumerator.Current;
                NextHitObject.CalculateStrains(CurrentHitObject, TimeRate);
                CurrentHitObject = NextHitObject;
            }

            return true;
        }



        // In milliseconds. For difficulty calculation we will only look at the highest strain value in each time interval of size STRAIN_STEP.
        // This is to eliminate higher influence of stream over aim by simply having more HitObjects with high strain.
        // The higher this value, the less strains there will be, indirectly giving long beatmaps an advantage.
        protected const double STRAIN_STEP = 400;

        // The weighting of each strain value decays to 0.9 * it's previous value
        protected const double DECAY_WEIGHT = 0.9;

        protected double CalculateDifficulty()
        {
            double ActualStrainStep = STRAIN_STEP * TimeRate;

            // Find the highest strain value within each strain step
            List<double> HighestStrains = new List<double>();
            double IntervalEndTime = ActualStrainStep;
            double MaximumStrain = 0; // We need to keep track of the maximum strain in the current interval

            DifficultyHitObjectTaiko PreviousHitObject = null;
            foreach (DifficultyHitObjectTaiko hitObject in DifficultyHitObjects)
            {
                // While we are beyond the current interval push the currently available maximum to our strain list
                while (hitObject.BaseHitObject.StartTime > IntervalEndTime)
                {
                    HighestStrains.Add(MaximumStrain);

                    // The maximum strain of the next interval is not zero by default! We need to take the last hitObject we encountered, take its strain and apply the decay
                    // until the beginning of the next interval.
                    if (PreviousHitObject == null)
                    {
                        MaximumStrain = 0;
                    }
                    else
                    {
                        double Decay = Math.Pow(DifficultyHitObjectTaiko.DECAY_BASE, (double)(IntervalEndTime - PreviousHitObject.BaseHitObject.StartTime) / 1000);
                        MaximumStrain = PreviousHitObject.Strain * Decay;
                    }

                    // Go to the next time interval
                    IntervalEndTime += ActualStrainStep;
                }

                // Obtain maximum strain
                if (hitObject.Strain > MaximumStrain)
                {
                    MaximumStrain = hitObject.Strain;
                }

                PreviousHitObject = hitObject;
            }

            // Build the weighted sum over the highest strains for each interval
            double Difficulty = 0;
            double Weight = 1;
            HighestStrains.Sort((a, b) => b.CompareTo(a)); // Sort from highest to lowest strain.

            foreach (double Strain in HighestStrains)
            {
                Difficulty += Weight * Strain;
                Weight *= DECAY_WEIGHT;
            }

            return Difficulty;
        }
    }
}
