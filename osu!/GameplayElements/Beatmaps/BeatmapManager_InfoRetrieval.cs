﻿using System;
using System.Collections.Generic;
using System.Text;
using osu_common.Helpers;
using osu.Graphics.Notifications;
using osu_common.Libraries.NetLib;
using osu.Configuration;
using osu.Online;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using osu.Helpers;
using osu.GameModes.Select;
using System.Diagnostics;

namespace osu.GameplayElements.Beatmaps
{
    internal static partial class BeatmapManager
    {
        internal static List<Beatmap> BeatmapInfoSendListAll;
        internal static List<Beatmap> BeatmapInfoSendListPartial;

        /// <summary>
        /// Used to temporarily store a list of baetmaps which are yet to have their online information retrieved.
        /// </summary>
        private static List<Beatmap> NewFilesList = new List<Beatmap>();

        private static bool pulledOnlineStatsOnce;

        internal static bool OnlineFavouritesLoaded;

        internal static event EventHandler NewBeatmapInfo;

        internal static void GetOnlineBeatmapInfo(Beatmap singleBeatmap)
        {
            GetOnlineBeatmapInfo(new List<Beatmap>() { singleBeatmap });
        }

        internal static void GetOnlineBeatmapInfo()
        {
            long tick = DateTime.Now.Ticks;

            const long check_interval = 10000L * 1000 * 60 * 60 * 24; //24 hours

            List<Beatmap> bm = Beatmaps.FindAll(b => b.BeatmapPresent && tick - b.LastInfoUpdate > check_interval && (b.BeatmapTopicId == 0 || b.PlayerRank == Rankings.N || b.SubmissionStatus == SubmissionStatus.Unknown));
            GetOnlineBeatmapInfo(bm);
        }

        internal static void GetOnlineBeatmapInfo(List<Beatmap> incoming)
        {
            if (incoming == null)
                return;

            List<Beatmap> localIncoming = new List<Beatmap>(incoming);
            GameBase.Scheduler.Add(delegate
            {
                if (BeatmapInfoSendListAll != null || BeatmapInfoSendListPartial != null || localIncoming == null)
                    return;

                List<Beatmap> sendList = new List<Beatmap>(localIncoming);
                sendList.Sort((a, b) => a.Filename.CompareTo(b.Filename));
                sendList.RemoveAll(b => b.BeatmapChecksum == null);

                BeatmapInfoSendListAll = sendList;

                getOnlineBeatmapInfoForQueued();
            });
        }

        const int maps_per_request = 80;

        private static void getOnlineBeatmapInfoForQueued()
        {
            if (BeatmapInfoSendListAll == null)
                return;

            if (GameBase.Mode == OsuModes.Play)
            {
                //delay if playing
                GameBase.Scheduler.AddDelayed(getOnlineBeatmapInfoForQueued, 4000);
                return;
            }

            // i351: Allocate maps_per_request of each to eat slightly more memory to avoid
            // having to grow the list possibly many times as it iterates.
            bBeatmapInfoRequest req = new bBeatmapInfoRequest(maps_per_request, maps_per_request);

            int count = BeatmapInfoSendListAll.Count;

            if (count < maps_per_request)
            {
                BeatmapInfoSendListPartial = new List<Beatmap>(BeatmapInfoSendListAll);
                BeatmapInfoSendListAll = null;
            }
            else
            {
                // i351: Pull entries from the end of the list to avoid a linear-time shift every pull.
                BeatmapInfoSendListPartial = new List<Beatmap>(BeatmapInfoSendListAll.GetRange(count - maps_per_request, maps_per_request));
                BeatmapInfoSendListAll.RemoveRange(count - maps_per_request, maps_per_request);
            }


            // BeatmapIds from the .osu files are often _wrong_. Until this is somehow fixed don't use them for info retrieval.
            foreach (Beatmap b in BeatmapInfoSendListPartial)
            {
                /*if (b.BeatmapId > 0)
                {
                    req.ids.Add(b.BeatmapId);
                }
                else*/
                {
                    string filename = Path.GetFileName(b.Filename);
                    if (!string.IsNullOrEmpty(filename))
                        req.filenames.Add(filename);
                }
            }

            //BeatmapInfoSendListPartial.RemoveAll(b => b.BeatmapId > 0); //don't use these when receiving data back.

            Debug.Print("Requested details for " + req.filenames.Count + " (filename) plus " + req.ids.Count + " (beatmap_id) beatmaps...");

            BanchoClient.SendRequest(RequestType.Osu_BeatmapInfoRequest, req);
        }

        internal static void IncomingBeatmapInfoReply(bBeatmapInfoReply reply)
        {
            try
            {
                //Transfer to a local copy.
                List<Beatmap> requestList = BeatmapInfoSendListPartial;
                BeatmapInfoSendListPartial = null;

                long tick = DateTime.Now.Ticks;

                if (requestList == null)
                    return;

                foreach (Beatmap b in requestList)
                {
                    b.SubmissionStatus = SubmissionStatus.NotSubmitted;
                    if (GameBase.User.Name != b.Creator)
                        b.BeatmapSetId = -1;
                    b.LastInfoUpdate = tick;
                    b.ClearRankings();
                }

                foreach (bBeatmapInfo bmi in reply.beatmapInfo)
                {
                    Beatmap b = bmi.id >= 0 ? requestList[bmi.id] : GetBeatmapById(bmi.beatmapId);

                    if (b == null)
                        continue;

                    if (bmi.checksum != b.BeatmapChecksum)
                        b.UpdateAvailable = true;

                    switch (bmi.ranked)
                    {
                        case 2:
                            b.SubmissionStatus = SubmissionStatus.Approved;
                            break;
                        case 1:
                            b.SubmissionStatus = SubmissionStatus.Ranked;
                            break;
                        default:
                            b.SubmissionStatus = SubmissionStatus.Pending;
                            break;
                    }

                    b.BeatmapId = bmi.beatmapId;
                    b.BeatmapSetId = bmi.beatmapSetId;
                    b.BeatmapTopicId = bmi.threadId;
                    b.PlayerRankOsu = bmi.osuRank;
                    b.PlayerRankFruits = bmi.fruitsRank;
                    b.PlayerRankTaiko = bmi.taikoRank;
                    b.PlayerRankMania = bmi.maniaRank;
                }

                if (NewBeatmapInfo != null)
                    NewBeatmapInfo(null, null);

                getOnlineBeatmapInfoForQueued();
            }
            catch (Exception e)
            {
                ErrorSubmission.Submit(new OsuError(e) { Feedback = "beatmap info reply" });
            }

            Debug.Print("Received details for " + reply.beatmapInfo.Count + " beatmaps!");
        }

        public static event VoidDelegate OnOnlineFavouritesLoaded;

        /// <summary>
        /// Invokes the on online favourites loaded.
        /// Note that this removes event bindings after called automatically.
        /// </summary>
        private static void InvokeOnOnlineFavouritesLoaded()
        {
            VoidDelegate loaded = OnOnlineFavouritesLoaded;
            if (loaded != null) loaded();

            OnOnlineFavouritesLoaded = null;
        }

        public static void LoadOnlineFavourites()
        {
            if (OnlineFavouritesLoaded)
                return;
            OnlineFavouritesLoaded = true;

            NotificationManager.ShowMessageMassive("Loading online favourites...", 1000);

            StringNetRequest dnr = new StringNetRequest(string.Format(Urls.GET_FAVOURITES, ConfigManager.sUsername, ConfigManager.sPassword));
            dnr.onFinish += delegate(string data, Exception e)
            {
                foreach (string s in data.Split('\n'))
                {
                    if (s.Length == 0) continue;

                    int beatmapSetId = Int32.Parse(s);
                    Beatmaps.FindAll(b => b.BeatmapSetId == beatmapSetId).ForEach(b => b.OnlineFavourite = true);
                }

                InvokeOnOnlineFavouritesLoaded();
            };
            NetManager.AddRequest(dnr);

        }

    }
}
