using System.Collections.Generic;
using System.IO;

namespace osu.GameplayElements.Beatmaps
{
    internal class BeatmapTreeLevel
    {
        internal List<Beatmap> Beatmaps;
        internal List<BeatmapTreeLevel> Children;
        internal int Level;
        internal string Name;
        internal string FullPath;
        internal BeatmapTreeLevel Parent;

        internal BeatmapTreeLevel(string fullPath, BeatmapTreeLevel parent)
        {
            string[] pathElements = fullPath.Split(Path.DirectorySeparatorChar);
            Name = pathElements[pathElements.Length - 1];
            FullPath = fullPath;
            Children = new List<BeatmapTreeLevel>();
            Beatmaps = new List<Beatmap>();
            Parent = parent;

            BeatmapTreeLevel test = this;
            while (test.Parent != null)
            {
                Level++;
                test = test.Parent;
            }
        }
    }
}