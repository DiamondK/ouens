﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using osu.GameModes.Edit.AiMod;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Fruits;
using osu_common;

namespace osu.GameplayElements.Beatmaps
{
    internal class BeatmapDifficultyCalculatorFruits : BeatmapDifficultyCalculator
    {
        public BeatmapDifficultyCalculatorFruits(Beatmap Beatmap)
            : base(Beatmap)
        {
        }

        protected override PlayModes PlayMode
        {
            get
            {
                return PlayModes.CatchTheBeat;
            }
        }

        static public Mods RelevantMods
        {
            get { return Mods.DoubleTime | Mods.HalfTime | Mods.HardRock | Mods.Easy; }
        }

        internal override HitObjectManager NewHitObjectManager()
        {
            return new HitObjectManagerFruits(false);
        }


        List<DifficultyHitObjectFruits> DifficultyHitObjects;

        private const double STAR_SCALING_FACTOR = 0.145;
        private const float PLAYFIELD_WIDTH = 512;


        protected override bool ModsRequireReload(Mods Mods)
        {
            // If we switch to hardrock or away from hardrock, then we need to reload!
            if (((HitObjectManager.ActiveMods ^ Mods) & Mods.HardRock) > 0)
            {
                return true;
            }

            return false;
        }

        protected override double ComputeDifficulty(Dictionary<String, String> CategoryDifficulty)
        {
            // Fill our custom DifficultyHitObject class, that carries additional information
            DifficultyHitObjects = new List<DifficultyHitObjectFruits>(HitObjects.Count);

            float catcherWidth = 305 / GameBase.GamefieldRatio * HitObjectManager.SpriteRatio * 0.7f;
            float catcherWidthHalf = catcherWidth / 2;

            HitObjectManagerFruits hom = HitObjectManager as HitObjectManagerFruits;
            hom.InitializeHyperDash(catcherWidth);

            catcherWidthHalf *= 0.8f;

            foreach (HitObject hitObject in HitObjects)
            {
                // We want to only consider fruits that contribute to the combo. Droplets are addressed as accuracy and spinners are not relevant for "skill" calculations.
                if (!(hitObject is HitCircleFruitsTickTiny) && !(hitObject is HitCircleFruitsSpin))
                {
                    DifficultyHitObjects.Add(new DifficultyHitObjectFruits(hitObject, catcherWidthHalf));
                }
            }

            // Sort DifficultyHitObjects by StartTime of the HitObjects - just to make sure. Not using CompareTo, since it results in a crash (HitObjectBase inherits MarshalByRefObject)
            DifficultyHitObjects.Sort((a, b) => a.BaseHitObject.StartTime - b.BaseHitObject.StartTime);

            if (CalculateStrainValues() == false)
            {
                return 0;
            }

            double StarRating = Math.Sqrt(CalculateDifficulty()) * STAR_SCALING_FACTOR;

            if (CategoryDifficulty != null)
            {
                CategoryDifficulty.Add("Aim", StarRating.ToString("0.00", GameBase.nfi));

                double PreEmpt = HitObjectManager.PreEmpt / TimeRate;

                CategoryDifficulty.Add("AR", (PreEmpt > 1200.0 ? -(PreEmpt - 1800.0) / 120.0 : -(PreEmpt - 1200.0) / 150.0 + 5.0).ToString("0.00", GameBase.nfi));
                CategoryDifficulty.Add("Max combo", DifficultyHitObjects.Count.ToString(GameBase.nfi));
            }

            return StarRating;
        }



        protected bool CalculateStrainValues()
        {
            // Traverse hitObjects in pairs to calculate the strain value of NextHitObject from the strain value of CurrentHitObject and environment.
            List<DifficultyHitObjectFruits>.Enumerator HitObjectsEnumerator = DifficultyHitObjects.GetEnumerator();
            if (HitObjectsEnumerator.MoveNext() == false)
            {
                return false;
            }

            DifficultyHitObjectFruits CurrentHitObject = HitObjectsEnumerator.Current;
            DifficultyHitObjectFruits NextHitObject;

            // First hitObject starts at strain 1. 1 is the default for strain values, so we don't need to set it here. See DifficultyHitObject.

            while (HitObjectsEnumerator.MoveNext())
            {
                NextHitObject = HitObjectsEnumerator.Current;
                NextHitObject.CalculateStrains(CurrentHitObject, TimeRate);
                CurrentHitObject = NextHitObject;
            }

            return true;
        }

        // In milliseconds. For difficulty calculation we will only look at the highest strain value in each time interval of size STRAIN_STEP.
        // This is to eliminate higher influence of stream over aim by simply having more HitObjects with high strain.
        // The higher this value, the less strains there will be, indirectly giving long beatmaps an advantage.
        protected const double STRAIN_STEP = 750;

        // The weighting of each strain value decays to 0.9 * it's previous value
        protected const double DECAY_WEIGHT = 0.94;

        protected double CalculateDifficulty()
        {
            // The strain step needs to be adjusted for the algorithm to be considered equal with speed changing mods
            double ActualStrainStep = STRAIN_STEP * TimeRate;

            // Find the highest strain value within each strain step
            List<double> HighestStrains = new List<double>();
            double IntervalEndTime = ActualStrainStep;
            double MaximumStrain = 0; // We need to keep track of the maximum strain in the current interval

            DifficultyHitObjectFruits PreviousHitObject = null;
            foreach (DifficultyHitObjectFruits hitObject in DifficultyHitObjects)
            {
                // While we are beyond the current interval push the currently available maximum to our strain list
                while (hitObject.BaseHitObject.StartTime > IntervalEndTime)
                {
                    HighestStrains.Add(MaximumStrain);

                    // The maximum strain of the next interval is not zero by default! We need to take the last hitObject we encountered, take its strain and apply the decay
                    // until the beginning of the next interval.
                    if (PreviousHitObject == null)
                    {
                        MaximumStrain = 0;
                    }
                    else
                    {
                        double Decay = Math.Pow(DifficultyHitObjectFruits.DECAY_BASE, (double)(IntervalEndTime - PreviousHitObject.BaseHitObject.StartTime) / 1000);
                        MaximumStrain = PreviousHitObject.Strain * Decay;
                    }

                    // Go to the next time interval
                    IntervalEndTime += ActualStrainStep;
                }

                // Obtain maximum strain
                if (hitObject.Strain > MaximumStrain)
                {
                    MaximumStrain = hitObject.Strain;
                }

                PreviousHitObject = hitObject;
            }

            // Build the weighted sum over the highest strains for each interval
            double Difficulty = 0;
            double Weight = 1;
            HighestStrains.Sort((a, b) => b.CompareTo(a)); // Sort from highest to lowest strain.

            foreach (double Strain in HighestStrains)
            {
                Difficulty += Weight * Strain;
                Weight *= DECAY_WEIGHT;
            }

            return Difficulty;
        }
    }
}
