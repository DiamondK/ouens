﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameModes.Select;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Online;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu_common.Helpers;
using osu.Helpers;
using osu_common.Libraries.NetLib;
using osu_common.Libraries.Osz2;
using osu.Graphics.Skinning;
using Amib.Threading;


namespace osu.GameplayElements.Beatmaps
{
    public delegate void GotOnlineScoresHandler(object sender);

    internal static partial class BeatmapManager
    {
        //todo: this is a bit ugly. combine them and use Path.Combine() at some point.
        internal static string SONGS_DIRECTORY { get { return GameBase.BeatmapDirectory + Path.DirectorySeparatorChar; } }
        internal static string FAILED_DIRECTORY { get { return SONGS_DIRECTORY + @"Failed" + Path.DirectorySeparatorChar; } }

        internal static List<Beatmap> Beatmaps;
        private static Dictionary<string, Beatmap> BeatmapsByChecksum;
        private static Dictionary<int, Beatmap> BeatmapsById;
        internal static List<string> ChangedFolders = new List<string>();
        internal static List<string> ChangedPackages = new List<string>();

        internal static int DatabaseVersion;

        internal static BeatmapTreeLevel Root;

        /// <summary>
        /// This should be a count of the total number of directories (.osz) and files (.osz2). In short, the number of beatmapsets loaded into osu!
        /// TODO: Make sure this is getting updated in all places it is incremented/decremented.
        /// </summary>
        public static int FolderFileCount;

        /// <summary>
        /// True after songs have been loaded at least once (usually from the deserialised database).
        /// </summary>
        private static bool InitialLoadComplete;

        internal static Beatmap lastAddedMap;

        internal static string DATABASE_FILENAME = @"osu!.db";

        internal static FileSystemWatcher SongWatcher;

        internal static int TotalAudioOnly;
        internal static int TotalUniqueSets;

        private static Beatmap _current;
        internal static Beatmap Current
        {
            get { return _current; }
            set
            {
                if (_current == value) return;

                //Run some cleanups on the current map as it is swapped out.
                if (_current != null)
                {
                    _current.Cleanup();
                    SkinManager.ClearBeatmapCache(true);
                }

                _current = value;
            }
        }


        internal static double MapRange(double difficulty, double min, double mid, double max)
        {
            if (difficulty > 5)
                return mid + (max - mid) * (difficulty - 5) / 5;
            if (difficulty < 5)
                return mid - (mid - min) * (5 - difficulty) / 5;
            return mid;
        }

        internal static bool SetCurrentFromFilename(string filename)
        {
            Current = GetBeatmapByFilename(filename);
            return Current != null;
        }

        /// <summary>
        /// Gets the beatmap by filename. O(log N)
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns></returns>
        internal static Beatmap GetBeatmapByFilename(string filename)
        {
            Beatmap b = new Beatmap { Filename = filename };
            int index = Beatmaps.BinarySearch(b);
            return index < 0 ? null : Beatmaps[index];
        }

        /// <summary>
        /// Gets the beatmap by checksum. O(1)
        /// </summary>
        /// <param name="hash">The hash.</param>
        /// <returns></returns>
        internal static Beatmap GetBeatmapByChecksum(string hash)
        {
            if (hash == null) return null;

            Beatmap b;
            return BeatmapsByChecksum.TryGetValue(hash, out b) ? b : null;
        }

        /// <summary>
        /// Get the BeatmapTreeLevel for specified path. Create if not existing.
        /// </summary>
        /// <returns></returns>
        internal static BeatmapTreeLevel GetTreeLevel(string path, bool ignoreExisting = false)
        {
            //check for file existence to avoid directories which could be perceived as filenames causing a problem
            if (path.EndsWith(".osz") && File.Exists(path))
                path = Path.GetDirectoryName(path);

            if (path.StartsWith(GameBase.BeatmapDirectory))
                path = path.Substring(GameBase.BeatmapDirectory.Value.Length);

            path = GeneralHelper.PathSanitise(path).TrimStart(Path.DirectorySeparatorChar);

            BeatmapTreeLevel level = Root;
            if (path.Length == 0)
                return level;

            string[] folders = path.Split(Path.DirectorySeparatorChar);

            if (ignoreExisting && folders.Length < 2) //not in a subfolder
            {
                BeatmapTreeLevel tl = new BeatmapTreeLevel(Path.Combine(BeatmapManager.SONGS_DIRECTORY, path), level);
                level.Children.Add(tl);
                return tl;
            }

            switch (folders[folders.Length - 1])
            {
                case "Failed":
                case "SubmissionCache":
                    //ignored folders.
                    return null;
            }

            string builtPath = GameBase.BeatmapDirectory;

            for (int i = 0; i < folders.Length; i++)
            {
                BeatmapTreeLevel tl = level.Children.Find(c => c.Name == folders[i]);

                builtPath = Path.Combine(builtPath, folders[i]);

                if (tl == null)
                {
                    //create one because it doesn't exist.
                    level.Children.Add(tl = new BeatmapTreeLevel(builtPath, level));
                }

                level = tl;
            }

            return level;
        }

        private static void ResetTreeLevel(BeatmapTreeLevel level)
        {
            if (level.Beatmaps.Count > 0)
            {
                //The full path is present so we should dispose of some beatmaps first.
                level.Beatmaps.ForEach(b => Remove(b));

                //Find unique sets
                Dictionary<string, int> setCheck = new Dictionary<string, int>();

                foreach (Beatmap b in level.Beatmaps)
                    if (b.AudioPresent)
                        setCheck[b.ContainingFolder] = 1;
                    else
                        TotalAudioOnly--;

                //Alter some counts.
                TotalUniqueSets -= setCheck.Count;
            }

            if (level.Children.Count > 0)
                level.Children.ForEach(ResetTreeLevel); //propagate down
        }

        internal static void ReloadDictionaries()
        {
            BeatmapsByChecksum.Clear();
            BeatmapsById.Clear();

            foreach (Beatmap b in Beatmaps)
            {
                if (b.BeatmapChecksum != null) BeatmapsByChecksum[b.BeatmapChecksum] = b;
                if (b.BeatmapId > 0) BeatmapsById[b.BeatmapId] = b;
            }
        }

        internal static bool Remove(Beatmap beatmap)
        {
            if (!Beatmaps.Remove(beatmap))
                return false;

            if (beatmap.BeatmapChecksum != null) BeatmapsByChecksum.Remove(beatmap.BeatmapChecksum);
            if (beatmap.BeatmapId > 0) BeatmapsById.Remove(beatmap.BeatmapId);
            return true;
        }

        internal static void DatabaseSerialize()
        {
            //never write a database if we aren't the lead spectator.
            if (GameBase.Tournament && !GameBase.TournamentManager)
                return;

            try
            {
                using (Stream stream = new SafeWriteStream(DATABASE_FILENAME))
                using (SerializationWriter sw = new SerializationWriter(stream))
                {
                    sw.Write(General.VERSION);
                    sw.Write(FolderFileCount);
                    sw.Write(BanchoClient.AllowUserSwitching);
                    sw.Write(BanchoClient.AllowUserSwitchingRestoration);
                    sw.Write(ConfigManager.sUsername);
                    sw.Write(Beatmaps);
                    sw.Write((int)BanchoClient.Permission.Value);
                }
            }
            catch (Exception e)
            {
            }
        }

        private static bool DatabaseDeserialize()
        {
            if (!File.Exists(DATABASE_FILENAME) && File.Exists("osu!.db"))
            {
                //copy real public database across for migration testing.
                File.Copy("osu!.db", DATABASE_FILENAME);
            }

            if (File.Exists(DATABASE_FILENAME))
            {
                try
                {
                    using (Stream stream = File.Open(DATABASE_FILENAME, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        using (SerializationReader sr = new SerializationReader(stream))
                        {
                            DatabaseVersion = sr.ReadInt32();
                            FolderFileCount = sr.ReadInt32();
                            BanchoClient.AllowUserSwitching = sr.ReadBoolean();
                            BanchoClient.AllowUserSwitchingRestoration = sr.ReadDateTime().ToLocalTime();

                            string checkUsername = sr.ReadString();
                            if (!BanchoClient.AllowUserSwitching && ConfigManager.sUsername != checkUsername)
                                ConfigManager.sUsername = checkUsername;

                            Beatmaps = (List<Beatmap>)sr.ReadBList<Beatmap>();

                            if (DatabaseVersion >= 20141028 && !BanchoClient.PermissionsReceivedOnce && GameBase.HasLogin)
                                BanchoClient.Permission = (Permissions)sr.ReadInt32();

                            ReloadDictionaries();
                        }
                    }
                }
                catch (Exception e)
                {
                    ErrorSubmission.Submit(new OsuError(e) { Feedback = "database-corrupt" });

                    TopMostMessageBox.Show(
                        "The local beatmap database seems corrupt.  Will create from scratch (this may take a while...).");
                    try
                    {
                        string backupFilename = DATABASE_FILENAME + "." + DateTime.Now.Ticks + ".bak";
                        if (File.Exists(DATABASE_FILENAME) && !File.Exists(backupFilename))
                            File.Move(DATABASE_FILENAME, backupFilename);
                    }
                    catch
                    {
                    }
                }
            }

            return Beatmaps != null;
        }

        private static void DatabasePostProcess()
        {
#if FAKE_MAPS
            for (int i = 0; i < 20000; i++)
            {
                Beatmap b = new Beatmap()
                {
                    Title = "beatmap " + (i / 5).ToString(),
                    Artist = "peppy" + (i / 100).ToString(),
                    Creator = "no one",
                    ContainingFolder = (i / 5).ToString(),
                    Filename = i.ToString() + ".osu",
                    BeatmapPresent = true
                };

                b.PopulateTitleStatics();
                b.AudioPresent = true;

                Beatmaps.Add(b);
            }
#endif

            InitialLoadComplete = true;

            Root.Beatmaps.AddRange(Beatmaps.FindAll(b => b.ContainingFolder.LastIndexOf(Path.DirectorySeparatorChar) < SONGS_DIRECTORY.Length));

            Dictionary<string, BeatmapTreeLevel> treeMatch = new Dictionary<string, BeatmapTreeLevel>();
            foreach (Beatmap bm in Beatmaps)
            {
                BeatmapTreeLevel tl;
                if (!treeMatch.TryGetValue(bm.ContainingFolder, out tl))
                {
                    tl = GetTreeLevel(bm.ContainingFolder, true);
                    treeMatch.Add(bm.ContainingFolder, tl);
                }

                tl.Beatmaps.Add(bm);
            }

            TotalAudioOnly = Beatmaps.FindAll(b => !b.AudioPresent).Count;

            return;
        }

        internal static int Add(Beatmap b)
        {
            int index = Beatmaps.BinarySearch(b);
            if (index >= 0)
            {
                if (Beatmaps[index].InOszContainer == b.InOszContainer)
                    return index;
                Beatmaps.Insert(index, b);
            }
            else
                Beatmaps.Insert(~index, b);

            if (b.BeatmapChecksum != null)
                BeatmapsByChecksum[b.BeatmapChecksum] = b;
            if (b.BeatmapId > 0)
                BeatmapsById[b.BeatmapId] = b;

            return -1;
        }

        public static Beatmap GetBeatmapById(int id)
        {
            if (id <= 0) return null;

            Beatmap b;
            return BeatmapsById.TryGetValue(id, out b) ? b : null;
        }

        public static Beatmap GetBeatmapBySetId(int id)
        {
            if (id <= 0) return null;
            return Beatmaps.Find(b => b.BeatmapSetId == id);
        }

        public static Beatmap GetBeatmapByTitle(string title)
        {
            return Beatmaps.Find(b => b.Title == title);
        }

        public static void DeleteBeatmap(Beatmap b, bool allDifficulties)
        {
            SongWatcher.EnableRaisingEvents = false;

            AudioEngine.FreeMusic();

            if (allDifficulties)
            {
                if (!b.BeatmapPresent || b.ContainingFolder.EndsWith(GameBase.BeatmapDirectory) || b.InOszContainer)
                {
                    try
                    {
                        if (b.InOszContainer)
                        {
                            AudioEngine.Stop();
                            AudioEngine.FreeMusic();
                            Osz2Factory.CloseMapPackage(b.Package);
                            RecycleBin.SendSilent(b.ContainingFolder);
                            FolderFileCount--;
                        }
                        else if (!b.ContainingFolder.EndsWith(GameBase.BeatmapDirectory))
                        {
                            RecycleBin.SendSilent(b.ContainingFolder);
                            FolderFileCount--;
                        }
                        else
                        {
                            b.DeleteFile(b.AudioFilename);
                        }
                    }
                    catch
                    {
                    }
                }
                else
                {
                    try
                    {
                        RecycleBin.SendSilent(b.ContainingFolder);
                        FolderFileCount--;

                    }
                    catch (Exception)
                    {
                    }
                }
            }
            else
            {
                b.DeleteFile(Current.Filename);
            }

            List<Beatmap> matches = allDifficulties ? Beatmaps.FindAll(bm => bm.ContainingFolder == b.ContainingFolder) : new List<Beatmap>() { Current };
            foreach (Beatmap bm in matches)
                Remove(bm);

            SongWatcher.EnableRaisingEvents = true;
        }

        public static event Action<string> OnStatusUpdate;

        public static bool ShowChangeNotifications = true;

        private static void InvokeOnStatusUpdate(string obj)
        {
            Action<string> update = OnStatusUpdate;
            if (update != null) update(obj);
        }

        internal static void Clear()
        {
            Beatmaps.Clear();
            BeatmapsByChecksum.Clear();
            BeatmapsById.Clear();
        }

        internal static void ClearStatusHandlers()
        {
            OnStatusUpdate = null;
        }

        internal static bool AssignStatusHandler(Action<string> action)
        {
            if (action == null) return false;

            var osu = OnStatusUpdate;

            if (osu != null)
                foreach (var v in osu.GetInvocationList())
                    if (v == action) return false;

            OnStatusUpdate += action;
            return true;
        }

        internal static void Load(Beatmap b)
        {
            if (b == null) return;

            BeatmapManager.Current = b;

            AudioEngine.LoadAudio(BeatmapManager.Current, false, false, true);
            AudioEngine.Play();
        }
    }
}