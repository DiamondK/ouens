﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using osu.Audio;
using osu.GameModes.Edit.AiMod;
using osu.GameModes.Play;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu_common;
using osu.GameModes.Play.Rulesets.Mania;

namespace osu.GameplayElements.Beatmaps
{
    public abstract class BeatmapDifficultyCalculator
    {
        internal Beatmap Beatmap;

        protected static readonly Mods[] BaseModCombinations = new Mods[]
        {
            Mods.None,
            Mods.DoubleTime,
            Mods.HalfTime,
            Mods.Easy,
            Mods.Easy | Mods.DoubleTime,
            Mods.Easy | Mods.HalfTime,
            Mods.HardRock,
            Mods.HardRock | Mods.DoubleTime,
            Mods.HardRock | Mods.HalfTime
        };

        public BeatmapDifficultyCalculator(Beatmap Beatmap)
        {
            // Avoid any possible data sharing and thus race conditions by working on a clone of the current beatmap.
            this.Beatmap = Beatmap.Clone();
        }


        internal HitObjectManager HitObjectManager = null;

        public virtual void Dispose()
        {
            if (HitObjectManager != null)
            {
                HitObjectManager.Dispose();
                HitObjectManager = null;
            }
        }

        protected abstract PlayModes PlayMode
        {
            get;
        }


        public virtual Mods[] ModCombinations { get { return BaseModCombinations; } }

        static public Mods MaskRelevantMods(Mods Mods)
        {
            return Mods & (BeatmapDifficultyCalculatorOsu.RelevantMods | BeatmapDifficultyCalculatorTaiko.RelevantMods | BeatmapDifficultyCalculatorFruits.RelevantMods | BeatmapDifficultyCalculatorMania.RelevantMods);
        }

        static public Mods MaskRelevantMods(Mods mods, PlayModes mode, Beatmap map)
        {
            switch (mode)
            {
                case PlayModes.Taiko:
                    return mods & BeatmapDifficultyCalculatorTaiko.RelevantMods;

                case PlayModes.CatchTheBeat:
                    return mods & BeatmapDifficultyCalculatorFruits.RelevantMods;

                case PlayModes.OsuMania:
                    return StageMania.CanonicalKeyMods(map, mods) & BeatmapDifficultyCalculatorMania.RelevantMods;

                default:
                    return mods & BeatmapDifficultyCalculatorOsu.RelevantMods;
            }
        }

        internal abstract HitObjectManager NewHitObjectManager();

        protected virtual bool ModsRequireReload(Mods Mods) { return false; }
        protected virtual void PreprocessHitObjects() { }
        protected abstract double ComputeDifficulty(Dictionary<String, String> CategoryDifficulty);
        

        internal double TimeRate = 1;



        private void LoadTiming()
        {
            int AudioRate;

            // This makes sure, that timing points are read correctly
            if (ModManager.CheckActive(HitObjectManager.ActiveMods, Mods.DoubleTime))
            {
                AudioRate = 150;
            }
            else if (ModManager.CheckActive(HitObjectManager.ActiveMods, Mods.HalfTime))
            {
                AudioRate = 75;
            }
            else
            {
                AudioRate = 100;
            }

            TimeRate = (double)AudioRate / 100.0;
        }


        internal List<HitObject> HitObjects = null;
        private void LoadHitObjects(Mods Mods)
        {
            HitObjects = null; // In case we don't succeed at least we won't have any HitObjects anymore. Better than wrong behaviour if stuff is done on the old HitObjects

            if (HitObjectManager != null)
            {
                HitObjectManager.Dispose();
            }

            HitObjectManager = NewHitObjectManager();
            HitObjectManager.IsDifficultyCalculator = true;

            if (!Beatmap.HeadersLoaded)
            {
                // Don't trust the beatmap id written in the file! It might be wrong! (see the big black for instance)
                int BeatmapId = Beatmap.BeatmapId;
                Beatmap.ProcessHeaders();
                Beatmap.BeatmapId = BeatmapId;
            }

            HitObjectManager.SetBeatmap(Beatmap, Mods);
                
            LoadTiming();
                
            HitObjectManager.UpdateVariables(false, true);

            if (!Beatmap.HeadersLoaded)
            {
                Debug.Print("Beatmap " + Beatmap.BeatmapId + " is corrupted.");
            }
            else
            {
                HitObjectManager.parse(FileSection.HitObjects, false);
                HitObjects = new List<HitObject>(HitObjectManager.hitObjects);

                // This needs to be executed here, because later on the static objects will potentially contain wrong information (e.g. AudioEngine's timing)
                PreprocessHitObjects();
            }
             
        }

        public double GetDifficulty(Mods Mods, Dictionary<string, string> CategoryDifficulty)
        {
            try
            {
                if (HitObjects == null || ModsRequireReload(Mods))
                {
                    LoadHitObjects(Mods);

                    if (HitObjects == null)
                    {
                        return 0;
                    }
                }
                else
                {
                    HitObjectManager.ActiveMods = Mods;
                    LoadTiming();

                    HitObjectManager.UpdateVariables(false, true);
                }

                double Difficulty = ComputeDifficulty(CategoryDifficulty);

                return Difficulty;
            }
            /*catch (System.FormatException)
            {
                Debug.Print("Format exception in: " + Beatmap.Filename);
            }
            catch (System.IndexOutOfRangeException)
            {
                Debug.Print("IndexOutOfRange exception in: " + Beatmap.Filename);
            }
            catch (ArgumentOutOfRangeException)
            {
                Debug.Print("ArgumentOutOfRange exception in: " + Beatmap.Filename);
            }*/
            catch
            {
#if DEBUG
                GameBase.Scheduler.Add(delegate
                {
                    NotificationManager.ShowMessage("Could not compute difficulty of beatmap " + Beatmap.BeatmapId + ".");
                });
#endif
            }

            return 0;
        }
    }
}
