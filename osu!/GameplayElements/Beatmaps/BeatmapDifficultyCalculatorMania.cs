﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using osu.GameModes.Edit.AiMod;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Mania;
using osu.GameplayElements.Scoring;
using osu_common;

namespace osu.GameplayElements.Beatmaps
{
    public class BeatmapDifficultyCalculatorMania : BeatmapDifficultyCalculator
    {
        private Mods keyMod;

        public BeatmapDifficultyCalculatorMania(Beatmap Beatmap, Mods keyMod)
            : base(Beatmap)
        {
            // Let the mania calculator only calculate difficulties for one specific key-mod at a time.
            Debug.Assert((keyMod & ~Mods.KeyMod) == Mods.None);

            // No key-mods for mania-specific maps. Conversions don't exist.
            if (Beatmap.PlayMode == PlayModes.OsuMania)
                keyMod = Mods.None;

            this.keyMod = keyMod;
        }

        protected override PlayModes PlayMode
        {
            get
            {
                return PlayModes.OsuMania;
            }
        }

        static public Mods RelevantMods
        {
            get { return Mods.DoubleTime | Mods.HalfTime | Mods.HardRock | Mods.Easy | Mods.KeyMod; }
        }

        public override Mods[] ModCombinations
        {
            get
            {
                Mods[] mods = new Mods[BaseModCombinations.Length];

                for (int i = 0; i < BaseModCombinations.Length; ++i)
                    mods[i] = keyMod | BaseModCombinations[i];

                return mods;
            }
        }

        internal override HitObjectManager NewHitObjectManager()
        {
            return new HitObjectManagerMania(false);
        }

        // 0.01 is good for stars
        private const double STAR_SCALING_FACTOR = 0.018;


        List<DifficultyHitObjectMania> DifficultyHitObjects;


        protected override bool ModsRequireReload(Mods Mods)
        {
            // If we switch keymods we need to reload
            if ((HitObjectManager.ActiveMods & Mods.KeyMod) != (Mods & Mods.KeyMod))
            {
                return true;
            }

            return false;
        }

        protected override double ComputeDifficulty(Dictionary<String, String> CategoryDifficulty)
        {
            // Fill our custom DifficultyHitObject class, that carries additional information
            DifficultyHitObjects = new List<DifficultyHitObjectMania>(HitObjects.Count);

            foreach (HitObject hitObject in HitObjects)
            {
                //DifficultyHitObjects.Add(new DifficultyHitObjectMania(hitObject));
                if (hitObject is HitCircleManiaRow)
                {
                    ((HitCircleManiaRow)hitObject).HitObjects.ForEach(n =>
                    {
                        DifficultyHitObjects.Add(new DifficultyHitObjectMania((HitCircleMania)n)); // This hitcircle should ALWAYS be of type HitCircleMania
                    });
                }
                else if (hitObject is HitCircleMania)
                {
                    DifficultyHitObjects.Add(new DifficultyHitObjectMania((HitCircleMania)hitObject));
                }
                else
                {
                    Debug.Print("Unknown mania circle type at: " + hitObject.StartTime);
                }
            }


            // Sort DifficultyHitObjects by StartTime of the HitObjects - just to make sure. Not using CompareTo, since it results in a crash (HitObjectBase inherits MarshalByRefObject)
            DifficultyHitObjects.Sort((a, b) => a.BaseHitObject.StartTime - b.BaseHitObject.StartTime);

            if (CalculateStrainValues() == false)
            {
                return 0;
            }

            double StarRating = CalculateDifficulty() * STAR_SCALING_FACTOR;

            if (CategoryDifficulty != null)
            {
                CategoryDifficulty.Add("Strain", StarRating.ToString("0.00", GameBase.nfi));

                // Mania already multiplied by the timerate, so essentially the hitwindow stays the same across doubletime and halftime.
                // Ceiling is required to lessen the rounding error
                CategoryDifficulty.Add("Hit window 300", Math.Ceiling(HitObjectManager.HitWindow300 / TimeRate).ToString("0", GameBase.nfi));

                CategoryDifficulty.Add("Score multiplier", ModManager.ScoreMultiplier(ModManager.ModStatus).ToString("0.00", GameBase.nfi));
            }

            return StarRating;
        }

        protected bool CalculateStrainValues()
        {
            // Traverse hitObjects in pairs to calculate the strain value of NextHitObject from the strain value of CurrentHitObject and environment.
            List<DifficultyHitObjectMania>.Enumerator HitObjectsEnumerator = DifficultyHitObjects.GetEnumerator();
            if (HitObjectsEnumerator.MoveNext() == false)
            {
                return false;
            }

            DifficultyHitObjectMania CurrentHitObject = HitObjectsEnumerator.Current;
            DifficultyHitObjectMania NextHitObject;

            // First hitObject starts at strain 1. 1 is the default for strain values, so we don't need to set it here. See DifficultyHitObject.

            while (HitObjectsEnumerator.MoveNext())
            {
                NextHitObject = HitObjectsEnumerator.Current;
                NextHitObject.CalculateStrains(CurrentHitObject, TimeRate);
                CurrentHitObject = NextHitObject;
            }

            return true;
        }



        // In milliseconds. For difficulty calculation we will only look at the highest strain value in each time interval of size STRAIN_STEP.
        // This is to eliminate higher influence of stream over aim by simply having more HitObjects with high strain.
        // The higher this value, the less strains there will be, indirectly giving long beatmaps an advantage.
        protected const double STRAIN_STEP = 400;

        // The weighting of each strain value decays to 0.9 * it's previous value
        protected const double DECAY_WEIGHT = 0.9;

        protected double CalculateDifficulty()
        {
            double ActualStrainStep = STRAIN_STEP * TimeRate;

            // Find the highest strain value within each strain step
            List<double> HighestStrains = new List<double>();
            double IntervalEndTime = ActualStrainStep;
            double MaximumStrain = 0; // We need to keep track of the maximum strain in the current interval

            DifficultyHitObjectMania PreviousHitObject = null;
            foreach (DifficultyHitObjectMania hitObject in DifficultyHitObjects)
            {
                // While we are beyond the current interval push the currently available maximum to our strain list
                while (hitObject.BaseHitObject.StartTime > IntervalEndTime)
                {
                    HighestStrains.Add(MaximumStrain);

                    // The maximum strain of the next interval is not zero by default! We need to take the last hitObject we encountered, take its strain and apply the decay
                    // until the beginning of the next interval.
                    if (PreviousHitObject == null)
                    {
                        MaximumStrain = 0;
                    }
                    else
                    {
                        double IndividualDecay = Math.Pow(DifficultyHitObjectMania.INDIVIDUAL_DECAY_BASE, (double)(IntervalEndTime - PreviousHitObject.BaseHitObject.StartTime) / 1000);
                        double OverallDecay = Math.Pow(DifficultyHitObjectMania.OVERALL_DECAY_BASE, (double)(IntervalEndTime - PreviousHitObject.BaseHitObject.StartTime) / 1000);
                        MaximumStrain = PreviousHitObject.IndividualStrain * IndividualDecay + PreviousHitObject.OverallStrain * OverallDecay;
                    }

                    // Go to the next time interval
                    IntervalEndTime += ActualStrainStep;
                }

                // Obtain maximum strain
                double Strain = hitObject.IndividualStrain + hitObject.OverallStrain;
                if (Strain > MaximumStrain)
                {
                    MaximumStrain = Strain;
                }

                PreviousHitObject = hitObject;
            }

            // Build the weighted sum over the highest strains for each interval
            double Difficulty = 0;
            double Weight = 1;
            HighestStrains.Sort((a, b) => b.CompareTo(a)); // Sort from highest to lowest strain.

            foreach (double Strain in HighestStrains)
            {
                Difficulty += Weight * Strain;
                Weight *= DECAY_WEIGHT;
            }

            return Difficulty;
        }
    }
}
