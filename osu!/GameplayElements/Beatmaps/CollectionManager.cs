using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Notifications;
using osu.Helpers;
using osu.Helpers.Forms;
using osu.Online;
using osu_common;
using osu_common.Helpers;
using SevenZip.Compression.LZMA;

namespace osu.GameplayElements.Beatmaps
{

    internal class CollectionManager
    {
        private static int DatabaseVersion;
        private static SortedDictionary<string, List<string>> collections;
        private static bool changesPresent;
        internal static SortedDictionary<string, List<string>> Collections
        {
            get
            {
                if (collections == null)
                    InitializeManager();
                return collections;
            }
        }

        internal static string DATABASE_FILENAME = @"collection.db";

        internal static List<string> FindCollection(string name)
        {
            if (Collections == null || name == null) return null;

            List<string> list;
            return Collections.TryGetValue(name, out list) ? list : null;
        }

        /// <summary>
        /// return collection name which contains the map
        /// return null if not found
        /// </summary>
        /// <param name="checksum"></param>
        /// <returns></returns>
        internal static List<string> FindCollectionByMap(string checksum)
        {
            if (Collections == null || checksum == null)
                return null;
            List<string> result = new List<string>();
            foreach (KeyValuePair<string, List<string>> pair in Collections)
            {
                if (pair.Value.IndexOf(checksum) >= 0)
                    result.Add(pair.Key);
            }
            return result;
        }

        internal static void InitializeManager()
        {
            if (collections != null)
                return;

            //ensure we have beatmaps available!
            if (BeatmapManager.Beatmaps == null)
                BeatmapManager.ProcessBeatmaps();

            collections = new SortedDictionary<string, List<string>>();

            bool databasePresent = File.Exists(DATABASE_FILENAME);

            if (databasePresent)
            {
                try
                {
                    using (Stream stream = File.Open(DATABASE_FILENAME, FileMode.Open))
                    using (SerializationReader sr = new SerializationReader(stream))
                    {
                        DatabaseVersion = sr.ReadInt32();

                        int count = sr.ReadInt32();

                        for (int i = 0; i < count; i++)
                        {
                            string name = sr.ReadString();
                            int mapCount = sr.ReadInt32();

                            List<string> list = new List<string>(mapCount);

                            for (int j = 0; j < mapCount; j++)
                            {
                                string checksum = sr.ReadString();
                                Beatmap map = BeatmapManager.GetBeatmapByChecksum(checksum);
                                if (map != null)
                                {
                                    list.Add(checksum);
                                }
                            }

                            collections.Add(name, list);
                        }
                    }
                }
                catch
                {
                    string backupFilename = DATABASE_FILENAME + @"." + DateTime.Now.Ticks + @".bak";
                    if (File.Exists(DATABASE_FILENAME) && !File.Exists(backupFilename))
                        File.Move(DATABASE_FILENAME, backupFilename);
                }
            }

            if (!databasePresent)
                SaveToDisk(true);
        }

        private static void migrateFav(string name, string value)
        {
            string[] arr = value.Split(',');
            List<String> list = new List<string>(arr);
            migrateList(name, list);
            changesPresent = true;
        }

        private static void migrateList(string name, List<string> migrate)
        {
            List<string> list;
            if (collections.ContainsKey(name))
                list = collections[name];
            else
                list = new List<string>();
            for (int i = 0; i < migrate.Count; i++)
            {
                if (list.IndexOf(migrate[i]) >= 0)
                    continue;
                Beatmap map = BeatmapManager.GetBeatmapByChecksum(migrate[i]);
                if (map != null)
                {
                    list.Add(migrate[i]);
                }
            }
            collections[name] = list;
        }

        internal static bool AddCollection(string name)
        {
            if (!collections.ContainsKey(name))
            {
                collections.Add(name, new List<string>());
                changesPresent = true;
                return true;
            }
            return false;
        }

        internal static bool AddToCollection(string name, string checksum)
        {
            InitializeManager();

            List<string> list;
            if (collections.ContainsKey(name))
                list = collections[name];
            else
                collections[name] = list = new List<string>();

            if (list.Contains(checksum)) return false;

            list.Add(checksum);
            changesPresent = true;
            return true;
        }

        internal static void SaveToDisk(bool force = false)
        {
            if (collections == null || (!force && !changesPresent))
                return;

            changesPresent = false;

            using (Stream stream = new SafeWriteStream(DATABASE_FILENAME))
            using (SerializationWriter sw = new SerializationWriter(stream))
            {
                sw.Write(General.VERSION);
                sw.Write(collections.Count);

                foreach (KeyValuePair<string, List<string>> s in collections)
                {
                    sw.Write(s.Key);
                    sw.Write(s.Value.Count);

                    foreach (string checksum in s.Value)
                        sw.Write(checksum);
                }
            }
        }

        internal static void Remove(string name)
        {
            collections.Remove(name);
            changesPresent = true;
        }

        internal static bool RemoveFromCollection(string name, string checksum)
        {
            changesPresent = true;
            return collections.ContainsKey(name) && collections[name].Remove(checksum);
        }
    }
}