﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using osu.GameModes.Edit.AiMod;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu_common;

namespace osu.GameplayElements.Beatmaps
{
    public class BeatmapDifficultyCalculatorOsu : BeatmapDifficultyCalculator
    {
        public BeatmapDifficultyCalculatorOsu(Beatmap Beatmap)
            : base(Beatmap)
        {
        }

        protected override PlayModes PlayMode
        {
            get
            {
                return PlayModes.Osu;
            }
        }

        static public Mods RelevantMods
        {
            get { return Mods.DoubleTime | Mods.HalfTime | Mods.HardRock | Mods.Easy; }
        }

        internal override HitObjectManager NewHitObjectManager()
        {
            return new HitObjectManagerOsu(false);
        }


        // Those values are used as array indices. Be careful when changing them!
        public enum DifficultyType : int
        {
            Speed = 0,
            Aim,
        };

        // We will store the HitObjects as a member variable.
        List<DifficultyHitObjectOsu> DifficultyHitObjects;

        private const double STAR_SCALING_FACTOR = 0.0675;
        private const double EXTREME_SCALING_FACTOR = 0.5;
        private const float PLAYFIELD_WIDTH = 512;


        protected override void PreprocessHitObjects()
        {
            foreach (HitObject hitObject in HitObjects)
            {
                if (hitObject.IsType(HitObjectType.Slider))
                {
                    ((SliderOsu)hitObject).UpdateCalculations(false);
                }
            }
        }

        protected override double ComputeDifficulty(Dictionary<String, String> CategoryDifficulty)
        {
            // Fill our custom DifficultyHitObject class, that carries additional information
            DifficultyHitObjects = new List<DifficultyHitObjectOsu>(HitObjects.Count);
            float CircleRadius = (PLAYFIELD_WIDTH / 16.0f) * (1.0f - 0.7f * (float)HitObjectManager.AdjustDifficulty(Beatmap.DifficultyCircleSize));

            foreach (HitObject hitObject in HitObjects)
            {
                DifficultyHitObjects.Add(new DifficultyHitObjectOsu(hitObject, CircleRadius));
            }

            // Sort DifficultyHitObjects by StartTime of the HitObjects - just to make sure. Not using CompareTo, since it results in a crash (HitObjectBase inherits MarshalByRefObject)
            DifficultyHitObjects.Sort((a, b) => a.BaseHitObject.StartTime - b.BaseHitObject.StartTime);


            if (CalculateStrainValues() == false)
            {
                return 0;
            }


            double SpeedDifficulty = CalculateDifficulty(DifficultyType.Speed);
            double AimDifficulty = CalculateDifficulty(DifficultyType.Aim);

            // OverallDifficulty is not considered in this algorithm and neither is HpDrainRate. That means, that in this form the algorithm determines how hard it physically is
            // to play the map, assuming, that too much of an error will not lead to a death.
            // It might be desirable to include OverallDifficulty into map difficulty, but in my personal opinion it belongs more to the weighting of the actual peformance
            // and is superfluous in the beatmap difficulty rating.
            // If it were to be considered, then I would look at the hit window of normal HitCircles only, since Sliders and Spinners are (almost) "free" 300s and take map length
            // into account as well.

            // The difficulty can be scaled by any desired metric.
            // In osu!tp it gets squared to account for the rapid increase in difficulty as the limit of a human is approached. (Of course it also gets scaled afterwards.)
            // It would not be suitable for a star rating, therefore:

            // The following is a proposal to forge a star rating from 0 to 5. It consists of taking the square root of the difficulty, since by simply scaling the easier
            // 5-star maps would end up with one star.
            double SpeedStars = Math.Sqrt(SpeedDifficulty) * STAR_SCALING_FACTOR;
            double AimStars = Math.Sqrt(AimDifficulty) * STAR_SCALING_FACTOR;

            if (CategoryDifficulty != null)
            {
                CategoryDifficulty.Add("Aim", AimStars.ToString("0.00", GameBase.nfi));
                CategoryDifficulty.Add("Speed", SpeedStars.ToString("0.00", GameBase.nfi));

                double hitWindow300 = HitObjectManager.HitWindow300 / TimeRate;
                double PreEmpt = HitObjectManager.PreEmpt / TimeRate;

                CategoryDifficulty.Add("OD", (-(hitWindow300 - 80.0) / 6.0).ToString("0.00", GameBase.nfi));
                CategoryDifficulty.Add("AR", (PreEmpt > 1200.0 ? -(PreEmpt - 1800.0) / 120.0 : -(PreEmpt - 1200.0) / 150.0 + 5.0).ToString("0.00", GameBase.nfi));

                int MaxCombo = 0;
                foreach (DifficultyHitObjectOsu hitObject in DifficultyHitObjects)
                {
                    MaxCombo += hitObject.MaxCombo;
                }
                CategoryDifficulty.Add("Max combo", MaxCombo.ToString(GameBase.nfi));
            }


            // Again, from own observations and from the general opinion of the community a map with high speed and low aim (or vice versa) difficulty is harder,
            // than a map with mediocre difficulty in both. Therefore we can not just add both difficulties together, but will introduce a scaling that favors extremes.
            double StarRating = SpeedStars + AimStars + Math.Abs(SpeedStars - AimStars) * EXTREME_SCALING_FACTOR;
            // Another approach to this would be taking Speed and Aim separately to a chosen power, which again would be equivalent. This would be more convenient if
            // the hit window size is to be considered as well.

            // Note: The star rating is tuned extremely tight! Airman (/b/104229) and Freedom Dive (/b/126645), two of the hardest ranked maps, both score ~4.66 stars.
            // Expect the easier kind of maps that officially get 5 stars to obtain around 2 by this metric. The tutorial still scores about half a star.
            // Tune by yourself as you please. ;)

            return StarRating;
        }


        protected bool CalculateStrainValues()
        {
            // Traverse hitObjects in pairs to calculate the strain value of NextHitObject from the strain value of CurrentHitObject and environment.
            List<DifficultyHitObjectOsu>.Enumerator HitObjectsEnumerator = DifficultyHitObjects.GetEnumerator();
            if (HitObjectsEnumerator.MoveNext() == false)
            {
                return false;
            }

            DifficultyHitObjectOsu CurrentHitObject = HitObjectsEnumerator.Current;
            DifficultyHitObjectOsu NextHitObject;

            // First hitObject starts at strain 1. 1 is the default for strain values, so we don't need to set it here. See DifficultyHitObject.

            while (HitObjectsEnumerator.MoveNext())
            {
                NextHitObject = HitObjectsEnumerator.Current;
                NextHitObject.CalculateStrains(CurrentHitObject, TimeRate);
                CurrentHitObject = NextHitObject;
            }

            return true;
        }

        // In milliseconds. For difficulty calculation we will only look at the highest strain value in each time interval of size STRAIN_STEP.
        // This is to eliminate higher influence of stream over aim by simply having more HitObjects with high strain.
        // The higher this value, the less strains there will be, indirectly giving long beatmaps an advantage.
        protected const double STRAIN_STEP = 400;

        // The weighting of each strain value decays to 0.9 * it's previous value
        protected const double DECAY_WEIGHT = 0.9;

        protected double CalculateDifficulty(DifficultyType Type)
        {
            double ActualStrainStep = STRAIN_STEP * TimeRate;

            // Find the highest strain value within each strain step
            List<double> HighestStrains = new List<double>();
            double IntervalEndTime = ActualStrainStep;
            double MaximumStrain = 0; // We need to keep track of the maximum strain in the current interval

            DifficultyHitObjectOsu PreviousHitObject = null;
            foreach (DifficultyHitObjectOsu hitObject in DifficultyHitObjects)
            {
                // While we are beyond the current interval push the currently available maximum to our strain list
                while (hitObject.BaseHitObject.StartTime > IntervalEndTime)
                {
                    HighestStrains.Add(MaximumStrain);

                    // The maximum strain of the next interval is not zero by default! We need to take the last hitObject we encountered, take its strain and apply the decay
                    // until the beginning of the next interval.
                    if (PreviousHitObject == null)
                    {
                        MaximumStrain = 0;
                    }
                    else
                    {
                        double Decay = Math.Pow(DifficultyHitObjectOsu.DECAY_BASE[(int)Type], (double)(IntervalEndTime - PreviousHitObject.BaseHitObject.StartTime) / 1000);
                        MaximumStrain = PreviousHitObject.Strains[(int)Type] * Decay;
                    }

                    // Go to the next time interval
                    IntervalEndTime += ActualStrainStep;
                }

                // Obtain maximum strain
                if (hitObject.Strains[(int)Type] > MaximumStrain)
                {
                    MaximumStrain = hitObject.Strains[(int)Type];
                }

                PreviousHitObject = hitObject;
            }

            // Build the weighted sum over the highest strains for each interval
            double Difficulty = 0;
            double Weight = 1;
            HighestStrains.Sort((a, b) => b.CompareTo(a)); // Sort from highest to lowest strain.

            //if (Type == DifficultyType.Aim && (HitObjectManager.ActiveMods == Mods.DoubleTime || HitObjectManager.ActiveMods == (Mods.HalfTime | Mods.Easy)))
            //    Debug.Print(HitObjectManager.Beatmap.Filename + " [" + HitObjectManager.ActiveMods.ToString() + "]: " + HighestStrains[0]);

            foreach (double Strain in HighestStrains)
            {
                Difficulty += Weight * Strain;
                Weight *= DECAY_WEIGHT;
            }

            return Difficulty;
        }

    }
}
