﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameModes.Select;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Online;
using osu_common;
using osu_common.Bancho.Objects;
using osu_common.Bancho.Requests;
using osu_common.Helpers;
using osu.Helpers;
using osu_common.Libraries.NetLib;
using osu_common.Libraries.Osz2;

namespace osu.GameplayElements.Beatmaps
{
    internal static partial class BeatmapManager
    {
        internal static bool Initialize(bool force = false)
        {
            //First check we have performed an initial load.
            //If not, try reading from osu!.db, else initialise the list.
            if (Beatmaps != null && Beatmaps.Count != 0 && !force) return false;


            BeatmapsByChecksum = new Dictionary<string, Beatmap>();
            BeatmapsById = new Dictionary<int, Beatmap>();

            InitialLoadComplete = false;
            Root = new BeatmapTreeLevel(GameBase.BeatmapDirectory, null);

            if (DatabaseDeserialize())
                DatabasePostProcess();
            else
            {
                Beatmaps = new List<Beatmap>();
                DatabaseVersion = General.VERSION;
            }

            InitializeMonitor();
            return true;
        }

        private static void InitializeMonitor()
        {
            if (SongWatcher == null)
            {
                SongWatcher = new FileSystemWatcher(BeatmapManager.SONGS_DIRECTORY);
                SongWatcher.IncludeSubdirectories = true;
                SongWatcher.Created += songWatcher_Changed;
                SongWatcher.Deleted += songWatcher_Changed;
                SongWatcher.Renamed += songWatcher_Changed;
            }

            SongWatcher.EnableRaisingEvents = true;
        }

        internal static void CheckAndProcess(bool ForceRefresh, bool UserForced = false)
        {
            bool newFiles = ChangedFolders.Count > 0 || ChangedPackages.Count > 0;

#if DEBUG
            Debug.Print("ChangedFolders: {0}\nChangedPackages: {1}", ChangedFolders.Count, ChangedPackages.Count);
#endif

            if (newFiles)
            {
                InvokeOnStatusUpdate(LocalisationManager.GetString(OsuString.BeatmapManager_LoadingNewFilesOnly));

                ProcessBeatmaps(true);

                if (pulledOnlineStatsOnce) //save some bandwidth when adding a single new song if possible.
                    GetOnlineBeatmapInfo(NewFilesList);
            }

            if ((!newFiles && ForceRefresh) || Beatmaps.Count == 0)
            {
                InvokeOnStatusUpdate(LocalisationManager.GetString(OsuString.BeatmapManager_ReloadingDatabase));
                ProcessBeatmaps(false, !UserForced);
            }

            if (!pulledOnlineStatsOnce)
            {
                GetOnlineBeatmapInfo();
                pulledOnlineStatsOnce = true;
            }

            ChangedFolders.Clear();
            ChangedPackages.Clear();
            NewFilesList.Clear();
        }

        static bool QuickFolderChecks;
        internal static void ProcessBeatmaps(bool newFilesOnly = false, bool quickFolderChecks = false)
        {
            SongWatcher.EnableRaisingEvents = false;
            QuickFolderChecks = quickFolderChecks;

            //Doing a full pass over the song folder is an expensive operation.
            //We should only do so if it is requested, or if we can't find the local osu!.db
            if (!newFilesOnly)
            {
                //FULL PROCESS
#if DEBUG
                Debug.Print("_______________ Full process initiated. _______________");
#endif

                //Mark every single beatmap as "not found" to begin with, then remove any remaining at the end which were not found.
                Beatmaps.ForEach(b =>
                {
                    b.DatabaseNotFound = true;
                    b.HeadersLoaded = false;
                });

                Root.Children.Clear();
                Root.Beatmaps.Clear();

                TotalUniqueSets = 0;
                TotalAudioOnly = 0;

                ProcessFolder(BeatmapManager.SONGS_DIRECTORY);

                int removed = Beatmaps.RemoveAll(b => b.DatabaseNotFound);

                if (removed > 0)
                    InvokeOnStatusUpdate(string.Format(LocalisationManager.GetString(OsuString.BeatmapManager_RemoveMissingMaps), removed));

                ReloadDictionaries();

                InvokeOnStatusUpdate(string.Format(LocalisationManager.GetString(OsuString.BeatmapManager_LoadedMaps), Beatmaps.Count));
            }
            else
            {
                //PARTIAL PROCESS

                for (int j = 0; j < ChangedPackages.Count; j++)
                {
                    string package = ChangedPackages[j];

                    if (!File.Exists(package))
                    {
                        //Package is no longer with us :(

                        List<Beatmap> matches = Beatmaps.FindAll(b => b.ContainingFolder == package);
                        matches.ForEach(b => Remove(b));

                        continue;
                    }
                    else
                    {
                        BeatmapTreeLevel level = GetTreeLevel(Path.GetDirectoryName(package));
                        if (level != null)
                            ProcessPackage(package, level);
                    }
                }

                //Special processing for importing new maps
                //Changed to for loop because foreach was getting modified internally.  not sure how this could happen.
                for (int j = 0; j < ChangedFolders.Count; j++)
                {
                    string folder = ChangedFolders[j];

                    bool folderStillExists = Directory.Exists(folder);

                    if (!folder.StartsWith(GameBase.BeatmapDirectory))
                    {
                        ProcessBeatmaps();
                        return;
                    }

                    BeatmapTreeLevel level = GetTreeLevel(folder);

                    if (level == null)
                        continue;

                    if (level.Children.Count > 0 && folderStillExists)
                        //ignore this folder change for now?
                        //sure, unless it was deleted.
                        continue;

                    ResetTreeLevel(level);
                    if (folderStillExists)
                        ProcessTree(level);
                    else
                    {
                        if (level.Parent != null)
                            level.Parent.Children.Remove(level);
                    }

                }
            }

            if (Current != null && newFilesOnly)
                SetCurrentFromFilename(Current.Filename);

            InitialLoadComplete = true;

            SongWatcher.EnableRaisingEvents = true;
        }

        private static void songWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            string fullPath = Path.GetDirectoryName(e.FullPath);

            if (fullPath.Contains("_tmp")) return;

            if (e.FullPath.EndsWith(".osz") && fullPath == GameBase.BeatmapDirectory && e.ChangeType == WatcherChangeTypes.Created)
            {
                if (!BeatmapImport.DoNewBeatmapCheck)
                {
                    BeatmapImport.SignalBeatmapCheck(true);
                    switch (GameBase.Mode)
                    {
                        case OsuModes.SelectPlay:
                        case OsuModes.SelectEdit:
                        case OsuModes.SelectMulti:
                        case OsuModes.MatchSetup:
                            NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.BeatmapManager_NewBeatmapDetected), 2000);
                            break;
                    }
                }
                return;
            }

            if (e.FullPath.EndsWith(".osz2") && !ChangedPackages.Contains(e.FullPath))
            {
                if (!BeatmapImport.DoNewBeatmapCheck)
                {
                    BeatmapImport.SignalBeatmapCheck(true);

                    switch (GameBase.Mode)
                    {
                        case OsuModes.SelectPlay:
                        case OsuModes.SelectEdit:
                        case OsuModes.SelectMulti:
                        case OsuModes.MatchSetup:
                            if (ShowChangeNotifications)
                                NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.BeatmapManager_NewBeatmapDetected), 2000);
                            break;
                    }
                }

                ChangedPackages.Add(e.FullPath);
            }
            else if (!ChangedFolders.Contains(fullPath))
            {
                BeatmapImport.SignalBeatmapCheck(true);

                switch (GameBase.Mode)
                {
                    case OsuModes.SelectPlay:
                    case OsuModes.SelectEdit:
                    case OsuModes.SelectMulti:
                    case OsuModes.MatchSetup:
                        if (ShowChangeNotifications)
                            NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.BeatmapManager_SongsFolderChanged), 2000);
                        break;
                }

                ChangedFolders.Add(fullPath);
            }
        }

        internal static void ProcessFolder(string folder)
        {
            ProcessTree(GetTreeLevel(folder));
        }

        internal static void ProcessTree(BeatmapTreeLevel currentLevel)
        {
            if (currentLevel == null) return;

            //never process songs if we aren't the lead spectator.
            if (GameBase.Tournament && !GameBase.TournamentManager)
                return;

            //the number of stray .osu files found in this tree level.
            int strayBeatmapCount = 0;

            string folder = currentLevel.FullPath;

            if (currentLevel != Root)
                InvokeOnStatusUpdate(folder.Replace(BeatmapManager.SONGS_DIRECTORY, string.Empty));

#if !DEBUG
            try
            {
#endif

            bool isTopFolder = folder == GameBase.BeatmapDirectory.Value;

            if (!isTopFolder && InitialLoadComplete && (Current == null || Current.ContainingFolder != folder))
            {
                string relativeFolder = folder.Replace(SONGS_DIRECTORY, string.Empty);
                bool found = false;

                //avoid hitting the filesystem at all wherever possible
                foreach (Beatmap fb in Beatmaps)
                {
                    if (fb.RelativeContainingFolder != relativeFolder) continue;

                    strayBeatmapCount++;
                    if (!fb.InOszContainer)
                    {
                        fb.DatabaseNotFound = false;

                        //we are matching based on filename here.
                        //the folder could have changed, and we need to update this change.
                        fb.ContainingFolder = folder;

                        currentLevel.Beatmaps.Add(fb);

                        found = true;
                    }
                }

                if (found)
                {
                    TotalUniqueSets++;
                    return;
                }
            }

            //load osz2 packages
            foreach (string f in Directory.GetFiles(folder, "*.osz2"))
            {
                if (f == "LastUpload.osz2") break;

                if (ProcessPackage(f, currentLevel))
                    TotalUniqueSets++;
            }

            //load osz directories
            foreach (string file in Directory.GetFiles(folder, "*.osu"))
            {
                strayBeatmapCount++;

                Beatmap b = new Beatmap { Filename = Path.GetFileName(file) };

                if (InitialLoadComplete)
                {
                    int index = Beatmaps.BinarySearch(b);

                    if (index >= 0)
                    {
                        Beatmap found = Beatmaps[index];

                        //this condition is required to make sure we don't match an osz2 to a folder.
                        //this causes really weird results. this whole process could be improved to avoid this ugliness.
                        if (!found.InOszContainer)
                        {
                            found.DatabaseNotFound = false;
                            found.BeatmapPresent = true;

                            //we are matching based on filename here.
                            //the folder could have changed, and we need to update this change.
                            found.ContainingFolder = folder;

                            currentLevel.Beatmaps.Add(found);

                            continue;
                        }
                    }
                }

                b.ContainingFolder = folder;
                try
                {
                    b.ProcessHeaders();
                }
                catch (UnauthorizedAccessException)
                {
                    continue;
                }

                if (!b.AudioPresent && !b.BeatmapPresent) continue;

                NewFilesList.Add(b);
                b.NewFile = true;

                Add(b);
                lastAddedMap = b;
                currentLevel.Beatmaps.Add(b);
            }

            if (strayBeatmapCount == 0 &&
                (currentLevel == Root ||
                 currentLevel.Parent == Root))
            {
                string audioFilename = null;
                string[] files = Directory.GetFiles(folder, @"*.mp3", SearchOption.TopDirectoryOnly);

                if (files.Length > 0) audioFilename = files[0];

                if (audioFilename != null)
                {
                    string asciiOnly = GeneralHelper.AsciiOnly(audioFilename);

                    if (asciiOnly != audioFilename)
                    {
                        if (asciiOnly.Length < 4)
                            asciiOnly = @"music.mp3";


                        if (GeneralHelper.FileMove(audioFilename, asciiOnly))
                            audioFilename = asciiOnly;
                    }

                    Beatmap b = new Beatmap
                    {
                        AudioFilename = Path.GetFileName(audioFilename),
                        Filename = Path.GetFileNameWithoutExtension(audioFilename) + @".osu",
                        DisplayTitle = Path.GetFileName(audioFilename),
                        DisplayTitleNoArtist = Path.GetFileName(audioFilename),
                        SortTitle = Path.GetFileName(audioFilename),
                        AudioPresent = true,
                        BeatmapPresent = false,
                        ContainingFolder = folder
                    };


                    int index = Add(b);

                    if (index >= 0)
                        Beatmaps[index].DatabaseNotFound = false;

                    currentLevel.Beatmaps.Add(b);

                    lastAddedMap = b;

                    TotalAudioOnly++;
                }
            }

            if (strayBeatmapCount > 0)
                TotalUniqueSets++;

            if (isTopFolder || strayBeatmapCount == 0)
            //Only process subfolders if they don't have any direct beatmaps contained in them.
            {
                foreach (string f in Directory.GetDirectories(folder))
                    ProcessFolder(f);
            }
#if !DEBUG
            }

            catch (Exception e)
            {
                TopMostMessageBox.Show("Error while processing " + folder + "\n" + e.ToString());
            }
#endif
        }

        /// <summary>
        /// Process and load a single osz2 package.
        /// </summary>
        private static bool ProcessPackage(string filename, BeatmapTreeLevel treeLevel)
        {
            MapPackage package = Osz2Factory.TryOpen(filename);

            if (package == null)
            {
                NotificationManager.ShowMessage("Error reading " + Path.GetFileName(filename), Color.Red, 4000);
                return false;
            }

            int mapsLoaded = 0;

            foreach (string file in package.MapFiles)
            {
                Beatmap b = new Beatmap { Filename = file };

                if (InitialLoadComplete)
                {
                    int index = Beatmaps.BinarySearch(b);

                    if (index >= 0)
                    {
                        Beatmap found = Beatmaps[index];

                        if (found.InOszContainer)
                        {
                            found.DatabaseNotFound = false;
                            found.BeatmapPresent = true;
                            found.ContainingFolder = filename;
                            found.InOszContainer = true;

                            treeLevel.Beatmaps.Add(found);
                            mapsLoaded++;
                            continue;
                        }
                    }
                }

                b.BeatmapSetId = Convert.ToInt32(package.GetMetadata(MapMetaType.BeatmapSetID));
                b.BeatmapId = package.GetIDByMap(file);

                b.ContainingFolder = filename;
                b.InOszContainer = true;
                b.AudioPresent = true;
                b.BeatmapPresent = true;

                b.ProcessHeaders();
                if (!b.HeadersLoaded)
                {
                    //failed?
                    continue;
                }

                b.NewFile = true;
                NewFilesList.Add(b);

                if (!b.AudioPresent && !b.BeatmapPresent) continue;

                Add(b);
                lastAddedMap = b;
                treeLevel.Beatmaps.Add(b);
                mapsLoaded++;
            }

            Osz2Factory.CloseMapPackage(package);
            //package.Unlock();

            return mapsLoaded > 0;
        }

    }
}