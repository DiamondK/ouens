﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets.Mania;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu_common;
using osu_common.Helpers;

namespace osu.GameplayElements.Beatmaps
{
    internal class BeatmapTreeItem
    {
        internal static readonly int AMOUNT_STARS = 10;
        internal static Vector2 DefaultPosition
        {
            get
            {
                return new Vector2(BeatmapTreeManager.XPosition + 500, GameBase.WindowHeightScaled / 2);
            }
        }

        private const float star_scale = 0.6f;

        internal static Color colourRootActive = new Color(163, 240, 44, 255);
        internal static Color colourRootInactive = new Color(35, 50, 143, 255);
        internal static Color colourRootInactiveContainsCurrent = new Color(35, 90, 193, 255);
        internal static Color orange = new Color(233, 104, 0, 240);
        internal static Color pink = new Color(235, 73, 153, 240);
        internal static Color blue = new Color(0, 150, 236, 240);
        internal static Color lightblue = ColourHelper.Lighten2(blue, 0.3f);
        internal static Color white = new Color(255, 255, 255, 220);
        internal static Color newmap = new Color(Color.MediumSlateBlue.R, Color.MediumSlateBlue.G, Color.MediumSlateBlue.B, 240);

        public static Color colourTextActive;
        public static Color colourTextInactive;
        public static Color colourTextInactiveFaded;

        internal Beatmap Beatmap;
        internal int GroupNumber = -1;

        bool hasPlayed;
        internal bool HasPlayed
        {
            get { return hasPlayed; }
            set
            {
                if (hasPlayed == value) return;
                hasPlayed = value;
                UpdateUnselectedColour();
            }
        }

        private bool hasMode;
        private bool hasRank;

        bool shouldShowThumbnail { get { return !OldLayout && Beatmap.BeatmapSetId > 0 && ConfigManager.sSongSelectThumbnails.Value; } }

        internal BeatmapTreeItem GroupLeader;
        internal bool IsGroupLeader
        {
            get
            {
                return this == GroupLeader;
            }
        }


        internal BeatmapTreeItem Parent;
        public bool SearchHidden;
        public bool SingleItem;

        private int starSpritesBeginIndex;
        private int blackStarSpritesBeginIndex;

        private double currentlyVisibleStars = -1;

        public Color UnselectedColour
        {
            get;
            protected set;
        }

        protected List<pDrawable> SpriteCollection;

        internal pSprite backgroundSprite;
        internal pText textTitle;
        internal pText textArtist;
        private pText textDifficulty;
        internal pSprite rankSprite;


        private static Transformation hoverEffect = new Transformation(TransformationType.Scale, 1F, 1.1F, 0, 300, EasingTypes.Out);

        internal List<pSprite> blackSprites = new List<pSprite>();
        PlayModes? modeOverride;

        internal BeatmapTreeItem(Beatmap beatmap = null, BeatmapTreeItem parent = null, PlayModes? playModeOverride = null)
        {
            Parent = parent;

            if (playModeOverride.HasValue)
                modeOverride = playModeOverride;

            UnselectedColour = orange;

            this.Beatmap = beatmap;

            if (beatmap != null)
            {
                //Don't show pink coloured new maps.
                if (!beatmap.BeatmapPresent)
                    UnselectedColour = newmap;

                hasPlayed = !beatmap.NewFile;
            }

            BackgroundColour = UnselectedColour;
        }

        internal void Dispose()
        {
            if (SpriteCollection != null)
            {
                foreach (pSprite s in SpriteCollection)
                {
                    pText pt = s as pText;
                    if (pt != null && pt.localTexture != null)
                    {
                        pt.localTexture.Dispose();
                        pt.localTexture = null;
                    }
                }
            }
        }

        internal virtual string Name { get { return Beatmap.TitleAuto; } }

        protected static pTexture s_menuButtonBackground;
        protected static pTexture s_star;
        internal pSprite modeSprite;
        private pSpriteDynamic thumbnail;
        public int AlwaysVisibleSprites;

        internal float StartDepth;

        internal Vector2 Position = DefaultPosition;


        internal bool Contains(Point position)
        {
            return backgroundSprite != null && backgroundSprite.drawRectangle.Contains(position);
        }

        internal void UpdateStarDifficultySprites(bool instant)
        {
            if (!IsInitialized || state < TreeItemState.ExtrasVisible)
            {
                return;
            }

            double stars = Math.Min(Beatmap.StarDisplay, 10.0); // We don't want to deal with more than 10 stars in this display

            if (currentlyVisibleStars == stars)
            {
                return;
            }

            UpdateDifficultyText();

            // We want the -1 placeholder for "inexistant difficulty" to be treated like 0 stars here
            if (stars < 0)
            {
                stars = 0;

                int BackgroundStarSpritesEndIndex = starSpritesBeginIndex + AMOUNT_STARS;
                for (int i = starSpritesBeginIndex; i < BackgroundStarSpritesEndIndex; ++i)
                {
                    pDrawable starSprite = SpriteCollection[i];
                    starSprite.FadeOut(instant ? 0 : (OldLayout ? 1000 : 600));
                }
            }
            // ... with the exception of the background stars appearing. We want these to only appear once difficulty has been computed. (for looking good!)
            else
            {
                int BackgroundStarSpritesEndIndex = starSpritesBeginIndex + AMOUNT_STARS;
                for (int i = starSpritesBeginIndex; i < BackgroundStarSpritesEndIndex; ++i)
                {
                    pDrawable starSprite = SpriteCollection[i];
                    starSprite.FadeIn(instant ? 0 : (OldLayout ? 1000 : 600));
                }
            }



            int StarSpritesEndIndex = starSpritesBeginIndex + AMOUNT_STARS * 2;

            for (int i = starSpritesBeginIndex + AMOUNT_STARS; i < StarSpritesEndIndex; ++i)
            {
                int starIndex = i - (starSpritesBeginIndex + AMOUNT_STARS);
                double starsLeft = stars - starIndex;
                pSprite starSprite = SpriteCollection[i] as pSprite;

                starSprite.Transformations.Clear();

                if (OldLayout)
                {
                    int targetWidth = (int)(starSprite.Width * starsLeft);

                    // It is important to also clip the width to negative values so the animation is delayed when the stars potentially appear again after the difficulty has been computed.
                    // Don't optimize this and set to 0!
                    starSprite.ClipWidthTo(targetWidth, instant ? 0 : 500, EasingTypes.OutCubic);
                }
                else
                {
                    float newScale = starsLeft <= 0 ? 0 : star_scale * (float)Math.Max(0.5f, Math.Min(1, starsLeft) + starIndex * 0.04f);
                    int amountStarsScaled = (int)Math.Floor(starIndex - Math.Min(stars, currentlyVisibleStars));

                    if (instant)
                    {
                        starSprite.ScaleTo(newScale, 0);
                    }
                    else
                    {
                        int timeAddition = currentlyVisibleStars <= stars ? amountStarsScaled : (int)Math.Floor(currentlyVisibleStars - stars) - amountStarsScaled - 1;
                        int startTime = GameBase.Time + timeAddition * 80 + 50;
                        starSprite.Transformations.Add(new Transformation(TransformationType.Scale, starSprite.Scale, newScale, startTime, startTime + 500, EasingTypes.OutBack));
                    }
                }
                    
            }


            currentlyVisibleStars = stars;
        }

        /// <summary>
        /// This is an upper bound for the maximum amount of sprites a BeatmapTreeItem can have. It is required to properly set the required depth of uninitialized BeatmapTreeItems.
        /// </summary>
        private const int MAX_AVAILABLE_SPRITES = 30;

        /// <summary>
        /// The amount by which the drawing depth is increased whenever it needs to be increased for sprites in BeatmapTreeItem.
        /// </summary>
        protected const float DEPTH_INCREASE_STEP = 0.000001f;

        /// <summary>
        /// The maximum depth difference there can be between the first and last sprite.
        /// </summary>
        internal const float MAX_DEPTH_SPAN = MAX_AVAILABLE_SPRITES * DEPTH_INCREASE_STEP;

        internal void UpdateDrawDepth(float depth)
        {
            StartDepth = depth;

            if (SpriteCollection == null)
            {
                return;
            }

            Debug.Assert(SpriteCollection.Count <= MAX_AVAILABLE_SPRITES);

            foreach (pSprite sprite in SpriteCollection)
            {
                sprite.Depth = depth;
                depth += DEPTH_INCREASE_STEP;
            }
        }

        protected virtual void PopulateSprites()
        {
            Debug.Assert(SpriteCollection == null && !IsInitialized);

            SpriteCollection = new List<pDrawable>();

            if (Beatmap != null)
            {
                PlayModes? mode = modeOverride;
                if (Beatmap != null && Beatmap.PlayMode != PlayModes.Osu)
                    mode = Beatmap.PlayMode;

                OldLayout = SkinManager.Current.Version < 2.2 && SkinManager.Load(@"menu-button-background").Source != SkinSource.Osu;

                string text = Beatmap.SortTitleAuto;
                string text2 = Beatmap.Creator.Length > 0 ? Beatmap.Creator : "unknown creator";

                backgroundSprite =
                    new pSprite(s_menuButtonBackground, Fields.TopLeft,
                                Origins.CentreLeft,
                                Clocks.Game,
                                Position, StartDepth, true, BackgroundColour, this);
                backgroundSprite.HandleInput = true;
                SpriteCollection.Add(backgroundSprite);

                bool thumb = shouldShowThumbnail;

                float xPadding = OldLayout ? 15 : 5;
                float thumbPadding = thumb ? 75 : 5;
                float yPadding = OldLayout ? -3 : 0;

                textTitle = new pText(Name, 16, Vector2.Zero, Vector2.Zero, StartDepth + DEPTH_INCREASE_STEP, true, colourTextInactive, false);
                textTitle.Origin = Origins.CentreLeft;
                textTitle.Tag = new Vector2(thumbPadding, yPadding - 16);
                blackSprites.Add(textTitle);
                SpriteCollection.Add(textTitle);

                textArtist = new pText(Beatmap.ArtistAuto + @" // " + Beatmap.Creator, 12, Vector2.Zero, Vector2.Zero, StartDepth + DEPTH_INCREASE_STEP, true, colourTextInactive, false);
                textArtist.Origin = Origins.CentreLeft;
                textArtist.Tag = new Vector2(thumbPadding + 1, yPadding - 4);
                blackSprites.Add(textArtist);
                SpriteCollection.Add(textArtist);


                if (thumb)
                {
                    bool highres = GameBase.WindowHeight >= 768;
                    thumbnail = new pSpriteDynamic(General.STATIC_WEB_ROOT_BEATMAP + @"/thumb/" + Beatmap.BeatmapSetId + (highres ? @"l" : @"") + @".jpg", @"Data\bt\" + Beatmap.BeatmapSetId + (highres ? @"l" : @"") + @".jpg", 500, Vector2.Zero, StartDepth + DEPTH_INCREASE_STEP);
                    ((pSpriteDynamic)thumbnail).OnTextureLoaded += delegate { thumbnail.FadeInFromZero(400); };

                    thumbnail.InitialColour = new Color(50, 50, 50, 255);
                    thumbnail.Additive = true;
                    thumbnail.VectorScale = new Vector2(1.425f, 1.425f) * (highres ? 0.5f : 1);
                    thumbnail.Origin = Origins.CentreLeft;
                    thumbnail.Tag = new Vector2(5.2f, 0.25f);
                    SpriteCollection.Add(thumbnail);
                }

                AlwaysVisibleSprites = SpriteCollection.Count;

                float starScale;
                if (OldLayout)
                {
                    starScale = 0.6f;
                }
                else
                {
                    starScale = 0.8f;
                }

                textDifficulty = new pText(string.Empty, 12, Vector2.Zero, Vector2.Zero, StartDepth + DEPTH_INCREASE_STEP, true, colourTextInactive, false);
                textDifficulty.Origin = Origins.CentreLeft;
                textDifficulty.TextBold = true;
                textDifficulty.Tag = new Vector2(thumbPadding + 1, yPadding + 7);
                blackSprites.Add(textDifficulty);
                SpriteCollection.Add(textDifficulty);

                UpdateDifficultyText();

                if (mode.HasValue)
                {
                    hasMode = true;
                    pTexture modeTexture = null;
                    switch (mode)
                    {
                        case PlayModes.Osu:
                            modeTexture = SkinManager.Load(@"mode-osu-small", SkinSource.Osu);
                            break;
                        case PlayModes.Taiko:
                            modeTexture = SkinManager.Load(@"mode-taiko-small", SkinSource.Osu);
                            break;
                        case PlayModes.CatchTheBeat:
                            modeTexture = SkinManager.Load(@"mode-fruits-small", SkinSource.Osu);
                            break;
                        case PlayModes.OsuMania:
                            modeTexture = SkinManager.Load(@"mode-mania-small", SkinSource.Osu);
                            break;
                    }

                    modeSprite = new pSprite(modeTexture, Fields.TopLeft, Origins.CentreLeft, Clocks.Game, Vector2.Zero, StartDepth + DEPTH_INCREASE_STEP, true, Color.White);
                    modeSprite.VectorScale = new Vector2(0.8f, 0.8f);
                    modeSprite.Tag = new Vector2(xPadding + thumbPadding + 1, yPadding - 13);
                    blackSprites.Add(modeSprite);
                    SpriteCollection.Add(modeSprite);
                }

                rankSprite = new pSprite(null, Origins.CentreLeft, Vector2.Zero, StartDepth + DEPTH_INCREASE_STEP, true, Color.White);
                rankSprite.Tag = new Vector2(xPadding + thumbPadding - 1, yPadding + (hasMode ? 14 : 0));
                SpriteCollection.Add(rankSprite);
                UpdateRank();
            }

            for (int i = 0; i < SpriteCollection.Count; i++)
                SpriteCollection[i].Alpha = 0;
        }

        internal void UpdateDifficultyText()
        {
            if (Beatmap == null) return;

            textDifficulty.Text = Beatmap.Version;
            if ((Player.Mode == PlayModes.OsuMania && Beatmap.PlayMode == PlayModes.Osu) || Beatmap.PlayMode == PlayModes.OsuMania)
                textDifficulty.Text += string.Format(@" ({0}K{1})", StageMania.ColumnsWithMods(Beatmap), Math.Round(Beatmap.DifficultyOverall) <= 4 ? @"↓" : string.Empty);
        }

        internal void UpdateRank()
        {
            rankSprite.Texture = Beatmap.PlayerRank > Rankings.D ? null : SkinManager.Load(@"ranking-" + Beatmap.PlayerRank + "-small");
            hasRank = rankSprite.Texture != null;
            UpdateHorizontalLayout();
        }

        float leftPadding;
        private void UpdateHorizontalLayout()
        {
            float lpadding = OldLayout ? 15 : 5;

            float old = leftPadding;
            leftPadding = ((state >= TreeItemState.ExtrasVisible) && (hasMode || hasRank) ? 20 : 3) + lpadding;

            if (old == leftPadding || SpriteCollection == null) return;

            float diff = leftPadding - old;

            SpriteCollection.ForEach(s =>
            {
                if (!(s.Tag is Vector2))
                    return;

                if (s == modeSprite || s == rankSprite || s == thumbnail) return;

                Vector2 v = (Vector2)s.Tag;
                v.X += diff;
                s.Tag = v;
            });
        }

        internal bool IsVisible
        {
            get { return (Parent == null || (Parent.Expanded && !Parent.SearchHidden)) && state > TreeItemState.Hidden && !SearchHidden; }
        }

        internal bool Hovering
        {
            get
            {
                return backgroundSprite != null && backgroundSprite.Hovering;
            }

            set
            {
                if (backgroundSprite != null)
                {
                    backgroundSprite.Hovering = value;
                }
            }
        }

        internal bool Hidden
        {
            get { return state <= TreeItemState.Hidden; }
            set
            {
                if (value)
                {
                    if (state > TreeItemState.Hidden)
                        State = TreeItemState.Hidden;
                }
                else
                {
                    if (state <= TreeItemState.Hidden)
                        State = SingleItem ? TreeItemState.ExtrasVisible : TreeItemState.Grouped;
                }
            }
        }

        public static void LoadSkinElements()
        {
            colourTextActive = SkinManager.Colours[@"SongSelectActiveText"];
            colourTextInactive = SkinManager.Colours[@"SongSelectInactiveText"];
            colourTextInactiveFaded = ColourHelper.ChangeAlpha(colourTextInactive, 50);

            s_menuButtonBackground = SkinManager.Load(@"menu-button-background");
            s_star = SkinManager.Load(@"star");
        }

        internal static bool OldLayout;

        internal void AddToSpriteManager(SpriteManager spriteManager)
        {
            spriteManager.AddRangeOrdered(SpriteCollection);
        }

        internal void RemoveFromSpriteManager(SpriteManager spriteManager)
        {
            spriteManager.RemoveRange(SpriteCollection);
        }

        internal void Move(Vector2 offset)
        {
            foreach (pSprite s in SpriteCollection)
            {
                s.Position = Position + offset;
                if (s.Tag is Vector2)
                    s.Position += (Vector2)s.Tag;
            }
        }

        public bool Active
        {
            get
            {
                return state >= TreeItemState.Active;
            }

            set
            {
                if (!value)
                {
                    if (state >= TreeItemState.Active)
                        State = TreeItemState.Expanded;
                }
                else
                    State = TreeItemState.Active;
            }
        }

        public bool Expanded
        {
            get
            {
                return state >= TreeItemState.Expanded;
            }

            set
            {
                if (Expanded == value) return;

                if (state < TreeItemState.Expanded)
                    State = TreeItemState.Expanded;
                else
                {
                    if (SingleItem)
                        State = TreeItemState.ExtrasVisible;
                    else
                        State = TreeItemState.Grouped;
                }
            }
        }

        public bool ShowingExtras
        {
            get
            {
                return state >= TreeItemState.ExtrasVisible;
            }
        }



        internal bool IsInitialized = false;


        internal void FadeOut(int duration)
        {
            if (!IsInitialized)
            {
                return;
            }

            foreach (pSprite s in SpriteCollection)
            {
                s.FadeOut(duration);
            }
        }

        internal void Initialize(bool instant = true)
        {
            Debug.Assert(IsInitialized == false);

            PopulateSprites();
            IsInitialized = true;

            // Update the sprites as if the last state has been hidden to achieve correct results.
            UpdateSprites(TreeItemState.Hidden, instant);
        }

        internal virtual void UpdateSprites(TreeItemState lastState, bool instant)
        {
            //handle hiding
            if (state <= TreeItemState.Hidden)
            {
                if (SpriteCollection[0].Alpha != 0)
                    SpriteCollection.ForEach(p => p.FadeOut(instant ? 0 : 200));
            }
            else if (lastState <= TreeItemState.Hidden)
            {
                int showCount = state >= TreeItemState.ExtrasVisible ? SpriteCollection.Count : AlwaysVisibleSprites;
                for (int i = 0; i < showCount; i++)
                {
                    SpriteCollection[i].FadeInFromZero(instant ? 0 : 200);
                }
            }

            UpdateHorizontalLayout();

            //handle stars
            if ((state >= TreeItemState.ExtrasVisible && lastState < TreeItemState.ExtrasVisible) || (state < TreeItemState.ExtrasVisible && lastState >= TreeItemState.ExtrasVisible))
            {
                float yPadding = OldLayout ? -3 : 0;
                float thumbPadding = shouldShowThumbnail ? 75 : 5;

                if (state >= TreeItemState.ExtrasVisible)
                {
                    starSpritesBeginIndex = SpriteCollection.Count;
                    blackStarSpritesBeginIndex = blackSprites.Count;

                    Color backgroundStarColour = new Color(255, 255, 255, 30);
                    for (int i = 0; i < AMOUNT_STARS; ++i)
                    {
                        pSprite star = new pSprite(s_star, Fields.TopLeft, Origins.Centre, Clocks.Game, Vector2.Zero, textTitle.Depth, true, backgroundStarColour, Beatmap)
                        {
                            VectorScale = new Vector2(star_scale, star_scale),
                            Tag = new Vector2(leftPadding + thumbPadding + (i + 0.5f) * (s_star.Width * 0.625f) * star_scale, yPadding + 18),
                            Additive = true,
                            Alpha = 0
                        };

                        if (!OldLayout)
                            star.Scale = 0.35f;

                        SpriteCollection.Add(star);
                    }

                    for (int i = 0; i < AMOUNT_STARS; ++i)
                    {
                        pSprite star = new pSprite(s_star, Fields.TopLeft, Origins.Centre, Clocks.Game, Vector2.Zero, textTitle.Depth + DEPTH_INCREASE_STEP, true, Color.White, Beatmap)
                        {
                            VectorScale = new Vector2(star_scale, star_scale),
                            Tag = new Vector2(leftPadding + thumbPadding + (i + 0.5f) * (s_star.Width * 0.625f) * star_scale, yPadding + 18),
                        };

                        if (OldLayout)
                            star.DrawWidth = -(int)(star.Width * i);
                        else
                            star.Scale = 0;

                        SpriteCollection.Add(star);
                        blackSprites.Add(star);
                    }

                    for (int i = 0; i < SpriteCollection.Count; i++)
                    {
                        pSprite p = SpriteCollection[i] as pSprite;

                        if (i >= AlwaysVisibleSprites && (i < starSpritesBeginIndex || i >= starSpritesBeginIndex + AMOUNT_STARS))
                            // We don't want our background sprites to fade in yet
                            p.FadeIn(instant ? 0 : 300);
                    }

                    UpdateStarDifficultySprites(instant);
                }
                else
                {
                    for (int i = 0; i < SpriteCollection.Count; i++)
                    {
                        pSprite p = SpriteCollection[i] as pSprite;
                        if (i >= AlwaysVisibleSprites)
                            p.FadeOut(instant ? 0 : 300);
                    }

                    int starSpritesEndIndex = starSpritesBeginIndex + AMOUNT_STARS * 2;
                    for (int i = starSpritesBeginIndex; i < starSpritesEndIndex; ++i)
                        SpriteCollection[i].AlwaysDraw = false;

                    SpriteCollection.RemoveRange(starSpritesBeginIndex, AMOUNT_STARS * 2);
                    blackSprites.RemoveRange(blackStarSpritesBeginIndex, AMOUNT_STARS);

                    currentlyVisibleStars = -1;
                }
            }

            //handle expanded state changes.
            if ((state >= TreeItemState.Expanded && lastState < TreeItemState.Expanded) || (state < TreeItemState.Expanded && lastState >= TreeItemState.Expanded))
            {
                if (state >= TreeItemState.Expanded)
                {
                    if (thumbnail != null)
                    {
                        thumbnail.Additive = false;
                        thumbnail.FadeInFromZero(instant ? 0 : 1000);
                        thumbnail.TimeBeforeLoad = 100;
                        thumbnail.FadeColour(new Color(255, 255, 255, 255), instant ? 0 : 300);
                    }
                }
                else
                {
                    if (thumbnail != null)
                    {
                        thumbnail.Additive = true;
                        thumbnail.TimeBeforeLoad = 500;
                        thumbnail.FadeColour(new Color(50, 50, 50, 255), instant ? 0 : 300);
                    }
                }

                UpdateHorizontalLayout();
            }

            UpdateUnselectedColour(instant ? 0 : 300);

            //handle active state changes.
            if (state == TreeItemState.Active)
            {
                blackSprites.ForEach(t => { t.InitialColour = BeatmapTreeItem.colourTextActive; });
            }
            else
            {
                blackSprites.ForEach(t =>
                {
                    Color c;
                    if (state <= TreeItemState.ExtrasVisible)
                        c = colourTextInactive;
                    else
                    {
                        if (t == textArtist || t == textTitle)
                            c = colourTextInactiveFaded;
                        else
                            c = colourTextInactive;
                    }

                    t.InitialColour = c;
                });
            }
        }


        internal virtual void OnStateChanged(TreeItemState lastState)
        {
            // We want all expanded beatmap tree items to be initialized here to ensure correct fading behavior. This does not imply any significant performance disadvantage.
            if (!IsInitialized && Expanded)
            {
                Initialize(false);
            }

            if (lastState <= TreeItemState.Hidden)
            {
                // Move to the correct position before fading in
                if (!IsGroupLeader && GroupLeader != null)
                {
                    Position = GroupLeader.Position;
                }
            }
        }

        protected TreeItemState state = TreeItemState.Hidden;
        internal TreeItemState State
        {
            get { return state; }

            set
            {
                if (state == value)
                    return;

                TreeItemState lastState = state;
                state = value;

                if (IsInitialized)
                {
                    Debug.Assert(SpriteCollection != null);
                    UpdateSprites(lastState, false);
                }

                OnStateChanged(lastState);
            }
        }


        internal Color BackgroundColour;

        internal bool FullyAppeared
        {
            get
            {
                return backgroundSprite != null && backgroundSprite.Alpha == 1;
            }
        }

        internal void FlashBackgroundColour(Color colour, int duration)
        {
            if (backgroundSprite != null)
                backgroundSprite.FlashColour(colour, duration);
        }

        internal void SetBackgroundColour(Color colour, int duration = 300)
        {
            BackgroundColour = colour;
            UpdateBackgroundColour(duration);
        }

        private void UpdateBackgroundColour(int duration)
        {
            if (backgroundSprite != null)
                backgroundSprite.FadeColour(BackgroundColour, duration);
        }

        private void UpdateUnselectedColour(int duration = 300)
        {
            switch (state)
            {
                case TreeItemState.Active:
                    UnselectedColour = BeatmapTreeItem.white;
                    break;

                case TreeItemState.Expanded:
                    UnselectedColour = BeatmapTreeItem.blue;
                    break;

                default:
                    UnselectedColour = HasPlayed ? orange : pink;
                    break;
            }

            SetBackgroundColour(UnselectedColour, duration);
        }
    }

    internal class BeatmapTreeGroup : BeatmapTreeItem
    {
        internal List<BeatmapTreeItem> Children = new List<BeatmapTreeItem>();

        private string name;
        private string nameWithCount;

        internal override string Name
        {
            get
            {
                return name;
            }
        }

        internal bool ContainsCurrent
        {
            get
            {
                return Children.Exists(c => (!c.SearchHidden && c.Beatmap == BeatmapManager.Current));
            }
        }

        public BeatmapTreeGroup(string name)
        {
            this.name = name;
            this.nameWithCount = name;
            Children = new List<BeatmapTreeItem>();

            //todo: remove this
            State = TreeItemState.Grouped;

            UnselectedColour = colourRootInactive;
            BackgroundColour = colourRootInactive;
        }

        internal int UpdateCount()
        {
            if (Children == null || Children.Count == 0) return 0;

            int count = Children.FindAll(c => !c.SearchHidden).Count;

            nameWithCount = name + " (" + count + " map" + (count != 1 ? "s" : "") + ")";

            if (IsInitialized)
                textTitle.Text = nameWithCount;

            return count;
        }

        internal void UpdateColours(bool instant = false)
        {
            if (!IsInitialized)
            {
                return;
            }

            if (state >= TreeItemState.Expanded)
            {
                textTitle.InitialColour = colourTextActive;
                UnselectedColour = colourRootActive;
            }
            else
            {
                textTitle.InitialColour = colourTextInactive;

                if (ContainsCurrent)
                {
                    UnselectedColour = colourRootInactiveContainsCurrent;
                }
                else
                {
                    UnselectedColour = colourRootInactive;
                }
            }

            SetBackgroundColour(UnselectedColour, instant ? 0 : 300);
        }


        internal override void UpdateSprites(TreeItemState lastState, bool instant)
        {
            //handle hiding
            if (state <= TreeItemState.Hidden)
            {
                if (backgroundSprite.Alpha != 0)
                    SpriteCollection.ForEach(p => p.FadeOut(instant ? 0 : 200));

                return;
            }

            //from this point onwards we are confident this item is being displayed.
            if (lastState <= TreeItemState.Hidden)
            {
                for (int i = 0; i < SpriteCollection.Count; i++)
                    SpriteCollection[i].FadeIn(instant ? 0 : 200);
            }

            UpdateColours(instant);
        }



        protected override void PopulateSprites()
        {
            if (SpriteCollection == null) SpriteCollection = new List<pDrawable>();

            backgroundSprite =
                    new pSprite(s_menuButtonBackground, Fields.TopLeft,
                                Origins.CentreLeft,
                                Clocks.Game,
                                Position, StartDepth, true, BackgroundColour,
                                this);
            backgroundSprite.HandleInput = true;
            SpriteCollection.Add(backgroundSprite);

            textTitle = new pText(nameWithCount, 24, Vector2.Zero, Vector2.Zero, StartDepth + DEPTH_INCREASE_STEP, true, colourTextInactive, false);
            textTitle.Origin = Origins.CentreLeft;
            textTitle.Tag = new Vector2(15, 0);

            blackSprites.Add(textTitle);
            SpriteCollection.Add(textTitle);

            UpdateCount();
        }


        internal override void OnStateChanged(TreeItemState lastState)
        {
        }
    }

    internal class BeatmapTreeItemComparer : IComparer<BeatmapTreeItem>
    {
        private Comparison<Beatmap> beatmapComparison;

        internal BeatmapTreeItemComparer(Comparison<Beatmap> beatmapComparison)
        {
            this.beatmapComparison = beatmapComparison;
        }

        public int Compare(BeatmapTreeItem t1, BeatmapTreeItem t2)
        {
            if (t1 == t2)
                return 0;
            if (t1.Beatmap == null)
                return -1;
            if (t2.Beatmap == null)
                return 1;

            return beatmapComparison(t1.Beatmap, t2.Beatmap);
        }
    }

    enum TreeItemState
    {
        Hidden,
        Grouped,
        ExtrasVisible,
        Expanded,
        Active
    }
}