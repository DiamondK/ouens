﻿using System;
using System.Collections;
using System.Collections.Generic;
using osu.Configuration;
using osu.Graphics.Sprites;
using System.IO;
using osu_common;
using Microsoft.Xna.Framework;
using osu.GameModes.Play.Rulesets.Mania;
using osu.Helpers;
using System.Diagnostics;
using Microsoft.Xna.Framework.Input;
using osu.Online;
using osu.Audio;
using Microsoft.Xna.Framework.Graphics;
using osu.Input;
using osu.Input.Handlers;
using Amib.Threading;
using osu.Graphics.Skinning;
using Un4seen.Bass;
using osu.GameModes.Play;
using osu.Online.Social;
using osu_common.Helpers;
using System.Threading;

namespace osu.GameplayElements.Beatmaps
{
    internal class BeatmapTreeManager : DrawableGameComponent
    {
        internal const float DRAW_DEPTH_START = 0.6f;

        private IComparer<BeatmapTreeItem> BTIComparer;

        internal Bindable<TreeGroupMode> CurrentGroupMode;
        internal Bindable<TreeSortMode> CurrentSortMode;

        private readonly SpriteManager spriteManagerPanels;

        private float DrawDepth = DRAW_DEPTH_START;

        private volatile int menuItemsGeneration = 0;
        private volatile int menuItemsGenerationPending = 0;
        internal List<BeatmapTreeItem> MenuItems = new List<BeatmapTreeItem>();

        internal int NextSearchVisible(int index, int change = 1)
        {
            do
            {
                index += change;
            }
            while (index >= 0 && index < MenuItems.Count && (MenuItems[index].SearchHidden || MenuItems[index] is BeatmapTreeGroup));

            return index;
        }

        internal int beatmapVisibleCount;
        internal int beatmapSetVisibleCount;

        private List<float> itemYDestinations = new List<float>();

        private const int SPACE_BETWEEN_ITEMS = 48;
        private float Height = -1;

        private const double DECAY_FACTOR_X_NORMAL = 0.95;
        private const double DECAY_FACTOR_Y_NORMAL = 0.875;

        internal bool IsRandomActive;
        private int randomCurrent;
        private float randomSpeed = 0.0001F;
        private List<int> randomRange = new List<int>();
        private const int RANDOM_HISTORY_MAX_LENGTH = 20;
        private LinkedList<Beatmap> randomHistory = new LinkedList<Beatmap>();

        internal bool HandleInput = true;
        internal bool AllowDragAnywhere = false;
        private bool definiteDrag;

        internal bool NextFrameInstantMove;

        private int hoverIndex = -1;

        internal int SelectedIndex
        {
            get
            {
                return selectedIndex;
            }
        }


        private float ScrollPositionAtIndex(int i, float offset)
        {
            Debug.Assert(i >= -1 && i < MenuItems.Count);
            return (Height > 0 && i >= 0) ? Math.Max(0, itemYDestinations[i] + offset) / Height : -1;
        }

        private BeatmapTreeItem ItemAtIndex(int i)
        {
            Debug.Assert(i >= -1 && i < MenuItems.Count);
            return i >= 0 ? MenuItems[i] : null;
        }

        /// <summary>
        /// The index of the BeatmapTreeGroup which is currently open.
        /// </summary>
        private int openGroupIndex = -1;

        internal BeatmapTreeGroup OpenGroup
        {
            get
            {
                BeatmapTreeGroup groupItem = ItemAtIndex(openGroupIndex) as BeatmapTreeGroup;

                Debug.Assert(openGroupIndex >= 0 ? groupItem != null : true);
                return groupItem;
            }
        }

        /// <summary>
        /// The index of the BeatmapTreeItem which is currently selected.
        /// </summary>
        private int selectedIndex = -1;

        internal Beatmap SelectedBeatmap
        {
            get
            {
                return HasSelectedItem ? SelectedBti.Beatmap : null;
            }
        }

        internal BeatmapTreeItem SelectedBti
        {
            get
            {
                return HasSelectedItem ? ItemAtIndex(selectedIndex) : null;
            }

            private set
            {
                selectedIndex = MenuItems.IndexOf(value);
            }
        }

        internal bool HasSelectedItem
        {
            get
            {
                Debug.Assert(selectedIndex >= -1 && selectedIndex < MenuItems.Count);
                return selectedIndex >= 0;
            }
        }

        internal bool HasSelectedBeatmap
        {
            get
            {
                return HasSelectedItem ? ItemAtIndex(selectedIndex).Beatmap != null : false;
            }
        }

        internal bool SelectionVisible
        {
            get
            {
                Debug.Assert(HasSelectedItem);
                return ItemAtIndex(selectedIndex).IsVisible;
            }
        }

        private bool IsKeyHovering
        {
            get
            {
                return keyHoverIndex >= 0;
            }
        }

        private bool isUsingKeys;

        /// <summary>
        /// The BeatmapTreeItem we are hovering over using our keys. This index is -1 if we are hovering over a selected BeatmapTreeItem!
        /// </summary>
        private int keyHoverIndex = -1;

        /// <summary>
        /// The BeatmapTreeItem which is currently selected by the means of keys. If a group is being hovered using the keys then its index is returned. Otherwise the currently selected beatmap's item's index is returned.
        /// </summary>
        internal int KeySelectedIndex
        {
            get
            {
                return keyHoverIndex >= 0 ? keyHoverIndex : selectedIndex;
            }
        }

        /// <summary>
        /// How far the tree is scrolled. Goes from 0 to 1.
        /// </summary>
        internal float ScrollPosition
        {
            get;
            private set;
        }


        internal float ScrollPositionYOffset
        {
            get
            {
                return -(Height * ScrollPosition);
            }
        }

        internal float XOffset;

        internal static float XPosition
        {
            get
            {
                return GameBase.WindowWidth / GameBase.WindowRatio - 340;
            }
        }


        /// <summary>
        /// This will be automatically adjusted as soon as SongSelect is updated for the first time.
        /// This does not need to be manually updated apart from inside the Update method and everything being called from there.
        /// </summary>
        private int itemBeginIndex = 0;

        /// <summary>
        /// This will be automatically adjusted as soon as SongSelect is updated for the first time.
        /// This does not need to be manually updated apart from inside the Update method and everything being called from there.
        /// </summary>
        private int itemEndIndex = 0;

        private bool IsHovering
        {
            get
            {
                return hoverIndex >= 0;
            }
        }

        private bool isDragging;

        private bool scrollByMousePosition;

        private int mouseDownIndex = -1;

        private int mouseHeightLast;
        private double scrollVelocity;


        /// <summary>
        /// Whenever new elements become visible they might need a star display update. If yes, then they are contained in this dictionary. TODO: Make this a set as soon as .net 4.0 is used!
        /// </summary>
        private Dictionary<BeatmapTreeItem, BeatmapTreeItem> needStarDisplayUpdate = new Dictionary<BeatmapTreeItem, BeatmapTreeItem>();

        internal delegate void BeatmapTreeItemHandler(object sender, BeatmapTreeItem item);
        internal event BeatmapTreeItemHandler OnSelectedBeatmap;
        internal event BeatmapTreeItemHandler OnSelectedGroup;
        internal event BeatmapTreeItemHandler OnRightClicked;
        internal event BeatmapTreeItemHandler OnActivated;
        internal event BeatmapTreeItemHandler OnExpanded;
        internal event BeatmapTreeItemHandler OnDragged;

        internal event VoidDelegate OnStartRandom;
        internal event VoidDelegate OnEndRandom;

        private SmartThreadPool backgroundWorker;

        internal BeatmapTreeManager(Game game)
            : base(game)
        {
            STPStartInfo stpStartInfo = new STPStartInfo();
            stpStartInfo.MaxWorkerThreads = 1;
            stpStartInfo.AreThreadsBackground = true;
            stpStartInfo.IdleTimeout = 1 * 1000; // 1 second is enough until idle timeout. If there are still beatmaps to process new work will be there near instantly.
            stpStartInfo.ThreadPriority = ThreadPriority.Normal;
            backgroundWorker = new SmartThreadPool(stpStartInfo);

            GameBase.OnUnload += GameBase_OnUnload;

            BeatmapManager.NewBeatmapInfo += BeatmapManager_NewBeatmapInfo;

            InputManager.Bind(InputEventType.OnMouseWheelUp, onMouseWheelUp);
            InputManager.Bind(InputEventType.OnMouseWheelDown, onMouseWheelDown);
            InputManager.Bind(InputEventType.OnClickUp, onClickUp);

            InputManager.Bind(InputEventType.OnClick, onClick);

            KeyboardHandler.OnKeyRepeat += KeyboardHandler_OnKeyRepeat;

            JoystickHandler.OnButtonPressed += JoystickHandler_OnButtonPressed;

            spriteManagerPanels = new SpriteManager(true);

            CurrentGroupMode = ConfigManager.sCurrentGroupMode;
            CurrentSortMode = ConfigManager.sCurrentSortMode;

            BTIComparer = new BeatmapTreeItemComparer(
                delegate(Beatmap b1, Beatmap b2)
                {
                    if (b1 == b2)
                        return 0;
                    if (!b1.BeatmapPresent)
                        return 1;
                    if (!b2.BeatmapPresent)
                        return -1;

                    int currentComparisonResult = 0;

                    switch (CurrentSortMode.Value)
                    {
                        case TreeSortMode.Artist:
                            currentComparisonResult = b1.Artist.CompareTo(b2.Artist);

                            if (currentComparisonResult != 0)
                                return currentComparisonResult;

                            goto case TreeSortMode.Title;

                        case TreeSortMode.BPM:
                            currentComparisonResult = b1.BeatsPerMinute.CompareTo(b2.BeatsPerMinute);
                            break;
                        case TreeSortMode.Creator:
                            currentComparisonResult = b1.Creator.CompareTo(b2.Creator);
                            break;
                        case TreeSortMode.Date:
                            if (b1.ContainingFolder != b2.ContainingFolder || (b1.InOszContainer && b2.InOszContainer) ||
                                Math.Abs((b1.DateModified - b2.DateModified).TotalSeconds) > 10)
                            {
                                currentComparisonResult = b1.DateModified.CompareTo(b2.DateModified);
                            }
                            else
                            {
                                currentComparisonResult = 0;
                            }

                            break;
                        case TreeSortMode.Difficulty:
                            currentComparisonResult = b1.StarDisplay.CompareTo(b2.StarDisplay);
                            break;
                        case TreeSortMode.Title:
                            currentComparisonResult = b1.Title.CompareTo(b2.Title);
                            break;
                        case TreeSortMode.Length:
                            currentComparisonResult = (b1.TotalLength / 1000).CompareTo(b2.TotalLength / 1000);
                            break;
                        case TreeSortMode.Rank:
                            currentComparisonResult = b2.PlayerRank.CompareTo(b1.PlayerRank);
                            break;
                    }

                    if (currentComparisonResult != 0) return currentComparisonResult;

                    currentComparisonResult = b1.RelativeContainingFolder.CompareTo(b2.RelativeContainingFolder);
                    if (currentComparisonResult != 0) return currentComparisonResult;

                    currentComparisonResult = b1.AppropriateMode.CompareTo(b2.AppropriateMode);
                    if (currentComparisonResult != 0) return currentComparisonResult;

                    // In set-internal sorting we want to keep the eyup fallback, since the difficulties automatically fall in the correct place after calculation (in almost negligible time) and we don't want them to jump around too much.
                    currentComparisonResult = b1.StarDisplayWithEyupFallback.CompareTo(b2.StarDisplayWithEyupFallback);
                    if (currentComparisonResult != 0) return currentComparisonResult;

                    currentComparisonResult = b1.Filename.CompareTo(b2.Filename);
                    if (currentComparisonResult != 0) return currentComparisonResult;

                    return 0;
                });
        }

        protected override void Dispose(bool disposing)
        {
            backgroundWorker.Shutdown(false, 0);
            backgroundWorker.Dispose();

            GameBase.OnUnload -= GameBase_OnUnload;

            BeatmapManager.NewBeatmapInfo -= BeatmapManager_NewBeatmapInfo;

            KeyboardHandler.OnKeyRepeat -= KeyboardHandler_OnKeyRepeat;

            JoystickHandler.OnButtonPressed -= JoystickHandler_OnButtonPressed;

            if (spriteManagerPanels != null)
                spriteManagerPanels.Dispose();

            DisposeMenuItems(MenuItems);

            base.Dispose(disposing);
        }

        void DisposeMenuItems(List<BeatmapTreeItem> items)
        {
            foreach (BeatmapTreeItem m in items)
            {
                m.Dispose();
            }
        }

        void GameBase_OnUnload(bool b)
        {
            DisposeMenuItems(MenuItems);
        }

        internal BeatmapTreeItem CreateItem(Beatmap b, BeatmapTreeItem parent, PlayModes? playModeOverride = null)
        {
            return new BeatmapTreeItem(b, parent, playModeOverride);
        }

        internal BeatmapTreeGroup CreateGroup(string name)
        {
            return new BeatmapTreeGroup(name);
        }


        private bool onClick(object sender, EventArgs e)
        {
            if (!HandleInput || (ChatEngine.IsFullVisible && ChatEngine.IsVisible))
                return false;

            // Ensure we hover correctly before processing the click.
            UpdateHovering();

            mouseDownIndex = hoverIndex;

            isUsingKeys = false;

            if (!IsHovering && InputManager.rightButton == ButtonState.Pressed)
            {
                scrollByMousePosition = true;
            }

            return false;
        }

        private bool onClickUp(object sender, EventArgs e)
        {
            bool wasDrag = definiteDrag;

            if (InputManager.leftButton != ButtonState.Pressed)
            {
                if (isDragging)
                {
                    // 2 30FPS frames are tolerated. Afterwards penalty is applied. This is due to touch screens / windows
                    // often not firing a mouse up event immediately after stopping movement.
                    scrollVelocity *= (float)Math.Pow(0.95, Math.Max(0, accumulatedTimeSinceLastMovement - 66));
                    accumulatedTimeSinceLastMovement = 0;
                }

                definiteDrag = false;
                isDragging = false;
            }

            try
            {
                if (!HandleInput)
                    return false;

                if (mouseDownIndex < 0 || mouseDownIndex >= MenuItems.Count)
                    return false;

                if (wasDrag || MenuItems.Count == 0 || !MenuItems[mouseDownIndex].Contains(MouseManager.MousePoint))
                    return false;

                isUsingKeys = false;

                BeatmapTreeItem m = MenuItems[mouseDownIndex];

                // We can either select or un-select a group or select a new beatmap. If we click on an already selected beatmap we start!
                if (m is BeatmapTreeGroup || mouseDownIndex != selectedIndex)
                {
                    // Select "optimal" difficuly when expanding a beatmap set
                    if (m.State <= TreeItemState.Grouped)
                    {
                        SelectRecommendedDifficulty(mouseDownIndex);
                    }
                    else
                    {
                        Select(mouseDownIndex);
                    }
                }
                else if (!InputManager.rightButtonLastBool)
                {
                    if (OnActivated != null)
                        OnActivated(this, m);
                }

                if (InputManager.rightButtonLastBool)
                {
                    if (OnRightClicked != null)
                        OnRightClicked(this, m);
                }
            }
            catch
            {
            }
            finally
            {
                mouseDownIndex = -1;
            }

            return false;
        }


        private bool onMouseWheelUp(object sender, EventArgs e)
        {
            isUsingKeys = false;

            scrollVelocity -= 0.4 * (1 + Math.Min(Math.Abs(scrollVelocity) / 2, 5));
            scrollDecay = 0.994;

            return true;
        }

        private bool onMouseWheelDown(object sender, EventArgs e)
        {
            isUsingKeys = false;

            scrollVelocity += 0.4 * (1 + Math.Min(Math.Abs(scrollVelocity) / 2, 5));
            scrollDecay = 0.994;

            return true;
        }

        private bool KeyboardHandler_OnKeyRepeat(object sender, Keys k, bool first)
        {
            if (!HandleInput) return false;

            switch (k)
            {
                case Keys.PageUp:
                    ChangeSelectionRelative(-10);
                    return true;
                case Keys.PageDown:
                    ChangeSelectionRelative(10);
                    return true;
                case Keys.Right:
                case Keys.Left:
                    if (first)
                    {
                        if (KeyboardHandler.ShiftPressed)
                        {
                            ChangeGroupRelative(k == Keys.Left ? -1 : 1);
                        }
                        else
                        {
                            if (IsKeyHovering)
                                SelectRecommendedDifficulty(keyHoverIndex);
                            else
                                ChangeSelectionRelative(k == Keys.Left ? -1 : 1, false, true);
                        }
                    }
                    return true;

                case JoyKey.Up:
                case Keys.Up:
                    ChangeSelectionRelative(-1);
                    return true;
                case JoyKey.Down:
                case Keys.Down:
                    ChangeSelectionRelative(1);
                    return true;
            }

            if (first)
            {
                switch (k)
                {
                    case Keys.Enter:
                        if (GameBase.Options.Expanded)
                            return true;

                        if (KeyboardHandler.ShiftPressed && !KeyboardHandler.ControlPressed)
                        {
                            ToggleCurrentGroup();
                        }
                        else
                        {
                            if (IsKeyHovering)
                            {
                                SelectRecommendedDifficulty(keyHoverIndex);
                            }
                            else
                            {
                                if (OnActivated != null)
                                    OnActivated(this, SelectedBti);
                            }
                        }

                        return true;
                }
            }

            return false;
        }

        void JoystickHandler_OnButtonPressed(object sender, List<Keys> keys)
        {
            Keys key = keys[0];
            switch (key)
            {
                case JoyKey.Right:
                    KeyboardHandler_OnKeyRepeat(null, Keys.Enter, true);
                    break;
            }
        }

        internal void BeatmapManager_NewBeatmapInfo(object sender, EventArgs e)
        {
            if (MenuItems == null)
                return;

            foreach (BeatmapTreeItem t in MenuItems)
            {
                if (t.Beatmap != null && t.rankSprite != null)
                {
                    if (t.Beatmap.PlayerRank <= Rankings.D && t.Beatmap.NewFile)
                        t.Beatmap.NewFile = false;
                    t.UpdateRank();
                }
            }

            if (BeatmapManager.BeatmapInfoSendListAll == null)
            {
                GameBase.Scheduler.Add(BeatmapManager.ReloadDictionaries);
                GameBase.Scheduler.Add(
                    delegate
                    {
                        UpdateList();
                    });
            }
        }

        internal void ResetHover()
        {
            hoverIndex = -1;
            ResetKeyHover();
        }

        internal void ResetKeyHover()
        {
            if (keyHoverIndex >= 0)
            {
                BeatmapTreeItem hoverItem = MenuItems[keyHoverIndex];
                hoverItem.SetBackgroundColour(hoverItem.UnselectedColour, 50);
            }

            keyHoverIndex = -1;
        }

        internal void ResetSelection()
        {
            ResetHover();
            selectedIndex = -1;
            openGroupIndex = -1;
        }

        internal void ScrollDistance(float distanceY, double decay = THROWING_SCROLL_DECAY)
        {
            if (Height <= 0)
            {
                return;
            }

            // Reverse target position out of current velocity (physically correct modelling for the win!)
            float previousTargetPosition = (float)TargetDistance - ScrollPositionYOffset;
            ScrollToPosition((previousTargetPosition + distanceY) / Height, decay);
        }

        private void ScrollToPosition(float newPosition, double decay = THROWING_SCROLL_DECAY)
        {
            if (decay == 0)
            {
                scrollVelocity = 0;
                ScrollPosition = newPosition;
                return;
            }

            float distanceY = (newPosition - ScrollPosition) * Height;
            scrollDecay = decay;

            // Solve equation dist = -v / ln(-scrollDeceleration) for dist.
            scrollVelocity = -distanceY * (float)Math.Log(scrollDecay);
        }

        internal void ReturnToHoveredItem(double decay = THROWING_SCROLL_DECAY)
        {
            int indexInQuestion = isUsingKeys && keyHoverIndex >= 0 ? keyHoverIndex : selectedIndex;

            BeatmapTreeItem m = ItemAtIndex(indexInQuestion);
            if (m != null && m.Parent != null && m.Parent.Expanded == false)
            {
                indexInQuestion = openGroupIndex;
            }

            HoverToItem(indexInQuestion, -220, decay);
        }

        internal void HoverToItem(int index, float offset, double decay = THROWING_SCROLL_DECAY)
        {
            float newPosition = ScrollPositionAtIndex(index, offset);
            if (newPosition >= 0)
            {
                ScrollToPosition(newPosition, decay);
            }
        }

        public override void Draw()
        {
            base.Draw();
            spriteManagerPanels.Draw();
        }


        private double accumulatedTimeSinceLastMovement = 0;

        private const double THROWING_SCROLL_DECAY = 0.996f; // Per millisecond!
        private double scrollDecay = THROWING_SCROLL_DECAY;
        private double scrollDistance = 0;

        public void HandleMouseMovement()
        {
            //Track the velocity of mouse while flicking the list.
            if (InputManager.leftButton == ButtonState.Pressed)
            {
                if (isDragging && GameBase.ElapsedMilliseconds > 0)
                {
                    // Speed needs to be accumulated over multiple frames due to framerate not being synced
                    // with the rate at which input devices report movement.
                    accumulatedTimeSinceLastMovement += GameBase.ElapsedMilliseconds;
                    double scrollVelocityThisFrame = ((mouseHeightLast - MouseManager.MousePoint.Y) * GameBase.WindowRatioScaleDown) / accumulatedTimeSinceLastMovement;

                    scrollDistance = scrollVelocityThisFrame * accumulatedTimeSinceLastMovement;

                    // We want to quickly adapt to new high speeds or direction changes but not too rapidly
                    // slow down. This is because before lifting the fingers / mouse button / pen we naturally
                    // slow down slightly.
                    bool highDecay =
                        Math.Sign(scrollVelocityThisFrame) == -Math.Sign(scrollVelocity) ||
                        Math.Abs(scrollVelocityThisFrame) > Math.Abs(scrollVelocity);

                    float decay = (float)Math.Pow(highDecay ? 0.90 : 0.95, accumulatedTimeSinceLastMovement);
                    if (scrollVelocityThisFrame != 0)
                    {
                        accumulatedTimeSinceLastMovement = 0;
                        scrollVelocity = scrollVelocity * decay + (1f - decay) * scrollVelocityThisFrame;

                        // Exponential scroll decay. It's more decay when dragging at lower speeds.
                        scrollDecay = scrollVelocity == 0 ? 0.5f : Math.Max(0.5, THROWING_SCROLL_DECAY - 0.002 / Math.Abs(scrollVelocity));
                    }

                    if (IsHovering && MouseManager.MousePosition.X / GameBase.WindowRatio < XPosition - 200)
                    {
                        if (OnDragged != null)
                            OnDragged(this, ItemAtIndex(hoverIndex));
                    }
                }

                // We drag if the left mouse button is pressed, but we need to activate it AFTER processing the movement of the current frame.
                // Otherwise when using touch input you can't scroll through song select.
                if (InputManager.leftButtonLast == ButtonState.Released && (AllowDragAnywhere || IsHovering))
                {
                    // Immediately stop scrolling as soon as we start dragging.
                    // This is required since set isDragging to true only after processing mouse movement for drags.
                    scrollDistance = 0;

                    isDragging = true;
                    accumulatedTimeSinceLastMovement = 0;
                }
            }
            else
            {
                // Should never be required due to OnClickUp, but let's make sure nothing breaks.
                isDragging = false;
            }

            mouseHeightLast = MouseManager.MousePoint.Y;

            if (InputManager.rightButton != ButtonState.Pressed)
            {
                scrollByMousePosition = false;
            }

            if ((MouseManager.MousePositionWindow.X < 200 && !isDragging) ||
                (ChatEngine.IsFullVisible && ChatEngine.IsVisible) ||
                isUsingKeys)
            {
                ReturnToHoveredItem(0.992);
            }
            else
            {
                if (InputManager.leftButton == ButtonState.Pressed || InputManager.rightButton == ButtonState.Pressed)
                    if (!definiteDrag && Vector2.DistanceSquared(MouseManager.ClickPosition, MouseManager.MousePosition) > 80 * 80)
                        definiteDrag = true;

                if (InputManager.leftButton != ButtonState.Pressed && scrollByMousePosition)
                {
                    float newPosition = OsuMathHelper.Clamp((InputManager.CursorPosition.Y - (70 * GameBase.WindowRatio)) / (GameBase.WindowHeight - (150 * GameBase.WindowRatio)), 0, 1.0f);
                    ScrollToPosition(newPosition, 0.992);
                }
            }
        }

        private void UpdateScrollPosition()
        {
            if (!isDragging)
            {
                double scrollVelocityPrev = scrollVelocity;
                scrollVelocity *= Math.Pow(scrollDecay, GameBase.ElapsedMilliseconds);

                scrollDistance = (scrollVelocityPrev != scrollVelocity) ? ((scrollVelocity - scrollVelocityPrev) / Math.Log(scrollDecay)) : scrollVelocityPrev;

                if (Math.Abs(scrollVelocity) < 0.01)
                {
                    scrollVelocity = 0;
                    scrollDecay = THROWING_SCROLL_DECAY;
                }
            }

            // Only adjust the scroll position if we actually have height to adjust. Otherwise we might get into nasty situations with for instance divisions by 0.
            if (Height > 0)
            {
                ScrollPosition = OsuMathHelper.Clamp((float)scrollDistance / Height + ScrollPosition, 0, 1);
            }
        }

        public override void Update()
        {
            UpdateRandom();

            if (HandleInput)
                HandleMouseMovement();

            UpdateScrollPosition();

            if (selectedIndex >= 0)
                SortBeatmapGroup(selectedIndex);

            UpdateItemPositions();

            base.Update();
        }

        private void UpdateHovering()
        {
            if (isDragging || scrollByMousePosition)
            {
                return;
            }

            bool isHovering = false;

            for (int i = itemBeginIndex; i < itemEndIndex; i++)
            {
                BeatmapTreeItem m = MenuItems[i];

                if (m.IsVisible && m.Hovering)
                {
                    if (hoverIndex != i && m.FullyAppeared)
                    {
                        hoverIndex = i;
                        if (Game.IsActive)
                        {
                            AudioEngine.Click();
                        }

                        m.FlashBackgroundColour(ColourHelper.Lighten2(m.BackgroundColour, 0.3f), 1000);
                    }

                    isHovering = true;
                }
            }

            if (!isHovering)
                hoverIndex = -1;
        }

        /// <summary>
        /// Handles various internal properties of the tree such a item states, group leaders and selected index. Is called whenever a change is made to the list (search, tab/tree change, new selection).
        /// </summary>
        internal void UpdateList()
        {
            int groupNumber = 0;
            BeatmapTreeItem currentGroupLeader = null;

            for (int i = 0; i < MenuItems.Count; i++)
            {
                BeatmapTreeItem m = MenuItems[i];

                BeatmapTreeGroup group = m as BeatmapTreeGroup;
                if (group != null)
                {
                    currentGroupLeader = null;
                    group.SearchHidden = group.UpdateCount() == 0;
                    group.UpdateColours();
                    continue;
                }

                if (m.SearchHidden)
                {
                    m.GroupLeader = null;
                    m.GroupNumber = -1;

                    m.State = TreeItemState.Hidden;
                    continue;
                }

                if (currentGroupLeader == null ||
                    currentGroupLeader.Beatmap.RelativeContainingFolder != m.Beatmap.RelativeContainingFolder)
                {
                    currentGroupLeader = m;
                }

                m.GroupLeader = currentGroupLeader;

                if (m.IsGroupLeader)
                {
                    ++groupNumber;

                    // Group leaders default to being a single item. This may later change as seen in the case above.
                    m.SingleItem = true;
                }
                else
                {
                    m.GroupLeader.SingleItem = false;
                    m.SingleItem = false;
                }

                m.GroupNumber = groupNumber;

                if (!m.Beatmap.NewFile)
                {
                    Debug.Assert(m.GroupLeader != null);
                    m.GroupLeader.HasPlayed = true;
                }
            }

            UpdateStates();

            ComputeItemYDestinations();
            UpdateKeyHoverColour();
        }

        internal void UpdateStates()
        {
            beatmapVisibleCount = 0;
            beatmapSetVisibleCount = 0;

            for (int i = 0; i < MenuItems.Count; ++i)
            {
                BeatmapTreeItem m = MenuItems[i];

                if (m is BeatmapTreeGroup || m.SearchHidden)
                {
                    continue;
                }

                UpdateState(m);

                if (!m.Hidden)
                {
                    ++beatmapVisibleCount;

                    if (m.IsGroupLeader)
                    {
                        ++beatmapSetVisibleCount;
                    }
                }
            }
        }

        private void UpdateKeyHoverColour()
        {
            if (IsKeyHovering)
            {
                Debug.Assert(keyHoverIndex < MenuItems.Count);

                BeatmapTreeItem m = MenuItems[keyHoverIndex];
                m.SetBackgroundColour(ColourHelper.Lighten(m.UnselectedColour, 0.4f), 50);
            }
        }

        private void ComputeItemYDestinations()
        {
            int amountVisibleItems = 0;

            itemYDestinations.Clear();
            itemYDestinations.Capacity = MenuItems.Count;

            BeatmapTreeItem m = null;
            BeatmapTreeItem mLastVisible = null;

            float yOffset = 0;

            for (int i = 0; i < MenuItems.Count; ++i)
            {
                m = MenuItems[i];

                if (m.IsVisible)
                {
                    if (m is BeatmapTreeGroup)
                    {
                        if (mLastVisible != null && !(mLastVisible is BeatmapTreeGroup))
                        {
                            yOffset += 10;
                        }
                    }
                    else if (m.Beatmap != null)
                    {
                        if (mLastVisible != null && (mLastVisible.Expanded || (!mLastVisible.Expanded && m.Expanded)))
                        {
                            yOffset += 10;
                        }
                    }


                    mLastVisible = m;
                    ++amountVisibleItems;
                }

                // The 200 makes the beatmap list start at the center rather than at the top when scrolled completely up
                itemYDestinations.Add(200 + yOffset + (amountVisibleItems - 1) * SPACE_BETWEEN_ITEMS);
            }

            if (amountVisibleItems <= 0)
                Height = -1;
            else
            {
                // Need to adjust scroll position so that we remain at the same absolute height (will relatively be different).
                float previousPosition = ScrollPosition * Height;
                Height = yOffset + (amountVisibleItems - 1) * SPACE_BETWEEN_ITEMS;
                ScrollPosition = Height <= 0 ? 0 : OsuMathHelper.Clamp(previousPosition / Height, 0, 1);
            }
        }

        private double TargetDistance
        {
            get
            {
                return -scrollVelocity / Math.Log(scrollDecay);
            }
        }

        private static float ItemXOffsetByYPosition(float yPosition)
        {
            return Math.Min(200, Math.Abs((yPosition / 480) - 0.5F) * 75);
        }

        private float ItemXDestinationWithoutOffset(int i)
        {
            float result = 0;

            BeatmapTreeItem m = MenuItems[i];

            if (m is BeatmapTreeGroup)
            {
                result -= 50;
            }

            if (m.Beatmap != null && m.Expanded)
            {
                result -= 50;
            }

            if (IsHovering && i == hoverIndex)
            {
                result -= 45;
            }

            return result;
        }

        private float ItemXDestination(int i)
        {
            return ItemXDestinationWithoutOffset(i) + ItemXOffsetByYPosition(MenuItems[i].Position.Y + ScrollPositionYOffset + (float)TargetDistance);
        }

        private float ItemYDestination(int i)
        {
            if (IsHovering)
            {
                if (hoverIndex < i)
                {
                    return itemYDestinations[i] + 10;
                }
                else if (hoverIndex > i)
                {
                    return itemYDestinations[i] - 10;
                }
            }

            return itemYDestinations[i];
        }

        private Vector2 ItemDestination(int i)
        {
            return new Vector2(ItemXDestination(i), ItemYDestination(i));
        }

        private static bool IsNotVisibleFromAbove(float yPosition)
        {
            return yPosition < -20;
        }

        private static bool IsNotVisibleFromBelow(float yPosition)
        {
            return yPosition > 640;
        }

        private void ExtendVisibleIndices(Vector2 offset)
        {
            // MenuItems can mutate, so we need to clamp this just in case.
            ClampVisibleItemWindow();

            // We make use of the fact, that the positions are ordered
            BeatmapTreeItem m;

            // Decrease itemBeginIndex as far as necessary
            int borderIndex = -1;
            Vector2 borderPosition = BeatmapTreeItem.DefaultPosition;

            for (int i = itemBeginIndex; i < itemEndIndex; ++i)
            {
                m = MenuItems[i];
                if (m.IsVisible)
                {
                    borderIndex = i;
                    borderPosition = m.Position;
                    break;
                }
            }

            while (itemBeginIndex > 0)
            {
                m = MenuItems[itemBeginIndex - 1];
                float yDestination = ItemYDestination(itemBeginIndex - 1);

                if (IsNotVisibleFromAbove(yDestination + offset.Y))
                {
                    break;
                }

                UpdateStarDifficultyIfNeeded(m, true);

                --itemBeginIndex;

                if (m.IsVisible)
                {
                    if (borderIndex >= 0)
                    {
                        float xDestination = ItemXDestinationWithoutOffset(itemBeginIndex);

                        borderPosition.Y += itemYDestinations[itemBeginIndex] - itemYDestinations[borderIndex];
                        borderPosition.X = (float)Math.Min(
                            borderPosition.X - ItemXDestinationWithoutOffset(borderIndex) + xDestination,
                            xDestination + offset.X + 200);

                        borderIndex = itemBeginIndex;
                    }
                    else
                    {
                        borderPosition = ItemDestination(itemBeginIndex) + offset;
                    }
                }

                m.Position = borderPosition;
            }

            // Increase itemEndIndex as far as possible
            borderIndex = -1;
            borderPosition = BeatmapTreeItem.DefaultPosition;

            for (int i = itemEndIndex - 1; i >= itemBeginIndex; --i)
            {
                m = MenuItems[i];
                if (m.IsVisible)
                {
                    borderIndex = i;
                    borderPosition = m.Position;
                    break;
                }
            }

            while (itemEndIndex < MenuItems.Count)
            {
                m = MenuItems[itemEndIndex];
                float yDestination = ItemYDestination(itemEndIndex);

                if (IsNotVisibleFromBelow(yDestination + offset.Y))
                {
                    break;
                }

                UpdateStarDifficultyIfNeeded(m, true);

                ++itemEndIndex;

                if (m.IsVisible)
                {
                    if (borderIndex >= 0)
                    {
                        float xDestination = ItemXDestinationWithoutOffset(itemEndIndex - 1);

                        borderPosition.Y += itemYDestinations[itemEndIndex - 1] - itemYDestinations[borderIndex];
                        borderPosition.X = (float)Math.Min(
                            borderPosition.X - ItemXDestinationWithoutOffset(borderIndex) + xDestination,
                            xDestination + offset.X + 200);

                        borderIndex = itemEndIndex - 1;
                    }
                    else
                    {
                        borderPosition = ItemDestination(itemEndIndex - 1) + offset;
                    }
                }

                m.Position = borderPosition;
            }
        }

        internal void ResetVisibleItemWindow()
        {
            itemBeginIndex = 0;
            itemEndIndex = MenuItems.Count;
        }

        internal void ClampVisibleItemWindow()
        {
            itemEndIndex = Math.Min(itemEndIndex, MenuItems.Count);
        }

        private void UpdateItemPositions()
        {
            Vector2 offsetX = new Vector2(
                XPosition + XOffset,
                0);

            Vector2 offsetY = new Vector2(
                0,
                ScrollPositionYOffset);

            ExtendVisibleIndices(offsetX + offsetY - new Vector2(0, (float)TargetDistance));
            UpdateHovering();

            spriteManagerPanels.Clear(false);

            for (int i = itemBeginIndex; i < itemEndIndex; i++)
            {
                BeatmapTreeItem m = MenuItems[i];

                if (m.IsVisible)
                {
                    Vector2 destination = ItemDestination(i) + offsetX;
                    Vector2 curr;

                    if (NextFrameInstantMove)
                    {
                        curr = destination;
                    }
                    else
                    {
                        curr = m.Position;

                        float distanceX = destination.X - curr.X;
                        distanceX = distanceX * (float)Math.Pow(DECAY_FACTOR_X_NORMAL, GameBase.FrameRatio);

                        float distanceY = destination.Y - curr.Y;
                        distanceY = distanceY * (float)Math.Pow(DECAY_FACTOR_Y_NORMAL, GameBase.FrameRatio); ;

                        curr.X = destination.X - distanceX;
                        curr.Y = destination.Y - distanceY;
                    }

                    m.Position = curr;

                    bool notVisibleAbove = IsNotVisibleFromAbove(curr.Y + ScrollPositionYOffset);
                    bool notVisibleBelow = IsNotVisibleFromBelow(curr.Y + ScrollPositionYOffset);

                    // Actually visible
                    if (!notVisibleAbove && !notVisibleBelow)
                    {
                        // We can only draw initialized panels. Initialize instantly, because we don't want any fade in at this point. (previously uninitialized panels shall be undistinguishable from initialized ones)
                        if (!m.IsInitialized)
                        {
                            m.Initialize();
                        }
                        else
                        {
                            UpdateStarDifficultyIfNeeded(m, false);
                        }

                        m.Move(offsetY);
                        m.AddToSpriteManager(spriteManagerPanels);
                    }
                    // If the item is not visible then we can just update its position and remove it from the list for the next frame. In case it re-enters the visible region it will be re-added by ExtendVisibleIndices.
                    else
                    {
                        if (IsNotVisibleFromBelow(destination.Y + ScrollPositionYOffset) && notVisibleBelow)
                        {
                            for (int j = itemEndIndex - 1; j >= i; --j)
                            {
                                MenuItems[j].Position = ItemDestination(j) + offsetX;
                            }

                            itemEndIndex = Math.Min(i, itemEndIndex);
                        }
                        else if (IsNotVisibleFromAbove(destination.Y + ScrollPositionYOffset) && notVisibleAbove)
                        {
                            for (int j = itemBeginIndex; j <= i; ++j)
                            {
                                MenuItems[j].Position = ItemDestination(j) + offsetX;
                            }

                            itemBeginIndex = Math.Max(i+1, itemBeginIndex);
                        }
                    }
                }
            }

            NextFrameInstantMove = false;
        }

        private void UpdateState(BeatmapTreeItem m)
        {
            if ((m.Parent != null && !m.Parent.Expanded) || m.SearchHidden)
            {
                m.State = TreeItemState.Hidden;
                return;
            }

            BeatmapTreeItem selectedBti = SelectedBti;
            bool isInSelectedFolder = selectedBti != null && selectedBti.Beatmap != null && m.Beatmap != null && m.Beatmap.RelativeContainingFolder == selectedBti.Beatmap.RelativeContainingFolder;

            if (m == selectedBti)
                m.State = TreeItemState.Active;
            else if (isInSelectedFolder)
                m.State = TreeItemState.Expanded;
            else if (m.SingleItem)
                m.State = TreeItemState.ExtrasVisible;
            else if (m.IsGroupLeader)
                m.State = TreeItemState.Grouped;
            else
                m.State = TreeItemState.Hidden;

            //handle background star calculation
            if (m.Expanded)
            {
                if (OnExpanded != null)
                    OnExpanded(this, m);
            }
        }

        private void UpdateStarDifficultyIfNeeded(BeatmapTreeItem m, bool instant)
        {
            if (needStarDisplayUpdate.ContainsKey(m))
            {
                m.UpdateStarDifficultySprites(instant);
                needStarDisplayUpdate.Remove(m);
            }
        }

        private void AddNeededStarDifficultyUpdateRange(int begin, int end)
        {
            for (int i = begin; i < end; ++i)
            {
                BeatmapTreeItem m = MenuItems[i];

                if (m is BeatmapTreeGroup)
                    continue;

                needStarDisplayUpdate.Add(m, m);
            }
        }

        public void UpdateStarDifficulty()
        {
            needStarDisplayUpdate.Clear();
            for (int i = 0; i < MenuItems.Count; ++i)
            {
                BeatmapTreeItem m = MenuItems[i];

                if (m is BeatmapTreeGroup)
                    continue;

                needStarDisplayUpdate.Add(m, m);
            }
        }

        public void UpdateStarDifficulty(Beatmap b)
        {
            List<BeatmapTreeItem> items = MenuItems.FindAll(item => item.Beatmap == b);

            foreach (BeatmapTreeItem m in items)
            {
                m.UpdateStarDifficultySprites(false);
            }
        }

        internal void FadeOut(int duration)
        {
            foreach (BeatmapTreeItem m in MenuItems)
            {
                m.FadeOut(duration);
            }
        }


        internal void SelectRecommendedDifficulty(int index, bool expandParent = true)
        {
            // We can only have a recommended difficulty when we are selecting a beatmap, not a group.
            if (index != -1 && !(MenuItems[index] is BeatmapTreeGroup) && !MenuItems[index].SearchHidden)
            {
                double recommendedDifficulty = GameBase.User.RecommendedDifficulty();

                // Find the group leader to start the search for recommended difficulty.
                for (; index > 0 && !MenuItems[index].IsGroupLeader; --index) { }

                double optimalDifference = Double.PositiveInfinity;

                for (int i = index; i < MenuItems.Count && MenuItems[i].GroupNumber == MenuItems[index].GroupNumber; i = NextSearchVisible(i))
                {
                    BeatmapTreeItem m = MenuItems[i];
                    if (m.Beatmap != null && m.Beatmap.AppropriateMode == Player.Mode)
                    {
                        double difference = Math.Abs(m.Beatmap.StarDisplayWithEyupFallback - recommendedDifficulty);
                        if (difference < optimalDifference)
                        {
                            optimalDifference = difference;
                            index = i;
                        }
                    }
                }
            }

            Select(index, expandParent);
        }

        internal void Select(Beatmap b, bool expandParent = true, string preferredParentName = null, bool chooseRecommendedDifficulty = false)
        {
            int index = MenuItems.FindIndex(bti => bti.Beatmap == b && (preferredParentName == null || (bti.Parent != null && bti.Parent.Name == preferredParentName)));

            // Couldn't find beatmap with preferred parent -> Search for beatmap in any place.
            if (preferredParentName != null && index == -1)
            {
                Select(b, expandParent, null, chooseRecommendedDifficulty);
            }
            else if (chooseRecommendedDifficulty)
            {
                SelectRecommendedDifficulty(index, expandParent);
            }
            else
            {
                Select(index, expandParent);
            }
        }

        internal void Select(int i, bool expandParent = true)
        {
            Debug.Assert(i >= -1 && i < MenuItems.Count);

            hoverIndex = i;

            bool didExpandGroup = false;

            if (i >= 0)
            {
                BeatmapTreeItem m = MenuItems[i];
                if (!(m is BeatmapTreeGroup))
                {
                    ResetKeyHover();

                    selectedIndex = i;
                    if (m.Parent != null && !m.Parent.Expanded && expandParent)
                    {
                        i = MenuItems.IndexOf(m.Parent);
                    }

                    if (OnSelectedBeatmap != null)
                        OnSelectedBeatmap(this, SelectedBti);
                }

                // i might have changed
                m = MenuItems[i];

                if (m is BeatmapTreeGroup)
                {
                    if (openGroupIndex != i && OpenGroup != null)
                    {
                        OpenGroup.Expanded = false;
                    }

                    openGroupIndex = i;
                    OpenGroup.Expanded = !OpenGroup.Expanded;
                    didExpandGroup = OpenGroup.Expanded;


                    if (OnSelectedGroup != null)
                        OnSelectedGroup(this, OpenGroup);
                }
            }
            else
            {
                selectedIndex = i;

                if (OnSelectedBeatmap != null)
                    OnSelectedBeatmap(this, null);
            }

            UpdateList();

            // We want to fade in all children correctly. Needs to be done after UpdateList to correctly account for invisible items
            if (didExpandGroup)
            {
                InitializeOpenGroupChildren();
            }

            ReturnToHoveredItem(0.99);
        }



        internal void Remove(int i)
        {
            RemoveWithoutUpdate(i);
            PostRemoval();
        }

        internal void Remove(IEnumerable<BeatmapTreeItem> items)
        {
            foreach (BeatmapTreeItem m in items)
            {
                RemoveWithoutUpdate(m);
            }

            PostRemoval();
        }

        private void RemoveWithoutUpdate(BeatmapTreeItem m)
        {
            RemoveWithoutUpdate(MenuItems.IndexOf(m));
        }

        private void RemoveWithoutUpdate(int i)
        {
            // Can't remove anything if we are empty
            if (i < 0 || MenuItems.Count == 0)
            {
                return;
            }

            bool wasVisible = ItemAtIndex(i).IsVisible;

            if (wasVisible)
            {
                Debug.Assert(beatmapVisibleCount > 0);
                --beatmapVisibleCount;
            }

            // We need to adjust the selected index if it comes after our i. Otherwise it can always just stay the same.
            if (selectedIndex == i)
            {
                int nextIndex = NextSearchVisible(i);

                // If we reached the end of the tree or the group changes, then we need to decrement the index to either avoid an IndexOutOfRange exception or changing the beatmap group while it might be possible to remain in it.
                if (nextIndex >= MenuItems.Count || ItemAtIndex(nextIndex).GroupNumber != ItemAtIndex(i).GroupNumber)
                {
                    selectedIndex = NextSearchVisible(selectedIndex, -1);

                    if (selectedIndex < 0 && beatmapVisibleCount > 0)
                    {
                        selectedIndex = NextSearchVisible(-1);
                    }
                }
                else
                {
                    selectedIndex = nextIndex;
                }
            }
            
            MenuItems.RemoveAt(i);

            // We may need to adjust our index to keep the item that we want to select selected after removal due to the indices at i and above shifting.
            if (selectedIndex > i)
            {
                --selectedIndex;
            }
        }

        private void PostRemoval()
        {
            if (HasSelectedItem)
            {
                ResetHover();

                if (SelectedBeatmap != null && SelectedBeatmap.RelativeContainingFolder != BeatmapManager.Current.RelativeContainingFolder)
                {
                    SelectRecommendedDifficulty(selectedIndex);
                }
                else
                {
                    Select(selectedIndex);
                }
            }
            else
            {
                ResetSelection();
            }

            UpdateList();
        }

        private void InitializeOpenGroupChildren()
        {
            Debug.Assert(OpenGroup != null && OpenGroup.Expanded);

            Vector2 position = OpenGroup.Position;

            for (int i = openGroupIndex + 1; i < MenuItems.Count && !(MenuItems[i] is BeatmapTreeGroup); ++i)
            {
                BeatmapTreeItem m = MenuItems[i];

                if (!m.IsInitialized && !IsNotVisibleFromBelow(position.Y + ScrollPositionYOffset) && !IsNotVisibleFromAbove(position.Y + ScrollPositionYOffset))
                    m.Initialize(false);

                m.Position = position;

                if (m.IsGroupLeader && m.IsVisible)
                    position.Y += SPACE_BETWEEN_ITEMS;
            }
        }

        private void ToggleCurrentGroup()
        {
            int index = OpenGroup == null ? selectedIndex : openGroupIndex;

            if (index == -1 || (MenuItems[index].Parent == null && !(MenuItems[index] is BeatmapTreeGroup)))
                return;

            while (!(MenuItems[index] is BeatmapTreeGroup))
                --index;

            ResetKeyHover();

            Select(index);
        }

        private void ChangeGroupRelative(int change)
        {
            int startIndex = OpenGroup == null ? selectedIndex : openGroupIndex;

            if (startIndex == -1 || (MenuItems[startIndex].Parent == null && !(MenuItems[startIndex] is BeatmapTreeGroup)))
                return;

            while (!(MenuItems[startIndex] is BeatmapTreeGroup))
                --startIndex;

            ResetKeyHover();

            int newIndex = startIndex;

            int changeSign = Math.Sign(change);
            int distanceTraveled = 0;

            do
            {
                newIndex = (MenuItems.Count + newIndex + changeSign) % MenuItems.Count;

                BeatmapTreeItem m = MenuItems[newIndex];

                if (!m.SearchHidden && m is BeatmapTreeGroup)
                {
                    ++distanceTraveled;
                }

            } while (startIndex != newIndex && distanceTraveled < Math.Abs(change));

            Select(newIndex);
        }

        private void ChangeSelectionRelative(int change, bool includeDifficulties = true, bool loadSong = false)
        {
            if (change == 0 || MenuItems.Count == 0)
                return;

            int startIndex = KeySelectedIndex;

            if (startIndex >= 0 && !ItemAtIndex(startIndex).IsVisible && OpenGroup != null && OpenGroup.Expanded)
            {
                startIndex = openGroupIndex;
            }

            if (startIndex == -1)
                return;

            int newIndex = startIndex;

            int originalGroupNumber = HasSelectedItem ? SelectedBti.GroupNumber : -1;

            int changeSign = Math.Sign(change);

            int distanceTraveled = 0;

            BeatmapTreeItem m;

            do
            {
                newIndex = (MenuItems.Count + newIndex + changeSign) % MenuItems.Count;

                m = MenuItems[newIndex];

                if (!m.SearchHidden &&
                    // If we do not include difficulties then we only stop when we find a new group (no BeatmapTreeGroups!)
                    (includeDifficulties || (!(m is BeatmapTreeGroup) && m.GroupNumber != originalGroupNumber)) &&
                    // If we include difficulties then we want to only select items which are not hidden
                    (!includeDifficulties || !m.Hidden))
                {
                    ++distanceTraveled;
                }

            } while (startIndex != newIndex && distanceTraveled < Math.Abs(change));


            if (loadSong)
            {
                SelectRecommendedDifficulty(newIndex);
            }
            else
            {
                ChangeSelectionWithoutOpen(newIndex);
            }
        }


        internal void ChangeSelectionWithoutOpen(int index)
        {
            ResetKeyHover();

            BeatmapTreeItem hoverItem = MenuItems[index];

            isUsingKeys = true;

            if (HasSelectedItem && hoverItem.GroupNumber == SelectedBti.GroupNumber)
            {
                Select(index);
            }
            else
            {
                keyHoverIndex = index;
                AudioEngine.Click();

                UpdateKeyHoverColour();
            }
        }

        /// <summary>
        /// Returns the index of the next visible beatmap, or -1
        /// </summary>
        /// <param name="i">The index to start at</param>
        internal int FindVisibleBeatmapIndex(int i)
        {
            return MenuItems.FindIndex(i, b => b.Beatmap != null && !b.Hidden && b.Beatmap.BeatmapPresent);
        }

        internal bool StartRandom()
        {
            if (MenuItems.Count < 2 || beatmapVisibleCount < 2 || GameBase.HasPendingDialog)
                return false;

            randomRange.Clear();

            int i = FindVisibleBeatmapIndex(0);
            while (i >= 0)
            {
                randomRange.Add(i);
                i = FindVisibleBeatmapIndex(i + 1);
            }

            if (randomRange.Count <= 0)
                return false;

            if (BeatmapManager.Current != null)
                randomHistory.AddLast(BeatmapManager.Current);

            Debug.Assert(RANDOM_HISTORY_MAX_LENGTH > 0);
            if (randomHistory.Count > RANDOM_HISTORY_MAX_LENGTH)
                randomHistory.RemoveFirst();

            randomSpeed = 0.0001F;
            IsRandomActive = true;

            if (OnStartRandom != null)
                OnStartRandom();

            ChangeSelectionWithoutOpen(randomRange[GameBase.random.Next(randomRange.Count)]);

            return true;
        }

        private void UpdateRandom()
        {
            if (IsRandomActive && GameBase.SixtyFramesPerSecondFrame && randomCurrent++ > randomSpeed)
            {
                if (beatmapVisibleCount <= 1)
                {
                    EndRandom();
                    return;
                }

                int i = KeySelectedIndex;

                i = FindVisibleBeatmapIndex(i + 1);
                if (i < 0)
                    i = FindVisibleBeatmapIndex(0); // wrap to the start

                if (i < 0)
                {
                    EndRandom();
                    return;
                }

                AudioEngine.Click((int)(Math.Min(50, (randomSpeed + 20)) / 50 * 100));

                randomCurrent = 0;
                randomSpeed *= 1.4F;

                if (randomSpeed > GameBase.random.Next(20, 50))
                {
                    EndRandom();
                    randomSpeed = 0.0001F;
                    return;
                }

                ChangeSelectionWithoutOpen(i);
            }
        }

        internal void EndRandom()
        {
            if (!IsRandomActive)
                return;

            IsRandomActive = false;

            if (OnEndRandom != null)
            {
                OnEndRandom();
            }

            BeatmapTreeItem m = ItemAtIndex(KeySelectedIndex);

            if (m != null && m.State <= TreeItemState.Grouped)
            {
                SelectRecommendedDifficulty(KeySelectedIndex);
            }
            else
            {
                Select(KeySelectedIndex);
            }
        }


        internal void UndoRandom()
        {
            if (randomHistory.Count == 0 || IsRandomActive)
                return;

            Select(randomHistory.Last.Value);
            randomHistory.RemoveLast();
        }


        /// <summary>
        /// Returns whether a sort has been performed.
        /// </summary>
        /// <param name="index">The beatmap group index is in will be sorted.</param>
        /// <returns></returns>
        internal void SortBeatmapGroup(int index)
        {
            int currentGroupNumber = MenuItems[index].GroupNumber;

            if (currentGroupNumber < 0) return;

            // Find beginning of group
            int begin = index;
            while (--begin >= 0 && MenuItems[begin].GroupNumber == currentGroupNumber) ;
            ++begin;

            // Find end of group
            int end = index;
            while (++end < MenuItems.Count && MenuItems[end].GroupNumber == currentGroupNumber) ;
            // We don't need to decrement end, we want it to be one after the actual end

            BeatmapTreeItem selectedBti = SelectedBti;
            bool changedOrder = SortRangeIfNotSortedAlready(MenuItems, begin, end);

            if (changedOrder)
            {
                SelectedBti = selectedBti;
                PostSort();
            }
        }

        /// <summary>
        /// Sorts the BeatmapTreeItems in the given range of indices if they are not correctly sorted already.
        /// </summary>
        /// <param name="begin">The inclusive index where the interval begins.</param>
        /// <param name="end">The exclusive index where the interval ends.</param>
        /// <returns>Whether a sort has been performed.</returns>
        internal bool SortRangeIfNotSortedAlready(List<BeatmapTreeItem> items, int begin, int end)
        {
            // Check whether we actually need sorting and if yes perform it and return
            for (int i = begin + 1; i < end; ++i)
            {
                if (BTIComparer.Compare(items[i - 1], items[i]) > 0)
                {
                    items.Sort(begin, end - begin, BTIComparer);
                    return true;
                }
            }

            return false;
        }

        internal void SortTreeAndReSelect()
        {
            ++menuItemsGenerationPending;
            int ownGeneration = menuItemsGenerationPending;

            backgroundWorker.QueueWorkItem(new Action(delegate
            {
                // Wait until previous generation update is finished. Since we run on 1 thread and only one frame needs to pass for this to happen we can employ busy waiting.
                while (ownGeneration - menuItemsGeneration > 1)
                {
                    Thread.Sleep(1);
                }

                List<BeatmapTreeItem> newMenuItems = new List<BeatmapTreeItem>();
                newMenuItems.AddRange(MenuItems);

                SortTreeBackground(newMenuItems);

                GameBase.Scheduler.Add(delegate
                {
                    EndRandom();

                    BeatmapTreeItem selectedBti = SelectedBti;
                    MenuItems = newMenuItems;
                    SelectedBti = selectedBti;

                    PostSort();

                    ReturnToHoveredItem(0);

                    ++menuItemsGeneration;
                });
            }));
        }


        internal void SortTreeBackground(List<BeatmapTreeItem> items)
        {
            for (int begin = 0; begin < items.Count; ++begin)
            {
                // Find where the next group starts
                int end;
                for (end = begin; end < items.Count && !(items[end] is BeatmapTreeGroup); ++end)
                {

                }

                // This is the case when begin is the index of a group.
                int size = end - begin;
                if (size == 0)
                {
                    continue;
                }

                SortRangeIfNotSortedAlready(items, begin, end);

                begin += size;
            }
        }

        internal void GenerateTree(TreeGroupMode mode, bool displayAll, VoidDelegate onCompletion)
        {
            ++menuItemsGenerationPending;
            int ownGeneration = menuItemsGenerationPending;

            backgroundWorker.QueueWorkItem(new Action(delegate
            {
                // Wait until previous generation update is finished. Since we run on 1 thread and only one frame needs to pass for this to happen we can employ busy waiting.
                while (ownGeneration - menuItemsGeneration > 1)
                {
                    Thread.Sleep(1);
                }

                List<BeatmapTreeItem> newMenuItems = new List<BeatmapTreeItem>(BeatmapManager.Beatmaps.Count);

                GenerateTreeBackground(newMenuItems, mode, displayAll);
                SortTreeBackground(newMenuItems);

                GameBase.Scheduler.Add(delegate
                {
                    EndRandom();
                    ResetSelection();

                    DisposeMenuItems(MenuItems);
                    MenuItems = newMenuItems;

                    CurrentGroupMode.Value = mode;
                    needStarDisplayUpdate.Clear();
                    PostSort();

                    if (onCompletion != null)
                        onCompletion();

                    ++menuItemsGeneration;
                });
            }));
        }

        private void GenerateTreeBackground(List<BeatmapTreeItem> items, TreeGroupMode mode, bool displayAll)
        {
            DateTime now = DateTime.Now;
            List<Beatmap> beatmapAvailable;
            BeatmapTreeGroup m;

            switch (mode)
            {
                case TreeGroupMode.Online_Favourites:
                    AddBeatmapsTopLevel(items, BeatmapManager.Beatmaps.FindAll(bm => bm.OnlineFavourite), displayAll);
                    break;

                case TreeGroupMode.Artist:
                    {
                        // Starts with 0-9
                        List<Beatmap> numeric = new List<Beatmap>();

                        // Starts with a latin letter
                        SortedDictionary<char, List<Beatmap>> dic = new SortedDictionary<char, List<Beatmap>>();
                        for (char c = 'A'; c <= 'Z'; c++)
                        {
                            dic.Add(c, new List<Beatmap>());
                        }

                        // Neither 0-9 nor latin letter
                        List<Beatmap> other = new List<Beatmap>();

                        foreach (Beatmap b in BeatmapManager.Beatmaps)
                        {
                            string str = b.Artist;
                            if (str.Length > 0)
                            {
                                char firstChar = str[0];
                                if (char.IsNumber(firstChar))
                                {
                                    numeric.Add(b);
                                    continue;
                                }
                                else
                                {
                                    char key = char.ToUpper(firstChar);
                                    if (key >= 'A' && key <= 'Z')
                                    {
                                        dic[key].Add(b);
                                        continue;
                                    }
                                }

                                other.Add(b);
                            }
                        }

                        GenerateGroup(items, "0-9", numeric, displayAll);

                        foreach (KeyValuePair<char, List<Beatmap>> pair in dic)
                        {
                            GenerateGroup(items, pair.Key.ToString(), pair.Value, displayAll);
                        }

                        GenerateGroup(items, "Other", other, displayAll);
                    }
                    break;

                case TreeGroupMode.Rank:
                    GenerateGroup(items, "Silver SS", BeatmapManager.Beatmaps.FindAll(s => s.PlayerRank == Rankings.XH), displayAll);
                    GenerateGroup(items, "Silver S", BeatmapManager.Beatmaps.FindAll(s => s.PlayerRank == Rankings.SH), displayAll);
                    GenerateGroup(items, "SS", BeatmapManager.Beatmaps.FindAll(s => s.PlayerRank == Rankings.X), displayAll);

                    for (int i = (int)Rankings.S; i < (int)Rankings.F; i++)
                    {
                        Rankings r = (Rankings)i;
                        GenerateGroup(items, r.ToString(), BeatmapManager.Beatmaps.FindAll(s => s.PlayerRank == r), displayAll);
                    }

                    GenerateGroup(items, "No Rank Achieved", BeatmapManager.Beatmaps.FindAll(s => s.PlayerRank >= Rankings.F), displayAll);
                    break;

                case TreeGroupMode.Title:
                    {
                        // Starts with 0-9
                        List<Beatmap> numeric = new List<Beatmap>();

                        // Starts with a latin letter
                        SortedDictionary<char, List<Beatmap>> dic = new SortedDictionary<char, List<Beatmap>>();
                        for (char c = 'A'; c <= 'Z'; c++)
                        {
                            dic.Add(c, new List<Beatmap>());
                        }

                        // Neither 0-9 nor latin letter
                        List<Beatmap> other = new List<Beatmap>();

                        foreach (Beatmap b in BeatmapManager.Beatmaps)
                        {
                            string str = b.Title;
                            if (str.Length > 0)
                            {
                                char firstChar = str[0];
                                if (char.IsNumber(firstChar))
                                {
                                    numeric.Add(b);
                                    continue;
                                }
                                else
                                {
                                    char key = char.ToUpper(firstChar);
                                    if (key >= 'A' && key <= 'Z')
                                    {
                                        dic[key].Add(b);
                                        continue;
                                    }
                                }

                                other.Add(b);
                            }
                        }

                        GenerateGroup(items, "0-9", numeric, displayAll);

                        foreach (KeyValuePair<char, List<Beatmap>> pair in dic)
                        {
                            GenerateGroup(items, pair.Key.ToString(), pair.Value, displayAll);
                        }

                        GenerateGroup(items, "Other", other, displayAll);
                    }
                    break;

                case TreeGroupMode.Creator:
                    {
                        SortedDictionary<string, List<Beatmap>> dic = new SortedDictionary<string, List<Beatmap>>();
                        foreach (Beatmap b in BeatmapManager.Beatmaps)
                        {
                            List<Beatmap> maps;
                            if (!dic.ContainsKey(b.Creator))
                            {
                                maps = new List<Beatmap>();
                                dic.Add(b.Creator, maps);
                            }
                            else
                            {
                                maps = dic[b.Creator];
                            }

                            maps.Add(b);
                        }

                        foreach (KeyValuePair<string, List<Beatmap>> pair in dic)
                        {
                            GenerateGroup(items, pair.Key, pair.Value, displayAll);
                        }
                    }
                    break;

                case TreeGroupMode.My_Maps:
                    AddBeatmapsTopLevel(items, BeatmapManager.Beatmaps.FindAll(s => s.Creator.ToLower() == ConfigManager.sUsername.Value.ToLower()), displayAll);
                    break;

                case TreeGroupMode.Difficulty:

                    GenerateGroup(items, "Not yet calculated", BeatmapManager.Beatmaps.FindAll(s => s.StarDisplay <= -0.1), displayAll);

                    for (double stars = 1.0; stars <= 10.0; stars += 1.0)
                    {
                        //We use strange bounds for these ranges, but must consider the special case for 1.
                        double lowerBound = stars == 1.0 ? -0.1 : stars - 0.5;
                        double upperBound = Math.Min(10.0, stars + 0.5);

                        string str = stars + " Star" + (stars != 1.0 ? "s" : "");
                        GenerateGroup(items, str, BeatmapManager.Beatmaps.FindAll(s => s.StarDisplay > lowerBound && s.StarDisplay <= upperBound), displayAll);
                    }

                    GenerateGroup(items, "Above 10 stars", BeatmapManager.Beatmaps.FindAll(s => s.StarDisplay > 10.0), displayAll);
                    break;

                case TreeGroupMode.Length:
                    for (int i = 1; i < 6; i++)
                    {
                        string str = "Under " + i + " minute" + (i > 1 ? "s" : "");
                        GenerateGroup(items, str, BeatmapManager.Beatmaps.FindAll(s => s.TotalLength < i * 60000 && s.TotalLength >= (i - 1) * 60000), displayAll);
                    }

                    GenerateGroup(items, "Under 10 minutes", BeatmapManager.Beatmaps.FindAll(s => s.TotalLength < 10 * 60000 && s.TotalLength >= 5 * 60000), displayAll);
                    GenerateGroup(items, "Over 10 minutes", BeatmapManager.Beatmaps.FindAll(s => s.TotalLength >= 10 * 60000), displayAll);
                    break;

                case TreeGroupMode.BPM:
                    for (int i = 1; i < 6; i++)
                    {
                        string str = "Under " + i * 60 + " BPM";
                        GenerateGroup(items, str, BeatmapManager.Beatmaps.FindAll(s => s.BeatsPerMinute < i * 60 && s.BeatsPerMinute >= (i - 1) * 60), displayAll);
                    }

                    GenerateGroup(items, "Over 300 BPM", BeatmapManager.Beatmaps.FindAll(s => s.BeatsPerMinute > 300), displayAll);
                    break;

                case TreeGroupMode.Show_All:
                    AddBeatmapsTopLevel(items, BeatmapManager.Beatmaps, displayAll);
                    break;

                case TreeGroupMode.Date:
                    GenerateGroup(items, "Today", BeatmapManager.Beatmaps.FindAll(s => now - s.DateModified < new TimeSpan(24, 0, 0)), displayAll);
                    GenerateGroup(items, "Yesterday", BeatmapManager.Beatmaps.FindAll(s => now - s.DateModified >= new TimeSpan(24, 0, 0) && now - s.DateModified < new TimeSpan(48, 0, 0)), displayAll);
                    GenerateGroup(items, "Last Week", BeatmapManager.Beatmaps.FindAll(s => now - s.DateModified >= new TimeSpan(2, 0, 0, 0) && now - s.DateModified < new TimeSpan(7, 0, 0, 0)), displayAll);
                    {
                        int i;
                        for (i = 1; i <= 5; i++)
                        {
                            string str = i + " Month" + (i > 1 ? "s" : "") + " Ago";
                            GenerateGroup(items, str, BeatmapManager.Beatmaps.FindAll(s =>
                                                now - s.DateModified >= new TimeSpan(7, 0, 0, 0) &&
                                                now - s.DateModified >= new TimeSpan(30 * (i - 1), 0, 0, 0) &&
                                                now - s.DateModified < new TimeSpan(30 * (i), 0, 0, 0)), displayAll);
                        }

                        GenerateGroup(items, "Over " + (i - 1) + " Months Ago", BeatmapManager.Beatmaps.FindAll(s => now - s.DateModified >= new TimeSpan(30 * (i - 1), 0, 0, 0) && s.DateModified != DateTime.MinValue), displayAll);
                    }
                    break;

                case TreeGroupMode.Last_Played:
                    GenerateGroup(items, "Today", BeatmapManager.Beatmaps.FindAll(s => now - s.DateLastPlayed < new TimeSpan(24, 0, 0)), displayAll);
                    GenerateGroup(items, "Yesterday", BeatmapManager.Beatmaps.FindAll(s => now - s.DateLastPlayed >= new TimeSpan(24, 0, 0) && now - s.DateLastPlayed < new TimeSpan(48, 0, 0)), displayAll);
                    GenerateGroup(items, "Last Week", BeatmapManager.Beatmaps.FindAll(s => now - s.DateLastPlayed >= new TimeSpan(2, 0, 0, 0) && now - s.DateLastPlayed < new TimeSpan(7, 0, 0, 0)), displayAll);
                    {
                        int i;
                        for (i = 1; i <= 5; i++)
                        {
                            string str = i + " Month" + (i > 1 ? "s" : "") + " Ago";
                            GenerateGroup(items, str, BeatmapManager.Beatmaps.FindAll(s =>
                                                now - s.DateLastPlayed >= new TimeSpan(7, 0, 0, 0) &&
                                                now - s.DateLastPlayed >= new TimeSpan(30 * (i - 1), 0, 0, 0) &&
                                                now - s.DateLastPlayed < new TimeSpan(30 * (i), 0, 0, 0)), displayAll);
                        }

                        GenerateGroup(items, "Over " + (i - 1) + " Months Ago", BeatmapManager.Beatmaps.FindAll(s => now - s.DateLastPlayed >= new TimeSpan(30 * (i - 1), 0, 0, 0) && s.DateLastPlayed != DateTime.MinValue), displayAll);
                    }

                    GenerateGroup(items, "Never Played", BeatmapManager.Beatmaps.FindAll(s => s.DateLastPlayed == DateTime.MinValue), displayAll);
                    break;

                case TreeGroupMode.RankedStatus:
                    GenerateGroup(items, "Ranked", BeatmapManager.Beatmaps.FindAll(s => s.SubmissionStatus == SubmissionStatus.Ranked || s.SubmissionStatus == SubmissionStatus.Approved), displayAll);
                    GenerateGroup(items, "Pending", BeatmapManager.Beatmaps.FindAll(s => s.SubmissionStatus == SubmissionStatus.Pending), displayAll);
                    GenerateGroup(items, "Not Submitted", BeatmapManager.Beatmaps.FindAll(s => s.SubmissionStatus == SubmissionStatus.NotSubmitted), displayAll);
                    GenerateGroup(items, "Unknown", BeatmapManager.Beatmaps.FindAll(s => s.SubmissionStatus == SubmissionStatus.Unknown), displayAll);
                    break;

                case TreeGroupMode.Mania_Keys:
                    beatmapAvailable = BeatmapManager.Beatmaps.FindAll(b => b.PlayMode == PlayModes.Osu || b.PlayMode == PlayModes.OsuMania);
                    for (int i = 1; i <= StageMania.MAX_COLUMNS; i++)
                        GenerateGroup(items, string.Format("{0} Keys", i), beatmapAvailable.FindAll(b => StageMania.ColumnsWithMods(b) == i), displayAll);
                    break;

                case TreeGroupMode.Mode:
                    GenerateGroup(items, OsuCommon.PlayModeString(PlayModes.Osu), BeatmapManager.Beatmaps.FindAll(b => b.PlayMode == PlayModes.Osu), displayAll);
                    GenerateGroup(items, OsuCommon.PlayModeString(PlayModes.Taiko), BeatmapManager.Beatmaps.FindAll(b => b.PlayMode == PlayModes.Taiko), displayAll);
                    GenerateGroup(items, OsuCommon.PlayModeString(PlayModes.CatchTheBeat), BeatmapManager.Beatmaps.FindAll(b => b.PlayMode == PlayModes.CatchTheBeat), displayAll);
                    GenerateGroup(items, OsuCommon.PlayModeString(PlayModes.OsuMania), BeatmapManager.Beatmaps.FindAll(b => b.PlayMode == PlayModes.OsuMania), displayAll);
                    break;

                case TreeGroupMode.Collection:

                    beatmapAvailable = new List<Beatmap>();
                    foreach (KeyValuePair<string, List<string>> kvp in CollectionManager.Collections)
                    {
                        // Find all beatmaps in the collection
                        if (kvp.Value.Count == 0)
                            continue;

                        beatmapAvailable.Clear();
                        foreach (string checksum in kvp.Value)
                        {
                            Beatmap b = BeatmapManager.GetBeatmapByChecksum(checksum);
                            if (b != null)
                            {
                                beatmapAvailable.Add(b);
                            }
                        }

                        GenerateGroup(items, kvp.Key, beatmapAvailable, displayAll);
                    }
                    break;
            }
        }

        private void AddBeatmapsTopLevel(List<BeatmapTreeItem> items, List<Beatmap> beatmaps, bool displayAll)
        {
            foreach (Beatmap b in beatmaps)
            {
                if (b.BeatmapPresent || displayAll)
                {
                    items.Add(CreateItem(b, null));
                }
            }
        }

        private void GenerateGroup(List<BeatmapTreeItem> items, string groupName, List<Beatmap> beatmaps, bool displayAll)
        {
            BeatmapTreeGroup group = CreateGroup(groupName);
            foreach (Beatmap b in beatmaps)
            {
                if (b.BeatmapPresent || displayAll)
                {
                    group.Children.Add(CreateItem(b, group));
                }
            }

            if (group.Children.Count > 0)
            {
                items.Add(group);
                items.AddRange(group.Children);
            }
        }


        internal void PostSort()
        {
            // Reset draw depth, because all BeatmapTreeItems will get an updated depth
            DrawDepth = DRAW_DEPTH_START;
            for (int i = 0; i < MenuItems.Count; i++)
            {
                BeatmapTreeItem item = MenuItems[i];
                item.UpdateDrawDepth(DrawDepth);
                DrawDepth += BeatmapTreeItem.MAX_DEPTH_SPAN;
            }

            UpdateList();

            ResetVisibleItemWindow();
        }
    }

    internal enum TreeGroupMode
    {
        None,
        Artist,
        BPM,
        Creator,
        Date,
        Difficulty,
        Length,
        Rank,
        My_Maps,
        Search = 12,
        Show_All = 12,
        Title,
        Last_Played,
        Online_Favourites,
        Mania_Keys,
        Mode,
        Collection,
        RankedStatus
    }

    internal enum TreeSortMode
    {
        Artist,
        BPM,
        Creator,
        Date,
        Difficulty,
        Length,
        Rank,
        Title
    }
}