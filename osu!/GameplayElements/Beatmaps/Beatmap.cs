﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu_common;
using osu_common.Bancho;
using osu_common.Helpers;
using osu_common.Libraries.NetLib;
using osu_common.Libraries.Osz2;
using osu.Helpers;
using System.Threading;
using osu.Configuration;
using osu.Online.P2P;
using MonoTorrent.Common;
using System.Windows.Forms;
using osu.GameModes.Edit.Forms;
using osu.GameModes.Select;
using osu.GameplayElements.Events;
using osu.Online;
using Amib.Threading;
using osu.GameModes.Play.Rulesets.Mania;

namespace osu.GameplayElements.Beatmaps
{
    public class Beatmap : BeatmapBase, bSerializable, IComparable<Beatmap>
    {
        private static readonly int[] VERSION_DIFFICULTY = new int[] {
            20150211, // standard
            20140610, // taiko
            20141123, // ctb
            20150110, // mania
        };

        private MapPackage _package;
        internal bool AlwaysShowPlayfield;

        internal string AudioExpectedMd5;

        internal string AudioFilename;
        internal int AudioLeadIn;
        private bool audioPresent = true;
        internal bool AudioPresent
        {
            get { return audioPresent; }
            set { audioPresent = value; }
        }
        internal Color BackgroundColour;
        internal string BackgroundImage;

        internal string BeatmapChecksum;

        internal int BeatmapId = 0;

        internal bool AllowOverrides
        {
            get
            {
                return GameBase.Mode == OsuModes.Edit || GameBase.RunningGameMode is PlayerTest;
            }
        }
        internal bool IsTemporary = false;
        internal bool ServerHasOsz2;
        internal bool BeatmapPresent;
        internal int BeatmapSetId = -1;
        internal int TotalRecords;
        internal int BeatmapTopicId;
        internal int BeatmapVersion;

        internal int LastEditTime;

        internal string RelativeContainingFolder;

        /// <summary>
        /// The relative path of this beatmap from the osu! folder. Can be either a folder (osz) or a file (osz2).
        /// </summary>
        internal string ContainingFolder
        {
            get { return BeatmapManager.SONGS_DIRECTORY + RelativeContainingFolder; }
            set
            {
                value = GeneralHelper.PathSanitise(value);

                int lastDirSeparator = value.LastIndexOf(Path.DirectorySeparatorChar);

                if (lastDirSeparator >= 0)
                {
                    int songsIndex = value.IndexOf(BeatmapManager.SONGS_DIRECTORY);
                    if (songsIndex >= 0)
                    {
                        //the absolute (or relative) songs directory has been included for some reason.
                        value = value.Substring(songsIndex + BeatmapManager.SONGS_DIRECTORY.Length);
                    }
                }

                RelativeContainingFolder = value.Trim(Path.DirectorySeparatorChar);
            }
        }

        internal Countdown Countdown;
        /// <summary>
        /// Simulates a note happening this many beats earlier for countdown purposes.
        /// </summary>
        internal int CountdownOffset = 0;
        internal ushort countNormal;
        internal ushort countSlider;
        internal ushort countSpinner;
        internal string Creator = string.Empty;
        internal bool CustomColours;
        internal bool DatabaseNotFound;
        internal DateTime DateLastPlayed;
        internal DateTime DateModified;
        internal SampleSet DefaultSampleSet;
        internal string DisplayTitle;
        internal string DisplayTitleFull;
        internal string DisplayTitleNoArtist;
        internal int DrainLength;
        internal int RealDrainLength;
        private double eyupStars;
        private double bemaniStars; //out-dated, used in convertion.
        private double maniaStars;
        // These will be 0 by default. 0 also means, that they are not computed yet, since for any beatmap with hit objects the difficulty will be >0.
        private Dictionary<int, double>[] TomStars = new Dictionary<int, double>[Enum.GetNames(typeof(PlayModes)).Length];
        // This is required to keep track of what has been computed in the _current_ instance of osu! regardless of what's stored in the DB. This is required to prevent computing the difficulty of the same map twice. (nasty race conditions)
        private bool computedDifficulties;
        internal string Filename = string.Empty;
        internal bool HeadersLoaded;
        internal bool IncorrectChecksum;
        internal bool InOszContainer;
        internal bool LetterboxInBreaks = true;
        internal bool WidescreenStoryboard;
        internal bool NewFile;
        internal int ObjectCount;
        internal string OnlineDisplayTitle = string.Empty;
        internal int OnlineOffset;
        internal Score onlinePersonalScore;
        internal float OnlineRating;
        internal List<Score> Scores = new List<Score>();
        internal int PlayerOffset;
        internal Rankings PlayerRankFruits = Rankings.N;
        internal Rankings PlayerRankOsu = Rankings.N;
        internal Rankings PlayerRankTaiko = Rankings.N;
        internal Rankings PlayerRankMania = Rankings.N;

        private PlayModes playMode;
        internal PlayModes PlayMode
        {
            get
            {
                return playMode;
            }

            set
            {
                if (Enum.IsDefined(typeof(PlayModes), value))
                {
                    playMode = value;
                }
                else
                {
                    playMode = PlayModes.Osu;
                }
            }
        }

        internal int PreviewTime;
        internal int SampleVolume = 100;
        internal string SkinPreference = string.Empty;
        internal string SortTitle;
        internal string Source = string.Empty;
        internal float StackLeniency;
        internal bool StoryFireInFront = true;
        internal SubmissionStatus SubmissionStatus = SubmissionStatus.Unknown;
        internal float TimelineZoom = 1;
        internal int TotalLength;
        internal bool UseSkinSpritesInSB;
        internal string Version = string.Empty;
        internal int VersionOffset;
        internal bool SpecialStyle;//used for 7+1 & 5+1 in mania
        internal int ManiaSpeed;
        internal bool SamplesMatchPlaybackRate;

        internal List<ControlPoint> ControlPoints = new List<ControlPoint>();
        internal List<ControlPoint> TimingPoints
        {
            get
            {
                if (ControlPoints == null) return null;
                return ControlPoints.FindAll(c => c.timingChange);
            }
        }

        private string extractionFolder;
        internal string ExtractionFolder
        {
            get
            {
                if (extractionFolder == null)
                    return null;

                //the foler may have vanished somewhere since setting this variable, so it's better to be safe here.
                if (!Directory.Exists(extractionFolder))
                {
                    extractionFolder = null;
                    return null;
                }

                return extractionFolder;
            }

            set
            {
                extractionFolder = value;
            }
        }
        /// <summary>
        /// Is set every time a beatmap info response is retrieved from the server.
        /// Can be used to limit how often the client requests information.
        /// </summary>
        internal long LastInfoUpdate = 0;

        internal bool AllowExtraction
        {
            get
            {
#if DEBUG
                return true;
#else
                return SubmissionStatus > SubmissionStatus.Unknown && SubmissionStatus < SubmissionStatus.EditableCutoff;
#endif
            }
        }
        internal bool UpdateAvailable;
        internal bool OnlineFavourite;
        internal class BeatmapChecksumComparer : IComparer<Beatmap>
        {
            public int Compare(Beatmap a, Beatmap b)
            {
                if (a.BeatmapChecksum == null)
                {
                    if (b.BeatmapChecksum == null) return 0;
                    return 1;
                }
                if (b.BeatmapChecksum == null) return -1;

                return a.BeatmapChecksum.CompareTo(b.BeatmapChecksum);
            }
        }

        public Beatmap()
        {
            for (int i = 0; i < TomStars.Length; ++i)
            {
                TomStars[i] = new Dictionary<int, double>();
            }
        }

        internal Beatmap(string filename)
            : base()
        {
            Filename = Path.GetFileName(filename);
            ContainingFolder = Path.GetDirectoryName(filename);

            ProcessHeaders();
        }

        internal Beatmap Clone()
        {
            return new Beatmap(Path.Combine(ContainingFolder, Filename));
        }

        internal bool ProcessHeaders()
        {
            ControlPoints.Clear();

            bool isNowPlaying = AudioEngine.LoadedBeatmap == this;

            if (isNowPlaying)
            {
                AudioEngine.ResetControlPoints();
                AudioEngine.CustomSamples = CustomSampleSet.Default;
            }

            PreviewTime = -1;
            AudioLeadIn = 0;

            ObjectCount = 0;
            DrainLength = 0;

            countSlider = 0;
            countNormal = 0;
            countSpinner = 0;
            BeatmapVersion = Beatmap.BEATMAP_VERSION;

            VersionOffset = 0;

            StackLeniency = 0.7f;

            Countdown = Countdown.Normal;

            OverlayPosition = OverlayPosition.NoChange;

            CustomColours = false;

            BackgroundImage = null;

            PlayMode = 0;

            DefaultSampleSet = SampleSet.Normal;

            IncorrectChecksum = false;
            bool hasApproachRate = false;

            if (InOszContainer)
            {
                if (Package == null)
                    return false;

                //retrieve the setId from the osz2 package
                if (BeatmapSetId <= 0)
                    BeatmapSetId = Convert.ToInt32(Package.GetMetadata(MapMetaType.BeatmapSetID));
                Title = Package.GetMetadata(MapMetaType.Title);

                ExtractionFolder = Path.Combine(@"Data\e\", BeatmapSetId + " - " + Title);
            }

            using (Stream str = GetBeatmapStream())
            {
                if (str == null)
                {
                    BeatmapPresent = false;
                    BeatmapSetId = -1;
                    BeatmapId = 0;

                    return false;
                }

                BeatmapPresent = true;

                using (StreamReader r = new StreamReader(str))
                {

                    //todo: make this work.
                    UpdateChecksum();

                    DateModified = File.GetLastWriteTimeUtc(InOszContainer ? ContainingFolder : FilenameFull);

                    FileSection currentSection = FileSection.Unknown;

                    int firstTime = -1;
                    int lastTime = -1;
                    int realLastTime = -1;
                    string lastTimeStr = string.Empty;
                    string realLastTimeStr = string.Empty;
                    int breakTime = 0;

                    string[] split;

                    char[] sep = new[] { ',' };

                    try
                    {
                        if (!r.EndOfStream)
                        {
                            string headerInfo = r.ReadLine();
                            if (headerInfo.IndexOf("osu file format") == 0)
                            {
                                BeatmapVersion = Int32.Parse(headerInfo.Remove(0, headerInfo.LastIndexOf('v') + 1));
                                if (isNowPlaying)
                                {
                                    AudioEngine.CustomSamples = BeatmapVersion < 4
                                                                    ? CustomSampleSet.Custom1
                                                                    : CustomSampleSet.Default;
                                }
                                VersionOffset = BeatmapVersion < 5 && GameBase.Mode == OsuModes.Edit ? 24 : 0;
                            }
                        }

                        while (!r.EndOfStream)
                        {
                            string line = r.ReadLine().Trim();

                            if (line.Length == 0)
                                continue;

                            string left = string.Empty;
                            string right = string.Empty;

                            if (currentSection != FileSection.HitObjects)
                            {
                                int index = line.IndexOf(':');

                                if (index >= 0)
                                {
                                    left = line.Remove(index).Trim();
                                    right = line.Remove(0, index + 1).Trim();
                                }
                                else if (line[0] == '[')
                                {
                                    try
                                    {
                                        currentSection = (FileSection)Enum.Parse(typeof(FileSection), line.Trim(new[] { '[', ']' }));
                                    }
                                    catch
                                    {
                                    }
                                    continue;
                                }
                            }

                            switch (currentSection)
                            {
                                case FileSection.TimingPoints:
                                    ControlPoint tp = HitObjectManager.ParseTimingPoint(line, VersionOffset, SampleVolume, DefaultSampleSet);
                                    if (tp != null) ControlPoints.Add(tp);
                                    break;
                                case FileSection.General:
                                    switch (left)
                                    {
                                        case "SampleSet":
                                            DefaultSampleSet = (SampleSet)Enum.Parse(typeof(SampleSet), right);
                                            if (isNowPlaying)
                                                AudioEngine.CurrentSampleSet = DefaultSampleSet;
                                            break;
                                        case "CustomSamples":
                                            if (isNowPlaying)
                                                AudioEngine.CustomSamples = right[0] == '1' ? CustomSampleSet.Custom1 : CustomSampleSet.Default;
                                            break;
                                        case "OverlayPosition":
                                            OverlayPosition = (OverlayPosition)Enum.Parse(typeof(OverlayPosition), right, true);
                                            break;
                                        case "Countdown":
                                            Countdown = (Countdown)Int32.Parse(right);
                                            break;
                                        case "AudioFilename":
                                            if (right.Length > 0)
                                                AudioFilename = right;
                                            break;
                                        case "AudioHash":
                                            AudioExpectedMd5 = right;
                                            break;
                                        case "AudioLeadIn":
                                            AudioLeadIn = Int32.Parse(right);
                                            break;
                                        case "PreviewTime":
                                            PreviewTime = Int32.Parse(right);
                                            break;
                                        case "SampleVolume":
                                            SampleVolume = Int32.Parse(right);
                                            break;
                                        case "StackLeniency":
                                            StackLeniency = Math.Max(0, Math.Min(1, float.Parse(right, GameBase.nfi)));
                                            break;
                                        case "Mode":
                                            PlayMode = (PlayModes)Int32.Parse(right);
                                            break;
                                        case "LetterboxInBreaks":
                                            LetterboxInBreaks = right[0] == '1';
                                            break;
                                        case "WidescreenStoryboard":
                                            WidescreenStoryboard = right[0] == '1';
                                            break;
                                        case "SkinPreference":
                                            SkinPreference = right;
                                            break;
                                        case "AlwaysShowPlayfield":
                                            AlwaysShowPlayfield = right[0] == '1';
                                            break;
                                        case "EpilepsyWarning":
                                            EpilepsyWarning = right[0] == '1';
                                            break;
                                        case "CountdownOffset":
                                            CountdownOffset = Math.Max(Math.Min(Int32.Parse(right), 3), 0);
                                            break;
                                        case "SpecialStyle":
                                            SpecialStyle = right[0] == '1';
                                            break;
                                        case "TimelineZoom":
                                            TimelineZoom = float.Parse(right);
                                            break;
                                        case "SamplesMatchPlaybackRate":
                                            SamplesMatchPlaybackRate = right[0] == '1';
                                            break;
                                    }
                                    break;
                                case FileSection.Metadata:
                                    switch (left)
                                    {
                                        case "Artist":
                                            Artist = right;
                                            break;
                                        case "ArtistUnicode":
                                            ArtistUnicode = right;
                                            break;
                                        case "Title":
                                            Title = right;
                                            break;
                                        case "TitleUnicode":
                                            TitleUnicode = right;
                                            break;
                                        case "Creator":
                                            Creator = right;
                                            break;
                                        case "Version":
                                            Version = right;
                                            break;
                                        case "Tags":
                                            Tags = right;
                                            break;
                                        case "Source":
                                            Source = right;
                                            break;
                                        case "BeatmapID":
                                            if (BeatmapId == 0)
                                                BeatmapId = Convert.ToInt32(right);
                                            break;
                                        case "BeatmapSetID":
                                            if (BeatmapSetId == -1)
                                                BeatmapSetId = Convert.ToInt32(right);
                                            break;
                                    }
                                    break;
                                case FileSection.Difficulty:
                                    switch (left)
                                    {
                                        case "HPDrainRate":
                                            if (BeatmapVersion >= 13)
                                                DifficultyHpDrainRate = Math.Min(10.0f, Math.Max(0.0f, Single.Parse(right, GameBase.nfi)));
                                            else
                                                DifficultyHpDrainRate = Math.Min((byte)10, Math.Max((byte)0, byte.Parse(right)));

                                            break;
                                        case "CircleSize":
                                            if (BeatmapVersion >= 13)
                                            {
                                                if (PlayMode == PlayModes.OsuMania)
                                                    DifficultyCircleSize = Math.Min(18.0f, Math.Max(1.0f, Single.Parse(right, GameBase.nfi)));
                                                else
                                                    DifficultyCircleSize = Math.Min(10.0f, Math.Max(0.0f, Single.Parse(right, GameBase.nfi)));
                                            }
                                            else
                                                DifficultyCircleSize = Math.Min((byte)10, Math.Max((byte)0, byte.Parse(right)));

                                            break;
                                        case "OverallDifficulty":
                                            if (BeatmapVersion >= 13)
                                                DifficultyOverall = Math.Min(10.0f, Math.Max(0.0f, Single.Parse(right, GameBase.nfi)));
                                            else
                                                DifficultyOverall = Math.Min((byte)10, Math.Max((byte)0, byte.Parse(right)));

                                            if (!hasApproachRate) DifficultyApproachRate = DifficultyOverall;
                                            break;
                                        case "SliderMultiplier":
                                            DifficultySliderMultiplier =
                                                Math.Max(0.4, Math.Min(3.6, Double.Parse(right, GameBase.nfi)));
                                            break;
                                        case "SliderTickRate":
                                            DifficultySliderTickRate =
                                                Math.Max(0.5, Math.Min(8, Double.Parse(right, GameBase.nfi)));
                                            break;
                                        case "ApproachRate":
                                            if (BeatmapVersion >= 13)
                                                DifficultyApproachRate = Math.Min(10.0f, Math.Max(0.0f, Single.Parse(right, GameBase.nfi)));
                                            else
                                                DifficultyApproachRate = Math.Min((byte)10, Math.Max((byte)0, byte.Parse(right)));

                                            hasApproachRate = true;
                                            break;
                                    }
                                    break;
                                case FileSection.Events:
                                    switch (line[0])
                                    {
                                        case '0': //EventType.Background
                                            split = line.Split(',');
                                            BackgroundImage = split[2].Trim('"');
                                            break;
                                        case '2': //EventType.Break
                                            split = line.Split(',');
                                            breakTime += (Int32.Parse(split[2]) - Int32.Parse(split[1]));
                                            break;
                                        case '3': //EventType.Color
                                            split = line.Split(',');
                                            BackgroundColour = new Color(byte.Parse(split[2]), byte.Parse(split[3]),
                                                                           byte.Parse(split[4]));
                                            break;
                                        case 'S':  //EventType.Sprite
                                            if (BackgroundImage == null && line[1] == 'P') //not Sample
                                            {
                                                split = line.Split(',');
                                                BackgroundImage = split[3].Trim('"');
                                            }
                                            break;
                                    }
                                    break;
                                case FileSection.HitObjects:
                                    split = line.Split(sep, 7);

                                    if (firstTime == -1)
                                        firstTime = Int32.Parse(split[2]);

                                    int objKind = Convert.ToInt32(split[3]) & 139;

                                    switch (objKind)
                                    {
                                        case 1:
                                            countNormal++;
                                            lastTimeStr = split[2];
                                            realLastTimeStr = lastTimeStr;
                                            break;
                                        case 2:
                                            countSlider++;
                                            //no idea yet
                                            lastTimeStr = split[2];
                                            realLastTimeStr = lastTimeStr;
                                            break;
                                        case 8:
                                            countSpinner++;
                                            lastTimeStr = split[2];
                                            realLastTimeStr = split[5];
                                            break;
                                        case 128:
                                            countSlider++;
                                            lastTimeStr = split[5].Split(':')[0];
                                            realLastTimeStr = lastTimeStr;
                                            break;
                                    }

                                    ObjectCount++;
                                    break;
                                case FileSection.Colours:
                                    CustomColours = true;
                                    break;
                            }
                        }
                    }
                    catch (Exception)
                    {
#if !ARCADE
                        NotificationManager.ShowMessageMassive("Processing beatmap's headers failed.  This map is likely corrupt!", 3000);
#endif
                    }

                    if (BeatmapSetId <= 0 && SubmissionStatus != SubmissionStatus.NotSubmitted && RelativeContainingFolder != null)
                    {
                        //we should try and get this from the folder or filename -- it's better than nothing.

                        string[] parts = RelativeContainingFolder.Split(Path.DirectorySeparatorChar)[0].Split(' ');
                        if (parts.Length > 0)
                        {
                            if (!Int32.TryParse(parts[0], out BeatmapSetId))
                                BeatmapSetId = -1;
                        }
                    }

                    PopulateTitleStatics();

                    if (lastTimeStr.Length > 0)
                        lastTime = Int32.Parse(lastTimeStr);
                    if (realLastTimeStr.Length > 0)
                        realLastTime = Int32.Parse(realLastTimeStr);

                    DrainLength = (lastTime - firstTime - breakTime) / 1000;

                    RealDrainLength = (realLastTime - firstTime - breakTime) / 1000;

                    AudioPresent = CheckFileExists(AudioFilename);

                    TotalLength = realLastTime;

                    ResetCached();

                    HeadersLoaded = true;
                }
            }

            return true;
        }

        internal double DifficultyPeppyStarsRaw
        {
            get
            {
                return
                    (DifficultyHpDrainRate + DifficultyOverall + DifficultyCircleSize +
                     OsuMathHelper.Clamp((float)ObjectCount / DrainLength * 8, 0, 16)) / 38 * 5;
            }
        }

        internal int DifficultyPeppyStars
        {
            get
            {
                return
                    (int)
                    Math.Round((DifficultyHpDrainRate + DifficultyOverall + DifficultyCircleSize +
                                OsuMathHelper.Clamp((float)ObjectCount / DrainLength * 8, 0, 16)) / 38 * 5);
            }
        }

        internal double DifficultyEyupStars
        {
            get
            {
                if (eyupStars > 0) return eyupStars;

                int totalHitObjects = countNormal + 2 * countSlider + 3 * countSpinner;
                double noteDensity = (double)totalHitObjects / DrainLength;
                double difficulty;

                if (totalHitObjects == 0 || ControlPoints.Count == 0)
                    return 0;

                if ((float)countSlider / totalHitObjects < 0.1)
                    difficulty = DifficultyHpDrainRate + DifficultyOverall + DifficultyCircleSize;
                else
                    difficulty = (DifficultyHpDrainRate + DifficultyOverall + DifficultyCircleSize +
                                  Math.Max(0,
                                           (Math.Min(4, 1000 / ControlPoints[0].beatLength * DifficultySliderMultiplier - 1.5) *
                                            2.5))) * 0.75;
                double star;

                if (difficulty > 21) //songs with insane accuracy/circle size/life drain
                    star = (Math.Min(difficulty, 30) / 3 * 4 + Math.Min(20 - 0.032 * Math.Pow(noteDensity - 5, 4), 20)) / 10;
                else if (noteDensity >= 2.5) //songs with insane number of beats per second
                    star = (Math.Min(difficulty, 18) / 18 * 10 +
                            Math.Min(40 - 40 / Math.Pow(5, 3.5) * Math.Pow((Math.Min(noteDensity, 5) - 5), 4), 40)) / 10;
                //exponent of 3.5 is fudged to give better results
                else if (noteDensity < 1) //songs with glacial number of beats per second
                    star = (Math.Min(difficulty, 18) / 18 * 10) / 10 + 0.25;
                else //all other songs of medium difficulty
                    star = (Math.Min(difficulty, 18) / 18 * 10 + Math.Min(25 * (noteDensity - 1), 40)) / 10;

                eyupStars = Math.Min(5, star);
                return eyupStars;
            }
        }


        public BeatmapDifficultyCalculator CreateDifficultyCalculator(PlayModes mode)
        {
            switch (mode)
            {
                case PlayModes.OsuMania:
                    return new BeatmapDifficultyCalculatorMania(this, ModManager.ModStatus & Mods.KeyMod);

                case PlayModes.CatchTheBeat:
                    return new BeatmapDifficultyCalculatorFruits(this);

                case PlayModes.Taiko:
                    return new BeatmapDifficultyCalculatorTaiko(this);

                default:
                    return new BeatmapDifficultyCalculatorOsu(this);
            }
        }

        /// <summary>
        /// Returns the tom stars currently stored for the current PlayMode and the selected mods.
        /// </summary>
        public double DifficultyTomStars(PlayModes mode, Mods mods = Mods.None)
        {
            mods = BeatmapDifficultyCalculator.MaskRelevantMods(mods, mode, this);
            if (!TomStars[(int)mode].ContainsKey((int)mods))
            {
                return -1;
            }

            return TomStars[(int)mode][(int)mods];
        }

        /// <summary>
        /// Sets the tom stars for the selected mods and mode.
        /// </summary>
        internal void SetDifficultyTomStars(PlayModes mode, Mods mods, double difficulty)
        {
            mods = BeatmapDifficultyCalculator.MaskRelevantMods(mods, mode, this);
            TomStars[(int)mode][(int)mods] = difficulty;
        }

        /// <summary>
        /// Sets the tom stars for the selected mode.
        /// </summary>
        internal void SetDifficultiesTomStars(PlayModes mode, Dictionary<int, double> difficulties)
        {
            Dictionary<int, double> current = TomStars[(int)mode];
            foreach (KeyValuePair<int, double> pair in difficulties)
            {
                current[pair.Key] = pair.Value;
            }
        }


        internal void ComputeAndSetDifficultiesTomStarsAllModes()
        {
            if (PlayMode == PlayModes.Osu)
            {
                Array playModesArray = Enum.GetValues(typeof(PlayModes));
                foreach (PlayModes mode in playModesArray)
                {
                    ComputeAndSetDifficultiesTomStars(mode);
                }
            }
            else
            {
                ComputeAndSetDifficultiesTomStars(PlayMode);
            }
        }

        internal void ComputeAndSetDifficultiesTomStars()
        {
            ComputeAndSetDifficultiesTomStars(AppropriateMode);
        }

        internal void ComputeAndSetDifficultiesTomStars(PlayModes mode)
        {
            SetDifficultiesTomStars(mode, ComputeDifficultiesTomStars(mode));
        }

        public void ComputeDifficultiesTomStarsBackground(SmartThreadPool targetThreadPool, WorkItemPriority priority, VoidDelegate callback)
        {
            ComputeDifficultiesTomStarsBackground(targetThreadPool, priority, AppropriateMode, callback);
        }



        /// <summary>
        /// Computes the tom stars for all possible mods and the selected mode in the background and then calls back the display if it exists.
        /// </summary>
        public void ComputeDifficultiesTomStarsBackground(SmartThreadPool targetThreadPool, WorkItemPriority priority, PlayModes mode, VoidDelegate callback)
        {
            // If the stars are not 0, then we don't need to compute anything.
            if (DifficultyTomStars(mode, ModManager.ModStatus) >= 0)
            {
                return;
            }

            // We know at this point that no difficulties have been computed. In a rare race condition this value may get set to false while another thread.
            // Is working on the difficulty calculation. We choose to accept that since it's very rare.
            computedDifficulties = false;

            // We can't find the difficulty online - need to compute locally
            targetThreadPool.QueueWorkItem(new Action(delegate
            {
                lock (this)
                {
                    if (computedDifficulties)
                    {
                        return;
                    }

                    computedDifficulties = true;
                }

                Dictionary<int, double> Difficulties = ComputeDifficultiesTomStars(mode);

                GameBase.Scheduler.Add(delegate
                {
                    SetDifficultiesTomStars(mode, Difficulties);
                    callback();
                });
            }), priority);
        }

        /// <summary>
        /// Computes the tom stars for all possible mods and the selected mode.
        /// </summary>
        public Dictionary<int, double> ComputeDifficultiesTomStars(PlayModes mode)
        {
            BeatmapDifficultyCalculator bdc = CreateDifficultyCalculator(mode);

            Dictionary<int, double> Difficulties = new Dictionary<int, double>();
            foreach (Mods Mods in bdc.ModCombinations)
            {
                Difficulties[(int)Mods] = bdc.GetDifficulty(Mods, null);
            }

            bdc.Dispose();

            return Difficulties;
        }


        /// <summary>
        /// deprecated
        /// </summary>
        internal double DifficultyEchoStars
        {
            get
            {
                // map eyupstars to a log scale
                return 5 - (5 * Math.Log((5 - DifficultyEyupStars) + 1, 6));
            }
        }

        /// <summary>
        /// Used for converted map (internal calculation)only
        /// </summary>
        internal double DifficultyBemaniStars
        {
            get
            {
                if (bemaniStars > 0)
                    return bemaniStars;
                float time = 0;
                if (DrainLength == 0)
                    time = 10000;
                else
                    time = DrainLength;
                if (PlayMode == PlayModes.OsuMania)
                    bemaniStars = (double)(((DifficultyHpDrainRate + DifficultyCircleSize) / 1.5 + Math.Max(((float)ObjectCount / time) * 9f, 0f)) / 38f * 5f) / 1.5;
                else
                    bemaniStars =
                        (double)(((DifficultyHpDrainRate + OsuMathHelper.Clamp(DifficultyApproachRate, 4, 7)) / 1.5 +
                                Math.Max(((float)ObjectCount / time) * 9f, 0f)) / 38f * 5f) / 1.15;
                bemaniStars = Math.Min(bemaniStars, 12);
                return bemaniStars;
            }
        }

        /// <summary>
        /// Used for star diaply for specific maps
        /// Version 2.2
        /// </summary>
        internal double DifficultBemaniStarDisplay
        {
            get
            {
                if (maniaStars > 0)
                    return maniaStars;
                if (ControlPoints == null || ControlPoints.Count == 0) return 0;

                double time = DrainLength == 0 ? (ControlPoints[ControlPoints.Count - 1].offset + 1) / 1000 : DrainLength;//when the only note is hold, drainLenght == 0
                float objCount = countNormal + countSlider * 1.2f;
                if (objCount == 0)
                    maniaStars = 0;
                else
                    maniaStars = Math.Min((DifficultyHpDrainRate / 14 + DifficultyOverall / 12) + (objCount / time) / 2.3 * Math.Pow(1.04, DifficultyCircleSize - 3), 5);
                return maniaStars;
            }
        }

        //reset star, force recalculate star next time.
        internal void ResetStar()
        {
            maniaStars = 0;
            bemaniStars = 0;
            eyupStars = 0;

            foreach (Dictionary<int, double> Difficulties in TomStars)
            {
                Difficulties.Clear();
            }

            lock (this)
            {
                computedDifficulties = false;
            }
        }

        internal double StarDisplay
        {
            get
            {
                return DifficultyTomStars(AppropriateMode, ModManager.ModStatus);
            }
        }

        internal double StarDisplayEyup
        {
            get
            {
                if (PlayMode == PlayModes.OsuMania)
                {
                    return DifficultBemaniStarDisplay;
                }
                else
                {
                    return DifficultyEyupStars;
                }
            }
        }

        internal double StarDisplayWithEyupFallback
        {
            get
            {
                double difficultyTomStars = StarDisplay;
                if (difficultyTomStars >= 0)
                {
                    return difficultyTomStars;
                }

                return StarDisplayEyup;
            }
        }

        internal PlayModes AppropriateMode
        {
            get
            {
                return PlayMode == PlayModes.Osu ? Player.Mode : PlayMode;
            }
        }

        internal string InfoText
        {
            get
            {
                double CS = HitObjectManager.ApplyModsToDifficulty(DifficultyCircleSize, 1.3, ModManager.ModStatus);
                double HP = HitObjectManager.ApplyModsToDifficulty(DifficultyHpDrainRate, 1.4, ModManager.ModStatus);
                double AR = HitObjectManager.ApplyModsToDifficulty(DifficultyApproachRate, 1.4, ModManager.ModStatus);
                double OD = HitObjectManager.ApplyModsToDifficulty(DifficultyOverall, 1.4, ModManager.ModStatus);

                double starDifficulty = StarDisplay;
                string difficultyFormat = String.Empty;

                string SpeedModificationIndicator = (ModManager.CheckActive(Mods.DoubleTime) ? @"▴" : (ModManager.CheckActive(Mods.HalfTime) ? @"▾" : @""));

                PlayModes mode = AppropriateMode;

                if (mode == PlayModes.Osu || mode == PlayModes.CatchTheBeat)
                {
                    difficultyFormat += @"CS:{0:0.0} AR:{1:0.0}" + SpeedModificationIndicator + @" ";
                }
                else if (mode == PlayModes.OsuMania)
                {
                    difficultyFormat += @"Keys:{0:0} ";
                }

                difficultyFormat += @"OD:{2:0.0}" + SpeedModificationIndicator + @" HP:{3:0.0}" + SpeedModificationIndicator + @" ";

                if (starDifficulty >= 0)
                {
                    difficultyFormat += @"Stars:{4:0.00}";
                }

                string result = string.Format(difficultyFormat,
                    mode == PlayModes.OsuMania ? StageMania.ColumnsWithMods(this) : Math.Round(CS, 1),
                    Math.Round(AR, 1),
                    Math.Round(OD, 1),
                    Math.Round(HP, 1),
                    Math.Round(starDifficulty, 2));

                if (PlayerOffset != 0 || OnlineOffset != 0)
                    result += '\n' + string.Format(LocalisationManager.GetString(OsuString.SongSelection_CustomOffset), (PlayerOffset > 0 ? @"+" : string.Empty), PlayerOffset, (OnlineOffset > 0 ? @"+" : string.Empty), OnlineOffset);

                return result;
            }
        }

        internal double BeatsPerMinute
        {
            get
            {
                if (ControlPoints == null || ControlPoints.Count == 0)
                    return 0;
                double lowest = 5000;
                for (int i = 0; i < ControlPoints.Count; i++)
                    if (ControlPoints[i].timingChange && ControlPoints[i].beatLength < lowest)
                        lowest = ControlPoints[i].beatLength;
                if (lowest == 0)
                    return 0;
                return 60000 / lowest;
            }
        }

        internal Vector3 BeatsPerMinuteRange
        {
            get
            {
                if (ControlPoints == null || ControlPoints.Count == 0)
                    return Vector3.Zero;

                double min = double.MaxValue;
                double max = double.MinValue;

                double lastTime = TotalLength;  //Not include last slider's end time.
                double thisBeatLength = 0;

                Dictionary<double, int> bpmDurations = new Dictionary<double, int>();

                for (int i = ControlPoints.Count - 1; i >= 0; i--)
                {
                    ControlPoint cp = ControlPoints[i];

                    if (cp.timingChange)
                        thisBeatLength = cp.beatLength;

                    if (thisBeatLength == 0 || cp.offset > lastTime || (!cp.timingChange && i > 0)) continue; //this should never happen

                    if (thisBeatLength < min) min = thisBeatLength;
                    if (thisBeatLength > max) max = thisBeatLength;

                    if (bpmDurations.ContainsKey(thisBeatLength))
                        bpmDurations[thisBeatLength] += (int)(lastTime - (i == 0 ? 0 : cp.offset));
                    else
                        bpmDurations[thisBeatLength] = (int)(lastTime - (i == 0 ? 0 : cp.offset));

                    lastTime = cp.offset;
                }

                int longestTime = 0;
                double median = 0;

                foreach (KeyValuePair<double, int> p in bpmDurations)
                {
                    if (p.Value > longestTime)
                    {
                        longestTime = p.Value;
                        median = p.Key;
                    }
                }

                return new Vector3((float)Math.Round(60000 / max), (float)Math.Round(60000 / min), (float)Math.Round(60000 / median));
            }
        }

        internal string BeatsPerMinuteRangeString
        {
            get
            {
                return RangeToString(BeatsPerMinuteRange);
            }
        }

        internal static string RangeToString(Vector3 range)
        {
            if (range.X == range.Y)
                return range.X.ToString("0");
            else
                return string.Format("{0:0}-{1:0} ({2:0})", range.X, range.Y, range.Z);
        }

        /// <summary>
        /// Beats the offset at.
        /// </summary>
        /// <param name="time">The time.</param>
        /// <returns></returns>
        internal double beatOffsetCloseToZeroAt(double time)
        {
            if (ControlPoints == null || ControlPoints.Count == 0)
                return 0;

            int point = 0;

            for (int i = 0; i < ControlPoints.Count; i++)
                if (ControlPoints[i].timingChange && ControlPoints[i].offset <= time)
                    point = i;

            double length = ControlPoints[point].beatLength;
            double offset = ControlPoints[point].offset;
            if (point == 0 && length > 0)
                while (offset > 0)
                    offset -= length;
            return offset;
        }

        internal double beatOffsetAt(double time)
        {
            if (ControlPoints == null || ControlPoints.Count == 0)
                return 0;

            int point = 0;

            for (int i = 0; i < ControlPoints.Count; i++)
                if (ControlPoints[i].timingChange && ControlPoints[i].offset <= time)
                    point = i;

            return ControlPoints[point].offset;
        }


        internal double beatLengthAt(double time, bool allowMultiplier = true)
        {
            if (ControlPoints == null || ControlPoints.Count == 0)
                return 0;

            int point = 0;
            int samplePoint = 0;

            for (int i = 0; i < ControlPoints.Count; i++)
                if (ControlPoints[i].offset <= time)
                {
                    if (ControlPoints[i].timingChange)
                        point = i;
                    else
                        samplePoint = i;
                }

            double mult = 1;

            if (allowMultiplier && samplePoint > point && ControlPoints[samplePoint].beatLength < 0)
                mult = ControlPoints[samplePoint].bpmMultiplier;

            return ControlPoints[point].beatLength * mult;
        }

        internal float bpmMultiplierAt(double time)
        {
            ControlPoint pt = controlPointAt(time);

            if (pt == null) return 1.0f;
            else return pt.bpmMultiplier;
        }

        // todo: refactor the above searches to use this
        internal int controlPointIndexAt(double time, bool allowInherit)
        {
            if (ControlPoints == null || ControlPoints.Count == 0) return -1;

            int point = 0;

            if (allowInherit)
            {
                for (int i = 0; i < ControlPoints.Count; i++)
                {
                    ControlPoint pt = ControlPoints[i];
                    if (pt.offset <= time) point = i;
                }
            }
            else
            {
                for (int i = 0; i < ControlPoints.Count; i++)
                {
                    ControlPoint pt = ControlPoints[i];
                    if ((pt.offset <= time) && (pt.timingChange)) point = i;
                }
            }

            return point;
        }

        /// <summary>
        /// Best-effort finding algorithm. Only returns null if there are no controlPoints available.
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        internal ControlPoint controlPointAt(double time)
        {
            if (ControlPoints == null || ControlPoints.Count == 0) return null;

            ControlPoint point = null;
            for (int i = 0; i < ControlPoints.Count; i++)
            {
                ControlPoint pt = ControlPoints[i];
                if (pt.offset <= time)
                    point = pt;
            }

            if (point == null)
                point = ControlPoints[0];

            return point;
        }

        internal ControlPoint controlPointAtBin(double time)
        {
            return customPointAtBin(time, ControlPoints);
        }

        internal static ControlPoint customPointAtBin(double time, List<ControlPoint> list)
        {
            if (list == null || list.Count == 0)
                return null;

            ControlPoint dummy = new ControlPoint(time, 0);
            int index = list.BinarySearch(dummy);
            if (index < 0)
                index = ~index;

            if (index <= 1)
                return list[0];

            return list[index - 1];
        }

        internal Rankings PlayerRank
        {
            get
            {
                switch (Player.Mode)
                {
                    case PlayModes.Osu:
                        return PlayerRankOsu;
                    case PlayModes.Taiko:
                        return PlayerRankTaiko;
                    case PlayModes.CatchTheBeat:
                        return PlayerRankFruits;
                    case PlayModes.OsuMania:
                        return PlayerRankMania;
                    default:
                        return Rankings.N;
                }
            }

            set
            {
                switch (Player.Mode)
                {
                    case PlayModes.Osu:
                        PlayerRankOsu = value;
                        return;
                    case PlayModes.Taiko:
                        PlayerRankTaiko = value;
                        return;
                    case PlayModes.CatchTheBeat:
                        PlayerRankFruits = value;
                        return;
                    case PlayModes.OsuMania:
                        PlayerRankMania = value;
                        return;
                    default:
                        return;
                }
            }
        }

        internal string StoryboardFilename
        {
            get
            {
                string storyboardFile = Filename.Replace(@".osu", @"");

                if (storyboardFile.Length == 0) return null;

                if (storyboardFile[storyboardFile.Length - 1] == ']')
                    storyboardFile = storyboardFile.Remove(storyboardFile.LastIndexOf('['));
                storyboardFile = storyboardFile.Trim() + @".osb";
                return storyboardFile;
            }
        }

        /// <summary>
        /// True after we have checked for this package once.  We don't want to be constantly checking.
        /// </summary>
        bool checkedForPackage;

        internal MapPackage Package
        {
            get
            {
                if (!InOszContainer)
                    return null;

                if (!checkedForPackage && _package != null) return _package;

                MapPackage package = Osz2Factory.TryOpen(ContainingFolder);

                if (_package != null || checkedForPackage)
                {
                    _package = package;
                    return package;
                }

                if (package == null)
                {
                    string name = ContainingFolder.Replace(".osz2", "") + ".osz2";
                    if (File.Exists(name))
                    {
                        package = Osz2Factory.TryOpen(name);
                        if (package == null)
                        {
                            checkedForPackage = false;
                            return null;
                        }
                        ContainingFolder = name;
                        InOszContainer = true;

                        //BeatmapManager.OszPackages.Add(package);
                    }
                    else
                    {
                        return null;
                    }
                }
                checkedForPackage = true;

                _package = package;
                return _package;
            }

            set
            {
                if (value == null) return;

                _package = value;
                InOszContainer = true;
            }
        }

        internal string FilenameFull
        {
            get
            {
                if (!InOszContainer)
                    return Path.Combine(ContainingFolder, Filename);
                if (ExtractionFolder != null && AllowOverrides)
                    return Path.Combine(ExtractionFolder, Filename);

                return Filename;
            }
        }

        #region bSerializable Members

        public void ReadFromStream(SerializationReader sr)
        {
            Artist = sr.ReadString();
            if (BeatmapManager.DatabaseVersion >= General.VERSION_FIRST_OSZ2)
                ArtistUnicode = sr.ReadString();
            Title = sr.ReadString();
            if (BeatmapManager.DatabaseVersion >= General.VERSION_FIRST_OSZ2)
                TitleUnicode = sr.ReadString();
            Creator = sr.ReadString();
            Version = sr.ReadString();
            AudioFilename = sr.ReadString();
            BeatmapChecksum = sr.ReadString();
            Filename = sr.ReadString();
            SubmissionStatus = (SubmissionStatus)sr.ReadByte();
            countNormal = sr.ReadUInt16();
            countSlider = sr.ReadUInt16();
            countSpinner = sr.ReadUInt16();
            DateModified = sr.ReadDateTime();

            if (BeatmapManager.DatabaseVersion >= 20140612)
            {
                DifficultyApproachRate = sr.ReadSingle();
                DifficultyCircleSize = sr.ReadSingle();
                DifficultyHpDrainRate = sr.ReadSingle();
                DifficultyOverall = sr.ReadSingle();
            }
            else
            {
                DifficultyApproachRate = sr.ReadByte();
                DifficultyCircleSize = sr.ReadByte();
                DifficultyHpDrainRate = sr.ReadByte();
                DifficultyOverall = sr.ReadByte();
            }

            DifficultySliderMultiplier = sr.ReadDouble();
#if Public
            if (BeatmapManager.DatabaseVersion >= 20140609)
#else
            if (BeatmapManager.DatabaseVersion >= 20140606)
#endif
            {
                // We discard stored difficulties if they are not recent
                if (BeatmapManager.DatabaseVersion < 20140610)
                {
                    for (int i = 0; i < 4; ++i)
                    {
                        sr.ReadDictionary<Mods, double>();
                    }
                }
                else
                {
                    for (int i = 0; i < 4; ++i)
                    {
                        if (BeatmapManager.DatabaseVersion < VERSION_DIFFICULTY[i])
                        {
                            sr.ReadDictionary<int, double>();
                        }
                        else
                        {
                            TomStars[i] = (Dictionary<int, double>)sr.ReadDictionary<int, double>();
                        }
                    }
                }
            }
            DrainLength = sr.ReadInt32();
            TotalLength = sr.ReadInt32();
            PreviewTime = sr.ReadInt32();
            ControlPoints = (List<ControlPoint>)sr.ReadBList<ControlPoint>();
            BeatmapId = sr.ReadInt32();
            BeatmapSetId = sr.ReadInt32();
            BeatmapTopicId = sr.ReadInt32();
            PlayerRankOsu = (Rankings)sr.ReadByte();
            PlayerRankFruits = (Rankings)sr.ReadByte();
            PlayerRankTaiko = (Rankings)sr.ReadByte();
            PlayerRankMania = (Rankings)sr.ReadByte();
            PlayerOffset = sr.ReadInt16();
            StackLeniency = sr.ReadSingle();
            PlayMode = (PlayModes)sr.ReadByte();
            Source = sr.ReadString();
            Tags = sr.ReadString();
            OnlineOffset = sr.ReadInt16();
            OnlineDisplayTitle = sr.ReadString();
            NewFile = sr.ReadBoolean();
            DateLastPlayed = sr.ReadDateTime();
            InOszContainer = sr.ReadBoolean();
            ContainingFolder = sr.ReadString();
            LastInfoUpdate = sr.ReadInt64();
            DisableSamples = sr.ReadBoolean();
            DisableSkin = sr.ReadBoolean();
            DisableStoryboard = sr.ReadBoolean();
            if (BeatmapManager.DatabaseVersion >= 20130624)
                DisableVideo = sr.ReadBoolean();
            if (BeatmapManager.DatabaseVersion >= 20130913)
                VisualSettingsOverride = sr.ReadBoolean();
            else
                VisualSettingsOverride = DisableVideo || DisableStoryboard || DisableSkin;
            if (BeatmapManager.DatabaseVersion < 20140608)
                sr.ReadInt16(); //used to contain per-map dim.
            LastEditTime = sr.ReadInt32();
            ManiaSpeed = sr.ReadByte();

            ObjectCount = countNormal + countSpinner + countSlider;
            BeatmapPresent = BeatmapChecksum != null;
            AudioPresent = true;

            PopulateTitleStatics();
        }

        public void WriteToStream(SerializationWriter sw)
        {
            sw.Write(Artist);
            sw.Write(ArtistUnicode);
            sw.Write(Title);
            sw.Write(TitleUnicode);
            sw.Write(Creator);
            sw.Write(Version);
            sw.Write(AudioFilename);
            sw.Write(BeatmapChecksum);
            sw.Write(Filename);
            sw.Write((byte)SubmissionStatus);
            sw.Write(countNormal);
            sw.Write(countSlider);
            sw.Write(countSpinner);
            sw.Write(DateModified);
            sw.Write(DifficultyApproachRate);
            sw.Write(DifficultyCircleSize);
            sw.Write(DifficultyHpDrainRate);
            sw.Write(DifficultyOverall);
            sw.Write(DifficultySliderMultiplier);
            for (int i = 0; i < 4; ++i)
            {
                sw.Write(TomStars[i]);
            }
            sw.Write(DrainLength);
            sw.Write(TotalLength);
            sw.Write(PreviewTime);
            sw.Write(ControlPoints);
            sw.Write(BeatmapId);
            sw.Write(BeatmapSetId);
            sw.Write(BeatmapTopicId);
            sw.Write((byte)PlayerRankOsu);
            sw.Write((byte)PlayerRankFruits);
            sw.Write((byte)PlayerRankTaiko);
            sw.Write((byte)PlayerRankMania);
            sw.Write((short)PlayerOffset);
            sw.Write(StackLeniency);
            sw.Write((byte)PlayMode);
            sw.Write(Source);
            sw.Write(Tags);
            sw.Write((short)OnlineOffset);
            sw.Write(OnlineDisplayTitle);
            sw.Write(NewFile);
            sw.Write(DateLastPlayed);
            sw.Write(InOszContainer);
            sw.Write(RelativeContainingFolder);
            sw.Write(LastInfoUpdate);
            sw.Write(DisableSamples);
            sw.Write(DisableSkin);
            sw.Write(DisableStoryboard);
            sw.Write(DisableVideo);
            sw.Write(VisualSettingsOverride);
            sw.Write(LastEditTime);
            sw.Write((byte)ManiaSpeed);
        }

        #endregion

        #region IComparable<Beatmap> Members

        public int CompareTo(Beatmap other)
        {
            return Filename.CompareTo(other.Filename);
        }

        #endregion


        internal void CheckUpToDate(GotOsz2Hashes callWhenReceived)
        {
            GotOsz2Hashes compareHashes = (b, h) =>
            {
                if (b == null || h == null)
                {
                    ServerHasOsz2 = false;
                    callWhenReceived(null, null);
                    return;
                }
                ServerHasOsz2 = true;

                if (!InOszContainer)
                    return;

                if (!Package.AcquireLock(100, false))
                    NotificationManager.ShowMessage("Error getting beatmap version: beatmap is in use.");

                UpdateAvailable = !Toolbox.ByteMatch(Package.hash_body, b) ||
                                  !Toolbox.ByteMatch(Package.hash_meta, h);

                callWhenReceived(b, h);
            };

            if (BeatmapSetId > 0)
                OsuMagnetFactory.WebGetOz2Hashes(BeatmapSetId, compareHashes);
        }



        StringNetRequest scoreRetrievalRequest;

        /// <summary>
        /// We only want to get one beatmap's scores at a time, so let's keep track of the last requested and cancel previously delayed retrievals.
        /// </summary>
        static Beatmap lastScoresRequestedBeatmap;
        static PlayModes lastScoresRequestedMode;

        internal bool IsRetrievingScores { get { return scoreRetrievalRequest != null; } }

        internal void GetOnlineScores(bool getScores, bool force, RankingType rankType = RankingType.Top)
        {
            if (scoreRetrievalRequest != null && !force) return;

            Scores.Clear();
            lastScoresRequestedBeatmap = this;

            PlayModes mode = PlayMode != PlayModes.Osu ? PlayMode : Player.Mode;
            lastScoresRequestedMode = mode;

            if (scoreRetrievalRequest != null)
            {
                scoreRetrievalRequest.onFinish -= getOnlineScoresCallback;
                scoreRetrievalRequest.Abort();
            }

            string osz2Hash = string.Empty;
            if (Package != null)
            {
                osz2Hash = (BitConverter.ToString(Package.hash_body) +
                BitConverter.ToString(Package.hash_meta)).Replace("-", "");
            }

            int delay = 400;

            switch (rankType)
            {
                case RankingType.Friends:
                    delay = 1000;
                    break;
                case RankingType.Country:
                    delay = 1000;
                    break;
            }

            string url = General.WEB_ROOT + "/web/osu-osz2-getscores.php?s=" + (getScores ? "0" : "1") +
                "&vv=2" +
                "&v=" + (int)rankType +
                "&c=" + BeatmapChecksum +
                "&f=" + GeneralHelper.UrlEncodeParam(Path.GetFileName(Filename)) +
                "&m=" + (int)(mode) +
                "&i=" + BeatmapSetId +
                "&mods=" + (int)(Mods)ModManager.ModStatus +
                "&h=" + osz2Hash +
                "&us=" + ConfigManager.sUsername + "&ha=" + ConfigManager.sPassword;

            scoreRetrievalRequest = new StringNetRequest(url);
            scoreRetrievalRequest.onFinish += getOnlineScoresCallback;

            GameBase.Scheduler.AddDelayed(delegate
            {
                if (lastScoresRequestedBeatmap != this || lastScoresRequestedMode != mode)
                {
                    scoreRetrievalRequest = null; //we were ousted by a newer request in the wait time.
                    return;
                }

                NetManager.AddRequest(scoreRetrievalRequest);
            }, force ? 0 : delay);
        }


        private void getOnlineScoresCallback(string data, Exception e)
        {
            if (e != null)
            {
                scoreRetrievalRequest = null;
                return;
            }

            IncorrectChecksum = false;

            try
            {
                string[] result = data.Split('\n');

                Scores.Clear();

                if (result.Length > 0)
                {

                    UpdateAvailable = false;
                    string[] statusResults = result[0].Split('|');
                    Boolean.TryParse(statusResults[1], out ServerHasOsz2);

                    switch (statusResults[0])
                    {
                        case "-1":
                            SubmissionStatus = SubmissionStatus.NotSubmitted;
                            IncorrectChecksum = true;
                            break;
                        case "0":
                            SubmissionStatus = SubmissionStatus.Pending;
                            break;
                        case "1":
                            SubmissionStatus = SubmissionStatus.Unknown;
                            UpdateAvailable = true;
                            break;
                        default:
                            SubmissionStatus = SubmissionStatus.Ranked;
                            break;
                        case "3":
                            SubmissionStatus = SubmissionStatus.Approved;
                            break;
                    }

                    if (statusResults.Length > 2)
                    {
                        Int32.TryParse(statusResults[2], out BeatmapId);
                        Int32.TryParse(statusResults[3], out BeatmapSetId);
                        Int32.TryParse(statusResults[4], out TotalRecords);
                    }
                    //UpdateAvailable = true;
                }

                if (result.Length > 1 && result[1].Length > 0)
                {
                    int offset = Int32.Parse(result[1]);
                    if (offset != OnlineOffset && PlayerOffset != 0)
                    {
                        NotificationManager.ShowMessageMassive(
                            "New online offset available.  Adjusting overall local offset from " + PlayerOffset + " to " +
                            offset + ".", 2000);
                        PlayerOffset = 0;
                    }

                    OnlineOffset = offset;
                    OnlineDisplayTitle = result[2].Replace('|', '\n');
                    OnlineRating = float.Parse(result[3], GameBase.nfi);
                }

                if (result.Length > 4)
                {
                    if (result[4].Length > 0)
                    {
                        onlinePersonalScore = ScoreFactory.CreateFromOnline(PlayMode != PlayModes.Osu ? PlayMode : Player.Mode, result[4], this);
                        if (DateLastPlayed == DateTime.MinValue)
                            DateLastPlayed = onlinePersonalScore.date;
                    }
                    else
                        onlinePersonalScore = null;

                    for (int i = 5; i < result.Length; i++)
                    {
                        string linein = result[i];
                        if (linein.Length == 0)
                            break;

                        Score score = ScoreFactory.CreateFromOnline(PlayMode != PlayModes.Osu ? PlayMode : Player.Mode, result[i], this);

                        Score dupe = Scores.Find(s => s.playerName == score.playerName);
                        if (dupe != null)
                        {
                            if (dupe.onlineRank > 0)
                                continue;
                            Scores.Remove(dupe);
                        }

                        Scores.Add(score);
                    }

                    Scores.Sort((a, b) =>
                    {
                        int c = b.totalScore.CompareTo(a.totalScore);
                        if (c != 0) return c;
                        return a.date.CompareTo(b.date);
                    });
                }
            }
            catch
            {
                NotificationManager.ShowMessage("Scores could not be retrieved.");
            }

            GameBase.Scheduler.Add(delegate
            {
                if (OnGotOnlineScores != null)
                    OnGotOnlineScores(this);
                scoreRetrievalRequest = null;
            });
        }

        internal event GotOnlineScoresHandler OnGotOnlineScores;
        internal OverlayPosition OverlayPosition;

        //Version 4 introduces custom samplesets per timing section.
        //Version 5 changes the map's offset by 24ms due to an internal calculation change.
        //Version 6 changes stacking algorithm and fixes animation speeds for storyboarded sprites.
        //Version 7 fixes multipart bezier slider math error (http://osu.sifterapp.com/projects/4151/issues/145)
        //Version 8 mm additions: constant sliderticks-per-beat.
        //                        HP drain changes near breaks
        //                        Taiko triple drumrolls
        //Version 9 makes bezier the default slider type, which now handles linear corners better.
        //          Spinner new combos are no longer forced. (Some restrictions re-imposed by the editor.)
        //Version 10 fixes sliders being 1/50 shorter than they should be for every bezier part.
        //Version 11 Support hold notes.
        //Version 14 Support per-node samplesets on sliders (ctb)
        public const int BEATMAP_VERSION = 14;
        public bool EpilepsyWarning;

        public bool DisableSamples;
        public bool DisableSkin;
        public bool DisableStoryboard;
        public bool DisableVideo;
        public bool VisualSettingsOverride;

        public int? DimLevel;
        public object BackgroundVisible = true;

        internal void CancelRequests()
        {
            StringNetRequest req = scoreRetrievalRequest;
            if (req != null) req.Abort();

            scoreRetrievalRequest = null;
            OnGotOnlineScores = null;
        }

        internal void PopulateTitleStatics()
        {
            string artist = Artist;

            if (Artist.Length > 0)
                SortTitle = artist + " - " + Title;
            else if (Title.Length > 0)
                SortTitle = Title;
            else
                SortTitle = Path.GetFileName(Filename);

            string versionString = (Version.Length > 0 ? " [" + Version + "]" : "");

            if (Artist.Length > 0)
            {
                DisplayTitle = artist + " - " + Title + versionString;
                artist = Source.Length > 0 ? Source + " (" + Artist + ")" : Artist;
                DisplayTitleFull = artist + " - " + Title + versionString;
            }
            else if (Title.Length > 0)
            {
                DisplayTitle = Title + versionString;
                DisplayTitleFull = DisplayTitle;
            }
            else
            {
                DisplayTitle = Path.GetFileName(Filename);
                DisplayTitleFull = DisplayTitle;
            }

            if (Title.Length > 0)
                DisplayTitleNoArtist = Title + versionString;
            else
                DisplayTitleNoArtist = Path.GetFileName(Filename);
        }

        internal void LoadUrlListing()
        {
            GameBase.ProcessStart(String.Format(Urls.BEATMAP_LISTING, BeatmapId));
        }

        internal void LoadUrlTopic()
        {
            GameBase.ProcessStart(String.Format(Urls.FORUM_TOPIC, BeatmapTopicId));
        }

        internal void LoadUrlReply()
        {
            GameBase.ProcessStart(String.Format(Urls.BEATMAP_REPLY, BeatmapTopicId));
        }

        internal void ReplaceWithUpdate()
        {
#if !DEBUG
            try
            {
#endif
            string filename = Path.Combine(ContainingFolder, Filename);
            File.Delete(filename);
            File.Move(filename + "_", filename);

            string oldChecksum = BeatmapChecksum;

            List<string> existCollection = CollectionManager.FindCollectionByMap(oldChecksum);

            ProcessHeaders();

            if (existCollection != null && existCollection.Count > 0)
            {
                foreach (string key in existCollection)
                {
                    CollectionManager.RemoveFromCollection(key, oldChecksum);
                    CollectionManager.AddToCollection(key, BeatmapChecksum);
                }

            }

            ResetStar();
            if (GameBase.RunningGameMode is SongSelection)
            {
                SongSelection ss = (SongSelection)GameBase.RunningGameMode;
                ss.ScheduleBeatmapDifficultyCalculation(this, WorkItemPriority.Normal);
            }
#if !DEBUG
            }
            catch
            {
            }
#endif
        }

        public override string ToString()
        {
            return DisplayTitle;
        }

        internal void ClearRankings()
        {
            PlayerRankFruits = Rankings.N;
            PlayerRankTaiko = Rankings.N;
            PlayerRankOsu = Rankings.N;
            PlayerRankMania = Rankings.N;
        }

        internal bool HasFileOverride(string filename)
        {
            return AllowOverrides && ExtractionFolder != null && File.Exists(Path.Combine(ExtractionFolder, filename));
        }

        internal Stream GetAudioStream()
        {
            if (!AudioPresent) return null;

            if (Package == null || HasFileOverride(AudioFilename))
                return GetFileStream(AudioFilename);

            Stream stream = Package.GetFile(AudioFilename);
            if (stream == null)
            {
                AudioPresent = false;
                return null;
            }
            return stream;
        }

        internal byte[] GetAudioBytes()
        {
            if (!AudioPresent) return null;

            if (Package == null || HasFileOverride(AudioFilename))
                return GetFileBytes(AudioFilename);

            using (Stream stream = Package.GetFile(AudioFilename))
            {
                if (stream == null)
                {
                    AudioPresent = false;
                    return null;
                }

                byte[] data = new byte[stream.Length];
                stream.Read(data, 0, data.Length);
                stream.Close();

                return data;
            }
        }

        internal Stream GetBeatmapStream()
        {
            return GetFileStream(Filename);
        }

        internal Stream GetFileStream(string filename)
        {
            try
            {
                if (Package == null)
                {
                    filename = Path.Combine(ContainingFolder, filename);

                    if (!File.Exists(filename)) return null;

                    return File.OpenRead(filename);
                }

                if (HasFileOverride(filename))
                    return File.OpenRead(Path.Combine(ExtractionFolder, filename));


                return Package.GetFile(filename);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        internal byte[] GetFileBytes(string filename)
        {
            byte[] data = null;
            if (Package != null && !Package.AcquireLock(1, false))
                return null;
            try
            {
                using (Stream stream = GetFileStream(filename))
                {
                    if (stream != null)
                    {
                        data = new byte[stream.Length];
                        stream.Read(data, 0, data.Length);
                        stream.Close();
                    }

                }
            }
            finally
            {
                if (Package != null)
                    Package.Unlock();
            }

            return data;
        }
        /*
        internal bool ConvertToFolder()
        {
            if (!InOszContainer) return false;

            BeatmapManager.SongWatcher.EnableRaisingEvents = false;

            string folder = ContainingFolder.Replace(".osz2", "");

            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            try
            {
                Package.ExtractAll(folder);
            }
            catch
            {
                BeatmapManager.SongWatcher.EnableRaisingEvents = true;
                return false;
            }

            AudioEngine.FreeMusic();

            File.Delete(ContainingFolder);

            List<Beatmap> sameSet = BeatmapManager.Beatmaps.FindAll(b => b.ContainingFolder == ContainingFolder);

            sameSet.ForEach(b => {
                b.ContainingFolder = folder;
                b._package = null;
                b.InOszContainer = false;
            });

            BeatmapManager.SongWatcher.EnableRaisingEvents = true;

            return true;
        }*/

        internal void ConvertToOsz2()
        {
            MapPackage p = ConvertToOsz2(Path.Combine(BeatmapManager.SONGS_DIRECTORY, RelativeContainingFolder + ".osz2"), true);
            if (p != null)
                Osz2Factory.CloseMapPackage(p); //todo: fix this maybe? change behaviour maybe?
        }

        internal MapPackage ConvertToOsz2(string osz2Path, bool replace)
        {
#if DEBUG
            if (BeatmapSetId <= 0)
                BeatmapSetId = -1;
#else
            if (BeatmapSetId <= 0) throw new InvalidOperationException("Cannot convert a map to osz2 which does not have a Beatmapset ID.");
#endif
            if (InOszContainer) return null;

            BeatmapManager.ShowChangeNotifications = false;

            MapPackage newPackage;

            try
            {
                //osz2path = BeatmapManager.SONGS_DIRECTORY + ContainingFolder.Remove(0, ContainingFolder.LastIndexOf('/') + 1) + ".osz2";

                BeatmapManager.SongWatcher.EnableRaisingEvents = false;
                //remove if it already exists:
                if (File.Exists(osz2Path))
                    File.Delete(osz2Path);

                newPackage = Osz2Factory.TryOpen(osz2Path, true, false);

                if (newPackage == null)
                    return null;

                if (!newPackage.AcquireLock(50, false))
                    return null;
                try
                {
                    newPackage.AddDirectory(ContainingFolder, true);
                    newPackage.AddMetadata(MapMetaType.BeatmapSetID, Convert.ToString(BeatmapSetId));
                    newPackage.AddMetadata(MapMetaType.Artist, Artist);
                    newPackage.AddMetadata(MapMetaType.Creator, Creator);
                    newPackage.AddMetadata(MapMetaType.Source, Source);
                    newPackage.AddMetadata(MapMetaType.Title, Title);
                    newPackage.AddMetadata(MapMetaType.TitleUnicode, TitleUnicode);
                    newPackage.AddMetadata(MapMetaType.ArtistUnicode, ArtistUnicode);
                    newPackage.AddMetadata(MapMetaType.Tags, Tags);
                }
                catch
                {
                    newPackage.Close();
                    return null;
                }

                string oldContainingFolder = ContainingFolder;

                List<Beatmap> sameSet = BeatmapManager.Beatmaps.FindAll(b => b.ContainingFolder == ContainingFolder);

                sameSet.ForEach(b =>
                {
                    if (replace)
                    {
                        b.ContainingFolder = osz2Path;
                        b.Filename = Path.GetFileName(b.Filename);
                        b.AudioFilename = Path.GetFileName(b.AudioFilename);
                        b._package = newPackage;
                        b.InOszContainer = true;
                    }
                    newPackage.SetMapID(b.Filename, b.BeatmapId);
                });

                newPackage.Save(false);

                if (!ContainingFolder.EndsWith(GameBase.BeatmapDirectory) && replace)
                {
                    AudioEngine.FreeMusic(); //moved this inside the if block as it was unloading unnecessarily -peppy
                    Directory.Delete(oldContainingFolder, true);
                }
            }
            finally
            {
                BeatmapManager.ShowChangeNotifications = true;
                BeatmapManager.SongWatcher.EnableRaisingEvents = true;
            }

            //newPackage.Unlock();

            return newPackage;
        }

        internal void DeleteFile(string filename)
        {
            if (InOszContainer)
            {
                Package.RemoveFile(filename);
                Package.Save();
            }
            else
                RecycleBin.SendSilent(Path.Combine(ContainingFolder, filename));
        }

        internal void UpdateChecksum()
        {

            string newChecksum = CryptoHelper.GetMd5String(GetFileBytes(Filename));

            if (newChecksum == BeatmapChecksum)
                return;

            bool didRemove = BeatmapManager.Remove(this);
            List<string> existCollection = CollectionManager.FindCollectionByMap(BeatmapChecksum);

            if (existCollection != null && existCollection.Count > 0)
            {
                foreach (string key in existCollection)
                {
                    CollectionManager.RemoveFromCollection(key, BeatmapChecksum);
                    CollectionManager.AddToCollection(key, newChecksum);
                }
            }
            BeatmapChecksum = newChecksum;
            if (didRemove) BeatmapManager.Add(this);
        }

        internal void ResetCached()
        {
            eyupStars = 0;
        }

        internal bool CheckFileExists(string filename)
        {
            if (string.IsNullOrEmpty(filename)) return false;

            if (Package == null)
                return File.Exists(Path.Combine(ContainingFolder, filename));

            return Package.FileExists(filename) || HasFileOverride(filename);
        }

        //A temporary replacement for p2p updating
        internal void UpdateOsz2Container()
        {
            if (Package == null)
                throw new Exception("Can only update osz2 packages.");

            PackageUpdater.UpdateCurrentPackage();
        }

        public enum CopyColissionMode
        {
            Ask,
            Overwrite,
            OverwriteIfNewer,
            Ignore
        }

        internal string RequireExtractionFolder()
        {
            if (!InOszContainer) return null;

            string _extractionFolder = ExtractionFolder;
            if (_extractionFolder == null)
                _extractionFolder = Path.Combine(@"Data\e\", BeatmapSetId + " - " + Title);

            if (!Directory.Exists(_extractionFolder))
                Directory.CreateDirectory(_extractionFolder);

            ExtractionFolder = _extractionFolder;

            return ExtractionFolder;
        }

        internal string Osz2ExtractSingleFile(string filename, bool overwriteExisting)
        {
            byte[] file = GetFileBytes(filename);

            if (file == null)
                return null;

            RequireExtractionFolder();

            string fullPath = Path.Combine(ExtractionFolder, filename);

            if (File.Exists(fullPath) && !overwriteExisting)
                return fullPath;

            try
            {


                File.WriteAllBytes(fullPath, file);
            }
            catch
            {
                return null;
            }

            return fullPath;
        }

        internal bool Osz2ExtractSafe(CopyColissionMode onCopyColission = CopyColissionMode.OverwriteIfNewer, bool ignoreNonExtractedFiles = false)
        {
            bool ret = true;

            RequireExtractionFolder();

            if (!Package.AcquireLock(100, false))
                return false;

            try
            {
                List<osu_common.Libraries.Osz2.FileInfo> files = Package.GetFileInfo();
                foreach (osu_common.Libraries.Osz2.FileInfo fileInfo in files)
                {
                    string file = fileInfo.Filename;

#if !DEBUG
                    if ((MapPackage.IsVideoFile(file) || MapPackage.IsMusicFile(file)) && (!osu.Online.BanchoClient.Connected || Creator != GameBase.User.Name))
                        continue;
#endif

                    string fullPath = Path.Combine(ExtractionFolder, file);
                    string dirName = Path.GetDirectoryName(fullPath);
                    if (!Directory.Exists(dirName))
                        Directory.CreateDirectory(dirName);


                    if (File.Exists(fullPath))
                    {
                        DateTime fileOverrideLastWrite = File.GetLastWriteTimeUtc(fullPath);
                        System.IO.FileInfo curFileInfo = new System.IO.FileInfo(fullPath);

                        long fileOverriddenFileTime = 0;
                        long fileBaseFileTime = 0;

                        try
                        {
                            fileOverriddenFileTime = fileOverrideLastWrite.ToFileTime();
                            fileBaseFileTime = fileInfo.ModifiedTime.ToFileTime();
                        }
                        catch { }

                        //within 4 second accuracy
                        if (Math.Abs(fileBaseFileTime - fileOverriddenFileTime) < 200000000 &&
                            curFileInfo.Length == fileInfo.Length - 4)
                            continue;

                        switch (onCopyColission)
                        {
                            case CopyColissionMode.Ask:
                                using (FileColissionDialog fileColissionDialog = new FileColissionDialog(fullPath, (int)curFileInfo.Length, curFileInfo.LastWriteTime,
                                                                                                  Path.Combine(ContainingFolder, file), fileInfo.Length, fileInfo.ModifiedTime.ToLocalTime()))
                                {
                                    if (fileColissionDialog.ShowDialog(GameBase.Form) == DialogResult.Ignore)
                                        continue;

                                    switch (fileColissionDialog.Result)
                                    {
                                        case FileColissionDialog.DialogOption.No:
                                            continue;
                                        case FileColissionDialog.DialogOption.NoToAll:
                                            onCopyColission = CopyColissionMode.Ignore;
                                            continue;
                                        case FileColissionDialog.DialogOption.YesToAll:
                                            onCopyColission = CopyColissionMode.Overwrite;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                break;
                            case CopyColissionMode.Ignore:
                                continue;
                            case CopyColissionMode.Overwrite:
                                break;
                            case CopyColissionMode.OverwriteIfNewer:
                                if (fileInfo.ModifiedTime < fileOverrideLastWrite)
                                    continue;
                                break;
                        }
                    }
                    else if (ignoreNonExtractedFiles)
                    {
                        continue;
                    }


                    using (Stream fileP = Package.GetFile(file))
                    {
                        if (fileP.Length == 0)
                            File.WriteAllBytes(fullPath, new byte[] { });
                        else
                        {
                            using (FileStream fileC = File.Create(fullPath, (int)fileP.Length, FileOptions.WriteThrough))
                            {
                                byte[] buffer = new byte[fileP.Length];
                                fileP.Read(buffer, 0, buffer.Length);
                                fileC.Write(buffer, 0, buffer.Length);
                                fileC.Flush();
                            }
                        }
                    }

                    try
                    {
                        File.SetCreationTimeUtc(fullPath, fileInfo.CreationTime);
                        File.SetLastWriteTimeUtc(fullPath, fileInfo.ModifiedTime);
                    }
                    catch
                    {
                        //may fail on non-existent timestamps
                    }
                }
            }
#if DEBUG
            catch (ExecutionEngineException e)
#else
            catch
#endif
            {
                ret = false;
            }
            finally
            {
                Package.Unlock();
            }

            return ret;


        }

        internal void Cleanup()
        {
            if (_package != null)
            {
                Osz2Factory.CloseMapPackage(_package);
                _package = null;
            }
        }
        #region unicode display
        /// <summary>
        /// Show unicode title and artist automatically
        /// </summary>
        internal string TitleAuto
        {
            get
            {
                if (ConfigManager.sShowUnicode && !string.IsNullOrEmpty(TitleUnicode))
                    return TitleUnicode;
                else
                    return Title;
            }
        }

        internal string ArtistAuto
        {
            get
            {
                if (ConfigManager.sShowUnicode && !string.IsNullOrEmpty(ArtistUnicode))
                    return ArtistUnicode;
                else
                    return Artist;
            }
        }

        internal string SortTitleAuto
        {
            get
            {
                if (ConfigManager.sShowUnicode && !string.IsNullOrEmpty(ArtistUnicode) && !string.IsNullOrEmpty(TitleUnicode))
                    return ArtistUnicode + " - " + TitleUnicode;
                else
                    return SortTitle;
            }
        }

        internal string DisplayTitleFullAuto
        {
            get
            {
                if (ConfigManager.sShowUnicode && !string.IsNullOrEmpty(ArtistUnicode) && !string.IsNullOrEmpty(TitleUnicode))
                {
                    string artist = Source.Length > 0 ? Source + " (" + ArtistUnicode + ")" : ArtistUnicode;
                    return artist + " - " + TitleUnicode + (Version.Length > 0 ? " [" + Version + "]" : "");
                }
                else
                    return DisplayTitleFull;
            }
        }
        #endregion
    }

    internal enum Countdown
    {
        Disabled = 0,
        Normal = 1,
        HalfSpeed = 2,
        DoubleSpeed = 3
    }

    internal enum OverlayPosition
    {
        NoChange = 0,
        Below = 1,
        Above = 2
    }

}
