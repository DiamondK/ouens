﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.Events.Trigger;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Scoring;
using osu.Graphics.Notifications;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu_common.Helpers;
using Un4seen.Bass;
using osu.GameplayElements.HitObjects.Fruits;

namespace osu.GameplayElements
{
    internal abstract partial class HitObjectManager
    {
        /// <summary>
        /// Loads the file.  Returns false if file was not found (and creates an empty map).
        /// </summary>
        /// <returns></returns>
        internal bool LoadWithEvents()
        {
            bool result = Load(true);
            LoadEvents(false);
            eventManager.UpdateBackground();

            return result;
        }

        /// <summary>
        /// Loads the file.
        /// Note: Must call LoadEvents to trigger the initial load of event content.
        /// Loads the file.  Returns false if file was not found (and creates an empty map).
        /// </summary>
        /// <param name="processHeaders">if set to <c>true</c> [process headers].</param>
        /// <returns></returns>
        internal bool Load(bool processHeaders)
        {
            AudioEngine.ClearSampleEvents();
            AudioEngine.ClearBeatmapCache();
            SkinManager.BeatmapColours.Clear();

            if (processHeaders)
                Beatmap.ProcessHeaders();

            if (eventManager != null) eventManager.Dispose();
            eventManager = new EventManager(this);

            if (GameBase.Mode != OsuModes.Edit)
            {
                spriteManager.ForwardPlayOptimisedAdd = true;
                eventManager.spriteManagerBG.ForwardPlayOptimisedAdd = true;
                eventManager.spriteManagerFG.ForwardPlayOptimisedAdd = true;
            }

            UpdateVariables(false);

            if (!BookmarksDontDelete)
            {
                if (Bookmarks == null)
                    Bookmarks = new List<int>();
                else
                    Bookmarks.Clear();
            }

            BookmarksDontDelete = false;

            Clear();

            bool beatmapExists = Beatmap.BeatmapPresent;

            if (beatmapExists)
            {
                try
                {
                    parse();
                }
                catch (Exception ex)
                {
                    String FailureMsg = LocalisationManager.GetString(OsuString.HitObjectManager_LoadSave_BeatmapFailLoad);
                    if (ex is OsbParserException)
                    {
                        FailureMsg += String.Format(LocalisationManager.GetString(OsuString.HitObjectManager_LoadSave_StoryboardErrorOnLine), ((OsbParserException)ex).LineNumber);
                    }

                    if (GameBase.Mode == OsuModes.Edit && DialogResult.Yes != NotificationManager.MessageBox(FailureMsg + LocalisationManager.GetString(OsuString.HitObjectManager_LoadSave_EditAsNewMap), MessageBoxButtons.YesNo, MessageBoxIcon.Error))
                    {
                        NotificationManager.MessageBox("osu! will do its best to load what it can of the map...", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        //throw new OsuFileUnreadableException();
                    }
                    else
                    {
                        Clear();
                        beatmapExists = false;
                    }
                }
            }

            Sort(false);

            List<int> loadableSampleSets = new List<int>();
            foreach (ControlPoint tp in Beatmap.ControlPoints)
                if ((int)tp.customSamples > 2 && !loadableSampleSets.Contains((int)tp.customSamples))
                    loadableSampleSets.Add((int)tp.customSamples);
            foreach (HitObject h in hitObjects)
                if ((int)h.CustomSampleSet > 2 && !loadableSampleSets.Contains((int)h.CustomSampleSet))
                    loadableSampleSets.Add((int)h.CustomSampleSet);
            foreach (int i in loadableSampleSets)
                AudioEngine.LoadExtraSamples(i);

            AudioEngine.LoadAllSamples();

            AudioEngine.CurrentSampleSet = SampleSet.None;

            if (Beatmap.BeatmapVersion < 4 || !beatmapExists)
                AudioEngine.LoadSampleSet(Beatmap.DefaultSampleSet, false);


            PostProcessing();
            eventManager.PostProcessing();

            if (GameBase.Mode != OsuModes.Edit)
            {
                spriteManager.ForwardPlayOptimisedAdd = false;
                eventManager.spriteManagerBGWide.ForwardPlayOptimisedAdd = false;
                eventManager.spriteManagerBG.ForwardPlayOptimisedAdd = false;
                eventManager.spriteManagerFG.ForwardPlayOptimisedAdd = false;
            }

            if (GameBase.Mode == OsuModes.Edit || Player.Mode == PlayModes.Osu)
                SkinManager.ComputeColours(this); // Only compute slider textures in osu!mode

            return beatmapExists;
        }

        /// <summary>
        /// Parses the sections contained in SectionsToParse of the beatmap .osu file.
        /// </summary>
        internal void parse(FileSection SectionsToParse = FileSection.All, bool updateChecksum = true)
        {
            FileSection currentSection = FileSection.Unknown;

            //Check file just before load -- ensures no modifications have occurred.
            if (updateChecksum)
            {
                Beatmap.UpdateChecksum();
            }

            List<string> readableFiles = new List<string>();

            readableFiles.Add(Beatmap.Filename);

            string storyBoardFile = Beatmap.StoryboardFilename;

            if (Beatmap.CheckFileExists(storyBoardFile))
                readableFiles.Add(storyBoardFile);

            bool hasCustomColours = false;
            bool hitObjectPreInit = false;
            bool firstColour = true;

            bool verticalFlip = (GameBase.Mode == OsuModes.Play &&
                                 ModManager.CheckActive(ActiveMods, Mods.HardRock));

            int linenumber;

            Variables = new Dictionary<string, string>();

            //The first file will be the actual .osu file.
            //The second file is the .osb for now.
            for (int fn = 0; fn < readableFiles.Count; fn++)
            {
                using (TextReader reader = (TextReader)new StreamReader(Beatmap.GetFileStream(readableFiles[fn])))
                {
                    linenumber = 0;

                    string line = null;

                    bool readNew = true;

                    while (true)
                    {
                        if (readNew)
                        {
                            line = reader.ReadLine();

                            if (line == null) break;

                            linenumber++;
                        }

                        readNew = true;

                        if (line.Length == 0 || line.StartsWith(@" ") || line.StartsWith(@"_") || line.StartsWith(@"//"))
                            continue;

                        try
                        {
                            if (currentSection == FileSection.Events) ParseVariables(ref line);
                        }
                        catch (Exception ex)
                        {
                            throw new OsbParserException(linenumber, ex);
                        }


                        if (line[0] == '[')
                        {
                            try
                            {
                                currentSection = (FileSection)Enum.Parse(typeof(FileSection), line.Trim(new[] { '[', ']' }));
                            }
                            catch (Exception)
                            {
                            }
                            continue;
                        }

                        // We might only want to parse certain parts of the .osu file (for performance mostly)
                        if ((SectionsToParse & currentSection) <= 0)
                        {
                            continue;
                        }

                        string[] split = line.Trim().Split(',');
                        string[] var = line.Trim().Split(':');
                        string key = string.Empty;
                        string val = string.Empty;
                        if (var.Length > 1)
                        {
                            key = var[0].Trim();
                            val = var[1].Trim();
                        }

                        switch (currentSection)
                        {
                            case FileSection.General:
                                switch (key)
                                {
                                    case @"EditorBookmarks":
                                        string[] strlist = val.Split(',');
                                        foreach (string s in strlist)
                                            if (s.Length > 0)
                                            {
                                                int bm = Int32.Parse(s);
                                                if (!Bookmarks.Contains(bm))
                                                    Bookmarks.Add(bm);
                                            }
                                        break;
                                    case @"EditorDistanceSpacing":
                                        ConfigManager.sDistanceSpacing.Value = Convert.ToDouble(val, GameBase.nfi);
                                        break;
                                    case @"StoryFireInFront":
                                        Beatmap.StoryFireInFront = val[0] == '1';
                                        break;
                                    case @"UseSkinSprites":
                                        Beatmap.UseSkinSpritesInSB = val[0] == '1';
                                        break;
                                }
                                break;
                            case FileSection.Editor:
                                //We only need to read this section if we are in the editor.
                                if (GameBase.Mode != OsuModes.Edit && !GameBase.TestMode)
                                    continue;

                                switch (key)
                                {
                                    case @"Bookmarks":
                                        string[] strlist = val.Split(',');
                                        foreach (string s in strlist)
                                            if (s.Length > 0)
                                            {
                                                int bm = Int32.Parse(s);
                                                if (!Bookmarks.Contains(bm))
                                                    Bookmarks.Add(bm);
                                            }
                                        break;
                                    case @"DistanceSpacing":
                                        ConfigManager.sDistanceSpacing.Value = Convert.ToDouble(val, GameBase.nfi);
                                        break;
                                    case @"BeatDivisor":
                                        ConfigManager.sEditorBeatDivisor.Value = Convert.ToInt32(val, GameBase.nfi);
                                        break;
                                    case @"GridSize":
                                        ConfigManager.sEditorGridSize.Value = Convert.ToInt32(val, GameBase.nfi);
                                        break;
                                    case @"TimelineZoom":
                                        Beatmap.TimelineZoom = (float)Convert.ToDecimal(val, GameBase.nfi);
                                        break;
                                }

                                break;
                            case FileSection.Colours:
                                if (!hasCustomColours)
                                {
                                    hasCustomColours = true;
                                    for (int w = 1; w <= SkinManager.MAX_COLOUR_COUNT; w++)
                                        SkinManager.BeatmapColours["Combo" + w.ToString()] = Color.TransparentWhite;

                                }

                                if ((!Beatmap.DisableSkin && !ConfigManager.sIgnoreBeatmapSkins)
                                    || GameBase.Mode == OsuModes.Edit)
                                {
                                    string[] splitn = val.Split(',');
                                    SkinManager.BeatmapColours[key] =
                                        new Color((byte)Convert.ToInt32(splitn[0]), (byte)Convert.ToInt32(splitn[1]),
                                                    (byte)Convert.ToInt32(splitn[2]));
                                }
                                break;
                            case FileSection.Variables:
                                string[] varSplit = line.Split('=');
                                if (varSplit.Length != 2) continue;
                                Variables.Add(varSplit[0], varSplit[1]);
                                break;
                            case FileSection.Events:
                                try
                                {
                                    EventTypes eType;
                                    Event e = null;

                                    try
                                    {
                                        eType = (EventTypes)Enum.Parse(typeof(EventTypes), split[0]);
                                    }
                                    catch
                                    {
                                        MessageBox.Show(string.Format(
                                            LocalisationManager.GetString(OsuString.HitObjectManager_LoadSave_StoryboardErrorOnLine_2), readableFiles[fn], linenumber, line) +
                                            LocalisationManager.GetString(OsuString.HitObjectManager_LoadSave_StoryboardErrorContent_1));
                                        continue;
                                    }

                                    switch (eType)
                                    {
                                        //case EventTypes.Colour:
                                        //    Color newColour =
                                        //        new Color((byte)Int32.Parse(split[2]), (byte)Int32.Parse(split[3]),
                                        //                    (byte)Int32.Parse(split[4]));
                                        //    int startTime = Int32.Parse(split[1]) + Beatmap.VersionOffset;

                                        //    if (firstColour)
                                        //    {
                                        //        //special case for the first colour of the song.
                                        //        eventManager.defaultPlayfieldBackground.InitialColour = newColour;
                                        //        EventManager.backgroundColour = newColour;
                                        //        firstColour = false;
                                        //    }

                                        //    Color prevColour = eventManager.defaultPlayfieldBackground.InitialColour;

                                        //    if (eventManager.defaultPlayfieldBackground.Transformations.Count > 0)
                                        //        prevColour =
                                        //            eventManager.defaultPlayfieldBackground.Transformations[
                                        //                eventManager.defaultPlayfieldBackground.Transformations.Count - 1].EndColour;
                                        //    eventManager.defaultPlayfieldBackground.Transformations.Add(
                                        //        new Transformation(prevColour, newColour,
                                        //                            startTime - 100 + Beatmap.VersionOffset,
                                        //                            startTime + 100 + Beatmap.VersionOffset));
                                        //    break;
                                        case EventTypes.Video:
                                        case EventTypes.Background:
                                            e = eventManager.Add(split[2].Trim(new[] { '"' }), Int32.Parse(split[1], GameBase.nfi) + Beatmap.VersionOffset);

                                            if (e != null)
                                            {
                                                if (e.Sprite != null)
                                                {
                                                    if (split.Length > 4)
                                                    {
                                                        Vector2 offset = new Vector2(Int32.Parse(split[3]), Int32.Parse(split[4]));
                                                        if (offset != Vector2.One)
                                                            e.Sprite.Position = offset;
                                                    }
                                                }

                                                e.LineNumber = linenumber;
                                                e.WriteToOsu = fn == 0;
                                            }
                                            break;
                                        case EventTypes.Sprite:
                                            try
                                            {
                                                e =
                                                    new EventSprite(GeneralHelper.CleanStoryboardFilename(split[3]),
                                                                    new Vector2(Int32.Parse(split[4]), Int32.Parse(split[5])),
                                                                    (Origins)Enum.Parse(typeof(Origins), split[2]),
                                                                    (StoryLayer)Enum.Parse(typeof(StoryLayer), split[1]),
                                                                    Beatmap.UseSkinSpritesInSB);
                                                e.LineNumber = linenumber;
                                                e.WriteToOsu = fn == 0;
                                            }
                                            catch (Exception)
                                            {
                                                continue;
                                            }
                                            eventManager.Add(e);
                                            break;
                                        case EventTypes.Animation:
                                            try
                                            {
                                                e =
                                                    new EventAnimation(GeneralHelper.CleanStoryboardFilename(split[3]),
                                                                        new Vector2(Int32.Parse(split[4]),
                                                                                    Int32.Parse(split[5])),
                                                                        (Origins)
                                                                        Enum.Parse(typeof(Origins), split[2]),
                                                                        (StoryLayer)
                                                                        Enum.Parse(typeof(StoryLayer), split[1]),
                                                                        Int32.Parse(split[6]),
                                                                        double.Parse(split[7], GameBase.nfi),
                                                                        Beatmap.UseSkinSpritesInSB,
                                                                        (LoopTypes)(split.Length > 8 ? Enum.Parse(typeof(LoopTypes), split[8]) : LoopTypes.LoopForever));
                                                e.LineNumber = linenumber;
                                                e.WriteToOsu = fn == 0;
                                            }
                                            catch
                                            {
                                                continue;
                                            }
                                            eventManager.Add(e);
                                            break;
                                        case EventTypes.Sample:
                                            try
                                            {
                                                string filename = GeneralHelper.CleanStoryboardFilename(split[3]);

                                                //    byte[] bytes = beatmap.GetFileBytes(filename);
                                                int channel = AudioEngine.LoadBeatmapSample(filename, false, true);
                                                //    int channel = bytes == null ? 0 : Bass.BASS_SampleLoad(bytes, 0, bytes.Length, 5, BASSFlag.BASS_SAMPLE_OVER_POS);
                                                e =
                                                    new EventSample(channel, filename,
                                                                    Int32.Parse(split[1], GameBase.nfi) +
                                                                    Beatmap.VersionOffset,
                                                                    (StoryLayer)Enum.Parse(typeof(StoryLayer), split[2]),
                                                                    split.Length > 4
                                                                        ? Math.Min(100,
                                                                                    Math.Max(0,
                                                                                            Int32.Parse(split[4],
                                                                                                        GameBase.nfi)))
                                                                        : 100);
                                                e.LineNumber = linenumber;
                                                e.WriteToOsu = fn == 0;
                                            }
                                            catch
                                            {
                                            }
                                            eventManager.Add(e);
                                            break;
                                        case EventTypes.Break:
                                            if (fn > 0)
                                                continue;
                                            e =
                                                new EventBreak(Int32.Parse(split[1], GameBase.nfi) + Beatmap.VersionOffset,
                                                    Int32.Parse(split[2], GameBase.nfi) + Beatmap.VersionOffset);
                                            e.LineNumber = linenumber;
                                            e.WriteToOsu = true;

                                            if (e.Length > 650)
                                                eventManager.Add(e);
                                            break;
                                    }

                                    line = reader.ReadLine();

                                    if (line == null) continue;

                                    ParseVariables(ref line);
                                    linenumber++;

                                    TransformationLoop currentLoop = null;

                                    bool hasStartTime = false;

                                    while (line.Trim().Length > 0 && (line[0] == ' ' || line[0] == '_'))
                                    {
                                        try
                                        {
                                            if (!EventManager.ShowStoryboard)
                                            {
                                                switch (eType)
                                                {
                                                    case EventTypes.Sprite:
                                                    case EventTypes.Animation:
                                                    case EventTypes.Video:
                                                        break;
                                                    default:
                                                        continue;
                                                }
                                            }

                                            split = line.Trim(' ', '_').Split(',');

                                            int arg = 1;

                                            int argCount = split.Length;

                                            if (line[1] != ' ' && line[1] != '_')
                                                currentLoop = null;

                                            EasingTypes easing = EasingTypes.None;
                                            int start = 0, end = 0, duration = 0;

                                            switch (split[0])
                                            {
                                                case @"L":
                                                case @"T":
                                                    break;
                                                default:
                                                    easing = (EasingTypes)Int32.Parse(split[arg++]);

                                                    start = Int32.Parse(split[arg++]) + Beatmap.VersionOffset;

                                                    if (split[arg].Length == 0)
                                                    {
                                                        end = start;
                                                        arg++;
                                                    }
                                                    else
                                                        end = Int32.Parse(split[arg++]) +
                                                                Beatmap.VersionOffset;

                                                    duration = end - start;
                                                    break;
                                            }

                                            if (end > eventManager.lastEventTime)
                                                eventManager.lastEventTime = end;
                                            if (start < eventManager.firstEventTime)
                                                eventManager.firstEventTime = start;

                                            bool first = true;

                                            int argBeforeLoop = -1; //ensure we don't get stuck on a value.

                                            while (arg < argCount && argBeforeLoop != arg)
                                            {
                                                argBeforeLoop = arg;

                                                //Found a transformation for current event
                                                Transformation t = null;


                                                switch (split[0])
                                                {
                                                    case @"M":
                                                        {
                                                            arg -= (first ? 0 : 2);

                                                            bool last = arg == argCount - 2;

                                                            Vector2 v1 = new Vector2(float.Parse(split[arg++], GameBase.nfi),
                                                                                        float.Parse(split[arg++], GameBase.nfi));
                                                            Vector2 v2 = last
                                                                                ? v1
                                                                                : new Vector2(float.Parse(split[arg++], GameBase.nfi),
                                                                                            float.Parse(split[arg++], GameBase.nfi));

                                                            t = new Transformation(v1, v2, start, end);
                                                        }

                                                        break;
                                                    case @"MX":
                                                        {
                                                            arg -= (first ? 0 : 1);

                                                            bool last = arg == argCount - 1;

                                                            float f1 = float.Parse(split[arg++], GameBase.nfi);
                                                            float f2 = last ? f1 : float.Parse(split[arg++], GameBase.nfi);
                                                            t = new Transformation(TransformationType.MovementX, f1, f2, start, end);
                                                        }
                                                        break;
                                                    case @"MY":
                                                        {
                                                            arg -= (first ? 0 : 1);

                                                            bool last = arg == argCount - 1;

                                                            float f1 = float.Parse(split[arg++], GameBase.nfi);
                                                            float f2 = last ? f1 : float.Parse(split[arg++], GameBase.nfi);
                                                            t = new Transformation(TransformationType.MovementY, f1, f2, start, end);
                                                        }
                                                        break;
                                                    case @"F":
                                                        {
                                                            arg -= (first ? 0 : 1);

                                                            bool last = arg == argCount - 1;

                                                            float f1 = float.Parse(split[arg++], GameBase.nfi);
                                                            float f2 = last ? f1 : float.Parse(split[arg++], GameBase.nfi);
                                                            t = new Transformation(TransformationType.Fade, f1, f2, start,
                                                                                    end);
                                                        }
                                                        break;
                                                    case @"R":
                                                        {
                                                            arg -= (first ? 0 : 1);

                                                            bool last = arg == argCount - 1;

                                                            float f1 = float.Parse(split[arg++], GameBase.nfi);
                                                            float f2 = last ? f1 : float.Parse(split[arg++], GameBase.nfi);
                                                            t = new Transformation(TransformationType.Rotation, f1, f2,
                                                                                    start, end);
                                                        }
                                                        break;
                                                    case @"S":
                                                        {
                                                            arg -= (first ? 0 : 1);

                                                            bool last = arg == argCount - 1;

                                                            float f1 = float.Parse(split[arg++], GameBase.nfi);
                                                            float f2 = last ? f1 : float.Parse(split[arg++], GameBase.nfi);
                                                            t = new Transformation(TransformationType.Scale, f1, f2, start,
                                                                                    end);
                                                        }
                                                        break;
                                                    case @"V":
                                                        {
                                                            arg -= (first ? 0 : 1);

                                                            bool last = arg == argCount - 2;

                                                            Vector2 v1 = new Vector2(float.Parse(split[arg++], GameBase.nfi),
                                                                                        float.Parse(split[arg++], GameBase.nfi));
                                                            Vector2 v2 = last
                                                                                ? v1
                                                                                : new Vector2(float.Parse(split[arg++], GameBase.nfi),
                                                                                            float.Parse(split[arg++], GameBase.nfi));

                                                            t = new Transformation(TransformationType.VectorScale, v1, v2,
                                                                                    start, end);
                                                        }
                                                        break;
                                                    case @"C":
                                                        {
                                                            arg -= (first ? 0 : 3);

                                                            bool last = arg == argCount - 3;

                                                            Color c1 = new Color(byte.Parse(split[arg++]),
                                                                                    byte.Parse(split[arg++]),
                                                                                    byte.Parse(split[arg++]));
                                                            Color c2 = last
                                                                            ? c1
                                                                            : new Color(byte.Parse(split[arg++]),
                                                                                        byte.Parse(split[arg++]),
                                                                                        byte.Parse(split[arg++]));
                                                            t = new Transformation(c1, c2, start, end);
                                                            break;
                                                        }
                                                    case @"P":
                                                        arg -= (first ? 0 : 3);

                                                        t = new Transformation();

                                                        t.Time1 = start;
                                                        t.Time2 = end;

                                                        switch (split[arg++])
                                                        {
                                                            case @"H":
                                                                if (e.Sprite != null && t.Duration == 0)
                                                                    e.Sprite.FlipHorizontal = true;
                                                                t.Type = TransformationType.ParameterFlipHorizontal;
                                                                break;
                                                            case @"V":
                                                                if (e.Sprite != null && t.Duration == 0)
                                                                    e.Sprite.FlipVertical = true;
                                                                t.Type = TransformationType.ParameterFlipVertical;
                                                                break;
                                                            case @"A":
                                                                if (e.Sprite != null && t.Duration == 0)
                                                                    e.Sprite.Additive = true;
                                                                t.Type = TransformationType.ParameterAdditive;
                                                                break;
                                                        }
                                                        break;
                                                    case @"L":
                                                        currentLoop =
                                                            new TransformationLoop(
                                                                Int32.Parse(split[arg++]) +
                                                                Beatmap.VersionOffset,
                                                                Int32.Parse(split[arg++]));
                                                        if (e != null)
                                                        {
                                                            if (e.Sprite.Loops == null)
                                                                e.Sprite.Loops = new List<TransformationLoop>();
                                                            e.Sprite.Loops.Add(currentLoop);
                                                        }
                                                        break;
                                                    case @"T":
                                                        EventTrigger trigger = EventTrigger.Create(split[arg++]);
                                                        if (split.Length > 2)
                                                        {
                                                            start = Int32.Parse(split[arg++]) +
                                                                    Beatmap.VersionOffset;
                                                            end = Int32.Parse(split[arg++]) +
                                                                    Beatmap.VersionOffset;
                                                        }
                                                        if (split.Length > 4)
                                                        {
                                                            TriggerGroup triggerGroup = (TriggerGroup)(-Int32.Parse(split[arg++]));
                                                            if (triggerGroup != TriggerGroup.None)
                                                                trigger.SetSpecificTriggerGroup(triggerGroup);
                                                        }

                                                        currentLoop = new TriggerLoop(trigger, start, end);

                                                        if (e != null)
                                                            e.BindEvent(currentLoop as TriggerLoop);
                                                        break;
                                                }


                                                if (t != null)
                                                {
                                                    t.Easing = easing;
                                                    if (currentLoop != null)
                                                    {
                                                        currentLoop.Transformations.AddInPlace(t);

                                                        if (!hasStartTime || currentLoop.StartTime < e.StartTime)
                                                        {
                                                            e.StartTime = currentLoop.StartTime;
                                                            hasStartTime = true;
                                                        }
                                                    }
                                                    else if (e != null)
                                                    {
                                                        e.Sprite.Transformations.AddInPlace(t);
                                                        if (!hasStartTime || start < e.StartTime)
                                                        {
                                                            e.StartTime = start;
                                                            hasStartTime = true;
                                                        }
                                                    }
                                                }

                                                start += duration;
                                                end += duration;
                                                first = false;
                                            }
                                        }
                                        catch (Exception exception)
                                        {
                                            MessageBox.Show(string.Format(LocalisationManager.GetString(OsuString.HitObjectManager_LoadSave_StoryboardErrorOnLine_2) + "\n\n" + exception.Message, readableFiles[fn], linenumber, line));
                                        }
                                        finally
                                        {
                                            line = reader.ReadLine();
                                            ParseVariables(ref line);
                                            linenumber++;
                                        }
                                    }

                                    if (e != null && e.Sprite != null && eventManager.spriteManagerBG.ForwardPlayOptimisations
                                        && e.Type != EventTypes.Background && e.Type != EventTypes.Video)
                                    {

                                        e.Sprite.MakeAllLoopsStatic();

                                        //We have to do the add here due to transformations needing to be populated
                                        //in order to make use of the display-order optimisations.
                                        if (e.Layer == StoryLayer.Background)
                                            eventManager.spriteManagerBG.Add(e.Sprite);
                                        else
                                            eventManager.spriteManagerFG.Add(e.Sprite);
                                    }

                                    readNew = false;
                                }
                                catch (Exception ex)
                                {
                                    throw new OsbParserException(linenumber, ex);
                                }
                                break;
                            case FileSection.HitObjects:
                                if (fn > 0)
                                    continue;

                                if (!hitObjectPreInit)
                                {
                                    ComboColoursReset();
                                    hitObjectPreInit = true;
                                }

                                // Mask out the first 4 bits for HitObjectType. This is redundant because they're masked out in the checks below, but still wise.
                                HitObjectType type = (HitObjectType)(Int32.Parse(split[3], GameBase.nfi)) & ~HitObjectType.ColourHax;
                                HitObjectSoundType soundType = (HitObjectSoundType)Int32.Parse(split[4], GameBase.nfi);
                                int x = (int)Math.Max(0, Math.Min(512, Decimal.Parse(split[0], GameBase.nfi)));
                                int y = (int)Math.Max(0, Math.Min(512, Decimal.Parse(split[1], GameBase.nfi)));
                                Vector2 pos = new Vector2(x, verticalFlip ? 384 - y : y);
                                int time = (int)Decimal.Parse(split[2], GameBase.nfi) + Beatmap.VersionOffset;

                                int combo_offset = (Convert.ToInt32(split[3], GameBase.nfi) >> 4) & 7; // mask out bits 5-7 for combo offset.
                                bool new_combo = type.IsType(HitObjectType.NewCombo);

                                SampleSet sampleSet = SampleSet.None, addSampleSet = SampleSet.None;
                                CustomSampleSet customSample = CustomSampleSet.Default;
                                string sampleFile = string.Empty;
                                int volume = 0;

                                if (type.IsType(HitObjectType.Normal))
                                {
                                    if (split.Length > 5 && split[5].Length > 0)
                                    {
                                        string[] ss = split[5].Split(':');
                                        sampleSet = (SampleSet)Convert.ToInt32(ss[0]);
                                        addSampleSet = (SampleSet)Convert.ToInt32(ss[1]);
                                        customSample = ss.Length > 2 ? (CustomSampleSet)Convert.ToInt32(ss[2]) : CustomSampleSet.Default;
                                        volume = ss.Length > 3 ? Int32.Parse(ss[3]) : 0;
                                        sampleFile = ss.Length > 4 ? ss[4] : string.Empty;
                                    }

                                    HitCircle h = hitFactory.CreateHitCircle(pos, time, forceNewCombo | new_combo, soundType, new_combo ? combo_offset : 0, sampleSet, addSampleSet, customSample, volume, sampleFile);
                                    AddCircle(h);
                                }
                                else if (type.IsType(HitObjectType.Slider))
                                {
                                    CurveTypes curveType = CurveTypes.Catmull;
                                    int repeatCount = 0;
                                    double length = 0;
                                    List<Vector2> points = new List<Vector2>();
                                    List<HitObjectSoundType> sounds = null;

                                    string[] pointsplit = split[5].Split('|');
                                    for (int i = 0; i < pointsplit.Length; i++)
                                    {
                                        if (pointsplit[i].Length == 1)
                                        {
                                            switch (pointsplit[i])
                                            {
                                                case @"C":
                                                    curveType = CurveTypes.Catmull;
                                                    break;
                                                case @"B":
                                                    curveType = CurveTypes.Bezier;
                                                    break;
                                                case @"L":
                                                    curveType = CurveTypes.Linear;
                                                    break;
                                                case @"P":
                                                    curveType = CurveTypes.PerfectCurve;
                                                    break;
                                            }
                                            continue;
                                        }

                                        string[] temp = pointsplit[i].Split(':');
                                        Vector2 v = new Vector2((int)Convert.ToDouble(temp[0], GameBase.nfi),
                                                                (int)
                                                                (verticalFlip
                                                                        ? 384 - Convert.ToDouble(temp[1], GameBase.nfi)
                                                                        : Convert.ToDouble(temp[1], GameBase.nfi)));
                                        //if (i > 1 || v != points[0]) fixed with new constructor
                                        //old maps stored the start point of a slider in this list.
                                        //newer ones don't but we should check anyway.
                                        points.Add(v);
                                    }

                                    repeatCount = Convert.ToInt32(split[6], GameBase.nfi);

                                    if (repeatCount > 9000)
                                    {
                                        throw new ArgumentOutOfRangeException(LocalisationManager.GetString(OsuString.HitObjectManager_LoadSave_ErrorTooManyRepeats));
                                    }

                                    if (split.Length > 7)
                                        length = Convert.ToDouble(split[7], GameBase.nfi);

                                    if (split.Length > 8 && split[8].Length > 0)
                                    {
                                        //Per-endpoint Sample Additions
                                        string[] adds = split[8].Split('|');
                                        if (adds.Length > 0)
                                        {
                                            sounds = new List<HitObjectSoundType>();
                                            int i;
                                            int addslength = Math.Min(adds.Length, repeatCount + 1);
                                            for (i = 0; i < addslength; i++)
                                            {
                                                int sound;
                                                Int32.TryParse(adds[i], out sound);
                                                sounds.Add((HitObjectSoundType)sound);
                                            }
                                            for (; i < repeatCount + 1; i++)
                                            {
                                                sounds.Add(soundType);
                                            }
                                        }
                                    }

                                    List<SampleSet> ss = new List<SampleSet>();
                                    List<SampleSet> ssa = new List<SampleSet>();

                                    if (split.Length > 9 && split[9].Length > 0)
                                    {
                                        string[] sets = split[9].Split('|');

                                        if (sets.Length > 0)
                                        {
                                            foreach (string t in sets)
                                            {
                                                string[] split2 = t.Split(':');
                                                ss.Add((SampleSet)Convert.ToInt32(split2[0]));
                                                ssa.Add((SampleSet)Convert.ToInt32(split2[1]));
                                            }
                                        }
                                    }

                                    if (sounds != null)
                                    {
                                        // pad the sampleset collections to have the correct length always
                                        if (ss.Count > repeatCount + 1)
                                        {
                                            ss.RemoveRange(repeatCount + 1, ss.Count - repeatCount - 1);
                                        }
                                        else
                                        {
                                            for (int z = ss.Count; z <= repeatCount; z++)
                                            {
                                                ss.Add(SampleSet.None);
                                            }
                                        }

                                        if (ssa.Count > repeatCount + 1)
                                        {
                                            ssa.RemoveRange(repeatCount + 1, ssa.Count - repeatCount - 1);
                                        }
                                        else
                                        {
                                            for (int z = ssa.Count; z <= repeatCount; z++)
                                            {
                                                ssa.Add(SampleSet.None);
                                            }
                                        }
                                    }

                                    if (split.Length > 10)
                                    {
                                        string[] split2 = split[10].Split(':');
                                        sampleSet = (SampleSet)Convert.ToInt32(split2[0]);
                                        addSampleSet = (SampleSet)Convert.ToInt32(split2[1]);
                                        customSample = split2.Length > 2 ? (CustomSampleSet)Convert.ToInt32(split2[2]) : CustomSampleSet.Default;
                                        volume = split2.Length > 3 ? Int32.Parse(split2[3]) : 0;
                                        sampleFile = split2.Length > 4 ? split2[4] : string.Empty;
                                    }

                                    Slider s = hitFactory.CreateSlider(pos, time, forceNewCombo || new_combo, soundType, curveType, repeatCount, length, points, sounds, new_combo ? combo_offset : 0, sampleSet, addSampleSet, ss, ssa, customSample, volume, sampleFile);

                                    AddSlider(s);
                                }
                                else if (type.IsType(HitObjectType.Spinner))
                                {
                                    if (split.Length > 6)
                                    {
                                        string[] ss = split[6].Split(':');
                                        sampleSet = (SampleSet)Convert.ToInt32(ss[0]);
                                        addSampleSet = (SampleSet)Convert.ToInt32(ss[1]);
                                        customSample = ss.Length > 2 ? (CustomSampleSet)Convert.ToInt32(ss[2]) : CustomSampleSet.Default;
                                        volume = ss.Length > 3 ? Int32.Parse(ss[3]) : 0;
                                        sampleFile = ss.Length > 4 ? ss[4] : string.Empty;
                                    }

                                    Spinner s = hitFactory.CreateSpinner(time, Math.Min(AudioEngine.AudioLength - 50, Convert.ToInt32(split[5], GameBase.nfi)) + Beatmap.VersionOffset, soundType, sampleSet, addSampleSet, customSample, volume, sampleFile);
                                    s.NewCombo = type.IsType(HitObjectType.NewCombo);

                                    AddSpinner(s);
                                }
                                else if (type.IsType(HitObjectType.Hold))
                                {
                                    //notice: this type of note is ONLY generated by bms converter or future editor version
                                    int end = time;
                                    if (split.Length > 5 && split[5].Length > 0)
                                    {
                                        string[] ss = split[5].Split(':');
                                        end = Convert.ToInt32(ss[0]);
                                        if (ss.Length > 1)
                                        {
                                            sampleSet = (SampleSet)Convert.ToInt32(ss[1]);
                                            addSampleSet = (SampleSet)Convert.ToInt32(ss[2]);
                                        }
                                        if (ss.Length > 3)
                                            customSample = (CustomSampleSet)Convert.ToInt32(ss[3]);
                                        volume = ss.Length > 4 ? Int32.Parse(ss[4]) : 0;
                                        sampleFile = ss.Length > 5 ? ss[5] : string.Empty;
                                    }

                                    HitCircle h = hitFactory.CreateSpecial(pos, time, end, forceNewCombo || new_combo, soundType, new_combo ? combo_offset : 0, sampleSet, addSampleSet, customSample, volume, sampleFile);
                                    if (h != null)
                                    {
                                        AddCircle(h);
                                    }
                                }
                                break;
                        }
                    }
                }

                if (!hitObjectPreInit) ComboColoursReset();

            }
        }

        private void ParseVariables(ref string line)
        {
            if (Variables == null || line.IndexOf('$') < 0) return;
            foreach (KeyValuePair<string, string> s in Variables)
                line = line.Replace(s.Key, s.Value);
        }

        internal bool Save(bool requireConfirmation, bool newFile, bool checkChangesOnly)
        {
            eventManager.Sort();

            Beatmap b = Beatmap;

            StringBuilder sb;

            b.BeatmapVersion = Beatmap.BEATMAP_VERSION;

            using (StringWriter writer = new StringWriter())
            {
                writer.WriteLine(@"osu file format v" + b.BeatmapVersion);
                writer.WriteLine();
                writer.WriteLine(@"[General]");
                writer.WriteLine(@"AudioFilename: " + Path.GetFileName(b.AudioFilename));
                writer.WriteLine(@"AudioLeadIn: " + b.AudioLeadIn);
                //writer.WriteLine("AudioHash: " + AudioEngine.AudioMd5);
                writer.WriteLine(@"PreviewTime: " + AudioEngine.PreviewTime);
                writer.WriteLine(@"Countdown: " + (int)b.Countdown);
                writer.WriteLine(@"SampleSet: " + ((int)AudioEngine.CurrentSampleSet > 2 ? SampleSet.None : AudioEngine.CurrentSampleSet));
                writer.WriteLine(@"StackLeniency: " + b.StackLeniency.ToString(GameBase.nfi));
                writer.WriteLine(@"Mode: " + (int)b.PlayMode);
                writer.WriteLine(@"LetterboxInBreaks: " + (b.LetterboxInBreaks ? @"1" : @"0"));
                if (!b.StoryFireInFront)
                    writer.WriteLine(@"StoryFireInFront: 0");
                if (b.UseSkinSpritesInSB)
                    writer.WriteLine(@"UseSkinSprites: 1");
                if (b.AlwaysShowPlayfield)
                    writer.WriteLine(@"AlwaysShowPlayfield: 1");
                if (b.OverlayPosition != OverlayPosition.NoChange)
                    writer.WriteLine(@"OverlayPosition: " + b.OverlayPosition);
                if (!string.IsNullOrEmpty(b.SkinPreference))
                    writer.WriteLine(@"SkinPreference:" + b.SkinPreference);
                if (b.EpilepsyWarning)
                    writer.WriteLine(@"EpilepsyWarning: 1");
                if (b.CountdownOffset > 0)
                    writer.WriteLine(@"CountdownOffset: " + b.CountdownOffset.ToString());
                if (b.PlayMode == PlayModes.OsuMania)
                    writer.WriteLine(@"SpecialStyle: " + (b.SpecialStyle ? @"1" : @"0"));
                writer.WriteLine(@"WidescreenStoryboard: " + (b.WidescreenStoryboard ? @"1" : @"0"));
                if (b.SamplesMatchPlaybackRate)
                    writer.WriteLine(@"SamplesMatchPlaybackRate: 1");

                writer.WriteLine();

                writer.WriteLine(@"[Editor]");

                if (Bookmarks.Count > 0)
                {
                    string bline = string.Empty;
                    foreach (int i in Bookmarks)
                        bline += i + @",";
                    bline = bline.Trim(',');
                    writer.WriteLine(@"Bookmarks: " + bline);
                }

                writer.WriteLine(@"DistanceSpacing: " + ConfigManager.sDistanceSpacing);
                writer.WriteLine(@"BeatDivisor: " + ConfigManager.sEditorBeatDivisor);
                writer.WriteLine(@"GridSize: " + ConfigManager.sEditorGridSize);
                writer.WriteLine(@"TimelineZoom: " + b.TimelineZoom.ToString(GameBase.nfi));

                writer.WriteLine();

                writer.WriteLine(@"[Metadata]");
                writer.WriteLine(@"Title:" + b.Title);
                writer.WriteLine(@"TitleUnicode:" + b.TitleUnicode);
                writer.WriteLine(@"Artist:" + b.Artist);
                writer.WriteLine(@"ArtistUnicode:" + b.ArtistUnicode);
                writer.WriteLine(@"Creator:" + b.Creator);
                writer.WriteLine(@"Version:" + b.Version);
                writer.WriteLine(@"Source:" + b.Source);
                writer.WriteLine(@"Tags:" + b.Tags);
                writer.WriteLine(@"BeatmapID:" + b.BeatmapId);
                writer.WriteLine(@"BeatmapSetID:" + b.BeatmapSetId);
                writer.WriteLine();

                writer.WriteLine(@"[Difficulty]");
                writer.WriteLine(@"HPDrainRate:" + b.DifficultyHpDrainRate.ToString(GameBase.nfi));
                writer.WriteLine(@"CircleSize:" + b.DifficultyCircleSize.ToString(GameBase.nfi));
                writer.WriteLine(@"OverallDifficulty:" + b.DifficultyOverall.ToString(GameBase.nfi));
                writer.WriteLine(@"ApproachRate:" + b.DifficultyApproachRate.ToString(GameBase.nfi));
                writer.WriteLine(@"SliderMultiplier:" +
                                 b.DifficultySliderMultiplier.ToString(GameBase.nfi));
                writer.WriteLine(@"SliderTickRate:" + b.DifficultySliderTickRate.ToString(GameBase.nfi));
                writer.WriteLine();

                WriteEvents(writer, true);

                writer.WriteLine();

                if (Beatmap.ControlPoints.Count > 0/*AudioEngine.beatSync*/)
                {
                    writer.WriteLine(@"[TimingPoints]");
                    writer.WriteLine(OutputTimingPoints(Beatmap.ControlPoints));
                }

                if (b.CustomColours)
                {
                    writer.WriteLine();
                    writer.WriteLine(@"[Colours]");
                    foreach (KeyValuePair<string, Color> s in SkinManager.BeatmapColours)
                    {
                        if (s.Value.A == 0)
                            continue;
                        writer.WriteLine("{0} : {1},{2},{3}", s.Key, s.Value.R,
                                         s.Value.G, s.Value.B);
                    }
                }

                writer.WriteLine();
                writer.WriteLine(@"[HitObjects]");

                SpinnerForceNewComboAll();
                Sort(true);

                foreach (HitObject h in hitObjects)
                {
                    if (h.StartTime > AudioEngine.AudioLength || h.StartTime < 0)
                        continue;

                    string extra = string.Empty;
                    if (h.IsType(HitObjectType.Slider))
                    {
                        extra = @",";
                        SliderOsu c = (SliderOsu)h;

                        if (c.SpatialLength == 0) continue;

                        //Add the curve type.
                        extra += c.CurveTypeSavable.ToString().Substring(0, 1) + @"|";

                        for (int i = 1; i < c.sliderCurvePoints.Count; i++)
                        {
                            Vector2 p = c.sliderCurvePoints[i];
                            extra += (int)p.X + @":" + (int)p.Y + @"|";
                        }

                        extra = extra.Trim('|');
                        extra += @"," + c.SegmentCount;
                        extra += @"," + c.SpatialLength.ToString(GameBase.nfi);

                        if (!c.unifiedSoundAddition)
                        {
                            extra += @",";
                            for (int i = 0; i < c.SoundTypeList.Count; i++)
                                extra += (int)c.SoundTypeList[i] + @"|";
                            extra = extra.Trim('|');

                            extra += @",";

                            for (int i = 0; i < c.SampleSetList.Count; i++)
                                extra += (int)c.SampleSetList[i] + @":" + (int)c.SampleSetAdditionList[i] + @"|";
                            extra = extra.Trim('|');

                            // sliders with unified sound additions can't have sampleset overrides :(
                            // this is required for graceful degradation on old builds.
                            extra += string.Format(",{0}:{1}:{2}:{3}:{4}", (int)h.SampleSet, (int)h.SampleSetAdditions, (int)h.CustomSampleSet, h.SampleVolume, h.SampleFile);
                        }
                    }
                    else if (h.IsType(HitObjectType.Spinner))
                        extra = string.Format(",{0},{1}:{2}:{3}:{4}:{5}", h.EndTime, (int)h.SampleSet, (int)h.SampleSetAdditions, (int)h.CustomSampleSet, h.SampleVolume, h.SampleFile);
                    else if (h.IsType(HitObjectType.Normal))
                        extra = string.Format(",{0}:{1}:{2}:{3}:{4}", (int)h.SampleSet, (int)h.SampleSetAdditions, (int)h.CustomSampleSet, h.SampleVolume, h.SampleFile);
                    else if (h.IsType(HitObjectType.Hold))
                        extra = string.Format(",{0}:{1}:{2}:{3}:{4}:{5}", h.EndTime, (int)h.SampleSet, (int)h.SampleSetAdditions, (int)h.CustomSampleSet, h.SampleVolume, h.SampleFile);

                    writer.WriteLine((int)h.Position.X + @"," + (int)h.Position.Y + @"," + h.StartTime + @"," + ((int)h.Type | ((h.ComboOffset & 7) << 4)).ToString() +
                                     @"," +
                                     (int)h.SoundType + extra);
                }

                sb = writer.GetStringBuilder();
            }

            string origMd5 = CryptoHelper.GetMd5(b.FilenameFull);
            string newMd5 = CryptoHelper.GetMd5String(sb.ToString());

            bool hasChanged = !origMd5.Equals(newMd5);

            if (checkChangesOnly)
                return hasChanged;

            if (requireConfirmation)
            {
                if (!origMd5.Equals(newMd5))
                {
                    if (GameBase.TestMode)
                    {
                        if (DialogResult.OK !=
                            MessageBox.Show(LocalisationManager.GetString(OsuString.HitObjectManager_LoadSave_TestModeDirtyDialog), @"osu!",
                                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation))
                            return false;
                    }
                    else
                    {
                        DialogResult r = MessageBox.Show(LocalisationManager.GetString(OsuString.HitObjectManager_LoadSave_SaveChangesDialog), @"osu!",
                                                         MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation,
                                                         MessageBoxDefaultButton.Button1);
                        switch (r)
                        {
                            case DialogResult.Cancel:
                                return false;
                            case DialogResult.No:
                                return true;
                            case DialogResult.OK:
                                break;
                        }
                    }
                }
                else
                    return true;
            }

            string testFilename = (b.Artist.Length > 0
                                       ? b.Artist + @" - " + b.Title
                                       : Path.GetFileNameWithoutExtension(b.AudioFilename))
                                  +
                                  (b.Creator.Length > 0
                                       ? @" (" + b.Creator + @")"
                                       : string.Empty) +
                                  (b.Version.Length > 0
                                       ? @" [" + b.Version + @"]"
                                       : string.Empty) + @".osu";

            testFilename = GeneralHelper.WindowsFilenameStrip(testFilename);

            if (newFile || testFilename != b.Filename)
            {
                if (File.Exists(b.ContainingFolder + @"\" + testFilename))
                {
                    DialogResult r =
                        MessageBox.Show(LocalisationManager.GetString(OsuString.HitObjectManager_LoadSave_FileAlreadyExistsDialog), @"osu!",
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation,
                                        MessageBoxDefaultButton.Button1);
                    switch (r)
                    {
                        case DialogResult.No:
                            return false;
                    }
                }

                if (!newFile)
                    GeneralHelper.FileDelete(b.FilenameFull);
                b.Filename = testFilename;
            }

            if (b.InOszContainer)
                b.RequireExtractionFolder();

            try
            {
                File.WriteAllText(b.FilenameFull, sb.ToString());
            }
            catch (Exception)
            {
                NotificationManager.ShowMessage("Unable to write to file " + b.FilenameFull);
                return false;
            }

            //Finished writing .osu


            //Start writing .osb if necessary
            if (eventManager.events.Find(e => !e.WriteToOsu) != null)
            {
                using (StringWriter writer = new StringWriter())
                {
                    if (Variables != null && Variables.Count > 0)
                    {
                        writer.WriteLine(@"[Variables]");
                        foreach (KeyValuePair<string, string> s in Variables)
                            writer.WriteLine("{0}={1}", s.Key, s.Value);
                        writer.WriteLine();
                    }

                    WriteEvents(writer, false);

                    sb = writer.GetStringBuilder();
                }

                string build = sb.ToString();

                foreach (KeyValuePair<string, string> s in Variables)
                    build = build.Replace(@"," + s.Value, @"," + s.Key);

                try
                {
                    File.WriteAllText(Path.Combine(b.InOszContainer ? b.ExtractionFolder : b.ContainingFolder, b.StoryboardFilename), build);
                }
                catch
                {
                    NotificationManager.ShowMessage("Unable to write to file " + b.StoryboardFilename);
                    return false;
                }

                //Force this to require an update after changes.
                if (Beatmap.SubmissionStatus > SubmissionStatus.NotSubmitted)
                    Beatmap.UpdateAvailable = hasChanged;
            }
            else
            {
                //Delete if the file exists (we dont need it)
                File.Delete(Path.Combine(b.InOszContainer ? b.ExtractionFolder : b.ContainingFolder, b.StoryboardFilename));
            }

            GameBase.SetTitle(Path.GetFileName(b.Filename));
            b.ResetStar();
            b.ProcessHeaders();
            AudioEngine.UpdateActiveTimingPoint(false);

            return true;
        }

        private void WriteEvents(StringWriter writer, bool osuFileFirstPass)
        {
            writer.WriteLine("[Events]");

            writer.WriteLine("//Background and Video events");
            foreach (
                Event e in eventManager.events.FindAll(ev => (ev.StoryboardIgnore && ev.WriteToOsu) == osuFileFirstPass)
                )
            {
                switch (e.Type)
                {
                    case EventTypes.Video:
                        writer.WriteLine("{0},{1},\"{2}\"", e.Type, e.StartTime,
                                         e.Filename);
                        break;
                    case EventTypes.Background:
                        writer.WriteLine("{0},{1},\"{2}\",{3},{4}", (int)e.Type, e.StartTime, GeneralHelper.PathStandardise(e.Filename), e.Sprite != null ? e.Sprite.Position.X : 0, e.Sprite != null ? e.Sprite.Position.Y : 0);
                        break;
                }
            }

            if (osuFileFirstPass)
            {
                writer.WriteLine("//Break Periods");

                foreach (EventBreak e in eventManager.eventBreaks)
                    writer.WriteLine("{0},{1},{2}", (int)e.Type, e.StartTime, e.EndTime);
            }


            for (int i = 0; i < eventManager.storyLayerSprites.Length; i++)
            {
                writer.WriteLine("//Storyboard Layer {0} ({1})", i, ((StoryLayer)i));

                foreach (Event e in eventManager.storyLayerSprites[i].FindAll(ev => ev.WriteToOsu == osuFileFirstPass))
                {
                    switch (e.Type)
                    {
                        case EventTypes.Animation:
                        case EventTypes.Sprite:
                            e.Sprite.Transformations.Sort();

                            EventAnimation animation = e as EventAnimation;

                            if (e.Type == EventTypes.Sprite)
                                writer.WriteLine("{0},{1},{2},\"{3}\",{4},{5}", e.Type, e.Layer,
                                                 e.Sprite.Origin,
                                                 e.Filename, (int)e.Sprite.InitialPosition.X,
                                                 (int)e.Sprite.InitialPosition.Y);
                            else
                                writer.WriteLine("{0},{1},{2},\"{3}\",{4},{5},{6},{7},{8}", e.Type, e.Layer,
                                                 e.Sprite.Origin,
                                                 e.Filename, (int)e.Sprite.InitialPosition.X,
                                                 (int)e.Sprite.InitialPosition.Y,
                                                 animation.Animation.TextureCount,
                                                 animation.FrameDelay,
                                                 animation.Animation.LoopType);
                            if (e.Sprite.Loops != null)
                                foreach (TransformationLoop l in e.Sprite.Loops)
                                {
                                    writer.WriteLine(" L,{0},{1}", l.StartTime, l.LoopCount);
                                    l.Transformations.Sort();
                                    foreach (Transformation t2 in l.Transformations)
                                    {
                                        if (t2.Duration == 0)
                                            writer.Write("  {0},{1},{2},,", getTypeAcronym(t2.Type), (int)t2.Easing,
                                                         t2.Time1);
                                        else
                                            writer.Write("  {0},{1},{2},{3},", getTypeAcronym(t2.Type), (int)t2.Easing,
                                                         t2.Time1, t2.Time2);

                                        ProcessTransformationType(writer, t2);

                                        writer.WriteLine();
                                    }
                                }
                            foreach (Transformation t in e.Sprite.Transformations)
                            {
                                if (t.IsLoopStatic)
                                    continue;

                                if (t.Duration == 0)
                                    writer.Write(" {0},{1},{2},,", getTypeAcronym(t.Type), (int)t.Easing, t.Time1);
                                else
                                    writer.Write(" {0},{1},{2},{3},", getTypeAcronym(t.Type), (int)t.Easing, t.Time1,
                                                 t.Time2);

                                ProcessTransformationType(writer, t);

                                writer.WriteLine();
                            }
                            if (e.EventLoopTriggers.Count > 0)
                            {
                                foreach (KeyValuePair<TriggerGroup, List<TriggerLoop>> s in e.EventLoopTriggers)
                                {
                                    foreach (TriggerLoop l in s.Value)
                                    {
                                        if (l.TriggerEndTime == 0)
                                            writer.Write(" T,{0}", l.Trigger);
                                        else
                                            writer.Write(" T,{0},{1},{2}", l.Trigger, l.TriggerStartTime,
                                                             l.TriggerEndTime);
                                        if (l.Trigger.HasSpecificTriggerGroup)
                                            writer.WriteLine(",{0}", -(int)l.Trigger.TriggerGroup);
                                        else
                                            writer.WriteLine();

                                        l.Transformations.Sort();
                                        foreach (Transformation t2 in l.Transformations)
                                        {
                                            if (t2.Duration == 0)
                                                writer.Write("  {0},{1},{2},,", getTypeAcronym(t2.Type), (int)t2.Easing,
                                                             t2.Time1);
                                            else
                                                writer.Write("  {0},{1},{2},{3},", getTypeAcronym(t2.Type),
                                                             (int)t2.Easing, t2.Time1, t2.Time2);

                                            ProcessTransformationType(writer, t2);

                                            writer.WriteLine();
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
            }

            writer.WriteLine("//Storyboard Sound Samples");

            foreach (EventSample e in eventManager.eventSamples.FindAll(ev => ev.WriteToOsu == osuFileFirstPass))
                writer.WriteLine("{0},{1},{2},\"{3}\",{4}", e.Type, e.StartTime, (int)e.Layer, GeneralHelper.PathStandardise(e.Filename), e.Volume);

            // Remove line numbers since they don't match with the file anymore
            eventManager.events.ForEach(e => e.LineNumber = 0);
        }

        internal static string OutputTimingPoints(List<ControlPoint> timing_points)
        {
            StringBuilder builder = new StringBuilder();
            foreach (ControlPoint t in timing_points)
                if (t.beatLength != 0)
                {
                    builder.AppendFormat(GameBase.nfi, "{0},{1},{2},{3},{4},{5},{6},{7}\r\n", t.offset,
                                     t.beatLength, (int)t.timeSignature, (int)t.sampleSet,
                                     (int)t.customSamples, t.volume, (t.timingChange ? @"1" : @"0"),
                                     (int)t.effectFlags);
                }
            return builder.ToString();
        }

        internal static List<ControlPoint> ParseTimingPoints(Beatmap beatmap, string src, out bool success, bool just_validate)
        {
            List<ControlPoint> result = just_validate ? null : new List<ControlPoint>();

            if (src.Contains("NaN") || src.Contains("Infinity"))
            {
                success = false;
                return null;
            }

            string[] split = src.Split('\n');
            success = true;
            foreach (string s in split)
            {
                if (s.Trim().Length == 0)
                    continue;
                ControlPoint tp_parsed = ParseTimingPoint(s, beatmap.VersionOffset, beatmap.SampleVolume, beatmap.DefaultSampleSet);
                if (tp_parsed != null)
                {
                    if (!just_validate)
                        result.Add(tp_parsed);
                }
                else
                {
                    success = false;
                    return null;
                }
            }
            return result;
        }

        internal static ControlPoint ParseTimingPoint(string src, int VersionOffset, int SampleVolume, SampleSet DefaultSampleSet)
        {
            try
            {
                string[] split = src.Split(',');
                if (split.Length > 2)
                {
                    int kiai_flags = split.Length > 7 ? Convert.ToInt32(split[7], GameBase.nfi) : 0;
                    return new ControlPoint(Double.Parse(split[0].Trim(), GameBase.nfi) + VersionOffset,
                                     Double.Parse(split[1].Trim(), GameBase.nfi),
                                     split[2][0] == '0' ? TimeSignatures.SimpleQuadruple :
                                     (TimeSignatures)Int32.Parse(split[2]),
                                     (SampleSet)Int32.Parse(split[3]),
                                     split.Length > 4
                                         ? (CustomSampleSet)Int32.Parse(split[4])
                                         : CustomSampleSet.Default,
                                     split.Length > 5 ? Int32.Parse(split[5]) : SampleVolume,
                                     split.Length > 6 ? split[6][0] == '1' : true,
                                     (EffectFlags)kiai_flags);
                }
                else if (split.Length == 2)
                    return new ControlPoint(Double.Parse(split[0].Trim(), GameBase.nfi) + VersionOffset,
                                     Double.Parse(split[1].Trim(), GameBase.nfi),
                                     TimeSignatures.SimpleQuadruple,
                                     DefaultSampleSet,
                                     AudioEngine.CustomSamples,
                                     100, true, EffectFlags.None);
                else
                    return null;
            }
            catch (FormatException)
            {
                return null;
            }
        }

        private static string getTypeAcronym(TransformationType type)
        {
            switch (type)
            {
                default:
                    return type.ToString().Substring(0, 1);
                case TransformationType.MovementX:
                    return @"MX";
                case TransformationType.MovementY:
                    return @"MY";
            }
        }

        private void ProcessTransformationType(StringWriter writer, Transformation t)
        {
            switch (t.Type)
            {
                case TransformationType.Movement:
                    if (t.StartVector == t.EndVector)
                        writer.Write("{0},{1}", (int)t.StartVector.X, (int)t.StartVector.Y);
                    else
                        writer.Write("{0},{1},{2},{3}", (int)t.StartVector.X, (int)t.StartVector.Y,
                                     (int)t.EndVector.X, (int)t.EndVector.Y);
                    break;
                case TransformationType.VectorScale:
                    if (t.StartVector == t.EndVector)
                        writer.Write("{0},{1}", t.StartVector.X.ToString(GameBase.nfi), t.StartVector.Y.ToString(GameBase.nfi));
                    else
                        writer.Write("{0},{1},{2},{3}", t.StartVector.X.ToString(GameBase.nfi), t.StartVector.Y.ToString(GameBase.nfi),
                                     t.EndVector.X.ToString(GameBase.nfi), t.EndVector.Y.ToString(GameBase.nfi));
                    break;
                case TransformationType.Fade:
                case TransformationType.Rotation:
                case TransformationType.Scale:
                case TransformationType.MovementX:
                case TransformationType.MovementY:
                    if (t.StartFloat == t.EndFloat)
                        writer.Write("{0}", t.StartFloat.ToString(GameBase.nfi));
                    else
                        writer.Write("{0},{1}", t.StartFloat.ToString(GameBase.nfi), t.EndFloat.ToString(GameBase.nfi));
                    break;
                case TransformationType.Colour:
                    if (t.StartColour == t.EndColour)
                        writer.Write("{0},{1},{2}", t.StartColour.R, t.StartColour.G,
                                     t.StartColour.B);
                    else
                        writer.Write("{0},{1},{2},{3},{4},{5}", t.StartColour.R, t.StartColour.G,
                                     t.StartColour.B, t.EndColour.R, t.EndColour.G, t.EndColour.B);
                    break;
                case TransformationType.ParameterFlipHorizontal:
                    writer.Write(@"H");
                    break;
                case TransformationType.ParameterFlipVertical:
                    writer.Write(@"V");
                    break;
                case TransformationType.ParameterAdditive:
                    writer.Write(@"A");
                    break;
            }
        }

        public bool forceNewCombo
        {
            get
            {
                return lastAddedObject == null || lastAddedObject is Spinner || lastAddedObject is HitCircleFruitsSpin;
            }
        }
    }

    internal class OsuFileUnreadableException : Exception
    {
    }

    internal class OsbParserException : Exception
    {
        public OsbParserException(int line_number, Exception original_exception)
            : base()
        {
            LineNumber = line_number;
            OriginalException = original_exception;
        }

        internal readonly int LineNumber;
        internal readonly Exception OriginalException;
    }
}
