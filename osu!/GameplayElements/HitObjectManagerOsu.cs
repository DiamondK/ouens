﻿using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Fruits;
using osu.Graphics.Skinning;
using osu.GameplayElements.HitObjects.Osu;
using osu.Audio;
using System;
using Un4seen.Bass;
using osu.Graphics.Sprites;

namespace osu.GameplayElements
{
    internal class HitObjectManagerOsu : HitObjectManager
    {
        internal HitObjectManagerOsu(bool withSpriteManager = true)
            : base(withSpriteManager)
        {
        }

        internal override HitFactory CreateHitFactory()
        {
            return new HitFactoryOsu(this);
        }

        internal override void UpdateHitObjects()
        {
            base.UpdateHitObjects();

            if (!Player.IsSliding && AudioEngine.SlideSamplesPlaying)
                AudioEngine.StopSlideSamples();
        }

        protected override void UpdateHitObject(HitObject h)
        {
            base.UpdateHitObject(h);
        }
    }
}