﻿using System;
using System.Collections.Generic;
using osu.Audio;
using osu.GameplayElements.HitObjects;
using osu.Graphics.Sprites;
using osu.GameplayElements.Events.Trigger;

namespace osu.GameplayElements.Events
{
    internal enum EventTypes
    {
        Background = 0,
        Video = 1,
        Break = 2,
        Colour = 3,
        Sprite = 4,
        Sample = 5,
        Animation = 6
    }

    internal enum TriggerGroup
    {
        // Negative values are used for user defined groups
        None = 0,
        HitSound = 1,
        Passing = 2,
        Failing = 3,
        HitObjectHit = 4
    }

    internal enum StoryLayer
    {
        Background = 0,
        Fail = 1,
        Pass = 2,
        Foreground = 3
    }

    internal abstract class Event : IComparable<Event>
    {
        internal int EndTime;

        internal Dictionary<TriggerGroup, List<TriggerLoop>> EventLoopTriggers =
            new Dictionary<TriggerGroup, List<TriggerLoop>>();

        internal String Filename;
        internal int LineNumber;
        internal StoryLayer Layer;
        internal bool Loaded;
        internal pSprite Sprite;
        internal int StartTime;
        public bool StoryboardIgnore;
        internal EventTypes Type;
        internal bool WriteToOsu;

        internal virtual int Length
        {
            get { return EndTime - StartTime; }
        }

        internal abstract void SetStartTime(int time);
        internal abstract void SetEndTime(int time);

        internal abstract bool Load();

        internal abstract Event Clone();

        internal void BindEvent(TriggerLoop loop)
        {
            EventTrigger eventTrigger = loop.Trigger;

            List<TriggerLoop> list;
            if (!EventLoopTriggers.TryGetValue(eventTrigger.TriggerGroup, out list))
            {
                list = new List<TriggerLoop>();
                EventLoopTriggers.Add(eventTrigger.TriggerGroup, list);
            }

            list.Add(loop);

            //Trigger loops are a special case where the spriteManager isn't smart enough to know
            //where to insert them (they might trigger earlier than any existing transformations)
            Sprite.AlwaysDraw = true;
            Sprite.Alpha = 0;

            eventTrigger.Bind(this, loop, Sprite);
        }

        internal void RemoveAllLoops(TriggerGroup triggerGroup)
        {
            List<TriggerLoop> list;
            if (EventLoopTriggers.TryGetValue(triggerGroup, out list))
                foreach (TriggerLoop l in list)
                    RemoveLoop(l);
        }

        internal void RemoveLoop(TriggerLoop loop)
        {
            List<Transformation> last = loop.GetLastTransformations();
            //remove any transformations from the previous trigger first...
            if (last != null)
                foreach (Transformation t in last)
                    Sprite.Transformations.Remove(t);
        }

        internal void BindAllEvents()
        {
            if (EventLoopTriggers.Count <= 0) return;

            Dictionary<TriggerGroup, List<TriggerLoop>> copy = EventLoopTriggers;
            EventLoopTriggers = new Dictionary<TriggerGroup, List<TriggerLoop>>();

            foreach (List<TriggerLoop> l in copy.Values)
                l.ForEach(BindEvent);
        }

        #region IComparable<Event> Members

        public int CompareTo(Event other)
        {
            int compare = StartTime.CompareTo(other.StartTime);
            if (compare != 0) return compare;

            return Type.CompareTo(other.Type);
        }

        #endregion
    }
}