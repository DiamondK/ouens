using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.Graphics;
using osu.Graphics.Notifications;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common.Helpers;
using System.Diagnostics;

namespace osu.GameplayElements.Events
{
    internal class EventManager : IDisposable
    {
        #region General

        internal static EventManager Instance;

        internal static Color backgroundColour;
        internal bool backgroundUsedInStoryboard;
        internal Event backgroundEvent;
        internal static EventBreak BreakCurrent;
        internal static bool BreakMode;

        private static bool showStoryboard = true;
        internal static bool ShowStoryboard
        {
            get { return showStoryboard; }
            set
            {
                if (value == showStoryboard) return;

                showStoryboard = value;

                if (Instance != null)
                    Instance.UpdateBackground();
            }
        }

        private static bool showVideo = true;
        internal static bool ShowVideo
        {
            get { return showVideo; }
            set
            {
                if (value == showVideo) return;

                showVideo = value;

                if (Instance != null)
                    foreach (pSprite p in Instance.videoSprites)
                        p.Bypass = !showVideo;
            }
        }

        private static float spriteDrawDepth;

        private float BackgroundDrawDepth;

        private readonly pSprite breakBottom;
        private readonly pSprite breakTop;
        internal List<EventBreak> eventBreaks;
        internal List<Event> events;
        internal List<EventSample> eventSamples;
        private bool firstRun = true;
        private bool passingCurrent = false;
        private bool beforeGameplay = true;
        private bool afterGameplay = false;
        private bool backgroundIsReady = false;

        internal SpriteManager spriteManagerBG;
        internal SpriteManager spriteManagerBGWide;
        internal SpriteManager spriteManagerFG;
        internal SpriteManager spriteManagerMasking = new SpriteManager(true) { Alpha = 0 };
        internal List<Event>[] storyLayerSprites;
        internal List<Event>[] storyLayerSamples;
        internal bool[] storyLayerEnabled;
        internal bool VideoPlaying;
        internal bool WidescreenStoryboard
        {
            get { return spriteManagerFG.IsWidescreen; }
            set
            {
                spriteManagerFG.SetWidescreen(value, true);
                spriteManagerBG.SetWidescreen(value, true);

                spriteManagerFG.Masking = !value;
                spriteManagerBG.Masking = !value;
            }
        }

        internal List<pVideo> videoSprites = new List<pVideo>();
        public int lastEventTime;
        public int firstEventTime;
        private readonly HitObjectManager hitObjectManager;

        private float VideoEfficiency = 1;

        internal EventManager(HitObjectManager hitObjectManager)
        {
            Instance = this;

            this.hitObjectManager = hitObjectManager;

            backgroundColour = new Color(163, 162, 255);

            breakTop = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft,
                            Clocks.Game, new Vector2(0, 0), 0.9f, true, Color.TransparentBlack);
            breakTop.ScaleToWindowRatio = false;
            breakTop.VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight * 0.125F);

            breakBottom = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft,
                            Clocks.Game, new Vector2(0, (float)(GameBase.WindowDefaultHeight * 0.875)), 0.9f, true,
                            Color.TransparentBlack);
            breakBottom.ScaleToWindowRatio = false;
            breakBottom.VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight * 0.125F);

            spriteManagerMasking.Add(new pSprite(SkinManager.Load("masking-border", SkinSource.Osu | SkinSource.Skin), Fields.TopCentre, Origins.TopRight, Clocks.Game, new Vector2(-319.5f, 0))
            {
                AlwaysDraw = true,
                ExactCoordinates = true,
            });

            spriteManagerMasking.Add(new pSprite(SkinManager.Load("masking-border", SkinSource.Osu | SkinSource.Skin), Fields.TopCentre, Origins.TopLeft, Clocks.Game, new Vector2(319.5f, 0))
            {
                FlipHorizontal = true,
                AlwaysDraw = true,
                ExactCoordinates = true,
            });

            float x = 1 + Math.Max(0, GameBase.WindowWidthScaled * 1.6f / 2 - 172 - 512) / 170f;
            spriteManagerMasking.SpriteList.ForEach(s =>
            {
                pSprite sp = s as pSprite;
                if (sp != null) s.VectorScale = new Vector2(s.VectorScale.X < 0 ? -x : x, 768f / sp.Height);
            });

            Reset();
        }

        private void Reset()
        {
            spriteDrawDepth = 0;
            BackgroundDrawDepth = 0.4F;

            dynamicLoadIndex = 0;

            float oldBlackness = spriteManagerBG != null ? spriteManagerBG.Blackness : 0;

            spriteManagerBGWide = new SpriteManager(true);
            spriteManagerBG = new SpriteManager() { Masking = true, Alpha = 0 };
            spriteManagerFG = new SpriteManager() { Masking = true, Alpha = 0 };
            spriteManagerMasking.Alpha = 0;

            spriteManagerBG.Blackness = oldBlackness;
            spriteManagerBGWide.Blackness = oldBlackness;
            spriteManagerFG.Blackness = oldBlackness;
            currentDim = (int)(oldBlackness * 100);

            if (GameBase.Mode == OsuModes.Play)
            {
                spriteManagerBG.ForwardPlayOptimisations = true;
                spriteManagerBGWide.ForwardPlayOptimisations = true;
                spriteManagerFG.ForwardPlayOptimisations = true;
            }

            int layers = Enum.GetValues(typeof(StoryLayer)).Length;

            storyLayerSprites = new List<Event>[layers];
            storyLayerSamples = new List<Event>[layers];
            storyLayerEnabled = new bool[layers];

            for (int i = 0; i < storyLayerSprites.Length; i++)
            {
                storyLayerSprites[i] = new List<Event>();
                storyLayerSamples[i] = new List<Event>();
                storyLayerEnabled[i] = true;
            }

            videoSprites.Clear();
            eventSamples = new List<EventSample>();
            eventBreaks = new List<EventBreak>();

            //New events list with only non-storyboarded content (bg/video).

            if (events != null)
            {
                events = events.FindAll(e => e.StoryboardIgnore);
                foreach (Event e in events)
                {
                    if (e.Sprite != null)
                    {
                        spriteManagerBGWide.Add(e.Sprite);
                        EventVideo ev = e as EventVideo;
                        if (ev != null && ev.Video != null) videoSprites.Add(ev.Video);
                    }
                }
            }
            else
            {
                events = new List<Event>();
            }

            spriteManagerBGWide.Add(breakTop);
            spriteManagerBGWide.Add(breakBottom);

            WidescreenStoryboard = BeatmapManager.Current.WidescreenStoryboard;

            OnHitSound = null;
            OnHitObjectHit = null;
            OnPassing = null;
            OnFailing = null;
        }

        public List<Event> visibleEvents
        {
            get
            {
                List<Event> events = new List<Event>();
                for (int i = 0; i < storyLayerEnabled.Length; i++)
                {
                    if (!storyLayerEnabled[i]) continue;
                    events.AddRange(storyLayerSprites[i].FindAll(e => e.Sprite != null));
                }
                return events;
            }
        }

        internal void ChangeColour(Color col)
        {
            backgroundColour = col;
        }

        internal void Add(Event e)
        {
            if (e.EndTime > lastEventTime)
                lastEventTime = e.EndTime;
            if (e.StartTime < firstEventTime)
                firstEventTime = e.StartTime;

            events.Add(e);

            if (e.Sprite != null && storyLayerEnabled[(int)e.Layer])
            {
                HasStoryboard = true;

                if (e.Layer == StoryLayer.Background)
                {
                    if (backgroundEvent != null && e.Filename == backgroundEvent.Filename)
                    {
                        //The background has been used in the storyboard.
                        //Remove the static background to optimise performance.
                        backgroundUsedInStoryboard = true;
                    }

                    //We don't add these here because they don't have transformations yet.
                    //If they don't have transformations, the time comparer fails.
                    //We add them AFTER transformations have been populated.
                    if (!spriteManagerBG.ForwardPlayOptimisations)
                        spriteManagerBG.Add(e.Sprite);
                }
                else
                    if (!spriteManagerFG.ForwardPlayOptimisations)
                        spriteManagerFG.Add(e.Sprite);
            }

            if (e.Layer == StoryLayer.Fail || e.Layer == StoryLayer.Pass)
                HasPassFailLayers = true;

            switch (e.Type)
            {
                case EventTypes.Break:
                    eventBreaks.Add((EventBreak)e);
                    eventBreaks.Sort();
                    break;
                case EventTypes.Video:
                    videoSprites.Add((pVideo)e.Sprite);
                    break;
                case EventTypes.Sample:
                    storyLayerSamples[(int)e.Layer].Add(e);

                    EventSample es = (EventSample)e;

                    AudioEngine.SampleEvents.Add(es.SampleCache);
                    eventSamples.Add(es);
                    eventSamples.Sort();
                    break;
                case EventTypes.Sprite:
                case EventTypes.Animation:
                    storyLayerSprites[(int)e.Layer].Add(e);
                    break;
            }
        }

        /// <summary>
        /// Adds the specified e.  Note this is only for sprites at the moment
        /// </summary>
        /// <param name="e">The e.</param>
        /// <param name="layer">The layer.</param>
        /// <param name="insertPos">The insert pos.</param>
        public void Add(Event e, StoryLayer layer, int insertPos)
        {
            if (e.EndTime > lastEventTime)
                lastEventTime = e.EndTime;
            if (e.StartTime < firstEventTime)
                firstEventTime = e.StartTime;

            e.Layer = layer;

            int intLayer = (int)layer;

            events.Add(e);

            if (insertPos > storyLayerSprites[intLayer].Count - 1)
                storyLayerSprites[intLayer].Add(e);
            else
                storyLayerSprites[intLayer].Insert(insertPos, e);
            int count = storyLayerSprites[intLayer].Count;

            for (int i = insertPos; i < count - 1; i++)
                storyLayerSprites[intLayer][i].Sprite.Depth = storyLayerSprites[intLayer][i + 1].Sprite.Depth;
            storyLayerSprites[intLayer][count - 1].Sprite.Depth = GetIncreasingDrawDepth(e.Layer);

            if (e.Sprite != null)
            {
                if (e.Layer == StoryLayer.Background)
                {
                    spriteManagerBG.Add(e.Sprite);
                    spriteManagerBG.ResortDepth();
                }
                else
                {
                    spriteManagerFG.Add(e.Sprite);
                    spriteManagerFG.ResortDepth();
                }
            }
        }

        internal Event Add(string filename, int time)
        {
            BackgroundDrawDepth += 0.001F;

#if !DEBUGz
            try
#endif
            {
                string extension = Path.GetExtension(filename).ToLower();

                Event e;
                pSprite videoSprite = null;

                switch (extension)
                {
                    case ".png":
                    case ".jpg":
                    case ".jpeg":
                        pTexture tex = SkinManager.Load(filename, SkinSource.Beatmap);
                        pSprite spr = null;

                        if (tex == null && GameBase.Mode == OsuModes.Play && !GameBase.TestMode)
                            BeatmapManager.Current.BackgroundVisible = false;
                        else
                            spr = new pSprite(tex, Fields.StoryboardCentre, Origins.Centre, Clocks.Audio, Vector2.Zero);

                        e = new EventSprite(spr, filename, -100000, 1000 * AudioEngine.AudioLength);

                        if (e.Sprite == null)
                        {
                            NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.OsuIsAngry), 5000);
                        }
                        else
                        {
                            e.Sprite.AlwaysDraw = true;
                            //e.Sprite.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, -100000, -100000));
                            //e.Sprite.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, 1000 * AudioEngine.AudioLength, 1000 * AudioEngine.AudioLength));
                            e.Sprite.Depth = BackgroundDrawDepth;
                        }

                        e.Type = EventTypes.Background;
                        e.WriteToOsu = true;
                        backgroundEvent = e;
                        break;
                    case ".avi":
                    case ".mpg":
                    case ".flv":
                    case ".wmv":
                        if (!BeatmapManager.Current.CheckFileExists(filename))
                            return null;

                        if (ShowVideo && ((GameBase.Mode != OsuModes.Edit && !GameBase.TestMode) || ConfigManager.sEditorVideo))
                        {
                            try
                            {
                                videoSprite = new pVideo(filename, time);
                                videoSprite.Scale = (float)GameBase.WindowDefaultWidth / videoSprite.DrawWidth;

                                videoSprites.Add((pVideo)videoSprite);
                            }
                            catch (Exception)
                            {
                                NotificationManager.ShowMessage("Video playback failed.  This could be due to missing dll files (try running the updater).");
                            }
                        }

                        e = new EventVideo(videoSprite as pVideo, filename, time);

                        if (videoSprite != null)
                        {
                            videoSprite.Depth = BackgroundDrawDepth;
                            videoSprite.Transformations.Add(
                                new Transformation(TransformationType.Fade, 0, 1, time, time + 1000));
                            videoSprite.Transformations.Add(
                                new Transformation(TransformationType.Fade, 1, 0, ((pVideo)videoSprite).EndTime - 1000,
                                                   ((pVideo)videoSprite).EndTime));

                            spriteManagerBGWide.AddNonOptimised(videoSprite);
                            e.Sprite.Depth = BackgroundDrawDepth;
                        }
                        e.WriteToOsu = true;
                        events.Add(e);

                        return e;
                    default:
                        throw new Exception("moo");
                }

                events.Add(e);

                e.StoryboardIgnore = true;

                if (e.Sprite != null)
                    spriteManagerBGWide.AddNonOptimised(e.Sprite);

                return e;
            }
#if !DEBUGz
            catch (Exception)
            {
                NotificationManager.ShowMessage("An error occurred while adding this file.");
            }
#endif

            return null;
        }

        internal void Remove(Event e)
        {
            if (!events.Contains(e))
                return;

            events.Remove(e);

            switch (e.Type)
            {
                case EventTypes.Sprite:
                case EventTypes.Animation:
                case EventTypes.Background:
                    if (e.Layer == StoryLayer.Background)
                    {
                        spriteManagerBG.Remove(e.Sprite);
                        spriteManagerBGWide.Remove(e.Sprite);
                    }
                    else
                        spriteManagerFG.Remove(e.Sprite);

                    storyLayerSprites[(int)e.Layer].Remove(e);
                    break;
                case EventTypes.Break:
                    eventBreaks.Remove((EventBreak)e);
                    break;
                case EventTypes.Video:
                    videoSprites.Remove((pVideo)e.Sprite);
                    break;
                case EventTypes.Sample:
                    EventSample _e = (EventSample)e;
                    AudioEngine.SampleEvents.Remove(_e.SampleCache);
                    eventSamples.Remove(_e);
                    break;
            }
        }

        internal void DrawBG()
        {
            spriteManagerBGWide.Draw();
            spriteManagerBG.Draw();
        }

        internal void DrawFG()
        {
            spriteManagerFG.Draw();
            if (HasStoryboard && showStoryboard) spriteManagerMasking.Draw();
        }

        public void Dispose()
        {
            foreach (pVideo v in videoSprites)
                v.Dispose();
            videoSprites.Clear();

            if (spriteManagerBGWide != null) spriteManagerBGWide.Dispose();
            if (spriteManagerBG != null) spriteManagerBG.Dispose();
            if (spriteManagerFG != null) spriteManagerFG.Dispose();
            if (spriteManagerMasking != null) spriteManagerMasking.Dispose();

            OnHitSound = null;
            OnHitObjectHit = null;
            OnPassing = null;
            OnFailing = null;

            Instance = null;
        }

        internal void Update()
        {
            float targetAlpha = backgroundIsReady && showStoryboard ? 1 : 0;
            float maskingTargetAlpha = WidescreenStoryboard ? 0 : targetAlpha;

            if (spriteManagerFG.Alpha != targetAlpha)
                spriteManagerBG.Alpha = spriteManagerFG.Alpha = MathHelper.Clamp(spriteManagerFG.Alpha + ((targetAlpha < spriteManagerFG.Alpha ? -0.07f : 0.07f) * (float)GameBase.FrameRatio), 0, 1);

            if (spriteManagerMasking.Alpha != maskingTargetAlpha)
                spriteManagerMasking.Alpha = MathHelper.Clamp(spriteManagerMasking.Alpha + ((maskingTargetAlpha < spriteManagerMasking.Alpha ? -0.07f : 0.07f) * (float)GameBase.FrameRatio), 0, 1);

            bool breakMode = false;
            if (hitObjectManager.hitObjectsCount > 0)
            {
                if (beforeGameplay && AudioEngine.Time > hitObjectManager.EarliestHitObject.HittableStartTime - hitObjectManager.PreEmpt)
                    beforeGameplay = false;
                else if (!afterGameplay && AudioEngine.Time > hitObjectManager.LatestHitObject.HittableEndTime + hitObjectManager.HitWindow50)
                    afterGameplay = true;
            }
            else
            {
                beforeGameplay = true;
                afterGameplay = false;
            }

            if (GameBase.Mode == OsuModes.Play && (beforeGameplay || afterGameplay))
                breakMode = true;

            for (int i = 0; i < eventBreaks.Count; i++)
            {
                EventBreak e = eventBreaks[i];

                if (AudioEngine.Time < e.StartTime || AudioEngine.Time > e.EndTime) continue;

                breakMode = true;
                BreakCurrent = e;

                break;
            }

            if (GameBase.Mode == OsuModes.Play && Player.Passing != passingCurrent)
            {
                passingCurrent = Player.Passing;

                if (passingCurrent)
                    InvokeOnPassing();
                else
                    InvokeOnFailing();

                //Sprites
                for (int i = 0; i < storyLayerSprites[(int)StoryLayer.Fail].Count; i++)
                {
                    Event e = storyLayerSprites[(int)StoryLayer.Fail][i];

                    e.Sprite.Bypass = passingCurrent;
                }

                //Samples
                for (int i = 0; i < storyLayerSamples[(int)StoryLayer.Fail].Count; i++)
                {
                    Event e = storyLayerSamples[(int)StoryLayer.Fail][i];

                    if (passingCurrent)
                        AudioEngine.SampleEvents.Remove(((EventSample)e).SampleCache);
                    else
                        AudioEngine.SampleEvents.Add(((EventSample)e).SampleCache);
                }

                if (!firstRun)
                {
                    //Sprites
                    for (int i = 0; i < storyLayerSprites[(int)StoryLayer.Pass].Count; i++)
                    {
                        Event e = storyLayerSprites[(int)StoryLayer.Pass][i];
                        e.Sprite.Bypass = !passingCurrent;
                    }

                    //Samples
                    for (int i = 0; i < storyLayerSamples[(int)StoryLayer.Pass].Count; i++)
                    {
                        Event e = storyLayerSamples[(int)StoryLayer.Pass][i];

                        if (passingCurrent)
                            AudioEngine.SampleEvents.Add(((EventSample)e).SampleCache);
                        else
                            AudioEngine.SampleEvents.Remove(((EventSample)e).SampleCache);
                    }
                }
                firstRun = false;
            }

            if (GameBase.Mode == OsuModes.Play && breakMode != BreakMode)
            {
                //toggles go here. are only run on switching mode.
                if (!breakMode)
                {
                    NotificationManager.ClearMessageMassive();
                    //Clear any displaying messages when gameplay starts.
                }
            }

            BreakMode = breakMode;

            if (!breakMode)
                BreakCurrent = null;

            UpdateVideo();

            UpdateDimming();

            if (GameBase.SixtyFramesPerSecondFrame && showStoryboard)
                LoadDynamic(false, true);
        }

        private void UpdateVideo()
        {
            VideoPlaying = false;

            if (showVideo)
            {
                //Handle video playback.
                foreach (pVideo v in videoSprites)
                {
                    VideoEfficiency = 1 - v.Alpha;

                    v.NextFrame();

                    if (v.video != null)
                    {
                        double variance = Math.Abs(v.video.LastSample - (AudioEngine.TimeUnedited - v.StartTime));

                        if (variance > 250 * AudioEngine.CurrentPlaybackRate / 100f)
                        {
                            v.SeekToCurrent(AllowVideoSeek);
                            AllowVideoSeek = false;
                        }
                    }
                }
            }
            else
                VideoEfficiency = 1;
        }

        internal void Flash(float intensity = 1)
        {
            if (DimMaximum == 100) return;
            currentDim -= (float)Math.Ceiling((currentDim - DimMinimum) * Math.Min(1, intensity));
        }

        internal static int UserDimLevel = ConfigManager.sDimLevel;

        private static int DimMinimum { get { return Math.Max(0, UserDimLevel - 30); } }
        private static int DimMaximum { get { return Math.Max(DimMinimum, Math.Min(100, UserDimLevel)); } }

        static float currentDim = 0;

        const float adjust_rate = 15;

        private void UpdateDimming()
        {
            if (!GameBase.SixtyFramesPerSecondFrame)
                return;

            if (currentDim > DimMaximum + float.Epsilon)
                currentDim -= (currentDim - DimMaximum) / adjust_rate;
            else if (!BreakMode && currentDim < DimMaximum)
            {
                currentDim += (DimMaximum - currentDim) / adjust_rate;

                if (GameBase.Mode == OsuModes.Play && breakTop.TagNumeric == 1)
                {
                    if (BeatmapManager.Current.LetterboxInBreaks)
                    {
                        breakTop.FadeOut(200);
                        breakBottom.FadeOut(200);
                    }
                }

                breakTop.TagNumeric = 0;
            }
            else if (BreakMode && (currentDim != DimMinimum || breakTop.TagNumeric == 0))
            {
                if (currentDim > DimMinimum)
                    currentDim -= (currentDim - DimMinimum) / adjust_rate;
                else
                    currentDim += (DimMinimum - currentDim) / adjust_rate;

                if (GameBase.Mode == OsuModes.Play && breakTop.TagNumeric == 0 && !afterGameplay && !beforeGameplay)
                {
                    if (BeatmapManager.Current.LetterboxInBreaks)
                    {
                        breakTop.FadeIn(200);
                        breakBottom.FadeIn(200);
                    }
                }

                breakTop.TagNumeric = 1;
            }

            float blackness = currentDim / 100f;

            spriteManagerBGWide.Blackness = blackness;
            spriteManagerBG.Blackness = blackness;
            spriteManagerFG.Blackness = blackness;
            spriteManagerMasking.Blackness = blackness;

            GameBase.TransitionManager.BackgroundAlpha = (1 - blackness) * VideoEfficiency;
        }

        #endregion

        internal void Sort()
        {
            if (GameBase.Mode != OsuModes.Edit)
                //this sort is used purely for loading optimisations (see LoadDynamic).
                //if it is triggered from the editor, all hell will break loose.. and was for quite a while there ^^;.
                events.Sort();
            eventBreaks.Sort((a, b) => a.StartTime.CompareTo(b.StartTime));
        }

        internal static float GetIncreasingDrawDepth(StoryLayer layer)
        {
            spriteDrawDepth += 0.0001f;
            return 0.5f + ((float)layer * 0.1f) + spriteDrawDepth;
        }

        internal void ToggleLayer(StoryLayer layer)
        {
            int index = (int)layer;
            if (storyLayerEnabled[index])
            {
                foreach (Event ev in storyLayerSprites[index].FindAll(e => e.Sprite != null))
                    ev.Sprite.Bypass = true;
            }
            else
            {
                foreach (Event ev in storyLayerSprites[index].FindAll(e => e.Sprite != null))
                    ev.Sprite.Bypass = false;
            }

            storyLayerEnabled[index] = !storyLayerEnabled[index];
        }

        public List<Event> Clone()
        {
            List<Event> clone = new List<Event>();
            foreach (Event e in events)
                if (!e.StoryboardIgnore)
                    clone.Add(e.Clone());
            return clone;
        }

        internal void LoadEvents(List<Event> events)
        {
            Reset();

            foreach (Event e in events)
            {
                if (!e.StoryboardIgnore)
                {
                    Add(e);
                    e.BindAllEvents();
                }
            }

            //clones are not made with loops already static; we need to process them here.
            spriteManagerBG.SpriteList.ForEach(p => p.MakeAllLoopsStatic());
            spriteManagerFG.SpriteList.ForEach(p => p.MakeAllLoopsStatic());

            hideBackgroundEvent();
        }

        private void hideBackgroundEvent()
        {
            if (backgroundEvent != null && backgroundEvent.Sprite != null)
                backgroundEvent.Sprite.Bypass = true;
        }

        public bool HasStoryboard;
        public bool HasPassFailLayers;

        internal static bool AllowVideoSeek;

        private int dynamicLoadIndex;
        private int dynamicLoadCompletedIndex;
        internal bool InitialLoadComplete { get; private set; }

        public void LoadDynamic(bool firstPass, bool incrementalLoading)
        {
#if !DEBUG
            try
            {
#endif

            EventBreak eb = eventBreaks.Find(e => e.StartTime > AudioEngine.Time);
            int loadPoint = incrementalLoading && eb != null ? eb.EndTime : Int32.MaxValue;

            if (firstPass)
            {
                Sort(); //run this once after loading.

                if (showStoryboard || !incrementalLoading)
                {
                    //First load is done on main thread.
                    foreach (Event e in events)
                    {
                        if (e.Sprite != null)
                        {
                            if (e.Sprite.Transformations.Count != 0 && e.Sprite.Transformations[0].Time1 > loadPoint)
                                break;

                            if (!e.Sprite.Bypass || !incrementalLoading)
                                e.Load();
                        }

                        dynamicLoadIndex++;
                    }
                }

                InitialLoadComplete = true;

                if (incrementalLoading)
                    dynamicLoadIndex = 0;
                else
                    dynamicLoadCompletedIndex = dynamicLoadIndex;
            }
            else if (dynamicLoadIndex < events.Count)
            {
                if (!InitialLoadComplete)
                    return;

                // Wait for the previous loading task to be completed
                // to avoid queuing them faster than they are processed
                if (dynamicLoadCompletedIndex < dynamicLoadIndex)
                    return;

                // Load events faster in the middle of a break
                bool canLoadMoreEvents = BreakCurrent != null
                    && BreakCurrent.StartTime + 1000 < AudioEngine.Time
                    && AudioEngine.Time < BreakCurrent.EndTime - 1000;

                bool hasLoadableEvent = false;
                int startIndex = dynamicLoadIndex;
                int endIndex = startIndex;
                for (; endIndex < events.Count; endIndex++)
                {
                    Event e = events[endIndex];

                    // Skip events that don't need to be loaded
                    if (e.Loaded || e.Sprite == null || e.Sprite.Transformations.Count == 0)
                    {
                        if (!hasLoadableEvent)
                            startIndex++;

                        continue;
                    }

                    hasLoadableEvent = true;

                    // Ensure events that are displayed or will be in the next seconds are loaded
                    if (e.StartTime < AudioEngine.Time + (canLoadMoreEvents ? 10000 : 2000))
                        continue;

                    // Stop loading when it reaches the load point
                    if (e.StartTime > loadPoint)
                        break;

                    // Load some events each time
                    if (endIndex - startIndex >= (canLoadMoreEvents ? 4 : 1))
                        break;
                }

                int loadCount = endIndex - startIndex;
                if (hasLoadableEvent && loadCount > 0)
                {
#if DEBUG
                    if (loadCount > 4)
                        Debug.Print("Dynamically loading " + loadCount + " event(s).");
#endif

                    // Dynamic loads can be shuffled to a second thread.
                    GameBase.RunGraphicsThread(delegate
                    {
                        for (int i = startIndex; i < endIndex; i++)
                        {
                            Event e = events[i];
                            if (!e.Loaded) e.Load();
                        }

                        dynamicLoadCompletedIndex = endIndex;
                    });
                }
                else
                {
                    dynamicLoadCompletedIndex = endIndex;
                }

                dynamicLoadIndex = endIndex;

#if DEBUG
                if (dynamicLoadIndex == events.Count)
                    Debug.Print("Finished loading all events.");
#endif
            }

            dynamicLoadIndex = Math.Min(dynamicLoadIndex, events.Count);

#if !DEBUG
            }
            catch
            {
                //ensure we continue loading the next events if anything bad happens loading a single event.
                dynamicLoadIndex++;
            }
#endif
        }

        internal static event HitObjectDelegate OnHitObjectHit;
        internal void InvokeOnHitObjectHit(HitObject h)
        {
            HitObjectDelegate del = OnHitObjectHit;
            if (del != null) del(h);
        }

        internal static event HitSoundDelegate OnHitSound;
        internal void InvokeOnHitSound(HitSoundInfo hitSoundInfo)
        {
            HitSoundDelegate sound = OnHitSound;
            if (sound != null) sound(hitSoundInfo);
        }

        internal static event VoidDelegate OnPassing;

        private static void InvokeOnPassing()
        {
            VoidDelegate passing = OnPassing;
            if (passing != null) passing();
        }

        internal static event VoidDelegate OnFailing;

        private static void InvokeOnFailing()
        {
            VoidDelegate failing = OnFailing;
            if (failing != null) failing();
        }

        public void RemoveAllBreaks()
        {
            new List<EventBreak>(eventBreaks).ForEach(Remove);
        }

        internal void UpdateBackground()
        {
            if (backgroundUsedInStoryboard && showStoryboard)
            {
                GameBase.TransitionManager.ClearBackground();
                backgroundIsReady = true;
            }
            else
            {
                Ruleset ruleset = Ruleset.Instance;
                pSprite eventSprite = backgroundEvent != null && backgroundEvent.Sprite != null ?
                    backgroundEvent.Sprite : null;

                bool storyboardVisible = HasStoryboard && showStoryboard;
                bool hasBackgroundOffset = eventSprite != null && eventSprite.Position != Vector2.Zero;
                bool applyRulesetAdjustment = ruleset != null && !HasStoryboard;

                AlignmentMode alignmentMode = storyboardVisible ?
                    WidescreenStoryboard ? AlignmentMode.StoryboardWidescreen : AlignmentMode.Storyboard
                    : AlignmentMode.Default;

                // Background offsets are relative to the StoryboardCentre Field,
                // if there is an offset, make sure that the background is on this field.
                if (hasBackgroundOffset && (alignmentMode & AlignmentMode.Storyboard) == 0)
                    alignmentMode = AlignmentMode.StoryboardWidescreen;

                backgroundIsReady = false;
                GameBase.TransitionManager.UpdateBackground(alignmentMode, backgroundSprite =>
                {
                    if (hasBackgroundOffset)
                        backgroundSprite.Position = eventSprite.Position;

                    if (applyRulesetAdjustment)
                        ruleset.AdjustBackgroundSprite(backgroundSprite);

                    backgroundIsReady = true;
                });
            }
        }

        internal void PostProcessing()
        {
            hideBackgroundEvent();

            foreach (pSprite p in spriteManagerBGWide.SpriteList)
            {
                if (p.Texture != GameBase.WhitePixel)
                    p.ScaleToScreen(!HasStoryboard || WidescreenStoryboard || BeatmapManager.Current.DisableStoryboard);
            }
        }
    }

    internal delegate void HitSoundDelegate(HitSoundInfo hitSoundInfo);
}