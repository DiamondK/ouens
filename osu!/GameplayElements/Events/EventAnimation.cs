﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameplayElements.Beatmaps;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;

namespace osu.GameplayElements.Events
{
    internal class EventAnimation : Event
    {
        private readonly Vector2 startPosition;
        private readonly Origins origin;
        private int FrameCount;
        private readonly bool useSkinSprites;
        private int endTime;

        internal pAnimation Animation;
        internal double FrameDelay;

        private int startTime;
        private float depth;
        private string[] Filenames;

        internal EventAnimation(pSprite sprite, string filename, int start, int end)
        {
            Sprite = sprite;
            Animation = sprite as pAnimation;
            startTime = 0;
            startTime = start;
            Type = EventTypes.Animation;
            endTime = end;

            Filename = filename.Replace(BeatmapManager.Current.ContainingFolder + Path.DirectorySeparatorChar, string.Empty);
        }

        internal EventAnimation(string filename, Vector2 startPosition, Origins origin, StoryLayer layer, int frameCount, double frameDelay,
            bool useSkinSprites, LoopTypes loopType)
        {
            this.startPosition = startPosition;
            this.origin = origin;
            this.FrameCount = frameCount;
            this.useSkinSprites = useSkinSprites;

            FrameDelay = Math.Max(1, frameDelay);

            Layer = layer;

            depth = EventManager.GetIncreasingDrawDepth(Layer);

            Animation =
                new pAnimation(null, Fields.Storyboard, origin, Clocks.Audio, startPosition,
                            depth, false, Color.White, null);
            Animation.LoopType = loopType;
            if (BeatmapManager.Current != null && BeatmapManager.Current.BeatmapVersion < 6)
                Animation.FrameDelay = Math.Round(0.015 * frameDelay) * 1.186 * GameBase.SIXTY_FRAME_TIME; //backwards compatibility for older storyboards.
            else
                Animation.FrameDelay = frameDelay;

            Sprite = Animation;
            Type = EventTypes.Animation;

            Filename = filename.Replace(BeatmapManager.Current.ContainingFolder + Path.DirectorySeparatorChar, string.Empty);

            Filenames = new string[FrameCount];

            for (int i = 0; i < FrameCount; i++)
            {
                string f = Filename.Replace(".", i + ".");
                SkinManager.Reference(f);
                Filenames[i] = f;
            }

            Animation.OnDisposable += Unload;
        }

        private void Unload(object sender, EventArgs e)
        {
            if (!Loaded)
                return;

            bool disposable = (bool)sender;

            if (!disposable)
                for (int i = 0; i < FrameCount; i++)
                    SkinManager.Dereference(Filenames[i]);
        }

        internal override void SetStartTime(int time)
        {
            throw new NotImplementedException();
        }

        internal override void SetEndTime(int time)
        {
            throw new NotImplementedException();
        }

        internal override bool Load()
        {
            if (Loaded) return false;

            Loaded = true;

            pTexture[] textures = null;

            if (useSkinSprites)
                textures = SkinManager.LoadAll(Path.GetFileNameWithoutExtension(Filename));

            if (!useSkinSprites || textures == null)
            {
                textures = new pTexture[FrameCount];

                if (Filenames != null)
                {
                    for (int i = 0; i < FrameCount; i++)
                        textures[i] = SkinManager.Load(Filenames[i], SkinSource.Beatmap);
                }
            }

            Animation.TextureArray = textures;

            return true;
        }

        internal override Event Clone()
        {
            pAnimation animationClone = (pAnimation)Animation.Clone();
            EventAnimation clone = new EventAnimation(animationClone, Filename, StartTime, EndTime);
            clone.FrameDelay = FrameDelay;
            clone.FrameCount = FrameCount;
            clone.WriteToOsu = WriteToOsu;
            clone.Layer = Layer;
            clone.StoryboardIgnore = StoryboardIgnore;
            return clone;
        }
    }
}