﻿using System.IO;
using osu.Graphics.Sprites;

namespace osu.GameplayElements.Events
{
    internal class EventVideo : Event
    {
        internal pVideo Video;

        internal EventVideo(pVideo video, string filename, int startTime)
        {
            Video = video;
            StoryboardIgnore = true;
            Type = EventTypes.Video;
            Filename = Path.GetFileName(filename);
            Sprite = video;
            StartTime = startTime;
            Loaded = true;
        }

        internal override bool Load()
        {
            return false;
        }

        internal override void SetStartTime(int time)
        {
            if (Video != null)
                Video.StartTime = time;
            StartTime = time;
        }

        internal override void SetEndTime(int time)
        {
            throw new System.NotImplementedException();
        }

        internal override Event Clone()
        {
            return new EventVideo(Video, Filename, StartTime);
        }
    }
}