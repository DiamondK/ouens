﻿using osu.Audio;
using osu.GameplayElements.Beatmaps;
using System;
using Un4seen.Bass;

namespace osu.GameplayElements.Events
{
    internal class EventSample : Event, IComparable<EventSample>
    {
        internal int Volume;
        internal SampleCacheItem SampleCache;
        internal int SampleId;
        internal bool Selected;

        internal EventSample(int channel,string filename, int time, StoryLayer layer, int volume)
        {
            this.Volume = volume;
            Type = EventTypes.Sample;
            Filename = filename;

            //todo: optimise this
            SampleId = channel;

            StartTime = time;
            Layer = layer;

            SampleCache = new SampleCacheItem(StartTime, SampleId,Volume,true);
        }

        internal override bool Load()
        {
            return false;
        }

        internal override void SetStartTime(int time)
        {
            throw new System.NotImplementedException();
        }

        internal override void SetEndTime(int time)
        {
            throw new System.NotImplementedException();
        }

        internal override Event Clone()
        {
            return new EventSample(SampleId, Filename, StartTime, Layer, Volume) { WriteToOsu = this.WriteToOsu };
        }

        public override string ToString()
        {
            int minute = StartTime / 1000 / 60;
            int sec = (StartTime - minute*1000*60) / 1000;
            int mill = StartTime - minute*1000*60 - sec*1000;
            return string.Format("{0}:{1}:{2}  {3}@{4}%", minute, sec, mill, Filename, Volume);
        }

        #region IComparable<EventBreak> Members

        public int CompareTo(EventSample other)
        {
            return StartTime.CompareTo(other.StartTime);
        }

        #endregion
    }
}