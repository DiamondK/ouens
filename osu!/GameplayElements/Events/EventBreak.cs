﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Sprites;

namespace osu.GameplayElements.Events
{
    internal class EventBreak : Event, IComparable<EventBreak>
    {
        public bool CustomStart = false, CustomEnd = false;

        internal EventBreak(int startTime, int endTime)
        {
            Type = EventTypes.Break;
            StartTime = startTime;
            EndTime = endTime;

            Loaded = true;
/*
            Sprite =
                new pSprite(GameBase.WhitePixel, Fields.StandardNoScale, Origins.TopLeft, Clocks.Audio, Vector2.Zero,
                            0.02F, false,
                            Color.TransparentBlack);
            //todo: fix this
            Sprite.VectorScale = new Vector2(GameBase.WindowWidth, GameBase.WindowHeight);

            Sprite.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, startTime, startTime + 300));
            Sprite.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, endTime - 300, endTime));*/
        }

        internal override void SetStartTime(int time)
        {
                StartTime = time;
    /*            Sprite.Transformations[0].Time1 = StartTime;
                Sprite.Transformations[0].Time2 = StartTime + 300;*/

        }

        internal override void SetEndTime(int time)
        {
                EndTime = time;
           /*     Sprite.Transformations[1].Time1 = EndTime - 300;
                Sprite.Transformations[1].Time2 = EndTime;*/
        }

        internal override bool Load()
        {
            return false;
        }

        internal override Event Clone()
        {
            EventBreak result = new EventBreak(StartTime, EndTime);
            result.CustomStart = CustomStart;
            result.CustomEnd = CustomEnd;
            return result;
        }

        #region IComparable<EventBreak> Members

        public int CompareTo(EventBreak other)
        {
            return StartTime.CompareTo(other.StartTime);
        }

        #endregion


    }
}