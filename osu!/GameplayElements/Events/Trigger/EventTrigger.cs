﻿using osu.Audio;
using osu.Graphics.Sprites;
using osu.Helpers;
using System;
using System.Collections.Generic;

namespace osu.GameplayElements.Events.Trigger
{
    abstract class EventTrigger
    {
        private TriggerGroup specificTriggerGroup = TriggerGroup.None;

        internal bool HasSpecificTriggerGroup
        {
            get
            {
                return specificTriggerGroup != TriggerGroup.None;
            }
        }

        internal TriggerGroup TriggerGroup
        {
            get
            {
                if (HasSpecificTriggerGroup)
                    return specificTriggerGroup;
                return BaseTriggerGroup;
            }
            set
            {
                // Prevents default groups to be picked
                if ((int)value > 0)
                    throw new InvalidOperationException("Invalid trigger group");

                // The group can't be changed once set because it might be already listed in an Event's EventLoopTriggers
                if (specificTriggerGroup != TriggerGroup.None)
                    throw new InvalidOperationException("A trigger's group can't be changed once set");

                specificTriggerGroup = value;
            }
        }

        internal abstract TriggerGroup BaseTriggerGroup
        {
            get;
        }

        internal abstract void Bind(Event evt, TriggerLoop loop, pSprite sprite);

        protected void Trigger(Event evt, TriggerLoop loop, pSprite sprite, bool alwaysClearLoops)
        {
            if (alwaysClearLoops)
                evt.RemoveAllLoops(TriggerGroup);

            if (loop.TriggerEndTime != 0 && 
                (AudioEngine.Time < loop.TriggerStartTime || AudioEngine.Time > loop.TriggerEndTime)) 
                    return;

            if (!alwaysClearLoops) 
            {
                if (HasSpecificTriggerGroup)
                    evt.RemoveAllLoops(TriggerGroup);
                else
                    evt.RemoveLoop(loop);
            }

            loop.StartTime = AudioEngine.Time;
            sprite.MakeLoopStatic(loop);
        }

        internal void SetSpecificTriggerGroup(TriggerGroup triggerGroup)
        {
            // Prevents default groups to be picked
            if ((int)triggerGroup > 0)
                throw new InvalidOperationException("Invalid trigger group");

            // The group can't be changed once set because it might be already listed in an Event's EventLoopTriggers
            if (specificTriggerGroup != TriggerGroup.None)
                throw new InvalidOperationException("A trigger's group can't be changed once set");

            specificTriggerGroup = triggerGroup;
        }

        public override string ToString()
        {
            return BaseTriggerGroup.ToString();
        }

        internal static EventTrigger Create(string triggerName) 
        {
            TriggerGroup triggerGroup;
            if (EnumHelper.TryParseStartsWith(triggerName, out triggerGroup))
            {
                int groupNameLength = triggerGroup.ToString().Length;
                string description = triggerName.Substring(groupNameLength, triggerName.Length - groupNameLength);

                switch (triggerGroup)
                {
                    case TriggerGroup.Passing:
                        return new EventTriggerPassing(description);
                    case TriggerGroup.Failing:
                        return new EventTriggerFailing(description);
                    case TriggerGroup.HitObjectHit:
                        return new EventTriggerHitObjectHit(description);
                    case TriggerGroup.HitSound:
                        return new EventTriggerHitSound(description);
                }
            }
            throw new Exception(triggerName + " isn't a valid trigger name");
        }
    }
}
