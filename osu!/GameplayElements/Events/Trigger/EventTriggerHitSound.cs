﻿using osu.Audio;
using osu.GameplayElements.HitObjects;
using osu.Graphics.Sprites;
using osu.Helpers;
using System;

namespace osu.GameplayElements.Events.Trigger
{
    class EventTriggerHitSound : EventTrigger
    {
        internal HitObjectSoundType SoundType;
        internal SampleSet SampleSet = SampleSet.All;
        internal SampleSet SampleSetAdditions = SampleSet.All;
        internal CustomSampleSet CustomSampleSet;
        internal bool MainSampleSetDefined;
        internal bool AdditionsSampleSetDefined;
        internal bool CheckCustomSampleSet;
        
        internal override TriggerGroup BaseTriggerGroup
        {
            get { 
                return TriggerGroup.HitSound; 
            }
        }

        internal EventTriggerHitSound(string description)
        {
            string remainingDescription = description;

            // Main and additions sample sets
            SampleSet parsedSampleSet;
            if (EnumHelper.TryParseStartsWith(remainingDescription, out parsedSampleSet)
                && parsedSampleSet != SampleSet.None)
            {
                SampleSet = parsedSampleSet;
                MainSampleSetDefined = true;
                remainingDescription = remainingDescription.Substring(SampleSet.ToString().Length,
                    remainingDescription.Length - SampleSet.ToString().Length);

                // Additions

                if (EnumHelper.TryParseStartsWith(remainingDescription, out parsedSampleSet)
                    && parsedSampleSet != SampleSet.None)
                {
                    SampleSetAdditions = parsedSampleSet;
                    AdditionsSampleSetDefined = true;
                    remainingDescription = remainingDescription.Substring(SampleSetAdditions.ToString().Length,
                        remainingDescription.Length - SampleSetAdditions.ToString().Length);
                }
            }

            // Sound type
            HitObjectSoundType parsedSoundType;
            if (EnumHelper.TryParseStartsWith(remainingDescription, out parsedSoundType)
                && parsedSoundType != HitObjectSoundType.None && parsedSoundType != HitObjectSoundType.Normal)
            {
                SoundType = parsedSoundType;
                remainingDescription = remainingDescription.Substring(SoundType.ToString().Length,
                    remainingDescription.Length - SoundType.ToString().Length);
            }

            // Custom sample set
            int parsedSampleSetIndex;
            if (Int32.TryParse(remainingDescription, out parsedSampleSetIndex))
            {
                CustomSampleSet = (CustomSampleSet)parsedSampleSetIndex;
                CheckCustomSampleSet = true;
            }

            // Make trigger descriptions have more intuitive results:
            // - HitSoundDrumWhistle refers to the whistle addition being from the drum sampleset, 
            //   if you'd wanted a trigger on a drum sampleset + any whistle addition (uncommon), you'd use HitSoundDrumAllWhistle
            if (SoundType != HitObjectSoundType.None && MainSampleSetDefined && !AdditionsSampleSetDefined)
            {
                SampleSetAdditions = SampleSet;
                SampleSet = SampleSet.All;

                MainSampleSetDefined = false;
                AdditionsSampleSetDefined = true;
            }

            // Check that the description is valid
            if (!ToString().Equals(BaseTriggerGroup.ToString() + description))
                throw new Exception("Invalid hitsound trigger description after " + ToString());
        }

        internal override void Bind(Event evt, TriggerLoop loop, pSprite sprite)
        {
            EventManager.OnHitSound += delegate(HitSoundInfo hitSoundInfo)
            {
                if (SampleSet != SampleSet.All && hitSoundInfo.SampleSet != SampleSet)
                    return;

                if (SampleSetAdditions != SampleSet.All && 
                    !(hitSoundInfo.SampleSetAdditions == SampleSetAdditions
                    || hitSoundInfo.SampleSetAdditions == SampleSet.None && hitSoundInfo.SampleSet == SampleSetAdditions))
                    return;

                if (SoundType != HitObjectSoundType.None && !hitSoundInfo.SoundType.IsType(SoundType))
                    return;

                if (CheckCustomSampleSet && hitSoundInfo.CustomSampleSet != CustomSampleSet)
                    return;

                Trigger(evt, loop, sprite, false);
            };
        }

        public override string ToString()
        {
            string triggerName = BaseTriggerGroup.ToString();
            if (MainSampleSetDefined)
                triggerName += SampleSet;
            if (AdditionsSampleSetDefined)
                triggerName += SampleSetAdditions;
            if (SoundType != HitObjectSoundType.None)
                triggerName += SoundType;
            if (CheckCustomSampleSet)
                triggerName += ((int)CustomSampleSet);
            return triggerName;
        }
    }
}
