﻿using osu.Graphics.Sprites;
using System;

namespace osu.GameplayElements.Events.Trigger
{
    class EventTriggerFailing : EventTrigger
    {
        internal override TriggerGroup BaseTriggerGroup
        {
            get { 
                return TriggerGroup.Failing; 
            }
        }

        public EventTriggerFailing(string description)
        {
            if (description.Length > 0)
                throw new ArgumentException("Invalid trigger description: " + description);
        }

        internal override void Bind(Event evt, TriggerLoop loop, pSprite sprite)
        {
            EventManager.OnFailing += delegate
            {
                Trigger(evt, loop, sprite, true);
            };
        }
    }
}
