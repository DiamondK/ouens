﻿using osu.GameplayElements.HitObjects;
using osu.Graphics.Sprites;
using System;

namespace osu.GameplayElements.Events.Trigger
{
    class EventTriggerHitObjectHit : EventTrigger
    {
        internal override TriggerGroup BaseTriggerGroup
        {
            get { 
                return TriggerGroup.HitObjectHit; 
            }
        }

        public EventTriggerHitObjectHit(string description)
        {
            if (description.Length > 0)
                throw new ArgumentException("Invalid trigger description: " + description);
        }

        internal override void Bind(Event evt, TriggerLoop loop, pSprite sprite)
        {
            EventManager.OnHitObjectHit += delegate(HitObject h)
            {
                //only trigger if the actual value of this hit is positive (wasn't a miss or worse).
                if (h.scoreValue > 0)
                    Trigger(evt, loop, sprite, false);
            };
        }
    }
}
