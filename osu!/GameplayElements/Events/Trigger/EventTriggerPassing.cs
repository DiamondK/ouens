﻿using osu.Graphics.Sprites;
using System;

namespace osu.GameplayElements.Events.Trigger
{
    class EventTriggerPassing : EventTrigger
    {
        internal override TriggerGroup BaseTriggerGroup
        {
            get { 
                return TriggerGroup.Passing; 
            }
        }

        public EventTriggerPassing(string description)
        {
            if (description.Length > 0)
                throw new ArgumentException("Invalid trigger description: " + description);
        }

        internal override void Bind(Event evt, TriggerLoop loop, pSprite sprite)
        {
            EventManager.OnPassing += delegate
            {
                Trigger(evt, loop, sprite, true);
            };
        }
    }
}
