﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameplayElements.Beatmaps;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;

namespace osu.GameplayElements.Events
{
    internal class EventSprite : Event
    {
        private readonly Vector2 startPosition;
        private readonly Origins origin;
        private readonly bool useSkinTextures;
        private int endTime;

        private int startTime;
        private float depth;

        internal EventSprite(pSprite sprite, string filename, int start, int end)
        {
            Sprite = sprite;
            startTime = 0;
            startTime = start;
            endTime = end;
            Type = EventTypes.Sprite;

            Filename = filename.Replace(BeatmapManager.Current.ContainingFolder + Path.DirectorySeparatorChar, string.Empty);
        }

        internal EventSprite(string filename, Vector2 startPosition, Origins origin, StoryLayer layer)
            : this(filename, startPosition, origin, layer, false)
        {

        }

        internal EventSprite(string filename, Vector2 startPosition, Origins origin, StoryLayer layer, bool useSkinTextures)
        {
            this.startPosition = startPosition;
            this.origin = origin;
            this.useSkinTextures = useSkinTextures;
            Layer = layer;
            Type = EventTypes.Sprite;
            depth = EventManager.GetIncreasingDrawDepth(Layer);
            Filename = filename.Replace(BeatmapManager.Current.ContainingFolder + Path.DirectorySeparatorChar, string.Empty);

            SkinManager.Reference(filename);

            Sprite = new pSprite(null, Fields.Storyboard, origin, Clocks.Audio, startPosition, depth, false, Color.White);

            Sprite.OnDisposable += Unload;
        }

        internal void Unload(object sender, EventArgs e)
        {
            bool disposable = (bool)sender;

            if (!disposable)
                SkinManager.Dereference(Filename);
        }

        internal override bool Load()
        {
            if (Loaded) return false;
            Loaded = true;

            if (useSkinTextures)
                Sprite.Texture = SkinManager.Load(Path.GetFileNameWithoutExtension(Filename)) ?? SkinManager.Load(Filename);
            else
                Sprite.Texture = SkinManager.Load(Filename, SkinSource.Beatmap);

            return true;
        }

        internal override void SetStartTime(int time)
        {
            throw new System.NotImplementedException();
        }

        internal override void SetEndTime(int time)
        {
            throw new System.NotImplementedException();
        }

        internal override Event Clone()
        {
            pSprite spriteClone = Sprite.Clone();
            EventSprite clone = new EventSprite(spriteClone, Filename, StartTime, EndTime);
            clone.WriteToOsu = WriteToOsu;
            clone.Type = Type;
            clone.Layer = Layer;
            clone.StoryboardIgnore = StoryboardIgnore;
            clone.EventLoopTriggers = EventLoopTriggers;
            return clone;
        }
    }
}
