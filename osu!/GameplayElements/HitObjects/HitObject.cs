using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Events;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu.Input;
using osu.GameplayElements.Beatmaps;
using osu_common;

namespace osu.GameplayElements.HitObjects
{
    internal abstract class HitObject : HitObjectBase, IDisposable, IComparable<HitObject>, IComparable<int>
    {
        #region General & Timing

        internal bool IsHit;

        private bool IsSelected;

        internal double MaxHp;

        /// <summary>
        /// This is only for Taiko but must be shared by all Taiko objects.
        /// It would logically go on a "HitObjectTaiko" class, but there's
        /// no multiple inheritance so boo.
        ///
        /// ^ what. why not just populate this for all hitobjects instead :(.
        /// </summary>
        internal bool Kiai;

        /// <summary>
        /// If this object has a New Combo marker, how many additional colours do we cycle by?
        /// </summary>
        internal int ComboOffset = 0;

        /// <summary>
        /// The zero-based index for which combo colour to use.
        /// Needed where an RGB triple is insufficient.
        /// </summary>
        internal int ComboColourIndex;


        internal HitObjectManager hitObjectManager;


        protected HitObject(HitObjectManager hom)
            : base()
        {
            this.hitObjectManager = hom;
        }

        ~HitObject()
        {
            Dispose(false);
        }


        internal bool Selected
        {
            get { return IsSelected; }

            set
            {
                if (IsSelected != value)
                {
                    IsSelected = value;
                    if (IsSelected)
                        Select();
                    else
                        Deselect();
                }
            }
        }

        internal virtual void SetColour(Color color)
        {
        }

        internal virtual void ApplyDim()
        {
            if (GameBase.Mode == OsuModes.Edit)
                return;

            Color col1 = new Color((byte)Math.Max(0, Colour.R * 0.75F), (byte)Math.Max(0, Colour.G * 0.75F), (byte)Math.Max(0, Colour.B * 0.75F), 255);
            Color col1_alt = new Color(195, 195, 195, 255);
            Color col2_alt = Color.White;

            foreach (pSprite p in DimCollection)
            {
                p.Transformations.RemoveAll(t => t.Type == TransformationType.Colour && t.TagNumeric == HitObject.DIM);
                Transformation tr = new Transformation(p.TagNumeric == 0 ? col1_alt : col1, p.TagNumeric == 0 ? col2_alt : Colour, StartTime - 400, StartTime - 300);
                if (tr != null) tr.TagNumeric = HitObject.DIM;
                p.Transformations.Add(tr);
            }
        }

        internal abstract IncreaseScoreType Hit();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing) { }

        internal abstract HitObject Clone();

        internal abstract void Select();
        internal abstract void Deselect();
        internal abstract void ModifyTime(int newTime);
        internal abstract void ModifyPosition(Vector2 newPosition);

        internal virtual void Update()
        {
            if (AudioEngine.IsReversed && IsHit && AudioEngine.Time < StartTime) Disarm();
        }

        internal virtual void Disarm()
        {
            SpriteCollection.ForEach(p => p.Transformations.RemoveAll(t => t.TagNumeric == ARMED));
            IsHit = false;
        }

        internal const int ARMED = 148;
        internal const int DIM = 147;

        internal virtual void Arm(bool isHit)
        {
            SpriteCollection.ForEach(p => p.Transformations.RemoveAll(t => t.TagNumeric == ARMED));
        }

        internal virtual void Draw()
        {
            return;
        }

        #endregion

        #region Drawing

        protected internal List<pSprite> DimCollection = new List<pSprite>();

        internal List<pSprite> SpriteCollection = new List<pSprite>();


        internal abstract bool IsVisible { get; }

        internal virtual Vector2 Position2
        {
            get { return Position; }
            set { }
        }

        #endregion

        #region Sound

        internal bool Dimmed;
        internal bool Sounded;

        internal int SampleAddr = -1;
        internal string SampleFile;
        internal int SampleVolume = 0;

        /// <summary>
        /// Sampleset for hit, slide, and tick sounds.
        /// SampleSet.None means to use the timing point's sampleset.
        /// </summary>
        internal SampleSet SampleSet;
        /// <summary>
        /// Sampleset for whistle, finish, and clap sounds.
        /// SampleSet.None means to use the timing point's sampleset.
        /// </summary>
        internal SampleSet SampleSetAdditions;
        internal CustomSampleSet CustomSampleSet;

        internal bool Drawable;
        /// <summary>
        /// Whether to add this object's score to the counters (hit300 count etc.)
        /// </summary>
        public bool IsScorable = true;
        public int TagNumeric;
        public int scoreValue;

        internal void ProcessSampleFile(string file)
        {
            if (hitObjectManager.spriteManager != null && !string.IsNullOrEmpty(file) && !AudioEngine.IgnoreBeatmapSamples)
            {
                SampleFile = file;

                //ensure slider has the right sample config.
                bool loop = false;
                if (this.IsType(HitObjectType.Slider))
                {
                    if (file.IndexOf(@"slider") > 0 && file.IndexOf(@"tick") == -1)
                        loop = true;
                }
                SampleAddr = AudioEngine.LoadBeatmapSample(file, loop, false);
            }
            else
                SampleAddr = -1;
        }

        internal void PlaySound(ref ControlPoint controlPoint, HitObjectSoundType soundType, SampleSet sampleSet, SampleSet sampleSetAdditions)
        {
            SampleSet ss = sampleSet == Audio.SampleSet.None ? controlPoint.sampleSet : sampleSet;
            CustomSampleSet css = CustomSampleSet == CustomSampleSet.Default ? controlPoint.customSamples : CustomSampleSet;
            int vol = SampleVolume != 0 ? SampleVolume : controlPoint.volume;

            HitSoundInfo hitSoundInfo = HitSoundInfo.GetLayered(soundType, ss, css, vol, ss == Audio.SampleSet.None ? ss : sampleSetAdditions);
            HitObjectManager.OnHitSound(hitSoundInfo);

            if (SampleAddr != -1)
                AudioEngine.PlaySample(SampleAddr, vol, PositionalSound, true);
            else
                AudioEngine.PlayHitSamples(hitSoundInfo, PositionalSound, true);
        }

        internal virtual void PlaySound()
        {
            ControlPoint pt = hitObjectManager.Beatmap.controlPointAt(StartTime + 5);
            if (pt == null) 
                return;

            PlaySound(ref pt, SoundType, SampleSet, SampleSetAdditions);
        }

        /// <summary>
        /// A list of sprites which are lightable for kiai.
        /// </summary>
        /// <value>The lightable sprites.</value>
        internal virtual List<pSprite> KiaiSprites
        {
            get { return DimCollection.Count > 0 ? new List<pSprite>() { DimCollection[0] } : new List<pSprite>(); }
        }

        protected virtual float PositionalSound { get { return (Position.X / 512f - 0.5f) * 0.8f; } }

        /// <summary>
        /// Gets the hittable end time (valid active object time for sliders etc. - used in taiko to extend when hits are valid).
        /// </summary>
        /// <value>The hittable end time.</value>
        internal virtual int HittableEndTime
        {
            get { return EndTime; }
        }

        /// <summary>
        /// Gets the hittable start time (valid active object time for sliders etc. - used in taiko to extend when hits are valid).
        /// </summary>
        /// <value>The hittable start time.</value>
        internal virtual int HittableStartTime
        {
            get { return StartTime; }
        }

        #endregion

        #region IComparable<HitObject> Members

        public int CompareTo(HitObject other)
        {
            if ((this is HitObjectDummy) || (other is HitObjectDummy)) return EndTime.CompareTo(other.EndTime);

            if (StartTime == other.StartTime) return other.NewCombo.CompareTo(NewCombo);
            return StartTime.CompareTo(other.StartTime);
        }

        #endregion

        internal abstract IncreaseScoreType GetScorePoints(Vector2 currentMousePos);

        internal virtual void StopSound()
        {
        }

        internal abstract void SetEndTime(int time);

        public int CompareTo(int other)
        {
            return EndTime.CompareTo(other);
        }

        internal virtual bool HitTest(Vector2 testPosition, bool hittableRangeOnly, float radius)
        {
            return ((!hittableRangeOnly && IsVisible) ||
                  (StartTime - hitObjectManager.PreEmpt <= AudioEngine.Time &&
                   StartTime + hitObjectManager.HitWindow50 >= AudioEngine.Time && !IsHit)) &&
                 (Vector2.DistanceSquared(testPosition, Position) <= radius * radius ||
                  (!hittableRangeOnly &&
                   Vector2.DistanceSquared(testPosition, Position2) <= radius * radius));
        }

        const int TAG_SHAKE = 123;

        internal virtual void Shake()
        {
            foreach (pSprite p in SpriteCollection)
            {
                p.Transformations.RemoveAll(t => t.Type == TransformationType.Movement && TagNumeric == TAG_SHAKE);

                Vector2 startPos = Position2;

                p.Transformations.Add(new Transformation(startPos, startPos + new Vector2(8, 0), AudioEngine.Time, AudioEngine.Time + 20) { TagNumeric = TAG_SHAKE });
                p.Transformations.Add(new Transformation(startPos + new Vector2(8, 0), startPos - new Vector2(8, 0), AudioEngine.Time + 20, AudioEngine.Time + 40) { TagNumeric = TAG_SHAKE });
                p.Transformations.Add(new Transformation(startPos - new Vector2(8, 0), startPos + new Vector2(8, 0), AudioEngine.Time + 40, AudioEngine.Time + 60) { TagNumeric = TAG_SHAKE });
                p.Transformations.Add(new Transformation(startPos + new Vector2(8, 0), startPos - new Vector2(8, 0), AudioEngine.Time + 60, AudioEngine.Time + 80) { TagNumeric = TAG_SHAKE });
                p.Transformations.Add(new Transformation(startPos + new Vector2(8, 0), startPos - new Vector2(8, 0), AudioEngine.Time + 80, AudioEngine.Time + 100) { TagNumeric = TAG_SHAKE });
                p.Transformations.Add(new Transformation(startPos + new Vector2(8, 0), startPos, AudioEngine.Time + 100, AudioEngine.Time + 120) { TagNumeric = TAG_SHAKE });
            }
        }

        protected virtual int LocalPreEmpt
        {
            get
            {
                return hitObjectManager.PreEmpt;
            }
        }
    }

    internal delegate void HitObjectDelegate(HitObject h);

    internal struct HitSoundInfo
    {
        public HitObjectSoundType SoundType;
        public SampleSet SampleSet;
        public SampleSet SampleSetAdditions;
        public CustomSampleSet CustomSampleSet;
        public int Volume;

        public HitSoundInfo(HitObjectSoundType SoundType, SampleSet SampleSet, CustomSampleSet CustomSampleSet, int Volume, SampleSet SampleSetAdditions = SampleSet.None)
        {
            this.SoundType = SoundType;
            this.SampleSet = SampleSet;
            this.SampleSetAdditions = SampleSetAdditions;
            this.CustomSampleSet = CustomSampleSet;
            this.Volume = Volume;
        }

        public static HitSoundInfo GetLayered(HitObjectSoundType SoundType, SampleSet SampleSet, CustomSampleSet CustomSampleSet, int Volume, SampleSet SampleSetAdditions = SampleSet.None) 
        {
            if (SoundType == HitObjectSoundType.None || (SkinManager.Current.LayeredHitSounds && BeatmapManager.Current.PlayMode != PlayModes.OsuMania))
                SoundType |= HitObjectSoundType.Normal;

            return new HitSoundInfo(SoundType, SampleSet, CustomSampleSet, Volume, SampleSetAdditions);
        }

        public override string ToString()
        {
            return String.Format(@"{1} {2} {3} {4}% - {0}", SoundType, SampleSet, SampleSetAdditions, CustomSampleSet, Volume);
        }
    };
}