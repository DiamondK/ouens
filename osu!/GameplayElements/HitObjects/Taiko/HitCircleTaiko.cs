﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Sprites;
using osu.Input;
using osu.GameModes.Play;

namespace osu.GameplayElements.HitObjects.Taiko
{
    internal class HitCircleTaiko : HitCircleOsu
    {
        private bool hitOnce;
        private int hitOnceTime;

        internal bool IsHitTwice { get; private set; }

        internal bool CorrectButtonIsLeft { get; private set; } //is a red hit

        public HitCircleTaiko(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType, bool kiai)
            : base(hom, startPosition, startTime, newCombo, soundType, 0)
        {
            IsHitTwice = false;
            CorrectButtonIsLeft = (SoundType & ~HitObjectSoundType.Finish & ~HitObjectSoundType.Normal) == HitObjectSoundType.None;

            float scale = DefaultScale;
            foreach (pSprite p in SpriteCollection)
                p.Scale *= scale;

            if (CorrectButtonIsLeft)
                base.SetColour(new Color(235, 69, 44));
            else
                base.SetColour(new Color(67, 142, 172));

            if (SpriteHitCircle2 != null)
                SpriteHitCircle2.RunAnimation = false;

            SpriteCollection.ForEach(s => s.Field = Fields.GamefieldWide);

            Kiai = kiai;
        }

        protected override bool ShowApproachCircle
        {
            get { return false; }
        }

        protected override bool ShowCircleText
        {
            get { return false; }
        }

        public override int ComboNumber
        {
            get { return -1; }
            set { base.ComboNumber = -1; }
        }

        internal override void ApplyDim()
        {
            //base.ApplyDim();
        }

        protected override float PositionalSound
        {
            get
            {
                return 0f;
            }
        }


        internal override void PlaySound()
        {
            ControlPoint pt = hitObjectManager.Beatmap.controlPointAt(StartTime + 5);
            int vol = SampleVolume != 0 ? SampleVolume : pt.volume;
            
            HitObjectManager.OnHitSound(new HitSoundInfo(SoundType, pt.sampleSet, pt.customSamples, vol));

            if (SoundType.IsType(HitObjectSoundType.Finish))
            {
                AudioEngine.PlayHitSamples(new HitSoundInfo(CorrectButtonIsLeft ? HitObjectSoundType.Finish : HitObjectSoundType.Whistle, pt.sampleSet, pt.customSamples, vol), PositionalSound, true);
            }
        }

        bool lastButton1;
        bool lastlastButton1;
        bool bothButtonsInstantaneous;
        bool secondHitAllowable;
        bool wrongButton;
        private IncreaseScoreType hitOnceValue;

        internal override bool HitTest(Vector2 testPosition, bool hittableRangeOnly, float radius)
        {
            lastlastButton1 = lastButton1;
            lastButton1 = false;
            bothButtonsInstantaneous = false;
            wrongButton = false;


            bool correctButton = false;
            if (InputManager.leftButton1i || InputManager.leftButton2i || Player.Relaxing)
            {
                if (CorrectButtonIsLeft || Player.Relaxing)
                {
                    correctButton = true;
                    lastButton1 = InputManager.leftButton1i;
                    bothButtonsInstantaneous = InputManager.leftButton1i && InputManager.leftButton2i;
                }
                else
                {
                    wrongButton = true;
                }
            }
            if (InputManager.rightButton1i || InputManager.rightButton2i)
            {
                if (!CorrectButtonIsLeft || Player.Relaxing)
                {
                    correctButton = true;
                    lastButton1 = InputManager.rightButton1i;
                    bothButtonsInstantaneous = InputManager.rightButton1i && InputManager.rightButton2i;
                }
                else
                {
                    wrongButton = true;
                }
            }

            //Exit early in the case the user has pressed the wrong button.
            if (wrongButton && !hitOnce)
                return Math.Abs(StartTime - AudioEngine.Time) <= hitObjectManager.HitWindow50 && !IsHit;

            secondHitAllowable = hitOnce && taikoLarge;

            //in relaxmdoe the correctbutton is whatever oyu want it to be
            bool correctButtonIsLeft = InputManager.leftButton1i || InputManager.leftButton2i;

            //For large taiko hits (finish hits) we need to allow for the input of two hits.
            //These may not come in on the same Update call.

            if (secondHitAllowable)
            {
                // <missing peppy comment>
                //If you dont keep holding the first button as you do the second beat, we read it as 
                //you didn't intend to do a doublehit, so we return false.
                //We need this limitation so we wont wait too long for a second hit to come,
                //as this would limit the player to do other beats after this large beat if the player 
                //didn't intend to doublehit.
                // </missing peppy comment>
                if (correctButtonIsLeft)
                {
                    if (!InputManager.leftButton1 || !InputManager.leftButton2)
                        secondHitAllowable = false;
                }
                else
                {
                    if (!InputManager.rightButton1 || !InputManager.rightButton2)
                        secondHitAllowable = false;
                }

                //The second hit must be within 30ms of the first.  This is equivalent of one (and a bit) frames when frame limited.
                secondHitAllowable &= AudioEngine.Time - hitOnceTime < 30;
            }

            return (
                //If this is the first hit of a finish or a normal hit, it must fall in the hitWindow50 range to be hit/missable
                (Math.Abs(StartTime - AudioEngine.Time) <= hitObjectManager.HitWindow50 && !hitOnce)
                //If this is the second hit of a finish, it must abide the above 30ms window only.
                || secondHitAllowable)
                //check correct button in both cases.
                && correctButton
                && !IsHit;
        }

        bool taikoLarge { get { return SoundType.IsType(HitObjectSoundType.Finish); } }

        internal override string SpriteNameHitCircle { get { return taikoLarge ? "taikobigcircle" : "taikohitcircle"; } }

        internal override IncreaseScoreType Hit()
        {
            //Small hit, do as per normal

            //If large, we expect to reach here twice.
            //Record the time of the first button press and if the second is within 30ms, double the value.

            int accuracy = Math.Abs(AudioEngine.Time - StartTime);

            if (accuracy < hitObjectManager.HitWindow300 && !wrongButton)
                hitValue = IncreaseScoreType.Hit300;
            else if (accuracy < hitObjectManager.HitWindow100 && !wrongButton)
                hitValue = IncreaseScoreType.Hit100;
            else
                hitValue = IncreaseScoreType.Miss;

            if (!hitOnce)
            {
                if (taikoLarge && hitValue != IncreaseScoreType.Miss)
                {
                    if (bothButtonsInstantaneous)
                    {
                        //Both buttons have been hit at the same time in the same frame.
                        //Easy.  If this always happened, things would be simple.
                        IsHit = true;
                        IsHitTwice = true;
                        hitValue |= IncreaseScoreType.TaikoLargeHitBoth;
                    }
                    else
                    {
                        //Only one hit was found.  We set hitOnce but don't mark this as hit.
                        //If HitTest() allows the second hit, it will reach the fallthrough condition below..
                        hitOnce = true;
                        hitOnceValue = hitValue;
                        hitOnceTime = AudioEngine.Time;
                        hitValue |= IncreaseScoreType.TaikoLargeHitFirst;
                    }

                }
                else
                    IsHit = true;

                if (hitValue > 0)
                    PlaySound();

                Arm(hitValue > 0);

                return hitValue;
            }

            if (secondHitAllowable && taikoLarge)
            {
                //If we got here, it's a large hit which has only been hit once.
                IsHit = true;
                IsScorable = false;

                if (AudioEngine.Time - hitOnceTime < 30 && lastlastButton1 != lastButton1)
                {
                    IsHitTwice = true;
                    return hitOnceValue | IncreaseScoreType.TaikoLargeHitSecond;
                }
            }

            return IncreaseScoreType.Ignore;
        }

        internal override void Arm(bool isHit)
        {
            if (isHit)
            {
                SpriteCollection.ForEach(s => s.Transformations.Clear());
                return;
            }

            base.Arm(false);
        }

        protected override int LocalPreEmpt
        {
            get
            {
                return (int)(600 / hitObjectManager.SliderVelocityAt(StartTime) / 0.6f * 1000);
            }
        }

        protected override float DefaultScale
        {
            get
            {
                return taikoLarge ? 1 : 0.65f;
            }
        }
    }
}
