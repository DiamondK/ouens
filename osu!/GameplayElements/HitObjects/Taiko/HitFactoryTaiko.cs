﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using osu.GameplayElements.HitObjects.Osu;
using osu.Audio;

namespace osu.GameplayElements.HitObjects.Taiko
{
    internal class HitFactoryTaiko : HitFactory
    {
        public HitFactoryTaiko(HitObjectManager hitObjectMananager)
            : base(hitObjectMananager)
        {
        }

        internal override HitCircle CreateHitCircle(Vector2 startPosition, int startTime, bool newCombo,
                                                    HitObjectSoundType soundType, int comboOffset, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            ControlPoint cp = hitObjectManager.Beatmap.controlPointAt(startTime + 5);
            bool kiai = cp != null ? cp.kiaiMode : false;
            return new HitCircleTaiko(hitObjectManager, startPosition, startTime, newCombo, soundType, kiai);
        }

        internal override Slider CreateSlider(Vector2 startPosition, int startTime, bool newCombo,
                            HitObjectSoundType soundType, CurveTypes curveType, int repeatCount, double sliderLength, List<Vector2> sliderPoints,
                            List<HitObjectSoundType> soundTypes, int comboOffset, SampleSet sampleSet, SampleSet addSet, List<SampleSet> sampleSets, List<SampleSet> sampleSetAdditions, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            ControlPoint cp = hitObjectManager.Beatmap.controlPointAt(startTime + 5);
            bool kiai = cp != null ? cp.kiaiMode : false;
            return new SliderTaiko(hitObjectManager, startPosition, startTime, newCombo, soundType, curveType, repeatCount, sliderLength, sliderPoints, soundTypes, kiai);
        }

        internal override Spinner CreateSpinner(int startTime, int endTime, HitObjectSoundType soundType, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            return new SpinnerTaiko(hitObjectManager, startTime, endTime, soundType);
        }

        internal override HitCircle CreateSpecial(Vector2 startPosition, int startTime, int endTime, bool newCombo,
                                                    HitObjectSoundType soundType, int comboOffset, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            return null;
        }
    }
}