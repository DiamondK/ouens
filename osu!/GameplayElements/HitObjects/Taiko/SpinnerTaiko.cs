﻿using System;
using Microsoft.Xna.Framework;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Scoring;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Sprites;
using osu.Input;
using osu_common;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics;
using System.Collections.Generic;
using osu_common.Helpers;

namespace osu.GameplayElements.HitObjects.Taiko
{
    internal class SpinnerTaiko : SpinnerOsu
    {
        private bool finished;
        private byte lastButton = 0; //none
        private const byte red_Button = 1;
        private const byte blue_Button = 2;
        private int lastHitTime;
        private int lastInvalidButtonTime;

        internal pSprite WarningIcon;

        internal int totalRotationRequirement;

        public SpinnerTaiko(HitObjectManager hom, int startTime, int endTime, HitObjectSoundType soundType)
            : base(hom, startTime, endTime, soundType)
        {
            rotationRequirement = (int)Math.Max(1, (rotationRequirement * 1.65f));
            totalRotationRequirement = rotationRequirement;

            if (ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.DoubleTime))
                rotationRequirement = Math.Max(1, (int)(rotationRequirement * 0.75f));
            if (ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.HalfTime))
                rotationRequirement = Math.Max(1, (int)(rotationRequirement * 1.5f));

            if (hitObjectManager.spriteManager != null)
            {
                spriteBonus.Transformations = new pList<Transformation>(spriteCircle.Transformations);
                spriteBonus.Text = (rotationRequirement + 1).ToString();

                WarningIcon =
                    new pSprite(SkinManager.Load("spinner-warning"), Fields.GamefieldWide, Origins.Centre,
                        Clocks.Audio, Position, SpriteManager.drawOrderBwd(StartTime + 1), false, Color.White);
                SpriteCollection.Add(WarningIcon);
                WarningIcon.TagNumeric = -5;
                DimCollection.Add(WarningIcon);

                WarningIcon.Transformations.Add(
                    new Transformation(TransformationType.Fade, 0, 1, startTime - hitObjectManager.PreEmpt,
                                           startTime - (int)(hitObjectManager.PreEmpt * 0.6)));
            }
            

            Kiai = false;
        }

        protected override Transformation fadeInTransformation
        {
            get
            {
                return new Transformation(TransformationType.Fade, 0, 1, StartTime - 1,
                                       StartTime + 200);
            }
        }

        internal override void InitializeSprites()
        {
            if (GameBase.GamefieldCorrectionOffsetActive)
                SPINNER_TOP -= 16;

            Color fade = (GameBase.Mode == OsuModes.Play &&
                          (ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.SpunOut) || Player.Relaxing2)
                              ? Color.Gray
                              : Color.White);

            spriteCircle =
                new pSprite(SkinManager.Load(@"spinner-circle", SkinSource.Osu | SkinSource.Skin), Fields.TopLeft,
                            Origins.Centre,
                            Clocks.Audio, new Vector2(320, SPINNER_TOP + 219), SpriteManager.drawOrderBwd(EndTime), false,
                            fade);
            SpriteCollection.Add(spriteCircle);

            if (!ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.Hidden))
            {
                pTexture highRes = SkinManager.Load(@"spinner-approachcircle", SkinSource.Skin);

                SpriteApproachCircle =
                new pSprite(SkinManager.Load(@"spinner-approachcircle", SkinSource.Osu | SkinSource.Skin), Fields.TopLeft, Origins.Centre,
                            Clocks.Audio, new Vector2(320, SPINNER_TOP + 219), SpriteManager.drawOrderBwd(EndTime - 2), false, fade);
                SpriteCollection.Add(SpriteApproachCircle);
            }

            spriteBonus = new pSpriteText("", SkinManager.Current.FontScore, SkinManager.Current.FontScoreOverlap,
                              Fields.TopLeft, Origins.Centre,
                              Clocks.Audio, new Vector2(320, SPINNER_TOP + 299),
                              SpriteManager.drawOrderBwd(EndTime - 3), false,
                              fade);
            SpriteCollection.Add(spriteBonus);

            spriteCircle.Scale *= 0.8f;

        }

        protected override void InitializeSpritesNoFadeIn()
        {

        }

        protected override void UpdateDraw()
        {
            base.UpdateDraw();

            if (SpriteApproachCircle != null)
            {
                SpriteApproachCircle.Scale *= 0.8f;
                SpriteApproachCircle.Transformations.ForEach(t => { t.StartFloat *= 0.8f; t.EndFloat *= 0.8f; });
            }
        }

        internal override void ModifyPosition(Vector2 newPosition)
        {
            WarningIcon.Position = newPosition;
            base.ModifyPosition(newPosition);
        }

        internal override IncreaseScoreType Hit()
        {
            IsHit = true;
            velocityCurrent = 0;
            StopSound();

            if (rotationRequirement < 0)
            {
                AudioEngine.PlaySample(@"spinner-osu", AudioEngine.VolumeSample, SkinSource.All);

                if (SpriteApproachCircle != null) SpriteApproachCircle.Transformations.Clear();

                spriteCircle.Transformations.RemoveAll(t => t.Type == TransformationType.Scale);
                spriteCircle.Transformations.Add(new Transformation(TransformationType.Scale, spriteCircle.Scale, spriteCircle.Scale + 0.05f, AudioEngine.Time, AudioEngine.Time + 300, EasingTypes.Out));

                SpriteCollection.ForEach(s => s.FadeOut(300));

                EndTime = AudioEngine.Time;
                return IncreaseScoreType.TaikoDenDenComplete;
            }

            return IncreaseScoreType.MissHpOnlyNoCombo;
        }

        internal override bool HitTest(Vector2 testPosition, bool hittableRangeOnly, float radius)
        {
            return ((!hittableRangeOnly && IsVisible) ||
                  (StartTime - hitObjectManager.PreEmpt <= AudioEngine.Time &&
                   StartTime + hitObjectManager.HitWindow50 >= AudioEngine.Time && !IsHit));
        }

        internal override void Draw()
        {
            base.Draw();
        }

        internal override IncreaseScoreType GetScorePoints(Vector2 currentMousePos)
        {
            if (AudioEngine.Time < StartTime || finished) return IncreaseScoreType.Ignore;

            bool increase = false;
            bool hitRed = InputManager.leftButton1i || InputManager.leftButton2i;
            bool hitBlue = InputManager.rightButton1i || InputManager.rightButton2i;

            // you dont have to alternate in relaxmode
            if (Player.Relaxing && (hitRed || hitBlue))
                increase = true;

            // punish if less then 30 ms has passed since the last invalid button
            // ignore if we are in relaxmode
            if ((AudioEngine.Time - lastInvalidButtonTime) >= 30 && !Player.Relaxing)
            {
                // Only increase if you alternate.
                // We start with lastButton=0 so the user can choose
                // the first note to begin to alternate with.
                if (hitRed)
                {
                    if (lastButton != red_Button)
                    {
                        increase = true;
                        lastButton = red_Button;
                    }
                    else
                    {
                        lastInvalidButtonTime = AudioEngine.Time;
                        return IncreaseScoreType.Ignore;
                    }
                }
                else if (hitBlue)
                {
                    if (lastButton != blue_Button)
                    {
                        increase = true;
                        lastButton = blue_Button;
                    }
                    else
                    {
                        lastInvalidButtonTime = AudioEngine.Time;
                        return IncreaseScoreType.Ignore;
                    }
                }

            }

            if (increase)
            {
                lastHitTime = AudioEngine.Time;
                velocityTheoretical += 0.06 * (1d - 2 / 4d);
                rotationRequirement--;
                spriteBonus.Text = (rotationRequirement + 1).ToString();

                spriteCircle.Transformations.RemoveAll(t => t.Type == TransformationType.Scale);
                spriteCircle.Transformations.Add(new Transformation(TransformationType.Scale, Math.Min(0.94f, spriteCircle.Scale + 0.02f), 0.8f, AudioEngine.Time,
                                                                   AudioEngine.Time + 400, EasingTypes.Out));

                spriteBonus.Transformations.RemoveAll(t => t.Type == TransformationType.Scale);
                spriteBonus.Transformations.Add(new Transformation(TransformationType.Scale, spriteBonus.Scale, 1.6f - 0.6f * (float)rotationRequirement / totalRotationRequirement, AudioEngine.Time,
                                                                   AudioEngine.Time + 60, EasingTypes.Out));
            }

            if (rotationRequirement < 0 && !finished)
            {
                EndTime = AudioEngine.Time;
                finished = true;
            }


            //velocityTheoretical *= 0.99f;
            // 0.99 ^ (2000/60) = 0,71533 ....
            velocityTheoretical *= Math.Pow(0.88, GameBase.FrameRatio);

            return increase ? IncreaseScoreType.TaikoDenDenHit : IncreaseScoreType.Ignore;
        }

        protected override int LocalPreEmpt
        {
            get
            {
                return (int)(600 / hitObjectManager.SliderVelocityAt(StartTime) / 0.6f * 1000);
            }
        }
    }
}