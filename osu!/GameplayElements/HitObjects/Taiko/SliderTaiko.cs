﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects.Osu;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input.Handlers;

namespace osu.GameplayElements.HitObjects.Taiko
{
    internal class SliderTaiko : SliderOsu
    {
        private pSpriteText counter;
        private int hitCount;
        private float hitrate;
        private float prevHits;
        private float prevHitsSmooth;
        private pSprite taikoEnd;
        private pSprite taikoMiddle;
        private bool endpointHittable = true;

        bool taikoLarge { get { return SoundType.IsType(HitObjectSoundType.Finish); } }

        internal SliderTaiko(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType,
                           CurveTypes curveType, int repeatCount, double sliderLength, List<Vector2> sliderPoints, List<HitObjectSoundType> soundTypes, bool kiai)
            : base(hom, startPosition, startTime, newCombo, soundType, curveType, repeatCount, sliderLength, sliderPoints, soundTypes, 0)
        {
            base.SetColour(new Color(252, 184, 6));
            Kiai = kiai;
        }

        internal override bool StartIsHit
        {
            get { return true; }
        }

        public override Vector2 EndPosition
        {
            get { return Position; }
            set { }
        }

        public override int ComboNumber
        {
            get { return -1; }
            set { base.ComboNumber = -1; }
        }

        internal override List<pSprite> KiaiSprites
        {
            get
            {
                return new List<pSprite>() { sliderStartCircle.SpriteCollection[0], taikoMiddle, taikoEnd };
            }
        }

        private double minHitDelay;
        public double MinHitDelay
        {
            get
            {
                if (minHitDelay == 0)
                {
                    double maxRate;
                    if (hitObjectManager.Beatmap.BeatmapVersion >= 8)
                    {
                        if ((hitObjectManager.Beatmap.DifficultySliderTickRate == 3) ||
                            (hitObjectManager.Beatmap.DifficultySliderTickRate == 6) ||
                            (hitObjectManager.Beatmap.DifficultySliderTickRate == 1.5d))
                            maxRate = AudioEngine.beatLengthAt(StartTime, false) / 6;
                        else
                            maxRate = AudioEngine.beatLengthAt(StartTime, false) / 8;

                    }
                    else
                        maxRate = AudioEngine.beatLengthAt(StartTime) / 8;

                    while (maxRate < 60)
                        maxRate *= 2;
                    while (maxRate > 120)
                        maxRate /= 2;
                    minHitDelay = maxRate;
                }

                return minHitDelay;
            }
        }

        internal override bool HitTest(Vector2 testPosition, bool hittableRangeOnly, float radius)
        {
            return HittableStartTime <= AudioEngine.Time && HittableEndTime >= AudioEngine.Time && !IsHit;
        }

        internal override IncreaseScoreType HitStart()
        {
            return IncreaseScoreType.Ignore;
        }

        internal override void SetColour(Color color)
        {
        }

        internal override IncreaseScoreType Hit()
        {
            IsHit = true;
            return IncreaseScoreType.Ignore;
        }

        internal override int HittableEndTime
        {
            get
            {
                return endpointHittable ? base.HittableEndTime + (int)MinHitDelay : base.HittableEndTime;
            }
        }

        internal override int HittableStartTime
        {
            get
            {
                return base.HittableStartTime - (int)MinHitDelay;
            }
        }

        internal override void Draw()
        {
        }

        internal override void Update()
        {
            if (GameBase.SixtyFramesPerSecondFrame)
            {
                if (prevHitsSmooth < prevHits)
                    prevHitsSmooth += 0.4f;
                else
                    prevHitsSmooth = prevHits;

                hitrate = Math.Max(0, 1 - (prevHitsSmooth / 10));

                Color c = new Color(252, (byte)(184 * hitrate), (byte)(6 * hitrate));
                if (Colour != c)
                {
                    sliderStartCircle.SetColour(Colour);
                    Colour = c;
                    taikoMiddle.InitialColour = c;
                    taikoEnd.InitialColour = c;
                }

                prevHits = Math.Max(0, prevHits - 0.2f);
            }

            base.Update();

            sliderBall.FlipHorizontal = false;
        }

        internal override void Shake()
        {
        }

        internal override void UpdateCalculations(bool force = true)
        {
            //we need a more advanced update function so shouldn't be overriding this one.
        }

        protected override void addStartCircle()
        {
            sliderStartCircle = (HitCircleOsu)hitObjectManager.hitFactory.CreateHitCircle(Position, StartTime, false,
                                SoundType, 0, SampleSet.None, SampleSet.None, CustomSampleSet.Default, 0, "");
            sliderStartCircle.SetColour(Colour);
        }


        internal void UpdateCalculations(bool force, double multiplier)
        {
            if (force || taikoMiddle == null)
            {
                //slider endpoint is not hittable when the next hitobject has
                //a starttime before the slider's endtime.
                List<HitObject> hitobjects = hitObjectManager.hitObjects;
                int ind = hitobjects.IndexOf(this);
                if (ind < hitobjects.Count - 1 &&
                    hitobjects[ind + 1].HittableStartTime - (EndTime + (int)MinHitDelay) <= (int)MinHitDelay)
                    endpointHittable = false;

                float scalefactor = !SoundType.IsType(HitObjectSoundType.Finish) ? 0.65f : 1;

                SpatialLength *= SegmentCount;
                double v = hitObjectManager.SliderScoringPointDistance * hitObjectManager.Beatmap.DifficultySliderTickRate;
                double b = hitObjectManager.Beatmap.beatLengthAt(StartTime);
                EndTime = StartTime + (int)(SpatialLength / v * b);

                SpatialLength *= multiplier;

                taikoMiddle = new pSprite(SkinManager.Load(@"taiko-roll-middle"), Fields.GamefieldWide,
                                          Origins.TopLeft, Clocks.Audio,
                                          new Vector2(Position.X,
                                                      Position.Y - hitObjectManager.HitObjectRadius * scalefactor),
                                          SpriteManager.drawOrderBwd(EndTime + 10),
                                          false, new Color(252, 184, 6));
                taikoMiddle.VectorScale =
                    new Vector2((float)SpatialLength * GameBase.WindowRatio / hitObjectManager.SpriteRatio * (1 / scalefactor), 1);
                taikoMiddle.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1,
                                                                   StartTime - hitObjectManager.PreEmpt, StartTime));
                taikoMiddle.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, EndTime,
                                                                   EndTime + hitObjectManager.PreEmptSliderComplete));
                spriteManager.Add(taikoMiddle);
                SpriteCollection.Add(taikoMiddle);
                DimCollection.Add(taikoMiddle);

                taikoEnd = new pSprite(SkinManager.Load(@"taiko-roll-end"), Fields.GamefieldWide, Origins.TopLeft,
                                       Clocks.Audio,
                                       new Vector2(Position.X, Position.Y - hitObjectManager.HitObjectRadius * scalefactor),
                                       SpriteManager.drawOrderBwd(EndTime + 8),
                                       false, new Color(252, 184, 6));
                taikoEnd.OriginPosition =
                    new Vector2(
                        (float)-SpatialLength * GameBase.WindowRatio / hitObjectManager.SpriteRatio * (1 / scalefactor), 0);
                taikoEnd.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1,
                                                                StartTime - hitObjectManager.PreEmpt, StartTime));
                taikoEnd.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, EndTime,
                                                                EndTime + hitObjectManager.PreEmptSliderComplete));

                spriteManager.Add(taikoEnd);
                SpriteCollection.Add(taikoEnd);
                DimCollection.Add(taikoEnd);

                sliderBall.Transformations.Add(new Transformation(sliderStartCircle.Position,
                                                                  sliderStartCircle.Position, StartTime - 500, EndTime));
                sliderFollower.Transformations.Add(new Transformation(sliderStartCircle.Position,
                                                                      sliderStartCircle.Position, StartTime, EndTime));

                counter =
                    new pSpriteText("", SkinManager.Current.FontScore, SkinManager.Current.FontScoreOverlap,
                                    Fields.GamefieldWide, Origins.Centre, Clocks.Audio,
                                    HitObjectManagerTaiko.HIT_LOCATION, 0.9f, false, Color.White);
                counter.Scale = 0.8f;
                counter.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, StartTime, StartTime));
                counter.Transformations.Add(new Transformation(TransformationType.Fade, 0.6f, 0, EndTime, EndTime + 300));
                spriteManager.Add(counter);

                int index = 0;

                for (double i = StartTime; i < HittableEndTime; i += MinHitDelay)
                {
                    if (i == StartTime)
                    {
                        hittablePoints.Add(new HitPoint((int)i, null));
                        index++;
                        continue;
                    }

                    bool major = ((hitObjectManager.Beatmap.DifficultySliderTickRate == 3) ||
                                  (hitObjectManager.Beatmap.DifficultySliderTickRate == 6) ||
                                  (hitObjectManager.Beatmap.DifficultySliderTickRate == 1.5d)) ? (index % 3) == 0 : (index % 4) == 0;

                    pSprite scoringDot = new pSprite(SkinManager.Load(@"sliderscorepoint"),
                                                        Fields.GamefieldWide, Origins.Centre, Clocks.Audio, new Vector2(Position.X,
                                                      Position.Y),
                                                        SpriteManager.drawOrderBwd(StartTime + 1), false, major ? Color.Yellow : Color.White, this);

                    if (!major)
                        scoringDot.Scale *= 0.8f;


                    scoringDot.OriginPosition.X -= (float)(SpatialLength * (float)(i - StartTime) / Length) * GameBase.WindowRatio / hitObjectManager.SpriteRatio / scoringDot.Scale * (1 / scalefactor);

                    scoringDot.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1,
                                                                StartTime - hitObjectManager.PreEmpt, StartTime));
                    scoringDot.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, EndTime,
                                                                    EndTime + hitObjectManager.PreEmptSliderComplete));

                    hittablePoints.Add(new HitPoint((int)i, scoringDot));

                    spriteManager.Add(scoringDot);
                    SpriteCollection.Add(scoringDot);
                    DimCollection.Add(scoringDot);

                    index++;
                }

                if (scalefactor != 1)
                    foreach (pSprite p in SpriteCollection)
                        p.Scale *= scalefactor;

                for (int i = 0; i < sliderStartCircle.SpriteCollection.Count; i++)
                {
                    pSprite s = sliderStartCircle.SpriteCollection[i];
                    foreach (Transformation t in s.Transformations)
                    {
                        if (t.Type ==
                            TransformationType.Fade &&
                            t.StartFloat == 1)
                        {
                            t.Time1 = EndTime;
                            t.Time2 = EndTime;
                        }
                    }
                    SpriteCollection.Add(s);
                    DimCollection.Add(s);
                    spriteManager.Add(s);
                }
            }
        }

        double lastHit;
        internal bool LastHitSuccessful;
        internal readonly List<HitPoint> hittablePoints = new List<HitPoint>();


        internal override IncreaseScoreType GetScorePoints(Vector2 currentMousePos)
        {
            if (IsHit)
                return IncreaseScoreType.Ignore;

            bool allow = MouseManager.GameClickRegistered;
            //allow &= AudioEngine.Time >= StartTime;

            if (!allow) return IncreaseScoreType.Ignore;

            double maxRate = MinHitDelay;
            if (!Player.Relaxing)
            {
                if (lastHit == 0)
                    lastHit = StartTime;
                else if (Math.Abs(AudioEngine.Time - (lastHit + maxRate)) < maxRate / 2)
                    lastHit += maxRate;
                else if (Math.Abs(AudioEngine.Time - (lastHit + maxRate * 2)) < maxRate)
                    lastHit += maxRate * 2;
                else if (Math.Abs(AudioEngine.Time - (lastHit + maxRate * 4)) < maxRate)
                    lastHit += maxRate * 4;
                else
                    allow = false;
            }

            LastHitSuccessful = allow;

            if (!allow)
            {
                double lastHitNew = StartTime;
                while (lastHitNew < AudioEngine.Time)
                    lastHitNew += maxRate * 2;

                lastHit = (int)lastHitNew;

                return IncreaseScoreType.Ignore;
            }

            //Find a consumable hit point...
            HitPoint p = hittablePoints.Find(hp => Math.Abs(hp.time - AudioEngine.Time) < MinHitDelay / 2 && !hp.consumed);

            if (p == null) return IncreaseScoreType.Ignore;

            p.ConsumeMe();

            hitCount++;
            counter.Text = hitCount.ToString();
            if (AudioEngine.Time < EndTime)
            {
                counter.Transformations.RemoveAll(t => t.TagNumeric == 1);
                Transformation tr = new Transformation(TransformationType.Fade, 1, 0.6f, AudioEngine.Time,
                                                       AudioEngine.Time + 300);
                tr.TagNumeric = 1;
                counter.Transformations.Add(tr);
            }

            prevHits += 2;
            if (taikoLarge)
                return IncreaseScoreType.TaikoDrumRoll | IncreaseScoreType.TaikoLargeHitBoth;
            return IncreaseScoreType.TaikoDrumRoll;
        }

        protected override int LocalPreEmpt
        {
            get
            {
                return (int)(600 / hitObjectManager.SliderVelocityAt(StartTime) / 0.6f * 1000);
            }
        }

        internal class HitPoint
        {
            internal readonly int time;
            internal readonly pSprite sprite;
            internal bool consumed;

            internal HitPoint(int time, pSprite sprite)
            {
                this.time = time;
                this.sprite = sprite;
            }

            internal void ConsumeMe()
            {
                consumed = true;
                if (sprite != null)
                {
                    sprite.FadeOut(40);
                }
            }
        }
    }
}