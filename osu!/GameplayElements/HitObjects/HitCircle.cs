﻿using System;
using osu.Audio;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Beatmaps;

namespace osu.GameplayElements.HitObjects
{
    internal abstract class HitCircle : HitObject
    {
        protected HitCircle(HitObjectManager hom)
            : base(hom)
        {

        }

        internal IncreaseScoreType hitValue;

        internal override IncreaseScoreType Hit()
        {
            IsHit = true;
            int hitTime = AudioEngine.Time;
            int accuracy = Math.Abs(hitTime - StartTime);

            if (accuracy < hitObjectManager.HitWindow300)
                hitValue = IncreaseScoreType.Hit300;
            else if (accuracy < hitObjectManager.HitWindow100)
                hitValue = IncreaseScoreType.Hit100;
            else if (accuracy < hitObjectManager.HitWindow50)
                hitValue = IncreaseScoreType.Hit50;
            else
                hitValue = IncreaseScoreType.Miss;

            if (hitValue > 0)
                PlaySound();

            Arm(hitValue > 0);

            return hitValue;
        }
    }
}