﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using osu.Audio;
using osu.GameplayElements.Scoring;
using osu_common;

namespace osu.GameplayElements.HitObjects.Osu
{
    internal class HitFactoryOsu : HitFactory
    {
        public HitFactoryOsu(HitObjectManager hitObjectMananager)
            : base(hitObjectMananager)
        {
        }

        internal override HitCircle CreateHitCircle(Vector2 startPosition, int startTime, bool newCombo,
                                                    HitObjectSoundType soundType, int comboOffset, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            HitCircleOsu c = new HitCircleOsu(hitObjectManager, startPosition, startTime, newCombo, soundType, comboOffset);
            c.SampleSet = sampleSet;
            c.SampleSetAdditions = addSet;
            c.CustomSampleSet = customSampleSet;
            c.SampleVolume = volume;
            c.ProcessSampleFile(sampleFile);
            return c;
        }

        internal override Slider CreateSlider(Vector2 startPosition, int startTime, bool newCombo,
                                              HitObjectSoundType soundType, CurveTypes curveType, int repeatCount, double sliderLength, List<Vector2> sliderPoints, List<HitObjectSoundType> soundTypes, int comboOffset,
            SampleSet sampleSet, SampleSet addSet, List<SampleSet> sampleSets, List<SampleSet> sampleSetAdditions, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            SliderOsu s = new SliderOsu(hitObjectManager, startPosition, startTime, newCombo, soundType, curveType, repeatCount, sliderLength, sliderPoints, soundTypes, comboOffset);
            s.SampleSet = sampleSet;
            s.SampleSetAdditions = addSet;

            s.SampleSetList = sampleSets;
            s.SampleSetAdditionList = sampleSetAdditions;
            s.CustomSampleSet = customSampleSet;
            s.SampleVolume = volume;
            s.ProcessSampleFile(sampleFile);

            if (s.sliderStartCircle != null)
            {
                HitSoundInfo hitsoundInfo = s.GetHitSoundInfo(0);
                s.sliderStartCircle.SampleSet = hitsoundInfo.SampleSet;
                s.sliderStartCircle.SampleSetAdditions = hitsoundInfo.SampleSetAdditions;
            }
            
            return s;
        }

        internal override Spinner CreateSpinner(int startTime, int endTime, HitObjectSoundType soundType, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            SpinnerOsu p = new SpinnerOsu(hitObjectManager, startTime, endTime, soundType);
            p.SampleSet = sampleSet;
            p.SampleSetAdditions = addSet;
            p.CustomSampleSet = customSampleSet;
            p.SampleVolume = volume;
            p.ProcessSampleFile(sampleFile);
            return p;
        }

        internal override HitCircle CreateSpecial(Vector2 startPosition, int startTime, int endTime, bool newCombo,
                                                    HitObjectSoundType soundType, int comboOffset, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            HitCircleHold h = new HitCircleHold(hitObjectManager, startPosition, startTime, newCombo, (soundType & HitObjectSoundType.Whistle) > 0, (soundType & HitObjectSoundType.Finish) > 0,
                                                    (soundType & HitObjectSoundType.Clap) > 0, 0);
            h.SampleSet = sampleSet;
            h.SampleSetAdditions = addSet;
            h.CustomSampleSet = customSampleSet;
            h.EndTime = endTime;
            h.SampleVolume = volume;
            h.ProcessSampleFile(sampleFile);
            return h;
        }
    }
}