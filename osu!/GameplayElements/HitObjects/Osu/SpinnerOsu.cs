﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Scoring;
using osu.GameplayElements.Beatmaps;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu.Input;
using osu.Input.Handlers;
using osu_common;
using Un4seen.Bass;

namespace osu.GameplayElements.HitObjects.Osu
{
    internal class SpinnerOsu : Spinner
    {
        protected pSprite SpriteApproachCircle;

        bool newStyleSpinner;

        protected pSprite spriteBackground;
        protected pSprite SpriteClear;
        protected pSprite spriteRpmBackground;
        protected pSpriteText spriteRpmText;
        protected pSprite spriteScoreMetre;
        protected pSprite SpriteSpin;
        protected Transformation circleRotation;
        private int framecount;
        private double lastMouseAngle;
        private int lastRotationCount;
        private double maxAccel;
        private bool passed;
        internal int rotationCount;
        internal int rotationRequirement;
        private double rpm;
        private int scoringRotationCount;
        private bool spinstarted;
        protected pSpriteText spriteBonus;
        protected pSprite spriteCircle;
        protected pSprite spriteGlow;
        protected pSprite spriteCircle2;
        internal double velocityCurrent;
        protected double velocityTheoretical;
        private int zeroCount;
        private const int SPINNER_CIRCLE_WIDTH = 666;
        protected int SPINNER_TOP = 45;

        private pSprite spriteMiddle;
        private pSprite spriteMiddle2;

        internal SpinnerOsu(HitObjectManager hom, int startTime, int endTime, HitObjectSoundType soundType)
            : base(hom, startTime, endTime, soundType)
        {
            if (hom.spriteManager != null)
            {
                InitializeSprites();
            }

            UpdateDraw();

            if (hom.spriteManager != null)
            {
                InitializeSpritesNoFadeIn();
            }

            recordMove = true;
            movePoint = new List<float>(MOVE_SAMPLE_COUNT);
        }

        protected virtual void InitializeSpritesNoFadeIn()
        {
            if (GameBase.Mode == OsuModes.Play)
            {
                SpriteSpin = new pSprite(SkinManager.Load(@"spinner-spin"), Fields.TopLeft, Origins.Centre,
                             Clocks.Audio, new Vector2(320, SPINNER_TOP + 335),
                             SpriteManager.drawOrderFwdLowPrio(StartTime + 6), false, Color.White);
                SpriteSpin.Transformations.Add(
                    new Transformation(TransformationType.Fade, 0, 1, StartTime - HitObjectManager.FadeIn / 2,
                                       StartTime));
                SpriteSpin.Transformations.Add(
                    new Transformation(TransformationType.Fade, 1, 0, EndTime - Math.Min(400, EndTime - StartTime),
                                       EndTime));
                SpriteCollection.Add(SpriteSpin);

                SpriteClear = new pSprite(SkinManager.Load(@"spinner-clear"), Fields.TopLeft, Origins.Centre,
                                          Clocks.Audio, new Vector2(320, SPINNER_TOP + 115),
                                          SpriteManager.drawOrderFwdLowPrio(StartTime + 7), false, Color.White);
                SpriteClear.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0, StartTime, EndTime));
                SpriteCollection.Add(SpriteClear);
            }

            if (newStyleSpinner)
            {
                spriteGlow = new pSprite(SkinManager.Load(@"spinner-glow"), Fields.TopLeft, Origins.Centre, Clocks.Audio, new Vector2(320, SPINNER_TOP + 219), SpriteManager.drawOrderFwdLowPrio(StartTime - 1), false, Color.TransparentWhite);
                spriteGlow.Additive = true;
                spriteGlow.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0, StartTime, EndTime));
                SpriteCollection.Add(spriteGlow);

                spriteMiddle.Scale = spriteMiddle2.Scale = spriteCircle.Scale = spriteCircle2.Scale = spriteGlow.Scale = 0.8f;
            }
        }

        internal virtual void InitializeSprites()
        {
            if (GameBase.GamefieldCorrectionOffsetActive)
                SPINNER_TOP -= 16;

            Color fade = (GameBase.Mode == OsuModes.Play && Player.currentScore != null &&
                          (ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.SpunOut) || Player.Relaxing2)
                              ? Color.Gray
                              : Color.White);

            if (GameBase.Mode == OsuModes.Play && SkinManager.Current.SpinnerFadePlayfield)
            {
                pSprite black = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft,
                Clocks.Audio, new Vector2(0, 0), SpriteManager.drawOrderFwdLowPrio(StartTime - 1), false, Color.Black);

                black.Scale = 1.6f;

                black.VectorScale = new Vector2(640, SPINNER_TOP);

                SpriteCollection.Add(black);

                if (GameBase.GamefieldCorrectionOffsetActive)
                {
                    black = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.TopLeft,
    Clocks.Audio, new Vector2(0, 480 - 19), SpriteManager.drawOrderFwdLowPrio(StartTime - 1), false, Color.Black);

                    black.Scale = 1.6f;

                    black.VectorScale = new Vector2(640, 19);
                    //Use 19 here instead of 16 as the spinner-background graphic seems a little short...

                    SpriteCollection.Add(black);
                }
            }

            newStyleSpinner = SkinManager.UseNewLayout && (SkinManager.IgnoreBeatmapSkin || SkinManager.Load(@"spinner-circle", SkinSource.Beatmap) == null) && (SkinManager.Load(@"spinner-background", SkinSource.Skin) == null);

            if (!newStyleSpinner)
            {
                pTexture bgTexture = null;
                if (!SkinManager.IgnoreBeatmapSkin)
                    bgTexture = SkinManager.Load(@"spinner-background.jpg", SkinSource.Beatmap);
                if (bgTexture == null)
                    bgTexture = SkinManager.Load(@"spinner-background", SkinSource.Beatmap | SkinSource.Skin);

                //Check for a jpg background for beatmap-based skins (used to reduce filesize), then fallback to png.
                spriteBackground =
                    new pSprite(bgTexture, Fields.TopLeft,
                                Origins.Centre,
                                Clocks.Audio, new Vector2(320, SPINNER_TOP + 219), SpriteManager.drawOrderFwdLowPrio(StartTime - 1),
                                false,
                                SkinManager.Colours.ContainsKey("SpinnerBackground") ? SkinManager.Colours["SpinnerBackground"] : new Color(100, 100, 100, 255));
                SpriteCollection.Add(spriteBackground);
            }

            if (newStyleSpinner)
            {
                spriteCircle =
                    new pSprite(SkinManager.Load(@"spinner-top"), Fields.TopLeft,
                                Origins.Centre,
                                Clocks.Audio, new Vector2(320, SPINNER_TOP + 219), SpriteManager.drawOrderFwdLowPrio(StartTime + 1), false,
                                fade);
                SpriteCollection.Add(spriteCircle);

                spriteCircle2 =
                    new pSprite(SkinManager.Load(@"spinner-bottom"), Fields.TopLeft,
                                Origins.Centre,
                                Clocks.Audio, new Vector2(320, SPINNER_TOP + 219), SpriteManager.drawOrderFwdLowPrio(StartTime), false,
                                fade);
                SpriteCollection.Add(spriteCircle2);

                spriteMiddle = new pSprite(SkinManager.Load(@"spinner-middle"), Fields.TopLeft, Origins.Centre, Clocks.Audio, new Vector2(320, SPINNER_TOP + 219), SpriteManager.drawOrderFwdLowPrio(StartTime + 3), false, Color.TransparentWhite);
                SpriteCollection.Add(spriteMiddle);

                spriteMiddle2 = new pSprite(SkinManager.Load(@"spinner-middle2"), Fields.TopLeft, Origins.Centre, Clocks.Audio, new Vector2(320, SPINNER_TOP + 219), SpriteManager.drawOrderFwdLowPrio(StartTime + 2), false, Color.TransparentWhite);
                SpriteCollection.Add(spriteMiddle2);
            }
            else
            {
                spriteCircle =
                    new pSprite(SkinManager.Load(@"spinner-circle"), Fields.TopLeft,
                                Origins.Centre,
                                Clocks.Audio, new Vector2(320, SPINNER_TOP + 219), SpriteManager.drawOrderFwdLowPrio(StartTime), false,
                                fade);
                SpriteCollection.Add(spriteCircle);
            }


            if (!newStyleSpinner)
            {
                spriteScoreMetre =
                    new pSprite(SkinManager.Load(@"spinner-metre"), Fields.TopLeft,
                                Origins.TopLeft,
                                Clocks.Audio, new Vector2(0, SPINNER_TOP), SpriteManager.drawOrderFwdLowPrio(StartTime + 1),
                                false,
                                fade);
                spriteScoreMetre.DrawHeight = 0;
                SpriteCollection.Add(spriteScoreMetre);
            }

            if (GameBase.Mode == OsuModes.Play)
            {
                spriteRpmBackground = new pSprite(SkinManager.Load(@"spinner-rpm"), Fields.TopLeft,
                                                  Origins.TopLeft,
                                                  Clocks.Audio,
                                                  new Vector2(233, 445), SpriteManager.drawOrderFwdLowPrio(StartTime + 3),
                                                  false,
                                                  fade);

                spriteRpmText = new pSpriteText("", SkinManager.Current.FontScore, SkinManager.Current.FontScoreOverlap,
                                                Fields.TopLeft, Origins.TopRight,
                                                Clocks.Audio, new Vector2(400, 448),
                                                SpriteManager.drawOrderFwdLowPrio(StartTime + 4), false,
                                                fade);
                spriteRpmText.Scale = 0.9f;

                SpriteCollection.Add(spriteRpmText);
                SpriteCollection.Add(spriteRpmBackground);
            }

            if (spriteCircle.Texture.Source != SkinSource.Osu && (!ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.Hidden)))
            {
                SpriteApproachCircle =
                new pSprite(SkinManager.Load(@"spinner-approachcircle"), Fields.TopLeft, Origins.Centre,
                            Clocks.Audio, new Vector2(320, SPINNER_TOP + 219), SpriteManager.drawOrderFwdLowPrio(StartTime + 2), false, fade);
                SpriteCollection.Add(SpriteApproachCircle);
            }

            spriteBonus = new pSpriteText("", SkinManager.Current.FontScore, SkinManager.Current.FontScoreOverlap,
                                          Fields.TopLeft, Origins.Centre,
                                          Clocks.Audio, new Vector2(320, SPINNER_TOP + 299),
                                          SpriteManager.drawOrderFwdLowPrio(StartTime + 3), false,
                                          fade);
            if (spriteCircle.Texture.Source == SkinSource.Osu)
                spriteBonus.Additive = true;

            SpriteCollection.Add(spriteBonus);
        }

        public override int ComboNumber
        {
            get { return 1; }
            set { }
        }

        public override Vector2 EndPosition
        {
            get { return Position; }
            set { Position = value; }
        }

        internal override bool IsVisible
        {
            get
            {
                return AudioEngine.Time >= StartTime - HitObjectManager.FadeIn &&
                       AudioEngine.Time <= EndTime;
            }
        }

        internal override void SetEndTime(int time)
        {
            EndTime = Math.Max(StartTime, time);
            UpdateDraw();
        }

        protected virtual Transformation fadeInTransformation
        {
            get
            {
                return new Transformation(TransformationType.Fade, 0, 1, StartTime - HitObjectManager.FadeIn,
                                       StartTime);
            }
        }

        protected virtual void UpdateDraw()
        {
            foreach (pSprite p in SpriteCollection)
            {
                if (p.TagNumeric == -5) continue;

                p.Transformations.Clear();
                p.Transformations.Add(fadeInTransformation);
                p.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, EndTime, EndTime + HitObjectManager.FadeOut));
            }

            if (SpriteApproachCircle != null)
                SpriteApproachCircle.Transformations.Add(
                    new Transformation(TransformationType.Scale, 1.86f, 0.1F, StartTime, EndTime));

            circleRotation = new Transformation(TransformationType.Rotation, 0, 0, StartTime, EndTime);
            if (spriteCircle != null)
                spriteCircle.Transformations.Add(circleRotation);


            if (spriteRpmText != null) spriteRpmText.Transformations.Add(new Transformation(spriteRpmText.Position + new Vector2(0, 50),
                                                                 spriteRpmText.Position,
                                                                 StartTime - HitObjectManager.FadeIn, StartTime,
                                                                 EasingTypes.Out));
            if (spriteRpmBackground != null) spriteRpmBackground.Transformations.Add(
                new Transformation(spriteRpmBackground.Position + new Vector2(0, 50),
                                   spriteRpmBackground.Position, StartTime - HitObjectManager.FadeIn, StartTime,
                                   EasingTypes.Out));

            rotationCount = 0;
            rotationRequirement = (int)((float)Length / 1000 * hitObjectManager.SpinnerRotationRatio);
            maxAccel = 0.00008 + Math.Max(0, (5000 - (double)Length) / 1000 / 2000);
        }

        internal override void Shake()
        {
            return;
        }

        float turnRatio { get { return spriteMiddle2 != null && spriteMiddle2.Texture != null ? 0.5f : 1; } }

#if ARCADE
        const int RELAX_BONUS_ACCEL = 12;
        const int RELAX_BONUS_VELOCITY = 2;
#else
        const int RELAX_BONUS_ACCEL = 4;
        const int RELAX_BONUS_VELOCITY = 1;
#endif

        float rotationCountLast = 0;
        float floatRotationCount = 0;
        private float lastFloatRotationCount;

        internal override void Update()
        {
            if (AudioEngine.Time < EndTime && IsHit)
                Disarm();

            if (IsHit || (!InputManager.ScorableFrame) || AudioEngine.Time < StartTime)
                return;

            framecount++;

            //float rpm = (float)scoringRotationCount / (AudioEngine.Time - StartTime) * 60000;
            rpm = rpm * 0.9 + 0.1 * (Math.Abs(velocityCurrent) * GameBase.SIXTY_FRAME_TIME * 60) / (Math.PI * 2) * 60;

            if (spriteRpmText != null) spriteRpmText.Text = string.Format("{0:#,0}", rpm);

            if (Player.Instance != null && Player.Mode == PlayModes.Osu)
                Player.Instance.LogSpinSpeed((int)rpm);
            if (spriteMiddle != null)
                spriteMiddle.InitialColour = ColourHelper.ColourLerp(Color.White, Color.Red, (float)(AudioEngine.Time - StartTime) / Length);

            if (GameBase.Mode == OsuModes.Edit)
            {
                circleRotation.EndFloat = (float)(rotationRequirement * Math.PI);
                floatRotationCount = rotationCount = (int)((float)(AudioEngine.Time - StartTime) / 1000 * hitObjectManager.SpinnerRotationRatio);
            }
            else if (AudioEngine.Time < EndTime && AudioEngine.Time > StartTime && !Player.Recovering)
            {
                if (spriteCircle.Transformations.Contains(circleRotation))
                    spriteCircle.Transformations.Remove(circleRotation);

                double maxAccelPerSec = maxAccel * GameBase.SIXTY_FRAME_TIME;

                if ((GameBase.Mode == OsuModes.Play && ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.SpunOut)) || Player.Relaxing2)
                    velocityCurrent = 0.03;
                else if (velocityTheoretical > velocityCurrent)
                    velocityCurrent = velocityCurrent +
                                      Math.Min(velocityTheoretical * RELAX_BONUS_VELOCITY - velocityCurrent,
                                               velocityCurrent < 0
#if !ARCADE
 && Player.Relaxing
#endif
 ? maxAccelPerSec / RELAX_BONUS_ACCEL
                                                   : maxAccelPerSec);
                else
                    velocityCurrent = velocityCurrent +
                                      Math.Max(velocityTheoretical * RELAX_BONUS_VELOCITY - velocityCurrent,
                                               velocityCurrent > 0
#if !ARCADE
 && Player.Relaxing
#endif
 ? -maxAccelPerSec / RELAX_BONUS_ACCEL
                                                   : -maxAccelPerSec);


                velocityCurrent = Math.Max(-0.05, Math.Min(velocityCurrent, 0.05));

                spriteCircle.Rotation = spriteCircle.Rotation + (float)(velocityCurrent * turnRatio * GameBase.SIXTY_FRAME_TIME);
                if (spriteMiddle2 != null)
                {
                    spriteMiddle2.Rotation = spriteMiddle2.Rotation + (float)(velocityCurrent * GameBase.SIXTY_FRAME_TIME);
                    spriteCircle2.Rotation = spriteCircle.Rotation / 3f;
                }


                if (velocityCurrent != 0)
                    StartSound();
                else
                    StopSound();

                float newFloatRotationCount = 0;

                if (GameBase.Mode == OsuModes.Play && ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.DoubleTime))
                    newFloatRotationCount = (float)((spriteCircle.Rotation / turnRatio / Math.PI) * 1.5);
                else if (GameBase.Mode == OsuModes.Play &&
                         ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.HalfTime))
                    newFloatRotationCount = (float)((spriteCircle.Rotation / turnRatio / Math.PI) * 0.75);
                else
                    newFloatRotationCount = (float)(spriteCircle.Rotation / turnRatio / Math.PI);

                floatRotationCount += Math.Abs(newFloatRotationCount - lastFloatRotationCount);
                lastFloatRotationCount = newFloatRotationCount;
            }

            rotationCount = (int)floatRotationCount;

            SetScoreMeter(Math.Abs(floatRotationCount) / rotationRequirement * 100);

            if (scoringRotationCount >= rotationRequirement && !passed)
            {
                if (spriteGlow != null)
                {
                    spriteGlow.InitialColour = new Color(3, 151, 255);
                    //spriteGlow.FlashColour(Color.White, 250);
                }

                passed = true;
                if (SpriteSpin != null)
                {
                    SpriteSpin.FadeOut(100);
                    SpriteClear.Transformations.Clear();
                    SpriteClear.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, AudioEngine.Time,
                                                                       Math.Min(EndTime, AudioEngine.Time + 400),
                                                                       EasingTypes.Out));
                    SpriteClear.Transformations.Add(new Transformation(TransformationType.Scale, 2, 0.8f,
                                                                       AudioEngine.Time,
                                                                       Math.Min(EndTime, AudioEngine.Time + 240),
                                                                       EasingTypes.Out));
                    SpriteClear.Transformations.Add(new Transformation(TransformationType.Scale, 0.8f, 1,
                                                                       Math.Min(EndTime, AudioEngine.Time + 240),
                                                                       Math.Min(EndTime, AudioEngine.Time + 400),
                                                                       EasingTypes.None));
                    SpriteClear.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, EndTime - 50,
                                                                       EndTime));
                }
            }

            if (scoringRotationCount > 0 && !spinstarted)
            {
                if (SpriteSpin != null)
                {
                    if (AudioEngine.Time > StartTime + 500)
                    {
                        SpriteSpin.FadeOut(300);
                        spinstarted = true;
                    }
                }
            }
        }

        private void StartSound()
        {
            AudioEngine.PlaySpinSample(SkinManager.Current.SpinnerFrequencyModulate ? (float?)scoringRotationCount / rotationRequirement : null);
        }

        internal override void StopSound()
        {
            AudioEngine.StopSpinSample();
        }

        private void SetScoreMeter(float percent)
        {
            if (spriteGlow != null)
            {
                if (!passed)
                {
                    spriteGlow.InitialColour = new Color(3, 151, 255);// ColourHelper.ColourLerp(Color.Orange, new Color(3, 151, 255), percent / 100f);
                    if (spriteGlow.Transformations.Count > 0)
                    {
                        spriteGlow.Transformations[0].StartFloat = Math.Min(1, percent / 100f);
                        spriteGlow.Transformations[0].EndFloat = Math.Min(1, percent / 100f);
                    }

                    spriteMiddle.Scale = spriteMiddle2.Scale = spriteCircle.Scale = spriteCircle2.Scale = spriteGlow.Scale = 0.8f +
                        OsuMathHelper.easeOutVal(percent / 100f, 0, 0.2f, 1);
                }
            }

            percent = Math.Min(99, percent);

            int randomAmount = (int)percent % 10;
            int barCount = (int)percent / 10;


            if (SkinManager.Current.SpinnerNoBlink || GameBase.random.NextDouble() < (float)randomAmount / 10)
                barCount++;

            if (spriteScoreMetre != null)
            {
                spriteScoreMetre.DrawTop = (int)(69.2 * (10 - barCount));
                spriteScoreMetre.DrawHeight = (int)(69.2 * (barCount));
                spriteScoreMetre.Position.Y = (float)(SPINNER_TOP + 43.25 * (10 - barCount));
            }
        }

        internal override void SetColour(Color color)
        {
        }

        internal override IncreaseScoreType Hit()
        {
            StopSound();

            if (spriteGlow != null)
                spriteGlow.FadeOut(300);

            IsHit = true;
            IncreaseScoreType val = IncreaseScoreType.Miss;
            if (scoringRotationCount > rotationRequirement + 1)
                val = IncreaseScoreType.Hit300;
            else if (scoringRotationCount > rotationRequirement)
                val = IncreaseScoreType.Hit100;
            else if (scoringRotationCount > rotationRequirement - 1)
                val = IncreaseScoreType.Hit50;
            if (val > 0)
                PlaySound();
            return val;
        }

        internal override HitObject Clone()
        {
            HitObject s = new SpinnerOsu(hitObjectManager, StartTime, EndTime, SoundType);
            s.Selected = Selected;
            s.NewCombo = NewCombo;
            s.SampleSet = SampleSet;
            s.SampleSetAdditions = SampleSetAdditions;
            s.SampleVolume = SampleVolume;
            s.SampleAddr = SampleAddr;
            s.SampleFile = SampleFile;
            return s;
        }

        internal override void Select()
        {
            spriteCircle.InitialColour = Color.BlueViolet;
        }

        internal override void Deselect()
        {
            spriteCircle.InitialColour = Color.White;
        }

        internal override void ModifyTime(int newTime)
        {
            int diff = newTime - StartTime;
            StartTime += diff;
            SetEndTime(EndTime + diff);
        }

        internal override void ModifyPosition(Vector2 newPosition)
        {
            return;
        }

        internal override void Disarm()
        {
            base.Disarm();
        }

        internal override IncreaseScoreType GetScorePoints(Vector2 currentMousePos)
        {
            if (!InputManager.ScorableFrame)
                return 0;

            //if (InputManager.ReplayMode && !AudioEngine.IsReversed)
            //{
            //    spriteCircle.Transformations.Add(
            //        new Transformation(TransformationType.Rotation, spriteCircle.CurrentRotation, spriteCircle.CurrentRotation, AudioEngine.Time, AudioEngine.Time) { TagNumeric = HitObject.ARMED });
            //}

            Vector2 calc = currentMousePos - spriteCircle.drawPosition;
            double newMouseAngle = Math.Atan2(calc.Y, calc.X);
            float newDistance = calc.LengthSquared();

            double angleDiff = newMouseAngle - lastMouseAngle;
            AddMoveSample(newDistance);
            if (newMouseAngle - lastMouseAngle < -Math.PI)
                angleDiff = (2 * Math.PI) + newMouseAngle - lastMouseAngle;
            else if (lastMouseAngle - newMouseAngle < -Math.PI)
                angleDiff = (-2 * Math.PI) - lastMouseAngle + newMouseAngle;

            if (angleDiff == 0)
            {
                if (zeroCount++ < 1)
                    velocityTheoretical = velocityTheoretical / 3;
                else
                    velocityTheoretical = 0;
            }
            else
            {
                zeroCount = 0;

                if (!Player.Relaxing &&
                    (
#if !ARCADE
(InputManager.leftButton == ButtonState.Released && InputManager.rightButton == ButtonState.Released) ||
#endif
 AudioEngine.Time < StartTime ||
                    AudioEngine.Time > EndTime))
                    angleDiff = 0;

                if (Math.Abs(angleDiff) < Math.PI && GameBase.SixtyFramesPerSecondLength > 0)
                    velocityTheoretical = angleDiff / GameBase.SIXTY_FRAME_TIME;
                else
                    velocityTheoretical = 0;
            }

            lastMouseAngle = newMouseAngle;

            return GetActualScore();
        }

        internal IncreaseScoreType GetActualScore()
        {
            IncreaseScoreType score = IncreaseScoreType.Ignore;

            if (rotationCount != lastRotationCount)
            {
                scoringRotationCount++;
                if (SkinManager.Current.SpinnerFrequencyModulate)
                    AudioEngine.UpdateSpinSample((float)scoringRotationCount / rotationRequirement);

                if (scoringRotationCount > rotationRequirement + 3 &&
                    (scoringRotationCount - (rotationRequirement + 3)) % 2 == 0)
                {
                    if (spriteGlow != null)
                        spriteGlow.FlashColour(Color.White, 200);


                    score = IncreaseScoreType.SpinnerBonus;
                    if (!ModManager.CheckActive(Mods.Cinema))
                        AudioEngine.PlaySample(@"spinnerbonus", AudioEngine.VolumeSample, SkinSource.All);
                    spriteBonus.Text = (1000 * (scoringRotationCount - (rotationRequirement + 3)) / 2).ToString();
                    spriteBonus.Transformations.Clear();
                    spriteBonus.Transformations.Add(
                        new Transformation(TransformationType.Fade, 1, 0, AudioEngine.Time, AudioEngine.Time + 800));
                    spriteBonus.Transformations.Add(
                        new Transformation(TransformationType.Scale, 2f, 1.28f, AudioEngine.Time, AudioEngine.Time + 800));
                    spriteBonus.Transformations[0].Easing = EasingTypes.Out;
                    spriteBonus.Transformations[1].Easing = EasingTypes.Out;
                    //Ensure we don't recycle this too early.
                    spriteBonus.Transformations.Add(
                        new Transformation(TransformationType.Fade, 0, 0, EndTime + 800, EndTime + 800));
                }
                else if (scoringRotationCount > 1 && scoringRotationCount % 2 == 0)
                    score = IncreaseScoreType.SpinnerSpinPoints;
                else if (scoringRotationCount > 1)
                    score = IncreaseScoreType.SpinnerSpin;
            }

            lastRotationCount = rotationCount;

            return score;
        }

        #region Spinner Cheat Detection (woc2006)
        private bool recordMove;
        private List<float> movePoint;
        private const int MOVE_SAMPLE_COUNT = 20;
        private const float CHEAT_DELTA = 1F;
        private float totalMoveDis;
        private int checkRunCount;

        //only worked for circle.
        private void AddMoveSample(float val)
        {
            if (!recordMove || Player.Instance == null)
                return;
            if (val < 100 || rpm < 450)  //10 pixels from centre
                return;
            //keep total count under MOVE_SAMPLE_COUNT
            if (movePoint.Count == MOVE_SAMPLE_COUNT)
            {
                totalMoveDis -= movePoint[0];
                movePoint.RemoveAt(0);
            }
            totalMoveDis += val;
            movePoint.Add(val);
            if (movePoint.Count < MOVE_SAMPLE_COUNT)
                return;
            float meanMoveDis = totalMoveDis / movePoint.Count;  // movePoint.Count = MOVE_SAMPLE_COUNT
            var cheatCount = 0;
            for (int i = 0; i < MOVE_SAMPLE_COUNT; i++)
            {
                if (Math.Abs(movePoint[i] - meanMoveDis) < CHEAT_DELTA)
                    cheatCount++;
            }
            movePoint.Clear();
            totalMoveDis = 0;
            checkRunCount++;
            if (checkRunCount >= 5)
                recordMove = false;
            if (cheatCount == MOVE_SAMPLE_COUNT)
            {
                Player.Instance.AddSpinnerFlag();
                recordMove = false;
            }
        }
        #endregion
    }
}