using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.GameplayElements.Beatmaps;
using osu_common;
using osu.Helpers;
using osu.Configuration;

namespace osu.GameplayElements.HitObjects.Osu
{
    internal class HitCircleSliderEnd : HitCircleOsu
    {
        private readonly Transformation appearfake;

        internal HitCircleSliderEnd(HitObjectManager hom, Vector2 pos, int startTime, int sliderStart, bool reverse, float angle, SliderOsu parent)
            : base(hom, pos, startTime, false, false, false, false, 0)
        {
            SpriteCollection.Remove(SpriteApproachCircle);
            SpriteCollection.Remove(SpriteHitCircleText);

            appearfake = new Transformation(TransformationType.Fade, 0, 0, sliderStart - hitObjectManager.PreEmptSliderComplete, Math.Min(sliderStart, sliderStart - hitObjectManager.PreEmptSliderComplete + HitObjectManager.FadeIn));

            if (reverse)
            {
                SpriteHitCircleText =
                    new pSprite(SkinManager.Load(@"reversearrow"), Fields.Gamefield,
                                Origins.Centre, Clocks.Audio, Position,
                                SpriteManager.drawOrderBwd(startTime - 3), false, Color.White);
                //SpriteHitCircleText.CurrentRotation = angle;
                SpriteHitCircleText.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, sliderStart, sliderStart + 150));
                SpriteCollection.Add(SpriteHitCircleText);
                DimCollection.Add(SpriteHitCircleText);
            }

            SpriteHitCircle1.Transformations.Clear();
            SpriteHitCircle1.Transformations.Add(appearfake);
            SpriteHitCircle2.Transformations.Clear();
            SpriteHitCircle2.Transformations.Add(appearfake);

            Disarm();

            if (reverse)
            {
                if (SkinManager.UseNewLayout)
                {
                    SpriteHitCircleText.Rotation = angle;
                    for (int i = sliderStart; i < startTime; i += 300)
                    {
                        int length = Math.Min(300, startTime - i);
                        SpriteHitCircleText.Transformations.Add(new Transformation(TransformationType.Scale, 1.3f, 1, i, i + length, EasingTypes.Out));
                    }
                }
                else
                {
                    for (int i = sliderStart; i < startTime; i += 300)
                    {
                        int length = Math.Min(300, startTime - i);
                        SpriteHitCircleText.Transformations.Add(new Transformation(TransformationType.Scale, 1.3F, 1, i, i + length));
                        SpriteHitCircleText.Transformations.Add(new Transformation(TransformationType.Rotation, angle + OsuMathHelper.Pi / 32, angle - OsuMathHelper.Pi / 32, i, i + length));
                    }
                }
            }
        }

        internal override void SetColour(Color color)
        {
            if (SpriteHitCircleText.Texture != null && SpriteHitCircleText.Texture.Source == SkinSource.Osu && color.R + color.G + color.B > 600)
            {
                SpriteHitCircleText.InitialColour = Color.Black;
                DimCollection.Remove(SpriteHitCircleText);
            }

            base.SetColour(color);
        }

        internal override void Disarm()
        {
            base.Disarm();

            for (int i = 0; i < SpriteCollection.Count; i++)
            {
                pSprite p = SpriteCollection[i];
                if (p.Clock == Clocks.Game)
                    continue;

                p.Transformations.RemoveAll(t => t.TagNumeric == ARMED);
                p.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, StartTime, StartTime) { TagNumeric = ARMED });
            }
        }

        internal override void Arm(bool isHit)
        {
            if (!isHit || (GameBase.Mode == OsuModes.Edit && !ConfigManager.sEditorHitAnimations))
                return;

            foreach (pSprite p in SpriteCollection)
            {
                if (p == SpriteSelectionCircle)
                    continue;

                p.Transformations.RemoveAll(t => t.TagNumeric == ARMED);
                p.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, StartTime, StartTime + HitObjectManager.FadeOut) { TagNumeric = ARMED });
                p.Transformations.Add(new Transformation(TransformationType.Scale, 1.0F, 1.4F, StartTime, (int)(StartTime + HitObjectManager.FadeOut), EasingTypes.Out) { TagNumeric = ARMED });
            }
        }
    }
}