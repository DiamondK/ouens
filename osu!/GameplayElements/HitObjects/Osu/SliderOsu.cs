using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Edit;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Beatmaps;
using osu.Graphics;
using osu.Graphics.OpenGl;
using osu.Graphics.Primitives;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu.Input;
using osu.Input.Handlers;
using osu_common;
using Un4seen.Bass;
using osu.GameplayElements.Scoring;
using osu.GameModes.Online;

namespace osu.GameplayElements.HitObjects.Osu
{
    internal class SliderOsu : Slider
    {
        internal override void SetEndTime(int time)
        {
            throw new NotImplementedException();
        }

        internal override List<pSprite> KiaiSprites
        {
            get
            {
                return new List<pSprite>() { sliderBody };
            }
        }

        #region General

        internal override void Disarm()
        {
            base.Disarm();
            StopSound();
            sliderStartCircle.Disarm();
            lastTickSounded = 0;
        }

        internal SliderOsu(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType,
                           CurveTypes curveType, int repeatCount, double sliderLength, List<Vector2> sliderPoints, List<HitObjectSoundType> soundTypes, int comboOffset)
            : base(hom)
        {
            CurveTypeSavable = CurveType = curveType;
            this.ComboOffset = comboOffset;
            spriteManager = hitObjectManager.spriteManager;
            StartTime = startTime;
            EndTime = startTime;
            Position = startPosition;
            EndPosition = startPosition;
            SoundType = soundType;

            if (sliderPoints == null)
            {
                sliderCurvePoints = new List<Vector2>();
                sliderCurvePoints.Add(Position);
            }
            else
            {
                sliderCurvePoints = sliderPoints;
                if (sliderCurvePoints.Count > 0)
                {
                    if (sliderCurvePoints[0] != Position)
                        sliderCurvePoints.Insert(0, Position);
                }
                else
                    sliderCurvePoints.Add(Position);
            }

            SegmentCount = Math.Max(1, repeatCount);

            unifiedSoundAddition = soundTypes == null || soundTypes.Count == 0;
            if (!unifiedSoundAddition)
            {
                SoundTypeList = soundTypes;
                SampleSetList = new List<SampleSet>(repeatCount + 1);
                SampleSetAdditionList = new List<SampleSet>(repeatCount + 1);
            }

            this.SpatialLength = sliderLength;
            Drawable = true;

            sliderEndCircles = new List<HitCircleSliderEnd>();

            Type = HitObjectType.Slider;
            if (newCombo)
                Type |= HitObjectType.NewCombo;

            if (spriteManager != null)
            {
                addStartCircle();

                sliderFollower =
                    new pAnimation(SkinManager.LoadAll(@"sliderfollowcircle"), Fields.Gamefield,
                                   Origins.Centre, Clocks.Audio, Position, 0.99f, false, Color.TransparentWhite,
                                   this);
                sliderFollower.SetFramerateFromSkin();

                pTexture[] sliderballtextures = SkinManager.LoadAll(@"sliderb", SkinSource.All, false);

                bool usingDefault = sliderballtextures[0].Source == SkinSource.Osu;

                sliderBall =
                    new pAnimation(sliderballtextures, Fields.Gamefield, Origins.Centre,
                                   Clocks.Audio, Position, 0.99f, false, usingDefault ? SkinManager.LoadColour(@"SliderBall") : Color.White, this);
                sliderBall.TrackRotation = true;


                if (usingDefault)
                {
                    sliderBallSpec =
                        new pSprite(SkinManager.Load(@"sliderb-spec", SkinSource.All), Fields.Gamefield, Origins.Centre,
                                    Clocks.Audio, Position, 1.0f, false, Color.White)
                        {
                            Additive = true
                        };

                    sliderBallNd =
                        new pSprite(SkinManager.Load(@"sliderb-nd", SkinSource.All), Fields.Gamefield, Origins.Centre,
                                    Clocks.Audio, Position, 0.98f, false,
                                    new Color(5, 5, 5));
                }

                sliderBody = new pSprite(null, Fields.Native, Origins.TopLeft, Clocks.Audio, Position, SpriteManager.drawOrderBwd(EndTime + 10), false, Color.White);
                sliderBody.IsDisposable = true;
            }
        }

        internal override void Shake()
        {
        }

        //we call it virtual,because in some cases the "endtime" != true EndTime
        internal int VirtualEndTime
        {
            get
            {
                return (int)Math.Floor(SpatialLength * hitObjectManager.Beatmap.beatLengthAt(StartTime) * SegmentCount * 0.01 / hitObjectManager.Beatmap.DifficultySliderMultiplier + StartTime);
            }
        }

        internal double VirtualLength
        {
            get
            {
                return Length * hitObjectManager.Beatmap.DifficultySliderMultiplier * 100 / SegmentCount / hitObjectManager.Beatmap.beatLengthAt(StartTime);
            }
        }

        protected virtual void addStartCircle()
        {
            sliderStartCircle =
                new HitCircleSliderStart(hitObjectManager, new Vector2((int)Position.X, (int)Position.Y), StartTime,
                                 NewCombo,
                                 unifiedSoundAddition ? SoundType : SoundTypeList[0], ComboOffset);
            Transformation t = new Transformation(TransformationType.Fade, 0, 0, StartTime - 10, StartTime - 10);

            sliderStartCircle.SpriteHitCircle1.Transformations.RemoveAt(0);
            sliderStartCircle.SpriteHitCircle1.Transformations.Insert(0, t);

            //The overlay doesn't need to be drawn in some cases (saving some fill rate and double opacity).
            if (!hitObjectManager.ShowOverlayAboveNumber || (hitObjectManager.Beatmap.OverlayPosition == OverlayPosition.NoChange && SkinManager.IsDefault))
            {
                sliderStartCircle.SpriteHitCircle2.Transformations.RemoveAt(0);
                sliderStartCircle.SpriteHitCircle2.Transformations.Insert(0, t);
            }
        }

        internal List<HitCircleOsu> sliderAllCircles
        {
            get
            {
                List<HitCircleOsu> l = new List<HitCircleOsu>();
                l.Add(sliderStartCircle);
                sliderEndCircles.ForEach(l.Add);
                return l;
            }
        }

        internal override HitObject Clone()
        {
            List<HitObjectSoundType> stl = null;
            if (SoundTypeList != null)
            {
                stl = new List<HitObjectSoundType>();
                stl.AddRange(SoundTypeList);
            }

            List<Vector2> scp = new List<Vector2>();
            scp.AddRange(sliderCurvePoints);


            SliderOsu s = new SliderOsu(hitObjectManager, Position, StartTime, Type.IsType(HitObjectType.NewCombo), SoundType, CurveType, SegmentCount,
                                        SpatialLength, scp, stl, ComboOffset);

            s.ComboNumber = ComboNumber;
            s.sliderRepeatPoints = new List<int>(sliderRepeatPoints);
            s.EndTime = EndTime;
            s.EndPosition = EndPosition;
            s.SetColour(Colour);
            s.Selected = Selected;
            s.BodySelected = BodySelected;
            s.ComboColourIndex = ComboColourIndex; // fix Undo issue?
            s.CurveTypeSavable = CurveTypeSavable;
            s.SampleSet = SampleSet;
            s.SampleSetAdditions = SampleSetAdditions;
            s.SampleVolume = SampleVolume;
            s.SampleAddr = SampleAddr;
            s.SampleFile = SampleFile;
            if (SampleSetList != null) s.SampleSetList = new List<SampleSet>(SampleSetList);
            if (SampleSetAdditionList != null) s.SampleSetAdditionList = new List<SampleSet>(SampleSetAdditionList);

            return s;
        }

        protected override void Dispose(bool isDisposing)
        {
            if (sliderBody != null)
                RemoveBody();

            base.Dispose(isDisposing);
        }

        #endregion

        #region Drawing

        internal readonly pAnimation sliderBall;
        internal readonly pSprite sliderBallSpec;
        internal readonly pSprite sliderBallNd;
        internal readonly List<HitCircleSliderEnd> sliderEndCircles;

        internal readonly pAnimation sliderFollower;
        protected SpriteManager spriteManager;
        internal CurveTypes CurveType;

        private int drawnSegments = 0;

        internal pSprite sliderBody;

        internal List<Vector2> sliderCurvePoints;
        internal List<Line> sliderCurveSmoothLines;
        internal double curveLength;

        private RenderTarget2D renderTarget;

        /// <summary>
        /// Cumulative list of curve lengths up to AND INCLUDING a given line.
        /// Unlike osum, this comes from the non-optimized collection of lines (sliderCurveSmoothLines).
        /// </summary>
        internal List<double> cumulativeLengths;

        internal HitCircleOsu sliderStartCircle;

        /// <summary>
        /// This is the minimum length the last segment in a slider is permitted to be.
        /// This fixes an issue where the last segment is extremely tiny, or even 0-length,
        /// leading to undefined/inaccurate orientation for its reverse arrow.
        /// This value can get pretty big without causing problems, since the tiny segment
        /// gets merged into the previous segment, yielding a slider with the correct length.
        /// </summary>
        private const double MIN_SEGMENT_LENGTH = 0.0001;

        public override Vector2 EndPosition { get; set; }

        public override int ComboNumber
        {
            get { return sliderStartCircle.ComboNumber; }
            set { sliderStartCircle.ComboNumber = value; }
        }

        internal override bool IsVisible
        {
            get
            {
                return
                    AudioEngine.Time >= StartTime - hitObjectManager.PreEmpt &&
                    AudioEngine.Time <= EndTime + HitObjectManager.FadeOut;
            }
        }

        internal override void Update()
        {
            base.Update();

            if (GameBase.Mode == OsuModes.Edit || placingPending)
                UpdateCalculations(false);

            if (sliderScoreTimingPoints.Count > 0)
            {
                int dotsPerRepeat = sliderScoreTimingPoints.Count / SegmentCount;
                sliderBall.Reverse = (lastTickSounded + dotsPerRepeat + 1) / dotsPerRepeat % 2 == 0;

                sliderBall.FlipHorizontal = (lastTickSounded + dotsPerRepeat + 1) / dotsPerRepeat % 2 == 0 && SkinManager.Current.SliderBallFlip;

                //slider ball operations
                sliderBall.FrameDelay = Math.Max((150 / Velocity) * GameBase.SIXTY_FRAME_TIME, GameBase.SIXTY_FRAME_TIME);
            }

            int closeTick = -1;
            for (int i = 0; i < sliderScoreTimingPoints.Count - 1; i++)
            {
                if (sliderScoreTimingPoints[i] > AudioEngine.Time)
                    break;
                closeTick = i;
            }

            if ((AudioEngine.Time >= StartTime &&
                 AudioEngine.Time <= EndTime) && AudioEngine.AudioState == AudioStates.Playing
                // && !(GameBase.Mode == OsuModes.Edit && !HitObjectManager.AutoPlay)
                )
            {
                if (IsSliding)
                {
                    SliderSoundStart();

                    if (closeTick > lastTickSounded)
                    {
                        lastTickSounded = closeTick;

                        if ((lastTickSounded + 1) % (sliderScoreTimingPoints.Count / SegmentCount) > 0)
                            PlaySound(true, lastTickSounded);
                        else
                            PlaySound(false, (lastTickSounded + 1) / (sliderScoreTimingPoints.Count / SegmentCount));

                        pSprite scoreTick = SpriteCollection.Find(p => p.TagNumeric == sliderScoreTimingPoints[lastTickSounded]);
                        if (scoreTick != null && scoreTick.Transformations.Count > 1)
                        {
                            Transformation tr = scoreTick.Transformations.Find(t => t.Type == TransformationType.Fade && t.EndFloat == 0);
                            if (tr != null)
                            {
                                tr.Time1 = sliderScoreTimingPoints[lastTickSounded];
                                tr.Time2 = sliderScoreTimingPoints[lastTickSounded];
                            }
                        }
                        lastTickSounded = closeTick;
                    }
                }
                else
                {
                    StopSound();
                    lastTickSounded = closeTick;
                }
            }
            else
            {
                //StopSound();
                lastTickSounded = -1;
            }
        }

        /// <summary>
        /// The slider curve type hit a fallback case during editing. This is the one we can be sure is the final type, and will be transferred to the actual curve type after placement.
        /// </summary>
        internal CurveTypes CurveTypeSavable;

        internal virtual void UpdateCalculations(bool force = true)
        {
            force |= SpatialLength == 0 || sliderCurveSmoothLines == null || placingPending;

            if (!force) return;

            RemoveBody();

            Velocity = hitObjectManager.SliderVelocityAt(StartTime);

            List<Line> path = new List<Line>();

            if (placingPending)
            {
                if (sliderCurvePoints.Contains(placingPoint))
                    placingPending = false;
                else
                    sliderCurvePoints.Add(placingPoint);
            }

            int linearSpacing = 8;

            switch (CurveType)
            {
                case CurveTypes.Catmull:
                    for (int j = 0; j < sliderCurvePoints.Count - 1; j++)
                    {
                        Vector2 v1 = (j - 1 >= 0 ? sliderCurvePoints[j - 1] : sliderCurvePoints[j]);
                        Vector2 v2 = sliderCurvePoints[j];
                        Vector2 v3 = (j + 1 < sliderCurvePoints.Count
                                          ? sliderCurvePoints[j + 1]
                                          : v2 + (v2 - v1));
                        Vector2 v4 = (j + 2 < sliderCurvePoints.Count
                                          ? sliderCurvePoints[j + 2]
                                          : v3 + (v3 - v2));

                        for (int k = 0; k < General.SLIDER_DETAIL_LEVEL; k++)
                            path.Add(
                                new Line(Vector2.CatmullRom(v1, v2, v3, v4, (float)k / General.SLIDER_DETAIL_LEVEL),
                                         Vector2.CatmullRom(v1, v2, v3, v4,
                                                            (float)(k + 1) / General.SLIDER_DETAIL_LEVEL)));
                        path[path.Count - 1].forceEnd = true;
                    }

                    CurveTypeSavable = CurveTypes.Catmull;
                    break;
                case CurveTypes.Bezier:
                    int lastIndex = 0;

                    for (int i = 0; i < sliderCurvePoints.Count; i++)
                    {
                        if (hitObjectManager.Beatmap.BeatmapVersion > 8)
                        {
                            bool multipartSegment = i < sliderCurvePoints.Count - 2 && sliderCurvePoints[i] == sliderCurvePoints[i + 1];

                            if (multipartSegment || i == sliderCurvePoints.Count - 1)
                            {
                                List<Vector2> thisLength = sliderCurvePoints.GetRange(lastIndex, i - lastIndex + 1);

                                if (thisLength.Count == 2)
                                {
                                    //we can use linear algorithm for this segment
                                    Line l = new Line(thisLength[0], thisLength[1]);
                                    int segments = Math.Max((int)l.rho / linearSpacing, 1);
                                    for (int j = 0; j < segments; j++)
                                    {
                                        Line l2 = new Line(l.p1 + (l.p2 - l.p1) * ((float)j / segments),
                                                     l.p1 + (l.p2 - l.p1) * ((float)(j + 1) / segments));
                                        l2.straight = true;
                                        path.Add(l2);
                                    }
                                }
                                else
                                {
                                    if (hitObjectManager.Beatmap.BeatmapVersion < 10)
                                    {
                                        //use the WRONG bezier algorithm. sliders will be 1/50 too short!
                                        List<Vector2> points = OsuMathHelper.CreateBezierWrong(thisLength);
                                        for (int j = 1; j < points.Count; j++)
                                            path.Add(new Line(points[j - 1], points[j]));
                                    }
                                    else
                                    {
                                        //use the bezier algorithm
                                        List<Vector2> points = OsuMathHelper.CreateBezier(thisLength);
                                        for (int j = 1; j < points.Count; j++)
                                            path.Add(new Line(points[j - 1], points[j]));
                                    }
                                }
                                path[path.Count - 1].forceEnd = true;

                                if (multipartSegment) i++;
                                //Need to skip one point since we consuned an extra.

                                lastIndex = i;
                            }
                        }
                        else if (hitObjectManager.Beatmap.BeatmapVersion > 6)
                        {
                            bool multipartSegment = i < sliderCurvePoints.Count - 2 && sliderCurvePoints[i] == sliderCurvePoints[i + 1];

                            if (multipartSegment || i == sliderCurvePoints.Count - 1)
                            {
                                List<Vector2> thisLength = sliderCurvePoints.GetRange(lastIndex, i - lastIndex + 1);

                                List<Vector2> points = OsuMathHelper.CreateBezier(thisLength);
                                for (int j = 1; j < points.Count; j++)
                                    path.Add(new Line(points[j - 1], points[j]));
                                path[path.Count - 1].forceEnd = true;

                                if (multipartSegment) i++;
                                //Need to skip one point since we consuned an extra.

                                lastIndex = i;
                            }
                        }
                        else
                        {
                            //This algorithm is broken for multipart sliders (http://osu.sifterapp.com/projects/4151/issues/145).
                            //Newer maps always use the one in the else clause.

                            if ((i > 0 && sliderCurvePoints[i] == sliderCurvePoints[i - 1]) ||
                            i == sliderCurvePoints.Count - 1)
                            {
                                List<Vector2> thisLength = sliderCurvePoints.GetRange(lastIndex, i - lastIndex + 1);

                                List<Vector2> points = OsuMathHelper.CreateBezier(thisLength);
                                for (int j = 1; j < points.Count; j++)
                                    path.Add(new Line(points[j - 1], points[j]));
                                path[path.Count - 1].forceEnd = true;

                                lastIndex = i;
                            }
                        }
                    }

                    CurveTypeSavable = CurveTypes.Bezier;
                    break;
                case CurveTypes.PerfectCurve:
                    // we may have 2 points when building the circle.
                    if (sliderCurvePoints.Count < 3)
                        goto case CurveTypes.Linear;
                    // more than 3 -> ignore them.
                    if (sliderCurvePoints.Count > 3)
                        goto case CurveTypes.Bezier;

                    Vector2 A = sliderCurvePoints[0];
                    Vector2 B = sliderCurvePoints[1];
                    Vector2 C = sliderCurvePoints[2];
                    // all 3 points are on a straight line, avoid undefined behaviour:
                    if ((B.X - A.X) * (C.Y - A.Y) - (C.X - A.X) * (B.Y - A.Y) == 0.0f)
                        goto case CurveTypes.Linear;

                    Vector2 centre;
                    float radius;
                    double t_initial, t_final;

                    OsuMathHelper.CircleThroughPoints(A, B, C, out centre, out radius, out t_initial, out t_final);

                    if (GameBase.Mode == OsuModes.Edit)
                    {
                        // Find the circle's bounding box
                        // Don't fall back to bezier if the slider is already placed and
                        // eg. the user rotates it.
                        // http://osu.ppy.sh/forum/t/116612
                        osu.Helpers.BoundingBox box = new Helpers.BoundingBox(OsuMathHelper.CirclePoint(centre, radius, t_initial), OsuMathHelper.CirclePoint(centre, radius, t_final));
                        double t_low = Math.Min(t_initial, t_final);
                        double t_high = Math.Max(t_initial, t_final);
                        int q = (int)(t_low * 2.0d / Math.PI + 1);
                        while (q * 0.5f * Math.PI < t_high)
                        {
                            box.Add(OsuMathHelper.CirclePoint(centre, radius, q * 0.5f * Math.PI));
                            q++;
                        }

                        if ((placingPending && ((float)box.Width > 512 || (float)box.Height > 384))
                            || ((float)box.Width > 640 || (float)box.Height > 640))
                            goto case CurveTypes.Bezier;
                    }

                    curveLength = Math.Abs((t_final - t_initial) * radius);
                    int _segments = (int)(curveLength * 0.125f);

                    Vector2 lastPoint = A;

                    for (int i = 1; i < _segments; i++)
                    {
                        double progress = (double)i / (double)_segments;
                        double t = t_final * progress + t_initial * (1 - progress);

                        Vector2 newPoint = OsuMathHelper.CirclePoint(centre, radius, t);
                        path.Add(new Line(lastPoint, newPoint));

                        lastPoint = newPoint;
                    }

                    path.Add(new Line(lastPoint, C));

                    CurveTypeSavable = CurveTypes.PerfectCurve;
                    break;
                case CurveTypes.Linear:
                    for (int i = 1; i < sliderCurvePoints.Count; i++)
                    {
                        Line l = new Line(sliderCurvePoints[i - 1], sliderCurvePoints[i]);
                        int segments = Math.Max((int)l.rho / 10, 1);

                        for (int j = 0; j < segments; j++)
                        {
                            Line l2 = new Line(l.p1 + (l.p2 - l.p1) * ((float)j / segments),
                                            l.p1 + (l.p2 - l.p1) * ((float)(j + 1) / segments));
                            l2.straight = true;
                            path.Add(l2);
                        }

                        path[path.Count - 1].forceEnd = true;
                    }

                    CurveTypeSavable = CurveTypes.Linear;
                    break;
            }

            //if (GameBase.GameState == GameStates.Edit)
            //{
            double total = 0;

            int pathCount = path.Count;

            switch (CurveType)
            {
                default:
                    for (int i = 0; i < pathCount; i++)
                        total += path[i].rho;
                    curveLength = total;
                    break;
            }

            int aimCount = 0;
            double tickDistance = (hitObjectManager.Beatmap.BeatmapVersion < 8) ? hitObjectManager.SliderScoringPointDistance :
                (hitObjectManager.SliderScoringPointDistance / hitObjectManager.Beatmap.bpmMultiplierAt(StartTime)); // Keep tick rate a TIME CONSTANT so non 0.5/2x slider speeds are well-behaved.

            //this will make sure the length of the slider is a multiple of the scoring points
            if (total > 0)
            {
                if (placingPending || SpatialLength == 0)
                {
                    //Currently in editor mode
                    if (Editor.Instance != null && Editor.isBeatSnapping)
                    {
                        double f = (tickDistance * hitObjectManager.Beatmap.DifficultySliderTickRate) / Editor.Instance.beatSnapDivisor;
                        aimCount = (int)Math.Max(0, (total + 1.0d) / f); // Slightly more generous rounding so we can have super symmetry sliders.
                        SpatialLength = aimCount * f;
                    }
                    else
                    {
                        aimCount = (int)(total / tickDistance);
                        SpatialLength = total;
                    }
                }

                if (tickDistance > SpatialLength)
                    tickDistance = SpatialLength;

                if (successfulPlace)
                {
                    successfulPlaceLength = aimCount;
                    successfulPlace = false;
                }

                double excess = total - SpatialLength;

                while (path.Count > 0)
                {
                    Line lastLine = path[path.Count - 1];
                    float lastLineLength = Vector2.Distance(lastLine.p1, lastLine.p2);

                    if (lastLineLength > excess + MIN_SEGMENT_LENGTH)
                    {
                        if (lastLine.p2 != lastLine.p1)
                        {
                            lastLine.p2 = lastLine.p1 +
                                      Vector2.Normalize(lastLine.p2 - lastLine.p1) * (lastLine.rho - (float)excess);
                        }
                        break;
                    }

                    path.Remove(lastLine);
                    excess -= lastLineLength;
                }
            }

            if (placingPending)
            {
                if (aimCount == successfulPlaceLength && aimCount != 0)
                {
                    sliderCurvePoints.RemoveAt(sliderCurvePoints.Count - 1);
                    placingValid = false;
                }
                else
                    placingValid = true;

                placingPending = false;
            }


            pathCount = path.Count;

            if (pathCount > 0)
            {
                //fill the cache
                sliderCurveSmoothLines = path;

                if (cumulativeLengths == null) cumulativeLengths = new List<double>(pathCount);
                else cumulativeLengths.Clear();

                total = 0.0d; // variable recycling ohoho

                foreach (Line l in path)
                {
                    total += l.rho;
                    cumulativeLengths.Add(total);
                }
            }

            if (spriteManager != null)
            {
                if (SpriteCollection.Count > 0)
                    spriteManager.RemoveRange(SpriteCollection);

                //update the follower path
                sliderFollower.Transformations.Clear();
                sliderBall.Transformations.Clear();
                if (sliderBallSpec != null) sliderBallSpec.Transformations.Clear();
                if (sliderBallNd != null) sliderBallNd.Transformations.Clear();
                sliderScoreTimingPoints.Clear();
                sliderEndCircles.Clear();
            }

            if (pathCount < 1)
                return;

            if (spriteManager != null)
            {
                SpriteCollection.Clear();
                DimCollection.Clear();

                //add in the static sprites which won't be changed in this method
                for (int i = 0; i < sliderStartCircle.SpriteCollection.Count; i++)
                {
                    pSprite p = sliderStartCircle.SpriteCollection[i];
                    p.Tag = this;
                    SpriteCollection.Add(p);
                }

                DimCollection.AddRange(sliderStartCircle.DimCollection);

                SpriteCollection.Add(sliderFollower);
                if (sliderBallSpec != null) SpriteCollection.Add(sliderBallSpec);
                if (sliderBallNd != null) SpriteCollection.Add(sliderBallNd);
                SpriteCollection.Add(sliderBall);

                if (path.Count > 2)
                {
                    float ballStartAngle = (float)Math.Atan2(path[0].p1.Y - path[0].p2.Y, path[0].p1.X - path[0].p2.X);
                    sliderBall.FlipVertical = ballStartAngle >= -Math.PI / 2 && ballStartAngle <= Math.PI / 2;
                }
            }

            //draw circles and calculate the follow path
            bool reverse = false;
            bool firstRun = true;
            double scoringLengthTotal = 0;
            double currentTime = StartTime;
            Vector2 p2 = new Vector2();
            Vector2 p1 = new Vector2();

            double scoringDistance = 0;

            Position2 = path[pathCount - 1].p2;



            for (int i = 0; i < SegmentCount; i++)
            {
                int reverseStartTime = (int)currentTime;

                bool endAccounted = false;

                List<pSprite> scoringDots = new List<pSprite>();

                for (int j = 0; j < pathCount; j++)
                {
                    Line l = path[j];

                    if (reverse)
                    {
                        p1 = l.p2;
                        p2 = l.p1;
                    }
                    else
                    {
                        p1 = l.p1;
                        p2 = l.p2;
                    }

                    //path for follower and ball
                    float distance = Vector2.Distance(l.p1, l.p2);

                    double duration = 1000F * distance / Velocity;

                    if (spriteManager != null)
                    {
                        Transformation t =
                            new Transformation(p1, p2, (int)currentTime, (int)(currentTime + duration));
                        sliderFollower.Transformations.Add(t);
                        sliderBall.Transformations.Add(t);
                        if (sliderBallSpec != null) sliderBallSpec.Transformations.Add(t);
                        if (sliderBallNd != null) sliderBallNd.Transformations.Add(t);
                    }

                    currentTime += duration;
                    scoringDistance += distance;

                    //sprites for scoring points (dots)
                    if (scoringDistance >= tickDistance ||
                        ((j == pathCount - 1) && !endAccounted))
                    {
                        scoringLengthTotal +=
                            Math.Min(tickDistance, scoringDistance);

                        int scoreTime = timeAtLength((float)scoringLengthTotal);

                        if (j == pathCount - 1)
                        {
                            scoringLengthTotal -= (tickDistance - scoringDistance);
                            scoringDistance = tickDistance - scoringDistance;
                            endAccounted = true;
                        }
                        else
                            scoringDistance -= Math.Min(tickDistance, scoringDistance);

                        if (spriteManager != null)
                        {
                            if (j != pathCount - 1)
                            {
                                float thisPointRatio = 1 - (float)(scoringDistance / Vector2.Distance(p1, p2));
                                Vector2 adjustedPos = p1 + (p2 - p1) * (thisPointRatio);


                                pSprite scoringDot =
                                    new pSprite(SkinManager.Load(@"sliderscorepoint"),
                                                Fields.Gamefield, Origins.Centre, Clocks.Audio, adjustedPos,
                                                SpriteManager.drawOrderBwd(StartTime - 5), false, Color.White, this);
                                if (scoringDot.Texture.Source == SkinSource.Osu)
                                    scoringDot.Additive = true;

                                //handle the first dots appearing on the preempt
                                if (firstRun)
                                {
                                    int startTime = (scoreTime - StartTime) / 2 + StartTime - hitObjectManager.PreEmptSliderComplete;
                                    int endTime = startTime + 150;// (int)(HitObjectManager.FadeIn * ((float)j / (pathCount - 1)));

                                    scoringDot.Transformations.Add(
                                        new Transformation(TransformationType.Fade, 0, 1, startTime, endTime));
                                    scoringDot.Transformations.Add(new Transformation(TransformationType.Scale, 0.5f, 1.2f, startTime, endTime));
                                    scoringDot.Transformations.Add(new Transformation(TransformationType.Scale, 1.2f, 1.0f, endTime, endTime + 150, EasingTypes.Out));
                                }
                                else
                                {
                                    int displayStartTime = reverseStartTime + (scoreTime - reverseStartTime) / 2;
                                    scoringDot.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, displayStartTime - 200, displayStartTime));
                                    scoringDot.Transformations.Add(new Transformation(TransformationType.Scale, 0.5f, 1.2f, displayStartTime - 200, displayStartTime - 50));
                                    scoringDot.Transformations.Add(new Transformation(TransformationType.Scale, 1.2f, 1.0f, displayStartTime - 50, displayStartTime + 150, EasingTypes.Out));
                                }
                                float radiusSquared = hitObjectManager.HitObjectRadius * hitObjectManager.HitObjectRadius;
                                if (GameBase.Mode != OsuModes.Play ||
                                    (Vector2.DistanceSquared(adjustedPos, Position2) >= radiusSquared &&
                                     Vector2.DistanceSquared(adjustedPos, Position) >= radiusSquared))
                                {
                                    SpriteCollection.Add(scoringDot);
                                    scoringDots.Add(scoringDot);
                                }

                                scoringDot.TagNumeric = scoreTime;
                            }
                        }

                        sliderScoreTimingPoints.Add(scoreTime);
                    }
                }

                if (spriteManager != null)
                    foreach (pSprite p in scoringDots)
                        p.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, (int)currentTime, (int)currentTime));

                float angle = (float)Math.Atan2(p1.Y - p2.Y, p1.X - p2.X);

                reverse = !reverse;
                path.Reverse();

                if (spriteManager != null)
                {
                    HitCircleSliderEnd h =
                        new HitCircleSliderEnd(hitObjectManager, p2, (int)(currentTime),
                        (firstRun ? StartTime - hitObjectManager.PreEmptSliderComplete : reverseStartTime - (int)(currentTime - reverseStartTime)),
                        (i < SegmentCount - 1), angle, this);

                    h.SetColour(sliderStartCircle.SpriteHitCircle1.InitialColour);
                    sliderEndCircles.Add(h);

                    if (firstRun)
                        DimCollection.AddRange(h.DimCollection);
                    SpriteCollection.AddRange(h.SpriteCollection);
                }
                firstRun = false;
            }

            if (reverse)
                path.Reverse();

            if (Selected && sliderEndCircles.Count > 0)
                sliderEndCircles[0].Select();

            EndPosition = p2;
            EndTime = (int)currentTime;

            if (spriteManager != null)
            {
                sliderBody.Depth = SpriteManager.drawOrderBwd(EndTime + 10);

                sliderBody.Transformations.Clear();
                sliderBody.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.97F, StartTime - hitObjectManager.PreEmpt, StartTime - hitObjectManager.PreEmpt + HitObjectManager.FadeIn));
                sliderBody.Transformations.Add(new Transformation(TransformationType.Fade, 0.97F, 0, EndTime, EndTime + HitObjectManager.FadeOut));

                SpriteCollection.Add(sliderBody);
                DimCollection.Add(sliderBody);
            }

            //a little hack to make sure the last scoring point occurs before the slider is totally gone.
            //i REALLY doubt this will be noticeable in any game mode.
            //This is also inaudible.
            if (sliderScoreTimingPoints.Count > 0)
                sliderScoreTimingPoints[sliderScoreTimingPoints.Count - 1] = Math.Max(StartTime + Length / 2, sliderScoreTimingPoints[sliderScoreTimingPoints.Count - 1] - 36);

            sliderTicksHitStatus = new bool[sliderScoreTimingPoints.Count];

            sliderRepeatPoints.Clear();
            for (int i = 0; i < sliderScoreTimingPoints.Count - 1; i++)
                if (sliderScoreTimingPoints.Count / SegmentCount > 0 &&
                    (i + 1) % (sliderScoreTimingPoints.Count / SegmentCount) == 0)
                    sliderRepeatPoints.Add(sliderScoreTimingPoints[i]);

            //update sliding process
            if (IsSliding) KillSlide();

            if (spriteManager != null) spriteManager.Add(SpriteCollection);
            ApplyDim();
        }

        float snakingProgress;
        internal override void Draw()
        {
            if (IsVisible && sliderCurveSmoothLines != null)
            {
                float progress = ConfigManager.sSnakingSliders && (GameBase.Mode != OsuModes.Edit || ConfigManager.sEditorSnakingSliders) && !AllowNewPlacing && !Selected ?
                    Math.Min(1, (AudioEngine.Time - (StartTime - hitObjectManager.PreEmpt)) / (hitObjectManager.PreEmpt / 3f)) : 1;

                if ((snakingProgress != progress && GameBase.SixtyFramesPerSecondFrame) || sliderBody == null || sliderBody.Texture == null || sliderBody.Texture.IsDisposed)
                {
                    // Uncommenting this line would enable FBO slider rendering on d3d but presently it causes errors in XNA.
                    //if ((SkinManager.SliderStyle == SliderStyle.PeppySliders) || !GameBase.FrameBufferSupported)
                    RemoveBody();

                    snakingProgress = progress;
                    List<Line> lineList = new List<Line>();

                    int storedStart = 0;
                    bool waiting = false;

                    double lengthToDraw = progress * SpatialLength;
                    int count = progress >= 1 ? sliderCurveSmoothLines.Count : cumulativeLengths.FindIndex(l => l > lengthToDraw) + 1;
                    // count is 0 if and only if the slider should contains all segments, so FindIndex returned -1.
                    if (count == 0) count = sliderCurveSmoothLines.Count;

                    // avoid a rounding error if fullyDrawn is true.
                    float countRemainder = progress >= 1 || count - 2 < 0 ? 0.0f : ((float)lengthToDraw - (count == 1 ? 0.0f : (float)cumulativeLengths[count - 2]));

                    Vector2 pos1 = GameBase.GamefieldToDisplay(Position);
                    Vector2 pos2 = GameBase.GamefieldToDisplay(Position2);

                    for (int i = drawnSegments; i < count; i++)
                    {
                        if (!waiting)
                            storedStart = i;

                        bool last = i == count - 1;
                        int min_dist = (sliderCurveSmoothLines[i].straight && SkinManager.SliderStyle != SliderStyle.PeppySliders) ? 32 : 6;

                        if (Vector2.Distance(sliderCurveSmoothLines[storedStart].p1, sliderCurveSmoothLines[i].p2) > min_dist ||
                            last || sliderCurveSmoothLines[i].forceEnd || (i == count - 2))
                        {
                            if (last && countRemainder > 0)
                            {
                                Line l = new Line(sliderCurveSmoothLines[storedStart].p1, sliderCurveSmoothLines[i].p2);
                                l.p2 = l.p1 + Vector2.Normalize(l.p2 - l.p1) * countRemainder;

                                GameBase.GamefieldToDisplay(ref l);

                                if (progress < 1)
                                    pos2 = l.p2;

                                lineList.Add(l);
                            }
                            else if (storedStart == i)
                            {
                                Line line = GameBase.GamefieldToDisplay(sliderCurveSmoothLines[i]);
                                pos2 = line.p2;
                                lineList.Add(line);
                            }
                            else
                            {
                                Line line = GameBase.GamefieldToDisplay(new Line(sliderCurveSmoothLines[storedStart].p1, sliderCurveSmoothLines[i].p2));
                                pos2 = line.p2;
                                lineList.Add(line);
                            }
                            waiting = false;
                        }
                        else
                            waiting = true;
                    }

                    drawnSegments = count;

                    if (count == 0)
                        return;

                    Vector2 centre = HitObjectManager.GamefieldSpriteRes / 2 * Vector2.One;
                    Vector2 centreOverlay = centre;

                    int width, height, left = 0, top = 0;

                    float radius = hitObjectManager.HitObjectRadius * GameBase.GamefieldRatio * (GameBase.D3D && GameBase.PixelShaderVersion < 2 ? (float)19 / 20 : 1);

                    int lineListCount = lineList.Count;

                    if (lineListCount > 0)
                    {
                        Vector2 topLeft = lineList[0].p1;
                        Vector2 bottomRight = lineList[0].p1;

                        for (int i = 0; i < lineListCount; i++)
                        {
                            Line l = lineList[i];
                            if (l.p2.X < topLeft.X)
                                topLeft.X = l.p2.X;
                            else if (l.p2.X > bottomRight.X)
                                bottomRight.X = l.p2.X;

                            if (l.p2.Y < topLeft.Y)
                                topLeft.Y = l.p2.Y;
                            else if (l.p2.Y > bottomRight.Y)
                                bottomRight.Y = l.p2.Y;
                        }

                        width = (int)(bottomRight.X - topLeft.X + radius * 2.3);
                        height = (int)(bottomRight.Y - topLeft.Y + radius * 2.3);
                        left = (int)(topLeft.X - radius * 1.15);
                        top = (int)(topLeft.Y - radius * 1.15);
                    }
                    else
                    {
                        width = (int)(radius * 2.2);
                        height = (int)(radius * 2.2);
                    }

                    int drawWidth = width;
                    int drawHeight = height;
                    int drawLeft = left;
                    int drawTop = top;


                    if (GameBase.Mode == OsuModes.Edit)
                    {
                        //make sure we are within bounds of the screen while drawing (otherwise things look ugly)
                        //only needs to run in edit mode, because in-game sliders are never moved back on-screen even if they are
                        //off-screen to begin with (which shouldn't happen anyway!).

                        int excess = 0;

                        excess = (drawTop + drawHeight) - (GameBase.WindowHeight + GameBase.WindowOffsetY);
                        if (excess > 0)
                        {
                            //goes off bottom of screen
                            for (int i = 0; i < lineListCount; i++)
                            {
                                Line l = lineList[i];
                                l.p1.Y -= excess;
                                l.p2.Y -= excess;
                            }

                            drawTop -= excess;
                            pos1.Y -= excess;
                            pos2.Y -= excess;
                        }

                        excess = (drawLeft + drawWidth) - (GameBase.WindowWidth);
                        if (excess > 0)
                        {
                            //goes off bottom of screen
                            for (int i = 0; i < lineListCount; i++)
                            {
                                Line l = lineList[i];
                                l.p1.X -= excess;
                                l.p2.X -= excess;
                            }

                            drawLeft -= excess;
                            pos1.X -= excess;
                            pos2.X -= excess;
                        }

                        excess = -drawTop;
                        if (excess > 0)
                        {
                            //goes off bottom of screen
                            for (int i = 0; i < lineListCount; i++)
                            {
                                Line l = lineList[i];
                                l.p1.Y += excess;
                                l.p2.Y += excess;
                            }

                            drawTop += excess;
                            pos1.Y += excess;
                            pos2.Y += excess;
                        }

                        excess = -drawLeft;
                        if (excess > 0)
                        {
                            //goes off bottom of screen
                            for (int i = 0; i < lineListCount; i++)
                            {
                                Line l = lineList[i];
                                l.p1.X += excess;
                                l.p2.X += excess;
                            }

                            drawLeft += excess;
                            pos1.X += excess;
                            pos2.X += excess;
                        }
                    }

                    bool DisableColourRotation = SkinManager.LoadColour("SliderTrackOverride").A > 0;

                    //start the drawing procedure
                    int index;

                    Color actualColour;
                    if (BeatmapManager.Current.PlayMode != PlayModes.Taiko)
                        actualColour = hitObjectManager.ComboColours[ComboColourIndex];
                    else
                        actualColour = (new Color(252, 184, 6)); //Taiko yellow

                    if (Colour != Color.Gray && MatchSetup.Match != null && MatchSetup.Match.matchTeamType == MatchTeamTypes.TagCoop && MatchSetup.TagComboColour != Color.TransparentWhite)
                    {
                        actualColour = MatchSetup.TagComboColour;
                        index = -2; // Multiplay custom colour
                    }
                    else if (DisableColourRotation)
                        index = 0;
                    else if (Colour == Color.Gray && actualColour != Color.Gray)
                    {
                        actualColour = Color.Gray;
                        index = -1; // Multiplay grey note
                    }
                    else
                        index = this.ComboColourIndex;

                    pTexture hitcircle = SkinManager.Load(@"hitcircle");
                    pTexture hitcircleoverlay = SkinManager.Load(@"hitcircleoverlay");

                    float adjustedSpriteRatio = hitObjectManager.SpriteRatio * 1f / hitcircle.DpiScale;
                    float adjustedSpriteRatioOverlay = hitObjectManager.SpriteRatio * 1f / hitcircleoverlay.DpiScale;
                    if (hitcircle.DpiScale != 1) centre *= 2;
                    if (hitcircleoverlay.DpiScale != 1) centreOverlay *= 2;

                    if (GameBase.D3D)
                    {
                        bool blank = false;
                        if (renderTarget == null || renderTarget.IsDisposed || sliderBody == null || sliderBody.Texture == null)
                        {
                            renderTarget = new RenderTarget2D(GameBase.graphics.GraphicsDevice, GameBase.WindowWidth,
                                                              GameBase.WindowHeight + GameBase.WindowOffsetY, 1, SurfaceFormat.Color);
                            blank = true;
                        }

                        GameBase.graphics.GraphicsDevice.SetRenderTarget(0, renderTarget);

                        if (blank || (SkinManager.SliderStyle == SliderStyle.PeppySliders) || !GameBase.FrameBufferSupported)
                            GameBase.graphics.GraphicsDevice.Clear(Color.TransparentBlack);

                        if (Selected)
                            SkinManager.SliderRenderer.Draw(lineList, null, hitObjectManager.HitObjectRadius * GameBase.GamefieldRatio, index,
                                                            bodySelected ? new Color(229, 44, 44) : new Color(49, 151, 255));
                        else SkinManager.SliderRenderer.Draw(lineList, null, hitObjectManager.HitObjectRadius * GameBase.GamefieldRatio, index);


                        GameBase.spriteBatch.GraphicsDevice.RenderState.AlphaBlendEnable = true;
                        GameBase.graphics.GraphicsDevice.RenderState.SeparateAlphaBlendEnabled = false;
                        GameBase.spriteBatch.GraphicsDevice.RenderState.AlphaSourceBlend = Blend.SourceAlpha;
                        GameBase.spriteBatch.GraphicsDevice.RenderState.AlphaDestinationBlend = Blend.InverseSourceAlpha;

                        //Draw the end-points using a spriteBatch. // TODO: separate endpoint sprites instead of drawing to slider texture.
                        GameBase.spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.None);

                        GameBase.spriteBatch.GraphicsDevice.RenderState.SeparateAlphaBlendEnabled = true;
                        GameBase.spriteBatch.GraphicsDevice.RenderState.AlphaSourceBlend = Blend.One;
                        GameBase.spriteBatch.GraphicsDevice.RenderState.AlphaDestinationBlend = Blend.InverseSourceAlpha;

                        GameBase.spriteBatch.Draw(hitcircle.TextureXna, pos2, null, actualColour, 0, centre, adjustedSpriteRatio, SpriteEffects.None, 0.3f);
                        GameBase.spriteBatch.Draw(hitcircleoverlay.TextureXna, pos2, null, Color.White, 0, centreOverlay, adjustedSpriteRatioOverlay, SpriteEffects.None, 0.4f);
                        GameBase.spriteBatch.Draw(hitcircle.TextureXna, pos1, null, actualColour, 0, centre, adjustedSpriteRatio, SpriteEffects.None, 0.5f);
                        GameBase.spriteBatch.Draw(hitcircleoverlay.TextureXna, pos1, null, Color.White, 0, centreOverlay, adjustedSpriteRatioOverlay, SpriteEffects.None, 0.6f);

                        GameBase.spriteBatch.End();

                        GameBase.graphics.GraphicsDevice.ResolveRenderTarget(0);

                        sliderBody.Texture = new pTexture(renderTarget.GetTexture());
                        sliderBody.Position = new Vector2(left, top);

                        GameBase.graphics.GraphicsDevice.SetRenderTarget(0, null);

                        sliderBody.DrawLeft = drawLeft;
                        sliderBody.DrawTop = drawTop;
                        sliderBody.DrawWidth = Math.Min(GameBase.WindowWidth, drawWidth);
                        sliderBody.DrawHeight = Math.Min(GameBase.WindowHeight + GameBase.WindowOffsetY, drawHeight);
                    }
                    else
                    {
                        int xSize = Math.Min(GameBase.WindowWidth, width);
                        int ySize = Math.Min(GameBase.WindowHeight, height); //size of texture

                        if (GlControl.SurfaceType == Gl.GL_TEXTURE_2D)
                        {
                            xSize = TextureGl.GetPotDimension(xSize);
                            ySize = TextureGl.GetPotDimension(ySize);
                        }

                        GlControl.Viewport = new System.Drawing.Rectangle(drawLeft, drawTop + ySize, drawLeft + xSize, -ySize);
                        GlControl.ResetViewport(false);

                        Gl.glClear(Gl.GL_COLOR_BUFFER_BIT);
                        Gl.glClear(Gl.GL_DEPTH_BUFFER_BIT);

                        if (Selected)
                            SkinManager.SliderRenderer.Draw(lineList, null, hitObjectManager.HitObjectRadius * GameBase.GamefieldRatio, index,
                                                            bodySelected ? new Color(229, 44, 44) : new Color(49, 151, 255));
                        else SkinManager.SliderRenderer.Draw(lineList, null, hitObjectManager.HitObjectRadius * GameBase.GamefieldRatio, index);

                        Vector2 scale = new Vector2(adjustedSpriteRatio, adjustedSpriteRatio);
                        Vector2 scaleOverlay = new Vector2(adjustedSpriteRatioOverlay, adjustedSpriteRatioOverlay);

                        Gl.glColorMask(255, 255, 255, 0);
                        GlControl.SetBlend(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA);

                        hitcircle.TextureGl.Draw(pos2, centre, actualColour, scale, 0, new Rectangle(0, 0, hitcircle.InternalWidth, hitcircle.InternalHeight));
                        hitcircleoverlay.TextureGl.Draw(pos2, centreOverlay, Color.White, scaleOverlay, 0, new Rectangle(0, 0, hitcircleoverlay.InternalWidth, hitcircleoverlay.InternalHeight));

                        hitcircle.TextureGl.Draw(pos1, centre, actualColour, scale, 0, new Rectangle(0, 0, hitcircle.InternalWidth, hitcircle.InternalHeight));
                        hitcircleoverlay.TextureGl.Draw(pos1, centreOverlay, Color.White, scaleOverlay, 0, new Rectangle(0, 0, hitcircleoverlay.InternalWidth, hitcircleoverlay.InternalHeight));

                        Gl.glColorMask(0, 0, 0, 255);
                        GlControl.SetBlend(Gl.GL_ONE, Gl.GL_ONE_MINUS_SRC_ALPHA);

                        hitcircle.TextureGl.Draw(pos2, centre, actualColour, scale, 0, new Rectangle(0, 0, hitcircle.InternalWidth, hitcircle.InternalHeight));
                        hitcircleoverlay.TextureGl.Draw(pos2, centreOverlay, Color.White, scaleOverlay, 0, new Rectangle(0, 0, hitcircleoverlay.InternalWidth, hitcircleoverlay.InternalHeight));

                        hitcircle.TextureGl.Draw(pos1, centre, actualColour, scale, 0, new Rectangle(0, 0, hitcircle.InternalWidth, hitcircle.InternalHeight));
                        hitcircleoverlay.TextureGl.Draw(pos1, centreOverlay, Color.White, scaleOverlay, 0, new Rectangle(0, 0, hitcircleoverlay.InternalWidth, hitcircleoverlay.InternalHeight));

                        Gl.glColorMask(255, 255, 255, 255);

                        int textureId = 0;
                        int[] textures = new int[1];
                        Gl.glGenTextures(1, textures);
                        textureId = textures[0];

                        GlControl.Bind(textureId);
                        Gl.glTexParameteri(GlControl.SurfaceType, Gl.GL_TEXTURE_MIN_FILTER, (int)Gl.GL_LINEAR);
                        Gl.glTexParameteri(GlControl.SurfaceType, Gl.GL_TEXTURE_MAG_FILTER, (int)Gl.GL_LINEAR);

                        Gl.glCopyTexImage2D(GlControl.SurfaceType, 0, Gl.GL_RGBA, 0, 0, xSize, ySize, 0);

                        TextureGl gl = new TextureGl(xSize, ySize);
                        gl.SetData(textureId);
                        sliderBody.Texture = new pTexture(null, gl, xSize, ySize);

                        sliderBody.Position = new Vector2(left, top);

                        //restore viewport
                        GlControl.ResetViewport();
                    }
                }
            }
            else
                RemoveBody();
        }

        internal void RemoveBody()
        {
            if (sliderBody != null && sliderBody.Texture != null)
            {
                sliderBody.Texture.Dispose();
                sliderBody.Texture = null;
            }

            drawnSegments = 0;
            snakingProgress = 0;
        }

        #endregion

        #region Placement

        internal bool AllowNewPlacing;
        private bool placingPending;
        private Vector2 placingPoint;
        internal bool placingValid;

        private bool successfulPlace;
        private int successfulPlaceLength;
        internal bool unifiedSoundAddition = true;

        private bool bodySelected;
        internal bool BodySelected
        {
            get { return bodySelected; }
            set
            {
                if (value == bodySelected)
                    return;

                bodySelected = value;
                if (sliderBody != null)
                    sliderBody.Dispose();
            }
        }

        internal override void SetColour(Color color)
        {
            if (spriteManager != null && color != Colour)
            {
                sliderStartCircle.SetColour(color);
                foreach (HitCircleSliderEnd c in sliderEndCircles)
                    c.SetColour(color);

                RemoveBody();

                Colour = color;

                if ((sliderBall.Texture.Source == SkinSource.Osu || SkinManager.Current.AllowSliderBallTint) && ConfigManager.sComboColourSliderBall)
                    sliderBall.InitialColour = color;

                base.SetColour(color);
                ApplyDim();
            }
        }

        internal override void Select()
        {
            if (sliderBody != null)
                sliderBody.Dispose();

            if (!sliderStartCircle.Selected)
            {
                sliderStartCircle.SpriteSelectionCircle.InitialColour = Color.White;
                sliderStartCircle.Select();
                //Use a blue highlight in editor when not selecting a specific endpoint.
            }

            if (sliderEndCircles.Count > 0 && !sliderEndCircles[0].Selected)
            {
                sliderEndCircles[0].SpriteSelectionCircle.InitialColour = Color.White;
                sliderEndCircles[0].Select();
                //Do the same for the "other end"
            }
        }

        internal override void Deselect()
        {
            bodySelected = false;
            if (sliderBody != null)
                sliderBody.Dispose();
            sliderAllCircles.ForEach(hc =>
            {
                hc.Selected = false;
                hc.Deselect();
            });
        }

        internal override void ModifyTime(int newTime)
        {
            ModifySliderTime(newTime, true);
        }

        internal void ModifySliderTime(int newTime, bool doUpdate)
        {
            sliderStartCircle.ModifyTime(newTime);
            StartTime = newTime;

            if (doUpdate)
                UpdateCalculations();
            else
            {
                //Special case to force update at next hitobjectmanager update.
                EndTime = newTime;
            }
        }

        internal override void ModifyPosition(Vector2 newPosition)
        {
            Vector2 change = newPosition - Position;

            for (int i = 0; i < sliderCurvePoints.Count; i++)
                sliderCurvePoints[i] += change;
            if (sliderCurveSmoothLines != null)
                for (int i = 0; i < sliderCurveSmoothLines.Count; i++)
                {
                    sliderCurveSmoothLines[i].p1 += change;
                    sliderCurveSmoothLines[i].p2 += change;
                }


            foreach (pSprite p in SpriteCollection)
            {
                if (p.Field == Fields.Gamefield)
                {
                    p.InitialPosition += change;
                    p.Position += change;
                }
                else
                {
                    p.InitialPosition += change * GameBase.WindowRatio * GameBase.GamefieldRatioWindowIndependent;
                    p.Position += change * GameBase.WindowRatio * GameBase.GamefieldRatioWindowIndependent;
                }
            }

            if (sliderBall != null)
                foreach (Transformation t in sliderBall.Transformations)
                    if (t.Type == TransformationType.Movement)
                    {
                        t.StartVector += change;
                        t.EndVector += change;
                    }

            Position = newPosition;
            Position2 += change;

            //sliderStartCircle.Position = newPosition;
            sliderStartCircle.ModifyPosition(newPosition);
            sliderEndCircles.ForEach(s => s.ModifyPosition(s.Position + change));

            EndPosition += change;
        }

        internal void AdjustRepeats2(int time)
        {
            int slideTime = SegmentLength;

            if (slideTime == 0 || time <= StartTime || sliderCurvePoints.Count < 2)
                return;

            int i = 1;
            int addedTime = StartTime + slideTime;
            while (addedTime < time)
            {
                addedTime += slideTime;
                i++;
            }

            if (SegmentCount != i)
            {
                SegmentCount = i;
                UpdateCalculations();
            }

            DisableNodalSamples();
        }

        internal void AdjustRepeats(int time)
        {
            int slideTime = SegmentLength;

            if (slideTime == 0 || time <= StartTime)
                return;

            int i = 1;
            int addedTime = StartTime + (int)(slideTime * 1.5);
            while (addedTime < time)
            {
                addedTime += slideTime;
                i++;
            }

            if (SegmentCount != i)
            {
                SegmentCount = i;
                UpdateCalculations();
            }

            //DisableNodalSamples();
            UpdateNodalSamples();
        }

        internal void PlacePoint(bool last, bool remove)
        {
            CurveType = CurveTypes.PerfectCurve;

            if (sliderCurvePoints.Count > 1)
            {
                if (!placingValid && !last && remove)
                    sliderCurvePoints.RemoveAt(sliderCurvePoints.Count - 1);
                else if (placingValid && sliderCurveSmoothLines != null)
                {
                    placingValid = false;
                    successfulPlace = true;
                }
                else if (!remove && !placingValid &&
                         sliderCurvePoints[sliderCurvePoints.Count - 1] !=
                         sliderCurvePoints[sliderCurvePoints.Count - 2])
                {
                    sliderCurvePoints.Add(sliderCurvePoints[sliderCurvePoints.Count - 1]);
                    placingValid = false;
                    successfulPlace = true;
                }
            }

            if (last) CurveType = CurveTypeSavable;
            AllowNewPlacing = !last;

            UpdateCalculations();
        }

        internal void PlacePointNext(Vector2 sliderPlacementPoint)
        {
            if (!AllowNewPlacing)
                return;

            CurveType = CurveTypes.PerfectCurve;

            if (placingValid && sliderCurvePoints[sliderCurvePoints.Count - 1] == placingPoint)
                sliderCurvePoints.RemoveAt(sliderCurvePoints.Count - 1);

            placingPoint = sliderPlacementPoint;
            placingValid = false;
            placingPending = true;
        }

        #endregion

        #region Positioning

        internal int timeAtLength(float length)
        {
            return (int)(StartTime + (length / Velocity) * 1000);
        }

        internal Vector2 currentPosition()
        {
            return PositionAtTime(AudioEngine.Time);
        }

        public override Vector2 PositionAtTime(int time)
        {
            if (sliderCurveSmoothLines == null) UpdateCalculations();

            if (time < StartTime || time > EndTime) return base.PositionAtTime(time);

            float pos = (time - StartTime) / ((float)Length / SegmentCount);

            if (pos % 2 > 1)
                pos = 1 - (pos % 1);
            else
                pos = (pos % 1);

            float lengthRequired = (float)(SpatialLength * pos);
            return positionAtLength(lengthRequired);
        }

        internal Vector2 positionAtLength(float length)
        {
            if (sliderCurveSmoothLines == null) UpdateCalculations();
            // todo: use cumulativeLengths binary search for this. (like stream)

            float lengthCurrent = 0;
            int i = 0;
            foreach (Line l in sliderCurveSmoothLines)
            {
                if (lengthCurrent >= length)
                    break;
                lengthCurrent += Vector2.Distance(l.p1, l.p2);
                i++;
            }

            if (sliderCurveSmoothLines.Count == 0)
                return Position;

            if (i == 0)
                return sliderCurveSmoothLines[i].p1;

            Vector2 linearAdd = (sliderCurveSmoothLines[i - 1].p2 - sliderCurveSmoothLines[i - 1].p1)
                                *
                                (1 -
                                 (Math.Abs(length - lengthCurrent)) /
                                 Vector2.Distance(sliderCurveSmoothLines[i - 1].p1,
                                                  sliderCurveSmoothLines[i - 1].p2));

            return sliderCurveSmoothLines[i - 1].p1 + linearAdd;
        }

        internal bool IsRetarded()
        {
            if (sliderCurveSmoothLines == null || cumulativeLengths == null)
                return false;

            float legalDistance = hitObjectManager.HitObjectRadius * 1.0f;
            float scanDistance = hitObjectManager.HitObjectRadius * 4.0f;

            for (int x = 0; x < sliderCurveSmoothLines.Count; x++)
            {
                Vector2 current = sliderCurveSmoothLines[x].p2;
                int lower_index = cumulativeLengths.FindLastIndex(d => d < cumulativeLengths[x] - scanDistance);
                int lower_bound = Math.Max(0, lower_index);
                int y;

                for (y = x; y >= lower_bound; y--)
                {
                    if (Vector2.Distance(current, sliderCurveSmoothLines[y].p1) >= legalDistance) break;
                }

                if (y + 1 == lower_index) return true;
            }

            return false;
        }

        #endregion

        #region Scoring

        private MouseButtons downButton = MouseButtons.None; //The mouse button pressed to begin sliding.
        internal int sliderTicksHit;
        private int InitTime;
        internal bool IsSliding;
        private int lastTickSounded;
        internal int sliderTicksMissed;
        internal bool[] sliderTicksHitStatus;
        internal List<int> sliderRepeatPoints = new List<int>();

        internal List<int> sliderScoreTimingPoints = new List<int>();
        public List<HitObjectSoundType> SoundTypeList = new List<HitObjectSoundType>();
        public List<SampleSet> SampleSetList = new List<SampleSet>();
        public List<SampleSet> SampleSetAdditionList = new List<SampleSet>();
        internal double Velocity;

        internal override Vector2 Position2 { get; set; }

        internal virtual bool StartIsHit
        {
            get { return sliderStartCircle.IsHit; }
        }

        internal virtual IncreaseScoreType HitStart()
        {
            if (InputManager.leftCond || InputManager.rightCond)
                //If no button is registered as the slider-down butt, register one now
                downButton = InputManager.leftCond
                                 ? MouseButtons.Left
                                 : InputManager.rightCond ? MouseButtons.Right : MouseManager.MouseDownButton;
            return sliderStartCircle.Hit();
        }

        internal override IncreaseScoreType Hit()
        {
            IsHit = true;
            StopSound();

            if (sliderStartCircle.hitValue > 0)
                sliderTicksHit++;

            if (IsSliding)
            {
                HitCircleSliderEnd h = new HitCircleSliderEnd(hitObjectManager, (EndPosition == Position ? Position2 : Position), EndTime, EndTime, false, 0, this);
                h.SetColour(sliderStartCircle.SpriteHitCircle1.InitialColour);
                h.SpriteCollection.ForEach(s => s.Clock = Clocks.AudioOnce);
                h.Arm(true);
                spriteManager.Add(h.SpriteCollection);
            }

            double hitFraction = (double)sliderTicksHit / (sliderScoreTimingPoints.Count + 1);

            if (hitFraction > 0)
                PlaySound();

            if (hitFraction == 1)
                return IncreaseScoreType.Hit300;
            if (hitFraction >= 0.5)
                return IncreaseScoreType.Hit100;
            if (hitFraction > 0)
                return IncreaseScoreType.Hit50;
            return IncreaseScoreType.Miss;
        }

        internal override void PlaySound()
        {
            // endIndex == 0 tells us to use the i46 fix so it must have the correct value even when unifiedSoundAddition is true.
            PlaySound(false, AudioEngine.Time > StartTime + Length / 2 ? SoundTypeList.Count - 1 : 0);
            //This crap is here because edit mode autoplay is pretty crap itself.
        }

        protected override float PositionalSound { get { return (sliderBall.Position.X / 512f - 0.5f) * 0.8f; } }

        internal void PlaySound(bool tickOnly, int endIndex)
        {
            if (AudioEngine.Time <= EndTime)
                SliderSoundStart();

            if (tickOnly)
            {
                ControlPoint pt = hitObjectManager.Beatmap.controlPointAt(sliderScoreTimingPoints[endIndex] + 2);
                SampleSet ss = SampleSet == SampleSet.None ? pt.sampleSet : SampleSet;
                AudioEngine.PlayTickSamples(new HitSoundInfo(SoundType, ss, pt.customSamples, pt.volume, SampleSetAdditions == Audio.SampleSet.None ? ss : SampleSetAdditions), PositionalSound);
            }
            else
            {
                HitSoundInfo hitSoundInfo = GetHitSoundInfo(endIndex);
                HitObjectManager.OnHitSound(hitSoundInfo);
                AudioEngine.PlayHitSamples(hitSoundInfo, PositionalSound, true);
            }
        }

        internal HitSoundInfo GetHitSoundInfo(int endIndex)
        {
            ControlPoint pt = null;
            HitObjectSoundType toPlay;
            SampleSet ss, ssa;

            if (endIndex == 0)
            {
                // The slider start is i46-proof.
                pt = hitObjectManager.Beatmap.controlPointAt(StartTime + 5);

                if (unifiedSoundAddition)
                {
                    toPlay = SoundType;
                    ss = SampleSet;
                    ssa = SampleSetAdditions;
                }
                else
                {
                    if (SoundTypeList.Count == 0)
                        toPlay = SoundType;
                    else
                        toPlay = SoundTypeList[0];

                    if (SampleSetList.Count == 0 || (hitObjectManager.Beatmap.BeatmapVersion < 14 && SampleSetList[0] == SampleSet.None))
                        ss = SampleSet;
                    else
                        ss = SampleSetList[0];

                    if (SampleSetAdditionList.Count == 0)
                        ssa = SampleSetAdditions;
                    else
                        ssa = SampleSetAdditionList[0];
                }
            }
            else
            {
                // Slider rebounds & ends are i46-proof too now.
                if ((endIndex >= this.SegmentCount) || (endIndex == -1))
                    // Add 2ms fudge to avoid rounding error: Rebounds placed exactly on section rollovers should use the NEW section's sampleset.
                    pt = hitObjectManager.Beatmap.controlPointAt(EndTime + 5);
                else
                    pt = hitObjectManager.Beatmap.controlPointAt(sliderRepeatPoints[endIndex - 1] + 5);

                if (unifiedSoundAddition)
                {
                    toPlay = SoundType;
                    ss = SampleSet;
                    ssa = SampleSetAdditions;
                }
                else
                {
                    if (endIndex >= SoundTypeList.Count)
                        toPlay = SoundType;
                    else
                        toPlay = SoundTypeList[endIndex];

                    if (endIndex >= SampleSetList.Count)
                        ss = SampleSet;
                    else
                        ss = SampleSetList[endIndex];

                    if (endIndex >= SampleSetAdditionList.Count)
                        ssa = SampleSetAdditions;
                    else
                        ssa = SampleSetAdditionList[endIndex];
                }

            }

            if (ss == Audio.SampleSet.None) ss = pt.sampleSet;
            if (ssa == Audio.SampleSet.None) ssa = ss;

            return HitSoundInfo.GetLayered(toPlay, ss, pt.customSamples, pt.volume, ssa);
        }

        internal void SliderSoundStart()
        {
            SampleSet slideSampleSet = SampleSet.None;
            SampleSet slideSampleSetAdditions = SampleSet.None;

            if (hitObjectManager.Beatmap.BeatmapVersion >= 14)
            {
                slideSampleSet = SampleSet;
                slideSampleSetAdditions = SampleSetAdditions;
            }

            AudioEngine.PlaySlideSamples(SoundType, slideSampleSet, slideSampleSetAdditions, SkinManager.Current.LayeredHitSounds, PositionalSound * 0.4f);
        }

        internal override void StopSound()
        {
            AudioEngine.StopSlideSamples();
        }

        internal void InitSlide()
        {
            InitTime = AudioEngine.Time;

            sliderFollower.Transformations.RemoveAll(
                delegate(Transformation t) { return t.Type != TransformationType.Movement; });

            int time;
            if (GameBase.Mode == OsuModes.Edit)
                time = StartTime;
            else
                time = Math.Max(AudioEngine.Time, StartTime);

            sliderFollower.Transformations.Add(
                new Transformation(TransformationType.Fade, 0, 1, time, Math.Min(EndTime, time + 60)));
            sliderFollower.Transformations.Add(
                new Transformation(TransformationType.Scale, 0.5F, 1, time, Math.Min(EndTime, time + 180),
                                   EasingTypes.Out));

            sliderFollower.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, EndTime, EndTime + 200,
                                                                  EasingTypes.In));
            sliderFollower.Transformations.Add(
                new Transformation(TransformationType.Scale, 1, 0.8f, EndTime, EndTime + 200, EasingTypes.Out));

            int count = sliderScoreTimingPoints.Count;
            int fadeout = 200;

            int delay = count > 1 ? Math.Min(fadeout, sliderScoreTimingPoints[1] - sliderScoreTimingPoints[0]) : fadeout;
            float ratio = 1.1f - ((float)delay / fadeout) * 0.1f;
            if (count > 0)
                foreach (int i in sliderScoreTimingPoints.GetRange(0, count - 1))
                    sliderFollower.Transformations.Add(
                        new Transformation(TransformationType.Scale, 1.1F, ratio, i,
                                           Math.Min(EndTime, i + delay)));

            sliderEndCircles.ForEach(delegate(HitCircleSliderEnd h) { h.Arm(true); });

            IsSliding = true;
        }

        internal void KillSlide()
        {
            if (!IsSliding)
                return;

            sliderFollower.Transformations.RemoveAll(
                delegate(Transformation t) { return t.Type != TransformationType.Movement; });

            int nextScorePoint = EndTime;
            foreach (int i in sliderScoreTimingPoints)
                if (i > AudioEngine.Time)
                {
                    nextScorePoint = i;
                    break;
                }

            sliderFollower.Transformations.Add(
                new Transformation(TransformationType.Fade, 1, 0, nextScorePoint - 100, nextScorePoint));
            sliderFollower.Transformations.Add(
                new Transformation(TransformationType.Scale, 1, 2, nextScorePoint - 100, nextScorePoint));

            if (!IsHit)
                sliderEndCircles.ForEach(delegate(HitCircleSliderEnd h) { h.Disarm(); });


            IsSliding = false;
        }

        internal override IncreaseScoreType GetScorePoints(Vector2 currentMousePos)
        {
            if (IsHit || AudioEngine.Time < StartTime)
                return IncreaseScoreType.Ignore;

            bool allowable = false;

            bool mouseDownAcceptable = false;
            bool mouseDownAcceptableSwap = MouseManager.GameDownState &&
                                           !(MouseManager.LastButton == (MouseButtons.Left | MouseButtons.Right) &&
                                             MouseManager.LastButton2 == MouseManager.MouseDownButton);

            if (MouseManager.GameDownState)
            {
                if (downButton == MouseButtons.None ||
                    (MouseManager.MouseDownButton != (MouseButtons.Left | MouseButtons.Right) && mouseDownAcceptableSwap))
                {
                    downButton = InputManager.leftCond
                                     ? MouseButtons.Left
                                     : InputManager.rightCond ? MouseButtons.Right : MouseManager.MouseDownButton;
                    mouseDownAcceptable = true;
                }
                else if ((MouseManager.MouseDownButton & downButton) > 0)
                    //Otherwise check if the correct button is down.
                    mouseDownAcceptable = true;
            }
            else
                downButton = MouseButtons.None;

            mouseDownAcceptable |= mouseDownAcceptableSwap | Player.Relaxing;

            if (mouseDownAcceptable)
            {
                //TODO: mm, this can't be good
                float radius = (IsSliding ? hitObjectManager.HitObjectRadius * 2.4F : hitObjectManager.HitObjectRadius);
                int offsetTime = AudioEngine.Time - (InputManager.CurrentCursorHandler is EloTouchscreenHandler && !InputManager.IsReplayMovement ? (int)ConfigManager.sTouchDragOffset : 0);

                //DON'T FUCKIGN TOUCH THIS
                Transformation f = sliderBall.Transformations.Find(t => t.Time1 <= offsetTime &&
                                                                        t.Time2 >= offsetTime);
                if (f == null && offsetTime > sliderBall.Transformations[sliderBall.Transformations.Count - 1].Time2)
                    f = sliderBall.Transformations[sliderBall.Transformations.Count - 1];

                //Special case for when drag offset causes timing to end up before the slider begins.
                if (InputManager.CurrentCursorHandler is EloTouchscreenHandler && !InputManager.IsReplayMovement && f == null && offsetTime < StartTime && AudioEngine.Time >= StartTime)
                    f = sliderBall.Transformations[0];

                if (f != null)
                {
                    Vector2 pos;
                    if (f.Time2 == f.Time1)
                        pos = f.EndVector;
                    else
                        pos = f.StartVector +
                              (f.EndVector - f.StartVector) *
                              (1 - (float)(f.Time2 - offsetTime) / (f.Time2 - f.Time1));

                    allowable = Vector2.DistanceSquared(GameBase.DisplayToGamefield(currentMousePos), pos) < radius * radius;
                }
            }

            if (allowable && !IsSliding)
            {
                InitSlide();
            }

            IncreaseScoreType score = IncreaseScoreType.Ignore;
            int pointCount = 0;

            while (pointCount < sliderScoreTimingPoints.Count && sliderScoreTimingPoints[pointCount] <= AudioEngine.Time)
                pointCount++;

            if (pointCount < sliderTicksHit + sliderTicksMissed && AudioEngine.IsReversed)
            {
                bool lastHit = true;
                //rewind logic.
                sliderTicksHit = 0;
                sliderTicksMissed = 0;

                for (int i = 0; i < pointCount; i++)
                    if (sliderTicksHitStatus[i])
                    {
                        sliderTicksHit++;
                        lastHit = true;
                    }
                    else
                    {
                        sliderTicksMissed++;
                        lastHit = false;
                    }

                if (lastHit)
                    InitSlide();
                else
                    KillSlide();
            }
            else
            {
                if (sliderTicksHit + sliderTicksMissed < pointCount)
                {
                    int index = sliderTicksHit + sliderTicksMissed;

                    if (allowable && InitTime <= sliderScoreTimingPoints[index])
                    {
                        sliderTicksHit++;
                        sliderTicksHitStatus[index] = true;

                        if (sliderScoreTimingPoints.Count == pointCount)
                            score = IncreaseScoreType.SliderEnd;
                        else if (pointCount % (sliderScoreTimingPoints.Count / SegmentCount) == 0)
                            score = IncreaseScoreType.SliderRepeat;
                        else
                            score = IncreaseScoreType.SliderTick;
                    }
                    else
                    {
                        sliderTicksMissed++;
                        sliderTicksHitStatus[index] = false;
                        if (sliderTicksHit + sliderTicksMissed == sliderScoreTimingPoints.Count)
                            score = IncreaseScoreType.MissHpOnlyNoCombo;
                        else
                            score = IncreaseScoreType.MissHpOnly;
                    }
                }
            }

            if (!allowable && IsSliding && sliderTicksHit + sliderTicksMissed < sliderScoreTimingPoints.Count)
                KillSlide();

            return score;
        }

        internal void EnableNodalSamples()
        {
            if (!unifiedSoundAddition) return;

            //fill list
            for (int i = 0; i < SegmentCount + 1; i++)
            {
                SoundTypeList.Add(SoundType);
                SampleSetList.Add(SampleSet);
                SampleSetAdditionList.Add(SampleSetAdditions);
            }

            //disabled unified samples.
            unifiedSoundAddition = false;
        }

        internal void DisableNodalSamples()
        {
            unifiedSoundAddition = true;

            SampleSet = Audio.SampleSet.None;
            SampleSetAdditions = Audio.SampleSet.None;

            SoundTypeList.Clear();
            SampleSetList.Clear();
            SampleSetAdditionList.Clear();
        }

        // Adjust sampleset and addition data to match the amount of slider circles
        internal void UpdateNodalSamples()
        {
            if (unifiedSoundAddition)
                return;

            int countDiff = (sliderAllCircles.Count - SampleSetList.Count);

            if (countDiff > 0) // There are more circles in list
            {
                for (int i = 0; i < countDiff; i++)
                {
                    SoundTypeList.Add(SoundType);
                    SampleSetList.Add(SampleSet);
                    SampleSetAdditionList.Add(SampleSetAdditions);
                }
            }
            else if (countDiff < 0) // There are less
            {
                countDiff = countDiff * -1;
                for (int i = 0; i < countDiff; i++)
                {
                    SoundTypeList.RemoveAt(SoundTypeList.Count - 1);
                    SampleSetList.RemoveAt(SampleSetList.Count - 1);
                    SampleSetAdditionList.RemoveAt(SampleSetAdditionList.Count - 1);
                }
            }

            // Check if nodal samples are still necessary

            if (SampleSet != Audio.SampleSet.None ||
                SampleSetAdditions != Audio.SampleSet.None)
                return;

            for (int i = 0; i < SampleSetList.Count; i++)
            {
                if (SoundTypeList[i] != SoundType ||
                    SampleSetList[i] != SampleSet ||
                    SampleSetAdditionList[i] != SampleSetAdditions) return;
            }

            DisableNodalSamples();
        }

        #endregion

        internal void MovePoint(Vector2 newPoint, int index)
        {
            Vector2 selectedPoint = sliderCurvePoints[index];

            //If red point, move both in unison
            bool IsRedPoint = index < sliderCurvePoints.Count - 1 &&
                sliderCurvePoints[index + 1] ==
                sliderCurvePoints[index];

            if (IsRedPoint)
                sliderCurvePoints[index + 1] = newPoint;
            sliderCurvePoints[index] = newPoint;

            //We need to update slider position when modifying the first point
            if (index == 0)
            {
                Position = newPoint;
                sliderStartCircle.ModifyPosition(newPoint);
            }

            SpatialLength = 0;
            UpdateCalculations();

            //Slider length is too short. Restore original slider point.
            if (SpatialLength == 0)
            {
                sliderCurvePoints[index] = selectedPoint;
                if (index == 0)
                {
                    Position = selectedPoint;
                    sliderStartCircle.ModifyPosition(selectedPoint);
                }
                SpatialLength = 0;
                UpdateCalculations();
            }

            //We want to deselect the end when a slider point is moved to avoid weird effects when the slider updates.
            if (index >= 0 && sliderEndCircles.Count > 0)
                sliderEndCircles[0].Deselect();
        }

        internal void PlacePoint(int foundIndex, Vector2 p3)
        {
            sliderCurvePoints.Insert(foundIndex, p3);
            CurveType = CurveTypes.PerfectCurve;
            SpatialLength = 0;
            UpdateCalculations();
        }

        internal void Reverse()
        {
            if (CurveType == CurveTypes.PerfectCurve && sliderCurvePoints.Count == 3)
            {
                Vector2 centre;
                float radius;
                double t_initial, t_final;

                OsuMathHelper.CircleThroughPoints(sliderCurvePoints[0],
                    sliderCurvePoints[1], sliderCurvePoints[2], out centre, out radius, out t_initial, out t_final);

                bool backwards = t_initial > t_final;
                t_final = OsuMathHelper.CircleTAt(Position2, centre); // correct starting point to old end point
                while (t_final < t_initial) t_final += 2 * Math.PI;
                if (backwards) t_final -= 2 * Math.PI;

                Vector2 start = OsuMathHelper.CirclePoint(centre, radius, t_final);
                ModifyPosition(start);
                sliderCurvePoints[0] = start;
                sliderCurvePoints[1] = OsuMathHelper.CirclePoint(centre, radius, (t_final + t_initial) * 0.5);
                sliderCurvePoints[2] = OsuMathHelper.CirclePoint(centre, radius, t_initial);
            }
            else
            {
                List<Vector2> oldCurve = new List<Vector2>(sliderCurvePoints);

                oldCurve.Reverse();

                if ((curveLength + 1.0d <= SpatialLength) || (curveLength - 1.0d >= SpatialLength))
                    oldCurve[0] = Position2;

                ModifyPosition(oldCurve[0]);
                sliderCurvePoints = oldCurve;
            }

            UpdateCalculations();

        }
    }

    internal enum CurveTypes
    {
        Catmull,
        Bezier,
        Linear,
        PerfectCurve
    } ;
}