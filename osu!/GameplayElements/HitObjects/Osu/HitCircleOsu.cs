using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Scoring;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu.GameplayElements.Beatmaps;
using osu.Graphics;
using osu.Configuration;

namespace osu.GameplayElements.HitObjects.Osu
{
    internal class HitCircleOsu : HitCircle
    {
        #region General & Timing

        private int comboNumber;
        private const float TEXT_SIZE = 0.8f;

        internal virtual string SpriteNameHitCircle { get { return @"hitcircle"; } }

        internal HitCircleOsu(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo)
            : this(hom, startPosition, startTime, newCombo, false, false, false, 0)
        {
        }

        internal HitCircleOsu(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType, int comboOffset)
            : this(hom, startPosition, startTime, newCombo, soundType.IsType(HitObjectSoundType.Whistle), soundType.IsType(HitObjectSoundType.Finish), soundType.IsType(HitObjectSoundType.Clap), comboOffset)
        {
        }

        protected override void Dispose(bool isDisposing)
        {
            base.Dispose(isDisposing);
        }

        internal HitCircleOsu(HitObjectManager hom, Vector2 pos, int startTime, bool newCombo, bool addWhistle, bool addFinish, bool addClap, int comboOffset)
            : base(hom)
        {
            Position = pos;
            StartTime = startTime;
            EndTime = startTime;

            Type = HitObjectType.Normal;
            SoundType = HitObjectSoundType.None;
            SampleSet = Audio.SampleSet.None;
            SampleSetAdditions = Audio.SampleSet.None;

            int p = LocalPreEmpt;

            if (newCombo)
            {
                Type |= HitObjectType.NewCombo;
                ComboOffset = comboOffset;
            }
            if (addWhistle)
                SoundType |= HitObjectSoundType.Whistle;
            if (addFinish)
                SoundType |= HitObjectSoundType.Finish;
            if (addClap)
                SoundType |= HitObjectSoundType.Clap;


            if (hom.spriteManager != null)
            {
                bool hidden = ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.Hidden);
                bool hasApproachCircle = ShowApproachCircle && (!hidden || (hom.hitObjects.Count == 0 && ConfigManager.sHiddenShowFirstApproach));

                Color white = Color.White;

                pTexture t_approachCircle = SkinManager.Load(@"approachcircle");
                pTexture t_hit1 = SkinManager.Load(SpriteNameHitCircle);
                pTexture[] t_hit2 = SkinManager.LoadAll(SpriteNameHitCircle + @"overlay");

                SpriteApproachCircle =
                    new pSprite(t_approachCircle, Fields.Gamefield, Origins.Centre,
                                Clocks.Audio, Position, SpriteManager.drawOrderFwdPrio(StartTime - p), false, white);
                if (hasApproachCircle)
                    SpriteCollection.Add(SpriteApproachCircle);
                SpriteApproachCircle.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.9F, startTime - p, Math.Min(startTime, startTime - p + HitObjectManager.FadeIn * 2)));
                SpriteApproachCircle.Transformations.Add(new Transformation(TransformationType.Scale, 4, 1, startTime - p, startTime));

                SpriteHitCircle1 = new pSprite(t_hit1, Fields.Gamefield, Origins.Centre, Clocks.Audio, Position, SpriteManager.drawOrderBwd(StartTime), false, white) { TagNumeric = 1 };
                SpriteCollection.Add(SpriteHitCircle1);
                DimCollection.Add(SpriteHitCircle1);


                SpriteHitCircle2 = new pAnimation(t_hit2, Fields.Gamefield, Origins.Centre, Clocks.Audio, Position,
                    SpriteManager.drawOrderBwd(StartTime - (hitObjectManager.ShowOverlayAboveNumber ? 2 : 1)), false, Color.White);
                SpriteHitCircle2.FrameDelay = 1000 / 2F;
                SpriteCollection.Add(SpriteHitCircle2);
                DimCollection.Add(SpriteHitCircle2);

                SpriteHitCircleText = new pSpriteText(null, SkinManager.Current.FontHitCircle, SkinManager.Current.FontHitCircleOverlap, Fields.Gamefield, Origins.Centre,
                                                      Clocks.Audio, Position, SpriteManager.drawOrderBwd(StartTime - (hitObjectManager.ShowOverlayAboveNumber ? 1 : 2)), false, Color.White);
                SpriteHitCircleText.Scale = TEXT_SIZE;

                if (ShowCircleText)
                    SpriteCollection.Add(SpriteHitCircleText);

                Transformation t;

                //fade in
                if (hidden)
                    t = new Transformation(TransformationType.Fade, 0, 1, startTime - p, startTime - (int)(p * 0.6));
                else
                    t = new Transformation(TransformationType.Fade, 0, 1, startTime - p, startTime - p + HitObjectManager.FadeIn);

                SpriteHitCircle1.Transformations.Add(t.Clone());
                SpriteHitCircle2.Transformations.Add(t.Clone());
                SpriteHitCircleText.Transformations.Add(t.Clone());

                //fade out
                if (GameBase.Mode == OsuModes.Edit)
                {
                    SpriteSelectionCircle =
                        new pSprite(SkinManager.Load(@"hitcircleselect"), Fields.Gamefield,
                                    Origins.Centre, Clocks.Game, Position,
                                    SpriteManager.drawOrderBwd(StartTime - 3), true, Color.TransparentWhite);
                    SpriteCollection.Add(SpriteSelectionCircle);

                    if (ConfigManager.sEditorHitAnimations)
                    {
                        Arm(true);
                    }
                    else
                    {
                        t = new Transformation(TransformationType.Fade, 1, 0, startTime, startTime + hitObjectManager.ForceFadeOut);
                        SpriteHitCircle1.Transformations.Add(t.Clone());
                        SpriteHitCircle2.Transformations.Add(t.Clone());
                        SpriteHitCircleText.Transformations.Add(t.Clone());
                        SpriteApproachCircle.Transformations.Add(t.Clone());
                    }

                    SpriteApproachCircle.Transformations.Add(new Transformation(TransformationType.Scale, 1, 1.1F, startTime, startTime + 100));
                }
                else
                {
                    Disarm();
                }
            }

        }

        protected virtual bool ShowCircleText
        {
            get { return true; }
        }

        protected virtual bool ShowApproachCircle
        {
            get { return true; }
        }

        internal override void SetEndTime(int time)
        {
            throw new Exception();
        }

        internal override HitObject Clone()
        {
            HitCircleOsu h = new HitCircleOsu(hitObjectManager, Position, StartTime, Type.IsType(HitObjectType.NewCombo),
                                              SoundType.IsType(HitObjectSoundType.Whistle),
                                              SoundType.IsType(HitObjectSoundType.Finish),
                                              SoundType.IsType(HitObjectSoundType.Clap), ComboOffset);

            h.SetColour(Colour);
            h.ComboNumber = ComboNumber;
            h.Selected = Selected;
            h.SampleSet = SampleSet;
            h.SampleSetAdditions = SampleSetAdditions;
            h.CustomSampleSet = CustomSampleSet;
            h.SampleVolume = SampleVolume;
            h.SampleAddr = SampleAddr;
            h.SampleFile = SampleFile;
            return h;
        }

        internal override void ModifyPosition(Vector2 newPosition)
        {
            Position = newPosition;

            foreach (pSprite s in SpriteCollection)
                s.Position = newPosition;
        }

        internal override void ModifyTime(int newTime)
        {
            int difference = newTime - StartTime;
            StartTime = newTime;
            EndTime = StartTime;
            SpriteApproachCircle.TimeWarp(difference);
            SpriteHitCircle1.TimeWarp(difference);
            SpriteHitCircle2.TimeWarp(difference);
            SpriteHitCircleText.TimeWarp(difference);
            SpriteSelectionCircle.TimeWarp(difference);
        }

        internal override void Select()
        {
            SpriteSelectionCircle.FadeIn(50);
        }

        internal override void Deselect()
        {
            SpriteSelectionCircle.FadeOut(50);
        }

        internal override void Disarm()
        {
            base.Disarm();

            bool hidden = ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.Hidden);
            bool hasApproachCircle = ShowApproachCircle && (!hidden || hitObjectManager.hitObjects.Count == 0);

            Transformation fadeout = hidden ?
                new Transformation(TransformationType.Fade, 1, 0, StartTime - (int)(LocalPreEmpt * 0.6), StartTime - (int)(LocalPreEmpt * 0.3)) { TagNumeric = ARMED } :
                new Transformation(TransformationType.Fade, 1, 0, StartTime + hitObjectManager.HitWindow100, StartTime + hitObjectManager.HitWindow50) { TagNumeric = ARMED };

            SpriteHitCircle1.Transformations.Add(fadeout.Clone());
            SpriteHitCircle2.Transformations.Add(fadeout.Clone());
            SpriteHitCircleText.Transformations.Add(fadeout.Clone());
            if (hasApproachCircle && hidden)
                SpriteApproachCircle.Transformations.Add(fadeout.Clone());

            SpriteHitCircle1.Scale = 1;
            SpriteHitCircle2.Scale = 1;
            SpriteHitCircleText.Scale = TEXT_SIZE;
        }

        internal override void Arm(bool isHit)
        {
            base.Arm(isHit);

            int armTime = GameBase.Mode != OsuModes.Edit ? AudioEngine.Time : StartTime;

            if (isHit)
            {
                //Fade out the actual hit circle
                Transformation circleScaleOut = new Transformation(TransformationType.Scale, 1.0F, 1.4F, armTime, (int)(armTime + (HitObjectManager.FadeOut)), EasingTypes.Out) { TagNumeric = ARMED };
                Transformation textScaleOut = new Transformation(TransformationType.Scale, 1.0F * TEXT_SIZE, 1.4F * TEXT_SIZE, armTime, (int)(armTime + (HitObjectManager.FadeOut)), EasingTypes.Out) { TagNumeric = ARMED };
                Transformation circleFadeOut = new Transformation(TransformationType.Fade, SpriteHitCircle1.Alpha, 0, armTime, armTime + HitObjectManager.FadeOut) { TagNumeric = ARMED };

                SpriteHitCircle1.Transformations.Add(circleScaleOut);
                SpriteHitCircle1.Transformations.Add(circleFadeOut);

                SpriteHitCircle2.Transformations.Add(circleScaleOut.Clone());
                SpriteHitCircle2.Transformations.Add(circleFadeOut.Clone());

                SpriteApproachCircle.Transformations.Add(new Transformation(TransformationType.Fade, SpriteApproachCircle.Alpha, 0, armTime, armTime) { TagNumeric = ARMED });

                if (SkinManager.UseNewLayout)
                {
                    SpriteHitCircleText.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, armTime, armTime + 60) { TagNumeric = ARMED });
                }
                else
                {
                    SpriteHitCircleText.Transformations.Add(textScaleOut);
                    SpriteHitCircleText.Transformations.Add(circleFadeOut.Clone());
                }
            }
            else
            {
                bool hidden = ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.Hidden);

                Transformation fadeout = new Transformation(TransformationType.Fade, !hidden ? SpriteHitCircle1.Alpha : 0, 0, armTime, armTime + 60) { TagNumeric = ARMED };
                SpriteHitCircle1.Transformations.Add(fadeout.Clone());
                SpriteHitCircle2.Transformations.Add(fadeout.Clone());
                SpriteHitCircleText.Transformations.Add(fadeout.Clone());
                SpriteApproachCircle.Transformations.Add(fadeout.Clone());

                SpriteHitCircle1.Scale = DefaultScale;
                SpriteHitCircle2.Scale = DefaultScale;
                SpriteHitCircleText.Scale = TEXT_SIZE;
            }
        }

        #endregion

        #region Drawing

        internal pSprite SpriteApproachCircle;
        internal pSprite SpriteHitCircle1;
        internal pAnimation SpriteHitCircle2;
        internal pSprite SpriteHitCircleText;
        internal pSprite SpriteSelectionCircle;

        public override int ComboNumber
        {
            get { return comboNumber; }
            set
            {
                pText pt = SpriteHitCircleText as pText;

                if (pt != null)
                    pt.Text = value > 0 ? value.ToString() : string.Empty;

                comboNumber = value;
            }
        }

        public override Vector2 EndPosition
        {
            get { return Position; }
            set { }
        }

        internal override bool IsVisible
        {
            get
            {
                return AudioEngine.Time >= StartTime - LocalPreEmpt &&
                     AudioEngine.Time <= EndTime + HitObjectManager.FadeOut + hitObjectManager.ForceFadeOut;
            }
        }

        //todo: get rid of this.
        Transformation editorFlash;

        internal override void SetColour(Color colour)
        {
            if (hitObjectManager.spriteManager != null && colour != Colour)
            {
                SpriteHitCircle1.InitialColour = colour;
                SpriteApproachCircle.InitialColour = colour;

                if (GameBase.Mode == OsuModes.Edit && !ConfigManager.sEditorHitAnimations)
                {
                    if (editorFlash != null)
                        SpriteHitCircle1.Transformations.Remove(editorFlash);
                    SpriteHitCircle1.Transformations.Add((editorFlash = new Transformation(colour, Color.White, StartTime - 5, EndTime - 5)));
                }

                Colour = colour;
                base.SetColour(colour);
                ApplyDim();
            }
        }

        #endregion

        internal override IncreaseScoreType GetScorePoints(Vector2 currentMousePos)
        {
            throw new NotImplementedException();
        }

        protected virtual float DefaultScale { get { return 1; } }
    }
}