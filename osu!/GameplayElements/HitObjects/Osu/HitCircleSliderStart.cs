using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Beatmaps;

namespace osu.GameplayElements.HitObjects.Osu
{
    internal class HitCircleSliderStart : HitCircleOsu
    {
        internal HitCircleSliderStart(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo)
            : base(hom, startPosition, startTime, newCombo)
        {
        }

        internal HitCircleSliderStart(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType, int comboOffset)
            : base(hom, startPosition, startTime, newCombo, soundType, comboOffset)
        {
        }

        internal HitCircleSliderStart(HitObjectManager hom, Vector2 pos, int startTime, bool newCombo, bool addWhistle, bool addFinish, bool addClap, int comboOffset)
            : base(hom, pos, startTime, newCombo, addWhistle, addFinish, addClap, comboOffset)
        {
        }

        internal override IncreaseScoreType Hit()
        {
            IncreaseScoreType ist = base.Hit();
            return ist <= 0 ? IncreaseScoreType.MissHpOnly : ist;
        }
    }
}