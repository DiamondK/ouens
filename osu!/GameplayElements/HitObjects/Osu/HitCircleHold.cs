using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Scoring;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu.GameplayElements.Beatmaps;
using osu.Graphics;

namespace osu.GameplayElements.HitObjects.Osu
{
    internal class HitCircleHold : HitCircleOsu
    {
        internal bool ActiveEnding;

        internal HitCircleHold(HitObjectManager hom, Vector2 pos, int startTime, bool newCombo, bool addWhistle, bool addFinish, bool addClap, int comboOffset)
            : base(hom, pos, startTime, newCombo, addWhistle, addFinish, addClap, comboOffset)
        {
            Type = HitObjectType.Hold;
            ActiveEnding = false;
        }

        internal override void ModifyTime(int newTime)
        {
            int difference = newTime - StartTime;
            StartTime = newTime;
            EndTime = EndTime + difference;
        }

        internal override void SetEndTime(int time)
        {
            EndTime = time;
        }

        internal override HitObject Clone()
        {
            HitCircleHold h = new HitCircleHold(hitObjectManager, Position, StartTime, false,
                                              SoundType.IsType(HitObjectSoundType.Whistle),
                                              SoundType.IsType(HitObjectSoundType.Finish),
                                              SoundType.IsType(HitObjectSoundType.Clap), ComboOffset
                );

            h.EndTime = EndTime; 
            h.SetColour(Colour);
            h.ComboNumber = ComboNumber;
            h.Selected = Selected;
            h.SampleSet = SampleSet;
            h.SampleSetAdditions = SampleSetAdditions;
            h.CustomSampleSet = CustomSampleSet;
            h.SampleVolume = SampleVolume;
            h.SampleAddr = SampleAddr;
            h.SampleFile = SampleFile;
            return h;
        }

        internal HitObject CloneAsNormal()
        {
            HitCircleOsu h = new HitCircleOsu(hitObjectManager, Position, StartTime, false, 
                                              SoundType.IsType(HitObjectSoundType.Whistle),
                                              SoundType.IsType(HitObjectSoundType.Finish),
                                              SoundType.IsType(HitObjectSoundType.Clap), ComboOffset);
            h.SetColour(Colour);
            h.ComboNumber = ComboNumber;
            h.Selected = Selected;
            h.SampleSet = SampleSet;
            h.SampleSetAdditions = SampleSetAdditions;
            h.CustomSampleSet = CustomSampleSet;
            h.SampleVolume = SampleVolume;
            h.SampleAddr = SampleAddr;
            h.SampleFile = SampleFile;
            return h;
        }

        internal override void PlaySound()
        {
            //ugly hack for not playing end sound
            if (Math.Abs(AudioEngine.Time - EndTime) < Math.Abs(AudioEngine.Time - StartTime))
                return;
            base.PlaySound();
        }
    }
}