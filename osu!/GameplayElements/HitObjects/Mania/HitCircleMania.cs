﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Scoring;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements.Beatmaps;
using osu.Graphics;

namespace osu.GameplayElements.HitObjects.Mania
{
    internal enum ManiaNoteType
    {
        Normal,
        Long,
        Special
    }

    internal class HitCircleMania : HitCircle
    {
        internal ColumnMania Column;
        internal int LogicalColumn;
        internal ManiaNoteType ManiaType = ManiaNoteType.Normal;
        internal int TimePress;
        internal int TimeRelease;
        internal bool Pressed;
        internal int Siblings = 1;
        internal float Length = 0;
        internal pAnimation s_note;
        internal bool IsMissing;
        internal bool IsFinished;

        public HitCircleMania(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType)
            : this(hom, hom.ManiaStage.ColumnAt(startPosition), startTime, soundType)
        { }

        public HitCircleMania(HitObjectManager hom, int column, int startTime, HitObjectSoundType soundType)
            : base(hom)
        {
            LogicalColumn = column;
            column = ((HitObjectManagerMania)hom).GetPhysicalColumn(column);
            Column = hom.ManiaStage.Columns[column];

            Position = new Vector2(Column.Left - Column.Host.Left + Column.Width / 2, -100);
            StartTime = startTime;
            EndTime = startTime;
            SoundType = soundType;

            if (!hom.ManiaStage.Minimal)
            {
                s_note =
                new pAnimation(LoadCustomImages("NoteImage" + column.ToString(), Column.NoteString), Fields.TopLeft, hom.ManiaStage.FlipOrigin(Origins.BottomCentre), Clocks.Audio, Position, 0.8f, false, Color.White);
                s_note.Alpha = 1;
                s_note.FlipVertical = SkinManager.Current.Version >= 2.5 && hom.ManiaStage.UpsideDown;
                ScaleAdjust(s_note);
                SpriteCollection.Add(s_note);
            }
        }

        protected pTexture[] LoadCustomImages(string key, string fallback)
        {
            return hitObjectManager.ManiaStage.Skin.LoadAll(key, fallback, hitObjectManager.ManiaStage.SkinSource);
        }

        protected pTexture[] LoadCustomImages(string key, pTexture[] fallback)
        {
            pTexture[] ret = LoadCustomImages(key, "");
            return ret ?? fallback;
        }

        protected void ScaleAdjust(pSprite d)
        {
            float widthFactor = (float)Column.Width / d.DrawWidth * 1.6f;
            float heightFactor = (float)Column.Host.MinimumColumnWidth / d.DrawWidth * 1.6f * (d.FlipVertical ? -1f : 1f);
            d.VectorScale = new Vector2(widthFactor, heightFactor);
        }

        protected override float PositionalSound {
            get { return ((Column + 1f) / hitObjectManager.ManiaStage.Columns.Count - 0.5f) * 0.8f; }
        }

        internal override void PlaySound()
        {
            ControlPoint pt = hitObjectManager.Beatmap.controlPointAtBin(StartTime + 2);
            if (pt != null)
                PlaySound(ref pt, SoundType, SampleSet, SampleSetAdditions);
        }

        internal override IncreaseScoreType Hit()
        {
            if (IsHit)
                return hitValue;
            IsHit = true;
            hitValue = IncreaseScoreType.MissMania;
            if (Pressed)
            {
                int accuracy = Math.Abs(TimePress - StartTime);
                HitObjectManagerMania homMania = hitObjectManager as HitObjectManagerMania;

                if (accuracy <= homMania.hitWindow300g)
                    hitValue = IncreaseScoreType.ManiaHit300g;
                else if (accuracy <= homMania.HitWindow300)
                    hitValue = IncreaseScoreType.ManiaHit300;
                else if (accuracy <= homMania.hitWindow200)
                    hitValue = IncreaseScoreType.ManiaHit200;
                else if (accuracy <= homMania.HitWindow100)
                    hitValue = IncreaseScoreType.ManiaHit100;
                else if (accuracy <= homMania.HitWindow50)
                    hitValue = IncreaseScoreType.ManiaHit50;

                PlaySound();
            }
            if (hitValue > IncreaseScoreType.ManiaHit100)
                Finish();
            else
                Miss();

            return hitValue;
        }

        internal virtual void SoundStart() { }

        public override int ComboNumber
        {
            get { return 0; }
            set { }
        }

        public override bool NewCombo
        {
            get;
            set;
        }

        public override Vector2 EndPosition
        {
            get { return Position; }
            set { throw new NotImplementedException(); }
        }

        internal override bool IsVisible
        {
            get { return SpriteCollection[0].IsVisible; }
        }

        internal override void SetColour(Color colour)
        {
            SpriteCollection[0].InitialColour = colour;
                Colour = colour;
        }

        internal override void Arm(bool isHit)
        {

        }

        internal override HitObject Clone()
        {
            throw new NotImplementedException();
        }

        internal override void Select()
        {
            throw new NotImplementedException();
        }

        internal override void Deselect()
        {
            throw new NotImplementedException();
        }

        internal override void ModifyTime(int newTime)
        {
            throw new NotImplementedException();
        }

        internal override void ModifyPosition(Vector2 newPosition)
        {
            SpriteCollection.ForEach(p => p.Position = newPosition);
        }

        internal override IncreaseScoreType GetScorePoints(Vector2 currentMousePos)
        {
            throw new NotImplementedException();
        }

        internal override void SetEndTime(int time)
        {
            throw new NotImplementedException();
        }

        internal virtual void Finish()
        {
            if (!IsFinished)
            {
                IsFinished = true;
                SpriteCollection.ForEach(p => p.Bypass = true);
            }
        }

        internal virtual void Miss()
        {
            if (!IsMissing)
            {
                IsMissing = true;
                SpriteCollection.ForEach(p => p.FadeColour(Color.Gray, 60));
            }
        }
    }
}
