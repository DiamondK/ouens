﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements.Beatmaps;

namespace osu.GameplayElements.HitObjects.Mania
{
    internal class SpinnerMania : Spinner
    {
        internal List<HitCircleMania> HitObjects;
        private bool[] lastRow;
        private int lastCount = 0;
        private ManiaConvertType convertType;

        internal SpinnerMania(HitObjectManager hom, int startTime, int endTime, HitObjectSoundType soundType, ManiaConvertType convertType, bool[] lastRow)
            : base(hom, startTime, endTime, soundType)
        {
            this.lastRow = lastRow;
            for (int i = 0; i < lastRow.Length; i++)
            {
                if (lastRow[i])
                {
                    lastCount++;
                }
            }
            this.convertType = convertType;
            if (lastCount == hitObjectManager.ManiaStage.Columns.Count)
                this.convertType &= ~ManiaConvertType.ForceNotStack;
            HitObjects = new List<HitCircleMania>();
        }

        internal void GenerateHitObjects()
        {
            if ((convertType & ManiaConvertType.NotChange) > 0)
            {
                Add(0, StartTime, EndTime);
                return;
            }
            if (EndTime - StartTime < 100)
            {
                if (hitObjectManager.ManiaStage.Columns.Count == 8)
                {
                    if (SoundType.IsType(HitObjectSoundType.Finish) && (EndTime - StartTime) < 1000)
                        Add(0, StartTime, StartTime);
                    else
                        Add(getColumnNext(hitObjectManager.ManiaStage.RandomStart), StartTime, StartTime);
                }
                else
                    Add(getColumnNext(0), StartTime, StartTime);
            }
            else
            {
                if (hitObjectManager.ManiaStage.Columns.Count == 8)
                {
                    if (SoundType.IsType(HitObjectSoundType.Finish) && (EndTime - StartTime) < 1000)
                        Add(0, StartTime, EndTime);
                    else
                        Add(getColumnNext(hitObjectManager.ManiaStage.RandomStart), StartTime, EndTime);
                }
                else
                    Add(getColumnNext(0), StartTime, EndTime);
            }
        }

        private void Add(int column, int startTime, int endTime)
        {
            if (startTime == endTime)
            {
                HitCircleMania note = new HitCircleMania(hitObjectManager, column, startTime, SoundType);
                note.SampleSet = SampleSet;
                note.SampleSetAdditions = SampleSetAdditions;
                note.CustomSampleSet = CustomSampleSet;
                note.SampleVolume = SampleVolume;
                note.Siblings = 1;
                HitObjects.Add(note);
            }
            else
            {
                HitCircleManiaLong note = new HitCircleManiaLong(hitObjectManager, column, startTime, endTime, HitObjectSoundType.Normal);
                note.SoundTypeStart = HitObjectSoundType.Normal;
                note.SoundTypeEnd = SoundType;
                note.SampleSet = SampleSet;
                note.SampleSetAdditions = SampleSetAdditions;
                note.SampleSetAtEnd = SampleSet;
                note.SampleSetAdditionsAtEnd = SampleSetAdditions;
                note.CustomSampleSet = CustomSampleSet;
                note.SampleVolume = SampleVolume;
                note.Siblings = 1;
                HitObjects.Add(note);
            }
        }

        private int getColumnNext(int start)
        {
            HitFactoryMania hitFactoryMania = hitObjectManager.hitFactory as HitFactoryMania;

            int next = hitFactoryMania.random.Next(start, hitObjectManager.ManiaStage.Columns.Count);
            if ((convertType & ManiaConvertType.ForceNotStack) > 0)
            {
                while(lastRow[next])
                    next = hitFactoryMania.random.Next(start, hitObjectManager.ManiaStage.Columns.Count);
            }
            return next;
        }

        public override int ComboNumber
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override Vector2 EndPosition
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        internal override bool IsVisible
        {
            get { throw new NotImplementedException(); }
        }

        internal override void SetColour(Color color)
        {
            throw new NotImplementedException();
        }

        internal override IncreaseScoreType Hit()
        {
            throw new NotImplementedException();
        }

        internal override HitObject Clone()
        {
            throw new NotImplementedException();
        }

        internal override void Select()
        {
            throw new NotImplementedException();
        }

        internal override void Deselect()
        {
            throw new NotImplementedException();
        }

        internal override void ModifyTime(int newTime)
        {
            throw new NotImplementedException();
        }

        internal override void ModifyPosition(Vector2 newPosition)
        {
            throw new NotImplementedException();
        }

        internal override IncreaseScoreType GetScorePoints(Vector2 currentMousePos)
        {
            throw new NotImplementedException();
        }

        internal override void SetEndTime(int time)
        {
            throw new NotImplementedException();
        }
    }
}