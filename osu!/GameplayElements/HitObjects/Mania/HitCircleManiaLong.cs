﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Scoring;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu.GameModes.Play.Rulesets.Mania;
using Un4seen.Bass;
using osu.GameplayElements.Beatmaps;

namespace osu.GameplayElements.HitObjects.Mania
{
    internal class HitCircleManiaLong : HitCircleMania
    {
        internal HitObjectSoundType SoundTypeEnd;
        internal HitObjectSoundType SoundTypeStart;
        internal SampleSet SampleSetAtEnd;
        internal SampleSet SampleSetAdditionsAtEnd;
        internal SampleSet SlideSampleSet;
        internal SampleSet SlideSampleSetAdditions;
        internal bool SoundAtEnd;
        internal pAnimation s_body;
        internal pAnimation s_rear;
        internal int lastScoreTime = -1;
        internal bool IsFrozen;
        internal bool IsUnfrozen;
        internal int UnfreezeTime;

        public HitCircleManiaLong(HitObjectManager hom, int column, int startTime, int endTime, HitObjectSoundType soundType)
            : base(hom, column, startTime, soundType)
        {
            //Physical column index
            column = Column;

            SoundType = soundType;
            SoundTypeStart = soundType;
            SoundTypeEnd = soundType;
            ManiaType = ManiaNoteType.Long;
            EndTime = endTime;
            SoundAtEnd = true;

            if (!hom.ManiaStage.Minimal)
            {
                //head note -- the origin one
                s_note.TextureArray = LoadCustomImages(string.Format("NoteImage{0}H", column), Column.NoteString + @"H");
                ScaleAdjust(s_note);

                //rear note
                s_rear = new pAnimation(LoadCustomImages(string.Format("NoteImage{0}T", column), s_note.TextureArray), Fields.TopLeft, s_note.Origin, Clocks.Audio, Position, 0.7975f, false, Color.White);
                s_rear.TextureArray = LoadCustomImages(string.Format("NoteImage{0}T", column), LoadCustomImages(Column.NoteString + @"T", s_note.TextureArray));
                s_rear.Alpha = s_note.Alpha;
                s_rear.FlipVertical = !s_note.FlipVertical;
                ScaleAdjust(s_rear);
                SpriteCollection.Add(s_rear);

                //body note
                s_body = new pAnimation(LoadCustomImages(string.Format("NoteImage{0}L", column), Column.NoteString + @"L"), Fields.TopLeft, s_note.Origin, Clocks.Audio, Position, 0.795F, false, Color.White);
                s_body.Alpha = s_note.Alpha;
                s_body.FrameDelay = 30;
                s_body.RunAnimation = false;  //run when press
                ScaleAdjust(s_body);
                SpriteCollection.Add(s_body);
            }
        }

        internal override void SoundStart()
        {
            SampleSet slideSampleSet = SampleSet.None;
            SampleSet slideSampleSetAdditions = SampleSet.None;

            if (hitObjectManager.Beatmap.BeatmapVersion >= 14)
            {
                slideSampleSet = SlideSampleSet;
                slideSampleSetAdditions = SlideSampleSetAdditions;
            }

            AudioEngine.PlaySlideSamples(SoundType, slideSampleSet, slideSampleSetAdditions, false, null);

            PlaySound(true);
        }

        internal override void StopSound()
        {
            AudioEngine.StopSlideSamples();
        }

        internal override void PlaySound()
        {
            if (SoundAtEnd)
                PlaySound(false);
        }

        internal void PlaySound(bool start)
        {
            ControlPoint pt = hitObjectManager.Beatmap.controlPointAtBin((start ? StartTime : EndTime) + 2);
            if (pt == null)
                return;

            if (start)
                PlaySound(ref pt, SoundTypeStart, SampleSet, SampleSetAdditions);
            else
                PlaySound(ref pt, SoundTypeEnd, SampleSetAtEnd, SampleSetAdditionsAtEnd);
        }

        //add combo
        internal IncreaseScoreType Holding()
        {
            IncreaseScoreType value = IncreaseScoreType.Ignore;
            if (lastScoreTime != -1 && AudioEngine.Time <= EndTime && AudioEngine.Time >= StartTime)
            {
                if (AudioEngine.Time - lastScoreTime >= JudgementMania.ComboIntv)
                {
                    lastScoreTime += JudgementMania.ComboIntv;
                    value = IncreaseScoreType.ComboAddition;
                }
            }
            /*if (AudioEngine.Time >= nextScoreTime && AudioEngine.Time <= EndTime && AudioEngine.Time >= StartTime)
            {
                nextScoreTime += JudgementBemani.ComboIntv;
                value = IncreaseScoreType.ComboAddition;
            }*/

            return value;
        }

        //initial judgement
        internal IncreaseScoreType HitStart()
        {
            //start animation
            s_body.RunAnimation = true;
            lastScoreTime = AudioEngine.Time;
            SoundStart();
            if (!IsMissing)
                ((HitObjectManagerMania)hitObjectManager).FreezeNote(this);
            return IncreaseScoreType.Ignore;
        }

        private bool hasHoldBreak;

        internal void UpdateMissing()
        {
            if (AudioEngine.Time > StartTime + hitObjectManager.HitWindow50 && TimePress == 0)
                Miss();
        }

        internal override void Miss()
        {
            if (!IsMissing)
            {
                ((HitObjectManagerMania)hitObjectManager).UnfreezeNote(this);
                base.Miss();
            }
        }

        internal override IncreaseScoreType Hit()
        {
            s_body.RunAnimation = false;
            lastScoreTime = -1;

            if (IsHit)
                return hitValue;

            if (AudioEngine.Time < StartTime + hitObjectManager.HitWindow50 && TimePress == 0)
                return IncreaseScoreType.Ignore;

            UpdateMissing();

            if (AudioEngine.Time < EndTime - hitObjectManager.HitWindow50)
            {
                if (Pressed)
                    return IncreaseScoreType.Ignore;
                else
                {
                    StopSound();
                    Miss();
                    hasHoldBreak = true;
                    return IncreaseScoreType.MissManiaHoldBreak;
                }
            }

            if (AudioEngine.Time < EndTime + hitObjectManager.HitWindow50 && Pressed)
                return IncreaseScoreType.Ignore;

            StopSound();
            IsHit = true;
            hitValue = IncreaseScoreType.MissMania;

            int start = 0;
            if (TimePress < StartTime - hitObjectManager.HitWindow50)
                start = EndTime - 1;   //hit too early ,set a 0 value
            else if (TimePress < StartTime)
                start = StartTime + (StartTime - TimePress);
            else
                start = TimePress;

            int end = 0;
            if (Pressed) //still holding
                end = AudioEngine.Time;
            else if (TimeRelease > EndTime)
                end = EndTime - (TimeRelease - EndTime);
            else
                end = TimeRelease;

            //depends on how long player hold the key.
            int total = EndTime - StartTime;
            int hold = Math.Abs(end - start);
            int diffStart = Math.Abs(start - StartTime);
            int diffEnd = Math.Abs(end - EndTime);
            int diffTotal = diffStart + diffEnd;
            float percent = (float)(end - start) / Length;

            HitObjectManagerMania homMania = hitObjectManager as HitObjectManagerMania;
            if (end < EndTime - homMania.HitWindow50)
                hitValue = IncreaseScoreType.MissMania; //is there any reason this was Miss instead of MissBemani?
            else if (diffStart <= homMania.hitWindow300g * 1.2 && diffTotal <= homMania.hitWindow300g * 2.4)
                hitValue = IncreaseScoreType.ManiaHit300g;
            else if (diffStart <= homMania.HitWindow300 * 1.1 && diffTotal <= homMania.HitWindow300 * 2.2)
                hitValue = IncreaseScoreType.ManiaHit300;
            else if (diffStart <= homMania.hitWindow200 && diffTotal <= homMania.hitWindow200 * 2)
                hitValue = IncreaseScoreType.ManiaHit200;
            else if (diffStart <= homMania.HitWindow100 && diffTotal <= homMania.HitWindow100 * 2)
                hitValue = IncreaseScoreType.ManiaHit100;
            else
                hitValue = IncreaseScoreType.ManiaHit50;

            if (hitValue >= IncreaseScoreType.ManiaHit300 && hasHoldBreak)
                hitValue = IncreaseScoreType.ManiaHit200;
            //Bug fix: If let go and just hit rear, get 50.
          //  if (hitValue == IncreaseScoreType.BemaniHit50 && hasHoldBreak)
          //      hitValue = IncreaseScoreType.MissBemani;

            if (hitValue > 0)
                PlaySound();

            //Only scroll past if its broken or released too early
            if (IsMissing || AudioEngine.Time < EndTime - hitObjectManager.HitWindow100)
                Miss();
            else
                Finish();

            return hitValue;
        }
    }
}
