﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Sprites;
using osu_common;
using Un4seen.Bass;

namespace osu.GameplayElements.HitObjects.Mania
{
    //used for special hold note. especially converted from bms
    internal class HitCircleManiaHold : HitCircleManiaLong
    {

        public HitCircleManiaHold(HitObjectManager hom, int column, int startTime, int endTime, HitObjectSoundType soundType)
            : base(hom, column, startTime, endTime,soundType)
        {
        }

        internal override void SoundStart()
        {
            PlaySound(true);
            SoundAtEnd = false;
        }

        internal override void StopSound()
        {
        }

        protected override void Dispose(bool isDisposing)
        {

            base.Dispose(isDisposing);
        }

    }
}