﻿using System;
using System.Collections.Generic;
using System.Text;
using osu.GameModes.Edit.AiMod;
using osu.GameModes.Play.Rulesets.Mania;
using Microsoft.Xna.Framework;

using osu.GameplayElements.Beatmaps;

namespace osu.GameplayElements.HitObjects.Mania
{
    class DifficultyHitObjectMania
    {
        // Factor by how much individual / overall strain decays per second. Those values are results of tweaking a lot and taking into account general feedback.
        internal static readonly double INDIVIDUAL_DECAY_BASE = 0.125;
        internal static readonly double OVERALL_DECAY_BASE = 0.30;

        internal HitCircleMania BaseHitObject;

        private double[] HeldUntil;

        private double[] IndividualStrains; // Measures jacks or more generally: repeated presses of the same button

        internal double IndividualStrain
        {
            get
            {
                return IndividualStrains[BaseHitObject.Column];
            }

            set
            {
                IndividualStrains[BaseHitObject.Column] = value;
            }
        }

        internal double OverallStrain = 1;    // Measures note density in a way

        internal DifficultyHitObjectMania(HitCircleMania BaseHitObject)
        {
            this.BaseHitObject = BaseHitObject;
            IndividualStrains = new double[BaseHitObject.hitObjectManager.ManiaStage.Columns.Count];
            HeldUntil = new double[BaseHitObject.hitObjectManager.ManiaStage.Columns.Count];

            for (int i = 0; i < BaseHitObject.hitObjectManager.ManiaStage.Columns.Count; ++i)
            {
                IndividualStrains[i] = 0;
                HeldUntil[i] = 0;
            }
        }

        internal void CalculateStrains(DifficultyHitObjectMania PreviousHitObject, double TimeRate)
        {
            // TODO: Factor in holds
            double Addition = 1.0;
            double TimeElapsed = (double)(BaseHitObject.StartTime - PreviousHitObject.BaseHitObject.StartTime) / TimeRate;
            double IndividualDecay = Math.Pow(INDIVIDUAL_DECAY_BASE, TimeElapsed / 1000);
            double OverallDecay = Math.Pow(OVERALL_DECAY_BASE, TimeElapsed / 1000);

            double HoldFactor = 1.0; // Factor to all additional strains in case something else is held
            double HoldAddition = 0; // Addition to the current note in case it's a hold and has to be released awkwardly

            // Fill up the heldUntil array
            for (int i = 0; i < BaseHitObject.hitObjectManager.ManiaStage.Columns.Count; ++i)
            {
                HeldUntil[i] = PreviousHitObject.HeldUntil[i];

                // If there is at least one other overlapping end or note, then we get an addition, buuuuuut...
                if (BaseHitObject.StartTime < HeldUntil[i] && BaseHitObject.EndTime > HeldUntil[i])
                {
                    HoldAddition = 1.0;
                }

                // ... this addition only is valid if there is _no_ other note with the same ending. Releasing multiple notes at the same time is just as easy as releasing 1
                if (BaseHitObject.EndTime == HeldUntil[i])
                {
                    HoldAddition = 0;
                }

                // We give a slight bonus to everything if something is held meanwhile
                if (HeldUntil[i] > BaseHitObject.EndTime)
                {
                    HoldFactor = 1.25;
                }
            }
            HeldUntil[BaseHitObject.Column] = BaseHitObject.EndTime;

            // Decay individual strains
            for (int i = 0; i < BaseHitObject.hitObjectManager.ManiaStage.Columns.Count; ++i)
            {
                IndividualStrains[i] = PreviousHitObject.IndividualStrains[i] * IndividualDecay;
            }
            // Increase individual strain in own column
            IndividualStrain += (2.0/* + (double)SpeedMania.Column / 8.0*/) * HoldFactor;

            OverallStrain = PreviousHitObject.OverallStrain * OverallDecay + (Addition + HoldAddition) * HoldFactor;
        }
    }
}
