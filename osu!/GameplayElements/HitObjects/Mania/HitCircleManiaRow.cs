﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Scoring;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements.Beatmaps;

namespace osu.GameplayElements.HitObjects.Mania
{
    /// <summary>
    /// notes at the same row
    /// </summary>
    internal class HitCircleManiaRow : HitCircle
    {
        internal List<HitCircleMania> HitObjects;
        private ManiaConvertType convertType;
        private bool[] lastRow;
        private int lastCount = 0;
        private int lastColumn = 0; //used when lastCount == 1

        private HitFactoryMania hitFactoryMania;

        public HitCircleManiaRow(HitObjectManager hom, Vector2 startPosition, int startTime, HitObjectSoundType soundType, ManiaConvertType cvtType, bool[] lastRow)
            : base(hom)
        {
            HitObjects = new List<HitCircleMania>();
            convertType = cvtType;
            this.lastRow = lastRow;
            Position = startPosition;
            StartTime = startTime;
            SoundType = soundType;

            for (int i = 0; i < lastRow.Length; i++)
            {
                if (lastRow[i])
                {
                    lastCount++;
                    lastColumn = i;
                }
            }
        }

        internal void GenerateHitObjects()
        {
            hitFactoryMania = hitObjectManager.hitFactory as HitFactoryMania;

            //sample specific,arranged by priority
            //don't generate mirror style for 7+1K
            if ((convertType & ManiaConvertType.KeepSingle) == 0)
            {
                if (SoundType.IsType(HitObjectSoundType.Finish) && hitObjectManager.ManiaStage.Columns.Count != 8)
                    convertType |= ManiaConvertType.Mirror;
                else if (SoundType.IsType(HitObjectSoundType.Clap))
                    convertType |= ManiaConvertType.Gathered;
            }

            //some special convertypes ignore other conversion indication.
            if ((convertType & ManiaConvertType.NotChange) > 0
                || hitObjectManager.ManiaStage.Columns.Count == 1)
            {
                Add(hitObjectManager.ManiaStage.ColumnAt(Position));
                PostProcessing();
                return;
            }

            if ((convertType & ManiaConvertType.Reverse) > 0 && lastCount > 0)
            {
                for (int i = hitObjectManager.ManiaStage.RandomStart; i < lastRow.Length; i++)
                {
                    if (lastRow[i])
                        Add(hitObjectManager.ManiaStage.Columns.Count - i - 1 + hitObjectManager.ManiaStage.RandomStart);
                }
                PostProcessing();
                return;
            }
            if ((convertType & ManiaConvertType.Cycle) > 0 && lastCount == 1)
            {
                //make sure last note not in centre column.
                if (!(hitObjectManager.ManiaStage.Columns.Count == 8 && lastColumn == 0) && (hitObjectManager.ManiaStage.Columns.Count % 2 == 0 || lastColumn != hitObjectManager.ManiaStage.Columns.Count / 2))
                {
                    lastColumn = hitObjectManager.ManiaStage.Columns.Count - lastColumn - 1 + hitObjectManager.ManiaStage.RandomStart;
                    Add(lastColumn);
                    PostProcessing();
                    return;
                }
            }
            if ((convertType & ManiaConvertType.ForceStack) > 0 && lastCount > 0)
            {
                //keep the same column with last row
                for (int i = hitObjectManager.ManiaStage.RandomStart; i < lastRow.Length; i++)
                {
                    if (lastRow[i])
                        Add(i);
                }
                PostProcessing();
                return;
            }
            if ((convertType & (ManiaConvertType.Stair | ManiaConvertType.ReverseStair)) > 0 && lastCount == 1)
            {
                if ((convertType & ManiaConvertType.Stair) > 0)
                {
                    lastColumn++;
                    if (lastColumn == hitObjectManager.ManiaStage.Columns.Count)
                        lastColumn = hitObjectManager.ManiaStage.RandomStart;
                }
                else
                {
                    lastColumn--;
                    if (lastColumn == hitObjectManager.ManiaStage.RandomStart - 1)
                        lastColumn = hitObjectManager.ManiaStage.Columns.Count - 1;
                }
                Add(lastColumn);
                PostProcessing();
                return;
            }
            if ((convertType & ManiaConvertType.KeepSingle) > 0)
            {
                AddRandomNote(1);
                PostProcessing();
                return;
            }

            if ((convertType & ManiaConvertType.Mirror) > 0)
            {
                if (hitObjectManager.Beatmap.DifficultyBemaniStars > 6.5)
                {
                    NoteCalculationMirror(0.88f, 0.88f, 0.62f);
                }
                else if (hitObjectManager.Beatmap.DifficultyBemaniStars > 4.0)
                {
                    NoteCalculationMirror(0.88f, 1f, 0.83f);
                }
                else
                    NoteCalculationMirror(0.88f, 1f, 1f);
            }
            else
            {
                if (hitObjectManager.Beatmap.DifficultyBemaniStars > 6.5)
                {
                    if ((convertType & ManiaConvertType.LowProbability) > ManiaConvertType.None)
                        NoteCalculationNormal(1f, 1f, 0.58f, 0.22f);
                    else
                        NoteCalculationNormal(1f, 1f, 0.38f, 0);
                }
                else if (hitObjectManager.Beatmap.DifficultyBemaniStars > 4.0)
                {
                    if ((convertType & ManiaConvertType.LowProbability) > ManiaConvertType.None)
                        NoteCalculationNormal(1f, 1f, 0.92f, 0.65f);
                    else
                        NoteCalculationNormal(1f, 1f, 0.85f, 0.48f);
                }
                else if (hitObjectManager.Beatmap.DifficultyBemaniStars > 2.0)
                {
                    if ((convertType & ManiaConvertType.LowProbability) > ManiaConvertType.None)
                        NoteCalculationNormal(1f, 1f, 1f, 0.82f);
                    else
                        NoteCalculationNormal(1f, 1f, 1f, 0.55f);
                }
                else
                    NoteCalculationNormal(1f, 1f, 1f, 1f);
            }
            if (hitObjectManager.ManiaStage.RandomStart > 0 && SoundType.IsType(HitObjectSoundType.Finish | HitObjectSoundType.Clap))
                Add(0);
            PostProcessing();
        }

        /// <summary>
        /// calculate how many notes in a row by giving probability
        /// </summary>
        /// <param name="noteNeed5"></param>
        /// <param name="noteNeed4"></param>
        /// <param name="noteNeed3"></param>
        /// <param name="noteNeed2"></param>
        /// <param name="rn"></param>
        private void NoteCalculationNormal(float noteNeed5, float noteNeed4, float noteNeed3, float noteNeed2)
        {
            //reduce probability by Column limit.
            switch (hitObjectManager.ManiaStage.Columns.Count)
            {
                case 2:
                    noteNeed5 = 1f;
                    noteNeed4 = 1f;
                    noteNeed3 = 1f;
                    noteNeed2 = 1f;
                    break;
                case 3:
                    noteNeed5 = 1f;
                    noteNeed4 = 1f;
                    noteNeed3 = 1f;
                    noteNeed2 = Math.Max(noteNeed2, 0.90f);
                    break;
                case 4:
                    noteNeed5 = 1f;
                    noteNeed4 = 1f;
                    noteNeed3 = Math.Max(noteNeed3, 0.96f);
                    noteNeed2 = Math.Max(noteNeed2, 0.77f);
                    break;
                case 5:
                    noteNeed5 = 1f;
                    noteNeed4 = Math.Max(noteNeed4, 0.97f);
                    noteNeed3 = Math.Max(noteNeed3, 0.85f);
                    break;
            }
            if (SoundType.IsType(HitObjectSoundType.Clap))
                noteNeed2 = 0;
            int noteNeed = hitFactoryMania.GetRandomValue(noteNeed2, noteNeed3, noteNeed4, noteNeed5);
            AddRandomNote(noteNeed);
        }

        /// <summary>
        /// Decide how many notes put on each side
        /// turn to NoteCalculationNormal if ForceNotStack in convert indication.
        /// </summary>
        /// <param name="centre">probability of putting a note in the centre column</param>
        /// <param name="noteNeed3"></param>
        /// <param name="noteNeed2"></param>
        /// <param name="rn"></param>
        private void NoteCalculationMirror(float centre, float noteNeed3, float noteNeed2)
        {
            if ((convertType & ManiaConvertType.ForceNotStack) > ManiaConvertType.None)
            {
                NoteCalculationNormal(noteNeed3, (noteNeed2 + noteNeed3) / 2, noteNeed2, noteNeed2 / 2);
            }
            else
            {
                //reduce probability by Column limit.
                switch (hitObjectManager.ManiaStage.Columns.Count)
                {
                    case 2:
                        noteNeed2 = 1f;
                        noteNeed3 = 1f;
                        centre = 1f;
                        break;
                    case 3:
                        noteNeed2 = 1f;
                        noteNeed3 = 1f;
                        centre = Math.Max(0.97f, centre);
                        break;
                    case 4:
                        noteNeed2 = Math.Max(0.8f, noteNeed2 * 2);
                        noteNeed3 = 1f;
                        centre = 1f;
                        break;
                    case 5:
                        noteNeed3 = 1f;
                        centre = Math.Max(0.97f, centre);
                        break;
                    case 6:
                        noteNeed2 = Math.Max(0.5f, noteNeed2 * 2);
                        noteNeed3 = Math.Max(0.85f, noteNeed3 * 2);
                        centre = 1f;
                        break;
                }

                double val = hitFactoryMania.random.NextDouble();
                bool mid = false;
                if (val > centre)
                {
                    mid = true;
                }
                val = hitFactoryMania.random.NextDouble();
                if (val >= noteNeed3)
                {
                    AddMirrorNote(3);
                    mid = false;
                }
                else if (val >= noteNeed2)
                {
                    AddMirrorNote(2);
                }
                else
                {
                    AddMirrorNote(1);
                }
                if (mid && hitObjectManager.ManiaStage.Columns.Count % 2 != 0)
                    Add(hitObjectManager.ManiaStage.Columns.Count / 2);

            }
        }

        //if noteCount==1,note will be placed at it's original column calculated by .ColumnAt
        private void AddRandomNote(int noteCount)
        {
            if ((convertType & ManiaConvertType.ForceNotStack) > ManiaConvertType.None)
            {
                int usable = hitObjectManager.ManiaStage.Columns.Count - hitObjectManager.ManiaStage.RandomStart;
                for (int i = 0; i < hitObjectManager.ManiaStage.Columns.Count; i++)
                    usable -= lastRow[i] ? 1 : 0;

                if (noteCount > usable)
                    noteCount = usable;
            }

            int nextCol = hitObjectManager.ManiaStage.ColumnAt(Position, true);
            bool[] currRow = new bool[hitObjectManager.ManiaStage.Columns.Count];
            while (noteCount > 0)
            {
                while (currRow[nextCol] || (lastRow[nextCol] && (convertType & ManiaConvertType.ForceNotStack) > ManiaConvertType.None))
                {
                    if ((convertType & ManiaConvertType.Gathered) > 0)
                    {
                        nextCol++;
                        if (nextCol == hitObjectManager.ManiaStage.Columns.Count)
                            nextCol = hitObjectManager.ManiaStage.RandomStart;
                    }
                    else
                        nextCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
                }
                Add(nextCol);
                currRow[nextCol] = true;
                noteCount--;
            }
        }

        private void AddMirrorNote(int noteCount)
        {
            bool[] currRow = new bool[hitObjectManager.ManiaStage.Columns.Count];
            int upper = hitObjectManager.ManiaStage.Columns.Count % 2 == 0 ? hitObjectManager.ManiaStage.Columns.Count / 2 : (hitObjectManager.ManiaStage.Columns.Count - 1) / 2;
            int next = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, upper);
            for (int i = 0; i < noteCount; i++)
            {
                while (currRow[next])
                    next = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, upper);
                currRow[next] = true;
                currRow[hitObjectManager.ManiaStage.Columns.Count - next - 1 + hitObjectManager.ManiaStage.RandomStart] = true;
                Add(next);
                Add(hitObjectManager.ManiaStage.Columns.Count - next - 1 + hitObjectManager.ManiaStage.RandomStart);
            }
        }

        private void Add(int col)
        {
            HitCircleMania note = new HitCircleMania(hitObjectManager, col, StartTime, SoundType);
            note.SampleSet = SampleSet;
            note.SampleSetAdditions = SampleSetAdditions;
            note.CustomSampleSet = CustomSampleSet;
            HitObjects.Add(note);
        }


        private void PostProcessing()
        {
            int siblings = HitObjects.Count;
            if (siblings == 0)
                return;

            int volume = SampleVolume;
            if ((convertType & ManiaConvertType.NotChange) == 0)
            {
                if (SampleVolume != 0)
                {
                    volume = Math.Min(SampleVolume, SampleVolume * 4 / 3 / siblings);
                }
                else
                {
                    ControlPoint cp = hitObjectManager.Beatmap.controlPointAt(StartTime);
                    if (cp != null)
                        volume = Math.Min(cp.volume, cp.volume * 4 / 3 / siblings);
                }
            }

            HitObjects.ForEach(h => 
            {
                h.SampleVolume = volume;
                h.Siblings = siblings;
            });
        }

        internal override bool IsVisible
        {
            get { return true; }
        }

        public override int ComboNumber
        {
            get { return 0; }
            set { }
        }

        public override bool NewCombo
        {
            get;
            set;
        }

        public override Vector2 EndPosition
        {
            get { return Position; }
            set { throw new NotImplementedException(); }
        }

        internal override void SetColour(Color colour)
        {
            Colour = colour;
        }

        internal override IncreaseScoreType Hit()
        {


            return hitValue;
        }

        internal override void Arm(bool isHit)
        {

        }

        internal override HitObject Clone()
        {
            throw new NotImplementedException();
        }

        internal override void Select()
        {
            throw new NotImplementedException();
        }

        internal override void Deselect()
        {
            throw new NotImplementedException();
        }

        internal override void ModifyTime(int newTime)
        {
            throw new NotImplementedException();
        }

        internal override void ModifyPosition(Vector2 newPosition)
        {
            SpriteCollection.ForEach(p => p.Position = newPosition);
        }

        internal override IncreaseScoreType GetScorePoints(Vector2 currentMousePos)
        {
            throw new NotImplementedException();
        }

        internal override void SetEndTime(int time)
        {
            throw new NotImplementedException();
        }

    }
}