﻿using System.Collections.Generic;
using System;
using Microsoft.Xna.Framework;
using osu.Audio;
using osu.GameplayElements.HitObjects.Osu;
using osu_common.Helpers;
using osu.GameplayElements.Beatmaps;
using osu.GameModes.Play.Rulesets.Mania;
using osu_common;
using osu.Configuration;
using osu.Input;
using osu.GameplayElements.Scoring;
using osu.Graphics.Skinning;

namespace osu.GameplayElements.HitObjects.Mania
{
    internal class HitFactoryMania : HitFactory
    {
        internal FastRandom random = new FastRandom(1337);
        internal FastRandom randomSync = new FastRandom(1337);
        internal FastRandom randomSpecial = new FastRandom(10086);//for non-conversion random.

        private Vector2 prevPos;
        private int prevTime;
        private int prevDelta;
        private bool[] prevRow;
        private List<int> prevNotes;
        private const int prevCount = 7;
        private double noteDensity = int.MaxValue;
        private ManiaConvertType currentStair = ManiaConvertType.Stair;

        public HitFactoryMania(HitObjectManager hitObjectMananager)
            : base(hitObjectMananager)
        {
            // scroll speed should not be set by difficuly calc
            if (!hitObjectManager.IsDifficultyCalculator)
            {
                ManiaSpeedSet set = InputManager.ReplayMode && !ModManager.CheckActive(Mods.Autoplay) ? ManiaSpeedSet.Unowned : ManiaSpeedSet.Owned;
                SpeedMania.SetSpeed(0, set | ManiaSpeedSet.Reset);
            }

            int seed = (int)Math.Round(hitObjectManager.Beatmap.DifficultyHpDrainRate + hitObjectManager.Beatmap.DifficultyCircleSize)
                * 20 + (int)(hitObjectManager.Beatmap.DifficultyOverall * 41.2) + (int)Math.Round(hitObjectManager.Beatmap.DifficultyApproachRate);
            random.Reinitialise(seed);
            randomSync.Reinitialise(seed);

            prevNotes = new List<int>(prevCount);
            record(Vector2.Zero, 0);
            prevRow = new bool[StageMania.ColumnsWithMods(hitObjectManager.Beatmap, hitObjectManager.ActiveMods)];
        }

        internal int GetRandomValue(float noteNeed2, float noteNeed3, float noteNeed4 = 1, float noteNeed5 = 1, float noteNeed6 = 1)
        {
            double val = random.NextDouble();
            if (val >= noteNeed6)
                return 6;
            else if (val >= noteNeed5)
                return 5;
            else if (val >= noteNeed4)
                return 4;
            else if (val >= noteNeed3)
                return 3;
            else if (val >= noteNeed2)
                return 2;
            else
                return 1;
        }

        private void densityUpdate(int Time)
        {
            if (prevNotes.Count == prevCount)
                prevNotes.RemoveAt(0);
            prevNotes.Add(Time);
            if (prevNotes.Count >= 2)
            {
                noteDensity = (double)(prevNotes[prevNotes.Count - 1] - prevNotes[0]) / prevNotes.Count;
            }
        }

        private void record(Vector2 position, int Time)
        {
            prevDelta = Time - prevTime;
            prevTime = Time;
            prevPos = position;
        }

        internal override HitCircle CreateHitCircle(Vector2 startPosition, int startTime, bool newCombo,
                                                    HitObjectSoundType soundType, int comboOffset, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            HitCircleManiaRow r = null;
            if (hitObjectManager.Beatmap.PlayMode == PlayModes.OsuMania)
            {
                r = new HitCircleManiaRow(hitObjectManager, startPosition, startTime, soundType, ManiaConvertType.NotChange, prevRow);
                r.SampleSet = sampleSet;
                r.SampleSetAdditions = addSet;
                r.CustomSampleSet = customSampleSet;
                r.SampleVolume = volume;
                r.GenerateHitObjects();
                
                r.HitObjects.ForEach(n => n.ProcessSampleFile(sampleFile));
                record(startPosition, startTime);
            }
            else
            {
                densityUpdate(startTime);//update before generate notes
                int delta = startTime - prevTime;
                double beatIntv = hitObjectManager.Beatmap.beatLengthAt(startTime, false);
                ManiaConvertType ctype = ManiaConvertType.None;
                if (delta <= 80)// more than 187bpm
                    ctype = ManiaConvertType.ForceNotStack | ManiaConvertType.KeepSingle;
                else if (delta <= 95)  //more than 157bpm
                    ctype = ManiaConvertType.ForceNotStack | ManiaConvertType.KeepSingle | currentStair;
                else if (delta <= 105)//140bpm
                    ctype = ManiaConvertType.ForceNotStack | ManiaConvertType.LowProbability;
                else if (delta <= 125)//120bpm
                    ctype = ManiaConvertType.ForceNotStack | ManiaConvertType.None;
                else
                {
                    float deltaPos = (startPosition - prevPos).Length();
                    ControlPoint cp = hitObjectManager.Beatmap.controlPointAt(startTime);
                    if (delta <= 135 && deltaPos < 20)  //111bpm
                        ctype = ManiaConvertType.Cycle | ManiaConvertType.KeepSingle;
                    else if (delta <= 150 && deltaPos < 20)  //100bpm stream, forceStack
                        ctype = ManiaConvertType.ForceStack | ManiaConvertType.LowProbability;
                    else if (deltaPos < 20 && noteDensity >= beatIntv / 2.5)
                        ctype = ManiaConvertType.Reverse | ManiaConvertType.LowProbability;
                    else if (noteDensity < beatIntv / 2.5 || (cp != null && cp.kiaiMode))  //high note density
                        ctype = ManiaConvertType.None;
                    else   //low note density
                        ctype = ManiaConvertType.LowProbability;
                }
                r = new HitCircleManiaRow(hitObjectManager, startPosition, startTime, soundType, ctype, prevRow);
                r.SampleSet = sampleSet;
                r.SampleSetAdditions = addSet;
                r.CustomSampleSet = customSampleSet;
                r.SampleVolume = volume;
                r.GenerateHitObjects();
                
                prevRow = new bool[hitObjectManager.ManiaStage.Columns.Count];
                foreach (HitCircleMania note in r.HitObjects)
                {
                    prevRow[note.LogicalColumn] = true;
                    if ((ctype & ManiaConvertType.Stair) > 0 && note.LogicalColumn == hitObjectManager.ManiaStage.Columns.Count - 1)
                        currentStair = ManiaConvertType.ReverseStair;
                    else if ((ctype & ManiaConvertType.ReverseStair) > 0 && note.LogicalColumn == hitObjectManager.ManiaStage.RandomStart)
                        currentStair = ManiaConvertType.Stair;
                    note.ProcessSampleFile(sampleFile);
                }
                record(startPosition, startTime);
            }
            return r;
        }

        internal override Slider CreateSlider(Vector2 startPosition, int startTime, bool newCombo,
                                              HitObjectSoundType soundType, CurveTypes curveType, int repeatCount, double sliderLength, List<Vector2> sliderPoints, List<HitObjectSoundType> soundTypes, int comboOffset,
            SampleSet sampleSet, SampleSet addSet, List<SampleSet> sampleSets, List<SampleSet> sampleSetAdditions, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            SliderMania s;
            if (hitObjectManager.Beatmap.PlayMode == PlayModes.OsuMania)
            {
                s = new SliderMania(hitObjectManager, startPosition, startTime, soundType, repeatCount, sliderLength, sliderPoints, soundTypes, ManiaConvertType.NotChange, prevRow);
                s.SampleSet = sampleSet;
                s.SampleSetAdditions = addSet;
                s.CustomSampleSet = customSampleSet;
                s.SampleVolume = volume;
                s.GenerateHitObjects();
                
                s.HitObjects.ForEach(n => n.ProcessSampleFile(sampleFile));
                record(startPosition, s.EndTime);
            }
            else
            {
                ControlPoint cp = hitObjectManager.Beatmap.controlPointAt(startTime);
                ManiaConvertType ctype = ManiaConvertType.None;
                if (cp != null && !cp.kiaiMode)
                    ctype = ManiaConvertType.LowProbability;
                s = new SliderMania(hitObjectManager, startPosition, startTime, soundType, repeatCount, sliderLength, sliderPoints, soundTypes, ctype, prevRow);
                s.SampleSet = sampleSet;
                s.SampleSetAdditions = addSet;
                s.SampleSetList = sampleSets;
                s.SampleSetAdditionList = sampleSetAdditions;
                s.CustomSampleSet = customSampleSet;
                s.SampleVolume = volume;
                s.GenerateHitObjects();
                
                prevRow = new bool[hitObjectManager.ManiaStage.Columns.Count];
                if (s.HitObjects.Count > 1)
                {
                    foreach (HitCircleMania hb in s.HitObjects.FindAll(h => h.EndTime == s.EndTime))
                        prevRow[hb.LogicalColumn] = true;
                }
                else
                {
                    prevRow[s.HitObjects[0].LogicalColumn] = true;
                }
                s.HitObjects.ForEach(n => n.ProcessSampleFile(sampleFile));
                int intv = (s.EndTime - startTime) / repeatCount;
                while (repeatCount-- >= 0)
                {
                    record(startPosition, startTime);
                    densityUpdate(startTime);
                    startTime += intv;
                }
            }
            return s;
        }

        internal override Spinner CreateSpinner(int startTime, int endTime, HitObjectSoundType soundType, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            SpinnerMania s;
            s = new SpinnerMania(hitObjectManager, startTime, endTime, soundType, ManiaConvertType.ForceNotStack, prevRow);
            s.SampleSet = sampleSet;
            s.SampleSetAdditions = addSet;
            s.CustomSampleSet = customSampleSet;
            s.SampleVolume = volume;
            s.GenerateHitObjects();

            s.HitObjects.ForEach(n => n.ProcessSampleFile(sampleFile));
            record(new Vector2(256, 192), endTime);
            densityUpdate(endTime);
            return s;
        }

        internal override HitCircle CreateSpecial(Vector2 startPosition, int startTime, int endTime, bool newCombo,
                                                    HitObjectSoundType soundType, int comboOffset, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            //notice:this type of note is ONLY generated by bms converter or future editor version
            HitCircleManiaHold h = new HitCircleManiaHold(hitObjectManager, hitObjectManager.ManiaStage.ColumnAt(startPosition), startTime, endTime, soundType);
            h.SampleSet = sampleSet;
            h.SampleSetAdditions = addSet;
            h.CustomSampleSet = customSampleSet;
        //    h.LoadSample();
            h.SampleVolume = volume;
            h.ProcessSampleFile(sampleFile);
            record(startPosition, endTime);
            densityUpdate(endTime);
            return h;
        }
    }

    internal enum ManiaConvertType
    {
        None = 0,
        /// <summary>
        /// Keep the same as last row.
        /// </summary>
        ForceStack = 1,
        /// <summary>
        /// Keep different from last row.
        /// </summary>
        ForceNotStack = 2,
        /// <summary>
        /// Keep as single note at its original position.
        /// </summary>
        KeepSingle = 4,
        /// <summary>
        /// Use a lower random value.
        /// </summary>
        LowProbability = 8,
        /// <summary>
        /// Reserved.
        /// </summary>
        Alternate = 16,
        /// <summary>
        /// Ignore the repeat count.
        /// </summary>
        ForceSigSlider = 32,
        /// <summary>
        /// Convert slider to circle.
        /// </summary>
        ForceNotSlider = 64,
        /// <summary>
        /// Notes gathered together.
        /// </summary>
        Gathered = 128,
        Mirror = 256,
        /// <summary>
        /// Change 0 -> 6.
        /// </summary>
        Reverse = 512,
        /// <summary>
        /// 1 -> 5 -> 1 -> 5 like reverse.
        /// </summary>
        Cycle = 1024,
        /// <summary>
        /// Next note will be at column + 1.
        /// </summary>
        Stair = 2048,
        /// <summary>
        /// Next note will be at column - 1.
        /// </summary>
        ReverseStair = 4096,
        /// <summary>
        /// For specific beatmaps.
        /// </summary>
        NotChange = 8192
    }
}