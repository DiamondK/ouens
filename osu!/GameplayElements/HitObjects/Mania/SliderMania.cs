﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.HitObjects.Osu;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input.Handlers;
using osu_common.Helpers;
using osu.Audio;
using osu.GameplayElements.Beatmaps;
using osu.GameModes.Play.Rulesets.Mania;
using System;
using osu.GameplayElements.Scoring;
using osu_common;

namespace osu.GameplayElements.HitObjects.Mania
{
    internal class SliderMania : SliderOsu
    {
        internal List<HitCircleMania> HitObjects;

        private ManiaConvertType convertType;
        private int deltaTime;
        private bool[] lastRow;
        private int lastCount = 0;

        private HitFactoryMania hitFactoryMania;

        internal SliderMania(HitObjectManager hom, Vector2 startPosition, int startTime, HitObjectSoundType soundType,
                              int repeatCount, double sliderLength, List<Vector2> sliderPoints,
                              List<HitObjectSoundType> soundTypes, ManiaConvertType cvtType, bool[] lastRow)
            : base(
                hom, startPosition, startTime, false, soundType, CurveTypes.Bezier, repeatCount, sliderLength, sliderPoints,
                soundTypes, 0)
        {
            hitFactoryMania = hitObjectManager.hitFactory as HitFactoryMania;

            HitObjects = new List<HitCircleMania>();
            EndTime = VirtualEndTime;   //that's enough.
            convertType = cvtType;
            deltaTime = Length / repeatCount;
            int col = hitObjectManager.ManiaStage.ColumnAt(startPosition);
            this.lastRow = lastRow;

       //     useBomb = false;
            for (int i = 0; i < lastRow.Length; i++)
            {
                if (lastRow[i])
                {
                    lastCount++;
                }
            }
        }

        internal void GenerateHitObjects()
        {
            int col = hitObjectManager.ManiaStage.ColumnAt(Position);
            int repeatCount = SegmentCount;

            //for specific only
            if ((convertType & ManiaConvertType.NotChange) > ManiaConvertType.None
                || hitObjectManager.ManiaStage.Columns.Count == 1)
            {
                Add(col, StartTime, EndTime);
                return;
            }

            if (repeatCount > 1)
            {
                if (deltaTime <= 90)
                {
                    AddRandomNote(1, StartTime, EndTime - StartTime);
                }
                else if (deltaTime <= 120)  //ignore repeatCount
                {
                    //    AddRandomNote(1, StartTime, EndTime - StartTime, 1);
                    this.convertType |= ManiaConvertType.ForceNotStack;
                    AddNormal(StartTime, deltaTime, repeatCount);
                }
                else if (deltaTime <= 160)  //special conversion
                {
                    AddStair(StartTime, deltaTime, repeatCount);
                }
                else if (deltaTime <= 200 && hitObjectManager.Beatmap.DifficultyBemaniStars > 3.0)
                {
                    AddMultiNormal(StartTime, deltaTime, repeatCount);
                }
                else
                {
                    int total = EndTime - StartTime;
                    if (total >= 4000)
                    {
                        NoteCalculation(StartTime, total, 1, 1, 0.77f);
                    }
                    else if (deltaTime > 400 && total < 4000 && repeatCount < hitObjectManager.ManiaStage.Columns.Count - 1 - hitObjectManager.ManiaStage.RandomStart)
                    {
                        AddTileNote(repeatCount, StartTime, deltaTime);
                    }
                    else
                    {
                        AddLongAndNormal(StartTime, deltaTime, repeatCount);
                    }
                }
            }
            else
            {
                if (deltaTime <= 110)
                {
                    if (lastCount < hitObjectManager.ManiaStage.Columns.Count)
                        this.convertType |= ManiaConvertType.ForceNotStack;
                    else
                        this.convertType &= ~ManiaConvertType.ForceNotStack;
                    AddNormal(StartTime, deltaTime, deltaTime < 80 ? 0 : 1);
                }
                else
                {
                    if (hitObjectManager.Beatmap.DifficultyBemaniStars > 6.5)
                    {
                        if ((convertType & ManiaConvertType.LowProbability) > 0)
                            NoteCalculation(StartTime, EndTime - StartTime, 1f, 0.7f, 0.22f);
                        else
                            NoteCalculation(StartTime, EndTime - StartTime, 0.97f, 0.64f, 0.15f);
                    }
                    else if (hitObjectManager.Beatmap.DifficultyBemaniStars > 4.0)
                    {
                        if ((convertType & ManiaConvertType.LowProbability) > 0)
                            NoteCalculation(StartTime, EndTime - StartTime, 1, 0.92f, 0.57f);
                        else
                            NoteCalculation(StartTime, EndTime - StartTime, 1, 0.82f, 0.44f);
                    }
                    else if (hitObjectManager.Beatmap.DifficultyBemaniStars > 2.5)
                    {
                        if ((convertType & ManiaConvertType.LowProbability) > 0)
                            NoteCalculation(StartTime, EndTime - StartTime, 1, 1, 0.7f);
                        else
                            NoteCalculation(StartTime, EndTime - StartTime, 1, 0.92f, 0.63f);
                    }
                    else
                    {
                        if ((convertType & ManiaConvertType.LowProbability) > 0)
                            NoteCalculation(StartTime, EndTime - StartTime, 1, 1, 0.83f);
                        else
                            NoteCalculation(StartTime, EndTime - StartTime, 1, 1f, 0.73f);
                    }
                }
            }
        }

        /// <summary>
        /// calculate how many notes in a row by giving probability
        /// </summary>
        /// <param name="deltaTime"></param>
        /// <param name="repeatCount"></param>
        /// <param name="noteNeed4">put 4 notes in a row if probability larger than this value</param>
        /// <param name="noteNeed3"></param>
        /// <param name="noteNeed2"></param>
        private void NoteCalculation(int startTime, int deltaTime, float noteNeed4, float noteNeed3, float noteNeed2)
        {
            //reduce probability by Column limit.
            switch (hitObjectManager.ManiaStage.Columns.Count)
            {
                case 2:
                    noteNeed4 = 1f;
                    noteNeed3 = 1f;
                    noteNeed2 = 1f;
                    break;
                case 3:
                    noteNeed4 = 1f;
                    noteNeed3 = 1f;
                    noteNeed2 = Math.Max(noteNeed2, 0.90f);
                    break;
                case 4:
                    noteNeed4 = 1f;
                    noteNeed3 = Math.Max(noteNeed3, 0.96f);
                    noteNeed2 = Math.Max(noteNeed2, 0.7f);
                    break;
                case 5:
                    noteNeed4 = Math.Max(noteNeed4, 0.97f);
                    noteNeed3 = Math.Max(noteNeed3, 0.90f);
                    noteNeed2 = Math.Max(noteNeed2, 0.66f);
                    break;
            }

            if ((convertType & ManiaConvertType.LowProbability)==0 && 
                (SoundType.IsType(HitObjectSoundType.Clap | HitObjectSoundType.Finish)
                || SoundTypeAt(startTime).IsType(HitObjectSoundType.Clap | HitObjectSoundType.Finish)))
                noteNeed2 = 0;
            AddRandomNote(hitFactoryMania.GetRandomValue(noteNeed2, noteNeed3, noteNeed4), startTime, deltaTime);
        }

        private HitObjectSoundType SoundTypeAt(int time)
        {
            if (unifiedSoundAddition)
                return SoundType;
            else
            {
                int index = deltaTime == 0 ? 0 : (time - StartTime) / deltaTime;
                if (index < SoundTypeList.Count)
                    return SoundTypeList[index];
                else
                    return SoundTypeList[SoundTypeList.Count - 1];
            }
        }

        private SampleSet SampleSetAt(int time)
        {
            if (unifiedSoundAddition || (convertType & ManiaConvertType.NotChange) > 0)
            {
                return SampleSet;
            }
            else
            {
                int index = deltaTime == 0 ? 0 : (time - StartTime) / deltaTime;
                return index < SampleSetList.Count ? SampleSetList[index] : SampleSetList[SampleSetList.Count - 1];
            }
        }

        private SampleSet SampleSetAdditionsAt(int time)
        {
            if (unifiedSoundAddition || (convertType & ManiaConvertType.NotChange) > 0)
            {
                return SampleSetAdditions;
            }
            else
            {
                int index = deltaTime == 0 ? 0 : (time - StartTime) / deltaTime;
                return index < SampleSetAdditionList.Count ? SampleSetAdditionList[index] : SampleSetAdditionList[SampleSetAdditionList.Count - 1];
            }
        }

        private void Add(int column, int startTime, int endTime, int siblings = 1, int simultaneousSiblings = 0)
        {
            if (simultaneousSiblings == 0)
                simultaneousSiblings = siblings;

            int volume = SampleVolume;
            if ((convertType & ManiaConvertType.NotChange) == 0)
            {
                if (SampleVolume != 0)
                {
                    volume = Math.Min(SampleVolume, SampleVolume * 4 / 3 / simultaneousSiblings);
                }
                else
                {
                    ControlPoint cp = hitObjectManager.Beatmap.controlPointAt(StartTime);
                    if (cp != null)
                        volume = Math.Min(cp.volume, cp.volume * 4 / 3 / simultaneousSiblings);
                }
            }

            if (startTime == endTime)
            {
                HitCircleMania note = new HitCircleMania(hitObjectManager, column, startTime, SoundTypeAt(startTime));
                note.SampleSet = SampleSetAt(startTime);
                note.SampleSetAdditions = SampleSetAdditionsAt(startTime);
                note.CustomSampleSet = CustomSampleSet;
                note.SampleVolume = volume;
                note.Siblings = siblings;
                HitObjects.Add(note);
            }
            else
            {
                HitCircleManiaLong note = new HitCircleManiaLong(hitObjectManager, column, startTime, endTime, SoundType);
                note.SoundTypeStart = SoundTypeAt(startTime);
                note.SoundTypeEnd = SoundTypeAt(endTime);
                note.SampleSet = SampleSetAt(startTime);
                note.SampleSetAdditions = SampleSetAdditionsAt(startTime);
                note.SampleSetAtEnd = SampleSetAt(endTime);
                note.SampleSetAdditionsAtEnd = SampleSetAdditionsAt(endTime);
                note.SlideSampleSet = SampleSet;
                note.SlideSampleSetAdditions = SampleSetAdditions;
                note.CustomSampleSet = CustomSampleSet;
                note.SampleVolume = volume;
                note.Siblings = siblings;
                HitObjects.Add(note);
            }
        }

        /// <summary>
        /// ■■■
        /// ■■■
        /// ■■■
        /// ■■
        /// ■
        /// </summary>
        /// <param name="noteCount"></param>
        /// <param name="startTime"></param>
        /// <param name="deltaTime"></param>
        private void AddTileNote(int noteCount, int startTime, int deltaTime)
        {
            int start = startTime;
            int end = startTime + deltaTime * noteCount;
            int repeat = Math.Min(noteCount, hitObjectManager.ManiaStage.Columns.Count);
            bool[] exist = new bool[hitObjectManager.ManiaStage.Columns.Count];
            int nextCol = hitObjectManager.ManiaStage.ColumnAt(Position, true);
            if ((convertType & ManiaConvertType.ForceNotStack) > 0 && lastCount < hitObjectManager.ManiaStage.Columns.Count)
            {
                while (lastRow[nextCol])
                    nextCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
            }
            while (repeat > 0)
            {
                while (exist[nextCol])
                    nextCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
                Add(nextCol, start, end, noteCount);
                exist[nextCol] = true;
                start += deltaTime;
                repeat--;
            }
        }

        /// <summary>
        /// Add HitCircleManiaLong HitObjects with random column and count.
        /// </summary>
        /// <param name="noteCount">number of notes at each row</param>
        /// <param name="startTime"></param>
        /// <param name="deltaTime"></param>
        /// <param name="repeatCount"></param>
        private void AddRandomNote(int noteCount, int startTime, int deltaTime)
        {
            int usable = hitObjectManager.ManiaStage.Columns.Count - hitObjectManager.ManiaStage.RandomStart - lastCount;
            if (usable >= noteCount)
            {
                bool[] exist = new bool[hitObjectManager.ManiaStage.Columns.Count];
                int nextCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
                for (int j = 0; j < noteCount; j++)
                {
                    while (exist[nextCol] || lastRow[nextCol])  //find available column
                        nextCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
                    Add(nextCol, startTime, startTime + deltaTime, noteCount);
                    exist[nextCol] = true;
                }
            }
            else
            {
                int restNote = noteCount;
                //placed in empty column first
                bool[] exist = new bool[hitObjectManager.ManiaStage.Columns.Count];
                int nextCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
                for (int j = 0; j < usable; j++)
                {
                    while (exist[nextCol] || lastRow[nextCol])
                        nextCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
                    Add(nextCol, startTime, startTime + deltaTime, noteCount);
                    exist[nextCol] = true;
                    restNote--;
                }
                for (int j = 0; j < restNote; j++)
                {
                    while (exist[nextCol])
                        nextCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
                    Add(nextCol, startTime, startTime + deltaTime, noteCount);
                    exist[nextCol] = true;
                }
            }
        }

        /// <summary>
        /// like this
        /// -
        ///  -
        ///   -
        ///    -
        ///   -
        ///  -
        /// -
        /// </summary>
        private void AddStair(int startTime, int deltaTime, int repeatCount)
        {
            int col = hitObjectManager.ManiaStage.ColumnAt(Position, true);
            bool inc = hitFactoryMania.random.NextDouble() > 0.5 ? true : false;
            while (repeatCount >= 0)
            {
                repeatCount--;
                Add(col, startTime, startTime);
                startTime += deltaTime;
                if (inc)
                {
                    if (col >= hitObjectManager.ManiaStage.Columns.Count - 1)
                    {
                        inc = false;
                        col--;
                    }
                    else
                        col++;
                }
                else
                {
                    if (col <= hitObjectManager.ManiaStage.RandomStart)
                    {
                        inc = true;
                        col++;
                    }
                    else
                        col--;
                }
            }
        }

        /// <summary>
        /// ignore slider structure and add normal hitobject at the giving deltaTime
        /// one note per row,no stack
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="deltaTime"></param>
        /// <param name="repeatCount"></param>
        private void AddNormal(int startTime, int deltaTime, int repeatCount)
        {
            int nextCol = hitObjectManager.ManiaStage.ColumnAt(Position, true);
            if ((convertType & ManiaConvertType.ForceNotStack) > 0 && lastCount < hitObjectManager.ManiaStage.Columns.Count)
            {
                while (lastRow[nextCol])
                    nextCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
            }
            int lastCol = nextCol;
            while (repeatCount >= 0)
            {
                Add(nextCol, startTime, startTime);
                while (nextCol == lastCol)
                    nextCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
                lastCol = nextCol;
                startTime += deltaTime;
                repeatCount--;
            }
        }

        /// <summary>
        /// ignore slider structure and add normal hitobject at the giving deltaTime
        /// 2~3 note per row, depends on difficulty
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="deltaTime"></param>
        /// <param name="repeatCount"></param>
        /// <param name="intv">this value should be in 1~Column-2</param>
        private void AddMultiNormal(int startTime, int deltaTime, int repeatCount)
        {
            bool legacy = hitObjectManager.ManiaStage.Columns.Count >= 4 && hitObjectManager.ManiaStage.Columns.Count <= 8;
            int intv = hitFactoryMania.random.Next(1, hitObjectManager.ManiaStage.Columns.Count - (legacy ? 1 : 0));

            int nextCol = hitObjectManager.ManiaStage.ColumnAt(Position, true);
            while (repeatCount >= 0)
            {
                Add(nextCol, startTime, startTime, 2);
                nextCol += intv;
                if (nextCol >= hitObjectManager.ManiaStage.Columns.Count - hitObjectManager.ManiaStage.RandomStart)
                    nextCol = nextCol - hitObjectManager.ManiaStage.Columns.Count - hitObjectManager.ManiaStage.RandomStart + (legacy ? 1 : 0);
                nextCol += hitObjectManager.ManiaStage.RandomStart;

                // better to not add so many consecutive "doubles" for 2K
                if (hitObjectManager.ManiaStage.Columns.Count > 2)
                    Add(nextCol, startTime, startTime, 2);

                nextCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
                startTime += deltaTime;
                repeatCount--;
            }
        }

        /// <summary>
        /// like
        /// ■  _ _
        /// ■   _
        /// ■    _
        /// ■   _  
        /// ■  _  _
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="deltaTime"></param>
        /// <param name="repeatCount"></param>
        private void AddLongAndNormal(int startTime, int deltaTime, int repeatCount)
        {
            int longCol = hitObjectManager.ManiaStage.ColumnAt(Position, true);
            if ((convertType & ManiaConvertType.ForceNotStack) > 0 && lastCount < hitObjectManager.ManiaStage.Columns.Count)
            {
                while (lastRow[longCol])
                    longCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
            }
            //the long note
            Add(longCol, startTime, EndTime);
            bool[] exist = new bool[hitObjectManager.ManiaStage.Columns.Count];
            int nextCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
            int noteNeed = 1;
            if (hitObjectManager.Beatmap.DifficultyBemaniStars > 6.5)
            {
                noteNeed = hitFactoryMania.GetRandomValue(0.37f, 1f);
            }
            else if (hitObjectManager.Beatmap.DifficultyBemaniStars > 4.0)
            {
                noteNeed = hitFactoryMania.GetRandomValue(hitObjectManager.ManiaStage.Columns.Count < 6 ? 0.88f : 0.55f, 1f);
            }
            else if (hitObjectManager.Beatmap.DifficultyBemaniStars > 2.5)
            {
                noteNeed = hitFactoryMania.GetRandomValue(hitObjectManager.ManiaStage.Columns.Count < 6 ? 1 : 0.76f, 1f);
            }
            else
                noteNeed = 0;

            noteNeed = Math.Min(noteNeed, hitObjectManager.ManiaStage.Columns.Count - 1);

            bool ignoreHead = (SoundTypeAt(startTime) <= HitObjectSoundType.Normal);
            while (repeatCount >= 0)
            {
                if (!(ignoreHead && startTime == StartTime))
                {
                    for (int i = 0; i < noteNeed; i++)
                    {
                        while (exist[nextCol] || nextCol == longCol)
                            nextCol = hitFactoryMania.random.Next(hitObjectManager.ManiaStage.RandomStart, hitObjectManager.ManiaStage.Columns.Count);
                        int siblings = noteNeed + 1;
                        int simultaneousSiblings = noteNeed + (repeatCount == SegmentCount || repeatCount == 0 ? 1 : 0);
                        Add(nextCol, startTime, startTime, siblings, simultaneousSiblings);
                        exist[nextCol] = true;
                    }
                }
                startTime += deltaTime;
                exist = new bool[hitObjectManager.ManiaStage.Columns.Count];
                repeatCount--;
            }
        }
    }
}