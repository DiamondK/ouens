﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input.Handlers;
using osu_common.Helpers;
using osu_common;

namespace osu.GameplayElements.HitObjects.Fruits
{
    internal class SliderFruits : SliderOsu
    {
        private readonly string fruit;
        internal List<HitCircleFruits> Fruits = new List<HitCircleFruits>();

        internal SliderFruits(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType,
                              CurveTypes curveType, int repeatCount, double sliderLength, List<Vector2> sliderPoints,
                              List<HitObjectSoundType> soundTypes, string fruit)
            : base(
                hom, startPosition, startTime, newCombo, soundType, curveType, repeatCount, sliderLength, sliderPoints,
                soundTypes, 0)
        {
            this.fruit = fruit;
            spriteManager = null;
        }

        internal override bool StartIsHit
        {
            get { return true; }
        }

        public override int ComboNumber
        {
            get { return -1; }
            set { base.ComboNumber = -1; }
        }

        internal override bool HitTest(Vector2 testPosition, bool hittableRangeOnly, float radius)
        {
            return StartTime <= AudioEngine.Time && EndTime >= AudioEngine.Time && !IsHit;
        }

        internal override IncreaseScoreType HitStart()
        {
            return IncreaseScoreType.Ignore;
        }

        internal override void SetColour(Color color)
        {
        }

        internal override IncreaseScoreType Hit()
        {
            IsHit = true;
            return IncreaseScoreType.Ignore;
        }

        internal override void Draw()
        {
        }

        internal override void Update()
        {
            UpdateCalculations(false);

            base.Update();

            sliderBall.FlipHorizontal = false;
            sliderBall.FlipVertical = false;
        }

        internal override void Shake()
        {
        }

        internal override void UpdateCalculations(bool force = true)
        {
            base.UpdateCalculations(force);

            // We need to abort here, otherwise we will run out of memory by creating too many tiny ticks for this slider.
            if (Length > 60000)
            {
                throw new ArgumentOutOfRangeException("Slider is too long. (Over 1 minute!)");
            }

            bool useCorrectSamplesets = hitObjectManager.Beatmap.PlayMode != PlayModes.CatchTheBeat 
                || hitObjectManager.Beatmap.BeatmapVersion >= 14;

            Fruits.Clear();

            HitCircleFruits f1 = new HitCircleFruits(hitObjectManager, Position, StartTime, NewCombo, unifiedSoundAddition ? SoundType : SoundTypeList[0], fruit);
            if (useCorrectSamplesets)
                ApplySamplesets(f1, 0);
	        else
	            f1.SampleSetAdditions = SampleSetAdditionList.Count > 0 && SampleSetAdditionList[0] != Audio.SampleSet.None ? SampleSetAdditionList[0] : SampleSetAdditions;
            f1.Type |= HitObjectType.NewCombo; //new colour

            Fruits.Add(f1);

            int lastTime = StartTime;

            HitFactoryFruits hitFactoryFruits = hitObjectManager.hitFactory as HitFactoryFruits;
            
            int fruitIndex = 1;
            for (int i = 0; i < sliderScoreTimingPoints.Count; i++)
            {
                int time = sliderScoreTimingPoints[i];

                if (time - lastTime > 80)
                {
                    float var = (time - lastTime);
                    while (var > 100)
                        var /= 2;


                    for (float j = lastTime + var; j < time; j += var)
                    {
                        HitCircleFruitsTickTiny f = new HitCircleFruitsTickTiny(hitObjectManager, PositionAtTime((int)j) + new Vector2(hitFactoryFruits.random.Next(-20, 20), 0), (int)j, false, SoundType, fruit);
                        Fruits.Add(f);
                    }
                }

                lastTime = time;

                if (i < sliderScoreTimingPoints.Count - 1)
                {
                    int repeatLocation = sliderRepeatPoints.BinarySearch(time);

                    if (repeatLocation >= 0)
                    {
                        HitCircleFruits f = new HitCircleFruits(hitObjectManager, repeatLocation % 2 == 1 ? Position : Position2, time, false, unifiedSoundAddition ? SoundType : SoundTypeList[repeatLocation + 1], fruit);
                        if (useCorrectSamplesets)
                            ApplySamplesets(f, fruitIndex);
                        Fruits.Add(f);

                        fruitIndex++;
                    }
                    else
                    {
                        HitCircleFruitsTick f = new HitCircleFruitsTick(hitObjectManager, PositionAtTime(time), time, false, SoundType, fruit);
                        Fruits.Add(f);
                    }
                }
            }

            HitCircleFruits f2 = new HitCircleFruits(hitObjectManager, EndPosition, EndTime, false, unifiedSoundAddition ? SoundType : SoundTypeList[SoundTypeList.Count - 1], fruit);
            if (useCorrectSamplesets)
                ApplySamplesets(f2, SampleSetAdditionList.Count - 1);
            else
                f2.SampleSetAdditions = SampleSetAdditionList.Count > 0 && SampleSetAdditionList[1] != Audio.SampleSet.None ? SampleSetAdditionList[1] : SampleSetAdditions;

            Fruits.Add(f2);
        }

        internal void ApplySamplesets(HitCircleFruits fruit, int index)
        {
            HitSoundInfo hitSoundInfo = GetHitSoundInfo(index);
            fruit.SampleSet = hitSoundInfo.SampleSet;
            fruit.SampleSetAdditions = hitSoundInfo.SampleSetAdditions;
        }

        internal override IncreaseScoreType GetScorePoints(Vector2 currentMousePos)
        {
            if (IsHit)
                return IncreaseScoreType.Ignore;

            bool allow = MouseManager.GameClickRegistered;
            allow &= AudioEngine.Time >= StartTime;

            return IncreaseScoreType.Ignore;
        }
    }
}