﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.Graphics.Sprites;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Skinning;

namespace osu.GameplayElements.HitObjects.Fruits
{
    class HitCircleFruitsTickTiny : HitCircleFruits
    {
        internal override bool CountCombo { get { return false; } }

        public HitCircleFruitsTickTiny(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType, string fruit)
            : base(hom, startPosition, startTime, newCombo, soundType, @"drop")
        { 
            SpriteCollection.ForEach(s => s.Scale *= 0.4f);

            Transformation rotate = new Transformation(TransformationType.Rotation,0,0, StartTime - hitObjectManager.PreEmpt, StartTime + 2000);
            rotate.EndFloat = (float)rotate.Duration / 200;

            SpriteCollection.ForEach(s => s.Transformations.Add(rotate));
        }

        internal override void PlaySound()
        {
            AudioEngine.PlaySample(AudioEngine.LoadSample(@"slidertick", false, false, AudioEngine.IgnoreBeatmapSamples ? SkinSource.ExceptBeatmap : SkinSource.All));
        }

        internal override IncreaseScoreType Hit()
        {
            IsHit = true;

            hitValue = ValidHit ? IncreaseScoreType.FruitTickTiny : IncreaseScoreType.FruitTickTinyMiss;

            Arm(hitValue > 0);

            return hitValue;
        }
    }
}
