﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Scoring;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu.Helpers;

namespace osu.GameplayElements.HitObjects.Fruits
{
    internal class HitCircleFruits : HitCircle
    {
        private pSprite SpriteHitCircle1;
        private pSprite SpriteHitCircle2;
        public bool ValidHit;
        public Vector2 CatchOffset;
        public bool HyperDash { get { return HyperDashTarget != null; } }
        public HitObject HyperDashTarget;
        internal float DistanceToHyperDash = 0;
        private pSprite SpriteHyperDash;
        internal virtual bool CountCombo { get { return true; } }

        public HitCircleFruits(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType, string fruit)
            : base(hom)
        {
            Position = new Vector2(OsuMathHelper.Clamp(startPosition.X, 0, GameBase.GamefieldDefaultWidth), 340);
            StartTime = startTime;
            EndTime = startTime;
            SoundType = soundType;

            NewCombo = newCombo;

            if (hom.spriteManager != null)
            {
                SpriteHitCircle1 =
                new pSprite(SkinManager.Load(@"fruit-" + fruit), Fields.Gamefield, Origins.Centre,
                            Clocks.Audio, Position, SpriteManager.drawOrderBwd(StartTime), false, Color.White);
                SpriteCollection.Add(SpriteHitCircle1);
                DimCollection.Add(SpriteHitCircle1);
                SpriteHitCircle1.TagNumeric = 1;

                SpriteHitCircle2 =
                    new pSprite(SkinManager.Load(@"fruit-" + fruit + "-overlay"), Fields.Gamefield,
                                Origins.Centre, Clocks.Audio, Position,
                                SpriteManager.drawOrderBwd(StartTime - 1), false, Color.White);
                SpriteCollection.Add(SpriteHitCircle2);
                DimCollection.Add(SpriteHitCircle2);

                SpriteHyperDash =
                    new pSprite(SkinManager.Load(@"fruit-" + fruit), Fields.Gamefield,
                                Origins.Centre, Clocks.Audio, Position,
                                SpriteManager.drawOrderBwd(StartTime + 1), false, Color.TransparentBlack);
                SpriteHyperDash.Additive = true;
                SpriteCollection.Add(SpriteHyperDash);
                DimCollection.Add(SpriteHyperDash);

                Transformation fall1 = new Transformation(new Vector2(Position.X, -100), new Vector2(Position.X, 340), StartTime - hitObjectManager.PreEmpt, StartTime);

                fall1.EndVector.Y += (fall1.EndVector.Y - fall1.StartVector.Y) * 0.2f;
                fall1.Time2 += (int)(fall1.Duration * 0.2f);

                float rotation = (float)GameBase.random.NextDouble() * 0.4f - 0.2f;

                SpriteCollection.ForEach(s =>
                {
                    s.Transformations.Add(fall1);
                    s.Rotation = rotation;
                });

                if (ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.Hidden))
                {
                    Transformation t = new Transformation(TransformationType.Fade, 1, 0, startTime - (int)(hitObjectManager.PreEmpt * 0.6),
                                           startTime - (int)(hitObjectManager.PreEmpt * 0.44));
                    SpriteCollection.ForEach(s => s.Transformations.Add(t));
                }
            }
            
        }

        public override int ComboNumber
        {
            get { return 0; }
            set { }
        }

        public override bool NewCombo
        {
            get;
            set;
        }

        public override Vector2 EndPosition
        {
            get { return Position; }
            set { throw new NotImplementedException(); }
        }

        internal override bool IsVisible
        {
            get { return SpriteHitCircle1.IsVisible; }
        }

        internal override void SetColour(Color colour)
        {
            base.SetColour(colour);
            if (SpriteHitCircle1 != null)
                SpriteHitCircle1.InitialColour = colour;
            Colour = colour;
        }

        internal override IncreaseScoreType Hit()
        {
            IsHit = true;

            if (ValidHit)
            {
                hitValue = IncreaseScoreType.Hit300;
                PlaySound();
            }
            else
            {
                hitValue = CountCombo ? IncreaseScoreType.Miss : IncreaseScoreType.MissHpOnly;
            }

            Arm(hitValue > 0);

            return hitValue;
        }

        internal override void Arm(bool isHit)
        {

        }

        internal override HitObject Clone()
        {
            throw new NotImplementedException();
        }

        internal override void Select()
        {
            throw new NotImplementedException();
        }

        internal override void Deselect()
        {
            throw new NotImplementedException();
        }

        internal override void ModifyTime(int newTime)
        {
            throw new NotImplementedException();
        }

        internal override void ModifyPosition(Vector2 newPosition)
        {
            SpriteCollection.ForEach(p => p.Position = newPosition);
        }

        internal override IncreaseScoreType GetScorePoints(Vector2 currentMousePos)
        {
            throw new NotImplementedException();
        }

        internal override void SetEndTime(int time)
        {
            throw new NotImplementedException();
        }

        internal void MakeHyperDash(HitObject target)
        {
            if (HyperDash) return;

            DistanceToHyperDash = 0;
            HyperDashTarget = target;

            if (hitObjectManager.spriteManager == null)
                return;

            SpriteHyperDash.InitialColour = SkinManager.CurrentFruitsSkin.ColourHyperDashFruit;

            Transformation t = new Transformation(TransformationType.Scale, SpriteHitCircle1.Scale * 1.2f, SpriteHitCircle1.Scale * 1.3f, SpriteHitCircle1.Transformations[0].Time1, SpriteHitCircle1.Transformations[0].Time1 + 500);
            t.Loop = true;
            SpriteHyperDash.Transformations.Add(t);

            if (!ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.Hidden))
            {
                t = new Transformation(TransformationType.Fade, 1, 0.5f, SpriteHitCircle1.Transformations[0].Time1, SpriteHitCircle1.Transformations[0].Time1 + 500);
                t.Loop = true;
                SpriteHyperDash.Transformations.Add(t);
            }
        }
    }
}