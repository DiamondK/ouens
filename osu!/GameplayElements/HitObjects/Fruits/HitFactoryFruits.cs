﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.GameModes.Play;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Scoring;
using osu_common;
using osu_common.Helpers;
using osu.Audio;

namespace osu.GameplayElements.HitObjects.Fruits
{
    class HitFactoryFruits : HitFactory
    {
        float lastStartX;
        int lastStartTime;

        public HitFactoryFruits(HitObjectManager hitObjectMananager) : base(hitObjectMananager)
        {
            random.Reinitialise(1337);
        }

        internal override HitCircle CreateHitCircle(Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType, int comboOffset, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            WarpSpacing(ref startPosition, startTime);

            HitCircleFruits h = new HitCircleFruits(hitObjectManager, startPosition, startTime, newCombo, soundType, GetNextFruit());
            h.Type |= HitObjectType.NewCombo; //because sorting colours needs this to make each fruit a different colour.
            h.SampleSet = sampleSet;
            h.SampleSetAdditions = addSet;
            h.CustomSampleSet = customSampleSet;
            h.SampleVolume = volume;
            h.ProcessSampleFile(sampleFile);
            return h;
        }

        private void WarpSpacing(ref Vector2 position, int startTime)
        {
            if (ModManager.CheckActive(hitObjectManager.ActiveMods, Mods.HardRock))
            {
                if (lastStartX == 0)
                {
                    lastStartX = position.X;
                    lastStartTime = startTime;
                    return;
                }

                float diff = lastStartX - position.X;
                int timeDiff = startTime - lastStartTime;

                if (timeDiff > 1000)
                {
                    lastStartX = position.X;
                    lastStartTime = startTime;
                    return;
                }

                if (diff == 0)
                {
                    bool right = random.NextBool();

                    float rand = Math.Min(20,random.Next(0, timeDiff/4));

                    if (right)
                    {
                        if (position.X + rand <= 512)
                            position.X += rand;
                        else
                            position.X -= rand;
                    }
                    else
                    {
                        if (position.X - rand >= 0)
                            position.X -= rand;
                        else
                            position.X += rand;
                    }

                    return;
                }

                if (Math.Abs(diff) < timeDiff / 3)
                {
                    if (diff > 0)
                    {
                        if (position.X - diff > 0)
                            position.X -= diff;
                    }
                    else
                    {
                        if (position.X - diff < 512)
                            position.X -= diff;
                    }
                }

                lastStartX = position.X;
                lastStartTime = startTime;
            }
        }

        private int currentFruit;
        private static readonly string[] fruits = new[]{"pear","grapes","apple","orange"};
        private static readonly string[] spinner_fruits = new[] { "bananas" };

        private string GetNextFruit()
        {
            string fruit = fruits[currentFruit];
            currentFruit = (currentFruit + 1) % fruits.Length;
            return fruit;
            
        }

        internal string GetRandomFruit()
        {
            return spinner_fruits[random.Next(0, spinner_fruits.Length)];
        }

        internal override Slider CreateSlider(Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType, CurveTypes curveType, int repeatCount, double sliderLength,
                                              List<Vector2> sliderPoints, List<HitObjectSoundType> soundTypes, int comboOffset, SampleSet sampleSet, SampleSet addSet, 
                                                List<SampleSet> sampleSets, List<SampleSet> sampleSetAdditions, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            SliderFruits sf = new SliderFruits(hitObjectManager, startPosition, startTime, newCombo, soundType, curveType, repeatCount, sliderLength, sliderPoints, soundTypes, GetNextFruit());

            lastStartX = sliderPoints[sliderPoints.Count - 1].X;
            lastStartTime = sf.EndTime;

            sf.SampleSet = sampleSet;
            sf.SampleSetAdditions = addSet;
            sf.SampleSetList = sampleSets;
            sf.SampleSetAdditionList = sampleSetAdditions;
            sf.CustomSampleSet = customSampleSet;
            sf.SampleVolume = volume;
            sf.ProcessSampleFile(sampleFile);
            return sf;
        }

        internal override Spinner CreateSpinner(int startTime, int endTime, HitObjectSoundType soundType, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            return new SpinnerFruits(hitObjectManager, startTime, endTime, soundType);
        }

        internal override HitCircle CreateSpecial(Vector2 startPosition, int startTime, int endTime, bool newCombo,
                                                    HitObjectSoundType soundType, int comboOffset, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile)
        {
            return null;
        }

        internal FastRandom random = new FastRandom(1337);
    }
}