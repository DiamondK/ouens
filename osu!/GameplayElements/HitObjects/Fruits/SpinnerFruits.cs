﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Beatmaps;

namespace osu.GameplayElements.HitObjects.Fruits
{
    internal class SpinnerFruits : Spinner
    {
        internal List<HitCircleFruitsSpin> Fruits = new List<HitCircleFruitsSpin>();

        internal SpinnerFruits(HitObjectManager hom, int startTime, int endTime, HitObjectSoundType soundType)
            : base(hom, startTime, endTime, soundType)
        {
            HitFactoryFruits hitFactoryFruits = hitObjectManager.hitFactory as HitFactoryFruits;

            bool first = true;
            float var = (endTime - startTime);
            while (var > 100)
                var /= 2;

            if (var <= 0)
            {
                return;
            }

            int count = 0;
            for (float j = startTime; j <= endTime; j += var)
            {
                HitCircleFruitsSpin f = new HitCircleFruitsSpin(hitObjectManager, new Vector2(hitFactoryFruits.random.Next(0, 512), 0),
                                                                (int) j, first && this.NewCombo, SoundType,
                                                                hitFactoryFruits.GetRandomFruit(), count);
                Fruits.Add(f);

                first = false;
                count++;
            }
        }

        public override int ComboNumber
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override Vector2 EndPosition
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        internal override bool IsVisible
        {
            get { throw new NotImplementedException(); }
        }

        internal override void SetColour(Color color)
        {
            throw new NotImplementedException();
        }

        internal override IncreaseScoreType Hit()
        {
            throw new NotImplementedException();
        }

        internal override HitObject Clone()
        {
            throw new NotImplementedException();
        }

        internal override void Select()
        {
            throw new NotImplementedException();
        }

        internal override void Deselect()
        {
            throw new NotImplementedException();
        }

        internal override void ModifyTime(int newTime)
        {
            throw new NotImplementedException();
        }

        internal override void ModifyPosition(Vector2 newPosition)
        {
            throw new NotImplementedException();
        }

        internal override IncreaseScoreType GetScorePoints(Vector2 currentMousePos)
        {
            throw new NotImplementedException();
        }

        internal override void SetEndTime(int time)
        {
            throw new NotImplementedException();
        }
    }
}