﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using Un4seen.Bass;

namespace osu.GameplayElements.HitObjects.Fruits
{
    class HitCircleFruitsSpin : HitCircleFruits
    {
        private int count;
        internal override bool CountCombo { get { return false; } }

        public HitCircleFruitsSpin(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType, string fruit, int count)
            : base(hom, startPosition, startTime, newCombo, soundType, fruit)
        {
            HitFactoryFruits hitFactoryFruits = hitObjectManager.hitFactory as HitFactoryFruits;

            float startScale = (float)hitFactoryFruits.random.NextDouble() * 1.6f + 0.6f;
            Transformation scale = new Transformation(TransformationType.Scale, startScale, 0.6f, StartTime - hitObjectManager.PreEmpt, StartTime);
            SpriteCollection.ForEach(s => s.Transformations.Add(scale));

            Transformation rotation = new Transformation(TransformationType.Rotation, (float)(GameBase.random.NextDouble() - 0.5f) * 2, (float)(GameBase.random.NextDouble() - 0.5f) * 2, StartTime - hitObjectManager.PreEmpt, StartTime);
            SpriteCollection.ForEach(s => s.Transformations.Add(rotation));

            if (hitObjectManager.ComboColours.Count > 0)
                base.SetColour(hitObjectManager.ComboColours[hitFactoryFruits.random.Next(0, hitObjectManager.ComboColours.Count)]);

            Type &= ~HitObjectType.NewCombo;

            this.count = count;
        }

        internal override void PlaySound()
        {
            AudioSample s = AudioEngine.PlaySample(@"metronomelow", AudioEngine.VolumeSample);
            s.PlaybackRate = 0.77f + count * 0.006f;
        }

        internal override void SetColour(Color colour)
        {
            Color col = colour;
            switch (GameBase.random.Next(0, 3))
            {
                default:
                    col = new Color(255, 240, 0);
                    break;
                case 1:
                    col = new Color(255, 192, 0);
                    break;
                case 2:
                    col = new Color(214, 221, 28);
                    break;
            }
            base.SetColour(col);
        }

        internal override IncreaseScoreType Hit()
        {
            IsHit = true;

            if (ValidHit)
            {
                hitValue = IncreaseScoreType.SpinnerBonus;
                PlaySound();
            }
            else
            {
                hitValue = IncreaseScoreType.Ignore;
            }

            Arm(hitValue > 0);

            return hitValue;
        }

        public void PlayActualSound()
        {
            base.PlaySound();
        }
    }
}
