﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

using osu.GameplayElements.Beatmaps;
using osu.Helpers;
using System.Diagnostics;

namespace osu.GameplayElements.HitObjects.Fruits
{
    class DifficultyHitObjectFruits
    {
        // Factor by how much individual / overall strain decays per second. Those values are results of tweaking a lot and taking into account general feedback.
        internal static readonly double DECAY_BASE = 0.20;
        private const float NORMALIZED_HITOBJECT_RADIUS = 41.0f;
        private const float ABSOLUTE_PLAYER_POSITIONING_ERROR = 16f;
        private float playerPositioningError;

        internal HitObject BaseHitObject;

        // Measures jump difficulty. CtB doesn't have something like button pressing speed or accuracy
        internal double Strain = 1;

        // This is required to keep track of lazy player movement (always moving only as far as necessary)
        // Without this quick repeat sliders / weirdly shaped streams might become ridiculously overrated
        internal float PlayerPositionOffset = 0;
        internal float LastMovement = 0;

        private float NormalizedPosition;
        private float ActualNormalizedPosition
        {
            get
            {
                return NormalizedPosition + PlayerPositionOffset;
            }
        }

        internal DifficultyHitObjectFruits(HitObject BaseHitObject, float catcherWidthHalf)
        {
            this.BaseHitObject = BaseHitObject;

            // We will scale everything by this factor, so we can assume a uniform CircleSize among beatmaps.
            float ScalingFactor = NORMALIZED_HITOBJECT_RADIUS / catcherWidthHalf;
            playerPositioningError = ABSOLUTE_PLAYER_POSITIONING_ERROR;// * ScalingFactor;
            NormalizedPosition = BaseHitObject.Position.X * ScalingFactor;
        }


        private readonly double DIRECTION_CHANGE_BONUS = 12.5;
        internal void CalculateStrains(DifficultyHitObjectFruits PreviousHitObject, double TimeRate)
        {
            // Rather simple, but more specialized things are inherently inaccurate due to the big difference playstyles and opinions make.
            // See Taiko feedback thread.
            double TimeElapsed = (double)(BaseHitObject.StartTime - PreviousHitObject.BaseHitObject.StartTime) / TimeRate;
            double Decay = Math.Pow(DECAY_BASE, TimeElapsed / 1000);

            // Update new position with lazy movement.
            PlayerPositionOffset =
                OsuMathHelper.Clamp(
                    PreviousHitObject.ActualNormalizedPosition,
                    NormalizedPosition - (NORMALIZED_HITOBJECT_RADIUS - playerPositioningError),
                    NormalizedPosition + (NORMALIZED_HITOBJECT_RADIUS - playerPositioningError)) // Obtain new lazy position, but be stricter by allowing for an error of a certain degree of the player.
                - NormalizedPosition; // Subtract HitObject position to obtain offset

            LastMovement = DistanceTo(PreviousHitObject);
            double Addition = SpacingWeight(LastMovement);

            if (NormalizedPosition < PreviousHitObject.NormalizedPosition)
            {
                LastMovement = -LastMovement;
            }

            HitCircleFruits previousHitCircle = PreviousHitObject.BaseHitObject as HitCircleFruits;
            
            double additionBonus = 0;
            double sqrtTime = Math.Sqrt(Math.Max(TimeElapsed, 25));

            // Direction changes give an extra point!
            if (Math.Abs(LastMovement) > 0.1)
            {
                if (Math.Abs(PreviousHitObject.LastMovement) > 0.1 && Math.Sign(LastMovement) != Math.Sign(PreviousHitObject.LastMovement))
                {
                    double bonus = DIRECTION_CHANGE_BONUS / sqrtTime;

                    // Weight bonus by how 
                    double bonusFactor = Math.Min(playerPositioningError, Math.Abs(LastMovement)) / playerPositioningError;

                    // We want time to play a role twice here!
                    Addition += bonus * bonusFactor;

                    // Bonus for tougher direction switches and "almost" hyperdashes at this point
                    if (previousHitCircle != null && previousHitCircle.DistanceToHyperDash <= 10)
                    {
                        additionBonus += 0.3 * bonusFactor;
                    }
                }

                // Base bonus for every movement, giving some weight to streams.
                Addition += 7.5 * Math.Min(Math.Abs(LastMovement), NORMALIZED_HITOBJECT_RADIUS * 2) / (NORMALIZED_HITOBJECT_RADIUS * 6) / sqrtTime;
            }

            // Bonus for "almost" hyperdashes at corner points
            if (previousHitCircle != null && previousHitCircle.DistanceToHyperDash <= 10)
            {
                if (!previousHitCircle.HyperDash)
                {
                    additionBonus += 1.0;
                }
                else
                {
                    // After a hyperdash we ARE in the correct position. Always!
                    PlayerPositionOffset = 0;
                }

                Addition *= 1.0 + additionBonus * ((10 - previousHitCircle.DistanceToHyperDash) / 10);
            }

            Addition *= 850.0 / Math.Max(TimeElapsed, 25);

            Strain = PreviousHitObject.Strain * Decay + Addition;
        }


        private static double SpacingWeight(float distance)
        {
            return Math.Pow(distance, 1.3) / 500;
        }

        internal float DistanceTo(DifficultyHitObjectFruits other)
        {
            return Math.Abs(ActualNormalizedPosition - other.ActualNormalizedPosition);
        }
    }
}
