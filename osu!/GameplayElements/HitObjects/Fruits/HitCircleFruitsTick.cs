﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Sprites;


namespace osu.GameplayElements.HitObjects.Fruits
{
    class HitCircleFruitsTick : HitCircleFruits
    {
        public HitCircleFruitsTick(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType, string fruit)
            : base(hom, startPosition, startTime, newCombo, soundType, "drop")
        { 
            SpriteCollection.ForEach(s => s.Scale *= 0.8f);

            HitFactoryFruits hitFactoryFruits = hitObjectManager.hitFactory as HitFactoryFruits;
            float startRotation = (float)hitFactoryFruits.random.NextDouble() * 2;

            Transformation rotate = new Transformation(TransformationType.Rotation, startRotation, 0, StartTime - hitObjectManager.PreEmpt, StartTime + 2000);
            rotate.EndFloat = (float)rotate.Duration / 200 + startRotation;

            SpriteCollection.ForEach(s => s.Transformations.Add(rotate));
        }

        internal override void PlaySound()
        {
            ControlPoint pt = hitObjectManager.Beatmap.controlPointAt(StartTime + 2);
            SampleSet ss = SampleSet == SampleSet.None ? pt.sampleSet : SampleSet;
            AudioEngine.PlayTickSamples(new HitSoundInfo(SoundType, ss, pt.customSamples, pt.volume, SampleSetAdditions == Audio.SampleSet.None ? ss : SampleSetAdditions), PositionalSound);
        }

        internal override IncreaseScoreType Hit()
        {
            IsHit = true;

            if (ValidHit)
            {
                hitValue = IncreaseScoreType.FruitTick;
                PlaySound();
            }
            else
            {
                hitValue = IncreaseScoreType.Miss;
            }

            Arm(hitValue > 0);

            return hitValue;
        }
    }
}
