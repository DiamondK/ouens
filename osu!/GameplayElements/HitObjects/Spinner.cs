﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Skinning;

namespace osu.GameplayElements.HitObjects
{
    abstract class Spinner : HitObject
    {
        protected Spinner(HitObjectManager hom, int startTime, int endTime, HitObjectSoundType soundType)
            : base(hom)
        {
            Position = new Vector2(GameBase.GamefieldDefaultWidth / 2, GameBase.GamefieldDefaultHeight / 2);
            StartTime = startTime;
            EndTime = endTime;

            Type = HitObjectType.Spinner;
            if (hitObjectManager.Beatmap.BeatmapVersion <= 8) Type |= HitObjectType.NewCombo;

            SoundType = soundType;
            Colour = Color.Gray;
        }

        internal override void PlaySound()
        {
            ControlPoint pt = hitObjectManager.Beatmap.controlPointAt(EndTime);
            if (pt != null)
                PlaySound(ref pt, SoundType, SampleSet, SampleSetAdditions);
        }
    }
}