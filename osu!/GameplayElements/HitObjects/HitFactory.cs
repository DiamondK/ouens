﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using osu.GameplayElements.HitObjects.Osu;
using osu.Audio;

namespace osu.GameplayElements.HitObjects
{
    internal abstract class HitFactory
    {
        protected readonly HitObjectManager hitObjectManager;

        internal HitFactory(HitObjectManager hitObjectMananager)
        {
            hitObjectManager = hitObjectMananager;
        }

        internal abstract HitCircle CreateHitCircle(Vector2 startPosition, int startTime, bool newCombo,
                                                    HitObjectSoundType soundType, int comboOffset, SampleSet sampleSet, SampleSet addSet,CustomSampleSet customSampleSet,int volume,string sampleFile);

        internal abstract Slider CreateSlider(Vector2 startPosition, int startTime, bool newCombo,
                                              HitObjectSoundType soundType, CurveTypes curveType, int repeatCount, double sliderLength, List<Vector2> sliderPoints, List<HitObjectSoundType> soundTypes, int comboOffset,
                                              SampleSet sampleSet, SampleSet addSet, List<SampleSet> sampleSets, List<SampleSet> sampleSetAdditions, CustomSampleSet customSampleSet, int volume, string sampleFile);

        internal abstract Spinner CreateSpinner(int startTime, int endTime, HitObjectSoundType soundType, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile);
        internal abstract HitCircle CreateSpecial(Vector2 startPosition, int startTime, int endTime,bool newCombo,
                                                    HitObjectSoundType soundType, int comboOffset, SampleSet sampleSet, SampleSet addSet, CustomSampleSet customSampleSet, int volume, string sampleFile);
    }
}