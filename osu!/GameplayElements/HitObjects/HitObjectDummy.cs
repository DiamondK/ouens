﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Beatmaps;

namespace osu.GameplayElements.HitObjects
{
    internal class HitObjectDummy : HitObject
    {
        internal HitObjectDummy(HitObjectManager hom, int time)
            : base(hom)
        {
            StartTime = time;
            EndTime = time;
        }

        public override int ComboNumber
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override Vector2 EndPosition
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        internal override bool IsVisible
        {
            get { throw new NotImplementedException(); }
        }

        internal override void SetColour(Color color)
        {
            throw new NotImplementedException();
        }

        internal override IncreaseScoreType Hit()
        {
            throw new NotImplementedException();
        }

        internal override HitObject Clone()
        {
            throw new NotImplementedException();
        }

        internal override void Select()
        {
            throw new NotImplementedException();
        }

        internal override void Deselect()
        {
            throw new NotImplementedException();
        }

        internal override void ModifyTime(int newTime)
        {
            throw new NotImplementedException();
        }

        internal override void ModifyPosition(Vector2 newPosition)
        {
            throw new NotImplementedException();
        }

        internal override IncreaseScoreType GetScorePoints(Vector2 currentMousePos)
        {
            throw new NotImplementedException();
        }

        internal override void SetEndTime(int time)
        {
            throw new NotImplementedException();
        }
    }
}