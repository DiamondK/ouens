#region Using Statements

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Fruits;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Primitives;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Helpers;
using osu.Input;
using osu_common;
using osu.GameModes.Edit;

#endregion

namespace osu.GameplayElements
{
    internal abstract partial class HitObjectManager : HitObjectManagerBase, IDisposable
    {
        internal void LoadObjects(List<HitObject> objects)
        {
            if (objects == null) return;

            spriteManager.Clear();
            hitObjects = objects;
        }

        internal virtual void ComboColoursReset(bool dimmed = false)
        {
            ComboColours.Clear();
            for (int i = 1; i <= SkinManager.MAX_COLOUR_COUNT; i++)
            {
                Color c = SkinManager.LoadColour(@"Combo" + i, !Beatmap.DisableSkin && !ConfigManager.sIgnoreBeatmapSkins);
                if (c.A != 0)
                    ComboColours.Add(c);
            }

            if (dimmed)
            {
                List<Color> dimmedList = new List<Color>();

                foreach (Color c in ComboColours)
                    dimmedList.Add(new Color((byte)(80 + c.R / 2), (byte)(80 + c.G / 2), (byte)(80 + c.B / 2)));

                ComboColours = dimmedList;
            }

        }

        public static void UpdateStaticVarsAllInstances(bool b)
        {
            lock (Instances)
                foreach (HitObjectManager h in Instances)
                    h.UpdateVariables(b);
        }

        public static void OnHitSound(HitSoundInfo hitSoundInfo)
        {
            lock (Instances)
                if (Instances.Count > 0 && Instances[0].eventManager != null)
                    Instances[0].eventManager.InvokeOnHitSound(hitSoundInfo);
        }

        #region Nested type: IntBoolStringBoolDelegate

        internal delegate void IntBoolStringBoolDelegate(IncreaseScoreType value, string str, HitObject ho);

        #endregion

        #region Statically Available Variables


        internal static Matrix projMatrix;
        internal static List<HitObjectManager> Instances = new List<HitObjectManager>();


        internal static readonly int ComboGeki = 95;
        internal static readonly int ComboKatu = 78;

        //Scaled fade in for aiming circle
        internal static readonly int FadeIn = 400;
        internal static readonly int FadeOut = 240;

        /// <summary>
        /// Distance between consecutive follow-line sprites.
        /// </summary>
        internal static readonly int FollowLineDistance = 32;

        internal static readonly int GamefieldSpriteRes = 128;

        /// <summary>
        /// Number of milliseconds to preempt the follow line.  Higher will make the line appear earlier.
        /// </summary>
        internal static readonly int FollowLinePreEmpt = 800;

        //Accuracy (in ms) for each kind of hit success

        //Time to fadein/out the hit amounts
        internal static readonly int HitFadeIn = 120;
        internal static readonly int HitFadeOut = 600;

        internal static readonly int PostEmpt = 500;

        internal enum Difficulties
        {
            Easy,
            Medium,
            Hard,
            Insane
        } ;

        #endregion

        #region General & Drawing

        internal Beatmap Beatmap;
        internal Obfuscated<Mods> ActiveMods;
        internal bool IsDifficultyCalculator;


        internal void SetBeatmap(Beatmap beatmap, Mods mods)
        {
            Beatmap = beatmap;
            ActiveMods = mods;

            // We want a new hitfactory for our new beatmap & mods combination! (different behaviour)
            hitFactory = CreateHitFactory();

            OnSetBeatmap();
        }

        protected virtual void OnSetBeatmap()
        {

        }


        public override BeatmapBase GetBeatmap()
        {
            return Beatmap;
        }

        public override List<HitObjectBase> GetHitObjects()
        {
            List<HitObjectBase> baseHitObjects = new List<HitObjectBase>();
            foreach (HitObject h in hitObjects)
                baseHitObjects.Add(h.Clone());

            return baseHitObjects;
        }

        public override double SliderVelocityAt(int time)
        {
            double beatLength = Beatmap.beatLengthAt(time);

            if (beatLength > 0)
                return
                    (SliderScoringPointDistance * Beatmap.DifficultySliderTickRate *
                     (1000F / beatLength));
            return SliderScoringPointDistance * Beatmap.DifficultySliderTickRate;
        }

        /// <summary>
        /// Applies the mods DoubleTime and HalfTime to a given rate.
        /// </summary>
        /// <param name="time">Rate to be modified.</param>
        /// <param name="mods">Mods to be applied.</param>
        /// <returns>Modified rate.</returns>
        public static double ApplyModsToRate(double rate, Mods mods)
        {
            if (ModManager.CheckActive(mods, Mods.DoubleTime))
            {
                return rate * 1.5;
            }
            else if (ModManager.CheckActive(mods, Mods.HalfTime))
            {
                return rate * 0.75;
            }
            else
            {
                return rate;
            }
        }

        public static double ApplyModsToRate(double rate)
        {
            return ApplyModsToRate(rate, ModManager.ModStatus);
        }

        /// <summary>
        /// Applies the mods DoubleTime and HalfTime to a given time or time interval.
        /// </summary>
        /// <param name="time">Time to be modified.</param>
        /// <param name="mods">Mods to be applied.</param>
        /// <returns>Modified time or time interval.</returns>
        public static double ApplyModsToTime(double time, Mods mods)
        {
            if (ModManager.CheckActive(mods, Mods.DoubleTime))
            {
                return time / 1.5;
            }
            else if (ModManager.CheckActive(mods, Mods.HalfTime))
            {
                return time / 0.75;
            }
            else
            {
                return time;
            }
        }

        public static double ApplyModsToTime(double time)
        {
            return ApplyModsToTime(time, ModManager.ModStatus);
        }

        /// <summary>
        /// Applies the mods Easy and HardRock to the provided difficulty value.
        /// </summary>
        /// <param name="difficulty">Difficulty value to be modified.</param>
        /// <param name="hardRockFactor">Factor by which HardRock increases difficulty.</param>
        /// <param name="mods">Mods to be applied.</param>
        /// <returns>Modified difficulty value.</returns>
        public static double ApplyModsToDifficulty(double difficulty, double hardRockFactor, Mods mods)
        {
            if (GameBase.Mode != OsuModes.Edit &&
                ModManager.CheckActive(mods, Mods.Easy))
                difficulty = Math.Max(0, difficulty / 2);
            if (GameBase.Mode != OsuModes.Edit &&
                ModManager.CheckActive(mods, Mods.HardRock))
                difficulty = Math.Min(10, difficulty * hardRockFactor);

            return difficulty;
        }

        public static double ApplyModsToDifficulty(double difficulty, double hardRockFactor)
        {
            return ApplyModsToDifficulty(difficulty, hardRockFactor, ModManager.ModStatus);
        }

        /// <summary>
        /// Applies currently active mods to a difficulty value representing circle size.
        /// </summary>
        /// <param name="difficulty">Circle size difficulty to be modified.</param>
        /// <returns>Modified circle size.</returns>
        public override double AdjustDifficulty(double difficulty)
        {
            return (HitObjectManager.ApplyModsToDifficulty(difficulty, 1.3, ActiveMods) - 5) / 5;
        }

        /// <summary>
        /// Maps a difficulty value [0, 10] to a range of resulting values with respect to currently active mods.
        /// </summary>
        /// <param name="difficulty">The difficulty value to be mapped.</param>
        /// <param name="min">Minimum of the resulting range which will be achieved by a difficulty value of 0.</param>
        /// <param name="mid">Midpoint of the resulting range which will be achieved by a difficulty value of 5.</param>
        /// <param name="max">Maximum of the resulting range which will be achieved by a difficulty value of 10.</param>
        /// <returns>Value to which the difficulty value maps in the specified range.</returns>
        public override double MapDifficultyRange(double difficulty, double min, double mid, double max)
        {
            difficulty = HitObjectManager.ApplyModsToDifficulty(difficulty, 1.4, ActiveMods);

            if (difficulty > 5)
                return mid + (max - mid) * (difficulty - 5) / 5;
            if (difficulty < 5)
                return mid - (mid - min) * (5 - difficulty) / 5;
            return mid;
        }

        internal float SpriteRatio;
        /// <summary>
        /// Gets the display size of the sprite.  That this does not consider shrinking to gamefield ratio.
        /// </summary>
        /// <value>The display size of the sprite.</value>
        internal float SpriteDisplaySize;
        internal List<int> Bookmarks;
        internal bool BookmarksDontDelete;
        //Used in editor to prolong the fadeout so we can see a trail of notes
        internal int ForceFadeOut = 1600;
        internal List<Color> ComboColours = new List<Color>();
        public int PreEmptSliderComplete;
        internal int CurrentComboBad;
        internal int CurrentComboKatu;
        internal int currentHitObjectIndex;
        internal EventManager eventManager;
        protected internal HitFactory hitFactory;
        internal List<HitObject> hitObjects = new List<HitObject>();
        internal int hitObjectsCount;
        internal List<HitObject> hitObjectsMinimal = new List<HitObject>();
        internal HitObject lastHitObject;
        internal SpriteManager spriteManager;
        private Dictionary<string, string> Variables;
        internal StageMania ManiaStage;
        private List<pAnimation> followPoints = new List<pAnimation>();

        protected virtual bool supportWidescreen
        {
            get { return false; }
        }

        /// <summary>
        /// The hit object with the earliest hittable start time, for IsPlaying checks.
        /// </summary>
        internal virtual HitObject EarliestHitObject
        {
            get { return hitObjectsCount > 0 ? hitObjects[0] : null; }
        }

        /// <summary>
        /// The hit object with the latest hittable end time, for IsPlaying checks.
        /// </summary>
        internal virtual HitObject LatestHitObject
        {
            get { return hitObjectsCount > 0 ? hitObjects[hitObjectsCount - 1] : null; }
        }

        internal HitObjectManager(bool withSpriteManager)
        {
            if (withSpriteManager)
            {
                spriteManager = new SpriteManager(supportWidescreen)
                {
                    HandleInput = false,
                    //AllowRewind = InputManager.ReplayMode
                };
                spriteManager.HandleFirstDraw();

                // Instances and unload are only used in conjunction with drawing and sounds. Pointless and not threadsafe to use them without a spritemanager.
                lock (Instances) Instances.Add(this);
                GameBase.OnUnload += GameBase_OnUnload;
            }

            if (OsuModes.Edit != GameBase.Mode)
            {
                ForceFadeOut = 0;

                if (spriteManager != null)
                    spriteManager.ForwardPlayOptimisations = true;
            }
            else
                ForceFadeOut = 1600;
        }



        void GameBase_OnUnload(bool b)
        {
            foreach (HitObject h in hitObjects)
            {
                SliderOsu s = h as SliderOsu;
                if (s != null)
                    s.RemoveBody();
            }
        }
        protected virtual bool UseComboColours
        {
            get { return (BeatmapManager.Current.PlayMode != PlayModes.Taiko); }
        }

        protected virtual bool AllowComboBonuses
        {
            get { return true; }
        }

        /// <summary>
        /// HitObjects outside this ms range will shake rather than get hit/missed.
        /// </summary>
        internal const int HITTABLE_RANGE = 400;

        internal abstract HitFactory CreateHitFactory();

        internal List<HitObject> Clone()
        {
            List<HitObject> clonedObjects = new List<HitObject>();
            foreach (HitObject h in hitObjects)
                clonedObjects.Add(h.Clone());

            return clonedObjects;
        }

        internal void UpdateVariables()
        {
            UpdateVariables(true, false);
        }

        internal void UpdateVariables(bool redrawSliders)
        {
            UpdateVariables(redrawSliders, false);
        }

        internal virtual void UpdateVariables(bool redrawSliders, bool lazy)
        {
            if (Beatmap != null)
            {
                SliderScoringPointDistance =
                    ((100 * Beatmap.DifficultySliderMultiplier) /
                     Beatmap.DifficultySliderTickRate);

                SpinnerRotationRatio =
                    MapDifficultyRange(Beatmap.DifficultyOverall, 3, 5, 7.5);

                HitWindow50 = (int)MapDifficultyRange(Beatmap.DifficultyOverall, 200, 150, 100);
                HitWindow100 = (int)MapDifficultyRange(Beatmap.DifficultyOverall, 140, 100, 60);
                HitWindow300 = (int)MapDifficultyRange(Beatmap.DifficultyOverall, 80, 50, 20);

                PreEmpt = (int)MapDifficultyRange(Beatmap.DifficultyApproachRate, 1800, 1200, 450);

                SpriteDisplaySize =
                    (float)
                    (GameBase.GamefieldWidth / 8f *
                    (1f - 0.7f * AdjustDifficulty(Beatmap.DifficultyCircleSize)));

                //Builds of osu! up to 2013-05-04 had the gamefield being rounded down, which caused incorrect radius calculations
                //in widescreen cases. This ratio adjusts to allow for old replays to work post-fix, which in turn increases the lenience
                //for all plays, but by an amount so small it should only be effective in replays.
                const float broken_gamefield_rounding_allowance = 1.00041f;

                HitObjectRadius = SpriteDisplaySize / 2f / GameBase.GamefieldRatio * broken_gamefield_rounding_allowance;

                PreEmptSliderComplete = (int)(PreEmpt * (float)2 / 3);

                if (lazy == false)
                    AudioEngine.UpdateActiveTimingPoint(true);

                SpriteRatio = SpriteDisplaySize / GamefieldSpriteRes;
                if (spriteManager != null)
                {
                    spriteManager.GamefieldSpriteRatio = SpriteRatio;
                }

                StackOffset = HitObjectRadius / 10;
                if (redrawSliders)
                    UpdateSlidersAll(false);
            }
        }

        internal void UpdateSliders()
        {
            UpdateSliders(false, false);
        }

        internal void UpdateSliders(bool visibleOnly, bool force)
        {
            int timeStart = (int)AudioEngine.CurrentOffset;

            int timeEnd = (int)(AudioEngine.ControlPoints.Count - 1 > AudioEngine.ActiveTimingPointIndex
                                     ? AudioEngine.ControlPoints[AudioEngine.ActiveTimingPointIndex + 1].offset
                                     : AudioEngine.AudioLength);
            if (hitObjects == null) return;
            List<HitObject> sliders = hitObjects.FindAll(o => o.StartTime >= timeStart && o.EndTime <= timeEnd &&
                                                              o.IsType(HitObjectType.Slider) &&
                                                              (!visibleOnly || o.IsVisible));
            int c = sliders.Count;
            for (int i = 0; i < c; i++)
            {
                SliderOsu s = (SliderOsu)sliders[i];
                if (force)
                    s.SpatialLength = 0;
                s.UpdateCalculations();
            }
        }

        internal void UpdateSlidersAll(bool force)
        {
            if (hitObjects == null) return;
            List<HitObject> sliders = hitObjects.FindAll(o => (o.Type & HitObjectType.Slider) > 0);
            int c = sliders.Count;
            for (int i = 0; i < c; i++)
            {
                SliderOsu s = (SliderOsu)sliders[i];
                if (force) s.SpatialLength = 0;
                s.UpdateCalculations();
            }
        }

        internal static void UpdateProjectionMatrix()
        {
            float viewportWidth = GameBase.graphics.PreferredBackBufferWidth;
            float viewportHeight = GameBase.graphics.PreferredBackBufferHeight;
            float scaleX = 1.0f / (viewportWidth / 2);
            float scaleY = 1.0f / (viewportHeight / 2);
            projMatrix = Matrix.CreateScale(scaleX, scaleY, 1) *
                         Matrix.CreateScale(1, -1, 1) *
                         Matrix.CreateTranslation(-1, 1, 1);
        }

        //Combo colours - cycled through this array
        internal event IntBoolStringBoolDelegate ForcedHit;

        protected void OnHit(IncreaseScoreType type, string s, HitObject h)
        {
            if (ForcedHit != null)
                ForcedHit(type, s, h);
        }

        internal void Clear()
        {
            //Clear counts instantly to ensure nothing strange happens.
            hitObjectsCount = 0;

            followPoints.Clear();

            spriteManager.Clear();
            if (hitObjects != null)
            {
                foreach (HitObject h in hitObjects)
                    h.Dispose();
                hitObjects.Clear();
            }
        }

        public virtual void Dispose()
        {
            GameBase.OnUnload -= GameBase_OnUnload;

            lock (Instances) Instances.Remove(this);

            foreach (HitObject h in hitObjects)
            {
                h.StopSound();
                h.Dispose();
            }

            if (spriteManager != null)
            {
                spriteManager.Dispose();
            }

            if (eventManager != null)
                eventManager.Dispose();


            hitObjects.Clear();
            hitObjects = null;
            hitObjectsCount = 0;

            if (Variables != null) Variables.Clear();
        }

        internal HitObject FindObjectAt(int x, int y, float radius)
        {
            if (hitObjectsMinimal.Count == 0)
                return null;

            Vector2 loc = new Vector2(x, y);

            HitObject closest = null;
            int closestDistance = 0;

            foreach (HitObject h in hitObjectsMinimal)
            {
                if (!h.IsVisible) continue;

                int time = Math.Min(Math.Abs(h.StartTime - AudioEngine.Time),
                                    Math.Abs(h.EndTime - AudioEngine.Time));

                if (Vector2.DistanceSquared(loc, h.Position) < radius * radius ||
                    Vector2.DistanceSquared(loc, h.Position2) < radius * radius)
                {
                    if (closest == null || time < closestDistance)
                    {
                        closest = h;
                        closestDistance = time;
                    }
                }

                if (h.IsType(HitObjectType.Slider) && closest == null)
                {
                    SliderOsu s = h as SliderOsu;
                    if (s.sliderCurveSmoothLines != null)
                        foreach (Line l in s.sliderCurveSmoothLines)
                        {
                            if (Vector2.Distance(loc, l.p1) < radius)
                            {
                                closest = h;
                                closestDistance = time;
                                break;
                            }
                        }
                }
            }

            return closest;
        }

        internal HitObject FindCircleAt(float x, float y, bool hittableRangeOnly)
        {
            return FindCircleAt(x, y, hittableRangeOnly, HitObjectRadius);
        }

        internal HitObject FindCircleAt(float x, float y, bool hittableRangeOnly, float radius)
        {
            bool checkScorable = !InputManager.ReplayMode && hittableRangeOnly;
            //Non-scorable HitObjects should not be returned by this function...
            //unless we are reading from a replay.
            //This is kinda hacky in retrospect.

            Vector2 v = new Vector2(x, y);

            foreach (HitObject h in hitObjectsMinimal)
                if ((!checkScorable || h.IsScorable) && h.HitTest(v, hittableRangeOnly, radius))
                    return h;
            return null;
        }

        HitObject lastAddedObject;

        internal virtual void AddSpecial(HitObject h) { }
        internal virtual void RemoveSpecial(HitObject h) { }

        internal virtual void Add(HitObject h, bool removeExisting = false)
        {
            if (removeExisting)
            {
                HitObject toDelete = hitObjectsMinimal.Find(t => t.StartTime == h.StartTime);
                if (toDelete != null)
                    Remove(toDelete, false);
            }

            int startIndex = hitObjects.BinarySearch(h);

            if (startIndex < 0)
                startIndex = ~startIndex;

            if (h.NewCombo && startIndex > 0)
                hitObjects[startIndex - 1].LastInCombo = true;

            // make sure notes with same timing are inserted in the right order -echo
            while (startIndex < hitObjects.Count && hitObjects[startIndex].StartTime == h.StartTime)
                startIndex++;

            hitObjects.Insert(startIndex, h);
            lastAddedObject = h;

            if (spriteManager != null)
            {
                spriteManager.Add(h.SpriteCollection);
            }

            hitObjectsCount++;
        }

        /// <summary>
        /// Hits the specified h.
        /// </summary>
        /// <param name="h">The h.</param>
        /// <param name="drawValue">Whether to draw the hit sprite.  Also counts as an actual miss if set to true.</param>
        /// <returns></returns>
        internal virtual IncreaseScoreType Hit(HitObject h)
        {
            lastHitObject = h;

            IncreaseScoreType hitValue = h.Hit();

            int index = hitObjects.BinarySearch(h);
            currentHitObjectIndex = index < 0 ? ~index : index;

            if (hitValue == IncreaseScoreType.Ignore)
                return hitValue;

            bool endOfCombo = false;

            string specialAddition = string.Empty;

            string spriteName;

            if (Player.currentScore != null && ModManager.CheckActive(ActiveMods, Mods.Perfect))
            {
                IncreaseScoreType check = (hitValue & IncreaseScoreType.HitValuesOnly);
                switch (check)
                {
                    case IncreaseScoreType.Hit100:
                    case IncreaseScoreType.Hit100k:
                    case IncreaseScoreType.Hit100m:
                    case IncreaseScoreType.Hit50:
                    case IncreaseScoreType.Hit50m:
                        hitValue = IncreaseScoreType.Miss;
                        break;
                }
            }

            Color particleColour = Color.White;
            int scoreAmount = 0;

            if (hitValue < 0)
                spriteName = "hit0";
            else
            {
                //Note that this can be done with .ToString() on the enum members.
                //I choose to explicitly list them to allow obfuscation on this enum.
                //Whether its worth it or not I don't know, but I'd prefer not to expose this enum so easily.
                switch (hitValue & IncreaseScoreType.HitValuesOnly)
                {
                    case IncreaseScoreType.Hit50:
                        spriteName = "hit50";
                        scoreAmount = 50;
                        particleColour = new Color(255, 216, 95);
                        break;
                    case IncreaseScoreType.Hit100:
                        spriteName = "hit100";
                        scoreAmount = 100;
                        particleColour = new Color(129, 255, 95);
                        break;
                    case IncreaseScoreType.Hit100k:
                        spriteName = "hit100k";
                        scoreAmount = 100;
                        particleColour = new Color(129, 255, 95);
                        break;
                    case IncreaseScoreType.Hit300:
                        spriteName = "hit300";
                        scoreAmount = 300;
                        particleColour = new Color(69, 255, 255);
                        break;
                    case IncreaseScoreType.Hit300g:
                        spriteName = "hit300g";
                        scoreAmount = 300;
                        particleColour = new Color(69, 255, 255);
                        break;
                    case IncreaseScoreType.Hit300k:
                        spriteName = "hit300k";
                        scoreAmount = 300;
                        particleColour = new Color(69, 255, 255);
                        break;
                    default:
                        spriteName = string.Empty;
                        break;
                }
            }

            if ((hitValue & IncreaseScoreType.TaikoLargeHitBoth) > 0 && hitValue > IncreaseScoreType.Miss)
                specialAddition = "k";

            switch (hitValue)
            {
                case IncreaseScoreType.Hit100:
                    CurrentComboKatu++;
                    break;
                case IncreaseScoreType.Hit50:
                case IncreaseScoreType.Miss:
                    CurrentComboBad++;
                    break;
            }

            if (AllowComboBonuses && (index >= 0 && (index == hitObjectsCount - 1 ||
                                                     (index < hitObjectsCount - 1 &&
                                                      hitObjects[index + 1].NewCombo))))
            {
                endOfCombo = true;

                bool backwardsMiss = false;

                if (!(h is SpinnerOsu))
                {
                    for (int i = index; i > -1; i--)
                    {
                        if (!hitObjects[i].IsHit)
                        {
                            backwardsMiss = true;
                            break;
                        }

                        if (hitObjects[i].NewCombo)
                            break;
                    }
                }

                if (hitValue > 0)
                {
                    if (CurrentComboKatu == 0 && CurrentComboBad == 0 && !backwardsMiss)
                    {
                        specialAddition = "g";
                        hitValue |= IncreaseScoreType.GekiAddition;
                    }
                    else if (CurrentComboBad == 0 && !backwardsMiss)
                    {
                        specialAddition = "k";
                        hitValue |= IncreaseScoreType.KatuAddition;
                    }
                    else
                    {
                        hitValue |= IncreaseScoreType.MuAddition;
                    }
                }

                CurrentComboKatu = 0;
                CurrentComboBad = 0;
            }

            pTexture particle = null;

            if (!((Player.Relaxing || Player.Relaxing2) && hitValue < IncreaseScoreType.Ignore))
            {
                pTexture[] hitTexture = textureForHitType(spriteName + specialAddition);
                if (hitTexture != null) particle = SkinManager.Load("particle" + scoreAmount, hitTexture[0].Source);

                bool isBelow = false;

                float depth;
                if (Player.Mode == PlayModes.Taiko)
                    depth = 0.96f;
                else if (h is SpinnerOsu)
                    depth = SpriteManager.drawOrderFwdLowPrio(h.StartTime + 20);
                else if (particle == null && (h is HitCircleOsu || h is SliderOsu))
                    depth = SpriteManager.drawOrderFwdPrio(h.EndTime - 4);
                else
                {
                    isBelow = true;
                    depth = SpriteManager.drawOrderFwdLowPrio(h.EndTime);
                }

                //Draw the hit value
                pAnimation p =
                    new pAnimation(hitTexture,
                                Fields.Gamefield,
                                Origins.Centre,
                                Clocks.AudioOnce, h.EndPosition, depth, false, Color.White) { LoopType = LoopTypes.LoopOnce };

                bool allowTransformations = p.TextureCount == 1;

                if (particle != null && isBelow && hitValue > IncreaseScoreType.Ignore)
                {
                    pAnimation p2 = (pAnimation)p.Clone();
                    p2.Depth = 1;
                    p2.Additive = true;
                    p2.Scale = 0.9f;

                    p2.Transformations.Add(new Transformation(TransformationType.Scale, 0.6F, 1.1F, AudioEngine.Time, (int)(AudioEngine.Time + (HitFadeIn * 0.8))));
                    p2.Transformations.Add(new Transformation(TransformationType.Scale, 1.1F, 0.9F, AudioEngine.Time + HitFadeIn, (int)(AudioEngine.Time + (HitFadeIn * 1.2))));
                    p2.Transformations.Add(new Transformation(TransformationType.Scale, 0.9F, 1.05F, AudioEngine.Time, AudioEngine.Time + PostEmpt + HitFadeOut));

                    p2.Transformations.Add(new Transformation(TransformationType.Fade, 0, 0.5f, AudioEngine.Time - 16, AudioEngine.Time + 40, EasingTypes.Out));
                    p2.Transformations.Add(new Transformation(TransformationType.Fade, 0.5f, 0, AudioEngine.Time + 40, AudioEngine.Time + 340));


                    spriteManager.Add(p2);
                }

                if (hitValue > IncreaseScoreType.Ignore && ConfigManager.sHitLighting && useHitLighting)
                {
                    pSprite plight =
                        new pSprite(SkinManager.Load(@"lighting"),
                                    Fields.Gamefield,
                                    Origins.Centre,
                                    Clocks.AudioOnce, h.EndPosition, 0, false, h.Colour);

                    if (!SkinManager.UseNewLayout)
                    {
                        int time2 = Player.KiaiActive && !ConfigManager.sMyPcSucks ? 300 : 600;
                        float scale2 = Player.KiaiActive && !ConfigManager.sMyPcSucks ? 2 : 1.85F;

                        plight.Transformations.Add(new Transformation(TransformationType.Scale, 0.8F, scale2, AudioEngine.Time, AudioEngine.Time + time2, EasingTypes.Out));
                        plight.Transformations.Add(new Transformation(TransformationType.Scale, scale2, 2.2F, AudioEngine.Time + time2, AudioEngine.Time + 1400));
                        plight.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, AudioEngine.Time, AudioEngine.Time + 1400));
                        plight.Additive = true;
                        spriteManager.Add(plight);

                    }
                    else
                    {
                        plight.Transformations.Add(new Transformation(TransformationType.Scale, 0.8F, 1.2f, AudioEngine.Time, AudioEngine.Time + 600, EasingTypes.Out));
                        plight.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, AudioEngine.Time, AudioEngine.Time + 200));
                        plight.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, AudioEngine.Time + 400, AudioEngine.Time + 1400));
                        plight.Additive = true;
                        spriteManager.Add(plight);
                    }
                }

                if (particle != null && hitValue >= 0)
                {
                    IEnumerable<pSprite> particles = getParticles(particle, 150);
                    foreach (pSprite p2 in particles)
                    {
                        p2.Position = h.EndPosition;

                        int time2 = GameBase.random.Next(500, 1200);

                        double angle = GameBase.random.NextDouble() * Math.PI * 2;
                        p2.Transformations[0].StartVector = h.EndPosition;
                        p2.Transformations[0].EndVector = h.EndPosition + new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle)) * GameBase.random.Next(0, 35);
                        p2.Transformations[0].Time1 = AudioEngine.Time - 100;
                        p2.Transformations[0].Time2 = AudioEngine.Time + time2;

                        p2.Transformations[1].StartFloat = InputManager.s_Cursor.Alpha;
                        p2.Transformations[1].Time1 = AudioEngine.Time;
                        p2.Transformations[1].Time2 = AudioEngine.Time + time2;

                        spriteManager.Add(p2);
                    }
                }

                bool isSpinner = h.IsType(HitObjectType.Spinner);

                if (hitValue == IncreaseScoreType.Miss)
                {
                    p.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, AudioEngine.Time, AudioEngine.Time + HitFadeIn));

                    if (allowTransformations)
                    {
                        if (Player.currentScore != null && GameBase.User.Status != bStatus.Multiplaying &&
                            ModManager.CheckActive(ActiveMods, Mods.SuddenDeath))
                            p.Transformations.Add(new Transformation(TransformationType.Scale, 2, 6, AudioEngine.Time, AudioEngine.Time + 600));
                        else
                            p.Transformations.Add(new Transformation(TransformationType.Scale, 2, 1, AudioEngine.Time, AudioEngine.Time + HitFadeIn));

                        float rotation = (float)((GameBase.random.NextDouble() - 0.5) * 0.3);


                        if (SkinManager.UseNewLayout && Player.Mode == PlayModes.Osu)
                            p.Transformations.Add(new Transformation(TransformationType.Movement, h.EndPosition + new Vector2(0, -5), h.EndPosition + new Vector2(0, 40), AudioEngine.Time, AudioEngine.Time + PostEmpt + HitFadeOut, EasingTypes.In));

                        p.Transformations.Add(new Transformation(TransformationType.Rotation, 0, rotation, AudioEngine.Time, AudioEngine.Time + HitFadeIn));
                        p.Transformations.Add(new Transformation(TransformationType.Rotation, rotation, rotation * 2, AudioEngine.Time + HitFadeIn, AudioEngine.Time + PostEmpt + HitFadeOut, EasingTypes.In));
                    }

                    p.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, AudioEngine.Time + PostEmpt, AudioEngine.Time + PostEmpt + HitFadeOut));
                }
                else if (hitValue > IncreaseScoreType.Ignore)
                {
                    if (isSpinner && !SkinManager.UseNewLayout)
                    {
                        pSprite p2 = new pSprite(SkinManager.Load(@"spinner-osu"),
                            Fields.TopLeft,
                            Origins.Centre,
                            Clocks.AudioOnce, new Vector2(320, 180), Player.Mode == PlayModes.Osu ? 0 : depth, false, Color.White);

                        HitTransformationsSuccessSpinner(p2);
                        spriteManager.Add(p2);
                    }

                    HitTransformationsSuccess(p, allowTransformations, particle != null);
                }

                hitSpritePostProcessing(p);

                spriteManager.Add(p);
            }

            if (endOfCombo && specialAddition.Length == 0)
                specialAddition = "b"; //this should probably be fixed... see ForcedHit for usage

            if (ForcedHit != null)
                //(hitValue <= 0 ? (drawValue ? IncreaseScoreType.Miss : IncreaseScoreType.MissHpOnly) : hitValue)
                ForcedHit(hitValue, specialAddition, h);

            if (eventManager != null)
                eventManager.InvokeOnHitObjectHit(h);

            return hitValue;
        }

        const int particles_cache_size = 1000;
        List<pSprite> particles = new List<pSprite>();
        int particlesLastUsed;

        private IEnumerable<pSprite> getParticles(pTexture texture, int count)
        {
            List<pSprite> sprites = new List<pSprite>(count);
            int searchSize = Math.Min(particles.Count, particles_cache_size);

            int found = 0;
            for (int i = 0; i < searchSize && found < count; i++)
            {
                particlesLastUsed = (particlesLastUsed + 1) % searchSize;

                pSprite n = particles[particlesLastUsed];
                if (n.Transformations[1].Time2 < AudioEngine.Time)
                {
                    n.Texture = texture;
                    n.IsVisible = true;
                    sprites.Add(n);
                    found++;
                }
            }

            while (found++ < count && particles.Count < particles_cache_size)
            {
                pSprite p = new pSprite(texture, Fields.Gamefield, Origins.Centre, Clocks.AudioOnce, Vector2.Zero, 0, false, Color.White) { Additive = true };
                p.Transformations.Add(new Transformation(Vector2.Zero, Vector2.Zero, 0, 0));
                p.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, 0, 0));
                particles.Add(p);
                sprites.Add(p);
            }

            return sprites;
        }

        protected virtual void hitSpritePostProcessing(pSprite p)
        {
        }

        protected virtual pTexture[] textureForHitType(string p)
        {
            return SkinManager.LoadAll(p);
        }

        protected virtual void HitTransformationsSuccessSpinner(pSprite p)
        {
            p.Transformations.Add(
                new Transformation(TransformationType.Fade, 0, 1, AudioEngine.Time,
                                   AudioEngine.Time + HitFadeIn));
            p.Transformations.Add(
                new Transformation(TransformationType.Fade, 1, 0,
                                   AudioEngine.Time + PostEmpt, AudioEngine.Time + PostEmpt + HitFadeOut));

            spriteManager.Add(p);
        }

        protected virtual void HitTransformationsSuccess(pSprite p, bool allowAnimation = true, bool particleAnimations = false)
        {
            p.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, AudioEngine.Time, AudioEngine.Time + (particleAnimations ? 80 : HitFadeIn)));

            if (allowAnimation)
            {
                if (!particleAnimations)
                {
                    p.Transformations.Add(
                        new Transformation(TransformationType.Scale, 0.6F, 1.1F, AudioEngine.Time,
                                           (int)(AudioEngine.Time + (HitFadeIn * 0.8))));

                    p.Transformations.Add(
                        new Transformation(TransformationType.Scale, 1.1F, 0.9F, AudioEngine.Time + HitFadeIn,
                                           (int)(AudioEngine.Time + (HitFadeIn * 1.2))));
                    p.Transformations.Add(
                        new Transformation(TransformationType.Scale, 0.9F, 1F, AudioEngine.Time + HitFadeIn,
                                           (int)(AudioEngine.Time + (HitFadeIn * 1.4))));
                }
                else
                {
                    p.Transformations.Add(new Transformation(TransformationType.Scale, 0.9F, 1.05F, AudioEngine.Time, AudioEngine.Time + PostEmpt + HitFadeOut));
                }
            }

            p.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, AudioEngine.Time + PostEmpt, AudioEngine.Time + PostEmpt + HitFadeOut));
        }

        internal bool IsComboEnd(HitCircleOsu h)
        {
            int index = hitObjects.IndexOf(h);
            return (index == hitObjects.Count - 1 || hitObjects[index + 1].IsType(HitObjectType.NormalNewCombo));
        }

        internal void Remove(HitObject[] list)
        {
            foreach (HitObject h in list)
            {
                Remove(h, false);
                h.Dispose();
            }
            Sort(false);
        }

        internal void Remove(HitObject h, bool runSort)
        {
            spriteManager.RemoveRange(h.SpriteCollection);

            if (hitObjects.Remove(h))
                hitObjectsCount--;
            hitObjectsMinimal.Remove(h);

            if (runSort)
                Sort(false);

            h.Dispose();
        }

        internal void Sort(bool fullSort = false)
        {
            hitObjectsCount = hitObjects.Count;

            if (fullSort)
            {
                // replaced with stable sort so notes with same timing stay in the same order -echo
                HitObject[] newObjects = ListHelper.StableSort(hitObjects);
                hitObjects.Clear();
                hitObjects.AddRange(newObjects);
            }

            int combo = 0;
            int comboNumber = 0;
            bool forceNew = false;
            int lastBreakPoint = -1;
            int colourCount = ComboColours.Count;

            int breakCount = eventManager.eventBreaks.Count;

            for (int i = 0; i < hitObjectsCount; i++)
            {
                HitObject currHitObject = hitObjects[i];

                //todo: can improve efficiency.
                while (lastBreakPoint + 1 < breakCount &&
                       eventManager.eventBreaks[lastBreakPoint + 1].EndTime < currHitObject.StartTime)
                {
                    lastBreakPoint++;
                    currHitObject.NewCombo = true;
                }

                int offset = !Beatmap.DisableSkin && !ConfigManager.sIgnoreBeatmapSkins ? currHitObject.ComboOffset : 0;

                if (currHitObject.IsType(HitObjectType.Spinner))
                {
                    if (Beatmap.BeatmapVersion <= 8)
                        forceNew = true;
                    else if (currHitObject.NewCombo)
                    {
                        // make colourhax possible when the spinner has a new combo but the object after does not:
                        // the player may set colourhax on the spinner itself in this case and get carried over.
                        combo += offset;
                        forceNew = true;
                    }
                }
                else if (forceNew || (currHitObject.Type & HitObjectType.NewCombo) > 0 || //Keep bit flag intact here as it is necessary for CTB to alternate fruit color while still keeping track of combo.
                         (i == 0 && !(currHitObject is HitCircleFruits)))
                {
                    comboNumber = 1;
                    currHitObject.ComboNumber = comboNumber;
                    combo += offset + 1;
                    forceNew = false;
                }
                else
                    currHitObject.ComboNumber = ++comboNumber;

                if (colourCount > 0 && UseComboColours)
                {
                    currHitObject.SetColour(ComboColours[combo % colourCount]);
                    currHitObject.ComboColourIndex = combo % colourCount;
                }
            }

            if (hitObjectsCount > 0)
                hitObjects[hitObjectsCount - 1].LastInCombo = true;
        }

        /// <summary>
        /// Finds the hitobject index of the first object after a break.
        /// </summary>
        internal int BreakObjectAfter(EventBreak b)
        {
            int breakMiddle = (b.EndTime - b.StartTime) / 2 + b.StartTime;

            //We first find the hitobject closest to the middle of the break (will be less than if anything).
            int index = hitObjects.BinarySearch(new HitObjectDummy(this, breakMiddle));

            if (index < 0) index = ~index;

            return index;
        }

        internal bool IsFirstObjectAfterBreak(HitObject obj, bool IncludeFirstObject)
        {
            // todo: optimize the shit out of this. (2 searches here)
            int index = hitObjects.FindIndex(o => (o == obj));

            if (index <= 0) return IncludeFirstObject;

            int end = hitObjects[index].StartTime;
            int start = hitObjects[index - 1].EndTime;

            EventBreak b = eventManager.eventBreaks.Find(q => ((q.StartTime > start) && (q.EndTime < end)));

            return (b != null);
        }

        /// <summary>
        /// Finds the hitobject index of the last object before a break.
        /// </summary>
        internal int BreakObjectBefore(EventBreak b)
        {
            return BreakObjectAfter(b) - 1;
        }

        /// <summary>
        /// Forces a new combo on the first object after a series of spinners if none of the spinners contain new combos.
        /// </summary>
        /// <param name="index">hitObjectManager.hitObjects index</param>
        internal void SpinnerForceNewCombo(int index)
        {
            if (!(hitObjects[index] is Spinner))
            {
                if (index == 0) return;
                index--;
                if (!(hitObjects[index] is Spinner)) return;
            }

            int x;
            for (x = index; x > 0; x--) // start searching at the first spinner
            {
                if (!(hitObjects[x] is Spinner))
                {
                    x++;
                    break;
                }
            }

            while (x < hitObjectsCount)
            {
                if (!(hitObjects[x] is Spinner))
                {
                    hitObjects[x].NewCombo = true;
                    return;
                }
                else if (hitObjects[x].NewCombo) return;
                x++;
            }
        }

        /// <summary>
        /// Performs the SpinnerForceNewCombo check for the entire map
        /// </summary>
        internal void SpinnerForceNewComboAll()
        {
            bool inSpinnerGroup = false;
            bool spinnerGroupCombo = false;
            for (int x = 0; x < hitObjects.Count; x++)
            {
                HitObject o = hitObjects[x];
                if (!o.IsType(HitObjectType.Spinner))
                {
                    if (inSpinnerGroup && !spinnerGroupCombo)
                    {
                        o.NewCombo = true;
                    }
                    inSpinnerGroup = false;
                    spinnerGroupCombo = false;
                }
                else
                {
                    spinnerGroupCombo |= o.NewCombo;
                }
            }
        }

        internal void Update()
        {
            UpdateBasic();

            UpdateHitObjects(); //this really needs to be fixed
        }

        internal virtual void UpdateBasic()
        {
            Editor edit = Editor.Instance;

            hitObjectsCount = hitObjects.Count;

            int minimalLeft = AudioEngine.Time - (edit != null && !ConfigManager.sEditorHitAnimations ? ForceFadeOut : PreEmpt);
            int minimalRight = AudioEngine.Time + PreEmpt;

            if (edit != null)
            {
                minimalLeft = Math.Min(minimalLeft, edit.TimelineLeftBound);
                minimalRight = Math.Max(minimalRight, edit.TimelineRightBound);
            }

            int startIndex = hitObjects.BinarySearch(new HitObjectDummy(this, minimalLeft));
            if (startIndex < 0)
                startIndex = ~startIndex;

            int endIndex;
            for (endIndex = startIndex; endIndex < hitObjectsCount - 1; endIndex++)
                if (hitObjects[endIndex].StartTime > minimalRight)
                    break;

            if (startIndex == hitObjectsCount)
            {
                hitObjectsMinimal.Clear();

                if (edit != null)
                    spriteManager.SpriteList.Clear();
            }
            else
            {
                hitObjectsMinimal = hitObjects.GetRange(startIndex, 1 + endIndex - startIndex);

                if (edit != null)
                {
                    spriteManager.SpriteList.Clear();

                    hitObjectsMinimal.ForEach(h => h.SpriteCollection.ForEach(spriteManager.Add));
                    
                    if (ConfigManager.sEditorFollowPoints)
                        AddFollowPoints(startIndex, endIndex);

                    //Make sure we draw the selection circles for hitcircles not in the minimal range.
                    if (Editor.Instance.Compose.selectedObjects.Count > 0)
                        foreach (HitObject h in Editor.Instance.Compose.selectedObjects)
                        {
                            HitCircleOsu hc;
                            if (!hitObjectsMinimal.Contains(h))
                            {
                                if (h.IsType(HitObjectType.Slider))
                                {
                                    SliderOsu s = h as SliderOsu;

                                    hc = s.sliderStartCircle;
                                    spriteManager.Add(hc.SpriteSelectionCircle);
                                    if (s.sliderAllCircles.Count > 1)
                                    {
                                        hc = s.sliderAllCircles[1];
                                        spriteManager.Add(hc.SpriteSelectionCircle);
                                    }
                                }
                                else
                                {
                                    hc = h as HitCircleOsu;

                                    if (hc != null)
                                    {
                                        spriteManager.Add(hc.SpriteSelectionCircle);
                                    }
                                }
                            }
                        }
                }
            }
        }

        internal virtual void UpdateHitObjects()
        {
            foreach (HitObject h in hitObjectsMinimal)
            {
                h.Update();
                UpdateHitObject(h);
            }
        }

        protected virtual void UpdateHitObject(HitObject currHitObject)
        {
            if (InputManager.ScorableFrame || !InputManager.ReplayMode)
            {
                //Handle audio events that can't be associated with user input.
                if (currHitObject.IsType(HitObjectType.Slider))
                {
                    SliderOsu slider = (SliderOsu)currHitObject;
                    //run operations on sliders which are calculated before drawing them.

                    if (GameBase.Mode == OsuModes.Play)
                    {
                        if (slider.StartTime + HitWindow50 < AudioEngine.Time && !slider.StartIsHit && GameBase.Mode == OsuModes.Play)
                            Hit(slider.sliderStartCircle);

                        if (slider.HittableEndTime <= AudioEngine.Time && !slider.IsHit)
                            Hit(slider);
                    }
                }
                else if (currHitObject.IsType(HitObjectType.Spinner))
                {
                    if (currHitObject.HittableEndTime < AudioEngine.Time && !currHitObject.IsHit && GameBase.Mode == OsuModes.Play)
                        Hit(currHitObject);
                }
                else
                {
                    //The case where the circle is not hit in time - we want to hit it to give it a 0 value hit
                    if (currHitObject.EndTime + HitWindow50 < AudioEngine.Time && !currHitObject.IsHit && GameBase.Mode == OsuModes.Play)
                        Hit(currHitObject);
                }
            }
        }

        /// <summary>
        /// Draws the targets.
        /// </summary>
        internal static void DrawTargets()
        {
            lock (Instances)
                foreach (HitObjectManager h in Instances)
                {
                    if (GameBase.Mode == OsuModes.Edit) //Only update in edit mode.
                    {
                        for (int i = 0; i < h.hitObjectsCount; i++)
                            if (h.hitObjects[i].Drawable)
                                h.hitObjects[i].Draw();
                    }
                    else if (Player.Loaded || GameBase.Mode != OsuModes.Play)
                    {
                        foreach (HitObject ho in h.hitObjectsMinimal)
                            if (ho.Drawable) ho.Draw();
                    }
                }
        }

        internal static void RecycleTargets()
        {
            lock (Instances)
                foreach (HitObjectManager h in Instances)
                {
                    foreach (HitObject ho in h.hitObjectsMinimal)
                    {
                        SliderOsu so = ho as SliderOsu;
                        if (so != null)
                            so.RemoveBody();
                    }
                }
        }

        internal void Draw()
        {
            spriteManager.Draw();
        }

        internal void LoadEvents(bool incrementalLoading)
        {
            eventManager.LoadDynamic(true, incrementalLoading);

            if (!eventManager.spriteManagerBG.ForwardPlayOptimisations)
            {
                eventManager.spriteManagerBG.SpriteList.ForEach(p => p.MakeAllLoopsStatic());
                eventManager.spriteManagerFG.SpriteList.ForEach(p => p.MakeAllLoopsStatic());
            }
        }

        protected virtual void PostProcessing()
        {
            if (GameBase.Mode == OsuModes.Edit) return;

            hitObjectsCount = hitObjects.Count;

            ApplyStacking();
            AddFollowPoints();
        }

        private void ApplyStacking()
        {
            Vector2 stackVector = new Vector2(StackOffset, StackOffset);

            const int STACK_LENIENCE = 3;

            if (Beatmap.BeatmapVersion > 5)
            {
                //New (fixed) stacking algorithm implemented in osu v6.

                foreach (HitObject h in hitObjects)
                {
                    SliderOsu s = h as SliderOsu;
                    if (s != null)
                        s.UpdateCalculations();
                }

                //Reverse pass for stack calculation.
                for (int i = hitObjectsCount - 1; i > 0; i--)
                {
                    int n = i;
                    /* We should check every note which has not yet got a stack.
                     * Consider the case we have two interwound stacks and this will make sense.
                     *
                     * o <-1      o <-2
                     *  o <-3      o <-4
                     *
                     * We first process starting from 4 and handle 2,
                     * then we come backwards on the i loop iteration until we reach 3 and handle 1.
                     * 2 and 1 will be ignored in the i loop because they already have a stack value.
                     */

                    HitObject objectI = hitObjects[i];

                    if (objectI.StackCount != 0 || objectI is Spinner) continue;

                    /* If this object is a hitcircle, then we enter this "special" case.
                     * It either ends with a stack of hitcircles only, or a stack of hitcircles that are underneath a slider.
                     * Any other case is handled by the "is Slider" code below this.
                     */
                    if (objectI is HitCircle)
                    {
                        while (--n >= 0)
                        {
                            HitObject objectN = hitObjects[n];

                            if (objectN is Spinner) continue;

                            if (objectI.StartTime - (PreEmpt * Beatmap.StackLeniency) > objectN.EndTime)
                                //We are no longer within stacking range of the previous object.
                                break;

                            /* This is a special case where hticircles are moved DOWN and RIGHT (negative stacking) if they are under the *last* slider in a stacked pattern.
                             *    o==o <- slider is at original location
                             *        o <- hitCircle has stack of -1
                             *         o <- hitCircle has stack of -2
                             */
                            if (objectN is Slider && Vector2.Distance(objectN.EndPosition, objectI.Position) < STACK_LENIENCE)
                            {
                                int offset = objectI.StackCount - objectN.StackCount + 1;
                                for (int j = n + 1; j <= i; j++)
                                {
                                    //For each object which was declared under this slider, we will offset it to appear *below* the slider end (rather than above).
                                    if (Vector2.Distance(objectN.EndPosition, hitObjects[j].Position) < STACK_LENIENCE)
                                        hitObjects[j].StackCount -= offset;
                                }

                                //We have hit a slider.  We should restart calculation using this as the new base.
                                //Breaking here will mean that the slider still has StackCount of 0, so will be handled in the i-outer-loop.
                                break;
                            }

                            if (Vector2.Distance(objectN.Position, objectI.Position) < STACK_LENIENCE)
                            {
                                //Keep processing as if there are no sliders.  If we come across a slider, this gets cancelled out.
                                //NOTE: Sliders with start positions stacking are a special case that is also handled here.

                                objectN.StackCount = objectI.StackCount + 1;
                                objectI = objectN;
                            }
                        }
                    }
                    else if (objectI is Slider)
                    {
                        /* We have hit the first slider in a possible stack.
                         * From this point on, we ALWAYS stack positive regardless.
                         */
                        while (--n >= 0)
                        {
                            HitObject objectN = hitObjects[n];

                            if (objectN is Spinner) continue;

                            if (objectI.StartTime - (PreEmpt * Beatmap.StackLeniency) > objectN.StartTime)
                                //We are no longer within stacking range of the previous object.
                                break;

                            if (Vector2.Distance(objectN.EndPosition, objectI.Position) < STACK_LENIENCE)
                            {
                                objectN.StackCount = objectI.StackCount + 1;
                                objectI = objectN;
                            }
                        }
                    }
                }

                for (int i = 0; i < hitObjectsCount; i++)
                {
                    HitObject currHitObject = hitObjects[i];

                    if (currHitObject.StackCount != 0)
                        currHitObject.ModifyPosition(currHitObject.Position - currHitObject.StackCount * stackVector);
                }
            }
            else
            {
                //Old (broken) stacking for backwards compatibility.
                for (int i = 0; i < hitObjectsCount; i++)
                {
                    HitObject currHitObject = hitObjects[i];

                    if (currHitObject is SliderOsu)
                        ((SliderOsu)currHitObject).UpdateCalculations();

                    //Handle stacking of HitCircles (when more than one is on the same position)
                    int startTime = currHitObject.EndTime;

                    if (currHitObject.StackCount == 0 || currHitObject.IsType(HitObjectType.Slider))
                    {
                        int sliderStack = 0;
                        for (int j = i + 1; j < hitObjectsCount; j++)
                        {
                            if (hitObjects[j].StartTime - (PreEmpt * Beatmap.StackLeniency) <= startTime)
                            {
                                if (Vector2.Distance(hitObjects[j].Position, currHitObject.Position) < 3)
                                {
                                    currHitObject.StackCount++;
                                    startTime = hitObjects[j].EndTime;
                                }
                                //Case for sliders - bump notes down and right, rather than up and left.
                                else if (Vector2.Distance(hitObjects[j].Position, currHitObject.Position2) < 3)
                                {
                                    sliderStack++;
                                    hitObjects[j].StackCount -= sliderStack;
                                    startTime = hitObjects[j].EndTime;
                                }
                            }
                            else
                                break;
                        }
                    }

                    if (currHitObject.StackCount != 0)
                        currHitObject.ModifyPosition(currHitObject.Position -
                                                     currHitObject.StackCount * stackVector);
                }
            }
        }

        internal void AddFollowPoints(int startIndex = 0, int endIndex = -1)
        {
            if (Beatmap.PlayMode != PlayModes.Osu)
                return;

            if (endIndex == -1)
                endIndex = hitObjectsCount - 1;

            pTexture[] fptextures = SkinManager.LoadAll(@"followpoint");

            int followPointIndex = 0;
            for (int i = startIndex + 1; i <= endIndex; i++)
            {
                HitObject currHitObject = hitObjects[i];

                //Draw connection lines
                if (!currHitObject.NewCombo &&
                    !hitObjects[i - 1].IsType(HitObjectType.Spinner) &&
                    !hitObjects[i].IsType(HitObjectType.Spinner))
                {
                    Vector2 pos1 = hitObjects[i - 1].EndPosition;
                    int time1 = hitObjects[i - 1].EndTime;
                    Vector2 pos2 = currHitObject.Position;
                    int time2 = currHitObject.StartTime;

                    int distance = (int)Vector2.Distance(pos1, pos2);
                    Vector2 distanceVector = pos2 - pos1;
                    int length = time2 - time1;

                    float angle = (float)Math.Atan2(pos2.Y - pos1.Y, pos2.X - pos1.X);

                    for (int j = (int)(FollowLineDistance * 1.5); j < distance - FollowLineDistance; j += FollowLineDistance)
                    {
                        float fraction = ((float)j / distance);
                        Vector2 posStart = pos1 + (fraction - 0.1f) * distanceVector;
                        Vector2 pos = pos1 + fraction * distanceVector;
                        int fadein = (int)(time1 + fraction * length) - FollowLinePreEmpt;
                        int fadeout = (int)(time1 + fraction * length);

                        pAnimation dot;
                        if (followPointIndex < followPoints.Count)
                        {
                            dot = followPoints[followPointIndex];
                            dot.Position = pos;

                            foreach (Transformation t in dot.Transformations)
                            {
                                if (t.Type == TransformationType.Fade && t.EndFloat == 0)
                                {
                                    t.Time1 = fadeout;
                                    t.Time2 = fadeout + FadeIn;
                                }
                                else
                                {
                                    t.Time1 = fadein;
                                    t.Time2 = fadein + FadeIn;
                                }

                                if (t.Type == TransformationType.Movement)
                                {
                                    t.StartVector = posStart;
                                    t.EndVector = pos;
                                }
                            }

                            dot.ResetAnimation();
                        }
                        else
                        {
                            dot = new pAnimation(fptextures, Fields.Gamefield, Origins.Centre, Clocks.Audio, pos, 0, false, Color.White, null);
                            dot.SetFramerateFromSkin();

                            dot.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, fadein, fadein + FadeIn));
                            if (SkinManager.IsDefault && GameBase.NewGraphicsAvailable)
                            {
                                dot.Transformations.Add(new Transformation(TransformationType.Scale, 1.5f, 1, fadein, fadein + FadeIn, EasingTypes.Out));
                                dot.Transformations.Add(new Transformation(TransformationType.Movement, posStart, pos, fadein, fadein + FadeIn, EasingTypes.Out));
                            }
                            dot.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, fadeout, fadeout + FadeIn));
                            
                            if (GameBase.Mode == OsuModes.Edit)
                                followPoints.Add(dot);
                        }

                        dot.Rotation = angle;
                        spriteManager.Add(dot);

                        followPointIndex++;
                    }
                }
            }
        }

        protected virtual void AddCircle(HitCircle h)
        {
            Add(h, false);
        }

        protected virtual void AddSpinner(Spinner s)
        {
            Add(s, false);
        }

        protected virtual void AddSlider(Slider s)
        {
            Add(s, false);
        }

        #endregion

        public bool ShowOverlayAboveNumber
        {
            get
            {
                //First check beatmap level overrides.
                switch (Beatmap.OverlayPosition)
                {
                    case OverlayPosition.Above:
                        return true;
                    case OverlayPosition.Below:
                        return false;
                }

                //If none is requested, then use skin setting.
                return SkinManager.Current.OverlayAboveNumber;
            }
        }

        protected virtual bool useHitLighting { get { return true; } }
    }


    internal enum FileSection
    {
        Unknown = 0,
        General = 1 << 0,
        Colours = 1 << 1,
        Editor = 1 << 2,
        Metadata = 1 << 3,
        TimingPoints = 1 << 4,
        Events = 1 << 5,
        HitObjects = 1 << 6,
        Difficulty = 1 << 7,
        Variables = 1 << 8,
        All = General | Colours | Editor | Metadata | TimingPoints | Events | HitObjects | Difficulty | Variables,
    } ;
}