﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.Configuration;
using osu.GameModes.Menus;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu.Online;
using osu_common;
using osu_common.Helpers;

namespace osu.GameplayElements
{
    internal class TransitionManager : DrawableGameComponent
    {
        SpriteManager spriteManagerBack = new SpriteManager() { Masking = true };
        SpriteManager spriteManagerBackWide = new SpriteManager(true) { Masking = true };
        SpriteManager spriteManagerFront = new SpriteManager(true);

        internal bool AllowParallax = true;

        internal TriangleVisualisation TrianglesBack = new TriangleVisualisation();
        internal TriangleVisualisation TrianglesFront = new TriangleVisualisation()
        {
            UseVelocityBasedFade = false,
            UseMenuIdleFade = false,
            LimitWhenRunningSlow = false,
            UseAudioVelocity = false
        };

        internal pTexture BackgroundTexture;
        internal pSprite BackgroundSprite;

        internal float BackgroundAlpha = 1;

        private AlignmentMode currentAlignmentMode = AlignmentMode.Default;
        private BackgroundAdjustmentDelegate currentBackgroundAdjustment;

        internal delegate void BackgroundAdjustmentDelegate(pSprite s);

        /// <summary>
        /// Some modes may request background to be hidden using ClearBackground.
        /// This stored the value of that to ensure it doesn't appear when unwanted.
        /// </summary>
        bool showBackground;

        /// <summary>
        /// UpdateBackground may be called multiple times, causing weird threading conditions.
        /// This variable is used to make sure only the last run Update will be applied.
        /// </summary>
        int backgroundUpdateCount;

        internal event VoidDelegate OnBackgroundChanged;
        Permissions loadedBackgroundMode = Permissions.None;

        public TransitionManager()
            : base(GameBase.Game)
        {
            Initialize();

            GameBase.OnModeChange += GameBase_OnModeChange;
            GameBase.OnResolutionChange += GameBase_OnResolutionChange;
            BanchoClient.OnPermissionChange += BanchoClient_OnPermissionChange;
        }

        void BanchoClient_OnPermissionChange()
        {
            if (BackgroundTexture != null && BackgroundTexture.Source != SkinSource.Beatmap)
                UpdateBackground();
        }

        internal int BGPixelsDrawn
        {
            get { return BackgroundSprite != null ? BackgroundSprite.PixelsDrawn : 0; }
        }

        void GameBase_OnResolutionChange()
        {
            updateBackgroundAlignment(BackgroundSprite, currentAlignmentMode, currentBackgroundAdjustment);
        }

        void GameBase_OnModeChange()
        {
            UpdateBackground();

            BackgroundAlpha = 0.6f;
            AllowParallax = true;
            if (BackgroundSprite != null)
                BackgroundSprite.Position = Vector2.Zero;
        }

        protected override void Dispose(bool disposing)
        {
            spriteManagerBack.Dispose();
            spriteManagerBackWide.Dispose();
            spriteManagerFront.Dispose();
            TrianglesBack.Dispose();
            TrianglesFront.Dispose();

            base.Dispose(disposing);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Draw()
        {
            spriteManagerBack.Draw();
            spriteManagerBackWide.Draw();

#if !TRIANGLES
            return;
#endif

            if (ConfigManager.sMenuTriangles)
                TrianglesBack.Draw();
            base.Draw();
        }

        public void DrawHigh()
        {
#if !TRIANGLES
            return;
#endif

            spriteManagerFront.Draw();
            TrianglesFront.Draw();
        }

        public override void Update()
        {
            float alphaDiff = (1 - BackgroundAlpha) - spriteManagerBackWide.Blackness;
            if (Math.Abs(alphaDiff) > 0.001f)
                spriteManagerBackWide.Blackness += alphaDiff * (float)Math.Min(1, GameBase.FrameRatio) * (Player.Playing ? 0.1f : 0.04f);
            else
                spriteManagerBackWide.Blackness = 1 - BackgroundAlpha;

            if (GameBase.Time < Menu.IntroLength)
                return;

            if (AllowParallax && ConfigManager.sMenuParallax)
            {
                float distanceX = InputManager.CursorPosition.X - (GameBase.WindowWidth / 2);
                float distanceY = InputManager.CursorPosition.Y - (GameBase.WindowHeight / 2);
                spriteManagerBackWide.ViewOffset = new Vector2(distanceX, distanceY) / GameBase.WindowRatio / 180;
                if (BackgroundSprite != null) BackgroundSprite.VectorScale = new Vector2(1.01f, 1.01f);
            }
            else
            {
                spriteManagerBackWide.ViewOffset = Vector2.Zero;
                if (BackgroundSprite != null) BackgroundSprite.VectorScale = Vector2.One;
            }

#if TRIANGLES
            if (GameBase.FadeState != FadeStates.WaitingLoad)
                TrianglesFront.VelocityCurrent = 100;

            TrianglesFront.ProduceTriangles = GameBase.GameQueuedState != OsuModes.Exit && (GameBase.FadeState == FadeStates.FadeOut || GameBase.fadeLevel > 80);


            if (GameBase.GameQueuedState == OsuModes.Exit)
            {
                TrianglesBack.ProduceTriangles = false;
                TrianglesBack.VelocityCurrent = (100 - AudioEngine.volumeMusicFade) * 0.5f;
            }
            else
            {
                switch (GameBase.Mode)
                {
                    case OsuModes.Menu:
                        TrianglesBack.ProduceTriangles = true;
                        break;
                    case OsuModes.SelectPlay:
                    case OsuModes.SelectEdit:
                    case OsuModes.SelectMulti:
                        TrianglesBack.ProduceTriangles = false;
                        TrianglesBack.VelocityCurrent = 100;
                        break;
                }

            }

            TrianglesBack.UseAudioVelocity = GameBase.Mode == OsuModes.Menu;

            TrianglesBack.Update();
            TrianglesFront.Update();
#endif
            spriteManagerBack.Blackness = spriteManagerBackWide.Blackness;
            spriteManagerBack.ViewOffset = spriteManagerBackWide.ViewOffset;

            base.Update();
        }

#if CHRISTMAS
        int loadedBackgroundIndex = -1;
#endif

        internal void ClearBackground(bool triggerEvent = true)
        {
            showBackground = false;

            if (BackgroundSprite == null)
                return;

            BackgroundSprite.AlwaysDraw = false;
            BackgroundSprite.FadeOut(250);
            BackgroundSprite = null;

            if (triggerEvent && OnBackgroundChanged != null) OnBackgroundChanged();
        }

        internal void UpdateBackground(AlignmentMode alignmentMode = AlignmentMode.Default, BackgroundAdjustmentDelegate backgroundAdjustment = null)
        {
            BackgroundTexture = null;
            showBackground = true;
            backgroundUpdateCount++;

            updateBackground(alignmentMode, backgroundAdjustment);
        }

        private void updateBackground(AlignmentMode alignmentMode, BackgroundAdjustmentDelegate backgroundAdjustment, pTexture loadedTexture = null)
        {
            if (GameBase.Time < Menu.IntroLength || !showBackground)
                return;

            if (BackgroundSprite != null)
                BackgroundSprite.FadeTo(1, 100);

            if (loadedTexture == null || loadedTexture.IsDisposed)
            {
                //we need to load a new texture.
                int updateCountAtStart = backgroundUpdateCount;

                GameBase.RunGraphicsThread(delegate
                {
                    if (updateCountAtStart != backgroundUpdateCount) return;

                    pTexture loaded = null;

                    //load from beatmap
                    if (BeatmapManager.Current != null && !string.IsNullOrEmpty(BeatmapManager.Current.BackgroundImage))
                    {
                        switch (GameBase.Mode)
                        {
                            case OsuModes.Edit:
                            case OsuModes.SelectEdit:
                            case OsuModes.SelectMulti:
                            case OsuModes.SelectPlay:
                            case OsuModes.Play:
                            case OsuModes.Rank:
                            case OsuModes.RankingTagCoop:
                            case OsuModes.RankingTeam:
                            case OsuModes.RankingVs:
                                loaded = SkinManager.Load(BeatmapManager.Current.BackgroundImage, SkinSource.Beatmap);
                                if (loaded != null) loaded.Disposable = true;
                                break;
                        }
                    }

#if CHRISTMAS
                    if (loaded == null)
                    {
                        const int available_xmas_backgrounds = 13;
                        if (loadedBackgroundIndex < 0)
                            loadedBackgroundIndex = GameBase.random.Next(1, available_xmas_backgrounds + 1);
                        else if (GameBase.Mode == OsuModes.Menu)
                            loadedBackgroundIndex = Math.Max(1, (loadedBackgroundIndex + 1) % (available_xmas_backgrounds + 1));

                        loaded = SkinManager.Load(@"menu-background-xmas-" + loadedBackgroundIndex, SkinSource.Osu);
                    }
#endif

                    //if we are a supporter, we can also check the skin for a custom menu-background.
                    if (loaded == null && (BanchoClient.Permission & Permissions.Supporter) > 0 && !SkinManager.IsDefault)
                        loaded = SkinManager.Load(@"menu-background.jpg", SkinSource.Skin);

                    //if we still haven't found a texture, we can fall back to the default.
                    if (loaded == null)
                        loaded = SkinManager.Load(@"menu-background", SkinSource.Osu);

                    //perform the final load.
                    if (loaded != null)
                    {
                        GameBase.Scheduler.Add(delegate
                        {
                            if (updateCountAtStart != backgroundUpdateCount) return;

                            updateBackground(alignmentMode, backgroundAdjustment, loaded);
                        });
                    }
                });
            }
            else
            {
                //we have finished loading a texture.
                BackgroundTexture = loadedTexture;

                loadedBackgroundMode = BanchoClient.Permission;

                bool isSameTexture = BackgroundSprite != null && BackgroundTexture == BackgroundSprite.Texture;

                if (isSameTexture && currentBackgroundAdjustment == null && backgroundAdjustment == null && currentAlignmentMode == alignmentMode)
                    return;

                bool firstChange = BackgroundSprite == null;

                ClearBackground(false);
                BackgroundSprite = createBackgroundSprite(alignmentMode);
                BackgroundSprite.FadeInFromZero(50);

                updateBackgroundAlignment(BackgroundSprite, alignmentMode, backgroundAdjustment);

                SpriteManager backgroundSpriteManager = (alignmentMode & AlignmentMode.Widescreen) != 0 ? spriteManagerBackWide : spriteManagerBack;
                backgroundSpriteManager.Add(BackgroundSprite);

                if (TrianglesBack != null)
                {
                    switch (BackgroundTexture.Source)
                    {
                        case SkinSource.Beatmap:
                            TrianglesBack.SetColours(BeatmapManager.Current.GetFileStream(BeatmapManager.Current.BackgroundImage), finishedReadingColours);
                            break;
                        case SkinSource.Skin:
                            Stream stream = new FileStream(BackgroundTexture.assetName, FileMode.Open, FileAccess.Read, FileShare.Read);
                            TrianglesBack.SetColours(stream, finishedReadingColours);
                            break;
                        default:
                            TrianglesBack.SetColours(null, finishedReadingColours);
                            break;
                    }

                }

                if (OnBackgroundChanged != null) OnBackgroundChanged();
                TrianglesBack.VelocityCurrent = firstChange ? 150 : 50;
            }
        }

        private void finishedReadingColours()
        {
            TrianglesBack.ProduceTriangles = GameBase.Mode != OsuModes.SelectPlay;
            if (BackgroundTexture != null && BackgroundTexture.Source != SkinSource.Beatmap && TrianglesFront.Colours.Count == 0)
                TrianglesFront.SetColours(TrianglesBack.Colours, false);
        }

        private pSprite createBackgroundSprite(AlignmentMode alignmentMode)
        {
            if (BackgroundTexture == null || BackgroundTexture.isDisposed)
                BackgroundTexture = SkinManager.Load(@"menu-background", SkinSource.Osu);

            Fields field = (alignmentMode & AlignmentMode.Storyboard) != 0 ?
                Fields.StoryboardCentre : Fields.Centre;

            pSprite s = new pSprite(BackgroundTexture, field, Origins.Centre, Clocks.Game, Vector2.Zero, SpriteManager.drawOrderBwd(GameBase.Time), true, Color.White);

            return s;
        }

        private void updateBackgroundAlignment(pSprite s, AlignmentMode alignmentMode, BackgroundAdjustmentDelegate backgroundAdjustment)
        {
            if (s == null || BackgroundTexture == null) return;

            if ((alignmentMode & AlignmentMode.Storyboard) != 0)
            {
                s.ScaleToScreen((alignmentMode & AlignmentMode.Widescreen) != 0);
            }
            else
            {
                //first align based on height
                s.Scale = (float)GameBase.WindowSpriteRes / BackgroundTexture.Height;

                //if it isn't wide enough, let's make it so.
                if (s.Width * s.Scale * GameBase.WindowRatioInverse < GameBase.WindowWidth)
                    s.Scale = GameBase.WindowWidth / ((float)s.Width * GameBase.WindowRatioInverse);
            }

            if (backgroundAdjustment != null)
                backgroundAdjustment(BackgroundSprite);

            currentAlignmentMode = alignmentMode;
            currentBackgroundAdjustment = backgroundAdjustment;
        }
    }

    [Flags]
    internal enum AlignmentMode
    {
        Widescreen = 1,
        Storyboard = 2,
        Default = Widescreen,
        StoryboardWidescreen = Storyboard | Widescreen
    }
}
