﻿using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Fruits;
using osu.Graphics.Skinning;
using osu.GameModes.Play.Rulesets.Fruits;
using System;
using osu.GameplayElements.Scoring;
using osu_common;
using osu.Graphics.Sprites;
using osu.Helpers;

namespace osu.GameplayElements
{
    internal class HitObjectManagerFruits : HitObjectManager
    {
        internal HitObjectManagerFruits(bool withSpriteManager = true)
            : base(withSpriteManager)
        {
            if (spriteManager != null)
            {
                spriteManager.SetWidescreen(true); //allow fruits to explode out into widescreen boundaries.
            }
        }

        internal override HitFactory CreateHitFactory()
        {
            return new HitFactoryFruits(this);
        }

        protected override void AddSlider(Slider s)
        {
            ((SliderFruits)s).UpdateCalculations(false);

            foreach (HitCircleFruits f in ((SliderFruits)s).Fruits)
                Add(f);
        }

        protected override void AddSpinner(Spinner s)
        {
            foreach (HitCircleFruitsSpin f in ((SpinnerFruits)s).Fruits)
                Add(f);
        }

        protected override void PostProcessing()
        {
        }

        internal override void UpdateVariables(bool redrawSliders, bool lazy)
        {
            base.UpdateVariables(redrawSliders, lazy);
        }

        internal void InitializeHyperDash(float catcherWidth)
        {
            int lastDirection = 0;
            float catcherWidthHalf = catcherWidth / 2;
            float lastExcess = catcherWidthHalf;

            for (int i = 0; i < hitObjectsCount - 1; i++)
            {
                HitCircleFruits currentObject = hitObjects[i] as HitCircleFruits;

                if (currentObject is HitCircleFruitsSpin || currentObject is HitCircleFruitsTickTiny) continue;

                HitObject nextObject = hitObjects[i + 1];

                while (nextObject is HitCircleFruitsSpin || nextObject is HitCircleFruitsTickTiny)
                {
                    if (++i == hitObjectsCount - 1) break;
                    nextObject = hitObjects[i + 1];
                }

                int thisDirection = nextObject.Position.X > currentObject.Position.X ? 1 : -1;
                float timeToNext = nextObject.StartTime - currentObject.EndTime - (float)(GameBase.SIXTY_FRAME_TIME / 4);
                float distanceToNext = Math.Abs(nextObject.Position.X - currentObject.Position.X) - (lastDirection == thisDirection ? lastExcess : catcherWidthHalf);

                if (timeToNext < distanceToNext)
                {
                    currentObject.MakeHyperDash(nextObject);
                    lastExcess = catcherWidthHalf;
                }
                else
                {
                    currentObject.DistanceToHyperDash = timeToNext - distanceToNext;
                    lastExcess = OsuMathHelper.Clamp(timeToNext - distanceToNext, 0, catcherWidthHalf);
                }

                lastDirection = thisDirection;
            }
        }

        internal override IncreaseScoreType Hit(HitObject h)
        {
            lastHitObject = h;

            IncreaseScoreType hitValue = h.Hit();

            if (Player.currentScore != null && ModManager.CheckActive(ActiveMods, Mods.Perfect))
            {
                if ((hitValue & IncreaseScoreType.FruitTickTinyMiss) > 0)
                    hitValue = IncreaseScoreType.Miss;
            }

            if (hitValue == IncreaseScoreType.Miss)
                CurrentComboBad++;

            int index = hitObjects.BinarySearch(h);

            currentHitObjectIndex = index < 0 ? ~index : index;

            string specialAddition = string.Empty;

            if (currentHitObjectIndex == hitObjectsCount - 1 || hitObjects[currentHitObjectIndex + 1].NewCombo)
            {
                specialAddition = CurrentComboBad == 0 ? "g" : "b";
                if (hitValue == IncreaseScoreType.Hit300 && CurrentComboBad == 0)
                    hitValue |= IncreaseScoreType.GekiAddition;
                CurrentComboBad = 0;
            }

            OnHit(hitValue, specialAddition, h);

            return hitValue;
        }
    }
}