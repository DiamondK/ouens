﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;
using osu.GameplayElements.HitObjects.Mania;
using osu.GameModes.Play.Rulesets;
using osu.GameModes.Play.Rulesets.Mania;
using osu.Input;
using osu_common.Helpers;
using osu.Helpers;
using osu.GameplayElements.Events;
using System.Diagnostics;
using osu.Graphics.Primitives;

namespace osu.GameplayElements
{
    internal class HitObjectManagerMania : HitObjectManager
    {
        internal int hitWindowEarly = 178;
        internal int hitWindow300g = 20;
        internal int hitWindow200 = 60;

        internal int FirstNoteTime = int.MaxValue;
        internal int LastNoteTime = int.MinValue;
        internal int LongestTime = int.MinValue;

        internal int CountNormal = 0;
        internal int CountLong = 0;
        internal int CountTotal = 0;

        protected List<pSprite> barLines;
        private List<ControlPoint> speedChanges;
        private List<ControlPoint> timingChanges;

        private bool perfectMod;
        private HitCircleMania[] nextSound;
        private int[] randomIndex;
        private int[] releaseTime;
        internal int PressHaxCount;
        private int pressHaxChecked;
        internal bool PressHax
        {
            get
            {
                if (pressHaxChecked == 0)
                    return false;
                return (float)PressHaxCount / pressHaxChecked > 0.6;
            }
        }

        protected override bool supportWidescreen
        {
            get { return true; }
        }

        private HitObject latestHitObject;
        internal override HitObject LatestHitObject
        {
            get { return latestHitObject; }
        }

        private const TransformationType transformationsToClear = TransformationType.MovementY | TransformationType.ClipRectangle | TransformationType.None;

        // note movement transformations are tagged with these
        private class TransformationTag
        {
            internal const byte BeforeStartTime = 1;
            internal const byte BetweenStartEndTime = 2;
            internal const byte AfterEndTime = 3;
            internal const byte Freeze = 4;
        }

        protected override void OnSetBeatmap()
        {
            if (spriteManager != null)
                ManiaStage = new StageMania(SkinManager.LoadManiaSkin(StageMania.ColumnsWithMods(Beatmap, ActiveMods), ActiveMods));
            else
                ManiaStage = new StageMania(this);

            nextSound = new HitCircleMania[ManiaStage.Columns.Count];
            releaseTime = new int[ManiaStage.Columns.Count];

            //generate random index
            randomIndex = new int[ManiaStage.Columns.Count];
            Dictionary<int, bool> exist = new Dictionary<int, bool>(ManiaStage.Columns.Count);

            Random random = new Random(Player.Seed);
            for (int i = 0; i < ManiaStage.Columns.Count; i++)
            {
                if (ManiaStage.AllowSpecialStyle && i == 0)
                {
                    if (ManiaStage.SpecialStyleRight)
                    {
                        randomIndex[0] = ManiaStage.Columns.Count - 1;
                        exist[ManiaStage.Columns.Count - 1] = true;
                    }
                    else
                    {
                        randomIndex[0] = 0;
                        exist[0] = true;
                    }
                }
                else
                {
                    int next = random.Next(0, ManiaStage.Columns.Count);
                    while (exist.ContainsKey(next))
                        next = random.Next(0, ManiaStage.Columns.Count);
                    randomIndex[i] = next;
                    exist[next] = true;
                }
            }
            exist.Clear();
        }

        internal HitObjectManagerMania(bool withSpriteManager = true)
            : base(withSpriteManager)
        {
            PressHaxCount = 0;
            pressHaxChecked = 0;
        }

        public override void Dispose()
        {
            if (ManiaStage != null)
                ManiaStage.Dispose();
            base.Dispose();
        }

        protected override bool AllowComboBonuses
        {
            get { return false; }
        }

        protected override bool UseComboColours { get { return false; } }

        protected override bool useHitLighting { get { return false; } }

        internal override HitFactory CreateHitFactory()
        {
            return new HitFactoryMania(this);
        }

        private int VariablesCalcu(double val)
        {
            if (ModManager.CheckActive(ActiveMods, Mods.HardRock))
                val /= 1.4;
            else if (ModManager.CheckActive(ActiveMods, Mods.Easy))
                val *= 1.4;
            /*
                DT changes the audio rate and therefore shortens the hitwindow
                at a system time level. But in mania, we don't want this side effect
                so we multiply a corressponding value to val to keep the hitwindow the same
                in system time level.
             */
            if (ModManager.CheckActive(ActiveMods, Mods.DoubleTime))
                val *= 1.5;
            else if (ModManager.CheckActive(ActiveMods, Mods.HalfTime))
                val *= 0.75;

            return (int)val;
        }

        internal override void UpdateVariables(bool redrawSliders, bool lazy)
        {
            base.UpdateVariables(redrawSliders, lazy);
            //Notice: OD is for specific maps only
            //and we fix hit window for converted maps.
            if (Beatmap.PlayMode == PlayModes.OsuMania)
            {
                float od = Math.Min(10.0f, Math.Max(0, 10.0f - Beatmap.DifficultyOverall));
                hitWindow300g = VariablesCalcu(16);
                HitWindow300 = VariablesCalcu(34 + 3 * od);
                hitWindow200 = VariablesCalcu(67 + 3 * od);
                HitWindow100 = VariablesCalcu(97 + 3 * od);
                HitWindow50 = VariablesCalcu(121 + 3 * od);
                hitWindowEarly = VariablesCalcu(158 + 3 * od);
            }
            else if (Math.Round(Beatmap.DifficultyOverall) > 4)
            {
                hitWindow300g = VariablesCalcu(16);
                HitWindow300 = VariablesCalcu(34);
                hitWindow200 = VariablesCalcu(67);
                HitWindow100 = VariablesCalcu(97);
                HitWindow50 = VariablesCalcu(121);
                hitWindowEarly = VariablesCalcu(158);
            }
            else
            {
                //1.4x
                hitWindow300g = VariablesCalcu(16);
                HitWindow300 = VariablesCalcu(47);
                hitWindow200 = VariablesCalcu(77);
                HitWindow100 = VariablesCalcu(97);
                HitWindow50 = VariablesCalcu(121);
                hitWindowEarly = VariablesCalcu(158);
            }

        }

        internal int GetPhysicalColumn(int column)
        {
            if (ModManager.CheckActive(ActiveMods, Mods.Random))
            {
                //special column is processed in randomIndex, so doesn't need to adjust it here.
                return randomIndex[column];
            }
            else if (ManiaStage.SpecialStyleRight)
            {
                if (column == 0)
                    return ManiaStage.Columns.Count - 1;
                else
                    return column - 1;
            }
            return column;
        }

        private void AddCounter(HitCircleMania h)
        {
            switch (h.ManiaType)
            {
                case ManiaNoteType.Normal:
                    CountNormal++;
                    break;
                case ManiaNoteType.Long:
                    CountLong++;
                    break;
            }
        }

        internal override void Add(HitObject h, bool removeExisting = false)
        {
            //If spriteManager is null, the note's sprites will not be added to it
            //This lets us keep spriteManager for other things like Kiai time bursts, but use our own sprite manager for notes
            SpriteManager temp = spriteManager;
            spriteManager = null;
            base.Add(h, removeExisting);
            spriteManager = temp;

            //We add the notes to a sprite manager in ReCalculate
        }

        private void AddManiaNote(HitCircleMania note)
        {
            if (note.StartTime < FirstNoteTime)
                FirstNoteTime = note.StartTime;
            if (note.EndTime > LastNoteTime)
            {
                LastNoteTime = note.EndTime;
                latestHitObject = note;
            }
            if (note.EndTime - note.StartTime > LongestTime)
                LongestTime = note.EndTime - note.StartTime;

            AddCounter(note);
            Add(note, false);
        }

        protected override void AddCircle(HitCircle h)
        {
            HitCircleMania hb = h as HitCircleMania;
            if (hb != null)
            {
                AddManiaNote(hb);
                return;
            }
            foreach (HitCircleMania note in ((HitCircleManiaRow)h).HitObjects)
                AddManiaNote(note);
        }

        protected override void AddSlider(Slider s)
        {
            foreach (HitCircleMania note in ((SliderMania)s).HitObjects)
                AddManiaNote(note);
        }

        protected override void AddSpinner(Spinner s)
        {
            foreach (HitCircleMania note in ((SpinnerMania)s).HitObjects)
                AddManiaNote(note);
        }

        //notice: notes' drawDepth are 0.8F
        protected override void PostProcessing()
        {
            //some important value
            PreEmpt = Math.Max((int)SpeedMania.TimeAt(0, ManiaStage.Skin.HitPosition), LongestTime) + 200;
            CountTotal = CountNormal + CountLong;

            //init lighting
            SkinSource skinSource = Beatmap.PlayMode == PlayModes.OsuMania ? SkinSource.All : (SkinSource.Skin | SkinSource.Osu);

            Score s = Player.currentScore;
            if (s != null && ModManager.CheckActive(s.enabledMods, Mods.Perfect))
                perfectMod = true;

            ReCalculate(-1);//for there maybe some notes put at 0 offset.

            Debug.Print("--------Mania--------");
            Debug.Write(string.Format("Note Count:{0}({1}/{2})\nPreEmpty:{3}\nMania Star:{4}\n", CountTotal, CountNormal, CountLong, PreEmpt, Beatmap.DifficultyBemaniStars));
        }

        internal void ReCalculate(int audioTime)
        {
            bool paused = false;
            bool upsideDown = ManiaStage.Skin.UpsideDown;

            if (audioTime > LastNoteTime)
                return;

            if (AudioEngine.AudioState == AudioStates.Playing)
            {
                AudioEngine.TogglePause();
                paused = true;
            }

            //Clear anything from a previous ReCalculate
            if (barLines != null)
            {
                barLines.ForEach(b => b.Transformations.Clear());
                barLines.Clear();
            }
            else
                barLines = new List<pSprite>();

            foreach (HitObject h in hitObjects)
            {
                if (((HitCircleMania)h).IsFinished)
                    continue;
                h.SpriteCollection.ForEach(s => s.Transformations.RemoveAll(t => (t.Type & transformationsToClear) > 0));
            }

            //Start a fresh forward play queue
            foreach (StageMania stage in ManiaStage)
            {
                stage.SpriteManagerNotes.Clear(false);
                stage.SpriteManagerNotes.ForwardPlayOptimisedAdd = true;
            }

            //Mania-specific maps use all control points while autoconverts only use BPM changes
            List<ControlPoint> beatmapControlPoints = Beatmap.PlayMode != PlayModes.OsuMania ? Beatmap.TimingPoints : Beatmap.ControlPoints;
            if (beatmapControlPoints.Count == 0 || hitObjects.Count == 0)
                return;

            //For moving things on the play field at the correct speed. It is "collapsed" so you can iterate it in both directions
            speedChanges = new List<ControlPoint>();
            //For placing bar lines
            timingChanges = new List<ControlPoint>();

            double lastBeatLength = beatmapControlPoints[0].beatLength;
            foreach (ControlPoint cp in beatmapControlPoints)
            {
                ControlPoint t = new ControlPoint();
                t.timingChange = false; //So that we can guarantee the ordering when searching
                t.beatLength = cp.beatLength < 0 ? lastBeatLength * cp.bpmMultiplier : cp.beatLength;
                t.timeSignature = cp.timeSignature;
                t.offset = cp.offset;

                //If an SV change right after the initial BPM is before the first note, apply it from the start of time just like the initial BPM
                if (speedChanges.Count == 1 && cp.beatLength < 0 && t.offset < FirstNoteTime)
                    t.offset = speedChanges[0].offset;

                //Collapse changes at the same offset
                if (speedChanges.Count > 0 && t.offset <= speedChanges[speedChanges.Count - 1].offset)
                    speedChanges.RemoveAt(speedChanges.Count - 1);

                //Collapse changes that leave us at the same beat length
                if (speedChanges.Count == 0 || t.beatLength != speedChanges[speedChanges.Count - 1].beatLength)
                    speedChanges.Add(t);

                //Uncollapsed timing changes
                if (cp.beatLength >= 0)
                {
                    timingChanges.Add(t);
                    lastBeatLength = t.beatLength;
                }
            }

            //Move the first offset of the control points to before the song starts
            //Doing this after collapsing speed changes makes it so that SV changes to the opening BPM apply from the very start of the song
            double preTimeStart = beatmapControlPoints[0].offset;
            while (preTimeStart >= 0)
                preTimeStart -= beatmapControlPoints[0].beatLength * (int)beatmapControlPoints[0].timeSignature;
            preTimeStart -= beatmapControlPoints[0].beatLength * (int)beatmapControlPoints[0].timeSignature;

            speedChanges[0].offset = preTimeStart;
            timingChanges[0].offset = preTimeStart;

            //Add a final control point to speed changes which is useful as an endpoint
            speedChanges.Add(new ControlPoint() { offset = Double.PositiveInfinity });

            //Add notes
            for (int i = 0; i < hitObjects.Count; i++)
            {
                HitCircleMania note = (HitCircleMania)hitObjects[i];
                HitCircleManiaLong noteLong = hitObjects[i] as HitCircleManiaLong;
                if (note.IsFinished)
                    continue;

                float headHeight = SpriteHeight(note.s_note);
                addTransformations(note.s_note, note.StartTime, headHeight);
                if (noteLong != null)
                {
                    float rearHeight = SpriteHeight(noteLong.s_rear);
                    addTransformations(noteLong.s_rear, note.EndTime, rearHeight);

                    addTransformations(noteLong.s_body, note.StartTime, headHeight / 2f, -headHeight / 2f, note.EndTime - note.StartTime);

                    //addTransformations very deliberately breaks up the transformations into three parts
                    //From offscreen to the StartTime -> from the StartTime to the EndTime -> from the end time to offscreen
                    //Using these StartTime and EndTime endpoints, we can find the distance between the head and rear sprites
                    float start = noteLong.s_body.Transformations.Find(t => t.Type == TransformationType.MovementY && t.TagNumeric == TransformationTag.BetweenStartEndTime).StartFloat;
                    float end = noteLong.s_body.Transformations.FindLast(t => t.Type == TransformationType.MovementY && t.TagNumeric == TransformationTag.BetweenStartEndTime).EndFloat;
                    noteLong.s_body.VectorScale.Y = Math.Abs(end - start) / noteLong.s_body.DrawHeight * 1.6f * ManiaStage.HeightRatio;
                    noteLong.s_body.FlipVertical = note.s_note.FlipVertical;

                    if (noteLong.IsFrozen || noteLong.IsUnfrozen)
                        FreezeNote(noteLong);
                    if (noteLong.IsUnfrozen)
                        UnfreezeNote(noteLong);
                }

                note.Column.Host.SpriteManagerNotes.Add(note.SpriteCollection);
            }

            //Add barlines
            if (!Configuration.ConfigManager.sIgnoreBarline)
            {
                Color c;
                if (!ManiaStage.Skin.Colours.TryGetValue(@"ColourBarline", out c))
                    c = Color.White;

                for (int i = 0; i < timingChanges.Count; i++)
                {
                    double beatTime = timingChanges[i].offset;
                    double timeEnd = LastNoteTime + 1;
                    if (i + 1 < timingChanges.Count)
                        timeEnd = timingChanges[i + 1].offset - 1;
                    if (timeEnd < audioTime - 1000)
                        continue;

                    while (beatTime < timeEnd)
                    {
                        foreach (StageMania stage in ManiaStage)
                        {
                            pSprite p = new pSprite(GameBase.WhitePixel, Fields.TopLeft, Origins.CentreLeft, Clocks.Audio, new Vector2(0, -100), 0.62F, false, c);
                            p.VectorScale = new Vector2(stage.Width * 1.6f, ManiaStage.Skin.BarlineHeight);

                            addTransformations(p, beatTime, ManiaStage.Skin.BarlineHeight);

                            barLines.Add(p);
                            stage.SpriteManagerNotes.Add(p);
                        }
                        beatTime += timingChanges[i].beatLength * (int)timingChanges[i].timeSignature;
                    }
                }
            }

            //Add arrows
            int arrowTime = FirstNoteTime - 1000;
            for (int arrowCount = 0; arrowTime > speedChanges[0].offset && arrowCount < 3; arrowCount++)
            {
                foreach (StageMania stage in ManiaStage)
                {
                    pSprite arrow = new pSprite(ManiaStage.Skin.Load(@"WarningArrow", @"mania-warningarrow", ManiaStage.SkinSource), Fields.TopLeft, ManiaStage.FlipOrigin(Origins.TopCentre), Clocks.Audio,
                                                new Vector2(stage.Width / 2, 0), 0.63F, false, Color.White);
                    if (SkinManager.Current.Version >= 2.4 || stage.HeightRatio < 1f)
                        arrow.Scale = stage.MinimumRatio;
                    arrow.FlipVertical = upsideDown;

                    //The origin of the arrow is the end of its tail. This is because the tail needs to be 1 second before the first note
                    addTransformations(arrow, arrowTime, -SpriteHeight(arrow));

                    barLines.Add(arrow);
                    stage.SpriteManagerNotes.Add(arrow);
                }
                arrowTime -= 1000;
            }

            //Stop adding to the forward play queue
            foreach (StageMania stage in ManiaStage)
                stage.SpriteManagerNotes.ForwardPlayOptimisedAdd = false;

            if (paused)
                AudioEngine.TogglePause();
        }

        private void addTransformations(pSprite sprite, double time, double extraDistance, float offsetY = 0, double extraTime = 0)
        {
            int index = findOffsetIndex(time, speedChanges);

            addBackwardsTransformations(sprite, time, index, extraDistance, offsetY);
            addForwardsTransformations(sprite, time, index, extraDistance, offsetY, extraTime);

            sprite.Transformations.Sort();
        }

        private void addBackwardsTransformations(pSprite sprite, double endTime, int index, double extraDistance, float offsetY = 0)
        {
            //Move backwards through speedChanges to find the exact time the note should appear
            double endPos = ManiaStage.Skin.HitPosition;
            double finalPos = 0 + Math.Min(extraDistance, 0);
            for (int i = index; i >= 0; i--)
            {
                //Move backwards to the start of this control point
                double startTime = speedChanges[i].offset;
                double startPos = endPos - SpeedMania.DistanceAt(speedChanges[i].beatLength, endTime - startTime);

                if (startTime == endTime)
                    continue;

                if (startPos < finalPos)
                {
                    startTime = endTime - SpeedMania.TimeAt(speedChanges[i].beatLength, endPos - finalPos);
                    startPos = finalPos;
                }

                sprite.Transformations.Add(new Transformation(TransformationType.MovementY,
                                                              ScaleFlipPosition((float)(startPos + offsetY)),
                                                              ScaleFlipPosition((float)(endPos + offsetY)),
                                                              (int)startTime, (int)endTime)
                                                              { TagNumeric = TransformationTag.BeforeStartTime });
                if (startPos == finalPos)
                    break;

                endTime = startTime;
                endPos = startPos;
            }
        }

        private void addForwardsTransformations(pSprite sprite, double startTime, int index, double extraDistance, float offsetY = 0, double extraTime = 0)
        {
            //Same thing as the method above, but forwards
            //This loop has two stages, first going until the final time, then going until the final position
            //To switch between the two stages, Infinity is used to disable the other final state
            double startPos = ManiaStage.Skin.HitPosition;
            double finalPos = Double.PositiveInfinity;
            double finalTime = startTime + extraTime;
            byte tag = TransformationTag.BetweenStartEndTime;
            for (int i = index; i + 1 < speedChanges.Count; i++)
            {
            REDO:
                //Move forwards either to the final time, or to when this control point ends
                double endTime = Math.Min(speedChanges[i + 1].offset, finalTime);
                double endPos = startPos + SpeedMania.DistanceAt(speedChanges[i].beatLength, endTime - startTime);

                if (startTime == endTime)
                    goto END;

                if (endPos > finalPos)
                {
                    endTime = startTime + SpeedMania.TimeAt(speedChanges[i].beatLength, finalPos - startPos);
                    endPos = finalPos;
                }

                sprite.Transformations.Add(new Transformation(TransformationType.MovementY,
                                                              ScaleFlipPosition((float)startPos + offsetY),
                                                              ScaleFlipPosition((float)endPos + offsetY),
                                                              (int)startTime, (int)endTime)
                                                              { TagNumeric = tag });
                if (endPos == finalPos)
                    break;

                startTime = endTime;
                startPos = endPos;

            END:
                //Reached the final time, so go to the final position
                if (endTime == finalTime)
                {
                    finalPos = endPos - ManiaStage.Skin.HitPosition + GameBase.WindowDefaultHeight + Math.Max(extraDistance, 0);
                    finalTime = Double.PositiveInfinity;
                    tag = TransformationTag.AfterEndTime;

                    //We need to redo this iteration because we may not be at the end of the control point
                    goto REDO;
                }
            }
        }

        private static int findOffsetIndex(double endTime, List<ControlPoint> speedChanges)
        {
            //Using timingChange = true, while all elements in speedChanges use false
            //This means that if our offset matches an item in the list, we'll always get the index before it, which is what we want
            ControlPoint target = new ControlPoint() { offset = endTime, timingChange = true };
            int index = speedChanges.BinarySearch(target);
            if (index < 0) index = ~index;
            if (--index < 0) index = 0;
            return index;
        }

        internal float ScaleFlipPosition(float position)
        {
            position = ManiaStage.UpsideDown ? GameBase.WindowDefaultHeight - position : position;
            return position * ManiaStage.HeightRatio;
        }

        internal float SpriteHeight(pSprite p)
        {
            return Math.Abs(p.DrawHeight / 1.6f * p.Scale * p.VectorScale.Y / ManiaStage.HeightRatio);
        }

        internal override void UpdateBasic()
        {
            int startIndex = hitObjects.BinarySearch(new HitObjectDummy(this, AudioEngine.Time - PreEmpt - 200));
            if (startIndex < 0)
                startIndex = ~startIndex;

            int endIndex;
            for (endIndex = startIndex; endIndex < hitObjectsCount - 1; endIndex++)
                if (hitObjects[endIndex].StartTime > AudioEngine.Time + 300)
                    break;

            if (startIndex == hitObjectsCount)
                hitObjectsMinimal.Clear();
            else
                hitObjectsMinimal = hitObjects.GetRange(startIndex, 1 + endIndex - startIndex);
        }

        internal override void UpdateHitObjects()
        {
            //notes hit here.
            bool[] hitted = new bool[ManiaStage.Columns.Count];//some notes have already hitted in that column
            Player.IsSliding = false;
            foreach (HitObject h in hitObjectsMinimal)
            {
                HitCircleMania curr = (HitCircleMania)h;

                curr.Update();

                if (curr.IsHit)
                {
                    //note been hitted too early, and it's still above judge line.
                    if (curr.StartTime > AudioEngine.Time)
                        hitted[curr.Column] = true;
                    continue;
                }
                if (hitted[curr.Column] || AudioEngine.Time < curr.StartTime - hitWindowEarly)
                    continue;

                //give a 0 value hit
                if (AudioEngine.Time - HitWindow100 >= curr.EndTime && !curr.IsHit)
                {
                    Hit(curr);
                    continue;
                }

                if (curr.Column.KeyState && !curr.Column.KeyStateLast)
                {
                    //press
                    if (curr.ManiaType == ManiaNoteType.Normal || curr.ManiaType == ManiaNoteType.Special)
                    {
                        curr.Pressed = true;
                        curr.TimePress = AudioEngine.Time;
                        hitted[curr.Column] = true;
                        nextSound[curr.Column] = null;
                        Player.Instance.LogHitError(curr); //mania here
                        Hit(curr);
                    }
                    else if (curr.ManiaType == ManiaNoteType.Long && !curr.Pressed)
                    {
                        Player.IsSliding = true;
                        curr.TimePress = AudioEngine.Time;
                        hitted[curr.Column] = true;
                        nextSound[curr.Column] = null;
                        curr.Pressed = true;
                        Player.Instance.LogHitError(curr);
                        HitStart((HitCircleManiaLong)curr);
                        curr.Column.HasLongHitLight = true;
                    }
                }
                else if (curr.Column.KeyState && curr.Column.KeyStateLast)
                {
                    //holding
                    if (curr.ManiaType == ManiaNoteType.Long)
                    {
                        if (curr.Pressed)
                        {
                            hitted[curr.Column] = true;
                            nextSound[curr.Column] = null;
                            Holding((HitCircleManiaLong)curr);
                            Player.IsSliding = true;
                            curr.Column.HasLongHitLight = true;
                        }
                        else
                            ((HitCircleManiaLong)curr).UpdateMissing();
                    }
                }
                else if (!curr.Column.KeyState)
                {
                    //not pressing
                    if (curr.ManiaType == ManiaNoteType.Long)
                    {
                        if (curr.Pressed)
                        {
                            curr.Pressed = false;
                            curr.TimeRelease = AudioEngine.Time;

                            Player.Instance.LogHitError(curr, AudioEngine.Time - curr.EndTime);

                            Hit(curr);
                        }
                        else
                            ((HitCircleManiaLong)curr).UpdateMissing();
                    }
                    curr.Column.HasLongHitLight = false;
                }
            }

            if (Player.Instance.Status != PlayerStatus.Playing)
                return;
            for (int i = 0; i < ManiaStage.Columns.Count; i++)
            {
                //release time hax check
                if (ManiaStage.Columns[i].KeyState && !ManiaStage.Columns[i].KeyStateLast)
                    releaseTime[i] = AudioEngine.Time;
                else if (!ManiaStage.Columns[i].KeyState && ManiaStage.Columns[i].KeyStateLast && releaseTime[i] != 0)
                {
                    //press-release interval of normal player should be 40~70ms
                    int interval = AudioEngine.Time - releaseTime[i];
                    if (interval < 20)
                        PressHaxCount += 2;
                    else if (interval < 38)  //based on cheat replay analysis
                        PressHaxCount++;
                    if (interval < 100)
                        pressHaxChecked++; //interval less than 100ms should be count as normal hit.
                }
                //empty press
                if (hitted[i])
                    continue;
                if (ManiaStage.Columns[i].KeyState && !ManiaStage.Columns[i].KeyStateLast)
                {
                    if (nextSound[i] == null)
                    {
                        HitCircleMania h = (HitCircleMania)hitObjects.Find(obj => obj.StartTime > AudioEngine.Time && ((HitCircleMania)obj).Column == i);
                        nextSound[i] = h;
                    }
                    if (nextSound[i] != null)
                        nextSound[i].PlaySound();

                }
            }
        }

        internal void FreezeNote(HitCircleManiaLong note)
        {
            //Remove the head sprites's after-EndTime transformations. These will be recalculated if an Unfreeze happens
            note.s_note.Transformations.RemoveAll(t => t.Type == TransformationType.MovementY && t.TagNumeric == TransformationTag.AfterEndTime);

            //Add a dummy transformation to keep the note alive for as long as it will be needed
            float freezePos = ScaleFlipPosition(ManiaStage.Skin.HitPosition);
            int freezeTime = note.EndTime + HitWindow50;
            note.s_note.Transformations.AddInPlace(new Transformation(TransformationType.MovementY, freezePos, freezePos, freezeTime, freezeTime) { TagNumeric = TransformationTag.Freeze });

            //Clip the note's other sprites above the head sprite
            note.s_rear.ClipRectangle = note.s_body.ClipRectangle = note.Column.Host.ScaleFlipRectangleFromTB(0, ManiaStage.Skin.HitPosition - SpriteHeight(note.s_note) / 2);

            note.IsFrozen = true;
        }

        internal void UnfreezeNote(HitCircleManiaLong note)
        {
            if (!note.IsFrozen)
                return;

            //Remove the dummy transformation we added
            note.s_note.Transformations.RemoveAll(t => t.Type == TransformationType.MovementY && t.TagNumeric == TransformationTag.Freeze);

            //Unfreeze the head sprite by adding back its forwards transformations
            int time = note.IsUnfrozen ? note.UnfreezeTime : Math.Max(note.StartTime, AudioEngine.Time);
            addForwardsTransformations(note.s_note, time, findOffsetIndex(time, speedChanges), SpriteHeight(note.s_note));
            note.s_note.Transformations.Sort();

            //Move the clip rectangle of the body and rear sprites down at the same speed of the new forwards transformations
            RectangleF rect = (RectangleF)note.s_body.ClipRectangle;
            foreach (Transformation t in note.s_note.Transformations)
            {
                if (t.Type == TransformationType.MovementY && t.TagNumeric == TransformationTag.AfterEndTime)
                {
                    float difference = t.EndFloat - t.StartFloat;
                    RectangleF endRect = new RectangleF(rect.X, rect.Y + Math.Min(difference, 0), rect.Width, rect.Height + Math.Abs(difference));
                    Transformation clipTransformation = new Transformation(rect, endRect, t.Time1, t.Time2, EasingTypes.None);
                    note.s_body.Transformations.Add(clipTransformation);
                    note.s_rear.Transformations.Add(clipTransformation);
                    rect = endRect;
                }
            }
            note.s_body.Transformations.Sort();
            note.s_rear.Transformations.Sort();

            note.IsFrozen = false;
            note.IsUnfrozen = true;
            note.UnfreezeTime = time;
        }

        internal IncreaseScoreType Holding(HitCircleManiaLong h)
        {
            lastHitObject = h;
            IncreaseScoreType hitValue = h.Holding();
            if (hitValue > IncreaseScoreType.Ignore)
                OnHit(hitValue, "", h);
            return hitValue;
        }

        internal IncreaseScoreType HitStart(HitCircleManiaLong h)
        {
            lastHitObject = h;
            IncreaseScoreType hitValue = h.HitStart();
            return hitValue;
        }

        internal override IncreaseScoreType Hit(HitObject h)
        {
            lastHitObject = h;

            IncreaseScoreType hitValue = h.Hit();


            int index = hitObjects.BinarySearch(h);
            currentHitObjectIndex = index < 0 ? ~index : index;

            if (hitValue == IncreaseScoreType.Ignore)
                return hitValue;

            if (perfectMod && hitValue < IncreaseScoreType.ManiaHit300)
                hitValue = IncreaseScoreType.MissMania;

            HitCircleMania be = (HitCircleMania)h;

            if (ManiaStage.Skin.SeparateScore)
                be.Column.Host.TriggerScoreIncrease(hitValue);
            else
            {
                foreach (StageMania stage in ManiaStage)
                    stage.TriggerScoreIncrease(hitValue);
            }

            if (hitValue > IncreaseScoreType.Ignore)
            {
                Player.Instance.OnHitSuccess(h);
                be.Column.AddHitLight();

                //Rest LN hitlight at the end of notes
                be.Column.HasLongHitLight = false;
            }

            OnHit(hitValue, "", h);
            return hitValue;
        }
    }
}
