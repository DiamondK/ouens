﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.HitObjects.Taiko;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Primitives;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu_common;

namespace osu.GameplayElements
{
    internal class HitObjectManagerTaiko : HitObjectManager
    {
        private float circleSizeOrg;
        private double sliderMultiplierOrig;

        internal HitObjectManagerTaiko(bool withSpriteManager = true)
            : base(withSpriteManager)
        {
        }

        public override void Dispose()
        {
            Beatmap.DifficultyCircleSize = circleSizeOrg;
            Beatmap.DifficultySliderMultiplier = sliderMultiplierOrig;
            base.Dispose();
        }

        protected override void OnSetBeatmap()
        {
            circleSizeOrg = Beatmap.DifficultyCircleSize;
            sliderMultiplierOrig = Beatmap.DifficultySliderMultiplier;
        }

        protected override bool AllowComboBonuses
        {
            get { return false; }
        }

        protected override bool UseComboColours { get { return false; } }

        protected override bool useHitLighting
        {
            get
            {
                return false;
            }
        }

        internal override HitFactory CreateHitFactory()
        {
            return new HitFactoryTaiko(this);
        }

        internal override void UpdateVariables(bool redrawSliders, bool lazy)
        {
            Beatmap.DifficultyCircleSize = 2;
            Beatmap.DifficultySliderMultiplier = sliderMultiplierOrig * 1.4f;

            base.UpdateVariables(redrawSliders, lazy);

            //Override to avoid changes from easy/hardrock mods.
            SpriteDisplaySize = GameBase.GamefieldWidth / 8f * (1f - 0.7f * -3f / 5);

            HitObjectRadius = SpriteDisplaySize / 2f / GameBase.GamefieldRatio;

            SpriteRatio = SpriteDisplaySize / GamefieldSpriteRes;
            if (spriteManager != null)
            {
                spriteManager.GamefieldSpriteRatio = SpriteRatio;
            }

            HitWindow50 =
                (int)MapDifficultyRange(Beatmap.DifficultyOverall, 135, 95, 70);
            HitWindow100 =
                (int)MapDifficultyRange(Beatmap.DifficultyOverall, 120, 80, 50);
            HitWindow300 =
                (int)MapDifficultyRange(Beatmap.DifficultyOverall, 50, 35, 20);

            PreEmpt = (int)(600 / SliderVelocityAt(0) / 0.6f * 1000); // bug bug bug bug
        }

        protected override void AddSlider(Slider s)
        {
            SliderTaiko st = (SliderTaiko)s;

            //The sprites get added in the update method - so we don't need to add them here.
            st.SpatialLength *= 1.4f; //See 1.4x multiplication of SliderMultiplier aboove.
            double l = st.SpatialLength;
            l *= st.SegmentCount;
            double v2 = SliderScoringPointDistance *
                        Beatmap.DifficultySliderTickRate;
            double bl = Beatmap.beatLengthAt(st.StartTime, true); // must be TRUE here so we get the correct duration
            st.EndTime = st.StartTime + (int)(l / v2 * bl);
            double v = SliderVelocityAt(st.StartTime);
            bl = Beatmap.beatLengthAt(st.StartTime, Beatmap.BeatmapVersion < 8);

            double skipPeriod = Math.Min(
                bl / Beatmap.DifficultySliderTickRate,
                (double)(st.EndTime - st.StartTime) / st.SegmentCount);

            // Only do the conversion if skipPeriod is actually above 0. It can be 0 for sliders with a length of 0 (=> EndTime == StartTime).
            // This avoids an endless loop.
            if (skipPeriod > 0 && Beatmap.PlayMode != PlayModes.Taiko && l / v * 1000 < 2 * bl)
            {
                int i = 0;
                for (double j = st.StartTime; j <= st.EndTime + skipPeriod / 8; j += skipPeriod)
                {
                    HitCircle h;

                    if (st.unifiedSoundAddition)
                    {
                        h = hitFactory.CreateHitCircle(Vector2.Zero, (int)j, false, st.SoundType, 0, SampleSet.None, SampleSet.None, CustomSampleSet.Default, 0, "");
                    }
                    else
                    {
                        h = hitFactory.CreateHitCircle(Vector2.Zero, (int)j, false, st.SoundTypeList[i], 0, SampleSet.None, SampleSet.None, CustomSampleSet.Default, 0, "");
                        i = (i + 1) % st.SoundTypeList.Count;
                    }

                    Add(h, false);
                }
                return;
            }


            base.AddSlider(st);
        }

        internal static readonly Vector2 HIT_LOCATION = new Vector2(160, 125);

        protected override void PostProcessing()
        {
            //Play-mode specific transformations
            if (GameBase.Mode == OsuModes.Edit) return;

            hitObjectsCount = hitObjects.Count;
            if (hitObjectsCount == 0) return;

            Vector2 hitLocation = HIT_LOCATION;
            int inLength = GameBase.WindowWidthScaled - 40;
            const int outLengthTimewise = 300;
            const int outLength = 300;

            double multipier = 1;

            bool hidden = ModManager.CheckActive(ActiveMods, Mods.Hidden) && !ModManager.CheckActive(ActiveMods, Mods.HardRock);

            if (hidden)
            {
                inLength = GameBase.WindowDefaultWidth - 40;
                spriteManager.SetVisibleArea(new RectangleF(0, 0, GameBase.WindowDefaultWidth, GameBase.WindowHeightScaled));
            }

            if (ModManager.CheckActive(ActiveMods, Mods.HardRock))
                multipier *= 1.4f * GameBase.WindowWidthScaled / GameBase.WindowDefaultWidth;
            else if (ModManager.CheckActive(ActiveMods, Mods.Easy))
                multipier *= 0.8f;

            for (int i = 0; i < hitObjectsCount; i++)
            {
                HitObject currHitObject = hitObjects[i];

                currHitObject.ModifyPosition(hitLocation);

                double outLengthCurr = outLength;

                double sv = SliderVelocityAt(currHitObject.StartTime) * multipier;

                if (currHitObject is SliderTaiko)
                {

                    SliderTaiko s = (SliderTaiko)currHitObject;

                    //Adjust the length before calculation to allow for the difficulty multiplier.
                    s.UpdateCalculations(true, multipier);

                    outLengthCurr += s.SpatialLength;
                }

                int inMsLength = (int)(inLength / sv * 1000);
                int outMsLength = (int)(outLengthTimewise / sv * 1000);

                SpinnerTaiko st = currHitObject as SpinnerTaiko;

                if (st != null)
                {
                    //special handling of taiko spinners, where we need to show a warning icon on the taiko bar.
                    pSprite s = st.WarningIcon;

                    const int appearTime = 200;

                    s.Transformations.Add(
                        new Transformation(new Vector2(s.Position.X + inLength, s.Position.Y),
                                           new Vector2(s.Position.X, s.Position.Y),
                                           currHitObject.StartTime - inMsLength, currHitObject.StartTime));
                    s.Transformations.Add(
                        new Transformation(new Vector2(s.Position.X, s.Position.Y),
                                           new Vector2(256, 196),
                                           currHitObject.StartTime, currHitObject.StartTime + appearTime));
                    s.Transformations.Add(
                        new Transformation(TransformationType.Scale, 1, 3,
                                           currHitObject.StartTime, currHitObject.StartTime + appearTime,
                                           EasingTypes.Out));

                    s.Transformations.Add(new Transformation(TransformationType.Fade, 1, 0, currHitObject.StartTime, currHitObject.StartTime
                        + appearTime));
                }
                else
                {
                    for (int j = 0; j < currHitObject.SpriteCollection.Count; j++)
                    {
                        pSprite s = currHitObject.SpriteCollection[j];

                        s.Transformations.Add(
                            new Transformation(new Vector2(s.Position.X + inLength, s.Position.Y),
                                               new Vector2(s.Position.X, s.Position.Y),
                                               currHitObject.StartTime - inMsLength, currHitObject.StartTime));
                        s.Transformations.Add(
                            new Transformation(new Vector2(s.Position.X, s.Position.Y),
                                               new Vector2(s.Position.X - (float)outLengthCurr, s.Position.Y),
                                               currHitObject.StartTime, currHitObject.EndTime + outMsLength));
                    }
                }
            }

            int lastHitTime = hitObjects[hitObjectsCount - 1].EndTime + 1;


            List<ControlPoint> tp = Beatmap.TimingPoints;

            if (tp == null || tp.Count == 0) return;

            int ctp = 0;

            double time = tp[0].offset;
            double measureLength = tp[0].beatLength * (int)tp[0].timeSignature;

            //barline time closest to 0 offset
            time -= measureLength * (int)(time / measureLength);

            //start barlines from positive offset
            if (time < 0)
                time += measureLength;

            pTexture tbl = SkinManager.Load(@"taiko-barline", SkinSource.Osu);

            //Precalculation of barlines.
            while (time <= lastHitTime)
            {
                ControlPoint currpoint = tp[ctp];

                if (time > currpoint.offset || (currpoint.effectFlags & EffectFlags.OmitFirstBarLine) == 0)
                {
                    pSprite barLine = new pSprite(tbl, Fields.GamefieldWide, Origins.Centre, Clocks.Audio, hitLocation, 0.0002f, false, Color.White);
                    barLine.Scale = 0.88f;

                    int intTime = (int)time;

                    double sv = SliderVelocityAt(intTime) * multipier;

                    int inMsLength = (int)(inLength / sv * 1000);
                    int outMsLength = (int)(outLengthTimewise / sv * 1000);

                    spriteManager.Add(barLine);

                    barLine.Transformations.Add(
                            new Transformation(new Vector2(barLine.Position.X + inLength, barLine.Position.Y),
                                               new Vector2(barLine.Position.X, barLine.Position.Y),
                                               intTime - inMsLength, intTime));
                    barLine.Transformations.Add(
                        new Transformation(new Vector2(barLine.Position.X, barLine.Position.Y),
                                           new Vector2(barLine.Position.X - outLength, barLine.Position.Y),
                                           intTime, intTime + outMsLength));
                }

                double bl = currpoint.beatLength;

                if (bl < 800) bl *= (int)currpoint.timeSignature;

                time += bl;

                if (ctp + 1 < tp.Count && time >= tp[ctp + 1].offset)
                {
                    ctp++;
                    time = tp[ctp].offset;
                }
            }
        }

        protected override void HitTransformationsSuccessSpinner(pSprite p)
        {
            p.Transformations.Add(
                new Transformation(new Vector2(320, 240), new Vector2(320, 150), AudioEngine.Time, (int)(AudioEngine.Time + HitFadeIn * 2), EasingTypes.Out));
            p.Transformations.Add(
                new Transformation(TransformationType.Scale, 0.4F, 1F, AudioEngine.Time,
                                   (int)(AudioEngine.Time + (HitFadeIn * 2)), EasingTypes.Out));
            p.Transformations.Add(
                new Transformation(TransformationType.Fade, 1, 0,
                                   AudioEngine.Time + HitFadeIn * 3,
                                   (int)(AudioEngine.Time + (HitFadeIn * 5.5))));
        }

        protected override void HitTransformationsSuccess(pSprite p, bool allowAnimation = true, bool particleAnimations = false)
        {
            if (allowAnimation)
            {
                p.Transformations.Add(
                    new Transformation(TransformationType.Scale, 0.6F, 1.1F, AudioEngine.Time,
                                       (int)(AudioEngine.Time + (HitFadeIn * 0.8))));
                p.Transformations.Add(
                    new Transformation(TransformationType.Scale, 1.1F, 0.9F, AudioEngine.Time + HitFadeIn,
                                       (int)(AudioEngine.Time + (HitFadeIn * 1.2))));
                p.Transformations.Add(
                    new Transformation(TransformationType.Scale, 0.9F, 1F, AudioEngine.Time + HitFadeIn,
                                       (int)(AudioEngine.Time + (HitFadeIn * 1.4))));
            }

            p.Transformations.Add(
                new Transformation(TransformationType.Fade, 0, 1, AudioEngine.Time,
                                   AudioEngine.Time + HitFadeIn));
            p.Transformations.Add(
                new Transformation(TransformationType.Fade, 1, 0,
                                   AudioEngine.Time + HitFadeIn,
                                   (int)(AudioEngine.Time + (HitFadeIn * 2.5))));
        }

        protected override void hitSpritePostProcessing(pSprite p)
        {
            p.Field = Fields.GamefieldWide;
            base.hitSpritePostProcessing(p);
        }

        protected override pTexture[] textureForHitType(string p)
        {
            pTexture[] tex = SkinManager.LoadAll("taiko-" + p);
            if (tex == null || tex.Length == 0)
                tex = SkinManager.LoadAll(p);
            return tex;
        }
    }
}