﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Audio;
using osu.GameModes.Play;
using osu.GameModes.Play.Rulesets;
using osu.GameplayElements.Beatmaps;
using osu.GameplayElements.Events;
using osu.GameplayElements.HitObjects;
using osu.GameplayElements.HitObjects.Osu;
using osu.GameplayElements.Scoring;
using osu.Graphics;
using osu.Graphics.Notifications;
using osu.Graphics.Skinning;
using osu.Graphics.Sprites;
using osu.Input;
using osu_common;

namespace osu.GameplayElements
{
    class HitObjectManagerOsuTarget : HitObjectManagerOsu
    {
        bool isPost = false;
        int firstObjectTime = -1;
        int lastObjectTime;

        int lastComboIndex = 0;
        List<int> comboTimes = new List<int>();

        List<HitObject> intendedObjects = new List<HitObject>();

        private Random targetRandom;

        protected override void OnSetBeatmap()
        {
            if (InputManager.ReplayMode && ModManager.CheckActive(InputManager.ReplayScore.enabledMods, Mods.Target))
            {
                targetRandom = new Random(InputManager.ReplayScore.Seed);
            }
            else
                targetRandom = new Random(Player.Seed);
        }

        internal override void Add(HitObject h, bool removeExisting = false)
        {
            if (!isPost)
            {
                if (firstObjectTime < 0) firstObjectTime = h.StartTime;
                lastObjectTime = h.EndTime;
                if (h.NewCombo) comboTimes.Add(h.StartTime);
                intendedObjects.Add(h);
                return;
            }

            base.Add(h, removeExisting);
        }

        internal override void UpdateVariables(bool redrawSliders, bool lazy)
        {
            base.UpdateVariables(redrawSliders, lazy);
            PreEmpt = 1500;
        }

        double[] piece_angles = new double[] { -Math.PI / 2, 0, Math.PI / 3, Math.PI / 2, Math.PI };

        internal override IncreaseScoreType Hit(HitObject h)
        {
            IncreaseScoreType val = base.Hit(h);

            if (val > 0)
            {
                float rotation = (float)(targetRandom.NextDouble() * Math.PI * 2);

                for (int i = 0; i < 5; i++)
                {
                    double actualPiceAngle = piece_angles[i] + rotation;
                    double velocity = 300 + targetRandom.NextDouble() * 100;

                    switch (val & IncreaseScoreType.BaseHitValuesOnly)
                    {
                        case IncreaseScoreType.Hit100:
                            velocity *= 0.4;
                            break;
                        case IncreaseScoreType.Hit50:
                            velocity *= 0.01;
                            break;
                    }

                    velocity *= Math.Pow(((HitCircleOsuTarget)h).TargetAccuracy, 4);

                    Vector2 gravityVector = new Vector2(
                        (float)(velocity * Math.Cos(actualPiceAngle)),
                        (float)(velocity * Math.Sin(actualPiceAngle))
                    );

                    const int duration = 600;
                    const int weight = 100;
                    const float scale = 0.8f;

                    pSprite piece =
                            new pSprite(SkinManager.Load(@"target-pt-" + (i + 1)),
                                        Fields.Gamefield,
                                        Origins.Centre,
                                        Clocks.AudioOnce, h.EndPosition, 0.99f, false, h.Colour);
                    piece.Rotation = rotation;
                    piece.ScaleTo(piece.Scale * scale, duration);
                    piece.FadeOutFromOne(duration);

                    spriteManager.Add(piece);
                    physics.Add(piece, gravityVector).weight = weight;

                    piece =
                            new pSprite(SkinManager.Load(@"targetoverlay-pt-" + (i + 1)),
                                        Fields.Gamefield,
                                        Origins.Centre,
                                        Clocks.AudioOnce, h.EndPosition, 1, false, Color.White);
                    piece.Rotation = rotation;
                    piece.ScaleTo(piece.Scale * scale, duration);
                    piece.FadeOutFromOne(duration);

                    spriteManager.Add(piece);
                    physics.Add(piece, gravityVector).weight = weight;
                }
            }

            return val;
        }


        int lastUsedIntended;
        protected override void PostProcessing()
        {
            isPost = true;

            Beatmap.StackLeniency = 0;

            double t = firstObjectTime;
            int i = 0;
            Vector2 lastPosition = new Vector2(256, 197);

            double speed = 0;
            double angle = targetRandom.NextDouble() * Math.PI * 2;

            while (t < lastObjectTime)
            {
                double progress = (t - firstObjectTime) / (lastObjectTime - firstObjectTime);

                bool speedIncrease = i % 8 == 0;

                if (lastComboIndex < comboTimes.Count && t >= comboTimes[lastComboIndex])
                    lastComboIndex++;

                if (speedIncrease)
                    speed = HitObjectRadius + (int)(progress * 40) / 40d * 333;

                Vector2 movement;

                ControlPoint cp = Beatmap.controlPointAt(t);

                double multiplier = cp.kiaiMode ? 1.2 : 1;

                if (speedIncrease)
                    multiplier *= 1.5;

                angle += (targetRandom.NextDouble() - 0.5) * 2;

                movement.X = lastPosition.X + (float)(speed * multiplier * Math.Cos(angle));
                movement.Y = lastPosition.Y + (float)(speed * multiplier * Math.Sin(angle));

                int tryCount = 0;
                while (movement.X < 0 || movement.Y < 0 || movement.X > 512 || movement.Y > 384 || (speed > HitObjectRadius && closeToRecent(movement, tryCount)))
                {
                    angle = targetRandom.NextDouble() * Math.PI * 2;
                    movement.X = lastPosition.X + (float)(speed * multiplier * Math.Cos(angle));
                    movement.Y = lastPosition.Y + (float)(speed * multiplier * Math.Sin(angle));

                    if (++tryCount > 10)
                        multiplier *= 0.9;
                }

                lastPosition = movement;

                HitCircleOsuTarget h = new HitCircleOsuTarget(this, lastPosition + new Vector2(0, -10), (int)t, speedIncrease, HitObjectSoundType.Normal, 0);

                EventBreak b = eventManager.eventBreaks.Find(br => br.StartTime <= t && br.EndTime >= t);
                if (b == null)
                {

                    int found = intendedObjects.BinarySearch(h);
                    if (found < 0) found = ~found - 1;

                    if (found >= 0 && found > lastUsedIntended && found < intendedObjects.Count)
                    {
                        lastUsedIntended = found;
                        HitObject closest = intendedObjects[found];
                        h.SampleSet = closest.SampleSet;
                        h.SampleSetAdditions = closest.SampleSetAdditions;
                        h.SoundType = closest.SoundType;
                        h.CustomSampleSet = closest.CustomSampleSet;
                    }

                    foreach (pSprite p in h.SpriteCollection)
                    {
                        //physics.Add(p, new Vector2((float)(progress * (GameBase.random.NextDouble() - 0.5)), 0), true).weight = -100;
                        p.Transformations.Add(new Transformation(TransformationType.Scale, p.Scale * 0.5f, p.Scale, (int)t - PreEmpt, (int)t));
                    }
                    AddCircle(h);
                }

                i++;
                t += Beatmap.beatLengthAt(t, false);
            }

            Sort(false);

            base.PostProcessing();
        }

        private bool closeToRecent(Vector2 checkPoint, int tryCount)
        {
            int check_count = 5;
            float lenience = 2.5f * (1 - tryCount / 10f);
            int check = 0;
            while (check++ < check_count && hitObjectsCount - check - 1 >= 0)
            {
                if (Vector2.Distance(checkPoint, hitObjects[hitObjectsCount - check - 1].Position) < HitObjectRadius * lenience)
                    return true;
            }

            return false;
        }

        PhysicsManager physics = new PhysicsManager();

        internal override void UpdateBasic()
        {
            physics.Update();

            foreach (HitObject h in hitObjectsMinimal)
                h.Position = h.SpriteCollection[0].Position;

            base.UpdateBasic();
        }
    }

    class HitCircleOsuTarget : HitCircleOsu
    {
        internal Vector2 TargetDistance;
        internal float TargetAccuracy;

        internal HitCircleOsuTarget(HitObjectManager hom, Vector2 startPosition, int startTime, bool newCombo, HitObjectSoundType soundType, int comboOffset)
            : base(hom, startPosition, startTime, newCombo, soundType, comboOffset)
        {
            SpriteHitCircleText.Bypass = true;
        }

        internal override string SpriteNameHitCircle
        {
            get
            {
                return @"target";
            }
        }

        protected override bool ShowApproachCircle
        {
            get
            {
                return false;
            }
        }

        internal override void Shake()
        {
            //base.Shake();
        }

        internal override IncreaseScoreType Hit()
        {
            Vector2 mouseGamefield = InputManager.ReplayMode ? InputManager.ReplayGamefieldCursor : GameBase.DisplayToGamefield(InputManager.CursorPosition);
            TargetDistance = mouseGamefield - Position;

            TargetAccuracy = 1 - Vector2.Distance(mouseGamefield, Position) / hitObjectManager.HitObjectRadius;

            return base.Hit();
        }

        internal override void Arm(bool isHit)
        {
            if (!isHit)
            {
                base.Arm(isHit);
                return;
            }

            SpriteCollection.ForEach(p => p.Transformations.RemoveAll(t => t.TagNumeric == ARMED));
        }

        internal override void ApplyDim()
        {
            Color col1 = new Color((byte)Math.Max(0, Colour.R * 0.45F), (byte)Math.Max(0, Colour.G * 0.45F), (byte)Math.Max(0, Colour.B * 0.45F), 255);
            Color col1_alt = new Color(195, 195, 195, 255);
            Color col2_alt = Color.White;

            foreach (pSprite p in DimCollection)
            {
                int bl = (int)hitObjectManager.Beatmap.beatLengthAt(StartTime, false);
                p.Transformations.RemoveAll(t => t.Type == TransformationType.Colour && t.TagNumeric == HitObject.DIM);
                Transformation tr = new Transformation(p.TagNumeric == 0 ? col1_alt : col1, p.TagNumeric == 0 ? col2_alt : Colour, StartTime - bl - 96, StartTime - bl);
                if (tr != null) tr.TagNumeric = HitObject.DIM;
                p.Transformations.Add(tr);
            }
        }
    }
}
