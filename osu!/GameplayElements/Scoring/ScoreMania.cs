﻿using osu.GameplayElements.Beatmaps;
using osu_common;
using osu_common.Bancho.Objects;

namespace osu.GameplayElements.Scoring
{
    internal class ScoreMania : Score
    {
        internal override bool replayAllowFrameSkip { get { return false; } }

        internal ScoreMania()
        {
            playMode = PlayModes.OsuMania;
        }

        internal ScoreMania(bScoreFrame frame, string name)
            : base(frame, name)
        {
            playMode = PlayModes.OsuMania;
        }

        internal ScoreMania(Beatmap beatmap, string name)
            : base(beatmap, name)
        {
            playMode = PlayModes.OsuMania;
        }

        internal ScoreMania(string input, Beatmap beatmap)
            : base(input, beatmap)
        {
            playMode = PlayModes.OsuMania;
        }

        internal override float accuracy
        {
            get
            {
                if (totalHits == 0)
                    return 1;

                return (float)(count50 * 50 + count100 * 100 + countKatu * 200 + (count300 + countGeki) * 300) / (totalHits * 300);
            }
        }

        internal override int totalHits
        {
            get { return count50 + count100 + count300 + countMiss + countGeki + countKatu; }
        }

        internal override int totalSuccessfulHits
        {
            get { return count50 + count100 + count300 + countGeki + countKatu; }
        }

        internal override Rankings ranking
        {
            get
            {
                float acc = accuracy;
                if (!pass) //something has gone wrong.
                    return Rankings.F;
                if (acc == 1)
                    return ModManager.CheckActive(enabledMods, Mods.Hidden | Mods.Flashlight | Mods.FadeIn)?Rankings.XH:Rankings.X;
                if (acc > 0.95)
                    return ModManager.CheckActive(enabledMods, Mods.Hidden | Mods.Flashlight | Mods.FadeIn)?Rankings.SH:Rankings.S;
                if (acc > 0.9)
                    return Rankings.A;
                if (acc > 0.8)
                    return Rankings.B;
                if (acc > 0.7)
                    return Rankings.C;
                return Rankings.D;
            }
        }
    }
}