using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.Graphics.Notifications;
using osu.Helpers;
using osu.Helpers.Forms;
using osu.Online;
using osu_common;
using osu_common.Helpers;
using SevenZip.Compression.LZMA;

namespace osu.GameplayElements.Scoring
{
    internal delegate void GotReplayDataHandler(object sender);

    internal class ScoreManager
    {
        internal const int SCORES_PER_SONG = 40;
        private static int DatabaseVersion;
        internal static Dictionary<string, List<Score>> LocalScores;
        private static bool changesPresent;

        internal static string DATABASE_FILENAME = @"scores.db";

        internal static List<Score> FindScores(string checksum, PlayModes playMode)
        {
            if (LocalScores == null) return null;

            List<Score> list;
            return LocalScores.TryGetValue(checksum, out list) ? list.FindAll(s => s.playMode == playMode) : null;
        }

        internal static void InitializeScoreManager()
        {
            if (LocalScores != null)
                return;

            //ensure we have beatmaps available!
            if (BeatmapManager.Beatmaps == null)
                BeatmapManager.ProcessBeatmaps();

            LocalScores = new Dictionary<string, List<Score>>();

            bool databasePresent = File.Exists(DATABASE_FILENAME);

            if (!databasePresent && File.Exists(@"scores.db"))
            {
                //copy real public database across for migration testing.
                File.Copy(@"scores.db", DATABASE_FILENAME);
                databasePresent = true;
            }

            if (databasePresent)
            {
                try
                {
                    using (Stream stream = File.Open(DATABASE_FILENAME, FileMode.Open))
                    using (SerializationReader sr = new SerializationReader(stream))
                    {
                        DatabaseVersion = sr.ReadInt32();

                        int count = sr.ReadInt32();

                        for (int i = 0; i < count; i++)
                        {
                            string checksum = sr.ReadString();
                            int scoreCount = sr.ReadInt32();

                            List<Score> list = new List<Score>(scoreCount);

                            for (int j = 0; j < scoreCount; j++)
                            {
                                Score s = ScoreFactory.Create((PlayModes)sr.ReadByte(), null);
                                s.ReadHeaderFromStream(sr);
                                if (ModManager.CheckActive(s.enabledMods, Mods.Target))
                                    s = new ScoreTarget(s);
                                s.ReadFromStream(sr);
                                list.Add(s);
                            }

                            if (BeatmapManager.GetBeatmapByChecksum(checksum) != null)
                                LocalScores.Add(checksum, list);
                            else
                            {
                                foreach (Score s in list)
                                    s.PurgeReplay();
                            }
                        }
                    }
                }
                catch
                {
                    string backupFilename = DATABASE_FILENAME + @"." + DateTime.Now.Ticks + @".bak";
                    if (File.Exists(DATABASE_FILENAME) && !File.Exists(backupFilename))
                        File.Move(DATABASE_FILENAME, backupFilename);
                }
            }

            if (DatabaseVersion < 1024 && Directory.GetFiles(@"Data/r").Length > 0)
            {
                GameBase.MenuActive = true;
                Maintenance m = new Maintenance(MaintenanceType.RestoreLocalScoresFromReplays);
                m.ShowDialog(GameBase.Form);
                GameBase.MenuActive = false;
            }

            //Clean up missing beatmaps.
            foreach (KeyValuePair<string, List<Score>> p in LocalScores)
            {
                if (BeatmapManager.GetBeatmapByChecksum(p.Key) != null) continue;

                foreach (Score s in p.Value)
                    s.PurgeReplay();
            }

            if (!databasePresent)
                SaveToDisk(true);
        }

        internal static int InsertScore(Score score, bool checkOnly, bool saveReplay = true)
        {
            InitializeScoreManager();

            if (score == null)
                return -1;

            int higherCount = 0;
            int count = 0;
            Score lowestHigherScore = null;
            Score lowestLowerScore = null;

            List<Score> songList = FindScores(score.fileChecksum, Player.Mode);
            if (songList == null)
            {
                songList = new List<Score>();
                LocalScores.Add(score.fileChecksum, songList);
                changesPresent = true;
            }

            foreach (Score s in songList)
            {
                count++;
                if (s.totalScore >= score.totalScore)
                {
                    higherCount++;
                    if (lowestHigherScore == null || s.totalScore < lowestHigherScore.totalScore)
                        lowestHigherScore = s;
                    if (higherCount >= SCORES_PER_SONG)
                        return -1;
                }
                else if (lowestLowerScore == null || s.totalScore < lowestLowerScore.totalScore)
                    lowestLowerScore = s;

                //Found duplicate (even though the
                if (s.offlineScoreChecksum == score.offlineScoreChecksum)
                    return count;
            }

            if (!checkOnly)
            {
                //The above list is a copy (from FindAll) so we want to get the actual list to add to.
                List<Score> actualList = LocalScores[score.fileChecksum];
                changesPresent = true;

                if (lowestLowerScore != null && count >= SCORES_PER_SONG)
                {
                    lowestLowerScore.PurgeReplay();
                    actualList.Remove(lowestLowerScore);
                }

                if (saveReplay)
                    ExportReplay(score, score.replayFilename);

                int index = actualList.BinarySearch(score);
                if (index < 0) index = ~index;

                actualList.Insert(index, score);
            }

            return higherCount + 1;
        }

        internal static void SaveToDisk(bool force = false)
        {
            if (LocalScores == null || (!force && !changesPresent))
                return;

            changesPresent = false;

            using (Stream stream = new SafeWriteStream(DATABASE_FILENAME))
            using (SerializationWriter sw = new SerializationWriter(stream))
            {
                sw.Write(General.VERSION);
                sw.Write(LocalScores.Count);

                foreach (KeyValuePair<string, List<Score>> s in LocalScores)
                {
                    sw.Write(s.Key);
                    sw.Write(s.Value.Count);
                    foreach (Score sc in s.Value)
                    {
                        sc.ClearReplayData();
                        sc.WriteToStream(sw);
                    }
                }
            }
        }

        public static Score ReadReplayFromFile(string filename)
        {
            return ReadReplayFromFile(filename, true);
        }

        public static Score ReadReplayFromFile(string filename, bool handlePickup)
        {
            //Make sure the score manager is already initialized.
            InitializeScoreManager();

            Score inScore = null;

            bool success = false;

            try
            {
                using (Stream stream = File.Open(filename, FileMode.Open))
                {
                    SerializationReader sr = new SerializationReader(stream);
                    inScore = ScoreFactory.Create((PlayModes)sr.ReadByte(), null);
                    inScore.ReadHeaderFromStream(sr);
                    if (ModManager.CheckActive(inScore.enabledMods, Mods.Target))
                        inScore = new ScoreTarget(inScore);
                    inScore.ReadFromStream(sr);
                    if (inScore.date < DateTime.UtcNow + new TimeSpan(365 * 5, 0, 0, 0))
                        success = true;
                }
            }
            catch { }


            if (!success)
            {
                try
                {
                    using (Stream stream = File.Open(filename, FileMode.Open))
                        inScore = (Score)DynamicDeserializer.Deserialize(stream);
                }
                catch
                {
                    NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.ScoreManager_ReplayCorrupt));
                    return null;
                }
            }

            if (inScore == null)
            {
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.ScoreManager_ReplayCorrupt));
                return null;
            }

            if (inScore.date.Year > 2050 || inScore.date == DateTime.MinValue)
            {
                string[] split = filename.Split('-');
                if (split.Length > 1)
                {
                    long outfiletime = 0;
                    if (long.TryParse(split[1].Replace(@".osr", string.Empty), out outfiletime))
                        inScore.date = DateTime.FromFileTimeUtc(outfiletime);
                }
            }

            if (inScore.date.Year > 2050)
            {
                //this score is TOTALLY fucked.
                File.Delete(filename);
                NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.ScoreManager_ReplayCorrupt));
                return null;
            }

            if (BeatmapManager.GetBeatmapByChecksum(inScore.fileChecksum) == null)
            {
                //attempt pickup.
                if (handlePickup)
                    OsuDirect.HandlePickup(inScore.fileChecksum, null,
                                           delegate
                                           {
                                               NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.ScoreManager_DontHaveBeatmap));
                                           });
                return null;
            }

            InsertScore(inScore, false);

            return inScore;
        }

        public static bool ExportReplay(Score score, string filename = null)
        {
            bool userTriggered = filename == null;

            if (filename == null)
            {
                if (!Directory.Exists(@"Replays")) Directory.CreateDirectory(@"Replays");
                filename = string.Format(@"{0} - {1} ({2:yyyy-MM-dd}) {3}.osr", score.playerName, BeatmapManager.Current.DisplayTitle, score.date.ToUniversalTime(), score.playMode);
                filename = @"Replays\" + GeneralHelper.WindowsFilenameStrip(filename);
            }

            bool success = true;
            if (score.rawReplayCompressed == null)
            {
                if (score.replay == null || score.replay.Count == 0)
                    success = false;
                else
                    score.rawReplayCompressed = SevenZipHelper.Compress(new ASCIIEncoding().GetBytes(score.replayFormatted));
            }

            if (success)
            {
                using (Stream stream = File.Open(filename, FileMode.Create))
                    score.WriteToStream(new SerializationWriter(stream));

                if (!userTriggered && score.Frames.Count > 0)
                {
                    using (Stream stream = File.Open(filename.Replace(@".osr", @".osg"), FileMode.Create))
                    using (SerializationWriter sw = new SerializationWriter(stream))
                    {
                        sw.Write(General.VERSION);
                        sw.Write(score.Frames);
                    }
                }
            }

            if (userTriggered)
            {
                if (success)
                    NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.ScoreManager_SavedReplayToFile), filename), Color.BlueViolet, 6000);
                else
                    NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.ScoreManager_ReplayFailed));
            }

            return success;
        }

        internal static void Remove(string checksum)
        {
            LocalScores.Remove(checksum);
            changesPresent = true;
        }

        internal static void RemoveScore(string checksum, string scoreChecksum)
        {
            if (LocalScores.ContainsKey(checksum))
            {
                List<Score> list = LocalScores[checksum];
                int index = list.FindIndex(s => s.scoreChecksum == scoreChecksum);
                if (index >= 0)
                    list.RemoveAt(index);
                LocalScores[checksum] = list;
                changesPresent = true;
            }
        }
    }
}