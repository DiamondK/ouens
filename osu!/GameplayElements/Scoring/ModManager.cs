﻿using System;
using osu.GameModes.Play;
using osu_common;
using osu.GameModes.Play.Rulesets.Mania;
using osu.GameplayElements.Beatmaps;
using osu.Helpers;
using System.Collections.Generic;
using osu.Graphics.Sprites;
using osu.Graphics.Skinning;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using osu.Input;
using osu.GameModes.Online;
using osu.GameModes.Select;

namespace osu.GameplayElements.Scoring
{
    internal static class ModManager
    {
        internal static Obfuscated<Mods> ModStatus = Mods.None;


        internal delegate void ModsHandler(object sender, Mods item);
        internal static event ModsHandler OnModsChanged;

        internal static void TriggerModsChanged()
        {
            if (OnModsChanged != null)
                OnModsChanged(null, ModStatus);
        }

        public static bool AllowRanking(Mods enabledMods)
        {
            if (Player.Mode == PlayModes.OsuMania)
            {
                if (CheckActive(Mods.HardRock | Mods.Random, enabledMods))
                    return false;
                if (BeatmapManager.Current.PlayMode == PlayModes.Osu)
                {
                    int columns = StageMania.ColumnsWithMods(BeatmapManager.Current, enabledMods);
                    if (columns < 4 || columns > 8)
                        return false;
                }
            }
            if (CheckActive(Mods.Autoplay, enabledMods))
                return false;
            if (CheckActive(Mods.Target, enabledMods))
                return false;
            return true;
        }

        internal static void Reset()
        {
            ModStatus = Mods.None;
        }

        internal static void CheckForCurrentPlayMode(Mods ensureSet = Mods.None)
        {
            switch (Player.Mode)
            {
                case PlayModes.Osu:
                    //  ModStatus &= ~(Mods.FadeIn);
                    break;
                case PlayModes.Taiko:
                    ModStatus &= ~(Mods.Relax2 | Mods.SpunOut | Mods.Target);
                    break;
                case PlayModes.CatchTheBeat:
                    ModStatus &= ~(Mods.Relax2 | Mods.SpunOut | Mods.Target);
                    break;
                case PlayModes.OsuMania:
                    ModStatus &= ~(Mods.Relax2 | Mods.SpunOut | Mods.Relax | Mods.Target);
                    break;
            }

            bool isMania = Player.Mode == PlayModes.OsuMania;

            if (!isMania)
                ModStatus &= ~(Mods.KeyMod | Mods.Random | Mods.FadeIn);

            //Loop through each bitwise mod for this check.
            for (Mods i = ensureSet != Mods.None ? ensureSet : (Mods)1;
                i < (ensureSet != Mods.None ? (Mods)((int)ensureSet << 1) : Mods.LastMod);
                i = (Mods)((int)i * 2))
            {
                Mods thisCheck = ModStatus & i;
                if (thisCheck == 0)
                    continue;

                switch (thisCheck)
                {
                    case Mods.Cinema:
                        ModStatus |= Mods.Autoplay;
                        break;
                    case Mods.NoFail:
                        ModStatus &= ~Mods.SuddenDeath;
                        ModStatus &= ~Mods.Perfect;
                        ModStatus &= ~Mods.Relax2;
                        ModStatus &= ~Mods.Relax;
                        break;
                    case Mods.Easy:
                        ModStatus &= ~Mods.HardRock;
                        break;
                    case Mods.Hidden:
                        if (isMania)
                        {
                            ModStatus &= ~Mods.Flashlight;
                            ModStatus &= ~Mods.FadeIn;
                        }
                        break;
                    case Mods.FadeIn:
                        ModStatus &= ~Mods.Hidden;
                        ModStatus &= ~Mods.Flashlight;
                        break;
                    case Mods.Flashlight:
                        if (isMania)
                        {
                            ModStatus &= ~Mods.Hidden;
                            ModStatus &= ~Mods.FadeIn;
                        }
                        break;
                    case Mods.HardRock:
                        ModStatus &= ~Mods.Easy;
                        break;
                    case Mods.SuddenDeath:
                        ModStatus &= ~Mods.NoFail;
                        ModStatus &= ~Mods.Relax2;
                        ModStatus &= ~Mods.Relax;
                        break;
                    case Mods.Perfect:
                        ModStatus |= Mods.SuddenDeath;
                        break;
                    case Mods.DoubleTime:
                        ModStatus &= ~Mods.HalfTime;
                        break;
                    case Mods.Nightcore:
                        ModStatus |= Mods.DoubleTime;
                        break;
                    case Mods.HalfTime:
                        ModStatus &= ~Mods.DoubleTime;
                        ModStatus &= ~Mods.Nightcore;
                        break;
                    case Mods.Relax:
                        ModStatus &= ~Mods.Relax2;
                        ModStatus &= ~Mods.NoFail;
                        ModStatus &= ~Mods.SuddenDeath;
                        ModStatus &= ~Mods.Perfect;
                        break;
                    case Mods.Relax2:
                        ModStatus &= ~Mods.SpunOut;
                        ModStatus &= ~Mods.Relax;
                        ModStatus &= ~Mods.NoFail;
                        ModStatus &= ~Mods.SuddenDeath;
                        ModStatus &= ~Mods.Perfect;
                        break;
                    case Mods.SpunOut:
                        ModStatus &= ~Mods.Relax2;
                        break;
                }
            }

            if ((ModStatus & Mods.Autoplay) > 0)
            {
                ModStatus &= ~Mods.SpunOut;
                ModStatus &= ~Mods.Relax;
                ModStatus &= ~Mods.Relax2;
                ModStatus &= ~Mods.SuddenDeath;
                ModStatus &= ~Mods.Perfect;
            }
        }

        internal static bool CheckActive(Mods mod)
        {
            return CheckActive(ModStatus, mod);
        }

        internal static bool CheckActive(Mods haystack, Mods needle)
        {
            return (haystack & needle) > 0;
        }

        internal static double ScoreMultiplier()
        {
            return ScoreMultiplier(ModStatus);
        }

        internal static double ScoreMultiplier(Mods modStatus)
        {

            double multiplier = 1;
            switch (Player.Mode)
            {
                case PlayModes.Osu:
                case PlayModes.Taiko:
                    if (CheckActive(modStatus, Mods.NoFail))
                        multiplier *= 0.5;
                    if (CheckActive(modStatus, Mods.Easy))
                        multiplier *= 0.5;
                    if (CheckActive(modStatus, Mods.HalfTime))
                        multiplier *= 0.3;
                    if (CheckActive(modStatus, Mods.Hidden))
                        multiplier *= 1.06;
                    if (CheckActive(modStatus, Mods.HardRock))
                        multiplier *= 1.06;
                    if (CheckActive(modStatus, Mods.DoubleTime))
                        multiplier *= 1.12;
                    if (CheckActive(modStatus, Mods.Flashlight))
                        multiplier *= 1.12;
                    if (CheckActive(modStatus, Mods.SpunOut))
                        multiplier *= 0.9;
                    if (CheckActive(modStatus, Mods.Relax) || CheckActive(modStatus, Mods.Relax2))
                        multiplier *= 0;
                    break;
                case PlayModes.CatchTheBeat:
                    if (CheckActive(modStatus, Mods.NoFail))
                        multiplier *= 0.5;
                    if (CheckActive(modStatus, Mods.Easy))
                        multiplier *= 0.5;
                    if (CheckActive(modStatus, Mods.HalfTime))
                        multiplier *= 0.3;
                    if (CheckActive(modStatus, Mods.Hidden))
                        multiplier *= 1.06;
                    if (CheckActive(modStatus, Mods.HardRock))
                        multiplier *= 1.12;
                    if (CheckActive(modStatus, Mods.DoubleTime))
                        multiplier *= 1.06;
                    if (CheckActive(modStatus, Mods.Flashlight))
                        multiplier *= 1.12;
                    if (CheckActive(modStatus, Mods.SpunOut))
                        multiplier *= 0.9;
                    if (CheckActive(modStatus, Mods.Relax) || CheckActive(modStatus, Mods.Relax2))
                        multiplier *= 0;
                    break;
                case PlayModes.OsuMania:
                    if (CheckActive(modStatus, Mods.NoFail))
                        multiplier *= 0.5;
                    if (CheckActive(modStatus, Mods.Easy))
                        multiplier *= 0.5;
                    if (CheckActive(modStatus, Mods.HalfTime))
                        multiplier *= 0.5;
                    if (CheckActive(modStatus, Mods.Hidden) || CheckActive(modStatus, Mods.FadeIn))
                        multiplier *= 1.06;
                    if (CheckActive(modStatus, Mods.HardRock))
                        multiplier *= 1.08;
                    if (CheckActive(modStatus, Mods.DoubleTime))
                        multiplier *= 1.1;
                    if (CheckActive(modStatus, Mods.Flashlight))
                        multiplier *= 1.06;
                    if (CheckActive(modStatus, Mods.SpunOut))
                        multiplier *= 0;
                    if (CheckActive(modStatus, Mods.Relax))
                        multiplier *= 0;
                    if (CheckActive(modStatus, Mods.Relax2))
                        multiplier *= 0;
                    if (CheckActive(modStatus, Mods.Random))
                        multiplier *= 1;
                    if (BeatmapManager.Current != null)
                    {
                        int org = StageMania.ColumnsWithoutMods(BeatmapManager.Current);
                        int use = StageMania.ColumnsWithMods(BeatmapManager.Current);
                        if (use > org)
                            multiplier *= 0.9;
                        else if (use < org)
                        {
                            multiplier *= (0.9 - 0.04 * (org - use));
                        }
                    }
                    break;
            }
            return Math.Max(0, multiplier);
        }

        private delegate void formatAddString(string shortString, string longString = null);

        private static string format(Mods mods, bool shortForm, bool showEmpty, bool addBrackets, bool addSpace)
        {
            if (mods == Mods.None)
                return showEmpty ? "None" : String.Empty;

            string r = string.Empty;

            formatAddString add = delegate(string shortString, string longString)
            {
                r += (shortForm ? shortString : longString ?? shortString) + ',';
            };

            PlayModes beatmapPlayMode = BeatmapManager.Current != null ? BeatmapManager.Current.PlayMode : PlayModes.Osu;

            if (CheckActive(Mods.Cinema, mods))
                add("Cinema");
            else if (CheckActive(Mods.Autoplay, mods))
                add("Auto");
            if (beatmapPlayMode == PlayModes.Osu && CheckActive(Mods.Target, mods))
                add("TP", "TargetPractice");
            if (beatmapPlayMode == PlayModes.Osu && CheckActive(Mods.SpunOut, mods))
                add("SO", "SpunOut");
            if (CheckActive(Mods.Easy, mods))
                add("EZ", "Easy");
            if (CheckActive(Mods.NoFail, mods))
                add("NF", "NoFail");
            if (CheckActive(Mods.Hidden, mods))
                add("HD", "Hidden");
            else if ((beatmapPlayMode == PlayModes.Osu || beatmapPlayMode == PlayModes.OsuMania) && CheckActive(Mods.FadeIn, mods))
                add("FI", "FadeIn");
            if (CheckActive(Mods.Nightcore, mods))
                add("NC", "Nightcore");
            else if (CheckActive(Mods.DoubleTime, mods))
                add("DT", "DoubleTime");
            if (CheckActive(Mods.HalfTime, mods))
                add("HT", "HalfTime");
            if (CheckActive(Mods.HardRock, mods))
                add("HR", "HardRock");
            if (beatmapPlayMode != PlayModes.OsuMania && CheckActive(Mods.Relax, mods))
                add("Relax");
            if (beatmapPlayMode == PlayModes.Osu && CheckActive(Mods.Relax2, mods))
                add("AP", "AutoPilot");
            if (CheckActive(Mods.Perfect, mods))
                add("PF", "Perfect");
            else if (CheckActive(Mods.SuddenDeath, mods))
                add("SD", "SuddenDeath");
            if (CheckActive(Mods.Flashlight, mods))
                add("FL", "Flashlight");

            if (beatmapPlayMode == PlayModes.Osu)
            {
                if (CheckActive(Mods.Key1, mods))
                    add("1K");
                else if (CheckActive(Mods.Key2, mods))
                    add("2K");
                else if (CheckActive(Mods.Key3, mods))
                    add("3K");
                else if (CheckActive(Mods.Key4, mods))
                    add("4K");
                else if (CheckActive(Mods.Key5, mods))
                    add("5K");
                else if (CheckActive(Mods.Key6, mods))
                    add("6K");
                else if (CheckActive(Mods.Key7, mods))
                    add("7K");
                else if (CheckActive(Mods.Key8, mods))
                    add("8K");
                else if (CheckActive(Mods.Key9, mods))
                    add("9K");
            }

            if (beatmapPlayMode == PlayModes.Osu || beatmapPlayMode == PlayModes.OsuMania)
            {
                if (CheckActive(Mods.KeyCoop, mods))
                    add("2P", "Co-op");
                if (CheckActive(Mods.Random, mods))
                    add("RD", "Random");
            }

            r = r.Trim(',');
            if (r.Length == 0)
                return "";
            if (addBrackets)
                r = "(" + r + ")";
            if (addSpace)
                r += " ";
            return r;
        }

        internal static string Format(Mods mods, bool showEmpty)
        {
            return format(mods, false, showEmpty, false, false);
        }

        internal static string FormatShort(Mods mods, bool addBrackets, bool addSpace)
        {
            return format(mods, true, false, addBrackets, addSpace);
        }

        internal static List<pSprite> ShowModSprites(Mods mods, Vector2 position, float scale, string tag = "")
        {
            List<pSprite> sprites = new List<pSprite>();

            int time = 0;
            float dep = 0;

            foreach (Mods m in Enum.GetValues(typeof(Mods)))
            {
                if (ModManager.CheckActive(mods, m))
                {
                    if (m == Mods.DoubleTime && ModManager.CheckActive(mods, Mods.Nightcore))
                        continue;
                    if (m == Mods.SuddenDeath && ModManager.CheckActive(mods, Mods.Perfect))
                        continue;

                    pSprite p =
                        new pSprite(SkinManager.Load(@"selection-mod-" + m.ToString().ToLower()),
                                    Fields.TopLeft,
                                    Origins.Centre,
                                    Clocks.Game,
                                    position, 0.975F + dep, true,
                                    Color.TransparentWhite);
                    p.Tag = tag;
                    p.Transformations.Add(new Transformation(TransformationType.Scale, 2 * scale, 1 * scale, GameBase.Time + time, GameBase.Time + time + 400) { Easing = EasingTypes.Out });
                    p.Transformations.Add(new Transformation(TransformationType.Fade, 0, 1, GameBase.Time + time, GameBase.Time + time + 400) { Easing = EasingTypes.Out });
                    sprites.Add(p);

                    time += 200;
                    dep += 0.00001f;
                    position.X += 20 * scale;
                }
            }

            return sprites;
        }
    }
}