using System;
using System.Collections.Generic;
using System.Text;
using osu.GameModes.Play;
using osu.GameplayElements.Beatmaps;
using osu.Input;
using osu_common;
using osu_common.Bancho.Objects;

namespace osu.GameplayElements.Scoring
{
    internal static class ScoreFactory
    {

        internal static Score Create(PlayModes playMode, Beatmap beatmap)
        {
            return Create(playMode, string.Empty, beatmap);
        }


        internal static Score Create(PlayModes playMode, string name, Beatmap beatmap)
        {
            switch (playMode)
            {
                case PlayModes.CatchTheBeat:
                    return new ScoreFruits(beatmap, name);
                case PlayModes.Taiko:
                    return new ScoreTaiko(beatmap, name);
                case PlayModes.OsuMania:
                    return new ScoreMania(beatmap, name);
                default:
                    if (Player.IsTargetPracticeMode)
                        return new ScoreTarget(beatmap, name);
                    else
                        return new Score(beatmap, name);
            }
        }

        public static Score Create(PlayModes playMode, bScoreFrame frame, string name)
        {
            switch (playMode)
            {
                case PlayModes.CatchTheBeat:
                    return new ScoreFruits(frame,name);
                case PlayModes.Taiko:
                    return new ScoreTaiko(frame, name);
                case PlayModes.OsuMania:
                    return new ScoreMania(frame, name);
                default:
                    return new Score(frame,name);
            }
        }

        public static Score CreateFromOnline(PlayModes playMode, string input, Beatmap beatmap)
        {
            switch (playMode)
            {
                case PlayModes.CatchTheBeat:
                    return new ScoreFruits(input, beatmap);
                case PlayModes.Taiko:
                    return new ScoreTaiko(input, beatmap);
                case PlayModes.OsuMania:
                    return new ScoreMania(input, beatmap);
                default:
                    return new Score(input, beatmap);
            }
        }
    }
}
