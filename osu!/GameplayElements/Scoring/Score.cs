using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Configuration;
using osu.GameplayElements.Beatmaps;
using osu.Input.Handlers;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho;
using osu_common.Bancho.Objects;
using osu_common.Helpers;
using osu_common.Libraries.NetLib;
using SevenZip.Compression.LZMA;
using System.Threading;
using osu.Graphics.Notifications;
using System.Diagnostics;
using osu.GameModes.Play;
using System.Drawing;
using osu.Helpers;
using osu.Online;

namespace osu.GameplayElements.Scoring
{
    internal class Score : bSerializable, IComparable<Score>, ICloneable
    {
        private static string SCORE_SUBMISSION_KEY = @"h89f2-890h2h89b34g-h80g134n90133";
        internal bool AllowSubmission = true;
        internal ushort count100;
        internal ushort count300;
        internal ushort count50;
        internal ushort countGeki;
        internal ushort countKatu;
        internal ushort countMiss;
        internal DateTime date;
        internal Obfuscated<Mods> enabledMods = Mods.None;
        internal PlayModes playMode;
        internal string fileChecksum = string.Empty;
        internal List<Vector2> hpGraph;
        internal bool isOnline;
        internal int maxCombo;
        internal long onlineId;
        internal int onlineRank;
        internal bool pass;
        internal bool exit;
        internal int failTime;
        internal bool perfect;
        internal string playerName;
        internal string scoreChecksumCheck;
        internal string rawGraph;
        internal byte[] rawReplayCompressed;
        internal List<bReplayFrame> replay;
        internal List<int> errorList;
        internal List<int> spinSpeedList;
        internal List<bool> scoringSectionResults = new List<bool>();
        internal List<bScoreFrame> Frames = new List<bScoreFrame>();
        internal ScoreSubmissionStatus submissionStatus;
        internal int totalScore;
        internal double totalScoreD;
        internal User user;
        internal int version = General.VERSION;
        internal double currentHp;
        internal ushort currentCombo;
        internal string WaitingResponse;
        internal MemoryStream screenshot;
        internal int Seed;

        internal Beatmap Beatmap;

        internal virtual bool replayAllowFrameSkip { get { return true; } }

        internal Score()
        {
            replay = new List<bReplayFrame>();
            date = DateTime.Now;
        }

        internal Score(Beatmap beatmap, string playerName)
        {
            if (beatmap != null)
                fileChecksum = beatmap.BeatmapChecksum;
            Beatmap = beatmap;
            this.playerName = playerName;
            date = DateTime.Now;
            pass = false;
            perfect = false;
            replay = new List<bReplayFrame>();
        }

        internal Score(string input, Beatmap beatmap)
        {
            user = new User();
            isOnline = true;

            if (beatmap != null)
            {
                Beatmap = beatmap;
                fileChecksum = beatmap.BeatmapChecksum;
            }

            perfect = false;
            replay = new List<bReplayFrame>();

            string[] line = input.Split('|');

            int i = 0;

            pass = true;
            onlineId = Convert.ToInt64(line[i++]);
            playerName = line[i++];
            user.Name = playerName;
            totalScore = Convert.ToInt32(line[i++]);
            maxCombo = Convert.ToUInt16(line[i++]);
            count50 = Convert.ToUInt16(line[i++]);
            count100 = Convert.ToUInt16(line[i++]);
            count300 = Convert.ToUInt16(line[i++]);
            countMiss = Convert.ToUInt16(line[i++]);
            countKatu = Convert.ToUInt16(line[i++]);
            countGeki = Convert.ToUInt16(line[i++]);
            perfect = line[i++] == @"1";
            enabledMods = (Mods)Convert.ToInt32(line[i++]);
            user.Id = Convert.ToInt32(line[i++]);
            if (line[i].Length > 0)
                onlineRank = Convert.ToInt32(line[i]);
            i++;
            try
            {
                date = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(Int32.Parse(line[i]));
                if (date < new DateTime(1990, 5, 31) /*|| date > DateTime.Now*/)
                    date = DateTime.Now; // when trying to export a corrupted replay file
            }
            catch
            {
                date = DateTime.Now;
            }
        }

        internal Score(bScoreFrame sf, string name)
        {
            pass = sf.pass;
            playerName = name;
            count300 = sf.count300;
            count100 = sf.count100;
            count50 = sf.count50;
            countGeki = sf.countGeki;
            countKatu = sf.countKatu;
            countMiss = sf.countMiss;
            totalScore = sf.totalScore;
            maxCombo = sf.maxCombo;
            perfect = sf.perfect;
            currentHp = sf.currentHp;
            currentCombo = sf.currentCombo;
            enabledMods = ModManager.ModStatus; //todo: wtf check this?
            date = DateTime.Now;
        }

        internal virtual float accuracy
        {
            get { return totalHits > 0 ? (float)(count50 * 50 + count100 * 100 + count300 * 300) / (totalHits * 300) : 0; }
        }

        private string onlineFormatted
        {
            get
            {
                string specialFlag = string.Empty;
                for (int i = 0; i < (int)Player.flag; i++)
                    specialFlag += ' ';
                Player.flag = 0;

                string player = playerName;

#if DEBUG
                if (player != ConfigManager.sUsername)
                    player += @"/" + ConfigManager.sUsername;
#endif

                return
                    String.Format(@"{0}:{1}:{2}:{3}:{4}:{5}:{6}:{7}:{8}:{9}:{10}:{11}:{12}:{13}:{14}:{15}:{16:yyMMddHHmmss}:{17}", fileChecksum,
                    player + ((BanchoClient.Permission & Permissions.Supporter) > 0 ? " " : ""), onlineScoreChecksum, count300, count100, count50,
                    countGeki, countKatu, countMiss, totalScore, maxCombo, perfect, ranking, (int)(Mods)enabledMods, pass, (int)playMode, date.ToUniversalTime(),
                    General.VERSION + specialFlag);
            }
        }

        internal string scoreChecksum
        {
            get { return CryptoHelper.GetMd5String(maxCombo + @"osu" + playerName + fileChecksum + totalScore + ranking); }
        }

        private string onlineScoreChecksum
        {
            get
            {
                string checkString =
                    string.Format(@"chickenmcnuggets{0}o15{1}{2}smustard{3}{4}uu{5}{6}{7}{8}{9}{10}{11}" + (char)0x51 + @"{12}{13}{15}{14:yyMMddHHmmss}{16}", count100 + count300, count50,
                                  countGeki, countKatu, countMiss, fileChecksum, maxCombo, perfect,
                                  playerName, totalScore, ranking, (int)(Mods)enabledMods, pass, (int)playMode, date.ToUniversalTime(), General.VERSION, GameBase.clientHash);
                string md5 = CryptoHelper.GetMd5String(checkString);
                if (md5.Length != 32)
                    throw new Exception(@"checksum failure");
                return md5;
            }
        }

        internal string offlineScoreChecksum
        {
            get
            {
                string checkString =
                    string.Format(@"{0}p{1}o{2}o{3}t{4}a{5}r{6}e{7}y{8}o{9}u{10}{11}{12}", count100 + count300, count50,
                                  countGeki, countKatu, countMiss, fileChecksum, maxCombo, perfect,
                                  playerName, totalScore, ranking, (int)(Mods)enabledMods, pass);
                return CryptoHelper.GetMd5String(checkString);
            }
        }

        internal virtual int totalHits
        {
            get { return count50 + count100 + count300 + countMiss; }
        }

        internal virtual int totalSuccessfulHits
        {
            get { return count50 + count100 + count300; }
        }

        internal string replayFormatted
        {
            get
            {
                StringBuilder replayData = new StringBuilder();

                if (replay != null)
                {
                    bReplayFrame lastF = new bReplayFrame(0, 0, 0, pButtonState.None);
                    foreach (bReplayFrame f in replay)
                    {
                        replayData.AppendFormat(@"{0}|{1}|{2}|{3},",
                                                f.time - lastF.time, (f.mouseX).ToString(GameBase.nfi),
                                                (f.mouseY).ToString(GameBase.nfi),
                                                (int)f.buttonState);
                        lastF = f;
                    }
                }
                replayData.AppendFormat(@"{0}|{1}|{2}|{3},", -12345, 0, 0, Seed);
                return replayData.ToString();
            }
        }

        internal virtual Rankings ranking
        {
            get
            {
                float ratio300 = (float)count300 / totalHits;
                float ratio50 = (float)count50 / totalHits;
                if (!pass) //something has gone wrong.
                    return Rankings.F;
                if (ratio300 == 1)
                    return ModManager.CheckActive(enabledMods, Mods.Hidden) ||
                           ModManager.CheckActive(enabledMods, Mods.Flashlight)
                               ? Rankings.XH
                               : Rankings.X;
                if (ratio300 > 0.9 && ratio50 <= 0.01 && countMiss == 0)
                    return ModManager.CheckActive(enabledMods, Mods.Hidden) ||
                           ModManager.CheckActive(enabledMods, Mods.Flashlight)
                               ? Rankings.SH
                               : Rankings.S;
                if ((ratio300 > 0.8 && countMiss == 0) || (ratio300 > 0.9))
                    return Rankings.A;
                if ((ratio300 > 0.7 && countMiss == 0) || (ratio300 > 0.8))
                    return Rankings.B;
                if (ratio300 > 0.6)
                    return Rankings.C;
                return Rankings.D;
            }
        }

        internal void createStaticsList()
        {
            if (hpGraph == null)
                hpGraph = new List<Vector2>();
            if (errorList == null)
                errorList = new List<int>();
            if (spinSpeedList == null)
                spinSpeedList = new List<int>();
        }

        internal List<double> ErrorStatics(List<int> list)
        {
            if (list == null || list.Count == 0)
                return null;
            List<double> result = new List<double>(4);
            double total = 0, _total = 0, totalAll = 0;
            int count = 0, _count = 0;
            int max = 0, min = int.MaxValue;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] > max)
                    max = list[i];
                if (list[i] < min)
                    min = list[i];
                totalAll += list[i];
                if (list[i] >= 0)
                {
                    total += list[i];
                    count++;
                }
                else
                {
                    _total += list[i];
                    _count++;
                }
            }
            double avarage = totalAll / list.Count;
            double variance = 0;
            for (int i = 0; i < list.Count; i++)
            {
                variance += Math.Pow(list[i] - avarage, 2);
            }
            variance = variance / list.Count;
            result.Add(_count == 0 ? 0 : _total / _count);//0
            result.Add(count == 0 ? 0 : total / count);//1
            result.Add(avarage);//2
            result.Add(variance);//3
            result.Add(Math.Sqrt(variance));//4
            result.Add(max);//5
            result.Add(min);//6
            return result;
        }

        internal string ghostFilename
        {
            get
            {
                return replayFilename.Replace(@".osr", @".osg");
            }
        }

        internal string replayFilename
        {
            get
            {
                long dateConverted;
                try { dateConverted = date.ToFileTimeUtc(); }
                catch { dateConverted = DateTime.Now.ToFileTimeUtc(); }
                return string.Format(@"Data\r\{0}-{1}.osr", fileChecksum, dateConverted);
            }
        }

        internal bool replayLocalPresent
        {
            get { return File.Exists(replayFilename); }
        }

        internal string GetGraphFormatted()
        {
            if (!string.IsNullOrEmpty(rawGraph))
                return rawGraph;

            StringBuilder graphData = new StringBuilder();
            float last = 0;

            if (hpGraph == null)
                return null;

            for (int i = 0; i < hpGraph.Count; i++)
            {
                Vector2 v = hpGraph[i];

                if (v.X - last > 2000 || i == hpGraph.Count - 1 || i == 0)
                {
                    last = v.X;
                    graphData.AppendFormat(@"{0}|{1},", Math.Round(v.X, 2).ToString(GameBase.nfi),
                                           Math.Round(v.Y, 2).ToString(GameBase.nfi));
                }
            }
            hpGraph.Clear();
            hpGraph = null;

            rawGraph = graphData.ToString();
            return rawGraph;
        }

        internal void Submit()
        {
            if (submissionStatus > ScoreSubmissionStatus.NotSubmitted)
                return;
            submissionStatus = ScoreSubmissionStatus.Submitting;

            if (!GameBase.HasLogin)
                return;
            BackgroundWorker b = new BackgroundWorker();
            b.DoWork += submit;
            b.RunWorkerAsync();
        }

        internal void GetReplayData()
        {
            DataNetRequest dnr = new DataNetRequest(string.Format(General.WEB_ROOT + @"/web/osu-getreplay.php?c={0}&m={1}&u={2}&h={3}",
                onlineId, (int)playMode, ConfigManager.sUsername, ConfigManager.sPassword));
            dnr.onFinish += dnr_onFinish;
            NetManager.AddRequest(dnr);
        }

        private void dnr_onFinish(byte[] data, Exception e)
        {
            rawReplayCompressed = data;
            ReadReplayData();
            if (GotReplayData != null)
                GotReplayData(this);
        }

        internal event GotReplayDataHandler GotReplayData;

        internal event GotReplayDataHandler SubmissionComplete;

        private void submit(object sender, DoWorkEventArgs e)
        {
#if ARCADE
            //Don't submit scores yet, no matter what.
            return;
#endif

#if NO_SCORE_SUBMIT
            NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Score_SubmissionDisabled));
            return;
#endif

            GameBase.User.spriteInfo.Text = LocalisationManager.GetString(OsuString.Score_SubmittingScore);
            try
            {
                byte[] zipped = new byte[0];

                if (pass)
                {
                    if (BeatmapManager.Current.onlinePersonalScore != null &&
                        totalScore > BeatmapManager.Current.onlinePersonalScore.totalScore)
                        BeatmapManager.Current.Scores.Clear();
                    //We should re-retrieve online scores no matter what, otherwise when clicking 'retry' the personal score won't be updated.

                    rawReplayCompressed = SevenZipHelper.Compress(new ASCIIEncoding().GetBytes(replayFormatted));

#if DEBUG
                    if (rawReplayCompressed.Length < 100)
                        LoadLocalData();
#endif

                    zipped = rawReplayCompressed;
                }

                FileUploadNetRequest req =
                    new FileUploadNetRequest(General.WEB_ROOT + @"/web/osu-submit-modular.php", zipped, @"replay", @"score");

                string iv = null;

#if SUBMISSION_DEBUG
                File.AppendAllText(@"DEBUG.txt", @"Debug at " + DateTime.Now + @"\n");
#endif

                if (pass)
                {
                    Process[] procs = GameBase.Processes;
                    GameBase.Processes = null;

                    if (procs == null || procs.Length == 0)
                        procs = Process.GetProcesses();
                    StringBuilder b = new StringBuilder();
                    foreach (Process p in procs)
                    {
                        string filename = string.Empty;
                        try
                        {
                            filename = p.MainModule.FileName;
                            FileInfo fi = new FileInfo(filename);
                            if (fi != null)
                                filename = CryptoHelper.GetMd5String(fi.Length.ToString()) + @" " + filename;
                        }
                        catch { }
                        b.AppendLine(filename + @" | " + p.ProcessName + @" (" + p.MainWindowTitle + @")");
                    }

#if SUBMISSION_DEBUG
                    File.AppendAllText(@"DEBUG.txt", @"Running Processes:\n" + b + @"\n\n");
#endif
                    req.request.Items.AddFormField(@"pl", CryptoHelper.EncryptString(b.ToString(), SCORE_SUBMISSION_KEY, ref iv));
                }
                else
                {
                    req.request.Items.AddFormField(@"x", exit ? @"1" : @"0");
                    req.request.Items.AddFormField(@"ft", failTime.ToString());
                }

#if SUBMISSION_DEBUG
                File.AppendAllText(@"DEBUG.txt", @"\n1:" + onlineFormatted + @"\n");
                File.AppendAllText(@"DEBUG.txt", @"\n2:" + GameBase.clientHash + @"\n");
                File.AppendAllText(@"DEBUG.txt", @"\n3:" + iv + @"\n");
#endif

                GameBase.arrived[5]++;

                req.request.Items.AddFormField(@"score", CryptoHelper.EncryptString(onlineFormatted, SCORE_SUBMISSION_KEY, ref iv));
                req.request.Items.AddFormField(@"fs", CryptoHelper.EncryptString(visualSettingsString, SCORE_SUBMISSION_KEY, ref iv));
                req.request.Items.AddFormField(@"c1", GameBase.CreateUniqueId());
                req.request.Items.AddFormField(@"pass", ConfigManager.sPassword);
                req.request.Items.AddFormField(@"s", CryptoHelper.EncryptString(GameBase.clientHash, SCORE_SUBMISSION_KEY, ref iv));

                try
                {
                    if (pass && screenshot != null)
                        req.request.Items.AddSubmitFile(@"i", @"i").AddDataArray(screenshot.ToArray());
                    else
                        req.request.Items.AddFormField(@"i", string.Empty);
                }
                catch { }

                GameBase.ChangeAllowance++;

                screenshot = null;

                req.request.Items.AddFormField(@"iv", iv);

                int retryCount = pass ? 10 : 2;
                int retryDelay = 7500;

                bool didError = false;

                while (retryCount-- > 0)
                {
                    try
                    {
                        WaitingResponse = req.BlockingPerform();

#if SUBMISSION_DEBUG
                        Debug.Print(WaitingResponse);
                        File.AppendAllText(@"DEBUG.txt", @"\nres:" + WaitingResponse + @"\n\n\n\n\n-------------------\n\n\n\n");
#endif

                        if (WaitingResponse.Contains(@"error:"))
                        {
                            switch (WaitingResponse.Replace(@"error: ", string.Empty))
                            {
                                case @"nouser":
                                    NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Score_ErrorNoUser));
                                    break;
                                case @"pass":
                                    NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Score_ErrorPassword));
                                    break;
                                case @"inactive":
                                case @"ban":
                                    NotificationManager.ShowMessage("ERROR: Your account is no longer active.  Please send an email to accounts@ppy.sh if you think this is a mistake.");
                                    break;
                                case @"beatmap":
                                    if (Beatmap != null && Beatmap.SubmissionStatus > SubmissionStatus.Pending)
                                    {
                                        NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Score_ErrorBeatmap));
                                        Beatmap.SubmissionStatus = SubmissionStatus.Unknown;
                                    }
                                    break;
                                case @"disabled":
                                    NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Score_ErrorDisabled));
                                    break;
                                case @"oldver":
                                    NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.Score_ErrorVersion));
                                    GameBase.CheckForUpdates(true);
                                    break;
                                case @"no":
                                    break;

                            }
                            didError = true;
                        }
                        break;
                    }
                    catch
                    {
                    }

                    if (retryDelay >= 60000)
                        NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.Score_SubmissionFailed), (retryDelay / 60000)));
                    Thread.Sleep(retryDelay);
                    retryDelay *= 2;
                }

                if (didError)
                {
                    submissionStatus = ScoreSubmissionStatus.Complete;
                    return;
                }
            }
            catch (Exception ex)
            {
            }

            if (SubmissionComplete != null)
                SubmissionComplete(this);

            submissionStatus = ScoreSubmissionStatus.Complete;

            if (!pass)
                GameBase.User.Refresh();
        }

        internal void ReadReplayData()
        {
            if (rawReplayCompressed == null || rawReplayCompressed.Length == 0) return;

            if (replay == null)
                replay = new List<bReplayFrame>();
            else
                replay.Clear();

            try
            {
                ReadReplayData(new ASCIIEncoding().GetString(SevenZipHelper.Decompress(rawReplayCompressed)));
            }
            catch (Exception)
            {
                replay.Clear();
            }

#if DEBUG
            StreamWriter w = File.CreateText(@"replay.txt");
            foreach (bReplayFrame f in replay)
                w.WriteLine(@"{0} {1} {2} {3} {4} {5} {6}", f.time, f.mouseX, f.mouseY, f.mouseLeft1, f.mouseLeft2,
                            f.mouseRight1, f.mouseRight2);
            w.Close();
#endif
        }

        internal void ReadReplayData(string replayData)
        {
            if (replayData.Length > 0)
            {
                string[] replaylines = replayData.Split(',');

                bReplayFrame lastF;
                if (replay.Count > 0)
                    lastF = replay[replay.Count - 1];
                else
                    lastF = new bReplayFrame(0, 0, 0, pButtonState.None);

                foreach (string replayline in replaylines)
                {
                    if (replayline.Length == 0)
                        continue;

                    string[] data = replayline.Split('|');

                    if (data.Length < 4)
                        continue;
                    if (data[0] == @"-12345")
                    {
                        Seed = Int32.Parse(data[3]);
                        continue;
                    }

                    pButtonState buttons = (pButtonState)Enum.Parse(typeof(pButtonState), data[3]);

                    bReplayFrame newF = new bReplayFrame(Int32.Parse(data[0]) + lastF.time,
                                                         float.Parse(data[1], GameBase.nfi),
                                                         float.Parse(data[2], GameBase.nfi),
                                                         buttons);
                    replay.Add(newF);

                    lastF = newF;
                }
            }
        }

        internal void ReadGraphData(string graphData)
        {
            hpGraph = new List<Vector2>();

            if (graphData.Length > 0)
            {
                string[] graphlines = graphData.Split(',');

                foreach (string graphline in graphlines)
                {
                    if (graphline.Length == 0)
                        continue;

                    string[] data = graphline.Split('|');
                    hpGraph.Add(
                        new Vector2((float)Decimal.Parse(data[0], GameBase.nfi),
                                    (float)Decimal.Parse(data[1], GameBase.nfi)));
                }
            }
        }

        internal void DisposeHpGraph()
        {
            GetGraphFormatted();

            if (hpGraph != null)
            {
                hpGraph.Clear();
                hpGraph = null;
            }
        }

        protected virtual void ReadModSpecificData(SerializationReader sr) { }
        protected virtual void WriteModSpecificData(SerializationWriter sw) { }

        public void ReadFromStream(SerializationReader sr)
        {
            if (sr.BaseStream.Position == 1)
                ReadHeaderFromStream(sr);

            rawReplayCompressed = sr.ReadByteArray();

            if (version >= 20140721)
                onlineId = sr.ReadInt64();
            else if (version >= General.VERSION_FIRST_OSZ2)
                onlineId = sr.ReadInt32();

            ReadModSpecificData(sr);

            if (scoreChecksumCheck != offlineScoreChecksum)
                throw new Exception(@"fucked score");
        }

        public void ReadHeaderFromStream(SerializationReader sr)
        {
            pass = true;
            version = sr.ReadInt32();
            fileChecksum = sr.ReadString();
            playerName = sr.ReadString();
            scoreChecksumCheck = sr.ReadString();
            count300 = sr.ReadUInt16();
            count100 = sr.ReadUInt16();
            count50 = sr.ReadUInt16();
            countGeki = sr.ReadUInt16();
            countKatu = sr.ReadUInt16();
            countMiss = sr.ReadUInt16();
            totalScore = sr.ReadInt32();
            maxCombo = sr.ReadUInt16();
            perfect = sr.ReadBoolean();
            enabledMods = (Mods)sr.ReadInt32();
            rawGraph = sr.ReadString();
            date = sr.ReadDateTime();
        }

        public void WriteToStream(SerializationWriter sw)
        {
            pass = true;
            sw.Write((byte)playMode); //Note that this is written out but not read back in.  ScoreFactory needs it.
            sw.Write(General.VERSION);
            sw.Write(fileChecksum);
            sw.Write(playerName);
            sw.Write(offlineScoreChecksum);
            sw.Write(count300);
            sw.Write(count100);
            sw.Write(count50);
            sw.Write(countGeki);
            sw.Write(countKatu);
            sw.Write(countMiss);
            sw.Write(totalScore);
            sw.Write((ushort)maxCombo);
            sw.Write(perfect);
            sw.Write((int)(Mods)enabledMods);
            sw.Write(GetGraphFormatted());
            sw.Write(date);
            sw.WriteByteArray(rawReplayCompressed);
            sw.Write(onlineId);
            WriteModSpecificData(sw);
        }

        public int CompareTo(Score other)
        {
            int comparison = other.totalScore.CompareTo(totalScore);
            return comparison == 0 ? date.CompareTo(other.date) : comparison;
        }

        internal void Reset()
        {
            currentCombo = 0;
            maxCombo = 0;
            countMiss = 0;
            count50 = 0;
            count100 = 0;
            count300 = 0;
            countGeki = 0;
            countMiss = 0;
            countKatu = 0;
            totalScore = 0;
        }

        internal void ClearReplayData()
        {
            replay = null;
            rawReplayCompressed = null;
            rawGraph = null;
        }

        internal void LoadLocalData()
        {
            if (!replayLocalPresent)
                return;

            Score inScore = ScoreManager.ReadReplayFromFile(replayFilename, false);

            if (inScore != null)
            {
                rawReplayCompressed = inScore.rawReplayCompressed;
                rawGraph = inScore.rawGraph;
            }
        }

        public string visualSettingsString
        {
            get
            {
                Beatmap b = BeatmapManager.Current;
                return string.Format(@"{0}:{1}:{2}:{3}:{4}", b.DimLevel, b.DisableSamples, b.DisableSkin, b.DisableStoryboard, b.BackgroundVisible);
            }
        }

        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        internal void PurgeReplay()
        {
            File.Delete(replayFilename);
            File.Delete(ghostFilename);
        }
    }

    enum ScoreSubmissionStatus
    {
        NotSubmitted,
        Submitting,
        Complete
    }
}