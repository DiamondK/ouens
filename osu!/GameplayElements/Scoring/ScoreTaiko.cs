﻿using osu.GameplayElements.Beatmaps;
using osu_common;
using osu_common.Bancho.Objects;

namespace osu.GameplayElements.Scoring
{
    internal class ScoreTaiko : Score
    {
        internal ScoreTaiko()
        {
            playMode = PlayModes.Taiko;
        }

        internal override float accuracy
        {
            get
            {
                return totalHits > 0 ? (float) (count100*150 + count300*300)/(totalHits*300) : 0;
            }
        }

        internal ScoreTaiko(bScoreFrame frame, string name)
            : base(frame,name)
        {
            playMode = PlayModes.Taiko;
        }

        internal ScoreTaiko(Beatmap beatmap, string name)
            : base(beatmap, name)
        {
            playMode = PlayModes.Taiko;
        }

        internal ScoreTaiko(string input, Beatmap beatmap)
            : base(input, beatmap)
        {
            playMode = PlayModes.Taiko;
        }
    }
}