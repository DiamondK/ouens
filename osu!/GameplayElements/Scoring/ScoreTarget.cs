using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework;
using osu.Configuration;
using osu.GameplayElements.Beatmaps;
using osu.Input.Handlers;
using osu.Online.Drawable;
using osu_common;
using osu_common.Bancho;
using osu_common.Bancho.Objects;
using osu_common.Helpers;
using osu_common.Libraries.NetLib;
using SevenZip.Compression.LZMA;
using System.Threading;
using osu.Graphics.Notifications;
using System.Diagnostics;
using osu.GameModes.Play;
using System.Drawing;
using osu.Helpers;
using osu.Online;

namespace osu.GameplayElements.Scoring
{
    internal class ScoreTarget : Score
    {

        internal float PositionTotal;

        public ScoreTarget(Beatmap beatmap, string playerName) :
            base(beatmap, playerName)
        {
        }

        public ScoreTarget(Score original)
        {
            pass = original.pass;
            version = original.version;
            fileChecksum = original.fileChecksum;
            playerName = original.playerName;
            scoreChecksumCheck = original.scoreChecksumCheck;
            count300 = original.count300;
            count100 = original.count100;
            count50 = original.count50;
            countGeki = original.countGeki;
            countKatu = original.countKatu;
            countMiss = original.countMiss;
            totalScore = original.totalScore;
            maxCombo = original.maxCombo;
            perfect = original.perfect;
            enabledMods = original.enabledMods;
            rawGraph = original.rawGraph;
            date = original.date;
        }
        
        internal override float accuracy
        {
            get
            {
                return PositionTotal / totalHits;
            }
        }

        internal override Rankings ranking
        {
            get
            {
                float ratio50 = (float)count50 / totalHits;

                if (accuracy.CompareTo(1f) == 0 && perfect)
                    return ModManager.CheckActive(enabledMods, Mods.Hidden) ||
                           ModManager.CheckActive(enabledMods, Mods.Flashlight)
                               ? Rankings.XH
                               : Rankings.X;
                if (accuracy > 0.9 && ratio50 <= 0.01 && perfect)
                    return ModManager.CheckActive(enabledMods, Mods.Hidden) ||
                           ModManager.CheckActive(enabledMods, Mods.Flashlight)
                               ? Rankings.SH
                               : Rankings.S;
                if ((accuracy > 0.8 && perfect) || accuracy > 0.9)
                    return Rankings.A;
                if ((accuracy > 0.7 && perfect) || (accuracy > 0.8))
                    return Rankings.B;
                if (accuracy > 0.6)
                    return Rankings.C;
                return Rankings.D;
            }
        }

        protected override void ReadModSpecificData(SerializationReader sr)
        {
            PositionTotal = (float)sr.ReadDouble();
        }

        protected override void WriteModSpecificData(SerializationWriter sw)
        {
            sw.Write((double)PositionTotal);
        }
    }
}