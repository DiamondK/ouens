﻿using osu.GameplayElements.Beatmaps;
using osu_common;
using osu_common.Bancho.Objects;

namespace osu.GameplayElements.Scoring
{
    internal class ScoreFruits : Score
    {
        internal override bool replayAllowFrameSkip { get { return false; } }
        
        internal ScoreFruits()
        {
            playMode = PlayModes.CatchTheBeat;
        }

        internal ScoreFruits(bScoreFrame frame, string name)
            : base(frame,name)
        {
            playMode = PlayModes.CatchTheBeat;
        }

        internal ScoreFruits(Beatmap beatmap, string name)
            : base(beatmap, name)
        {
            playMode = PlayModes.CatchTheBeat;
        }

        internal ScoreFruits(string input, Beatmap beatmap)
            : base(input, beatmap)
        {
            playMode = PlayModes.CatchTheBeat;
        }
        
        internal override int totalHits
        {
            get { return count50 + count100 + count300 + countMiss + countKatu; }
        }

        internal override int totalSuccessfulHits
        {
            get { return count50 + count100 + count300; }
        }

        internal override float accuracy
        {
            get
            {
                if (totalHits == 0) return 1;

                return (float)totalSuccessfulHits / totalHits;
            }
        }

        internal override Rankings ranking
        {
            get
            {
                float acc = accuracy;
                if (!pass) //something has gone wrong.
                    return Rankings.F;
                if (acc == 1)
                    return ModManager.CheckActive(enabledMods, Mods.Hidden) ||
                           ModManager.CheckActive(enabledMods, Mods.Flashlight)
                               ? Rankings.XH
                               : Rankings.X;
                if (acc > 0.98)
                    return ModManager.CheckActive(enabledMods, Mods.Hidden) ||
                           ModManager.CheckActive(enabledMods, Mods.Flashlight)
                               ? Rankings.SH
                               : Rankings.S;
                if (acc > 0.94)
                    return Rankings.A;
                if (acc > 0.9)
                    return Rankings.B;
                if (acc > 0.85)
                    return Rankings.C;
                return Rankings.D;
            }
        }
    }
}