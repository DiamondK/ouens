using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using osu.Input;

namespace osu.GameplayElements.Scoring
{
    internal struct ReplayFrame
    {
        internal ButtonState left;
        internal Vector2 position;
        internal ButtonState right;
        internal int time;

        internal ReplayFrame(int time, Vector2 position, ButtonState left, ButtonState right)
        {
            this.position = position;
            this.left = left;
            this.right = right;
            this.time = time;
        }

        internal ReplayFrame(int time, Vector2 position, pButtonState mouse)
        {
            this.position = position;
            left = ((mouse & pButtonState.Left) > 0 ? ButtonState.Pressed : ButtonState.Released);
            right = ((mouse & pButtonState.Right) > 0 ? ButtonState.Pressed : ButtonState.Released);
            this.time = time;
        }
    }
}