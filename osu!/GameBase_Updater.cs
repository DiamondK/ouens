using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using osu.Configuration;
using osu.Graphics.Notifications;
using osu.Helpers;
using osu_common.Helpers;
using osu_common.Libraries.NetLib;
using osu_common.Updater;

namespace osu
{
    public partial class GameBase : Game
    {
        /// <summary>
        /// The current state of the updater process.
        /// </summary>
        internal static UpdateStates UpdateState;

        /// <summary>
        /// We might poll multiple times for updates. After subsequent polls, we need to remember if we have waiting updates.
        /// </summary>
        private static bool UpdatePendingRestart;

        /// <summary>
        /// Callback for events that need to be fired when update progress occurs.
        /// </summary>
        internal static VoidDelegate UpdateStateChanged;

        /// <summary>
        /// The update must happen. Restart straight away rather than completing update next startup.
        /// </summary>
        private static bool UpdateForceRestart;

        /// <summary>
        /// Last update check time.
        /// </summary>
        private static DateTime UpdateLastCheck;

        internal static void CheckForUpdates(bool forceRestart = false, bool forceRecheck = false)
        {
            switch (UpdateState)
            {
                case UpdateStates.Checking:
                case UpdateStates.Updating:
                    //we are already performing an update; abort.
                    return;
            }

            UpdateForceRestart |= forceRestart;

#if Public
            if (!forceRestart && !forceRecheck && (DateTime.Now - UpdateLastCheck).TotalSeconds < 1800)
                //check once every 30 minutes.
                return;
#endif

            if (GameBase.Tournament)
                return;

#if ARCADE || PublicNoUpdate || DEBUG
            //don't ever update for certain build types.
            return;
#endif

#if Public && !Beta
            if (!string.IsNullOrEmpty(General.SUBVERSION))
            {
                //don't update for now. allow people to test without forceful updates.
                if (DateTime.Now < DateTime.ParseExact(General.VERSION.ToString(), @"yyyyMMdd", nfi).AddDays(7))
                    return;
            }
#endif

            SetUpdateState(UpdateStates.Checking);
            UpdateLastCheck = DateTime.Now;

            Scheduler.Add(delegate
            {
                RunBackgroundThread(delegate
                {
                    while (Mode == OsuModes.Play) Thread.Sleep(1000);

                    CommonUpdater.Check(SetUpdateState, ConfigManager.sReleaseStream.Value,
                        Mode == OsuModes.Menu || Mode == OsuModes.Unknown ? ThreadPriority.Highest : ThreadPriority.BelowNormal);
                });
            });
        }

        internal static string ExeHash(string filename, bool useCache = true)
        {
            if (useCache && ConfigManager.Configuration.ContainsKey(@"h_" + filename))
                return ConfigManager.Configuration[@"h_" + filename].ToString();
            return (ConfigManager.Configuration[@"h_" + filename] = CryptoHelper.GetMd5(filename)).ToString();
        }

        internal static void SetUpdateState(UpdateStates state)
        {
            Scheduler.Add(delegate
            {
                if (state == UpdateState) return;

                UpdateState = state;

                VoidDelegate d = UpdateStateChanged;
                if (d != null) d();

                switch (UpdateState)
                {
                    case UpdateStates.EmergencyFallback:
                        //let's hope we never need this.
                        if (!File.Exists(@"osume.exe"))
                            goto case UpdateStates.Error;

                        Instance.Exit();
                        ProcessStart(@"osume.exe");
                        break;
                    case UpdateStates.Completed:
                        ConfigManager.sUpdateFailCount.Value = 0;

                        //Update has completed and doesn't need a restart.
                        ConfigManager.sUpdatePending.Value = false;
                        ConfigManager.SaveConfig();

#if !Release
                        string lastVersion = ConfigManager.sLastVersion;
                        NotificationManager.ShowMessage(
                            string.Format(LocalisationManager.GetString(OsuString.Update_Complete), General.BUILD_NAME) + '\n' +
                            LocalisationManager.GetString(OsuString.GameBase_Updater_Changelog),
                            Color.Pink, 10000, delegate { GameBase.ProcessStart(@"http://osu.ppy.sh/p/changelog?v=" + General.BUILD_NAME + @"&l=" + lastVersion); });
#else
                        NotificationManager.ShowMessage(string.Format(LocalisationManager.GetString(OsuString.Update_Complete), General.BUILD_NAME), Color.Pink, 10000);
#endif
                        break;
                    case UpdateStates.Error:
                        ConfigManager.sUpdatePending.Value = false;

                        //Update errored at some point. Write out debug and force a restart and osume update.
                        if (CommonUpdater.LastError != null)
                        {
                            RunBackgroundThread(delegate
                            {
                                ErrorSubmission.Submit(new OsuError(CommonUpdater.LastError)
                                {
                                    Feedback = @"update error",
                                    ILTrace = CommonUpdater.LastErrorExtraInformation ?? string.Empty
                                });

                                CommonUpdater.ResetError();
                            });
                        }

                        ConfigManager.ResetHashes();

                        ConfigManager.sUpdateFailCount.Value++;
                        break;
                    case UpdateStates.NeedsRestart:

                        //the update could have already moved the files into their new place, so we want to make sure we have reloaded the master config file.
                        ConfigManager.ReloadHashCache();

                        ConfigManager.sUpdateFailCount.Value = 0;

                        bool isNewUpdate = !ConfigManager.sUpdatePending.Value;

                        ConfigManager.sUpdatePending.Value = true;

                        //Update completed but needs a restart. We either want to force a restart or just wait for the next user-triggered restart.
                        if (UpdateForceRestart)
                            CompleteUpdate();
                        else if (Mode != OsuModes.Menu && isNewUpdate)
                            NotificationManager.ShowMessage(LocalisationManager.GetString(OsuString.General_NewVersion), Color.Pink, 10000);

                        UpdatePendingRestart = true;
                        break;
                    case UpdateStates.NoUpdate:
                        ConfigManager.sUpdateFailCount.Value = 0;
                        break;
                }
            });
        }

        internal static void CompleteUpdate(bool instant = false)
        {
            if (!instant && UpdateState != UpdateStates.NeedsRestart && UpdateState != UpdateStates.Error)
                return;

            switch (Mode)
            {
                default:
                    //if we aren't in a mostly-idle mode, wait before completing the restart.
                    Scheduler.AddDelayed(delegate { CompleteUpdate(); }, 1000);
                    return;
                case OsuModes.Menu:
                case OsuModes.SelectPlay:
                case OsuModes.SelectEdit:
                    break;
            }

            if (!instant) NotificationManager.ShowMessageMassive(LocalisationManager.GetString(OsuString.Update_Restart), 120000);

            GameBase.Scheduler.AddDelayed(delegate
            {
                if (UpdateState == UpdateStates.NeedsRestart || UpdateState == UpdateStates.Completed)
                    Restart();
                else
                    //an error may have occurred; run the standard updater.
                    GameBase.ChangeMode(OsuModes.Update);
            }, instant ? 0 : 6000);
        }
    }
}
