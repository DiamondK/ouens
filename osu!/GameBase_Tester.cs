﻿#if DEBUG
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using osu.GameplayElements.Beatmaps;
using osu_common;
using osu.Graphics.Notifications;
using osu.GameModes.Play;
using System.Threading;
using osu.GameplayElements.Scoring;
using osu.Configuration;

namespace osu
{

    public partial class GameBase : Game
    {
        public static bool PREDEFINED_TEST        = false;

        const OsuModes INITIAL_MODE        = OsuModes.RankingTeam;
        const PlayModes INITIAL_PLAY_MODE  = PlayModes.Osu;
        const bool USE_LAST_PLAYED_BEATMAP = true;

        //play mode specifics
        const bool AUTOMATIC_SKIP          = true;
        const bool AUTOPLAY                = true;

        private void InitializeTester()
        {
            if (!PREDEFINED_TEST)
                return;

            if (BeatmapManager.Beatmaps.Count > 0)
            {
                if (USE_LAST_PLAYED_BEATMAP)
                {
                    List<Beatmap> temp = new List<Beatmap>(BeatmapManager.Beatmaps);
                    temp.Sort((a, b) => { return a.DateLastPlayed.CompareTo(b.DateLastPlayed); });
                    BeatmapManager.Current = temp[temp.Count - 1];
                }
            }

            switch (INITIAL_MODE)
            {
                case OsuModes.Play:
                    if (BeatmapManager.Current == null)
                    {
                        NotificationManager.ShowMessage("Couldn't start in specified test mode because no beatmaps were available.");
                        return;
                    }

                    if (AUTOMATIC_SKIP)
                    {
                        GameBase.RunBackgroundThread(delegate
                        {
                            while (true)
                            {
                                if (Player.Instance != null && Player.Instance.Status != PlayerStatus.Busy)
                                {
                                    GameBase.Scheduler.Add(delegate { Player.Instance.DoSkip(); });
                                    if (Player.HasSkipped)
                                        break;
                                }

                                Thread.Sleep(200);
                            }
                        });
                    }

                    if (AUTOPLAY)
                        ModManager.ModStatus |= Mods.Autoplay;

                    break;
            }

            GameQueuedState = INITIAL_MODE;
            Player.Mode = INITIAL_PLAY_MODE;
        }
    }
}
#endif