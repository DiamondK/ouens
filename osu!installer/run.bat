@echo off

mkdir output
del /Q output\*

"%HOME%\Dropbox\Apps\wix\candle.exe" -out "output\setup.wixobj" "setup.wxs"
"%HOME%\Dropbox\Apps\wix\candle.exe" -out "output\msifactui.wixobj" "msifactui.wxs"
"%HOME%\Dropbox\Apps\wix\candle.exe" -out "output\Common.wixobj" "Common.wxs"
"%HOME%\Dropbox\Apps\wix\candle.exe" -out "output\ErrorText.wixobj" "ErrorText.wxs"
"%HOME%\Dropbox\Apps\wix\candle.exe" -ext WixUtilExtension -out "output\ProgressText.wixobj" "ProgressText.wxs"
"%HOME%\Dropbox\Apps\wix\light.exe"  -ext WixUtilExtension -loc "English-US.wxl" -b "..\osu!" -out "output\setup.msi" "output\setup.wixobj" "output\msifactui.wixobj" "output\Common.wixobj" "output\ErrorText.wixobj" "output\ProgressText.wixobj"
if ERRORLEVEL 1 goto error

::sign the msi...
"%HOME%\Dropbox\Apps\signtool.exe" sign /f "%HOME%\Dropbox\Important Stuffs\deanherbert.pfx" /p osuismyfriend /t http://timestamp.comodoca.com/authenticode "output\setup.msi"

goto end

:error
echo - Build failed!
exit /B 1

:end

"%HOME%\Dropbox\Apps\dotNetInstaller\InstallerLinker.exe" /c:dotnetinstall.xml /t:"%HOME%\Dropbox\Apps\dotNetInstaller\dotNetInstaller.exe" /i:icon.ico /o:output\osu!install.exe

"%HOME%\Dropbox\Apps\signtool.exe" sign /f "%HOME%\Dropbox\Important Stuffs\deanherbert.pfx" /p osuismyfriend /t http://timestamp.comodoca.com/authenticode "output\osu!install.exe"

::upload the result
::sftpc -profile=..\osu!.tlp -cmd="put output\osu!install.exe /var/www/release/osu!install.exe_ -o"
::sexec -profile=..\osu!.tlp -cmd="mv /var/www/release/osu\!install.exe_ /var/www/release/osu\!install.exe"