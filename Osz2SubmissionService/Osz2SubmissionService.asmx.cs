﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.IO;
using osu_common.Libraries.Osz2;
using System.Text;
using osu_common.Helpers;
using ICSharpCode.SharpZipLib.Zip;
using osu_common.Libraries;
using FileInfo = osu_common.Libraries.Osz2.FileInfo;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using Amazon.S3.Util;
using Amazon.S3.Transfer;

namespace Osz2SubmissionService
{


    /// <summary>
    /// This beatmapsubmission webservice allows other webservices with no .net access to update osz2 mappackages.
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Osz2SubmissionService : System.Web.Services.WebService
    {
        private const long PartSize = 1024 * 1024; // 1 MB

        enum UpdateResponseCode
        {
            UpdateSuccessful = 0,
            UnknownError = -1,
            FileDoesNotExist = -2,
            Osz2Corrupted = -3,
            InvalidMetaDataKey = -4,
            CopyingFailed = -5,
            PartIsOutOfBounds = -6,
            FailedToReplace = -7
        }

        public static bool s3put(string filename, Stream stream)
        {
            log("Putting s3://{0} ({1:0,0})", filename, stream.Length);

            try
            {
                using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client(@"AKIAJ2UM2AGXBMIPF5EQ", @"UKtB97UANKXPhRI3edldqMcDAEAx7sgtsBImDK1B", RegionEndpoint.USWest2))
                {
                    TransferUtility tu = new TransferUtility(client);
                    tu.Upload(stream, @"osu-central", filename);
                }
            }
            catch (Exception e)
            {
                log(e);
                return false;
            }
            return true;
        }

        public static bool s3putFile(string key, string filelocation, bool deleteOnCompletion = false)
        {
            bool success = false;
            using (FileStream stream = File.OpenRead(filelocation))
                success = s3put(key, stream);
            if (deleteOnCompletion) File.Delete(filelocation);
            return success;
        }

        public static byte[] s3get(string filename, bool removeOnFetch = false)
        {
            log("Retrieving s3://{0}", filename);

            try
            {
                using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client(@"AKIAJ2UM2AGXBMIPF5EQ", @"UKtB97UANKXPhRI3edldqMcDAEAx7sgtsBImDK1B", RegionEndpoint.USWest2))
                {
                    TransferUtility tu = new TransferUtility(client);

                    using (MemoryStream ms = new MemoryStream())
                    using (Stream res = tu.OpenStream(@"osu-central", filename))
                    {
                        byte[] buffer = new byte[65536];
                        int bytesRead = -1;
                        while ((bytesRead = res.Read(buffer, 0, buffer.Length)) > 0)
                            ms.Write(buffer, 0, bytesRead);
                        if (removeOnFetch) client.DeleteObject(new DeleteObjectRequest() { BucketName = @"osu-central", Key = filename });
                        return ms.ToArray();
                    }
                }
            }
            catch (Exception e)
            {
                log(e);
                return null;
            }
        }

        private static void log(Exception e)
        {
            log(e.ToString());
        }

        private static void log(string format, params object[] args)
        {
            File.AppendAllText("log.txt", DateTime.Now.ToString() + " " + string.Format(format, args) + "\n");
        }

        /// <summary>
        /// get from s3 and store to a temporary file for futher processing
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string s3getToTemp(string filename, bool removeOnFetch = false)
        {
            string path = Path.GetTempPath() + filename;

            if (File.Exists(path) && new System.IO.FileInfo(path).Length > 0 && !removeOnFetch)
                return path;

            string directory = Path.GetDirectoryName(path);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            log("Need temporary file {0}", filename);
            byte[] bytes = s3get(filename, removeOnFetch);

            if (bytes == null) return null;

            try
            {
                File.WriteAllBytes(path, bytes);
            }
            catch (Exception e)
            {
                File.Delete(path);
                log(e);
            }

            return path;
        }

        public static void s3purgeTemp(string filename)
        {
            string path = Path.GetTempPath() + filename;
            File.Delete(path);
        }

        /// <summary>
        /// A relayed action on a loaded mappackage. The caller of the action is responsible for any
        /// exception the action might cause.
        /// </summary>
        /// <param name="M">The successfully loaded mappackage (non null)</param>
        /// <returns>When processing fails functionality-wise a custom update-responce can be returned.
        ///          The standard response should be UpdateSuccessful</returns>
        delegate UpdateResponseCode MapPackageAction(MapPackage M);


        /// <summary>
        /// Updates a difficulty of a beatmapset(Osz2 mappackage) by replacing a Osu file.
        /// </summary>
        /// <param name="BeatmapSetID">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <param name="OsuRawFiledata">The filecontents of the difficulty(osu)</param>
        /// <param name="OsuFilename">The full name of the difficulty filename including the extension</param>
        /// <returns>A UpdateResponseCode cast to an int</returns>
        [WebMethod]
        public int UpdateOsuFile(string Package, byte[] Key, byte[] OsuRawFiledata, string OsuFilename)
        {
            return (int)DoMapPackageActionSafe(
                (Osz2Beatmap) =>
                {
                    //Osz2Beatmap.RemoveFile(OsuFilename);
                    Osz2Beatmap.AddFile(OsuFilename, OsuRawFiledata, DateTime.UtcNow, DateTime.UtcNow);
                    return UpdateResponseCode.UpdateSuccessful;
                }, Package, Key, true, false
            );
        }

        /// <summary>
        /// Updates the metadata of a beatmapset(Osz2 mappackage).
        /// </summary>
        /// <param name="BeatmapSetID">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <param name="Metadata">All the metadata that needs to be changed stored in a semi-assoicative array,
        /// all the keys are in the odd indices, and the values are in the even indices.
        /// The key values must be parsable as a MapMetaType</param>
        /// <returns>A UpdateResponseCode cast to an int</returns>
        [WebMethod]
        public int UpdateMetadata(string Package, byte[] Key, string[] Metadata)
        {
            if (Metadata.Length % 2 > 0 || Metadata.Length < 2)
                return (int)UpdateResponseCode.InvalidMetaDataKey;

            return (int)DoMapPackageActionSafe(
                (M) =>
                {
                    for (int i = 0; i < Metadata.Length; i += 2)
                        if (Enum.IsDefined(typeof(MapMetaType), Metadata[i]))
                            M.AddMetadata((MapMetaType)Enum.Parse(typeof(MapMetaType), Metadata[i]), Metadata[i + 1]);
                        else
                            return UpdateResponseCode.InvalidMetaDataKey;
                    return UpdateResponseCode.UpdateSuccessful;

                }, Package, Key, true, false
            );
        }

        /// <summary>
        ///  Returns both the bodyhash and the metadata hash by reading the osz2 mappackage.
        /// </summary>
        /// <param name="Package">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <returns>Returns the bodyhash (hashes[0]) and the metadatahash(hashes[1]). </returns>
        [WebMethod]
        public byte[][] GetHashes(string Package, byte[] Key)
        {
            byte[][] hashes = new byte[2][];
            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {
                hashes[0] = M.hash_body;
                hashes[1] = M.hash_meta;
                return UpdateResponseCode.UpdateSuccessful;
            }, Package, Key, false, true);

            if (code != UpdateResponseCode.UpdateSuccessful)
                return null;

            return hashes;

        }

        [WebMethod]
        public void PurgeCache(string Package)
        {
            s3purgeTemp("osz2/" + Package);
        }

        /// <summary>
        /// Patches the mappackage by applyng a bzpatch located in the Osz2PatchPath on the
        /// package located in the Osz2Path. When patching fails the original file remains.
        /// </summary>
        /// <param name="Package">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <returns>The UpdateResponseCode of the custom defined action when no exception has occured
        ///  while loading the beatmap(osz2) file, or a failed UpdateResponseCode when there has.</returns>
        [WebMethod]
        public int PatchMappackageWithKey(string Package, byte[] Key = null)
        {
            string Osz2PatchFilename = s3getToTemp("working/" + Package.Replace(".osz2", ".patch"), true);
            if (Osz2PatchFilename == null) return (int)UpdateResponseCode.FileDoesNotExist;

            string Osz2Filename = s3getToTemp("osz2/" + Package);
            if (Osz2Filename == null) return (int)UpdateResponseCode.FileDoesNotExist;

            string Osz2FilenameTemp = Osz2Filename + ".temp";

            if (!MapPackage.UpdateFromPatch(Osz2Filename, Osz2PatchFilename, Osz2FilenameTemp))
            {
                File.Delete(Osz2PatchFilename);
                File.Delete(Osz2FilenameTemp);
                return (int)UpdateResponseCode.UnknownError;
            }

            File.Delete(Osz2Filename);
            File.Move(Osz2FilenameTemp, Osz2Filename);

            //check if updated mappackage is valid
            UpdateResponseCode code = DoMapPackageActionSafe((M) => { return UpdateResponseCode.UpdateSuccessful; }, Osz2Filename, Key, false, false);
            if (code != UpdateResponseCode.UpdateSuccessful)
                return (int)code;

            //copy patched file back to the original
            try
            {
                //when replacing failed it will automaticaly be restored to the previous version
                s3putFile("osz2/" + Package, Osz2Filename);
            }
            catch (Exception e)
            {
                log(e);
                //todo add logger here
                code = UpdateResponseCode.CopyingFailed;
            }
            finally
            {
                File.Delete(Osz2PatchFilename);
            }

            return (int)code;
        }

        /// <summary>
        /// Get the contents of a beatmap (difficulty) as a string.
        /// </summary>
        /// <param name="Package">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <param name="BeatmapID">"The ID of the beatmap (difficulty)"</param>
        /// <returns>The contents of a beatmap, or null if file does not exist</returns>
        [WebMethod]
        public string GetMapContents(string Package, byte[] Key, int BeatmapID)
        {
            string contents = null;
            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {
                string filename = M.GetMapByID(BeatmapID);
                if (filename == string.Empty)
                    return UpdateResponseCode.FileDoesNotExist;

                using (StreamReader reader = new StreamReader(M.GetFile(filename)))
                    contents = reader.ReadToEnd();

                return UpdateResponseCode.UpdateSuccessful;
            }, Package, Key, false, false);

            if (code != UpdateResponseCode.UpdateSuccessful)
                return Convert.ToString((int)code);

            return contents;
        }



        /// <summary>
        /// Get the contents of a beatmap (difficulty) as a string by filename.
        /// </summary>
        /// <param name="Package">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <param name="Filename">"The filename of the beatmap (difficulty)"</param>
        /// <returns>The contents of a beatmap, or null if file does not exist</returns>
        [WebMethod]
        public string GetMapContentsByName(string Package, byte[] Key, string Filename)
        {
            string contents = null;
            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {

                if (!M.FileExists(Filename))
                    return UpdateResponseCode.FileDoesNotExist;

                using (StreamReader reader = new StreamReader(M.GetFile(Filename)))
                    contents = reader.ReadToEnd();

                return UpdateResponseCode.UpdateSuccessful;
            }, Package, Key, false, false);

            if (code != UpdateResponseCode.UpdateSuccessful)
                return Convert.ToString((int)code);

            return contents;
        }

        [WebMethod]
        public string ConvertToOsz(string Package, byte[] Key)
        {
            string tempFolder = Path.GetTempPath() + Path.GetFileNameWithoutExtension(Package) + "-" + DateTime.Now.Ticks;

            log("Converting {0} from osz2 to osz", Package);

            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {
                try
                {
                    List<FileInfo> files = M.GetFileInfo();
                    foreach (FileInfo fileInfo in files)
                    {
                        string file = fileInfo.Filename;

                        string fullPath = Path.Combine(tempFolder, file);
                        string dirName = Path.GetDirectoryName(fullPath);

                        if (!Directory.Exists(dirName))
                            Directory.CreateDirectory(dirName);

                        DateTime fileOverrideLastWrite = File.GetLastWriteTimeUtc(fullPath);
                        System.IO.FileInfo curFileInfo = new System.IO.FileInfo(fullPath);

                        long fileOverriddenFileTime = 0;
                        long fileBaseFileTime = 0;

                        try
                        {
                            fileOverriddenFileTime = fileOverrideLastWrite.ToFileTime();
                            fileBaseFileTime = fileInfo.ModifiedTime.ToFileTime();
                        }
                        catch {

                        }

                        //within 4 second accuracy
                        if (Math.Abs(fileBaseFileTime - fileOverriddenFileTime) < 200000000 && curFileInfo.Length == fileInfo.Length - 4)
                            continue;

                        using (Stream fileP = M.GetFile(file))
                        using (FileStream fileC = File.Create(fullPath, (int)fileP.Length, FileOptions.WriteThrough))
                        {
                            byte[] buffer = new byte[fileP.Length];
                            fileP.Read(buffer, 0, buffer.Length);
                            fileC.Write(buffer, 0, buffer.Length);
                            fileC.Flush();
                        }

                        try
                        {
                            File.SetCreationTimeUtc(fullPath, fileInfo.CreationTime);
                            File.SetLastWriteTimeUtc(fullPath, fileInfo.ModifiedTime);
                        }
                        catch (Exception e)
                        {
                            log(e);
                            //may fail on non-existent timestamps
                        }
                    }
                }
                catch (Exception e)
                {
                    log(e);
                }

                return UpdateResponseCode.UpdateSuccessful;
            }, Package, Key, false, false);

            string oszFilename = Package.Replace(".osz2", ".osz");

            string oszTempPath = Path.Combine(Path.GetTempPath(), oszFilename);

            File.Delete(oszTempPath);

            ZipConstants.DefaultCodePage = 932;
            FastZip fz = new FastZip();
            fz.CreateZip(oszTempPath, tempFolder, true, ".*");

            try { Directory.Delete(tempFolder, true); }
            catch (Exception e)
            {
                log(e);
            }

            s3putFile("osz/" + oszFilename, oszTempPath, true);

            return oszFilename;
        }

        /// <summary>
        /// Extracts a file from the osz2 package to the osz2-patches folder.
        /// </summary>
        /// <param name="BeatmapSetID">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <param name="Filename">A valid filename that exists in the osz2 package</param>
        /// <returns>Returns the generated filename of the extracted file.
        /// Returns an UpdateResponseCode converted to a string if not exists/returns>
        [WebMethod]
        public string ExtractFile(string Package, byte[] Key, string Filename)
        {
            string newFile = String.Empty;
            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {
                if (!M.FileExists(Filename))
                    return UpdateResponseCode.FileDoesNotExist;

                int bufferSize = 1048576;
                int count;

                newFile = Package + "_" + Filename;

                using (Stream file = M.GetFile(Filename))
                using (FileStream output = File.Open(newFile, FileMode.Create))
                {
                    byte[] buffer = new byte[bufferSize];
                    while ((count = file.Read(buffer, 0, bufferSize)) > 0)
                        output.Write(buffer, 0, count);

                    output.Close();
                }

                return UpdateResponseCode.UpdateSuccessful;
            }, Package, Key, false, false);

            if (code != UpdateResponseCode.UpdateSuccessful)
                return Convert.ToString((int)code);

            string filename = Path.GetFileName(newFile);
            s3putFile(@"working/" + filename, newFile, true);

            return filename;
        }



        /// <summary>
        /// Gets the md5 hash of a osu beatmap file.
        /// </summary>
        /// <param name="BeatmapSetID">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <param name="BeatmapID">"The ID of the beatmap (difficulty)"</param>
        /// <returns>The md5 hash of the beatmap, or null if the file does not exist</returns>
        [WebMethod]
        public byte[] GetMapMd5(string Package, byte[] Key, int BeatmapID)
        {
            byte[] hash = null;
            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {
                string filename = M.GetMapByID(BeatmapID);
                if (filename == string.Empty)
                    return UpdateResponseCode.FileDoesNotExist;

                hash = M.GetMapHash(filename);
                return UpdateResponseCode.UpdateSuccessful;
            }, Package, Key, false, false);

            if (code != UpdateResponseCode.UpdateSuccessful)
                return null;

            return hash;
        }

        [WebMethod]
        public string GetPackageMd5(string Package)
        {
            string file = s3getToTemp("osz2/" + Package);
            string md5 = CryptoHelper.GetMd5(file);
            return md5;
        }

        /// <summary>
        /// Gets the filename from a beatmap(difficulty) by ID
        /// </summary>
        /// <param name="Package">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <param name="BeatmapID">"The ID of the beatmap (difficulty)"</param>
        /// <returns>The filename, or null if the file does not exist</returns>
        [WebMethod]
        public string GetMapName(string Package, byte[] Key, int BeatmapID)
        {
            string filename = null;
            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {
                filename = M.GetMapByID(BeatmapID);
                if (filename == string.Empty)
                    return UpdateResponseCode.FileDoesNotExist;

                return UpdateResponseCode.UpdateSuccessful;
            }, Package, Key, false, true);

            if (code != UpdateResponseCode.UpdateSuccessful)
                return null;

            return filename;
        }

        /// <summary>
        /// Checks whether the beatmap has video enabled
        /// </summary>
        /// <param name="Package">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <returns>The filename, or null if the file does not exist</returns>
        [WebMethod]
        public bool HasVideoData(string Package, byte[] Key)
        {
            bool hasVideo = false;
            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {
                int offset, length;
                M.getVideoOffset(out offset, out length);
                hasVideo = offset != -1;

                return UpdateResponseCode.UpdateSuccessful;
            }, Package, Key, false, false);

            return hasVideo;

        }

        /// <summary>
        /// Returns the cached md5 hash of a file from the osz2 package.
        /// </summary>
        /// <param name="BeatmapSetID">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <param name="Filename">A valid filename that exists in the osz2 package</param>
        /// <returns>Returns the cached md5 hash of a file from the osz2 package,
        ///          or it returns null if no such file exists or isn't cached yet./returns>
        [WebMethod]
        public byte[] GetCachedFileMd5(string Package, byte[] key, string Filename)
        {
            byte[] hash = null;
            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {
                hash = M.GetCachedFileHash(Filename);
                return UpdateResponseCode.UpdateSuccessful;

            }, Package, key, false, false);

            return hash;
        }

        /// <summary>
        /// Returns the list of all fileinfo headers for all the files contained in the mappackage.
        /// </summary>
        /// <param name="Package">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <returns>Return the list of all fileinfo headers contained in the mappackage by encoding it into a string:
        ///          Fileinfo will be written in a comma seperated list as: filename, offset, length, md5(string), dateCreated, dateModified
        ///          Multiple fileinfo headers will be seperated by the '|' char.
        ///          New: Also returns DataOffset following after a newline</returns>
        [WebMethod]
        public string GetFileInfoList(string Package, byte[] Key)
        {
            StringBuilder fileInfoList = new StringBuilder(20 * (15 + 8 + 6 + 16 * 2));
            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {
                List<FileInfo> list = M.GetFileInfo();
                foreach (FileInfo info in list)
                {
                    fileInfoList.AppendFormat("{0}:{1}:{2}:{3}:{4}:{5}|", info.Filename, info.Offset,
                        info.Length, BitConverter.ToString(info.Hash).Replace("-", ""), info.CreationTime.ToBinary(),
                        info.ModifiedTime.ToBinary());
                }
                fileInfoList.Remove(fileInfoList.Length - 1, 1);
                fileInfoList.AppendLine().Append(Convert.ToString(M.DataOffset));

                return UpdateResponseCode.UpdateSuccessful;

            }, Package, Key, false, false);

            if (fileInfoList.Length == 0)
                return null;

            return fileInfoList.ToString();
        }

        /// <summary>
        /// Returns the raw (encrypted) fileheader of the osz2-package.
        /// </summary>
        /// <param name="Package">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <returns> Returns the raw fileheader of the osz2-package
        /// or returns null if retrieving the header failed.</returns>
        [WebMethod]
        public byte[] GetRawOsz2Header(string Package, byte[] Key)
        {
            byte[] header = null;
            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {
                header = M.getRawHeader();
                return UpdateResponseCode.UpdateSuccessful;

            }, Package, Key, false, false);

            return header;
        }

        /// <summary>
        /// Returns the contents (or part of it) of the file as a byte array.
        /// </summary>
        /// <param name="Package">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <param name="Filename">A valid filename that exists in the osz2 package</param>
        /// <param name="Part">The zero-based-part-index of the file.
        /// When the file is larger than 1 MB the file will be divided in multiple 1MB-parts</param>
        /// <returns>Returns the complete file if smaller than 1 MB, or a specific part of the file when larger.
        ///          Returns null if the requested file does not exist. /returns>
        [WebMethod]
        public byte[] GetFileContents(string Package, byte[] Key, string Filename, int Part, bool Raw)
        {
            byte[] chunk = null;
            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {
                using (Stream file = M.GetFile(Filename, Raw))
                {
                    int partCount = (int)((file.Length - 1) / PartSize) + 1;
                    if (Part >= partCount)
                        return UpdateResponseCode.PartIsOutOfBounds;

                    int partLength = (int)Math.Min((file.Length - Part * PartSize), PartSize);
                    chunk = new byte[partLength];
                    file.Position += Part * PartSize;
                    file.Read(chunk, 0, (int)partLength);

                }
                return UpdateResponseCode.UpdateSuccessful;

            }, Package, Key, false, false);

            return chunk;
        }


        /// <summary>
        /// Large files are divided by 1MB parts, this will return the part-count of the requested file
        /// </summary>
        /// <param name="Package">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <param name="Filename">A valid filename that exists in the osz2 package</param>
        /// <returns>The amount of parts the file is divided.
        /// Will return a failed UpdateResponseCode when an error occured</returns>
        [WebMethod]
        public int GetFilePartCount(string Package, byte[] Key, string Filename)
        {
            int count = 0;
            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {
                using (Stream file = M.GetFile(Filename, false))
                    count = (int)((file.Length - 1) / PartSize) + 1;

                return UpdateResponseCode.UpdateSuccessful;

            }, Package, Key, false, false);

            if (code != UpdateResponseCode.UpdateSuccessful)
                return (int)code;

            return count;
        }


        /// <summary>
        /// Returns video-info: name, size, offset
        /// </summary>
        /// <param name="BeatmapSetID">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <returns> Returns the following values: name, size, offset
        ///           in a comma-seperated list.</returns>
        [WebMethod]
        public string GetVideoInfo(string Package, byte[] Key)
        {

            string videoInfo = null;
            UpdateResponseCode code = DoMapPackageActionSafe(
            (M) =>
            {
                int offset, length;
                string name = String.Empty;
                M.getVideoOffset(out offset, out length);
                if (offset == -1)
                    return UpdateResponseCode.UpdateSuccessful;

                name = M.GetFileInfo().Find((F) => MapPackage.IsVideoFile(F.Filename)).Filename;
                videoInfo = String.Format("{0},{1},{2}", name, length, offset);

                return UpdateResponseCode.UpdateSuccessful;
            }, Package, Key, false, false);

            return videoInfo;
        }

        /// <summary>
        /// Creates a osz2 version of a osz-beatmap with a valid BeatmapSetID.
        /// Keeps the original osz beatmap intact, but does overwrite the osz2 version if any exists.
        /// </summary>
        /// <param name="BeatmapSetID">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <param name="Artist"></param>
        /// <param name="Creator"></param>
        /// <param name="Source"></param>
        /// <param name="TitleUnicode"></param>
        /// <param name="ArtistUnicode"></param>
        /// <param name="Tags"></param>
        /// <param name="OszFilename">The full path to the osz file. Should not be a no-video version.</param>
        /// <returns>A UpdateResponseCode cast to an int.</returns>
        [WebMethod]
        public int CreateOsz2Package(int BeatmapSetID, byte[] Key, string Artist, string Creator, string Source, string Title,
                                     string TitleUnicode, string ArtistUnicode, string Tags, string OszFilename)
        {
            return 0;

            //UpdateResponseCode code = UpdateResponseCode.UpdateSuccessful;

            //if (!File.Exists(OszFilename))
            //    return (int)UpdateResponseCode.FileDoesNotExist;

            //MapPackage newPackage = null;
            //string osz2Filename = Path.Combine(Osz2Path, String.Format("{0}.osz2", BeatmapSetID));
            //string oszExtractedTemp = Path.Combine(Osz2PatchPath, String.Format("{0}-extracted", BeatmapSetID));
            //try
            //{

            //    Directory.CreateDirectory(oszExtractedTemp);
            //    FastZip fz = new FastZip();
            //    fz.ExtractZip(OszFilename, oszExtractedTemp, ".*");

            //    if (File.Exists(osz2Filename))
            //        File.Delete(osz2Filename);

            //    newPackage = new MapPackage(osz2Filename, true);
            //    newPackage.AddDirectory(oszExtractedTemp, true);
            //    newPackage.AddMetadata(MapMetaType.BeatmapSetID, Convert.ToString(BeatmapSetID));
            //    newPackage.AddMetadata(MapMetaType.Artist, Artist);
            //    newPackage.AddMetadata(MapMetaType.Creator, Creator);
            //    newPackage.AddMetadata(MapMetaType.Source, Source);
            //    newPackage.AddMetadata(MapMetaType.Title, Title);
            //    newPackage.AddMetadata(MapMetaType.TitleUnicode, TitleUnicode);
            //    newPackage.AddMetadata(MapMetaType.ArtistUnicode, ArtistUnicode);
            //    newPackage.AddMetadata(MapMetaType.Tags, Tags);
            //    newPackage.Save();

            //}
            //catch (Exception e)
            //{
            //    if (newPackage != null)
            //    {
            //        newPackage.Dispose();
            //        newPackage = null;
            //    }

            //    if (File.Exists(osz2Filename))
            //        File.Delete(osz2Filename);

            //    code = UpdateResponseCode.FailedToReplace;
            //}
            //finally
            //{
            //    if (Directory.Exists(oszExtractedTemp))
            //        Directory.Delete(oszExtractedTemp, true);

            //    if (newPackage != null)
            //        newPackage.Dispose();
            //}

            //return (int)code;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="SourcePackage">The filename of the osz2 package that needs to be re-encrypted</param>
        /// <param name="TargetPackage">Target osz2 package which will be encrypted with the new key.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <returns>An UpdateResponseCode cast to an int.</returns>
        [WebMethod]
        public int ChangeKey(string SourcePackage, string TargetPackage, byte[] OldKey, byte[] NewKey)
        {
            log("Changing key for {0} / {1}", SourcePackage, TargetPackage);

            string osz2FilenameOld = s3getToTemp("osz2/" + SourcePackage);
            string osz2FilenameNew = osz2FilenameOld + ".new";

            try
            {
                //ensure an existing new file doesn't existing exist.
                File.Delete(osz2FilenameNew);

                if (!File.Exists(osz2FilenameOld))
                {
                    log("couldn't find local file {0}", osz2FilenameOld);
                    return (int)UpdateResponseCode.FileDoesNotExist;
                }

                using (MapPackage oldPackage = new MapPackage(osz2FilenameOld, OldKey, false, false))
                using (MapPackage newPackage = new MapPackage(osz2FilenameNew, NewKey, true, false))
                {

                    Dictionary<MapMetaType, string> metaData = oldPackage.GetAllMetadata();

                    foreach (KeyValuePair<MapMetaType, string> mapMeta in metaData)
                        newPackage.AddMetadata(mapMeta.Key, mapMeta.Value);

                    List<FileInfo> fileInfo = oldPackage.GetFileInfo();
                    foreach (FileInfo fo in fileInfo)
                        using (var br = new BinaryReader(oldPackage.GetFile(fo.Filename)))
                        {
                            newPackage.AddFile(fo.Filename, br.ReadBytes((int)br.BaseStream.Length),
                                fo.CreationTime, fo.ModifiedTime);
                        }

                    newPackage.Save();
                }

                s3putFile("osz2/" + TargetPackage, osz2FilenameNew);
                File.Delete(osz2FilenameNew);

            }
            catch (Exception e)
            {
                File.Delete(osz2FilenameOld);
                File.Delete(osz2FilenameNew);
                log(e);
            }

            return (int)UpdateResponseCode.UpdateSuccessful;
        }

        /// <summary>
        ///  Calls a MapPackageAction in a safe environment by catching all the exceptions
        ///  and translating them to appropiate responsecodes.
        /// </summary>
        /// <param name="Action">A MapPackageAction which updates the osz2 file in the save environment</param>
        /// <param name="BeatmapSetID">The filename of the osz2 package.</param>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <param name="SaveData">Whether to save the data after using the custom defined action on the beatmap</param>
        /// <returns>the UpdateResponseCode of the custom defined action when no exception has occured
        ///  while loading the beatmap(osz2) file, or a failed UpdateResponseCode when there has.</returns>
        private UpdateResponseCode DoMapPackageActionSafe(MapPackageAction Action, int BeatmapSetID, byte[] Key, bool SaveData, bool MetadataOnly)
        {
            return DoMapPackageActionSafe(Action, BeatmapSetID + ".osz2", Key, SaveData, MetadataOnly);
        }

        /// <summary>
        ///  Calls a MapPackageAction in a safe environment by catching all the exceptions
        ///  and translating them to appropiate responsecodes.
        /// </summary>
        /// <param name="Action">A MapPackageAction which updates the osz2 file in the save environment</param>
        /// <param name="MappackageFile">Path to the osz2 fileparam>
        /// <param name="Key">The key used to decrypt or encrypt the mappackage</param>
        /// <param name="SaveData">Whether to save the data after using the custom defined action on the beatmap</param>
        /// <returns>the UpdateResponseCode of the custom defined action when no exception has occured
        ///  while loading the beatmap(osz2) file, or a failed UpdateResponseCode when there has.</returns>
        private UpdateResponseCode DoMapPackageActionSafe(MapPackageAction Action, string MappackageFile, byte[] Key, bool SaveData, bool MetadataOnly)
        {
            string originalPath = MappackageFile;

            if (!File.Exists(MappackageFile))
                MappackageFile = s3getToTemp("osz2/" + MappackageFile);
            else
                originalPath = Path.GetFileName(originalPath);

            UpdateResponseCode CustomResponseCode;
            if (!File.Exists(MappackageFile))
                return UpdateResponseCode.FileDoesNotExist;
            try
            {
                using (MapPackage Osz2Beatmap = new MapPackage(MappackageFile, Key, false, MetadataOnly))
                {
                    CustomResponseCode = Action(Osz2Beatmap);
                    if (CustomResponseCode == UpdateResponseCode.UpdateSuccessful && SaveData)
                        Osz2Beatmap.Save();
                    Osz2Beatmap.Close();
                }

                if (CustomResponseCode == UpdateResponseCode.UpdateSuccessful && SaveData) s3putFile("osz2/" + originalPath, MappackageFile);
            }
            catch (IOException e)
            {
                log(e);
                return UpdateResponseCode.Osz2Corrupted;
            }
            catch (Exception e)
            {
                log(e);
                return UpdateResponseCode.UnknownError;
            }
            finally
            {
            }

            return CustomResponseCode;
        }
    }
}