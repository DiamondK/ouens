﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace osudata.Libraries.FFmpeg
{
    public class VideoDecoder : IDisposable
    {
        #region Delegates

        public delegate void EndOfFileHandler();

        #endregion

        private const int PIXEL_FORMAT = (int)FFmpeg.PixelFormat.PIX_FMT_RGB32;

        private IntPtr buffer;
        public int BufferSize;

        private FFmpeg.AVCodecContext codecCtx;
        private double currentDisplayTime;
        private Thread decodingThread;
        private FFmpeg.AVFormatContext formatContext;
        public double FrameDelay;
        private int frameFinished;
        private bool isDisposed;
        private double lastPts = 0;
        private IntPtr packet;
        private IntPtr pCodec;
        private IntPtr pCodecCtx;
        private IntPtr pFormatCtx;
        private IntPtr pFrame;
        private IntPtr pFrameRGB;
        private FFmpeg.AVStream stream;
        private GCHandle streamHandle;
        private bool videoOpened;
        private int videoStream;
        //private Queue<VideoFrame> waitingFrames = new Queue<VideoFrame>();
        private double[] FrameBufferTimes;
        private byte[][] FrameBuffer;

        public VideoDecoder(int bufferSize)
        {
            BufferSize = bufferSize;

            FrameBufferTimes = new double[BufferSize];
            FrameBuffer = new byte[BufferSize][];

            FFmpeg.av_register_all();
            videoOpened = false;
        }

        public double Length
        {
            get
            {
                long duration = stream.duration;
                if (duration < 0) return 36000000;
                return duration * FrameDelay;
            }
        }

        public int width
        {
            get { return codecCtx.width; }
        }

        public int height
        {
            get { return codecCtx.height; }
        }

        public double CurrentTime
        {
            get { return currentDisplayTime; }
        }

        private double startTimeMs
        {
            get { return 1000 * stream.start_time * FrameDelay; }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion

        ~VideoDecoder()
        {
            Dispose(false);
        }

        public void Dispose(bool disposing)
        {
            if (isDisposed) return;
            isDisposed = true;

            if (decodingThread != null)
                decodingThread.Abort();

            try
            {
                Marshal.FreeHGlobal(packet);
                Marshal.FreeHGlobal(buffer);
            }
            catch
            {
            }

            try
            {
                FFmpeg.av_free(pFrameRGB);
                FFmpeg.av_free(pFrame);
            }
            catch
            {
            }

            try
            {
                if (streamHandle != null) streamHandle.Free();
            }
            catch { }

            frameFinished = 0;

            try
            {
                FFmpeg.avcodec_close(pCodecCtx);
                FFmpeg.av_close_input_file(pFormatCtx);
            }
            catch
            {
            }
        }

        public bool Open(string path)
        {
            FileStream str = File.OpenRead(path);
            OpenStream(str);

            return true;
        }

        int writeCursor;
        int readCursor;

        public bool Open(byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0) return false;

            streamHandle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            IntPtr ptr = streamHandle.AddrOfPinnedObject();

            if (videoOpened) return false;

            videoOpened = true;

            string path = "memory:" + ptr + "|" + bytes.Length;

            int ret = FFmpeg.av_open_input_file(out pFormatCtx, path, IntPtr.Zero, bytes.Length, IntPtr.Zero);

            if (ret != 0)
                throw new Exception("Couldn't open input file");

            ret = FFmpeg.av_find_stream_info(pFormatCtx);

            if (ret < 0)
                throw new Exception("Couldn't find stream info");

            FFmpeg.dump_format(pFormatCtx, 0, path, 0);

            formatContext =
                (FFmpeg.AVFormatContext)Marshal.PtrToStructure(pFormatCtx, typeof(FFmpeg.AVFormatContext));

            videoStream = -1;
            int nbStreams = formatContext.nb_streams;

            for (int i = 0; i < nbStreams; i++)
            {
                FFmpeg.AVStream str = (FFmpeg.AVStream)
                                      Marshal.PtrToStructure(formatContext.streams[i], typeof(FFmpeg.AVStream));
                FFmpeg.AVCodecContext codec = (FFmpeg.AVCodecContext)
                                              Marshal.PtrToStructure(str.codec, typeof(FFmpeg.AVCodecContext));

                if (codec.codec_type == FFmpeg.CodecType.CODEC_TYPE_VIDEO)
                {
                    videoStream = i;
                    stream = str;
                    codecCtx = codec;
                    pCodecCtx = stream.codec;
                    break;
                }
            }
            if (videoStream == -1)
                throw new Exception("couldn't find video stream");

            FrameDelay = FFmpeg.av_q2d(stream.time_base);

            pCodec = FFmpeg.avcodec_find_decoder(codecCtx.codec_id);

            if (pCodec == IntPtr.Zero)
                throw new Exception("couldn't find decoder");

            if (FFmpeg.avcodec_open(pCodecCtx, pCodec) < 0)
                throw new Exception("couldn't open codec");

            pFrame = FFmpeg.avcodec_alloc_frame();
            pFrameRGB = FFmpeg.avcodec_alloc_frame();

            if (pFrameRGB == IntPtr.Zero)
                throw new Exception("couldn't allocate RGB frame");

            int numBytes = FFmpeg.avpicture_get_size(PIXEL_FORMAT, codecCtx.width, codecCtx.height);
            buffer = Marshal.AllocHGlobal(numBytes);

            FFmpeg.avpicture_fill(pFrameRGB, buffer, PIXEL_FORMAT, codecCtx.width, codecCtx.height);

            packet = Marshal.AllocHGlobal(57); // 52 = size of packet struct

            for (int i = 0; i < BufferSize; i++)
                FrameBuffer[i] = new byte[width * height * 4];

            decodingThread = new Thread(Decode);
            decodingThread.IsBackground = true;
            decodingThread.Start();

            return true;
        }

        private void Decode()
        {
            try
            {
                while (true)
                {
                    bool gotNewFrame = false;

                    lock (this)
                    {
                        while (writeCursor - readCursor < BufferSize && FFmpeg.av_read_frame(pFormatCtx, packet) >= 0)
                        {
                            if (Marshal.ReadInt32(packet, 24) == videoStream)
                            {
                                //double pts = Marshal.ReadInt64(packet, 0);
                                double dts = Marshal.ReadInt64(packet, 8);

                                IntPtr data = Marshal.ReadIntPtr(packet, 16); // 16 = offset of data
                                int size = Marshal.ReadInt32(packet, 20); // 20 = offset of size

                                FFmpeg.avcodec_decode_video(pCodecCtx, pFrame, ref frameFinished, data, size);

                                if (frameFinished != 0 && Marshal.ReadIntPtr(packet, 16) != IntPtr.Zero)
                                {
                                    int correct = FFmpeg.img_convert(pFrameRGB, PIXEL_FORMAT, pFrame,
                                                                     (int)codecCtx.pix_fmt, codecCtx.width,
                                                                     codecCtx.height);

                                    //if (Marshal.ReadIntPtr(packet, 16) != IntPtr.Zero) // packet->data != null
                                    if (correct == 0)
                                    {
                                        Marshal.Copy(Marshal.ReadIntPtr(pFrameRGB), FrameBuffer[writeCursor % BufferSize], 0, FrameBuffer[writeCursor % BufferSize].Length);
                                        FrameBufferTimes[writeCursor % BufferSize] = (dts - stream.start_time) * FrameDelay * 1000;

                                        writeCursor++;

                                        lastPts = dts;

                                        gotNewFrame = true;
                                    }

                                    //FFmpeg.av_free(data);
                                }
                            }
                        }
                    }

                    if (!gotNewFrame)
                        Thread.Sleep(15);
                }
            }
            catch (ThreadAbortException)
            {
                return;
            }
            catch (Exception e)
            {
                using (StreamWriter sw = File.CreateText("video-debug.txt"))
                {
                    sw.WriteLine(e.ToString());
                }
            }
        }

        public bool OpenStream(Stream inStream)
        {
            byte[] bytes = new byte[inStream.Length];
            inStream.Read(bytes, 0, (int)inStream.Length);
            return Open(bytes);
        }

        /// <summary>
        /// Decodes one frame.
        /// Returns null if decoding full video has finished.
        /// </summary>
        /// <returns></returns>
        public byte[] GetFrame(int time)
        {
            while (readCursor < writeCursor - 1 && FrameBufferTimes[(readCursor + 1) % BufferSize] <= time)
                readCursor++;


            if (readCursor < writeCursor)
            {
                currentDisplayTime = FrameBufferTimes[readCursor % BufferSize];
                return FrameBuffer[readCursor % BufferSize];
            }

            return null;
        }

        public void Seek(int time)
        {
            lock (this)
            {
                int flags = 0;
                double timestamp = (double)time / 1000 / FrameDelay + stream.start_time;
                if (timestamp < lastPts)
                    flags = FFmpeg.AVSEEK_FLAG_BACKWARD;
                FFmpeg.av_seek_frame(pFormatCtx, videoStream, (long)timestamp, flags);
                readCursor = 0;
                writeCursor = 0;
            }
        }
    }
}