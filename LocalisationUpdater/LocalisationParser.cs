﻿using System;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;

namespace LocalisationUpdater
{
    public class LocalisationParser
    {
        public string Text;
        public string FilePath;
        public bool TextModified;

        string prefix;
        public string Prefix
        {
            get { return prefix; }
            set
            {
                prefix = replaceInvalidChars(value);
                if (!prefix.EndsWith("_"))
                    prefix += '_';
            }
        }
        public string Variable = "";
        string key;
        public string Key
        {
            get { return key; }
            set { key = removeInvalidChars(value); }
        }
        public string FullKey
        {
            get { return Prefix + Key; }
        }

        public int Offset = 0;
        public int Line = 1;

        int lastLine = 0;
        string context;
        public string Context
        {
            get
            {
                if (Line == lastLine)
                    return context;

                int lineStart = Offset > 0 ? Text.LastIndexOf('\n', Offset - 1) + 1 : 0;
                int lineEnd = Text.IndexOf('\n', Offset);
                if (lineEnd < 0)
                    lineEnd = Text.Length;

                lastLine = Line;
                return context = Text.Substring(lineStart, lineEnd - lineStart).Trim();
            }
        }

        public TokenType Type = TokenType.None;
        public string Token = null;

        public string String
        {
            get
            {
                switch (Type)
                {
                    case TokenType.String:
                        return Token.Substring(1, Token.Length - 2);
                    case TokenType.AtString:
                        return Token.Substring(2, Token.Length - 3);
                    default:
                        return Token;
                }
            }
        }

        public LocalisationParser(string text, string prefix)
        {
            Text = text;
            Prefix = prefix;
            Key = "";
        }

        public LocalisationParser(string file)
            : this(File.ReadAllText(file), Path.GetFileNameWithoutExtension(file))
        {
            FilePath = file;
        }

        public bool NextString()
        {
            UpdateFile();

            while (NextToken())
            {
                if (Context.Contains("SkinManager.") || Context.Contains("AudioEngine.") || Context.Contains(".Name"))
                    continue;

                switch (Type)
                {
                    case TokenType.Identifier:
                        Variable = Token;
                        break;
                    case TokenType.String:
                        switch (String.Length)
                        {
                            case 1:
                                ReplaceToken("@\"" + String + "\"");
                                continue;
                            case 0:
                                ReplaceToken("string.Empty");
                                continue;
                        }

                        Key = String;
                        if (Key.Length > 50)
                            Key = Variable;

                        return true;
                }
            }

            return false;
        }

        private static Regex searchRegex = new Regex
        (@"
            \G
            (?:
                # Block comment
                (?:
                    /\* (?s: .*? ) \*/
                )
                |
                # Line comment
                (?:
                    // .* \n
                )
                |
                # Advance search by one character
                (?s: . )
            )*?
            (?:
                # Identifier
                (
                    \w+ (?: \.\w+ )*
                )
                |
                # String literal
                (""
                    (?: [^""\n\\] | \\. )*
                "")
                |
                # At-string literal
                (@""
                    (?: [^""] | """" )*
                "")
                |
                # Character literal
                ('
                    \\? .
                ')
            )
        ", RegexOptions.IgnorePatternWhitespace);

        public bool NextToken()
        {
            // move past current token
            if (Token != null)
                increaseOffsetBy(Token);

            Match m = searchRegex.Match(Text, Offset);
            if (!m.Success)
            {
                Token = null;
                Type = TokenType.None;
                return false;
            }

            // Find which capture group matched something
            int i;
            for (i = 1; i < (int)TokenType.NumberOfTypes; i++)
            {
                if (m.Groups[i].Success)
                    break;
            }

            Group g = m.Groups[i];
            Token = g.Value;
            Type = (TokenType)i;

            // Only move offset to the start of the token, since we might replace the token
            increaseOffsetBy(m.Value.Substring(0, m.Length - g.Length));

            return true;
        }

        private void increaseOffsetBy(string str)
        {
            Offset += str.Length;
            Line += str.Count(x => x == '\n');
        }

        public void ReplaceToken(string replacement)
        {
            if (Token == null)
                return;

            Text = Text.Remove(Offset, Token.Length).Insert(Offset, replacement);
            Token = replacement;
            TextModified = true;
            lastLine = 0; // force a context update
        }

        public void ReplaceAll(string replacement)
        {
            if (Token == null)
                return;

            // save our current state so we can return to it
            string token = Token;
            TokenType type = Type;
            int offset = Offset;
            int line = Line;

            do
            {
                if (Type == type && Token == token)
                    ReplaceToken(replacement);
            }
            while (NextToken());

            Token = replacement;
            Type = type;
            Offset = offset;
            Line = line;
        }

        public void ReplaceStringWithKey()
        {
            ReplaceAll("LocalisationManager.GetString(OsuString." + FullKey + ")");
        }

        public void ReplaceStringWithConstant()
        {
            string escaped = escapeString(String);
            if (escaped != null)
                ReplaceAll("@\"" + escaped + "\"");
        }

        public void UpdateFile()
        {
            if (FilePath != null && TextModified)
            {
                File.WriteAllText(FilePath, Text);
                TextModified = false;
            }
        }

        private static string escapeString(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '\\')
                {
                    switch (str[i + 1])
                    {
                        case '\\':
                            str = str.Remove(i, 1);
                            break;
                        case '"':
                            str = str.Remove(i, 1);
                            str = str.Insert(i, "\"");
                            break;
                        default:
                            return null;
                    }
                }
            }
            return str;
        }

        private static Regex notLetters = new Regex(@"\P{L}+");

        private static string replaceInvalidChars(string str)
        {
            return ucfirst(string.Join("_", notLetters.Split(str)));
        }

        private static string removeInvalidChars(string str)
        {
            return string.Join("", notLetters.Split(str).Select(x => ucfirst(x)));
        }

        private static string ucfirst(string str)
        {
            if (str == null)
                return null;

            if (str.Length <= 1)
                return str.ToUpper();

            return char.ToUpper(str[0]) + str.Substring(1);
        }
    }

    public enum TokenType
    {
        None,
        Identifier,
        String,
        AtString,
        Character,
        NumberOfTypes
    };
}
