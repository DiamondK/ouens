﻿using System;
using System.Collections.Generic;
using System.Linq;
using Google.GData.Spreadsheets;
using Google.GData.Client;
using System.IO;
using System.Configuration;

namespace LocalisationUpdater
{
    class LocalisationUpdater
    {
        internal static string osu_path;

        static SpreadsheetsService ss = new SpreadsheetsService("osu-localisation");
        static SpreadsheetEntry sheet;

        static void Main(string[] args)
        {
            osu_path = Environment.CurrentDirectory;
            while (!File.Exists(osu_path + @"\\osu!.sln"))
            {
                int index = osu_path.LastIndexOf('\\');
                if (index < 0)
                {
                    Console.WriteLine("Failed to find directory containing osu.sln");
                    Console.WriteLine("Falling back to current directory");
                    osu_path = Environment.CurrentDirectory;
                    break;
                }
                else
                    osu_path = osu_path.Remove(index);
            }
            osu_path += '\\';

            bool virgin = true;

        Start:

            Console.Write("[U]pdate [E]xtract Extract[S]ingle: ");
            string response = Console.ReadLine().Trim();

            if (virgin)
            {
                InitWorksheet();
                virgin = false;
            }

            switch (response.ToUpper())
            {
                default:
                    ProcessWorksheet();
                    break;
                case "E":
                    FindCodeAndRunProcess();
                    break;
                case "S":
                    Console.Write("Filename: ");
                    string search = Console.ReadLine().Trim();
                    FindCodeAndRunProcess(search);
                    break;
            }

            Console.WriteLine("Finished!");
            Console.WriteLine();
            Console.WriteLine("Press R to restart");
            response = Console.ReadLine().Trim();

            if (response.ToUpper() == "R")
            {
                Console.Clear();
                goto Start;
            }
        }

        private static void FindCodeAndRunProcess(string search = null)
        {
            List<string> files = new List<string>();
            FindCodeRecursive(osu_path, files, search);
            RunProcess(files);
        }

        private static void FindCodeRecursive(string dir, List<string> output, string search = null)
        {
            foreach (string subdir in Directory.GetDirectories(dir))
                FindCodeRecursive(subdir, output, search);

            output.AddRange(Directory.GetFiles(dir, search != null ? "*" + search + "*.cs" : "*.cs"));
        }

        private static void RunProcess(List<string> fileList)
        {
            foreach (string file in fileList)
            {
            FileStart:
                LocalisationParser parser = new LocalisationParser(file);
                while (parser.NextString())
                {
                StringStart:
                    Console.Clear();
                    Console.WriteLine("File: " + file.Remove(0, osu_path.Length));
                    Console.WriteLine("Key: " + parser.FullKey);
                    Console.WriteLine("String: " + parser.Token);
                    Console.WriteLine("Context: " + string.Format("L{0}: {1}", parser.Line, parser.Context.Trim()));
                    Console.WriteLine();
                    Console.Write("[U]se [K]ey [C]onstant [S]kip [P]refix [R]estart Skip[F]ile [Q]uit: ");
                    string s = Console.ReadLine().Trim();
                    Console.WriteLine();
                    switch (s.ToUpper())
                    {
                        default:
                            goto StringStart;
                        case "S":
                            continue;
                        case "R":
                            goto FileStart;
                        case "F":
                            goto FileEnd;
                        case "Q":
                            goto ProcessEnd;
                        case "":
                        case "U":
                            AddNewLocalisation(parser.FullKey, parser.String);
                            parser.ReplaceStringWithKey();
                            break;
                        case "C":
                            parser.ReplaceStringWithConstant();
                            break;
                        case "K":
                            Console.WriteLine("Leave blank to cancel");
                            Console.Write("Key: " + parser.Prefix);
                            string key = Console.ReadLine().Trim();
                            if (!string.IsNullOrEmpty(key))
                                parser.Key = key;

                            goto StringStart;
                        case "P":
                            Console.WriteLine("Leave blank to cancel");
                            Console.Write("Prefix: ");
                            string prefix = Console.ReadLine().Trim();
                            if (!string.IsNullOrEmpty(prefix))
                                parser.Prefix = prefix;

                            goto StringStart;
                    }
                    parser.UpdateFile();
                }
            FileEnd: ;
            }
        ProcessEnd: ;
        }

        private static void InitWorksheet()
        {
            Console.WriteLine("Connecting to google docs...");

            ss.setUserCredentials(ConfigurationManager.AppSettings["docs-username"], ConfigurationManager.AppSettings["docs-password"]);

            SpreadsheetFeed feed = ss.Query(new SpreadsheetQuery());
            sheet = null;

            Console.WriteLine("Finding spreadsheet...");
            foreach (SpreadsheetEntry entry in feed.Entries)
            {
                if (entry.Title.Text == "osu! localisation")
                {
                    sheet = entry;
                    break;
                }
            }

            if (sheet == null)
            {
                Console.WriteLine("Failed to get spreadsheet");
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
                Environment.Exit(0);
            }

            Console.WriteLine("Retrieving contents...");
            WorksheetEntry worksheet = GetWorksheet();
        }

        private static WorksheetEntry GetWorksheet()
        {
            AtomLink link = sheet.Links.FindService(GDataSpreadsheetsNameTable.WorksheetRel, null);
            WorksheetQuery query = new WorksheetQuery(link.HRef.ToString());
            WorksheetFeed wfeed = ss.Query(query);
            return wfeed.Entries[0] as WorksheetEntry;
        }

        private static void AddNewLocalisation(string key, string text)
        {
            WorksheetEntry worksheet = GetWorksheet();

            uint row = ++worksheet.Rows;
            worksheet.Update();

            CellQuery cellQuery = new CellQuery(worksheet.CellFeedLink);
            cellQuery.MinimumRow = row;
            cellQuery.ReturnEmpty = ReturnEmptyCells.yes;
            cellQuery.MinimumColumn = 1;
            cellQuery.MaximumColumn = 2;
            CellFeed cellFeed = ss.Query(cellQuery);

            foreach (CellEntry cell in cellFeed.Entries)
            {
                switch (cell.Column)
                {
                    case 1:
                        cell.InputValue = key;
                        break;
                    case 2:
                        cell.InputValue = text;
                        break;
                }

                cell.Update();
            }
        }

        private static List<LanguageCodeWriter> writers = new List<LanguageCodeWriter>();

        private static void ProcessWorksheet()
        {
            WorksheetEntry worksheet = GetWorksheet();

            AtomLink cellFeedLink = worksheet.Links.FindService(GDataSpreadsheetsNameTable.CellRel, null);

            List<string> keys = new List<string>();

            CellQuery query = new CellQuery(cellFeedLink.HRef.ToString());
            CellFeed feed = ss.Query(query);

            Console.WriteLine("Processing cells...");

            string currentKey = null;
            foreach (CellEntry cell in feed.Entries)
            {
                switch (cell.Column)
                {
                    case 1:
                        if (cell.Value == null || cell.Value.Contains(' '))
                            currentKey = null;
                        else if (cell.Row < 5)
                            break;
                        else
                        {
                            Console.WriteLine("Processing key: " + cell.Value);
                            keys.Add(currentKey = cell.Value);
                            if (currentKey.Contains("DONT_TRANSLATE"))
                                currentKey = null;
                        }
                        break;
                    default:
                        if (cell.Row == 2)
                        {
                            //langauge codes
                            writers.Add(new LanguageCodeWriter(cell.Value));
                            continue;
                        }

                        if (cell.Row < 5) break;

                        if (writers.Count > 0 && currentKey != null)
                        {
                            if (cell.Value == null) continue;

                            string content = cell.Value.Trim(' ', '\n', '\r', '\t').Replace("\n", "\\n");

                            if (content.IndexOf(@"\\") >= 0)
                            {
                                Console.WriteLine("key {0} translation {1} failed with excess escape characters", currentKey, content);
                                Console.ReadLine();
                            }

                            try
                            {
                                int argCount = content.Count<char>(c => c == '{');
                                string[] args = new string[argCount];
                                for (int i = 0; i < argCount; i++)
                                    args[i] = "test";

                                string test = string.Format(content, args);
                            }
                            catch
                            {
                                Console.WriteLine("key {0} translation {1} failed with string format", currentKey, content);
                                Console.ReadLine();
                            }

                            writers[(int)cell.Column - 2].Add(currentKey, content);
                        }

                        break;
                }
            }

            //write enum out to .cs file
            using (TextWriter w = new StreamWriter(osu_path + @"osu!common\Helpers\LocalisationManager_Strings.cs"))
            {
                //header
                w.WriteLine("namespace osu_common.Helpers");
                w.WriteLine("{");
                w.WriteLine("    public enum OsuString");
                w.WriteLine("    {");
                foreach (string s in keys)
                    w.WriteLine("        {0},", s);
                //footer
                w.WriteLine("    }");
                w.WriteLine("}");
            }
        }
    }

    public class LanguageCodeWriter
    {
        string Code;
        string Filename;

        StreamWriter writer;

        internal LanguageCodeWriter(string code)
        {
            Code = code;
            Filename = LocalisationUpdater.osu_path + @"osu!\Localisation\" + code + ".txt";

            File.Delete(Filename);
            writer = File.CreateText(Filename);
            writer.AutoFlush = true;
        }

        internal void Add(string key, string val)
        {
            writer.Write(key + "=" + val + "\n");
        }
    }
}
